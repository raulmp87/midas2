package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

/**
 * Clase que sirve como contenedor de la informaci\u00f3n
 * del reporte de historial de póliza por id agente.
 * 
 * @author AFIRME
 * 
 * @since 28072016
 * 
 * @version 1.0
 *
 */
public class ReporteHistorialPolizaDTO implements Serializable{
	
	private static final long serialVersionUID = 2172573311789386567L;
	
	private Long numCotizacion;
    private Long numCentroEmis;
    private Long numPoliza;
    private String sitPoliza;
    private Long numRenovPol;
    private Long numEndoso;
    private String cvetDoctoVerp;
    private Long numLinNegocio;
    private Long numInciso;
    private String sitIncisoPol;
    private String motSitIncPol;
    private Long numContratante;
    private String fechaEmision;
    private String horaEmision;
    private Date iniVigencia;
    private Date finVigencia;
    private Long numAgente;
    private Long numMoneda;
    private String cveAmis;
    private String descVehiculo;
    private Long modelo;
    private Double primaEndoso;
    private Double primaInciCot;
    private Double primaDM;
    private Double primaRT;
    private Double primaRC;
    private Double saRC;
    private Double primaGM;
    private Double saGM;
    private Double primaEE;
    private Double saEndoso;
    private Double primaAJ;
    private Double primaAV;
    private Double primaExecdeDM;
    private Double primaPerdidaTotalDM;
    private Double primaAccCond;
    private Double saAccCond;
    private Double primaExtencionRC;
    private Double primaAdapacionConversion;
    private Double saAdapacionConversion;
    private Double primaExecdedRT;
    private String nombreConductor;
    private String nombreTitular;
    private String serie;
    private String motor;
    private String placas;
    private Double pctBonifComis;
    private Double pctDesctoPol;
    private Double factDctoIgpmCob;
    private Double factDctoIgpmEnd;
    private Double factDctoIgpmPol;
    private Double pctDescCobDM;
    private Double pctDescCobRT;
    private Double pctDescCobRC;
    private Double pctDescCobGM;
    private Double pctDescBobEnd;
    private Double pctDescCobAJ;
    private Double pctDescCobAV;
    private Double pctDescCobExecdedDM;
    private Double pctDescCobptDM;
    private Double pctDescCobAccCond;
    private Double pctDescCobextRC;
    private Double pctDescCobAdapConv;
    private Double pctDescCobexecdedRT;
    private String idOficina;
    private String nombreOficina;
    private String idGerencia;
    private String nombreGerencia;
    private Long idCentroOper;
    private String nomCentroOper;
    private String sitIncisoPolAct;
    private String nomProducto;
    private String nomContratante;
    private String sistemaEmision;
    private String polRCExt;
    private Long valorVehiculo;
    private Double deducibleDM;
    private Double deducibleRT;
    private Double deducibleRC;
    private String asistenciaJuridica;
    private String asistenciaVial;
    private String exencionDM;
    private String exencionRT;
    private String extencionRC;
    private Double rcExcesoMuerte;
    private Double saRcExcesoMuerte;
    private Double deducibleRCExmu;
    private Long capacidadPasajeros;
    private String servicio;
    private String uso;
    private String tipoMercado;
    private Long numeroRemolques;
    private Double financiamiento;
    private Double gastosExpedicion;
    private String formaPago;
    private Double iva;
    private String adaptacionesConversiones;
    private String equipoEspecial;
    private String idTopoliza;
    private Long idInciso;
    private Long idCotizacion;
    private Long idLinNegocio;
    private Long idRTEndoso;
    private Long idSolicitud;
    private String observacionesEndoso;
    private String observacionesInciso;
    private Long idTitular;
    private String cvelServicioAuto;
    private String cvelUsoAuto;
    private String tipoMercancia;
    private Long idFormaPago;
    private String descripcionEndoso;
    private String cvetEndoso;
    private Long idPaquete;
    private String descripcionPaquete;
    private Long idVersionPoliza;

	public ReporteHistorialPolizaDTO() {
	}

	@Exportable(columnName = "NUMCOTIZACION", columnOrder = 0)
	public Long getNumCotizacion() {
		return numCotizacion;
	}

	public void setNumCotizacion(Long numCotizacion) {
		this.numCotizacion = numCotizacion;
	}

	@Exportable(columnName = "NUMCENTROEMIS", columnOrder = 1)
	public Long getNumCentroEmis() {
		return numCentroEmis;
	}

	public void setNumCentroEmis(Long numCentroEmis) {
		this.numCentroEmis = numCentroEmis;
	}

	@Exportable(columnName = "NUMPOLIZA", columnOrder = 2)
	public Long getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}

	@Exportable(columnName = "SITPOLIZA", columnOrder = 3)
	public String getSitPoliza() {
		return sitPoliza;
	}

	public void setSitPoliza(String sitPoliza) {
		this.sitPoliza = sitPoliza;
	}

	@Exportable(columnName = "NUM_RENOV_POLIZA", columnOrder = 4)
	public Long getNumRenovPol() {
		return numRenovPol;
	}

	public void setNumRenovPol(Long numRenovPol) {
		this.numRenovPol = numRenovPol;
	}

	@Exportable(columnName = "NUMENDOSO", columnOrder = 5)
	public Long getNumEndoso() {
		return numEndoso;
	}

	public void setNumEndoso(Long numEndoso) {
		this.numEndoso = numEndoso;
	}
	
	@Exportable(columnName = "DESC_ENDOSO", columnOrder = 6)
	public String getDescripcionEndoso() {
		return descripcionEndoso;
	}

	public void setDescripcionEndoso(String descripcionEndoso) {
		this.descripcionEndoso = descripcionEndoso;
	}

	@Exportable(columnName = "CVE_T_DOCTO_VERP", columnOrder = 7)
	public String getCvetDoctoVerp() {
		return cvetDoctoVerp;
	}

	public void setCvetDoctoVerp(String cvetDoctoVerp) {
		this.cvetDoctoVerp = cvetDoctoVerp;
	}

	@Exportable(columnName = "NUMLINNEGOCIO", columnOrder = 8)
	public Long getNumLinNegocio() {
		return numLinNegocio;
	}

	public void setNumLinNegocio(Long numLinNegocio) {
		this.numLinNegocio = numLinNegocio;
	}

	@Exportable(columnName = "NUMINCISO", columnOrder = 9)
	public Long getNumInciso() {
		return numInciso;
	}

	public void setNumInciso(Long numInciso) {
		this.numInciso = numInciso;
	}

	@Exportable(columnName = "SITINCISO", columnOrder = 10)
	public String getSitIncisoPol() {
		return sitIncisoPol;
	}

	public void setSitIncisoPol(String sitIncisoPol) {
		this.sitIncisoPol = sitIncisoPol;
	}

	@Exportable(columnName = "MOT_SIT_INC_POL", columnOrder = 11)
	public String getMotSitIncPol() {
		return motSitIncPol;
	}

	public void setMotSitIncPol(String motSitIncPol) {
		this.motSitIncPol = motSitIncPol;
	}

	@Exportable(columnName = "NUMCONTRATANTE", columnOrder = 12)
	public Long getNumContratante() {
		return numContratante;
	}

	public void setNumContratante(Long numContratante) {
		this.numContratante = numContratante;
	}

	@Exportable(columnName = "FECH_EMISION", columnOrder = 13)
	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	@Exportable(columnName = "HORA_EMISION", columnOrder = 14)
	public String getHoraEmision() {
		return horaEmision;
	}

	public void setHoraEmision(String horaEmision) {
		this.horaEmision = horaEmision;
	}

	@Exportable(columnName = "F_INI_VIGENCIA", columnOrder = 15)
	public Date getIniVigencia() {
		return iniVigencia;
	}

	public void setIniVigencia(Date iniVigencia) {
		this.iniVigencia = iniVigencia;
	}

	@Exportable(columnName = "F_FIN_VIGENCIA", columnOrder = 16)
	public Date getFinVigencia() {
		return finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

	@Exportable(columnName = "NUMNUMAGENTE", columnOrder = 17)
	public Long getNumAgente() {
		return numAgente;
	}

	public void setNumAgente(Long numAgente) {
		this.numAgente = numAgente;
	}

	@Exportable(columnName = "NUM_MONEDA", columnOrder = 18)
	public Long getNumMoneda() {
		return numMoneda;
	}

	public void setNumMoneda(Long numMoneda) {
		this.numMoneda = numMoneda;
	}

	@Exportable(columnName = "CVE_AMIS", columnOrder = 19)
	public String getCveAmis() {
		return cveAmis;
	}

	public void setCveAmis(String cveAmis) {
		this.cveAmis = cveAmis;
	}

	@Exportable(columnName = "DESC_VECHICULO", columnOrder = 20)
	public String getDescVehiculo() {
		return descVehiculo;
	}

	public void setDescVehiculo(String descVehiculo) {
		this.descVehiculo = descVehiculo;
	}

	@Exportable(columnName = "MODELO", columnOrder = 21)
	public Long getModelo() {
		return modelo;
	}

	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}

	@Exportable(columnName = "PRIMA_ENDOSO", columnOrder = 24, format="$#,##0.00")
	public Double getPrimaEndoso() {
		return primaEndoso;
	}

	public void setPrimaEndoso(Double primaEndoso) {
		this.primaEndoso = primaEndoso;
	}

	@Exportable(columnName = "PRIMA_INCICO_T", columnOrder = 25,  format="$#,##0.00")
	public Double getPrimaInciCot() {
		return primaInciCot;
	}

	public void setPrimaInciCot(Double primaInciCot) {
		this.primaInciCot = primaInciCot;
	}

	@Exportable(columnName = "PRIMA_DM", columnOrder = 26, format="$#,##0.00")
	public Double getPrimaDM() {
		return primaDM;
	}

	public void setPrimaDM(Double primaDM) {
		this.primaDM = primaDM;
	}

	@Exportable(columnName = "PRIMA_RT", columnOrder = 27, format="$#,##0.00")
	public Double getPrimaRT() {
		return primaRT;
	}

	public void setPrimaRT(Double primaRT) {
		this.primaRT = primaRT;
	}

	@Exportable(columnName = "PRIMA_RC", columnOrder = 28, format="$#,##0.00")
	public Double getPrimaRC() {
		return primaRC;
	}

	public void setPrimaRC(Double primaRC) {
		this.primaRC = primaRC;
	}

	@Exportable(columnName = "PRIMA_RC", columnOrder = 29, format="$#,##0.00")
	public Double getSaRC() {
		return saRC;
	}

	public void setSaRC(Double saRC) {
		this.saRC = saRC;
	}

	@Exportable(columnName = "PRIMA_GM", columnOrder = 30, format="$#,##0.00")
	public Double getPrimaGM() {
		return primaGM;
	}

	public void setPrimaGM(Double primaGM) {
		this.primaGM = primaGM;
	}

	@Exportable(columnName = "SA_GM", columnOrder = 31, format="$#,##0.00")
	public Double getSaGM() {
		return saGM;
	}

	public void setSaGM(Double saGM) {
		this.saGM = saGM;
	}

	@Exportable(columnName = "PRIMA_EE", columnOrder = 32, format="$#,##0.00")
	public Double getPrimaEE() {
		return primaEE;
	}

	public void setPrimaEE(Double primaEE) {
		this.primaEE = primaEE;
	}

	@Exportable(columnName = "SA_EE", columnOrder = 33, format="$#,##0.00")
	public Double getSaEndoso() {
		return saEndoso;
	}

	public void setSaEndoso(Double saEndoso) {
		this.saEndoso = saEndoso;
	}

	@Exportable(columnName = "PRIMA_AJ", columnOrder = 34, format="$#,##0.00")
	public Double getPrimaAJ() {
		return primaAJ;
	}

	public void setPrimaAJ(Double primaAJ) {
		this.primaAJ = primaAJ;
	}

	@Exportable(columnName = "PRIMA_AV", columnOrder = 35, format="$#,##0.00")
	public Double getPrimaAV() {
		return primaAV;
	}

	public void setPrimaAV(Double primaAV) {
		this.primaAV = primaAV;
	}

	@Exportable(columnName = "PRIMA_EXEC_DED_DM", columnOrder = 36, format="$#,##0.00")
	public Double getPrimaExecdeDM() {
		return primaExecdeDM;
	}

	public void setPrimaExecdeDM(Double primaExecdeDM) {
		this.primaExecdeDM = primaExecdeDM;
	}

	@Exportable(columnName = "PRIMA_PT_DM", columnOrder = 37, format="$#,##0.00")
	public Double getPrimaPerdidaTotalDM() {
		return primaPerdidaTotalDM;
	}

	public void setPrimaPerdidaTotalDM(Double primaPerdidaTotalDM) {
		this.primaPerdidaTotalDM = primaPerdidaTotalDM;
	}

	@Exportable(columnName = "PRIMA_ACC_COND", columnOrder = 38, format="$#,##0.00")
	public Double getPrimaAccCond() {
		return primaAccCond;
	}

	public void setPrimaAccCond(Double primaAccCond) {
		this.primaAccCond = primaAccCond;
	}

	@Exportable(columnName = "SA_ACC_COND", columnOrder = 39, format="$#,##0.00")
	public Double getSaAccCond() {
		return saAccCond;
	}

	public void setSaAccCond(Double saAccCond) {
		this.saAccCond = saAccCond;
	}

	@Exportable(columnName = "PRIMA_EXT_RC", columnOrder = 40, format="$#,##0.00")
	public Double getPrimaExtencionRC() {
		return primaExtencionRC;
	}

	public void setPrimaExtencionRC(Double primaExtencionRC) {
		this.primaExtencionRC = primaExtencionRC;
	}

	@Exportable(columnName = "PRIMA_ADAP_CONV", columnOrder = 41, format="$#,##0.00")
	public Double getPrimaAdapacionConversion() {
		return primaAdapacionConversion;
	}

	public void setPrimaAdapacionConversion(Double primaAdapacionConversion) {
		this.primaAdapacionConversion = primaAdapacionConversion;
	}

	@Exportable(columnName = "SA_ADAP_CONV", columnOrder = 42, format="$#,##0.00")
	public Double getSaAdapacionConversion() {
		return saAdapacionConversion;
	}

	public void setSaAdapacionConversion(Double saAdapacionConversion) {
		this.saAdapacionConversion = saAdapacionConversion;
	}

	@Exportable(columnName = "PRIMA_EXT_DED_RT", columnOrder = 43, format="$#,##0.00")
	public Double getPrimaExecdedRT() {
		return primaExecdedRT;
	}

	public void setPrimaExecdedRT(Double primaExecdedRT) {
		this.primaExecdedRT = primaExecdedRT;
	}
	
	@Exportable(columnName = "NOM_CONDUCTOR", columnOrder = 44)
	public String getNombreConductor() {
		return nombreConductor;
	}

	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	@Exportable(columnName = "NOM_TITULAR", columnOrder = 45)
	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	@Exportable(columnName = "SERIE", columnOrder = 46)
	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Exportable(columnName = "MOTOR", columnOrder = 47)
	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	@Exportable(columnName = "PLACAS", columnOrder = 48)
	public String getPlacas() {
		return placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	@Exportable(columnName = "PCT_BONIF_COMIS", columnOrder = 49, format="%#,##0.00")
	public Double getPctBonifComis() {
		return pctBonifComis;
	}

	public void setPctBonifComis(Double pctBonifComis) {
		this.pctBonifComis = pctBonifComis;
	}

	@Exportable(columnName = "PCT_BONIF_COMIS", columnOrder = 50, format="%#,##0.00")
	public Double getPctDesctoPol() {
		return pctDesctoPol;
	}

	public void setPctDesctoPol(Double pctDesctoPol) {
		this.pctDesctoPol = pctDesctoPol;
	}

	@Exportable(columnName = "FACT_DOCTO_IGPMCOB", columnOrder = 51, format="$#,##0.00")
	public Double getFactDctoIgpmCob() {
		return factDctoIgpmCob;
	}

	public void setFactDctoIgpmCob(Double factDctoIgpmCob) {
		this.factDctoIgpmCob = factDctoIgpmCob;
	}

	@Exportable(columnName = "FACT_DCTO_IGPMEND", columnOrder = 52, format="$#,##0.00")
	public Double getFactDctoIgpmEnd() {
		return factDctoIgpmEnd;
	}

	public void setFactDctoIgpmEnd(Double factDctoIgpmEnd) {
		this.factDctoIgpmEnd = factDctoIgpmEnd;
	}

	@Exportable(columnName = "FACT_DCTO_IGPMPOL", columnOrder = 53)
	public Double getFactDctoIgpmPol() {
		return factDctoIgpmPol;
	}

	public void setFactDctoIgpmPol(Double factDctoIgpmPol) {
		this.factDctoIgpmPol = factDctoIgpmPol;
	}

	@Exportable(columnName = "PCT_DESC_COB_DM", columnOrder = 54)
	public Double getPctDescCobDM() {
		return pctDescCobDM;
	}

	public void setPctDescCobDM(Double pctDescCobDM) {
		this.pctDescCobDM = pctDescCobDM;
	}

	@Exportable(columnName = "PCT_DESC_COB_RT", columnOrder = 55)
	public Double getPctDescCobRT() {
		return pctDescCobRT;
	}

	public void setPctDescCobRT(Double pctDescCobRT) {
		this.pctDescCobRT = pctDescCobRT;
	}

	@Exportable(columnName = "PCT_DESC_COB_RC", columnOrder = 56)
	public Double getPctDescCobRC() {
		return pctDescCobRC;
	}

	public void setPctDescCobRC(Double pctDescCobRC) {
		this.pctDescCobRC = pctDescCobRC;
	}

	@Exportable(columnName = "PCT_DESC_COB_GM", columnOrder = 57)
	public Double getPctDescCobGM() {
		return pctDescCobGM;
	}

	public void setPctDescCobGM(Double pctDescCobGM) {
		this.pctDescCobGM = pctDescCobGM;
	}

	@Exportable(columnName = "PCT_DESC_COB_EE", columnOrder = 58)
	public Double getPctDescBobEnd() {
		return pctDescBobEnd;
	}

	public void setPctDescBobEnd(Double pctDescBobEnd) {
		this.pctDescBobEnd = pctDescBobEnd;
	}

	@Exportable(columnName = "PCT_DESC_COB_AJ", columnOrder = 59)
	public Double getPctDescCobAJ() {
		return pctDescCobAJ;
	}

	public void setPctDescCobAJ(Double pctDescCobAJ) {
		this.pctDescCobAJ = pctDescCobAJ;
	}

	@Exportable(columnName = "PCT_DESC_COB_AV", columnOrder = 60)
	public Double getPctDescCobAV() {
		return pctDescCobAV;
	}

	public void setPctDescCobAV(Double pctDescCobAV) {
		this.pctDescCobAV = pctDescCobAV;
	}

	@Exportable(columnName = "PCT_DESC_COB_EXEC_DED_DM", columnOrder = 61)
	public Double getPctDescCobExecdedDM() {
		return pctDescCobExecdedDM;
	}

	public void setPctDescCobExecdedDM(Double pctDescCobExecdedDM) {
		this.pctDescCobExecdedDM = pctDescCobExecdedDM;
	}

	@Exportable(columnName = "PCT_DESC_COB_PT_DM", columnOrder = 62)
	public Double getPctDescCobptDM() {
		return pctDescCobptDM;
	}

	public void setPctDescCobptDM(Double pctDescCobptDM) {
		this.pctDescCobptDM = pctDescCobptDM;
	}

	@Exportable(columnName = "PCT_DESC_COB_ACC_COND", columnOrder = 63)
	public Double getPctDescCobAccCond() {
		return pctDescCobAccCond;
	}

	public void setPctDescCobAccCond(Double pctDescCobAccCond) {
		this.pctDescCobAccCond = pctDescCobAccCond;
	}

	@Exportable(columnName = "PCT_DESC_COB_ext_rc", columnOrder = 64)
	public Double getPctDescCobextRC() {
		return pctDescCobextRC;
	}

	public void setPctDescCobextRC(Double pctDescCobextRC) {
		this.pctDescCobextRC = pctDescCobextRC;
	}

	@Exportable(columnName = "PCT_DESC_COB_ADAP_CONV", columnOrder = 65)
	public Double getPctDescCobAdapConv() {
		return pctDescCobAdapConv;
	}

	public void setPctDescCobAdapConv(Double pctDescCobAdapConv) {
		this.pctDescCobAdapConv = pctDescCobAdapConv;
	}

	@Exportable(columnName = "PCT_DESC_COB_EXEC_DED_RT", columnOrder = 66)
	public Double getPctDescCobexecdedRT() {
		return pctDescCobexecdedRT;
	}

	public void setPctDescCobexecdedRT(Double pctDescCobexecdedRT) {
		this.pctDescCobexecdedRT = pctDescCobexecdedRT;
	}

	@Exportable(columnName = "ID_OFICINA", columnOrder = 67)
	public String getIdOficina() {
		return idOficina;
	}

	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}

	@Exportable(columnName = "NOM_OFICINA", columnOrder = 68)
	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Exportable(columnName = "ID_GERENCIA", columnOrder = 69)
	public String getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}

	@Exportable(columnName = "NOM_GERENCIA", columnOrder = 70)
	public String getNombreGerencia() {
		return nombreGerencia;
	}

	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}

	@Exportable(columnName = "ID_CENTRO_OPER", columnOrder = 71)
	public Long getIdCentroOper() {
		return idCentroOper;
	}

	public void setIdCentroOper(Long idCentroOper) {
		this.idCentroOper = idCentroOper;
	}

	@Exportable(columnName = "NOM_CENTRO_OPER", columnOrder = 72)
	public String getNomCentroOper() {
		return nomCentroOper;
	}

	public void setNomCentroOper(String nomCentroOper) {
		this.nomCentroOper = nomCentroOper;
	}

	@Exportable(columnName = "SIT_INCISO_POL_ACT", columnOrder = 73)
	public String getSitIncisoPolAct() {
		return sitIncisoPolAct;
	}

	public void setSitIncisoPolAct(String sitIncisoPolAct) {
		this.sitIncisoPolAct = sitIncisoPolAct;
	}

	@Exportable(columnName = "NOM_PRODUCTO", columnOrder = 74)
	public String getNomProducto() {
		return nomProducto;
	}

	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}

	@Exportable(columnName = "NOM_CONTRATANTE", columnOrder = 75)
	public String getNomContratante() {
		return nomContratante;
	}

	public void setNomContratante(String nomContratante) {
		this.nomContratante = nomContratante;
	}

	@Exportable(columnName = "SISTEMA_EMISION", columnOrder = 76)
	public String getSistemaEmision() {
		return sistemaEmision;
	}

	public void setSistemaEmision(String sistemaEmision) {
		this.sistemaEmision = sistemaEmision;
	}

	@Exportable(columnName = "POL_RC_EXT", columnOrder = 77)
	public String getPolRCExt() {
		return polRCExt;
	}

	public void setPolRCExt(String polRCExt) {
		this.polRCExt = polRCExt;
	}

	@Exportable(columnName = "VALOR_VEHICULO", columnOrder = 78)
	public Long getValorVehiculo() {
		return valorVehiculo;
	}

	public void setValorVehiculo(Long valorVehiculo) {
		this.valorVehiculo = valorVehiculo;
	}

	@Exportable(columnName = "DEDUCIBLE_DM", columnOrder = 79, format="$#,##0.00")
	public Double getDeducibleDM() {
		return deducibleDM;
	}

	public void setDeducibleDM(Double deducibleDM) {
		this.deducibleDM = deducibleDM;
	}

	@Exportable(columnName = "DEDUCIBLE_RT", columnOrder = 80, format="$#,##0.00")
	public Double getDeducibleRT() {
		return deducibleRT;
	}

	public void setDeducibleRT(Double deducibleRT) {
		this.deducibleRT = deducibleRT;
	}

	@Exportable(columnName = "DEDUCIBLE_RC", columnOrder = 81, format="$#,##0.00")
	public Double getDeducibleRC() {
		return deducibleRC;
	}

	public void setDeducibleRC(Double deducibleRC) {
		this.deducibleRC = deducibleRC;
	}

	@Exportable(columnName = "ASISTENCIA_JURIDICA", columnOrder = 82)
	public String getAsistenciaJuridica() {
		return asistenciaJuridica;
	}

	public void setAsistenciaJuridica(String asistenciaJuridica) {
		this.asistenciaJuridica = asistenciaJuridica;
	}

	@Exportable(columnName = "ASISTENCIA_VIAL", columnOrder = 83)
	public String getAsistenciaVial() {
		return asistenciaVial;
	}

	public void setAsistenciaVial(String asistenciaVial) {
		this.asistenciaVial = asistenciaVial;
	}

	@Exportable(columnName = "EXTENCION_DM", columnOrder = 84)
	public String getExencionDM() {
		return exencionDM;
	}

	public void setExencionDM(String exencionDM) {
		this.exencionDM = exencionDM;
	}

	@Exportable(columnName = "EXTENCION_RT", columnOrder = 85)
	public String getExencionRT() {
		return exencionRT;
	}

	public void setExencionRT(String exencionRT) {
		this.exencionRT = exencionRT;
	}

	@Exportable(columnName = "EXTENCION_RC", columnOrder = 86)
	public String getExtencionRC() {
		return extencionRC;
	}

	public void setExtencionRC(String extencionRC) {
		this.extencionRC = extencionRC;
	}

	@Exportable(columnName = "RC_EXCESO_MUERTE", columnOrder = 87, format="$#,##0.00")
	public Double getRcExcesoMuerte() {
		return rcExcesoMuerte;
	}

	public void setRcExcesoMuerte(Double rcExcesoMuerte) {
		this.rcExcesoMuerte = rcExcesoMuerte;
	}

	@Exportable(columnName = "SA_RC_EXCESO_MUERTE", columnOrder = 88, format="$#,##0.00")
	public Double getSaRcExcesoMuerte() {
		return saRcExcesoMuerte;
	}

	public void setSaRcExcesoMuerte(Double saRcExcesoMuerte) {
		this.saRcExcesoMuerte = saRcExcesoMuerte;
	}

	@Exportable(columnName = "DEDUCIBLE_RC_EXMU", columnOrder = 89, format="$#,##0.00")
	public Double getFeducibleRCExmu() {
		return deducibleRCExmu;
	}

	public void setDeducibleRCExmu(Double deducibleRCExmu) {
		this.deducibleRCExmu = deducibleRCExmu;
	}

	@Exportable(columnName = "CAPACIDAD_PASAJEROS", columnOrder = 90)
	public Long getCapacidadPasajeros() {
		return capacidadPasajeros;
	}

	public void setCapacidadPasajeros(Long capacidadPasajeros) {
		this.capacidadPasajeros = capacidadPasajeros;
	}

	@Exportable(columnName = "SERVICIO", columnOrder = 91)
	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	@Exportable(columnName = "USO", columnOrder = 92)
	public String getUso() {
		return uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}
	
	@Exportable(columnName = "TIPO_MERCADO", columnOrder = 93)
	public String getTipoMercado() {
		return tipoMercado;
	}

	public void setTipoMercado(String tipoMercado) {
		this.tipoMercado = tipoMercado;
	}

	@Exportable(columnName = "NUMERO_REMOLQUES", columnOrder = 94)
	public Long getNumeroRemolques() {
		return numeroRemolques;
	}

	public void setNumeroRemolques(Long numeroRemolques) {
		this.numeroRemolques = numeroRemolques;
	}

	@Exportable(columnName = "FINANCIAMIENTO", columnOrder = 95, format="$#,##0.00")
	public Double getFinanciamiento() {
		return financiamiento;
	}

	public void setFinanciamiento(Double financiamiento) {
		this.financiamiento = financiamiento;
	}

	@Exportable(columnName = "GASTOS_EXPEDICIÓN", columnOrder = 96)
	public Double getGastosExpedicion() {
		return gastosExpedicion;
	}

	public void setGastosExpedicion(Double gastosExpedicion) {
		this.gastosExpedicion = gastosExpedicion;
	}

	@Exportable(columnName = "FORMA_PAGO", columnOrder = 97)
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	@Exportable(columnName = "IVA", columnOrder = 98, format="$#,##0.00")
	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	@Exportable(columnName = "ADAPTACIONES_CONVERSIONES", columnOrder = 99)
	public String getAdaptacionesConversiones() {
		return adaptacionesConversiones;
	}

	public void setAdaptacionesConversiones(String adaptacionesConversiones) {
		this.adaptacionesConversiones = adaptacionesConversiones;
	}

	@Exportable(columnName = "EQUIPO_ESPECIAL", columnOrder = 100)
	public String getEquipoEspecial() {
		return equipoEspecial;
	}

	public void setEquipoEspecial(String equipoEspecial) {
		this.equipoEspecial = equipoEspecial;
	}
	
	@Exportable(columnName = "OBSERVACIONES", columnOrder = 101)
	public String getObservacionesInciso() {
		return observacionesInciso;
	}

	public void setObservacionesInciso(String observacionesInciso) {
		this.observacionesInciso = observacionesInciso;
	}

	@Exportable(columnName = "OBSERVACIONES_ENDOSO", columnOrder = 102)
	public String getObservacionesEndoso() {
		return observacionesEndoso;
	}

	public void setObservacionesEndoso(String observacionesEndoso) {
		this.observacionesEndoso = observacionesEndoso;
	}

	public String getIdTopoliza() {
		return idTopoliza;
	}

	public void setIdTopoliza(String idTopoliza) {
		this.idTopoliza = idTopoliza;
	}

	public Long getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(Long idInciso) {
		this.idInciso = idInciso;
	}

	public Long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public Long getIdLinNegocio() {
		return idLinNegocio;
	}

	public void setIdLinNegocio(Long idLinNegocio) {
		this.idLinNegocio = idLinNegocio;
	}

	public Long getIdRTEndoso() {
		return idRTEndoso;
	}

	public void setIdRTEndoso(Long idRTEndoso) {
		this.idRTEndoso = idRTEndoso;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

	public String getCvelServicioAuto() {
		return cvelServicioAuto;
	}

	public void setCvelServicioAuto(String cvelServicioAuto) {
		this.cvelServicioAuto = cvelServicioAuto;
	}

	public String getCvelUsoAuto() {
		return cvelUsoAuto;
	}

	public void setCvelUsoAuto(String cvelUsoAuto) {
		this.cvelUsoAuto = cvelUsoAuto;
	}

	public String getTipoMercancia() {
		return tipoMercancia;
	}

	public void setTipoMercancia(String tipoMercancia) {
		this.tipoMercancia = tipoMercancia;
	}

	public Long getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getCvetEndoso() {
		return cvetEndoso;
	}

	public void setCvetEndoso(String cvetEndoso) {
		this.cvetEndoso = cvetEndoso;
	}

	@Exportable(columnName = "IDPAQUETE", columnOrder = 103)
	public Long getIdPaquete() {
		return idPaquete;
	}

	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}

	@Exportable(columnName = "PAQUETE", columnOrder = 104)
	public String getDescripcionPaquete() {
		return descripcionPaquete;
	}

	public void setDescripcionPaquete(String descripcionPaquete) {
		this.descripcionPaquete = descripcionPaquete;
	}

	public Long getIdVersionPoliza() {
		return idVersionPoliza;
	}

	public void setIdVersionPoliza(Long idVersionPoliza) {
		this.idVersionPoliza = idVersionPoliza;
	}
}
