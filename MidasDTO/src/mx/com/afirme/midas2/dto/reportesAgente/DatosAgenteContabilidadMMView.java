package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosAgenteContabilidadMMView implements Serializable {
	
	private static final long serialVersionUID = 1L;
    public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.contabilidadMM.archivo.nombre";
    
	private Long id;
	private Long idAgente;
	private String nom_agente;
	private String desc_moneda;
	private Date fechaMovimiento;
	private String nom_supervisoria;
	private Long id_ramo_contable;
	private Long id_subr_contable;
	private String ramo1;
	private String ramo2;
	private String contratante;
	private Double imp_comis_agte;
	private Double imp_iva_acred;
	private Double imp_iva_ret;
	private Double imp_isr;
    private String desc_movto;
    
    @Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getNom_supervisoria() {
		return nom_supervisoria;
	}
	public void setNom_supervisoria(String nom_supervisoria) {
		this.nom_supervisoria = nom_supervisoria;
	}
	public Long getId_ramo_contable() {
		return id_ramo_contable;
	}
	public void setId_ramo_contable(Long id_ramo_contable) {
		this.id_ramo_contable = id_ramo_contable;
	}
	public Long getId_subr_contable() {
		return id_subr_contable;
	}
	public void setId_subr_contable(Long id_subr_contable) {
		this.id_subr_contable = id_subr_contable;
	}
	public String getRamo1() {
		return ramo1;
	}
	public void setRamo1(String ramo1) {
		this.ramo1 = ramo1;
	}
	public String getRamo2() {
		return ramo2;
	}
	public void setRamo2(String ramo2) {
		this.ramo2 = ramo2;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Double getImp_comis_agte() {
		return imp_comis_agte;
	}
	public void setImp_comis_agte(Double imp_comis_agte) {
		this.imp_comis_agte = imp_comis_agte;
	}
	public Double getImp_iva_acred() {
		return imp_iva_acred;
	}
	public void setImp_iva_acred(Double imp_iva_acred) {
		this.imp_iva_acred = imp_iva_acred;
	}
	public Double getImp_iva_ret() {
		return imp_iva_ret;
	}
	public void setImp_iva_ret(Double imp_iva_ret) {
		this.imp_iva_ret = imp_iva_ret;
	}
	public Double getImp_isr() {
		return imp_isr;
	}
	public void setImp_isr(Double imp_isr) {
		this.imp_isr = imp_isr;
	}
	public String getDesc_movto() {
		return desc_movto;
	}
	public void setDesc_movto(String desc_movto) {
		this.desc_movto = desc_movto;
	}
	public String getNom_agente() {
		return nom_agente;
	}
	public void setNom_agente(String nom_agente) {
		this.nom_agente = nom_agente;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	} 
	
}
