package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosHonorariosAgentes implements Serializable {
	private static final long serialVersionUID = 1784878947634134280L;
	private String	idGerencia;
	private String	nombreGerencia;
	private String	idOficina;
	private String	nombreOficina;
	private Long	idSupervisoria;
	private String	nombreSupervisoria;
	private Long	idAgente;
	private String	nombre;
	private String	origen;
	private Long	anoMes;
	private Date	fechaMovimiento;
	private BigDecimal	importeBase;
	private BigDecimal	importeIva;
	private BigDecimal	importeIvaRetenido;
	private BigDecimal	importeIsrRetenido;
	private String	estatus;
	private String	emailAgente;
	private String	correoCc;
	private BigDecimal	importeGravable;
	private BigDecimal	importeSubtotal;
	private BigDecimal	importeExento;
	private BigDecimal	importeTotal;
	private String  importeTotalCLetra;
	private BigDecimal 	importeRetenidoEstatal;
	private String numeroCheque;
	private Date fechaAplicacion;
	private String claveServicio;
	private String descripcionServicio;
	private BigDecimal	importeConcepto;
	
	@Id
	public Long getIdAgente() {
		return idAgente;
	}
	public String getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}
	public String getNombreGerencia() {
		return nombreGerencia;
	}
	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}
	public String getIdOficina() {
		return idOficina;
	}
	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public Long getIdSupervisoria() {
		return idSupervisoria;
	}
	public void setIdSupervisoria(Long idSupervisoria) {
		this.idSupervisoria = idSupervisoria;
	}
	public String getNom_supervisoria() {
		return nombreSupervisoria;
	}
	public void setNom_supervisoria(String nom_supervisoria) {
		this.nombreSupervisoria = nom_supervisoria;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public Long getAnoMes() {
		return anoMes;
	}
	public void setAnoMes(Long anoMes) {
		this.anoMes = anoMes;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public BigDecimal getImporteBase() {
		return importeBase;
	}
	public void setImporteBase(BigDecimal importeBase) {
		this.importeBase = importeBase;
	}
	public BigDecimal getImporteIva() {
		return importeIva;
	}
	public void setImporteIva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}
	public BigDecimal getImporteIvaRetenido() {
		return importeIvaRetenido;
	}
	public void setImporteIvaRetenido(BigDecimal importeIvaRetenido) {
		this.importeIvaRetenido = importeIvaRetenido;
	}
	public BigDecimal getImporteIsrRetenido() {
		return importeIsrRetenido;
	}
	public void setImporteIsrRetenido(BigDecimal importeIsrRetenido) {
		this.importeIsrRetenido = importeIsrRetenido;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getEmailAgente() {
		return emailAgente;
	}
	public void setE_mail_agte(String emailAgente) {
		this.emailAgente = emailAgente;
	}
	public String getCorreoCc() {
		return correoCc;
	}
	public void setCorreoCc(String correoCc) {
		this.correoCc = correoCc;
	}
	public BigDecimal getImporteGravable() {
		return importeGravable;
	}
	public void setImporteGravable(BigDecimal importeGravable) {
		this.importeGravable = importeGravable;
	}
	public BigDecimal getImporteSubtotal() {
		return importeSubtotal;
	}
	public void setImporteSubtotal(BigDecimal importeSubtotal) {
		this.importeSubtotal = importeSubtotal;
	}
	public BigDecimal getImporteExento() {
		return importeExento;
	}
	public void setImporteExento(BigDecimal importeExento) {
		this.importeExento = importeExento;
	}
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getImporteTotalCLetra() {
		return importeTotalCLetra;
	}
	public void setImporteTotalCLetra(String importeTotalCLetra) {
		this.importeTotalCLetra = importeTotalCLetra;
	}
	public BigDecimal getImporteRetenidoEstatal() {
		return importeRetenidoEstatal;
	}
	public void setImporteRetenidoEstatal(BigDecimal importeRetenidoEstatal) {
		this.importeRetenidoEstatal = importeRetenidoEstatal;
	}
	public String getNumeroCheque() {
		return numeroCheque;
	}
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}
	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
	public String getClaveServicio() {
		return claveServicio;
	}
	public void setClaveServicio(String claveServicio) {
		this.claveServicio = claveServicio;
	}
	public String getDescripcionServicio() {
		return descripcionServicio;
	}
	public void setDescripcionServicio(String descripcionServicio) {
		this.descripcionServicio = descripcionServicio;
	}
	public BigDecimal getImporteConcepto() {
		return importeConcepto;
	}
	public void setImporteConcepto(BigDecimal importeConcepto) {
		this.importeConcepto = importeConcepto;
	}
	
}
