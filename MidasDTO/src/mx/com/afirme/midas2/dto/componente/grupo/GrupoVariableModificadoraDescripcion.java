package mx.com.afirme.midas2.dto.componente.grupo;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControls;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;

@DynamicControls(value = { 
		@DynamicControl(atributoMapeo="idGrupo", tipoControl = TipoControl.SELECT,etiqueta="Grupo",editable=true,claveCascada="GPO-VAR-MOD-DESC"),
		@DynamicControl(atributoMapeo="id", tipoControl = TipoControl.SELECT,etiqueta="Descripci\u00F3n",editable=true,claveCascada="SEC-GPO-VAR-MOD-DESC")
	}
)
public interface GrupoVariableModificadoraDescripcion {

}
