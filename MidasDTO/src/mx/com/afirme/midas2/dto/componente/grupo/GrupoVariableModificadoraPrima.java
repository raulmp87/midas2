package mx.com.afirme.midas2.dto.componente.grupo;


import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControls;

@DynamicControls(value = { 
		@DynamicControl(atributoMapeo="grupo.id", tipoControl = TipoControl.SELECT,etiqueta="Grupo",editable=true,claveCascada="GPO-VAR-MOD-PRIMA"),
		@DynamicControl(atributoMapeo="id", tipoControl = TipoControl.SELECT,etiqueta="Variable",editable=true,claveCascada="SEC-GPO-VAR-MOD-PRIMA")
	}
)

public interface GrupoVariableModificadoraPrima {
}
