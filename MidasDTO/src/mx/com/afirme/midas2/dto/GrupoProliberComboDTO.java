package mx.com.afirme.midas2.dto;

import mx.com.afirme.midas.base.CacheableDTO;

public class GrupoProliberComboDTO extends CacheableDTO {

	private static final long serialVersionUID = 1L;
	
	private String claveGrupoProliber;
	
	public String getClaveGrupoProliber() {
		return claveGrupoProliber;
	}

	public void setClaveGrupoProliber(String claveGrupoProliber) {
		this.claveGrupoProliber = claveGrupoProliber;
	}

	@Override
	public Object getId() {
		return getClaveGrupoProliber();
	}

	@Override
	public String getDescription() {
		return getClaveGrupoProliber();
	}

	@Override
	public boolean equals(Object object) {
		if(object != null && ! (object instanceof GrupoProliberComboDTO))
			return false;
		else if (((GrupoProliberComboDTO)object).getClaveGrupoProliber() != null){
			return getClaveGrupoProliber().equals(((GrupoProliberComboDTO)object).getClaveGrupoProliber());
		}
		return false;
	}

}
