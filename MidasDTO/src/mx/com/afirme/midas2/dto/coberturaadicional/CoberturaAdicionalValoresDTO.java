package mx.com.afirme.midas2.dto.coberturaadicional;

import java.io.Serializable;
import java.math.BigDecimal;

public class CoberturaAdicionalValoresDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal totalKilometros;
	private BigDecimal totalMinutos;
	private BigDecimal montoPrimaKilometros;
	private BigDecimal montoPrimaMinutos;
	private BigDecimal montoAnualCobertura;
	
	
	public BigDecimal getTotalKilometros() {	
		return totalKilometros;
	}
	public void setTotalKilometros(BigDecimal totalKilometros) {
		this.totalKilometros = totalKilometros;
	}
	public BigDecimal getTotalMinutos() {
		return totalMinutos;
	}
	public void setTotalMinutos(BigDecimal totalMinutos) {
		this.totalMinutos = totalMinutos;
	}
	public BigDecimal getMontoPrimaKilometros() {
		return montoPrimaKilometros;
	}
	public void setMontoPrimaKilometros(BigDecimal montoPrimaKilometros) {
		this.montoPrimaKilometros = montoPrimaKilometros;
	}
	public BigDecimal getMontoPrimaMinutos() {
		return montoPrimaMinutos;
	}
	public void setMontoPrimaMinutos(BigDecimal montoPrimaMinutos) {
		this.montoPrimaMinutos = montoPrimaMinutos;
	}
	public BigDecimal getMontoAnualCobertura() {
		return montoAnualCobertura;
	}
	public void setMontoAnualCobertura(BigDecimal montoAnualCobertura) {
		this.montoAnualCobertura = montoAnualCobertura;
	}
}