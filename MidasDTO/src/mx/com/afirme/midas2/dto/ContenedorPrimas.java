package mx.com.afirme.midas2.dto;

import java.math.BigDecimal;

public 	class ContenedorPrimas {
	private BigDecimal totalPrimaPropias = new BigDecimal(0);
	private BigDecimal totalPrimaExternas = new BigDecimal(0);
	
	private BigDecimal idTcSubramo;
	
	public BigDecimal getTotalPrimaPropias() {
		return totalPrimaPropias;
	}
	public void setTotalPrimaPropias(BigDecimal totalPrimaPropias) {
		this.totalPrimaPropias = totalPrimaPropias;
	}
	public BigDecimal getTotalPrimaExternas() {
		return totalPrimaExternas;
	}
	public void setTotalPrimaExternas(BigDecimal totalPrimaExternas) {
		this.totalPrimaExternas = totalPrimaExternas;
	}
	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}
	public BigDecimal getIdTcSubramo() {
		return idTcSubramo;
	}
}
