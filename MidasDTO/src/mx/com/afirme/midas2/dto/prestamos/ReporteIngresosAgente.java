package mx.com.afirme.midas2.dto.prestamos;

import java.io.Serializable;


public class ReporteIngresosAgente implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7343273932122948757L;

	private double importeTotalIngresosAgente;
	private double promedioImporteIngresos;
	private double porcentajeRepresentapagoMensual;
	private double importeOtorgado;
	private Integer plazos;
	private double interesOrdinario;
	private double interesMoratorio;
	private double abonoCapital;
	private double importeTotalIntersOrdinario;
	private double importeInteresMoratorio;
	private double iva;
	private double total;
	private double totalAbonoCapital;
	private String nombreAgente;
	private Long idAgente;
	
	
	public double getTotalAbonoCapital() {
		return totalAbonoCapital;
	}
	public void setTotalAbonoCapital(double totalAbonoCapital) {
		this.totalAbonoCapital = totalAbonoCapital;
	}
	public double getImporteTotalIngresosAgente() {
		return importeTotalIngresosAgente;
	}
	public void setImporteTotalIngresosAgente(double importeTotalIngresosAgente) {
		this.importeTotalIngresosAgente = importeTotalIngresosAgente;
	}
	public double getPromedioImporteIngresos() {
		return promedioImporteIngresos;
	}
	public void setPromedioImporteIngresos(double promedioImporteIngresos) {
		this.promedioImporteIngresos = promedioImporteIngresos;
	}
	public double getPorcentajeRepresentapagoMensual() {
		return porcentajeRepresentapagoMensual;
	}
	public void setPorcentajeRepresentapagoMensual(
			double porcentajeRepresentapagoMensual) {
		this.porcentajeRepresentapagoMensual = porcentajeRepresentapagoMensual;
	}
	
	public double getImporteOtorgado() {
		return importeOtorgado;
	}
	public void setImporteOtorgado(double importeOtorgado) {
		this.importeOtorgado = importeOtorgado;
	}
	public Integer getPlazos() {
		return plazos;
	}
	public void setPlazos(Integer plazos) {
		this.plazos = plazos;
	}
	public double getInteresOrdinario() {
		return interesOrdinario;
	}
	public void setInteresOrdinario(double interesOrdinario) {
		this.interesOrdinario = interesOrdinario;
	}
	public double getInteresMoratorio() {
		return interesMoratorio;
	}
	public void setInteresMoratorio(double interesMoratorio) {
		this.interesMoratorio = interesMoratorio;
	}
	public double getAbonoCapital() {
		return abonoCapital;
	}
	public void setAbonoCapital(double abonoCapital) {
		this.abonoCapital = abonoCapital;
	}
	public double getImporteTotalIntersOrdinario() {
		return importeTotalIntersOrdinario;
	}
	public void setImporteTotalIntersOrdinario(double importeTotalIntersOrdinario) {
		this.importeTotalIntersOrdinario = importeTotalIntersOrdinario;
	}
	public double getImporteInteresMoratorio() {
		return importeInteresMoratorio;
	}
	public void setImporteInteresMoratorio(double importeInteresMoratorio) {
		this.importeInteresMoratorio = importeInteresMoratorio;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	
}
