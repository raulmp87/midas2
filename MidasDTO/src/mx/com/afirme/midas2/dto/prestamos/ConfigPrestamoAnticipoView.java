package mx.com.afirme.midas2.dto.prestamos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
/**
 * @author admin
 * Clase para utilizarla como filtro de busqueda en Configuracion de prestamos y anticipos 
 * para la pantalla de listar prestamos y anticipos existentes.
 */
@Entity
public class ConfigPrestamoAnticipoView implements Serializable{

	private static final long serialVersionUID = -4510465224644792220L;
	private Long id;
	private Long idAgente;
	private String nombreCompreto;
	private String tipoMovimiento;
	private Date fechaAltaMovimiento;
	private String estatus;
	private Double importeOtorgado;
	private Integer numeroPagos;
//	private Date fechaInicioCalculo;
	private String   plazos;
	private String fechaAltaMovimientoString;
	private String fechaInicioCalculoString;
	private Double importePago;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	} 
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreCompreto() {
		return nombreCompreto;
	}
	public void setNombreCompreto(String nombreCompreto) {
		this.nombreCompreto = nombreCompreto;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaMovimiento() {
		return fechaAltaMovimiento;
	}
	public void setFechaAltaMovimiento(Date fechaAltaMovimiento) {
		this.fechaAltaMovimiento = fechaAltaMovimiento;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaAltaMovimientoString = sdf.format(fechaAltaMovimiento);
	}
	public String getestatus() {
		return estatus;
	}
	public void setIdEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Double getImporteOtorgado() {
		return importeOtorgado;
	}
	public void setImporteOtorgado(Double importeOtorgado) {
		this.importeOtorgado = importeOtorgado;
	}
	public Integer getNumeroPagos() {
		return numeroPagos;
	}
	public void setNumeroPagos(Integer numeroPagos) {
		this.numeroPagos = numeroPagos;
	}
	
//	@Temporal(TemporalType.DATE)
//	public Date getFechaInicioCalculo() {
//		return fechaInicioCalculo;
//	}
//	public void setFechaInicioCalculo(Date fechaInicioCalculo) {
//		this.fechaInicioCalculo = fechaInicioCalculo;
//		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy"); 
//		this.fechaInicioCalculoString = sdf1.format(fechaInicioCalculo);
//	}
	public String getPlazos() {
		return plazos;
	}
	public void setPlazos(String plazos) {
		this.plazos = plazos;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	@Transient
	public String getFechaAltaMovimientoString() {
		return fechaAltaMovimientoString;
	}
	public void setFechaAltaMovimientoString(String fechaAltaMovimientoString) {
		this.fechaAltaMovimientoString = fechaAltaMovimientoString;
	}
	@Transient
	public String getFechaInicioCalculoString() {
		return fechaInicioCalculoString;
	}
	public void setFechaInicioCalculoString(String fechaInicioCalculoString) {
		this.fechaInicioCalculoString = fechaInicioCalculoString;
	}
	public Double getImportePago() {
		return importePago;
	}
	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}

}
