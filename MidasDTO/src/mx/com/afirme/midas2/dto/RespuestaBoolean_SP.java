package mx.com.afirme.midas2.dto;

public class RespuestaBoolean_SP {
	
	private Integer resultadoSP;

	public Integer getResultadoSP() {
		return resultadoSP;
	}

	public void setResultadoSP(Integer resultadoSP) {
		this.resultadoSP = resultadoSP;
	}
	
	public Boolean getResultadoSPBoolean()
	{
		if(this.resultadoSP != null && this.resultadoSP == 1)
		{
			return Boolean.TRUE;			
		}
		
		return Boolean.FALSE;
	}
	
}
