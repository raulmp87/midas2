package mx.com.afirme.midas2.dto.prestadordeservicio;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class PrestadorDeServicioDTO {
	
	@NotNull
	private Long oficinaId;
	private String referencia;
	
	
	//Datos del prestador del servicio
	private String numPrestador;
	private String oficina; 
	
	@NotNull
	private String tipoPersona;
	@NotNull
	private String estatus;
	
	//Datos del prestador del servicio Persona Fisica
	@NotNull
	private String nombre;
	@NotNull
	private String apellidoPaterno; 
	@NotNull
	private String apellidoMaterno;
	
	
	//Datos del prestador del servicio Persona Moral
	@NotNull
	private String nombreDeLaEmpresa;
	private String nombreComercial; 
	private String administrador;
	
	private Date fechaDeAlta;
	private Date fechaDeBaja;
	private String tipoPrestador;

	//Poliza RC
	private String numPoliza;
	private Long ciaDeSeguros;
	private Date terminoVigencia;
	
	//Direccion
	@NotNull
	private String idPaisString;
	@NotNull
	private String idEstadoString; 
	@NotNull
	private String idMunicipioString;
	@NotNull
	private String nombreCalle;
	@NotNull
	private String nombreCalleNumero;
	private String numeroInterno;
	@Pattern(regexp="\\d+")
	@NotNull
	private String codigoPostal;
	private String nombreColonia;
	private String nombreColoniaDiferente;

	
	
	//Datos de localizacion
	
	private String telefono1Lada;
	private String telefono1;
	private String telefono2Lada;
	private String telefono2;
	private String emailPrincipal;
	private String celularLada;
	private String celular;
	private String otrosLada;
	private String otros;
	private String emailAdicional;
	@NotNull
	private String rfc;
	@NotNull
	private Date fechaNacimiento;
	@NotNull
	private String curp;
	private String descripcion;
	
	//Servicios profesionales
	private Integer serviciosProfesionales;
	
	//Servicios Compensaciones Adicionales
	private Integer vidaCa;
	private Integer daniosCa;
	private Integer autosCa;
	private Integer formaPagoCa;
	
	///permisosCompensaciones
	private boolean tienePermisoCA;
	
	List<String> tipoPrestadorSelected;
	
	private String codigoUsuarioPrestador;
	
	
	public String getNumPrestador() {
		return numPrestador;
	}
	public void setNumPrestador(String numPrestador) {
		this.numPrestador = numPrestador;
	}
	public String getOficina() {
		return oficina;
	}
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getTipoPrestador() {
		return tipoPrestador;
	}
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public Long getCiaDeSeguros() {
		return ciaDeSeguros;
	}
	public void setCiaDeSeguros(Long ciaDeSeguros) {
		this.ciaDeSeguros = ciaDeSeguros;
	}
	
	public String getTelefono1() {
		return telefono1;
	}
	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}
	public String getTelefono2() {
		return telefono2;
	}
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}
	public String getEmailPrincipal() {
		return emailPrincipal;
	}
	public void setEmailPrincipal(String emailPrincipal) {
		this.emailPrincipal = emailPrincipal;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}
	public String getEmailAdicional() {
		return emailAdicional;
	}
	public void setEmailAdicional(String emailAdicional) {
		this.emailAdicional = emailAdicional;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getAdministrador() {
		return administrador;
	}
	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}
	public String getNombreDeLaEmpresa() {
		return nombreDeLaEmpresa;
	}
	public void setNombreDeLaEmpresa(String nombreDeLaEmpresa) {
		this.nombreDeLaEmpresa = nombreDeLaEmpresa;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public Date getFechaDeAlta() {
		return fechaDeAlta;
	}
	public void setFechaDeAlta(Date fechaDeAlta) {
		this.fechaDeAlta = fechaDeAlta;
	}
	public Date getFechaDeBaja() {
		return fechaDeBaja;
	}
	public void setFechaDeBaja(Date fechaDeBaja) {
		this.fechaDeBaja = fechaDeBaja;
	}
	public Date getTerminoVigencia() {
		return terminoVigencia;
	}
	public void setTerminoVigencia(Date terminoVigencia) {
		this.terminoVigencia = terminoVigencia;
	}
	public List<String> getTipoPrestadorSelected() {
		return tipoPrestadorSelected;
	}
	public void setTipoPrestadorSelected(List<String> tipoPrestadorSelected) {
		this.tipoPrestadorSelected = tipoPrestadorSelected;
	}
	public String getIdPaisString() {
		return idPaisString;
	}
	public void setIdPaisString(String idPaisString) {
		this.idPaisString = idPaisString;
	}
	public String getIdEstadoString() {
		return idEstadoString;
	}
	public void setIdEstadoString(String idEstadoString) {
		this.idEstadoString = idEstadoString;
	}
	public String getIdMunicipioString() {
		return idMunicipioString;
	}
	public void setIdMunicipioString(String idMunicipioString) {
		this.idMunicipioString = idMunicipioString;
	}
	public String getNombreCalle() {
		return nombreCalle;
	}
	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	public String getNombreColoniaDiferente() {
		return nombreColoniaDiferente;
	}
	public void setNombreColoniaDiferente(String nombreColoniaDiferente) {
		this.nombreColoniaDiferente = nombreColoniaDiferente;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombreCalleNumero() {
		return nombreCalleNumero;
	}
	public void setNombreCalleNumero(String nombreCalleNumero) {
		this.nombreCalleNumero = nombreCalleNumero;
	}
	public String getNumeroInterno() {
		return numeroInterno;
	}
	public void setNumeroInterno(String numeroInterno) {
		this.numeroInterno = numeroInterno;
	}
	public String getTelefono1Lada() {
		return telefono1Lada;
	}
	public void setTelefono1Lada(String telefono1Lada) {
		this.telefono1Lada = telefono1Lada;
	}
	public String getTelefono2Lada() {
		return telefono2Lada;
	}
	public void setTelefono2Lada(String telefono2Lada) {
		this.telefono2Lada = telefono2Lada;
	}
	public String getCelularLada() {
		return celularLada;
	}
	public void setCelularLada(String celularLada) {
		this.celularLada = celularLada;
	}
	public String getOtrosLada() {
		return otrosLada;
	}
	public void setOtrosLada(String otrosLada) {
		this.otrosLada = otrosLada;
	}
	public Integer getServiciosProfesionales() {
		return serviciosProfesionales;
	}
	public void setServiciosProfesionales(Integer serviciosProfesionales) {
		this.serviciosProfesionales = serviciosProfesionales;
	}
	public String getCodigoUsuarioPrestador() {
		return codigoUsuarioPrestador;
	}
	public void setCodigoUsuarioPrestador(String codigoUsuarioPrestador) {
		this.codigoUsuarioPrestador = codigoUsuarioPrestador;
	}
	public Integer getVidaCa() {
		return vidaCa;
	}
	public Integer getDaniosCa() {
		return daniosCa;
	}
	public Integer getAutosCa() {
		return autosCa;
	}
	public Integer getFormaPagoCa() {
		return formaPagoCa;
	}
	public void setVidaCa(Integer vidaCa) {
		this.vidaCa = vidaCa;
	}
	public void setDaniosCa(Integer daniosCa) {
		this.daniosCa = daniosCa;
	}
	public void setAutosCa(Integer autosCa) {
		this.autosCa = autosCa;
	}
	public void setFormaPagoCa(Integer formaPagoCa) {
		this.formaPagoCa = formaPagoCa;
	}
	public boolean isTienePermisoCA() {
		return tienePermisoCA;
	}
	public void setTienePermisoCA(boolean tienePermisoCA) {
		this.tienePermisoCA = tienePermisoCA;
	}

}
