package mx.com.afirme.midas2.dto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * Anotacion para procesar los campos requeridos de clientes
 * @author vmhersil
 *
 */
@Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD, ElementType.FIELD})
public @interface ClientBeanValidator {
	public static enum ValidationType{
		NONE("none"),REQUIRED("required"),NUMBER("number"),ALPHANUMERIC("alpha"),EMAIL("email");
		private String value;
		private ValidationType(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	public static enum SectionType{
		DATOS_GENERALES("DATOS_GENERALES"),DATOS_CONTACTO("DATOS_CONTACTO"),DATOS_FISCALES("DATOS_FISCALES"),DATOS_COBRANZA("DATOS_COBRANZA"),AVISOS_SINIESTROS("AVISOS_SINIESTROS"),CANAL_VENTAS("CANAL_VENTAS");
		private String value;
		private SectionType(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	//Atributo mapping label indica cual es el campo que se va a validar de un atributo,
	//este valor debe de coincidir con el campo MIDAS.TOCONFIGCAMPO.descripcion
	public String mappingLabel();
	public String errorMessage() default "midas.cliente.generic.error";
	public SectionType seccion();
	public String modelView();
	public ValidationType validationType() default mx.com.afirme.midas2.dto.ClientBeanValidator.ValidationType.NONE;
}
