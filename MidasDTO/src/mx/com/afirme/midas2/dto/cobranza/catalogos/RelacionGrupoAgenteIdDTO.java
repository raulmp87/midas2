package mx.com.afirme.midas2.dto.cobranza.catalogos;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class RelacionGrupoAgenteIdDTO implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer centroEmisor;
	
	private BigDecimal numeroPoliza;
	
	private Integer numeroRenovacion;
	
	public Integer getCentroEmisor() {
		return centroEmisor;
	}

	public void setCentroEmisor(Integer centroEmisor) {
		this.centroEmisor = centroEmisor;
	}

	public BigDecimal getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(BigDecimal numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public Integer getNumeroRenovacion() {
		return numeroRenovacion;
	}

	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}
}
