package mx.com.afirme.midas2.dto.cobranza.programapago;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class ReciboVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7026737158080296178L;
	//Variables JSON
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getNumExhibicion() {
		return numExhibicion;
	}
	public void setNumExhibicion(Integer numExhibicion) {
		this.numExhibicion = numExhibicion;
	}
	public String getCveRecibo() {
		return cveRecibo;
	}
	public void setCveRecibo(String cveRecibo) {
		this.cveRecibo = cveRecibo;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public Date getFechaCubreDesde() {
		return fechaCubreDesde;
	}
	public void setFechaCubreDesde(Date fechaCubreDesde) {
		this.fechaCubreDesde = fechaCubreDesde;
	}
	public Date getFechaCubreHasta() {
		return fechaCubreHasta;
	}
	public void setFechaCubreHasta(Date fechaCubreHasta) {
		this.fechaCubreHasta = fechaCubreHasta;
	}
	public BigDecimal getImpPrimaNeta() {
		return impPrimaNeta;
	}
	public void setImpPrimaNeta(BigDecimal impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}
	public BigDecimal getImpRcgosPagoFR() {
		return impRcgosPagoFR;
	}
	public void setImpRcgosPagoFR(BigDecimal impRcgosPagoFR) {
		this.impRcgosPagoFR = impRcgosPagoFR;
	}
	public BigDecimal getImpDerechos() {
		return impDerechos;
	}
	public void setImpDerechos(BigDecimal impDerechos) {
		this.impDerechos = impDerechos;
	}
	public BigDecimal getImpIVA() {
		return impIVA;
	}
	public void setImpIVA(BigDecimal impIVA) {
		this.impIVA = impIVA;
	}
	public BigDecimal getImpPrimaTotal() {
		return impPrimaTotal;
	}
	public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public String getCveTipoDocumento() {
		return cveTipoDocumento;
	}
	public void setCveTipoDocumento(String cveTipoDocumento) {
		this.cveTipoDocumento = cveTipoDocumento;
	}
	public String getSitRecibo() {
		return sitRecibo;
	}
	public void setSitRecibo(String sitRecibo) {
		this.sitRecibo = sitRecibo;
	}
	public BigDecimal getImpBonComPN() {
		return impBonComPN;
	}
	public void setImpBonComPN(BigDecimal impBonComPN) {
		this.impBonComPN = impBonComPN;
	}
	public BigDecimal getImpBonComRPF() {
		return impBonComRPF;
	}
	public void setImpBonComRPF(BigDecimal impBonComRPF) {
		this.impBonComRPF = impBonComRPF;
	}
	public BigDecimal getImpBonComis() {
		return impBonComis;
	}
	public void setImpBonComis(BigDecimal impBonComis) {
		this.impBonComis = impBonComis;
	}
	public BigDecimal getImpOtrosImptos() {
		return impOtrosImptos;
	}
	public void setImpOtrosImptos(BigDecimal impOtrosImptos) {
		this.impOtrosImptos = impOtrosImptos;
	}
	public BigDecimal getImpComAgtPN() {
		return impComAgtPN;
	}
	public void setImpComAgtPN(BigDecimal impComAgtPN) {
		this.impComAgtPN = impComAgtPN;
	}
	public BigDecimal getImpComAgtRPF() {
		return impComAgtRPF;
	}
	public void setImpComAgtRPF(BigDecimal impComAgtRPF) {
		this.impComAgtRPF = impComAgtRPF;
	}
	public BigDecimal getImpComAgt() {
		return impComAgt;
	}
	public void setImpComAgt(BigDecimal impComAgt) {
		this.impComAgt = impComAgt;
	}
	public BigDecimal getImpComSupPN() {
		return impComSupPN;
	}
	public void setImpComSupPN(BigDecimal impComSupPN) {
		this.impComSupPN = impComSupPN;
	}
	public BigDecimal getImpComSupRPF() {
		return impComSupRPF;
	}
	public void setImpComSupRPF(BigDecimal impComSupRPF) {
		this.impComSupRPF = impComSupRPF;
	}
	public BigDecimal getImpComSup() {
		return impComSup;
	}
	public void setImpComSup(BigDecimal impComSup) {
		this.impComSup = impComSup;
	}
	public BigDecimal getImpSobreComAgt() {
		return impSobreComAgt;
	}
	public void setImpSobreComAgt(BigDecimal impSobreComAgt) {
		this.impSobreComAgt = impSobreComAgt;
	}
	public BigDecimal getImpSobreComProm() {
		return impSobreComProm;
	}
	public void setImpSobreComProm(BigDecimal impSobreComProm) {
		this.impSobreComProm = impSobreComProm;
	}
	public BigDecimal getImpCesionDerechosAgt() {
		return impCesionDerechosAgt;
	}
	public void setImpCesionDerechosAgt(BigDecimal impCesionDerechosAgt) {
		this.impCesionDerechosAgt = impCesionDerechosAgt;
	}
	public BigDecimal getImpCesionDerechosProm() {
		return impCesionDerechosProm;
	}
	public void setImpCesionDerechosProm(BigDecimal impCesionDerechosProm) {
		this.impCesionDerechosProm = impCesionDerechosProm;
	}
	public BigDecimal getImpSobreComUdiAgt() {
		return impSobreComUdiAgt;
	}
	public void setImpSobreComUdiAgt(BigDecimal impSobreComUdiAgt) {
		this.impSobreComUdiAgt = impSobreComUdiAgt;
	}
	public BigDecimal getImpSobreComUdiProm() {
		return impSobreComUdiProm;
	}
	public void setImpSobreComUdiProm(BigDecimal impSobreComUdiProm) {
		this.impSobreComUdiProm = impSobreComUdiProm;
	}
	public BigDecimal getImpBonoAgt() {
		return impBonoAgt;
	}
	public void setImpBonoAgt(BigDecimal impBonoAgt) {
		this.impBonoAgt = impBonoAgt;
	}
	public BigDecimal getImpBomoProm() {
		return impBomoProm;
	}
	public void setImpBomoProm(BigDecimal impBomoProm) {
		this.impBomoProm = impBomoProm;
	}
	private Integer numExhibicion;
	private String cveRecibo;
	private Date fechaVencimiento;
	private Date fechaCubreDesde;
	private Date fechaCubreHasta;
	private BigDecimal impPrimaNeta;
	private BigDecimal impRcgosPagoFR;
	private BigDecimal impDerechos;
	private BigDecimal impIVA;
	private BigDecimal impPrimaTotal;


	
	private Date fechaRegistro;
	private String codigoUsuarioCreacion;
	private String cveTipoDocumento;
	private String sitRecibo;

	private BigDecimal impBonComPN;

	private BigDecimal impBonComRPF;

	private BigDecimal impBonComis;

	private BigDecimal impOtrosImptos;

	private BigDecimal impComAgtPN;

	private BigDecimal impComAgtRPF;

	private BigDecimal impComAgt;

	private BigDecimal impComSupPN;

	private BigDecimal impComSupRPF;

	private BigDecimal impComSup;

	private BigDecimal impSobreComAgt;

	private BigDecimal impSobreComProm;

	private BigDecimal impCesionDerechosAgt;

	private BigDecimal impCesionDerechosProm;
	private BigDecimal impSobreComUdiAgt;
	private BigDecimal impSobreComUdiProm;
	private BigDecimal impBonoAgt;
	private BigDecimal impBomoProm;
	
}
