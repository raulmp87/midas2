package mx.com.afirme.midas2.dto.cobranza.catalogos;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "AFR_TBL_RELGRUPOAGTE", schema = "SEYCOS")
public class RelacionGrupoAgenteDTO implements java.io.Serializable, Entidad{
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "centroEmisor", column = @Column(name = "CE")),
			@AttributeOverride(name = "numeroPoliza", column = @Column(name = "POLIZA")),
			@AttributeOverride(name = "numeroRenovacion", column = @Column(name = "REN")) })
	private RelacionGrupoAgenteIdDTO id;
	
	@Column(name="ID")
	private Integer numeroCuenta;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIFICA")
	public Date fechaModifica;
	
	@Column(name="USUARIO_MODIFICA")
	public String usuarioModifica;
	
	@Column(name="ESTATUS")
	public Integer estatus;

	public RelacionGrupoAgenteIdDTO getId() {
		return id;
	}

	public void setId(RelacionGrupoAgenteIdDTO id) {
		this.id = id;
	}

	public Integer getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(Integer numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public Date getFechaModifica() {
		return fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getUsuarioModifica() {
		return usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RelacionGrupoAgenteIdDTO getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	} 
	
}
