package mx.com.afirme.midas2.dto.cobranza.programapago;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;


public class ProgramaPagoDTO {
	private static final long serialVersionUID = 1L;
	
	private String nombreContratante;
	private Integer numRecibos;
	private Date iniProg;
	private Date finProg;
	private BigDecimal primaNeta;
	private BigDecimal recargos;
	private BigDecimal derechos;
	private BigDecimal iva;
	private BigDecimal ivaTotal;
	private List<ToRecibo> listadoRecibos;
	private BigDecimal numProgPago;
	private BigDecimal idtocotizacion;
	private BigDecimal numinciso;
	private BigDecimal impComAgt;
	
	
	
	
	public BigDecimal getImpComAgt() {
		return impComAgt;
	}

	public void setImpComAgt(BigDecimal impComAgt) {
		this.impComAgt = impComAgt;
	}

	public BigDecimal getIdtocotizacion() {
		return idtocotizacion;
	}

	public void setIdtocotizacion(BigDecimal idtocotizacion) {
		this.idtocotizacion = idtocotizacion;
	}

	public BigDecimal getNuminciso() {
		return numinciso;
	}

	public void setNuminciso(BigDecimal numinciso) {
		this.numinciso = numinciso;
	}

	public BigDecimal getNumProgPago() {
		return numProgPago;
	}

	public void setNumProgPago(BigDecimal numProgPago) {
		this.numProgPago = numProgPago;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public Integer getNumRecibos() {
		return numRecibos;
	}

	public void setNumRecibos(Integer numRecibos) {
		this.numRecibos = numRecibos;
	}

	public Date getIniProg() {
		return iniProg;
	}

	public void setIniProg(Date iniProg) {
		this.iniProg = iniProg;
	}

	public Date getFinProg() {
		return finProg;
	}

	public void setFinProg(Date finProg) {
		this.finProg = finProg;
	}

	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}

	public BigDecimal getRecargos() {
		return recargos;
	}

	public void setRecargos(BigDecimal recargos) {
		this.recargos = recargos;
	}

	public BigDecimal getDerechos() {
		return derechos;
	}

	public void setDerechos(BigDecimal derechos) {
		this.derechos = derechos;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaTotal() {
		return ivaTotal;
	}

	public void setIvaTotal(BigDecimal ivaTotal) {
		this.ivaTotal = ivaTotal;
	}

	public List<ToRecibo> getListadoRecibos() {
		return listadoRecibos;
	}

	public void setListadoRecibos(List<ToRecibo> listadoRecibos) {
		this.listadoRecibos = listadoRecibos;
	}

	

}
