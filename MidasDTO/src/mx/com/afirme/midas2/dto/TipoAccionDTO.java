package mx.com.afirme.midas2.dto;

import java.io.Serializable;

public class TipoAccionDTO implements Serializable {
	
	private static final long serialVersionUID = -3413118302475616096L;
	
	private static final String agregarModificar = "1";
	
	private static final String ver = "2";
	
	private static final String eliminar = "3";
	
	private static final String editar = "4";
	
	private static final String filtrar = "5";
	
	private static final String nuevoEndosoCot = "1";
	
	private static final String editarEndosoCot = "2";
	
	private static final String consultarEndosoCot = "3";
	
	private static final String altaIncisoEndosoCot = "4";
	
	public static String getAgregarModificar() {
		return agregarModificar;
	}
	
	public static String getVer() {
		return ver;
	}
	
	public static String getEliminar() {
		return eliminar;
	}
	
	public static String getEditar(){
		return editar;
	}
	
	public static String getFiltrar(){
		return filtrar;
	}
	
	public static String getNuevoEndosoCot() {
		return nuevoEndosoCot;
	}
	
	public static String getEditarEndosoCot() {
		return editarEndosoCot;
	}

	public static String getConsultarEndosoCot() {
		return consultarEndosoCot;
	}

	public static String getAltaIncisoEndosoCot() {
		return altaIncisoEndosoCot;
	}
	
	
}
