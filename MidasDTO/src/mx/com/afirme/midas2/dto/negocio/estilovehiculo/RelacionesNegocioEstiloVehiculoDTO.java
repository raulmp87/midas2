package mx.com.afirme.midas2.dto.negocio.estilovehiculo;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;

public class RelacionesNegocioEstiloVehiculoDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<NegocioEstiloVehiculo> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioEstiloVehiculo> disponibles) {
		this.disponibles = disponibles;
	}
	public List<NegocioEstiloVehiculo> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioEstiloVehiculo> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioEstiloVehiculo> disponibles;
	public List<NegocioEstiloVehiculo> asociadas;
}
