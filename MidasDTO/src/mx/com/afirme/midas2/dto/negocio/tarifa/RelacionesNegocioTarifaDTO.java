package mx.com.afirme.midas2.dto.negocio.tarifa;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;

public class RelacionesNegocioTarifaDTO  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private NegocioSeccion negocioSeccion;
	private NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion;
	private MonedaDTO monedaDTO;
	private List<AgrupadorTarifaSeccion> posibles;

	public void setPosibles(List<AgrupadorTarifaSeccion> posibles) {
		this.posibles = posibles;
	}

	public List<AgrupadorTarifaSeccion> getPosibles() {
		return posibles;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioAgrupadorTarifaSeccion(
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion) {
		this.negocioAgrupadorTarifaSeccion = negocioAgrupadorTarifaSeccion;
	}

	public NegocioAgrupadorTarifaSeccion getNegocioAgrupadorTarifaSeccion() {
		return negocioAgrupadorTarifaSeccion;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

}
