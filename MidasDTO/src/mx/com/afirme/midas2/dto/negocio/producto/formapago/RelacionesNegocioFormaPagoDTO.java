package mx.com.afirme.midas2.dto.negocio.producto.formapago;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;

public class RelacionesNegocioFormaPagoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1929199538629168724L;
	
	private List<NegocioFormaPago> asociadas;
	private List<NegocioFormaPago> disponibles;
	public List<NegocioFormaPago> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioFormaPago> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioFormaPago> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioFormaPago> disponibles) {
		this.disponibles = disponibles;
	}

	
}
