package mx.com.afirme.midas2.dto.negocio.cliente;

import java.io.Serializable;

public class ResultadoActualizacionNodo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tipoRespuesta;
	private String idOriginal;
	private String idResultado;
	private String tipoMensaje;
	private Boolean operacionExitosa;
	private String mensaje;
	private String idNegocio;
	
	
	public String getTipoRespuesta() {
		return tipoRespuesta;
	}
	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	public String getIdOriginal() {
		return idOriginal;
	}
	public void setIdOriginal(String idOriginal) {
		this.idOriginal = idOriginal;
	}
	public String getIdResultado() {
		return idResultado;
	}
	public void setIdResultado(String idResultado) {
		this.idResultado = idResultado;
	}
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public Boolean getOperacionExitosa() {
		return operacionExitosa;
	}
	public void setOperacionExitosa(Boolean operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(String idNegocio) {
		this.idNegocio = idNegocio;
	}
}
