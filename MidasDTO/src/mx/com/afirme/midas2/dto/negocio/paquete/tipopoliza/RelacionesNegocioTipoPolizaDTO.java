package mx.com.afirme.midas2.dto.negocio.paquete.tipopoliza;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

public class RelacionesNegocioTipoPolizaDTO implements Serializable{

	private static final long serialVersionUID = 1469211129613956375L;
	private List<NegocioTipoPoliza> disponibles;
	private List<NegocioTipoPoliza> asociadas;


	public List<NegocioTipoPoliza> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioTipoPoliza> disponibles) {
		this.disponibles = disponibles;
	}
	public List<NegocioTipoPoliza> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioTipoPoliza> asociadas) {
		this.asociadas = asociadas;
	}
	
}
