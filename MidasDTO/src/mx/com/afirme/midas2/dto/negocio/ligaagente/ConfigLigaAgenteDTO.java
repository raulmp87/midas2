package mx.com.afirme.midas2.dto.negocio.ligaagente;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

import org.springframework.stereotype.Component;

@Component
public class ConfigLigaAgenteDTO {

	private Long 	id;
	private String	nombre;
	private String	estatus;
	private String	descripcion;
	private String	correo;
	private Date	fechaCreacion;
	private String	codigoUsuarioCreacion;
	private Long	idAgente;
	private String	nombreAgente;
	private String	descripcionNegocio;
	private String 	liga;
	
	private Long	idNegocio;
	private Date	fechaInicio;
	private Date	fechaFin;
	
	private Boolean emitir;
	
	
	public ConfigLigaAgenteDTO(){
	}
	
	public ConfigLigaAgenteDTO(Long id, String nombre, String estatus,
			String descripcion, String correo, Date fechaCreacion, 
			String codigoUsuarioCreacion, Long idAgente, String nombreAgente,
			String descripcionNegocio, String liga, Boolean emitir) {
		this.id = id;
		this.nombre = nombre;
		this.estatus = estatus;
		this.descripcion = descripcion;
		this.correo = correo;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.nombreAgente = nombreAgente;
		this.idAgente = idAgente;
		this.descripcionNegocio = descripcionNegocio;
		this.liga = liga;
		this.emitir = emitir;
	}

	@Exportable(columnName="CODIGO", columnOrder=0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Exportable(columnName="NOMBRE", columnOrder=1)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Exportable(columnName="ESTATUS", columnOrder=5)
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Exportable(columnName="DESCRIPCION", columnOrder=8)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Exportable(columnName="CORREO", columnOrder=9)
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Exportable(columnName="FECHA", columnOrder=6)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Exportable(columnName="USUARIO CREACION", columnOrder=7)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Exportable(columnName="ID AGENTE", columnOrder=2)
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@Exportable(columnName="NOMBRE AGENTE", columnOrder=3)
	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	@Exportable(columnName="NEGOCIO", columnOrder=4)
	public String getDescripcionNegocio() {
		return descripcionNegocio;
	}

	public void setDescripcionNegocio(String descripcionNegocio) {
		this.descripcionNegocio = descripcionNegocio;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	@Exportable(columnName="PERMITE EMITIR", columnOrder=10, fixedValues="true=SI,false=NO")
	public Boolean getEmitir() {
		return emitir;
	}

	public void setEmitir(Boolean emitir) {
		this.emitir = emitir;
	}
	
}
