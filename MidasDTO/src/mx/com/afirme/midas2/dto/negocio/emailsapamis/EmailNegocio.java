package mx.com.afirme.midas2.dto.negocio.emailsapamis;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dto.general.Email;

/*********************************************************************************
 * Nombre de la Clase: 	EmailNegocio
 * Descripción:			Entidad para el manejo de las transacciones con la tabla
 * 						TONEGEMAIL de la BD.
 * 
 * @author 				eduardo.chavez
 * @since				2015-10-14
 *********************************************************************************/
@Entity
@Table(name="TONEGEMAIL", schema = "MIDAS")
public class EmailNegocio implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTONEGEMAIL")
	@SequenceGenerator(name="SEQTONEGEMAIL", schema = "MIDAS", sequenceName="SEQTONEGEMAIL",allocationSize=1)
	@Column(name="IDTONEGEMAIL")
	private long idEmailNegocio;

	@ManyToOne
	@JoinColumn(name="IDTCEMAIL")
	private Email email;
	
	@Column(name="IDTONEGOCIO")
	private long idNegocio;

	@ManyToOne
	@JoinColumn(name="IDTONEGEMAILTIPOUSUARIO")
	private EmailNegocioTipoUsuario emailNegocioTipoUsuario;
	
	@Column(name="ESTATUS")
	private long estatus;
	
	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
	
	public long getIdEmailNegocio() {
		return idEmailNegocio;
	}

	public void setIdEmailNegocio(long idEmailNegocio) {
		this.idEmailNegocio = idEmailNegocio;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public EmailNegocioTipoUsuario getEmailNegocioTipoUsuario() {
		return emailNegocioTipoUsuario;
	}

	public void setEmailNegocioTipoUsuario(
			EmailNegocioTipoUsuario emailNegocioTipoUsuario) {
		this.emailNegocioTipoUsuario = emailNegocioTipoUsuario;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}
	
	/*************************************************
	 * 												 *
	 * Métodos implementados de la Interfaz Entidad. *
	 *  											 *
	 *************************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmailNegocio()==0?null:getIdEmailNegocio();
	}

	@Override
	public String getValue() {		
		return "{ " 
				+ "email: " + getEmail().getValue() + " | "
				+ "idNegocio: " + getIdNegocio() + " | "
				+ "emailNegocioTipoUsuario: " + getEmailNegocioTipoUsuario().getValue() +
			   " }";
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
