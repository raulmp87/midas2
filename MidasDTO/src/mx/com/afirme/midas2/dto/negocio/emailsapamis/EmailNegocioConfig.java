package mx.com.afirme.midas2.dto.negocio.emailsapamis;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TONEGEMAILCONFIG", schema = "MIDAS")
public class EmailNegocioConfig  implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTONEGEMAILCONFIG")
	@SequenceGenerator(name="SEQTONEGEMAILCONFIG", schema = "MIDAS", sequenceName="SEQTONEGEMAILCONFIG",allocationSize=1)
	@Column(name="IDTONEGEMAILCONFIG")
	private long idEmailNegocioConfig;

	@ManyToOne
	@JoinColumn(name="IDTONEGALERTAS")
	private EmailNegocioAlertas alertasEmailNegocio;

	@ManyToOne
	@JoinColumn(name="IDTONEGEMAIL")
	private EmailNegocio emailNegocio;

	@Column(name="ESTATUS")
	private long estatus;

	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
	
	public long getIdEmailNegocioConfig() {
		return idEmailNegocioConfig;
	}

	public void setIdEmailNegocioConfig(long idEmailNegocioConfig) {
		this.idEmailNegocioConfig = idEmailNegocioConfig;
	}
	
	public EmailNegocioAlertas getAlertasEmailNegocio() {
		return alertasEmailNegocio;
	}

	public void setAlertasEmailNegocio(EmailNegocioAlertas alertasEmailNegocio) {
		this.alertasEmailNegocio = alertasEmailNegocio;
	}

	public EmailNegocio getEmailNegocio() {
		return emailNegocio;
	}

	public void setEmailNegocio(EmailNegocio emailNegocio) {
		this.emailNegocio = emailNegocio;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}
	
	/*************************************************
	 * 												 *
	 * Métodos implementados de la Interfaz Entidad. *
	 *  											 *
	 *************************************************/
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmailNegocioConfig()==0?null:getIdEmailNegocioConfig();
	}

	@Override
	public String getValue() {
		return  "{ "
					+ "alertasEmailNegocio: " + getAlertasEmailNegocio().getValue() + " | " 
					+ "emailNegocio: " + getEmailNegocio().getValue() +
				" }";
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
