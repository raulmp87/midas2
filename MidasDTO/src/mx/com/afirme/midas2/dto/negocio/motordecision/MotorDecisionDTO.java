package mx.com.afirme.midas2.dto.negocio.motordecision;

import java.util.List;

public class MotorDecisionDTO {

	public enum TipoConsulta{
		TIPO_CONSULTA_SUMASASEGURADAS,
		TIPO_CONSULTA_DERECHOS
	};
	
	private List<FiltrosMotorDecisionDTO> filtrosDTO;
	private TipoConsulta tipoConsulta;
	private Long idToNegocio;
	
	public List<FiltrosMotorDecisionDTO> getFiltrosDTO() {
		return filtrosDTO;
	}
	public void setFiltrosDTO(List<FiltrosMotorDecisionDTO> filtrosDTO) {
		this.filtrosDTO = filtrosDTO;
	}
	
	public TipoConsulta getTipoConsulta() {
		return tipoConsulta;
	}
	public void setTipoConsulta(TipoConsulta tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	
	public class FiltrosMotorDecisionDTO{
		private Boolean requerido;
		private String nombreFiltro;
		private Object valorFiltro;
		
		public FiltrosMotorDecisionDTO(){
			
		}
		
		public FiltrosMotorDecisionDTO(Boolean requerido, String nombreFiltro,
				Object valorFiltro) {
			super();
			this.requerido = requerido;
			this.nombreFiltro = nombreFiltro;
			this.valorFiltro = valorFiltro;
		}
		public Boolean getRequerido() {
			return requerido;
		}
		public void setRequerido(Boolean requerido) {
			this.requerido = requerido;
		}
		
		public String getNombreFiltro() {
			return nombreFiltro;
		}
		public void setNombreFiltro(String nombreFiltro) {
			this.nombreFiltro = nombreFiltro;
		}
		
		public Object getValorFiltro() {
			return valorFiltro;
		}
		public void setValorFiltro(Object valorFiltro) {
			this.valorFiltro = valorFiltro;
		}
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
}
