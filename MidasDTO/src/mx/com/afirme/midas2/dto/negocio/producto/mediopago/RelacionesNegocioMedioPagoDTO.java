package mx.com.afirme.midas2.dto.negocio.producto.mediopago;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;

public class RelacionesNegocioMedioPagoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9131694931091690794L;
	
	private List<NegocioMedioPago> asociadas;
	private List<NegocioMedioPago> disponibles;
	
	public List<NegocioMedioPago> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioMedioPago> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioMedioPago> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioMedioPago> disponibles) {
		this.disponibles = disponibles;
	}

}
