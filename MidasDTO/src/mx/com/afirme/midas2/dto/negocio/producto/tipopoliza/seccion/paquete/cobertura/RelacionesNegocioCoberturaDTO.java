package mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.cobertura;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

public class RelacionesNegocioCoberturaDTO implements Serializable {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
private List<NegocioCobPaqSeccion> disponibles;
private List<NegocioCobPaqSeccion> asociadas;

public List<NegocioCobPaqSeccion> getDisponibles() {
	return disponibles;
}
public void setDisponibles(List<NegocioCobPaqSeccion> disponibles) {
	this.disponibles = disponibles;
}
public List<NegocioCobPaqSeccion> getAsociadas() {
	return asociadas;
}
public void setAsociadas(List<NegocioCobPaqSeccion> asociadas) {
	this.asociadas = asociadas;
}

}
