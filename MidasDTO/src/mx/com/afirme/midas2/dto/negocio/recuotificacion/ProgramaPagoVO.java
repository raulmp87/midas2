package mx.com.afirme.midas2.dto.negocio.recuotificacion;

import java.io.Serializable;

import mx.com.afirme.midas2.util.StringUtil;

public class ProgramaPagoVO implements Serializable{
	

	private static final long serialVersionUID = 3713536082019169182L;
	private Long versionId;
	private String progPagoId;
	private String numProgPago;
	private String pcteProgPago;
	
	public ProgramaPagoVO(){
		super();
	}
	
	public ProgramaPagoVO(Long id, String progPagoId, String numProgPago, String pcteProgPago){
		this.versionId = id;
		this.progPagoId = progPagoId;
		this.numProgPago = numProgPago;
		this.pcteProgPago = pcteProgPago;
	}
	
	public Long getProgPagoIdLong(){		
		return StringUtil.isNumeric(progPagoId) ? Long.valueOf(progPagoId) : null;
	}
	
	public Integer getNumProgPagoInteger(){
		return StringUtil.isNumeric(numProgPago) ? Integer.valueOf(numProgPago) : null;
	}
	
	public Double getPcteProgPagoDouble(){
		Double pcte;
		try{
			pcte = Double.parseDouble(pcteProgPago);
		}catch(Exception ex){
			pcte = null;
		}
		return pcte;
	}
	
	public Long getVersionId() {
		return versionId;
	}
	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}
	public String getProgPagoId() {
		return progPagoId;
	}
	public void setProgPagoId(String progPagoId) {
		this.progPagoId = progPagoId;
	}
	public String getNumProgPago() {
		return numProgPago;
	}
	public void setNumProgPago(String numProgPago) {
		this.numProgPago = numProgPago;
	}
	public String getPcteProgPago() {
		return pcteProgPago;
	}
	public void setPcteProgPago(String pcteProgPago) {
		this.pcteProgPago = pcteProgPago;
	}						
}