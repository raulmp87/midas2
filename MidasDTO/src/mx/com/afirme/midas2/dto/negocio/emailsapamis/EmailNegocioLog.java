package mx.com.afirme.midas2.dto.negocio.emailsapamis;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.dto.general.Email;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;

//import mx.com.afirme.midas2.dto.sapamis.Temporal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*********************************************************************************
 * Nombre de la Clase: 	EmailNegocioLog
 * Descripción:			Entidad para el manejo de las transacciones con la tabla
 * 						TONEGEMAILLOG de la BD.
 * 
 * @author 				Luis Ibarra
 * @since				24/Nov/2015
 *********************************************************************************/
@Entity
@Table(name="TONEGEMAILLOG", schema = "MIDAS")
public class EmailNegocioLog implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTONEGEMAILLOG")
	@SequenceGenerator(name="SEQTONEGEMAILLOG", schema = "MIDAS", sequenceName="SEQTONEGEMAILLOG",allocationSize=1)
	@Column(name="IDTONEGEMAILLOG")
	private long idEmailNegocioLog;
	

	@Column(name="IDTONEGOCIO")
	private long idNegocio;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISALERTAS")
	private CatAlertasSapAmis alerta;
	
	@ManyToOne
	@JoinColumn(name="IDTCEMAIL")
	private Email email;
	
	@Column(name="CUERPO")
	private String cuerpo;
	
    @Temporal(TemporalType.DATE)
	@Column(name="FECHA")
	private Date fecha;
    
    @Column(name="ID_SAP_AMIS_ACCIONES_RELACION")
    private long idSapAmisAccionesRelacion;

	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
    
	public long getIdEmailNegocioLog() {
		return idEmailNegocioLog;
	}

	public void setIdEmailNegocioLog(long idEmailNegocioLog) {
		this.idEmailNegocioLog = idEmailNegocioLog;
	}

	public long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public CatAlertasSapAmis getAlerta() {
		return alerta;
	}

	public void setAlerta(CatAlertasSapAmis alerta) {
		this.alerta = alerta;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
    
	public long getIdSapAmisAccionesRelacion() {
		return idSapAmisAccionesRelacion;
	}

	public void setIdSapAmisAccionesRelacion(long idSapAmisAccionesRelacion) {
		this.idSapAmisAccionesRelacion = idSapAmisAccionesRelacion;
	}

	/*************************************************
	 * 												 *
	 * Métodos implementados de la Interfaz Entidad. *
	 *  											 *
	 *************************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmailNegocioLog()==0?null:getIdEmailNegocioLog();
	}

	@Override
	public String getValue() {		
		return getCuerpo();
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	
}
