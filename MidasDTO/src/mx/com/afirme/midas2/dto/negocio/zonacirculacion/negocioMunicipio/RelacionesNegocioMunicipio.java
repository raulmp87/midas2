package mx.com.afirme.midas2.dto.negocio.zonacirculacion.negocioMunicipio;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;

public class RelacionesNegocioMunicipio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4876633861357203538L;
	
	private List<NegocioMunicipio> asociadas;
	private List<NegocioMunicipio> disponibles;
	
	public List<NegocioMunicipio> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioMunicipio> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioMunicipio> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioMunicipio> disponibles) {
		this.disponibles = disponibles;
	}
}
