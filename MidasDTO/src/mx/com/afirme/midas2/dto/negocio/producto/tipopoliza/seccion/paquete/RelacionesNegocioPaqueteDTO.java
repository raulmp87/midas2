package mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

public class RelacionesNegocioPaqueteDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1734750006135323345L;

	private List<NegocioPaqueteSeccion> disponibles;
	private List<NegocioPaqueteSeccion> asociadas;
	
	
	public List<NegocioPaqueteSeccion> getDisponibles(){
		return disponibles;
	}

	public void setDisponibles(List<NegocioPaqueteSeccion> disponibles){
		this.disponibles =  disponibles;
	}

	public List<NegocioPaqueteSeccion> getAsociadas(){
		return asociadas;
	}

	public void setAsociadas(List<NegocioPaqueteSeccion> asociadas){
		this.asociadas =  asociadas;
	}




}