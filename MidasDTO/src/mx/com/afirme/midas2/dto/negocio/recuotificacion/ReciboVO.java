package mx.com.afirme.midas2.dto.negocio.recuotificacion;

import java.io.Serializable;

import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService.TipoSaldo;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;

public class ReciboVO implements Serializable{

	private static final long serialVersionUID = -6371345837944924597L;
	private Long progPagoId;
	private String reciboId;
	private String tipoModificacion;
	private String numRecibo;
	private String pcteRecibo;
	
	public ReciboVO(){
		super();
	}
	
	public ReciboVO(Long progPagoId, String reciboId, String tipoModificacion, String numRecibo, String pcteRecibo){
		this.progPagoId = progPagoId;
		this.reciboId = reciboId;
		this.tipoModificacion = tipoModificacion;
		this.numRecibo = numRecibo;
		this.pcteRecibo = pcteRecibo;
	}
	
	public Long getProgPagoId() {
		return progPagoId;
	}

	public void setProgPagoId(Long progPagoId) {
		this.progPagoId = progPagoId;
	}
	
	public TipoSaldo getTipoModificacionEnum(){
		return EnumUtil.fromValue(TipoSaldo.class, tipoModificacion);
	}
	
	public String getTipoModificacion() {
		return tipoModificacion;
	}

	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}

	public Integer getNumProgPagoInteger(){
		return StringUtil.isNumeric(numRecibo) ? Integer.valueOf(numRecibo) : null;
	}
		
	public String getNumRecibo() {
		return numRecibo;
	}

	public void setNumRecibo(String numRecibo) {
		this.numRecibo = numRecibo;
	}

	public Double getPcteReciboDouble(){
		Double pcte;
		try{
			pcte = Double.parseDouble(pcteRecibo);
		}catch(Exception ex){
			pcte = null;
		}
		return pcte;
	}
	
	public String getPcteRecibo() {
		return pcteRecibo;
	}

	public void setPcteRecibo(String pcteRecibo) {
		this.pcteRecibo = pcteRecibo;
	}

	public Integer getReciboIdInteger(){
		return StringUtil.isNumeric(reciboId) ? Integer.valueOf(reciboId) : null;
	}
	
	public String getReciboId() {
		return reciboId;
	}

	public void setReciboId(String reciboId) {
		this.reciboId = reciboId;
	}

	
	
	
}
