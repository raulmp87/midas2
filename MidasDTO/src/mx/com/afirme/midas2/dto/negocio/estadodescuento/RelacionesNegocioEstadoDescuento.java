package mx.com.afirme.midas2.dto.negocio.estadodescuento;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;

public class RelacionesNegocioEstadoDescuento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1199158976917126711L;

	private List<NegocioEstadoDescuento> asociadas;
	private List<NegocioEstadoDescuento> disponibles;
	
	public List<NegocioEstadoDescuento> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioEstadoDescuento> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioEstadoDescuento> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioEstadoDescuento> disponibles) {
		this.disponibles = disponibles;
	}
}