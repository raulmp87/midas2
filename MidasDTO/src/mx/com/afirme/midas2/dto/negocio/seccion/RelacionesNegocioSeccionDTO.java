package mx.com.afirme.midas2.dto.negocio.seccion;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

public class RelacionesNegocioSeccionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<NegocioSeccion> disponibles;
	private List<NegocioSeccion> asociadas;
	
	
	public void setDisponibles(List<NegocioSeccion> disponibles) {
		this.disponibles = disponibles;
	}
	public List<NegocioSeccion> getDisponibles() {
		return disponibles;
	}
	public void setAsociadas(List<NegocioSeccion> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioSeccion> getAsociadas() {
		return asociadas;
	}
	
	
}
