package mx.com.afirme.midas2.dto.negocio.emailsapamis;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;

/*********************************************************************************
 * Nombre de la Clase: 	EmailNegocioConfig
 * Descripción:			Entidad para el manejo de las transacciones con la tabla
 * 						TONEGEMAILCONFIG de la BD.
 * 
 * @author 				eduardo.chavez
 * @since				2015-10-16
 *********************************************************************************/
@Entity
@Table(name="TONEGALERTAS", schema = "MIDAS")
public class EmailNegocioAlertas  implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTONEGALERTAS")
	@SequenceGenerator(name="SEQTONEGALERTAS", schema = "MIDAS", sequenceName="SEQTONEGALERTAS",allocationSize=1)
	@Column(name="IDTONEGALERTAS")
	private long idEmailNegocioAlertas;
	
	@Column(name="IDTONEGOCIO")
	private long idNegocio;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISALERTAS")
	private CatAlertasSapAmis catAlertasSapAmis;
	
	@Column(name="ESTATUS")
	private long estatus;

	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
	

	public long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public CatAlertasSapAmis getCatAlertasSapAmis() {
		return catAlertasSapAmis;
	}

	public void setCatAlertasSapAmis(CatAlertasSapAmis catAlertasSapAmis) {
		this.catAlertasSapAmis = catAlertasSapAmis;
	}
	
	public long getIdEmailNegocioAlertas() {
		return idEmailNegocioAlertas;
	}

	public void setIdEmailNegocioAlertas(long idEmailNegocioAlertas) {
		this.idEmailNegocioAlertas = idEmailNegocioAlertas;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}
	
	/*************************************************
	 * 												 *
	 * Métodos implementados de la Interfaz Entidad. *
	 *  											 *
	 *************************************************/

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmailNegocioAlertas()==0?null:getIdEmailNegocioAlertas();
	}

	@Override
	public String getValue() {
		return "idNegocio: " + getIdNegocio() +
				" | catAlertasSapAmis: { " + getCatAlertasSapAmis().getValue() + " }";
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
