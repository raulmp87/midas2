package mx.com.afirme.midas2.dto.negocio.motordecision;

import java.util.List;

public class ResultadoMotorDecisionAtributosDTO {

	public static enum TipoResultado{
		RES_TIPO_DERECHOS,
		RES_TIPO_SUMASE_MAXMIN,
		RES_TIPO_SUMASE_VALORES
	}; 
	public static enum TipoValor{
		TIPO_VALOR_DEFAULT,
		TIPO_VALOR_MINIMO,
		TIPO_VALOR_MAXIMO,
		TIPO_VALOR_GENERICO
	}
	
	private List<Object> valores;
	private String texto;
	private TipoResultado tipoResultado;
	
	public List<Object> getValores() {
		return valores;
	}
	public void setValores(List<Object> valores) {
		this.valores = valores;
	}

	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	public TipoResultado getTipoResultado() {
		return tipoResultado;
	}
	public void setTipoResultado(TipoResultado tipoResultado) {
		this.tipoResultado = tipoResultado;
	}

	
	public class ValorTipo{
		private String valor;
		private TipoValor tipValor;
		private Long IdKey;
		
		public String getValor() {
			return valor;
		}
		public void setValor(String valor) {
			this.valor = valor;
		}
		
		public TipoValor getTipValor() {
			return tipValor;
		}
		public void setTipValor(TipoValor tipValor) {
			this.tipValor = tipValor;
		}
		
		public Long getIdKey() {
			return IdKey;
		}
		public void setIdKey(Long idKey) {
			IdKey = idKey;
		}
	}
	
}
