package mx.com.afirme.midas2.dto.negocio.antecedentes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAComentarios;

public class PreparaListasNegocioAntecedentes implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<NegocioCEAAnexos> anexos;
	private List<NegocioCEAComentarios> comentarios;
	
	public PreparaListasNegocioAntecedentes(){
		anexos = new ArrayList<NegocioCEAAnexos>();
		comentarios = new ArrayList<NegocioCEAComentarios>(); 
	}

	public List<NegocioCEAAnexos> getAnexos() {
		return anexos;
	}
	public void setAnexos(List<NegocioCEAAnexos> anexos) {
		this.anexos = anexos;
	}

	public List<NegocioCEAComentarios> getComentarios() {
		return comentarios;
	}
	public void setComentarios(List<NegocioCEAComentarios> comentarios) {
		this.comentarios = comentarios;
	}
}