package mx.com.afirme.midas2.dto.negocio.emailsapamis;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*********************************************************************************
 * Nombre de la Clase: 	EmailNegocioTipoUsuario
 * Descripción:			Entidad para el manejo de las transacciones con la tabla
 * 						TONEGEMAILTIPOUSUARIO de la BD.
 * 
 * @author 				eduardo.chavez
 * @since				2015-10-15
 *********************************************************************************/
@Entity
@Table(name="TONEGEMAILTIPOUSUARIO", schema = "MIDAS")
public class EmailNegocioTipoUsuario implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTONEGEMAILTIPOUSUARIO")
	@SequenceGenerator(name="SEQTONEGEMAILTIPOUSUARIO", schema = "MIDAS", sequenceName="SEQTONEGEMAILTIPOUSUARIO",allocationSize=1)
	@Column(name="IDTONEGEMAILTIPOUSUARIO")
	private long idEmailNegocioTipoUsuario;
	
	@Column(name="TONEGEMAILTIPOUSUARIO")
	private String emailNegocioTipoUsuario;
	
	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
	
	public long getIdEmailNegocioTipoUsuario() {
		return idEmailNegocioTipoUsuario;
	}

	public void setIdEmailNegocioTipoUsuario(long idEmailNegocioTipoUsuario) {
		this.idEmailNegocioTipoUsuario = idEmailNegocioTipoUsuario;
	}

	public String getEmailNegocioTipoUsuario() {
		return emailNegocioTipoUsuario;
	}

	public void setEmailNegocioTipoUsuario(String emailNegocioTipoUsuario) {
		this.emailNegocioTipoUsuario = emailNegocioTipoUsuario;
	}

	/*************************************************
	 * 												 *
	 * Métodos implementados de la Interfaz Entidad. *
	 *  											 *
	 *************************************************/

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmailNegocioTipoUsuario()==0?null:getIdEmailNegocioTipoUsuario();
	}

	@Override
	public String getValue() {
		return "{ " 
					+ "emailNegocioTipoUsuario: " + getEmailNegocioTipoUsuario() +
			    " }";
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
