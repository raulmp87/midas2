package mx.com.afirme.midas2.dto.negocio.derechos;

import java.math.BigDecimal;

public class NegocioConfiguracionDerechoDTO {

	private BigDecimal idToNegConfigDerecho;
	
	private Long idToNegocio;
	private String negocioDescripcion;
	
	private BigDecimal idToNegTipoPoliza;
	private String tipoPolizaDescripcion;
	
	private BigDecimal idToNegSeccion;
	private String seccionDescripcion;
	
	private Long idToNegPaqueteSeccion;
	private String paqueteDescripcion;
	
	private Short idTcMoneda;
	private String monedaDescripcion;
	
	private String idTipoDerecho;
	private String tipoDerechoDescripcion;
	
	private Double importeDerecho;
	private boolean importeDefault;
	
	private BigDecimal idTcTipoUsoVehiculo;
	private String tipoUsoDescripcion;
	
	private String stateId;
	private String stateName;
	
	private String cityId;
	private String cityName;
	
	private Integer totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	
	public NegocioConfiguracionDerechoDTO() {
		super();
	}
	
	public NegocioConfiguracionDerechoDTO(BigDecimal idToNegConfigDerecho,
			Long idToNegocio, String negocioDescripcion,
			BigDecimal idToNegTipoPoliza, String tipoPolizaDescripcion,
			BigDecimal idToNegSeccion, String seccionDescripcion,
			Long idToNegPaqueteSeccion, String paqueteDescripcion,
			Short idTcMoneda, String monedaDescripcion, String idTipoDerecho,
			String tipoDerechoDescripcion, Double importeDerecho,
			BigDecimal idTcTipoUsoVehiculo, String tipoUsoDescripcion,
			String stateId, String stateName, String cityId, String cityName, boolean importeDefault) {
		super();
		this.idToNegConfigDerecho = idToNegConfigDerecho;
		this.idToNegocio = idToNegocio;
		this.negocioDescripcion = negocioDescripcion;
		this.idToNegTipoPoliza = idToNegTipoPoliza;
		this.tipoPolizaDescripcion = tipoPolizaDescripcion;
		this.idToNegSeccion = idToNegSeccion;
		this.seccionDescripcion = seccionDescripcion;
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
		this.paqueteDescripcion = paqueteDescripcion;
		this.idTcMoneda = idTcMoneda;
		this.monedaDescripcion = monedaDescripcion;
		this.idTipoDerecho = idTipoDerecho;
		this.tipoDerechoDescripcion = tipoDerechoDescripcion;
		this.importeDerecho = importeDerecho;
		this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
		this.tipoUsoDescripcion = tipoUsoDescripcion;
		this.stateId = stateId;
		this.stateName = stateName;
		this.cityId = cityId;
		this.cityName = cityName;
		this.importeDefault = importeDefault;
	}
	public BigDecimal getIdToNegConfigDerecho() {
		return idToNegConfigDerecho;
	}
	public void setIdToNegConfigDerecho(BigDecimal idToNegConfigDerecho) {
		this.idToNegConfigDerecho = idToNegConfigDerecho;
	}
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	public String getNegocioDescripcion() {
		return negocioDescripcion;
	}
	public void setNegocioDescripcion(String negocioDescripcion) {
		this.negocioDescripcion = negocioDescripcion;
	}
	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}
	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}
	public String getTipoPolizaDescripcion() {
		return tipoPolizaDescripcion;
	}
	public void setTipoPolizaDescripcion(String tipoPolizaDescripcion) {
		this.tipoPolizaDescripcion = tipoPolizaDescripcion;
	}
	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}
	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}
	public String getSeccionDescripcion() {
		return seccionDescripcion;
	}
	public void setSeccionDescripcion(String seccionDescripcion) {
		this.seccionDescripcion = seccionDescripcion;
	}
	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}
	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}
	public String getPaqueteDescripcion() {
		return paqueteDescripcion;
	}
	public void setPaqueteDescripcion(String paqueteDescripcion) {
		this.paqueteDescripcion = paqueteDescripcion;
	}
	public Short getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(Short idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public String getMonedaDescripcion() {
		return monedaDescripcion;
	}
	public void setMonedaDescripcion(String monedaDescripcion) {
		this.monedaDescripcion = monedaDescripcion;
	}
	public String getIdTipoDerecho() {
		return idTipoDerecho;
	}
	public void setIdTipoDerecho(String idTipoDerecho) {
		this.idTipoDerecho = idTipoDerecho;
	}
	public String getTipoDerechoDescripcion() {
		return tipoDerechoDescripcion;
	}
	public void setTipoDerechoDescripcion(String tipoDerechoDescripcion) {
		this.tipoDerechoDescripcion = tipoDerechoDescripcion;
	}
	public Double getImporteDerecho() {
		return importeDerecho;
	}
	public void setImporteDerecho(Double importeDerecho) {
		this.importeDerecho = importeDerecho;
	}
	public BigDecimal getIdTcTipoUsoVehiculo() {
		return idTcTipoUsoVehiculo;
	}
	public void setIdTcTipoUsoVehiculo(BigDecimal idTcTipoUsoVehiculo) {
		this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
	}
	public String getTipoUsoDescripcion() {
		return tipoUsoDescripcion;
	}
	public void setTipoUsoDescripcion(String tipoUsoDescripcion) {
		this.tipoUsoDescripcion = tipoUsoDescripcion;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

	public boolean isImporteDefault() {
		return importeDefault;
	}

	public void setImporteDefault(boolean importeDefault) {
		this.importeDefault = importeDefault;
	}
	
	
	
}
