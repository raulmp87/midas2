package mx.com.afirme.midas2.dto.negocio.ligaagente;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

import org.springframework.stereotype.Component;

@Component
public class MonitoreoLigaAgenteDTO {

	private Long		id;
	private String		nombre;
	private Long		idAgente;
	private String		nombreAgente;
	private String		descripcionNegocio;
	private BigDecimal	idCotizacion;
	private Date		fechaCotizacion;
	private String		prospectoCotizacion;
	private String		telefonoProspecto;
	private String		correoProspecto;
	private String		producto;
	private String		tipoPoliza;
	private String		lineaNegocio;
	private String		marca;
	private String		modelo;
	private String		descripcionFinalVehiculo;
	private String		numeroSerieVehiculo;
	private Double		primaNeta;
	private Double		primaTotal;
	private String		numeroPoliza;
	private Date		fechaEmision;
	
	private Long		contadorTotal;
	private Long		contadorPolizas;
	
	private Date		fechaInicio;
	private Date		fechaFin;
	private boolean		incluirPolizas;
	private Long		idNegocio;
	
	private Integer		totalCount;
	private Integer		posStart;
	private Integer		count;
	private String		orderByAttribute;
	
	public MonitoreoLigaAgenteDTO(){
	}
	
	public MonitoreoLigaAgenteDTO(Long id, String nombre, Long idAgente,
			String nombreAgente, String descripcionNegocio,
			BigDecimal idCotizacion, Date fechaCotizacion,
			String prospectoCotizacion, String telefonoProspecto,
			String correoProspecto, String producto, String tipoPoliza,
			String lineaNegocio, String marca, String modelo,
			String descripcionFinalVehiculo, String numeroSerieVehiculo,
			Double primaNeta, Double primaTotal, String numeroPoliza,
			Date fechaEmision) {
		this.id = id;
		this.nombre = nombre;
		this.idAgente = idAgente;
		this.nombreAgente = nombreAgente;
		this.descripcionNegocio = descripcionNegocio;
		this.idCotizacion = idCotizacion;
		this.fechaCotizacion = fechaCotizacion;
		this.prospectoCotizacion = prospectoCotizacion;
		this.telefonoProspecto = telefonoProspecto;
		this.correoProspecto = correoProspecto;
		this.producto = producto;
		this.tipoPoliza = tipoPoliza;
		this.lineaNegocio = lineaNegocio;
		this.marca = marca;
		this.modelo = modelo;
		this.descripcionFinalVehiculo = descripcionFinalVehiculo;
		this.numeroSerieVehiculo = numeroSerieVehiculo;
		this.primaNeta = primaNeta;
		this.primaTotal = primaTotal;
		this.numeroPoliza = numeroPoliza;
		this.fechaEmision = fechaEmision;
	}
	@Exportable(columnName="CODIGO", columnOrder=0)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Exportable(columnName="NOMBRE", columnOrder=1)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Exportable(columnName="ID AGENTE", columnOrder=2)
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	@Exportable(columnName="NOMBRE AGENTE", columnOrder=3)
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	@Exportable(columnName="NEGOCIO", columnOrder=4)
	public String getDescripcionNegocio() {
		return descripcionNegocio;
	}
	public void setDescripcionNegocio(String descripcionNegocio) {
		this.descripcionNegocio = descripcionNegocio;
	}
	@Exportable(columnName="COTIZACION", columnOrder=5)
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	@Exportable(columnName="FECHA COTIZACION", columnOrder=6)
	public Date getFechaCotizacion() {
		return fechaCotizacion;
	}
	public void setFechaCotizacion(Date fechaCotizacion) {
		this.fechaCotizacion = fechaCotizacion;
	}
	@Exportable(columnName="PROSPECTO", columnOrder=7)
	public String getProspectoCotizacion() {
		return prospectoCotizacion;
	}
	public void setProspectoCotizacion(String prospectoCotizacion) {
		this.prospectoCotizacion = prospectoCotizacion;
	}
	@Exportable(columnName="TELEFONO PROSPECTO", columnOrder=8)
	public String getTelefonoProspecto() {
		return telefonoProspecto;
	}
	public void setTelefonoProspecto(String telefonoProspecto) {
		this.telefonoProspecto = telefonoProspecto;
	}
	@Exportable(columnName="CORREO PROSPECTO", columnOrder=9)
	public String getCorreoProspecto() {
		return correoProspecto;
	}
	public void setCorreoProspecto(String correoProspecto) {
		this.correoProspecto = correoProspecto;
	}
	@Exportable(columnName="PRODUCTO", columnOrder=10)
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	@Exportable(columnName="TIPO POLIZA", columnOrder=11)
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	@Exportable(columnName="LINEA DE NEGOCIO", columnOrder=12)
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	@Exportable(columnName="MARCA", columnOrder=13)
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	@Exportable(columnName="MODELO", columnOrder=14)
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	@Exportable(columnName="DESCRIPCION FINAL", columnOrder=15)
	public String getDescripcionFinalVehiculo() {
		return descripcionFinalVehiculo;
	}
	public void setDescripcionFinalVehiculo(String descripcionFinalVehiculo) {
		this.descripcionFinalVehiculo = descripcionFinalVehiculo;
	}
	@Exportable(columnName="NUMERO SERIE", columnOrder=16)
	public String getNumeroSerieVehiculo() {
		return numeroSerieVehiculo;
	}
	public void setNumeroSerieVehiculo(String numeroSerieVehiculo) {
		this.numeroSerieVehiculo = numeroSerieVehiculo;
	}
	@Exportable(columnName="PRIMA NETA", columnOrder=17)
	public Double getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	@Exportable(columnName="PRIMA TOTAL", columnOrder=18)
	public Double getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}
	@Exportable(columnName="POLIZA", columnOrder=19)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	@Exportable(columnName="FECHA EMISION", columnOrder=20)
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public boolean isIncluirPolizas() {
		return incluirPolizas;
	}
	public void setIncluirPolizas(boolean incluirPolizas) {
		this.incluirPolizas = incluirPolizas;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

	public Long getContadorTotal() {
		return contadorTotal;
	}

	public void setContadorTotal(Long contadorTotal) {
		this.contadorTotal = contadorTotal;
	}

	public Long getContadorPolizas() {
		return contadorPolizas;
	}

	public void setContadorPolizas(Long contadorPolizas) {
		this.contadorPolizas = contadorPolizas;
	}	
	
	public Long getContadorCotizaciones(){
		return contadorTotal - contadorPolizas;
	}
	
}
