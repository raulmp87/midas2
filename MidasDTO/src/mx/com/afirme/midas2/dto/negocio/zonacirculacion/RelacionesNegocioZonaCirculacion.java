package mx.com.afirme.midas2.dto.negocio.zonacirculacion;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;

public class RelacionesNegocioZonaCirculacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4876633861357203538L;
	
	private List<NegocioEstado> asociadas;
	private List<NegocioEstado> disponibles;
	
	public List<NegocioEstado> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioEstado> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioEstado> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioEstado> disponibles) {
		this.disponibles = disponibles;
	}
}
