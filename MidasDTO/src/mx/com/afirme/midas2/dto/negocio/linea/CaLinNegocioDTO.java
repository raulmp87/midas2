package mx.com.afirme.midas2.dto.negocio.linea;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="VCALINNEGOCIO", schema = "MIDAS")
public class CaLinNegocioDTO implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID")
	private BigDecimal id;
	
	@Column(name="IDTOPOLIZA")
	private BigDecimal idToPoliza;
	
	@Column(name="NUMEROSERIE")
	private String numeroSerie;
	
	@Column(name="CALINNEGOCIOSEYCOS")
	private int lineaNegocio;
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public int getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(int lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
