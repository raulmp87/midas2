package mx.com.afirme.midas2.dto.negocio.seccion.tiposervicio;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;

public class RelacionesNegocioTipoServicioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4876633861357203538L;
	
	private List<NegocioTipoServicio> asociadas;
	private List<NegocioTipoServicio> disponibles;
	
	public List<NegocioTipoServicio> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioTipoServicio> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioTipoServicio> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioTipoServicio> disponibles) {
		this.disponibles = disponibles;
	}
}
