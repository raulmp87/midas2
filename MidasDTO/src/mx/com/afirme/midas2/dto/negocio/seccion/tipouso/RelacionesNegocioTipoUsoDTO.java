package mx.com.afirme.midas2.dto.negocio.seccion.tipouso;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;

public class RelacionesNegocioTipoUsoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5758775337105124013L;
	
	private List<NegocioTipoUso> asociadas;
	private List<NegocioTipoUso> disponibles;
	
	public List<NegocioTipoUso> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<NegocioTipoUso> asociadas) {
		this.asociadas = asociadas;
	}
	public List<NegocioTipoUso> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<NegocioTipoUso> disponibles) {
		this.disponibles = disponibles;
	}

}
