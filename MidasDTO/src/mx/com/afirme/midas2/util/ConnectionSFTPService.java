package mx.com.afirme.midas2.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

/**
 * Clase que define la conexi&oacute;n al servidor SFTP
 * 
 * @author SEGUROS AFIRME
 * 
 * @category UTILS
 * 
 * @since 01022017
 *
 */
public class ConnectionSFTPService implements Serializable {
	
	private static final long serialVersionUID = -2226496596763169649L;
	private static final Logger LOG = LoggerFactory.getLogger(ConnectionSFTPService.class);
	
	/**
	 * <p>Constante que contiene el tipo de conexi&oacute;n a travez de la que se
	 * realizara la conexi&oacute;n con el servidor.
	 * </p>
	 */
	public static final String PROTOCOLO_CONEXION = "sftp";
	
	private Integer port;
	private String host;
	private String user;
	private String pass;
	private Session session;
	private ChannelSftp channel;
	
	private JSch javaSecure = null;
	
	/**
	 * Metodo constructor que inicializa la conexi&oacute;n al servidor.
	 * 
	 * @param generalParameter Parametro que contiene la informaci&oacute;n para realizar la conexi&oacute;n.
	 * 
	 * @throws Exception Generada al momento de intentar crear la conexi&oacute;n.
	 */
	public ConnectionSFTPService(ParametroGeneralDTO generalParameter){
		initClient(generalParameter);
	}
	
	/**
	 * M&eacute;todo que genera la conexi&oacute;n al servidor SFTP.
	 * 
	 * @param generalParameter Parametro general que contiene la informaci&oacute;n 
	 * para generar la conexi&oacute;n al servidor.
	 * 
	 * @throws Exception Generada al intentar realizar la conexi&oacute;n.
	 */
	public void initClient(ParametroGeneralDTO generalParameter){
		LOG.info("::::::::: [INF] ::::::::: >>>>>>>> Entra a inicializar el cliente SFTP");
		
		try{
			String[] atributosConexion = generalParameter.getValor().split("\\|");
			
			host = atributosConexion[0];
			port = Integer.parseInt(atributosConexion[1]);
			user = atributosConexion[2];
			pass = atributosConexion[3];
		} catch (Exception e){
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * M&eacute;todo que crea la sesi&oacute;n de conexi&oacute;n
	 * al servidor SFTP
	 * 
	 */
	public void createSession(){
		try{
			javaSecure = new JSch();
			
			session = javaSecure.getSession(user, host, port);
			session.setPassword(pass);
			session.setConfig("StrictHostKeyChecking", "no");
			
			LOG.info("::::::::: [INF] ::::::::: ESTABLECIENDO CONEXION...");
			
		} catch (Exception e){
			new RuntimeException(e);
		}
	}
	
	/**
	 * M&eacute;todo que establece el canal de conexion
	 * al servidor a travez del protocolo SFTP.
	 * 
	 */
	public void createConnectionChannelSFTP(){
		if(session != null){
			try{
				if(!session.isConnected()){
					session.connect();
				}
				
				channel = (ChannelSftp) session.openChannel(PROTOCOLO_CONEXION);
				channel.connect();
				LOG.info("::::::::: [INF] ::::::::: CANAL DE CONEXION [SFTP]");
			} catch (Exception e){
				throw new RuntimeException(e);
			}
		} else {
			throw new RuntimeException("No est\u00e1 disponible la sesi\u00f3n.");
		}
	}
	
	/**
	 * M&eacute;todo que obtiene el archivo especificado.
	 * 
	 * @param fileName Nombre del archivo el cual se quiere recuperar.
	 * 
	 * @return InputStream que contiene el archivo.
	 */
	public InputStream getFile(String fileName){
		return getFile(fileName, null);
	}
	
	/**
	 * M&eacute;todo que obtiene el archivo especificado sobre la ruta definida.
	 * 
	 * @param fileName Nombre del archivo el cual se quiere recuperar.
	 * 
	 * @param path Ruta en la que se encuentra el archivo.
	 * 
	 * @return InputStream que contiene el archivo
	 */
	public InputStream getFile(String fileName, String path){
		if(path == null)
			path = "";
		InputStream stream = null;
		try{
			if(!isConnectedServer()){
				createSession();
				createConnectionChannelSFTP();
			}
			
			stream = channel.get(path + fileName);
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return stream;
	}
	
	/**
	 * M&eacute;todo que cierra la sesi&oacute;n del servidor.
	 */
	public void closeServerSession(){
		LOG.info("::::::::: [INF] ::::::::: CERRANDO SESI\u00f3N");
		if(session != null && session.isConnected()){
			if(channel != null && channel.isConnected()){
				channel.disconnect();
			}
			session.disconnect();
		}
	}
	
	/**
	 * M&eacute;todo que valida si la sesi&oacute;n y el canal se encuentran conectado
	 * al servidor.
	 * 
	 * @return Bandera que determina s&iacute; se encuentra o no conectado.
	 */
	public boolean isConnectedServer(){
		boolean flag = Boolean.FALSE;
		if(session != null && session.isConnected()){
			flag = channel != null && channel.isConnected();
		}
		return flag;
	}
	
	/**
	 * M&eacute;todo que evalua la existencia de un directorio en espec&iacute;fico dentro del servidor.
	 * 
	 * @param directory Nombre de la carpeta que se requiere validar s&iacute; existe.
	 * 
	 * @return respuesta positiva o negativa de la existencia del directorio.
	 */
	public boolean validateDirectory(String directory){
		boolean flag = Boolean.FALSE;
		if(isConnectedServer()){
			try{
				String currDirecory = channel.pwd();
				SftpATTRS atrs = channel.stat(currDirecory + "/" + directory);
				flag = atrs != null && atrs.isDir();
			} catch (Exception err){
				LOG.info("::::::::: [ERR] ::::::::: No existe el directorio", err);
			}
		} else {
			LOG.info("No se encuentra conectado con el servidor, no puede evaluar si existe el directorio");
			throw new RuntimeException("No se encuentra conectado con el servidor, no puede evaluar si existe el directorio");
		}
		return flag;
	}
	
	/**
	 * M&eacute;todo que se encarga de crear un nuevo directorio
	 * para almacenar los archivos de texto que se van a a generar.
	 * 
	 * @param directory Nombre del directorio.
	 */
	public void createDirectory(String directory){
		if(!validateDirectory(directory)){
			try{
				LOG.info("::::::::: [INF] ::::::::: GENERANDO DIRECTORIO " + directory);
				channel.mkdir(directory);
			} catch (Exception err){
				throw new RuntimeException(err);
			}
		}
	}
	
	/**
	 * <p>M&eacute;todo que crea un archivo de texto dentro de un directorio especifico 
	 * del servidor remoto de <b>SFTP</b></p>
	 * 
	 * @param fileName <p>Nombre que llevar&aacute; el archivo de texto.</p>
	 * 
	 * @param directoryName <p>Nombre del directorio sobre el cual se realizar&aacute; la escritura del archivo.</p>
	 * 
	 * @param data <p>Contenido del archivo de texto</p>
	 */
	public void createFileIntoDirectory(String fileName, String directoryName, String data){
		LOG.info("::::::::: [INF] >>>>>> Entra a intentar crear el archio de texto ");
		if(!validateDirectory(directoryName))
			createDirectory(directoryName);
		try{
			channel.cd(directoryName);
			
			InputStream stream = new ByteArrayInputStream(data.getBytes(TextFileUtils.ENCODING_UTF_8));
			LOG.info("::::::::: [INF] :::::: CREANDO EL ARCHIVO... ");
			channel.put(stream, fileName);
		} catch (Exception err){
			LOG.info("::::::::: [ERR] ::::::::: ", err);
			throw new RuntimeException(err);
		}
	}
	
	/**
	 * <p>M&eacute;todo que genera la conexi&oacute;n
	 * entre el servidor y la aplicaci&oacute;n.
	 * </p>
	 */
	public void connectServer(){
		if(!isConnectedServer()){
			createSession();
			createConnectionChannelSFTP();
		}
	}
}
