package mx.com.afirme.midas2.util;



import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.MultipleDocumentExportable;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @author Lizeth De La Garza
 * 
 *<p>Helper para Exportar un listado a Excel.</p>
 *<p>El listado debe tener configuradas las anotaciones de @Exportable en los métodos <b>get</b>
 *para poder identificar que columnas desean ser exportadas.</p>
 */
public class ExcelExporter  {
	
	private static final Logger LOG = Logger.getLogger(ExcelExporter.class);
	
	public static final String DEFAULT_DOCUMENT = "DEFAULT";


	private Class<?> beanClass = null;
	private String documento = DEFAULT_DOCUMENT;

	private CellStyle headerStyle;
	private Font headerFont;
	

	private SimpleDateFormat defaultSdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private Map<Integer, CellStyle> cellStyleMap = new HashMap<Integer, CellStyle>();
	
	private CellStyle csNumeric;
	
	private Workbook workbook;	
	
	private boolean autoFilter = false;
	private boolean autoSizeColumn = false;
	private String sheetName = "Hoja 1";
	private int sheetCount = 0;
	private List<Exportable> summarizeColumns = new ArrayList<Exportable>();


	/**
	 * Constructor
	 * @param clazz : La clase que contiene el listado, no debe venir nula
	 */
	public ExcelExporter(Class<?> clazz) {
		beanClass = clazz;
		workbook = new HSSFWorkbook();
	}

	/**
	 * Constructor
	 * @param clazz : La clase que contiene el listado, no debe venir nula
	 * @param documento: Documento al que pertenece la columna
	 */
	public ExcelExporter(Class<?> clazz, String documento) {
		beanClass = clazz;
		this.documento = documento;
		workbook = new HSSFWorkbook();
	}
	
	
	/**
	 * Constructor
	 * @param beanClass : La clase que contiene el listado, no debe venir nula
	 * @param workbook: La implementación del workbook a usar HSSF, XSSF, SXSSF
	 */
	public ExcelExporter(Class<?> beanClass, Workbook workbook) {
		this.beanClass = beanClass;
		this.workbook = workbook;
	}

	/**
	 * Constructor
	 * @param clazz : La clase que contiene el listado, no debe venir nula
	 * @param documento: Documento al que pertenece la columna
	 */
	public ExcelExporter(Class<?> clazz, String documento, Workbook workbook) {
		beanClass = clazz;
		this.documento = documento;
		this.workbook = workbook;
	}
	
	/**
	 * Indica si se requiere agregar un auto filtro en los encabezados.
	 * @param autoFilter true or false
	 */
	public void setAutoFilter(boolean autoFilter) {
		this.autoFilter = autoFilter;
	}
	
	/**
	 * <p>Indica si se requiere que las columnas se ajusten al tamaño de los contenidos de las celdas.</p>
	 * <p>NOTA: Esto puede provocar mayor tiempo de ejecucion proporcional al tamaño del archivo por lo que no es recomendable
	 * para listados con un nivel grande de registros.</p>
	 * @param autoSizeColumn
	 */
	public void setAutoSizeColumn(boolean autoSizeColumn) {
		this.autoSizeColumn = autoSizeColumn;
	}
	
	/**
	 * Indica el nombre de la hoja actual
	 * @param sheetName Nombre de la Hoja
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
	

	public InputStream export(Collection<?> dataSource) {
		this.workbook = getWorkbook(dataSource);
		workbook.setActiveSheet(0);
		return WriterUtil.writeWorkBook(workbook);

	}
	/**
	 * Método que genera la exportación de un listado a Excel
	 * @param dataSource @Collection de cualquier tipo que contenga anotaciones de tipo @Exportable
	 * @return InputStream del EXCEL
	 */
	public Workbook getWorkbook(Collection<?> dataSource) {

		Sheet sheet	= null;	
		int columns = 0;
		int dataRows = 0;
		
		Object headerInstance = null;

		try {
			long startTime = System.currentTimeMillis();
			if (beanClass != null) {	
				createHeaderStyle(this.workbook);

				sheet 	= this.workbook.createSheet(this.sheetName);
				sheetCount = 1;
				dataRows = 1;
				Cell cell = null;	
				
				DataFormat dataFormat = this.workbook.createDataFormat();
				
				csNumeric = this.workbook.createCellStyle();				
				csNumeric.setDataFormat(dataFormat.getFormat("0"));

				headerInstance = (Object) beanClass.newInstance();

				printHeader(headerInstance, sheet);
				

				if (dataSource != null ) {
					List<Integer> catalogs = new ArrayList<Integer>();
					
					for (Object element : dataSource) {
						
						if (dataRows == getMaxSheetRow()) {							
							sheetCount++;
							sheet = this.workbook.createSheet(this.sheetName + '-' + sheetCount);
							printHeader(headerInstance, sheet);		
							dataRows = 1;
						}
						
						Row row = sheet.createRow(dataRows);
						dataRows++;						

						for (Method method : element.getClass().getMethods()) {	
							Exportable annotation = getExportableAnnotation(method);

							if (annotation != null) {

								cell = row.createCell(annotation.columnOrder());

								Object value = method.invoke(element);		

								if (annotation.fixedValues() != null && !annotation.fixedValues().equals("")) {
									String relatedValue = null;
									if (value != null) {
										String[] fixedValues = annotation.fixedValues().split(",");
										if (fixedValues.length > 0) {
											for (String fixedValue : fixedValues) {
												String target = StringUtils.substringBefore(fixedValue, "=");
												if (target != null && target.equals(value.toString())) {
													relatedValue = StringUtils.substringAfter(fixedValue, "=");
													break;
												}
											}
										}													

									} else {
										relatedValue = annotation.whenValueNull();
									}
									cell.setCellValue(relatedValue);
								} else if (annotation.catalog()) {									
									if(value!=null && value instanceof String[] && !catalogs.contains(annotation.columnOrder())){
										String[] bancos = (String[]) value;
										DVConstraint dvConstraintInsBan = DVConstraint.createExplicitListConstraint(bancos);
										HSSFDataValidation data_validation = new HSSFDataValidation(new CellRangeAddressList(1,((String[])value).length+1, 
												annotation.columnOrder(), annotation.columnOrder()), dvConstraintInsBan);
										data_validation.setSuppressDropDownArrow(false);
										sheet.addValidationData(data_validation);
										catalogs.add(annotation.columnOrder());
									}
								} else if (value != null && value instanceof Date) {
									SimpleDateFormat sdf = null;
									if (annotation.format() != null && !annotation.format().equals("")) {
										sdf = new SimpleDateFormat(annotation.format());										
									} else {		
										sdf = this.defaultSdf;
									}

									cell.setCellValue(sdf.format((Date) value));
								} else if (value != null && value instanceof Number ) {

									if (annotation.format() != null && !annotation.format().equals("")) {
										cell.setCellStyle(cellStyleMap.get(annotation.columnOrder()));
									} else {
										cell.setCellStyle(csNumeric);
									}

									cell.setCellValue(new Double(String.valueOf(value)));
								} else {
									cell.setCellType(Cell.CELL_TYPE_STRING);

									if (value == null) {
										if (annotation.whenValueNull() != null && !annotation.whenValueNull().equals("")) {
											cell.setCellValue(annotation.whenValueNull());	
										}

									} else {
										cell.setCellValue(String.valueOf(value));	
									}												
								}				
							}
						}
					}
				}
				
				if (autoSizeColumn) {
					for (int colIndex = 1; colIndex <= columns; colIndex++) {
						sheet.autoSizeColumn(colIndex);
					}
				}

				if (autoFilter) {
					sheet.setAutoFilter(CellRangeAddress.valueOf("A1:" + (Character.toString((char)( 65+columns-1))) + "1"));
				}
				
				if (!summarizeColumns.isEmpty()) {
					Row row = sheet.createRow(dataRows);
					printSummarize(row);
				}
				
			}
			final long endTime = System.currentTimeMillis();
			String totalTime = "Export time: " + (endTime - startTime) / 1000 + " seconds";
			LOG.debug("getWorkbook() " + totalTime);


		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} 

		return this.workbook;
	}
	
	
	private int getMaxSheetRow() {
		if(workbook instanceof XSSFWorkbook || workbook instanceof SXSSFWorkbook) {
			return SpreadsheetVersion.EXCEL2007.getMaxRows();
		} else {
			return SpreadsheetVersion.EXCEL97.getMaxRows();
		}
	}

	/**
	 * Método que genera la exportación de un listado en un formato CSV, el cual
	 * EXCEL reconoce y lo abre automaticamente. Este metodo puede ser utilizado
	 * para procesar gran cantidad de registros con mayor eficiencia.
	 * @param dataSource @Collection de cualquier tipo que contenga anotaciones de tipo @Exportable
	 * @return InputStream del CSV que puede ser leido por EXCEL
	 */
	public InputStream exportCSV(Collection<?> dataSource) {

		List<String> rows = new ArrayListNullAware<String>();
		
		int columns = 0;
		Object[] values = null;
		List<Method> documentMethods = new ArrayListNullAware<Method>();
		Map<String, Exportable> documentAnnotations = new HashMap<String, Exportable>();
		Exportable annotation = null;
		Object value = null;
		String relatedValue = null;
		String[] fixedValues = null;
		String target = null;
		DecimalFormat df = null;
		Object beanInstance = null;
		String[] documentos = null;
		boolean documentFound = false;
				
		if (beanClass != null) {
			
			try {
			
				beanInstance = (Object) beanClass.newInstance();
				
				values = new Object[beanClass.getMethods().length];
							
				// header
				for (Method method : beanInstance.getClass().getMethods()) {
					
					documentFound = false;
					
					for (Exportable exp : getExportableAnnotations(method)) {
						
						if (documentFound) break;
						
						if (exp.document() != null && !exp.document().equals("")) {
							
							documentos = exp.document().split(",");
							
							for (String documentoItem : documentos) {
								
								if (this.documento.equals(documentoItem)) {
									
									documentMethods.add(method);
									documentAnnotations.put(method.getName(), exp);
									values[exp.columnOrder()] = exp.columnName();
									columns++;
									documentFound = true;
									break;
									
								}
								
							}
							
						}
						
					}
					
				}
				
				rows.add(WriterUtil.writeCSVRow(values));
							
				if (dataSource != null ) {
					
					for (Object element : dataSource) {
											
						values = new Object[columns];
						
						for (Method method : documentMethods) {	
							
							annotation = documentAnnotations.get(method.getName());							
							
							value = method.invoke(element);		
							
							if (annotation.fixedValues() != null && !annotation.fixedValues().equals("")) {
								relatedValue = null;
								if (value != null) {
									fixedValues = annotation.fixedValues().split(",");
									if (fixedValues.length > 0) {
										for (String fixedValue : fixedValues) {
											target = StringUtils.substringBefore(fixedValue, "=");
											if (target != null && target.equals(value.toString())) {
												relatedValue = StringUtils.substringAfter(fixedValue, "=");
												break;
											}
										}
									}													
									
								} else {
									relatedValue = annotation.whenValueNull();
								}
															
								values[annotation.columnOrder()] = relatedValue;
							
							} else if (value instanceof Date) {
								
								if (annotation.format() != null && !annotation.format().equals("")) {
									this.defaultSdf = new SimpleDateFormat(annotation.format());										
								}
								
								value = this.defaultSdf.format((Date) value);
								values[annotation.columnOrder()] = String.valueOf(value);
								
							} else if (value instanceof Number ) {
															
								if (annotation.format() != null && !annotation.format().equals("")) {
									
									df = new DecimalFormat(annotation.format());
									value = df.format(value);
									
								}							
								
								values[annotation.columnOrder()] = String.valueOf(value);
								
							} else {
								
								if (value != null) {
									
									values[annotation.columnOrder()] = String.valueOf(value);
									
								} else {
									
									values[annotation.columnOrder()] = annotation.whenValueNull();
									
								}												
							}	
													
							
						}
						
						rows.add(WriterUtil.writeCSVRow(values));
						
					}
				}
	
				return WriterUtil.writeStringRecords(rows);
				
			} catch (Exception ex) {
				
				ex.printStackTrace();
				
			}
		}

		return null;

	}

	
	
	/**
	 * Método que genera la exportación de un listado a Excel utilizando POI y lo entrega en un objeto de Transporte preconfigurado
	 * @param dataSource @Collection de cualquier tipo que contenga anotaciones de tipo @Exportable
	 * @param fileName Nombre del archivo a generar (sin extension, la extension XLS se agregara automaticamente)
	 * @return Un objeto de transporte preconfigurado de tipo TransporteImpresionDTO con el archivo de EXCEL generado
	 */
	public TransporteImpresionDTO exportXLS(Collection<?> dataSource, String fileName) {
		
		return WriterUtil.wrapInXLSFile(export(dataSource), fileName);
		
	}
	
	/**
	 * Método que genera la exportación de un listado a Excel utilizando CSV y lo entrega en un objeto de Transporte preconfigurado.
	 * El archivo generado es un archivo de texto (CSV) que EXCEL reconoce y lo abre automaticamente. Este metodo puede ser utilizado
	 * para procesar gran cantidad de registros con mayor eficiencia.
	 * @param dataSource @Collection de cualquier tipo que contenga anotaciones de tipo @Exportable
	 * @param fileName Nombre del archivo a generar (sin extension, la extension CSV se agregara automaticamente)
	 * @return Un objeto de transporte preconfigurado de tipo TransporteImpresionDTO con el archivo CSV generado
	 */
	public TransporteImpresionDTO exportCSV(Collection<?> dataSource, String fileName) {
		
		return WriterUtil.wrapInCSVFile(exportCSV(dataSource), fileName);
		
	}
	
	private List<Exportable> getExportableAnnotations(Method method) {
		
		MultipleDocumentExportable groupingAnnotation = null;
		List<Exportable> annotations = new ArrayListNullAware<Exportable>();
		
		if (method.isAnnotationPresent(Exportable.class) || method.isAnnotationPresent(MultipleDocumentExportable.class)) {
			
			if (method.isAnnotationPresent(MultipleDocumentExportable.class)) {
				
				groupingAnnotation = method.getAnnotation(MultipleDocumentExportable.class);
				
				for (Exportable annotation : groupingAnnotation.value()) {
					
					annotations.add(annotation);
					
				}
				
			} else {
				
				annotations.add(method.getAnnotation(Exportable.class));
				
			}
			
		}
		
		return annotations;
		
	}
	
	private Exportable getExportableAnnotation(Method method) {
		
		Exportable annotation = null;
		
		if (method.isAnnotationPresent(Exportable.class) || method.isAnnotationPresent(MultipleDocumentExportable.class)) {
			
			if (method.isAnnotationPresent(MultipleDocumentExportable.class)) {
				
				MultipleDocumentExportable groupingAnnotation = method.getAnnotation(MultipleDocumentExportable.class);
				
				for (Exportable innerAnotation : groupingAnnotation.value()) {
					
					annotation = getExportableAnnotation(innerAnotation);
					
					if (annotation != null) {
						break;
					}					
				}
				
			} else {				
				annotation = getExportableAnnotation(method.getAnnotation(Exportable.class));
				
			}			
		}			
		return annotation;
		
	}
	
	private Exportable getExportableAnnotation(Exportable annotation) {
		if (annotation.document() != null && !annotation.document().equals("")) {
			
			String[] documents = annotation.document().split(",");
			
			for (String documentItem : documents) {
				
				if (this.documento.equals(documentItem)) {
					return annotation;
				}
			}
		}
		return null;
		
	}
	
	
	private int printHeader(Object beanInstance, Sheet sheet ) {
		int columns = 0;
		Row row = sheet.createRow(0);
		
		for (Method method : beanInstance.getClass().getMethods()) {									

			Exportable annotation = getExportableAnnotation(method);
			
			if (annotation != null) {
				
				printHeaderCell(row, annotation.columnName(), annotation.columnOrder());
				
				if (annotation.format() != null && !annotation.format().equals("")
						&& (Number.class.isAssignableFrom(method.getReturnType())
							|| double.class.isAssignableFrom(method.getReturnType())
							|| int.class.isAssignableFrom(method.getReturnType())
							|| short.class.isAssignableFrom(method.getReturnType())
							|| float.class.isAssignableFrom(method.getReturnType())
							|| long.class.isAssignableFrom(method.getReturnType())
				)) {
					
					CellStyle cs = this.workbook.createCellStyle();
					DataFormat customDataFormat = this.workbook.createDataFormat();
					cs.setDataFormat(customDataFormat.getFormat(annotation.format()));
					
					cellStyleMap.put(annotation.columnOrder(), cs);
				}
				
				sheet.autoSizeColumn(annotation.columnOrder());
				columns++;
				
				if (annotation.summarize()) {
					summarizeColumns.add(annotation);
				}
							
			}																
			
		}
		
		return columns;
	}

	private void printHeaderCell(Row row, String columnName, int index) {
		Cell cell = row.createCell(index);
		cell.setCellValue(columnName);
		cell.setCellStyle(headerStyle);
	}

	private void createHeaderStyle(Workbook workbook) {
		headerFont =  workbook.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setFontHeightInPoints((short)11);
		headerStyle = workbook.createCellStyle();
		headerStyle.setFont(headerFont);
	}
		
	
	private String getExcelColumnName(int number) {
        final StringBuilder sb = new StringBuilder();

        while (number >=  0) {
            int numChar = (number % 26)  + 65;
            sb.append((char)numChar);
            number = (number  / 26) - 1;
        }
        return sb.reverse().toString();
    }
	
	private void printSummarize(Row row) {
		
		for (Exportable annotation: summarizeColumns) {
			Cell cell = row.createCell(annotation.columnOrder());
			StringBuilder sumFormula = new StringBuilder("SUM(");
			sumFormula.append(getExcelColumnName(annotation.columnOrder())).append("2:")
			.append(getExcelColumnName(annotation.columnOrder())).append(row.getRowNum())
			.append(")");
			cell.setCellType(Cell.CELL_TYPE_FORMULA);
			cell.setCellFormula(sumFormula.toString());
			
			if (annotation.format() != null && !annotation.format().equals("")) {
				cell.setCellStyle(cellStyleMap.get(annotation.columnOrder()));
			} else {
				cell.setCellStyle(csNumeric);
			}
		}
	}

	    public static void copySheets(Sheet newSheet, Sheet sheet){
        copySheets(newSheet, sheet, true);
    }
    public static void copySheets(Sheet newSheet, Sheet sheet, boolean copyStyle){
        int maxColumnNum = 0;
        Map<Integer, CellStyle> styleMap = (copyStyle)
                ? new HashMap<Integer, CellStyle>() : null;
 
        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            Row srcRow = sheet.getRow(i);
            Row destRow = newSheet.createRow(i);
            if (srcRow != null) {
                ExcelExporter.copyRow(sheet, newSheet, srcRow, destRow, styleMap);
                if (srcRow.getLastCellNum() > maxColumnNum) {
                    maxColumnNum = srcRow.getLastCellNum();
                }
            }
        }
        for (int i = 0; i <= maxColumnNum; i++) {
            newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
        }
    }
 
    public static void copyRow(Sheet srcSheet, Sheet destSheet, Row srcRow, Row destRow, Map<Integer, CellStyle> styleMap) {
        destRow.setHeight(srcRow.getHeight());
        for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
            Cell oldCell = srcRow.getCell(j);
            Cell newCell = destRow.getCell(j);
            if (oldCell != null) {
                if (newCell == null) {
                    newCell = destRow.createCell(j);
                }
                copyCell(oldCell, newCell, styleMap);
            }
        }
         
    }
    public static void copyCell(Cell oldCell, Cell newCell, Map<Integer, CellStyle> styleMap) {
        if(styleMap != null) {
            if(oldCell.getSheet().getWorkbook() == newCell.getSheet().getWorkbook()){
                newCell.setCellStyle(oldCell.getCellStyle());
            } else{
                int stHashCode = oldCell.getCellStyle().hashCode();
                CellStyle newCellStyle = styleMap.get(stHashCode);
                if(newCellStyle == null){
                    newCellStyle = newCell.getSheet().getWorkbook().createCellStyle();
                    newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
                    styleMap.put(stHashCode, newCellStyle);
                }
                newCell.setCellStyle(newCellStyle);
            }
        }
        switch(oldCell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_BLANK:
                newCell.setCellType(Cell.CELL_TYPE_BLANK);
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            default:
                break;
        }
         
    }

}

