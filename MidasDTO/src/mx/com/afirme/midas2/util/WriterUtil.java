package mx.com.afirme.midas2.util;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

public class WriterUtil {
	
	private static final Logger LOG = Logger.getLogger(WriterUtil.class);

	protected WriterUtil() {
		
	}
	
	public static InputStream writeWorkBook(Workbook workbook) {
		
		ByteArrayOutputStream bos = null;
		
		try {
			
			bos = new ByteArrayOutputStream();

			workbook.write(bos);
			
		} catch (Exception e) {
			
			LOG.error(e.getMessage());
			
		} finally {
			
			if (bos != null) {
				try {
					
					bos.close();
					
				} catch (IOException e) {
					
					LOG.error(e.getMessage());
					
				}
			}
			
		}
		
		if (bos == null) {
			return null;
		}
		
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	
	public static InputStream writeStringRecords(List<String> records) {
		
		ByteArrayOutputStream bos = null;
		
		BufferedWriter writer = null;
		
		try {
			
			bos = new ByteArrayOutputStream();
			
			writer = new BufferedWriter(new OutputStreamWriter(bos));
			
			for (String record : records) {
								
				writer.write(record);
				
				writer.newLine();
			}
			
			writer.flush();
			
		} catch (Exception e) {
			
			LOG.error(e.getMessage());
			
		} finally {
			
			try {
				
				if (bos != null) {
					
					bos.close();
					
				}

				if (writer != null) {
					
					writer.close();
					
				}
				
			} catch (IOException e) {
				
				LOG.error(e.getMessage());
				
			}
			
		}
		
		if (bos == null) { 
			return null;
		}
		
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	public static InputStream writeZip (List<TransporteImpresionDTO> files) {
		
		ByteArrayOutputStream bos = null;
		ZipEntry ze = null;
		ZipOutputStream zos = null;
		InputStream in;
		int len = 0;
		byte[] buffer = new byte[1024];
		
		try{
			
    		bos = new ByteArrayOutputStream();
    		
    		zos = new ZipOutputStream(bos);
    		
    		for (TransporteImpresionDTO file : files) {
    			
    			ze = new ZipEntry(file.getFileName());
        		zos.putNextEntry(ze);
        		in = file.getGenericInputStream();
     
        		while ((len = in.read(buffer)) > 0) {
        			zos.write(buffer, 0, len);
        		}
     
        		in.close();
        		zos.closeEntry();
        		
    		}
    						
			
		} catch (Exception ex) {
			
			LOG.error(ex.getMessage());
	    	   
	    } finally {
				
			try {
				
				zos.close();
	    		bos.close();
	    		
			} catch (IOException e) {
				
				LOG.error(e.getMessage());
				
			}
				
		}
		
		return new ByteArrayInputStream(bos.toByteArray());
		
	}
	
	
	public static String writeCSVRow(Object[] values) {
		final StringBuilder row = new StringBuilder();
		for (Object value : values) {
			if (value != null) {
				if (value instanceof String) {
					row.append(((String) value).replace("\n", " ").replace("\r"," ")).append(",");
				} else {
					row.append(value).append(",");
				}
			}
		}
		return row.toString();
	}
	
	public static TransporteImpresionDTO wrapInXLSFile (InputStream inputStream, String fileName) {
		
		return wrapInFile(inputStream, fileName, "application/vnd.ms-excel", "XLS");
		
	}
	
	public static TransporteImpresionDTO wrapInCSVFile (InputStream inputStream, String fileName) {
		
		return wrapInFile(inputStream, fileName, "application/octet-stream", "CSV");
		
	}
	
	public static TransporteImpresionDTO wrapInZIPFile (List<TransporteImpresionDTO> files, String fileName) {
		
		return wrapInZIPFile (writeZip(files), fileName);
		
	}
	
	public static TransporteImpresionDTO wrapInZIPFile (InputStream inputStream, String fileName) {
		
		return wrapInFile(inputStream, fileName, "application/zip", "ZIP");
		
	}

	public static TransporteImpresionDTO wrapInXMLFile (InputStream inputStream, String fileName) {
		
		return wrapInFile(inputStream, fileName, "application/xml", "XML");
		
	}
	
	public static TransporteImpresionDTO wrapInPDFFile (InputStream inputStream, String fileName) {
		
		return wrapInFile(inputStream, fileName, "application/pdf", "PDF");
		
	}
	
	private static TransporteImpresionDTO wrapInFile (InputStream inputStream, String fileName, String contentType, String fileExtension) {
		
		String localFileName = null;
		TransporteImpresionDTO file = new TransporteImpresionDTO();
		
		file.setGenericInputStream(inputStream);
		file.setContentType(contentType);
		
		localFileName = (fileName != null?fileName:"DEFAULT");
		
		file.setFileName(StringUtils.substringBefore(localFileName.toUpperCase(), "." + fileExtension) + "." + fileExtension);	
		
		return file;
		
	}

	
	
}
