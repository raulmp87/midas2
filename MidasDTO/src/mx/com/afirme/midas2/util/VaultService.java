package mx.com.afirme.midas2.util;

import javax.ejb.Remote;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;


public interface VaultService {
	public void copyFile(ControlArchivoDTO in, ControlArchivoDTO out);
}
