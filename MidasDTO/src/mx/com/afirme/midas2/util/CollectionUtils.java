package mx.com.afirme.midas2.util;

import java.util.Collection;
import java.util.Collections;

/**
 * Utilerias para clases de tipo Collections
 * @author Lizeth De La Garza
 *
 */
public class CollectionUtils {
	
 /**
  * Retorna una instancia vacia en caso de que sea nula. 
  * @param <T>
  * @param iterable
  * @return
  */
	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
	    return iterable == null ? Collections.<T>emptyList() : iterable;
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmpty(Collection coll) {
		return org.apache.commons.collections.CollectionUtils.isNotEmpty(coll);
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Collection coll) {
		return org.apache.commons.collections.CollectionUtils.isEmpty(coll);
	}
}
