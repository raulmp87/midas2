package mx.com.afirme.midas2.util;

import javax.ejb.Remote;


public interface ValidadorJs {

	/**
	 * Este metodo regresa un string que representa a la funcion javascript
	 * que se manda a invocar cuando un elemento cambia de valor.
	 * @param id el id del elemento HTML que se va a validar. 
	 * @param dependenciaId es el id del elemento de la que se tiene dependencia. Puede ser null dependiendo del caso.
	 * @param tipoValidador el tipo validador. Puede ser null
	 * @param tipoDependencia el tipo de dependencia. Puede ser null
	 * @return
	 */
	public String generarValidacionJs(String id, String dependenciaId, Integer tipoValidador, Integer tipoDependencia);
		

	/**
	 * Este metodo genera el javascript a ejecutar cuando el valor de un elemento cambia y por lo tanto
	 * se debe de ejecutar este script para refrescar al otro elemento (dependencia).
	 * @param id el id del elemento HTML que se va a validar. 
	 * @param dependenciaId es el id del elemento de la que se tiene dependencia.
	 * @param tipoDependencia el tipo de dependencia. Puede ser null
	 * @return la cadena javascript para ser ejecutada. Si no hay ninguna cadena asociada
	 * al validador regresa una cadena vacia.
	 */
	public String generaTipoDependenciaJs(String id, String dependenciaId, Integer tipoDependencia, boolean prepareForEval);
}
