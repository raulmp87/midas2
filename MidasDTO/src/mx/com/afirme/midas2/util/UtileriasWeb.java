package mx.com.afirme.midas2.util;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.js.util.StringUtil;


public class UtileriasWeb {

	public static final String SEPARADOR = "_";
	public static final String SEPARADOR_MEDIO = "-";
	public static final String SEPARADOR_VERTICAL = "|";
	public static final String SEPARADOR_COMA = ",";
	public static final String STRING_EMPTY= "";
	public static final String STRING_CERO= "0";

	//Catalogos	
	public static final String CIUDADES = "CIUDADES";
	public static final String ESTADOS = "ESTADOS";
	public static final String COLONIAS = "COLONIAS";
	public static final String BANCOS = "BANCOS";

	public static final String KEY_ESTATUS = "estatus";
	public static final String KEY_CATEGORIA = "categoria";
	public static final String KEY_PAIS_MEXICO = "PAMEXI";
	
	/**
	 * Este metodo sirve para que genere una expresion OGNL para las colleciones
	 * de tipo Map las cuales se manejan con indices. Por ejemplo podemos mandar a
	 * llamar el metodo getOGNLCollectionExpression("empresa.empleado", "Alfredo")
	 * el resultado seria empresa.empleado['Alfredo'] ya convertido a OGNL.
	 * @param expression
	 * @param key
	 * @return
	 */
	public static String getOGNLCollectionExpression(String expression, String key) {
		return expression + "['" + key + "']";
	}
	
	public static String getIndexFromCollectionExpression(String collectionExpresion) {
		int endIndex = collectionExpresion.lastIndexOf("]");
		int startIndex = collectionExpresion.lastIndexOf("[");
		String tmpStr = collectionExpresion.substring(startIndex + 1, endIndex);
		tmpStr = tmpStr.replaceAll(Pattern.quote("'"), "");
		return tmpStr;
	}
	
	public static boolean isDecimalValido(String numero, int maxNumEnteros, int maxNumDecimales) {
		if (numero == null) {
			return false;
		}
		
		try {
			Double.parseDouble(numero);
		} catch(NumberFormatException e) {
			return false;
		}
		
		int indicePunto = numero.indexOf(".");
				
		if (indicePunto != -1) {
			String entero = StringUtils.stripStart(numero.substring(0, indicePunto), STRING_CERO);
			String decimal = StringUtils.stripEnd(numero.substring(indicePunto + 1), STRING_CERO);
			
			if (entero.length() <= maxNumEnteros && decimal.length() <= maxNumDecimales) {
				return true;
			}
		} else {
			String entero = StringUtils.stripStart(numero, STRING_CERO);
			if (entero.length() <= maxNumEnteros) {
				return true;
			}
		}
		
		return false;
	}
	
	public static Double eliminaFormatoMoneda(String cadenaMoneda) {
		String pattern = "[^\\s0-9]";
		Double cantidad = null;
		if(cadenaMoneda != null && !cadenaMoneda.equals("") && !cadenaMoneda.contains("$")){
			cantidad = Double.parseDouble(cadenaMoneda);
		}else if (!cadenaMoneda.equals("")) {
			cantidad = new Double(Double.parseDouble(cadenaMoneda.replaceAll(
					pattern, "")) / 100);
		}
		return cantidad;
	}
	
	 /*
	  * Metodo que codifica acentos-ñ's y los caracteres de latin basico de unicode para IE y lo ignora en Firefox
	  * @param descripcion cadena a codificar
	  * @return cadena codificada
	  */
	 public static String parseEncodingISO(String descripcion){
		 if(descripcion != null){
			//String patron2 = "^[\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00D1\u00F1\u00BF\u00A1\u0020-\u007D]+$";
			//boolean valido2 = descripcion.matches(patron2);
			
			String pattern1 = "^[\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00D1\u00F1\u00BF\u00A1]+$";
			Pattern p = Pattern.compile(pattern1, Pattern.MULTILINE);
			boolean valido2 = p.matcher(descripcion).find();
			
			if(!valido2){
				try {
					descripcion = new String(descripcion.getBytes("ISO-8859-1"), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		 }
		return descripcion;
	 }	
	 
	 public static String formatoMoneda(Double cantidad) {
			NumberFormat formateador = new DecimalFormat("$#,##0.00");
			return cantidad != null ? formateador.format(cantidad) : null;
		}
		
	public static String formatoMoneda(BigDecimal cantidad) {
		if (cantidad != null) {
			return formatoMoneda(cantidad.doubleValue());
		} else {
			return null;
		}
	}
	
	public static Double parseaformatoMoneda(String cadenaMoneda) {
		NumberFormat formateador = new DecimalFormat("$#,##0.00");
		Double cantidad = null;
		try{
			cantidad = new Double(formateador.parse(cadenaMoneda).doubleValue());
		}catch(Exception e){
			System.out.println("Error en UtileriasWeb.parseaformatoMoneda para la cadena ->" + cadenaMoneda + "<- ==> " + e.getMessage());
		}
		return cantidad;
	}
	
	public static boolean isFechaValida(String fecha) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
	
	/**
	 * Valida si una cadena de caracteres se encuentra vacia
	 * 
	 * @param cadena
	 * @return
	 */
	public static boolean esCadenaVacia(String cadena) {
		boolean esVacia = true;
		if (cadena != null) {
			esVacia = (cadena.trim().length() == 0);
		}
		return esVacia;
	}
	
	/**
	 * Retorna el key de un Map 
	 * @param map
	 * @return key
	 **/
	public static String getValueMap(Map<String, String> map){
		Iterator<Entry<String, String>> it = map.entrySet().iterator();
		String key = "";
		while(it.hasNext()){
			Map.Entry<String, String> pairs = it.next();
			key = pairs.getKey();
		  break;
		}
		return key;
	}
		
	public static String getString(String texto){
		if(texto==null){
			return STRING_EMPTY;
		}
		return texto;
	}
	
	public static boolean validaDatosEmpty(List<String> datos){
		for(String dato : datos){
			if(StringUtil.isEmpty(dato)){
				return true;
			}
		}
		return false;		
	}
	
	public static String toUpperCase(String valor){
		if(valor!=null && valor!=""){
			return valor.toUpperCase();
		}
		return null;
	}
	
	public static Double getDoubleValue(Double valor){
		if(valor!=null){
			return valor;
		}
		return 0.00;
	}
	
	public static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

	public static boolean validarRfcPersonaFisica(String rfc){
		    rfc=rfc.toUpperCase().trim();
		    return rfc.toUpperCase().matches("[A-Z&]{4}[0-9]{6}[A-Z0-9]{3}");
	}
		 
	public static boolean validarRfcPersonaMoral(String rfc){
			    rfc=rfc.toUpperCase().trim();
			    return rfc.toUpperCase().matches("[A-Z&]{3}[0-9]{6}[A-Z0-9]{3}");
	}
}

