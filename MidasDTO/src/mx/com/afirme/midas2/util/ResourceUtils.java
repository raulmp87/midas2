package mx.com.afirme.midas2.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceUtils {
	public static synchronized String getMessage(String saResourceBundle,
			String saMessageKey) {
		ResourceBundle objlResourceBundle = getResource(saResourceBundle);
		Enumeration<String> objlEnum = null;
		boolean blExist = false;
		String slMessage = null;
		for (objlEnum = objlResourceBundle.getKeys(); !isNull(objlEnum)
				&& objlEnum.hasMoreElements() && !blExist;) {
			if (saMessageKey.equalsIgnoreCase((String) objlEnum.nextElement())) {
				blExist = true;
			}
		}

		if (blExist) {
			slMessage = objlResourceBundle.getString(saMessageKey);
		}
		return slMessage;
	}

	public static synchronized ResourceBundle getResource(
			String saResourceBundle) {
		ResourceBundle objlResource = null;
		objlResource = ResourceBundle.getBundle(saResourceBundle);
		return objlResource;
	}

	public static boolean isNull(String saValue) {
		boolean blNullEmpty;
		if (saValue != null && !saValue.trim().equals("")) {
			blNullEmpty = false;
		} else {
			blNullEmpty = true;
		}
		return blNullEmpty;
	}

	public static boolean isNull(Object objaValue) {
		boolean blIsNull = false;
		if (objaValue == null) {
			blIsNull = true;
		} else if (isNull(objaValue.toString())) {
			blIsNull = true;
		}
		return blIsNull;
	}

	public static String getDate(Date objaDate, String saFormat) {
		SimpleDateFormat objlFormat = new SimpleDateFormat(saFormat,
				new Locale("es", "MX"));
		String slDate = objlFormat.format(objaDate);
		return slDate;
	}

}