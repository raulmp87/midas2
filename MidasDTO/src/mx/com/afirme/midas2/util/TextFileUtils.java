package mx.com.afirme.midas2.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.sql.CLOB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <p>Clase que contiene la utileria para realizar distintos procesamientos
 * de files de texto como lo que son: 
 * 	<ul>
 * 		<li><b>Lectura</b></li>
 * 		<li><b>Escritura</b></li>
 * 	</ul></p>
 * <p>Asi como la administracion de estos files alojados en servidores
 * remotos, el protocolo de conexion es atravez de <b>SFTP/SSH</b></p>
 * 
 * @since 06032017
 * 
 * @version 1.0
 * 
 * @category UTIL
 * 
 * @author SEGUROS AFIRME
 *
 */
public class TextFileUtils implements Serializable {
	
	private static String currDate = new SimpleDateFormat("ddmmyyyy").format(new Date());
	private static String RUTA_GENERAL_ALMACENAMIENTO_ARCHIVOS = "apps/pagos_programados/";
	
	/**
	 * <p>Constante de encoding con el cual se estaran trabajando
	 * los files.</p>
	 */
	public static final String ENCODING_UTF_8 = "UTF-8";
	public static final String PATH_FILE_VENTACRUZADA = RUTA_GENERAL_ALMACENAMIENTO_ARCHIVOS + "VENTACRUZADA";
	public static final String PATH_FILE_IBS =  RUTA_GENERAL_ALMACENAMIENTO_ARCHIVOS;
	public static final String PATH_WINSTON_DATA = RUTA_GENERAL_ALMACENAMIENTO_ARCHIVOS + "ws";
	public static final String PATH_CAJERO = RUTA_GENERAL_ALMACENAMIENTO_ARCHIVOS + "PROVMENSAJERIA";
	public static final String FILE_NAME_CYBER = "PagosG4.txt";
	public static final String FILE_NAME_SELCOR = "BlancosG4.txt";
	public static final String FILE_NAME_IBS = "PAGOPROG.txt";
	public static final String FILE_NAME_ESTRA = "lay_cob_estra.txt";
	public static final String FILE_NAME_WINSTON = "WINSTONDATA" + currDate  + ".txt";
	public static final String FILE_NAME_PRIMAS_DEP = "PRIMASDEP" + currDate + ".txt";
	public static final String FILE_NAME_CAJERO = "cajero"+ currDate + ".txt";
	public static final String FILE_NAME_WINSTON_LOCAL = "archivo_WD.txt";
	public static final String FILE_NAME_WINSTON_FORANEO = "archivo_WD_For.txt";
	public static final String FILE_NAME_WINSTON_LOCAL_DIR = "archivo_WD_DIR.txt";
	public static final String FILE_NAME_WINSTON_FORANEO_DIR = "archivo_WD_For_DIR.txt";
	public static final String FILE_NAME_WINSTON_LOCAL_VID = "archivo_WD_VID.txt";
	public static final String FILE_NAME_RESULTADO_CANC = "CAN" + currDate + ".txt";
	public static final String FILE_NAME_ID_CLIENTE = "IdCliente.txt";
	public static final String FILE_NAME_RESPCTE = "RESPCTE.txt";
	
	private static final long serialVersionUID = 2617415820886889659L;
	private static final Logger LOG = LoggerFactory.getLogger(TextFileUtils.class);
	
	private String fileName;
	private String pathFile;
	private File file;
	private StringBuffer fileContent;
	
	private CLOB sqlContent;
	private String sqlContentS;
	
	public TextFileUtils(){
		this(null);
	}
	
	public TextFileUtils(String fileName){
		this(fileName, null);
	}
	
	public TextFileUtils(String fileName, String pathFile){
		this.fileName = fileName;
		this.pathFile = pathFile;
	}
	
	/**
	 * <p>M&eacute;todo que se encarga de procesar el file para generar
	 * un elemento de la lista de String, por cada rengl&oacute;n contenido en el file.</p>
	 * 
	 * @param file Objeto que se procesara para obterner su contenido.
	 * 
	 * @return Lista de cadenas que representan cada uno de los
	 * renglones que componen el file.
	 */
	public List<String> getDataFile(File file){
		return getDataFile(getInputStreamFromFile(file));
	}
	
	/**
	 * <p>M&eacute;todo que se encarga de procesar el file para generar
	 * un elemento de la lista de String, por cada rengl&oacute;n contenido en el file.</p>
	 * 
	 * @param file Objeto que se procesara para obterner su contenido.
	 * 
	 * @return Lista de cadenas que representan cada uno de los
	 * renglones que componen el file.
	 */
	public List<String> getDataFile(InputStream file){
		List<String> informacionfile = new ArrayList<String>();
		String linea = null;
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(file, TextFileUtils.ENCODING_UTF_8));
			while ((linea = br.readLine()) != null) {
				informacionfile.add(linea);
			}
			br.close();
		}catch (Exception err){
			LOG.info("::::::::::: [ERR] ::::::::::: Hubo un problema al leer el file." , err);
			throw new RuntimeException(err);
		}
		
		return informacionfile;
	}
	
	/**
	 * M&eacute;todo que genera un InputStream a partir de un file.
	 * 
	 * @param file Objeto que representa al file que se combertir&aacute;
	 * en un InputStream.
	 * 
	 * @return InputStream.
	 */
	private InputStream getInputStreamFromFile(File file){
		InputStream input = null;
		try{
			input = new FileInputStream(file);
		} catch(Exception err){
			throw new RuntimeException(err);
		}
		return input;
	}
	
	/**
	 * M&eacute;todo que recibe como argumento un objeto de tipo CLOB para
	 * realizar la combercio&oacute;n.
	 * 
	 * @param sqlContent Informaci&oacute;n que contiene el elemento.
	 * 
	 * @return String que es mucho m&aacute;s sencillo de manejar.
	 */
	public static StringBuffer clobToStringBuffer(CLOB sqlContent){
		StringBuffer fileContent = new StringBuffer();
		Reader reader;
		BufferedReader br;
		int chr = 0;
		try{
			reader = sqlContent.characterStreamValue();
			br = new BufferedReader(reader);
			
			while(-1 != (chr = br.read())){
				fileContent.append((char)chr);
			}
			
			br.close();
			
		}catch(Exception err){
			throw new RuntimeException(err);
		}
		return fileContent;
	}
	
	/**
	 * <p>M&eacute;todo que realiza la comberci&oacute;n del
	 * objeto de resultado de tipo <code>oracle.sql.CLOB</code> cuando se llena el 
	 * objeto a travez del mapeo de resultado del m&eacute;todo de
	 * <code>estableceMapeoResultados</code> de la clase
	 * <code>mx.com.afirme.midas.interfaz.StoredProcedureHelper</code></p>
	 * 
	 * @return Informaci&oacute;n contenida dentro del objeto CLOB
	 */
	public StringBuffer clobToStringBuffer(){
		fileContent = clobToStringBuffer(sqlContent);
		return fileContent;
	}
	
	/**
	 * <p>M&eacute;todo que realiza la comberci&oacute;n de una lista de
	 * objetos tipo CLOB a una lista de objetos tipo StringBuffer.</p>
	 * 
	 * @param sqlContent Lista de elementos a combertir.
	 * 
	 * @return Lista de elementos combertidos.
	 */
	public static List<StringBuffer> listClobToListStringBuffer(List<CLOB> sqlContent){
		List<StringBuffer> informacion = new ArrayList<StringBuffer>();
		if(sqlContent != null && !sqlContent.isEmpty()){
			for(CLOB content : sqlContent){
				informacion.add(clobToStringBuffer(content));
			}
		}
		return informacion;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public StringBuffer getFileContent() {
		return fileContent;
	}

	public void setFileContent(StringBuffer fileContent) {
		this.fileContent = fileContent;
	}

	public CLOB getSqlContent() {
		return sqlContent;
	}

	public void setSqlContent(CLOB sqlContent) {
		this.sqlContent = sqlContent;
	}

	public String getSqlContentS() {
		return sqlContentS;
	}

	public void setSqlContentS(String sqlContentS) {
		this.sqlContentS = sqlContentS;
	}
	
}
