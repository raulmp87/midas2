package mx.com.afirme.midas2.util;

public enum Currency {
	MXN(484),
	USD(840),
	UNKNOWN(0);
	
	private final int currency;
	
	Currency(int currency){
		this.currency = currency;
	}
	
	public int code(){
		return currency;
	}
	
	public static Currency fromInteger(int currency){
		switch (currency) {
		case 484:
			return Currency.MXN;
		case 840:
			return Currency.USD;
		default:
			return Currency.UNKNOWN;
		}
	}
}
