package mx.com.afirme.midas2.util;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import mx.com.afirme.midas.sistema.SystemException;
/**
 * 
 * @author Angel Verdugo
 * 
 */
public class DateUtils {
	
	public static final String FORMATO_FECHA= "dd/MM/yyyy";
	 
	public static final String FORMATO_FECHA_ORACLE= "yyyy-MM-dd HH:mm:ss.SSS";
	
	public enum Indicador{Days,Hours,Minutes,Seconds,Millis;
     
    public int getValue(){
         int val = 1;
         if (this ==  Days) {
             val = 24 * 60 * 60 * 1000;
         }else if (this ==  Hours) {
             val =  60 * 60 * 1000;
         }else if (this ==  Minutes) {
             val = 60 * 1000;
         }else if (this ==  Seconds) {
             val = 1000;
         }
         //defaul es milisegundos = 1;
         return val;
     }
     /**
      * Obtiene el valor dado en la metríca seleccionada por la enumeración.
      * @param millis
      * @return 
      */
     public double getValue(Long millis){
         double res = 1;
         int val = getValue();
         res  = millis / val ;
         return res;
     }
 };
 
 public static double dateDiff(Date dateOne, Date dateTwo,Indicador indicador){
     double d = 0;
     
     Calendar calDateOne =initCalendar(dateOne, indicador);
     Calendar calDateTwo = initCalendar(dateTwo, indicador);
      // serealiza la diferencia      
     long res = calDateOne.getTimeInMillis() - calDateTwo.getTimeInMillis();
     d = indicador.getValue(res);
     
     return d;
 }
 private static Calendar initCalendar(Date date,Indicador indicator){
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(date);
     if (indicator == Indicador.Days) {
         calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH ) , 
             calendar.get(Calendar.DATE), 0, 0, 0);
     }else if (indicator == Indicador.Hours) {
         calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH ) , 
             calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY), 0, 0);
     }else if (indicator == Indicador.Minutes) {
         calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH ) , 
             calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
     }else if (indicator == Indicador.Seconds) {
         calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH ) , 
             calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),calendar.get(Calendar.SECOND));
     }else {//if (indicator == Indicador.Seconds) {
         //ya esta por default toda la fecha
     }       
     return calendar;
 }
 
 public static Date add(Date date, Indicador indicador, int value){
	 Calendar cal = Calendar.getInstance();
	 cal.setTime(date);
	 if (indicador == Indicador.Days) {
		cal.add(Calendar.DATE, value);
	}else if (indicador == Indicador.Hours) {
		cal.add(Calendar.HOUR_OF_DAY, value);
	}else if (indicador == Indicador.Minutes) {
		cal.add(Calendar.MINUTE, value);
	}else if (indicador == Indicador.Seconds) {
		cal.add(Calendar.SECOND, value);
	}else if (indicador == Indicador.Millis) {
		cal.add(Calendar.MILLISECOND, value);
	}else{
		throw new RuntimeException("[DateUtils][add]No esta implementado esta opción aun."+indicador);
	}
	 return cal.getTime();
 }
 
 public  static int diferenciaDiasFechas(String fechaInicial, String fechaFinal) {
	 Date fechaIngreso=null;
        Date fechaFin=null;

        try {
               fechaIngreso = new SimpleDateFormat("dd/MM/yyyy").parse(fechaInicial);
               fechaFin = new SimpleDateFormat("dd/MM/yyyy").parse(fechaFinal);
        
        SimpleDateFormat y = new SimpleDateFormat("yyyy");
        SimpleDateFormat m = new SimpleDateFormat("MM");
        SimpleDateFormat d = new SimpleDateFormat("dd");
        DateTime start = new DateTime(Integer.valueOf(y.format(fechaIngreso)), Integer.valueOf(m.format(fechaIngreso)), Integer.valueOf(d.format(fechaIngreso)), 0, 0, 0, 0);
        DateTime end = new DateTime(Integer.valueOf(y.format(fechaFin)), Integer.valueOf(m.format(fechaFin)), Integer.valueOf(d.format(fechaFin)), 0, 0, 0, 0);
        Period period = new Period(start, end ,PeriodType.days());
        return period.getDays();
        } catch (ParseException e) {
               return 0;
        }
}
 public static String[] diasSem() {
		String[] diasSemMalIndices = DateFormatSymbols.getInstance(new Locale("es")).getWeekdays();
		String[] diaSem = Arrays.copyOfRange(diasSemMalIndices , 2, diasSemMalIndices.length + 1);
		diaSem[6] = diasSemMalIndices[1];
	
		return diaSem;
	}

 
	public static String toString(Date fecha, String formato) {
		String fechaStr = null;
		SimpleDateFormat sdf = null;
		if (formato != null) {
			sdf = new SimpleDateFormat(formato);
		}else{
			sdf = new SimpleDateFormat(FORMATO_FECHA);			
		}
		fechaStr = sdf.format(fecha);
		sdf = null;
		return fechaStr;
	}
	
	public static java.util.Date convertirStringToUtilDate(String fecha, String formato) throws SystemException{
		SimpleDateFormat format = new SimpleDateFormat(formato);
        Date parsed = null;
        
		try {
			parsed = format.parse(fecha);
		} catch (ParseException e) {
			throw new SystemException("Error al parsear la fecha "+ fecha);
		}
        return parsed;
	}
}
