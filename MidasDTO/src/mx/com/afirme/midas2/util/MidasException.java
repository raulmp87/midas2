package mx.com.afirme.midas2.util;
/**
 * Excepcion del sistema de Midas
 * @author vmhersil
 *
 */
public class MidasException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4106173114539598945L;

	public MidasException(String message){
		super(message);
	}
	
	public MidasException(Throwable e){
		super(e);
	}
}
