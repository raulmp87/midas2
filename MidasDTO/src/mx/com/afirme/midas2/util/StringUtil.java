package mx.com.afirme.midas2.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class StringUtil {

	public static final String EMPTY_STRING = "";

	//joksrc This method decodes any text with special characters such as the letter Ñ.
	 public static String decodificadorUri(String texto){
		  class UriChar{
			  String uri;
			  String ch;
			  public UriChar(String uri, String ch){
				  this.uri = uri;
				  this.ch = ch;
			  }
			  
		  }
		  //Diccionario de caracteres para cuando el request es por JSON
		  ArrayList<UriChar> chars = new ArrayList<UriChar>();
		  chars.add(new UriChar("%C3%81", "Á")); chars.add(new UriChar("%C3%89", "É")); chars.add(new UriChar("%C3%8D", "Í")); chars.add(new UriChar("%C3%93", "Ó")); chars.add(new UriChar("%C3%9A", "Ú"));
		  chars.add(new UriChar("%C3%A1", "á")); chars.add(new UriChar("%C3%A9", "é")); chars.add(new UriChar("%C3%AD", "í")); chars.add(new UriChar("%C3%B3", "ó")); chars.add(new UriChar("%C3%BA", "ú"));
		  chars.add(new UriChar("%C3%91", "Ñ")); chars.add(new UriChar("%C3%B1", "ñ")); chars.add(new UriChar("%2C", ","));
		  chars.add(new UriChar("%C3%9C", "Ü")); chars.add(new UriChar("%C3%BC", "ü")); chars.add(new UriChar("%26", "&"));		
		  if (texto != null){
			  for(UriChar element : chars){
				  texto = texto.replaceAll(element.uri, element.ch);
			  }
		  }
		  return texto;
	  }
	
	public static String capitalize(String string) {
		char chars[] = string.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		return new String(chars);
	}

	public static boolean isEmpty(String string) {
		boolean isEmpty = false;
		if (string == null)
			isEmpty = true;
		else if (string.trim().equals(""))
			isEmpty = true;
		return isEmpty;
	}

	public static String trim(String string) {
		return StringUtil.trim(string, null);
	}

	public static String trim(String string, String defaultValue) {
		if (string != null)
			return string.trim();
		else
			return defaultValue;
	}

	public static String trunc(String string, int size, String defaultValue) {
		String substring = defaultValue;
		if (string != null) {
			try {
				substring = string.substring(0, size);
			} catch (IndexOutOfBoundsException ioobException) {
				substring = string;
			} // End of try/catch
		} // End of if/else
		return substring;
	}

	public static String trunc(String string, int size) {
		return StringUtil.trunc(string, size, StringUtil.EMPTY_STRING);
	}

	/**
	 * Capitalizes the first word in the given <code>string</code>.
	 * 
	 * @param string
	 *            The <code>String</code> to be capitalized.
	 * @return The capitalized <code>string</code>.
	 */
	public static String capitalizeFirstWord(String string) {
		if (StringUtil.isEmpty(string)) {
			return string;
		} else {
			return Character.toUpperCase(string.charAt(0))
					+ ((string.length() > 1) ? string.substring(1)
							: StringUtil.EMPTY_STRING);
		} // End of if/else
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Long.parseLong(str);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Obtiene una cadena encriptada en SHA1.
	 * 
	 * @param string
	 *            Cadena a encriptar
	 * @return Cadena encriptada
	 */

	public static String sha1(String string) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(string.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16)
					.substring(1));
		}
		return sb.toString();
	}

}