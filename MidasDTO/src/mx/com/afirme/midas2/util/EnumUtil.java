package mx.com.afirme.midas2.util;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;

/**
 * 
 * @author Lizeth De La Garza
 *
 */
public class EnumUtil {

	/**
	 * Retorna la descripcion asociada a un Valor de un ENUM
	 * @param <T> Enum que implementa interfaz EnumBase
	 * @param enumClass 
	 * @param val El valor que esta asociado a la descripcion que se buscara
	 * @return
	 */

	public static <T extends EnumBase<K>, K extends Object> String getLabel(Class<T> enumClass, K val) {

		if (val != null) {
			for (T obj :  enumClass.getEnumConstants()) {	    	
				if (obj.getValue().equals(val)) {	        	
					return obj.getLabel();       	            
				}
			}
		}	    
		return null;
	}


	/**
	 * Retorna si un valor es equivalente a un enum
	 * @param <T> Enum que implementa interfaz EnumBase
	 * @param val El valor a comparar
	 * @return
	 */
	public static <T extends Enum<T> & EnumBase<K>, K extends Object> boolean equalsValue(	T enumObj, K value) {
		return (value == null)? false : enumObj.getValue().equals(value);
	}
	
	/**
	 * Retorna si un valor es equivalente a alguno de los enum a comparar
	 * @param <T> Enums que implementan interfaz EnumBase
	 * @param val El valor a comparar
	 * @return
	 */
	public static <T extends Enum<T> & EnumBase<K>, K extends Object> boolean equalsValue( K value,	T... enumObjs) {
		for (T enumObj : enumObjs) {
			if (enumObj.getValue().equals(value)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}


	/**
	 * Retorna el Enum equivalente al valor proporcionado
	 * @param Class<T> enumClass Clase del Enum que implementa interfaz EnumBase
	 * @param val El valor correspondiente
	 * @return
	 */
	public static <T extends Enum<T> & EnumBase<K>, K extends Object>  T fromValue(Class<T> enumClass, K value) throws IllegalArgumentException {
		try{
			System.out.println("*****************************" + enumClass.getEnumConstants().toString());
			for (T enumObj :  enumClass.getEnumConstants()) {
				if (equalsValue(enumObj, value)) {

					return enumObj;
				}
			}
			return null;
		}catch( ArrayIndexOutOfBoundsException e ) {
			throw new IllegalArgumentException("Unknown enum value :"+ value);
		}
	}
	
	/**
	 * Retorna un mapa con los valores de un enum. Sirve para la generacion de combos de valores de un enum
	 * @param <T> Enums que implementan interfaz EnumBase
	 * @param <K> Value del enum
	 * @param enumClass Clase del Enum que implementa interfaz EnumBase
	 * @return
	 */
	public static <T extends Enum<T> & EnumBase<K>, K extends Object> Map<K,String> getMap(Class<T> enumClass){
		Map<K, String>  map = new LinkedHashMap<K, String>();
		for(T enumObj : enumClass.getEnumConstants()){
			map.put((K)enumObj.getValue(), enumObj.getLabel());
		}		
		return map;		
	}

}
