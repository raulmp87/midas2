package mx.com.afirme.midas2.util;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8639832679537456893L;
	private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);
	
	/**
	 * <p>M&eacute;todo para concatenar varios archivos PDF's en uno solo.</p>
	 * 
	 * @param List, Lista de ByteArray con cada uno de los archivos.
	 * @param OutputStream, Objeto con los archivos concatenados.
	 * 
	 */
	public static void pdfConcantenate(List inputByteArray, OutputStream outputStream){  
	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;
	          Iterator iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){   
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              document = new Document(reader.getPageSizeWithRotation(1));
	                                  
	              writer = new PdfCopy(document, outputStream);
	             
	              document.open();           
	            }
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	  
	    	  LOG.error("Error al concatenar los PDFs : " + e.getMessage(), e);
	      }	      
	  }	
	
}
