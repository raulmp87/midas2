package mx.com.afirme.midas2.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Query;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.apache.commons.beanutils.MethodUtils;

public class JpaUtil {
	
	public static final String EQUAL = "=";
	public static final String GREATER= ">";
	public static final String GREATEROREQUAL= ">=";
	public static final String LESS= "<";
	public static final String LESSOREQUAL= "<=";
	public static final String LIKE = "LIKE";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	public static final String IN = "DESC";

	public static void addParameter(StringBuilder query, String modelName,
			Object object, String name, Map<String, Object> parameters) {
		addParameter(query, modelName, object, name, parameters, "=");
	}

	public static void addParameter(StringBuilder query, String modelName,
			Object object, String name, Map<String, Object> parameters,
			String condition) {
		if (object != null && object.toString().length() > 0) {	
			normalizeQuery(query);			
			query.append(" ");
			query.append(modelName);
			query.append(" ");
			query.append(condition);
			query.append(" :");
			query.append(normalizeParameterName(name));
			parameters.put(normalizeParameterName(name), object);
		}
	}
	
	
	public static void addParameterIn(StringBuilder query, String columna, String valores) {
		valores = valores.trim();
		boolean esPrimero = true;
		String valoresArray[] = valores.split("_");
		if(valoresArray != null){
			
			if(!query.toString().toUpperCase().contains("WHERE")){
				query.append(" WHERE ");
			}
			query.append(" ");
			query.append(columna);
			query.append(" IN ");
			query.append(" ( ");
			for(String valor: valoresArray){
				if(!esPrimero){
					query.append(",");
				}
				query.append(" '");
				query.append(valor);
				query.append("' ");
				esPrimero = false;
			}
			query.append(" ) ");
		}
	}
	
	public static void addParameterLike(StringBuilder query, String modelName,
			Object object, String name, Map<String, Object> parameters) {
		if (object != null && object.toString().length() > 0) {
			normalizeQuery(query);
			query.append(" UPPER(");
			query.append(modelName);
			query.append(" ) LIKE UPPER(");
			query.append(" :");
			query.append(normalizeParameterName(name));
			query.append(")");
			parameters.put(normalizeParameterName(name), "%".concat(object.toString()).concat("%"));
		}
	}

	public static void addDateParameter(StringBuilder query, String modelName,
			Object object, String name, Map<String, Object> parameters, String condition) {
		if (object != null && object.toString().length() > 0) {					
			normalizeQuery(query);
			query.append(getDateSentence(modelName, name, condition));			
			parameters.put(normalizeParameterName(name), object);
		}
	}
	
	public static void addTruncParameter(StringBuilder query, String modelName,
			Object object, String name, Map<String, Object> parameters, String condition) {
		if (object != null && object.toString().length() > 0) {		
			normalizeQuery(query);
			query.append("  FUNC('trunc'," + modelName + ')' + condition +  ':' + normalizeParameterName(name));			
			parameters.put(normalizeParameterName(name), object);
		}
	}


	public static String getDateSentence(String property, String propertyMap, String condition) {
		return new StringBuilder(property).append(" ").append(condition).append(" :").append(normalizeParameterName(propertyMap)).toString();		
	}


	public static String getStrValue(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}
	
	public static String getTrimStrValue(Object obj) {
		String rtn = getStrValue(obj);
		if(rtn != null){
			rtn = rtn.trim();
		}
		return rtn;
	}
	
	private static void normalizeQuery(StringBuilder query){
		if(!query.toString().contains("WHERE")){
			query.append(" WHERE");
		}			
		if(!query.toString().trim().endsWith("WHERE")){
			query.append(" AND ");				
		}
	} 
	
	/**
	 * Genera la sentencia JQPL para un objeto simple de busqueda basada en los objetos de tipo filtro
	 * @param query
	 * @param filter
	 * @param parameters
	 */
	public static <E extends Entidad> void createSimpleFilterQuery(Class<E> entidad, StringBuilder query, 
			Object filter, Map<String, Object> parameters){
		createSimpleFilterQuery(entidad, query, filter, parameters, null);
	}
	
	/**
	 * Genera la sentencia JQPL para un objeto simple de busqueda basada en los objetos de tipo filtro con orden simple
	 * @param query
	 * @param filter
	 * @param parameters
	 * @param orderBy nombre de la propiedad por la cual se desea ordenar la consulta, se puede especificar la direccion del ordenamiento
	 * 			concatenando al final uno de los valores ASC o DESC separado por "-". Ejemplo:  nombreOficina-DESC
	 */
	public static <E extends Entidad> void createSimpleFilterQuery(Class<E> entidad, StringBuilder query, 
			Object filter, Map<String, Object> parameters, String orderBy){
		String modelName = "e";
		query.append("SELECT ").append(modelName).append(" FROM ").append(entidad.getSimpleName()).append(" ").append(modelName);		
		addFilterParameter(modelName, query, filter, parameters);
		if(!StringUtil.isEmpty(orderBy)){
			
			//Se extiende la funcionalidad para soportar la direccion del order by		
			String orderByColumn;
			String orderByDirection = ASC;
			
			if(orderBy.contains("-"))
			{
				orderByColumn = orderBy.split("-")[0];
				orderByDirection = orderBy.split("-")[1];				
			}else
			{
				orderByColumn = orderBy;				
			}
			
			query.append(" ORDER BY ").append(modelName).append(".").append(orderByColumn).append(" ").append(orderByDirection);
		}
	}
	
	/**
	 * Utileria que lee el contenido de un objeto que sirve de filtro. 
	 * Si el atributo es diferente de nulo lo concatena como clausula de tipo AND al query principal. 
	 * Para la generacion del query se utiliza el nombre del atributo en el objeto filtro, pero puede 
	 * cambiarse si se usa la anotacion FilterPersistenceAnnotation en la cual se puede definir
	 * el nombre con el que se debe mapear en el query (que debe se igual al de la entidad)
	 * @param query queryBuilder hasta el momento
	 * @param filter objeto filtro
	 * @param parameters listado de parametros
	 * @return
	 */
	public static void addFilterParameter(String modelName, StringBuilder query, Object filter, Map<String, Object> parameters){
		String key = null;
		List<Method> getters = new ArrayList<Method>(); 		
		if(filter == null){
			return;
		}
		for(Method method : filter.getClass().getDeclaredMethods()){
			if(method.getName().startsWith("get") && !method.getName().equals("getClass")){
				getters.add(method);
			}
		}
		for(Method method :filter.getClass().getSuperclass().getDeclaredMethods()){
			if(method.getName().startsWith("get") && !method.getName().equals("getClass") && !contains(getters, method)){
				getters.add(method);
			}
		}		
		for(Method method : getters){
			FilterPersistenceAnnotation.OperationType operation = OperationType.EQUALS;
			Object value = null;
			operation = OperationType.EQUALS;
			boolean isExcluded = false;
			boolean truncateDate = false;
			String symbol = EQUAL;
			String model = null;
			String paramKey = null;
			try {
				value = MethodUtils.invokeMethod(filter, method.getName(), null);				
			} catch (Exception e) {			
			}
			if(value != null && !StringUtil.isEmpty(value.toString())){
				if(method.isAnnotationPresent(FilterPersistenceAnnotation.class)){
					FilterPersistenceAnnotation annotation = 
						method.getAnnotation(FilterPersistenceAnnotation.class);
					key = annotation.persistenceName();
					isExcluded = annotation.excluded();
					operation = annotation.operation();
					truncateDate = annotation.truncateDate();
					paramKey = annotation.paramKey();
				}else{
					key = method.getName().substring(3);					
					key = key.substring(0, 1).toLowerCase() + key.substring(1);
				}
				model = modelName + "." + key;
				paramKey = StringUtil.isEmpty(paramKey) ? key : paramKey;
				if(!isExcluded){										
					switch(operation){							
						case LIKE:
							symbol = LIKE;							
							break;							
						case GREATERTHAN:
							symbol = GREATER;
							break;
						case GREATERTHANEQUAL:
							symbol = GREATEROREQUAL;
							break;
						case LESSTHAN:
							symbol = LESS;
							break;
						case LESSTHANEQUAL:
							symbol = LESSOREQUAL;
							break;
						default:
							break;						
					}
					if(symbol.equals(LIKE)){
						JpaUtil.addParameterLike(query, model, value, paramKey, parameters);
					}else if(!truncateDate){
						JpaUtil.addParameter(query, model, value, paramKey, parameters, symbol);
					}else{
						JpaUtil.addTruncParameter(query, model, value, paramKey, parameters, symbol);
					}			
				}
			}			
		}
	}
	
	private static boolean contains(List<Method> methods, Method method){
		for(Method m : methods){
			if(m.getName().equals(method.getName())){
				return true;
			}
		}
		return false;
	}
	
	public static void setQueryParametersByProperties(Query entityQuery,
			Map<String, Object> parameters) {		
		for(Entry<String, Object> entry : parameters.entrySet()){
			entityQuery.setParameter(entry.getKey(), entry.getValue());
		}
	}
		
	private static String normalizeParameterName(String name){
		String normalized = name.replaceAll("\\.", UtileriasWeb.SEPARADOR); 
		return normalized; 
	}
	
	/**
	 * Method to transform a <code>List<String></code> 
	 * to String separate with comma character
	 * 
	 * @param lsToTransform List to transform to unique Strinig chaine
	 * 
	 * @return Chaine of elements separate with comma
	 */
	public static String transformListToString(List<String> lsToTransform){
		StringBuilder transform = new StringBuilder();
		if(!lsToTransform.isEmpty()){
			for(String element : lsToTransform){
				transform.append(element)
					.append(",");
			}
		}
		return !transform.toString().isEmpty() ? transform.toString() : null;
	}
}
