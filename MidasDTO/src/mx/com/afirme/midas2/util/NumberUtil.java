package mx.com.afirme.midas2.util;

import java.text.DecimalFormat;
import java.text.ParseException;

public class NumberUtil {

	public static int parseInt(String pattern, String source)
			throws ParseException {
		DecimalFormat decimalFormat = new DecimalFormat(pattern);
		Number number = decimalFormat.parse(source);
		return number.intValue();
	}

	public static String format(String pattern, int intValue) {
		DecimalFormat decimalFormat = new DecimalFormat(pattern);
		return decimalFormat.format(intValue);
	}

	public static Integer toInteger(String integerStringValue) {
		Integer integerValue = null;
		if (integerStringValue != null
				&& integerStringValue.trim().length() > 0) {
			integerValue = new Integer(integerStringValue);
		} // End of if
		return integerValue;
	}
	
	public static boolean isInteger(String integerStringValue) {
	  boolean isInteger = false;
	  try {
	    if(!StringUtil.isEmpty(integerStringValue)) {
	      NumberUtil.toInteger(integerStringValue);
	      isInteger = true;
	    } // End of if
	  } catch (Exception exception) {
	    isInteger = false;
	  } // End of try/catch
	  return isInteger;
	}

}
