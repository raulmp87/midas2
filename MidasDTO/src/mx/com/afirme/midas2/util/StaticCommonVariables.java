package mx.com.afirme.midas2.util;
/**
 * Clase que contiene objetos comunes, se deben de poner como estaticos
 * para reutilizarlos y no volver a crear objetos en memoria.
 * @author vmhersil
 *
 */
public class StaticCommonVariables {
	//Variable para utilizarse al crear las entidades en JPA
	public static final String DEFAULT_SCHEMA="MIDAS";
}
