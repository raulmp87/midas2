package mx.com.afirme.midas2.util.apn;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;

import com.relayrides.pushy.apns.ApnsEnvironment;
import com.relayrides.pushy.apns.ApnsPushNotification;
import com.relayrides.pushy.apns.ExpiredToken;
import com.relayrides.pushy.apns.ExpiredTokenListener;
import com.relayrides.pushy.apns.FailedConnectionListener;
import com.relayrides.pushy.apns.PushManager;
import com.relayrides.pushy.apns.PushManagerConfiguration;
import com.relayrides.pushy.apns.RejectedNotificationListener;
import com.relayrides.pushy.apns.RejectedNotificationReason;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.MalformedTokenStringException;
import com.relayrides.pushy.apns.util.SSLContextUtil;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;

public class SendAPNs {
	private final static Logger LOG = Logger.getLogger(SendAPNs.class);
	private PushManager<SimpleApnsPushNotification> pushManager;

	/**
	 * Envía una notificación APN a un dispositivo iOS
	 * 
	 * @param data
	 * @param registrationId
	 * @param pathFile
	 * @param pw
	 * @param message
	 * @param environment
	 * @throws UnrecoverableKeyException
	 * @throws KeyManagementException
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws MalformedTokenStringException
	 * @throws InterruptedException
	 */
	public void sendAPN(Map<String, String> data, String registrationId, 
			String pathFile, String pw, String message, String environment) 
			throws UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException, 
			CertificateException, IOException, InterruptedException{

		if (pushManager == null) {
			pushManager = new PushManager<SimpleApnsPushNotification>(
					this.getApnsEnvironment(environment),
					SSLContextUtil.createDefaultSSLContext(pathFile, pw),
					null,
					null,
					null,
					new PushManagerConfiguration(),
					"PushManager"
					);
	
			pushManager.registerRejectedNotificationListener(new RejectedNotificationListener<ApnsPushNotification>() {
				@Override
				public void handleRejectedNotification(PushManager<? extends ApnsPushNotification> pushManager,
						ApnsPushNotification notification, RejectedNotificationReason rejectionReason) {
					LOG.error(" sendAPN() -- Error al mandar la notificacion por APN" + notification + " " + rejectionReason);
				}
			});
	
			pushManager.registerFailedConnectionListener(new FailedConnectionListener<ApnsPushNotification>() {
				@Override
				public void handleFailedConnection(PushManager<? extends ApnsPushNotification> pushManager,
						Throwable cause) {
					LOG.error("sendAPN() -- " + pushManager + " " + cause.getMessage(), cause);
				}
			});
	
			pushManager.registerExpiredTokenListener(new ExpiredTokenListener<ApnsPushNotification>() {
				@Override
				public void handleExpiredTokens(PushManager<? extends ApnsPushNotification> pushManager,
						Collection<ExpiredToken> expiredTokens) {
					for (ExpiredToken token : expiredTokens) {
						LOG.info("Expired Tokens " + token);
					}
				}
			});
			pushManager.start();
		}

		byte[] token = null;
		try {
			token = TokenUtil.tokenStringToByteArray(registrationId);
		} catch (MalformedTokenStringException e) {
			LOG.error(e.getMessage());
		}

		final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();

		payloadBuilder.setAlertBody(message);
		payloadBuilder.setSoundFileName("ring-ring.aiff");

		if (data != null){
			for (Map.Entry<String, String> entry: data.entrySet()){
				payloadBuilder.addCustomProperty(entry.getKey(), entry.getValue());
			}
		}

		final String payload = payloadBuilder.buildWithDefaultMaximumLength();
		pushManager.getQueue().put(new SimpleApnsPushNotification(token, payload));
		LOG.info("Mensaje puesto en la Queue:" + pushManager.getQueue());
	}
	
	public ApnsEnvironment getApnsEnvironment(String environment){
		ApnsEnvironment apnsEnvironment = null;
		
		if (environment.equals("sandbox")){
			apnsEnvironment = ApnsEnvironment.getSandboxEnvironment();
		} else if (environment.equals("prod")){
			apnsEnvironment = ApnsEnvironment.getProductionEnvironment();
		}
		return apnsEnvironment;
	}
}
