package mx.com.afirme.midas2.util;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Clase con metodos estaticos para invocar metodos,obtener informacion de anotaciones, obtener atributos de cualquier anotacion.
 * Super clase xD
 * @author vmhersil
 *
 */
public class ReflectionUtils {
	/**
	 * Busca si un atributo o atributos de una clase className utiliza una anotacion especifica.
	 * @param className
	 * @param annotationClass
	 * @return lista de atributos que usan la anotacion annotationClass
	 */
	public static List<Field> getFieldsWithAnnotation(Class<?> className,Class<?> annotationClass){
		List<Field> fieldsWithAnnotations=new ArrayList<Field>();
		if(className!=null && annotationClass!=null){
			Field[] fields=getAllFields(className);
			if(fields!=null && fields.length>0){
				for(Field field:fields){
					Annotation[] annotations=field.getDeclaredAnnotations();
					if(annotations!=null && annotations.length>0){
						for(Annotation annotation:annotations){
							if(annotation.annotationType().equals(annotationClass)){
								if(field!=null){
									fieldsWithAnnotations.add(field);
								}
							}
						}
					}
				}
			}
		}
		return fieldsWithAnnotations;
	}
	/**
	 * Permite agrupar campos o atributos de una clase por medio de los valores de un atributo de una anotacion.
	 * Ejemplo:
	 * @ Anotacion1(seccion="datosGenerales")
	 * private String nombre;
	 * @ Anotacion1(seccion="datosContacto")
	 * private String celular;
	 * @ Anotacion1(seccion="datosGenerales")
	 * private String apellido;
	 * 
	 * El metodo te regresara un mapa de 2 elementos ("datosGenerales",lista con el campo nombre y apellido), y 
	 * el otro elemento ("datosContacto",con la lista de 1 elemento->celular)
	 * @param className
	 * @param annotationClass
	 * @param attribute
	 * @return
	 */
	public static Map<String, List<Field>> getFieldsWithAnnotationGroupByAttribute(Class<?> className,Class<?> annotationClass,String attribute){
		Map<String,List<Field>> fields=new HashMap<String, List<Field>>();
		List<Field> campos=getFieldsWithAnnotation(className, annotationClass);
		if(campos!=null && !campos.isEmpty()){
			for(Field campo:campos){
				Object attributeValue=getAttributeValueFromAnnotation(campo, annotationClass, attribute);
				if(attributeValue!=null){
					String value=attributeValue.toString();
					//Si el mapa no contiene esa llave, entonces la agrega
					if(!fields.containsKey(value)){
						fields.put(value, new ArrayList<Field>());
					}
					List<Field> fieldsOfAttribute=fields.get(value);
					//Si no hay nada en la lista o si no existe aun en la lista de elementos agrupados por el atributo actual, entonces se agrega-
					if((fieldsOfAttribute.isEmpty())||!fieldExistInList(campo, fieldsOfAttribute)){
						fieldsOfAttribute.add(campo);
					}
				}
			}
		}
		return fields;
	}
	/**
	 * Indica si un campo existe en una lista de campos
	 * @param field
	 * @param fieldList
	 * @return
	 */
	private static boolean fieldExistInList(Field field,List<Field> fieldList){
		for(Field f:fieldList){
			String fieldName=field.getName();
			if(fieldName.equals(f.getName())){
				return true;
			}
		}
		return false;
	}
	/**
	 * Ejecuta el método get de un atributo y obtiene su valor
	 * @param object
	 * @param field
	 * @return
	 */
	public static Object getValue(Object object,Field field){
		Object value=null;
		if(field!=null && object!=null){
			String fieldName=field.getName();
			String methodName="get"+capitalize(fieldName);
			try {
				Method getterMethod=object.getClass().getMethod(methodName, null);
				if(getterMethod!=null){
					value=getterMethod.invoke(object,null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return value;
	}
	/**
	 * Busca un metodo en la clase
	 * @param className
	 * @param methodName
	 * @return
	 */
	private static Method findMethodInClass(Class<?> className,String methodName){
		Method method=null;
		Method[] methods=getAllMethods(className);
		if(methods!=null && methods.length>0){
			for(Method m:methods){
				if(methodName.equals(m.getName())){
					method=m;
					break;
				}
			}
		}
		return method;
	}
	/**
	 * Encuentra un atributo dentro de una clase, buscando entre todos los atributos(incluyendo los heredados)
	 * @param className
	 * @param fieldName
	 * @return
	 */
	@SuppressWarnings("unused")
	private static Field findFieldInClass(Class<?> className,String fieldName){
		Field field=null;
		Field[] fields=getAllFields(className);
		if(fields!=null && fields.length>0){
			for(Field f:fields){
				if(fieldName.equals(f.getName())){
					field=f;
					break;
				}
			}
		}
		return field;
	}
	/**
	 * Se obtienen todos los metodos de una clase, incluyendo los heredados
	 * @param className
	 * @return
	 */
	private static Method[] getAllMethods(Class<?> className){
		Method[] methods=null;
		List<Method> variables=new ArrayList<Method>();
		Class<?> superClass=className.getSuperclass();
		if(superClass!=null){
			Method[] m=getAllMethods(superClass);
			if(m!=null && m.length>0){
				for(Method method:m){
					variables.add(method);
				}
			}
			if(className.getDeclaredFields()!=null && className.getDeclaredFields().length>0){
				for(Method method:className.getDeclaredMethods()){
					variables.add(method);
				}
			}
			methods=new Method[0];
			methods=variables.toArray(methods);
		}else{
			methods=className.getDeclaredMethods();
		}
		return methods;
	}
	/**
	 * Invoca un metodo pasandole el objeto actual, el nombre del metodo a ejecutar y sus parametros 
	 * @param object
	 * @param methodName
	 * @param params
	 * @return
	 */
	public static Object invoke(Object object,String methodName,Object... params){
		Object value=null;
		if(object!=null && methodName!=null && !methodName.isEmpty()){
			Method method=findMethodInClass(object.getClass(), methodName);
			if(method!=null){
				try {
					if(method!=null){
						value=method.invoke(object,params);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	/**
	 * Obtiene el valor de un atributo de una anotacion de un atributo de un objeto.
	 * Ejemplo, suponer que se tiene la clase Cliente, y tiene un atributo nombre, el atributo tiene una anotacion llamada Anotacion1.
	 * En la anotacion1 tiene un atributo llamado mappingLabel:
	 * @ Anotacion1(mappingLabel="Nombre del cliente")
	 * private String nombre;
	 * 
	 * Este metodo regresará el valor del atributo mappingLabel para el campo nombre. 
	 * 
	 * @param field es el campo del cual queremos obtener el valor del atributo de la anotacion.(En el ejemplo es nombre)
	 * @param annotationClass Indica la clase de la anotacion.(En el ejemplo es Anotacion1)
	 * @param attribute nombre del atributo del cual queremos obtener su valor.
	 * @return
	 */
	public static Object getAttributeValueFromAnnotation(Field field,Class<?> annotationClass,String attribute){
		Object value=null;
		if(field!=null && annotationClass!=null && (attribute!=null && !attribute.isEmpty())){
			Annotation[] annotations=field.getAnnotations();
			if(annotations!=null && annotations.length>0){
				for(Annotation annotation:annotations){
					if(annotation.annotationType().equals(annotationClass)){
						try {
							value=invoke(annotation,attribute,new Object[]{});
							//Si se obtuvo un valor en la anotacion y si es el tipo de anotacion que se buscaba, entonces es el valor correcto
							if(value!=null){
								break;
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}
			}
		}
		return value;
	}
	/**
	 * Cambia una cadena en camel case
	 * @param s
	 * @return
	 */
	private static String capitalize( String s ) {
		for (int i = 0; i < s.length(); i++) {
			if (i == 0) {
				s = String.format("%s%s", Character.toUpperCase(s.charAt(0)),s.substring(1));
			}
			if (!Character.isLetterOrDigit(s.charAt(i))) {
				if (i + 1 < s.length()) {
					s = String.format("%s%s%s", s.subSequence(0, i + 1),Character.toUpperCase(s.charAt(i + 1)),s.substring(i + 2));
				}
			}

		}
	    return s;
	}
	/**
	 * Obtiene los campos de la clase incluyendo los atributos heredados
	 * @param className
	 * @return
	 */
	public static Field[] getAllFields(Class<?> className){
		Field[] fields=null;
		List<Field> variables=new ArrayList<Field>();
		Class<?> superClass=className.getSuperclass();
		if(superClass!=null){
			Field[] f=getAllFields(superClass);
			if(f!=null && f.length>0){
				for(Field field:f){
					variables.add(field);
				}
			}
			if(className.getDeclaredFields()!=null && className.getDeclaredFields().length>0){
				for(Field field:className.getDeclaredFields()){
					variables.add(field);
				}
			}
			fields=new Field[0];
			fields=variables.toArray(fields);
		}else{
			fields=className.getDeclaredFields();
		}
		return fields;
	}
	/**
	 * Obtiene un mapa de un Bean con su propiedad del bean y su valor
	 * @param bean
	 * @return
	 */
	public static Map<String,Object> getMapFromBean(Object bean){
		Class<?> className=(bean!=null)?bean.getClass():null;
		Map<String,Object> mappedValues=new HashMap<String, Object>();
		if(className==null){
			return null;
		}
		Field[] fields=getAllFields(className);
		if(fields!=null && fields.length>0){
			for(Field field:fields){
				String property=field.getName();
				Object value=getValue(bean, field);
				//Solamente se consideran los atributos que no son nulos
				if(value!=null){
					mappedValues.put(property, value);
				}
			}
		}
		return mappedValues;
	}
}
