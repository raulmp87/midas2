package mx.com.afirme.midas2.util;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.domain.cobranza.Recibos;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransactionResponse;
import mx.com.afirme.midas2.exeption.ApplicationException;


public interface MailService {
	public void sendMail(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment);

	public void sendMail(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment, String subject);

	public void sendMail(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting);
	
	public void sendMailAgenteNotificacion(List<String> address,List<String> cc,List<String> cco, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting,int tipoTemplate);

	public void sendMail(String address,List<String> cc,List<String> cco, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting,int tipoTemplate);
	
	public void sendMail(List<String> address, String title, String message);
	
	public void sendMessage(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments, List<String> cc,
			List<String> cco);
	
	/***
	 * 
	 * @param recipients
	 * @param subject
	 * @param messageContent
	 * @param from
	 * @param attachments
	 * @param cc con copia
	 * @param cco con copia oculta
	 * @param mapInlineImages permite el envío de imagenes embebidas en el contenido del mensaje, 
	 *        Map con los elementos: imageId - imageFilePath
	 */
	public void sendMessage(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments,List<String> cc,
			List<String> cco, Map<String, String> mapInlineImages);

	public void sendMailWithExceptionHandler(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting) throws ApplicationException;		
	
	public void sendMessageWithExceptionHandler(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments, List<String> cc,
			List<String> cco) throws Exception;
	
	public void sendMailToRoleList(List<String> roleList, String title,
			String message);

	public void sendMailToAdmin(String title, String message);
	
	public void sendMailToAdminAgentes(String title, String message,
			String subject, List<ByteArrayAttachment> attachment, String tipoCalculo);
	
	public  String getmensajeFacturaRecibos(Recibos reciboaenviar);
	public String generarPlantillaPortal(List<ReciboDTO> recibosDTO, PROSATransactionResponse transactionResponse, PolizaDTO poliza);
}
