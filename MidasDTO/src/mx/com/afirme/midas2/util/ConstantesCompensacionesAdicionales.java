/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.util;


public class ConstantesCompensacionesAdicionales {
	
	public static final String ROL_GERENTE_RECURSOS ="Gerente_Recursos";
	
	/// Contraprestacion
	public static final boolean CONTRAPRESTACIONSI = true;
	public static final boolean CONTRAPRESTACIONNO = false;
	
	/// Tipos de Compensaciones
	public static final int PORPRIMA = 1;
	public static final int PORBAJASINIESTRALIDAD = 2;
	public static final int DERECHOPOLIZA = 3;
	public static final int BONO = 4;
	public static final int SOBRECOMISION = 5;
	public static final int PORUTILIDAD = 6;
	public static final int PORCUMPLIMIENTODEMETA = 7;
	
	/// Tipos de inclusiones
	public static final int INCLUIRNINGUNO = 0;
	public static final int INCLUIRCUADERNO = 1;
	public static final int INCLUIRCONVENIOESPECIAL = 2;	
	
	/// Si/No/Activado/Desactivado
	public static final int SI = 1;
	public static final int NO = 0;	
	public static final int ACTIVADO = 1;
	public static final int DESACTIVADO = 0;
	public static boolean BORRADOLOGICOSI = true;
	public static boolean BORRADOLOGICONO = false;
	
	/// Tipo de Condiciones
	public static final int SINAJUSTE = 1;
	public static final int CONAJUSTECOMISIONNORMAL = 2;
	public static final int CONAJUSTECOMISIONPACTADA = 3;
	public static final int SINAJUSTECOMISIONPACTADA = 4;
	
	/// Tipo de emision/emisor
	public static final int EMISIONINTERNA = 1;
	public static final int EMISIONEXTERNA = 2;
	
	/// Tipo de contratos
	public static final int CONTRATOART102 = 1;
	public static final int CONTRATOSERVICIOS = 2;
	
	//RAMOS
	public enum RAMO {
		NONE ("", -1L),
	    AUTOS ("A", 1L),
	    DANOS ("D", 2L),
	    VIDA("V", 3L),
	    BANCA("B", 4L); 
	    
	    private final String tipo; 
	    private final Long valor;
	 
	    RAMO (String color, Long valor) { 
	        this.tipo = color;
	        this.valor = valor;
	    } 
	    
	    public String getTipo() { 
	    	return tipo; 
	    }
	    
	    public Long getValor() {
	    	return valor; 
	    }
	    	    
	    public static RAMO findByType(String type){
	    	RAMO ramo = RAMO.NONE;
	    	for(RAMO ra: values()){	    		
	    		if(ra.getTipo().equals(type)){
	    			ramo = ra;
	    			break;
	    		}	    		
	    	}
	    	return ramo;
	    }
	    
	} 
		
	//PROCESOS PARA CARGA DE FILTROS EN JSP
	public enum TIPO_PROCESO {
		AGREGAR(0l, "AGREGAR"),
		CONSULTA(1l, "CONSULTA"), 
		MODIFICACION (2l, "MODIFICACION");
				
		private long id;
		private String nombre;
				
		private TIPO_PROCESO(long id, String nombre) {
			this.id = id;
			this.nombre = nombre;
		}
		
		public long getId() {
			return id;
		}
		
		public String getNombre() {
			return nombre;
		}
		
		public static TIPO_PROCESO parse(long id) {
			TIPO_PROCESO e = null; // Default
            for (TIPO_PROCESO item : TIPO_PROCESO.values()) {
                if (item.getId() ==id) {
                    e = item;
                    break;
                }
            }
            return e;
        }
	}

	/// Tipo de Entidades
	public static final Long AGENTE = new Long(1);
	public static final Long PROMOTOR = new Long(2);
	public static final Long PROVEEDOR = new Long(3);
	
	public static final String TOKENLIMITADOR = "$";
	
	/// Niveles por Baja Siniestralidad
	public static final String NOMBRER = "nombreR";
	public static final String INICIALR = "inicialR";
	public static final String FINALR = "finalR";
	public static final String COMPENSACIONR = "compensacionR";
	
	/// Base Calculo
	public static final int PRIMANETA = 1;
	public static final int PRIMANETARECARGO = 2;
	public static final int PRIMACEDIDA = 3;
	public static final int PRIMATOTAL = 4;
	public static final int PRIMARIESGO = 5;
	
	/// Propiedades de Columnas
	public static final String PROPIEDADID = "id";
	public static final String PROPIEDADNOMBRE = "nombre";
	public static final String PROPIEDADVALOR = "valor";
	public static final String PROPIEDADDESCRIPCION = "descripcion";
	
	///Estatus
	public static final Long ESTATUS_ACTIVA= new Long(1);
	public static final Long ESTATUS_INACTIVA= new Long(2);
	public static final Long ESTATUS_PENDIENTE= new Long(3);
	public static final Long ESTATUS_RECHAZADO= new Long(4);
	public static final Long ESTATUS_APROBADO= new Long(5);
	
	/// TipoAutorizacion
	public static final Long AUTORIZACION_DIRECTORTECNICO= new Long(1);
	public static final Long AUTORIZACION_ADMONAGENTES= new Long(2);
	public static final Long AUTORIZACION_JURIDICO= new Long(3);
	
	public static final String CONFIGURACION = "CONFIGURACION";
	public static final String MODIFICACION = "MODIFICACION";
	public static final String AUTORIZACION_AREA_TECNICA= "AUTORIZACION AREA TECNICA";
	public static final String AUTORIZACION_ADMINISTRADOR_AGENTE= "AUTORIZACION ADMINISTRADOR AGENTE";
	public static final String AUTORIZACION_AREA_JURIDICA= "AUTORIZACION AREA JURIDICA";
	
	public static final String TAB_COMPENSACION = "compensacion";
	public static final String TAB_CONTRAPRESTACION = "contraprestacion";
	public static final String TAB_COMISIONES = "comisiones";
	
	public static final String COTIZACION_ID = "cotizacionId";
	public static final String NEGOCIO_ID = "negocioId";
	public static final String NEGOCIOVIDA_ID = "negocioVidaId";
	public static final String CONFIGURACIONBANCA_ID = "configuracionBancaId";
	public static final String FN_AUTORIZA_TECNICO = "FN_M2_AutorizaTecnico_CA";
	public static final String FN_AUTORIZA_AGENTE = "FN_M2_AutorizaAgente_CA";
	public static final String FN_AUTORIZA_JURIDICO = "FN_M2_AutorizaJuridico_CA";
	
	

}
