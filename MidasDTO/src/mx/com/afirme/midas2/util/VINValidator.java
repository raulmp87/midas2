package mx.com.afirme.midas2.util;


import java.util.HashMap;
import java.util.Map;

public class VINValidator {

  private static final VINValidator INSTANCE = 
    new VINValidator();
 
  private Map<String, Integer> translations = 
    new HashMap<String, Integer>(); 
  
  private int[] weights = 
    {8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2}; 
  
  public static VINValidator getInstance(){
    return VINValidator.INSTANCE;
  }
  
  public VINValidator(){
    //load translations
    translations.put("A", 1);
    translations.put("B", 2);
    translations.put("C", 3);
    translations.put("D", 4);
    translations.put("E", 5);
    translations.put("F", 6);
    translations.put("G", 7);
    translations.put("H", 8);
    translations.put("J", 1);
    translations.put("K", 2);
    translations.put("L", 3);
    translations.put("M", 4);
    translations.put("N", 5);
    translations.put("P", 7);
    translations.put("R", 9);
    translations.put("S", 2);
    translations.put("T", 3);
    translations.put("U", 4);
    translations.put("V", 5);
    translations.put("W", 6);
    translations.put("X", 7);
    translations.put("Y", 8);
    translations.put("Z", 9); 
  }
  
  private int[] translate(String serialNumber){    
    int[] translation = new int[17];
    char[] serial = serialNumber.toCharArray();    
    try{
      for(int i = 0; i <17; i++){        
        String value = String.valueOf(serial[i]);
        if(NumberUtil.isInteger(value)){
          translation[i] = Integer.parseInt(value);
        }else{
          translation[i] = translations.get(value);
        }      
      }
    }catch(Exception ex){
      throw new RuntimeException( 
          "No se puede traducir el número de serie. Probablemente contiene caracteres invalidos");
    }
    return translation;
  }
  
  private int getVerificationDigit(int[] data){   
    int modular = 0;
    int total = 0;
    for(int i = 0; i <17; i++){
      total += data[i] * weights[i];
    }
    modular = total % 11; //11 is fixed 
    return modular;
  }
  
  public boolean isValid(String serialNumber){
    boolean valid = true;
    String verificationDigit = null;        
    //check if serial number is empty
    if(StringUtil.isEmpty(serialNumber)){
      throw new RuntimeException("El número de serie es nulo o vacío");
    }        
    serialNumber = serialNumber.trim().toUpperCase();        
    //if serial number length is not equal to 17, the VIN validation 
    //cannot be applied, therefore the seral number is valid 
    if(serialNumber.length() != 17){
      return true;
    }     
    int digit = getVerificationDigit(translate(serialNumber));  
    if(digit == 10){
      verificationDigit = "X";    
    }else{
      verificationDigit = String.valueOf(digit);
    }
    if(!serialNumber.substring(8, 9).equals(verificationDigit)){
      valid = false;
    }
    return valid;
  }
  
  public String getVerificationDigit(String serialNumber){
    String verificationDigit = null;        
    //check if serial number is empty
    try{       
      int digit = getVerificationDigit(translate(serialNumber));  
      if(digit == 10){
        verificationDigit = "X";    
      }else{
        verificationDigit = String.valueOf(digit);
      }
    }catch(Exception ex){
      verificationDigit = "No calculable"; 
    }
    return verificationDigit;
  }
 
  
}

