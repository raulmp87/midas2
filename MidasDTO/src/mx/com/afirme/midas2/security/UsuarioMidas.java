package mx.com.afirme.midas2.security;
import javax.persistence.Column;
import javax.persistence.Id;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
/**
 * Entidad mapeada a la vista de VW_USUARIOMIDAS
 * @author Armando Garcia
 * @version 1.0
 * @created 24-jun-2014 02:05:23 p.m.
 * 
 * @deprecated Se debe usuar usuarioservice
 */
@Deprecated
public class UsuarioMidas implements Entidad {
	
	private static final long serialVersionUID = -9026203922937942155L;

	@Id
	@Column(name="USER_ID")
	private Long idUsuario;
	
	@Column(name="CVE_USUARIO")
	private String claveUsuario;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="USERNAME")
	private String userName;

	public UsuarioMidas(){

	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void finalize() throws Throwable {

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return idUsuario;
	}

	@Override
	public String getValue() {	
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return idUsuario;
	}

}