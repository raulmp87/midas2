package mx.com.afirme.midas2.exeption;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@javax.ejb.ApplicationException(rollback=true)
public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 6555671132638656933L;
	
	private List<String> errors = new ArrayList<String>();
	private Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
	
	private String message = "";
	
	public ApplicationException(String message) {
		errors.add(message);
		this.message = buildMessage();
	}
	
	public ApplicationException(List<String> errors,  Map<String, List<String>> fieldErrors) {
		this.errors = errors;
		this.fieldErrors = fieldErrors;
		this.message = buildMessage();
	}
	
	public ApplicationException(ErrorBuilder errorBuilder) {
		this(errorBuilder.getErrors(), errorBuilder.getFieldErrors());
	}
	
	public List<String> getErrors() {
		return new ArrayList<String>(errors);
	}
	
	public Map<String, List<String>> getFieldErrors() {
		Map<String, List<String>> fieldErrorsCopy = new HashMap<String, List<String>>();
		for (Entry<String, List<String>> entry : fieldErrors.entrySet()) {
			fieldErrorsCopy.put(entry.getKey(), new ArrayList<String>(entry.getValue()));
		}
		return fieldErrorsCopy;
	}
	
	private String buildMessage() {
		StringBuilder message = new StringBuilder();
		for (String error : errors) {
			message.append(error + " ");
		}
		
		for (Entry<String, List<String>> entry : fieldErrors.entrySet()) {
			message.append(entry.getKey() + ": ");
			for (String error : entry.getValue()) {
				message.append(error + " ");
			}
		}
		
		return message.toString();
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}

}
