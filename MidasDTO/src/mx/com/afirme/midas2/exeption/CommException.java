package mx.com.afirme.midas2.exeption;

public class CommException extends RuntimeException{

	private static final long serialVersionUID = -6473800207690602117L;
	
	public static enum CODE{
		DATA_ERROR, COMM_ERROR, GENERAL_ERROR, PARAMS_ERROR
	};
	
	private CODE code;
	
	private String cleanMessage;

	public CommException(){
		super();
	}
	
	public CommException(CODE codigo){
		super();
		this.code = codigo;		
	}
	
	public CommException(CODE codigo, String message){
		super();
		this.code = codigo;		
		this.cleanMessage = message;		 
	}

	public String getCleanMessage() {
		return cleanMessage;
	}

	public void setCleanMessage(String cleanMessage) {
		this.cleanMessage = cleanMessage;
	}

	public void setCode(CODE code) {
		this.code = code;
	}

	public CODE getCode() {
		return code;
	}
	
	
}
