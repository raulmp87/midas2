package mx.com.afirme.midas2.exeption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorBuilder implements Serializable {

	private static final long serialVersionUID = 6642418339824151044L;
	
	private List<String> errors = new ArrayList<String>();
	private Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
	
	
	public ErrorBuilder() {
	}
	
	public ErrorBuilder(List<String> errors, Map<String, List<String>> fieldErrors) {
		this.errors = errors;
		this.fieldErrors = fieldErrors;
	}
	
	public ErrorBuilder addFieldError(String fieldName, String message) {
		List<String> fieldErrorsList = fieldErrors.get(fieldName);
		if (fieldErrorsList == null) {
			fieldErrorsList = new ArrayList<String>();
			fieldErrors.put(fieldName, fieldErrorsList);
		}
		fieldErrorsList.add(message);
		return this;
	}
	
	public ErrorBuilder addError(String message) {
		errors.add(message);
		return this;
	}
	
	
	public List<String> getErrors() {
		return errors;
	}
	
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public Map<String, List<String>> getFieldErrors() {
		return fieldErrors;
	}
	
	public void setFieldErrors(Map<String, List<String>> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
	
	public boolean hasErrors() {
		return errors.size() > 0 || fieldErrors.size() > 0;
	}
	
}
