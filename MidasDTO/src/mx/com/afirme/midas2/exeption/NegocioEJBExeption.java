package mx.com.afirme.midas2.exeption;

import javax.ejb.EJBException;
/**
 * 
 * @author Angel Verdugo
 *
 */
public class NegocioEJBExeption extends EJBException {
	private String errorCode;
	private String messageClean;
	private Object[] values;
	
	public NegocioEJBExeption(String errorCode,String messageDetail){
		super("<NE>"+errorCode+"<NE><NEM>"+messageDetail+"<NEM>");
		this.errorCode = errorCode;
		this.messageClean  = messageDetail;
	}
	
	public NegocioEJBExeption(String errorCode,Object[] values){
		super("<NE>"+errorCode+"<NE><NEM>"+values+"<NEM>");
		this.errorCode = errorCode;
		this.messageClean  = values.toString();
		this.values = values;
	}
	public NegocioEJBExeption(String errorCode,Object[] values,String messageDetail){
		super(messageDetail);
		this.errorCode = errorCode;
		this.messageClean  = messageDetail;
		this.values = values;
	}
	
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public String getMessageClean() {
		return messageClean;
	}

	
	public Object[] getValues() {
		return values;
	}
	
	
}
