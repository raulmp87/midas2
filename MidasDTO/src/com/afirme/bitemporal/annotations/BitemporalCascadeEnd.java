package com.afirme.bitemporal.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Indicates that a relationship must be cascade ended if the parent is ended.</p>
 * <p>Only properties that extend Continuity are allowed to be annotated.</p>
 * @author alfredo.osorio
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BitemporalCascadeEnd {

}
