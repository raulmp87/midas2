package com.afirme.bitemporal.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;

/**
 * <p>Indicates that the framework must check that a Bitemporal of a Continuity property is not null.
 * The framework does the validation whenever a new value is added by the {@link BitemporalProperty}'s setter methods.</p> 
 * 
 * <p>Only properties that extend Continuity are allowed to be annotated.</p>
 * 
 * @author alfredo.osorio
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BitemporalNotNull {

}
