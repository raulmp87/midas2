package com.anasoft.os.daofusion.bitemporal;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AnnotationUtils {

	public static <T extends Annotation> Field[] getFieldsWithAnnotation(Class<?> clazz, Class<T> annotationClass, boolean makeAccessible) {
		List<Field> fieldsList = new ArrayList<Field>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(annotationClass) != null) {
				if (makeAccessible) {
					field.setAccessible(true);
				}
				fieldsList.add(field);
			}
		}
		return fieldsList.toArray(new Field[fieldsList.size()]);
	}
	
	public static Object getFieldValue(Field field, Object obj) {
		try {
			return field.get(obj);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
