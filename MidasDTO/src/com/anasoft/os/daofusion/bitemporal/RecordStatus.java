package com.anasoft.os.daofusion.bitemporal;

/**
 * @author alfredo.osorio
 */
public enum RecordStatus {
	NOT_IN_PROCESS, TO_BE_ADDED, TO_BE_ENDED;
}
