/*
 * (c) Copyright Ervacon 2007.
 * All Rights Reserved.
 */

package com.anasoft.os.daofusion.bitemporal;


/**
 * A bitemporal object. A bitemporal object is essentially a value tracked in two time dimensions:
 * 
 * <ul>
 * 	<li><i>Validity time</i> - indicates when the value was valid.
 * 	<li><i>Recording time</i> - indicates when the value was known.
 * </ul>
 * 
 * <p>
 * 
 * Tracking information bitemporally allows you to aswer questions along the lines of
 * "On January 2, 1999, what did we think the valid value was for September 1, 1980?".
 * 
 * <p>
 * 
 * In most cases, application level should not directly implement this interface, but should
 * instead wrap existing value classes bitemporally using a {@link BitemporalWrapper}.
 * 
 * @see BitemporalTrace
 * @see BitemporalWrapper
 * 
 * @author Erwin Vervaet
 * @author Christophe Vanfleteren
 * @author alfredo.osorio
 */
public interface Bitemporal {

	/**
	 * Returns the interval in which this bitemporal object is valid.
	 */
	public IntervalWrapper getValidityInterval();

	/**
	 * Returns the interval in which this bitemporal object is known.
	 */
	public IntervalWrapper getRecordInterval();

	/**
	 * End the recording interval of this bitemporal object, indicating that it has been superseded by a new object,
	 * or is deemed as no longer relevant (i.e. because it was faulty knowledge) and should be "forgotten".
	 */
	public void end(boolean twoPhaseMode);
	
	/**
	 * Used by the framework to indicate that this object must be ended but without using cascade end. 
	 * Clients must use <code>end()</code> instead of this method.
	 * @param twoPhaseMode
	 */
	public void endOnlyThis(boolean twoPhaseMode);
	
	/**
	 * Create and return a new bitemporal object representing the same value as this object, but with specified
	 * validity. The recording interval of the returned object will always be {@link TimeUtils#fromNow() from now on}.
	 * 
	 * @param validityInterval Validity interval of the new object.
	 * @param twoPhaseMode indicates whether to use twoPhaseMode
	 * @return New bitemporal object with the given validity and its value retained from its original.
	 */
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode);
	
	/**
	 * Obtain the <code>RecordStatus</code>.
	 * @return
	 */
	public RecordStatus getRecordStatus();
	
	/**
	 * Clears the record status.
	 */
	public void clearRecordStatus();
		
	/**
	 * Indicates whether the record is in two-phase process.
	 * @return
	 */
	public boolean isInProcess();
	
	/**
	 * Commits only if the record is in process.
	 */
	public void commitTwoPhase();
	
	/**
	 * Obtains the value id associated with this <code>Bitemporal</code>. 
	 * @return
	 */
	public String getValueId();

}
