package com.anasoft.os.daofusion.bitemporal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.joda.time.Interval;

/**
 * JODA Interval wrapper used by JPA.
 * Objects of this class are immutable.
 * @author alfredo.osorio
 * @author jorge.cano
 */
@Embeddable
@Access(AccessType.FIELD)
public class IntervalWrapper implements Serializable {

	@Transient
	private Interval interval;
	@Temporal(TemporalType.TIMESTAMP)
	protected Date from;
	@Temporal(TemporalType.TIMESTAMP)
	protected Date to;

	//Required by JPA
	protected IntervalWrapper() {
	}

	public IntervalWrapper(Interval interval) {
		this.from = interval.getStart().toDate();
		this.to = interval.getEnd().toDate();
		this.interval = interval;
	}

	public Interval getInterval() {
		if (interval == null) {
			this.interval = new Interval(from.getTime(), to.getTime());
		}
		return interval;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((interval == null) ? 0 : interval.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntervalWrapper other = (IntervalWrapper) obj;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if (interval != null) {
			return interval.toString();
		} else {
			return "";
		}
	}
	
	@Transient
	public Date getTo()
	{
		return to;		
	}
	
}
