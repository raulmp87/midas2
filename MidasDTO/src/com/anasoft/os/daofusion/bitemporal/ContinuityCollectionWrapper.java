package com.anasoft.os.daofusion.bitemporal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

/**
 * Holder of a collection of <code>Continuity</code>. 
 * Provides methods to retrieve only the valid values for the given date. 
 * It is useful to represent a one-to-many relationship.
 * @author alfredo.osorio
 */
public class ContinuityCollectionWrapper<V, T extends Bitemporal, X extends Continuity<V, T>> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<X> continuities;
	
	public ContinuityCollectionWrapper(Collection<X> continuities) {
		this.continuities = continuities;
	}
	
	private <A> void addIfNotNull(Collection<A> collection, A obj) {
		if (obj != null) {
			collection.add(obj);
		}
	}
	
	/**
	 * Returns the value valid {@link TimeUtils#now() now} as currently known.
	 */
	public Collection<V> now() {
		Collection<V> values = new ArrayList<V>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(values, continuity.getBitemporalProperty().now());			
		}
		return values;
	}

	/**
	 * Returns the value valid on specified date as currently known.
	 */
	public Collection<V> on(DateTime validOn) {
		Collection<V> values = new ArrayList<V>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(values, continuity.getBitemporalProperty().on(validOn));			
		}
		return values;
	}

	/**
	 * Returns the value valid on specified date as known on given date.
	 */
	public Collection<V> on(DateTime validOn, DateTime knownOn) {
		Collection<V> values = new ArrayList<V>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(values, continuity.getBitemporalProperty().on(validOn, knownOn));
		}
		return values;
	}
	
	/**
	 * Returns the value valid on specified date also including the records in process.
	 * This always uses <code>knwonOn</code> as currently known.
	 */
	public Collection<V> nowInProcess() {
		Collection<V> values = new ArrayList<V>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(values, continuity.getBitemporalProperty().nowInProcess());
		}
		return values;
	}
	
	/**
	 * Returns the value valid on specified date also including the records in process.
	 * This always uses <code>knwonOn</code> as currently known.
	 */
	public Collection<V> onInProcess(DateTime validOn) {
		Collection<V> values = new ArrayList<V>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(values, continuity.getBitemporalProperty().onInProcess(validOn));
		}
		return values;
	}

	/**
	 * Returns the {@link Bitemporal} object valid {@link TimeUtils#now() now} as currently known. 
	 */
	public Collection<T> get() {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().get());
		}
		return bitemporals;
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date as currently known.
	 */
	public Collection<T> get(DateTime validOn) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().get(validOn));
		}
		return bitemporals;
	}
	
	/**
	 * Returns the {@link Bitemporal} object to be ended on specified date as currently known.
	 */
	public Collection<T> getToBeEnded(DateTime validOn) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().getToBeEnded(validOn));
		}
		return bitemporals;
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date as known on given date.
	 */
	public Collection<T> get(DateTime validOn, DateTime knownOn) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().get(validOn, knownOn));
		}
		return bitemporals;
	}
	
	/**
	 * Returns the {@link Bitemporal} object valid {@link TimeUtils#now() now} as currently known 
	 * including the records in process. 
	 */
	public Collection<T> getInProcess() {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().getInProcess());
		}
		return bitemporals;
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date also including the records in process.
	 * This always uses knwonOf as currently known.
	 */
	public Collection<T> getInProcess(DateTime validOn) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().getInProcess(validOn));
		}
		return bitemporals;
	}
	
	/**
	 * Returns the {@link Bitemporal} only including the records in process.
	 * This always uses knwonOf as currently known.
	 */
	public Collection<T> getOnlyInProcess(DateTime validOn) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().getOnlyInProcess(validOn));
		}
		return bitemporals;
	}
	
	/**
	 * Based on twoPhaseMode it decides whether to use <code>get()</code> or <code>getInProcess()</code>
	 * @param twoPhaseMode
	 * @return
	 */
	public Collection<T> getInProcessOrGet(DateTime validOn, boolean twoPhaseMode) {
		Collection<T> bitemporals = new ArrayList<T>();
		for (Continuity<V, T> continuity : continuities) {
			addIfNotNull(bitemporals, continuity.getBitemporalProperty().getInProcessOrGet(validOn, twoPhaseMode));
		}
		return bitemporals;
	}
	
	/**
	 * Gets the <code>continuities</code> being wrapped.
	 * @return
	 */
	public Collection<X> getValue() {
		return continuities;
	}

}
