package com.anasoft.os.daofusion.bitemporal;

import org.joda.time.DateTime;

/**
 * Provider that obtains the current date using the Java Virtual Machine Date Time.
 * @author amosomar
 */
public class JavaVmCurrentDateProvider implements CurrentDateProvider {

	@Override
	public DateTime getCurrentDate() {
		return new DateTime();
	}

}
