/*
 * (c) Copyright Ervacon 2007.
 * All Rights Reserved.
 */

package com.anasoft.os.daofusion.bitemporal;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;


/**
 * Decorates a value with bitemporal information, making it possible to
 * bitemporally track the value in a {@link BitemporalTrace}. A
 * {@link BitemporalWrapper} allows you to bitemporally track existing value
 * classes, for instance strings. This class implements the {@link Bitemporal}
 * interface for use with JPA / Hibernate.
 * 
 * <p>
 * 
 * Due to the nature of bitemporality, the wrapped value should be immutable.
 * The value itself will never change, instead new values will be added to the
 * {@link BitemporalTrace} to represent changes of the value. A
 * {@link BitemporalWrapper} itself is not immutable, its record interval can be
 * {@link #end() ended}.
 * 
 * <p>
 * 
 * Instances of this class are serializable if the wrapped value is
 * serializable.
 * 
 * <p>
 * 
 * Objects of this class are not thread-safe.
 * 
 * @param <V>
 *            Value tracked by {@link BitemporalTrace}.
 * 
 * @see Bitemporal
 * @see BitemporalTrace
 * 
 * @author Erwin Vervaet
 * @author Christophe Vanfleteren
 * @author igor.mihalik
 * @author alfredo.osorio
 */
@MappedSuperclass
public abstract class BitemporalWrapper<V, C> implements
		Bitemporal, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private IntervalWrapper validityInterval;

	private IntervalWrapper recordInterval;
	
	private RecordStatus recordStatus = RecordStatus.NOT_IN_PROCESS;

	private String valueId;
	
	protected BitemporalWrapper() {
		// default constructor required by Hibernate
	}

	/**
	 * Bitemporally wrap the given value. Validity will be as specified, and the
	 * recording interval will be {@link TimeUtils#fromNow() from now on}.
	 * 
	 * @param value
	 *            The value to wrap (can be <tt>null</tt>).
	 * @param validityInterval
	 *            Validity of the value.
	 */
	public BitemporalWrapper(V value, IntervalWrapper validityInterval, C continuity, String valueId, boolean twoPhaseMode) {
		if (validityInterval == null) {
			throw new IllegalArgumentException(
					"The validity interval is required");
		}
		if (continuity == null) {
			throw new IllegalArgumentException("Continuity is required.");
		}
		if(this.validityInterval == null) this.validityInterval = new IntervalWrapper();
		this.validityInterval = validityInterval;
		setContinuity(continuity);
		if (!twoPhaseMode) {
			this.recordInterval = new IntervalWrapper(TimeUtils.fromNow());
		} else {
			//When in two-phase mode no record interval is set because we don't know it yet.
			this.recordStatus = RecordStatus.TO_BE_ADDED;
		}
		setValue(value);
		if (valueId == null) {
			this.valueId = UUID.randomUUID().toString().toUpperCase();
		} else {
			this.valueId = valueId;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQBITEMPORALWRAPPERID")
	@SequenceGenerator(name = "SEQBITEMPORALWRAPPERID", sequenceName = "MIDAS.SEQBITEMPORALWRAPPERID", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Set the wrapped value, possibly <tt>null</tt>.
	 */
	protected abstract void setValue(V value);

	/**
	 * Returns the wrapped value, possibly <tt>null</tt>.
	 */
	@Transient
	public abstract V getValue();

	/**
	 * @see com.anasoft.os.daofusion.bitemporal.Bitemporal#getValidityInterval()
	 */
	@AttributeOverrides({
		@AttributeOverride(name = "to", column = @Column(name = "validTo", columnDefinition = "TIMESTAMP")),
		@AttributeOverride(name = "from", column = @Column(name = "validFrom", columnDefinition = "TIMESTAMP")) })	
	public IntervalWrapper getValidityInterval() {
		return validityInterval;
	}
	
	/**
	 * Set the validity interval for the wrapped value.
	 */
	public void setValidityInterval(IntervalWrapper validityInterval) {
		this.validityInterval = validityInterval;
	}

	/**
	 * Set the validity interval for the wrapped value.
	 */
	public void setRecordInterval(IntervalWrapper recordInterval) {
		this.recordInterval = recordInterval;
	}

	/**
	 * @see com.anasoft.os.daofusion.bitemporal.Bitemporal#getRecordInterval()
	 */
	@AttributeOverrides({
			@AttributeOverride(name = "to", column = @Column(name = "recordTo", columnDefinition = "TIMESTAMP")),
			@AttributeOverride(name = "from", column = @Column(name = "recordFrom", columnDefinition = "TIMESTAMP")) })
	public IntervalWrapper getRecordInterval() {
		return recordInterval;
	}
	
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	@Transient
	public boolean isInProcess() {
		if (recordStatus.equals(RecordStatus.NOT_IN_PROCESS)) {
			return false;
		}
		return true;
	}
	

	/**
	 * @see com.anasoft.os.daofusion.bitemporal.Bitemporal#end()
	 */
	public void end(boolean twoPhaseMode) {
		endOnlyThis(twoPhaseMode);

		//Cascade end all fields annotated with BitemporalCascadeEnd.
		C continuity = getContinuity();
		Map<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuities = BitemporalCommons.getChildContinuities(continuity);
		for (Entry<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuiyEntry : fieldChildContinuities.entrySet()) {
			for (Continuity<?, ? extends Bitemporal> childContinuity : fieldChildContinuiyEntry.getValue()) {
				childContinuity.getBitemporalProperty().end(validityInterval, twoPhaseMode);
			}
		}
	}

	/**
	 * @see com.anasoft.os.daofusion.bitemporal.Bitemporal#endOnlyThis()
	 */
	public void endOnlyThis(boolean twoPhaseMode) {
		if (!twoPhaseMode) {
			this.recordInterval = new IntervalWrapper(TimeUtils.interval(
					getRecordInterval().getInterval().getStart(), TimeUtils.now()));
		} else {
			setRecordStatus(RecordStatus.TO_BE_ENDED);
		}
	}
	
	
	public void clearRecordStatus() {
		setRecordStatus(RecordStatus.NOT_IN_PROCESS);
	}
		
	public void commitTwoPhase() {
		if (getRecordStatus().equals(RecordStatus.TO_BE_ENDED)) {
			endOnlyThis(false);
			clearRecordStatus();
		} else if(getRecordStatus().equals(RecordStatus.TO_BE_ADDED)) {
			this.recordInterval = new IntervalWrapper(TimeUtils.fromNow());
			clearRecordStatus();
		}
	}

	@Override
	public String toString() {
		return getValidityInterval() + "  ~  " + getRecordInterval() + "  ~  "
				+ String.valueOf(getValue());
	}
	
	public abstract C getContinuity();
	
	protected abstract void setContinuity(C continuity);
	
	@Override
	public String getValueId() {
		return valueId;
	}
	
	protected void setValueId(String valueId) {
		this.valueId = valueId;
	}
	
}
