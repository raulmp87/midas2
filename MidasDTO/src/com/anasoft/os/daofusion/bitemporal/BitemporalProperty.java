/*
 * (c) Copyright Ervacon 2007.
 * All Rights Reserved.
 */

package com.anasoft.os.daofusion.bitemporal;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;

/**
 * Represents a bitemporally tracked property of a class (for instance the name of a person).
 * 
 * <p>
 * 
 * The {@link BitemporalProperty} class provides a high-level API expressed in terms of actual
 * value classes (e.g. String), layered on top of low-level constructs such as the {@link BitemporalTrace}
 * and {@link Bitemporal} objects. To be able to provide an API at the level of actual value classes,
 * the {@link BitemporalProperty} uses a {@link ValueAccessor} to extract actual values from
 * {@link Bitemporal} objects.
 * 
 * @param <V> Value wrapped by the {@link Bitemporal} object.
 * @param <T> {@link Bitemporal} object implementation that wraps the given value type.
 * 
 * @see BitemporalTrace
 * @see ValueAccessor
 * 
 * @author Erwin Vervaet
 * @author Christophe Vanfleteren
 * @author alfredo.osorio
 */
@SuppressWarnings("unchecked")
public class BitemporalProperty<V, T extends Bitemporal> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BitemporalTrace trace;
	private ValueAccessor<V, T> accessor;
	
	/**
	 * Create a new bitemporal property on top of given data collection and using
	 * the given value accessor.
	 */
	public BitemporalProperty(Collection<? extends Bitemporal> data, ValueAccessor<V, T> accessor) {
		this(new BitemporalTrace((Collection<Bitemporal>) data), accessor);
	}
	
	/**
	 * Create a new bitemporal property wrapping the given trace and using the given
	 * value accessor.
	 */
	public BitemporalProperty(BitemporalTrace trace, ValueAccessor<V, T> accessor) {
		if (trace == null) {
			throw new IllegalArgumentException("The bitemporal trace is required");
		}
		if (accessor == null) {
			throw new IllegalArgumentException("The value accessor is required");
		}
		this.trace = trace;
		this.accessor = accessor;
	}

	/**
	 * Returns the wrapped bitemporal trace.
	 */
	public BitemporalTrace getTrace() {
		return trace;
	}

	/**
	 * Returns the value valid {@link TimeUtils#now() now} as currently known.
	 */
	public V now() {
		return accessor.extractValue(get());
	}

	/**
	 * Returns the value valid on specified date as currently known.
	 */
	public V on(DateTime validOn) {
		return accessor.extractValue(get(validOn));
	}

	/**
	 * Returns the value valid on specified date as known on given date.
	 */
	public V on(DateTime validOn, DateTime knownOn) {
		return accessor.extractValue(get(validOn, knownOn));
	}
	
	/**
	 * Returns the value valid on specified date also including the records in process.
	 * This always uses <code>knwonOn</code> as currently known.
	 */
	public V nowInProcess() {
		return accessor.extractValue(getInProcess());
	}
	
	/**
	 * Returns the value valid on specified date also including the records in process.
	 * This always uses <code>knwonOn</code> as currently known.
	 */
	public V onInProcess(DateTime validOn) {
		return accessor.extractValue(getInProcess(validOn));
	}

	/**
	 * Returns the {@link Bitemporal} object valid {@link TimeUtils#now() now} as currently known. 
	 */
	public T get() {
		return get(TimeUtils.now());
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date as currently known.
	 */
	public T get(DateTime validOn) {
		return get(validOn, TimeUtils.now());
	}

	/**
	 * Returns the {@link Bitemporal} object to be ended on specified date as currently known.
	 */
	public T getToBeEnded(DateTime validOn) {
		return getToBeEnded(validOn, TimeUtils.now());
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date as known on given date.
	 */
	public T get(DateTime validOn, DateTime knownOn) {
		Collection<T> coll = (Collection<T>) trace.get(validOn, knownOn);
		if (coll.isEmpty()) {
			return null;
		} else {
			// assume single valued
			return coll.iterator().next();
		}
	}
	
	/**
     * Returns latest {@link Bitemporal} object when given date is after validity Interval and known on
     * specified date.
     */ 
	public T getLatestKnownAfterValidityInterval(DateTime validOn, DateTime knownOn) {
		Collection<T> coll = (Collection<T>) trace.getLatestKnownAfterValidityInterval(validOn, knownOn);
		if (coll.isEmpty()) {
			return null;
		} else {
			// assume single valued
			return coll.iterator().next();
		}
	}
	
	
	/**
	 * Returns the {@link Bitemporal} object to be ended on specified date as known on given date.
	 */
	public T getToBeEnded(DateTime validOn, DateTime knownOn) {
		Collection<T> coll = (Collection<T>) trace.getToBeEnded(validOn, knownOn);
		if (coll.isEmpty()) {
			return null;
		} else {
			// assume single valued
			return coll.iterator().next();
		}
	}
	
	/**
	 * Returns the {@link Bitemporal} object valid {@link TimeUtils#now() now} as currently known 
	 * including the records in process. 
	 */
	public T getInProcess() {
		return getInProcess(TimeUtils.now());
	}

	/**
	 * Returns the {@link Bitemporal} object valid on specified date also including the records in process.
	 * This always uses knwonOf as currently known.
	 */
	public T getInProcess(DateTime validOn) {
		Collection<T> coll = (Collection<T>) trace.getInProcess(validOn);
		if (coll.isEmpty()) {
			return null;
		} else {
			// assume single valued
			return coll.iterator().next();
		}
	}
	
	public T getOnlyInProcess(DateTime validOn) {
		Collection<T> coll = (Collection<T>) trace.getOnlyInProcess(validOn);
		if (coll.isEmpty()) {
			return null;
		} else {
			// assume single valued
			return coll.iterator().next();
		}
	}
	
	/**
	 * Based on twoPhaseMode it decides whether to use <code>get()</code> or <code>getInProcess()</code>
	 * @param twoPhaseMode
	 * @return
	 */
	public T getInProcessOrGet(DateTime validOn, boolean twoPhaseMode) {
		if (twoPhaseMode) {
			return getInProcess(validOn);
		}
		return get(validOn);
	}
	
	/**
	 * Returns the history of the value as currently known.
	 * This informs you about how the valid value changed, as we currently know it.
	 */
	public List<T> getHistory() {
		return getHistory(TimeUtils.now());
	}

	/**
	 * Returns the history of the value as known on given date.
	 * This informs you about how the valid value changed, as known on given date.
	 */
	public List<T> getHistory(DateTime knownOn) {
		return (List<T>) trace.getHistory(knownOn);
	}

	/**
	 * Returns the evolution of the value currently valid.
	 * This informs you about how our knowledge about the value currently valid evolved.
	 */
	public List<T> getEvolution() {
		return getEvolution(TimeUtils.now());
	}

	/**
	 * Returns the evolution of the value valid on given date.
	 * This informs you about how our knowledge about the value valid on given date evolved.
	 */
	public List<T> getEvolution(DateTime validOn) {
		return (List<T>) trace.getEvolution(validOn);
	}

	/**
	 * Returns the {@link Bitemporal} object collection contained within the trace.
	 */
	public Collection<T> getData() {
	    return (Collection<T>) trace.getData();
	}
		
	/**
	 * Set the value of this bitemporal property. The new value will be valid
	 * {@link TimeUtils#fromNow() from now on}.
	 * 
	 * @param value
	 * @param twoPhaseMode
	 * @return {@link Bitemporal} instance for given value
	 */
	public T set(V value, boolean twoPhaseMode) {
		IntervalWrapper i = new IntervalWrapper(TimeUtils.fromNow());
		return set(value, i, twoPhaseMode);
	}
	
	/**
	 * Set the value of this bitemporal property for specified validity interval.
	 * 
     * @param value
     * @param validityInterval
     * @param twoPhaseMode
     * @return {@link Bitemporal} instance for given value
	 */
	public T set(V value, IntervalWrapper validityInterval, boolean twoPhaseMode) {
		T wrapValue = accessor.wrapValue(value, validityInterval, twoPhaseMode);
        trace.add(wrapValue, twoPhaseMode);
        return wrapValue;
	}

	/**
	 * <i>Forget</i> the currently valid value.
	 */
	public void end(boolean twoPhaseMode) {
		end(TimeUtils.now(), twoPhaseMode);
	}

	/**
	 * <i>Forget</i> the valid valid on given date.
	 */
	public void end(DateTime validOn, boolean twoPhaseMode) {
		trace.end(validOn, twoPhaseMode, false);
	}
	
	
	/**
	 * <i>Forget</i> every valid value for the given interval.
	 */
	public void end(IntervalWrapper interval, boolean twoPhaseMode) {
		trace.end(interval, twoPhaseMode);
	}

	/**
	 * Returns whether or not this property has a known value currently valid.
	 */
	public boolean hasValue() {
		return hasValueOn(TimeUtils.now());
	}

	/**
	 * Returns whether or not this property has a value valid on given date.
	 */
	public boolean hasValueOn(DateTime validOn) {
		return hasValueOn(validOn, TimeUtils.now());
	}

	/**
	 * Returns whether or not this property had a value valid on given date as known on specified date.
	 */
	public boolean hasValueOn(DateTime validOn, DateTime knownOn) {
		return !trace.get(validOn, knownOn).isEmpty();
	}
	
	public boolean hasRecordsInProcess() {
		return trace.hasRecordsInProcess();
	}
	
	public void commitTwoPhase() {
		trace.commitTwoPhase();
	}
	
	public void rollbackTwoPhase() {
		trace.rollbackTwoPhase();
	}

	@Override
	public String toString() {
		return String.valueOf(now());
	}
	
	
	/**
	 * Updates the collection of bimteporals and the continuity's composition relationships to the known state at the specified time making
	 * it the current known version.
	 * @param knownOn
	 * @param twoPhaseMode
	 */
	public void updateTo(DateTime knownOn, boolean twoPhaseMode) {
		trace.updateTo(knownOn, twoPhaseMode);
	}
	
	/**
	 * Reverts the changes made at the specified <code>knownOn</code>. 
	 * @param knownOn is used to retrieve the valid bitemporals at that given point of time.
	 * @param twoPhaseMode
	 */
	public void revert(DateTime knownOn, boolean twoPhaseMode) {
		revert(knownOn, twoPhaseMode, false);
	}
	
	/**
	 * Reverts the changes made at the specified <code>knownOn</code>. If <code>extendEnd</code> is true the
	 * bitemporals that have a parent continuity and that shared the same end datetime of the parent while they
	 * were alive will be extended to its parent end time if it is the case that the parent continuity end time
	 * was extended during that time.  
	 * @param knownOn is used to retrieve the valid bitemporals at that given point of time.
	 * @param twoPhaseMode
	 * @param extendEnd
	 */
	public void revert(DateTime knownOn, boolean twoPhaseMode, boolean extendEnd) {
		trace.revert(knownOn, twoPhaseMode, extendEnd);
	}
	
	/**
	 * Obtains the current history. If includeInProcess true then it includes the changes that have been made.
	 * @param includeInProcess
	 */
	public List<T> getCurrentHistory(boolean includeInProcess) {
		return (List<T>) trace.getCurrentHistory(includeInProcess);
	}
	
	/**
	 * Obtains the current validity intervals. 
	 * This is useful to know the total validity interval regardless of the bitemporals that contains them.
	 * @param knownOn
	 * @return
	 */
	public List<IntervalWrapper> getMergedValidityIntervals() {
		return trace.getMergedValidityIntervals(TimeUtils.now());
	}
	
	/**
	 * Obtains the validity intervals for the given <code>knownOn</code>. 
	 * This is useful to know the total validity interval regardless of the bitemporals that contains them.
	 * @param knownOn
	 * @return
	 */
	public List<IntervalWrapper> getMergedValidityIntervals(DateTime knownOn) {
		return trace.getMergedValidityIntervals(knownOn);
	}
	
	/**
	 * Obtains the  current validity intervals, if <code>includeInProcess</code> is true it will take into account the records that are in process.
	 * This is useful to know the total validity interval regardless of the bitemporals that contains them.
	 * @param knownOn
	 * @return
	 */
	public List<IntervalWrapper> getCurrentMergedValidityIntervals(boolean includeInProcess) {		
		return trace.getCurrentMergedValidityIntervals(includeInProcess);
	}
}
