package com.anasoft.os.daofusion.bitemporal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.afirme.bitemporal.annotations.BitemporalCascadeEnd;

abstract class BitemporalCommons {

	
	/**
	 * Return the child continuities associated with a given object grouped by field.
	 * @param parentContinuity
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<Field, List<Continuity<?, ? extends Bitemporal>>> getChildContinuities(Object parentContinuity) {
		
		Map<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuities = new HashMap<Field, List<Continuity<?,? extends Bitemporal>>>();
		
		Field[] fieldsWithAnnotation = AnnotationUtils.getFieldsWithAnnotation(parentContinuity.getClass(), BitemporalCascadeEnd.class, true);
		for (Field field : fieldsWithAnnotation) {
			Object obj = AnnotationUtils.getFieldValue(field, parentContinuity);
			List<Continuity<?, ? extends Bitemporal>> childContinuities = new ArrayList<Continuity<?,? extends Bitemporal>>(); 
			if (!(obj instanceof Collection) && !(obj instanceof Continuity<?, ?>)) {
				throw new RuntimeException(
						"Not a collection of continuities or a continuity.");
			}
			if (obj instanceof Continuity<?, ?>) {
				if (obj != null) {
					childContinuities.add((Continuity<?, ? extends Bitemporal>) obj);
				}
			} else {
				//A collection
				Collection childContinuities2 = (Collection) obj;
				for (Object childContinuityObj : childContinuities2) {
					if (!(childContinuityObj instanceof Continuity<?, ?>)) {
						throw new RuntimeException("Not a continuity. Only continuities can be annotated with BitemporalCascadeEnd.");
					}
		    		Continuity<?, ? extends Bitemporal> childContinuity = (Continuity<?, ? extends Bitemporal>) childContinuityObj;
		    		childContinuities.add(childContinuity);
				}
			}
			fieldChildContinuities.put(field, childContinuities);
		}
		return fieldChildContinuities;
	}
}
