/*
 * (c) Copyright Ervacon 2007.
 * All Rights Reserved.
 */

package com.anasoft.os.daofusion.bitemporal;


/**
 * Represents the continuity of an object and it holds a reference to a <code>BitemporalProperty</code>.
 * @author alfredo.osorio
 */
public interface Continuity<V, T extends Bitemporal> {

	/**
	 * Returns the <code>BitemporalProperty</code> tracked by this Continuity.
	 * @param <T>
	 * @return
	 */
	public BitemporalProperty<V, T> getBitemporalProperty();
	
}
