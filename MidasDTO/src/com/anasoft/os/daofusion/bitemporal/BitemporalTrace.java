/*
 * (c) Copyright Ervacon 2007.
 * All Rights Reserved.
 */

package com.anasoft.os.daofusion.bitemporal;

import java.io.PrintWriter;  
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.afirme.bitemporal.annotations.BitemporalNotNull;

/**
 * A trace of {@link Bitemporal} objects, bitemporally tracking some value (for
 * instance the name of a person). A bitemporal trace works on top of (wraps)
 * a collection of {@link Bitemporal} objects, representing the raw data to query
 * and manipulate.
 * 
 * <p>
 * 
 * Together with {@link Bitemporal}, {@link BitemporalTrace} provides a low-level API
 * for bitemporal data tracking and manipulation expressed in terms of {@link Bitemporal}
 * objects.
 * 
 * <p>
 * 
 * A bitemporal trace will be serializable if all {@link Bitemporal} objects it contains
 * are serializable.
 * 
 * <p>
 * 
 * A bitemporal trace is not thread-safe.
 * 
 * @see Bitemporal
 * 
 * @author Erwin Vervaet
 * @author Christophe Vanfleteren
 * @author igor.mihalik
 * @author alfredo.osorio
 */
public class BitemporalTrace implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<Bitemporal> data;
	private static final Logger log = Logger.getLogger(BitemporalTrace.class);

    /**
     * Create a new bitemporal trace working on top of given data collection.
     */
    public BitemporalTrace(Collection<Bitemporal> data) {
        if (data == null) {
            throw new IllegalArgumentException("The bitemporal data is required");
        }
        this.data = data;
    }

    /**
     * Returns the wrapped data collection.
     */
    public Collection<Bitemporal> getData() {
        return this.data;
    }

    /**
     * Returns {@link Bitemporal} objects valid on given date as known on
     * specified date.
     */
    public Collection<Bitemporal> get(DateTime validOn, DateTime knownOn) {
        Collection<Bitemporal> result = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) 
            		&& bitemporal.getValidityInterval().getInterval().contains(validOn)
                    && bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
                result.add(bitemporal);
            }
        }
        return result;
    }
    
    /**
     * Returns latest {@link Bitemporal} object when given date is after validity Interval and known on
     * specified date.
     */  
    public Collection<Bitemporal> getLatestKnownAfterValidityInterval(DateTime validOn, DateTime knownOn) {
        Collection<Bitemporal> result = new LinkedList<Bitemporal>();
        
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) 
            		&& (validOn.compareTo(bitemporal.getValidityInterval().getInterval().getStart()) >= 0)
            		&& (validOn.compareTo(bitemporal.getValidityInterval().getInterval().getEnd())>= 0)
                    && bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
            	
            	result.add(bitemporal);                     
            }
        }
        
        return result;
    }
    
    /**
     * Returns {@link Bitemporal} objects to be ended on given date as known on
     * specified date.
     */
    public Collection<Bitemporal> getToBeEnded(DateTime validOn, DateTime knownOn) {
        Collection<Bitemporal> result = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ENDED) 
            		&& bitemporal.getValidityInterval().getInterval().contains(validOn)
                    && bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
                result.add(bitemporal);                
            }
        }
        return result;
    }
    
    
    
    /**
     * Returns {@link Bitemporal} objects valid on given date also including the records in process.
     * specified date.
     * This always uses <code>knwonOn</code> as currently known.
     */
    public Collection<Bitemporal> getInProcess(DateTime validOn) {
        Collection<Bitemporal> result = new LinkedList<Bitemporal>();
        //knownOn is always now for this case.
        final DateTime knownOn = TimeUtils.now();
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.isInProcess() && bitemporal.getValidityInterval().getInterval().contains(validOn)
                    && bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
                result.add(bitemporal);
            }else if (bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) 
        			&& bitemporal.getValidityInterval().getInterval().contains(validOn)) {
                result.add(bitemporal);        		
        	}
        }
        return result;
    }
    
    public Collection<Bitemporal> getOnlyInProcess(DateTime validOn) {
        Collection<Bitemporal> result = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) 
        			&& bitemporal.getValidityInterval().getInterval().contains(validOn)) {
                result.add(bitemporal);        		
        	}
        }
        return result;
    }
    
    /**
     * Returns the history of the tracked value, as known on specified time.
     * The history informs you about how the valid value changed over time.
     */
    public Collection<Bitemporal> getHistory(DateTime knownOn) {
        Collection<Bitemporal> history = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) 
            		&& bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
                history.add(bitemporal);
            }
        }
        return history;
    }
    
    public Collection<Bitemporal> getCurrentHistory(boolean includeInProcess) {
    	if (includeInProcess) {
    		return getCurrentHistoryIncludeInProcess();
    	} else {
        	return getCurrentHistory();
    	}
    }
    
    public Collection<Bitemporal> getCurrentHistory() {
        return getHistory(TimeUtils.now());
    }
    
    public Collection<Bitemporal> getCurrentHistoryIncludeInProcess() {
    	Collection<Bitemporal> history = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ENDED) 
            		&& (bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) || 
            				bitemporal.getRecordInterval().getInterval().contains(TimeUtils.now()))) {
                history.add(bitemporal);
            }
        }
        return history;
    }

    /**
     * Returns the evolution of the tracked value for a specified validity date.
     * The evolution informs you about how knowledge about the value valid at a
     * certain date evolved.
     */
    public Collection<Bitemporal> getEvolution(DateTime validOn) {
        Collection<Bitemporal> evolution = new LinkedList<Bitemporal>();
        for (Bitemporal bitemporal : data) {
            if (!bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED) && 
            		bitemporal.getValidityInterval().getInterval().contains(validOn)) {
                evolution.add(bitemporal);
            }
        }
        return evolution;
    }
    
    
    /**
     * Adds the given {@link Bitemporal} object to the trace, manipulating the trace
     * as necessary. This is essentially the basic bitemporal data manipulation
     * operation.
     */
    public void add(Bitemporal newValue, boolean twoPhaseMode) {
    	add(newValue, twoPhaseMode, true);    	
    }   

    private void add(Bitemporal newValue, boolean twoPhaseMode, boolean validateBitemporalPropertiesNotNull) {
    	if (validateBitemporalPropertiesNotNull) {
    		validateBitemporalPropertiesNotNull(newValue, twoPhaseMode);
    	}
    	
        Collection<Bitemporal> toEnd = getItemsThatNeedToBeEnded(newValue.getValidityInterval(), twoPhaseMode);

        Collection<Bitemporal> toAdd = new LinkedList<Bitemporal>();
        DateTime validityStartOfNewValue = newValue.getValidityInterval().getInterval().getStart();
        for (Bitemporal validOnStartOfNewValue : getInProcessOrGet(validityStartOfNewValue, twoPhaseMode)) {
            if (validityStartOfNewValue.compareTo(validOnStartOfNewValue.getValidityInterval().getInterval().getStart()) > 0) {
				IntervalWrapper validityInterval = 
					new IntervalWrapper(TimeUtils.interval(
						validOnStartOfNewValue.getValidityInterval()
						.getInterval().getStart(),validityStartOfNewValue));
                toAdd.add(validOnStartOfNewValue.copyWith(validityInterval, twoPhaseMode));
            }
        }

        if (!(newValue.getValidityInterval().getInterval().getEnd().getMillis() == TimeUtils.ACTUAL_END_OF_TIME)) {
            DateTime validityEndOfNewValue = newValue.getValidityInterval().getInterval().getEnd();
            for (Bitemporal validOnEndOfNewValue : getInProcessOrGet(validityEndOfNewValue, twoPhaseMode)) {
                if (validityEndOfNewValue.compareTo(validOnEndOfNewValue.getValidityInterval().getInterval().getStart()) > 0) {
    				IntervalWrapper validityInterval = 
    					new IntervalWrapper(TimeUtils.interval(validityEndOfNewValue,
    							validOnEndOfNewValue.getValidityInterval().getInterval().getEnd()));
                    toAdd.add(validOnEndOfNewValue.copyWith(validityInterval, twoPhaseMode));
                }
            }
        }

        end(toEnd, twoPhaseMode, true);
                
        for (Bitemporal toBeAdded : toAdd) {
    		data.add(toBeAdded);
        }
        data.add(newValue);        
    }
    
    public void end(DateTime validOn, boolean twoPhaseMode, boolean onlyThis) {
    	end(getInProcessOrGet(validOn, twoPhaseMode), twoPhaseMode, onlyThis);
    }
    
    /**
	 * Based in twoPhaseMode it decides whether to use <code>get()</code> or <code>getInProcess()</code>
	 * @param twoPhaseMode
	 * @return
	 */
	public Collection<Bitemporal> getInProcessOrGet(DateTime validOn, boolean twoPhaseMode) {
		if (twoPhaseMode) {
			return getInProcess(validOn);
		}
		return get(validOn, TimeUtils.now());
	}

    
    public void end(Collection<Bitemporal> bitemporals, boolean twoPhaseMode, boolean onlyThis) {
    	for (Bitemporal bitemporal : bitemporals) {
    		//The records with status TO_BE_ADDED are removed from the list. This is the only case when it is allowed to
			//remove data from the list and that's because it was a record in process and the operation could be as if nothing had happened.
    		if (bitemporal.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)) {
    			data.remove(bitemporal);
    		}
    		
			if (onlyThis) {
				bitemporal.endOnlyThis(twoPhaseMode);
			} else {
				bitemporal.end(twoPhaseMode);
			}
		}
    }
    
    public void end(IntervalWrapper validityInterval, boolean twoPhaseMode) {
    	end(validityInterval, null, twoPhaseMode, true);
    }
    
    public void end(Bitemporal bitemporal, boolean twoPhaseMode, boolean cascadeEnd) {
    	end(null, bitemporal, twoPhaseMode, cascadeEnd);
    }
    
    private void end(IntervalWrapper validityInterval, Bitemporal bitemporal, boolean twoPhaseMode, boolean cascadeEnd) {
    	//Obtain the interval from the bitemporal in case it is not null.
		if (bitemporal != null) {
			validityInterval = bitemporal.getValidityInterval();
		}
    	Collection<Bitemporal> toEnd = getItemsThatNeedToBeEnded(validityInterval, bitemporal, twoPhaseMode);
    	Collection<Bitemporal> toAdd = new LinkedList<Bitemporal>();
    	for (Bitemporal b : toEnd) {    		
    		if (validityInterval.getInterval().getStart().compareTo(b.getValidityInterval().getInterval().getStart()) > 0) {
    			IntervalWrapper newValidityInterval = 
					new IntervalWrapper(TimeUtils.interval(
						b.getValidityInterval()
						.getInterval().getStart(),validityInterval.getInterval().getStart()));
    			toAdd.add(b.copyWith(newValidityInterval, twoPhaseMode));
    		}
    		
    		if (!(validityInterval.getInterval().getEnd().getMillis() == TimeUtils.ACTUAL_END_OF_TIME)) {
    			if (validityInterval.getInterval().getEnd().compareTo(b.getValidityInterval().getInterval().getEnd()) < 0) {
        			IntervalWrapper newValidityInterval = 
    					new IntervalWrapper(TimeUtils.interval(
    						validityInterval.getInterval().getEnd(),b.getValidityInterval().getInterval().getEnd()));
        			toAdd.add(b.copyWith(newValidityInterval, twoPhaseMode));
        		}
    		}
    		
    	}
    	
		for (Bitemporal b : toAdd) {
    		add(b, twoPhaseMode);
        }
		
		//Refresh toEnd with the new items that were added.
		toEnd = getItemsThatNeedToBeEnded(validityInterval, bitemporal, twoPhaseMode);
		end(toEnd, twoPhaseMode, !cascadeEnd);
    }
    	
	private Map<Field, Continuity<?, ? extends Bitemporal>> getBitemporalProperties(Bitemporal newValue) {
    	//For the momment we only inspect fields of BitemporalWrapper objects.
    	if (!(newValue instanceof BitemporalWrapper<?, ?>)) {
    		return new HashMap<Field, Continuity<?,? extends Bitemporal>>();
    	}
    	BitemporalWrapper<?, ?> bitemporalWrapper = (BitemporalWrapper<?, ?>) newValue;
    	Object childContinuity = bitemporalWrapper.getContinuity();
    	return getBitemporalProperties(childContinuity);
	}
    
    @SuppressWarnings("unchecked")
	private Map<Field, Continuity<?, ? extends Bitemporal>> getBitemporalProperties(Object childContinuity) {
    	Map<Field, Continuity<?, ? extends Bitemporal>> properties = new HashMap<Field, Continuity<?,? extends Bitemporal>>(); 
    	for (Field field : AnnotationUtils.getFieldsWithAnnotation(childContinuity.getClass(), BitemporalNotNull.class, true)) {
    		Continuity<?, ? extends Bitemporal> parentContinuity;
			try {
				Object obj = field.get(childContinuity);
	    		if (!(obj instanceof Continuity<?, ?>)) {
	    			throw new RuntimeException("Not a continuity. BitemporalNotNull annotation must be placed in a field that is a Continuity.");
	    		}
				parentContinuity = (Continuity<?, ? extends Bitemporal>) obj;
				//Null continuity objects are not allowed so we throw an exception.
				if (parentContinuity == null) {
					throw new IllegalArgumentException(field.getName() + " continuity is null in an object of class " + childContinuity.getClass());
				}
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			properties.put(field, parentContinuity);
    	}
    	return properties;
	}

    /**
     * Validates the Bitemporal validity period for every property annotated with <code>BitemporalNotNull</code>.
     */
	private void validateBitemporalPropertiesNotNull(Bitemporal newValue, boolean twoPhaseMode) {
    	for (Map.Entry<Field, Continuity<?, ? extends Bitemporal>> entry : getBitemporalProperties(newValue).entrySet()) {
    		validateBitemporalPropertyNotNull(newValue, entry.getKey().getName(), entry.getValue(), twoPhaseMode);
    	}
	}
    
	private void validateBitemporalPropertyNotNull(Bitemporal newValue, String continuityFieldName, Continuity<?, ? extends Bitemporal> parentContinuity, boolean twoPhaseMode) {
    	final String message = "No bitemporal object found at the specified interval " + newValue.getValidityInterval() + " for field " + continuityFieldName + " of class " + newValue.getClass();
    	Interval newValueValidity = newValue.getValidityInterval().getInterval();
		Bitemporal bitemporalAtStart = parentContinuity.getBitemporalProperty().getInProcessOrGet(newValueValidity.getStart(), twoPhaseMode);
		if (bitemporalAtStart == null) {
			throw new IllegalArgumentException(message);
		}
		if (bitemporalAtStart.getValidityInterval().getInterval().getEnd().compareTo(newValueValidity.getEnd()) >= 0) {
			return;
		}
		
		Bitemporal bitemporalAtEnd = parentContinuity
				.getBitemporalProperty()
				.getInProcessOrGet(
						new DateTime(newValueValidity.getEnd().getMillis() - 1),
						twoPhaseMode);
		if (bitemporalAtEnd == null) {
			throw new IllegalArgumentException(message);
		}
		
		//there is no gap between them.
		if (bitemporalAtStart.getValidityInterval().getInterval().gap(bitemporalAtEnd.getValidityInterval().getInterval()) == null) {
			return;
		}
		
		Bitemporal nextBitemporal = bitemporalAtStart;
		do {
    		nextBitemporal = parentContinuity.getBitemporalProperty().getInProcessOrGet(nextBitemporal.getValidityInterval().getInterval().getEnd(), twoPhaseMode); 		
		}while(nextBitemporal != null && !nextBitemporal.equals(bitemporalAtEnd));
		
		//if nextBitemporal is null it means that there was a gap before the cycle could reach the bitemporalAtEnd.
		if (nextBitemporal == null) {
			throw new IllegalArgumentException(message);
		}
		
		return;
    }
    
	
	private Collection<Bitemporal> getItemsThatNeedToBeEnded(IntervalWrapper validityInterval, boolean twoPhaseMode) {
        return getItemsThatNeedToBeEnded(validityInterval, null, twoPhaseMode);
    }
	
	/**
	 * Obtains the bitemporal that must be ended either by the validityInterval or by the bitemporal, only one of them must be provided.
	 * @param validityInterval the validity interval
	 * @param bitemporal serves to filter only the bitemporals that match the bitemporal by its value and that its validity interval overlaps
	 * with the existing ones.
	 * @param twoPhaseMode
	 * @return
	 */
	private Collection<Bitemporal> getItemsThatNeedToBeEnded(IntervalWrapper validityInterval, Bitemporal bitemporal, boolean twoPhaseMode) {
		boolean filterBitemporal = false;
		if (bitemporal != null) {
			validityInterval = bitemporal.getValidityInterval();
			filterBitemporal = true;
		}
        Collection<Bitemporal> toEnd = new HashSet<Bitemporal>();
        for (Bitemporal possibleOverlap : getCurrentHistory(twoPhaseMode)) {
            if (validityInterval.getInterval().overlaps(possibleOverlap.getValidityInterval().getInterval())) {
            	if (filterBitemporal) {
            		//Must be equal
            		if (isSameValue(bitemporal, possibleOverlap)) {
            			toEnd.add(possibleOverlap);
            		}
            		
            	} else {
                    toEnd.add(possibleOverlap);
            	}
            }
        }
        return toEnd;
    }

    
    public void rollbackTwoPhase() {
    	for (Bitemporal b : data) {
    		if (b.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)) {
    			data.remove(b);
    		} else if(b.getRecordStatus().equals(RecordStatus.TO_BE_ENDED)) {
    			b.clearRecordStatus();
    		}
    	}
    }
    
    public void commitTwoPhase() {
    	for (Bitemporal b : data) {
    		b.commitTwoPhase();
    	}
    }
    
    public boolean hasRecordsInProcess() {
    	for (Bitemporal b : data) {
    		if (b.isInProcess()) {
    			return true;
    		}
    	}
    	return false;
    }

    @Override
    public String toString() {
        StringWriter buf = new StringWriter();
        PrintWriter bufWriter = new PrintWriter(buf);
        for (Bitemporal bitemporal : data) {
            bufWriter.println(bitemporal);
        }
        return buf.toString();
    }

	public void updateTo(DateTime knownOn, boolean twoPhaseMode) {
		rollbackTwoPhase();
		
		Collection<Bitemporal> toEnd = new LinkedList<Bitemporal>();
		Collection<Bitemporal> toAdd = new LinkedList<Bitemporal>();
		for (Bitemporal bitemporal : data) {
			//Terminar los registros que son vigentes y no se encuentren dentro del tiempo indicado (knownOn).
			if (bitemporal.getRecordInterval().getInterval().getEnd().getMillis() == TimeUtils.ACTUAL_END_OF_TIME) {
				if (!bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
					toEnd.add(bitemporal);
				}
			} else {
				//De los registros que no son vigentes tenemos que clonar solamente aquellos que contienen el tiempo dado.
				if (bitemporal.getRecordInterval().getInterval().contains(knownOn)) {
	                toAdd.add(bitemporal.copyWith(bitemporal.getValidityInterval(), twoPhaseMode)); 
				}
			}
		}
		
		end(toEnd, twoPhaseMode, true);
		for (Bitemporal bitemporal : toAdd) {
			data.add(bitemporal);
		}
		
		//Cascade updateTo to all child continuities, these are the ones that are marked with BitemporalCascadeEnd.
		Continuity<?, ? extends Bitemporal> continuity = getContinuity();
		if (continuity == null) {
			return;
		}
		Map<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuities = BitemporalCommons.getChildContinuities(continuity);
		for (Entry<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuiyEntry : fieldChildContinuities.entrySet()) {
			for (Continuity<?, ? extends Bitemporal> childContinuity : fieldChildContinuiyEntry.getValue()) {
				childContinuity.getBitemporalProperty().updateTo(knownOn, twoPhaseMode);
			}
		}
	}

	/**
	 * Obtains the continuity associated with this trace. When there is no data there is no way to determine the continuity so this
	 * method can return null.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Continuity<?, ? extends Bitemporal> getContinuity() {
		int i = 1;
		Bitemporal firstBitemporal = null;
		for (Bitemporal bitemporal : data) {
			if (i == 1){
				firstBitemporal = bitemporal;
				break;
			}
		}
		if (firstBitemporal == null) {
			return null;
		}
		BitemporalWrapper<?, ?> bitemporalWrapper = (BitemporalWrapper<?, ?>) firstBitemporal;
		return 	(Continuity<?, ? extends Bitemporal>) bitemporalWrapper.getContinuity();
	}

	@SuppressWarnings("unchecked")
	public void revert(DateTime knownOn, boolean twoPhaseMode, boolean extendEnd) {
		if (data.size() == 0) {
			log.debug("No data to revert.");
			return;
		}
		
		Collection<Bitemporal> historyAtKnownOn = getHistory(knownOn);
		Collection<Bitemporal> historyBeforeKnownOn = getHistory(new DateTime(knownOn.getMillis() - 1));
		BitemporalDifferences bitemporalDifferences = getBitemporalDifferences(historyBeforeKnownOn, historyAtKnownOn, twoPhaseMode);

		//Remove what was added with cascade true and that corresponds to the specified bitemporal.
		for (Bitemporal bitemporal : bitemporalDifferences.getAdded()) {
			end(bitemporal, twoPhaseMode, true);
		}
		
		//Remove what was modified with cascade false
		for (Bitemporal bitemporal : bitemporalDifferences.getModified()) {
			end(bitemporal, twoPhaseMode, false);
		}
		
		//Add what was removed (only fill blank spaces).
		Continuity<?, ? extends Bitemporal> continuity = getContinuity();
		Continuity<?, ? extends Bitemporal> parentContinuity = getParentContinuity();
		
		//We need to obtain what overlaps for a given bitemporal in the current history of that continuity.
		Collection<Bitemporal> bitemporalsToAdd = null;
		if (parentContinuity != null) {
			Collection<Bitemporal> removedBitemporals = bitemporalDifferences.getRemoved();
			if (extendEnd) {
				LinkedList<Bitemporal> orderedRemovedBitemporals = orderByValidityStart(bitemporalDifferences.getRemoved());
				Bitemporal lastRemovedBitemporal = orderedRemovedBitemporals.getLast();
				LinkedList<Bitemporal> orderedParentHistoryBeforeKnownOn = orderByValidityStart(parentContinuity.getBitemporalProperty().getHistory(new DateTime(knownOn.getMillis() - 1)));
				Bitemporal lastParentBitemporal = orderedParentHistoryBeforeKnownOn.getLast();
				Bitemporal newLastRemovedBitemporal = null;
				if (lastParentBitemporal.getValidityInterval().getInterval().getEnd().equals(lastRemovedBitemporal.getValidityInterval().getInterval().getEnd())) {
					//Check parent current history
					List<IntervalWrapper> parentCurrentMergedValidityIntervals = parentContinuity.getBitemporalProperty().getCurrentMergedValidityIntervals(twoPhaseMode);
					if (parentCurrentMergedValidityIntervals.size() > 0) {
						IntervalWrapper lastParentCurrentMergedValidityInterval = parentCurrentMergedValidityIntervals.get(parentCurrentMergedValidityIntervals.size() - 1);
						if (lastParentBitemporal.getValidityInterval().getInterval().getEnd().compareTo(lastParentCurrentMergedValidityInterval.getInterval().getEnd()) < 0) {
							//the interval at the end has changed in the parent continuity so we should extend the validity interval of the bitemporal that was removed
							//but first we have to check if there are not overlaps.
							IntervalWrapper extendedIntervalWrapper = new IntervalWrapper(new Interval(lastParentBitemporal.getValidityInterval().getInterval().getEnd(), lastParentCurrentMergedValidityInterval.getInterval().getEnd()));
							if (!overlaps(extendedIntervalWrapper, getCurrentHistory(twoPhaseMode))) {
								IntervalWrapper newLastRemovedBitemporalInterval = new IntervalWrapper(new Interval(lastRemovedBitemporal.getValidityInterval().getInterval().getStart(), extendedIntervalWrapper.getInterval().getEnd()));
								newLastRemovedBitemporal = lastRemovedBitemporal.copyWith(newLastRemovedBitemporalInterval, twoPhaseMode);
							}
						}
					}
				}
				if (newLastRemovedBitemporal != null) {
					removedBitemporals = new LinkedList<Bitemporal>(removedBitemporals);
					removedBitemporals.remove(lastRemovedBitemporal);
					removedBitemporals.add(newLastRemovedBitemporal);
				}
			}

			bitemporalsToAdd = new LinkedList<Bitemporal>();
			Collection<Bitemporal> currentHistory = (Collection<Bitemporal>) parentContinuity
					.getBitemporalProperty().getCurrentHistory(twoPhaseMode);
			for (Bitemporal removedBitemporal : removedBitemporals) {
				bitemporalsToAdd.addAll(getOverlappedBitemporals(removedBitemporal, currentHistory, twoPhaseMode));
			}
		} else {
			bitemporalsToAdd = bitemporalDifferences.getRemoved();
		}
		
		for (Bitemporal bitemporal : bitemporalsToAdd) {
			add2(bitemporal, twoPhaseMode, false);
		}
		
		//Cascade revert to all child continuities, these are the ones that are marked with BitemporalCascadeEnd.	
		Map<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuities = BitemporalCommons.getChildContinuities(continuity);
		for (Entry<Field, List<Continuity<?, ? extends Bitemporal>>> fieldChildContinuiyEntry : fieldChildContinuities.entrySet()) {
			for (Continuity<?, ? extends Bitemporal> childContinuity : fieldChildContinuiyEntry.getValue()) {
				childContinuity.getBitemporalProperty().revert(knownOn, twoPhaseMode, extendEnd);
			}
		}
	}
	
	private boolean overlaps(IntervalWrapper validityInterval, Collection<Bitemporal> bitemporals) {
		for (Bitemporal bitemporal : bitemporals) {
			if (bitemporal.getValidityInterval().getInterval().overlaps(validityInterval.getInterval())) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Using the given <code>bitemporal</code> obtains only the parts that overlap with the <code>baseBitemporals</code>.
	 * @param baseBitemporals
	 * @param twoPhaseMode
	 * @return
	 */
	private Collection<Bitemporal> getOverlappedBitemporals(Bitemporal bitemporal, Collection<Bitemporal> baseBitemporals, boolean twoPhaseMode) {
		Collection<Bitemporal> overlappedBitemporals = new LinkedList<Bitemporal>();
		List<IntervalWrapper> mergedValidityIntervals = getMergedValidityIntervals(baseBitemporals);
		for (IntervalWrapper validityInterval : mergedValidityIntervals) {			
			Interval overlap = bitemporal.getValidityInterval().getInterval().overlap(validityInterval.getInterval());
			if (overlap != null) {
				overlappedBitemporals.add(bitemporal.copyWith(
						new IntervalWrapper(overlap), twoPhaseMode));
			}
		}
		return overlappedBitemporals;
	}
	
	public List<IntervalWrapper> getMergedValidityIntervals(DateTime knownOn) {
		return getMergedValidityIntervals(getHistory(knownOn));
	}
	
	public List<IntervalWrapper> getCurrentMergedValidityIntervals(boolean includeInProcess) {
		return getMergedValidityIntervals(getCurrentHistory(includeInProcess));
	}
	
	/**
	 * Obtains the validity intervals from the given <code>bitemporals</code>. This is useful to know the total validity interval regardless
	 * of the bitemporals that contains them.
	 * @param baseBitemporals
	 * @return
	 */
	private List<IntervalWrapper> getMergedValidityIntervals(Collection<Bitemporal> baseBitemporals) {
		List<IntervalWrapper> mergedValidityIntervals = new LinkedList<IntervalWrapper>();
		List<LinkedList<Bitemporal>> groups = getBitemporalGroupsByValidityInterval(null, baseBitemporals);
		for (LinkedList<Bitemporal> group : groups) {
			IntervalWrapper mergedValidityInterval = null;
			if (group.size() > 1) {
				Bitemporal first = group.getFirst();
				Bitemporal last = group.getLast();
				mergedValidityInterval = new IntervalWrapper(new Interval(first
						.getValidityInterval().getInterval().getStart(), last
						.getValidityInterval().getInterval().getEnd()));
			} else {
				mergedValidityInterval = group.getFirst().getValidityInterval();
			}
			mergedValidityIntervals.add(mergedValidityInterval);
		}
		return mergedValidityIntervals;
	}
	
	/**
	 * This only fills the spaces of the trace that are not occupied by other bitemporals.
	 * @param newValue
	 * @param twoPhase
	 */
	private void add2(Bitemporal newValue, boolean twoPhaseMode, boolean validateBitemporalPropertiesNotNull) {
		Collection<Bitemporal> currentHistory = getCurrentHistory(twoPhaseMode);
		Collection<Bitemporal> notOverlappedBitemporals = getNotOverlappedBitemporals(newValue, currentHistory, twoPhaseMode);
		for (Bitemporal notOverlappedBitemporal : notOverlappedBitemporals) {
			if (validateBitemporalPropertiesNotNull) {
				validateBitemporalPropertiesNotNull(newValue, twoPhaseMode);
			}
			data.add(notOverlappedBitemporal);
		}
		Collection<Bitemporal> mergedBitemporals = mergeBitemporals(newValue, twoPhaseMode);
		
		for (Bitemporal mergedBitemporal : mergedBitemporals) {
			//Remove from the data the bitemporals that were added and are included in the bitemporals that were merged. This wouldn't be necessary if
			//the end could finish the bitemporals with zero duration record interval.
			for (Bitemporal notOverlappedBitemporal : notOverlappedBitemporals) {
				boolean containsInterval = mergedBitemporal.getValidityInterval().getInterval().contains(
						notOverlappedBitemporal.getValidityInterval()
								.getInterval());
				if (containsInterval) {
					data.remove(notOverlappedBitemporal);
				}
			}
			//Add the mergedBitemporal. No need to validate the bitemporals because it is supposed that the existing bitemporals must be
			//already valid.
			add(mergedBitemporal, twoPhaseMode, false);
		}
	}
	
	/**
	 * Merges bitemporals that corresponds only with the value wrapped by newValue. 
	 * In other words it ignores everything else that is not related with newValue.
	 * @param newValue 
	 * @return a collection of the bitemporals that were merged in the process. 
	 * If no bitemporals were merged in the process an empty collection is returned.
	 */
	private Collection<Bitemporal> mergeBitemporals(Bitemporal newValue, boolean twoPhaseMode) {
		Collection<Bitemporal> mergedBitemporals = new LinkedList<Bitemporal>();
		List<LinkedList<Bitemporal>> groups = getBitemporalGroupsByValidityInterval(newValue, getCurrentHistory(twoPhaseMode));
		
		for (LinkedList<Bitemporal> group : groups) {
			if (group.size() > 1) {
				Bitemporal first = group.getFirst();
				Bitemporal last = group.getLast();
				Bitemporal mergedBitemporal = first.copyWith(new IntervalWrapper(new Interval(first
						.getValidityInterval().getInterval().getStart(), last
						.getValidityInterval().getInterval().getEnd())),
						twoPhaseMode);
				mergedBitemporals.add(mergedBitemporal);
			}
		}
		
		return mergedBitemporals;
	}
	
	/**
	 * Obtains groups of bitemporals where each group can contain more than one Bitemporal which is the case when a bitemporal
	 * abuts with another by its validity interval. 
	 * @param bitemporal (optional) bitemporal to consider to form the groups, in the case that it is provided then only groups
	 * of that bitemporal will be formed.
	 * @param baseBitemporals
	 * @return
	 */
	private List<LinkedList<Bitemporal>> getBitemporalGroupsByValidityInterval(Bitemporal bitemporal, Collection<Bitemporal> baseBitemporals) {
		Collection<Bitemporal> orderedCurrentHistory = orderByValidityStart(baseBitemporals);
		List<LinkedList<Bitemporal>> groups = new ArrayList<LinkedList<Bitemporal>>();
		
		Bitemporal lastBitemporalFound = null;
		LinkedList<Bitemporal> currentGroup = null;
		for (Bitemporal b : orderedCurrentHistory) {
			if (bitemporal == null || isSameValue(b, bitemporal)) {
				if (lastBitemporalFound == null
						|| !lastBitemporalFound.getValidityInterval()
								.getInterval().abuts(
										b.getValidityInterval()
												.getInterval())) {
					currentGroup = new LinkedList<Bitemporal>();
					groups.add(currentGroup);
				}
				lastBitemporalFound = b;
				currentGroup.add(lastBitemporalFound);
			}
		}

		return groups;
	}

	/**
	 * Checks if two bitemporals are equal by comparing the value they wrap.
	 * @param bitemporal1
	 * @param bitemporal2
	 * @return
	 */
	private boolean isSameValue(Bitemporal bitemporal1, Bitemporal bitemporal2) {
		return bitemporal1.getValueId().equals(bitemporal2.getValueId());
	}
	
	/**
	 * Obtains the value that is being wrapped by the Bitemporal.
	 * @param bitemporal
	 * @return
	 */
	private Object getValue(Bitemporal bitemporal) {
		if (!(bitemporal instanceof BitemporalWrapper<?, ?>)) {
			throw new IllegalArgumentException(
					"Not a BitemporalWrapper. Currently the only allowed Bitemporal implementation is BitemporalWrapper.");
		}
		BitemporalWrapper<?, ?> bitemporalWrapper = (BitemporalWrapper<?, ?>) bitemporal;
		return bitemporalWrapper.getValue();
	}

	private BitemporalDifferences getBitemporalDifferences(Collection<Bitemporal> left,
			Collection<Bitemporal> right, boolean twoPhaseMode) {
		BitemporalDifferences bitemporalDifferences = new BitemporalDifferences();
		
		//Check what bitemporals were removed and added.
		Collection<Bitemporal> bAddedList = new LinkedList<Bitemporal>();
		Collection<Bitemporal> bRemovedList = new LinkedList<Bitemporal>();
		for (Bitemporal bitemporal : left) {
			if (!right.contains(bitemporal)) {
				bRemovedList.add(bitemporal);
			}
		}
		for (Bitemporal bitemporal : right) {
			if (!left.contains(bitemporal)) {
				bAddedList.add(bitemporal);				
			}
		}
		
		//With this bitemporals obtain what was the total result. What was changed?
		//Check if the bitemporals that were removed are included or part of them are in the bitemporals that were added. 
		//This way we can obtain the exact period(s) that were removed.
		for (Bitemporal bRemoved : bRemovedList) {
			Collection<Bitemporal> bitemporalsThatOverlapWithSameValue = new LinkedList<Bitemporal>();
			for (Bitemporal bAdded : bAddedList) {
				Interval overlap = bRemoved.getValidityInterval().getInterval().overlap(
						bAdded.getValidityInterval().getInterval());
				if (overlap != null) {
					if (isSameValue(bRemoved, bAdded)) {
						bitemporalsThatOverlapWithSameValue.add(bAdded);
					} else {
						bitemporalDifferences.getModified().add(
								bAdded.copyWith(new IntervalWrapper(overlap),
										twoPhaseMode));
					}
				}
				
			}
			bitemporalDifferences.getRemoved().addAll(
					getNotOverlappedBitemporals(bRemoved,
							bitemporalsThatOverlapWithSameValue, twoPhaseMode));
		}
		//Obtain the portion that was really addded.
		for (Bitemporal bAdded : bAddedList) {
			bitemporalDifferences.getAdded().addAll(
					getNotOverlappedBitemporals(bAdded, bRemovedList,
							twoPhaseMode));
		}
		
		return bitemporalDifferences;
	}
	
	/**
	 * Obtains the parts of the <code>bitemporal</code> that does not overlap with the existing <code>baseBitemporals</code>.
	 * @param bitemporal
	 * @param baseBitemporals
	 * @return a collection of the Bitemporals that does not overlap with the existing ones. 
	 * It can return a collection with only the same instance of the <code>bitemporal</code> which is the case when there are no overlaps or an empty
	 * collection if the bitemporal totally overlaps.
	 */
	private Collection<Bitemporal> getNotOverlappedBitemporals(Bitemporal bitemporal, Collection<Bitemporal> baseBitemporals, boolean twoPhaseMode) {
		Collection<Bitemporal> notOverlappedBitemporals = new LinkedList<Bitemporal>();
		Collection<Bitemporal> baseBitemporalsOrdered = orderByValidityStart(baseBitemporals);
		Collection<Bitemporal> overlappedBitemporals = new LinkedList<Bitemporal>();
		for (Bitemporal baseBitemporal : baseBitemporalsOrdered) {
			Interval overlap = bitemporal.getValidityInterval().getInterval().overlap(baseBitemporal.getValidityInterval().getInterval());
			if (overlap != null) {
				Bitemporal overlappedBitemporal = baseBitemporal.copyWith(new IntervalWrapper(overlap), twoPhaseMode);
				overlappedBitemporals.add(overlappedBitemporal);
			}
		}

		if (overlappedBitemporals.size() == 0) {
			//No overlaps so returning collection with the a copy of the provided bitemporal.
			notOverlappedBitemporals.add(bitemporal.copyWith(bitemporal.getValidityInterval(), twoPhaseMode));
		} else {			
			Interval previousInterval = new Interval(bitemporal
					.getValidityInterval().getInterval().getStart(), bitemporal
					.getValidityInterval().getInterval().getStart());
			Interval lastInterval = new Interval(bitemporal
					.getValidityInterval().getInterval().getEnd(), bitemporal
					.getValidityInterval().getInterval().getEnd());
			int i = 1;
			for (Bitemporal overlappedBitemporal : overlappedBitemporals) {
				//This happens when the overlappedBitemporal overlaps the whole interval of the bitemporal.
				if (overlappedBitemporal.getValidityInterval().getInterval()
						.equals(bitemporal.getValidityInterval().getInterval())) {
					break;
				}
								
				Interval gap = overlappedBitemporal.getValidityInterval().getInterval().gap(previousInterval);
				if (gap != null) {
					notOverlappedBitemporals.add(bitemporal.copyWith(new IntervalWrapper(gap), twoPhaseMode));
				}
				
				if (i == overlappedBitemporals.size()
						&& !overlappedBitemporal.getValidityInterval()
								.getInterval().getEnd().equals(bitemporal
								.getValidityInterval().getInterval().getEnd())) {
					IntervalWrapper gapAtTheEnd = new IntervalWrapper(overlappedBitemporal.getValidityInterval().getInterval().gap(lastInterval));
					notOverlappedBitemporals.add(bitemporal.copyWith(gapAtTheEnd, twoPhaseMode));
				}
				
				previousInterval = overlappedBitemporal.getValidityInterval().getInterval();
				i++;
			}
		}
		return notOverlappedBitemporals;
	}
	
	private class BitemporalDifferences implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private Collection<Bitemporal> added = new LinkedList<Bitemporal>();
		private Collection<Bitemporal> removed = new LinkedList<Bitemporal>();
		private Collection<Bitemporal> modified = new LinkedList<Bitemporal>();
		
		public Collection<Bitemporal> getAdded() {
			return added;
		}
		
		public Collection<Bitemporal> getRemoved() {
			return removed;
		}
		
		public Collection<Bitemporal> getModified() {
			return modified;
		}
	}
	
	
	private LinkedList<Bitemporal> orderByValidityStart(Collection<? extends Bitemporal> bitemporals) {
		LinkedList<Bitemporal> sortedBitemporals = new LinkedList<Bitemporal>();
		Map<DateTime, Bitemporal> map = new TreeMap<DateTime, Bitemporal>();
		for (Bitemporal bitemporal : bitemporals) {
			map.put(bitemporal.getValidityInterval().getInterval().getStart(), bitemporal);
		}
		for (Entry<DateTime, Bitemporal> entry : map.entrySet()) {
			sortedBitemporals.add(entry.getValue());
		}
		return sortedBitemporals;
	}
	
	private Continuity<?, ? extends Bitemporal> getParentContinuity() {
		Continuity<?, ? extends Bitemporal> continuity = getContinuity();
		if (continuity == null) {
			return null;
		}
		Map<Field, Continuity<?, ? extends Bitemporal>> bitemporalProperties = getBitemporalProperties(continuity);
		//FIXME: At the moment it is not possible to have more than one @BitemporalNotNull because we wouldn't be able to
		//determine which of them is the parent continuity. One way to solve this is by making the continuities to implement an
		//interface, another way would be to create another annotation.
		Continuity<?, ? extends Bitemporal> parentContinuity = null;
		if (bitemporalProperties.size() > 1) {
			throw new IllegalStateException("It is not possible at the momment to have more than one field with @BitemporalNotNull in a Continuity.");
		}
		for (Map.Entry<Field, Continuity<?, ? extends Bitemporal>> entry : bitemporalProperties.entrySet()) {
			parentContinuity = entry.getValue();
    	}
		return parentContinuity;
	}
	
}
