package com.anasoft.os.daofusion.bitemporal;

import org.joda.time.DateTime;

public interface CurrentDateProvider {

	public DateTime getCurrentDate();
	
}
