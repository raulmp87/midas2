package mx.com.afirme.midas.poliza;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumeroPolizaCompletoMidasTest {

	@Test
	public void testParseNumeroPolizaCompleto() {
		NumeroPolizaCompletoMidas result = NumeroPolizaCompletoMidas
				.parseNumeroPolizaCompleto("3101-12345678-00");
		assertEquals("3101-12345678-00", result.toString());
	}

	@Test
	public void testParseNumeroPolizaCompleto_OldNumber() {
		NumeroPolizaCompletoMidas result = NumeroPolizaCompletoMidas
				.parseNumeroPolizaCompleto("3101-123456-00");
		assertEquals("3101-00123456-00", result.toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParseNumeroPolizaCompleto_InvalidCharacters() {
		NumeroPolizaCompletoMidas.parseNumeroPolizaCompleto("3101s123456-00");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParseNumeroPolizaCompleto_InvalidCharactersNumber() {
		NumeroPolizaCompletoMidas
				.parseNumeroPolizaCompleto("3101-040123456-00");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParseNumeroPolizaCompleto_Null() {
		NumeroPolizaCompletoMidas.parseNumeroPolizaCompleto(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParseNumeroPolizaCompleto_Empty() {
		NumeroPolizaCompletoMidas.parseNumeroPolizaCompleto(" ");
	}

}
