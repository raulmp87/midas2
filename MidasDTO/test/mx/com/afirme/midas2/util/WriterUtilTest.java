package mx.com.afirme.midas2.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class WriterUtilTest {
	
	private WriterUtil tested;
	
	@Before
	public void setUp(){
		tested = new WriterUtil();
	}
	
	@Test
	public void testWriteCSVRow_OneInteger() {
		Object[] param = {Integer.valueOf(1)};
		assertEquals("1,", WriterUtil.writeCSVRow(param));
	}

	@Test
	public void testWriteCSVRow_TwoIntegers() {
		Object[] param = {Integer.valueOf(1), Integer.valueOf(2)};
		assertEquals("1,2,", WriterUtil.writeCSVRow(param));
	}
	
	@Test
	public void testWriteCSVRow_TwoIntegersAndTwoStrings() {
		Object[] param = {Integer.valueOf(1), Integer.valueOf(2), "Hola\ncomo\restas", "Espero no del todo mal"};
		assertEquals("1,2,Hola como estas,Espero no del todo mal,", WriterUtil.writeCSVRow(param));
	}
	
	@Test
	public void testWriteCSVRow_EmptyParam() {
		Object[] param = {};
		assertEquals("", WriterUtil.writeCSVRow(param));
	}
	
	@Test
	public void testWriteCSVRow_WithNullValue() {
		Object[] param = {null};
		assertEquals("", WriterUtil.writeCSVRow(param));
	}
	

}
