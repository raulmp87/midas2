<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>Login Midas</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Login page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
			
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>

	<body id="bg_login">

		<div id="page_login">
			<div id="encabezado_bg_login"></div>
			<div id="cont_bg_login">
				<div class="cont_login">

					<div id="encabezado1">

						<div class="logoASlogin"></div>
					</div>

					<div class="bg_gris">

						<div class="info_login">
							<div class="logomidas"></div>
							<h1>
								Bienvenido
							</h1>
							<p>
								Favor de ingresar su usuario y contraseņa para entrar al sistema
							</p>
							
							<midas:formulario accion="/usuario/validarUsuario.do">
							<table width="100%" border="0">
								<tr>
									<td width="32%" class="right_txt">
										Usuario:
									</td>
									<td width="68%">
										<midas:texto propiedadFormulario="usuario" nombreFormulario="validarUsuarioForm"/>
									</td>
								</tr>
								<tr>
									<td class="right_txt">
										Contraseņa:
									</td>
									<td>
										<midas:password propiedadFormulario="password" nombreFormulario="validarUsuarioForm" estilo="cajaTexto"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="b_entrar der">
											<a href="#" onclick="javascript: submitForm();">Entrar</a>
										</div>
										<div class="b_borrar der">
											<a href="#" onclick="javascript: window.location.href = '/MidasWeb/login.jsp';">Borrar</a>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										
									</td>
								</tr>
							</table>
						
						</midas:formulario>
						</div>
						<div class="img_login"></div>

					</div>
					<!--fin contenido -->
				</div>
			</div>
			<div id="pie_bg_login"></div>
		</div>
	</body>
</html>
