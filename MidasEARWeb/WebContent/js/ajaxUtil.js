function dataGridSendData(processor) {
	processor.sendData();
}

function mostrarTabDeComponente(tabBar, nombreDelTab) {
	tabBar.setTabActive(nombreDelTab);
}

function ejecutarFuncionYHacerAccion(funcionAEjecutar, strNombreDeLaBandera, funcionExito, funcionNoExito) {
	eval(funcionAEjecutar);
	
	var bandera = null;
	if (strNombreDeLaBandera != null) {
		 bandera = window[strNombreDeLaBandera];
	}
	
	if (bandera != null) {
		if (!bandera) {
			eval(funcionExito);
		}
		else {
			eval(funcionNoExito);
		}
	}
}

/* processor: String
 * bandera: String
 */

function actualizarGridAnexosReaseguro(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,'mostrarMensajeExito()', 'mostrarMensajeFallido()');
}


function actualizarGridProducto(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,'mostrarMensajeExitoYCambiarTab(configuracionProductoTabBar, \'detalle\')', 'mostrarMensajeFallido()');
			//'mostrarTabDeComponente(configuracionProductoTabBar, \'detalle\')', 'mostrarMensajeFallido()');
}

function actualizarGridCgAnexos(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,'mostrarMensajeExitoYCambiarTab(configuracionSeccionTabBar, \'detalle\')', 'mostrarMensajeFallido()');
}

function actualizarGridTipoPoliza(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,
			'mostrarMensajeExitoYCambiarTab(configuracionTipoPolizaTabBar, \'detalle\')', 'mostrarMensajeFallido()');
	
}

function actualizarGridCobertura(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,
			'mostrarTabDeComponente(configuracionCoberturaTabBar, \'detalle\')', 'mostrarMensajeFallido()');
}

function actualizarGridSeccion(processor, bandera) {
	ejecutarFuncionYHacerAccion('dataGridSendData(' + processor + ')', bandera,
			'mostrarTabDeComponente(configuracionSeccionTabBar, \'detalle\')', 'mostrarMensajeFallido()');
}

function mostrarMensajeExitoYCambiarTab(tabBar, nombreDelTab) {
	mostrarMensajeExito();
	mostrarTabDeComponente(tabBar, nombreDelTab);
}

function mostrarMensajeFallido() {
	mostrarVentanaMensaje('10', 'Algunos datos introducidos no son v\u00e1lidos.');
}

function mostrarMensajeErrorCobertura() {
	mostrarVentanaMensaje('10', 'La cobertura no puede ser desasociada porque esta siendo utilizada en una cotizaci\u00f3n.');
}

function mostrarMensajeExito() {
	mostrarVentanaMensaje('30', 'Acci\u00F3n realizada correctamente.');
}

function eliminarDocumentoAnexoProducto(){
	var selectedId = documentos.getSelectedRowId();
	if(selectedId != null && selectedId != undefined){
		documentos.deleteSelectedItem();
		documentoAnexoProductoProcessor.sendData();
		mostrarMensajeExitoYCambiarTab(configuracionProductoTabBar, 'detalle');
	}
}

function eliminarDocumentoAnexoTipoPoliza(){
	documentos.deleteSelectedItem();
	documentoAnexoTipoPolizaProcessor.sendData();
	mostrarMensajeExitoYCambiarTab(configuracionTipoPolizaTabBar, 'detalle');
}

function eliminarDocumentoAnexoCobertura(){
	documentos.deleteSelectedItem();
	documentoAnexoCoberturaProcessor.sendData();
	mostrarMensajeExitoYCambiarTab(configuracionCoberturaTabBar, 'detalle');
}

function eliminarDocumentoAnexoReferencias(){
	documentosAnexosRefGrid.deleteSelectedItem();
	documentoAnexoTipoReferenciaProcessor.sendData();
	mostrarMensajeExitoYCambiarTab(configuracionSeccionTabBar, 'detalle');
}