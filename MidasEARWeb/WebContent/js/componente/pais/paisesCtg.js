function cargaPais(){
	
var selected="";
url = "/MidasWeb/componente/paises/paisJson.action";
	jQuery.asyncGetJSON(url,null,function(data){
		
		jQuery.each( data['paises'], function( key, val ) {
			
			if(key == 'PAMEXI'){
				selected="selected='selected'";
			}else{
				selected = "";
			}
			 jQuery("#pais").append('<option '+selected+' value=' + key + '>' + val + '</option>');
		 });
	});
cargaEstados();
}

function cargaEstados(){

	var paisId = jQuery("#pais").val();
	var options = "";

	url = "/MidasWeb/componente/paises/estadoJson.action?paisId="+paisId;
	jQuery.asyncGetJSON(url,null,function(data){
		
		jQuery.each( data['estados'], function( key, val ) {
			options += "<option value=" + key + ">" + val + "</option>";
		 });
		
		jQuery("#estados").html(options);
	});
cargaCiudad();
}

function cargaCiudad(){

	var estadoId = jQuery("#estados").val();
	var options = "";

	url = "/MidasWeb/componente/paises/ciudadJson.action?estadoId="+estadoId;
	jQuery.asyncGetJSON(url,null,function(data){
		
		jQuery.each( data['ciudades'], function( key, val ) {
			options += "<option value=" + key + ">" + val + "</option>";
		 });
		
		jQuery("#ciudades").html(options);
	});
cargaColonias();
}

function cargaColonias(){

	var ciudadId = jQuery("#ciudades").val();
	var options = "";

	url = "/MidasWeb/componente/paises/coloniaJson.action?ciudadId="+ciudadId;
	jQuery.asyncGetJSON(url,null,function(data){
		
		jQuery.each( data['colonias'], function( key, val ) {
			options += "<option value=" + encodeURI(key) + ">" + val + "</option>";
		 });
		jQuery("#colonias").html(options);
	});

	cargaCp();	
}


function cargaCp(){

	//alert("Voy por cp");
	var coloniaId = jQuery("#colonias").val();
	var ciudadId = jQuery("#ciudades").val();
	//alert("ciu:"+ciudadId+" col:"+decodeURI(encodeURI(coloniaId)) );

	url = "/MidasWeb/componente/paises/codigoPostalJson.action?coloniaId="+coloniaId+"&ciudadId="+ciudadId;
	alert(url);
		jQuery.asyncGetJSON(url,null,function(data){
			
			jQuery.each( data['cpId'], function( key, val ) {
				//alert(val);
				jQuery("#cp").attr("value",val);
			});
			
		});
}
