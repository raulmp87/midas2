
/**
*   @desc: Cargado grid Scrollable
*   @param: name - Nombre del grid a cargar (obligatorio si se tiene mas de un grid en la pagina)
*   @type: public
*/
function loadGrid(name, properties) {
		
	var isNew = ((properties == null || properties == undefined)?true:false);
	var prop;
		
	//Significa que ya no hay mas grids
	
	if (name == null && jQuery('#gridName').val() == null) {
		
		return;
		
	} else if (name == null) {
		
		name = jQuery('#gridName').val();
		
	}
	
	if (properties == null || properties == undefined) {
		
		prop = new ScrollableProperties(name);
		
	} else {
		
		prop = properties;
		
	}
		
	//Creacion del DataGrid
	
	var scrollableGrid = prop.createGrid(prop.gridName);
							
	//Evento al seleccionar un registro del grid
	scrollableGrid.attachEvent("onRowSelect", function(id,ind){
		
		var _prop = this.getUserData("","properties");
		
		if (_prop.selectJs != null && _prop.selectJs != undefined && _prop.selectJs != '') {
			var contSelectJS = "(" + id + ","+ind+")";
			eval(_prop.selectJs + contSelectJS);
			
		}
		
	});
	
	scrollableGrid.attachEvent("onCheck", function(rId,cInd,state){
	    
		var _prop = this.getUserData("","properties");
		
		if (_prop.checkJs != null && _prop.checkJs != undefined && _prop.checkJs != '') {
			var checkJsParams = "(" + rId + "," + cInd + "," + state +")";
			eval(_prop.checkJs + checkJsParams);
			
		}
			
	});
		
	scrollableGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue){
	    			
		var _prop = this.getUserData("","properties");
		
		if (_prop.cellChangedJs != null && _prop.cellChangedJs != undefined && _prop.cellChangedJs != '' && stage == 2) {
			var cellChangedJsParams = "(" + rId + "," + cInd + "," + (typeof nValue == 'string' ? "'" + nValue + "'":nValue) +")";
			eval(_prop.cellChangedJs + cellChangedJsParams);
		}
		return true;
	});
	
	//Evento al ordenar una columna del grid
		
	scrollableGrid.attachEvent("onBeforeSorting", function (columnIndex, sortType, sortDirection) {
		
		var _prop = this.getUserData("","properties");
		
		_prop.setSortingClause(this.getColumnId(columnIndex), sortDirection);
							
		loadGrid(_prop.gridName, _prop);
		
		//Cancels client side sorting by returning “false” from event handler function for onBeforeSorting event.
		return false;
	    
	});
	
	//Evento al iniciar la carga del grid
	
	scrollableGrid.attachEvent("onXLS", function(grid){
		
		var _prop = new ScrollableProperties(jQuery(this.entBox).attr('id'));
		
		jQuery('<div id="indicador" align="left"><br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Procesando la informaci&oacute;n, espere un momento por favor...</font></div>')
		.appendTo(_prop.container + ' > div#espacioIndicador');
		
	});
	
	//Evento al terminar de cargar el grid
	
	scrollableGrid.attachEvent("onXLE", function(grid){
		
		var _prop = new ScrollableProperties(jQuery(this.entBox).attr('id'));
				
		jQuery(_prop.container + ' > div > div#indicador').remove();
					
		if (_prop.isGridSorted()) {
			//Sets Sort Image visible for the column we sort and with the direction we need.
			
			this.setSortImgState(true, this.getColIndexById(_prop.sortedColumn), _prop.sortDirection);
			
		}
		
		this.setUserData("","properties", _prop);
		
    });	
	
	scrollableGrid.enableSmartRendering(true, 50);
			
	scrollableGrid.load(prop.url);
				
}



function ScrollableProperties(name) {
	
	this.gridName = name;
	this.container = 'div#' + this.gridName + '_contenedor';
	this.loadAction = jQuery(this.container + ' > #loadAction').val();
	this.form = jQuery(this.container + ' > #gridForm').val();
	this.selectJs = jQuery(this.container + ' > #selectJs').val();
	this.checkJs = jQuery(this.container + ' > #checkJs').val();
	this.cellChangedJs = jQuery(this.container + ' > #cellChangedJs').val();
	this.gridColumnsPath = jQuery(this.container + ' > #gridColumnsPath').val();
	this.gridRowPath = jQuery(this.container + ' > #gridRowPath').val();
	
	this.sortedColumnDiv = '<input type="hidden" value="" id="sortedColumn">';
	this.sortDirectionDiv = '<input type="hidden" value="" id="sortDirection">';
	
	this.url = null;
	this.sortedColumn = null;
	this.sortDirection = null;
	
	if (jQuery(this.container + ' > #sortedColumn').length > 0) {
		
		this.sortedColumn = jQuery(this.container + ' > #sortedColumn').val();
		this.sortDirection = jQuery(this.container + ' > #sortDirection').val();
		
	}
	
	this.generateUrl();
	
}

ScrollableProperties.prototype={
	
		generateUrl : function() {
			
			this.url = "";
			this.url = this.url + (this.form != null ? "?" + jQuery("#" + this.form).serialize() : "");
			this.url = this.url + (this.gridColumnsPath != null ? (this.url.indexOf("?") >= 0 ? "&" : "?") + "gColPath=" + this.gridColumnsPath : "");
			this.url = this.url + (this.gridRowPath != null ? (this.url.indexOf("?") >= 0 ? "&" : "?") +  "gRowPath=" + this.gridRowPath : "");
			
			if (this.isGridSorted()) {
			
				this.url = this.url + (this.url.indexOf("?") >= 0 ? "&" : "?") + "orderBy=" + this.sortedColumn + "&direct=" + this.sortDirection;
				
			}
						
			this.url = this.loadAction + this.url;
			
		},
		
		isGridSorted : function() {
			
			if (this.sortedColumn != null && this.sortedColumn != "" && this.sortDirection != null && this.sortDirection != "") {
				
				return true;
				
			}
			
			return false;
			
		},
		
		createGrid : function(gridName) {
		
			jQuery('div#' + this.gridName).empty();
			
			return new dhtmlXGridObject(gridName);
								
		}, 
		
		setSortingClause : function(sortedColumn, sortDirection) {
			
			this.sortedColumn = sortedColumn;
			this.sortDirection = sortDirection;
						
			if (jQuery(this.container + ' > #sortedColumn').length == 0) {
			
				jQuery(this.sortedColumnDiv).appendTo(this.container);
				jQuery(this.sortDirectionDiv).appendTo(this.container);
				
			}
									
			jQuery(this.container + ' > #sortedColumn').attr('value', this.sortedColumn);
			jQuery(this.container + ' > #sortDirection').attr('value', this.sortDirection);
			
			this.generateUrl();
			
		}
				
}


jQuery(document).ready(function(){
	
	var name = jQuery("#gridNameFirst").val();
	
	jQuery("#gridNameFirst").remove(); 
	
	loadGrid(name); 
	
	
});
