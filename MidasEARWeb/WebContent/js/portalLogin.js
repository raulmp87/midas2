
jQuery(document).ready(function() {
	
    /*Fullscreen background*/
    $.backstretch(contextPath + "/img/backgrounds/1.jpg");    
   
    /*Form validation*/
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {    	
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});    	
    });    
});

//Validar Email Crear Usuario
$(document).ready(function (){
	$('#createUser').submit(function (e){
		var mail = $('#form-email').val();
		if (validaEmail($('#form-email').val()) == false || validaEmail($('#form-retypeEmail').val()) == false){
			
			return false;
		}		
		if($('#form-email').val() != $('#form-retypeEmail').val()){
			$('#textDinamico').text("Los correos introducidos no coinciden.")
			$('#mensajeCustom').modal();
			e.preventDefault();
		}
	})
});

//Validar Email Restaurar Contraseña
$(document).ready(function (){
	$('#restaurarPassword').submit(function (e){
		var mail = $('#form-email').val();
		if (validaEmail($('#form-email').val()) == false){			
			return false;
			e.preventDefault();
		}		
	})
});

function validaEmail(email){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

//Validar Password Login
$(document).ready(function(){
	$('#loginForm').submit(function(event){
		data = $('#form-password').val();
	    var len = data.length;	       
	    if(len != 8 ){
	    	$('#textDinamico').text("La contraseña debe ser de 8 caracteres.");
	    	$('#mensajeCustom').modal();
	    	event.preventDefault();
	    }       
	});
});

//Validar Password CreateUser
$(document).ready(function(){
	   $('#createUser').submit(function(event){	   
	        data = $('#form-password').val();
	        var len = data.length;	       
	        if(len < 1 ) {	        	
	            event.preventDefault();
	        }else if(len != 8 ){
	        	$('#textDinamico').text("La contraseña debe ser de 8 caracteres.");
				$('#mensajeCustom').modal();
	        	event.preventDefault();
	        }if($('#form-password').val() != $('#form-retypePassword').val()) {
	        	$('#textDinamico').text("Las contraseñas introducidas no coinciden.")
				$('#mensajeCustom').modal();
	            event.preventDefault();
	        }	        
	    });
});

//Validar Cambio Password
$(document).ready(function(){
	   $('#changePassword').submit(function(event){	   
	        data = $('#form-newPassword').val();
	        console.log('data:= '+data);
	        var len = data.length;	       
	        if(len < 1 ) {	        	
	            event.preventDefault();
	        }else if(len != 8 ){
	        	$('#textDinamico').text("La contraseña debe ser de 8 caracteres.")
				$('#mensajeCustom').modal();
	        	event.preventDefault();
	        }if($('#form-newPassword').val() != $('#form-confirmNewPassword').val()) {
	        	$('#textDinamico').text("Las contraseñas no coinciden.")
				$('#mensajeCustom').modal();
	            event.preventDefault();
	        }	        
	    });
});

//Validar telefonoCelular
$(document).ready(function(){
	$('#createUser').submit(function(event){
		tel = $('#form-cellPhone').val();
		var len = tel.length;
		if(len > 10 || len < 10){		
			$('#textDinamico').text("El teléfono debe ser a 10 Dígitos.")
			$('#mensajeCustom').modal();
			event.preventDefault();
		}
	});	
});