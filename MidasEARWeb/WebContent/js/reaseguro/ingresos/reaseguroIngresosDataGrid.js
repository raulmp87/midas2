var listaIngresosReaseguro;
function mostrarGridListaIngresos() {
	listaIngresosReaseguro = new dhtmlXGridObject('gridboxListaIngresos');
	listaIngresosReaseguro.setHeader(",Monto Ingreso,Moneda,Referencia,TC,Concepto,Num Cuenta,Fecha");
	listaIngresosReaseguro.setInitWidths("30,110,80,100,30,100,100,100");
	listaIngresosReaseguro.setColumnIds("seleccionado,montoIngreso,moneda,referencia,tipoCambio,concepto,numeroCuenta,fecha");
	listaIngresosReaseguro.setColAlign("center,center,center,center,center,center,center,center");
	listaIngresosReaseguro.setColSorting("int,str,str,str,str,str,str,str");
	listaIngresosReaseguro.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro");
	listaIngresosReaseguro.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	listaIngresosReaseguro.setSkin("light"); 
	listaIngresosReaseguro.enableDragAndDrop(false);
	listaIngresosReaseguro.enableLightMouseNavigation(false);
	listaIngresosReaseguro.init();
	
	listaIngresosReaseguro.load('/MidasWeb/reaseguro/ingresos/mostrarIngresosPendientes.do', null, 'json');
}

var polizaAdministrarIngresosGrid;
function mostrarPolizaAdministrarIngresosGrid(nomAsegurado, numPoliza, tipoReaseguro){
	if (validarFormatoNumeroPolizaRptsReaseguro(numPoliza)){
		var polizaAdministrarIngresosGridPath='/MidasWeb/reaseguro/ingresos/mostrarPolizas.do?nomAsegurado='+nomAsegurado+'&numPoliza='+numPoliza+'&tipoReaseguro='+tipoReaseguro;
		polizaAdministrarIngresosGrid = new dhtmlXGridObject('polizaAdministrarIngresosGrid');
		polizaAdministrarIngresosGrid.setHeader(",Nom Asegurado,N&uacute;mero de P&oacute;liza,Fecha de emisi&oacute;n,Fecha de Vigencia");	
		polizaAdministrarIngresosGrid.setColumnIds("seleccionado,nomAsegurado,numeroPoliza,fechaEmision,fechaVigencia");
		polizaAdministrarIngresosGrid.setInitWidths("30,200,150,150,200");
		polizaAdministrarIngresosGrid.setColAlign("center,center,center,center,center");
		polizaAdministrarIngresosGrid.setColSorting("int,str,int,str,str");
		polizaAdministrarIngresosGrid.setColTypes("ra,ro,ro,ro,ro");
		polizaAdministrarIngresosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
	    });
		polizaAdministrarIngresosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		polizaAdministrarIngresosGrid.setSkin("light");		
		polizaAdministrarIngresosGrid.enableDragAndDrop(false);	
		polizaAdministrarIngresosGrid.enableLightMouseNavigation(false);
		polizaAdministrarIngresosGrid.init();
		polizaAdministrarIngresosGrid.load(polizaAdministrarIngresosGridPath, null, 'json');
	}
}

var detallePolizaAdministrarIngresosGrid;
function mostrarDetallePolizaAdministrarIngresosGrid(numeroPoliza){
	var detallePolizaAdministrarIngresosGridPath='/MidasWeb/reaseguro/ingresos/mostrarEstadosCuentaAdministrarIngresos.do?numeroPoliza='+numeroPoliza;
	detallePolizaAdministrarIngresosGrid = new dhtmlXGridObject('detallePolizaAdministrarIngresosGrid');
	detallePolizaAdministrarIngresosGrid.setHeader(",P&oacute;liza,Endoso,Subramo,Tipo de Reaseguro,Forma de Pago,Suscripci&oacute;n"
			+",Reasegurador,Corredor,Moneda,Saldo T&eacute;cnico");	
	detallePolizaAdministrarIngresosGrid.setColumnIds("seleccionado,poliza,endoso,subRamo,tipoReaseguro,formaPago,suscripcion"
			+",reasegurador,corredor,moneda,saldoTecnico");
	detallePolizaAdministrarIngresosGrid.setInitWidths("30,100,100,150,150,150,150"
			+",200,200,100,100");
	detallePolizaAdministrarIngresosGrid.setColAlign("center,center,center,center,center,center,center"
			+",center,center,center,center");
	detallePolizaAdministrarIngresosGrid.setColSorting("int,int,int,str,str,str,str"
			+",str,str,str,str");
	detallePolizaAdministrarIngresosGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro"
			+",ro,ro,ro,ro");
	detallePolizaAdministrarIngresosGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingEstadosCuenta');
    });    
	detallePolizaAdministrarIngresosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingEstadosCuenta');
    }); 
	detallePolizaAdministrarIngresosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	detallePolizaAdministrarIngresosGrid.setSkin("light");	
	detallePolizaAdministrarIngresosGrid.enableDragAndDrop(false);	
	detallePolizaAdministrarIngresosGrid.enableLightMouseNavigation(false);
	detallePolizaAdministrarIngresosGrid.init();
	
	if ($('tipoReaseguro') != null){
		if (parseInt($('tipoReaseguro').value) == 3){ //Si el combo tipoReaseguro est� en "Facultativo"
			detallePolizaAdministrarIngresosGrid.setColumnHidden(1,false);
			detallePolizaAdministrarIngresosGrid.setColumnHidden(2,false);
		}else{
			detallePolizaAdministrarIngresosGrid.setColumnHidden(1,true);
			detallePolizaAdministrarIngresosGrid.setColumnHidden(2,true);
		}
	}
	
	detallePolizaAdministrarIngresosGrid.load(detallePolizaAdministrarIngresosGridPath, null, 'json');
}

var listaEstadosCuentaRelacionarIngreso;
function mostrarEstadosCuentaRelacionarIngresoGrid(idsEdosCta, idIngreso){
	listaEstadosCuentaRelacionarIngreso = new dhtmlXGridObject('gridboxListaEstadosCuentaRelacionarIngreso');
	listaEstadosCuentaRelacionarIngreso.setHeader("Tipo de Reaseguro,Suscripci&oacute;n,Nombre Reasegurador/Retenci&oacute;n,Subramo,Moneda,Saldo T&eacute;cnico,Monto");
	listaEstadosCuentaRelacionarIngreso.setInitWidths("140,100,230,100,90,120,100");
	listaEstadosCuentaRelacionarIngreso.setColumnIds("a,b,c,d,e,f,monto");
	listaEstadosCuentaRelacionarIngreso.setColAlign("center,center,center,center,center,center,center");
	listaEstadosCuentaRelacionarIngreso.setColSorting("str,str,str,str,str,str,str");
	listaEstadosCuentaRelacionarIngreso.setColTypes("ro,ro,ro,ro,ro,ro,ed");
	listaEstadosCuentaRelacionarIngreso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	listaEstadosCuentaRelacionarIngreso.setSkin("light"); 
	listaEstadosCuentaRelacionarIngreso.enableDragAndDrop(false);
	listaEstadosCuentaRelacionarIngreso.enableLightMouseNavigation(false);
	listaEstadosCuentaRelacionarIngreso.attachEvent("onEditCell",function(stage,rowId,cellIndex,newValue,oldValue){
			if (rowId == -1){
				return false;
			}else{
				if (stage == 2){
					if(newValue != ''){
						newValue = newValue.replace("$", "");
						var cont = newValue.length;
						for (i = 0; i < cont; i=i+1)
							newValue = newValue.replace(",", "");
					}
					if (isNumeric(newValue) || isFloat(newValue)){
						var saldoTecnico = listaEstadosCuentaRelacionarIngreso.cellById(rowId,cellIndex-1).getValue();
						saldoTecnico = saldoTecnico.replace("$", "");
						var cont = saldoTecnico.length;
						for (i = 0; i < cont; i++)
							saldoTecnico = saldoTecnico.replace(",", "");
						
						if (rowId < 0 || parseFloat(saldoTecnico) >= parseFloat(newValue)){
							listaEstadosCuentaRelacionarIngreso.cellById(rowId,cellIndex).setValue(formatCurrency4Decs(newValue));
							return true;
						}else{
							//alert('El monto del Ingreso debe ser menor o igual al Saldo Tecnico del Estado de Cuenta correspondiente');
							return true;
						}
					}else{
						alert('La cifra proporcionada no es v\u00e1lida');
						return false;	
					}
				}
			}
		
	});
	listaEstadosCuentaRelacionarIngreso.init();
	
	listaEstadosCuentaRelacionarIngreso.load('/MidasWeb/reaseguro/ingresos/listarEstadosCuentaRelacionarIngreso.do?idsEdosCta='+idsEdosCta+'&idIngreso='+idIngreso, null, 'json');
}