/*Este modulo es para importar las librerias y las funciones globales de el modulo de reaseguro*/

function horaMayorOIgualQue(hr0,hr1){
	var hora1=0;
	var hora2=0;
	
	if (!esHoraValida(hr0) || !esHoraValida(hr1)){
		return false;
	}
	
	hora1 = 100*parseInt(hr0.substring(0,2)) + parseInt(hr0.substring(4,5));
	hora2 = 100*parseInt(hr1.substring(0,2)) + parseInt(hr1.substring(4,5));	
	return (hora1>=hora2);
}

function limpiarDivsEdoCtaFacultativos(){
	limpiarDiv('contenido_porReasegurador');
	limpiarDiv('contenido_porPoliza');
	limpiarDiv('contenido_porSiniestro');
	limpiarDiv('contenido_porContratoFacultativo');
	limpiarDiv('contenido_tipoReaseguro');
}

function limpiarDivsAutorizarEgresos(){
	limpiarDiv('contenido_facultativoEdoCta');
	limpiarDiv('contenido_exhibicionVencida');
	limpiarDiv('contenido_exhibicionConPagoVencido');
}

function limpiarDiv(nombreDiv){
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined){
		div.innerHTML = '';
	}
}

function comprobarSiBisisesto(anio){
	if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
	    return true;
	}
	else {
	    return false;
	}
}

function esFechaValidaFormatoMesAnio(fecha){
    if (fecha != undefined && fecha.value != "" ){
        if (!/^\d{2}\/\d{4}$/.test(fecha.value)){
            alert("formato de fecha no v\xE1lido (mm/aaaa)");
            fecha.value = '';
            return false;
        }
	    var mes  =  parseInt(fecha.value.substring(0,2),10);
	    var anio  =  parseInt(fecha.value.substring(3,7),10);
	    var mesValido = false;
	    var anioValido = false;
		if(mes>0 && mes<=12)
			mesValido = true;
		if(anio>1900)
			anioValido = true;
	    
		if(!mesValido || !anioValido){
	        alert("La fecha introducida es err\xF3nea.");
	    	fecha.value = '';
	        return false;
	    }
	}
}

function esFechaValida(fecha){
    if (fecha != undefined && fecha.value != "" ){
        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha.value)){
            alert("formato de fecha no v\xE1lido (dd/mm/aaaa)");
            fecha.value = '';
            return false;
        }
        var dia  =  parseInt(fecha.value.substring(0,2),10);
        var mes  =  parseInt(fecha.value.substring(3,5),10);
        var anio =  parseInt(fecha.value.substring(6),10);
 
    switch(mes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numDias=31;
            break;
        case 4: case 6: case 9: case 11:
            numDias=30;
            break;
        case 2:
            if (comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
            break;
        default:
            alert("La fecha introducida err\xF3nea");
        	fecha.value = '';
            return false;
    }
 
		if (dia>numDias || dia==0){
		    alert("La fecha introducida err\xF3nea");
		    fecha.value = '';
		    return false;
		}
		return true;
    }
}

function validarFecha(fecha){ //En string es decir el value
	var errores = "";
    if (fecha != undefined && fecha != "" ){
        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)){
            errores += "Formato de fecha no v\xE1lido (dd/mm/aaaa)<br/>";
        }
        else {
        	var dia  =  parseInt(fecha.substring(0,2),10);
            var mes  =  parseInt(fecha.substring(3,5),10);
            var anio =  parseInt(fecha.substring(6),10);
            
            switch(mes){
            	case 1:
            	case 3:
            	case 5:
            	case 7:
            	case 8:
            	case 10:
            	case 12:
            		numDias=31;
            		break;
            	case 4: case 6: case 9: case 11:
            		numDias=30;
            		break;
            	case 2:
            		if (comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
            		break;
            	default:
            		errores += "La fecha introducida err\xF3nea<br/>";
            }

            if (dia>numDias || dia==0){
        	    errores += "El dia introducido es err\xF3neo<br/>";
        	}
        }
    }
    
    return errores;
}

function esHoraValida(cadena){
	var horaExpr =  new RegExp("(([0-1][0-9])|([2][0-3])):([0-5][0-9])");  
	return (horaExpr.test( cadena ));
}

function validaHoraLineas(idElemento){
	var elemento = $(idElemento);
	if (!esHoraValida(elemento.value)){
		alert('Hora incorrecta');
		$(idElemento).value="00:00";
	}
}

function fechaMayorOIgualQue(fec0, fec1){
    var bRes = false;
    var sDia0 = fec0.substr(0, 2);
    var sMes0 = fec0.substr(3, 2);
    var sAno0 = fec0.substr(6, 4);
    var sDia1 = fec1.substr(0, 2);
    var sMes1 = fec1.substr(3, 2);
    var sAno1 = fec1.substr(6, 4);
    if (sAno0 > sAno1) bRes = true;
    else {
     if (sAno0 == sAno1){
      if (sMes0 > sMes1) bRes = true;
      else {
       if (sMes0 == sMes1)
        if (sDia0 >= sDia1) bRes = true;
      }
     }
    }
    return bRes;
   } 

function fechaMayorQue(fec0, fec1){
    var bRes = false;
    var sDia0 = fec0.substr(0, 2);
    var sMes0 = fec0.substr(3, 2);
    var sAno0 = fec0.substr(6, 4);
    var sDia1 = fec1.substr(0, 2);
    var sMes1 = fec1.substr(3, 2);
    var sAno1 = fec1.substr(6, 4);
    if (sAno0 > sAno1) bRes = true;
    else {
     if (sAno0 == sAno1){
      if (sMes0 > sMes1) bRes = true;
      else {
       if (sMes0 == sMes1)
        if (sDia0 > sDia1) bRes = true;
      }
     }
    }
    return bRes;
   } 

function formatoFechaCalendario(){
	return "%d/%m/%Y";
}

function esValidoRangoDeFechasLineas(){
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	if (fechaInicial.length!=0 && fechaInicial.length!=0){
		if (fechaInicial.length==0 || fechaFinal.length==0){
			alert('Debe seleccionar la fecha');
			return false;
		}else{
			if(!fechaMayorOIgualQue(fechaFinal,fechaInicial)){
				alert('Rango de fechas Incorrecto');
				return false;
			}
		}
				
		if (fechaInicial == fechaFinal && !horaMayorOIgualQue($('horaFinal').value,$('horaInicial').value)){
			alert('Rango de fecha no v\xE1lido por la hora');
			return false;
		}
	}else{
		alert('Debe seleccionar la fecha');
		return false;
	}
	if (fechaInicial.length != 0){
		fechaInicial += ' ' +trim($('horaInicial').value); 
	}
	if (fechaFinal.length != 0){
		fechaFinal += ' ' + trim($('horaFinal').value); 
	}	
	$('fechaInicial').value=fechaInicial;
	$('fechaFinal').value=fechaFinal;
	return true;
}

function validarNumeroMaximoYDecimales(numero, numeroMaximo, numeroMaximoDeDecimales, desplegarAlert){
	var cad = "" + numero;
	var arr = cad.split('.');
	var numeroMaximoDeEnteros= numeroMaximo - numeroMaximoDeDecimales;
	if (cad.length > (numeroMaximo + 1) || (arr[0].length > numeroMaximoDeEnteros) || (arr.length > 1 && arr[1].length > numeroMaximoDeDecimales)){
		if (desplegarAlert)
			alert('El n\u00famero introducido no debe exceder los ' + numeroMaximo + ' d\u00edgitos, y no debe tener mas de ' + (numeroMaximo-numeroMaximoDeDecimales) + " enteros y " + numeroMaximoDeDecimales + ' decimales')
		
		return false;
	}
	
	return true;
}

function mostrarDivParticipaciones(){	
	if((document.getElementById('idTmContratoCuotaParte')!==null && document.getElementById('idTmContratoCuotaParte').value!=='') || (document.getElementById('idTmContratoPrimerExcedente')!==null && document.getElementById('idTmContratoPrimerExcedente').value!=='')){
		document.getElementById('resultados').style.display='block';
		document.getElementById('b_continuar').style.display='none';
		document.getElementById('b_regresar').style.display='none';
	}else{
		document.getElementById('resultados').style.display='none';
		document.getElementById('b_continuar').style.display='block';
		document.getElementById('b_regresar').style.display='block';
	}	
}

function mostrarDivReaseguro(idDiv){
	$(idDiv).style.display='';
}

function ocultarDivReaseguro(idDiv){
	$(idDiv).style.display='none';
}

function inicializarCombosEdoCtaTipoReasegurador(){
	//document.getElementById('idtcReaseguradorCorredor').disabled=true;
}

function inicializarCombosAdmonEgresos(){
	//document.getElementById('idtcReaseguradorCorredor').disabled=true;
}

function inicializarCombosAdmonIngresos(){
	//document.getElementById('idtcReaseguradorCorredor').disabled=true;
}

function interacCmbsTipoReaseguroReasegurador(comboTipoReaseguro){
	if(parseInt(comboTipoReaseguro.value) == 4){
		$('comboReasegurador').style.display='none';
		$('comboRetencion').style.display='block';
		document.getElementById('idtcReaseguradorCorredor').selectedIndex='';
	}else{
		$('comboReasegurador').style.display='block';
		$('comboRetencion').style.display='none';
		document.getElementById('idtcReaseguradorCorredor').disabled=false;
	}
	
	/*if(comboTipoReaseguro.value == ''){
		document.getElementById('idtcReaseguradorCorredor').selectedIndex='';
		document.getElementById('idtcReaseguradorCorredor').disabled=true;
	}*/
	var tipoReaseguro = $('tipoReaseguro').selectedIndex;
	//Si se selecciono facultativo o cuotaparte o primer excedente
	if (tipoReaseguro > 0){
		var opcionRetencion = new Option("RETENCION","-1");
		var comboReasegurador = $('idtcReaseguradorCorredor');
		var nOpciones = comboReasegurador.options.length;
		var i = 0;
		var posicionRetencion=-1;
		for (;i<nOpciones;i++){
			if (comboReasegurador.options[i].value<0){
				posicionRetencion=i;
				break;
			}
		}	
		//Este caso se da cuando se selecciona cuota parte o facultativo
		if (tipoReaseguro==1 || tipoReaseguro==3){
			if (posicionRetencion==-1)
				comboReasegurador.options[nOpciones]=opcionRetencion;
		}
		//Si se selecciona primer excedente
		if (tipoReaseguro == 2){
			if (posicionRetencion!=-1)
				comboReasegurador.options[posicionRetencion]=null;
		}
	}
	
	var tipoReaseguro = $('tipoReaseguro').value;
	// El siguiente c�digo aplica solamente en el m�dulo Administrar Ingresos y Administrar Egresos
	if (tipoReaseguro==3){ // Cuando se elija Tipo reaseguro Facultativo, si el div 'divBusquedaPolizas' existe entonces se trata del CU administrar ingresos, y se procede a mostrar/ocultar dicho div
		if ($('divBusquedaPolizas') != null && $('divGridBusquedaPolizas') != null){
			$('divBusquedaPolizas').style.display = 'block';
			$('divGridBusquedaPolizas').style.display = 'block';
			document.getElementById('botonMostrarOcultarDiv').src="/MidasWeb/img/minus_green.png";
		}
	}else{
		if ($('divBusquedaPolizas') != null && $('divGridBusquedaPolizas') != null){
			$('divBusquedaPolizas').style.display = 'none';
			$('divGridBusquedaPolizas').style.display = 'none';
			document.getElementById('botonMostrarOcultarDiv').src="/MidasWeb/img/add_green.png";
		}
	}
	// Fin de c�digo aplica solamente en el m�dulo Administrar Ingresos
}

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null); 
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
  if ((evt.keyCode == 13) && (node.type=="text"))  { return false;} 
}

function manejarComboSuscripcionEdoCtaTipoReaseguro(){
	var ids;
	if ($('idEjercicio').value != '' && $('idTcSubRamo').value != '' && $('tipoReaseguro').value != '' ){
		$('idSuscripcion').disabled = false;
		llenaComboSuscripciones($('idEjercicio').value, $('idTcSubRamo').value, $('tipoReaseguro').value);
	}else{
		$('idSuscripcion').disabled = true;
		$('idSuscripcion').value = '';
	}
}

function llenaComboSuscripciones(idEjercicio,idTcSubRamo,tipoReaseguro) {
	new Ajax.Request("/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do", {
			method : "post",
			asynchronous : false,
			parameters : "idEjercicio=" +idEjercicio+ "&idTcSubRamo=" +idTcSubRamo+ "&tipoReaseguro=" +tipoReaseguro,
			onSuccess : function(transport) {
				loadCombo($('idSuscripcion'), transport.responseXML);
			}
		});
}

function regresarRegistrarContratoCuotaParte(funcionesAEjecutar, xmlDoc){
	var funcionesJS;
	if (funcionesAEjecutar==0){
		funcionesJS = 'mostrarContratosCuotaParteGrids()';
	}else if (funcionesAEjecutar==1){
		funcionesJS = 'initParticipacionesCPGrids(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids()';
	}
	procesarRespuestaXml(xmlDoc, "sendRequest(document.contratoCuotaParteForm, '/MidasWeb/contratos/contratocuotaparte/regresarRegistrarContratoCuotaParte.do', 'contenido','" + funcionesJS + "')");
}

function notificacionResultadoOperacion(){
	var mensaje;
	var tipoMensaje;
	var img;
	if ($('mensaje') !== null && $('tipoMensaje') !== null){
		mensaje = $('mensaje').value;
		tipoMensaje = $('tipoMensaje').value;
		if (parseInt(tipoMensaje) == 30)
			img = '<img alt="Exito" src="/MidasWeb/img/okIcon.gif" />';
		else
			img = '<img alt="Error" src="/MidasWeb/img/errorIcon.gif" />';
		
		if (document.getElementById('mensajeResultadoOperacion') !== null){
			document.getElementById('mensajeResultadoOperacion').innerHTML = img + "&nbsp;" + mensaje;
			document.getElementById('mensajeResultadoOperacion').style.display="block";
			setTimeout("document.getElementById('mensajeResultadoOperacion').style.display='none'",2000);
		}
	}
}

function buscarEstadosCuentaTipoReaseguro(mensajeEjercicioNoSeleccionado){
	if ($('idEjercicio').value == ''){
		mostrarVentanaMensaje(10, mensajeEjercicioNoSeleccionado,null);
//		alert(mensajeEjercicioNoSeleccionado); 
	}
	else if($('idtcReaseguradorCorredor').value == ''){
		mostrarVentanaMensaje(10, 'Seleccione un reasegurador.',null);
	}
	else if($('idTcMoneda').value == ''){
		mostrarVentanaMensaje(10, 'Seleccione una moneda.',null);
	}
	else if($('tipoReaseguro').value == ''){
		mostrarVentanaMensaje(10, 'Seleccione tipo de reaseguro.',null);
	}
	else{
		sendRequest(document.estadoCuentaTipoReaseguroForm, '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/obtenerEstadosCuenta.do', null,'mostrarEstadosCuentaTipoReaseguroGrid()');
	}
}

function mostrarDetalleEstadoCuentaTipoReaseguro(mensajeEstadoCuentaNoSeleccionado){
	var checkIds = estadoCuentaTipoReaseguroGrid.getCheckedRows(0);
	if (checkIds == '')
		alert(mensajeEstadoCuentaNoSeleccionado);
	else{
//		var mostrarEjerciciosAnteriores = document.getElementById('checkEjerciciosAnteriores').checked;
		sendRequest(document.estadoCuentaTipoReaseguroForm,
			'/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle.do?idEstadoCuenta='+checkIds+'&mostrarEjerciciosAnteriores=true'/*+mostrarEjerciciosAnteriores*/,
			'contenido', 'inicializaComponentesEstadoCuentaTipoReaseguro()');
	}
}

function guardarObservacionesEstadoDeCuenta(){
	var obs = $('observaciones').value;
	sendRequest(document.estadoCuentaContratoFacultativoForm,'/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/guardarObservaciones.do', null, 'procesarRespuestaXml(transport.responseXML,null)');
}

function guardarObservacionesEstadoCuentaTipoReaseguro(){
	var obs = $('observaciones').value;
	sendRequest(document.estadoCuentaTipoReaseguroForm,'/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/guardarObservaciones.do', null, 'procesarRespuestaXml(transport.responseXML,null)');
}

function mostrarDetalleEstadoCuentaContratoFacultativo(mensajeEstadoCuentaNoSeleccionado){
	 var checkIds = detallePolizaEstadoCuentaGrid.getCheckedRows(0);
 	 if (checkIds == '')
		 alert(mensajeEstadoCuentaNoSeleccionado);
	 else
		 sendRequest(document.estadoCuentaContratoFacultativoForm,'/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle.do?idEstadoCuenta='+checkIds, 'contenido', 'inicializaComponentesEstadoCuentaContratoFacultativo()');
}

var pestaniaACargar,pestaniaHijaACargar;
function cargarEstadosCuentaAdmonMovs(pestania1,pestania2){
	pestaniaACargar = pestania1;
	pestaniaHijaACargar = pestania2;
	sendRequest(null,'/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do','contenido','dhx_init_tabbars(); cargarPestaniaEstadosCuentaTabBar();');
}

function cargarPestaniaEstadosCuentaTabBar(){
	if (pestaniaACargar != null && pestaniaACargar != ''){
		estadosCuentaFacultativoTabBar.setTabActive(pestaniaACargar);
	}
}

function cargarPestaniaEstadosCuentaFacultativoTabBar(){
	if (pestaniaHijaACargar != null && pestaniaHijaACargar != ''){
			estadosCuentaTabBar.setTabActive(pestaniaHijaACargar);
	}
}

function buscarContratosEdoCuentaContratosFacultativos(mensajePolizaNoSeleccionada){
	 var numeroPoliza = polizaEstadoCuentaGrid.getCheckedRows(0);
 	 if (numeroPoliza == ''){
		 alert(mensajePolizaNoSeleccionada);
	 }else{
		 var poliza=numeroPoliza.split(",");
		 mostrarDetallePolizaEstadoCuentaGrid(poliza[0]);
	 }
}

function inicializarComponentesAdministrarIngresos(){
	inicializarCombosAdmonIngresos();
	mostrarPolizaAdministrarIngresosGrid('undefined', 'undefined');
	mostrarDetallePolizaAdministrarIngresosGrid();
	mostrarGridListaIngresos();
}

function inicializarComponentesAdministrarEgresos(){
	inicializarCombosAdmonEgresos();
	mostrarPolizaAdministrarEgresosGrid("undefined", "undefined");
	mostrarDetallePolizaAdministrarEgresosGrid();
	mostraGridListaEgresos();
}

function buscarEstadosCuentaAdministrarIngresos(mensajeEjercicioNoSeleccionado){
	 var numeroPoliza = polizaAdministrarIngresosGrid.getCheckedRows(0);
	if ($('idEjercicio').value == ''){
		alert(mensajeEjercicioNoSeleccionado); 
	}else{
		sendRequest(document.administrarIngresosForm, '/MidasWeb/reaseguro/ingresos/obtenerEstadosCuenta.do?numeroPoliza='+numeroPoliza, null,'mostrarDetallePolizaAdministrarIngresosGrid('+ numeroPoliza +')');
	}
}

function buscarEstadosCuentaAdministrarEgresos() {
	var numeroPoliza = polizaAdministrarEgresosGrid.getCheckedRows(0);
	if($('idEjercicio').value) {
		if(numeroPoliza) {
			sendRequest(document.administrarEgresosForm, '/MidasWeb/reaseguro/egresos/obtenerEstadosCuenta.do?numeroPoliza='+ numeroPoliza, 
					null,'mostrarDetallePolizaAdministrarEgresosGrid('+ numeroPoliza +')');
		} else {
			var tipoMensaje = "20";
			var mensaje = "Por favor, seleccione una p\u00F3liza para efectuar la b\u00FAsqueda.";
			mostrarVentanaMensaje(tipoMensaje, mensaje, null);				 
		}
	} else {
		var tipoMensaje = "20";
		var mensaje = "Por favor, seleccione un ejercicio para efectuar la b\u00FAsqueda.";
		mostrarVentanaMensaje(tipoMensaje, mensaje, null);				  
	}	 
}

function seleccionarTodosEdosCtaEgresos(seleccionado){	
	if(seleccionado){
		detallePolizaAdministrarEgresosGrid.checkAll();			
	}else{
		detallePolizaAdministrarEgresosGrid.uncheckAll();		
	}	
	calcularSaldoTotalEdosCtaEgresos();
}

function calcularSaldoTotalEdosCtaEgresos(rId, cId, state){
	if(state=="0")
		uncheckSeleccionarTodos();
	
	var checkedRowsIdsStr = detallePolizaAdministrarEgresosGrid.getCheckedRows(0);	
	var checkedRowsIds = checkedRowsIdsStr.split(",");	
	var saldoTecnicoTotal = 0; 
	var saldoTecnicoEstadoCuenta = 0;
	var moneda = "";
	var monedasDistintas = false;
	for(i=0;i<checkedRowsIds.length;i++){
		if(checkedRowsIds[i]!==null && checkedRowsIds[i]!==""){			
			rowIndex = detallePolizaAdministrarEgresosGrid.getRowIndex(checkedRowsIds[i]);
			
			//Se valida que exista una sola moneda, de otra forma no se puede sumar el saldo total.
			if(moneda===""){
				moneda = detallePolizaAdministrarEgresosGrid.cellByIndex(rowIndex, 9).getValue();
			}else if(moneda !== detallePolizaAdministrarEgresosGrid.cellByIndex(rowIndex, 9).getValue()){
				monedasDistintas = true;
				break;
			}
							
			saldoTecnicoEstadoCuenta = parseFloat(detallePolizaAdministrarEgresosGrid.cellByIndex(rowIndex, 11).getValue());		
			saldoTecnicoTotal += saldoTecnicoEstadoCuenta;
		}
	}	
	if(!monedasDistintas)
		$("saldoTecnicoTotal").innerHTML = formatCurrency4Decs(saldoTecnicoTotal);
	else
		$("saldoTecnicoTotal").innerHTML = "Monedas distintas";
}

function uncheckSeleccionarTodos(){
	$('seleccionarTodosChk').checked = false;
}

function mostrarOcultarDivsIngresosEgresos(divObj, divObj2){
	if(divObj.style.display == 'block'){
		divObj.style.display = 'none';
		divObj2.style.display = 'none';
		document.getElementById('botonMostrarOcultarDiv').src="/MidasWeb/img/add_green.png";
	}else
		if(divObj.style.display == 'none'){
			divObj.style.display = 'block';
			divObj2.style.display = 'block'; 
			document.getElementById('botonMostrarOcultarDiv').src="/MidasWeb/img/minus_green.png";
		}
}

function validarRelacionIngresoEdosCta(){
	var idsEdosCta = detallePolizaAdministrarIngresosGrid.getCheckedRows(0);
	var idIngreso = listaIngresosReaseguro.getCheckedRows(0);
	var mensaje = "";
	
	if (idIngreso.length == 0)
		mensaje = "Por favor seleccione un ingreso de la lista de la derecha";
	
	if (mensaje.length > 0)
		alert(mensaje);
	else{
		sendRequest(null, '/MidasWeb/reaseguro/ingresos/validarRelacionIngresoEdosCta.do?idsEdosCta='+idsEdosCta+'&idIngreso='+idIngreso, null, 'mostrarRegistrarIngreso(transport.responseXML,"'+ idsEdosCta +'","'+idIngreso+'")');
	}
}

function validarRelacionEgresoEdosCta(){
	var idsEdosCta = detallePolizaAdministrarEgresosGrid.getCheckedRows(0);
	var idEgreso = listaEgresosReaseguro.getCheckedRows(0);
	var mensaje = "";
	
	if (idsEdosCta.length == 0){
		tipoMensaje = "20";
		mensaje = "Por favor seleccione por lo menos un estado de cuenta de la lista.";
		mostrarVentanaMensaje(tipoMensaje, mensaje, null);		
	}else{
		sendRequest(null, '/MidasWeb/reaseguro/egresos/validarRelacionEgresoEdosCta.do?idsEdosCta='+idsEdosCta, null, 'mostrarRegistrarEgreso(transport.responseXML,"'+ idsEdosCta +'");reiniciarGridsEgresos();');
	}
}
/*
function validarBorrarEgresoEdosCta(){
	var idsEdosCta = detallePolizaAdministrarEgresosGrid.getCheckedRows(0);
	var idEgreso = listaEgresosReaseguro.getCheckedRows(0);
	var mensaje = "";
	if (idEgreso.length == 0){
		alert("Por favor seleccione uno Egreso de la lista inferior");
	}else{
		if (confirm("� Deseas eliminar el Egreso seleccionado ?")) {
			sendRequest(null, '/MidasWeb/reaseguro/egresos/validarBorraRelacionEgresoEdosCta.do?idEgreso='+idEgreso, null, 'procesarRespuestaXml(transport.responseXML,"mostraGridListaEgresos()")');			
		}
	}
}
*/
function BorrarEgresoEdosCta(){
	var idEgreso = listaEgresosReaseguroCancel.getCheckedRows(0);
	var mensaje = "";
	if (idEgreso.length == 0){
		alert("Por favor seleccione uno Egreso de la lista.");
	}else{
		if (confirm("� Deseas eliminar el Egreso seleccionado ?")) {
			sendRequest(null, '/MidasWeb/reaseguro/egresos/borraRelacionEgresoEdosCta.do?idEgreso='+idEgreso, null, 'procesarRespuestaXml(transport.responseXML,"mostraGridListaEgresosCancel()")');			
		}
	}
}


function mostrarRegistrarIngreso(xmlDoc, idsEdosCta, idIngreso){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if(parseInt(id) == 30){
		sendRequest(null, '/MidasWeb/reaseguro/ingresos/mostrarIngreso.do?idIngreso='+idIngreso+'&idsEdosCta='+idsEdosCta, 'contenido', 'mostrarEstadosCuentaRelacionarIngresoGrid("'+idsEdosCta+'","'+idIngreso+'")');
	}else{
		procesarRespuestaXml(xmlDoc, null);
		//alert(mensaje);
	}
}

function mostrarRegistrarEgreso(xmlDoc, idsEdosCta){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if(parseInt(id) == 30){
		sendRequest(null, '/MidasWeb/reaseguro/egresos/mostrarRegistrarEgreso.do?idsEdosCta='+idsEdosCta, 'contenido', 'cargarCalendarioAgregarEgreso();mostrarEstadosCuentaEgresoGrid("'+idsEdosCta+'")');
	}else{
		procesarRespuestaXml(xmlDoc, null);
	}
}

function mostrarCoberturasEstadoCuenta(){	
	idsEdosCta = gridEstadosCuentaFacultativos.getCheckedRows(0);	
	if(idsEdosCta.length>0){
		//ocultarDivReaseguro("coberturasEdosCuentaFacultativos");
		ocultarDivReaseguro("exhibicionesCoberturasEdosCuentaFacultativos");
		mostrarDivReaseguro("coberturasEdosCuentaFacultativos");
		mostrarCoberturasEstadosCuentaFacultativosGrid(idsEdosCta);
		actualizarMontoEgreso();
	}else{
		idMensajeInfo = "20";
		mensaje = "Por favor, seleccione al menos un estado de cuenta de la lista." 
		mostrarVentanaMensaje(idMensajeInfo, mensaje, null);		
	}
}

function mostrarExhibicionesCoberturaEstadoCuenta(){
	idsCobRea = gridCoberturasEstadosCuentaFacultativos.getCheckedRows(0);
	if(idsCobRea.length>0){
		//ocultarDivReaseguro("exhibicionesCoberturasEdosCuentaFacultativos");
		mostrarDivReaseguro("exhibicionesCoberturasEdosCuentaFacultativos");
		mostrarExhibicionesCoberturasEstadosCuentaFacultativosGrid(idsCobRea);	
		actualizarMontoEgreso();		
	}else{
		idMensajeInfo = "20";
		mensaje = "Por favor, seleccione al menos una cobertura de la lista."; 
		mostrarVentanaMensaje(idMensajeInfo, mensaje, null);		
	}
}

function cargarComboSeccionesEgreso(){
	child = "idToSeccion";
	action = "/MidasWeb/reaseguro/egresos/cargarComboSecciones.do"
	new Ajax.Request(action, {
		method : "post",
		asynchronous : false,
		parameters : null,
		onSuccess : function(transport) {
			loadCombo(child, transport.responseXML);
		} // End of onSuccess
	});
	removeAllOptionsExt($("idToCobertura"));
}


function modificarMontoTotalEgreso(montoTotalEgreso){
	$('montoEgresoValor').value = montoTotalEgreso;
	$("monto").value = formatCurrency4Decs(montoTotalEgreso);	
}

function cambiarEstatusSeleccionExhibiciones(estatusSeleccion){	
	var idToSeccion = $("idToSeccion").value;
	var idToCobertura = $("idToCobertura").value;	
	if(idToSeccion !== ""){
		gridExhibicionesCoberturasEstadosCuentaFacultativos.forEachRow(			
				function(id){
					var cellChkPagar = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,11);
					var cellCandidatoAPagar = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,13);
					var cellIdToSeccion = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,14);
					var cellIdToCobertura = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,15);
					if(cellCandidatoAPagar.getValue() === "1"){
						modificar = false;
						
						if(cellIdToSeccion.getValue() === idToSeccion)
							modificar = true;													
						
						if(idToCobertura!=="" && cellIdToCobertura.getValue() !== idToCobertura)
							modificar = false;													
						
						if(modificar)
							cellChkPagar.setValue(estatusSeleccion);																		
					}
				}	
		);
		actualizarMontoEgreso();
	}
}


function relacionarIngresoReaseguro(mensajeErrorComboConcepto){
	var idsEdoCtaMonto = '';
	var id = '';
	var idIngreso = $('idIngreso').value;
	var monto;
	var lengthCell = 0;
	if ($('tipoCambio') != null)
		fromCurrencyToFloat($('tipoCambio'));
	if ($('idConcepto').value != ''){
		for (var i=0; i<listaEstadosCuentaRelacionarIngreso.getRowsNum(); i = i + 1){	     	     	     
			id = listaEstadosCuentaRelacionarIngreso.getRowId(i);
			cellMonto = listaEstadosCuentaRelacionarIngreso.cellByIndex(i,6);
			if (id != -1){
				if (cellMonto.getValue() != ''){
					lengthCell = cellMonto.getValue().length;
					monto = cellMonto.getValue();
					monto = monto.replace("$", "");
					for (i2 = 0; i2 < lengthCell; i2++){
						monto = monto.replace(",", "");
					}
					
					idsEdoCtaMonto = idsEdoCtaMonto + id + "_" + monto;
				}else
					idsEdoCtaMonto = idsEdoCtaMonto + id + "_0.0";
			     
			     if(i !== listaEstadosCuentaRelacionarIngreso.getRowsNum()-1)
			    	 idsEdoCtaMonto = idsEdoCtaMonto + "/";
			}
		     
		}
		if($('idReferenciaExterna').value != ''){
			sendRequest(document.administrarIngresosForm, '/MidasWeb/reaseguro/ingresos/relacionarIngreso.do?idsEdoCtaMonto='+idsEdoCtaMonto+'&idIngreso='+idIngreso, null, 'respuestaXMLRelacionarIngreso(transport.responseXML)');
		}
		else
			alert('Seleccione una referencia para aplicar el ingreso.');
	}else{
		alert(mensajeErrorComboConcepto);
	}
}

function respuestaXMLRelacionarIngreso(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	var afterClose = '';
	if(parseInt(id) == 30){
		afterClose = 'closeMostrarRegistrarIngreso()';
	}else{
		afterClose = null;
	}
	
	procesarRespuestaXml(xmlDoc, afterClose);
}

function registrarEgresoReaseguro(){
	var idsEdoCtaMonto = '';
	var idsEdoCtaAutomaticos_Montos = "";
	if($("mostrarGridEdosCuentaAutomaticos").value==="true"){
		//Cada rengl�n tiene el id en el siguiente formato:
		//idEstadoCuenta_SaldoTecnico$idEstadoCuenta_SaldoTecnico$idEstadoCuenta_SaldoTecnico...
		idsEdoCtaAutomaticos_Montos = gridEstadosCuentaAutomaticos.getCheckedRows(0);		
	}
		
	var idsExhibicionesEdoCtaFacultativos = ""; 
	if($('mostrarGridEdosCuentaFacultativos').value === "true" && gridExhibicionesCoberturasEstadosCuentaFacultativos != null){
		//Cada rengl�n tiene el id en el siguiente formato:
		//idToPlanPagosCobertura-numeroExhibicion-idReasegurador
		idsExhibicionesEdoCtaFacultativos = gridExhibicionesCoberturasEstadosCuentaFacultativos.getCheckedRows(11);			
	}	
	
	var idsEdoCtaMonto = idsEdoCtaAutomaticos_Montos + "|" + idsExhibicionesEdoCtaFacultativos;
	
	
	/*
	var id = '';
	var monto;
	var lengthCell = 0;
	if ($('tipoCambio') != null)
		fromCurrencyToFloat($('tipoCambio'));
		
	for (var i=0; i<listaEstadosCuentaRelacionarEgreso.getRowsNum(); i = i + 1){	     	     	     
		id = listaEstadosCuentaRelacionarEgreso.getRowId(i);
		cellMonto = listaEstadosCuentaRelacionarEgreso.cellByIndex(i,6);
		if (id != -1){
			if (cellMonto.getValue() != ''){
				lengthCell = cellMonto.getValue().length;
				monto = cellMonto.getValue();
				monto = monto.replace("$", "");
				for (i2 = 0; i2 < lengthCell; i2++){
					monto = monto.replace(",", "");
				}
				
				idsEdoCtaMonto = idsEdoCtaMonto + id + "_" + monto;
			}else
				idsEdoCtaMonto = idsEdoCtaMonto + id + "_0.0";
		     
		     if(i !== listaEstadosCuentaRelacionarEgreso.getRowsNum()-1)
		    	 idsEdoCtaMonto = idsEdoCtaMonto + "/";
		}
	     
	}
	*/
	
	//fromCurrencyToFloat($('monto'));
	//Se agrega validacion al dar click en el bot�n de Programar Pago
	if($('montoEgresoValor').value=="" || $('montoEgresoValor').value==parseInt(0)){
		mostrarVentanaMensaje('10','El monto del Egreso por estado de cuenta debe ser mayor a cero' , null);
	}else{
		sendRequest(document.administrarEgresosForm, '/MidasWeb/reaseguro/egresos/agregarEgreso.do?idsEdoCtaMonto='+idsEdoCtaMonto, null, 'respuestaXMLRegistrarEgreso(transport.responseXML)');
	}
	//formatCurrency4Decs($('monto').value);
	//formatCurrency4Decs($('tipoCambio').value);
}

function respuestaXMLRegistrarEgreso(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	var afterClose = '';
	if(parseInt(id) == 30){
		afterClose = 'closeMostrarRegistrarEgreso()';
	}else{
		afterClose = null;
	}
	
	procesarRespuestaXml(xmlDoc, afterClose);
}

function seleccionarContratoCuotaParte(){
	var checkedId = contratos.getCheckedRows(0);
	if (checkedId.length > 0){
		sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/seleccionar.do', 'contenido','initParticipacionesCPGrids(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids(),formatearMontosRegistrarLinea()');
	}else{
		alert('Seleccione un contrato de la lista');
	}
}

function seleccionarContratoPrimerExcedente(){
	var checkedId = contratosPrimerExcedente.getCheckedRows(0);
	if (checkedId.length > 0){
		sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/seleccionar.do', 'contenido','initParticipacionesPEGrids(),mostrarParticipacionesPEGrids(), initParticipacionesCPGrids_recargarTemporal(), mostrarParticipacionesCPGrids(),formatearMontosRegistrarLinea()');
	}else{
		alert('Seleccione un contrato de la lista');
	}
}

var fechaEgreso;
function cargarCalendarioAgregarEgreso(){
		fechaEgreso = new dhtmlxCalendarObject("calendarioDiv");
		fechaEgreso.hide();
		fechaEgreso.attachEvent("onClick",function(fecha){
			$('fecha').value=fechaEgreso.getFormatedDate(formatoFechaCalendario(),fecha);
			mostrarCalendarioAgregarEgreso();
		});
}

function mostrarCalendarioAgregarEgreso(){
	if (fechaEgreso!=null){
		if (fechaEgreso.isVisible()){
			fechaEgreso.hide();
		}else{
			fechaEgreso.show();
		}
	}
}

function confirmarPagoAdministrarEgresosReaseguro(){
	var checkedId = listaEgresosReaseguro.getCheckedRows(0);
	if (checkedId.length > 0){
		if(confirm('\u00BFDeseas Confirmar el Pago?')){
		   sendRequest(null,'/MidasWeb/reaseguro/egresos/confirmarPago.do?idEgreso='+checkedId, null,'procesarRespuestaXml(transport.responseXML, null),mostraGridListaEgresos()');
		}
	}else{
		alert('Seleccione un Egreso de la lista de la derecha');
	}
}

function cargarEnNuevaVentana(accion, titulo){
	window.open(accion, '_blank', titulo);
}

function desasociarContratoCPLinea(){
	if ($('folioContratoCuotaParte').value != '' && $('folioContratoCuotaParte').value != '0'){
		if (confirm('Desea desasociar el contrato Cuota Parte de esta L\u00EDnea?'))
			sendRequest(document.configuracionLineaForm, '/MidasWeb/contratos/linea/desasociarContratoCPLinea.do',null,'resultadoDesasociarContratoCPLinea(transport.responseXML)');
	}
}

function resultadoDesasociarContratoCPLinea(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	
	if(parseInt(id) == 30){
		$('folioContratoCuotaParte').value = '';
		$('idTmContratoCuotaParte').value = '';
		$('participacionesCP').value = '';
		$('porcentajeDeCesion').value = '0';
		procesarCalculoMontosLinea();
		initParticipacionesCPGrids();
		mostrarParticipacionesCPGrids();
	}
	procesarRespuestaXml(xmlDoc, null);
	
}

function desasociarContratoPELinea(){
	if ($('folioContratoPrimerExcedente').value != '' && $('folioContratoPrimerExcedente').value != '0'){
		if (confirm('Desea desasociar el contrato Primer Excedente de esta L\u00EDnea?'))
			sendRequest(document.configuracionLineaForm, '/MidasWeb/contratos/linea/desasociarContratoPELinea.do',null,'resultadoDesasociarContratoPELinea(transport.responseXML)');
	}
}

function resultadoDesasociarContratoPELinea(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	
	if(parseInt(id) == 30){
		$('folioContratoPrimerExcedente').value = '';
		$('idTmContratoPrimerExcedente').value = '';
		$('participacionesPE').value = '';
		$('montoPleno').value = '0';
		$('numeroPlenos').value = '0';
		procesarCalculoMontosLinea();
		initParticipacionesPEGrids();
		mostrarParticipacionesPEGrids();
	}
	procesarRespuestaXml(xmlDoc, null);
	
}


function notificacionResultadoOperacionModificado(){
	var mensaje;
	var tipoMensaje;
	var img;
	if ($('mensaje') !== null && $('tipoMensaje') !== null){
		mensaje = $('mensaje').value;
		tipoMensaje = $('tipoMensaje').value;
		if (parseInt(tipoMensaje) == 30)
			img = '<img alt="Exito" src="/MidasWeb/img/okIcon.gif" />';
		else
			img = '<img alt="Error" src="/MidasWeb/img/errorIcon.gif" />';
		
		if (document.getElementById('mensajeResultadoOperacion') !== null){
			document.getElementById('mensajeResultadoOperacion').innerHTML = img + "&nbsp;" + mensaje;
			document.getElementById('mensajeResultadoOperacion').style.display="block";
		}
	}
}

function procesarCalculoMontosLinea(){
	eliminarFormatoMontosRegistrarLinea();
	calcularDatosRegistrarLinea();
	formatearMontosRegistrarLinea();
}

function formatearMontosRegistrarLinea(){
	validarMontoRegistrarEgreso($('maximo'));
	validarMontoRegistrarEgreso($('cesionCuotaParte'));
	validarMontoRegistrarEgreso($('cesionPrimerExcedente'));
	validarMontoRegistrarEgreso($('capacidadMaximaLinea'));
	validarMontoRegistrarEgreso($('montoPleno'));
	validarMontoRegistrarEgreso($('totalRetencion'));
	validarMontoRegistrarEgreso($('totalCedido'));
}

function eliminarFormatoMontosRegistrarLinea(){
	fromCurrencyToFloat($('maximo'));
	fromCurrencyToFloat($('cesionCuotaParte'));
	fromCurrencyToFloat($('cesionPrimerExcedente'));
	fromCurrencyToFloat($('capacidadMaximaLinea'));
	fromCurrencyToFloat($('montoPleno'));
	fromCurrencyToFloat($('totalRetencion'));
	fromCurrencyToFloat($('totalCedido'));
}

function fromCurrencyToFloat(objetoTXT){
	var valueToConvert = objetoTXT.value;
	if(valueToConvert != ''){
		valueToConvert = valueToConvert.replace("$", "");
		var cont = valueToConvert.length;
		for (i = 0; i < cont; i++)
			valueToConvert = valueToConvert.replace(",", "");
	}
	objetoTXT.value = valueToConvert;
}

function convertirValorDeMonedaAFlotante(valueToConvert){
	if(valueToConvert != ''){
		valueToConvert = valueToConvert.replace("$", "");
		var cont = valueToConvert.length;
		for (i = 0; i < cont; i++)
			valueToConvert = valueToConvert.replace(",", "");
	}else{
		valueToConvert = "0";
	}
	
	return valueToConvert;
}

function validarMontoRegistrarEgreso(objetoTXT){
	if (objetoTXT != null){
		var valor = objetoTXT.value;
		if(valor != ''){
			valor = valor.replace("$", "");
			var cont = valor.length;
			for (i = 0; i < cont; i++)
				valor = valor.replace(",", "");
		}else{
			valor = '0';
		}
		
		//JL 09/06/2011 Se agrega validacion para permitir numeros negativos
		if (isNumeric(valor) || isFloat(valor) || esNumeroNegativo(valor)){
			objetoTXT.value = formatCurrency(valor);
		}else{
			alert('La cifra proporcionada no es v\u00e1lida');
			objetoTXT.value = '$0.00';
		}
	}
}

function mostrarReporteMovtosPorReaseguradorParametro(numeroPoliza, tipoReporte){	
	var pIdMoneda;
	var pFechaInicial =$('fechaInicial').value;
	var pFechaFinal =$('fechaFinal').value;
	var url;
	var mensaje='';
	if ($('pIdMoneda') != null){
		pIdMoneda = $('pIdMoneda').value;
	}else{
		pIdMoneda=-1;
	}
	if ($('fechaInicial') != null && $('fechaFinal') != null){
		pFechaInicial = $('fechaInicial').value;
		pFechaFinal = $('fechaFinal').value;
		if($('fechaInicial').value == '' || $('fechaFinal').value == ''){
			mensaje = "Por favor seleccione un rango de fechas";
		}
	}
	url = "/MidasWeb/contratos/movimiento/rptMovtosPorReasegurador.do";
	url = url + "?pIdToPoliza="+numeroPoliza+"&tipoReporte="+tipoReporte+"&pIdMoneda="+pIdMoneda+"&pFechaInicial="+pFechaInicial
	+"&pFechaFinal="+pFechaFinal;
	if (mensaje == '')
		cargarEnNuevaVentana(url, "Reporte Movimientos por Reasegurador");
	else
		alert(mensaje);
}

function mostrarReporteContratosPolizaParametro(numeroPoliza, tipoReporte){
	
	//alert('mostrarReporteContratosPolizaParametro(numeroPoliza, tipoReporte)');
	
	var pIdMoneda;
	var pFechaInicial = null;
	var pFechaFinal = null;
	if ($('pIdMoneda') != null)
		pIdMoneda = $('pIdMoneda').value;
	else
		pIdMoneda=-1;
	
	cargarEnNuevaVentana("/MidasWeb/contratos/movimiento/rptSoporteContratoPoliza.do?pIdToPoliza="+numeroPoliza+"&tipoReporte="+tipoReporte,"Soporte de Contratos de P\u00f3liza");
}

function mostrarReporteSoporteReaseguroEnPolizaNumeroPoliza(numeroPoliza, tipoReporte){
	var pIdMoneda;
	var pFechaInicial = null;
	var pFechaFinal = null;
	if ($('pIdMoneda') != null)
		pIdMoneda = $('pIdMoneda').value;
	else
		pIdMoneda=-1;
	cargarEnNuevaVentana("/MidasWeb/contratos/movimiento/rptSoporteContratoPoliza.do?pIdToPoliza=-1&tipoReporte="+tipoReporte+"&pFechaInicial="+pFechaInicial+"&pFechaFinal="+pFechaFinal, "Reporte Soporte Reaseguro en P\u00f3liza");
}

function cargarComponentesRptMovtosPorReasegurador(){
	mostrarPolizaAdministrarReporteGrid('undefined', 'undefined');
	manipulaCalendarioLineas();
	document.administrarEgresosForm.reporteAMostrar[0].checked=true;
}

function cargarComponentesRptSoporteContratoPoliza(){
	mostrarPolizaAdministrarReporteGridContrato('undefined', 'undefined');
}

function mostrarReporteSoporteReaseguroEnPoliza(){
	var pFechaInicial=null;
	var pFechaFinal=null;
	var band=true;
	var tipoReporte="PDF";
	if ($('pIdMoneda') != null)
		pIdMoneda = $('pIdMoneda').value;
	else
		pIdMoneda=-1;
	
	if ($('fechaInicial') != null){
		pFechaInicial = $('fechaInicial').value;
		if($('fechaInicial').value == '')
		band = false;
	}
	
	if ($('fechaFinal') != null){
		pFechaFinal = $('fechaFinal').value;
		if($('fechaFinal').value == '')
			band = false;
	}
	
	if ($('tipoReporte') != null){
		if($('tipoReporte').value != '')
			tipoReporte = $('tipoReporte').value;
	}
	
	if(band)
		cargarEnNuevaVentana("/MidasWeb/contratos/movimiento/rptSoporteContratoPoliza.do?pIdToPoliza=-1&tipoReporte="+tipoReporte+"&pFechaInicial="+pFechaInicial+"&pFechaFinal="+pFechaFinal, "Reporte Soporte Reaseguro en P\u00f3liza");
	else
		alert('Por favor seleccione un rango de fechas');
}



function guardarConfiguracionCoberturaContFacultativo(idTdContratoFacultativo){	
	var mensaje = '';
	 eliminarFormatoMontosRegistrarCotizacionFacultativa();
	if (!isNumeric($('primaFacultadaCobertura').value) && !isFloat($('primaFacultadaCobertura').value)){
		mensaje = '<br/>La Prima Facultada debe ser un n&uacute;mero v&aacute;lido';
	}
	//JL 09/06/2011 Se permite captura de prima negativa para emisi�n de nota de cr�dito.
	/*else{
		var primaFacultadaCoberturaTmp = parseFloat($('primaFacultadaCobertura').value);
		var primaTotalTmp = parseFloat($('primaTotalCobertura').value);
		if (primaFacultadaCoberturaTmp < primaTotalTmp){
			mensaje = '<br/>La Prima Facultada deber ser <b>mayor o igual</b> a la Prima Total';
		}
	}
	*/
	if($('primaAdicionalCobertura') != null){
		if(!isNumeric($('primaAdicionalCobertura').value) && !isFloat($('primaAdicionalCobertura').value)){
			mensaje = '<br/>La Prima Adicional de la Cobertura deber ser n&uacute;mero v&aacute;lido';
		}
	}
	if (!isNumeric($('deducibleCobertura').value) && !isFloat($('deducibleCobertura').value))
		mensaje = mensaje + '<br />El Deducible de Cobertura debe ser un n&uacute;mero v&aacute;lido';
		
	$('planPagosStr').value = obtenerPlanPagosStr(); 
	
	if (mensaje == '')
		sendRequest(document.detalleContratoFacultativoForm,'/MidasWeb/contratofacultativo/cobertura/guardarDatosDeLaCobertura.do', null, 'procesarRespuestaGuardarCoberturaContFacultativo(transport.responseXML); formatearMontosRegistrarCotizacionFacultativa(); mostrarEstatusAutorizacionPlanPagos();');
	else{
		mensaje = 'Los siguientes datos son incorrectos:' + mensaje;
		mostrarVentanaMensaje('10', mensaje, null);
	}
	
	
}

function obtenerPlanPagosStr(){
	planPagosStr = "";
	gridPlanPagosFacultativos.forEachRow(function(rowId){
		for (var i=0; i<gridPlanPagosFacultativos.getColumnsNum(); i++){
			planPagosStr = planPagosStr + gridPlanPagosFacultativos.cells(rowId,i).getValue() + "_";
            
        }
		planPagosStr = planPagosStr.substring(0, planPagosStr.length-1);		
		planPagosStr = planPagosStr + "|";
    });      
	planPagosStr = planPagosStr.substring(0, planPagosStr.length-1);	
	return planPagosStr;
}

function mostrarEstatusAutorizacionPlanPagos(){	
	new Ajax.Request('/MidasWeb/contratofacultativo/pagocobertura/obtenerEstatusAutorizacionPlanPagosCobertura.do?'+
					 'idTdContratoFacultativo='+$("idTdContratoFacultativo").value, {
		method : "post",
		encoding : "UTF-8",
		parameters : null,
		onCreate : function(transport) {		
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplete
		onSuccess : function(transport) {		
			actualizarEstatusAutorizacionPlanPagos(transport.responseXML);
		} // End of onSuccess		
	});
	return true;
}

function actualizarEstatusAutorizacionPlanPagos(docXml){	
	var items = docXml.getElementsByTagName("item");		
	if(items!=null  && items.length>0){
		var item = items[0];																
		$("estatusAutorizacionPlanPagos").value = item.getElementsByTagName("estatusAutorizacion")[0].firstChild.nodeValue.trim();				
						
		if($("estatusAutorizacionPlanPagos").value==="0"){
			document.getElementById("imgAutorizacionPlanPagos").src = "/MidasWeb/img/blank.gif";
			document.getElementById("imgAutorizacionPlanPagos").alt = "";
			document.getElementById("imgAutorizacionPlanPagos").title = "";
		}else if($("estatusAutorizacionPlanPagos").value==="1"){
			document.getElementById("imgAutorizacionPlanPagos").src = "/MidasWeb/img/ico_yel.gif";
			document.getElementById("imgAutorizacionPlanPagos").alt = "Requiere autorizaci�n";
			document.getElementById("imgAutorizacionPlanPagos").title = "Requiere autorizaci�n";
		}else if($("estatusAutorizacionPlanPagos").value==="7"){
			document.getElementById("imgAutorizacionPlanPagos").src = "/MidasWeb/img/ico_green.gif";
			document.getElementById("imgAutorizacionPlanPagos").alt = "Autorizado";
			document.getElementById("imgAutorizacionPlanPagos").title = "Autorizado";
		}else if($("estatusAutorizacionPlanPagos").value==="8"){
			document.getElementById("imgAutorizacionPlanPagos").src = "/MidasWeb/img/ico_red.gif";
			document.getElementById("imgAutorizacionPlanPagos").alt = "Rechazado";
			document.getElementById("imgAutorizacionPlanPagos").title = "Rechazado";
		}
	}
}

function procesarRespuestaGuardarCoberturaContFacultativo(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var descripcionXml = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	var mensaje = descripcionXml;
	var idTmContratoFacultativo;
	var idTdContratoFacultativo;
	if (tipoMensaje == '40'){
		tipoMensaje = '30';
		descripcionXml = descripcionXml.split('_');
		for (i = 0; i < descripcionXml.length; i=i+1){
			if (i == 0)
				mensaje = descripcionXml[0];
			if (i == 1){
				idTmContratoFacultativo = parseInt(descripcionXml[1]);
			}
			
			if (i > 1){
				idTdContratoFacultativo = parseInt(descripcionXml[i]);
			}
		}
		recargaCoberturasFacultativaTree(idTmContratoFacultativo, idTdContratoFacultativo);	
	}
	mostrarVentanaMensaje(tipoMensaje, mensaje, null);
}

function formatearMontosRegistrarCotizacionFacultativa(){
	validarMontoRegistrarEgreso($('primaFacultadaCobertura'));
	validarMontoRegistrarEgreso($('primaTotalCobertura'));
	validarMontoRegistrarEgreso($('sumaFacultada'));
	validarMontoRegistrarEgreso($('primaNoDevengada'));
}

function eliminarFormatoMontosRegistrarCotizacionFacultativa(){
	fromCurrencyToFloat($('primaFacultadaCobertura'));
	fromCurrencyToFloat($('primaTotalCobertura'));
	fromCurrencyToFloat($('sumaFacultada'));
	if ($('primaNoDevengada') != null){
		fromCurrencyToFloat($('primaNoDevengada'));
	}
	if ($('montoPrimaAdicional') != null){
		fromCurrencyToFloat($('montoPrimaAdicional'));
	}
}

function mostrarReporteOrdenPagoReaseguro(){
	var checkedId = listaEgresosReaseguro.getCheckedRows(0);
	if (checkedId.length > 0){
		cargarEnNuevaVentana('/MidasWeb/reaseguro/egresos/rptOrdenPagoReaseguro.do?idToEgresoReaseguro='+checkedId, "Reporte Orden de Pago");
	}else{
		alert('Seleccione un Egreso de la lista de la derecha');
	}
}

function mostrarReporteSoporteOrdenPagoReaseguro(){
	var checkedId = listaEgresosReaseguro.getCheckedRows(0);
	if (checkedId.length > 0){
		cargarEnNuevaVentana('/MidasWeb/reaseguro/egresos/obtenerReporteSoporteOrdenPago.do?idToEgresoReaseguro='+checkedId, "Reporte Soporte Orden de Pago");
	}else{
		alert('Seleccione un Egreso de la lista de la derecha');
	}
}

function mostrarIndicadorCargaGenerico(divName){
	var newHtml = '<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Procesando la informaci&oacute;n, espere un momento por favor...</font>';
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).innerHTML = newHtml;
		document.getElementById(divName).style.display='block';
	}
}
function ocultarIndicadorCargaGenerico(divName){
	if(document.getElementById(divName) != null){
		document.getElementById(divName).style.display='none';
		document.getElementById(divName).innerHTML = '';
	}
}


function completaElCien(indicador){
	if(indicador == 1){
		var ret = document.contratoCuotaParteForm.porcentajeDeRetencion.value;
		var valor = ((100.0 - parseFloat(ret))+"").substr(0,7);
		if(valor >= 0.0 && valor <= 100.0)
			document.contratoCuotaParteForm.porcentajeDeCesion.value = valor;
		else 
			document.contratoCuotaParteForm.porcentajeDeCesion.value = "";
	}else if(indicador == 2){
		var ret = document.contratoCuotaParteForm.porcentajeDeCesion.value;
		var valor = ((100.0 - parseFloat(ret))+"").substr(0,7);
		if(valor >= 0.0 && valor <= 100.0)
			document.contratoCuotaParteForm.porcentajeDeRetencion.value = valor;
		else
			document.contratoCuotaParteForm.porcentajeDeRetencion.value = "";
	}
}

function validarDetalleParticipacionFacultativa(){
	sendRequest(null,'/MidasWeb/contratofacultativo/contrato/validarDetalleParticipacionFacultativa.do',null,'procesarRespuestaXMLAgregarParticipacionFac(transport.responseXML)');
}

function procesarRespuestaXMLAgregarParticipacionFac(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var descripcionXml = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if (tipoMensaje == '10'){
		//procesarRespuestaXml(xmlDoc, null);
		alert(descripcionXml);
	}
}

function cargarComponentesRptEstadisticaFiscalFacultativo(){
	manipulaCalendarioLineas();
	mostrarPolizaRptEstadisticaFiscalFacultativoGrid('undefined', 'undefined');
}

function actualizarFiltroReporteEstadisticaFiscalFacultativo(){
	switch (parseInt($('comboTipoReporte').value)){
	case 0:
		$('divBusquedaPorReasegurador').style.display='none';
		$('busquedaPorPoliza01').style.display='none';
		$('busquedaPorPoliza02').style.display='none';
		$('busquedaPorPoliza03').style.display='none';
		$('b_buscar').style.display='none';
		$('polizaRptEstadisticaFiscalFacultativoGrid').style.display='none';
		break;
	case 1:
		$('divBusquedaPorReasegurador').style.display='block';
		$('busquedaPorPoliza01').style.display='none';
		$('busquedaPorPoliza02').style.display='none';
		$('busquedaPorPoliza03').style.display='none';		
		$('b_buscar').style.display='none';
		$('polizaRptEstadisticaFiscalFacultativoGrid').style.display='none';
		break;
	case 2:
		$('divBusquedaPorReasegurador').style.display='none';
		$('busquedaPorPoliza01').style.display='block';
		$('busquedaPorPoliza02').style.display='block';
		$('busquedaPorPoliza03').style.display='block';
		$('b_buscar').style.display='block';
		$('polizaRptEstadisticaFiscalFacultativoGrid').style.display='block';
	}
}

function mostrarReporteEstadisticaFiscalFacultativo(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var idPoliza;
	var idReaseguradorCorredor;
	var mensaje = '';
	if (fechaInicial != '' && fechaFinal != ''){
		switch (parseInt($('comboTipoReporte').value)){
		case 1:
			idReaseguradorCorredor = $('idtcReaseguradorCorredor').value;
			idPoliza = '';
			if (idReaseguradorCorredor == '')
				mensaje = 'Debe elegir un Reasegurador/Corredor';
			break;
		case 2:
			idPoliza = polizaRptEstadisticaFiscalFacultativoGrid.getCheckedRows(0);
			idReaseguradorCorredor = '';
			if (idPoliza == '')
				mensaje = 'Elige una P\u00f3liza de la lista';
		}
		if (mensaje == '')
			cargarEnNuevaVentana('/MidasWeb/contratofacultativo/rptEstadisticaFiscal.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&idReaseguradorCorredor='+idReaseguradorCorredor+'&idPoliza='+idPoliza, 'Reporte Estad\u00edstica Fiscal de Facultativo');
		else
			alert(mensaje);
	}else{
		alert('Debe elegir un rango de fechas correcto');
	}
}

function cargarComponentesRptEstadisticaFiscalAutomaticos(){
	manipulaCalendarioLineas();
	$('tipoParticipante').value = 1;
	llenarCombo($('tipoParticipante'),'idtcReaseguradorCorredor','/MidasWeb/reaseguradorCorredor.do');
}

function actualizarFiltroReporteEstadisticaFiscalAutomaticos(){
	switch (parseInt($('comboTipoReporte').value)){
	case 0:
		$('divBusquedaPorReasegurador').style.display='none';
		break;
	case 1:
		$('divBusquedaPorReasegurador').style.display='block';
		break;
	case 2:
		$('divBusquedaPorReasegurador').style.display='none';
	}
}

function mostrarReporteEstadisticaFiscalAutomaticos(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var idReaseguradorCorredor;
	var mensaje = '';
	if (fechaInicial != '' && fechaFinal != ''){
		switch (parseInt($('comboTipoReporte').value)){
		case 1:
			idReaseguradorCorredor = $('idtcReaseguradorCorredor').value;
			if (idReaseguradorCorredor == '')
				mensaje = 'Debe elegir un Reasegurador/Corredor';
		}
		if (mensaje == '')
			cargarEnNuevaVentana('/MidasWeb/contratos/rptEstadisticaFiscal.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&idReaseguradorCorredor='+idReaseguradorCorredor, 'Reporte Estad\u00edstica Fiscal de Contratos Autom\u00e1ticos');
		else
			alert(mensaje);
	}else{
		alert('Debe elegir un rango de fechas correcto');
	}
}

function ocultaMovimientoSiniestroConReaseguro(opcionReporteMovimientoSiniestroConReaseguro){
	if(opcionReporteMovimientoSiniestroConReaseguro == '1'){
		document.getElementById('numeroSiniestro').style.display = 'none';
		document.getElementById('tipoMovimiento').style.display = 'none';
		document.getElementById('reasegurador').style.display = 'none';
		document.getElementById('porFechas').style.display = '';
	}
	else if(opcionReporteMovimientoSiniestroConReaseguro == '2'){
		document.getElementById('numeroSiniestro').style.display = '';
		document.getElementById('tipoMovimiento').style.display = 'none';
		document.getElementById('reasegurador').style.display = 'none';
		document.getElementById('porFechas').style.display = 'none';
	}
	else if(opcionReporteMovimientoSiniestroConReaseguro == '3'){
		document.getElementById('numeroSiniestro').style.display = 'none';
		document.getElementById('tipoMovimiento').style.display = '';
		document.getElementById('reasegurador').style.display = 'none';
		document.getElementById('porFechas').style.display = 'none';
	}
	else if(opcionReporteMovimientoSiniestroConReaseguro == '4'){
		document.getElementById('numeroSiniestro').style.display = 'none';
		document.getElementById('tipoMovimiento').style.display = 'none';
		document.getElementById('reasegurador').style.display = '';
		document.getElementById('porFechas').style.display = 'none';
	}
}

function ocultaMovimientoSiniestroReservasPendientes(opcionReporteMovimientoSiniestroReservasPendientes){
	if(opcionReporteMovimientoSiniestroReservasPendientes == '1'){
		document.getElementById('porFechas').style.display = '';
		document.getElementById('ramoOficial').style.display = 'none';
	}
	else if(opcionReporteMovimientoSiniestroReservasPendientes == '2'){
		//document.getElementById('porFechas').style.display = 'none';
		//document.getElementById('ramoOficial').style.display = '';
	}
}

function actualizarFiltroReporteMovimientosPorReasegurador(){
	switch (parseInt($('comboTipoReporte').value)){
	case 0:
		$('divBusquedaPolizas').style.display='none';
		$('divBusquedaSubRamo').style.display='none';
		break;
	case 1:
		$('divBusquedaPolizas').style.display='block';
		$('divBusquedaSubRamo').style.display='none';
		break;
	case 2:
		$('divBusquedaPolizas').style.display='none';
		$('divBusquedaSubRamo').style.display='block';
	}
}

function mostrarReporteMovtosPorReasegurador(tipoReporte){
	var pFechaInicial=null;
	var pFechaFinal=null;
	var mensaje='';
	var pIdToPoliza;
	var pIdRamo;
	var pIdSubRamo;
	var rptDetallado = document.administrarEgresosForm.reporteAMostrar[1].checked?1:0;
	var conRetencion = $('comboRetencion').value;
	var url;
	
	if ($('pIdMoneda') != null)
		pIdMoneda = $('pIdMoneda').value;
	else
		pIdMoneda=-1;
	
	if ($('fechaInicial') != null && $('fechaFinal') != null){
		pFechaInicial = $('fechaInicial').value;
		pFechaFinal = $('fechaFinal').value;
		if($('fechaInicial').value == '' || $('fechaFinal').value == ''){
			mensaje = "Por favor seleccione un rango de fechas";
		}else{
			switch (parseInt($('comboTipoReporte').value)){
			case 1:
				pIdToPoliza = polizaAdministrarReporteGrid.getCheckedRows(0);
				if (pIdToPoliza == '')
					mensaje = 'Por favor seleccione una P\u00f3liza';
				
				break;
			case 2:
				pIdRamo = $('idTcRamo').value;
				pIdSubRamo =$('idTcSubRamo').value;
				if (pIdSubRamo == '')
					mensaje = "Por favor seleccione un Sub Ramo";
			}
		}
	}
	
	url = "/MidasWeb/contratos/movimiento/rptMovtosPorReasegurador.do";
	url = url + "?pIdToPoliza="+pIdToPoliza+"&tipoReporte="+tipoReporte+"&pIdMoneda="+pIdMoneda+"&pFechaInicial="+pFechaInicial
		+"&pFechaFinal="+pFechaFinal+"&pIdRamo="+pIdRamo+"&pIdSubRamo="+pIdSubRamo+"&rptDetallado="+rptDetallado+"&conRetencion="+conRetencion;
	if(tipoReporte == 'TXT')
		url += "&metodoObjetos=true"
	if (mensaje == '')
		cargarEnNuevaVentana(url, "Reporte Movimientos por Reasegurador");
	else
		alert(mensaje);
	
}

function validarFormatoNumeroPolizaRptsReaseguro(numPoliza){
	var polizaPattern= /^\d{4}-\d{6}-\d{2}$/;
	var bandera = true;
	if (numPoliza != 'undefined' && numPoliza != ''){
		if(numPoliza.length<14 || !numPoliza.match(polizaPattern)){
			alert("La P\u00f3liza debe cumplir con el formato adecuado (9999-999999-99)");
			ocultarIndicadorCargaGenerico('loadingPolizas');
			bandera = false;
		}
	}
	return bandera;
}

function mostrarFiltroReportePerfilCartera(){
	manipulaCalendarioDobleRptPerfilCartera();
	reportePerfilCarteraGrid();
}

function mostrarFiltroReporteTrimestralReaseguro(){
	manipulaCalendarioLineas();
	//agregarOpcionTodasAComboMoneda();
}

function agregarOpcionTodasAComboMoneda(){
	var opcionTodas = new Option("Todas","-1");
	var comboMoneda = $('moneda');
	var nOpciones = comboMoneda.options.length;
	comboMoneda.options[nOpciones]=opcionTodas;
}

function rptTrimestralReaseguro() {
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var tipoCambio = $('tipoCambio').value;
	var moneda = $('moneda').value;
	var tipoReporteTrim = $('tipoReporteTrim').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (tipoCambio != '' && tipoCambio > 0){
			if (moneda != ''){
				var url = '/MidasWeb/reaseguro/reportes/rptTrimestralReaseguro.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&tipoCambio='+tipoCambio+'&moneda='+moneda+'&tipoReporteTrim='+tipoReporteTrim;
				cargarEnNuevaVentana(url,'Reporte de Trimestral de Reaseguro');
			}
			else{
				alert('Por favor seleccione una moneda');
			}
		}else{
			alert('Por favor introduce un Tipo de Cambio');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function rptSaldoReasegurador(){
	var fechaInicial = $('fechaCorte').value;
	
	if (fechaInicial != ''){
		var url = '/MidasWeb/reaseguro/reportes/rptSaldoReasegurador.do?fechaCorte='+fechaInicial;
		cargarEnNuevaVentana(url,'Reporte de Fecha de Corte');
	}
	else{
		alert('Por favor seleccione una fecha');
	}

}

function llenaComboEventosCatastroficosReaseguro() {
	new Ajax.Request("/MidasWeb/reaseguro/reportes/cargaComboEventoCatastrofico.do", {
		method : "post",
		asynchronous : false,
		onSuccess : function(transport) {
			loadCombo($('eventoCatastrofico'), transport.responseXML);
		}
	});
}

var calendarioSimpleGenerico;

function mostrarCalendarioSimpleGenerico(){
	if (calendarioSimpleGenerico!=null){
		if (calendarioSimpleGenerico.isVisible()){
			calendarioSimpleGenerico.hide();
		}else{
			calendarioSimpleGenerico.show();
		}
	}
	else{
		crearCalendarioSimpleGenerico();
		if (!calendarioSimpleGenerico.isVisible()){
			calendarioSimpleGenerico.show();
		}
	}
}

function crearCalendarioSimpleGenerico(){
	if (calendarioSimpleGenerico != null){
		calendarioSimpleGenerico = null;
	}
	calendarioSimpleGenerico = new dhtmlxCalendarObject("calendarioSimpleGenericoDiv", true, {isMonthEditable: true, isYearEditable: true});
	calendarioSimpleGenerico.setDateFormat(formatoFechaCalendario());
	calendarioSimpleGenerico.attachEvent("onClick",function(fecha){
 		$('fechaCorte').value=calendarioSimpleGenerico.getFormatedDate(formatoFechaCalendario(),fecha);
 		calendarioSimpleGenerico.hide();
	});
	calendarioSimpleGenerico.hide();
	
}

var calendarioDobleRptCumulos;
function manipulaCalendarioDobleRptCumulos(){	
	calendarioDobleRptCumulos = new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
	calendarioDobleRptCumulos.setDateFormat(formatoFechaCalendario());
	
	calendarioDobleRptCumulos.leftCalendar.attachEvent("onClick",function(fecha){
 		$('fechaInicial').value=calendarioDobleRptCumulos.leftCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
 			$('fechaFinal').value = '';
 		}
	});
	
 	calendarioDobleRptCumulos.rightCalendar.attachEvent("onClick",function(fecha){
 		$('fechaFinal').value=calendarioDobleRptCumulos.rightCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
			$('fechaInicial').value = '';
		}
 		calendarioDobleRptCumulos.hide();
	});
 	calendarioDobleRptCumulos.hide();
}

function mostrarFiltroRptSiniestrosEventoCatastrofico(){
	crearCalendarioSimpleGenerico();
	llenaComboEventosCatastroficosReaseguro();
}

function rptSiniestrosEventoCatastrofico(){
	var pFechaCorte = $('fechaCorte').value;
	var eventoCatastrofico = $('eventoCatastrofico').value;
	if (pFechaCorte != ''){
		if (eventoCatastrofico != ''){
				var url = '/MidasWeb/reaseguro/reportes/rptSiniestrosEventoCatastrofico.do?pFechaCorte='+pFechaCorte+'&eventoCatastrofico='+eventoCatastrofico;
				cargarEnNuevaVentana(url,'Reporte de Trimestral de Reaseguro');
		}else{
			alert('Por favor seleccione un Evento Catastr\u00f3fico');
		}
	}else{
		alert('Por favor seleccione una fecha de corte');
	}
}

function mostrarFiltroRptSiniestrosTentPlan(){
	manipulaCalendarioLineas();
	crearCalendarioSimpleGenerico();
}

function mostrarFiltroRptSinReservaPendienteAcum(){
	crearCalendarioSimpleGenerico();
}

function rptSiniestrosTentPlan(){
	var pFechaCorte = $('fechaCorte').value;
	var idPrioridadSiniestro = $('idPrioridadSiniestro').value;
	var pFechaInicial = $('fechaInicial').value;
	var pFechaFinal = $('fechaFinal').value;
	if (pFechaInicial != '' && pFechaFinal != ''){
		if (pFechaCorte != ''){
			if (idPrioridadSiniestro != ''){
					var url = '/MidasWeb/reaseguro/reportes/rptSiniestrosTentPlan.do?pFechaCorte='+pFechaCorte+'&pFechaInicial='+pFechaInicial+'&pFechaFinal='+pFechaFinal+'&idPrioridadSiniestro='+idPrioridadSiniestro;
					cargarEnNuevaVentana(url,'Reporte de Siniestros Tent Plan');
			}else{
				alert('Por favor seleccione una Prioridad');
			}
		}else{
			alert('Por favor seleccione una fecha de corte');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function mostrarFiltroRptSiniestrosWorkingCover(){
	manipulaCalendarioLineas();
	crearCalendarioSimpleGenerico();
}

function rptSiniestrosWorkingCover(){
	var pFechaCorte = $('fechaCorte').value;
	var idPrioridadSiniestro = $('idPrioridadSiniestro').value;	
	var pFechaInicial = $('fechaInicial').value;
	var pFechaFinal = $('fechaFinal').value;
	if (pFechaInicial != '' && pFechaFinal != ''){
		if (pFechaCorte != ''){
			if (idPrioridadSiniestro != ''){
					var url = '/MidasWeb/reaseguro/reportes/rptSiniestrosWorkingCover.do?pFechaCorte='+pFechaCorte+'&pFechaInicial='+pFechaInicial+'&pFechaFinal='+pFechaFinal+'&idPrioridadSiniestro='+idPrioridadSiniestro;
					cargarEnNuevaVentana(url,'Reporte de Siniestros Working Cover');
			}else{
				alert('Por favor seleccione una prioridad');
			}
		}else{
			alert('Por favor seleccione una fecha de corte');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function cargarComponentesRptDistribucionReaseguro(){
	manipulaCalendarioLineas();
	agregarOpcionTodasAComboMoneda();
}

function cargarComponentesRptEmisionReaseguro(){
	manipulaCalendarioLineas();
	agregarOpcionTodasAComboMoneda();
}

function rptDistribucionReaseguro(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var moneda = $('moneda').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (moneda != ''){
			var url = '/MidasWeb/reaseguro/reportes/rptDistribucion.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&moneda='+moneda;
			cargarEnNuevaVentana(url,'Reporte de Distribuci\u00f3n de Reaseguro');
		}
		else{
			alert('Por favor seleccione una moneda');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function rptEmisionReaseguro(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var moneda = $('moneda').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (moneda != ''){
			var url = '/MidasWeb/reaseguro/reportes/rptEmision.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&moneda='+moneda;
			cargarEnNuevaVentana(url,'Reporte de Emisi\u00f3n de Reaseguro');
		}
		else{
			alert('Por favor seleccione una moneda');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function rptIngresosReaseguro(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	if (fechaInicial != '' && fechaFinal != ''){
		var url = '/MidasWeb/reaseguro/reportes/rptIngresosReaseguro.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal;
		cargarEnNuevaVentana(url,'Reporte de Ingresos de Reaseguro');
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function cargarComponentesRptMovtosPorContrato(){
	manipulaCalendarioLineas();
	agregarOpcionTodasAComboMoneda();
}

function rptMovtosPorContrato(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var moneda = $('moneda').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (moneda != ''){
		var url = '/MidasWeb/reaseguro/reportes/rptMovtosPorContrato.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&moneda='+moneda;
		cargarEnNuevaVentana(url,'Reporte de Ingresos de Reaseguro');
		}else{
			alert('Por favor seleccione una moneda');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function cargarComponentesRptSiniestrosPorReasegurador(){
	manipulaCalendarioLineas();
	agregarOpcionTodasAComboMoneda();
}

function rptSiniestrosPorReasegurador(tipoReporte){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var moneda = $('moneda').value;
	var c = $('idtcReaseguradorCorredor').value;
	var tipo = $('tipoParticipante').value;
	
	var validar = true;
	if (fechaInicial != '' && fechaFinal != ''){
		if (moneda != ''){
			if (c == ''){
				c='0';
			}
			if(tipo != '2'){
				if(c=='0'){
					validar=false;
				}else{
					validar=true;
				}
			}	
			
			if(tipo != ''){
				if(validar){
					var url = '/MidasWeb/reaseguro/reportes/rptSiniestrosPorReasegurador.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+
						'&moneda='+moneda+'&idtcReaseguradorCorredor='+c+'&tipoReporte='+tipoReporte;
					cargarEnNuevaVentana(url,'Reporte de Ingresos de Reaseguro');
				}else{
					alert('Por favor seleccione un reasegurador');
				}
			}else{
				alert('Por favor seleccione un Tipo');
			}
		}else{
			alert('Por favor seleccione una moneda');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function rptPerfilCartera(tipoReporte) {
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var tipoCambio = $('tipoCambio').value;
	var tipoReportePC = $('tipoReportePerfilCatera').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (tipoCambio != '' && tipoCambio > 0){
			var url = '/MidasWeb/contratos/cartera/rptPerfilCartera.do?fechaInicial='+fechaInicial+
					'&fechaFinal='+fechaFinal+'&tipoCambio='+tipoCambio+'&tipoReporte='+tipoReporte+
					'&tipoReportePC='+tipoReportePC;
			window.open(url,'Reporte de Perfil de Carteras');
		}else{
			alert('Por favor introduzca un Tipo de Cambio');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function validarMostrarRptPerfilCartera(tipoReporte){
	if (seGuardaUltimoRangoPerfilCartera()){
		if (editarPerfilCartera()){
			rptPerfilCartera(tipoReporte);
		}else{
			alert('Revise que los rangos sean correctos');
		}
	}else{
		rptPerfilCartera(tipoReporte);
	}
}

function seGuardaUltimoRangoPerfilCartera(){
	var bandera = true;
		var contenidoCelda = reportePerfilCartera.cellByIndex(reportePerfilCartera.getRowsNum()-1,3);
		if (!isNumeric(contenidoCelda.getValue()))
			bandera = false;

	return bandera;
}

function rptSiniestrosConReaseguro(){
	 var pFechaInicial = $('fechaInicial').value;
     var pFechaFinal = $('fechaFinal').value;
     var pNoSiniestro = $('pNoSiniestro').value;
     var pTipoMovimiento = $('movimiento').value;
     var pIdReasegurador = $('idtcReaseguradorCorredor').value;
     var opcionReporteMovimientoSiniestroConReaseguro = $('parametrosReporte').value;
     if (pFechaInicial != '' && pFechaFinal != ''){
		if(opcionReporteMovimientoSiniestroConReaseguro == '1'){
			pNoSiniestro = null;
			pTipoMovimiento = null;
			pIdReasegurador = null;
		}else{
			if(opcionReporteMovimientoSiniestroConReaseguro == '2'){
			pTipoMovimiento = null;
			pIdReasegurador = null;
				if (pNoSiniestro == ''){
					alert('Por favor introduce un numero de siniestro');
					return false;
				}
			}else{
				if(opcionReporteMovimientoSiniestroConReaseguro == '3'){
				pNoSiniestro = null;
				pIdReasegurador = null;
					if (pTipoMovimiento == ''){
						alert('Por favor selecciona un tipo de movimiento');
						return false;
					}
				}
				else{
					if(opcionReporteMovimientoSiniestroConReaseguro == '4'){
					pNoSiniestro = null;
					pTipoMovimiento = null;
						if (pIdReasegurador == ''){
							alert('Por favor selecciona un reasegurador');
							return false;
						}
					}
				}
			}
		}
		var url = '/MidasWeb/reaseguro/reportes/rptSiniestrosConReaseguro.do?fechaInicial='+pFechaInicial+'&fechaFinal='+
		pFechaFinal+'&pNoSiniestro='+pNoSiniestro+'&pTipoMovimiento='+pTipoMovimiento+'&pIdReasegurador='+pIdReasegurador;
		cargarEnNuevaVentana(url,'Reporte de Siniestros con Reaseguro');
    }else{
    	alert('Por favor seleccione un rango de fechas');
    }
	
}

function rptSinReservaPendiente() {
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	if (fechaInicial != '' && fechaFinal != ''){
		var url = '/MidasWeb/reaseguro/reportes/rptSinReservaPendiente.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal;
		cargarEnNuevaVentana(url,'Reporte Movimientos de Siniestros de Reservas Pendientes');
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function rptSinReservaPendienteAcum(reporteALanzar){
		var fechaCorte = $('fechaCorte').value;
		var url = '/MidasWeb/reaseguro/reportes/rptSinReservaPendienteAcum.do?reporteALanzar='+reporteALanzar+'&fechacorte='+fechaCorte;
		cargarEnNuevaVentana(url,'Reporte de Movimientos de Siniestros de Reservas Pendientes Acumulado');
}

function recalcularPorcentajeMontoFacultativo(tipoRecalculo){
	/* tipoRecalculo: 1 Rec�lculo por porcentaje, 2 Rec�lculo por monto a facultar*/
	var idTmContratoFacultativo = $('idTmContratoFacultativo').value;
	var porcentajeFacultativoBck = $('porcentajeFacultativo').value;
	fromCurrencyToFloat($('sumaFacultada'));
	var sumaFacultadaBck = $('sumaFacultada').value;
	if ((tipoRecalculo==1 && porcentajeFacultativoBck != null && porcentajeFacultativoBck != '')
	|| (tipoRecalculo==2 && sumaFacultadaBck != null && sumaFacultadaBck != '')){
		sendRequest(null,'/MidasWeb/contratofacultativo/recalcularPorcentajeMontoFacultativo.do?porcentajeFacultativo='+porcentajeFacultativoBck+
				'&sumaFacultada='+sumaFacultadaBck+'&tipoRecalculo='+tipoRecalculo+'&idTmContratoFacultativo='+idTmContratoFacultativo,null,'procesarRespuestaRecalculoProcentajeMontoFacultativo(transport.responseXML)');
	}else{
		mostrarVentanaMensaje(10, "Por favor introduce una cantidad v&aacute;lida",null); 
	}
}

function procesarRespuestaRecalculoProcentajeMontoFacultativo(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var idTdContratoFacultativo;
	var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
//	mostrarVentanaMensaje(tipoMensaje, mensaje, null);
	if($('idTdContratoFacultativo')){ //Esto valida que esta funci�n se haya invocado desde realizarConfiguracionCobertura.jsp
		var id = $('idTdContratoFacultativo').value;
		mostrarVentanaMensaje(tipoMensaje, mensaje, null);
		cargarDetalleCoberturaFacultativo(id);
//		cargarDetalleCoberturaFacultativo($('idTdContratoFacultativo').value);
	}
	else{
		if($('idToSlipCotizacion')){
			var id = $('idToSlipCotizacion').value;
			mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			cargarCotizacionFacultativa(id);
		}else{
			if ($('idToSlipDetalleCotizacion')){
				var id = $('idToSlipDetalleCotizacion').value;
				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
				cargarSlipDetalleCotizacionFacultativa(id);
			}
		}
	}
}

function cargarDetalleCoberturaFacultativo(idTdContratoFacultativo){
	var id=''+idTdContratoFacultativo;
	sendRequest(null,'/MidasWeb/contratofacultativo/cobertura/mostrarDetalle.do?id='+idTdContratoFacultativo,'configuracion_detalle','formatearMontosRegistrarCotizacionFacultativa();mostrarGridParticipacionFacultativo('+id+');mostrarGridPlanPagosFacultativos('+id+');');
}

function cargarCotizacionFacultativa(idToSlip){
	sendRequest(null,'/MidasWeb/contratofacultativo/slip/mostrarDetalle.do?id='+idToSlip,'configuracion_detalle','validarMontoRegistrarEgreso('+$('sumaFacultada')+')');
}

function cargarSlipDetalleCotizacionFacultativa(idToSlip){
	sendRequest(null,'/MidasWeb/contratofacultativo/slip/mostrarDetalle.do?id='+idToSlip,'configuracion_detalle','validarMontoRegistrarEgreso('+$('sumaFacultada')+')');
}

function procesarRespuestaAutorizacionCotizFac(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var veredicto = item.getElementsByTagName("veredicto")[0].firstChild.nodeValue;
	
	desplegarVeredictoAutorizacion(xmlDoc,null);
	if (parseInt(veredicto) == 1){
		var idToCotizacion = document.getElementsByName("slipForm.idToCotizacion");
		if (idToCotizacion != null)
			recargarArbolCotizacionFacultativo(0,idToCotizacion[0].value);
		else
			recargarArbolRealizarCotizacionFac();
	}
}

function agregarCotizacionFacultativa(form){
   	sendRequest(form,'/MidasWeb/contratofacultativo/agregarCotizacionFacultativa.do',null,'procesarRespuestaCotizacionFacultativa(transport.responseXML,transport.responseText,"recargarCotizacionfacultativa()")');
};

function agregarCotizacionFacultativa_nota(idContratoFacultativo){
	var NotaCobertura = $('notaCobejrtura').value;
	var ajustadorNombrado = $('ajustadorNombrado').value;
	sendRequest(null,'/MidasWeb/solicitud/modificarNotaCobertura.do?id='+idContratoFacultativo+'&nota='+NotaCobertura+'&ajustadorNombrado='+ajustadorNombrado,'configuracion_detalle',null);
				     
};

function recargarCotizacionfacultativa(){
//	 tree.destructor();
	 var idToCotizacion = document.getElementsByName("slipForm.idToCotizacion");
		if (idToCotizacion != null)
			recargarArbolCotizacionFacultativo(0,idToCotizacion[0].value);
		else
			cargandoTree('configuracionFacultativa0',document.getElementById('treeboxbox_tree'));
	 limpiarDiv('configuracion_detalle');
}

function procesarRespuestaCotizacionFacultativa(xmlDoc, textDoc, afterClose){
	var x;
	if (xmlDoc && xmlDoc.text != ''){
	 	var items = xmlDoc.getElementsByTagName("item");
		var item = items[0];
		var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		mostrarVentanaMensaje(tipoMensaje, mensaje, afterClose);
	}else{
		if (textDoc && textDoc.text != ''){
			$('configuracion_detalle').innerHTML=textDoc;
		}
	}
}

function sendEliminarFacultativo(form){
	if (confirm('Esta seguro de eliminar la cotizacion facultativa?')){	
   	 sendRequest(form,'/MidasWeb/contratofacultativo/eliminarCotizacionFacultativa.do', 'configuracion_detalle',null);
     tree.destructor();
     cargandoTree('configuracionFacultativa0',treeboxbox_tree);
	}
}

function sendDetalleFacultativo(form){
   	sendRequest(form,'/MidasWeb/contratofacultativo/agregarDetalleCotizacionFacultativa.do', null,null);
    tree.destructor();
    cargandoTree('configuracionFacultativa0',treeboxbox_tree);
}

function sendModificarDetalleFacultativo(form){
   	sendRequest(form,'/MidasWeb/contratofacultativo/modificarCotizacionFacultativa.do', null, 'procesarRespuestaCotizacionFacultativa(transport.responseXML,transport.responseText,null)');
}

function autorizarContratoFacultativo(idTmContratoFacultativo){
	new Ajax.Request('/MidasWeb/contratofacultativo/contrato/autorizarContratoFacultativo.do?idTmContratoFacultativo='+idTmContratoFacultativo, {
		method : "post", asynchronous : false, 
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML, null);
			actualizarAlAutorizarContratoFacultativo();
		} // End of onSuccess
		
	});
}

function actualizarAlAutorizarContratoFacultativo(){
	limpiarDiv('loadingIndicatorComps');
	tree.destructor();
	cargandoTree('configuracionFacultativa1', document.getElementById('treeboxbox_tree'));
	limpiarDiv('configuracion_detalle');
}

function cancelarContratoFacultativo(idToSlip){
	new Ajax.Request('/MidasWeb/contratofacultativo/contrato/cancelarContratoFacultativo.do?idToSlip='+idToSlip, {
		method : "post", asynchronous : false, 
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML,null);
		} // End of onSuccess
		
	});
	tree.destructor();
	cargandoTree('configuracionFacultativa3',treeboxbox_tree);
	document.getElementById('configuracion_detalle').innerHTML = "";
}

function mostrarRadiosRetencionRptReas(bandera){
	if (parseInt(bandera) == 1)
		$('divComboRetencion').style.display='block';
	else
		$('divComboRetencion').style.display='none';
}

function cargarComponentesRptCumulosReaseguro(){
	manipulaCalendarioLineas();
}

function cargarComponentesRptSaldoReasegurador(){
//	manipulaCalendarioDobleRptCumulos();
	manipulaCalendarioLineas();
}

function rptCumulosReaseguro(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var tipoDeducible = $('tipoDeducible').value;
	var tipoContrato = $('tipoContrato').value;
	var tipoCumulo = $('tipoCumulo').value;
	if (fechaInicial != '' && fechaFinal != ''){
		if (tipoDeducible != ''){
			if (tipoContrato != ''){
				if (tipoCumulo != ''){
					var url = '/MidasWeb/reaseguro/reportes/rptCumulos.do?fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&tipoDeducible='+tipoDeducible+'&tipoContrato='+tipoContrato+'&tipoCumulo='+tipoCumulo;
					cargarEnNuevaVentana(url,'Reporte de C\u00famulos');
				}
				else{
					alert('Por favor seleccione un tipo de c\u00famulo');
				}
			}
			else{
				alert('Por favor seleccione un tipo de contrato');
			}
		}else{
			alert('Por favor seleccione un tipo de deducible');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function configurarPagosReaseguradores(){
	var idTmContratoFacultativo = $('idTmContratoFacultativo').value;
	sendRequest(null,'/MidasWeb/contratofacultativo/contrato/mostrarConfiguracionReaseguradores.do?idTmContratoFacultativo='+idTmContratoFacultativo,'configuracion_detalle','mostrarConfiguracionReaseguradoresContratoFacultativoGrid()')
}


function mostrarConfiguracionReaseguradorContratoFacultativo(idtcreaseguradorcorredor, idTmContratoFacultativo, nombreReasegurador, formaPago, montoTotal){
	$('nombreReasegurador').value = nombreReasegurador;
	$('formaPago').value = formaPago;
	$('montoTotal').value = formatCurrency4Decs(montoTotal);
	
	$('configuracionReaseguradores').style.display='block';
	mostrarConfiguracionReaseguradorContratoFacultativoGrid(idtcreaseguradorcorredor, idTmContratoFacultativo);
}

function guardarConfiguracionPagosFacultativoReasegurador(){
	var ids_montos = "";
	var rowsNum = configuracionReaseguradorContratoFacultativoGrid.getRowsNum() - 2;
	if (configuracionReaseguradorContratoFacultativoGrid != null){
		for (var i=0; i<rowsNum; i = i + 1){	  
			var id = configuracionReaseguradorContratoFacultativoGrid.getRowId(i);
			var monto = convertirValorDeMonedaAFlotante(configuracionReaseguradorContratoFacultativoGrid.cellByIndex(i,1).getValue());
			ids_montos = ids_montos + id + '_' + monto;
			if (i != rowsNum)
				ids_montos = ids_montos + '/';
		}
		sendRequest(null, '/MidasWeb/contratofacultativo/contrato/guardarConfiguracionReasegurador.do?ids_montos='+ids_montos, null, 'procesarRespuestaXml(transport.responseXML, null)');
	}
	
}

function recalcularMontoTotalPagosConfiguracionReaseguradorContratoFacultativo(){
	var montoTotalPagos = 0;
	var montoTotal = 0;
	var montoExcedenteFaltante = 0;
	var rowsNum = configuracionReaseguradorContratoFacultativoGrid.getRowsNum() - 2;
	var excedenteFaltante = "";
	if (configuracionReaseguradorContratoFacultativoGrid != null){
		for (var i=0; i<rowsNum; i = i + 1){	  
			var id = configuracionReaseguradorContratoFacultativoGrid.getRowId(i);
			var monto = convertirValorDeMonedaAFlotante(configuracionReaseguradorContratoFacultativoGrid.cellByIndex(i,1).getValue());
			montoTotalPagos = montoTotalPagos + parseFloat(monto);
		}
		//configuracionReaseguradorContratoFacultativoGrid.cellByIndex(rowsNum,1).setValue("<font style='color:#0000CC;'><b>" + formatCurrency4Decs(montoTotalPagos) + "</b></font>");
		montoTotal = convertirValorDeMonedaAFlotante($('montoTotal').value);
		montoExcedenteFaltante = montoTotal - montoTotalPagos;
		
		if (montoExcedenteFaltante > 0)
			excedenteFaltante = "Faltante:";
		if (montoExcedenteFaltante < 0){
			excedenteFaltante = "Excedente:";
			montoExcedenteFaltante = montoExcedenteFaltante * -1;
		}
		
		  
		configuracionReaseguradorContratoFacultativoGrid.cellByIndex(rowsNum+1,0).setValue("<b>" + excedenteFaltante + "</b>");
		configuracionReaseguradorContratoFacultativoGrid.cellByIndex(rowsNum+1,1).setValue("<font style='color:#FF0000;'><b>" + formatCurrency4Decs(montoExcedenteFaltante) + "</b></font>");
	}
}

function mostrarOcultarTipoCambioEgresosReaseguro(monedaActual){
	var monedaOriginal = $('idTcMonedaEdosCta').value;
			
	if (parseInt(monedaOriginal) != parseInt(monedaActual)){
		$('tipoCambioDiv').style.display='block';
		$('leyendaTipoCambioDiv').style.display='block';
		$('pesosXDolarDiv').style.display='block';			
	}else{
		$('tipoCambioDiv').style.display='none';
		$('leyendaTipoCambioDiv').style.display='none';
		$('pesosXDolarDiv').style.display='none';		
	}
	actualizarMontoEgreso();
}

var calendarioRI;
function mostrarCalendarioSimpleRegistrarIngresoReaseguro(){
	if (calendarioRI==null && calendarioRI==undefined){
		calendarioRI = new dhtmlxCalendarObject('fechaIngreso', true, {isMonthEditable: true, isYearEditable: true});
		calendarioRI.setDateFormat("%d/%m/%Y");
		calendarioRI.show();
	}
	else{
		if (calendarioRI.isVisible()){
			calendarioRI.hide();
		}
		else{
			calendarioRI.show();
		}
	}
}

function mostrarTituloFacultativo(tituloACargar){
	var titulo = '';
	if (tituloACargar == 0)
		titulo = 'Cotizaci&oacute;n Negociaci&oacute;n';
	if (tituloACargar == 1)
		titulo = 'Cotizaci&oacute;n Autoriza';
	if (tituloACargar == 2)
		titulo = 'Contratos Autorizados';
	if (tituloACargar == 3)
		titulo = 'Solicitud Cancelados';
	if (tituloACargar == 4)
		titulo = 'Cancelados';
	
	document.getElementById('tituloSuperior').style.display = 'block';
	document.getElementById('tituloSuperior').innerHTML = titulo;
}

function modificarPorcentajesContratosReaseguro(){
	var porcentajeFac = $('porcentajeFacultativo').value;
	var facultativoOriginal = $('facultativoOriginal').value;
	
	
	var respuesta = true;
	if (porcentajeFac == '' || parseFloat(porcentajeFac) == 0){
		if(confirm('Ha definido en Cero el porcentaje de Facultativo, el Facultativo ser\u00e1 autorizado, ' + String.fromCharCode(191) +'desea continuar?')){
			respuesta = true; 
		}else{
			respuesta = false;
		}
	} else if (parseFloat(porcentajeFac) != parseFloat(facultativoOriginal)){
		if(confirm('Al cambiar el porcentaje de Facultativo, se eliminar\u00e1 cualquier Plan de Pagos que haya sido definido, ' + String.fromCharCode(191) +'desea continuar?')){
			respuesta = true; 
		}else{
			respuesta = false;
		}
	}
	
	if (respuesta){
		sendRequest(document.configurarPorcentajesFacultativoForm, '/MidasWeb/contratofacultativo/modificarPorcentajesContratos.do?facultativoOriginal=' + facultativoOriginal, null, 'respuestaModificarPorcentajesContratosReaseguro(transport.responseXML, "cerrarVentanaModificarPorcentajes(1)")');
	}
	
}

function respuestaModificarPorcentajesContratosReaseguro(xmlDoc, funcionJS){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	
	if (parseInt(tipoMensaje) == 32){
		funcionJS = "recargarCotizacionNegociacionFacultativo()";
		tipoMensaje = "30";
	}
	
	mostrarVentanaMensaje(tipoMensaje, mensaje, funcionJS);
}

function recargarCotizacionNegociacionFacultativo(){
	cerrarVentanaModificarPorcentajes(0);
	parent.sendRequestFromAccordion(null,'/MidasWeb/sistema/configuracion/listar.do', 'contenido', "cargandoTree('configuracionFacultativa0','treeboxbox_tree')");
}

function mostrarFiltroRptReservasRiesgo(){
	manipulaCalendarioLineas();
	crearCalendarioSimpleGenerico();
}

function rptReservasRiesgo(){
	var pFechaCorte = $('fechaCorte').value;
	var pFechaInicial = $('fechaInicial').value;
	var pFechaFinal = $('fechaFinal').value;
	if (pFechaInicial != '' && pFechaFinal != ''){
		if (pFechaCorte != ''){
			var url = '/MidasWeb/reaseguro/reportes/rptReservasRiesgo.do?pFechaCorte='+pFechaCorte+'&pFechaInicial='+pFechaInicial+'&pFechaFinal='+pFechaFinal;
			cargarEnNuevaVentana(url,'Reporte de Reservas de Riesgo');
		}else{
			alert('Por favor seleccione una fecha de corte');
		}
	}else{
		alert('Por favor seleccione un rango de fechas');
	}
}

function mostrarFiltroCotizacionesFacultativo(){
	var htmlAInsertar = "<table id='filtros' width='190px' style='table-layout:auto'><tr><td colspan='3'><center><font color='green' size='-2'>Listado de Cotizaciones en Facultativo</font></center></td></tr><tr><td width='5%'>Cot.:</td><td width='50%'><input type='text' class='cajaTexto' id='idToCotizacionBusqueda' onkeypress='return soloNumeros(this,event,false)'/></td><td width='45%'><div id='b_buscar'><a href='#' onclick='javascript:buscarCotizacionFacultativo()'>Buscar</a></div></td></tr></table><br/><div width='190px' height='100px' id='filtroCotizacionesFacultativoGridDiv'></div>";
	$('divSuperior').innerHTML = htmlAInsertar;
	$('divSuperior').style.display = 'block';
	mostrarFiltroCotizacionesFacultativoGrid('');
}

function buscarCotizacionFacultativo(){
	mostrarFiltroCotizacionesFacultativoGrid($('idToCotizacionBusqueda').value);
}

function recargarArbolCotizacionFacultativo(estatus, idToCotizacion){
	 tree.destructor();
	 sendRequest(null, '/MidasWeb/contratofacultativo/contrato/guardarIdCotizacionEnSesion.do?idToCotizacion='+idToCotizacion, 
			 null, "cargandoTree('configuracionFacultativa" + estatus + "', 'treeboxbox_tree')");
	 limpiarDiv('configuracion_detalle');
}

function guardarPrimaNoDevengadaFacultativo(){
	fromCurrencyToFloat($('primaNoDevengada'));
	if($('primaNoDevengada').value != ''){
		guardarConfiguracionCoberturaContFacultativo($('idTdContratoFacultativo').value)
	}else{
		alert("Proporcione un valor para la Prima No Devengada");
	}
}

function generarReporteSaldos(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	var tipoReporte = $('tipoReporte').value;
	var idMoneda = $('moneda').value;
	var tipoContratoChk = document.reportesReaseguroForm.tipoContrato;
	var tipoContratoSel = '';
	if (fechaInicial != '' && fechaFinal != ''){
		for(i=0; i <tipoContratoChk.length; i++){
			if(tipoContratoChk[i].checked){
				tipoContratoSel = tipoContratoChk[i].value;
			}
		}
		if (tipoContratoSel != ''){
			var url = '/MidasWeb/reaseguro/reportes/generarReporteSaldosPorPolizaSiniestro.do?' +
			'fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+'&tipoContrato='+tipoContratoSel+
			'&tipoReporte='+tipoReporte+'&idMoneda='+idMoneda;
			cargarEnNuevaVentana(url,'Reporte de saldos por '+tipoReporte);
		}
		else{
			mostrarVentanaMensaje('10','Por favor seleccione el tipo de consulta.' , null);
		}
	}else{
		mostrarVentanaMensaje('10', 'Por favor seleccione un rango de fechas.', null);
	}
}

function generarReporteSaldosTrimestrales(){
	var idTcReasegurador = $('idtcReaseguradorCorredor').value;
	var ejercicio = $('idEjercicio').value;
	var suscripcion = $('idSuscripcion').value;
	var moneda = $('idMoneda').value;
	
	var incluirSaldosCuotaParte = $('incluirSaldosCuotaParte').checked
	var incluirSaldosPrimerExcedente = $('incluirSaldosPrimerExcedente').checked
	var incluirSaldosFacultativo = $('incluirSaldosFacultativo').checked
	var incluirSaldosEjerciciosAnteriores = $('incluirSaldosEjerciciosAnteriores').checked
	var desglosarPorMes = $('desglosarPorMes').checked
	
	if(idTcReasegurador != ''){
		if(ejercicio >=2008 && ejercicio <=2050){
			if(suscripcion != ''){
				if(moneda != ''){
					if(incluirSaldosCuotaParte || incluirSaldosPrimerExcedente || incluirSaldosFacultativo){
						var url = '/MidasWeb/reaseguro/reportes/generarReporteSaldosPorTrimestre.do?' +
						'idTcReasegurador='+idTcReasegurador+'&ejercicio='+ejercicio+'&suscripcion='+suscripcion+
						'&moneda='+moneda+'&incluirSaldosCuotaParte='+incluirSaldosCuotaParte+
						'&incluirSaldosPrimerExcedente='+incluirSaldosPrimerExcedente+'&incluirSaldosFacultativo='+incluirSaldosFacultativo+
						'&incluirSaldosFacultativo='+incluirSaldosFacultativo+'&incluirSaldosEjerciciosAnteriores='+incluirSaldosEjerciciosAnteriores+
						'&desglosarPorMes='+desglosarPorMes+'&desglosarPorConceptos='+desglosarPorMes;
						cargarEnNuevaVentana(url,'Reporte de saldos por trimestre');
					}
					else{
						mostrarVentanaMensaje('10', 'Seleccione al menos un tipo de contrato.', null);
					}
				}
				else{
					mostrarVentanaMensaje('10', 'Seleccione una moneda.', null);
				}
			}
			else{
				mostrarVentanaMensaje('10', 'Seleccione una suscripci&oacute;n.', null);
			}
		}
		else{
			mostrarVentanaMensaje('10', 'Introduzca un ejercicio v&aacute;lido.', null);
		}
	}
	else{
		mostrarVentanaMensaje('10', 'Seleccione un reasegurador.', null);
	}
}

function generarReporteNegociosFacultativosTrimestrales(){
	var idTcReasegurador = $('idtcReaseguradorCorredor').value;
	var ejercicio = $('idEjercicio').value;
	var suscripcion = $('idSuscripcion').value;
	
	if(idTcReasegurador != ''){
		if(ejercicio >=2008 && ejercicio <=2050){
			if(suscripcion != ''){
				var url = '/MidasWeb/reaseguro/reportes/generarReporteNegociosFacultativosPorTrimestre.do?' +
				'idTcReasegurador='+idTcReasegurador+'&ejercicio='+ejercicio+'&suscripcion='+suscripcion;
				cargarEnNuevaVentana(url,'Reporte de negocios facultativos por trimestre');
			}
			else{
				mostrarVentanaMensaje('10', 'Seleccione una suscripci&oacute;n.', null);
			}
		}
		else{
			mostrarVentanaMensaje('10', 'Introduzca un ejercicio v&aacute;lido.', null);
		}
	}
	else{
		mostrarVentanaMensaje('10', 'Seleccione un reasegurador.', null);
	}
}

function regenerarPlanPagosCobertura(){
	var idTdContratoFacultativo = $('idTdContratoFacultativo').value;
	var numeroExhibiciones = $('numeroExhibiciones').value;
	
	if(numeroExhibiciones !== ""){		
		actionURL = '/MidasWeb/contratofacultativo/pagocobertura/regenerarPlanPagosCobertura.do?'+
		   'idTdContratoFacultativo='+idTdContratoFacultativo+'&numeroExhibiciones='+numeroExhibiciones;		
		gridPlanPagosFacultativos.clearAll();		
		gridPlanPagosFacultativos.load(actionURL, 'json');		
	}else{
		alert("N\u00FAmero de exhibiciones inv\u00E1lido.");
	}
}

function autorizarPlanPagosCobertura(){
	new Ajax.Request('/MidasWeb/contratofacultativo/pagocobertura/autorizarPlanPagosCobertura.do?'+
			 'idTdContratoFacultativo='+$("idTdContratoFacultativo").value, {
		method : "post",
		encoding : "UTF-8",
		parameters : null,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplete
		onSuccess : function(transport) {							
			actualizarEstatusAutorizacionPlanPagos(transport.responseXML);
		} // End of onSuccess		
	});
	return true;
}

function agregarContratosCobertura(form){
	var fecha = document.getElementById("fechaInicial").value;
	if (fecha == null || fecha == "") {
		alert("Por favor elija una fecha v\xE1lida.");
		return false;
	}
	var ramo = document.getElementById("id_ramo").options[document.getElementById("id_ramo").selectedIndex].text
	if (ramo == "Seleccione ...") {
		alert("Por favor elija una opci\xF3n v\xE1lida.");
		return false;
	}
	var claveEsquemas = document.getElementById("claveEsquemas").value;
	if (claveEsquemas == null || claveEsquemas == "") {
		alert("Por favor introduzca una Clave de Esquemas v\xE1lida.");
		return false;
	}
	var monto = document.getElementById("monto").value;
	if (monto == null || monto == "") {
		alert("Por favor introduzca un monto v\xE1lido.");
		return false;
	}
	var calificacion = document.getElementById("calificacion").value;
	if (calificacion == null || calificacion == "") {
		alert("Por favor introduzca una calificaci\xF3n v\xE1lida.");
		return false;
	}
	
	var location = "/MidasWeb/reaseguro/riesgoscontraparte/agregar.do";
	location = location+"?fecha_corte="+fecha+"&ramo="+ramo+"&claveEsquemas="+claveEsquemas+"&monto="+monto+"&calificacion="+calificacion;
	sendRequest(form, location, 'resultados', null);
	
	}

function generarReporte(form){
	var fecha = document.getElementById("fechaInicial").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
	if (fecha == null || fecha == "") {
		mostrarMensajeInformativo('Por favor elija una fecha v\xE1lida.', '20');
		jQuery("#fechaCorte").focus();
		return;
	}
	
	if (tipoCambio == null || tipoCambio == "" || isNaN(tipoCambio) || tipoCambio == 0){
		mostrarMensajeInformativo('Por favor introduzca un tipo de cambio v\xE1lido.', '20');
		return;
	}
	
	new Ajax.Request("/MidasWeb/reaseguro/reportes/reportercscontraparte/getArchivosPorCargar.do", {
		method : "post",
		asynchronous : false,
		parameters : "fechaCorte="+fecha,
		onSuccess : function(transport) {
				 var response = transport.responseText;
				 
			     mostrarMensajeConfirm(response+"\r\n"+"<br>\u00BFDesea Generarlo?","20","generarContratosContraParte()",null,null,null);
				
		     },
		  onFailure: function() { 
		  mostrarMensajeInformativo('No se pudo procesar el reporte '+reporte, '20');}
	});	
		
}

function generarReporteRR6(form, reporte){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
	if (fechaIni == null || fechaIni == "" || fechaFin == null || fechaFin == "") {
		mostrarMensajeInformativo('Por favor elija una fecha v\xE1lida.', '20');
		jQuery("#fechaInicial").focus();
		return;
	}
	
	var e = document.getElementById("reporte");
	var reporte = e.options[e.selectedIndex].value;
	
	if(reporte == 0){
		mostrarMensajeInformativo('Por favor elija una opci&oacute;n v\xE1lida.', '20');
		return;
	}
	dwr.util.setValue("reporte", reporte);
	
	if (tipoCambio == null || tipoCambio == "" || isNaN(tipoCambio)){
		mostrarMensajeInformativo('Por favor introduzca un tipo de cambio v\xE1lido.', '20');
		return;
	}
		
	new Ajax.Request("/MidasWeb/danios/reportes/reporterr6/getArchivosPorCargar.do", {
		method : "post",
		asynchronous : false,
		parameters : "reporte="+reporte+"&fechaFinal="+fechaFin,
		onSuccess : function(transport) {
				 var response = transport.responseText;
				 
			     mostrarMensajeConfirm(response+"\r\n"+"<br>\u00BFDesea Generarlo?","20","generarContratos()",null,null,null);
				
		     },
		  onFailure: function() { //alert('No se pudo procesar el archivo '+reporte); 
		  mostrarMensajeInformativo('No se pudo procesar el reporte '+reporte, '20');}
	});			
		
}

function procesarReporteRR6(form, accion, accionSA){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
	if (fechaIni == null || fechaIni == "" || fechaFin == null || fechaFin == "") {
		mostrarMensajeInformativo('Por favor elija una fecha v\xE1lida.', '20');
		jQuery("#fechaFinal").focus();
		return;
	}
	
	var e = document.getElementById("reporte");
	var reporte = e.options[e.selectedIndex].value;
	
	if(reporte == 0){
		mostrarMensajeInformativo('Por favor elija una opci&oacute;n v\xE1lida.', '20');
		return;
	}
	dwr.util.setValue("reporte", reporte);
	
	if (tipoCambio == null || tipoCambio == "" || isNaN(tipoCambio)){
		mostrarMensajeInformativo('Por favor introduzca un tipo de cambio v\xE1lido.', '20');
		return;
	}
	new Ajax.Request("/MidasWeb/danios/reportes/reporterr6/procesarReporteRR6.do", {
		method : "post",
		asynchronous : false,
		parameters : "reporte="+reporte+"&fechaInicio="+fechaIni+"&fechaFinal="+fechaFin+"&tipoCambio="+tipoCambio+"&accion="+accion+"&accionSA="+accionSA,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		},
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		},
		onSuccess : function(transport) {
				 var response = transport.responseText;
				 if(response == "0"){
					 actualizarEstatusReporteRR6();
				 }else if(response == "SIN_DATOS"){
					 mostrarMensajeConfirm("No hay datos de SA en Solvencia.<br/>"+"Este proceso tarda mucho tiempo.\r\n"+"\u00BFDesea procesar?","20","reprocesarReporteRR6("+reporte+","+tipoCambio+","+2+","+2+")",null,null,null);
				 }else{
					 mostrarMensajeConfirm(response+"\r\n"+"\u00BFDesea reprocesar?","20","reprocesarReporteRR6("+reporte+","+tipoCambio+","+2+","+2+")",null,null,null);
				 }
		     },
		  onFailure: function() { //alert('No se pudo procesar el archivo '+reporte); 
		  mostrarMensajeInformativo('No se pudo procesar el reporte '+reporte, '20');}
	});
	
}

function reprocesarReporteRR6(reporte,tipoCambio,accion,accionSA){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	new Ajax.Request("/MidasWeb/danios/reportes/reporterr6/procesarReporteRR6.do", {
		method : "post",
		asynchronous : false,
		parameters : "reporte="+reporte+"&fechaInicio="+fechaIni+"&fechaFinal="+fechaFin+"&tipoCambio="+tipoCambio+"&accion="+accion+"&accionSA="+accionSA,
		onSuccess : function(transport) {
			sendRequest(null, '/MidasWeb/danios/reportes/reporterr6/listarDocumentos.do?fechaFinal='+fechaFin, 'resultadosDocumentos', null);
		},
		  onFailure: function() { alert('No se pudo procesar el archivo '+reporte); }
	});	
}

function actualizarEstatusReporteRR6(){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	if (fechaIni == null || fechaIni == "" || fechaFin == null || fechaFin == "") {
		mostrarMensajeInformativo('Por favor elija una fecha de inicio y fin de trimestre v\xE1lida.', '20');
		jQuery("#fechaFinal").focus();
		return;
	}
	sendRequest(null, '/MidasWeb/danios/reportes/reporterr6/listarDocumentos.do?fechaFinal='+fechaFin, 'resultadosDocumentos', null);
}

function actualizarEstatusCargaReporteRR6(){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	if (fechaIni == null || fechaIni == "" || fechaFin == null || fechaFin == "") {
		mostrarMensajeInformativo('Por favor elija una fecha de inicio y fin de trimestre v\xE1lida.', '20');
		jQuery("#fechaFinal").focus();
		return;
	}
	sendRequest(null, '/MidasWeb/danios/reportes/reporterr6/listarCargas.do?fechaFinal='+fechaFin, 'resultadosCargas', null);
}

function interacCmbsRR6(tipoReporte){
	if(tipoReporte.value === '2' || tipoReporte.value == 6){
		document.getElementById('tipoCambio').disabled=false;
		$('tipoCambio').style.display='block';
		$('titulo').style.display='block';
		
	}else{
		document.getElementById('tipoCambio').disabled=true;
		$('tipoCambio').style.display='none';
		$('titulo').style.display='none';
	}
	
}

function deshabBotonRR6(tipoReporte){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	if(tipoReporte.value === '1' || tipoReporte.value === '8'){
		$('procesar').style.display='none';
	}else if(tipoReporte.value === '4'){
		if (fechaFin.substring(3, 5)!='12') {
			$('procesar').style.display='none';
		}else{
			$('procesar').style.display='block';		
		}
	}else{
		$('procesar').style.display='block';		
	}	
}

function generarContratos(){
	var fechaIni = document.getElementById("fechaInicial").value;
	var fechaFin = document.getElementById("fechaFinal").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
		
	var e = document.getElementById("reporte");
	var reporte = e.options[e.selectedIndex].value;
	
	dwr.util.setValue("reporte", reporte);
	
	var location = "/MidasWeb/danios/reportes/reporterr6/generarReporteRR6.do";
	location = location+"?reporte="+reporte+"&fechaInicio="+fechaIni+"&fechaFinal="+fechaFin+"&tipoCambio="+tipoCambio;
	window.open(location,"reporte");
}

function actualizarEstatusContraParte(){
	var fechaCorte = document.getElementById("fechaInicial").value;
	if (fechaCorte == null || fechaCorte == "") {
		mostrarMensajeInformativo('Por favor elija una fecha de corte v\xE1lida.', '20');
		jQuery("#fechaCorte").focus();
		return;
	}
	sendRequest(null, '/MidasWeb/reaseguro/reportes/reportercscontraparte/listarDocumentos.do?fechaCorte='+fechaCorte, 'resultadosDocumentos', null);
}

function actualizarEstatusCargaContraParte(){
	var fechaCorte = document.getElementById("fechaInicial").value;
	if (fechaCorte == null || fechaCorte == "") {
		mostrarMensajeInformativo('Por favor elija una fecha de corte v\xE1lida.', '20');
		jQuery("#fechaCorte").focus();
		return;
	}
	sendRequest(null, '/MidasWeb/reaseguro/reportes/reportercscontraparte/listarCargas.do?fechaFinal='+fechaCorte, 'resultadosCargas', null);
}

function procesarReporteContraParte(form, accion){
	var fechaCorte = document.getElementById("fechaInicial").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
	if (fechaCorte == null || fechaCorte == "") {
		mostrarMensajeInformativo('Por favor elija una fecha v\xE1lida.', '20');
		jQuery("#fechaCorte").focus();
		return;
	}
	
	if (tipoCambio == null || tipoCambio == "" || isNaN(tipoCambio)){
		mostrarMensajeInformativo('Por favor introduzca un tipo de cambio v\xE1lido.', '20');
		return;
	}
	new Ajax.Request("/MidasWeb/reaseguro/reportes/reportercscontraparte/procesarReporteContraParte.do", {
		method : "post",
		asynchronous : false,
		parameters : "fechaCorte="+fechaCorte+"&tipoCambio="+tipoCambio+"&accion="+accion,
		onSuccess : function(transport) {
				 var response = transport.responseText;
				 if(response == "0"){
					 mostrarMensajeInformativo('Procesando reporte. Presione el boton Actualizar Estatus Midas', '20');
				 }else{
			     mostrarMensajeConfirm(response+"\r\n"+"\u00BFDesea reprocesar?","20","reprocesarReporteContraParte("+tipoCambio+","+2+")",null,null,null);
				 }
		     },
		  onFailure: function() { 
		  mostrarMensajeInformativo('No se pudo procesar el reporte '+reporte, '20');}
	});
	
}

function reprocesarReporteContraParte(tipoCambio,accion){
	var fechaCorte = document.getElementById("fechaInicial").value;
	new Ajax.Request("/MidasWeb/reaseguro/reportes/reportercscontraparte/procesarReporteContraParte.do", {
		method : "post",
		asynchronous : false,
		parameters : "fechaCorte="+fechaCorte+"&tipoCambio="+tipoCambio+"&accion="+accion,
		onSuccess : function(transport) {
			sendRequest(null, '/MidasWeb/reaseguro/reportes/reportercscontraparte/listarDocumentos.do?fechaCorte='+fechaCorte, 'resultadosDocumentos', null);
		},
		  onFailure: function() { alert('No se pudo procesar el archivo '+reporte); }
	});	
}

function generarContratosContraParte(){
	var fechaCorte = document.getElementById("fechaInicial").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
		
	var location = "/MidasWeb/reaseguro/reportes/reportercscontraparte/generarReporte.do";
	location = location+"?fechaCorte="+fechaCorte+"&tipoCambio="+tipoCambio;
	window.open(location,"reporte");
}

