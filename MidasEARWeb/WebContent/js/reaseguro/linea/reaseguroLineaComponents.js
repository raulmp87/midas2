/******************************************************
 ************************************Modulos de la linea
 ******************************************************/

function calcularDatosRegistrarLinea(){
	if ($('maximo').value === '')
		$('maximo').value = 0;
	if ($('porcentajeDeCesion').value === '')
		$('porcentajeDeCesion').value = 0;
	if ($('cesionCuotaParte').value === '')
		$('cesionCuotaParte').value = 0;
	if ($('cesionPrimerExcedente').value === '')
		$('cesionPrimerExcedente').value = 0;
	if ($('capacidadMaximaLinea').value === '')
		$('capacidadMaximaLinea').value = 0;
	if ($('totalRetencion').value === '')
		$('totalRetencion').value = 0;
	if ($('totalCedido').value === '')
		$('totalCedido').value = 0;
	if ($('montoPleno').value === '')
		$('montoPleno').value = 0;
	if ($('numeroPlenos').value === '')
		$('numeroPlenos').value = 0;

	$('cesionCuotaParte').value = parseFloat($('porcentajeDeCesion').value) * parseFloat($('maximo').value)/100.0;
	$('cesionPrimerExcedente').value = parseFloat($('montoPleno').value) * parseFloat($('numeroPlenos').value);
	$('capacidadMaximaLinea').value = parseFloat($('maximo').value) + (parseFloat($('montoPleno').value) * parseFloat($('numeroPlenos').value));
	$('totalRetencion').value = parseFloat($('maximo').value) - (parseFloat($('porcentajeDeCesion').value) * parseFloat($('maximo').value))/100.0;
	$('totalCedido').value = parseFloat($('capacidadMaximaLinea').value) - parseFloat($('totalRetencion').value);
}

var calendarioDobleLineas;
var calendarioDoble;

function validaHoraInicialLineas(){	validaHoraLineas('horaInicial'); }
function validaHoraFinalLineas(){ validaHoraLineas('horaFinal'); }

function manipulaCalendarioLineas(){	
	calendarioDobleLineas = new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
	calendarioDobleLineas.setDateFormat(formatoFechaCalendario());
	
	calendarioDobleLineas.leftCalendar.attachEvent("onClick",function(fecha){
 		$('fechaInicial').value=calendarioDobleLineas.leftCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
 			$('fechaFinal').value = '';
 		}
	});
	
 	calendarioDobleLineas.rightCalendar.attachEvent("onClick",function(fecha){
 		$('fechaFinal').value=calendarioDobleLineas.rightCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
			$('fechaInicial').value = '';
		}
	});
 	calendarioDobleLineas.hide();
}
function manipulaCalendarioDoble(){	
	calendarioDoble = new dhtmlxDblCalendarObject('rangoDeFechas2', true, {isMonthEditable: true, isYearEditable: true});
	calendarioDoble.setDateFormat(formatoFechaCalendario());
	
	calendarioDoble.leftCalendar.attachEvent("onClick",function(fecha){
 		$('fechaInicial2').value=calendarioDoble.leftCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal2').value,$('fechaInicial2').value)){
 			$('fechaFinal2').value = '';
 		}
	});
	
 	calendarioDoble.rightCalendar.attachEvent("onClick",function(fecha){
 		$('fechaFinal2').value=calendarioDoble.rightCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal2').value,$('fechaInicial2').value)){
			$('fechaInicial2').value = '';
		}
	});
 	calendarioDoble.hide();
}
function mostrarCalendarioDobleLinea(){

	if (calendarioDobleLineas!=null){
		if (calendarioDobleLineas.isVisible()){
			calendarioDobleLineas.hide();
		}else{
			calendarioDobleLineas.show();
			
			jQuery("input,textarea,select").focus(function(){
				calendarioDobleLineas.hide();
			});
			
		}
	}
}



function mostrarCalendarioDoble(){
	if (calendarioDoble!=null){
		if (calendarioDoble.isVisible()){
			calendarioDoble.hide();
		}else{
			calendarioDoble.show();
		}
	}
}
function rojosSeleccionadosLineaNegociacion(){
	var indiceSeleccionados = lineasNegociacion.getCheckedRows(0);
	var listaSeleccionados = indiceSeleccionados.split(","); 
	var rojos = new Array();
	var i = 0,j=0;
	for (i=0;i<listaSeleccionados.length;i++)
		if (listaSeleccionados[i]<0) rojos[j++]=listaSeleccionados[i];	
	return rojos;
}
function mostrarAgregarLinea(){
	var listaRojos = rojosSeleccionadosLineaNegociacion();
		
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	var idTmLinea = trim($('idTmLinea').value);
	var estatus = trim($('idTmLinea').value);
	
	if (esValidoRangoDeFechasLineas()){		
		if (listaRojos.length>1){
			$('fechaInicial').value = fechaInicial;
			$('fechaFinal').value = fechaFinal;
			alert("S\u00F3lo es posible agregar un elemento de rojo a la vez");
			return;
		}
		
		if (listaRojos.length==1){
			var i=0;
			var col = lineasNegociacion.getColIndexById('idTcRamo');
			var idTcRamo = lineasNegociacion.cellById(listaRojos[0],col).getValue();
			col = lineasNegociacion.getColIndexById('idTcSubRamo');
			var idTcSubRamo = lineasNegociacion.cellById(listaRojos[0],col).getValue();
			$('idTcSubRamo').value=idTcSubRamo;
			for (i=0;i<$('idTcRamo').options.length;i++){
				if ($('idTcRamo').options[i].value==idTcRamo){
					$('idTcRamo').options[i].selected=true;
					break;
				}
			}
		}
		if ($('idTcRamo').value==''){
			$('fechaInicial').value = fechaInicial;
			$('fechaFinal').value = fechaFinal;
			alert('Seleccione un ramo');			
			return;
		}
		sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarAgregarInicial.do','contenido','formatearMontosRegistrarLinea()');
		
	}else{
		$('fechaInicial').value = fechaInicial;
		$('fechaFinal').value = fechaFinal;
	}
}
 
function mensajeAutorizacionLinea(){
	mensajeGuardadoExito='La linea ha sido guardada exit\u00f3samente';
	mensajeAutorizado='La linea ha sido autorizada exit\u00f3samente';
	mensajeNoAutorizado='La linea no ha sido autorizada, verifique por favor que, en caso de existir, los contratos tengan participaciones v\u00e1lidas';
	if($('mensajeAutorizar').value != '') 
		if($('mensajeAutorizar').value == '1')
			alert(mensajeGuardadoExito + ", " + mensajeAutorizado); 
		else
			alert(mensajeGuardadoExito + ", " + mensajeNoAutorizado); 
	else
		alert(mensajeGuardadoExito);
}

function guardaSesionAgregarLinea(){
	fobj = document.configuracionLineaForm;
	new Ajax.Request('/MidasWeb/contratos/linea/guardaSesion.do', { method : "post", 
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null });
}

function rptLineasNegociacion(estatus, tipoReporte) {
	var url = '/MidasWeb/contratos/linea/rptLineasContratos.do?&tipoReporte='+tipoReporte+'&estatus='+estatus;
	
	cargarEnNuevaVentana(url,'Reporte Lineas');
}

var fechaFinalExtenderVigencia;
var originalfechaFinal;
function mostrarExtenderVigenciaLinea(){
	var indiceSeleccionados = lineasVigencias.getCheckedRows(0);
	var esUnElemento = trim(indiceSeleccionados).length>0 && (indiceSeleccionados.search(/,/) == -1);
	
	if (esUnElemento){
		sendRequest(document.configuracionLineaForm,
			'/MidasWeb/contratos/linea/mostrarExtenderVigenciaLinea.do?idTmLinea='+indiceSeleccionados,
			'contenido', 'mostrarExtenderVigenciaGrid();cargaCalendarioExtenderVigencia();');	
	}else{
		alert('Seleccione un elemento de la lista');
	}
}
function validarFechaExtenderVigenciaEscrito(input) { //El input es el de la fecha final.
	var fechaFinal = input.value;
	var fechaInicial = $('fechaInicial').value;
	var fa = new Date();
	var fechaActual = fa.getDate()+"/"+ (fa.getMonth()+1) + "/" +fa.getFullYear(); //Este campo se va obtener de un hidden field.
	var errores1 = validarFecha(fechaFinal);
	var errores2 = "";
	var escribir = false;
	if (errores1 == "") {
		errores2= validarFechaExtenderVigencia(fechaInicial, fechaFinal, fechaActual);
		if (errores2 == "") {
			$('fechaFinalOriginal').value = fechaFinal;
			escribir = true;
		}
		else {
			mostrarVentanaMensaje("10", errores2);
			$('fechaFinal').value = $('fechaFinalOriginal').value;
		}
	}
	else {
		mostrarVentanaMensaje("10", errores1);
		$('fechaFinal').value = $('fechaFinalOriginal').value;
	}
	
	return escribir;
}

function cargaCalendarioExtenderVigencia(){
	fechaFinalExtenderVigencia = new dhtmlxCalendarObject("fechaFinal");
	fechaFinalExtenderVigencia.setDateFormat(formatoFechaCalendario());
	$('fechaFinalOriginal').value = $('fechaFinal').value;
/*	fechaFinalExtenderVigencia.attachEvent("onClick",function(fechaFinal){
		var fechaInicial = $('fechaInicial').value;
		var fechaActual = "20/11/09"; //Este campo se va obtener de un hidden field.
		var fechaFinalString = $('fechaFinal').value;
		var errores = validarFechaExtenderVigencia(fechaInicial, fechaFinalString, 
				fechaActual);
		var actualizarFecha = false;
		if (errores == "") {
			$('fechaFinalOriginal').value = fechaFinalString;
			actualizarFecha = true;
		}
		else {
			$('fechaFinal').value = $('fechaFinalOriginal').value;
			mostrarVentanaMensaje("10", errores);
		}
		return actualizarFecha;
	});	*/
}
function validarFechaExtenderVigencia(fechaInicial, fechaFinal, fechaActual) {
	var errores = "";
	if(!fechaMayorOIgualQue(fechaFinal,fechaInicial)) {
		errores += "La fecha final no puede ser menor o igual que la fecha inicial.";  
	}else if(!fechaMayorQue(fechaFinal,fechaActual)){
		errores += "La fecha final no puede ser menor o igual que la fecha actual."; 
	}
	return errores;
}
function extenderVigenciaLineas(){
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal =  $('fechaFinal').value;
	if (!esValidoRangoDeFechasLineas())return;

	if (confirm('\u00BFDesea extender la vigencia de este elemento \n y de los elementos relacionados?')){
		lineasExtenderVigencia.checkAll();
		var listaIds = lineasExtenderVigencia.getAllRowIds(0);		
		sendRequest(document.configuracionLineaForm,
				'/MidasWeb/contratos/linea/extenderVigenciaLinea.do?idTmLinea='+listaIds
					+'&fi='+$('fechaInicial').value+'&ff='+$('fechaFinal').value,
				null, 'procesarRespuestaXml(transport.responseXML,null)');
	}
	$('fechaInicial').value = fechaInicial;
	$('fechaFinal').value = fechaFinal;
}

function renovarVigenciaLineas(nombreFormulario){
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	var estatus = $('estatus').value;
	if (fechaInicial.length != 0){
		fechaInicial += ' ' +trim($('horaInicial').value); 
	}
	if (fechaFinal.length != 0){
		fechaFinal += ' ' + trim($('horaFinal').value); 
	}	
	
	/*
	if (confirm('\u00BFDesea renovar los contratos?')){
		sendRequest(document.configuracionLineaForm,
			'/MidasWeb/contratos/linea/renovarLinea.do?fi='+
			fechaInicial+'&ff='+fechaFinal+
			'&estatus='+$('estatus').value+"&idTcRamo="+$('idTcRamo').value,
			null, 'mostrarLinea'+nombreFormulario+'Grids()');
		
	}*/
}

function desautorizarLinea(){
	var indiceSeleccionados = lineasContratos.getCheckedRows(0);
	var esUnElemento = trim(indiceSeleccionados).length>0;// &&
															// (indiceSeleccionados.search(/,/)
															// > -1);
	
	if (esUnElemento){	
		sendRequest(document.configuracionLineaForm,
				'/MidasWeb/contratos/linea/desautorizarLinea.do?idTcLinea='+indiceSeleccionados,
				null, 'mostrarLineaContratoGrids()');
	}else{
		alert('Seleccione al menos un elemento de la lista');
	}
}

function sendRequestLineaContrato(caso,idTmLinea,idFormulario){
	var nombreFormulario='';
	switch(idFormulario){
		case 1:{nombreFormulario='Contrato';break;}
		case 2:{nombreFormulario='Negociacion';break;}
		case 3:{nombreFormulario="Vigencia";break;}
	}
	switch(caso){
		case 1:{
			sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarLinea.do?idTmLinea='+idTmLinea+'&formularioOrigen='+nombreFormulario, 'contenido', 'mostarContratosLineaVigenciaContrato(),formatearMontosRegistrarLinea()');
			break;
		}
		case 2:{
			sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarModificar.do?idTmLinea='+idTmLinea+'&id='+idTmLinea, 'contenido', 'cargarComponentesModificarLinea(),formatearMontosRegistrarLinea()');
			break;
		}
		case 3:{
			if (confirm('\u00BFDesea eliminar esta linea?')){
				new Ajax.Request('/MidasWeb/contratos/linea/borrarLineaById.do?idTmLinea='+idTmLinea, {
					method : "post",asynchronous : false,
					onSuccess : function(transport) {
						var xmlDoc = transport.responseXML;
						var items = xmlDoc.getElementsByTagName("item");
						var item = items[0];
						var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
						var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
						alert(text);
						if (value > 0){
							lineasNegociacion.deleteRow(idTmLinea);
						}
					},
					onException : function(transport){
						alert("Se encontro un problema y no pudo borrarse");
					}
				});
			}
			break;
		}
		case 4:{
			break;
		}
		case 5:{
			break;
		}
	}
}

function cargarComponentesModificarLinea(){
	initParticipacionesPEGrids();
	mostrarParticipacionesPEGrids();
	initParticipacionesCPGrids();
	mostrarParticipacionesCPGrids()
}



function autorizarLineas(){
	var indiceSeleccionados = lineasNegociacion.getCheckedRows(0);
	var esUnElemento = trim(indiceSeleccionados).length;// (indiceSeleccionados.search(/,/)
														// > -1);
	var listaRojos = rojosSeleccionadosLineaNegociacion();
	if (listaRojos.length>0){
		alert("No es posible autorizar las lineas porque existe un elemento de rojo seleccionado");
		return;
	}
	$('idTmLinea').value = trim(indiceSeleccionados);
	if (esUnElemento){	
		if (confirm('\u00BFDesea Autorizar los elementos seleccionados?')){
			sendRequestAutorizarLineasNegociacion(document.configuracionLineaForm,
				'/MidasWeb/contratos/linea/autorizarLineas.do',
				null, 'mostrarLineaNegociacionGrids()');
		}
	}else{
		alert('Seleccione al menos un elemento de la lista');
	}
}

function cargarComponentesLineaContrato(){
	mostrarLineaContratoGrids();
	manipulaCalendarioLineas();
}

function cargarComponentesLineaNegociacion(){
	mostrarLineaNegociacionGrids();
	manipulaCalendarioLineas();
}

function cargarComponentesLineaVigencia(){
	mostrarLineaVigenciaGrids();
	manipulaCalendarioLineas();
}

function guardarAutorizarLinea(fobj){
	var estatusLinea;
	eliminarFormatoMontosRegistrarLinea();
	var mensajeValidacionCampos='';
	if ($('idTcSubRamo') == null)
		mensajeValidacionCampos='Subramo,';
	else
		if($('idTcSubRamo').value == '')
			mensajeValidacionCampos='Subramo, ';
	
	
	if ($('modoDistribucion') == null)
		mensajeValidacionCampos=mensajeValidacionCampos+'Tipo de Distribucion, ';
	else
		if($('modoDistribucion').value == '')
			mensajeValidacionCampos=mensajeValidacionCampos+'Tipo de Distribucion, ';
	
	if ($('maximo') == null)
		mensajeValidacionCampos=mensajeValidacionCampos+'Maximo, ';
	else
		if($('maximo').value == '')
			mensajeValidacionCampos=mensajeValidacionCampos+'Maximo, ';
	
	if (mensajeValidacionCampos == ''){	
		if(validarParticipacionesCPyPE()){
			new Ajax.Request("/MidasWeb/contratos/linea/agregar.do", {
					method : "post",
					asynchronous : false,
					parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
							true) : null,
					onSuccess : function(transport) {
								estatusLinea = obtenerEstatusLinea(transport.responseXML);
								formatearMontosRegistrarLinea();
								if (estatusLinea == 'guardadoNoAutorizado')
									alert('La l\u00ednea ha sido guardada pero no pudo ser autorizada, por favor revise que los datos sean correctos');
								else{
									if (estatusLinea == 'noGuardado')
										alert('La l\u00ednea no puede ser guardada debido a que se traslapa en fechas con otra l\u00ednea del mismo Subramo, escoja un Subramo distinto');
									else{
										if (estatusLinea == 'guardado')
											alert('La l\u00ednea ha sido guardada correctamente');
										else	
											if (estatusLinea == 'guardadoAutorizado')
													alert('La l\u00ednea ha sido guardada y Autorizada');
										
										if (estatusLinea == 'guardadoAutorizado' || estatusLinea == 'guardado')
											sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/listarLineaNegociacion.do', 'contenido','cargarComponentesLineaNegociacion()');
									}
									
									
								}
					} // End of onSuccess
					});
		
		}else{
			alert('La l\u00ednea no pudo guardarse, por favor revise que los datos sean correctos')
		}
	}else
		alert('Los siguientes datos son requeridos: ' + mensajeValidacionCampos + 'por favor introduzca los valores correspondientes');
	
	
}

function obtenerEstatusLinea(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var estatus = item.getElementsByTagName("estatus")[0].firstChild.nodeValue;
	
	return estatus;
}

/********************************************************
 *************************************Modulos cuota parte
 *******************************************************/
function autorizarCP(){
	new Ajax.Request("/MidasWeb/contratos/contratocuotaparte/autorizarContrato.do", {
		method : "post",
		asynchronous : false,
		parameters : "idTmContratoCuotaParte=" + $('idTmContratoCuotaParte').value,
		onSuccess : function(transport) {
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			alert(text);
		}
	});	
}

function crearContrato(idTmContratoCuotaParte, fechaInicial, fechaFinal, procentajeRetencion, porcentajeCesion){
	if(fechaInicial !== null && fechaFinal !== null && procentajeRetencion != null && porcentajeCesion != null){			
		new Ajax.Request("/MidasWeb/contratos/contratocuotaparte/crearContrato.do", {
			method : "post",
			asynchronous : false,
			parameters : "id="+idTmContratoCuotaParte+"&fechaInicial="+fechaInicial + 
						 "&fechaFinal="+fechaFinal+"&porcentajeRetencion="+procentajeRetencion+
						 "&porcentajeCesion="+porcentajeCesion,
			onSuccess : function(transport) {
				imprimeIdContrato(transport.responseXML);
			} 
		});
	}
	
}

function imprimeIdContrato(doc){	
	var id = doc.getElementsByTagName("id")[0].firstChild.nodeValue;		
	alert(id);
	$('idTmContratoCuotaParte').value = id;
}

function guardarContratoCP(){
	var esValidaParticipacion=true;
	var grid = participacionesPE_desplegarPE;
	var sumaParticipaciones=0;
	col = coberturaCotizacionGrids.getColIndexById('porcentajeParticipacion');
	for (var i=0; i<grid.getRowsNum() && grid.getRowsNum()>0; i++){
		sumaParticipaciones += parseFloat(grid.cellByIndex(i,col).getValue());
	}
	if (sumaParticipaciones>100.0){
		alert("La suma de los porcentajes de participaci\u00f3n en el Contrato Cuota Parte no debe ser mayor a 100");
		return;
	}
	if (esValidaParticipacion){
		sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');
	}
}
/************************************************************
 *************************************Modulos primer excedente
 ************************************************************/
function autorizarPE(){
	new Ajax.Request("/MidasWeb/contratos/contratoprimerexcedente/autorizarContrato.do", {
		method : "post",
		asynchronous : false,
		parameters : "idTmContratoPrimerExcedente=" + $('idTmContratoPrimerExcedente').value,
		onSuccess : function(transport) {
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			alert(text);
		} 
	});	
}

function calcularLimiteMaximoContratoPE(object){	
	var numeroPlenos = $('numeroPlenos').value;
	var montoPleno = $('montoPleno').value;
	
	if (numeroPlenos === null)
		numeroPlenos=0;
	if (montoPleno === null)
		montoPleno=0;
	var prod = numeroPlenos * montoPleno;
	if (!validarNumeroMaximoYDecimales(prod,20,8,false)){
		alert("El n\u00famero introducido genera un L\u00edmite M\u00e1ximo demasiado grande, introduce un n\u00famero menor");
		prod = 0;
		object.value = 0;
	}
	
	$('limiteMaximo').value = prod;
	document.contratoPrimerExcedenteForm.limiteMaximo = prod;
}

function guardarContratoPE(){
	var esValidaParticipacion=true;
	var grid = participacionesPE_desplegarPE;
	var sumaParticipaciones=0;
	col = coberturaCotizacionGrids.getColIndexById('porcentajeParticipacion');
	for (var i=0; i<grid.getRowsNum() && grid.getRowsNum()>0; i++){
		sumaParticipaciones += parseFloat(grid.cellByIndex(i,col).getValue());
	}
	if (sumaParticipaciones>100.0){
		alert("La suma de los porcentajes de participaci\u00f3n en el Contrato Primer Excedente no debe ser mayor a 100");
		return;
	}
	if (esValidaParticipacion){
		sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');
	}
}
/********************************************************
 *************************************Modulos comunes para cuota parte y primer excendete 
 *******************************************************/

function sendRequestParticipantesContrato(action, id){
	url = "";
	funcionJS = "void(0)";
	switch(action){
		case 1 : url = "/MidasWeb/contratos/participacion/mostrarModificar.do?id="+id; 
				sendRequest(document.contratoCuotaParteForm,url,'contenido',funcionJS);
				 break;
		case 2 : url = "/MidasWeb/contratos/participacion/mostrarBorrar.do?id="+id;
				 sendRequest(document.contratoCuotaParteForm,url,'contenido',funcionJS);
		      	 break;
		case 3 : url = "/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do?id="+id; 
				 funcionJS = 'initReaseguradoresPorCorredor('+id+');mostrarReaseguradoresPorCorredorGrids()';
				 sendRequest(document.contratoCuotaParteForm,url,'contenido',funcionJS);
				 break;
		case 4 : url = "/MidasWeb/contratos/participacion/mostrarModificar.do?id="+id; 
				 sendRequest(document.contratoPrimerExcedenteForm,url,'contenido',funcionJS);
				 break;
		case 5 : url = "/MidasWeb/contratos/participacion/mostrarBorrar.do?id="+id;
				 sendRequest(document.contratoPrimerExcedenteForm,url,'contenido',funcionJS);
		      	 break;
		case 6 : url = "/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do?id="+id; 
				 funcionJS = 'initReaseguradoresPorCorredor('+id+');mostrarReaseguradoresPorCorredorGrids()';
				 sendRequest(document.contratoPrimerExcedenteForm,url,'contenido',funcionJS);
				 break;
	}
	
}

function sendRequestParticipanteCorredor(action, id){
	url = "";
	switch(action){
		case 1 : url = "/MidasWeb/contratos/participacioncorredor/mostrarModificar.do?id="+id; 
				 break;
		case 2 : url = "/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do?id="+id; 
		      	 break;
	}
	sendRequest(document.participacionCorredorForm,url,'contenido',null);
}

function sendRequestMostrarDetalleContratos(action, id, fobj, idTcRamo, ventanaAccionada){
	document.configuracionLineaForm.idTcRamo.value = idTcRamo;
	sendRequestContrato(action, id, document.configuracionLineaForm,ventanaAccionada);
}

function sendRequestContrato(action, id, fobj, ventanaAccionada){
	url = '';
	reglaNavegacion = ''; // 0 indica que mostrarRegistrarContrato.do se
							// invoc� desde listar Contrato (CP o PE), 1 indica
							// que se invoc� desde agregar l�nea
	formularioOrigen = 'ListarContrato';
	
	if (action == 5){
		reglaNavegacion = 1; 
		action = 1;
		guardaSesionAgregarLinea();
	}
	if (action == 6){		
		reglaNavegacion = 1;
		action = 3;
		guardaSesionAgregarLinea();
	}
	if (action == 7){
		formularioOrigen='ListarLineas';
		action = 2;
	}
	if (action == 8){		
		formularioOrigen='ListarLineas';
		action = 4;
	}
	if (action == 9){ //Caso en donde se consulta el detalle de un contrato cuota parte desde el modulo Agregar Linea
		formularioOrigen='AgregarLinea';
		action = 2;
		guardaSesionAgregarLinea();
	}
	if (action == 10){ //Caso en donde se consulta el detalle de un contrato primer excedente desde el modulo Agregar Linea
		formularioOrigen='AgregarLinea';
		action = 4;
		guardaSesionAgregarLinea();
	}
	
	switch(action){
		case 1 : url = "/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do?idContratoCP="+id+"&reglaNavegacion="+reglaNavegacion;
				sendRequest(fobj,url,'contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');	
				break;
		case 2 : url = "/MidasWeb/contratos/contratocuotaparte/mostrarDetalleContrato.do?idContratoCP="+id+"&formularioOrigen="+formularioOrigen+"&ventanaAccionada="+ventanaAccionada;
				sendRequest(fobj,url,'contenido','initParticipantesContratoCPGrids_desplegarCP(),mostrarParticipantesCPGrids_desplegarCP()');
				break;
		case 3 : url = "/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do?idContratoPE="+id+"&reglaNavegacion="+reglaNavegacion;
				sendRequest(fobj,url,'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE()');
				break;
		case 4 : url = "/MidasWeb/contratos/contratoprimerexcedente/mostrarDetalleContrato.do?idContratoPE="+id+"&formularioOrigen="+formularioOrigen+"&ventanaAccionada="+ventanaAccionada; 
				sendRequest(fobj,url,'contenido','initParticipantesContratoPEGrids_desplegarPE(),mostrarParticipantesPEGrids_desplegarPE()');
				break;
		case -7:
		case -8:
			alert('No existe el contrato seleccionado');
			break;
	}
}

function validarParticipacionesCPyPE(){
	var totalPorcentajesCP = 0;
	var totalPorcentajesPE = 0;
	if (participacionesCP != null)
	for (var i=0; i<participacionesCP.getRowsNum(); i=i+1){	     	     	     
		id = participacionesCP.getRowId(i);
		cellComision = participacionesCP.cellByIndex(i,1);
		totalPorcentajesCP+=parseInt(cellComision.getValue());
		
		if(cellComision.getValue()===null || cellComision.getValue()===''){
			alert('Es necesario asignar un porcentaje de comision a todos los participantes del contrato de Cuota Parte.');
			
			return false;
		}
		
	}
	
	// para Participaciones Primer Excedente
	if (participacionesPE != null)
	for (var i=0; i<participacionesPE.getRowsNum(); i=i+1){	     	     	     
		id = participacionesPE.getRowId(i);
		cellComision = participacionesPE.cellByIndex(i,1);
		totalPorcentajesPE+=parseInt(cellComision.getValue());
		
		if(cellComision.getValue()===null || cellComision.getValue()===''){
			alert('Es necesario asignar un porcentaje de comision a todos los participantes del contrato de Primer Excedente.');
			return false;
		}
		
	}
	
	/*
	if (totalPorcentajesCP > 100){
		alert('La suma de los porcentajes de comisi\u00f3n en el Contrato de Cuota Parte no debe ser mayor a 100   ' + totalPorcentajesCP + '  %');
		return false;
	}
	
	if (totalPorcentajesPE > 100){
		alert('La suma de los porcentajes de comisi\u00f3n en el Contrato de Primer Excedente no debe ser mayor a 100');
		return false;
	}
	*/
	formarStringDeParticipacionesCPyPE();
	
	return true;
}

function formarStringDeParticipacionesCPyPE(){ // Funci�n para formar una
												// cadena del tipo
												// id_comision/id_comision(..)
												// para cargar en el formulario
												// y no perderse al cambiar de
												// pantalla
	var aux='';
	if ($('folioContratoCuotaParte').value == '' || $('folioContratoCuotaParte').value == '0')
		participacionesCP = null;
	
	if ($('folioContratoPrimerExcedente').value == '' || $('folioContratoPrimerExcedente').value == '0')
		participacionesPE = null;
	
	if (participacionesCP != null){
		for (var i=0; i<participacionesCP.getRowsNum(); i = i + 1){	     	     	     
			id = participacionesCP.getRowId(i);
			var cellComision = participacionesCP.cellByIndex(i,1);
			var cellComisionCombinacion = participacionesCP.cellByIndex(i,2);
			if (cellComision.getValue() != '')
				aux = aux + id + "_" + cellComision.getValue();
			else
				aux = aux + id + "_0.0";
		     
			if (cellComisionCombinacion.getValue() != ''){
				aux = aux + "_" + cellComisionCombinacion.getValue();
			}else
				aux = aux + "_0.0";
			
		     if(i !== participacionesCP.getRowsNum()-1)
		        aux = aux + "/";
		     
		}
		$("participacionesCP").value=aux;
	}
	if (participacionesPE != null){
		aux='';
		for (var i=0; i<participacionesPE.getRowsNum();i=i+1){	 
			id = participacionesPE.getRowId(i);
			var cellComision = participacionesPE.cellByIndex(i,1);
			var cellComisionCombinacion = participacionesPE.cellByIndex(i,2);
			if (cellComision.getValue() != '')
				aux = aux + id + "_" + cellComision.getValue();
			else
				aux = aux + id + "_0.0";
			
			if (cellComisionCombinacion.getValue() != ''){
				aux = aux + "_" + cellComisionCombinacion.getValue();
			}else
				aux = aux + "_0.0";
			
		     if(i!=participacionesPE.getRowsNum()-1)
		        aux = aux + "/";
		}
		$("participacionesPE").value=aux;
	}
	
	sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarAgregar.do',null, null);
}

function agregarParticipacionCuotaParte() {
	var errores = validarParticipacion();
	
	if(errores == "") { 
		sendRequest(document.participacionForm,
				'/MidasWeb/contratos/participacion/agregar.do', 
				'contenido',
				'mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');
	}
	else {
		mostrarVentanaMensaje("10", errores);
	}
}

function agregarParticipacionPrimerExcedente() {
	var errores = validarParticipacion();
	
	if(errores == "") { 
		sendRequest(document.participacionForm,
				'/MidasWeb/contratos/participacion/agregar.do',
				'contenido',
				'mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');
	}
	else {
		mostrarVentanaMensaje("10", errores);
	}
}

function modificarParticipacionCuotaParte() {
	var errores = validarParticipacion();
	if(errores == "") { 
		sendRequest(document.participacionForm,
				'/MidasWeb/contratos/participacion/modificar.do', 
				'contenido',
				'mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');
	}
	else {
		mostrarVentanaMensaje("10", errores);
	}
}

function modificarParticipacionPrimerExcedente() {
	var errores = validarParticipacion();
	if(errores == "") { 
		sendRequest(document.participacionForm,
				'/MidasWeb/contratos/participacion/modificar.do', 
				'contenido',
				'mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');
	}
	else {
		mostrarVentanaMensaje("10", errores);
	}
}

function validarParticipacion() {
	var errores = "";
	var porcentaje = $('porcentajeParticipacion').value;
	var sumaTotalParticipacion = parseFloat(document.participacionForm.sumaTotalParticipacion.value);

	if (!validarRango(porcentaje,0,100)) {
		errores += "Los porcentajes deben ser mayores o iguales que 0.00% y menores o iguales que 100%"
	}
	
	if (porcentaje != "") {
		var porcentajeDbl = parseFloat(porcentaje);
		var porcentajeTotal = sumaTotalParticipacion + porcentajeDbl;
		var maximoPermitido = 100 - sumaTotalParticipacion;
		if (porcentajeTotal > 100) {
			errores += "Se ha excedido del 100 % total de participacion." +
				" Lo maximo permitido para no excederlo es: " + maximoPermitido; 
		}
	}
	
	return errores;
}

function sendRequestAutorizarLineasNegociacion(fobj, actionURL, targetId, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
			// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
			if (targetId != null)
				$(targetId).innerHTML = transport.responseText;
			if (pNextFunction !== null && pNextFunction !== '') {
				eval(pNextFunction);
			} 
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			alert(text);
		} // End of onSuccess
		
	});
}

var calendarioDobleRptPerfilCartera;
function manipulaCalendarioDobleRptPerfilCartera(){	
	calendarioDobleRptPerfilCartera = new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
	calendarioDobleRptPerfilCartera.setDateFormat(formatoFechaCalendario());
	
	calendarioDobleRptPerfilCartera.leftCalendar.attachEvent("onClick",function(fecha){
 		$('fechaInicial').value=calendarioDobleRptPerfilCartera.leftCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
 			$('fechaFinal').value = '';
 		}
	});
	
 	calendarioDobleRptPerfilCartera.rightCalendar.attachEvent("onClick",function(fecha){
 		$('fechaFinal').value=calendarioDobleRptPerfilCartera.rightCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
			$('fechaInicial').value = '';
		}
 		calendarioDobleRptPerfilCartera.hide();
	});
 	calendarioDobleRptPerfilCartera.hide();
}

function mostrarCalendarioDobleRptPerfilCartera(){
	if (calendarioDobleRptPerfilCartera!=null){
		if (calendarioDobleRptPerfilCartera.isVisible()){
			calendarioDobleRptPerfilCartera.hide();
		}else{
			calendarioDobleRptPerfilCartera.show();
		}
	}
}

