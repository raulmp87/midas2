/******************************************************
 ************************************Modulos de la linea
 ******************************************************/
var lineasExtenderVigenciaPath;
var lineasExtenderVigencia;
function initLineasExtenderVigenciaGrids() {
	lineasExtenderVigenciaPath = '/MidasWeb/contratos/linea/mostrarContratosPorLinea.do?idTmLinea='+$('idTmLinea').value;	
}

function headersGridLineas(){
	return ",Vigencia, Ramo, Subramo,"+
	"Modo de Distribuci&oacute;n,Contrato CP, % Reaseguro CP, Cesi&oacute;n del CP, M&aacute;ximo, Contrato 1E,"+
	"Monto del Pleno, Num. Plenos, Cesi&oacute;n del 1E, Capacidad Max. L&iacute;nea, Total Retenci&oacute;n,Total Cedido,"+
		",,,,";
}

function idsGridLineas(){
	return "idLinea,vigencia,ramo,subramo,"
	+"modoDistribucion,ccp,preaseguroCP,cesionCP,maximo,cpe,"
	+"montoPleno,numeroPlenos,cesionPE,capacidadMaxima,totalRetencion,totalCedido,"+
		"iconoConsultar,iconoModificar,iconoBorrar,consultaCP,consultaCE";
}
function alignGridLineas(){
	return "center,center,center,center,"+
	"center,center,center,center,center,center,"
	+"center,center,center,center,center,center,"+
	"center,center,center,center,center";
}
function sortingGridLineas(){
	return "int,str,int,str,"+
	"str,str,str,str,str,str,"
	   +"str,str,str,str,str,str,"+
		"str,str,str,str,str";
}

function mostrarExtenderVigenciaGrid(){
	lineasExtenderVigencia = new dhtmlXGridObject('lineaExtenderVigenciaParteGrid');
	lineasExtenderVigencia.setHeader(headersGridLineas());
	lineasExtenderVigencia.setColumnIds(idsGridLineas());
	lineasExtenderVigencia.setInitWidths("0,250,170,170," +
			"150,100,130,100,100,100,"+
			"120,100,100,150,120,100,"+
			"0,0,0,0,0");
	lineasExtenderVigencia.setColAlign(alignGridLineas());
	lineasExtenderVigencia.setColSorting(alignGridLineas());
	lineasExtenderVigencia.setColTypes("ra,ro,ro,ro,"+
			"ro,ro,ro,ro,ro,ro,"
			+"ro,ro,ro,ro,ro,ro,"+
			"img,img,img,img,img");
	lineasExtenderVigencia.enableResizing("false,true,true,true," +
			"true,true,true,true,true,true," +
			"true,true,true,true,true,true," +
			"false,false,false,false,false");
	lineasExtenderVigencia.setColumnHidden(0, true);
	lineasExtenderVigencia.setColumnHidden(16, true);
	lineasExtenderVigencia.setColumnHidden(17, true);
	lineasExtenderVigencia.setColumnHidden(18, true);
	lineasExtenderVigencia.setColumnHidden(19, true);
	lineasExtenderVigencia.setColumnHidden(20, true);
	lineasExtenderVigencia.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	lineasExtenderVigencia.setSkin("light");		
	lineasExtenderVigencia.enableDragAndDrop(false);	
	lineasExtenderVigencia.enableLightMouseNavigation(false);
	lineasExtenderVigencia.attachEvent("onEditCell",seleccionarLineasExtenderVigencia);
	lineasExtenderVigencia.init();	
	initLineasExtenderVigenciaGrids();
	
	lineasExtenderVigencia.load(lineasExtenderVigenciaPath, null, 'json');
	
}
function seleccionarLineasExtenderVigencia(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
				
	if(lineasExtenderVigencia.cells(rowId,cellInd).isChecked()){
		$('seleccionado').value = rowId;		
		processor.setUpdated(rowId,false,"update");
	}else{
		processor.setUpdated(rowId,true,"update");
	}
}

var lineasContratoPath;
var lineasContratos;
function initLineasContratoGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/contratocuotaparte/seleccionar.do");
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	
	if (fechaInicial.length != 0){
		fechaInicial += ' ' +trim($('horaInicial').value); 
	}
	if (fechaFinal.length != 0){
		fechaFinal += ' ' + trim($('horaFinal').value); 
	}
	
	lineasContratosPath = '/MidasWeb/contratos/linea/listarLineaContratoFiltrado.do?fi='+
					fechaInicial+'&ff='+fechaFinal+
					'&estatus='+$('estatus').value+"&idTcRamo="+$('idTcRamo').value;
}

function mostrarLineaContratoGrids() {  
	lineasContratos = new dhtmlXGridObject('lineaContratoParteGrid');
	lineasContratos.setHeader(headersGridLineas());
	lineasContratos.setColumnIds(idsGridLineas());
	lineasContratos.setInitWidths("20,250,170,170," +
			"150,100,130,100,100,100,"+
			"120,100,100,150,120,100,"+
			"30,0,0,30,30");
	lineasContratos.setColAlign(alignGridLineas());
	lineasContratos.setColSorting(sortingGridLineas());
	lineasContratos.setColTypes("ra,ro,ro,ro,"+
			"ro,ro,ro,ro,ro,ro,"
			+"ro,ro,ro,ro,ro,ro,"+
			"img,img,img,img,img");

	lineasContratos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	lineasContratos.setSkin("light");		
	lineasContratos.enableDragAndDrop(false);	
	lineasContratos.enableLightMouseNavigation(false);
	lineasContratos.attachEvent("onEditCell",seleccionarLineasContrato);
	lineasContratos.init();	

	initLineasContratoGrids();
	lineasContratos.load(lineasContratosPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(lineasContratos);		
}

function seleccionarLineasContrato(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
				
	if(lineasContratos.cells(rowId,cellInd).isChecked()){
		$('seleccionado').value = rowId;		
		processor.setUpdated(rowId,false,"update");
	}else{
		processor.setUpdated(rowId,true,"update");
	}
}

var lineasVigenciaPath;
var lineasVigencias;
function initLineasVigenciaGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/contratocuotaparte/seleccionar.do");
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	
	if (fechaInicial.length != 0){
		fechaInicial += ' ' +trim($('horaInicial').value); 
	}
	if (fechaFinal.length != 0){
		fechaFinal += ' ' + trim($('horaFinal').value); 
	}
	lineasVigenciasPath = '/MidasWeb/contratos/linea/listarLineaVigenciaFiltrado.do?fi='+
	fechaInicial+'&ff='+fechaFinal+	
	'&estatus='+$('estatus').value+"&idTcRamo="+$('idTcRamo').value;
}

function mostrarLineaVigenciaGrids() {
	lineasVigencias = new dhtmlXGridObject('lineaVigenciaParteGrid');
	lineasVigencias.setHeader(headersGridLineas());
	lineasVigencias.setColumnIds(idsGridLineas());
	lineasVigencias.setInitWidths("20,250,170,170," +
			"150,100,130,100,100,100,"+
			"120,100,100,150,120,100,"+
			"30,0,0,30,30");
	lineasVigencias.setColAlign(alignGridLineas());
	lineasVigencias.setColSorting(sortingGridLineas());
	lineasVigencias.setColTypes("ra,ro,ro,ro,"+
			"ro,ro,ro,ro,ro,ro,"
			+"ro,ro,ro,ro,ro,ro,"+
			"img,img,img,img,img");

	lineasVigencias.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	lineasVigencias.setSkin("light");		
	lineasVigencias.enableDragAndDrop(false);	
	lineasVigencias.enableLightMouseNavigation(false);
	lineasVigencias.attachEvent("onEditCell",seleccionarLineasVigencia);
	lineasVigencias.init();	

	initLineasVigenciaGrids();
	
	lineasVigencias.load(lineasVigenciasPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(lineasVigencias);	
	
}

function seleccionarLineasVigencia(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
				
	if(lineasVigencias.cells(rowId,cellInd).isChecked()){
		$('seleccionado').value = rowId;		
		processor.setUpdated(rowId,false,"update");
	}else{
		processor.setUpdated(rowId,true,"update");
	}
}


var lineasNegociacionPath;
var lineasNegociacion;
function initLineasNegociacionGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/contratocuotaparte/seleccionar.do");
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	
	if (fechaInicial.length != 0){
		fechaInicial += ' ' +trim($('horaInicial').value); 
	}
	if (fechaFinal.length != 0){
		fechaFinal += ' ' + trim($('horaFinal').value); 
	}
	lineasNegociacionPath = '/MidasWeb/contratos/linea/listarLineaNegociacionFiltrado.do?fi='+
	fechaInicial+'&ff='+fechaFinal+	
	'&estatus='+$('estatus').value+"&idTcRamo="+$('idTcRamo').value;
}

function mostrarLineaNegociacionGrids() {
	lineasNegociacion = new dhtmlXGridObject('lineaNegociacionParteGrid');
	lineasNegociacion.setHeader(headersGridLineas()+",,");
	lineasNegociacion.setColumnIds(idsGridLineas()+",idTcRamo,idTcSubRamo");
	lineasNegociacion.setInitWidths("20,250,170,170," +
			"150,100,130,100,100,100,"+
			"120,100,100,150,120,100,"+
			"0,30,30,30,30,0,0");
	lineasNegociacion.setColAlign(alignGridLineas()+",center,center");
	lineasNegociacion.setColSorting(sortingGridLineas()+",int,int");
	lineasNegociacion.setColTypes("ch,ro,ro,ro,"+
			"ro,ro,ro,ro,ro,ro,"
			+"ro,ro,ro,ro,ro,ro,"
			+"img,img,img,img,img,ro,ro");

	lineasNegociacion.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	lineasNegociacion.setSkin("light");		
	lineasNegociacion.enableDragAndDrop(false);	
	lineasNegociacion.enableLightMouseNavigation(false);
	lineasNegociacion.attachEvent("onEditCell",seleccionarLineasNegociacion);
	lineasNegociacion.init();	
	initLineasNegociacionGrids();
	
	lineasNegociacion.load(lineasNegociacionPath, validarLineasNegociacion, 'json');	
}

function seleccionarLineasNegociacion(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
	if(lineasNegociacion.cells(rowId,cellInd).isChecked()){
		var indiceSeleccionados = lineasNegociacion.getCheckedRows(0);
		var listaSeleccionados = indiceSeleccionados.split(",");
		var i = 0;			
		if (rowId <0 ){
			for (;i<listaSeleccionados.length;i++)
				lineasNegociacion.setRowTextNormal(listaSeleccionados[i]);
			lineasNegociacion.checkAll(false);
			lineasNegociacion.cells(rowId,cellInd).setValue(1);
		}else{
			for (;i<listaSeleccionados.length;i++)
				if (listaSeleccionados[i]<0){
					lineasNegociacion.cells(listaSeleccionados[i],cellInd).setValue(0);
					lineasNegociacion.setRowTextNormal(listaSeleccionados[i]);
				}
		}
		lineasNegociacion.setRowTextBold(rowId);
	}else{
		lineasNegociacion.setRowTextNormal(rowId);
	}
	return true;
}

function validarLineasNegociacion(){	
	var rowsString = lineasNegociacion.getAllRowIds();
	var rows = rowsString.split(",");

	for (var i=0;i<rows.length;i++){
		if (rows[i]<0){			
			lineasNegociacion.setRowTextStyle(rows[i], "color: red");// font-weight: bold;
		}
	}
}

/********************************************************
 *************************************Modulos cuota parte
 *******************************************************/

var contratosPath;
var contratos;
function initContratosCuotaParteGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/contratocuotaparte/seleccionar.do");
	contratosPath = '/MidasWeb/contratos/contratocuotaparte/listarFiltrado.do?fi='+
					$('fechaInicial').value+'&ff='+$('fechaFinal').value;	
}

function mostrarContratosCuotaParteGrids() {
	contratos = new dhtmlXGridObject('contratosCuotaParteGrid');
	contratos.setHeader(",Folio,% Cesi\u00F3n,% Retenci\u00F3n,Tipo de moneda,Estatus,,");
	contratos.setColumnIds("seleccionado,folio,porcentajeCesion,porcentajeRetencion,tipoMoneda,estatus,x,y");
	contratos.setInitWidths("20,60,80,100,150,110,30,30");
	contratos.setColAlign("center,center,center,center,center,center");
	contratos.setColSorting("int,str,int,int,str,str,str,str");
	contratos.setColTypes("ra,ro,ro,ro,ro,ro,img,img");
	contratos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	contratos.setSkin("light");
	contratos.enableDragAndDrop(false);
	contratos.enableLightMouseNavigation(false);
	contratos.attachEvent("onEditCell",seleccionarContratoCP);
	contratos.init();	
	
	initContratosCuotaParteGrids();
	
	contratos.load(contratosPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(contratos);	
}

function seleccionarContratoCP(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
				
	if(contratos.cells(rowId,cellInd).isChecked()){
		$('seleccionado').value = rowId;		
		processor.setUpdated(rowId,false,"update");
	}else{
		processor.setUpdated(rowId,true,"update");
	}
}

var participacionesCPPath;
var participacionesCP;

function initParticipacionesCPGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/guardarParticipacionesCP.do");
	participacionesCPPath = '/MidasWeb/contratos/linea/cargarParticipacionesCP.do?idTmLinea='+ $('idTmLinea').value +'&idContratoCP='+$('idTmContratoCuotaParte').value+"&formatoGridADesplegar=0";	
}

function initParticipacionesCPGrids_recargarTemporal() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/guardarParticipacionesCP.do");
	participacionesCPPath = '/MidasWeb/contratos/linea/cargarParticipacionesCP.do?participacionesCP='+$('participacionesCP').value;	
}

function mostrarParticipacionesCPGrids() {
	participacionesCP = new dhtmlXGridObject('participacionesCPGrid');									  
	participacionesCP.setHeader("Nombre del participante,% Comisi\u00F3n, % Comisi\u00F3n Primer Riesgo");
	participacionesCP.setColumnIds("nombre,comision,comisionCombinacion");
	participacionesCP.setInitWidths("200,90,200");
	participacionesCP.setColAlign("center,center,center");
	participacionesCP.setColSorting("str,int,int");
	participacionesCP.setColTypes("ro,ed,ed");
	participacionesCP.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesCP.setSkin("light");
	participacionesCP.attachEvent("onEditCell",function(stage,rowId,cellIndex,newValue,oldValue){
		if (stage==2&&(cellIndex==1 || cellIndex==2)){			
			if (newValue < 0 || newValue > 100){
				alert('El porcentaje de Comisi\u00F3n debe ser entre 0 y 100%');
				return false;
			}
			if (!isNumeric(valor) && !isFloat(valor)){
				alert('El porcentaje de Comisi\u00F3n debe ser entre 0 y 100%');
				return false;
			}
			
		}
			return true;
			});
	participacionesCP.enableDragAndDrop(false);	
	participacionesCP.enableLightMouseNavigation(false);
	participacionesCP.init();
	
	participacionesCP.load(participacionesCPPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesCP);	
}


/*
 * C�digo para el grid de Participantes (en Agregar participantes a un Contrato
 * Cuota Parte)
 */

function initParticipantesContratoCPGrids_registrarCP() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/guardarParticipacionesCP.do");
	participacionesCPPath = '/MidasWeb/contratos/linea/cargarParticipacionesCP.do?idContratoCP='+$('idTmContratoCuotaParte').value+"&formatoGridADesplegar=1";	
}

var participacionesCP_registrarCP;

function mostrarParticipantesCPGrids_registrarCP(){
	participacionesCP_registrarCP = new dhtmlXGridObject('participacionesCPGrid');								  
	participacionesCP_registrarCP.setHeader("Nombre,CNFS,Cuenta (Pesos), Cuenta (D\u00F3lares),Contacto,% Participaci\u00F3n,,,Reasegurador");
	participacionesCP_registrarCP.setColumnIds("nombre,cnfs,cuentaPesos,cuentaDolares,contacto,porcentajeParticipacion,x,y,z");
	participacionesCP_registrarCP.setInitWidths("150,100,150,150,150,120,40,40,100");
	participacionesCP_registrarCP.setColAlign("center,center,center,center,center,center,center,center,center");
	participacionesCP_registrarCP.setColSorting("str,str,str,str,str,str,str,str,str");
	participacionesCP_registrarCP.setColTypes("ro,ro,ro,ro,ro,ro,img,img,img");
	participacionesCP_registrarCP.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesCP_registrarCP.setSkin("light");		
	participacionesCP_registrarCP.enableDragAndDrop(false);
	participacionesCP_registrarCP.enableLightMouseNavigation(false);
	participacionesCP_registrarCP.init();
	
	participacionesCP_registrarCP.load(participacionesCPPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesCP_registrarCP);
}

/*
 * Fin del C�digo para el grid de Participantes (en Agregar participantes a un
 * Contrato Cuota Parte)
 */

/*
 * C�digo para el grid de Participantes (en Desplegar participantes de un
 * Contrato Cuota Parte)
 */
var participacionesCP_desplegarCP;
var participacionesCP_desplegarCP_Path;

function initParticipantesContratoCPGrids_desplegarCP() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/cargarParticipacionesCP.do");
	participacionesCP_desplegarCP_Path = '/MidasWeb/contratos/linea/cargarParticipacionesCP.do?idContratoCP='+$('idTmContratoCuotaParte').value+"&formatoGridADesplegar=2";	
}

function mostrarParticipantesCPGrids_desplegarCP(){
	participacionesCP_desplegarCP = new dhtmlXGridObject('participacionesCPGrid_desplegar');								  
	participacionesCP_desplegarCP.setHeader("Nombre,CNFS,Cuenta (Pesos),Cuenta (D\u00F3lares),Contacto,% Participaci\u00F3n");
	participacionesCP_desplegarCP.setColumnIds("nombre,cnfs,cuentaPesos,cuentaDolares,contacto,porcentajeParticipacion");
	participacionesCP_desplegarCP.setInitWidths("150,100,150,150,150,130,40,40,100");
	participacionesCP_desplegarCP.setColAlign("center,center,center,center,center,center");
	participacionesCP_desplegarCP.setColSorting("str,str,str,str,str,str");
	participacionesCP_desplegarCP.setColTypes("ro,ro,ro,ro,ro,ro");
	participacionesCP_desplegarCP.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesCP_desplegarCP.setSkin("light");		
	participacionesCP_desplegarCP.enableDragAndDrop(false);
	participacionesCP_desplegarCP.enableLightMouseNavigation(false);
	participacionesCP_desplegarCP.init();
	
	participacionesCP_desplegarCP.load(participacionesCP_desplegarCP_Path, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesCP_desplegarCP);
}
/*
 * Fin del C�digo para el grid de Participantes (en Desplegar participantes de
 * un Contrato Cuota Parte)
 */

/************************************************************
 *************************************Modulos primer excedente
 ************************************************************/
/*
 * C�digo para el grid de Participantes (en Desplegar participantes de un
 * Contrato 1E)
 */
var participacionesPE_desplegarPE;
var participacionesPE_desplegarPE_Path;

function initParticipantesContratoPEGrids_desplegarPE() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/cargarParticipacionesPE.do");
	participacionesPE_desplegarPE_Path = '/MidasWeb/contratos/linea/cargarParticipacionesPE.do?idContratoPE='+$('idTmContratoPrimerExcedente').value+"&formatoGridADesplegar=2";	
}

function mostrarParticipantesPEGrids_desplegarPE(){
	participacionesPE_desplegarPE = new dhtmlXGridObject('participacionesPEGrid_desplegar');								  
	participacionesPE_desplegarPE.setHeader("Nombre,CNFS,Cuenta (Pesos), Cuenta (D\u00F3lares),Contacto,Porcentaje Participaci\u00F3n");
	participacionesPE_desplegarPE.setColumnIds("nombre,cnfs,cuentaPesos,cuentaDolares,contacto,porcentajeParticipacion");
	participacionesPE_desplegarPE.setInitWidths("150,100,150,150,150,200,40,40,100");
	participacionesPE_desplegarPE.setColAlign("center,center,center,center,center,center");
	participacionesPE_desplegarPE.setColSorting("str,str,str,str,str,str");
	participacionesPE_desplegarPE.setColTypes("ro,ro,ro,ro,ro,ro");
	participacionesPE_desplegarPE.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesPE_desplegarPE.setSkin("light");		
	participacionesPE_desplegarPE.enableDragAndDrop(false);
	participacionesPE_desplegarPE.enableLightMouseNavigation(false);
	participacionesPE_desplegarPE.init();
	
	participacionesPE_desplegarPE.load(participacionesPE_desplegarPE_Path, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesPE_desplegarPE);
}
/*
 * Fin del C�digo para el grid de Participantes (en Desplegar participantes de
 * un Contrato Cuota Parte)
 */

/*
 * C�digo para el grid de Participantes (en Agregar participantes a un Contrato
 * Primer Excedente)
 */

var participacionesPEPath_registrarPE;
var participacionesPE_registrarPE;

function initParticipantesContratoPEGrids_registrarPE() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/guardarParticipacionesPE.do");
	participacionesPEPath_registrarPE = '/MidasWeb/contratos/linea/cargarParticipacionesPE.do?idContratoPE='+$('idTmContratoPrimerExcedente').value+"&formatoGridADesplegar=1";	
}

function mostrarParticipantesPEGrids_registrarPE(){
	participacionesPE_registrarPE = new dhtmlXGridObject('participacionesPEGrid');								  
	participacionesPE_registrarPE.setHeader("Nombre,CNFS,Cuenta (Pesos), Cuenta (D\u00F3lares),Contacto,% Participaci\u00F3n,,,Reasegurador");
	participacionesPE_registrarPE.setColumnIds("nombre,cnfs,cuentaPesos,cuentaDolares,contacto,porcentajeParticipacion,x,y,z");
	participacionesPE_registrarPE.setInitWidths("150,100,150,150,150,120,40,40,100");
	participacionesPE_registrarPE.setColAlign("center,center,center,center,center,center,center,center,center");
	participacionesPE_registrarPE.setColSorting("str,str,str,str,str,str,str,str,str");
	participacionesPE_registrarPE.setColTypes("ro,ro,ro,ro,ro,ro,img,img,img");
	participacionesPE_registrarPE.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesPE_registrarPE.setSkin("light");		
	participacionesPE_registrarPE.enableDragAndDrop(false);
	participacionesPE_registrarPE.enableLightMouseNavigation(false);
	participacionesPE_registrarPE.init();
	
	participacionesPE_registrarPE.load(participacionesPEPath_registrarPE, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesPE_registrarPE);
}

/* C�digo para el grid de Participaciones de Contrato Primer Excedente */

var participacionesPEPath;
var participacionesPE;

function initParticipacionesPEGrids() {
	processor = new dataProcessor("/MidsasWeb/contratos/linea/guardarParticipacionesPE.do");
	participacionesPEPath = '/MidasWeb/contratos/linea/cargarParticipacionesPE.do?idTmLinea='+ $('idTmLinea').value +'&idContratoPE='+$('idTmContratoPrimerExcedente').value+"&formatoGridADesplegar=0";	
}

function initParticipacionesPEGrids_recargarTemporal() {
	processor = new dataProcessor("/MidasWeb/contratos/linea/guardarParticipacionesPE.do");
	participacionesPEPath = '/MidasWeb/contratos/linea/cargarParticipacionesPE.do?participacionesPE='+$('participacionesPE').value;	
}

function mostrarParticipacionesPEGrids(){
	participacionesPE = new dhtmlXGridObject('participacionesPEGrid');								  
	participacionesPE.setHeader("Nombre del participante,% Comisi\u00F3n, % Comisi\u00F3n Primer Riesgo");
	participacionesPE.setColumnIds("nombre,comision,comisionCombinacion");
	participacionesPE.setInitWidths("200,90,200");
	participacionesPE.setColAlign("center,center,center");
	participacionesPE.setColSorting("str,int,int");
	participacionesPE.setColTypes("ro,ed,ed");
	participacionesPE.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesPE.setSkin("light");
	participacionesPE.attachEvent("onEditCell",function(stage,rowId,cellIndex,newValue,oldValue){
		if (stage==2&&(cellIndex==1 || cellIndex==2)){			
			if (newValue < 0 || newValue > 100 ){
				alert('El porcentaje de Comisi\u00F3n debe ser entre 0 y 100%');
				return false;
			}
			
		}
			return true;
			});
	participacionesPE.enableDragAndDrop(false);
	participacionesPE.enableLightMouseNavigation(false);
	participacionesPE.init();
	
	
	participacionesPE.load(participacionesPEPath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(participacionesPE);
}

/* Fin del C�digo para el grid de Participaciones de Contrato Primer Excedente */

/* Funciones M�dulo Contrato Primer excedente */

function initContratosPrimerExcedenteGrids() {
	processor = new dataProcessor("/MidasWeb/contratos/contratoprimerexcedente/seleccionar.do");
	contratosPrimerExcedentePath = '/MidasWeb/contratos/contratoprimerexcedente/listarFiltradoEnGrid.do?fi='+
					$('fechaInicial').value+'&ff='+$('fechaFinal').value;	
}

function mostrarContratosPrimerExcedenteGrids() {
	contratosPrimerExcedente = new dhtmlXGridObject('contratosPrimerExcedenteGrid');									  
	contratosPrimerExcedente.setHeader(",Folio Contrato,N&uacute;mero Plenos,Monto Pleno,Tipo de moneda,Monto M&aacute;ximo,Estatus,,");
	contratosPrimerExcedente.setColumnIds("seleccionado,folio,numeroPlenos,montoPleno,tipoMoneda,montoMaximo,estatus,x,y");
	contratosPrimerExcedente.setInitWidths("20,150,150,100,150,110,110,30,30");
	contratosPrimerExcedente.setColAlign("center,center,center,center,center,center,center,center,center");
	contratosPrimerExcedente.setColSorting("int,str,int,int,str,int,str,str,str");
	contratosPrimerExcedente.setColTypes("ra,ro,ro,ro,ro,ro,ro,img,img");
	contratosPrimerExcedente.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	contratosPrimerExcedente.setSkin("light");
	contratosPrimerExcedente.enableDragAndDrop(false);
	contratosPrimerExcedente.enableLightMouseNavigation(false);
	contratosPrimerExcedente.attachEvent("onEditCell",seleccionarContratoPE);
	contratosPrimerExcedente.init();
	
	initContratosPrimerExcedenteGrids();
	
	contratosPrimerExcedente.load(contratosPrimerExcedentePath, null, 'json');	

	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(contratosPrimerExcedente);	
}

function seleccionarContratoPE(stage,rowId,cellInd) {
	if(stage==0)
		return true;		
				
	if(contratosPrimerExcedente.cells(rowId,cellInd).isChecked()){
		$('seleccionado').value = rowId;		
		processor.setUpdated(rowId,false,"update");
	}else{
		processor.setUpdated(rowId,true,"update");
	}
}

/********************************************************
 *************************************Modulos comunes para cuota parte y primer excendete 
 *******************************************************/

var reaseguradoresPorCorredor;
var reaseguradoresPorCorredorPath;

function initReaseguradoresPorCorredor(idTdParticipacion) {
	processor = new dataProcessor('/MidasWeb/contratos/participacioncorredor/agregar.do');
	reaseguradoresPorCorredorPath = '/MidasWeb/contratos/participacioncorredor/cargarReaseguradores.do?idTdParticipacion='+idTdParticipacion;
}

function mostrarReaseguradoresPorCorredorGrids() {
	reaseguradoresPorCorredor = new dhtmlXGridObject('reaseguradoresPorCorredorGrid');									  
	reaseguradoresPorCorredor.setHeader("Nombre,Nombre Corto,CNFS,% Participaci\u00F3n,,");
	reaseguradoresPorCorredor.setColumnIds("nombre,nombreCorto,cnfs,porcentajeParticipacion,x,y");
	reaseguradoresPorCorredor.setInitWidths("200,200,100,150,40,40");
	reaseguradoresPorCorredor.setColAlign("center,center,center,center,center");
	reaseguradoresPorCorredor.setColSorting("str,str,str,int,str,str");
	reaseguradoresPorCorredor.setColTypes("ro,ro,ro,ro,img,img");
	reaseguradoresPorCorredor.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	reaseguradoresPorCorredor.setSkin("light");		
	reaseguradoresPorCorredor.enableDragAndDrop(false);
	reaseguradoresPorCorredor.enableLightMouseNavigation(false);
	reaseguradoresPorCorredor.init();
	
	reaseguradoresPorCorredor.load(reaseguradoresPorCorredorPath, null, 'json');
	
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");	
	processor.init(reaseguradoresPorCorredor);
}

function mostarContratosLineaVigenciaContrato(){
	participacionesCP = new dhtmlXGridObject('participacionesCPGrid');									  
	participacionesCP.setHeader("Nombre del participante,% Comisi\u00F3n, % Comisi\u00F3n Primer Riesgo");
	participacionesCP.setColumnIds("nombre,comision,comisionCombinacion");
	participacionesCP.setInitWidths("200,90,200");
	participacionesCP.setColAlign("center,center,center");
	participacionesCP.setColSorting("str,int,int");
	participacionesCP.setColTypes("ro,ro,ro");
	participacionesCP.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesCP.setSkin("light");
	participacionesCP.enableDragAndDrop(false);	
	participacionesCP.enableLightMouseNavigation(false);
	participacionesCP.init();
	participacionesCPPath = '/MidasWeb/contratos/linea/cargarParticipacionesCP.do?idTmLinea='+ $('idTmLinea').value +'&idContratoCP='+$('idTmContratoCuotaParte').value+"&formatoGridADesplegar=0";		
	participacionesCP.load(participacionesCPPath, null, 'json');	
	
	participacionesPE = new dhtmlXGridObject('participacionesPEGrid');								  
	participacionesPE.setHeader("Nombre del participante,% Comisi\u00F3n, % Comisi\u00F3n Primer Riesgo");
	participacionesPE.setColumnIds("nombre,comision,comisionCombinacion");
	participacionesPE.setInitWidths("200,90,200");
	participacionesPE.setColAlign("center,center,center");
	participacionesPE.setColSorting("str,int,int");
	participacionesPE.setColTypes("ro,ro,ro");
	participacionesPE.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionesPE.setSkin("light");
	participacionesPE.enableDragAndDrop(false);
	participacionesPE.enableLightMouseNavigation(false);
	participacionesPE.init();
	participacionesPEPath = '/MidasWeb/contratos/linea/cargarParticipacionesPE.do?idTmLinea='+ $('idTmLinea').value +'&idContratoPE='+$('idTmContratoPrimerExcedente').value+"&formatoGridADesplegar=0";
	participacionesPE.load(participacionesPEPath, null, 'json');	

}