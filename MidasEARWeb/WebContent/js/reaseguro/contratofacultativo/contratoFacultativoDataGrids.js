var polizaRptEstadisticaFiscalFacultativoGrid;
function mostrarPolizaRptEstadisticaFiscalFacultativoGrid(nomAsegurado, numPoliza){
	if (validarFormatoNumeroPolizaRptsReaseguro(numPoliza)){
		var polizaRptEstadisticaFiscalFacultativoGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do?nomAsegurado='+nomAsegurado+'&numPoliza='+numPoliza;
		polizaRptEstadisticaFiscalFacultativoGrid = new dhtmlXGridObject('polizaRptEstadisticaFiscalFacultativoGrid');
		polizaRptEstadisticaFiscalFacultativoGrid.setHeader(",N&uacute;mero de P&oacute;liza,Nom Asegurado,Fecha de emisi&oacute;n,Fecha de Vigencia");	
		polizaRptEstadisticaFiscalFacultativoGrid.setColumnIds("seleccionado,numeroPoliza,nomAsegurado,fechaEmision,fechaVigencia");
		polizaRptEstadisticaFiscalFacultativoGrid.setInitWidths("30,200,150,150,200");
		polizaRptEstadisticaFiscalFacultativoGrid.setColAlign("center,center,center,center,center");
		polizaRptEstadisticaFiscalFacultativoGrid.setColSorting("int,str,str,str,str");
		polizaRptEstadisticaFiscalFacultativoGrid.setColTypes("ra,ro,ro,ro,ro");
		polizaRptEstadisticaFiscalFacultativoGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
	    }); 
		polizaRptEstadisticaFiscalFacultativoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		polizaRptEstadisticaFiscalFacultativoGrid.setSkin("light");		
		polizaRptEstadisticaFiscalFacultativoGrid.enableDragAndDrop(false);	
		polizaRptEstadisticaFiscalFacultativoGrid.enableLightMouseNavigation(false);
		polizaRptEstadisticaFiscalFacultativoGrid.init();	
		
		polizaRptEstadisticaFiscalFacultativoGrid.load(polizaRptEstadisticaFiscalFacultativoGridPath, null, 'json');
	}
}

var configuracionReaseguradoresContratoFacultativoGrid;
function mostrarConfiguracionReaseguradoresContratoFacultativoGrid(){
	mostrarIndicadorCargaGenerico('loadingDataGrid');
	var idTmContratoFacultativo = $('idTmContratoFacultativo').value;
	configuracionReaseguradoresContratoFacultativoGrid = new dhtmlXGridObject('configuracionReaseguradoresContratoFacultativoGridDiv');
	configuracionReaseguradoresContratoFacultativoGrid.setHeader("Reasegurador,Forma de Pago, Prima,");	
	configuracionReaseguradoresContratoFacultativoGrid.setColumnIds("reasegurador,formaPago,prima,x");
	configuracionReaseguradoresContratoFacultativoGrid.setInitWidths("300,150,150,30");
	configuracionReaseguradoresContratoFacultativoGrid.setColAlign("center,center,center,center");
	configuracionReaseguradoresContratoFacultativoGrid.setColSorting("str,str,str,str");
	configuracionReaseguradoresContratoFacultativoGrid.setColTypes("ro,ro,ro,img");
	configuracionReaseguradoresContratoFacultativoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	configuracionReaseguradoresContratoFacultativoGrid.setSkin("light");		
	configuracionReaseguradoresContratoFacultativoGrid.enableDragAndDrop(false);	
	configuracionReaseguradoresContratoFacultativoGrid.enableLightMouseNavigation(false);
	configuracionReaseguradoresContratoFacultativoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingDataGrid');
    }); 
	configuracionReaseguradoresContratoFacultativoGrid.init();	

	configuracionReaseguradoresContratoFacultativoGrid.load('/MidasWeb/contratofacultativo/contrato/mostrarGridConfiguracionReaseguradores.do?idTmContratoFacultativo='+idTmContratoFacultativo, null, 'json');
}

var configuracionReaseguradorContratoFacultativoGrid;
function mostrarConfiguracionReaseguradorContratoFacultativoGrid(idtcreaseguradorcorredor, idTmContratoFacultativo){
	configuracionReaseguradorContratoFacultativoGrid = new dhtmlXGridObject('configuracionReaseguradorContratoFacultativoGridDiv');
	configuracionReaseguradorContratoFacultativoGrid.setHeader("Fecha de Pago,Monto");	
	configuracionReaseguradorContratoFacultativoGrid.setColumnIds("fechaPago,monto");
	configuracionReaseguradorContratoFacultativoGrid.setInitWidths("150,200");
	configuracionReaseguradorContratoFacultativoGrid.setColAlign("center,center");
	configuracionReaseguradorContratoFacultativoGrid.setColSorting("str,str");
	configuracionReaseguradorContratoFacultativoGrid.setColTypes("ro,ed");
	configuracionReaseguradorContratoFacultativoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	configuracionReaseguradorContratoFacultativoGrid.setSkin("light");		
	configuracionReaseguradorContratoFacultativoGrid.enableDragAndDrop(false);	
	configuracionReaseguradorContratoFacultativoGrid.enableLightMouseNavigation(false);
	configuracionReaseguradorContratoFacultativoGrid.attachEvent("onEditCell",function(stage,rowId,cellIndex,newValue,oldValue){
		if (rowId == -1 || rowId == -2)
			return false;
		
		if (stage == 2){
			if(newValue != ''){
				newValue = newValue.replace("$", "");
				var cont = newValue.length;
				for (i = 0; i < cont; i=i+1)
					newValue = newValue.replace(",", "");
			}
			if (isNumeric(newValue) || isFloat(newValue)){
				if (newValue < 0)
					return false;
				configuracionReaseguradorContratoFacultativoGrid.cellById(rowId,cellIndex).setValue(formatCurrency4Decs(newValue));
				recalcularMontoTotalPagosConfiguracionReaseguradorContratoFacultativo();
				return true;
			}else{
				alert('La cifra proporcionada no es v\u00e1lida');
				return false;	
			}
		}
	});
	configuracionReaseguradorContratoFacultativoGrid.init();	

	configuracionReaseguradorContratoFacultativoGrid.load('/MidasWeb/contratofacultativo/contrato/mostrarGridConfiguracionReasegurador.do?idtcreaseguradorcorredor='+idtcreaseguradorcorredor+'&idTmContratoFacultativo='+idTmContratoFacultativo, null, 'json');
}

var filtroCotizacionesFacultativoGrid;
function mostrarFiltroCotizacionesFacultativoGrid(idToCotizacion){
	filtroCotizacionesFacultativoGrid = new dhtmlXGridObject('filtroCotizacionesFacultativoGridDiv');
	filtroCotizacionesFacultativoGrid.setHeader("<center>Cotizaci&oacute;n</center>");	
	filtroCotizacionesFacultativoGrid.setColumnIds("cotizacion");
	filtroCotizacionesFacultativoGrid.setInitWidths("190");
	filtroCotizacionesFacultativoGrid.setColAlign("center");
	filtroCotizacionesFacultativoGrid.setColSorting("str");
	filtroCotizacionesFacultativoGrid.setColTypes("ro");
	filtroCotizacionesFacultativoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	filtroCotizacionesFacultativoGrid.setSkin("light");		
	filtroCotizacionesFacultativoGrid.enableDragAndDrop(false);	
	filtroCotizacionesFacultativoGrid.enableLightMouseNavigation(false);
	
	filtroCotizacionesFacultativoGrid.init();	

	filtroCotizacionesFacultativoGrid.load('/MidasWeb/contratofacultativo/contrato/mostrarGridFiltroCotizacionesFacultativo.do?idToCotizacion='+idToCotizacion, null, 'json');
}