function mostrarSlipGeneral(idToSlip,slipGeneral,tipo){
  // se realizar estas condiciones para cargar los grid adicionales de cada slip
	if(tipo == -1){
		window.open('/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral , "configuracion_detalle" , "toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=780,height=600");
	}
	if(tipo == 9){//se despliega la informacion general de la cotizacion
		document.location.href = '/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 0){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipGeneral.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 1){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipAviacion.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 2){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipBarco.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 3){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipEquipoContratista.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 4){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipIncendio.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 5){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipObraCivil.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 6){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCConstructores.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 7){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
	if(tipo == 8){
		document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral;
	}
} 

function guardarSlip(form){
	sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlip.do', null,'procesarRespuestaXml(transport.responseXML,null); mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipBarco(form){
	sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipBarco.do', null,'procesarRespuestaXml(transport.responseXML,null); mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipAviacion(form){
	sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipAviacion.do', null,'procesarRespuestaXml(transport.responseXML,null);  mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipFuncionarios(form){
	sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipRCFuncionarios.do', null,'procesarRespuestaXml(transport.responseXML,null); mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipIncendio(form){
	 sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipIncendio.do', null,'procesarRespuestaXml(transport.responseXML,null);  mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipObraCivil(form){
	 sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipObraCivil.do', null,'procesarRespuestaXml(transport.responseXML,null);  mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipContructores(form){
	 sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipRCConstructores.do', null,'procesarRespuestaXml(transport.responseXML,null);  mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function guardarSlipEquipoContratista(form){
	 sendRequest(form,'/MidasWeb/contratofacultativo/slip/guardarSlipEquipoContratista.do', null,'procesarRespuestaXml(transport.responseXML,null);  mostrarSlipGeneral('+form.idToSlip.value+',0,9);');
}

function mostrarEditarSlipGrid(idToSlip, slipGeneral){
	var editarSlipGrid = new dhtmlXGridObject('editarSlipIncisoGrid');									  
	editarSlipGrid.setHeader("Numero Inciso, Descripcion, Detalle");
	editarSlipGrid.setColumnIds("numeroInciso,descripcionInciso,detalle");
	editarSlipGrid.setInitWidths("150, 350, 100");
	editarSlipGrid.setColAlign("left,left,left");
	editarSlipGrid.setColSorting("str,str,str");
	editarSlipGrid.setColTypes("ro,ro,img");
	editarSlipGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	editarSlipGrid.setSkin("light");
	editarSlipGrid.enableLightMouseNavigation(false);
	editarSlipGrid.init();		
	editarSlipGrid.load('/MidasWeb/contratofacultativo/slip/editarSlipIncisoGrid.do?idToSlip='+idToSlip+'&slipGeneral='+slipGeneral, null, 'json');	
}
