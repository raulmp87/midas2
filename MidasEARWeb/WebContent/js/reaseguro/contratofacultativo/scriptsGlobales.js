function recargarArbolRealizarCotizacionFac(){
	document.getElementById('loadingIndicatorComps').innerHTML = '';
	tree.destructor();
	cargandoTree('configuracionFacultativa0',treeboxbox_tree);
	document.getElementById('configuracion_detalle').innerHTML = '';
}

 

function desplegarVeredictoAutorizacion(xmlDoc, afterClose) {
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;
	
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	var veredicto = item.getElementsByTagName("veredicto")[0].firstChild.nodeValue;
	if (veredicto == 1)
		procesarRespuestaXml(xmlDoc, afterClose);
	else{
		var html = "<div></div>";
			html = html  + "<div class=\"mensaje_contenido\" style='height:100%;width:100%;border:none;'>";
			html = html  + "<div id=\"mensajeImg\" style='width:500px;align:right;margin-bottom:15px;'>";
			html = html  + "<img src='/MidasWeb/img/infoIcon.png' align=\"right\" /> <br/><br/>";
			html = html  + "</div>";
			html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\" style='height:100%;width:100%;'>";
			html = html  + mensaje;
			html = html  + "<div style=\"margin-right:33px\" class=\"b_aceptar\"><a href=\"javascript:void(0);\" onclick=\" cerrarVentanaDesplegarVeredictoAutorizacion();\">Aceptar</a></div>";
			html = html  + "</div>";
			html = html  + "</div>";
			
			if (parent.dhxWins==null){
				parent.dhxWins = new dhtmlXWindows();
			}
			
			if (parent.dhxWinsMsg==null){
				parent.dhxWinsMsg = new parent.dhtmlXWindows();
			}
			
			parent.dhxWinsMsg.setImagePath("/MidasWeb/img/dhxwindow/");
			if (mensajeAUsuario==null || parent.dhxWinsMsg.window("mensajeAUsuario").isHidden()){
				mensajeAUsuario = parent.dhxWinsMsg.createWindow("mensajeAUsuario", 500, 340, 550, 350);
				mensajeAUsuario.center();
				mensajeAUsuario.setText("Resultado de la Autorizaci&oacute;n");
				
				for (var a in parent.dhxWins.wins) {
					if (parent.dhxWins.wins[a].isModal()) {
						ventanaInvocadora = parent.dhxWins.wins[a];
						ventanaInvocadora.setModal(false);
						zIndexInvocadora = ventanaInvocadora.style.zIndex;
						ventanaInvocadora.style.zIndex = 0;
					}
				}
				
				mensajeAUsuario.setModal(true);
				mensajeAUsuario.attachHTMLString(html);
				mensajeAUsuario.attachEvent("onClose", function(win){
					
					if(ventanaInvocadora != null) {
						ventanaInvocadora.setModal(true);
						ventanaInvocadora.style.zIndex = zIndexInvocadora;
					}
					
					if (afterClose != null) {
						eval(afterClose);
					}
					parent.dhxWinsMsg.window("mensajeAUsuario").setModal(false);
					parent.dhxWinsMsg.window("mensajeAUsuario").hide();
					mensajeAUsuario =null;
					
				});
			}
			mensajeAUsuario.button("park").hide();
	}
	
}

function cerrarVentanaDesplegarVeredictoAutorizacion() {
	parent.dhxWinsMsg.window("mensajeAUsuario").setModal(false);
	parent.dhxWinsMsg.window("mensajeAUsuario").hide();
	parent.dhxWinsMsg.window("mensajeAUsuario").close();
}