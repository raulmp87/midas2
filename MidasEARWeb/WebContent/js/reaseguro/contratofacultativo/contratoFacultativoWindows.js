var ventanaModificarPorcentajes;

function mostrarModificarPorcentajesContratos(idToSlip){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaModificarPorcentajes = dhxWins.createWindow("modificarPorcentajes", 400, 320, 610, 235);
	ventanaModificarPorcentajes.setText("Modificar Porcentajes");
	ventanaModificarPorcentajes.setModal(true);
	ventanaModificarPorcentajes.center();
	ventanaModificarPorcentajes.attachURL("/MidasWeb/contratofacultativo/mostrarModificarPorcentajesContratos.do?idToSlip=" + idToSlip);
}

function cerrarVentanaModificarPorcentajes(actualizar){
	parent.dhxWins.window("modificarPorcentajes").setModal(false);
	parent.dhxWins.window("modificarPorcentajes").hide();
	parent.ventanaModificarPorcentajes=null;
	if (parseInt(actualizar) == 1){
		var id = $('idToSlip').value;
		parent.sendRequestFromAccordion(null,'/MidasWeb/contratofacultativo/slip/mostrarDetalle.do?id='+id, 'configuracion_detalle', null);
	}
}