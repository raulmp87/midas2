var polizaAdministrarReporteGrid;


function mostrarPolizaAdministrarReporteGrid(nomAsegurado, numPoliza){
	
	if (validarFormatoNumeroPolizaRptsReaseguro(numPoliza)){
	  	var polizaAdministrarReporteGridPath='/MidasWeb/contratos/movimiento/mostrarPolizas.do?nomAsegurado='+nomAsegurado+'&reporte=1&numPoliza='+numPoliza;
	 	polizaAdministrarReporteGrid = new dhtmlXGridObject('polizaReporteMovtosPorReaseguradorGrid');
	 	polizaAdministrarReporteGrid.setHeader(",Nombre del Asegurado,N&uacute;mero de P&oacute;liza,Fecha de emisi&oacute;n,Fecha de Vigencia,,");	
	 	polizaAdministrarReporteGrid.setColumnIds("seleccionado,nomAsegurado,numeroPoliza,fechaEmision,fechaVigencia,pdf,xls");
	 	polizaAdministrarReporteGrid.setInitWidths("30,320,150,130,130,30,30");
	 	polizaAdministrarReporteGrid.setColAlign("center,center,center,center,center,center,center");
	 	polizaAdministrarReporteGrid.setColSorting("str,str,str,str,str,str,str");
	 	polizaAdministrarReporteGrid.setColTypes("ra,ro,ro,ro,ro,img,img");
	 	polizaAdministrarReporteGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
	    });
	  	polizaAdministrarReporteGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	 	polizaAdministrarReporteGrid.setSkin("light");		
	 	polizaAdministrarReporteGrid.enableDragAndDrop(false);	
	 	polizaAdministrarReporteGrid.enableLightMouseNavigation(false);
	 	polizaAdministrarReporteGrid.init();	
	 	polizaAdministrarReporteGrid.load(polizaAdministrarReporteGridPath, null, 'json');
	}
}



function mostrarPolizaAdministrarReporteGridContrato(nomAsegurado, numPoliza){
	if (validarFormatoNumeroPolizaRptsReaseguro(numPoliza)){
	  	var polizaAdministrarReporteGridPath='/MidasWeb/contratos/movimiento/mostrarPolizas.do?nomAsegurado='+nomAsegurado+'&reporte=2&numPoliza='+numPoliza;
	 	polizaAdministrarReporteGrid = new dhtmlXGridObject('polizaReporteMovtosPorReaseguradorGrid');
	 	polizaAdministrarReporteGrid.setHeader(",Nombre del Asegurado,N&uacute;mero de P&oacute;liza,Fecha de emisi&oacute;n,Fecha de Vigencia,,");	
	 	polizaAdministrarReporteGrid.setColumnIds("seleccionado,nomAsegurado,numeroPoliza,fechaEmision,fechaVigencia,pdf,xls");
	 	polizaAdministrarReporteGrid.setInitWidths("0,320,150,130,130,30,30");
	 	polizaAdministrarReporteGrid.setColAlign("center,center,center,center,center,center,center");
	 	polizaAdministrarReporteGrid.setColSorting("str,str,str,str,str,str,str");
	 	polizaAdministrarReporteGrid.setColTypes("ra,ro,ro,ro,ro,img,img");
	 	polizaAdministrarReporteGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
	    });
	  	polizaAdministrarReporteGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	 	polizaAdministrarReporteGrid.setSkin("light");		
	 	polizaAdministrarReporteGrid.enableDragAndDrop(false);	
	 	polizaAdministrarReporteGrid.enableLightMouseNavigation(false);
	 	polizaAdministrarReporteGrid.init();	
	 	polizaAdministrarReporteGrid.load(polizaAdministrarReporteGridPath, null, 'json');
	}
}