
/**
 * Funci�n utilizada para solicitar la impresi�n de un estado de cuenta facultativo seg�n los diferentes criterios de filtrado.
 */
function imprimirEstadoCuentaFacultativo(){
	var clavePolizaSubRamo = obtieneValorComponente('clavePolizaSubramo');
	var separarPorReasegurador = obtieneValorComponente('separarPorReasegurador');
	var hasta = obtieneValorComponente('hasta');
	var idPoliza = obtieneValorComponente('idPoliza');
	var idToPoliza = obtieneValorComponente('idToPoliza');
	var idSiniestroSubRamo = obtieneValorComponente('claveSiniestroSubRamo');
	var idSiniestro = obtieneValorComponente('idReporteSiniestro');
	var idReasegurador = obtieneValorComponente('idReasegurador');
	var idMoneda = obtieneValorComponente('idMoneda');
	var idSubRamo = obtieneValorComponente('idSubRamo');
	var tipoConsulta = obtieneValorComponente('tipoConsulta');
	var idEstadoCuenta = obtieneValorComponente('idEstadoCuenta');
	var suscripcion = obtieneValorComponente('suscripcion');
	var fechaInicial = obtieneValorComponente('fechaInicial');
	var fechaFinal = obtieneValorComponente('fechaFinal');

	var url = '/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do?idEstadoCuenta='+idEstadoCuenta+
			'&suscripcion='+suscripcion+'&fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+
			'&idPolizaSubRamo='+clavePolizaSubRamo+'&separarPorReasegurador='+separarPorReasegurador+
			'&hasta='+hasta+'&idPoliza='+idPoliza+'&idSiniestroSubRamo='+idSiniestroSubRamo+
			'&idSiniestro='+idSiniestro+'&idReporteSiniestro='+idSiniestro+'&idReasegurador='+idReasegurador+'&idMoneda='+idMoneda+
			'&idSubRamo='+idSubRamo+'&tipoConsulta='+tipoConsulta+'&idToPoliza='+idToPoliza;
	
	window.open(url ,'PDF');
}

/**
 * Funci�n utilizada para solicitar la impresi�n del reporte de soporte del estado de cuenta facultativo seg�n los diferentes criterios de filtrado.
 */
function imprimirReporteSoporteEstadoCuentaFacultativo(tipoConsultaOpcional){
	var clavePolizaSubRamo = obtieneValorComponente('clavePolizaSubramo');
	var separarPorReasegurador = obtieneValorComponente('separarPorReasegurador');
	var hasta = obtieneValorComponente('hasta');
	var idPoliza = obtieneValorComponente('idPoliza');
	var idToPoliza = obtieneValorComponente('idToPoliza');
	var idSiniestroSubRamo = obtieneValorComponente('claveSiniestroSubRamo');
	var idSiniestro = obtieneValorComponente('idReporteSiniestro');
	var idReasegurador = obtieneValorComponente('idReasegurador');
	var idMoneda = obtieneValorComponente('idMoneda');
	var idSubRamo = obtieneValorComponente('idSubRamo');
	var tipoConsulta = obtieneValorComponente('tipoConsulta');
	var idEstadoCuenta = obtieneValorComponente('idEstadoCuenta');
	var suscripcion = obtieneValorComponente('suscripcion');
	var fechaInicial = obtieneValorComponente('fechaInicial');
	var fechaFinal = obtieneValorComponente('fechaFinal');

	if(tipoConsultaOpcional != null && tipoConsultaOpcional != '')
		tipoConsulta = tipoConsultaOpcional;
	
	var url = '/MidasWeb/reaseguro/estadoCuentaFacultativo/obtenerReporteMovimientosEstadoCuentaCxP.do?idEstadoCuenta='+idEstadoCuenta+
			'&suscripcion='+suscripcion+'&fechaInicial='+fechaInicial+'&fechaFinal='+fechaFinal+
			'&idPolizaSubRamo='+clavePolizaSubRamo+'&separarPorReasegurador='+separarPorReasegurador+
			'&hasta='+hasta+'&idPoliza='+idPoliza+'&idSiniestroSubRamo='+idSiniestroSubRamo+
			'&idSiniestro='+idSiniestro+'&idReporteSiniestro='+idSiniestro+'&idReasegurador='+idReasegurador+'&idMoneda='+idMoneda+
			'&idSubRamo='+idSubRamo+'&tipoConsulta='+tipoConsulta+'&idToPoliza='+idToPoliza;
	
	window.open(url ,'XLS');
}

function obtieneValorComponente(objId){
	var valor = '';
	var object = document.getElementById(objId);
	if(object != null)
		valor=object.value;
	return valor;
}

function abrirReporteEstadoCuentaFacultativo(form){ 
	window.open('/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do?idEstadoCuenta='+$('idEstadoCuenta').value +'&suscripcion='+$('suscripcion').value+'&fechaInicial='+$('fechaInicial').value+'&fechaFinal='+$('fechaFinal').value ,'PDF');
}

function mostrarRptEstadoCuentaMasivo(){
	var idsEstadosCuenta = '';
	var numSuscripcion = '';
	if (estadoCuentaTipoReaseguroGrid.getRowsNum() > 0){
		for (var i=0;i<estadoCuentaTipoReaseguroGrid.getRowsNum();i=i+1){	     	     	     
			id = estadoCuentaTipoReaseguroGrid.getRowId(i);
//			numSuscripcion = estadoCuentaTipoReaseguroGrid.cellByIndex(i,3);
			idsEstadosCuenta = idsEstadosCuenta + id /*+ "_" + numSuscripcion.getValue()*/;
		    if(i !== estadoCuentaTipoReaseguroGrid.getRowsNum()-1)
		    	idsEstadosCuenta = idsEstadosCuenta + ",";
		}
//		var mostrarEjerciciosAnteriores = document.getElementById('checkEjerciciosAnteriores').checked;
		cargarEnNuevaVentana('/MidasWeb/reaseguro/reporte/rptEstadoCuentaMasivo.do?idsEstadosCuenta='+idsEstadosCuenta+"&mostrarEjerciciosAnteriores=true",'Impresi\u00f3n Masiva de Estados de Cuenta');
	}else{
		alert('No hay estados de cuenta listados');
	}
}

function mostrarRptEstadoCuentaFacultativoMasivo(){
	var idsEstadosCuenta = '';
	var numSuscripcion = '';
	if (detallePolizaEstadoCuentaGrid.getRowsNum() > 0){
		for (var i=0;i<detallePolizaEstadoCuentaGrid.getRowsNum();i=i+1){	     	     	     
			id = detallePolizaEstadoCuentaGrid.getRowId(i);
			numSuscripcion = detallePolizaEstadoCuentaGrid.cellByIndex(i,7);
			idsEstadosCuenta = idsEstadosCuenta + id + "_" + numSuscripcion.getValue();
		    if(i !== detallePolizaEstadoCuentaGrid.getRowsNum()-1)
		    	idsEstadosCuenta = idsEstadosCuenta + ",";
		}
			cargarEnNuevaVentana('/MidasWeb/reaseguro/reporte/rptEstadoCuentaFacultativoMasivo.do?idsEstadosCuenta='+idsEstadosCuenta,'Impresi\u00f3n Masiva de Estados de Cuenta');
	}else{
		alert('No hay estados de cuenta listados');
	}
}
