/*********************************************************
 *************************************** Estados de cuenta
 ********************************************************/

function inicializaComponentesEstadoCuentaTipoReaseguro(){	
	mostrarAcumuladorEstadoCuentaGrid();
}

function inicializaComponentesEstadoCuentaContratoFacultativo(){	
	mostrarAcumuladorEstadoCuentaGrid();
	mostrarPagosEstadoCuentaGrid();
}

var acumuladorEstadoCuentaGrid;
function mostrarAcumuladorEstadoCuentaGrid(){
	var mostrarEjerciciosAnteriores = '';
	if (document.getElementById('checkEjerciciosAnteriores') != null && document.getElementById('checkEjerciciosAnteriores') != undefined)
		mostrarEjerciciosAnteriores = document.getElementById('checkEjerciciosAnteriores').checked;
	var acumuladorEstadoCuentaGridPath='/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do?idEstadoCuenta=' + $('idEstadoCuenta').value+'&mostrarEjerciciosAnteriores='+mostrarEjerciciosAnteriores;
	inicializaGridAcumuladorEstadoCuenta('automatico');	
	
	acumuladorEstadoCuentaGrid.load(acumuladorEstadoCuentaGridPath, null, 'json');
}

function inicializaGridAcumuladorEstadoCuenta(tipo){
	acumuladorEstadoCuentaGrid = new dhtmlXGridObject('detalleAcumuladorEstadoCuentaGrid');
	if(tipo == 'automatico'){
		var periodo = 'Per\u00edodo Anterior';
		if ($('formaPago') != null)
			periodo = 'Per\u00edodo ' + $('formaPago').value + ' Anterior';
		acumuladorEstadoCuentaGrid.setHeader(periodo+",<div align='center'>Conceptos</div>,<div align='center'>Debe</div>,<div align='center'>Haber</div>,<div align='center'>Saldo</div>");
		acumuladorEstadoCuentaGrid.setColumnIds("periodo,concepto,debe,haber,saldo");
		acumuladorEstadoCuentaGrid.setInitWidths("200,300,120,120,120");
		acumuladorEstadoCuentaGrid.setColAlign("center,left,center,center,center");
		acumuladorEstadoCuentaGrid.setColTypes("ro,ro,ro,ro,ro");
	}
	else if (tipo == 'facultativo'){
		acumuladorEstadoCuentaGrid.setHeader(",<div align='center'>Conceptos</div>,<div align='center'>Debe</div>,<div align='center'>Haber</div>,<div align='center'>Saldo</div>");
		acumuladorEstadoCuentaGrid.setColumnIds("periodo,concepto,debe,haber,saldo");
		acumuladorEstadoCuentaGrid.setInitWidths("0,*,120,120,120");
		acumuladorEstadoCuentaGrid.setColAlign("center,left,center,center,center");
		acumuladorEstadoCuentaGrid.setColTypes("ro,ro,ro,ro,ro");
	}

	acumuladorEstadoCuentaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	acumuladorEstadoCuentaGrid.setSkin("light");
	acumuladorEstadoCuentaGrid.enableDragAndDrop(false);
	acumuladorEstadoCuentaGrid.enableLightMouseNavigation(false);
	acumuladorEstadoCuentaGrid.attachEvent("onXLS", function(grid){
		totalRequests = totalRequests + 1;
		showIndicator();
    });
	acumuladorEstadoCuentaGrid.attachEvent("onXLE", function(grid){
		totalRequests = totalRequests - 1;
		hideIndicator();
    });
	acumuladorEstadoCuentaGrid.init();
}

function abrirReporteEstadoCuenta(idToEstadoCuenta,mostrarEjerciciosAnteriores,tipoReporte){ 
	if(idToEstadoCuenta != null && idToEstadoCuenta != undefined){
		if(mostrarEjerciciosAnteriores == null || mostrarEjerciciosAnteriores==undefined)
			mostrarEjerciciosAnteriores=false;
		if(tipoReporte == null || tipoReporte == undefined)
			tipoReporte = 'PDF';
		var location = '/MidasWeb/reaseguro/reporte/estadoCuenta.do?idEstadoCuenta='+idToEstadoCuenta+'&mostrarEjerciciosAnteriores='+mostrarEjerciciosAnteriores+'&tipoReporte='+tipoReporte;
		window.open(location,tipoReporte);
	}
	else
		alert("Id Estado Cuenta no v�lido: "+idToEstadoCuenta);
}

var pagoEstadoCuentaGrid;
function mostrarPagosEstadoCuentaGrid(){
 	var pagoEstadoCuentaGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePagos.do?idEstadoCuenta=' + $('idEstadoCuenta').value;
	pagoEstadoCuentaGrid = new dhtmlXGridObject('detallePagosEstadoCuentaGrid');
	pagoEstadoCuentaGrid.setHeader("N&uacute;mero de Pago,Monto,Fecha de Pago,Estatus");	
	pagoEstadoCuentaGrid.setColumnIds("numeroPago,monto,fechaPago,estatus");
	pagoEstadoCuentaGrid.setInitWidths("150,150,150,150");
	pagoEstadoCuentaGrid.setColAlign("center,center,center,center");
	pagoEstadoCuentaGrid.setColSorting("int,int,str,str");
	pagoEstadoCuentaGrid.setColTypes("ro,ro,ro,ro");

	pagoEstadoCuentaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	pagoEstadoCuentaGrid.setSkin("light");		
	pagoEstadoCuentaGrid.enableDragAndDrop(false);	
	pagoEstadoCuentaGrid.enableLightMouseNavigation(false);
	//pagoEstadoCuentaGrid.attachEvent("onEditCell",null);
	pagoEstadoCuentaGrid.init();	
	
	pagoEstadoCuentaGrid.load(pagoEstadoCuentaGridPath, null, 'json');
}

/*
 * Estados de Cuenta por Tipo de Reaseguro
 * 
 */
var estadoCuentaTipoReaseguroGrid;
var estadoCuentaTipoReaseguroGridPath;
function mostrarEstadosCuentaTipoReaseguroGrid(){
	estadoCuentaTipoReaseguroGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarEstadosCuentaTipoReaseguroGrid.do';
	estadoCuentaTipoReaseguroGrid = new dhtmlXGridObject('estadoCuentaTipoReaseguroGrid');
	estadoCuentaTipoReaseguroGrid.setHeader(",Subramo,Tipo de Reaseguro,Suscripci&oacute;n,,Reasegurador,Corredor,Moneda,Saldo T&eacute;cnico");	
	estadoCuentaTipoReaseguroGrid.setColumnIds("seleccionado,subRamo,tipoDeReaseguro,suscripcion,numSuscripcion,reasegurador,corredor,moneda,saldoTecnico");
	estadoCuentaTipoReaseguroGrid.setInitWidths("30,200,150,170,0,150,150,100,100");
	estadoCuentaTipoReaseguroGrid.setColAlign("center,center,center,center,center,center,center,center,center");
	estadoCuentaTipoReaseguroGrid.setColSorting("int,str,str,str,str,str,str,str,str");
	estadoCuentaTipoReaseguroGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro");
	estadoCuentaTipoReaseguroGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingEstadosCuenta');
    });    
	estadoCuentaTipoReaseguroGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingEstadosCuenta');
    }); 
	estadoCuentaTipoReaseguroGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	estadoCuentaTipoReaseguroGrid.setSkin("light");		
	estadoCuentaTipoReaseguroGrid.enableDragAndDrop(false);	
	estadoCuentaTipoReaseguroGrid.enableLightMouseNavigation(false);
	estadoCuentaTipoReaseguroGrid.init();	
	
	estadoCuentaTipoReaseguroGrid.load(estadoCuentaTipoReaseguroGridPath, null, 'json');
}

/******************************************************************************
***************************************Estados de cuenta contratos facultativos
*******************************************************************************/

function inicializaComponentesListarEstadosCuentaContratoFacultativo(){	
	mostrarPolizaEstadoCuentaGrid('undefined', 'undefined',false);
	mostrarDetallePolizaEstadoCuentaGrid();
}

var polizaEstadoCuentaGrid;
function mostrarPolizaEstadoCuentaGrid(nomAsegurado, numPoliza,mostrarMensajeSinRegistros){
	if (validarFormatoNumeroPolizaRptsReaseguro(numPoliza)){
		var polizaEstadoCuentaGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do?nomAsegurado='+nomAsegurado+'&numPoliza='+numPoliza;
		// Bloque opcional para consulta de p�lizas por reasegurador, aplica s�lo para el tab de "Por Reasegurador"
		var selectReasegurador = document.getElementById('idtcReaseguradorCorredor');
		var selectMoneda = document.getElementById('comboIdMoneda');
		if(selectReasegurador != null){
			polizaEstadoCuentaGridPath = polizaEstadoCuentaGridPath + '&idTcReasegurador='+selectReasegurador.value;
		}
		if(selectMoneda != null){
			polizaEstadoCuentaGridPath = polizaEstadoCuentaGridPath + '&idMoneda='+selectMoneda.value;
		}
		//Finaliza bloque de consulta por reasegurador y moneda
		
		polizaEstadoCuentaGrid = new dhtmlXGridObject('polizaEstadoCuentaGrid');
				
		polizaEstadoCuentaGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCargaGenerico('loadingPolizas');
	    });    
		polizaEstadoCuentaGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
			
			if(grid.getRowsNum() == 0){
				
				if(mostrarMensajeSinRegistros == undefined || mostrarMensajeSinRegistros == null ||
						mostrarMensajeSinRegistros == true){
					mostrarVentanaMensaje('20','No se encontraron registros.\n Introduzca criterios de b&uacute;squeda v&aacute;lidos.',null);
				}
			}
	    }); 	
		
		polizaEstadoCuentaGrid.load(polizaEstadoCuentaGridPath);
	}
}

var detallePolizaEstadoCuentaGrid;
function mostrarDetallePolizaEstadoCuentaGrid(numeroPoliza){
	var detallePolizaEstadoCuentaGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePoliza.do?numeroPoliza='+numeroPoliza;
	detallePolizaEstadoCuentaGrid = new dhtmlXGridObject('detallePolizaEstadoCuentaGrid');
	detallePolizaEstadoCuentaGrid.setHeader(",P&oacute;liza,Endoso,Nota de Cobertura,Subramo,Tipo de Reaseguro, Suscripci&oacute;n,,Forma de Pago"
			+",Reasegurador,Corredor,Moneda,Saldo T&eacute;cnico");	
	detallePolizaEstadoCuentaGrid.setColumnIds("seleccionado,poliza,endoso,notaCobertura,subRamo,tipoReaseguro,suscripcion,numSuscripcion,formaPago"
			+",reasegurador,corredor,moneda,saldoTecnico,estatus");
	detallePolizaEstadoCuentaGrid.setInitWidths("30,100,100,150,100,150,150"
			+",0,200,200,100,100,110");
	detallePolizaEstadoCuentaGrid.setColAlign("center,center,center,center,center,center,center,center"
			+",center,center,center,center,center");
	detallePolizaEstadoCuentaGrid.setColSorting("int,int,int,str,str,str,str"
			+",str,str,str,str,str,str");
	detallePolizaEstadoCuentaGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro"
			+",ro,ro,ro,ro,ro,ro");
	detallePolizaEstadoCuentaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingEstadosCuentaFacultativo');
    });    
	detallePolizaEstadoCuentaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingEstadosCuentaFacultativo');
    }); 
	detallePolizaEstadoCuentaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	detallePolizaEstadoCuentaGrid.setSkin("light");		
	detallePolizaEstadoCuentaGrid.enableDragAndDrop(false);	
	detallePolizaEstadoCuentaGrid.enableLightMouseNavigation(false);
	detallePolizaEstadoCuentaGrid.init();	
	detallePolizaEstadoCuentaGrid.load(detallePolizaEstadoCuentaGridPath, null, 'json');
}

//Funciones para mostrar estados de cuenta facultativos por p�liza
var calendarioHasta;

function manipulaCalendarioHasta(){	
	calendarioHasta = new dhtmlxCalendarObject('fechaHasta', true, {isMonthEditable: true, isYearEditable: true});
	calendarioHasta.setDateFormat("%m/%Y");
	
	calendarioHasta.attachEvent("onClick",function(fecha){
 		$('fechaHasta').value=calendarioHasta.getFormatedDate("%m/%Y",fecha);
	});
	
 	calendarioHasta.hide();
}

function mostrarCalendarioHasta(){
	if (calendarioHasta!=null){
		if (calendarioHasta.isVisible()){
			calendarioHasta.hide();
		}else{
			calendarioHasta.show();
		}
	}
	else{
		manipulaCalendarioHasta();
	}
}

function inicializaComponentesListarEstadosCuentaContratoFacultativoPorPoliza(){
	mostrarPolizaEstadoCuentaGrid('undefined', 'undefined',false);
	mostrarDetallePolizaEstadoCuentaPorPolizaGrid();
}

function buscarEdoCtaFacultativosPorPoliza(mensajePolizaNoSeleccionada){
	 var numeroPoliza = polizaEstadoCuentaGrid.getCheckedRows(0);
 	 if (numeroPoliza == ''){
		 mostrarVentanaMensaje('10',mensajePolizaNoSeleccionada,null);
	 }else{
		 var poliza=numeroPoliza.split(",");
		 mostrarDetallePolizaEstadoCuentaPorPolizaGrid(poliza[0]);
	 }
}

function mostrarDetalleEstadoCuentaFacultativoPorPolizaSubRamo(mensajeEstadoCuentaNoSeleccionado){
	 var checkIds = detallePolizaEstadoCuentaGrid.getCheckedRows(0);
	 var hasta = document.getElementById('fechaHasta').value;
 	 if (checkIds == '')
 		 mostrarVentanaMensaje('10',mensajeEstadoCuentaNoSeleccionado,null);
 	 else if (hasta == ''){
 		 mostrarVentanaMensaje('10','Seleccione una fecha de corte',null);
 	 }
	 else{
		 var url = '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorPoliza/mostrarDetalle.do?idPolizaSubRamo='+checkIds;
		 url = url + '&fechaHasta='+document.getElementById('fechaHasta').value;
		 url = url + '&separarPorReasegurador='+document.getElementById('checkSepararConceptosPorAcumulador').checked;
		 sendRequest(document.estadoCuentaContratoFacultativoForm,url, 'contenido', 'inicializaComponentesEdoCtaFacultativoPorPoliza()');
	}
}

function mostrarDetalleEstadoCuentaFacultativoPorPoliza(mensajeNoSeleccionado){
	 var checkIds = polizaEstadoCuentaGrid.getCheckedRows(0);
	 var hasta = document.getElementById('fechaHasta').value;
 	 if (checkIds == '')
 		 mostrarVentanaMensaje('10',mensajeNoSeleccionado,null);
 	 else if (hasta == ''){
 		 mostrarVentanaMensaje('10','Seleccione una fecha de corte',null);
 	 }
	 else{
		 var url = '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorPoliza/mostrarDetalle.do?idPoliza='+checkIds;
		 url = url + '&fechaHasta='+document.getElementById('fechaHasta').value;
		 url = url + '&separarPorReasegurador='+document.getElementById('checkSepararConceptosPorAcumulador').checked;
		 sendRequest(document.estadoCuentaContratoFacultativoForm,url, 'contenido', 'inicializaComponentesEdoCtaFacultativoPorPoliza()');
	}
}

function inicializaComponentesEdoCtaFacultativoPorPoliza(){	
	mostrarAcumuladorEdoCtaFacultativoPorPolizaGrid();
}

var detallePolizaEstadoCuentaGrid;
function mostrarDetallePolizaEstadoCuentaPorPolizaGrid(numeroPoliza){
	var detallePolizaEstadoCuentaGridPath='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarEstadosCuentaPorPoliza.do?idToPoliza='+numeroPoliza;
	detallePolizaEstadoCuentaGrid = new dhtmlXGridObject('detalleEstadoCuentaFacGrid');
	detallePolizaEstadoCuentaGrid.setHeader(",Subramo,Distribuci&oacute;n,Inciso,Subinciso, Saldo T&eacute;cnico");	
	detallePolizaEstadoCuentaGrid.setColumnIds("seleccionado,subRamo,distribucion,inciso,subinciso,saldoTecnico");
	detallePolizaEstadoCuentaGrid.setInitWidths("30,*,100,70,70,150");
	detallePolizaEstadoCuentaGrid.setColAlign("center,left,center,center,center,left");
	detallePolizaEstadoCuentaGrid.setColSorting("int,str,str,int,int,str");
	detallePolizaEstadoCuentaGrid.setColTypes("ch,ro,ro,ro,ro,ro");
	detallePolizaEstadoCuentaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingEstadosCuentaFacultativo');
    });
	detallePolizaEstadoCuentaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingEstadosCuentaFacultativo');
    });
	detallePolizaEstadoCuentaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	detallePolizaEstadoCuentaGrid.setSkin("light");
	detallePolizaEstadoCuentaGrid.enableLightMouseNavigation(true);
	detallePolizaEstadoCuentaGrid.init();
	detallePolizaEstadoCuentaGrid.load(detallePolizaEstadoCuentaGridPath, null, 'json');
}

function mostrarAcumuladorEdoCtaFacultativoPorPolizaGrid(){
	var clavePolizaSubRamo = document.getElementById('clavePolizaSubramo');
	var separarConceptosPorAcumulador = document.getElementById('separarPorReasegurador');
	var hasta = document.getElementById('hasta');
	var idPoliza = document.getElementById('idPoliza');
	var idPolizaSubRamoContrato = '',separar = '',h='',pol='';
	if(clavePolizaSubRamo != null)
		idPolizaSubRamoContrato = clavePolizaSubRamo.value;
	if(separarConceptosPorAcumulador != null)
		separar = separarConceptosPorAcumulador.value;
	if(hasta!= null)
		h = hasta.value;
	if(idPoliza != null)
		pol = idPoliza.value;
	var url ='/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do?idPolizaSubRamo=' + idPolizaSubRamoContrato;
	url = url + '&idPoliza='+pol+'&hasta='+h+'&separarPorReasegurador='+separar;
	inicializaGridAcumuladorEstadoCuenta('facultativo');
	acumuladorEstadoCuentaGrid.load(url, null, 'json');
}
//Fin de Funciones para mostrar estados de cuenta facultativos por p�liza

//Funciones para mostrar estados de cuenta facultativos por siniestro

function inicializaComponentesEdoCtaFacultativoPorSiniestro(){
	creaCalendars('busquedaFechaSiniestro','S');
	inicializaEstadoCuentaPorSiniestroGrid();
	inicializaGridSubRamosPorSiniestro('siniestroEstadoCuenta');
}

var siniestroEstadoCuentaGrid;
function inicializaEstadoCuentaPorSiniestroGrid(){
	siniestroEstadoCuentaGrid = new dhtmlXGridObject('siniestroEstadoCuentaGrid');
	siniestroEstadoCuentaGrid.setHeader(",No. Reporte,Fecha,No. P&oacute;liza,Nombre/Raz&oacute;n social,Fecha emisi&oacute;n");	
	siniestroEstadoCuentaGrid.setColumnIds("seleccionado,numeroSiniestro,fechaSiniestro,numeroPoliza,nomAsegurado,fechaEmision");
	siniestroEstadoCuentaGrid.setInitWidths("30,110,80,110,*,100");
	siniestroEstadoCuentaGrid.setColAlign("center,left,left,left,left,left");
	siniestroEstadoCuentaGrid.setColSorting("int,str,str,str,str,str");
	siniestroEstadoCuentaGrid.setColTypes("ra,ro,ro,ro,ro,ro");
	siniestroEstadoCuentaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingSiniestros');
    });
	siniestroEstadoCuentaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingSiniestros');
    }); 
	siniestroEstadoCuentaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	siniestroEstadoCuentaGrid.setSkin("light");		
	siniestroEstadoCuentaGrid.enableLightMouseNavigation(true);
	siniestroEstadoCuentaGrid.init();
}

var subRamosPorSiniestroGrid;
function inicializaGridSubRamosPorSiniestro(divId){
	var divObject = document.getElementById(divId);
	if(divObject != null && divObject != undefined){
		divObject.innerHTML = '';
		subRamosPorSiniestroGrid = new dhtmlXGridObject(divId);
		subRamosPorSiniestroGrid.setHeader(",SubRamo Afectado");
		subRamosPorSiniestroGrid.setColumnIds("seleccionado,subramo");
		subRamosPorSiniestroGrid.setInitWidths("30,*");
		subRamosPorSiniestroGrid.setColAlign("center,left");
		subRamosPorSiniestroGrid.setColTypes("ch,ed");
		subRamosPorSiniestroGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		subRamosPorSiniestroGrid.setSkin("light");
		subRamosPorSiniestroGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCargaGenerico('loadingEstadosCuentaFacultativoSiniestro');
	    });
		subRamosPorSiniestroGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingEstadosCuentaFacultativoSiniestro');
	    });
		subRamosPorSiniestroGrid.init();
	}
}

function mostrarGridSiniestro(){
	var asegurado = trim(document.getElementById('busquedaNomAsegurado').value);
	var numeroPoliza = trim(document.getElementById('busquedaNumPoliza').value);
	var numeroSiniestro = trim(document.getElementById('busquedaNumSiniestro').value);
	var fechaSiniestro = trim(document.getElementById('busquedaFechaSiniestro').value);
	
	var consultaPorReaseguradorValida = false;
	
	// Bloque opcional para consulta de p�lizas por reasegurador, aplica s�lo para el tab de "Por Reasegurador"
	var selectReasegurador = document.getElementById('idtcReaseguradorCorredor');
	var selectMoneda = document.getElementById('comboIdMoneda');
	var urlReasegurador = '';
	if(selectReasegurador != null){
		urlReasegurador = urlReasegurador + '&idTcReasegurador='+selectReasegurador.value;
	}
	if(selectMoneda != null){
		urlReasegurador= urlReasegurador+ '&idMoneda='+selectMoneda.value;
	}
		//Finaliza bloque de consulta por reasegurador y moneda
	
	if(asegurado == '' && numeroPoliza == '' && numeroSiniestro == '' && fechaSiniestro == '' && urlReasegurador == ''){
		mostrarVentanaMensaje('10','Debe introducir al menos un criterio de filtrado.',null);
	}
	else if(validarFormatoNumeroPolizaRptsReaseguro(numeroPoliza)){
		inicializaEstadoCuentaPorSiniestroGrid();
		var url='/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarEstadoCuentaPorSiniestro.do?' +
			'nomAsegurado='+asegurado+'&numPoliza='+numeroPoliza+'&numSiniestro='+numeroSiniestro+'&fechaSiniestro='+fechaSiniestro+urlReasegurador;
		siniestroEstadoCuentaGrid.load(url, '', 'json');
	}
}

function mostrarSubRamosAfectadosGrid(mensajeSiniestroNoSeleccionado){
	 var checkIds = siniestroEstadoCuentaGrid.getCheckedRows(0);
 	 if (checkIds == '')
		 mostrarVentanaMensaje('10',mensajeSiniestroNoSeleccionado,null);
	 else{
		 var url = "/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarSubRamosAfectadosPorSiniestro.do?idReporteSiniestro="+checkIds;
		 inicializaGridSubRamosPorSiniestro('siniestroEstadoCuenta');
		 subRamosPorSiniestroGrid.load(url,null,'json');
	 }
}

function mostrarDetalleEstadoCuentaFacultativoPorSiniestroSubRamo(mensajeEstadoCuentaNoSeleccionado){
	 var checkIds = subRamosPorSiniestroGrid.getCheckedRows(0);
	 var hasta = document.getElementById('fechaHasta').value;
 	 if (checkIds == '')
 		 mostrarVentanaMensaje('10',mensajeEstadoCuentaNoSeleccionado,null);
 	 else if(hasta == ''){
 		 mostrarVentanaMensaje('10','Seleccione una fecha de corte.',null);
 	 }
	 else{
		 var url = '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorSiniestro/mostrarDetalle.do?idSiniestroSubRamo='+checkIds;
		 url = url + '&fechaHasta='+document.getElementById('fechaHasta').value;
		 url = url + '&separarPorReasegurador='+document.getElementById('checkSepararConceptosPorAcumulador').checked;
		 sendRequest(document.estadoCuentaContratoFacultativoForm,url, 'contenido', 'inicializaGridEdoCtaFacultativoPorSiniestro()');
	}
}

function mostrarDetalleEstadoCuentaFacultativoPorSiniestro(mensajeEstadoCuentaNoSeleccionado){
	var checkIds = siniestroEstadoCuentaGrid.getCheckedRows(0);
	var hasta = document.getElementById('fechaHasta').value;
 	if (checkIds == '')
		mostrarVentanaMensaje('10',mensajeEstadoCuentaNoSeleccionado,null);
 	else if(hasta == ''){
 		 mostrarVentanaMensaje('10','Seleccione una fecha de corte.',null);
 	 }
	else{
		var url = '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorSiniestro/mostrarDetalle.do?idReporteSiniestro='+checkIds;
		url = url + '&fechaHasta='+document.getElementById('fechaHasta').value;
		url = url + '&separarPorReasegurador='+document.getElementById('checkSepararConceptosPorAcumulador').checked;
		sendRequest(document.estadoCuentaContratoFacultativoForm,url, 'contenido', 'inicializaGridEdoCtaFacultativoPorSiniestro()');
	}
}

function inicializaGridEdoCtaFacultativoPorSiniestro(){	
	var claveSiniestroSubRamo = document.getElementById('claveSiniestroSubRamo');
	var separarConceptosPorAcumulador = document.getElementById('separarPorReasegurador');
	var hasta = document.getElementById('hasta');
	var idSiniestro = document.getElementById('idReporteSiniestro');
	var idSiniestroSubRamoContrato = '',separar = '',h='',sin='';
	if(claveSiniestroSubRamo != null)
		idSiniestroSubRamoContrato = claveSiniestroSubRamo.value;
	if(separarConceptosPorAcumulador != null)
		separar = separarConceptosPorAcumulador.value;
	if(hasta!= null)
		h = hasta.value;
	if(idSiniestro != null)
		sin = idSiniestro.value;
	var url ='/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do?idSiniestroSubRamo=' + idSiniestroSubRamoContrato;
	url = url + '&idReporteSiniestro='+sin+'&hasta='+h+'&separarPorReasegurador='+separar;
	inicializaGridAcumuladorEstadoCuenta('facultativo');
	acumuladorEstadoCuentaGrid.load(url, null, 'json');
}

//Fin de Funciones para mostrar estados de cuenta facultativos por siniestro

//Inicio de funciones para mostrar estados de cuenta facultativos por reasegurador
function inicializaComponentesEdoCtaFacultativoPorReasegurador(){
	creaCalendars('busquedaFechaSiniestro','S');
	inicializaEstadoCuentaPorSiniestroGrid();
	mostrarPolizaEstadoCuentaGrid(null, '',false);
}

function consultarEstadoCuentaPorReasegurador(tipoConsulta){
	if(tipoConsulta != null && tipoConsulta!=''){
		var idReasegurador,idMoneda,idSubRamo,hasta,idSiniestro,idPoliza;
		idReasegurador = document.getElementById('idtcReaseguradorCorredor').value;
		var url = '/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorReasegurador/mostrarDetalle.do?tipoConsulta='+tipoConsulta+'&idReasegurador='+idReasegurador;
		var afterLoadFunction = 'inicializaGridEdoCtaFacultativoPorReasegurador()';
		var divDestino = 'contenido';
		var form = document.estadoCuentaContratoFacultativoForm;
		idMoneda = document.getElementById('comboIdMoneda').value;
		idSubRamo = document.getElementById('idSubRamo').value;
		hasta = document.getElementById('fechaHasta').value;
		if(idReasegurador != ''){
			if(hasta != ''){
				if(tipoConsulta == 'saldoTecnico' || tipoConsulta == 'saldoTotalSiniestros' || tipoConsulta == 'saldoTotalPolizas'){
				if(idMoneda != ''){
					url= url+'&idMoneda='+idMoneda+'&idSubRamo='+idSubRamo;
					url= url+'&hasta='+hasta;
					sendRequest(form,url, divDestino, afterLoadFunction);
				}
				else{
					mostrarVentanaMensaje('10','Seleccione la moneda.',null);
				}
			}
			else if(tipoConsulta == 'saldoPorSiniestro'){
				var idSiniestro = siniestroEstadoCuentaGrid.getCheckedRows(0);
				if(idSiniestro != ''){
					url=url+'&idSubRamo='+idSubRamo+'&hasta='+hasta;
					url=url+'&idSiniestro='+idSiniestro;
					sendRequest(form,url, divDestino, afterLoadFunction);
				}
				else{
					mostrarVentanaMensaje('10','Debe seleccionar un siniestro.',null);
				}
			}
			else if(tipoConsulta == 'saldoPorPoliza'){
				var idPoliza = polizaEstadoCuentaGrid.getCheckedRows(0);
				if(idPoliza != ''){
					url=url+'&idSubRamo='+idSubRamo+'&hasta='+hasta;
					url=url+'&idPoliza='+idPoliza;
					sendRequest(form,url, divDestino, afterLoadFunction);
				}
				else{
					mostrarVentanaMensaje('10','Debe seleccionar una p&oacute;liza.',null);
				}
			}
			}
			else{
				mostrarVentanaMensaje('10','Seleccione una fecha de corte.',null);
			}
		}
		else{
			mostrarVentanaMensaje('10','Seleccione un reasegurador.',null);
		}
	}
}

/**
 * Esta funci�n deber� leer los par�metros de configuraci�n del estado de cuenta, instnciar el grid de conceptos
 * y llenarlo con en action habitual, que deber� responder la cadena json correspondiente.
 */
function inicializaGridEdoCtaFacultativoPorReasegurador(){
	var idReasegurador = document.getElementById('idReasegurador').value;
	var idMoneda = document.getElementById('idMoneda').value;
	var idSubRamo = document.getElementById('idSubRamo').value;
	var idSiniestro = document.getElementById('idReporteSiniestro').value;
	var hasta = document.getElementById('hasta').value;
	var tipoConsulta = document.getElementById('tipoConsulta').value;
	var idPoliza = document.getElementById('idPoliza').value;
	
	var url ='/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do?tipoConsulta=' + tipoConsulta;
	url = url + '&idPoliza='+idPoliza+'&hasta='+hasta+'&idSiniestro='+idSiniestro+'&idSubRamo='+idSubRamo;
	url = url+'&idMoneda='+idMoneda+'&idReasegurador='+idReasegurador;
	
	inicializaGridAcumuladorEstadoCuenta('facultativo');
	
	acumuladorEstadoCuentaGrid.load(url, null, 'json');
}
//Fin de funciones para mostrar estados de cuenta facultativos por reasegurador