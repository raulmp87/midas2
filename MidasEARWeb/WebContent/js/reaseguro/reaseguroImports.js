/* IMPORTS de los distintos JS de reaseguro  */
path = '/MidasWeb/js/reaseguro/';
function loadScript(url){
	 var script = document.createElement("script")
	 script.type = "text/javascript";
	 script.src = path + url;
	 document.getElementsByTagName("head")[0].appendChild(script);
}
/*Imports*/
/*Reaseguro->ContratosProporcionales->AdministracionLineasYContratos*/
loadScript('reaseguro.js');
loadScript('contratofacultativo/scriptsGlobales.js');
loadScript('linea/reaseguroLineaComponents.js');
loadScript('linea/reaseguroLineaDataGrid.js');
loadScript('linea/reaseguroLineaWindow.js');
loadScript('estadocuenta/reaseguroEstadoCuentaComponents.js');
loadScript('estadocuenta/reaseguroEstadoCuentaDataGrid.js');
loadScript('estadocuenta/reaseguroEstadoCuentaWindow.js');
loadScript('egresos/reaseguroEgresosDataGrid.js');
loadScript('egresos/reaseguroEgresosWindow.js');
loadScript('ingresos/reaseguroIngresosDataGrid.js');
loadScript('ingresos/reaseguroIngresosWindow.js');
loadScript('movimientos/reaseguroMovimientosDataGrid.js');
loadScript('contratofacultativo/slip/slip.js');
loadScript('contratofacultativo/contratoFacultativoDataGrids.js');
loadScript('contratofacultativo/contratoFacultativoWindows.js');
