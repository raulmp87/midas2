
var polizaAdministrarEgresosGrid;
function mostrarPolizaAdministrarEgresosGrid(nomAsegurado, numPoliza, tipoReaseguro){
	if (nomAsegurado || numPoliza ) {
		var polizaAdministrarEgresosGridPath='/MidasWeb/reaseguro/egresos/mostrarPolizas.do?nomAsegurado=' + nomAsegurado 
			+ '&numPoliza=' + numPoliza + '&tipoReaseguro=' + tipoReaseguro;
		polizaAdministrarEgresosGrid = new dhtmlXGridObject('polizaAdministrarEgresosGrid');
		polizaAdministrarEgresosGrid.setHeader(",Nom Asegurado,N&uacute;mero de P&oacute;liza,Fecha de emisi&oacute;n,Fecha de Vigencia");	
		polizaAdministrarEgresosGrid.setColumnIds("seleccionado,nomAsegurado,numeroPoliza,fechaEmision,fechaVigencia");
		polizaAdministrarEgresosGrid.setInitWidths("30,200,150,150,200");
		polizaAdministrarEgresosGrid.setColAlign("center,center,center,center,center");
		polizaAdministrarEgresosGrid.setColSorting("int,str,int,str,str");
		polizaAdministrarEgresosGrid.setColTypes("ra,ro,ro,ro,ro");
		polizaAdministrarEgresosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('loadingPolizas');
	    });    
		polizaAdministrarEgresosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		polizaAdministrarEgresosGrid.setSkin("light");		
		polizaAdministrarEgresosGrid.enableDragAndDrop(false);	
		polizaAdministrarEgresosGrid.enableLightMouseNavigation(false);
		polizaAdministrarEgresosGrid.init();	
		
		mostrarIndicadorCargaGenerico("loadingPolizas");
		polizaAdministrarEgresosGrid.load(polizaAdministrarEgresosGridPath, null, 'json');
	} else {
		var tipoMensaje = "20";
		var mensaje = "Necesita escribir una p\u00F3liza o un nombre de asegurado";
		mostrarVentanaMensaje(tipoMensaje, mensaje, null);				
	}
}

var detallePolizaAdministrarEgresosGrid;
function mostrarDetallePolizaAdministrarEgresosGrid(numeroPoliza){
	var detallePolizaAdministrarEgresosGridPath='/MidasWeb/reaseguro/egresos/mostrarEstadosCuentaAdministrarEgresos.do?numeroPoliza='+numeroPoliza;
	detallePolizaAdministrarEgresosGrid = new dhtmlXGridObject('detallePolizaAdministrarEgresosGrid');
	detallePolizaAdministrarEgresosGrid.setHeader(",P&oacute;liza,Endoso,Subramo,Tipo de Reaseguro,Forma de Pago,Suscripci&oacute;n/Vigencia"
			+",Reasegurador,Corredor,Moneda,Saldo T&eacute;cnico,");	
	detallePolizaAdministrarEgresosGrid.setColumnIds("seleccionado,poliza,endoso,subRamo,tipoReaseguro,formaPago,suscripcion"
			+",reasegurador,corredor,moneda,saldoTecnico,saldoTecnicoValor");
	detallePolizaAdministrarEgresosGrid.setInitWidths("30,100,100,150,150,150,150"
			+",200,200,100,100,0");
	detallePolizaAdministrarEgresosGrid.setColAlign("center,center,center,center,center,center,center"
			+",center,center,center,center,center");
	detallePolizaAdministrarEgresosGrid.setColSorting("int,int,int,str,str,str,str"
			+",str,str,str,str,str");
	detallePolizaAdministrarEgresosGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro"
			+",ro,ro,ro,ro,ro");
	detallePolizaAdministrarEgresosGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCargaGenerico('loadingEstadosCuenta');
    });    
	detallePolizaAdministrarEgresosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCargaGenerico('loadingEstadosCuenta');
    }); 
	detallePolizaAdministrarEgresosGrid.attachEvent("onCheck", calcularSaldoTotalEdosCtaEgresos); 
	
	detallePolizaAdministrarEgresosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	detallePolizaAdministrarEgresosGrid.setSkin("light");		
	detallePolizaAdministrarEgresosGrid.enableDragAndDrop(false);	
	detallePolizaAdministrarEgresosGrid.enableLightMouseNavigation(false);
	
	detallePolizaAdministrarEgresosGrid.setColumnHidden(11,true);

	detallePolizaAdministrarEgresosGrid.init();	
	
	/*
	if ($('tipoReaseguro') != null){
		if (parseInt($('tipoReaseguro').value) == 3){ //Si el combo tipoReaseguro est� en "Facultativo"
			detallePolizaAdministrarEgresosGrid.setColumnHidden(1,false);
			detallePolizaAdministrarEgresosGrid.setColumnHidden(2,false);
		}else{
			detallePolizaAdministrarEgresosGrid.setColumnHidden(1,true);
			detallePolizaAdministrarEgresosGrid.setColumnHidden(2,true);
		}
	}
	*/
	detallePolizaAdministrarEgresosGrid.load(detallePolizaAdministrarEgresosGridPath, onLoadEstadosCuentaEgresos, 'json');
}

function onLoadEstadosCuentaEgresos(){
	calcularSaldoTotalEdosCtaEgresos();
	uncheckSeleccionarTodos();
	
}

var listaEgresosReaseguro;
function mostraGridListaEgresos() {
	listaEgresosReaseguro = new dhtmlXGridObject('gridboxListaEgresos');
	listaEgresosReaseguro.setHeader(",Monto Egreso,Moneda,Referencia,Fecha,estatus");
	listaEgresosReaseguro.setInitWidths("30,110,80,100,100,130");
	listaEgresosReaseguro.setColumnIds("seleccionado,montoEgreso,moneda,referencia,fecha,estatus");
	listaEgresosReaseguro.setColAlign("center,center,center,center,center,center");
	listaEgresosReaseguro.setColSorting("int,str,str,str,str,str");
	listaEgresosReaseguro.setColTypes("ra,ro,ro,ro,ro,ro");
	listaEgresosReaseguro.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	listaEgresosReaseguro.setSkin("light"); 
	listaEgresosReaseguro.enableDragAndDrop(false);
	listaEgresosReaseguro.enableLightMouseNavigation(false);
	listaEgresosReaseguro.init();
	
	listaEgresosReaseguro.load('/MidasWeb/reaseguro/egresos/mostrarEgresosPendientes.do', null, 'json');
}

var listaEgresosReaseguroCancel;
function mostraGridListaEgresosCancel() {		
	listaEgresosReaseguroCancel = new dhtmlXGridObject('gridboxListaEgresosCancel');
	listaEgresosReaseguroCancel.setHeader(",Monto Egreso,Moneda,Referencia,Fecha,estatus");
	listaEgresosReaseguroCancel.setInitWidths("30,110,80,100,100,130");
	listaEgresosReaseguroCancel.setColumnIds("seleccionado,montoEgreso,moneda,referencia,fecha,estatus");
	listaEgresosReaseguroCancel.setColAlign("center,center,center,center,center,center");
	listaEgresosReaseguroCancel.setColSorting("int,str,str,str,str,str");
	listaEgresosReaseguroCancel.setColTypes("ra,ro,ro,ro,ro,ro");
	listaEgresosReaseguroCancel.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	listaEgresosReaseguroCancel.setSkin("light"); 
	listaEgresosReaseguroCancel.enableDragAndDrop(false);
	listaEgresosReaseguroCancel.enableLightMouseNavigation(false);
	listaEgresosReaseguroCancel.init();
	
	listaEgresosReaseguroCancel.load('/MidasWeb/reaseguro/egresos/mostrarEgreso.do', null, 'json');
}


var gridEstadosCuentaAutomaticos;
var gridEstadosCuentaFacultativos;
function mostrarEstadosCuentaEgresoGrid(idsEdosCta){	
	if($("mostrarGridEdosCuentaAutomaticos").value==="true")
		mostrarEstadosCuentaAutomaticosGrid(idsEdosCta);
	
	if($("mostrarGridEdosCuentaFacultativos").value==="true"){
		mostrarEstadosCuentaFacultativosGrid(idsEdosCta);	
		ocultarDivReaseguro("coberturasEdosCuentaFacultativos");
		ocultarDivReaseguro("exhibicionesCoberturasEdosCuentaFacultativos");
	}
}

function mostrarEstadosCuentaAutomaticosGrid(idsEdosCta){
	gridEstadosCuentaAutomaticos = new dhtmlXGridObject('gridboxEdosCuentaAutomaticos');
	gridEstadosCuentaAutomaticos.setHeader("Pagar,Tipo de Reaseguro,Periodo,Nombre del reasegurador/corredor,Moneda,Saldo T&eacute;cnico,");
	gridEstadosCuentaAutomaticos.setInitWidths("60,150,170,250,130,145,0");
	gridEstadosCuentaAutomaticos.setColumnIds("seleccionar,tipoReaseguro,periodo,nombreReaseguradorCorredor,moneda,saldoTecnico,saldoTecnicoValor");
	gridEstadosCuentaAutomaticos.setColAlign("center,center,center,center,center,center,center");
	gridEstadosCuentaAutomaticos.setColSorting("str,str,str,str,str,str,str");
	gridEstadosCuentaAutomaticos.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro");
	gridEstadosCuentaAutomaticos.setColumnHidden(6, true);
	gridEstadosCuentaAutomaticos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	gridEstadosCuentaAutomaticos.setSkin("light"); 
	gridEstadosCuentaAutomaticos.enableDragAndDrop(false);
	gridEstadosCuentaAutomaticos.enableLightMouseNavigation(false);	
	gridEstadosCuentaAutomaticos.enableAutoHeight(true);
	gridEstadosCuentaAutomaticos.enableAutoWidth(true);
	gridEstadosCuentaAutomaticos.attachEvent("onCheck", actualizarMontoEgreso);
	gridEstadosCuentaAutomaticos.init();	
	gridEstadosCuentaAutomaticos.load('/MidasWeb/reaseguro/egresos/listarEstadosCuentaAutomaticosEgreso.do?idsEdosCta='+idsEdosCta, onLoadEstadosCuentaAutomaticos, 'json');	
}

function onLoadEstadosCuentaAutomaticos(){
	gridEstadosCuentaAutomaticos.sortRows(2,"str","asc");
	actualizarMontoEgreso();	
}

function mostrarEstadosCuentaFacultativosGrid(idsEdosCta){	
	gridEstadosCuentaFacultativos = new dhtmlXGridObject('gridboxEdosCuentaFacultativos');
	gridEstadosCuentaFacultativos.setHeader(",Tipo de Reaseguro,Vigencia,Nombre del reasegurador/corredor,Subramo,Moneda,Saldo T&eacute;cnico,");
	gridEstadosCuentaFacultativos.setInitWidths("20,145,155,260,103,100,110,0");
	gridEstadosCuentaFacultativos.setColumnIds("seleccionar,tipoReaseguro,vigencia,nombreReaseguradorCorredor,subramo,moneda,saldoTecnico,saldoTecnicoValor");
	gridEstadosCuentaFacultativos.setColAlign("center,center,center,center,center,center,center,center");
	gridEstadosCuentaFacultativos.setColSorting("str,str,str,str,str,str,str,str");
	gridEstadosCuentaFacultativos.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
	gridEstadosCuentaFacultativos.setColumnHidden(7, true);
	gridEstadosCuentaFacultativos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	gridEstadosCuentaFacultativos.setSkin("light"); 
	gridEstadosCuentaFacultativos.enableDragAndDrop(false);
	gridEstadosCuentaFacultativos.enableLightMouseNavigation(false);
	gridEstadosCuentaFacultativos.enableAutoHeight(true);
	gridEstadosCuentaFacultativos.enableAutoWidth(true);
	gridEstadosCuentaFacultativos.init();	
	gridEstadosCuentaFacultativos.load('/MidasWeb/reaseguro/egresos/listarEstadosCuentaFacultativosEgreso.do?idsEdosCta='+idsEdosCta, null, 'json');	
}

var gridCoberturasEstadosCuentaFacultativos = null;
function mostrarCoberturasEstadosCuentaFacultativosGrid(idsEdosCta){	
	if(gridCoberturasEstadosCuentaFacultativos===null){
		gridCoberturasEstadosCuentaFacultativos = new dhtmlXGridObject('gridboxCoberturasEdosCuentaFacultativos');	
		gridCoberturasEstadosCuentaFacultativos.setHeader(",P&oacute;liza,Endoso,Inciso,Secci&oacute;n,Cobertura,Subinciso,Prima de reaseguro,Reasegurador,"+
												"% Participaci&oacute;n,Participaci&oacute;n,Retener impuesto,Comisi&oacute;n,Prima neta de reaseguro");	
		gridCoberturasEstadosCuentaFacultativos.setInitWidths("20,103,65,65,100,150,80,150,120,120,100,140,80,180");
		gridCoberturasEstadosCuentaFacultativos.setColumnIds("seleccionar,poliza,endoso,inciso,seccion,cobertura,subinciso,primaReaseguro,reasegurador,"+
		  										   "porcentajeParticipacion,participacion,retenerImpuesto,comision,primaNetaReaseguro");
		gridCoberturasEstadosCuentaFacultativos.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center");
		gridCoberturasEstadosCuentaFacultativos.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str");
		gridCoberturasEstadosCuentaFacultativos.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
		gridCoberturasEstadosCuentaFacultativos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		gridCoberturasEstadosCuentaFacultativos.setSkin("light");
		gridCoberturasEstadosCuentaFacultativos.enableDragAndDrop(false);
		gridCoberturasEstadosCuentaFacultativos.enableLightMouseNavigation(false);
		gridCoberturasEstadosCuentaFacultativos.enableAutoHeight(true,600);
		gridCoberturasEstadosCuentaFacultativos.enableAutoWidth(true,900,100);
		gridCoberturasEstadosCuentaFacultativos.init();
	}
	gridCoberturasEstadosCuentaFacultativos.clearAll(false);
	gridCoberturasEstadosCuentaFacultativos.load('/MidasWeb/reaseguro/egresos/listarCoberturasEstadosCuentaEgreso.do?idsEdosCta='+idsEdosCta, null, 'json');
	
	if(gridExhibicionesCoberturasEstadosCuentaFacultativos !== null)
		gridExhibicionesCoberturasEstadosCuentaFacultativos.clearAll(false);
}

var gridExhibicionesCoberturasEstadosCuentaFacultativos = null;
function mostrarExhibicionesCoberturasEstadosCuentaFacultativosGrid(idsCobRea){	
	if(gridExhibicionesCoberturasEstadosCuentaFacultativos===null){
		gridExhibicionesCoberturasEstadosCuentaFacultativos = new dhtmlXGridObject('gridboxExhibicionesCoberturasEdosCuentaFacultativos');	
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setHeader("P&oacute;liza,Endoso,Inciso,Secci&oacute;n,Cobertura,Subinciso,Reasegurador,"+				
																	  "No. Exhibici&oacute;n, Fecha de pago,Monto a pagar,Estatus,Pagar,,,,");	
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setInitWidths("103,65,65,100,150,80,150,110,120,120,100,60,0,0,0,0");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColumnIds("poliza,endoso,inciso,seccion,cobertura,subinciso,primaReaseguro,reasegurador,"+
		  										   						 "numeroExhibicion,fechaPago,montoPagoTotal,estatusPago,pagar,montoPagoTotalValor,candidatoAPagar,idToSeccion,idToCobertura");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColSorting("str,na,na,na,str,na,str,int,date,na,str,na,na,na,na,na");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,ro,ro");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setSkin("light");
		gridExhibicionesCoberturasEstadosCuentaFacultativos.enableDragAndDrop(false);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.enableLightMouseNavigation(false);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.enableAutoHeight(true,300);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.enableAutoWidth(true,900,100);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColumnHidden(12, true);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColumnHidden(13, true);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColumnHidden(14, true);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.setColumnHidden(15, true);
		gridExhibicionesCoberturasEstadosCuentaFacultativos.init();
		gridExhibicionesCoberturasEstadosCuentaFacultativos.attachEvent("onCheck", actualizarMontoEgreso);
	}
	gridExhibicionesCoberturasEstadosCuentaFacultativos.clearAll(false);
	gridExhibicionesCoberturasEstadosCuentaFacultativos.load('/MidasWeb/reaseguro/egresos/listarExhibicionesCoberturasEstadosCuentaEgreso.do?idsCobRea='+idsCobRea, onLoadGridExhibiciones, 'json');		
}

function onLoadGridExhibiciones(){
	cargarComboSeccionesEgreso();
	deshabilitarPagosNoPendientes();	
}

//Deshabilita los checkbox del grid de exhibiciones donde la exhibici�n no sea candidata a pagarse
//(Una exhibici�n candidata a pagarse es aqu�lla que tiene estatus "PENDIENTE")
function deshabilitarPagosNoPendientes(){	
	gridExhibicionesCoberturasEstadosCuentaFacultativos.forEachRow(			
					function(id){
						var cellChkPagar = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,11);
						var cellCandidatoAPagar = gridExhibicionesCoberturasEstadosCuentaFacultativos.cells(id,13);
						if(cellCandidatoAPagar.getValue() === "0"){
							cellChkPagar.setValue("0");
							cellChkPagar.setDisabled(true);							
						}
					}
	)
}

//Suma los montos de las exhibiciones seleccionadas y actualiza el textbox del monto del egreso
function actualizarMontoEgreso(){		
	var monedaOriginal = $('idTcMonedaEdosCta').value;
	var monedaActual = $('idTcMoneda').value;	
	var montoTotalEgreso = 0;
	
	if (parseInt(monedaOriginal) != parseInt(monedaActual) && $('tipoCambio').value === ""){	
		mensaje = "Por favor, asignar un tipo de cambio para calcular el monto del egreso.";
		idMensajeInfo = "20";
		mostrarVentanaMensaje(idMensajeInfo, mensaje, null);
	}else if(parseInt(monedaOriginal) != parseInt(monedaActual) && parseFloat($('tipoCambio').value) === 0){		
		mensaje = "El tipo de cambio debe ser mayor a cero.";
		idMensajeInfo = "20";
		mostrarVentanaMensaje(idMensajeInfo, mensaje, null);
	}else{		
		if($('mostrarGridEdosCuentaAutomaticos').value === "true"){
			var checkedRowsIdsStr = gridEstadosCuentaAutomaticos.getCheckedRows(0);	
			var checkedRowsIds = checkedRowsIdsStr.split(",");	
				
			for(i=0;i<checkedRowsIds.length;i++){
				if(checkedRowsIds[i]!==null && checkedRowsIds[i]!==""){			
					rowIndex = gridEstadosCuentaAutomaticos.getRowIndex(checkedRowsIds[i]);
					montoEstadoCuenta = parseFloat(gridEstadosCuentaAutomaticos.cellByIndex(rowIndex, 6).getValue());		
					montoTotalEgreso += montoEstadoCuenta;
				}
			}
		}
		
		if($('mostrarGridEdosCuentaFacultativos').value === "true" && gridExhibicionesCoberturasEstadosCuentaFacultativos != null){						
			var checkedRowsIdsStr = gridExhibicionesCoberturasEstadosCuentaFacultativos.getCheckedRows(11);	
			var checkedRowsIds = checkedRowsIdsStr.split(",");	
				
			for(i=0;i<checkedRowsIds.length;i++){
				if(checkedRowsIds[i]!==null && checkedRowsIds[i]!==""){			
					rowIndex = gridExhibicionesCoberturasEstadosCuentaFacultativos.getRowIndex(checkedRowsIds[i]);
					montoExhibicion = parseFloat(gridExhibicionesCoberturasEstadosCuentaFacultativos.cellByIndex(rowIndex, 12).getValue());		
					montoTotalEgreso += montoExhibicion;
				}
			}
		}		
				
		if (parseInt(monedaOriginal) != parseInt(monedaActual)){				
			tipoCambio = parseFloat($('tipoCambio').value);
			if(monedaOriginal === "484")//Pesos
				factorTipoCambio = 1.00/tipoCambio;
			else //Dolares
				factorTipoCambio = tipoCambio;
			
			montoTotalEgreso = montoTotalEgreso * factorTipoCambio; 						
		}
	}
		
	modificarMontoTotalEgreso(montoTotalEgreso);			
}

function reiniciarGridsEgresos(){
	gridEstadosCuentaFacultativos = null;
	gridCoberturasEstadosCuentaFacultativos = null;
	gridExhibicionesCoberturasEstadosCuentaFacultativos = null;
}

function mostraGridPolizaRelacionada() {
	var polizaRelacionada = new dhtmlXGridObject('gridboxPolizaRelacionada');
	polizaRelacionada.setHeader("Poliza,Endoso,Ramo");
	polizaRelacionada.setInitWidths("250,250,*");
	polizaRelacionada.setColAlign("center,center,center");
	polizaRelacionada.setColSorting("str,str,str");
	polizaRelacionada.setColTypes("ro,ro,ro");
	polizaRelacionada.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	polizaRelacionada.setSkin("light"); 
	polizaRelacionada.enableDragAndDrop(false);
	polizaRelacionada.enableLightMouseNavigation(false);
	polizaRelacionada.init();
}

function mostraGridParticipacionReaseguradores() {
	var participacionReaseguradores = new dhtmlXGridObject('gridboxParticipacionReaseguradores');
	participacionReaseguradores.setHeader("Suma Asegurada,Prima Neta Poliza,Prima de Reaseguro,% Participacion, Participacion, %Comision, Comision, Prima Reaseguro");
	participacionReaseguradores.setInitWidths("150,160,160,120,100,100,100,130");
	participacionReaseguradores.setColAlign("center,center,center,center,center,center,center,center");
	participacionReaseguradores.setColSorting("str,str,str,str,str,str,str,str");
	participacionReaseguradores.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
	participacionReaseguradores.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionReaseguradores.setSkin("light"); 
	participacionReaseguradores.enableDragAndDrop(false);
	participacionReaseguradores.enableLightMouseNavigation(false);
	participacionReaseguradores.init();
}

function mostraGridParticipacionReaseguradoresCorredor() {
	var participacionReaseguradoresCorredor = new dhtmlXGridObject('gridboxParticipacionReaseguradoresCorredor');
	participacionReaseguradoresCorredor.setHeader("Nombre del Reasegurador,RGRE,% Participacion,Local Extranjero,Monto Asegurado");
	participacionReaseguradoresCorredor.setInitWidths("180,120,*,*,*,*");
	participacionReaseguradoresCorredor.setColAlign("center,center,center,center,center");
	participacionReaseguradoresCorredor.setColSorting("str,str,str,str,str");
	participacionReaseguradoresCorredor.setColTypes("ro,ro,ro,ro,ro");
	participacionReaseguradoresCorredor.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	participacionReaseguradoresCorredor.setSkin("light"); 
	participacionReaseguradoresCorredor.enableDragAndDrop(false);
	participacionReaseguradoresCorredor.enableLightMouseNavigation(false);
	participacionReaseguradoresCorredor.init();
}

function mostraGridOrdenDePagos() {
	var OrdenDePagos = new dhtmlXGridObject('gridboxOrdenDePagos2');
	OrdenDePagos.setHeader("Numero de Pago,Monto,Fecha de Pago,Estatus");
	OrdenDePagos.setInitWidths("120,120,*,*");
	OrdenDePagos.setColAlign("center,center,center,center");
	OrdenDePagos.setColSorting("str,str,str,str");
	OrdenDePagos.setColTypes("ro,ro,ro,ro");
	OrdenDePagos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	OrdenDePagos.setSkin("light"); 
	OrdenDePagos.enableDragAndDrop(false);
	OrdenDePagos.enableLightMouseNavigation(false);
	OrdenDePagos.init();
}

var autorizacionesEgresosGrid;

function inicializaComponentesListarAutorizaciones(tipoGrid){
//	alert('inicializando el data grid, tipoGrid: '+tipoGrid);
	if(tipoGrid != undefined && tipoGrid != null){
		var url = null;
		autorizacionesEgresosGrid = new dhtmlXGridObject('autorizacionPagosGrid');
		autorizacionesEgresosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		autorizacionesEgresosGrid.setSkin("light");		
		
		autorizacionesEgresosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCargaGenerico('cargandoAutorizaciones');
	    });    
		autorizacionesEgresosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCargaGenerico('cargandoAutorizaciones');
	    }); 
		if(tipoGrid == 0){
			autorizacionesEgresosGrid.setHeader(",P&oacute;liza,Endoso,Vigencia,Reasegurador,Corredor,SubRamo,Moneda,Estatus");
			autorizacionesEgresosGrid.setColumnIds("seleccionado,numeroPoliza,endoso,vigencia,reasegurador,corredor,subramo,moneda,estatus");
			autorizacionesEgresosGrid.setInitWidths("30,120,60,190,150,150,150,70,100");
			autorizacionesEgresosGrid.setColAlign("center,center,center,center,left,left,left,left,left");
			autorizacionesEgresosGrid.setColSorting("int,str,int,str,str,str,str,str,str");
			autorizacionesEgresosGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
			
		}else if(tipoGrid == 1 || tipoGrid==2){
			autorizacionesEgresosGrid.setHeader(",P&oacute;liza,Endoso,Inciso,SubInciso,Seccion,Cobertura,Reasegurador,Corredor,Exhibicion,Fecha pago,Monto,Estatus");	
			autorizacionesEgresosGrid.setColumnIds("seleccionado,numeroPoliza,endoso,inciso,subinciso,seccion,cobertura,reasegurador,corredor,exhibicion,fechaPago,primaNeta,estatus");
			autorizacionesEgresosGrid.setInitWidths("30,120,70,70,70,120,200,150,150,85,95,80,100");
			autorizacionesEgresosGrid.setColAlign("center,center,center,center,center,left,left,left,left,center,center,right,left");
			autorizacionesEgresosGrid.setColSorting("int,str,int,int,int,str,str,str,str,int,str,str,str");
			autorizacionesEgresosGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
		}else{
			alert("Tipo de grid no v�lido.");
			return;
		}
		autorizacionesEgresosGrid.init();
		autorizacionesEgresosGrid.setCSVDelimiter("\t");
		autorizacionesEgresosGrid.loadCSVString(document.getElementById('csvContenidoDataGrid').value);
	}
}

function autorizarPagosPendientes(autorizacion){
	 var checkIds = autorizacionesEgresosGrid.getCheckedRows(0);
 	 if (checkIds == ''){
 		 mostrarVentanaMensaje('10','Seleccione al menos un registro de la tabla.',null);
 	 }
	 else{
		 var tipoConsulta = document.getElementById('tipoConsulta').value;
		 var claveConsulta;
		 var url = '/MidasWeb/reaseguro/egresos/autorizaciones/autorizarPagoPendiente.do?idAutorizaciones='+checkIds;
		 url = url + '&tipoAutorizacion='+tipoConsulta;
		 url = url + '&autorizado='+autorizacion;
		 var divDestino;
		 if(tipoConsulta == 'pagoFacultativo'){
			 divDestino = 'contenido_facultativoEdoCta';
			 claveConsulta = 0;
		 }
		 else if(tipoConsulta == 'pagosVencidos'){
			 divDestino = 'contenido_exhibicionVencida';
			 claveConsulta = 1;
		 }
		 else{
			 divDestino = 'contenido_exhibicionConPagoVencido';
			 claveConsulta = 2;
		 }
		 var nextFunction = "limpiarDivsAutorizarEgresos();sendRequest(null,'/MidasWeb/reaseguro/egresos/autorizaciones/mostrarAutorizaciones.do?tipo="+
		 	tipoConsulta+"'"+",'"+divDestino+"',"+"'inicializaComponentesListarAutorizaciones("+claveConsulta+")');"

		 new Ajax.Request(url, {
				method : "post",
				asynchronous : false,
				parameters : null,
				onSuccess : function(transport) {
					procesarRespuestaXml(transport.responseXML, nextFunction);}
			});
	}
}