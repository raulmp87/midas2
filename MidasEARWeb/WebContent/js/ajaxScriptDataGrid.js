var aumentosProductoProcessor;
var aumentosProductoError = false;

var recargosProductoProcessor;
var recargosProductoError = false;

var descuentosProductoProcessor;
var descuentosProductoError = false;

var monedaProductoProcessor;

var exclusionRecargoProductoProcessor;
var exclusionAumentoProductoProcessor;
var exclusionDescuentoProductoProcessor;

var facultativoCoberturaGrid;
var cedulasAsociadas;
var cedulasPorAsociar

function validaGrid(banderaErrorGrid) {
	if(!banderaErrorGrid) { 
		return true; 
	} else {
		//mostrarVentanaMensaje("10", "Algunos datos introducidos no son v&aacute;lidos");
		return false;
	}
}

/*Grid de cobertura cotizacion en Danios->ordenes de trabajo-> coberturas 
function cargaComponentesCoberturaCotizacion(){
	var ids = new Array(1);
	creaArbolOrdenesDeTrabajo('iniciarCoberturaCotizacionGrids');
	mostrarCoberturaODTGrids();	
}

function iniciarCoberturaCotizacionGrids(idToCotizacion,numeroInciso,idToSeccion){
	mostrarCoberturaODTGrids(idToCotizacion,numeroInciso,idToSeccion);
}
var coberturaCotizacionPath;
*/


var coberturaCotizacionGrids;

function mostrarCoberturaODTGrids(idToCotizacion,numeroInciso,idToSeccion){

	coberturaCotizacionGrids = new dhtmlXGridObject("cotizacionCoberturasGrid");
	coberturaCotizacionGrids.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coberturaCotizacionGrids.setEditable(true);
	coberturaCotizacionGrids.setSkin("light");

	coberturaCotizacionGrids.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comisi\u00F3n,Coaseguro,,Deducible,,Tipo Deducible,M\u00EDnimo, M\u00E1ximo,T.L\u00EDmite Deducible,A/R/D,,,");
	coberturaCotizacionGrids.setInitWidths("0,0,0,0,0,0,0,0,300,100,180,50,100,80,80,30,80,30,60,40,40,60,60,0,0,0");
	coberturaCotizacionGrids.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,edn,ro,co,img,co,img,coro,edn,edn,coro,img,ro,ro,ro");
	coberturaCotizacionGrids.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,left,left,left,left,center,center,center");
	coberturaCotizacionGrids.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int,int");
	coberturaCotizacionGrids.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
	coberturaCotizacionGrids.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,tipoDeducible,minimo,maximo,tipoLimiteDeducible,img3,desglosaRiesgo,claveSubIncisos,claveIgualacion");		
	coberturaCotizacionGrids.setColumnHidden(0,true);
	coberturaCotizacionGrids.setColumnHidden(1,true);
	coberturaCotizacionGrids.setColumnHidden(2,true);
	coberturaCotizacionGrids.setColumnHidden(3,true);
	coberturaCotizacionGrids.setColumnHidden(4,true);
	coberturaCotizacionGrids.setColumnHidden(5,true);
	coberturaCotizacionGrids.setColumnHidden(6,true);
	coberturaCotizacionGrids.setColumnHidden(7,true);
	
	coberturaCotizacionGrids.setColumnHidden(11,true);
	coberturaCotizacionGrids.setColumnHidden(12,true);
	coberturaCotizacionGrids.setColumnHidden(13,true);
	coberturaCotizacionGrids.setColumnHidden(14,true);
	coberturaCotizacionGrids.setColumnHidden(15,true);
	coberturaCotizacionGrids.setColumnHidden(16,true);
	coberturaCotizacionGrids.setColumnHidden(17,true);
	coberturaCotizacionGrids.setColumnHidden(18,true);
	coberturaCotizacionGrids.setColumnHidden(19,true);
	coberturaCotizacionGrids.setColumnHidden(20,true);
	coberturaCotizacionGrids.setColumnHidden(21,true);
	coberturaCotizacionGrids.setColumnHidden(22,true);
	coberturaCotizacionGrids.setColumnHidden(23,true);
	coberturaCotizacionGrids.setColumnHidden(24,true);
	coberturaCotizacionGrids.setColumnHidden(25,true);	
	coberturaCotizacionGrids.setNumberFormat("$0,000.00",10);
	coberturaCotizacionGrids.setNumberFormat("$0,000.00",12);
	coberturaCotizacionGrids.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = coberturaCotizacionGrids.cellById(rId, 4);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 9) {
			var existeObligatoriaParcialContratada = false;
			coberturaCotizacionGrids.forEachRow(function(id){
				var obligatoriedadCurrentRow = coberturaCotizacionGrids.cellById(id, 4);
				var claveContrato = coberturaCotizacionGrids.cellById(id, 9);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Cobertura no puede ser descontratada.\nDebe contratar al menos una Cobertura Obligatoria Parcial.");
				return false;
			}
		}
		if(stage == 2 && cInd == 9 ){
			seleccionarCobertura(stage,rId,cInd,nValue,oValue);
		}
		return true;
	});	
	coberturaCotizacionGrids.attachEvent("onRowCreated",function(rowId, rowObj) {
		coberturaCotizacionGrids.cellById(rowId, 12).setDisabled(true);
		var tipoSumaAsegurada = coberturaCotizacionGrids.cellById(rowId, 5);
		var desglosaRiesgo = coberturaCotizacionGrids.cellById(rowId, 23);
		var requiereSubIncisos = coberturaCotizacionGrids.cellById(rowId, 24);
		var obligatoriedad = coberturaCotizacionGrids.cellById(rowId, 4);
		var sumaAsegurada = coberturaCotizacionGrids.cellById(rowId, 10);
		var claveContrato = coberturaCotizacionGrids.cellById(rowId, 9);
		var coaseguro = coberturaCotizacionGrids.cellById(rowId, 14);
		var deducible = coberturaCotizacionGrids.cellById(rowId, 16);
		var cellARD = coberturaCotizacionGrids.cellById(rowId, 22);
		var claveIgualacion = coberturaCotizacionGrids.cellById(rowId, 25);
		
		if(claveContrato.getValue()=='1'){
			var idToCotizacion =  coberturaCotizacionGrids.cellById(rowId, 0).getValue();
			var numeroInciso =  coberturaCotizacionGrids.cellById(rowId, 1).getValue();
			var idToSeccion =  coberturaCotizacionGrids.cellById(rowId, 2).getValue();
			var idToCobertura =  coberturaCotizacionGrids.cellById(rowId, 3).getValue();
			if(claveIgualacion.getValue() == "1") {
				var cadena = "javascript: sendRequest(null,&#39;/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion="+idToCotizacion;
				cadena = cadena +"&numeroInciso="+numeroInciso+"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura;
				cadena = cadena +"&#39;,&#39;configuracion_detalle&#39;,null);";
				cellARD.setValue("/MidasWeb/img/details.gif^Aumentos/Recargos/Descuentos^"+cadena+"^_self");
			} else {
				coberturaCotizacionGrids.setCellExcellType(rowId,22,"ro");
				coberturaCotizacionGrids.cellById(rowId, 22).setValue('');
			}
		}else{
			coberturaCotizacionGrids.setCellExcellType(rowId,22,"ro");
			coberturaCotizacionGrids.setCellExcellType(rowId,17,"ro");
			coberturaCotizacionGrids.setCellExcellType(rowId,15,"ro");			
			//coberturaCotizacionGrids.setCellExcellType(rowId,18,"ro");
			//coberturaCotizacionGrids.setCellExcellType(rowId,21,"ro");
			
			//Agregado por CesarMorales..
			if(tipoSumaAsegurada.getValue()=='1' || tipoSumaAsegurada.getValue()=='3'){
				coberturaCotizacionGrids.cellById(rowId, 10).setValue('0.00');
			}
			/////
			coberturaCotizacionGrids.cellById(rowId, 11).setValue('0.0000');
			coberturaCotizacionGrids.cellById(rowId, 12).setValue('0.0');
			coberturaCotizacionGrids.cellById(rowId, 22).setValue('');
			coberturaCotizacionGrids.cellById(rowId, 17).setValue('');
			coberturaCotizacionGrids.cellById(rowId, 15).setValue('');
			//coberturaCotizacionGrids.cellById(rowId, 18).setValue('');
			//coberturaCotizacionGrids.cellById(rowId, 21).setValue('');

		}
		
		if(tipoSumaAsegurada.getValue() == '2' || desglosaRiesgo.getValue() == '1' || requiereSubIncisos.getValue() == '1'){
			sumaAsegurada.setDisabled(true);
		}else{			
			coberturaCotizacionGrids.setCellExcellType(rowId,10,"edn");		
		}

		if(desglosaRiesgo.getValue() === '1'){				
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);
			coberturaCotizacionGrids.cellById(rowId, 18).setDisabled(true);
			coberturaCotizacionGrids.cellById(rowId, 19).setDisabled(true);
			coberturaCotizacionGrids.cellById(rowId, 20).setDisabled(true);
			coberturaCotizacionGrids.cellById(rowId, 21).setDisabled(true);
		}else{		
			var idToCobertura = coberturaCotizacionGrids.cellById(rowId,3);		
			var coaseguroCombo = coberturaCotizacionGrids.getCustomCombo(rowId,14);
			var deducibleCombo = coberturaCotizacionGrids.getCustomCombo(rowId,16);			
			
			getCoasegurosDeducibles("C", idToCobertura.getValue(),coaseguroCombo);				
			getCoasegurosDeducibles("D", idToCobertura.getValue(),deducibleCombo);		

	
		}
		if(obligatoriedad.getValue() === '3'){
			claveContrato.setDisabled(true);
		}
		if (claveContrato.getValue() === '0'){
			sumaAsegurada.setDisabled(true);
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);			
		}
		
		return true;
	});	

	
	coberturaCotizacionGrids.attachEvent("onCheckbox",function(rowId, cellInd, state) {
		coberturaCotizacionGrids.cellById(rowId, 10).setDisabled(!state);
		return true;
	});	
	
	coberturaCotizacionGrids.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacion/cobertura/mostrarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	coberturaCotizacionGrids.load(cotizacionCoberturaPath,null, 'json');	
	/*coberturaCotizacionGrids.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
	coberturaCotizacionGrids.customGroupFormat=function(name,count){
		var titulo;
		if(name == "0") {
			titulo = "Coberturas no Contratadas";
		} else {
			titulo = "Coberturas Contratadas";
		}
        return titulo + " (" + count + ")";
    }*/
	processor = new dataProcessor("/MidasWeb/cotizacion/cobertura/guardarCobertura.do");
	
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("cell");
	processor.setVerificator(10,mayorA0);
	//processor.setVerificator(14,mayorA0);
	//processor.setVerificator(17,mayorA0);
	processor.defineAction("coberturaContratada", function(node){
		var rowId = node.getAttribute("sid");
		var idCotizacion = coberturaCotizacionGrids.cellById(rowId, 0);
		creaArbolOrdenesDeTrabajo(idCotizacion.getValue());
		var index = coberturaCotizacionGrids.getRowIndex(rowId);
		coberturaCotizacionGrids.changeRowId(rowId, "-1");
		coberturaCotizacionGrids.deleteRow("-1");
		coberturaCotizacionGrids.addRow(rowId, node.firstChild.data, index);
		var claveContrato = coberturaCotizacionGrids.cellById(rowId, 9);

		if(claveContrato.getValue() != 1) {
			var cuota = coberturaCotizacionGrids.cellById(rowId, 11);
			cuota.setDisabled(true);
			var comision = coberturaCotizacionGrids.cellById(rowId, 13);
			comision.setDisabled(true);
			coberturaCotizacionGrids.setCellExcellType(rowId,14,"ro");
			var combo1 = coberturaCotizacionGrids.cellById(rowId, 14);
			combo1.setValue('');
			coberturaCotizacionGrids.setCellExcellType(rowId,15,"ro");
			var img1 = coberturaCotizacionGrids.cellById(rowId, 15);
			img1.setValue('');
			coberturaCotizacionGrids.setCellExcellType(rowId,16,"ro");
			var combo2 = coberturaCotizacionGrids.cellById(rowId, 16);
			combo2.setValue('');
			coberturaCotizacionGrids.setCellExcellType(rowId,17,"ro");
			var img2 = coberturaCotizacionGrids.cellById(rowId, 17);
			img2.setValue('')
			coberturaCotizacionGrids.setCellExcellType(rowId,22,"ro");
			var img3 = coberturaCotizacionGrids.cellById(rowId, 22);
			img3.setValue('')			
		}
		return true;
	});
/*	
	processor.attachEvent("onAfterUpdate",function(sid,action,tid,xml_node){
		coberturaCotizacionGrids.unGroup();
		coberturaCotizacionGrids.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
		coberturaCotizacionGrids.customGroupFormat=function(name,count){
			var titulo;
			if(name == "0") {
				titulo = "Coberturas no Contratadas";
			} else {
				titulo = "Coberturas Contratadas";
			}
	        return titulo + " (" + count + ")";
	    }
        return true;
   })
   	*/
	processor.init(coberturaCotizacionGrids);		
}

/*
var cotizacionCoberturaPath;
var cotizacionCoberturaGrids;

function habilitaCamposTablaCoberturaCotizacion(){
	for (var i=0; i<coberturaCotizacionGrids.getRowsNum() && coberturaCotizacionGrids.getRowsNum()>0; i++){
		col = coberturaCotizacionGrids.getColIndexById('obligatoriedad');
		if (coberturaCotizacionGrids.cellByIndex(i,col).getValue()==2){
			col = coberturaCotizacionGrids.getColIndexById('contratada');
			coberturaCotizacionGrids.cellByIndex(i,col).setDisabled(true);
		}
		
		col = coberturaCotizacionGrids.getColIndexById('habilitaSumaAsegurada');
		if (coberturaCotizacionGrids.cellByIndex(i,col).getValue()==0){
			col = coberturaCotizacionGrids.getColIndexById('valorSumaAsegurada');
			coberturaCotizacionGrids.cellByIndex(i,col).setDisabled(true);
		}
	}
	
}

function initCoberturaCotizacionGrids(idToCotizacion,numeroInciso,idToSeccion) {
	processor = new dataProcessor("/MidasWeb/ordentrabajo/cobertura/guardarCoberturas.do");
	coberturaCotizacionPath = '/MidasWeb/ordentrabajo/cobertura/mostrarCoberturasPorSeccion.do';
	coberturaCotizacionPath += '?idToCotizacion='+idToCotizacion;
	coberturaCotizacionPath += '&numeroInciso='+numeroInciso;
	coberturaCotizacionPath += '&idToSeccion='+idToSeccion;					
}

function mostrarCoberturaODTGrids(idToCotizacion,numeroInciso,idToSeccion){
	coberturaCotizacionGrids = new dhtmlXGridObject('coberturaCotizacionGrid');
	coberturaCotizacionGrids.setHeader(",,,"
			+",,,," 
			+"Cobertura , Contratada, Valor Suma Asegurada");
	coberturaCotizacionGrids.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,"
			+"obligatoriedad,habilitaSumaAsegurada,requeridas,excluidas,"
			+"cobertura,contratada,valorSumaAsegurada");
	coberturaCotizacionGrids.setInitWidths("0,0,0,"+
			"0,0,0,0,"
			+"*,80,200");
	coberturaCotizacionGrids.setColAlign("center,center,center,"
			+"center,center,center,center,"
			+"center,center,center");
	coberturaCotizacionGrids.setColSorting("int,int,int,"
			+"int,int,str,str,"+
			"int,int,int");
	coberturaCotizacionGrids.setColTypes("ro,ro,ro,"
			+"ro,ro,ro,ro,"
			+"ro,ch,ed");
	
	coberturaCotizacionGrids.attachEvent("onXLE", function(){
		
	});
	coberturaCotizacionGrids.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coberturaCotizacionGrids.setSkin("light");		
	coberturaCotizacionGrids.enableDragAndDrop(false);
	coberturaCotizacionGrids.enableLightMouseNavigation(false);
	coberturaCotizacionGrids.attachEvent("onEditCell",seleccionarCoberturaCotizacion);
	coberturaCotizacionGrids.init();	
	
	initCoberturaCotizacionGrids(idToCotizacion,numeroInciso,idToSeccion);
	coberturaCotizacionGrids.load(coberturaCotizacionPath,habilitaCamposTablaCoberturaCotizacion , 'json');	
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("cell");	
	processor.defineAction("updated", function(node){
		var idToCotizacion = node.getAttribute("idToCotizacion");
		var idElement = node.getAttribute("extra");
		creaArbolOrdenesDeTrabajo(idToCotizacion);
		ordenTrabajoTreeAbreElementById = idElement;
		return true;
	});
	processor.init(coberturaCotizacionGrids);
}
*/
function seleccionarCoberturaCotizacion(stage,rowId,cellIndex,newValue,oldValue) {
	if(stage==0)
		return true;				
	if (stage == 1){		
		col = coberturaCotizacionGrids.getColIndexById('contratada'); 
		if (col==cellIndex){
			var estanSeleccionadasRequeridas = true;
			var estanSeleccionadasExcluidas = false;
			var indexRequeridas = coberturaCotizacionGrids.getColIndexById('requeridas');
			var indexExcluidas = coberturaCotizacionGrids.getColIndexById('excluidas');
			var arrayRequeridas = coberturaCotizacionGrids.cellById(rowId,indexRequeridas).getValue();
			var arrayExcluidas = coberturaCotizacionGrids.cellById(rowId,indexExcluidas).getValue();
			var requeridas = arrayRequeridas.split('_');
			var excluidas = arrayExcluidas.split('_');

			for (var i = 0; i < requeridas.length && arrayRequeridas!='' && coberturaCotizacionGrids.cells(rowId,cellIndex).isChecked(); i++){
				if (coberturaCotizacionGrids.getRowIndex(requeridas[i])>-1 && !coberturaCotizacionGrids.cellById(requeridas[i],cellIndex).isChecked()){
					estanSeleccionadasRequeridas = false;
				}
			}
			
			if (!estanSeleccionadasRequeridas)	{
				processor.setUpdated(rowId,false,"update");	
				coberturaCotizacionGrids.cells(rowId,cellIndex).setValue(oldValue);
				return false;
			}
			
			for (var i = 0; i < excluidas.length && arrayExcluidas!='' && coberturaCotizacionGrids.cells(rowId,cellIndex).isChecked(); i++){
				if (coberturaCotizacionGrids.getRowIndex(excluidas[i])>-1 && coberturaCotizacionGrids.cellById(excluidas[i],cellIndex).isChecked()){
					estanSeleccionadasExcluidas = true;
				}
			}
			
			if (estanSeleccionadasExcluidas)	{
				alert('No se puede seleccionar porque esta seleccionada una cobertura que la excluye');
				coberturaCotizacionGrids.cells(rowId,cellIndex).setValue(oldValue);
				return false;
			}
			
			var idToCotizacion = coberturaCotizacionGrids.cells(rowId,coberturaCotizacionGrids.getColIndexById('idToCotizacion')).getValue();
			var numeroInciso = coberturaCotizacionGrids.cells(rowId,coberturaCotizacionGrids.getColIndexById('numeroInciso')).getValue();
			var idToSeccion = coberturaCotizacionGrids.cells(rowId,coberturaCotizacionGrids.getColIndexById('idToSeccion')).getValue();
			var idElement = idToCotizacion + '_' + numeroInciso + '_' + idToSeccion;
			
			/*if(coberturaCotizacionGrids.cells(rowId,cellIndex).isChecked()){
				processor.sendData(rowId);
				processor.setUpdated(rowId,false,"update");
				creaArbolOrdenesDeTrabajo(idToCotizacion);
				ordenTrabajoTreeAbreElementById = idElement;
			}else{
				processor.sendData(rowId);
				processor.setUpdated(rowId,true,"update");
				creaArbolOrdenesDeTrabajo(idToCotizacion);
				ordenTrabajoTreeAbreElementById = idElement;
			}*/
			//processor.sendData(rowId);
		}
	}		
	
	col = coberturaCotizacionGrids.getColIndexById('valorSumaAsegurada');
	if (stage==2 && col==cellIndex){
		var valor = coberturaCotizacionGrids.cellById(rowId,cellIndex).getValue();
		if ( valor>=0 ){
			//processor.sendData(rowId);
			//processor.setUpdated(rowId,false,"update");
		}else{
			return false;
		}
	}
	return true;
}
/*fin cobertura cotizacion*/

/* Codigo documentos anexos en danios->cotizacion->documentosAnexos*/
var docAnexoPath;
var docAnexoGrid;
var docAnexoProcessor;

function cargaComponentesDocAnexos(){
	docAnexoGrid = new dhtmlXGridObject('contenido_documentosAnexosGrid');
	docAnexoGrid.setHeader(",,Seleccione,Nivel,Anexo,");
	docAnexoGrid.setColumnIds("idToCotizacion,obligatoriedad,claveSeleccion,tipo,descripcion,download");
	docAnexoGrid.setInitWidths("0,0,100,80,*,20");
	docAnexoGrid.setColAlign("center,center,center,center,left,center");
	docAnexoGrid.setColSorting("int,int,int,str,str,na");
	docAnexoGrid.setColTypes("ro,ro,ch,ro,ro,img");
	docAnexoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	docAnexoGrid.setSkin("light");
	docAnexoGrid.enableDragAndDrop(true);
	docAnexoGrid.enableLightMouseNavigation(false);
	docAnexoGrid.attachEvent("onEditCell",function(){
		return true;
	});
	docAnexoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		var obligatoriedad=docAnexoGrid.cellById(rowId, 1).getValue();
		var tipo=docAnexoGrid.cellById(rowId, 3).getValue();
			
		if(obligatoriedad=="3" && (tipo=="TP" || tipo=="P")){
			docAnexoGrid.cellById(rowId, 2).setDisabled(true);
		}
	});	
	//docAnexoGrid.attachEvent("onDrop",cambiarPosicionDocAnexo);
	docAnexoGrid.attachEvent("onDrop",function(rowId) {
		docAnexoProcessor.setUpdated(rowId,true,"update");
		return true;
	});
	docAnexoGrid.setColumnHidden(0, true);
	docAnexoGrid.setColumnHidden(1, true);
	docAnexoGrid.init();	
	
	docAnexoProcessor = new dataProcessor("/MidasWeb/cotizacion/documento/docAnexoCot/modificarDocAnexo.do");
	
	docAnexoPath = '/MidasWeb/cotizacion/documento/docAnexoCot/mostrarListarAnexos.do?idToCotizacion='+$('idToCotizacion').value;
	docAnexoProcessor.attachEvent("onAfterUpdateFinish",function(){
		if(docAnexoProcessor.getSyncState()){
			orden = docAnexoGrid.getAllRowIds();
			idcotizacion = parent.document.getElementById("idToCotizacion").value;
			new Ajax.Request('/MidasWeb/cotizacion/documento/docAnexoCot/guardarOrden.do?orden='+orden+'&idToCotizacion='+idcotizacion,{
				method : "post",
				asynchronous : false
			});
			hideIndicator();
			}
			return true;
			});
	docAnexoGrid.load(docAnexoPath, null, 'json');	
	docAnexoProcessor.enableDataNames(true);
	docAnexoProcessor.setTransactionMode("POST");
//	docAnexoProcessor.setUpdateMode("row");	
	docAnexoProcessor.setUpdateMode("off");
	docAnexoProcessor.init(docAnexoGrid);
}

function descargarAnexo(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}
//function cambiarPosicionDocAnexo(){
//	orden = docAnexoGrid.getAllRowIds();	
//	new Ajax.Request('/MidasWeb/cotizacion/documento/docAnexoCot/guardarOrden.do?orden='+orden,null);	
//}

/*function seleccionarDocAnexo(stage,rowId,cellIndex,newValue,oldValue){
//	if(stage==0)
//		return true;
	if (stage==1){
		if(docAnexoGrid.cells(rowId,cellIndex).isChecked()){
			docAnexoProcessor.setUpdated(rowId,false,"update");
			docAnexoProcessor.sendData(rowId);
		}else{
			docAnexoProcessor.setUpdated(rowId,true,"update");
			docAnexoProcessor.sendData(rowId);
		}
	}
	return true;
}*/
/* Fin Codigo documentos anexos en danios->cotizacion->documentosAnexos*/

/* C�digo para textos adicionales danios->cotizacion->textosAdicionales*/
var texAdicionalPath;
var texAdicionalGrid;

function cargaComponentesTexAdicional(){
	mostrarTexAdicionalGrid();
}

function initTexAdicionalGrid() {
	processor = new dataProcessor("/MidasWeb/cotizacion/documento/texAdicionalCot/agregarTextoAdicional.do");
	texAdicionalPath = '/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarListarTextosAdicionales.do?idToCotizacion='+
					$('idToCotizacion').value;	
}

function mostrarTexAdicionalGrid() {
	texAdicionalGrid = new dhtmlXGridObject('contenido_textosAdicionalesGrid');
	texAdicionalGrid.setHeader("Texto Adicional,Solicitado por,Autorizado/Rechazado por, Autorizaci&oacute;n, Fecha,");
	texAdicionalGrid.setColumnIds("textAdicional,nombreUsuarioCreacion,nombreUsuarioModificacion,estatus,fechaCreacion,borrar");
	texAdicionalGrid.setInitWidths("300,180,180,100,120,30");
	texAdicionalGrid.setColAlign("left,center,center,center,center,center");
	texAdicionalGrid.setColSorting("str,str,str,str,str,str");
	texAdicionalGrid.setColTypes("ed,ro,ro,img,ro,img");
	texAdicionalGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	texAdicionalGrid.setSkin("light");		
	texAdicionalGrid.enableDragAndDrop(true);
	texAdicionalGrid.enableLightMouseNavigation(false);
	texAdicionalGrid.attachEvent("onEditCell",seleccionarTexAdicional);
	texAdicionalGrid.attachEvent("onDrop",cambiarPosicionTexAdicional);
	texAdicionalGrid.attachEvent("onBeforeDrag",antesMoverTexAdicional);
	texAdicionalGrid.enableMultiline(true);

	texAdicionalGrid.init();	
	
	initTexAdicionalGrid();
	texAdicionalGrid.load(texAdicionalPath, null, 'json');
}

function antesMoverTexAdicional(id){
	if (id < 0){
		return false;
	}
	return true;
}

function agregarFilaTexAdicional(){
	var newRow = '';
	rowID = texAdicionalGrid.getRowIndex(-1); 
	if (rowID < 0){
		newRow += ','+$('usuario').value+',,/MidasWeb/img/pixel.gif,'+$('fecha').value+',';
		newRow += '/MidasWeb/img/delete14.gif^Borrar^javascript: borrarTexAdicionalCot(-1);^_self';
		texAdicionalGrid.addRow(-1,newRow);		
	}else{
		alert('Tiene que escribir el texto pendiente antes de agregar otro');
	}
}

function borrarTexAdicionalCot(id){
	texAdicionalGrid.deleteRow(id);
}

function cambiarPosicionTexAdicional(id){	
	if (id > 0){
		rowID = texAdicionalGrid.getRowIndex(-1);
		if (rowID > -1){
			for(var i=rowID;i<texAdicionalGrid.getRowsNum();i++)
				texAdicionalGrid.moveRowDown(-1);	
		}
		orden = texAdicionalGrid.getAllRowIds();
		new Ajax.Request('/MidasWeb/cotizacion/documento/texAdicionalCot/guardarOrden.do?orden='+orden,null);
		return true;
	}
}

function seleccionarTexAdicional(stage,rowId,cellIndex,newValue,oldValue){
	var seGuardo = false;
	if(stage==0)
		return true;	
	if (stage==1)
		texAdicionalGrid.setRowTextBold(rowId);
		col = texAdicionalGrid.getColIndexById('textAdicional');
		
	if (col==cellIndex && stage==2){
		texAdicionalGrid.setRowTextNormal(rowId);
		if (trim(newValue)!=oldValue && trim(newValue).length > 0){
			new Ajax.Request("/MidasWeb/cotizacion/documento/texAdicionalCot/agregarTextoAdicional.do?idToCotizacion="
					+$('idToCotizacion').value+"&posicion="+texAdicionalGrid.getRowsNum(), {
				method : "post",
				asynchronous : false,
				parameters : "gr_id=" + rowId + "&textAdicional=" + encodeURIComponent(newValue),
				onSuccess : function(transport) {
					new Ajax.Request('/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarTexAdicional.do',{
						method : "post",
						asynchronous : false,
						onSuccess : function(transport) {
							$('contenido_textosAdicionales').innerHTML = transport.responseText;
							cargaComponentesTexAdicional();
						}
					}); 
				} // End of onSuccess
			});	
		}
	}

	return seGuardo;
}

function resultadoGuardarTexAdicional(xmlDoc, rowId){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;

	if (id > 0){
		texAdicionalGrid.changeRowId(rowId,id);
		col = texAdicionalGrid.getColIndexById('estatus');
		texAdicionalGrid.cellById(id,col).setValue('/MidasWeb/img/b_pendiente.gif^Pendiente');
		col = texAdicionalGrid.getColIndexById('borrar');
		urlBorrar = '/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestTexAdicionalCot(1,'+id+');^_self';
		texAdicionalGrid.cellById(id,col).setValue(urlBorrar);
		return true;
	}
	return false;
}

function sendRequestTexAdicionalCot(actionId,id){
	switch(actionId){
		case 1:
			if (confirm('\u00BFDesea eliminar el texto adicional seleccionado?')){
				eliminarTexAdicional(id);
			}
			break;
	}
}

function eliminarTexAdicional(id){
	var pars = 'idToTexAdicionalCot='+id;

	
	new Ajax.Request('/MidasWeb/cotizacion/documento/texAdicionalCot/borrarTextoAdicional.do?idToTexAdicionalCot='+id, {
		method : "post",asynchronous : false,parameters :pars,
		onSuccess : function(transport) {
			sendRequest(null,'/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarTexAdicional.do','contenido_textosAdicionales','cargaComponentesTexAdicional()');
		}
	});	
}
/* Fin C�igo para el grid que lista de textos adicionales */


/* Textos adicionales por autorizar en danios->cotizacion->autorizacionTextosAdionales*/
var texAdicionalPorAutorizarPath;
function cargaComponentesTexAdicionalPorAutorizar(){
	mostrarTexAdicionalPorAutorizarGrid();
}

function mostrarTexAdicionalPorAutorizarGrid() {
	texAdicionalPorAutorizarGrid = new dhtmlXGridObject('contenido_textosAdicionalesPorAutorizarGrid');
	texAdicionalPorAutorizarGrid.setHeader(",Autorizaci&oacute;n,Texto Adicional,Solicitado por,Autorizado por, Fecha,");
	texAdicionalPorAutorizarGrid.setColumnIds("seleccionado,icon,textAdicional,nombreUsuarioCreacion,nombreUsuarioModificacion,fechaCreacion,estatus");
	texAdicionalPorAutorizarGrid.setInitWidths("30,80,300,200,200,120,0");
	texAdicionalPorAutorizarGrid.setColAlign("center,center,center,center,center,center,center");
	texAdicionalPorAutorizarGrid.setColSorting("int,str,str,str,str,str,int");
	texAdicionalPorAutorizarGrid.setColTypes("ch,img,ro,ro,ro,ro,ro");
	texAdicionalPorAutorizarGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	texAdicionalPorAutorizarGrid.setSkin("light");		
	texAdicionalPorAutorizarGrid.enableDragAndDrop(false);
	texAdicionalPorAutorizarGrid.enableLightMouseNavigation(false);
	texAdicionalPorAutorizarGrid.attachEvent("onEditCell",seleccionarTexAdicionalPorAutorizar);
		
	texAdicionalPorAutorizarPath = '/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarAutorizarTextosAdicionales.do?idToCotizacion='+
	$('idToCotizacion').value;
	texAdicionalPorAutorizarGrid.init();
	texAdicionalPorAutorizarGrid.load(texAdicionalPorAutorizarPath, null, 'json');

	processorTAA = new dataProcessor("/MidasWeb/cotizacion/documento/texAdicionalCot/cambiarEstatusTextoAdicional.do");
	processorTAA.enableDataNames(true);
	processorTAA.setTransactionMode("POST");
	processorTAA.setUpdateMode("off");	
	processorTAA.attachEvent("onAfterUpdateFinish",function(){
		mostrarTexAdicionalPorAutorizarGrid();
	});
	processorTAA.init(texAdicionalPorAutorizarGrid);
	

}

function seleccionarTexAdicionalPorAutorizar(stage,rowId,cellIndex,newValue,oldValue){
	if(stage==0)
		return true;		
				
	if(texAdicionalPorAutorizarGrid.cells(rowId,cellIndex).isChecked()){
		processorTAA.setUpdated(rowId,false,"update");
	}else{
		processorTAA.setUpdated(rowId,true,"update");
	}
}

function procesarTextosAdicionales(estatus){
	var indiceSeleccionados = texAdicionalPorAutorizarGrid.getCheckedRows(0);
	var esUnElemento = trim(indiceSeleccionados).length>0 ;

	if (!esUnElemento){
		alert('Seleccione al menos un elemento de la lista');
		return false;
	}
	
	for(var i=0;i<texAdicionalPorAutorizarGrid.getRowsNum();i++){
		col = texAdicionalPorAutorizarGrid.getColIndexById('estatus');
		texAdicionalPorAutorizarGrid.cellByIndex(i,col).setValue(estatus);
	}
	processorTAA.sendData();	
}
/*Fin codigo Textos adicionales por autorizar */

/* Grid de Secciones en Da�os->Orden Trabajo->Administracion Secciones */

var seccionesPorInciso;

function inicializarAdministracionDeSecciones(){
	$('idToCotizacion').value = 1;
	creaArbolOrdenesDeTrabajo('cargaSeccionesPorInciso');
	mostrarSeccionesPorInciso(-1); 
}

function inicializarArbolAgregarSubIncisos(){
	creaArbolOrdenesDeTrabajo('cargaSeccionesPorInciso');
}

function cargaSeccionesPorInciso(numeroInciso, nivel, idToCotizacion){
	if (nivel == 2){
		document.getElementById('configuracion_detalle').style.display ="block";
		mostrarSeccionesPorInciso(numeroInciso,idToCotizacion);
	}
}

function mostrarSeccionesPorInciso(numeroInciso,idToCotizacion, origen, tipoEndoso){
	if(origen == 'END') {
		return mostrarSeccionesPorIncisoEndoso(numeroInciso,idToCotizacion, tipoEndoso);
	} else if(origen == 'COT'){
		return mostrarSeccionesPorIncisoCOT(numeroInciso,idToCotizacion, tipoEndoso);
	}
	seccionesPorInciso = new dhtmlXGridObject('seccionesPorIncisoGrid');									  
	seccionesPorInciso.setHeader("Secci\u00F3n,Contratada,Clave Obligatoriedad,Suma Asegurada,Prima Neta,Subincisos,puedeTenerSubIncisos");
	seccionesPorInciso.setColumnIds("seccion,claveContrato,claveObligatoriedad,sumaAsegurada,primaNeta,seleccionado,puedeTenerSubIncisos");
	seccionesPorInciso.setInitWidths("200,100,0,130,0,100,100");
	seccionesPorInciso.setColAlign("left,center,center,center,center,center,center");
	seccionesPorInciso.setColSorting("str,str,int,str,str,str,int");
	seccionesPorInciso.setColTypes("ro,ch,ro,edn,edn,ra,ro");
	seccionesPorInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesPorInciso.setColumnHidden(2, true);
	seccionesPorInciso.setColumnHidden(3, true);
	seccionesPorInciso.setColumnHidden(4, true);	
	seccionesPorInciso.setColumnHidden(6, true);
	seccionesPorInciso.setNumberFormat("$0,000.00", 3);
	seccionesPorInciso.setNumberFormat("$0,000.00", 4);
	seccionesPorInciso.setSkin("light");
	seccionesPorInciso.attachEvent("onRowCreated", function(rId,rObj,rXml){
		var claveObligatoriedad = seccionesPorInciso.cellById(rId, 2);
		if(claveObligatoriedad.getValue() == 3) {
			seccionesPorInciso.cellById(rId, 1).setDisabled(true);
		}
		var puedeTenerSubIncisos = seccionesPorInciso.cellById(rId, 6);
		if (puedeTenerSubIncisos.getValue() > 1){
			seccionesPorInciso.setCellExcellType(rId,5,"ro");
			seccionesPorInciso.cellById(rId, 5).setValue('');
		}
		seccionesPorInciso.cellById(rId, 3).setDisabled(true);
		seccionesPorInciso.cellById(rId, 4).setDisabled(true);
	});

	seccionesPorInciso.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = seccionesPorInciso.cellById(rId, 2);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 1) {
			var existeObligatoriaParcialContratada = false;
			seccionesPorInciso.forEachRow(function(id){
				var obligatoriedadCurrentRow = seccionesPorInciso.cellById(id, 2);
				var claveContrato = seccionesPorInciso.cellById(id, 1);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Secci\u00F3n no puede ser descontratada.\nDebe contratar al menos una Secci\u00F3n Obligatoria Parcial.");
				return false;
			}
		}
		return true;
	});

	seccionesPorInciso.attachEvent("onCellChanged",function(rId,cInd,nValue){
		var claveContrato = seccionesPorInciso.cellById(rId, 1);
		if(cInd == 5 && nValue== 1 && claveContrato.getValue() == '0') {
			cellInTurn = seccionesPorInciso.cellById(rId,5);
			document.getElementById('resultados').style.display ="none";
			document.getElementById('b_agregar').style.display ="none";
			processor.setUpdated(rId, false,'updated');
			return false;
		}
		cellInTurn = seccionesPorInciso.cellById(rId,6);
		if (cellInTurn.getValue() < 2){ 
			if (cInd == 5 && nValue== 1){
				cellInTurn = seccionesPorInciso.cellById(rId,1);
				ids=rId.split(',');
				$('idToCotizacion').value=ids[0];
				$('numeroInciso').value=ids[1];
				$('idToSeccion').value=ids[2];
				document.getElementById('b_agregar').style.display ="block";
				document.getElementById('resultados').style.display ="block";
				sendRequest(null,'/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do?origen=COT&idSeccionCot=' + rId,'resultados',null);
				processor.setUpdated(rId, false,'updated');
			}
		}
	});
	
	seccionesPorInciso.attachEvent("onCheck",function(rId,cInd,state){
		if (cInd == 1){
			cellInTurn = seccionesPorInciso.cellById(rId,1);
			sendRequest(null,'/MidasWeb/cotizacion/modificarSeccion.do?idSeccionCot=' + rId + "&contratada=" + state + "&obligatoriedad=" + cellInTurn.getValue(),null,'creaArbolOrdenesDeTrabajo('+idToCotizacion+')');
			processor.setUpdated(rId, false, 'updated');
			if(state == false) {
				document.getElementById('resultados').innerHTML = "";
				var sumaAsegurada = seccionesPorInciso.cellById(rId, 3);
				sumaAsegurada.setValue('0');
				var primaNeta = seccionesPorInciso.cellById(rId, 4);
				primaNeta.setValue('0');
			}
		}
	});
	
	seccionesPorInciso.enableDragAndDrop(false);	
	seccionesPorInciso.enableLightMouseNavigation(false);
	seccionesPorInciso.init();
	seccionesPorInciso.load('/MidasWeb/cotizacion/cargarSeccionesPorInciso.do?numeroInciso='+numeroInciso+'&idToCotizacion='+idToCotizacion, null, 'json');
	processor = new dataProcessor("/MidasWeb/cotizacion/modificarSeccion.do");
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");
	processor.init(seccionesPorInciso);	
}

function mostrarSeccionesPorIncisoEndoso(numeroInciso,idToCotizacion, tipoEndoso){
	seccionesPorInciso = new dhtmlXGridObject('seccionesPorIncisoGrid');									  
	seccionesPorInciso.setHeader("Secci\u00F3n,Contratada,Clave Obligatoriedad,Suma Asegurada,Prima Neta, Subincisos,puedeTenerSubIncisos");
	seccionesPorInciso.setColumnIds("seccion,claveContrato,claveObligatoriedad,sumaAsegurada,primaNeta,seleccionado,puedeTenerSubIncisos");
	seccionesPorInciso.setInitWidths("200,100,0,130,100,100,100");
	seccionesPorInciso.setColAlign("left,center,center,center,center,center,center");
	seccionesPorInciso.setColSorting("str,str,int,str,str,str,int");
	seccionesPorInciso.setColTypes("ro,ch,ro,edn,edn,ra,ro");
	seccionesPorInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesPorInciso.setColumnHidden(2, true);
	seccionesPorInciso.setColumnHidden(3, true);
	seccionesPorInciso.setColumnHidden(6, true);
	seccionesPorInciso.setNumberFormat("$0,000.00", 3);
	seccionesPorInciso.setNumberFormat("$0,000.00", 4);
	seccionesPorInciso.setSkin("light");
	seccionesPorInciso.attachEvent("onRowCreated", function(rowId, rowObj){
		var claveObligatoriedad = seccionesPorInciso.cellById(rowId, 2);
		if(claveObligatoriedad.getValue() == 3) {
			seccionesPorInciso.cellById(rowId, 1).setDisabled(true);
		}
		var puedeTenerSubIncisos = seccionesPorInciso.cellById(rowId, 6);
		if (puedeTenerSubIncisos.getValue() > 1){
			seccionesPorInciso.setCellExcellType(rowId,5,"ro");
			seccionesPorInciso.cellById(rowId, 5).setValue('');
		}
		seccionesPorInciso.cellById(rowId, 3).setDisabled(true);
		seccionesPorInciso.cellById(rowId, 4).setDisabled(true);
	});
	seccionesPorInciso.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = seccionesPorInciso.cellById(rId, 2);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 1) {
			var existeObligatoriaParcialContratada = false;
			seccionesPorInciso.forEachRow(function(id){
				var obligatoriedadCurrentRow = seccionesPorInciso.cellById(id, 2);
				var claveContrato = seccionesPorInciso.cellById(id, 1);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Secci\u00F3n no puede ser descontratada.\nDebe contratar al menos una Secci\u00F3n Obligatoria Parcial.");
				return false;
			}
		}
		return true;
	});

	seccionesPorInciso.attachEvent("onCellChanged",function(rId,cInd,nValue){
		var claveContrato = seccionesPorInciso.cellById(rId, 1);
		if(cInd == 5 && nValue== 1 && claveContrato.getValue() == '0') {
			cellInTurn = seccionesPorInciso.cellById(rId,5);
			document.getElementById('resultados').style.display ="none";
			document.getElementById('b_agregar').style.display ="none";
			processor.setUpdated(rId, false,'updated');
			return false;
		}
		cellInTurn = seccionesPorInciso.cellById(rId,6);
		if (cellInTurn.getValue() < 2){ 
			if (cInd == 5 && nValue== 1){
				cellInTurn = seccionesPorInciso.cellById(rId,1);
				ids=rId.split(',');
				$('idToCotizacion').value=ids[0];
				$('numeroInciso').value=ids[1];
				$('idToSeccion').value=ids[2];
				document.getElementById('b_agregar').style.display ="block";
				document.getElementById('resultados').style.display ="block";
				sendRequest(null,'/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do?origen=END&claveTipoEndoso=' + tipoEndoso + '&idSeccionCot=' + rId,'resultados',null);
				processor.setUpdated(rId, false,'updated');
			}
		}
	});
	
	seccionesPorInciso.attachEvent("onCheck",function(rId,cInd,state){
		if (cInd == 1){
			cellInTurn = seccionesPorInciso.cellById(rId,1);
			sendRequest(null,'/MidasWeb/cotizacion/modificarSeccion.do?idSeccionCot=' + rId + "&contratada=" + state + "&obligatoriedad=" + cellInTurn.getValue(),null,'creaArbolCotizacionEndoso('+idToCotizacion+','+tipoEndoso+')');
			processor.setUpdated(rId, false, 'updated');
			if(state == false) {
				document.getElementById('resultados').innerHTML = "";
				var sumaAsegurada = seccionesPorInciso.cellById(rId, 3);
				sumaAsegurada.setValue('0');
				var primaNeta = seccionesPorInciso.cellById(rId, 4);
				primaNeta.setValue('0');
			}
		}
	});
	
	seccionesPorInciso.enableDragAndDrop(false);	
	seccionesPorInciso.enableLightMouseNavigation(false);
	seccionesPorInciso.init();
	seccionesPorInciso.load('/MidasWeb/cotizacion/cargarSeccionesPorInciso.do?numeroInciso='+numeroInciso+'&idToCotizacion='+idToCotizacion, null, 'json');
	processor = new dataProcessor("/MidasWeb/cotizacion/modificarSeccion.do");
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");
	processor.init(seccionesPorInciso);	
}
function mostrarSeccionesPorIncisoCOT(numeroInciso,idToCotizacion){
	seccionesPorInciso = new dhtmlXGridObject('seccionesPorIncisoGrid');									  
	seccionesPorInciso.setHeader("Secci\u00F3n,Contratada,Clave Obligatoriedad,Suma Asegurada,Prima Neta, Subincisos,puedeTenerSubIncisos");
	seccionesPorInciso.setColumnIds("seccion,claveContrato,claveObligatoriedad,sumaAsegurada,primaNeta,seleccionado,puedeTenerSubIncisos");
	seccionesPorInciso.setInitWidths("200,100,0,130,100,100,100");
	seccionesPorInciso.setColAlign("left,center,center,center,center,center,center");
	seccionesPorInciso.setColSorting("str,str,int,str,str,str,int");
	seccionesPorInciso.setColTypes("ro,ch,ro,edn,edn,ra,ro");
	seccionesPorInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesPorInciso.setColumnHidden(2, true);
	seccionesPorInciso.setColumnHidden(3, true);
	seccionesPorInciso.setColumnHidden(6, true);
	seccionesPorInciso.setNumberFormat("$0,000.00", 3);
	seccionesPorInciso.setNumberFormat("$0,000.00", 4);
	seccionesPorInciso.setSkin("light");
	seccionesPorInciso.attachEvent("onRowCreated", function(rowId, rowObj){
		var claveObligatoriedad = seccionesPorInciso.cellById(rowId, 2);
		if(claveObligatoriedad.getValue() == 3) {
			seccionesPorInciso.cellById(rowId, 1).setDisabled(true);
		}
		var puedeTenerSubIncisos = seccionesPorInciso.cellById(rowId, 6);
		if (puedeTenerSubIncisos.getValue() > 1){
			seccionesPorInciso.setCellExcellType(rowId,5,"ro");
			seccionesPorInciso.cellById(rowId, 5).setValue('');
		}
		seccionesPorInciso.cellById(rowId, 3).setDisabled(true);
		seccionesPorInciso.cellById(rowId, 4).setDisabled(true);
	});
	seccionesPorInciso.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = seccionesPorInciso.cellById(rId, 2);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 1) {
			var existeObligatoriaParcialContratada = false;
			seccionesPorInciso.forEachRow(function(id){
				var obligatoriedadCurrentRow = seccionesPorInciso.cellById(id, 2);
				var claveContrato = seccionesPorInciso.cellById(id, 1);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Secci\u00F3n no puede ser descontratada.\nDebe contratar al menos una Secci\u00F3n Obligatoria Parcial.");
				return false;
			}
		}
		return true;
	});

	seccionesPorInciso.attachEvent("onCellChanged",function(rId,cInd,nValue){
		var claveContrato = seccionesPorInciso.cellById(rId, 1);
		if(cInd == 5 && nValue== 1 && claveContrato.getValue() == '0') {
			cellInTurn = seccionesPorInciso.cellById(rId,5);
			document.getElementById('resultados').style.display ="none";
			document.getElementById('b_agregar').style.display ="none";
			processor.setUpdated(rId, false,'updated');
			return false;
		}
		cellInTurn = seccionesPorInciso.cellById(rId,6);
		if (cellInTurn.getValue() < 2){ 
			if (cInd == 5 && nValue== 1){
				cellInTurn = seccionesPorInciso.cellById(rId,1);
				ids=rId.split(',');
				$('idToCotizacion').value=ids[0];
				$('numeroInciso').value=ids[1];
				$('idToSeccion').value=ids[2];
				document.getElementById('b_agregar').style.display ="block";
				document.getElementById('resultados').style.display ="block";
				sendRequest(null,'/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do?origen=ODT&idSeccionCot=' + rId,'resultados',null);
				processor.setUpdated(rId, false,'updated');
			}
		}
	});
	
	seccionesPorInciso.attachEvent("onCheck",function(rId,cInd,state){
		if (cInd == 1){
			cellInTurn = seccionesPorInciso.cellById(rId,1);
			sendRequest(null,'/MidasWeb/cotizacion/modificarSeccion.do?idSeccionCot=' + rId + "&contratada=" + state + "&obligatoriedad=" + cellInTurn.getValue(),null,'creaArbolCotizacion('+idToCotizacion+')');
			processor.setUpdated(rId, false, 'updated');
			if(state == false) {
				document.getElementById('resultados').innerHTML = "";
				var sumaAsegurada = seccionesPorInciso.cellById(rId, 3);
				sumaAsegurada.setValue('0');
				var primaNeta = seccionesPorInciso.cellById(rId, 4);
				primaNeta.setValue('0');
			}
		}
	});
	
	seccionesPorInciso.enableDragAndDrop(false);	
	seccionesPorInciso.enableLightMouseNavigation(false);
	seccionesPorInciso.init();
	seccionesPorInciso.load('/MidasWeb/cotizacion/cargarSeccionesPorInciso.do?numeroInciso='+numeroInciso+'&idToCotizacion='+idToCotizacion, null, 'json');
	processor = new dataProcessor("/MidasWeb/cotizacion/modificarSeccion.do");
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");
	processor.init(seccionesPorInciso);	
}

/* Fin del grid de Secciones en Da�os->Orden Trabajo->Administracion Secciones */

function mostrarAumentosProductoGrids(idProducto) {
	var asociados = new dhtmlXGridObject('aumentosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio,Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		aumentosProductoProcessor.setUpdated(rowId, true, "deleted");
		aumentosProductoProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/producto/mostrarAumentosAsociados.do?id='+ idProducto, null, 'json');

	aumentosProductoProcessor = new dataProcessor('/MidasWeb/configuracion/producto/guardarAumentoAsociado.do');
	aumentosProductoProcessor.enableDataNames(true);
	aumentosProductoProcessor.setTransactionMode("POST");
	aumentosProductoProcessor.setUpdateMode("off");
	aumentosProductoProcessor.setVerificator(6, function(value,colName){
		var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			aumentosProductoError = true;
			return false;
		}
	    if(val <= 0){
	    	aumentosProductoError = true;
	        return false;
	    } else {
	    	aumentosProductoError = false;
	        return true;
	    }
	});
	aumentosProductoProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject('aumentosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio,Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/producto/mostrarAumentosPorAsociar.do?id='+ idProducto, null, 'json');
}


function mostrarGridParticipacionFacultativo(id) {
	//cleanGridParticipacionFacultativo();	
	divFacultativo = document.getElementById("gridboxFacultativo");
	divFacultativo.style.display = "";
	divCorredor = document.getElementById("gridboxFacultativoCorredor");
	divCorredor.style.display = "none";
	divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
	divCorredorBoton.style.display = "none";
	facultativoCoberturaGrid = new dhtmlXGridObject('gridboxFacultativo');
	facultativoCoberturaGrid.setHeader("Reasegurador/Corredor,%Participaci&oacute;n,Cuenta Pesos,Cuenta D&oacute;lares,Contacto,%Comisi&oacute;n,,,");
	facultativoCoberturaGrid.setInitWidths("180,120,100,110,80,90,20,20,25");
	facultativoCoberturaGrid.setColumnIds("nombre,porcentajeParticipacion,cuentaBancoDTOByIdtccuentabancopesos,cuentaBancoDTOByIdtccuentabancodolares,contacto,comision,x,y,z");
	facultativoCoberturaGrid.setColAlign("center,left,center,center,center,center,center,center,center");
	facultativoCoberturaGrid.setColSorting("str,str,str,str,str,str,str,str,str");
	facultativoCoberturaGrid.setColTypes("ro,ro,ro,ro,ro,ro,img,img,img");
	facultativoCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	facultativoCoberturaGrid.setSkin("light"); 
	facultativoCoberturaGrid.enableDragAndDrop(false);
	facultativoCoberturaGrid.enableLightMouseNavigation(false);
 	facultativoCoberturaGrid.init(); 	
 	facultativoCoberturaGrid.load('/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacion.do?id='+ id, null, 'json');
}

var gridPlanPagosFacultativos;
function mostrarGridPlanPagosFacultativos(id) {	
	divGridPlanPagosFacultativos = document.getElementById("gridPlanPagosFacultativos");															
	divGridPlanPagosFacultativos.style.display = "";			
	gridPlanPagosFacultativos = new dhtmlXGridObject('gridPlanPagosFacultativos');
	gridPlanPagosFacultativos.setHeader("No. Exhibici&oacute;n,Fecha de pago,Monto a pagar");
	gridPlanPagosFacultativos.setInitWidths("110,110,120");
	gridPlanPagosFacultativos.setColumnIds("numeroExhibicion,fechaPago,montoAPagar");
	gridPlanPagosFacultativos.setColAlign("center,center,center");
	gridPlanPagosFacultativos.setColSorting("str,date,str");
	gridPlanPagosFacultativos.setColTypes("ro,dhxCalendar,edn");
	gridPlanPagosFacultativos.setDateFormat("%d/%m/%Y");
	gridPlanPagosFacultativos.setNumberFormat("$0,000.00", 2, ".", ",");
	gridPlanPagosFacultativos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	gridPlanPagosFacultativos.setSkin("light"); 
	gridPlanPagosFacultativos.enableDragAndDrop(false);
	gridPlanPagosFacultativos.enableLightMouseNavigation(false);
	gridPlanPagosFacultativos.enableAutoHeight(true);
	gridPlanPagosFacultativos.enableAutoWidth(true);
	gridPlanPagosFacultativos.init();
	gridPlanPagosFacultativos.load('/MidasWeb/contratofacultativo/pagocobertura/mostrarPlanPagosCobertura.do?id='+ id, null, 'json');		
}

function mostrarGridParticipacionFacultativoEndosoAnterior(id) {
    //cleanGridParticipacionFacultativo();
    divFacultativo = document.getElementById("gridboxFacultativoEndosoAnterior");
    divFacultativo.style.display = "";
    divCorredor = document.getElementById("gridboxFacultativoCorredorEndosoAnterior");
    divCorredor.style.display = "none";
    /*divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
    divCorredorBoton.style.display = "none";*/
    var porAsociar = new dhtmlXGridObject('gridboxFacultativoEndosoAnterior');
    porAsociar.setHeader("Reasegurador/Corredor,%Participaci&oacute;n,Cuenta Pesos,Cuenta D&oacute;lares,Contacto,%Comisi&oacute;n,");
    porAsociar.setInitWidths("180,110,100,110,80,90,20");
    porAsociar.setColumnIds("nombre,porcentajeParticipacion,cuentaBancoDTOByIdtccuentabancopesos,cuentaBancoDTOByIdtccuentabancodolares,contacto,comision,x");
    porAsociar.setColAlign("center,left,center,center,center,center,center");
    porAsociar.setColSorting("str,str,str,str,str,str,str");
    porAsociar.setColTypes("ro,ro,ro,ro,ro,ro,img");
    porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
    porAsociar.setSkin("light");
    porAsociar.enableDragAndDrop(false);
    porAsociar.enableLightMouseNavigation(false);
     porAsociar.init();
     porAsociar.load('/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacion.do?id='+ id+"&esEndoso=true", null, 'json');
}

function mostrarGridParticipacionFacultativoCorredor(id) {
	divCorredor = document.getElementById("gridboxFacultativoCorredor");
	divCorredor.style.display = "";
	divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
	divCorredorBoton.style.display = "";
 	divFacultativo = document.getElementById("gridboxFacultativo");
 	divFacultativo.style.display = "none";
  	var porAsociarCorredor = new dhtmlXGridObject('gridboxFacultativoCorredor');
	porAsociarCorredor.setHeader("Nombre,Nombre Corto,CNFS  ,Porcentaje Participaci&oacute;n,,");
	porAsociarCorredor.setInitWidths("210,100,80,180,90,*,*");
	porAsociarCorredor.setColumnIds("nombre,nombreCorto,CNFS,porcentajeParticipacion,x,y");
	porAsociarCorredor.setColAlign("center,center,center,center,center,center");
	porAsociarCorredor.setColSorting("str,str,str,str,str,str");
	porAsociarCorredor.setColTypes("ro,ro,ro,ro,img,img");
	porAsociarCorredor.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociarCorredor.setSkin("light"); 
	porAsociarCorredor.enableDragAndDrop(false);
	porAsociarCorredor.enableLightMouseNavigation(false);
	porAsociarCorredor.init();
	porAsociarCorredor.load('/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacionCorredor.do?id='+ id, null, 'json');
}

function mostrarGridParticipacionFacultativoCorredorEndosoAnterior(id) {
    divCorredor = document.getElementById("gridboxFacultativoCorredorEndosoAnterior");
    divCorredor.style.display = "";
    divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredorEndosoAnterior");
    divCorredorBoton.style.display = "";
     divFacultativo = document.getElementById("gridboxFacultativoEndosoAnterior");
     divFacultativo.style.display = "none";
      var porAsociarCorredor = new dhtmlXGridObject('gridboxFacultativoCorredorEndosoAnterior');
    porAsociarCorredor.setHeader("Nombre,Nombre Corto,CNFS  ,Porcentaje Participaci&oacute;n");
    porAsociarCorredor.setInitWidths("*,100,80,180");
    porAsociarCorredor.setColumnIds("nombre,nombreCorto,CNFS,porcentajeParticipacion");
    porAsociarCorredor.setColAlign("center,center,center,center");
    porAsociarCorredor.setColSorting("str,str,str,str");
    porAsociarCorredor.setColTypes("ro,ro,ro,ro");
    porAsociarCorredor.setImagePath("/MidasWeb/img/dhtmlxgrid/");
    porAsociarCorredor.setSkin("light");
    porAsociarCorredor.enableDragAndDrop(false);
    porAsociarCorredor.enableLightMouseNavigation(false);
    porAsociarCorredor.init();
    porAsociarCorredor.load('/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacionCorredor.do?id='+ id+"&esEndoso=true", null, 'json');
}

function mostrarGridAgregarFacultativo(){
	divFacultativo = document.getElementById("gridboxFacultativo");
	divFacultativo.style.display = "";
	divCorredor = document.getElementById("gridboxFacultativoCorredor");
	divCorredor.style.display = "none";
	divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
	divCorredorBoton.style.display = "none";	
}

function mostrarGridAgregarFacultativoEndosoAnterior(){
    divFacultativo = document.getElementById("gridboxFacultativoEndosoAnterior");
    divFacultativo.style.display = "";
    divCorredor = document.getElementById("gridboxFacultativoCorredorEndosoAnterior");
    divCorredor.style.display = "none";
    divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredorEndosoAnterior");
    divCorredorBoton.style.display = "none";   
}


function cleanGridParticipacionFacultativo(){
	porAsociar.destructor();
}







function mostrarRecargosProductoGrids(idProducto) {
	var asociados = new dhtmlXGridObject('recargosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");	
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		recargosProductoProcessor.setUpdated(rowId, true, "deleted");
		recargosProductoProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/producto/mostrarRecargosAsociados.do?id='+ idProducto, null, 'json');

	recargosProductoProcessor = new dataProcessor('/MidasWeb/configuracion/producto/guardarRecargoAsociado.do');
	recargosProductoProcessor.enableDataNames(true);
	recargosProductoProcessor.setTransactionMode("POST");
	recargosProductoProcessor.setUpdateMode("off");
	recargosProductoProcessor.setVerificator(6, function(value,colName){
		var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			recargosProductoError = true;
			return false;
		}
	    if(val <= 0){
	    	recargosProductoError = true;
	        return false;
	    } else {
	    	recargosProductoError = false;
	        return true;
	    }
	});
	recargosProductoProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject('recargosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/producto/mostrarRecargosPorAsociar.do?id='+ idProducto, null, 'json');
}

function mostrarDescuentosProductoGrids(idProducto) {
	var asociados = new dhtmlXGridObject('descuentosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		descuentosProductoProcessor.setUpdated(rowId, true, "deleted");
		descuentosProductoProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/producto/mostrarDescuentosAsociados.do?id='+ idProducto, null, 'json');

	descuentosProductoProcessor = new dataProcessor('/MidasWeb/configuracion/producto/guardarDescuentoAsociado.do');
	descuentosProductoProcessor.enableDataNames(true);
	descuentosProductoProcessor.setTransactionMode("POST");
	descuentosProductoProcessor.setUpdateMode("off");
	descuentosProductoProcessor.setVerificator(6, function(value,rowId,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			descuentosProductoError = true;
			return false;
		}
	    if(val <= 0){
	    	descuentosProductoError = true;
	        return false;
	    } else {
	    	descuentosProductoError = false;
	        return true;
	    }*/
		if(value > 0) {
			var tipo = asociados.cellById(rowId, 5);
			if(tipo.getValue() == '%') {
				if(value > 100) {
					descuentosProductoError = true;
			        return false;
				}
			}
			descuentosProductoError = false;
	        return true;
	    } else {
	    	descuentosProductoError = true;
	        return false;
	    }
	});
	descuentosProductoProcessor.init(asociados);
	
	var porAsociar = new dhtmlXGridObject('descuentosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/producto/mostrarDescuentosPorAsociar.do?id='+ idProducto, null, 'json');
}

function mostrarMonedaProductoGrids(idPadre){
	var monedasAsociadas = new dhtmlXGridObject('monedasAsociadasGrid');
	monedasAsociadas.setHeader("idPadre, Descripci\u00F3n");
	monedasAsociadas.setColumnIds("idPadre,descripcion");
	monedasAsociadas.setInitWidths("1,200");
	monedasAsociadas.setColAlign("center,left");
	monedasAsociadas.setColSorting("int,str");
	monedasAsociadas.setColTypes("ro,ro");
	monedasAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	monedasAsociadas.setSkin("light");
	monedasAsociadas.setColumnHidden(0, true);
	monedasAsociadas.enableDragAndDrop(true);
	monedasAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		monedaProductoProcessor.setUpdated(rowId, true, "deleted");
		monedaProductoProcessor.sendData(rowId);
	});
	monedasAsociadas.enableLightMouseNavigation(true);
	monedasAsociadas.init();
	monedasAsociadas.load('/MidasWeb/configuracion/producto/mostrarMonedasAsociadas.do?id='+ idPadre, null, 'json');

	monedaProductoProcessor = new dataProcessor("/MidasWeb/configuracion/producto/guardarMonedaAsociada.do");
	monedaProductoProcessor.enableDataNames(true);
	monedaProductoProcessor.setTransactionMode("POST");
	monedaProductoProcessor.setUpdateMode("off");
	monedaProductoProcessor.init(monedasAsociadas);

	monedasNoAsociadas = new dhtmlXGridObject('monedasPorAsociarGrid');
	monedasNoAsociadas.setHeader("idPadre, Descripci\u00F3n");
	monedasNoAsociadas.setInitWidths("1,200");
	monedasNoAsociadas.setColAlign("center,left");
	monedasNoAsociadas.setColSorting("int,str");
	monedasNoAsociadas.setColTypes("ro,ro");
	monedasNoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	monedasNoAsociadas.setSkin("light");
	monedasNoAsociadas.setColumnHidden(0, true);
	monedasNoAsociadas.enableDragAndDrop(true);
	monedasNoAsociadas.enableLightMouseNavigation(true);
	monedasNoAsociadas.init();
	monedasNoAsociadas.load('/MidasWeb/configuracion/producto/mostrarMonedasPorAsociar.do?id='+ idPadre, null, 'json');
}

function mostrarRamosProductoGrid(prod) {
	var ramosAsociados = new dhtmlXGridObject('ramosAsociadosGrid');
	ramosAsociados.setHeader("C\u00F3digo, Descripci\u00F3n Ramo,Obligatorio");
	ramosAsociados.setColumnIds("codigo,descripcion,obligatorio");
	ramosAsociados.setInitWidths("80,200,1");
	ramosAsociados.setColAlign("left,left,left");
	ramosAsociados.setColSorting("int,str,str");
	ramosAsociados.setColTypes("ro,ro,ro");
	ramosAsociados.setColumnHidden(2, true);
	ramosAsociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosAsociados.setSkin("light");
	ramosAsociados.enableDragAndDrop(true);
	ramosAsociados.attachEvent("onBeforeDrag",function(IdSource){
		if (ramosAsociados.cells(IdSource,2).getValue() == "true"){
			document.getElementById('mensaje').style.visibility = 'visible';
			document.getElementById('mensaje').style.visibility = 'visible';
			return false;
		}
		else{
			document.getElementById('mensaje').style.visibility = 'hidden';
			document.getElementById('mensaje').style.visibility = 'hidden';
			return true;
		}
	});
	ramosAsociados.init();
	ramosAsociados.load('/MidasWeb/configuracion/producto/mostrarRamosAsociados.do?id='+ prod, null, 'json');
	
	ramosProductoProcessor = new dataProcessor("/MidasWeb/configuracion/producto/guardarRamoAsociado.do?id="+ prod);
	ramosProductoProcessor.enableDataNames(true);
	ramosProductoProcessor.setTransactionMode("POST");
	ramosProductoProcessor.setUpdateMode("off");
	ramosProductoProcessor.init(ramosAsociados);
	
	var ramosPorAsociar = new dhtmlXGridObject('ramosPorAsociarGrid');
	ramosPorAsociar.setHeader("C\u00F3digo, Descripci\u00F3n Ramo,Obligatorio");
	ramosPorAsociar.setInitWidths("80,200,1,obligatorio");
	ramosPorAsociar.setColAlign("left,left,left");
	ramosPorAsociar.setColSorting("int,str,str");
	ramosPorAsociar.setColTypes("ro,ro,ro");
	ramosPorAsociar.setColumnHidden(2, true);
	ramosPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosPorAsociar.setSkin("light");
	ramosPorAsociar.enableDragAndDrop(true);
	ramosPorAsociar.init();
	ramosPorAsociar.load('/MidasWeb/configuracion/producto/mostrarRamosPorAsociar.do?id='+ prod, null, 'json');
}

/**  Crear los grids de tipos de cedula  */

function mostrarRecargosCP(idCobertura,idSeccion){
	document.getElementById("idToSeccion").value=idSeccion;
	document.getElementById("idToCobertura").value=idCobertura;
	documentos= new dhtmlXGridObject('recargosCpGrid');
	documentos.setHeader("Codigo Postal, Tipo valor, Valor, Fecha Vencimiento");
	documentos.setColumnIds("id1,id2,id3,id4");
	documentos.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter");
	documentos.setInitWidths("70,100,100,158");
	documentos.setColAlign("right,left,left,left");
	documentos.setColTypes("dyn,ed,ed,ed");
	documentos.setColSorting("str,str,str");
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");
	documentos.enableDragAndDrop(true);
	documentos.load('/MidasWeb/configuracion/seccion/mostrarGridRecargoCP.do?idCobertura='+idCobertura+'&idSeccion='+idSeccion, null, 'json');
	documentos.init();
	documentos.attachEvent("onEnter", function(id,ind){
		var bandera=0;
	    if (documentos.cells(id,1).getValue().toUpperCase()=="MONTO" || documentos.cells(id,1).getValue().toUpperCase()=="PORCENTAJE"){
	    	bandera=0;
	    	if (documentos.cells(id,1).getValue().toUpperCase()=="PORCENTAJE" && documentos.cells(id,2).getValue()>=-100&& documentos.cells(id,2).getValue()<=100)
	    		bandera=0;
	    	else {
	    		if (documentos.cells(id,1).getValue().toUpperCase()=="MONTO" && !isNaN(documentos.cells(id,2).getValue())){
	    			bandera=0;
	    		}else{
	    		bandera=1;
	    		alert("Rango invalido");
	    		}
	    	}
	    }else{
	    	alert("Valor no v\u00E1lido: '"+documentos.cells(id,1).getValue()+"' el registro no se guardar\u00E1");
	    	bandera=1;
	    }
	    
	    if (bandera==0){	
		//alert("3:/");
		var cadena= id+","+documentos.cells(id,0).getValue()+","+documentos.cells(id,1).getValue()+","+documentos.cells(id,2).getValue()+","+documentos.cells(id,3).getValue();
		//alert(id+","+cadena);
	    jQuery.ajax({
	        type: "POST",
	        url:"/MidasWeb/componente/vehiculo/guardaElementoGrid.do",
	        async: true,
	        data:
	            {
	        	string:     cadena
	            },
	        dataType: "text",
	        success: function (response){
	        	//if ( response ==0 )
	        		//{
	        		alert(response);
	        		mostrarRecargosCP(idCobertura,idSeccion);
	        		
	        	//}
	        }           
	      });
		}
		
		
	});
}

function mostrarAdjuntarArchivoExcelSoporteWindow(){
	idSeccion=document.getElementById("idToSeccion").value;
	idCobertura=document.getElementById("idToCobertura").value;
	if(dhxWins != null) {
        dhxWins.unload();
    }
    dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
    var acercaDeWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
    acercaDeWindow.setText("Carga Excel de Soporte");
    acercaDeWindow.button("minmax1").hide();
    acercaDeWindow.button("park").hide();
    acercaDeWindow.setModal(true);
    acercaDeWindow.center();
    acercaDeWindow.denyResize();
    acercaDeWindow.attachHTMLString("<div id='vault'></div>");

    var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
vault.setServerHandlers("/MidasWeb/configuracion/seccion/uploadHandlerExcelldeSoporte.do",
                       "/MidasWeb/configuracion/seccion/getInfoHandler.do",
                      "/MidasWeb/configuracion/seccion/getIdHandler.do");
	vault.setFilesLimit(1);
    vault.onAddFile = function(fileName) {
    var ext = this.getFileExtension(fileName);
        if (ext == "xlsx" || ext == "xls" ) {
            vault.onUploadComplete = function(files) {
                    var s="";
                    for (var i=0; i<files.length; i++) {
                        var file = files[i];
                        s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
                        
                    }
                    mostrarRecargosCP(idCobertura,idSeccion);
                    alert("El archivo se ha cargado");
                    dhxWins.window("adjuntarArchivoSolicitud").close();
            };
            return true;
        }
            else {
                alert("tiene que ser un documento de Excell");
                    return false;
                }
    };
   
   vault.create("vault");  
   vault.setFormField("idSeccion", idSeccion);
   vault.setFormField("idCobertura", idCobertura);

}

function downloadIt()
{

    window.open("/MidasWeb/configuracion/seccion/downloadIt.do", "Descargar plantilla", "width=300, height=200");
	
}


function mostrarTipoDeCedulaGrids(idSeccion) {
	document.getElementById("idSeccion").setAttribute("value", idSeccion);
	
	cedulasAsociadas = new dhtmlXGridObject('cedulasAsociadasGrid');
	cedulasAsociadas.setHeader("");
	cedulasAsociadas.setInitWidths("478");
	cedulasAsociadas.setColAlign("left");
	cedulasAsociadas.setColSorting("str");
	cedulasAsociadas.setColTypes("ro");
	cedulasAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cedulasAsociadas.setSkin("light");
	cedulasAsociadas.enableDragAndDrop(true);
	cedulasAsociadas.init();
	cedulasAsociadas.load('/MidasWeb/configuracion/seccion/mostrarCedulasAsociadas.do?id='+ idSeccion, null, 'json');
	
	cedulasPorAsociar = new dhtmlXGridObject('cedulasPorAsociarGrid');
	cedulasPorAsociar.setHeader("");
	cedulasPorAsociar.setInitWidths("478");
	cedulasPorAsociar.setColAlign("left");
	cedulasPorAsociar.setColSorting("str");
	cedulasPorAsociar.setColTypes("ro");
	cedulasPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cedulasPorAsociar.setSkin("light");
	cedulasPorAsociar.enableDragAndDrop(true);
	cedulasPorAsociar.init();
	cedulasPorAsociar.load('/MidasWeb/configuracion/seccion/mostrarCedulasNoAsociadasSeccion.do?id='+ idSeccion, null, 'json');
}

function guardarTipoDeCedulaGrids() {
	var url = "/MidasWeb/configuracion/seccion/guardarCedulasAsociadasPorSeccion.do";
	jQuery.ajax({
		type: "POST",
		url: url,
		async: true,
		data: {
			idsCedulas : cedulasAsociadas.getAllRowIds(","),
            idSeccion : document.getElementById("idSeccion").value
		},
		dataType: "text",
		success: function (response) {
			if (response == 1)
                alert("Se han guardado exitosamente las cédulas");
            else
                alert("Ha ocurrido un error");
		}
	});
}

function guardarAsosiacionRamosProducto(){
	ramosProductoProcessor.sendData();
	//configuracionProductoTabBar.setTabActive('detalle');
	mostrarMensajeExitoYCambiarTab(configuracionProductoTabBar, 'detalle');
}

/**
 * Funcion para mostrar los documentos Anexos en el cat�logo de producto
 */
var documentoAnexoProductoProcessor;
var documentoAnexoProductoSecuenciaError=false;
var documentoAnexoProductoDescripcionError=false;
var documentos;
var productoid=null;
function mostrarDocumentosProductoGrid(prod) {
	documentos= new dhtmlXGridObject('documentosAnexosGrid');
	documentos.setHeader("idToProducto,Archivo, Obligatorio, # Secuencia, Descripci\u00F3n, Descargar");
	documentos.setColumnIds("idToProducto,nombreArchivo,claveObligatoriedad,numeroSecuencia,descripcionDocumento,descargar");
	documentos.setInitWidths("2,100,100,120,290,100");
	documentos.setColAlign("left,left,center,left,left,center");
	documentos.setColTypes("ro,ro,coro,ed,ed,img");
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");
	var combo = documentos.getCombo(2);
	combo.put("0","OPCIONAL");
	combo.put("3","OBLIGATORIO");
	combo.save();
	documentos.setColumnHidden(0, true);
	documentos.attachEvent("onRowCreated",function(rowId) {
		var secuencia = documentos.cellById(rowId, 3);
		if(secuencia.getValue() == "0"){
			documentoAnexoProductoProcessor.setUpdated(rowId, true, "updated");
		}
	});
	documentos.init();
	if (prod!=undefined)	productoid=prod;
	else	prod=productoid;
	documentos.load('/MidasWeb/catalogos/producto/mostrarDocumentosAnexos.do?id='+ prod, null, 'json');
	
	documentoAnexoProductoProcessor = new dataProcessor("/MidasWeb/catalogos/producto/guardarDocumentoAnexo.do");
	documentoAnexoProductoProcessor.enableDataNames(true);
	documentoAnexoProductoProcessor.setTransactionMode("POST");
	documentoAnexoProductoProcessor.setUpdateMode("off");
	documentoAnexoProductoProcessor.setVerificator(3, function(value,colName){
		/*var val = parseInt(value.toString()._dhx_trim());
		if(isNaN(val)) {
			documentoAnexoProductoSecuenciaError = true;
			return false;
		}
	    if(val <= 0){
	    	documentoAnexoProductoSecuenciaError = true;
	        return false;
	    } else {
	    	documentoAnexoProductoSecuenciaError = false;
	        return true;
	    }*/
		if(value > 0){
	    	documentoAnexoProductoSecuenciaError = false;
	        return true;
	    } else {
	    	documentoAnexoProductoSecuenciaError = true;
	        return false;
	    }
	});
	documentoAnexoProductoProcessor.setVerificator(4, function(value,colName){
		var val = value.toString()._dhx_trim();
		if(documentoAnexoProductoSecuenciaError == false){
		if(val.length==0) {
			//documentoAnexoProductoDescripcionError = true;
			documentoAnexoProductoSecuenciaError = true;
			return false;
		}
	    else {
	    	//documentoAnexoProductoDescripcionError = false;
	    	documentoAnexoProductoSecuenciaError = false;
	        return true;
	    }
		}
	});
	
	documentoAnexoProductoProcessor.init(documentos);
}

/**
 * Funciones para mostrar los documentos Anexos en la configuraci�n de TipoPoliza
 */
var documentoAnexoTipoPolizaProcessor;
var documentoAnexoTipoPolizaError=false;
var polizaid=null;
function mostrarDocumentosTipoPolizaGrid(poliza) {
	documentos= new dhtmlXGridObject('documentosAnexosGrid');
	documentos.setHeader("idToTipoPoliza,Archivo, Obligatorio, # Secuencia, Descripci\u00f3n, Descargar");
	documentos.setColumnIds("idToTipoPoliza,nombreArchivo,claveObligatoriedad,numeroSecuencia,descripcionDocumento,descargar");
	documentos.setInitWidths("2,100,100,120,290,100");
	documentos.setColAlign("left,left,center,left,left,center");
	documentos.setColTypes("ro,ro,coro,ed,ed,img");
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");
	var combo = documentos.getCombo(2);
	combo.put("0","OPCIONAL");
	combo.put("3","OBLIGATORIO");
	combo.save();
	documentos.setColumnHidden(0, true);
	documentos.attachEvent("onRowCreated",function(rowId) {
		var secuencia = documentos.cellById(rowId, 3);
		if(secuencia.getValue() == "0"){
			documentoAnexoTipoPolizaProcessor.setUpdated(rowId, true, "updated");
		}
	});
	documentos.init();
	if (poliza!=undefined)	polizaid=poliza;
	else	poliza=polizaid;
	documentos.load('/MidasWeb/configuracion/tipopoliza/mostrarDocumentosAnexos.do?id='+ poliza, null, 'json');
	
	documentoAnexoTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarDocumentoAnexo.do");
	documentoAnexoTipoPolizaProcessor.enableDataNames(true);
	documentoAnexoTipoPolizaProcessor.setTransactionMode("POST");
	documentoAnexoTipoPolizaProcessor.setUpdateMode("off");
	documentoAnexoTipoPolizaProcessor.setVerificator(3, checkIfInteger);
	documentoAnexoTipoPolizaProcessor.setVerificator(4, chackIfString);
	documentoAnexoTipoPolizaProcessor.init(documentos);
}
function chackIfString(value,colName){
	var val = value.toString()._dhx_trim();
	if(val.length==0) {
		documentoAnexoTipoPolizaError = true;
		return false;
	}
    else {
    	documentoAnexoTipoPolizaError = false;
        return true;
    }
}
function checkIfInteger(value,colName){
	var val = parseInt(value.toString()._dhx_trim());
	if(isNaN(val)) {
		documentoAnexoTipoPolizaError = true;
		return false;
	}
    if(val <= 0){
    	documentoAnexoTipoPolizaError = true;
        return false;
    } else {
    	documentoAnexoTipoPolizaError = false;
        return true;
    }
}
/**
 * Fin de Funciones para mostrar los documentos Anexos en la configuraci�n de TipoPoliza
 */

/**
 * Funciones para mostrar los documentos Anexos de CG en la configuración de Sección
 */
var documentoAnexoCgProcessor;
var documentoAnexoCgError=false;
var seccionid=null;
function mostrarDocumentosCgGrid(seccion) {
	documentos= new dhtmlXGridObject('documentosAnexosCGGrid');
	documentos.setHeader("id,Nombre, No. de Registro, Fecha Inicio, Fecha Fin, Activo, Portal, ED, Descargar");
	documentos.setColumnIds("id,nombre,numero_registro,fecha_inicio,fecha_fin,activo,portal,ed,descargar");

	documentos.setInitWidths("2,113,120,80,80,50,50,40,70");
	documentos.setColAlign("left,left,center,center,center,center,center,center,center");
	documentos.setColSorting("int,str,str,str,str,na,na,na,na");
	documentos.setColTypes("ro,ro,ed,ed,ed,ch,ch,ch,img");
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");

	documentos.setColumnHidden(0, true);

	documentos.attachEvent("onRowCreated",function(rowId) {
		var num_registro = documentos.cellById(rowId, 2);
		var fec_inicio = documentos.cellById(rowId, 3);
		var fec_fin = documentos.cellById(rowId, 4);
		if(num_registro.getValue() == ""){ //busco por "", y no por "default" por ya en el grid
			documentoAnexoCgProcessor.setUpdated(rowId, true, "updated");
		}
	});

	documentos.init();

	if (seccion!=undefined)
		seccionid=seccion;
	else
		seccion=seccionid;

	documentos.load('/MidasWeb/configuracion/seccion/mostrarDocumentosCondiciones.do?id='+ seccion, null, 'json');
	documentoAnexoCgProcessor = new dataProcessor("/MidasWeb/configuracion/seccion/guardarDocumentoCondiciones.do");
	documentoAnexoCgProcessor.enableDataNames(true);
	documentoAnexoCgProcessor.setTransactionMode("POST");
	documentoAnexoCgProcessor.setUpdateMode("off");
	documentoAnexoCgProcessor.setVerificator(2, function(value,colName){
		var val = value.toString()._dhx_trim();
		if(val.length==0) {
			documentoAnexoCgError = true;
			return false;
		} else {
		   	documentoAnexoCgError = false;
       		return true;
    	}
	});
	var fechaInicio="";
	var fechaFin="";
	documentoAnexoCgProcessor.setVerificator(3, function(value,colName){
		var val = value.toString()._dhx_trim();
		if(documentoAnexoCgError == false){
			if(val.length==0) {
				documentoAnexoCgError = true;
				return false;
			} else {
				documentoAnexoCgError = validaFormatoFecha(val,value);
				if(documentoAnexoCgError == false){
					fechaInicio = new Date(val);
					return true;
				}else
					return false;
	    	}
		}
	});
	documentoAnexoCgProcessor.setVerificator(4, function(value,colName){
		var val = value.toString()._dhx_trim();
		if(documentoAnexoCgError == false){
			if(val.length==0) {
				documentoAnexoCgError = false;
				return true;
			} else {
				documentoAnexoCgError = validaFormatoFecha(val,value);
				if(documentoAnexoCgError == false){
					fechaFin = new Date(val);
					if(fechaFin < fechaInicio){
						mostrarVentanaMensaje("10","¡ Fecha Fin debe ser Mayor a Fecha Inicio, Actualize Fecha Fin !");
						return false;
					}else
						return true;
				}else
					return false;
	    	}
		}
	});
	// FechaFin y StatusActivo no se validan
	documentoAnexoCgProcessor.init(documentos);
}


/**
 * Se valida el Formato de Fecha,
 * Devuelve true/false segun errores en la fecha, por si presenta alguno(true), o si es correcta(false) 
 */
function validaFormatoFecha(val,value){
    formatoFecha = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;			    
    if(formatoFecha.test(val)) {
		if(regs = value.match(formatoFecha)) {
			if(regs[1] < 1 || regs[1] > 31) {
	   			mostrarVentanaMensaje("10","¡ Valor [" + regs[1] + "] para Dia No V\u00e1lido !");
				return true;
			}
			if(regs[2] < 1 || regs[2] > 12) {
				mostrarVentanaMensaje("10","¡ Valor [" + regs[2] + "] para Mes No V\u00e1lido !");
				return true;
			}
			if(regs[3] < 1902 || regs[3] > 2099) {
				mostrarVentanaMensaje("10","¡ Valor [" + regs[3] + "] para A\u00f1o No V\u00e1lido !, Se espera un valor entre [1902-2099].");
				return true;
			}
		}
		return false;
	} else {
		mostrarVentanaMensaje("10","¡ Valor ["+ val +"] para Fecha No V\u00e1lido !, Se espera en el Formato [DD/MM/YYYY].");
		return true;
	}
}

/**
 * Descarga documentos de Fortimax / Condiciones Generales
 */
function descargarDocumentoFortimax(fortimaxIDFile) {
	window.open('/MidasWeb/cgcondiciones/descargarArchivoFortimax.do?fortimaxIDFile=' + fortimaxIDFile, 'download');
}
/**
 * Fin de Funciones para mostrar los documentos Anexos de CG en la configuración de Seccion
 */

/**
 * Funcion para mostrar los documentos Anexos en el cat�logo de cobertura
 */
var documentoAnexoCoberturaProcessor;
var documentoAnexoCoberturaSecuenciaError=false;
var documentoAnexoCoberturaDescripcionError=false;
var documentos;
var coberturaid=null;
function mostrarDocumentosCoberturaGrid(cobertura) {
	documentos= new dhtmlXGridObject('documentosAnexosGrid');
	documentos.setHeader("idToCobertura,Archivo, Obligatorio, # Secuencia, Descripci\u00F3n, Descargar");
	documentos.setColumnIds("idToCobertura,nombreArchivo,claveObligatoriedad,numeroSecuencia,descripcionDocumento,descargar");
	documentos.setInitWidths("2,100,100,120,290,100");
	documentos.setColAlign("left,left,center,left,left,center");
	documentos.setColTypes("ro,ro,coro,ed,ed,img");
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");
	var combo = documentos.getCombo(2);
	combo.put("0","OPCIONAL");
	combo.put("1","OPCIONAL DEFAULT");
	combo.put("3","OBLIGATORIO");
	combo.save();
	documentos.setColumnHidden(0, true);
	documentos.attachEvent("onRowCreated",function(rowId) {
		var secuencia = documentos.cellById(rowId, 3);
		if(secuencia.getValue() == "0"){
			documentoAnexoCoberturaProcessor.setUpdated(rowId, true, "updated");
		}
	});
	documentos.init();
	if (cobertura!=undefined)	coberturaid=cobertura;
	else	cobertura=coberturaid;
	documentos.load('/MidasWeb/catalogos/cobertura/mostrarDocumentosAnexos.do?id='+ cobertura, null, 'json');
	
	documentoAnexoCoberturaProcessor = new dataProcessor("/MidasWeb/catalogos/cobertura/guardarDocumentoAnexo.do");
	documentoAnexoCoberturaProcessor.enableDataNames(true);
	documentoAnexoCoberturaProcessor.setTransactionMode("POST");
	documentoAnexoCoberturaProcessor.setUpdateMode("off");
	documentoAnexoCoberturaProcessor.setVerificator(3, function(value,colName){
		var val = parseInt(value.toString()._dhx_trim());
		if(isNaN(val)) {
			documentoAnexoCoberturaSecuenciaError = true;
			return false;
		}
	    if(val <= 0){
	    	documentoAnexoCoberturaSecuenciaError = true;
	        return false;
	    } else {
	    	documentoAnexoCoberturaSecuenciaError = false;
	        return true;
	    }
	});
	documentoAnexoCoberturaProcessor.setVerificator(4, function(value,colName){
		var val = value.toString()._dhx_trim();
		if(val.length==0) {
			documentoAnexoCoberturaDescripcionError = true;
			return false;
		}
	    else {
	    	documentoAnexoCoberturaDescripcionError = false;
	        return true;
	    }
	});
	documentoAnexoCoberturaProcessor.init(documentos);
}

/**
 * Funciones para mostrar las comisiones por cotizacion
 */
var comisionProcessor;
var comisionCotizacionError=false;
var comisiones;
var autorizarComisionProcessor;
var rechazarComisionProcessor;

function mostrarAutorizarComisionesPorCotizacionGrid(idCotizacion,tipoEdicion) {
	if (tipoEdicion != undefined && tipoEdicion != null){
		comisiones= new dhtmlXGridObject('autorizarComisionesCotizacionGrid');
		comisiones.setHeader("Ramo,SubRamo,Tipo %,Comisi\u00F3n General, Comisi\u00F3n de la cotizaci\u00f3n,Comisi\u00F3n, Comisi\u00F3n Cedida,#cspan,#cspan");
		comisiones.setColumnIds("descripcionRamo,descripcionSubRamo,tipoPorcentaje,comisionDefault,comisionCotizacion,comision,comisionCedida,estado,autorizacion");
		comisiones.setInitWidths("100,160,40,140,125,100,100,20,20");
		comisiones.setColAlign("left,left,center,center,center,center,center,center,center");
		comisiones.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		comisiones.setSkin("light");
		comisiones.enableMultiline(true);		
		comisiones.enableResizing("false,false,false,false,false,false,false,false,false");
		if (tipoEdicion == 'autorizacion'){
			comisiones.setColSorting("str,str,str,int,int,str,str,str,int");
			comisiones.setColTypes("ro,ro,ro,ro,ro,ro,ro,img,ch");					
			comisiones.attachEvent("onEditCell",seleccionarComision);
			comisiones.init();
			comisiones.load("/MidasWeb/cotizacion/comision/mostrarComisionesCotizacion.do?id="+ idCotizacion+"&tipoEdicion="+tipoEdicion, null, 'json');
			//Procesor para autorizar la comision
			autorizarComisionProcessor = new dataProcessor("/MidasWeb/cotizacion/comision/autorizarComisionCotizacion.do");
			autorizarComisionProcessor.enableDataNames(true);
			autorizarComisionProcessor.setTransactionMode("POST");
			autorizarComisionProcessor.setUpdateMode("off");
			autorizarComisionProcessor.init(comisiones);
			//Procesor para rechazar la comision
			rechazarComisionProcessor = new dataProcessor("/MidasWeb/cotizacion/comision/rechazarComisionCotizacion.do");
			rechazarComisionProcessor.enableDataNames(true);
			rechazarComisionProcessor.setTransactionMode("POST");
			rechazarComisionProcessor.setUpdateMode("off");
			rechazarComisionProcessor.attachEvent("onAfterUpdateFinish",function (){
				var cotizacion = document.getElementById("idToCotizacion");
				if (cotizacion != null && cotizacion != undefined){
					mostrarAutorizarComisionesPorCotizacionGrid(cotizacion.value,'autorizacion');
				}});
			rechazarComisionProcessor.init(comisiones);
		}
	}
}

//Funci�n invocada para mostrar el grid de editar comisiones de una cotizaci�n
function mostrarEdicionComisionesPorCotizacionGrid(idCotizacion,tipoEdicion) {
	if (tipoEdicion != undefined && tipoEdicion != null){
		comisiones= new dhtmlXGridObject('editarComisionesCotizacionGrid');
		comisiones.setHeader("Ramo,SubRamo,Tipo %,Comisi\u00F3n General, Comisi\u00F3n de la cotizaci\u00F3n,Comisi\u00F3n, Comisi\u00F3n Cedida, Comisi\u00F3n del Per\u00EDodo, Comisi\u00F3n RPF,#cspan,#cspan");
		comisiones.setColumnIds("descripcionRamo,descripcionSubRamo,tipoPorcentaje,comisionDefault,comisionCotizacion,comision,comisionCedida,comisionPorDevengar,comisionRPF,estado,autorizacion");
		comisiones.setInitWidths("100,160,40,140,125,100,100,100,100,20,20");
		comisiones.setColAlign("left,left,center,center,center,center,center,center,center,center,center");
		
		//comisiones.setHeader("Ramo,SubRamo,Tipo %,Comision General, Comision de la cotizacion,#cspan,#cspan");
		//comisiones.setColumnIds("descripcionRamo,descripcionSubRamo,tipoPorcentaje,comisionDefault,comisionCotizacion,estado,autorizacion");
		//comisiones.setInitWidths("100,160,40,140,125,20,20");
		//comisiones.setColAlign("left,left,center,center,center,center,center");
		comisiones.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		comisiones.setSkin("light");
		comisiones.enableMultiline(true);
		//comisiones.enableResizing("false,false,false,false,false,false,false");
		comisiones.enableResizing("false,false,false,false,false,false,false,false,false,false,false");
		if (tipoEdicion == 'comision'){
			//comisiones.setColSorting("str,str,str,int,int,str,str");
			//comisiones.setColTypes("ro,ro,ro,ro,ed,img,img");
			comisiones.setColSorting("str,str,str,int,int,str,str,str,str,str,str");
			comisiones.setColTypes("ro,ro,ro,ro,ed,ro,ro,ro,ro,img,img");		
			comisiones.setColumnHidden(9, true);
			comisiones.attachEvent("onEditCell",validarAutorizacion);
			comisiones.init();
			comisiones.load("/MidasWeb/cotizacion/comision/mostrarComisionesCotizacion.do?id="+ idCotizacion+"&tipoEdicion="+tipoEdicion, null, 'json');
			
			//Processor para guardar el porcentaje de la comision
			comisionProcessor = new dataProcessor("/MidasWeb/cotizacion/comision/guardarComisionCotizacion.do");
			comisionProcessor.enableDataNames(true);
			comisionProcessor.setTransactionMode("POST");
			comisionProcessor.setUpdateMode("off");
			comisionProcessor.setVerificator(4, function(value,colName){
				var val = parseFloat(value.toString()._dhx_trim());
				if(isNaN(val)) {
					documentoAnexoProductoError = true;
					return false;}
			    if(val <= 0){
			    	documentoAnexoProductoError = true;
			        return false;
			    } else {
			    	documentoAnexoProductoError = false;
			        return true;}
			});
			comisionProcessor.init(comisiones);
		}
	}
}

function seleccionarComision(stage,rowId,cellIndex,newValue,oldValue){
	if (stage==1)
		if(!comisiones.cells(rowId,cellIndex).isChecked()){
			autorizarComisionProcessor.setUpdated(rowId,false,"update");
			rechazarComisionProcessor.setUpdated(rowId,false,"update");
			comisiones.setUpdated(rowId,false,"update");
		}
	return true;
}

function autorizarComisionesSeleccionadas(){
	autorizarComisionProcessor.sendData();
	rechazarComisionProcessor = null;
	rechazarComisionProcessor = new dataProcessor("/MidasWeb/cotizacion/comision/rechazarComisionCotizacion.do");
	rechazarComisionProcessor.enableDataNames(true);
	rechazarComisionProcessor.setTransactionMode("POST");
	rechazarComisionProcessor.setUpdateMode("off");
	rechazarComisionProcessor.attachEvent("onAfterUpdateFinish",function (){
		var cotizacion = document.getElementById("idToCotizacion");
		if (cotizacion != null && cotizacion != undefined){
			mostrarAutorizarComisionesPorCotizacionGrid(cotizacion.value,'autorizacion');
		}});
	rechazarComisionProcessor.init(comisiones);
}

function rechazarComisionesSeleccionadas(){
	rechazarComisionProcessor.sendData();
	/*var cotizacion = document.getElementById("idToCotizacion");
	if (cotizacion != null && cotizacion != undefined){
		mostrarComisionesPorCotizacionGrid(cotizacion.value,'autorizacion');
	}*/
}

function validarAutorizacion(stage,rowId,cellInd,newValue,oldValue){
	if(cellInd == 4){
		var cellComisionNueva = comisiones.cellById(rowId, cellInd);
		var cellComisionDefault = comisiones.cellById(rowId, 3);
		var cellAutorizacion = comisiones.cellById(rowId,9);
		
		if (stage==2){
			if (checkIfNotZero(cellComisionNueva.getValue(),null)){
				cellComisionNueva.setValue(parseFloat(cellComisionNueva.getValue().toString()._dhx_trim()));
				if (newValue!=undefined && oldValue!=undefined){
					newValue = parseFloat(cellComisionNueva.getValue().toString()._dhx_trim());
					oldValue = parseFloat(oldValue.toString()._dhx_trim());
					if (newValue==oldValue)
						comisiones.setUpdated(rowId,false,"update");
					else
						if(cellComisionNueva.getValue() > parseFloat(cellComisionDefault.getValue()))
							cellAutorizacion.setValue("/MidasWeb/img/b_pendiente.gif^Pendiente^javascript:void(0);^_self");
						else	
							cellAutorizacion.setValue("/MidasWeb/img/blank.gif^Autorizado");
				}
				//else	cellAutorizacion.setValue("/MidasWeb/img/b_aceptar.gif^Autorizado^javascript:void(0);^_self");
				}
			else	return false;
		}
    }
	return true;
}
/**
 * Fin de funciones para mostrar las comisiones por cotizacion
 */
function mostrarExclusionRecargoProducto(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesRecargoProductoGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n Recargo,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoAsociadas.setColumnIds("claveRecargo,descripcionRecargo,codigoTipoPoliza,descripcionTipoPoliza");
	excRecargosProductoAsociadas.setInitWidths("80,210,100,210");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionRecargoProductoProcessor.setUpdated(rowId, true, "deleted");
		exclusionRecargoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionRecargoProductoProcessor.setUpdated(rowId, true, "inserted");
		exclusionRecargoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoAsociadas.do?id=" + id, null, 'json');

	exclusionRecargoProductoProcessor = new dataProcessor("/MidasWeb/configuracion/producto/guardarExcRecargoProductoAsociada.do");
	exclusionRecargoProductoProcessor.enableDataNames(true);
	exclusionRecargoProductoProcessor.setTransactionMode("POST");
	exclusionRecargoProductoProcessor.setUpdateMode("off");	
	exclusionRecargoProductoProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesRecargoProductoPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var id = document.getElementById("idToProducto").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("recargoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do?id="+id+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesRecargoProductoPorRecargo(selectRecargo){
	var idToRecargo = null;
	var id = document.getElementById("idToProducto").value;
	if(selectRecargo.selectedIndex !=0 ){
		idToRecargo = selectRecargo.value;
		document.getElementById("tipoPolizaSelect_rec").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToRecargo != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do?id="+id+"&idToRecargo="+idToRecargo, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionAumentoProducto(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesAumentoProductoGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoAsociadas.setColumnIds("claveAumento,descripcionAumento,codigoTipoPoliza,descripcionTipoPoliza");
	excRecargosProductoAsociadas.setInitWidths("80,210,100,210");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionAumentoProductoProcessor.setUpdated(rowId, true, "deleted");
		exclusionAumentoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionAumentoProductoProcessor.setUpdated(rowId, true, "inserted");
		exclusionAumentoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoAsociadas.do?id=" + id, null, 'json');

	exclusionAumentoProductoProcessor = new dataProcessor("/MidasWeb/configuracion/producto/guardarExcAumentoProductoAsociada.do");
	exclusionAumentoProductoProcessor.enableDataNames(true);
	exclusionAumentoProductoProcessor.setTransactionMode("POST");
	exclusionAumentoProductoProcessor.setUpdateMode("off");	
	exclusionAumentoProductoProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesAumentoProductoPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var idToProducto = document.getElementById("idToProducto").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("aumentoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do?id="+idToProducto+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do?id="+idToProducto, null, 'json');
}

function mostrarExclusionesAumentoProductoPorAumento(selectAumento){
	var idToAumento = null;
	var idToProducto = document.getElementById("idToProducto").value;
	if(selectAumento.selectedIndex !=0 ){
		idToAumento = selectAumento.value;
		document.getElementById("tipoPolizaSelect_aum").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToAumento != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do?id="+idToProducto+"&idToAumento="+idToAumento, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do?id="+idToProducto, null, 'json');
}

function mostrarExclusionDescuentoProducto(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesDescuentoProductoAsociadasGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n Descuento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoAsociadas.setColumnIds("claveDescuento,descripcionDescuento,codigoTipoPoliza,descripcionTipoPoliza");
	excRecargosProductoAsociadas.setInitWidths("80,210,100,210");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionDescuentoProductoProcessor.setUpdated(rowId, true, "deleted");
		exclusionDescuentoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionDescuentoProductoProcessor.setUpdated(rowId, true, "inserted");
		exclusionDescuentoProductoProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoAsociadas.do?id=" + id, null, 'json');

	exclusionDescuentoProductoProcessor = new dataProcessor("/MidasWeb/configuracion/producto/guardarExcDescuentoProductoAsociada.do");
	exclusionDescuentoProductoProcessor.enableDataNames(true);
	exclusionDescuentoProductoProcessor.setTransactionMode("POST");
	exclusionDescuentoProductoProcessor.setUpdateMode("off");	
	exclusionDescuentoProductoProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesDescuentoProductoPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var id = document.getElementById("idToProducto").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("descuentoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do?id="+id+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesDescuentoProductoPorDescuento(selectDescuento){
	var idToDescuento = null;
	var id = document.getElementById("idToProducto").value;
	if(selectDescuento.selectedIndex !=0 ){
		idToDescuento = selectDescuento.value;
		document.getElementById("tipoPolizaSelect_des").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoProductoNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento,Tipo P\u00F3liza, Descripci\u00F3n Tipo P\u00F3liza");
	excRecargosProductoPorAsociar.setInitWidths("80,210,100,210");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToDescuento != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do?id="+id+"&idToDescuento="+idToDescuento, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do?id="+id, null, 'json');
}

/* Configuracion Tipo Poliza */
var aumentosTipoPolizaProcessor;
var aumentosTipoPolizaError = false;

var recargosTipoPolizaProcessor;
var recargosTipoPolizaError = false;

var descuentosTipoPolizaProcessor;
var descuentosTipoPolizaError = false;

var monedaTipoPolizaProcessor;
var ramosTipoPolizaProcessor;

var exclusionRecargoTipoPolizaProcessor;
var exclusionRecargoTipoPolizaProcessorError = false;
var exclusionAumentoTipoPolizaProcessor;
var exclusionDescuentoTipoPolizaProcessor;

function mostrarAumentosTipoPolizaGrids(id) {
	var asociados = new dhtmlXGridObject('aumentosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		aumentosTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		aumentosTipoPolizaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/tipopoliza/mostrarAumentosAsociados.do?id='+ id, null, 'json');

	aumentosTipoPolizaProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/guardarAumentoAsociado.do');
	aumentosTipoPolizaProcessor.enableDataNames(true);
	aumentosTipoPolizaProcessor.setTransactionMode("POST");
	aumentosTipoPolizaProcessor.setUpdateMode("off");
	aumentosTipoPolizaProcessor.setVerificator(6, function(value,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			aumentosTipoPolizaError = true;
			return false;
		}
	    if(val <= 0){
	    	aumentosTipoPolizaError = true;
	        return false;
	    } else {
	    	aumentosTipoPolizaError = false;
	        return true;
	    }*/
		if(value > 0) {
			aumentosTipoPolizaError = false;
			return true;
		} else {
			aumentosTipoPolizaError = true;
			return false;
		}
	});
	aumentosTipoPolizaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject('aumentosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");	
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/tipopoliza/mostrarAumentosPorAsociar.do?id='+ id, null, 'json');
}

function mostrarRecargosTipoPolizaGrids(id) {
	var asociados = new dhtmlXGridObject('recargosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		recargosTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		recargosTipoPolizaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/tipopoliza/mostrarRecargosAsociados.do?id='+ id, null, 'json');

	recargosTipoPolizaProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/guardarRecargoAsociado.do');
	recargosTipoPolizaProcessor.enableDataNames(true);
	recargosTipoPolizaProcessor.setTransactionMode("POST");
	recargosTipoPolizaProcessor.setUpdateMode("off");
	recargosTipoPolizaProcessor.setVerificator(6, function(value,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			recargosTipoPolizaError = true;
			return false;
		}
	    if(val <= 0){
	    	recargosTipoPolizaError = true;
	        return false;
	    } else {
	    	recargosTipoPolizaError = false;
	        return true;
	    }*/
		if(value > 0) {
			recargosTipoPolizaError = false;
			return true;
		} else {
			recargosTipoPolizaError = true;
			return false;
		}
	});
	recargosTipoPolizaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject('recargosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");	
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/tipopoliza/mostrarRecargosPorAsociar.do?id='+ id, null, 'json');
}

function mostrarDescuentosTipoPolizaGrids(id) {
	var asociados = new dhtmlXGridObject('descuentosAsociadosGrid');
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		descuentosTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		descuentosTipoPolizaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load('/MidasWeb/configuracion/tipopoliza/mostrarDescuentosAsociados.do?id='+ id, null, 'json');

	descuentosTipoPolizaProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/guardarDescuentoAsociado.do');
	descuentosTipoPolizaProcessor.enableDataNames(true);
	descuentosTipoPolizaProcessor.setTransactionMode("POST");
	descuentosTipoPolizaProcessor.setUpdateMode("off");
	descuentosTipoPolizaProcessor.setVerificator(6, function(value,rowId,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			descuentosTipoPolizaError = true;
			return false;
		}
	    if(val <= 0){
	    	descuentosTipoPolizaError = true;
	        return false;
	    } else {
	    	descuentosTipoPolizaError = false;
	        return true;
	    }*/
		if(value > 0) {
			var tipo = asociados.cellById(rowId, 5);
			if(tipo.getValue() == '%') {
				if(value > 100) {
					descuentosTipoPolizaError = true;
			        return false;
				}
			}
			descuentosTipoPolizaError = false;
			return true;
		} else {
			descuentosTipoPolizaError = true;
			return false;
		}
	});
	descuentosTipoPolizaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject('descuentosPorAsociarGrid');
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");	
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;	
	});
	porAsociar.init();
	porAsociar.load('/MidasWeb/configuracion/tipopoliza/mostrarDescuentosPorAsociar.do?id='+ id, null, 'json');
}

function mostrarMonedaTipoPolizaGrids(idPadre){
	var monedasAsociadas = new dhtmlXGridObject('monedasAsociadasGrid');
	monedasAsociadas.setHeader("idPadre, Descripci\u00F3n");
	monedasAsociadas.setColumnIds("idPadre,descripcion");
	monedasAsociadas.setInitWidths("1,200");
	monedasAsociadas.setColAlign("center,left");
	monedasAsociadas.setColSorting("int,str");
	monedasAsociadas.setColTypes("ro,ro");
	monedasAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	monedasAsociadas.setSkin("light");
	monedasAsociadas.setColumnHidden(0, true);
	monedasAsociadas.enableDragAndDrop(true);
	monedasAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		monedaTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		monedaTipoPolizaProcessor.sendData(rowId);
	});
	monedasAsociadas.enableLightMouseNavigation(true);
	monedasAsociadas.init();
	monedasAsociadas.load('/MidasWeb/configuracion/tipopoliza/mostrarMonedasAsociadas.do?id='+ idPadre, null, 'json');

	monedaTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarMonedaAsociada.do");
	monedaTipoPolizaProcessor.enableDataNames(true);
	monedaTipoPolizaProcessor.setTransactionMode("POST");
	monedaTipoPolizaProcessor.setUpdateMode("cell");
	monedaTipoPolizaProcessor.init(monedasAsociadas);

	monedasNoAsociadas = new dhtmlXGridObject('monedasPorAsociarGrid');
	monedasNoAsociadas.setHeader("idPadre, Descripci\u00F3n");
	monedasNoAsociadas.setInitWidths("1,200");
	monedasNoAsociadas.setColAlign("center,left");
	monedasNoAsociadas.setColSorting("int,str");
	monedasNoAsociadas.setColTypes("ro,ro");
	monedasNoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	monedasNoAsociadas.setSkin("light");
	monedasNoAsociadas.setColumnHidden(0, true);
	monedasNoAsociadas.enableDragAndDrop(true);
	monedasNoAsociadas.enableLightMouseNavigation(true);
	monedasNoAsociadas.init();
	monedasNoAsociadas.load('/MidasWeb/configuracion/tipopoliza/mostrarMonedasPorAsociar.do?id='+ idPadre, null, 'json');
}

function mostrarRamosTipoPolizaGrid(prod) {
	var ramosAsociados = new dhtmlXGridObject('ramosAsociadosGrid');
	ramosAsociados.setHeader("C\u00F3digo, Descripci\u00F3n Ramo,Obligatorio");
	ramosAsociados.setColumnIds("codigo,descripcion,obligatorio");
	ramosAsociados.setInitWidths("80,200,1");
	ramosAsociados.setColAlign("center,left,left");
	ramosAsociados.setColSorting("int,str,str");
	ramosAsociados.setColTypes("ro,ro,ro");
	ramosAsociados.setColumnHidden(2, true);
	ramosAsociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosAsociados.setSkin("light");
	ramosAsociados.enableDragAndDrop(true);
	ramosAsociados.attachEvent("onBeforeDrag",function(IdSource){
		if (ramosAsociados.cells(IdSource,2).getValue() == "true"){
			document.getElementById('mensaje').style.visibility = 'visible';
			document.getElementById('mensaje').style.visibility = 'visible';
			return false;
		}
		else{
			document.getElementById('mensaje').style.visibility = 'hidden';
			document.getElementById('mensaje').style.visibility = 'hidden';
			return true;
		}
	});
	
	ramosAsociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		ramosTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		ramosTipoPolizaProcessor.sendData(rowId);
	});
	
	
	ramosAsociados.init();
	ramosAsociados.load('/MidasWeb/configuracion/tipopoliza/mostrarRamosAsociados.do?id='+ prod, null, 'json');
	
	ramosTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarRamoAsociado.do");
	ramosTipoPolizaProcessor.enableDataNames(true);
	ramosTipoPolizaProcessor.setTransactionMode("POST");
	ramosTipoPolizaProcessor.setUpdateMode("off");
	ramosTipoPolizaProcessor.init(ramosAsociados);
	
	var ramosPorAsociar = new dhtmlXGridObject('ramosPorAsociarGrid');
	ramosPorAsociar.setHeader("C\u00F3digo, Descripci\u00F3n Ramo");
	ramosPorAsociar.setInitWidths("80,200");
	ramosPorAsociar.setColAlign("center,left");
	ramosPorAsociar.setColSorting("int,str");
	ramosPorAsociar.setColTypes("ro,ro");
	ramosPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosPorAsociar.setSkin("light");
	ramosPorAsociar.enableDragAndDrop(true);
	ramosPorAsociar.init();
	ramosPorAsociar.load('/MidasWeb/configuracion/tipopoliza/mostrarRamosPorAsociar.do?id='+ prod, null, 'json');
}

function guardarAsosiacionRamosTipoPoliza(){
	ramosTipoPolizaProcessor.sendData();
	//configuracionTipoPolizaTabBar.setTabActive('detalle');
	mostrarMensajeExitoYCambiarTab(configuracionTipoPolizaTabBar, 'detalle');
}

function mostrarExclusionRecargoTipoPoliza(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesRecargoTipoPolizaGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n Recargo, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoAsociadas.setColumnIds("claveRecargo,descripcionRecargo,codigoCobertura,descripcionCobertura");
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionRecargoTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		exclusionRecargoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		//exclusionRecargoTipoPolizaProcessor.setUpdated(rowId, true, "inserted");
		//exclusionRecargoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaAsociadas.do?id=" + id, null, 'json');

	exclusionRecargoTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarExcRecargoTipoPolizaAsociada.do");
	exclusionRecargoTipoPolizaProcessor.enableDataNames(true);
	exclusionRecargoTipoPolizaProcessor.setTransactionMode("POST");
	exclusionRecargoTipoPolizaProcessor.setUpdateMode("off");	
	exclusionRecargoTipoPolizaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesRecargoTipoPolizaPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("recargoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do?id="+id+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesRecargoTipoPolizaPorRecargo(selectRecargo){
	var idToRecargo = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectRecargo.selectedIndex !=0 ){
		idToRecargo = selectRecargo.value;
		document.getElementById("tipoPolizaSelect_rec").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Recargo, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToRecargo != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do?id="+id+"&idToRecargo="+idToRecargo, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionAumentoTipoPoliza(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesAumentoTipoPolizaAsociadasGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n Aumento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoAsociadas.setColumnIds("claveRecargo,descripcionAumento,codigoCobertura,descripcionCobertura");
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionAumentoTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		exclusionAumentoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionAumentoTipoPolizaProcessor.setUpdated(rowId, true, "inserted");
		exclusionAumentoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaAsociadas.do?id=" + id, null, 'json');

	exclusionAumentoTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarExcAumentoTipoPolizaAsociada.do");
	exclusionAumentoTipoPolizaProcessor.enableDataNames(true);
	exclusionAumentoTipoPolizaProcessor.setTransactionMode("POST");
	exclusionAumentoTipoPolizaProcessor.setUpdateMode("off");	
	exclusionAumentoTipoPolizaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesAumentoTipoPolizaPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("aumentoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do?id="+id+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesAumentoTipoPolizaPorAumento(selectAumento){
	var idToAumento = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectAumento.selectedIndex !=0 ){
		idToAumento = selectAumento.value;
		document.getElementById("tipoPolizaSelect_aum").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Aumento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToAumento != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do?id="+id+"&idToAumento="+idToAumento, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionDescuentoTipoPoliza(id) {
	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesDescuentoTipoPolizaAsociadasGrid');
	excRecargosProductoAsociadas.setHeader("Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoAsociadas.setColumnIds("claveRecargo,descripcionDescuento,codigoCobertura,descripcionCobertura");
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	excRecargosProductoAsociadas.setColAlign("center,left,center,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionDescuentoTipoPolizaProcessor.setUpdated(rowId, true, "deleted");
		exclusionDescuentoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		//exclusionDescuentoTipoPolizaProcessor.setUpdated(rowId, true, "inserted");
		//exclusionDescuentoTipoPolizaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaAsociadas.do?id=" + id, null, 'json');

	exclusionDescuentoTipoPolizaProcessor = new dataProcessor("/MidasWeb/configuracion/tipopoliza/guardarExcDescuentoTipoPolizaAsociada.do");
	exclusionDescuentoTipoPolizaProcessor.enableDataNames(true);
	exclusionDescuentoTipoPolizaProcessor.setTransactionMode("POST");
	exclusionDescuentoTipoPolizaProcessor.setUpdateMode("off");	
	exclusionDescuentoTipoPolizaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesDescuentoTipoPolizaPorPoliza(selectTP){
	var idToTipoPoliza = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectTP.selectedIndex !=0 ){
		idToTipoPoliza = selectTP.value;
		document.getElementById("descuentoSelect").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToTipoPoliza != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do?id="+id+"&idToTipoPoliza="+idToTipoPoliza, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

function mostrarExclusionesDescuentoTipoPolizaPorDescuento(selectDescuento){
	var idToDescuento = null;
	var id = document.getElementById("idToTipoPoliza").value;
	if(selectDescuento.selectedIndex !=0 ){
		idToDescuento = selectDescuento.value;
		document.getElementById("tipoPolizaSelect_des").selectedIndex=0;
	}
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoTipoPolizaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader("Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Cobertura");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("center,left,center,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.init();
	if(idToDescuento != null)
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do?id="+id+"&idToDescuento="+idToDescuento, null, 'json');
	else
		excRecargosProductoPorAsociar.load("/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do?id="+id, null, 'json');
}

/* Configuracion Seccion */

var coberturasSeccionPorcessor;
var ramosSeccionProcessor;
var seccionRequeridaProcessor;

function mostrarCoberturasSeccion(idSeccion) {
	var coberturasAsociadasSeccion = new dhtmlXGridObject('coberturasAsociadasSeccionGrid');
	coberturasAsociadasSeccion.setHeader("C\u00F3digo, Descripci\u00F3n Cobertura, Clave Obligatoriedad");
	coberturasAsociadasSeccion.setColumnIds("codigo,descripcion,claveObligatoriedad");
	coberturasAsociadasSeccion.setInitWidths("130,200,160");
	coberturasAsociadasSeccion.setColAlign("center,left,left");
	coberturasAsociadasSeccion.setColSorting("int,str,str");
	coberturasAsociadasSeccion.setColTypes("ro,ro,coro");
	coberturasAsociadasSeccion.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coberturasAsociadasSeccion.setSkin("light");
	coberturasAsociadasSeccion.enableDragAndDrop(true);
	var combo = coberturasAsociadasSeccion.getCombo(2);
	combo.put("0","OPCIONAL");
	combo.put("1","OPCIONAL DEFAULT");
	combo.put("2","OBLIGATORIO PARCIAL");
	combo.put("3","OBLIGATORIO");
	combo.save();
	coberturasAsociadasSeccion.attachEvent("onBeforeRowDeleted",function(rowId) {
		coberturasSeccionPorcessor.setUpdated(rowId, true, "deleted");
		coberturasSeccionPorcessor.sendData(rowId);
	});
	coberturasAsociadasSeccion.init();
	coberturasAsociadasSeccion.load('/MidasWeb/configuracion/seccion/mostrarCoberturasAsociadas.do?id='+idSeccion, null, 'json');
	
	coberturasSeccionPorcessor = new dataProcessor("/MidasWeb/configuracion/seccion/guardarCoberturaAsociada.do?id="+idSeccion);
	coberturasSeccionPorcessor.enableDataNames(true);
	coberturasSeccionPorcessor.setTransactionMode("POST");
	coberturasSeccionPorcessor.setUpdateMode("off");
	coberturasSeccionPorcessor.defineAction("errorDesasociarCobertura", function(node){
		mostrarMensajeErrorCobertura();
		return false;
	});

	//iniciarManejoEventosDataProcessor(coberturasSeccionPorcessor,'mostrarTabDeComponente(configuracionSeccionTabBar, \'detalle\')');

	coberturasSeccionPorcessor.init(coberturasAsociadasSeccion);
	
	var coberturasporAsociarSeccion = new dhtmlXGridObject('coberturasPorAsociarSeccionGrid');
	coberturasporAsociarSeccion.setHeader("C\u00F3digo, Descripci\u00F3n Cobertura, Clave Obligatoriedad");
	coberturasporAsociarSeccion.setInitWidths("130,200,160");
	coberturasporAsociarSeccion.setColAlign("center,left,left");
	coberturasporAsociarSeccion.setColSorting("int,str,str");
	coberturasporAsociarSeccion.setColTypes("ro,ro,ro");
	coberturasporAsociarSeccion.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coberturasporAsociarSeccion.setSkin("light");
	coberturasporAsociarSeccion.enableDragAndDrop(true);
	coberturasporAsociarSeccion.init();
	coberturasporAsociarSeccion.load('/MidasWeb/configuracion/seccion/mostrarCoberturasPorAsociar.do?id='+idSeccion, null, 'json');
}

function mostrarRamosSeccionGrid(prod) {
	ramosAsociados = new dhtmlXGridObject('ramosAsociadosGrid');
	ramosAsociados.setHeader("C\u00F3digo, Descripci\u00F3n Ramo");
	ramosAsociados.setColumnIds("codigo,descripcion");
	ramosAsociados.setInitWidths("80,200");
	ramosAsociados.setColAlign("center,left");
	ramosAsociados.setColSorting("int,str");
	ramosAsociados.setColTypes("ro,ro");
	ramosAsociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosAsociados.setSkin("light");
	ramosAsociados.enableDragAndDrop(true);
	ramosAsociados.init();
	ramosAsociados.load('/MidasWeb/configuracion/seccion/mostrarRamosAsociados.do?id='+ prod, null, 'json');

	ramosSeccionProcessor = new dataProcessor("/MidasWeb/configuracion/seccion/guardarRamoAsociado.do?id="+ prod);
	ramosSeccionProcessor.enableDataNames(true);
	ramosSeccionProcessor.setTransactionMode("POST");
	ramosSeccionProcessor.setUpdateMode("off");
	ramosSeccionProcessor.init(ramosAsociados);
	
	ramosPorAsociar = new dhtmlXGridObject('ramosPorAsociarGrid');
	ramosPorAsociar.setHeader("C\u00F3digo, Descripci\u00F3n Ramo");
	ramosPorAsociar.setInitWidths("80,200");
	ramosPorAsociar.setColAlign("center,left");
	ramosPorAsociar.setColSorting("int,str");
	ramosPorAsociar.setColTypes("ro,ro");
	ramosPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	ramosPorAsociar.setSkin("light");
	ramosPorAsociar.enableDragAndDrop(true);
	ramosPorAsociar.init();
	ramosPorAsociar.load('/MidasWeb/configuracion/seccion/mostrarRamosPorAsociar.do?id='+ prod, null, 'json');
}

function mostrarSeccionRequerida(idSeccion){
	var seccionesRequeridas = new dhtmlXGridObject('seccionesRequeridasGrid');
	seccionesRequeridas.setHeader("idSeccionRequerida, C\u00F3digo, Nombre Bien/Secci\u00F3n, Descripci\u00F3n Bien/Secci\u00F3n");
	seccionesRequeridas.setColumnIds("idSeccionRequerida,codSeccion,nomSeccion,descSeccion");
	seccionesRequeridas.setInitWidths("1,80,180,200");
	seccionesRequeridas.setColAlign("left,center,left,left");
	seccionesRequeridas.setColSorting("int,str,str,str");
	seccionesRequeridas.setColTypes("ro,ro,ro,ro");
	seccionesRequeridas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesRequeridas.setSkin("light");
	seccionesRequeridas.enableDragAndDrop(true);
	seccionesRequeridas.attachEvent("onBeforeRowDeleted",function(rowId) {
		seccionRequeridaProcessor.setUpdated(rowId, true, "deleted");
		seccionRequeridaProcessor.sendData(rowId);
	});
	seccionesRequeridas.enableLightMouseNavigation(true);
	seccionesRequeridas.setColumnHidden(0, true);
	seccionesRequeridas.setColumnHidden(2, true);
	seccionesRequeridas.init();
	seccionesRequeridas.load('/MidasWeb/configuracion/seccion/mostrarSeccionesRequeridas.do?id='+idSeccion, null, 'json');
	
	seccionRequeridaProcessor = new dataProcessor("/MidasWeb/configuracion/seccion/guardarSeccionRequerida.do");
	seccionRequeridaProcessor.enableDataNames(true);
	seccionRequeridaProcessor.setTransactionMode("POST");
	seccionRequeridaProcessor.setUpdateMode("off");
	seccionRequeridaProcessor.init(seccionesRequeridas);
	
	var seccionesNoRequeridas = new dhtmlXGridObject('seccionesNoRequeridasGrid');
	seccionesNoRequeridas.setHeader("idSeccionRequerida, C\u00F3digo, Nombre Bien/Secci\u00F3n, Descripci\u00F3n Bien/Secci\u00F3n");
	seccionesNoRequeridas.setInitWidths("1,80,1800,200");
	seccionesNoRequeridas.setColAlign("left,center,left,left");
	seccionesNoRequeridas.setColSorting("int,str,str,str");
	seccionesNoRequeridas.setColTypes("ro,ro,ro,ro");
	seccionesNoRequeridas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesNoRequeridas.setSkin("light");
	seccionesNoRequeridas.setColumnHidden(0, true);
	seccionesNoRequeridas.setColumnHidden(2, true);
	seccionesNoRequeridas.enableDragAndDrop(true);
	seccionesNoRequeridas.enableLightMouseNavigation(true);
	seccionesNoRequeridas.init();
	seccionesNoRequeridas.load('/MidasWeb/configuracion/seccion/mostrarSeccionesPorAsociar.do?id='+idSeccion, null, 'json');
}

/* Configuracion Cobertura*/
var aumentosCoberturaProcessor;
var aumentosCoberturaError = false;

var recargosCoberturaProcessor;
var recargosCoberturaError = false;

var descuentosCoberturaProcessor;
var descuentosCoberturaError = false;

var riesgoCoberturaProcessor;
var riesgoCoberturaError = false;

var exclusionRecargoCoberturaProcessor;
var exclusionAumentoCoberturaProcessor;
var exclusionDescuentoCoberturaProcessor;
var exclusionCoberturaProcessor;
var coberturasRequeridasProcessor;

var coasegurosCoberturaGrid;
var coasegurosCoberturaProcessor;
var coasegurosCoberturaError = false;

var deduciblesCoberturaGrid;
var deduciblesCoberturaProcessor;
var deduciblesCoberturaError = false;

var endosoDeclaracionGrid;
var endosoDeclaracionProcessor;
var endosoDeclaracionError = false;

function mostrarAumentosCoberturaGrids(id) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarAumentoAsociado.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarAumentosAsociados.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarAumentosPorAsociar.do';
	var asociadosDiv = 'aumentosAsociadosCoberturaGrid';
	var porAsociarDiv = 'aumentosPorAsociarCoberturaGrid';

	var asociados = new dhtmlXGridObject(asociadosDiv);
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		aumentosCoberturaProcessor.setUpdated(rowId, true, "deleted");
		aumentosCoberturaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + '?id='+ id, null, 'json');

	aumentosCoberturaProcessor = new dataProcessor(processorPath);
	aumentosCoberturaProcessor.enableDataNames(true);
	aumentosCoberturaProcessor.setTransactionMode("POST");
	aumentosCoberturaProcessor.setUpdateMode("off");
	aumentosCoberturaProcessor.setVerificator(6, function(value,colName){
		var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			aumentosCoberturaError = true;
			return false;
		}
	    if(val <= 0){
	    	aumentosCoberturaError = true;
	        return false;
	    } else {
	    	aumentosCoberturaError = false;
	        return true;
	    }
	});

	iniciarManejoEventosDataProcessor(aumentosCoberturaProcessor,'mostrarTabDeComponente(configuracionCoberturaTabBar, \'detalle\')');
	
	
	aumentosCoberturaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject(porAsociarDiv);
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Aumento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");	
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;	
	});
	porAsociar.init();
	porAsociar.load(porAsociarPath + '?id='+ id, null, 'json');
}

function mostrarRecargosCoberturaGrids(id) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarRecargoAsociado.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarRecargosAsociados.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarRecargosPorAsociar.do';
	var asociadosDiv = 'recargosAsociadosCoberturaGrid';
	var porAsociarDiv = 'recargosPorAsociarCoberturaGrid';

	var asociados = new dhtmlXGridObject(asociadosDiv);
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		recargosCoberturaProcessor.setUpdated(rowId, true, "deleted");
		recargosCoberturaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + '?id='+ id, null, 'json');

	recargosCoberturaProcessor = new dataProcessor(processorPath);
	recargosCoberturaProcessor.enableDataNames(true);
	recargosCoberturaProcessor.setTransactionMode("POST");
	recargosCoberturaProcessor.setUpdateMode("off");
	recargosCoberturaProcessor.setVerificator(6, function(value,colName){
		var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			recargosCoberturaError = true;
			return false;
		}
	    if(val <= 0){
	    	recargosCoberturaError = true;
	        return false;
	    } else {
	    	recargosCoberturaError = false;
	        return true;
	    }
	});
	
	
	iniciarManejoEventosDataProcessor(recargosCoberturaProcessor,'mostrarTabDeComponente(configuracionCoberturaTabBar, \'detalle\')');
	
	recargosCoberturaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject(porAsociarDiv);
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Recargo, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	porAsociar.init();
	porAsociar.load(porAsociarPath + '?id='+ id, null, 'json');
}

function mostrarDescuentosCoberturaGrids(id) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarDescuentoAsociado.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarDescuentosAsociados.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarDescuentosPorAsociar.do';
	var asociadosDiv = 'descuentosAsociadosCoberturaGrid';
	var porAsociarDiv = 'descuentosPorAsociarCoberturaGrid';

	var asociados = new dhtmlXGridObject(asociadosDiv);
	asociados.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	asociados.setColumnIds("idPadre,descripcion,obligatorio,comercialTecnico,aplicaReaseguro,claveTipo,valor");
	asociados.setInitWidths("1,150,100,120,140,40,50");
	asociados.setColAlign("center,left,center,center,center,center,center");
	asociados.setColSorting("int,str,int,int,int,str,int");
	asociados.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	var combo = asociados.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = asociados.cellById(rowId, cellInd);
			var cell2 = asociados.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;
	});
	asociados.attachEvent("onRowCreated",function(rowId, rowObj) {
		var cell = asociados.cellById(rowId, 3);
		var cell2 = asociados.cellById(rowId, 4);
		if(cell.getValue() == 2) {
			cell2.setChecked(true);
			cell2.setDisabled(true);
		} else {
			cell2.setDisabled(false);
		}
		return true;
	});
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		descuentosCoberturaProcessor.setUpdated(rowId, true, "deleted");
		descuentosCoberturaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + '?id='+ id, null, 'json');

	descuentosCoberturaProcessor = new dataProcessor(processorPath);
	descuentosCoberturaProcessor.enableDataNames(true);
	descuentosCoberturaProcessor.setTransactionMode("POST");
	descuentosCoberturaProcessor.setUpdateMode("off");
	descuentosCoberturaProcessor.setVerificator(6, function(value,colName){
		var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			descuentosCoberturaError = true;
			return false;
		}
	    if(val <= 0){
	    	descuentosCoberturaError = true;
	        return false;
	    } else {
	    	descuentosCoberturaError = false;
	        return true;
	    }
	});
	
	iniciarManejoEventosDataProcessor(descuentosCoberturaProcessor,'mostrarTabDeComponente(configuracionCoberturaTabBar, \'detalle\')');
	
	
	descuentosCoberturaProcessor.init(asociados);

	var porAsociar = new dhtmlXGridObject(porAsociarDiv);
	porAsociar.setHeader("idPadre,Descripci\u00F3n,Obligatorio, Tipo Descuento, Aplica Reaseguro,Tipo, Valor");
	porAsociar.setInitWidths("1,150,100,120,140,40,50");
	porAsociar.setColAlign("center,left,center,center,center,center,center");
	porAsociar.setColSorting("int,str,int,int,int,str,int");
	porAsociar.setColTypes("ro,ro,ch,coro,ch,ro,ed");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	var combo = porAsociar.getCombo(3);
	combo.put("1","COMERCIAL");
	combo.put("2","TECNICO");
	combo.save();
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.attachEvent("onEditCell",function(stage,rowId,cellInd) {
		if(cellInd == 3 && stage == 2){
			var cell = porAsociar.cellById(rowId, cellInd);
			var cell2 = porAsociar.cellById(rowId, 4);
			if(cell.getValue() == 2) {
				cell2.setChecked(true);
				cell2.setDisabled(true);
			} else {
				cell2.setChecked(false);
				cell2.setDisabled(false);
			}
	    }
		return true;	
	});
	porAsociar.init();
	porAsociar.load(porAsociarPath + '?id='+ id, null, 'json');
}

function mostrarRiesgosCobertura(idCobertura,idSeccion){
	var riesgosAsociadosCobertura = new dhtmlXGridObject('riesgosAsociadosCoberturaGrid');
	riesgosAsociadosCobertura.setHeader("idToSeccion, C\u00F3digo, Nombre, Descripci\u00F3n Riesgo,Editar");
	riesgosAsociadosCobertura.setColumnIds("idToSeccion,codRiesgo,nombreRiesgo,descRiesgo,editar");
	riesgosAsociadosCobertura.setInitWidths("1,80,200,*,60");
	riesgosAsociadosCobertura.setColAlign("left,center,left,left,center");
	riesgosAsociadosCobertura.setColSorting("int,int,str,str,str");
	riesgosAsociadosCobertura.setColTypes("ro,ro,ro,ro,img");
	riesgosAsociadosCobertura.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	riesgosAsociadosCobertura.setSkin("light");
	riesgosAsociadosCobertura.enableDragAndDrop(true);
	riesgosAsociadosCobertura.attachEvent("onBeforeRowDeleted",function(rowId) {
		riesgoCoberturaProcessor.setUpdated(rowId, true, "deleted");
		riesgoCoberturaProcessor.sendData(rowId);
	});
	riesgosAsociadosCobertura.attachEvent("onRowAdded",function(rowId) {
		riesgoCoberturaProcessor.setUpdated(rowId, true, "inserted");
		riesgoCoberturaProcessor.sendData(rowId);
	});
	riesgosAsociadosCobertura.setColumnHidden(0, true);
	riesgosAsociadosCobertura.init();
	riesgosAsociadosCobertura.load('/MidasWeb/configuracion/cobertura/mostrarRiesgosAsociados.do?id='+idCobertura+'&idPadre='+idSeccion, null, 'json');
	
	riesgoCoberturaProcessor = new dataProcessor("/MidasWeb/configuracion/cobertura/guardarRiesgoAsociado.do?id="+idCobertura+"&idPadre="+idSeccion);
	riesgoCoberturaProcessor.enableDataNames(true);
	riesgoCoberturaProcessor.setTransactionMode("POST");
	riesgoCoberturaProcessor.setUpdateMode("off");
	riesgoCoberturaProcessor.init(riesgosAsociadosCobertura);
	
	var riesgosPorAsociarCobertura = new dhtmlXGridObject('riesgosPorAsociarCoberturaGrid');
	riesgosPorAsociarCobertura.setHeader("idToSeccion, C\u00F3digo, Nombre, Descripci\u00F3n Riesgo,Editar");
	riesgosPorAsociarCobertura.setInitWidths("1,80,200,*,30");
	riesgosPorAsociarCobertura.setColAlign("left,center,left,left,center");
	riesgosPorAsociarCobertura.setColSorting("int,int,str,str,str");
	riesgosPorAsociarCobertura.setColTypes("ro,ro,ro,ro,img");
	riesgosPorAsociarCobertura.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	riesgosPorAsociarCobertura.setSkin("light");
	riesgosPorAsociarCobertura.enableDragAndDrop(true);
	riesgosPorAsociarCobertura.setColumnHidden(0, true);
	riesgosPorAsociarCobertura.setColumnHidden(4, true);
	/*var combo = riesgosPorAsociarCobertura.getCombo(3);
	combo.put("0","OPCIONAL");
	combo.put("1","OPCIONAL DEFAULT");
	combo.put("2","OBLIGATORIO PARCIAL");
	combo.put("3","OBLIGATORIO");
	combo.save();*/
	riesgosPorAsociarCobertura.init();
	riesgosPorAsociarCobertura.load('/MidasWeb/configuracion/cobertura/mostrarRiesgosPorAsociar.do?id='+idCobertura+'&idPadre='+idSeccion, null, 'json');
}

function mostrarExclusionRecargosCoberturaGrids(id,idToSeccion) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarRecargoCoberturaExcluido.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaExcluidos.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do';
	var exclusionProductoHeader = "Clave,Descripci\u00F3n Recargo,C\u00F3digo, Descripci\u00F3n Riesgo";
	var exclusionProductoIds = "claveRecargo,descripcionRecargo,codigoRiesgo,descripcionRiesgo";

	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesRecargoCoberturaAsociadasGrid');
	excRecargosProductoAsociadas.setHeader(exclusionProductoHeader);
	excRecargosProductoAsociadas.setColumnIds(exclusionProductoIds);
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	excRecargosProductoAsociadas.setColAlign("left,left,left,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionRecargoCoberturaProcessor.setUpdated(rowId, true, "deleted");
		exclusionRecargoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionRecargoCoberturaProcessor.setUpdated(rowId, true, "inserted");
		exclusionRecargoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionRecargoCoberturaProcessor.setUpdated(rowId, true, "inserted");
		exclusionRecargoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load(asociadosPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');

	exclusionRecargoCoberturaProcessor = new dataProcessor(processorPath);
	exclusionRecargoCoberturaProcessor.enableDataNames(true);
	exclusionRecargoCoberturaProcessor.setTransactionMode("POST");
	exclusionRecargoCoberturaProcessor.setUpdateMode("off");	
	exclusionRecargoCoberturaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load(porAsociarPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesRecargoCoberturaPorRiesgo(selectTP){
	var idToRiesgo = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectTP.selectedIndex !=0 ){
		idToRiesgo = selectTP.value;
		document.getElementById("recargoSelect").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave,Descripci\u00F3n Recargo,C\u00F3digo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	if(idToRiesgo != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do?id=' + id+"&idToSeccion="+idToSeccion+"&idToRiesgo="+idToRiesgo, null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesRecargoCoberturaPorRecargo(selectRecargo){
	var idToRecargo = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectRecargo.selectedIndex !=0 ){
		idToRecargo = selectRecargo.value;
		document.getElementById("riesgoSelect_rec").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave,Descripci\u00F3n Recargo,C\u00F3digo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesRecargoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	if(idToRecargo != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do?id=' + id+"&idToSeccion="+idToSeccion+"&idToRecargo="+idToRecargo, null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionAumentosCoberturaGrids(id,idToSeccion) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarExcAumentoCoberturaAsociada.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaAsociadas.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do';
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Aumento, Riesgo, Descripci\u00F3n Riesgo";
	var exclusionProductoIds = "claveAumento,descripcionAumento,codigoRiesgo,descripcionRiesgo";

	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesAumentoCoberturaAsociadasGrid');
	excRecargosProductoAsociadas.setHeader(exclusionProductoHeader);
	excRecargosProductoAsociadas.setColumnIds(exclusionProductoIds);
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	excRecargosProductoAsociadas.setColAlign("left,left,left,left");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionAumentoCoberturaProcessor.setUpdated(rowId, true, "deleted");
		exclusionAumentoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionAumentoCoberturaProcessor.setUpdated(rowId, true, "inserted");
		exclusionAumentoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load(asociadosPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');

	exclusionAumentoCoberturaProcessor = new dataProcessor(processorPath);
	exclusionAumentoCoberturaProcessor.enableDataNames(true);
	exclusionAumentoCoberturaProcessor.setTransactionMode("POST");
	exclusionAumentoCoberturaProcessor.setUpdateMode("off");	
	exclusionAumentoCoberturaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	/*excRecargosProductoPorAsociar.setInitWidths("1,1,80,200,200,80,80");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,int,str,str,str,int,int");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro,ro,ch,ro");*/
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load(porAsociarPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesAumentoCoberturaPorRiesgo(selectTP){
	var idToRiesgo = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectTP.selectedIndex !=0 ){
		idToRiesgo = selectTP.value;
		document.getElementById("aumentoSelect").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Aumento, Riesgo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	if(idToRiesgo != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion+'&idToRiesgo='+idToRiesgo, null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesAumentoCoberturaPorAumento(selectAumento){
	var idToAumento = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectAumento.selectedIndex !=0 ){
		idToAumento = selectAumento.value;
		document.getElementById("riesgoSelect_aum").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Aumento, Riesgo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesAumentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	if(idToAumento != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion+'&idToAumento='+idToAumento, null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionDescuentosCoberturaGrids(id,idToSeccion) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarExcDescuentoCoberturaAsociada.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaAsociadas.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do';
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Riesgo";
	var exclusionProductoIds = "claveDescuento,descripcionDescuento,codigoRiesgo,descripcionRiesgo";

	var excRecargosProductoAsociadas = new dhtmlXGridObject('exclusionesDescuentoCoberturaAsociadasGrid');
	excRecargosProductoAsociadas.setHeader(exclusionProductoHeader);
	excRecargosProductoAsociadas.setColumnIds(exclusionProductoIds);
	//excRecargosProductoAsociadas.setInitWidths("1,1,80,200,200,80,80");
	excRecargosProductoAsociadas.setInitWidths("80,200,80,200");
	//excRecargosProductoAsociadas.setColAlign("left,left,left,left,left,left,left");
	excRecargosProductoAsociadas.setColAlign("left,left,left,left");
	//excRecargosProductoAsociadas.setColSorting("int,int,str,str,str,int,int");
	excRecargosProductoAsociadas.setColSorting("int,str,str,str");
	//excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro,ro,ch,ro");
	excRecargosProductoAsociadas.setColTypes("ro,ro,ro,ro");
	excRecargosProductoAsociadas.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoAsociadas.setSkin("light");
	excRecargosProductoAsociadas.enableDragAndDrop(true);
	/*excRecargosProductoAsociadas.setColumnHidden(0, true);
	excRecargosProductoAsociadas.setColumnHidden(1, true);
	excRecargosProductoAsociadas.setColumnHidden(2, true);*/
	excRecargosProductoAsociadas.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionDescuentoCoberturaProcessor.setUpdated(rowId, true, "deleted");
		exclusionDescuentoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.attachEvent("onRowAdded",function(rowId) {
		exclusionDescuentoCoberturaProcessor.setUpdated(rowId, true, "inserted");
		exclusionDescuentoCoberturaProcessor.sendData(rowId);
	});
	excRecargosProductoAsociadas.enableLightMouseNavigation(true);
	excRecargosProductoAsociadas.init();
	excRecargosProductoAsociadas.load(asociadosPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');

	exclusionDescuentoCoberturaProcessor = new dataProcessor(processorPath);
	exclusionDescuentoCoberturaProcessor.enableDataNames(true);
	exclusionDescuentoCoberturaProcessor.setTransactionMode("POST");
	exclusionDescuentoCoberturaProcessor.setUpdateMode("off");	
	exclusionDescuentoCoberturaProcessor.init(excRecargosProductoAsociadas);
	
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	//excRecargosProductoPorAsociar.setInitWidths("1,1,80,200,200,80,80");
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	//excRecargosProductoPorAsociar.setColAlign("left,left,left,left,left,left,left");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	//excRecargosProductoPorAsociar.setColSorting("int,int,str,str,str,int,int");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	//excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro,ro,ch,ro");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load(porAsociarPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesDescuentoCoberturaPorRiesgo(selectTP){
	var idToRiesgo = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectTP.selectedIndex !=0 ){
		idToRiesgo = selectTP.value;
		document.getElementById("descuentoSelect").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	excRecargosProductoPorAsociar.load(porAsociarPath + "?id=" + id+"&idToSeccion="+idToSeccion, null, 'json');
	if(idToRiesgo != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion+'&idToRiesgo='+idToRiesgo , null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionesDescuentoCoberturaPorDescuento(selectDescuento){
	var idToDescuento = null;
	var id = document.getElementById("idToCobertura").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	if(selectDescuento.selectedIndex !=0 ){
		idToDescuento = selectDescuento.value;
		document.getElementById("riesgoSelect_des").selectedIndex=0;
	}
	var exclusionProductoHeader = "Clave, Descripci\u00F3n Descuento, C\u00F3digo, Descripci\u00F3n Riesgo";
	var excRecargosProductoPorAsociar = new dhtmlXGridObject('exclusionesDescuentoCoberturaNoAsociadasGrid');
	excRecargosProductoPorAsociar.setHeader(exclusionProductoHeader);
	excRecargosProductoPorAsociar.setInitWidths("80,200,80,200");
	excRecargosProductoPorAsociar.setColAlign("left,left,left,left");
	excRecargosProductoPorAsociar.setColSorting("int,str,str,str");
	excRecargosProductoPorAsociar.setColTypes("ro,ro,ro,ro");
	excRecargosProductoPorAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	excRecargosProductoPorAsociar.setSkin("light");
	excRecargosProductoPorAsociar.enableDragAndDrop(true);
	excRecargosProductoPorAsociar.enableLightMouseNavigation(true);
	excRecargosProductoPorAsociar.init();
	if(idToDescuento != null)
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion+'&idToDescuento='+idToDescuento , null, 'json');
	else
		excRecargosProductoPorAsociar.load('/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do?id=' + id+"&idToSeccion="+idToSeccion, null, 'json');
}

function mostrarExclusionCoberturaGrids(id) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarCoberturasExcluidas.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarCoberturasExcluidas.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarCoberturasPorExcluir.do';
	var exclusionProductoHeader = "idToCoberturaBase,C\u00F3digo,Nombre, Descripci\u00F3n";
	var exclusionProductoIds = "idToCoberturaBase,codigo,nombre,descripcion";
	
	var asociados = new dhtmlXGridObject('excluidosGrid');
	asociados.setHeader(exclusionProductoHeader);
	asociados.setColumnIds(exclusionProductoIds);
	asociados.setInitWidths("1,80,200,540");
	asociados
	.attachHeader("&nbsp;,#text_filter,&nbsp;,#text_filter,&nbsp;");
	asociados.setColAlign("left,left,left,left");
	asociados.setColSorting("int,str,str,str");
	asociados.setColTypes("ro,ro,ro,ro");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	asociados.enableDragAndDrop(true);
	asociados.setColumnHidden(0, true);
	asociados.setColumnHidden(2, true);
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		exclusionCoberturaProcessor.setUpdated(rowId, true, "deleted");
		exclusionCoberturaProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + "?id=" + id, null, 'json');
	
	exclusionCoberturaProcessor = new dataProcessor(processorPath);
	exclusionCoberturaProcessor.enableDataNames(true);
	exclusionCoberturaProcessor.setTransactionMode("POST");
	exclusionCoberturaProcessor.setUpdateMode("off");	
	exclusionCoberturaProcessor.init(asociados);
	
	var porAsociar = new dhtmlXGridObject('porExcluirGrid');
	porAsociar.setHeader(exclusionProductoHeader);
	porAsociar.setInitWidths("1,80,200,540");
	porAsociar
	.attachHeader("&nbsp;,#text_filter,&nbsp;,#text_filter,&nbsp;");
	porAsociar.setColAlign("left,left,left,left");
	porAsociar.setColSorting("int,str,str,str");
	porAsociar.setColTypes("ro,ro,ro,ro");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	porAsociar.enableDragAndDrop(true);
	porAsociar.setColumnHidden(0, true);
	porAsociar.setColumnHidden(2, true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.init();
	porAsociar.load(porAsociarPath + "?id=" + id, null, 'json');
}

function mostrarCoberturasRequeridasGrids(id) {
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarCoberturasRequeridas.do';
	var asociadosPath = '/MidasWeb/configuracion/cobertura/mostrarCoberturasRequeridas.do';
	var porAsociarPath = '/MidasWeb/configuracion/cobertura/mostrarCoberturasNoRequeridas.do';
	var exclusionProductoHeader = "idToCoberturaBase,C\u00F3digo,Nombre, Descripci\u00F3n";
	var exclusionProductoIds = "idToCoberturaBase,codigo,nombre,descripcion";
	
	var asociados = new dhtmlXGridObject('coberturasRequeridasGrid');
	asociados.setHeader(exclusionProductoHeader);
	asociados.setColumnIds(exclusionProductoIds);
	asociados.setInitWidths("1,80,200,540");
	asociados
	.attachHeader("&nbsp;,#text_filter,&nbsp;,#text_filter,&nbsp;");
	asociados.setColAlign("left,left,left,left");
	asociados.setColSorting("int,str,str,str");
	asociados.setColTypes("ro,ro,ro,ro");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	asociados.enableDragAndDrop(true);
	asociados.setColumnHidden(0, true);
	asociados.setColumnHidden(2, true);
	asociados.attachEvent("onBeforeRowDeleted",function(rowId) {
		coberturasRequeridasProcessor.setUpdated(rowId, true, "deleted");
		coberturasRequeridasProcessor.sendData(rowId);
	});
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + "?id=" + id, null, 'json');
	
	coberturasRequeridasProcessor = new dataProcessor(processorPath);
	coberturasRequeridasProcessor.enableDataNames(true);
	coberturasRequeridasProcessor.setTransactionMode("POST");
	coberturasRequeridasProcessor.setUpdateMode("off");	
	coberturasRequeridasProcessor.init(asociados);
	
	var porAsociar = new dhtmlXGridObject('coberturasNoRequeridasGrid');
	porAsociar.setHeader(exclusionProductoHeader);
	porAsociar.setInitWidths("1,80,200,540");
	porAsociar
	.attachHeader("&nbsp;,#text_filter,&nbsp;,#text_filter,&nbsp;");
	porAsociar.setColAlign("left,left,left,left");
	porAsociar.setColSorting("int,str,str,str");
	porAsociar.setColTypes("ro,ro,ro,ro");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	porAsociar.enableDragAndDrop(true);
	porAsociar.setColumnHidden(0, true);
	porAsociar.setColumnHidden(2, true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.init();
	porAsociar.load(porAsociarPath + "?id=" + id, null, 'json');
}
function mostrarEndosoDeclaracion(idToCotizacion){
	var processorPath = '/MidasWeb/cotizacion/endoso/guardarEmbarque.do';
	var registrosAsociadosPath = "/MidasWeb/cotizacion/endoso/mostrarEmbarques.do";
	
	endosoDeclaracionGrid = new dhtmlXGridObject('declaracionEmbarquesDiv');
	endosoDeclaracionGrid.setHeader("idToCotizacion,No.Inciso,Secci\u00F3n,Per\u00EDodo de Declaraci\u00f3n,No. Total Embarques,Valor Embarques,Cuota,Prima Neta");
	endosoDeclaracionGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,periodo,numeroEmbarques,valor,cuota,primaNeta");
	endosoDeclaracionGrid.setInitWidths("1,70,100,150,150,150,100,150");
	endosoDeclaracionGrid.setColAlign("left,center,center,center,center,center,center,center");
	endosoDeclaracionGrid.setColTypes("ro,coro,coro,edn,edn,edn,ro,ro");
	endosoDeclaracionGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	endosoDeclaracionGrid.setSkin("light");
	endosoDeclaracionGrid.enableMultiline(true);
	endosoDeclaracionGrid.setNumberFormat("$0,000.00",5);
	endosoDeclaracionGrid.setNumberFormat("$0,000.00",7);	
	endosoDeclaracionGrid.setColumnHidden(0,true);
	endosoDeclaracionGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		if(stage == 2 && cInd == 1 ){
			var seccionCombo = endosoDeclaracionGrid.getCustomCombo(rId,2);
			var inciso =  endosoDeclaracionGrid.cellById(rId, cInd);
			getSecciones(idToCotizacion,inciso.getValue(),seccionCombo)	
		}	
		return true;
	});	
	endosoDeclaracionGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		var seccionCombo = endosoDeclaracionGrid.getCustomCombo(rowId,2);
		var inciso =  endosoDeclaracionGrid.cellById(rowId, 1);
		var seccion =  endosoDeclaracionGrid.cellById(rowId, 2);	
		getSecciones(idToCotizacion,inciso.getValue(),seccionCombo)	
		seccion.setValue(seccion.getValue());
		return true;
	});	
	endosoDeclaracionGrid.init();
	endosoDeclaracionGrid.load(registrosAsociadosPath+'?id='+idToCotizacion, null, 'json');

	endosoDeclaracionProcessor = new dataProcessor(processorPath);
	endosoDeclaracionProcessor.enableDataNames(true);
	endosoDeclaracionProcessor.setTransactionMode("POST");
	endosoDeclaracionProcessor.setUpdateMode("off");
	endosoDeclaracionProcessor.setVerificator(1, function(value,colName){
	    if(value >= 1){
	    	endosoDeclaracionError = false;
	        return true;
	    } 
	});	
	endosoDeclaracionProcessor.setVerificator(2, function(value,colName){
		if(value == '') {
			endosoDeclaracionError = true;
	        return false;
		} else if(value != ''){
	    	endosoDeclaracionError = false;
	        return true;
	    }
	});
	endosoDeclaracionProcessor.setVerificator(3, function(value,colName){
		if(value == '' || value == 0 ) {
			endosoDeclaracionError = true;
	        return false;
		} else if(value != ''){
	    	endosoDeclaracionError = false;
	        return true;
	    }
	});
	endosoDeclaracionProcessor.attachEvent("onAfterUpdateFinish", function(){
		
		if(endosoDeclaracionProcessor.getSyncState()){
			mostrarEndosoDeclaracion(idToCotizacion);
			//hideIndicator();
		}
		return true;
	});
	endosoDeclaracionProcessor.attachEvent("onBeforeDataSending",function(id){
		//showIndicatorSimple();
		return true;
	});	
	endosoDeclaracionProcessor.init(endosoDeclaracionGrid);
}
function agregarEmbarque(idToCotizacion){
	var incisoCombo;
	if (endosoDeclaracionGrid.getRowsNum() == 0 ) {
		endosoDeclaracionGrid.addRow(1,idToCotizacion+",,,,,,,");		
		incisoCombo = endosoDeclaracionGrid.getCustomCombo(1,1);
		getIncisos(idToCotizacion, incisoCombo)				
	} else {
		var secuencia = parseInt(endosoDeclaracionGrid.getRowsNum())+ 1;
		endosoDeclaracionGrid.addRow(secuencia,idToCotizacion+",,,,,,,");
		incisoCombo = endosoDeclaracionGrid.getCustomCombo(endosoDeclaracionGrid.getRowsNum(),1);
		getIncisos(idToCotizacion, incisoCombo)		
	}

}
function borrarEmbarque(){	
	if(endosoDeclaracionGrid.getSelectedId() == null){
		alert("Para eliminar un registro debe seleccionarlo primero.");
	}else{
		endosoDeclaracionGrid.deleteSelectedItem();
	}	
}
function esEndosoValido(){
	if(endosoDeclaracionGrid.getRowsNum() > 0){
		var nextFunction = "procesarRespuesta('listarCotizacionesEndoso();');mostrarEndosoDeclaracion("+document.cotizacionEndosoForm.idToCotizacion.value+")";
		parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/liberar.do','contenido_endoso', nextFunction);
	}else{
		alert("Debe agregar una Declaraci\u00f3n de Embarques para Liberar.");
		return false;
	}
}

function liberarCotizacionEndoso(){
	parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/liberar.do','contenido_endoso','procesaRespuestaLiberacionCotEndoso();');
}

function procesaRespuestaLiberacionCotEndoso(){
	if($('tipoMensaje')){
		if($('tipoMensaje').value === 30){
			mostrarVentanaMensaje($('tipoMensaje').value,$('mensaje').value,'listarCotizacionesEndoso();');
		 }else{
			mostrarVentanaMensaje($('tipoMensaje').value,$('mensaje').value,null);
		}
	}
	
}

function getIncisos(idToCotizacion, combo){
	new Ajax.Request("/MidasWeb/cotizacion/endoso/getIncisos.do", {
		method : "post",
		asynchronous : false,
		parameters :"id=" + idToCotizacion ,
		onSuccess : function(transport) {
			cargaComboGrid(combo,transport.responseXML);
		} // End of onSuccess
	});		
}
function getSecciones(idToCotizacion, numeroInciso, combo){
	new Ajax.Request("/MidasWeb/cotizacion/endoso/getSecciones.do", {
		method : "post",
		asynchronous : false,
		parameters :"id="+ idToCotizacion + "&numeroInciso=" + numeroInciso ,
		onSuccess : function(transport) {
			cargaComboGrid(combo,transport.responseXML);
		} // End of onSuccess
	});		
}
function mostrarCoasegurosCobertura(id,idToCobertura,idToSeccion){
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarCoaseguros.do';
	var registrosAsociadosPath = "/MidasWeb/configuracion/cobertura/mostrarCoaseguros.do";
	
	coasegurosCoberturaGrid = new dhtmlXGridObject('coasegurosCoberturaDiv');
	coasegurosCoberturaGrid.setHeader("idSeccion, idCobertura, idRiesgo,# Secuencia, Valor, Clave Default");
	coasegurosCoberturaGrid.setColumnIds("idSeccion,idCobertura,idRiesgo,numeroSecuencia,valor,claveDefault");
	coasegurosCoberturaGrid.setInitWidths("1,1,1,100,200,200");
	coasegurosCoberturaGrid.setColAlign("left,left,left,left,left,center");
	coasegurosCoberturaGrid.setColTypes("ro,ro,ro,ro,ed,ra");
	coasegurosCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coasegurosCoberturaGrid.setSkin("light");
	coasegurosCoberturaGrid.setColumnHidden(0,true);
	coasegurosCoberturaGrid.setColumnHidden(1,true);
	coasegurosCoberturaGrid.setColumnHidden(2,true);
	coasegurosCoberturaGrid.setColumnHidden(3,true);
	coasegurosCoberturaGrid.init();
	coasegurosCoberturaGrid.load(registrosAsociadosPath+'?id='+id+'&idPadre='+idToCobertura+'a'+idToSeccion, null, 'json');

	coasegurosCoberturaProcessor = new dataProcessor(processorPath);
	coasegurosCoberturaProcessor.enableDataNames(true);
	coasegurosCoberturaProcessor.setTransactionMode("POST");
	coasegurosCoberturaProcessor.setUpdateMode("cell");
	coasegurosCoberturaProcessor.setVerificator(4, function(value,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			coasegurosCoberturaError = true;
			return false;
		}*/
		if(value == '') {
			coasegurosCoberturaError = true;
	        return false;
		}
	    if(value >= 0){
	    	coasegurosCoberturaError = false;
	        return true;
	    } else {
	    	coasegurosCoberturaError = true;
	        return false;
	    }
	});
	
	coasegurosCoberturaProcessor.defineAction("duplicateValue",mensajeDuplicado);
	coasegurosCoberturaProcessor.attachEvent("onAfterUpdate", mensajeExito);
	coasegurosCoberturaProcessor.init(coasegurosCoberturaGrid);
	
}

function confirmCoaseguroCobertura(){
	var id = coasegurosCoberturaGrid.getSelectedId();
	if (id != null){
		if (confirm('\u00BFSeguro que quieres eliminar este registro?')){
			coasegurosCoberturaGrid.deleteSelectedItem();
		}
	}else{
		alert('Debes seleccionar un registro para eliminar. ');
	}
	
};

function mensajeDuplicado(node){
	mostrarVentanaMensaje('20', 'El valor ingresado esta duplicado');
	mostrarDeduciblesCobertura(jQuery("input[name='idToCobertura']").val(),0,0);
}

function mensajeErrorDeducibleCobertura(node){
	mostrarVentanaMensaje('10', 'Ocurrio un error inesperado');
	mostrarDeduciblesCobertura(jQuery("input[name='idToCobertura']").val(),0,0);
}

function mensajeExitoCobertura(node){
	mostrarMensajeExito();
	mostrarDeduciblesCobertura(jQuery("input[name='idToCobertura']").val(),0,0);
}

function mensajeExito(node){
	mostrarMensajeExito();
}

function agregarCoaseguroCobertura(idToRiesgo,idToCobertura,idToSeccion){
	if (coasegurosCoberturaGrid.getRowsNum() == 0 ) {
		coasegurosCoberturaGrid.addRow(1, idToSeccion + "," + idToCobertura + "," + idToRiesgo + ",1,,0");
	} else {
		var secuencia = parseInt(coasegurosCoberturaGrid.getRowId(coasegurosCoberturaGrid.getRowsNum() - 1)) + 1;
		coasegurosCoberturaGrid.addRow(secuencia, idToSeccion + "," + idToCobertura + "," + idToRiesgo + "," + secuencia + ",,0");
	}
}

function mostrarDeduciblesCobertura(id,idToCobertura,idToSeccion){
	var processorPath = '/MidasWeb/configuracion/cobertura/guardarDeducibles.do';
	var registrosAsociadosPath = "/MidasWeb/configuracion/cobertura/mostrarDeducibles.do";
	deduciblesCoberturaGrid = new dhtmlXGridObject('deduciblesCoberturaDiv');
	deduciblesCoberturaGrid.setHeader("idSeccion, idCobertura, idRiesgo,# Secuencia, Valor, Clave Default");
	deduciblesCoberturaGrid.setColumnIds("idSeccion,idCobertura,idRiesgo,numeroSecuencia,valor,claveDefault");
	deduciblesCoberturaGrid.setInitWidths("1,1,1,100,200,200");
	deduciblesCoberturaGrid.setColAlign("left,left,left,left,left,center");
	deduciblesCoberturaGrid.setColTypes("ro,ro,ro,ro,ed,ra");
	deduciblesCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	deduciblesCoberturaGrid.setSkin("light");
	deduciblesCoberturaGrid.setColumnHidden(0,true);
	deduciblesCoberturaGrid.setColumnHidden(1,true);
	deduciblesCoberturaGrid.setColumnHidden(2,true);
	deduciblesCoberturaGrid.setColumnHidden(3,true);
	deduciblesCoberturaGrid.init();
	deduciblesCoberturaGrid.load(registrosAsociadosPath+'?id='+id+'&idPadre='+idToCobertura+'a'+idToSeccion, null, 'json');

	deduciblesCoberturaProcessor = new dataProcessor(processorPath);
	deduciblesCoberturaProcessor.enableDataNames(true);
	deduciblesCoberturaProcessor.setTransactionMode("POST");
	deduciblesCoberturaProcessor.setUpdateMode("cell");
	deduciblesCoberturaProcessor.setVerificator(4, function(value,colName){
		if(value == ''){
			return false;
		}
		if(dwr.util.getValue("permiteDeducibleCero") == 1){
			return value >= 0;
		}
		return value > 0;
	});
	
	
	//iniciarManejoEventosDataProcessor(deduciblesCoberturaProcessor,'mostrarTabDeComponente(configuracionCoberturaTabBar, \'detalle\')');
	deduciblesCoberturaProcessor.defineAction("duplicateValue",mensajeDuplicado);
	deduciblesCoberturaProcessor.defineAction("excepcionJava",mensajeErrorDeducibleCobertura);
	deduciblesCoberturaProcessor.attachEvent("onAfterUpdate", mensajeExitoCobertura);
	deduciblesCoberturaProcessor.init(deduciblesCoberturaGrid);
}

function agregarDeducibleCobertura(idToRiesgo,idToCobertura,idToSeccion){
	if (deduciblesCoberturaGrid.getRowsNum() ==0 ) {
		deduciblesCoberturaGrid.addRow(1, idToSeccion + "," + idToCobertura + "," + idToRiesgo + ",1,,0");
	} else {
		var secuencia = parseInt(deduciblesCoberturaGrid.getRowId(deduciblesCoberturaGrid.getRowsNum() - 1)) + 1;
		deduciblesCoberturaGrid.addRow(secuencia, idToSeccion + "," + idToCobertura + "," + idToRiesgo + "," + secuencia + ",,0");
	}
}

/* Configuracion Riesgo */
var coasegurosRiesgoGrid;
var coasegurosRiesgoProcessor;
var coasegurosRiesgoError = false;

var deduciblesRiesgoGrid;
var deduciblesRiesgoProcessor;
var deduciblesRiesgoError = false;

function mostrarCoasegurosRiesgo(id,idToCobertura,idToSeccion){
	var processorPath = '/MidasWeb/configuracion/riesgo/guardarCoaseguro.do';
	var registrosAsociadosPath = "/MidasWeb/configuracion/riesgo/mostrarCoasegurosRegistrados.do";
	
	coasegurosRiesgoGrid = new dhtmlXGridObject('coasegurosRiesgoDiv');
	coasegurosRiesgoGrid.setHeader("idSeccion, idCobertura, idRiesgo,# Secuencia, Valor, Clave Default");
	coasegurosRiesgoGrid.setColumnIds("idSeccion,idCobertura,idRiesgo,numeroSecuencia,valor,claveDefault");
	coasegurosRiesgoGrid.setInitWidths("1,1,1,100,200,200");
	coasegurosRiesgoGrid.setColAlign("left,left,left,left,left,center");
	coasegurosRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ra");
	coasegurosRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coasegurosRiesgoGrid.setSkin("light");
	coasegurosRiesgoGrid.setColumnHidden(0,true);
	coasegurosRiesgoGrid.setColumnHidden(1,true);
	coasegurosRiesgoGrid.setColumnHidden(2,true);
	coasegurosRiesgoGrid.setColumnHidden(3,true);
	coasegurosRiesgoGrid.init();
	coasegurosRiesgoGrid.load(registrosAsociadosPath+'?id='+id+'&idPadre='+idToCobertura+'a'+idToSeccion, null, 'json');

	coasegurosRiesgoProcessor = new dataProcessor(processorPath);
	coasegurosRiesgoProcessor.enableDataNames(true);
	coasegurosRiesgoProcessor.setTransactionMode("POST");
	coasegurosRiesgoProcessor.setUpdateMode("cell");
	coasegurosRiesgoProcessor.setVerificator(4, function(value,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			coasegurosRiesgoError = true;
			return false;
		}*/
	    if(value > 0){
	    	coasegurosRiesgoError = false;
	        return true;
	    } else {
	    	coasegurosRiesgoError = true;
	        return false;
	    }
	});
	coasegurosRiesgoProcessor.init(coasegurosRiesgoGrid);
}

//Ya no se usa por la incidencia 73
function agregarCoaseguroRiesgo(idToRiesgo,idToCobertura,idToSeccion){
	if (coasegurosRiesgoGrid.getRowsNum() ==0 ) {
		coasegurosRiesgoGrid.addRow(1, idToSeccion + "," + idToCobertura + "," + idToRiesgo + ",1,,0");
	} else {
		var secuencia = parseInt(coasegurosRiesgoGrid.getRowId(coasegurosRiesgoGrid.getRowsNum() - 1)) + 1;
		coasegurosRiesgoGrid.addRow(secuencia, idToSeccion + "," + idToCobertura + "," + idToRiesgo + "," + secuencia + ",,0");
	}
}

function mostrarDeduciblesRiesgo(id,idToCobertura,idToSeccion){
	var processorPath = '/MidasWeb/configuracion/riesgo/guardarDeducible.do';
	var registrosAsociadosPath = "/MidasWeb/configuracion/riesgo/mostrarDeduciblesRegistrados.do";
	
	deduciblesRiesgoGrid = new dhtmlXGridObject('deduciblesRiesgoDiv');
	deduciblesRiesgoGrid.setHeader("idSeccion, idCobertura, idRiesgo,# Secuencia, Valor, Clave Default");
	deduciblesRiesgoGrid.setColumnIds("idSeccion,idCobertura,idRiesgo,numeroSecuencia,valor,claveDefault");
	deduciblesRiesgoGrid.setInitWidths("1,1,1,100,200,200");
	deduciblesRiesgoGrid.setColAlign("left,left,left,left,left,center");
	deduciblesRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ra");
	deduciblesRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	deduciblesRiesgoGrid.setSkin("light");
	deduciblesRiesgoGrid.setColumnHidden(0,true);
	deduciblesRiesgoGrid.setColumnHidden(1,true);
	deduciblesRiesgoGrid.setColumnHidden(2,true);
	deduciblesRiesgoGrid.setColumnHidden(3,true);
	deduciblesRiesgoGrid.init();
	deduciblesRiesgoGrid.load(registrosAsociadosPath+'?id='+id+'&idPadre='+idToCobertura+'a'+idToSeccion, null, 'json');

	deduciblesRiesgoProcessor = new dataProcessor(processorPath);
	deduciblesRiesgoProcessor.enableDataNames(true);
	deduciblesRiesgoProcessor.setTransactionMode("POST");
	deduciblesRiesgoProcessor.setUpdateMode("cell");
	deduciblesRiesgoProcessor.setVerificator(4, function(value,colName){
		/*var val = parseFloat(value.toString()._dhx_trim());
		if(isNaN(val)) {
			deduciblesRiesgoError = true;
			return false;
		}*/
	    if(value > 0){
	    	deduciblesRiesgoError = false;
	        return true;
	    } else {
	    	deduciblesRiesgoError = true;
	        return false;
	    }
	});
	deduciblesRiesgoProcessor.init(deduciblesRiesgoGrid);
}

//Ya no se usa por la incidencia 72
function agregarDeducibleRiesgo(idToRiesgo,idToCobertura,idToSeccion){
	if (deduciblesRiesgoGrid.getRowsNum() ==0 ) {
		deduciblesRiesgoGrid.addRow(1, idToSeccion + "," + idToCobertura + "," + idToRiesgo + ",1,,0");
	} else {
		var secuencia = parseInt(deduciblesRiesgoGrid.getRowId(deduciblesRiesgoGrid.getRowsNum() - 1)) + 1;
		deduciblesRiesgoGrid.addRow(secuencia, idToSeccion + "," + idToCobertura + "," + idToRiesgo + "," + secuencia + ",,0");
	}
}

/* Otros */
var paisesAsociadosTipoDestinoTransporte;
var paisesPorAsociarTipoDestinoTransporte;
var processor;
var asociadosPath;
var porAsociarPath;

function initPaisPorTipoDestinoTransporteGrids(){
	processor = new dataProcessor("/MidasWeb/catalogos/paistipodestinotransporte/agregar.do");
	asociadosPath = '/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesAsociados.do';
	porAsociarPath = '/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesPorAsociar.do';
}

function mostrarGridsPaisTipoDestinoTransporte(idTipoDestinoTransporte) {   
	asociados = new dhtmlXGridObject('paisesAsociadosGrid'); // debe coincidir con el nombre del div contenedor
	asociados.setHeader("idTipoDestino, Id del Pa\u00EDs, Pa\u00EDs");
	asociados.setColumnIds("idTipoDestinoTransporte,idPais,countryName");
	asociados.setInitWidths("200,200,200");
	asociados.setColAlign("center,center,center");
	asociados.setColSorting("int,str,str");
	asociados.setColTypes("ro,ro,ro");
	asociados.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	asociados.setSkin("light");
	asociados.setColumnHidden(0, true);
	asociados.enableDragAndDrop(true);
	asociados.enableLightMouseNavigation(true);
	asociados.init();
	asociados.load(asociadosPath + '?idTipoDestinoTransporte=' + idTipoDestinoTransporte, null, 'json');

	processor.enableDataNames(true);
	processor.setTransactionMode("POST", false);
	processor.setUpdateMode("off");
	processor.init(asociados);

	porAsociar = new dhtmlXGridObject('paisesPorAsociarGrid');
	porAsociar.setHeader("idTipoDestino,Id del Pa\u00EDs, Pa\u00EDs");
	porAsociar.setColumnIds("idTipoDestinoTransporte,idPais,countryName");
	porAsociar.setInitWidths("200,200,200");
	porAsociar.setColAlign("center,center,center");
	porAsociar.setColSorting("int,str,str");
	porAsociar.setColTypes("ro,ro,ro");
	porAsociar.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	porAsociar.setSkin("light");
	porAsociar.setColumnHidden(0, true);
	porAsociar.enableDragAndDrop(true);
	porAsociar.enableLightMouseNavigation(true);
	porAsociar.init();
	porAsociar.load(porAsociarPath + '?idTipoDestinoTransporte=' + idTipoDestinoTransporte, null, 'json');
}

function myErrorHandler(obj){
    alert("Ocurrio un error.\n" + obj.firstChild.nodeValue);
    error = true;
    processor.stopOnError = true;
    return false;
}

function checkIfNotZero(value,colName){
	/*var val = parseFloat(value.toString()._dhx_trim());
	if(isNaN(val)) {
		error = true;
		return false;
	}*/
    if(value > 0){
    	error = false;
        return true;
    } else {
    	error = true;
        return false;
    }
}

function checkIfNotEmpty(value,colName){
	var val = value.toString()._dhx_trim();
    if(val.length <= 0){
    	error = true;
        return false
    } else {
    	error = false;
        return true;
    }
}

function cargaDataGridproductoHijos(idEntidad,idPadre,mostrarBorrados){
	var tipoPolizaGrid = new dhtmlXGridObject('tipoPolizaGrid');
	
	tipoPolizaGrid.setHeader("C&oacute;digo,Nombre,Descripci&oacute;n,Acciones,#cspan,#cspan");
	tipoPolizaGrid.setColumnIds("codigo,nombre,descripcion,detalle,modificar,borrar");
	tipoPolizaGrid.setInitWidths("55,250,*,30,30,30");
	tipoPolizaGrid.setColAlign("left,left,left,center,center,center");
	tipoPolizaGrid.setColSorting("str,str,str,str,str,str");
	tipoPolizaGrid.setColTypes("ro,ro,ro,img,img,img");
	tipoPolizaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	tipoPolizaGrid.setSkin("light");
	
	tipoPolizaGrid.enablePaging(true,5,3,"pagingArea",true,"infoArea");
	tipoPolizaGrid.setPagingSkin("bricks");
	
	tipoPolizaGrid.enableDragAndDrop(false);	
	tipoPolizaGrid.enableLightMouseNavigation(true);
	tipoPolizaGrid.init();
	
	
	//tipoPolizaGrid.load("/MidasWeb/configuracion/tipopoliza/listarJsonPorPradre.do?id=" + idEntidad+"&verInactivos=" 
	//		+ GLOBAL_VER_ELEMENTOS_INACTIVOS, null, 'json');
	
	
	if (mostrarBorrados != undefined && mostrarBorrados != null)
		tipoPolizaGrid.load("/MidasWeb/configuracion/tipopoliza/listarJsonPorPradre.do?id=" + idEntidad+"&verInactivos=" +mostrarBorrados, null, 'json');
	else
		tipoPolizaGrid.load("/MidasWeb/configuracion/tipopoliza/listarJsonPorPradre.do?id=" + idEntidad+"&verInactivos=" 
				+ GLOBAL_VER_ELEMENTOS_INACTIVOS, null, 'json');
		
}

function cargaDataGridproductoBorrados(idProducto,check){
	cargaDataGridproductoHijos(idProducto,null,check.checked);
	
}

function cargaDataGridtipopolizaHijos(idEntidad,idPadre,mostrarBorrados){
	var seccionGrid = new dhtmlXGridObject('seccionGrid');
	
	seccionGrid.setHeader("C\u00F3digo,Versi\u00F3n,Nombre,Descripci\u00F3n,Acciones,#cspan,#cspan");
	seccionGrid.setColumnIds("codigo,version,nombre,descripcion,detalle,modificar,borrar");
	seccionGrid.setInitWidths("55,55,*,200,30,30,30");
	seccionGrid.setColAlign("left,left,left,left,center,center,center");
	seccionGrid.setColSorting("str,str,str,str,str,str,str");
	seccionGrid.setColTypes("ro,ro,ro,ro,img,img,img");
	seccionGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionGrid.setSkin("light");
	
	seccionGrid.enablePaging(true,5,3,"pagingArea",true,"infoArea");
	seccionGrid.setPagingSkin("bricks");
	
	seccionGrid.enableDragAndDrop(false);	
	seccionGrid.enableLightMouseNavigation(true);
	seccionGrid.init();
	
	if (mostrarBorrados != undefined && mostrarBorrados != null)
		seccionGrid.load("/MidasWeb/configuracion/seccion/listarJsonPorPradre.do?id=" + idEntidad+"&verInactivos=" 
				+ mostrarBorrados, null, 'json');
	else
		seccionGrid.load("/MidasWeb/configuracion/seccion/listarJsonPorPradre.do?id=" + idEntidad+"&verInactivos=" 
				+ GLOBAL_VER_ELEMENTOS_INACTIVOS, null, 'json');
}

function cargaDataGridseccionHijos(idEntidad,idPadre,mostrarBorrados){
	var seccionGrid = new dhtmlXGridObject('coberturaGrid');
	
	seccionGrid.setHeader("C\u00F3digo,Nombre,Descripci\u00F3n,Es Obligatorio");
	seccionGrid.setColumnIds("codigo,nombre,descripcion,obligario");
	seccionGrid.setInitWidths("55,167,*,70");
	seccionGrid.setColAlign("left,left,left,left");
	seccionGrid.setColSorting("str,str,str,str");
	seccionGrid.setColTypes("ro,ro,ro,ro");
	seccionGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionGrid.setSkin("light");
	
	seccionGrid.enablePaging(true,5,3,"pagingArea",true,"infoArea");
	seccionGrid.setPagingSkin("bricks");
	
	seccionGrid.enableDragAndDrop(false);	
	seccionGrid.enableLightMouseNavigation(true);
	seccionGrid.init();
	seccionGrid.load("/MidasWeb/configuracion/cobertura/listarJsonPorPradre.do?id=" + idEntidad, null, 'json');
}

function cargaDataGridcoberturaHijos(idEntidad,idPadre,mostrarBorrados){
	var seccionGrid = new dhtmlXGridObject('riesgoGrid');
	
	seccionGrid.setHeader("C\u00F3digo,Nombre,Descripci\u00F3n");
	seccionGrid.setColumnIds("codigo,nombre,descripcion");
	seccionGrid.setInitWidths("55,165,*");
	seccionGrid.setColAlign("left,left,left");
	seccionGrid.setColSorting("str,str,str");
	seccionGrid.setColTypes("ro,ro,ro");
	seccionGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionGrid.setSkin("light");
	
	seccionGrid.enablePaging(true,5,3,"pagingArea",true,"infoArea");
	seccionGrid.setPagingSkin("bricks");
	
	seccionGrid.enableDragAndDrop(false);	
	seccionGrid.enableLightMouseNavigation(true);
	seccionGrid.init();
	seccionGrid.load("/MidasWeb/configuracion/riesgo/listarJsonPorPradre.do?id=" + idEntidad + "&idPadre=" + idPadre, null, 'json');
}
/** Funciones para Primer Riesgo y LUC **/

var accordionPrimerRiesgoLUC;


function inicializaPrimerRiesgoLUC(idToCotizacion){
	accordionPrimerRiesgoLUC = new dhtmlXAccordion("accordionPrimerRiesgoLUC");
	accordionPrimerRiesgoLUC.addItem("a1", "Primer Riesgo");
	accordionPrimerRiesgoLUC.addItem("a2", "LUC");
	accordionPrimerRiesgoLUC.cells("a1").attachURL("/MidasWeb/cotizacion/primerRiesgoLUC/mostrarPrimerRiesgo.do?id="+idToCotizacion);
	accordionPrimerRiesgoLUC.cells("a2").attachURL("/MidasWeb/cotizacion/primerRiesgoLUC/mostrarLUC.do?id="+idToCotizacion);
	accordionPrimerRiesgoLUC.openItem("a1");
	accordionPrimerRiesgoLUC.setEffect(true);
/*
	var primerRiesgo = accordionPrimerRiesgoLUC.cells("a1").attachGrid();
	primerRiesgo.setHeader("&nbsp;,Seccion, Totales, Totales a 1er Riesgo");
	primerRiesgo.setColumnIds("check,descripcionSeccion,total,totalPrimerRiesgo");
	primerRiesgo.setInitWidths("30,200,200,200");
	primerRiesgo.setColAlign("center,left,center,center");
	primerRiesgo.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	primerRiesgo.setSkin("light");
	primerRiesgo.setColSorting("int,str,int,int");
	primerRiesgo.setColTypes("ch,ro,ro,ro");
	//primerRiesgo.attachEvent("onEditCell",totalizar);
	primerRiesgo.enableLightMouseNavigation(true);
	primerRiesgo.enableMultiline(true);	
	primerRiesgo.init();
	primerRiesgo.load('/MidasWeb/cotizacion/primerRiesgoLUC/datosPrimerRiesgo.do?id='+ idCotizacion, null, 'json');	
*/	
}
/** Fin de Funciones Primer Riesgo y LUC **/
var clientesGrid=null;

/** Registrar Ingresos **/


var registrarIGrid ;
var ingresoReaseguroSelec = "undefined";
function registrarIngresosGrid(){
	registrarIGrid = new dhtmlXGridObject('registrarIGrid');
	
	registrarIGrid.setHeader("Id, Monto Del Ingreso,,Moneda,Referencia,Num Cuenta,Fecha,,");
	registrarIGrid.setColumnIds("id,ingreso,ingresoOrig,moneda,referencia,numcuenta,fecha,fechaOrig,seleccionar");
	registrarIGrid.setInitWidths("0, 160, 0, 80, 150, 140, 200, 0,*");
	registrarIGrid.setColAlign("center,center,center,center,center,center,center,center,center");
	registrarIGrid.setColSorting("str,str,str,str,str,str,str,str,str");
	registrarIGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ra");
	registrarIGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	registrarIGrid.setSkin("light");
	
	registrarIGrid.enableDragAndDrop(false);	
	registrarIGrid.enableLightMouseNavigation(false);
	registrarIGrid.init();
	registrarIGrid.attachEvent("onCheck", ingresoReaseguroSeleccionado);
	registrarIGrid.load('/MidasWeb/contratos/ingresos/listarIngresosReaseguro.do', null, 'json');
	ingresoReaseguroSelec = "undefined";
}


function modificarRegistrarIngresosReaseguro(){
	encontrarIngresoReaseguroSeleccionado();
	if(ingresoReaseguroSelec != null){
		modificarRegistrarIngresos(ingresoReaseguroSelec, registrarIGrid.cellById(ingresoReaseguroSelec,2).getValue(), registrarIGrid.cellById(ingresoReaseguroSelec,3).getValue(), registrarIGrid.cellById(ingresoReaseguroSelec,4).getValue(), registrarIGrid.cellById(ingresoReaseguroSelec,5).getValue(), registrarIGrid.cellById(ingresoReaseguroSelec,7).getValue());
	}
}

function ingresoReaseguroSeleccionado(rId, cId, state){
	ingresoReaseguroSelec = rId;
}

function encontrarIngresoReaseguroSeleccionado(){
	if((!registrarIGrid.doesRowExist(ingresoReaseguroSelec+"")) || ingresoReaseguroSelec == "undefined"){
		var va = 0;
		while(true){
			if(!(registrarIGrid.doesRowExist(registrarIGrid.getRowId(va)))){
				ingresoReaseguroSelec = null;
				break;
			}
			var riId = registrarIGrid.getRowId(va);
			if(registrarIGrid.cellById(riId,6).getValue() == 1){
				ingresoReaseguroSelec = riId;
				break;
			}	
			va = va + 1;
		}
	}
}

function borrarIngresoReaseguro(){
	encontrarIngresoReaseguroSeleccionado();
	if(ingresoReaseguroSelec != null){
		if(confirm(String.fromCharCode(191) + "Desea borrar el ingreso seleccionado?")){
			sendRequest(null,'/MidasWeb/contratos/ingresos/borrarIngresosReaseguro.do?idIngresoReaseguro='+ingresoReaseguroSelec,null,null);
			registrarIGrid.deleteRow(ingresoReaseguroSelec);
			ingresoReaseguroSelec = "undefined";
		}
	}
}

function adjuntaParametrosURLFiltradoClientes(tipo){
	var loadURL = "/MidasWeb/cliente/listarFiltrado.do";
	var nombre;
	var apPaterno;
	var apMaterno;
	var rfc;
	var numeroAsegurado;
	if (tipo=='fisica'){
		nombre = trim(document.getElementById('cajaNombre').value);
		apPaterno = trim(document.getElementById('cajaApellidoPaterno').value);
		apMaterno = (document.getElementById('cajaApellidoMaterno').value);
		rfc = trim(document.getElementById('cajaCodigoRFC').value);
		numeroAsegurado = trim(document.getElementById('cajaNumeroAsegurado').value);
		if (nombre == '' && apPaterno == '' && apMaterno=='' && rfc=='' && numeroAsegurado == ''){
			alert("Introduzca por lo menos un dato.");
			return "error";
		}
		loadURL += "?tipoPersona=fisica"
		if (nombre != null && nombre != undefined && nombre != ''){
			loadURL += "&nombre=" + nombre;
		}
		if (apPaterno != null && apPaterno != undefined && apPaterno != ''){
			loadURL += "&apellidoPaterno=" + apPaterno;
		}
		if(apMaterno != null && apMaterno != undefined && apMaterno != ''){
			loadURL += "&apellidoMaterno=" + apMaterno;
		}
		if(rfc != null && rfc != undefined && rfc != ''){
			loadURL += "&rfc=" + rfc;
		}
		if(numeroAsegurado != null && numeroAsegurado != undefined && numeroAsegurado != ''){
			loadURL += "&numeroAsegurado=" + numeroAsegurado;
		}
	}
	else if (tipo == 'moral'){
		nombre = trim(document.getElementById('cajaNombreMoral').value);
		rfc = trim(document.getElementById('cajaCodigoRFCMoral').value);
		numeroAsegurado = trim(document.getElementById('cajaNumeroAseguradoMoral').value);
		if (nombre == '' && rfc=='' && numeroAsegurado == ''){
			alert("Introduzca por lo menos un dato.");
			return "error";
		}
		loadURL += "?tipoPersona=moral"
		if (nombre != null && nombre != undefined && nombre != ''){
			loadURL += "&nombre=" + nombre;
		}
		if(rfc != null && rfc != undefined && rfc != ''){
			loadURL += "&rfc=" + rfc;
		}
		if(numeroAsegurado != null && numeroAsegurado != undefined && numeroAsegurado != ''){
			loadURL += "&numeroAsegurado=" + numeroAsegurado;
		}
	}
	return loadURL;
}

function cargarClientesFiltradoDataGrid(tipo){
	var URL;
	if (tipo == 'fisica'){
		URL = adjuntaParametrosURLFiltradoClientes('fisica');
		if (URL == 'error'){
			return;
		}
		clientesGrid = new dhtmlXGridObject('clientesGrid')
		clientesGrid.setHeader("Nombre,Direcci\u00F3n,RFC");
	}
	else if (tipo == 'moral'){
		URL = adjuntaParametrosURLFiltradoClientes('moral');
		if (URL == 'error'){
			return;
		}
		clientesGrid = new dhtmlXGridObject('clientesGrid')
		clientesGrid.setHeader("Raz\u00F3n Social,Direcci\u00F3n,RFC");
	}
	else	return;
	clientesGrid.setColumnIds("nombre,direccion,rfc");
	clientesGrid.setInitWidths("250,*,130");
	clientesGrid.setColAlign("left,left,left");
	clientesGrid.setColSorting("str,str,str");
	clientesGrid.setColTypes("ro,ro,ro");
	clientesGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	clientesGrid.setSkin("light");
	clientesGrid.enablePaging(true,14,5,"pagingArea",true,"infoArea");
	clientesGrid.setPagingSkin("bricks");
	clientesGrid.enableLightMouseNavigation(false);
	clientesGrid.attachEvent("onRowDblClicked",function(rowId,cellInd) {
		//var nombre = clientesGrid.cellById(rowId,0).getValue();
		var idCliente = rowId;
		var idCotizacion = document.getElementById("idCotizacion").value;
		var codigoCliente = document.getElementById("codigoPersona").value;
		new Ajax.Request('/MidasWeb/cotizacion/asignarCliente.do?idToCotizacion='+idCotizacion+"&idCliente="+idCliente+"&codigoCliente="+codigoCliente,{
			method : "post",
			asynchronous : false,
			onSuccess : function(transport) {
				var xmlDoc = transport.responseXML;
				var items = xmlDoc.getElementsByTagName("item");
				var item = items[0];
				var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
				var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
				parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
				alert(text);
				parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
				parent.ventanaPersona.close();
				parent.accordionOT.openItem("a2");
				parent.accordionOT.openItem("a1");
			}
		});
		return true;
	});
	clientesGrid.init();
	clientesGrid.load(URL, null, 'json');
}

var reportePerfilCartera;
function reportePerfilCarteraGrid(){
	reportePerfilCartera= new dhtmlXGridObject('listarReportePerfilCartera');
	reportePerfilCartera.setHeader("Id,Posici\u00F3n,Rango Inferior,Rango Superior, Seleccionar");
	reportePerfilCartera.setColumnIds("id,pos,ri,rs,sl");
	reportePerfilCartera.setInitWidths("0,0,200,200,100");
	reportePerfilCartera.setColAlign("left,left,left,left,center");
	reportePerfilCartera.setColTypes("ro,ro,ro,ro,ra");
	reportePerfilCartera.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	reportePerfilCartera.setSkin("light");
	reportePerfilCartera.enableDragAndDrop(false);
	reportePerfilCartera.enableLightMouseNavigation(true);
	reportePerfilCartera.setColumnHidden(0, true);
	reportePerfilCartera.setColumnHidden(1, true);
	reportePerfilCartera.attachEvent("onCheck", rangoSeleccionado);
	reportePerfilCartera.init();
	reportePerfilCartera.clearAndLoad('/MidasWeb/contratos/cartera/rPC.do',habilitarEditar,'json');
	
}
var rangoSelec;
function rangoSeleccionado(rId, cId, state){
	rangoSelec = rId;
}

function habilitarEditar(){
	var total = 1;
	while(reportePerfilCartera.doesRowExist((total+1)+"")){
		total = total + 1;
	}
	reportePerfilCartera.setCellExcellType(total, 3, "ed");
}

function editarPerfilCartera(){
	var total = 1;
	while(reportePerfilCartera.doesRowExist((total+1)+"")){
		total = total + 1;
	}
	var newId = total +"";
	var cellobj = reportePerfilCartera.cellById(newId, 3);
	var cellobj2 = reportePerfilCartera.cellById(newId, 2);
	var valor = parseInt(cellobj.getValue() + "");
	var valor2 = parseInt(cellobj2.getValue() + "");
	var val1 = false;
	if(valor == ""){
		reportePerfilCartera.setCellTextStyle(newId, 3, "color:red;");
	}else if((valor + "")== cellobj.getValue()){
		reportePerfilCartera.setCellTextStyle(newId, 3, "color:black;");
		val1 = true;
	}else{
		reportePerfilCartera.setCellTextStyle(newId, 3, "color:red;");
	}
	if((valor2 < valor) && val1){
		reportePerfilCartera.setCellExcellType(total, 3, "ro");
		newId = parseInt(newId) +1;
		pos = parseInt(reportePerfilCartera.cellById(newId-1, 1).getValue()+"") + 1;
		sendRequest(null, '/MidasWeb/contratos/cartera/agregarRangoSumaAsegurada.do?idTcRangoSumaAsegurada='+(newId -1)+'&posicion='+(pos-1)+'&rango='+ valor,null,null);
		reportePerfilCartera.addRow(newId+"",""+newId+","+pos +","+ (valor + 1)+",Nuevo Rango,Seleccionar", newId);
		reportePerfilCartera.setCellExcellType(newId, 3, "ed");
		return true;
	}else {
		reportePerfilCartera.setCellTextStyle(newId, 3, "color:red;");
		return false;
	}
}

function borrarPerfilCarteraGrid(){
	if(rangoSelec != null && rangoSelec != "0"){
		var total = 1;
		while(reportePerfilCartera.doesRowExist((total+1)+"")){
			total = total + 1;
		}
		if(rangoSelec!=(total+"")){
			if(confirm("Desea Borrar el rango de " + reportePerfilCartera.cellById(rangoSelec+"",2).getValue()+ " a " + reportePerfilCartera.cellById(rangoSelec+"",3).getValue())){
				sendRequest(null,'/MidasWeb/contratos/cartera/borrarRangoSumaAsegurada.do?idTcRangoSumaAsegurada='+rangoSelec,null,null);
				var i = parseInt(rangoSelec);
				reportePerfilCartera.cellById(rangoSelec,3).setValue(reportePerfilCartera.cellById((i+1)+"",3).getValue());
				i = i + 1;
				while(i < total){
					reportePerfilCartera.cellById(i+"",2).setValue(reportePerfilCartera.cellById((i+1)+"",2).getValue());
					reportePerfilCartera.cellById(i+"",3).setValue(reportePerfilCartera.cellById((i+1)+"",3).getValue());
					i = i + 1;
				}
				reportePerfilCartera.setCellExcellType(i-1, 3, "ed");
				reportePerfilCartera.deleteRow(i+"");

			}
		}
	}
}
/**
 * Componentes para configuracion de paquetes
 */

var coberturasRegistradasPaqGrid;
var coberturasDisponiblesPaqGrid;
var registroCoberturasProcessor;
var liberarPaqueteProcessor;
var eliminarPaqueteProcessor;
var definirPaqueteDefaultProcessor;
var gridPaquetes;

function mostrarPaquetesTipoPoliza(idToTipoPoliza){
	if(idToTipoPoliza == null || idToTipoPoliza == undefined || idToTipoPoliza == ''){
		var objidPol = document.getElementById("idToTipoPoliza");
		if(objidPol != null )
			idToTipoPoliza = objidPol.value;
	}
	var urlConsulta = '/MidasWeb/configuracion/tipopoliza/obtenerPaquetesPorTipoPoliza.do?id='+ idToTipoPoliza;
	gridPaquetes = new dhtmlXGridObject('configuracionPaquetesGrid');
	
	gridPaquetes.customGroupFormat=function(text,count){
        return text;
    };
	
	gridPaquetes.attachEvent("onXLE", function(grid){
		gridPaquetes.groupBy(1,["","#cspan","#title","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
    });
	
	gridPaquetes.load(urlConsulta);
	
	//Creacion de los DataProcessor
	instanciarLiberarPaquetePolizaProcessor(true);

	instanciarEliminarPaquetePolizaProcessor(true);
	
	instanciarDefaultPaquetePolizaProcessor(true);
}

function instanciarLiberarPaquetePolizaProcessor(forzarInstancia){
	if( liberarPaqueteProcessor == undefined || liberarPaqueteProcessor == null || forzarInstancia == true){
		liberarPaqueteProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/paquete/liberarPaquetePoliza.do');
		liberarPaqueteProcessor.enableDataNames(true);
		liberarPaqueteProcessor.setTransactionMode("POST");
		liberarPaqueteProcessor.setUpdateMode("off");
		liberarPaqueteProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
		liberarPaqueteProcessor.init(gridPaquetes);
	}
	else if(liberarPaqueteProcessor.obj == null){
		liberarPaqueteProcessor.init(gridPaquetes);
		liberarPaqueteProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
	}
}

function instanciarEliminarPaquetePolizaProcessor(forzarInstancia){
	if(eliminarPaqueteProcessor == undefined || eliminarPaqueteProcessor == null || forzarInstancia == true){
		eliminarPaqueteProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/paquete/eliminarPaquetePoliza.do');
		eliminarPaqueteProcessor.enableDataNames(true);
		eliminarPaqueteProcessor.setTransactionMode("POST");
		eliminarPaqueteProcessor.setUpdateMode("off");
		eliminarPaqueteProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
		eliminarPaqueteProcessor.init(gridPaquetes);
	}
	else if(eliminarPaqueteProcessor.obj == null){
		eliminarPaqueteProcessor.init(gridPaquetes);
		eliminarPaqueteProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
	}
}

function instanciarDefaultPaquetePolizaProcessor(forzarInstancia){
	if(definirPaqueteDefaultProcessor == undefined || definirPaqueteDefaultProcessor == null || forzarInstancia == true){
		definirPaqueteDefaultProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/paquete/definirPaquetePoliza.do');
		definirPaqueteDefaultProcessor.enableDataNames(true);
		definirPaqueteDefaultProcessor.setTransactionMode("POST");
		definirPaqueteDefaultProcessor.setUpdateMode("off");
		definirPaqueteDefaultProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
		definirPaqueteDefaultProcessor.init(gridPaquetes);
	}
	else if(definirPaqueteDefaultProcessor.obj == null){
		definirPaqueteDefaultProcessor.init(gridPaquetes);
		definirPaqueteDefaultProcessor.attachEvent("onAfterUpdate",mostrarMensajeModificacionPaquete);
	}
}

function modificarPaqueteCliente(idToTipoPoliza,idToPaquetePoliza){
	var urlMostrarModificar='/MidasWeb/configuracion/tipopoliza/paquete/desplegarPaquete.do' +
		'?claveAccion=editarPaquete&idToTipoPoliza='+idToTipoPoliza+'&paquetePolizaForm.idToPaquetePoliza='+idToPaquetePoliza;
	var afterRequestFunction = 'poblarCoberturasRegistradas('+idToTipoPoliza+','+idToPaquetePoliza+');poblarCoberturasDisponibles('+idToTipoPoliza+','+idToPaquetePoliza+');';
	sendRequest(null,urlMostrarModificar, 'contenido_paquetes',afterRequestFunction);
}

function liberarPaquetePoliza(idToPaquetePoliza){
	instanciarLiberarPaquetePolizaProcessor();
	var cell = gridPaquetes.findCell(idToPaquetePoliza, 0, true);
	var selectedId = cell[0][0];
	liberarPaqueteProcessor.setUpdated(selectedId,true);
	liberarPaqueteProcessor.sendData();
}

function eliminarPaquetePoliza(idToPaquetePoliza){
	if(confirm("Esta opci\u00f3n borrar\u00e1 por completo el Paquete, desea continuar?")){
		instanciarEliminarPaquetePolizaProcessor();
		var cell = gridPaquetes.findCell(idToPaquetePoliza, 0, true);
		var selectedId = cell[0][0];
		eliminarPaqueteProcessor.setUpdated(selectedId,true);
		eliminarPaqueteProcessor.sendData();
	}
}

function definirPaqueteDefault(idToPaquetePoliza){
	if(confirm("Esta opci\u00f3n establecer\u00e1 al paquete seleccionado como el predeterminado para el tipo de p\u00f3liza, desea continuar?")){
		instanciarDefaultPaquetePolizaProcessor();
		var cell = gridPaquetes.findCell(idToPaquetePoliza, 0, true);
		var selectedId = cell[0][0];
		definirPaqueteDefaultProcessor.setUpdated(selectedId,true);
		definirPaqueteDefaultProcessor.sendData();
	}
}

function marcarRegistroEliminarCobPaqGrid(rowId){
	registroCoberturasProcessor.setUpdated(rowId, true, "deleted");
	registroCoberturasProcessor.sendData(rowId);
}

function mostrarMensajeRegistroCoberturaPaq(sid,action,tid,node){
	var tipoRespuesta = node.getAttribute("type");
	var mensajeError = node.firstChild.data;
	var tipoMensaje = node.getAttribute("tipoMensaje");
	var idToPoliza = document.getElementById('idToTipoPoliza').value;
	var idToPaquetePoliza = document.getElementById('idToPaquetePoliza').value;
	
	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
		mostrarVentanaMensaje(tipoMensaje, mensajeError);
	}
	
	poblarCoberturasRegistradas(idToPoliza,idToPaquetePoliza);
	poblarCoberturasDisponibles(idToPoliza,idToPaquetePoliza);
	return true; 
}

function mostrarMensajeModificacionPaquete(sid,action,tid,node){
	var tipoRespuesta = node.getAttribute("type");
	var mensajeError = node.firstChild.data;
	var tipoMensaje = node.getAttribute("tipoMensaje");
	var idToPoliza = node.getAttribute('idToTipoPoliza');
	
	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
		mostrarVentanaMensaje(tipoMensaje, mensajeError);
	}
	
	mostrarPaquetesTipoPoliza(idToPoliza);
	return true; 
}

function poblarCoberturasRegistradas(idToTipoPoliza,idToPaquetePoliza){
	if(idToTipoPoliza == null || idToTipoPoliza == undefined || idToTipoPoliza == ''){
		var objidPol = document.getElementById("idToTipoPoliza");
		if(objidPol != null )
			idToTipoPoliza = objidPol.value;
	}
	if(idToPaquetePoliza == null || idToPaquetePoliza == undefined || idToPaquetePoliza == ''){
		var objidPaq = document.getElementById("idToPaquetePoliza");
		if(objidPaq != null )
			idToPaquetePoliza = objidPaq.value;
	}
	var urlCoberturasRegistradas = '/MidasWeb/configuracion/tipopoliza/paquete/obtenerCoberturasRegistradas.do?' +
		'idToTipoPoliza='+ idToTipoPoliza+'&paquetePolizaForm.idToPaquetePoliza='+idToPaquetePoliza;
	
	coberturasRegistradasPaqGrid = new dhtmlXGridObject('coberturasRegistradasGrid');
	
	coberturasRegistradasPaqGrid.load(urlCoberturasRegistradas);
	
	//Creacion del DataProcessor
	registroCoberturasProcessor = new dataProcessor('/MidasWeb/configuracion/tipopoliza/paquete/guardarCoberturaPaquete.do');

	registroCoberturasProcessor.enableDataNames(true);
	registroCoberturasProcessor.setTransactionMode("POST");
	registroCoberturasProcessor.setUpdateMode("cell");
	
	registroCoberturasProcessor.attachEvent("onAfterUpdate",mostrarMensajeRegistroCoberturaPaq);
	
	registroCoberturasProcessor.init(coberturasRegistradasPaqGrid);
}


function poblarCoberturasDisponibles(idToTipoPoliza,idToPaquetePoliza){
	if(idToTipoPoliza == null || idToTipoPoliza == undefined || idToTipoPoliza == ''){
		var objidPol = document.getElementById("idToTipoPoliza");
		if(objidPol != null )
			idToTipoPoliza = objidPol.value;
	}
	if(idToPaquetePoliza == null || idToPaquetePoliza == undefined || idToPaquetePoliza == ''){
		var objidPaq = document.getElementById("idToPaquetePoliza");
		if(objidPaq != null )
			idToPaquetePoliza = objidPaq.value;
	}
	var urlCoberturasDisponibles = '/MidasWeb/configuracion/tipopoliza/paquete/obtenerCoberturasDisponibles.do?' +
		'idToTipoPoliza='+ idToTipoPoliza+'&paquetePolizaForm.idToPaquetePoliza='+idToPaquetePoliza;
	
	coberturasDisponiblesPaqGrid = new dhtmlXGridObject('coberturasDisponiblesGrid');
	
	coberturasDisponiblesPaqGrid.load(urlCoberturasDisponibles);
}

function mostrarMensajeUsuarioRegistroPaquete(){
	var objDiv = document.getElementById("errorRegistroPaquete");
	if(objDiv != null){
		var mensaje = objDiv.innerHTML;
		if(mensaje != null && mensaje != undefined){
			if(trim(mensaje) != ''){
				mostrarVentanaMensaje("10", mensaje);
			}
		}
	}
}

/**
 * Finaliza componentes para configuracion de paquetes
 */


/** Funciones Documentos Referencias **/
var documentoAnexoTipoReferenciaProcessor;
var documentosAnexosRefGrid;

function obtenerDocumentosReferencias(){
	documentosAnexosRefGrid = new dhtmlXGridObject('documentosAnexosGrid');
	documentosAnexosRefGrid.load("/MidasWeb/documentos/referencias/obtenerDocumentosReferencias.action?idToSeccion="+ dwr.util.getValue("idToSeccion"));
	documentoAnexoTipoReferenciaProcessor = new dataProcessor("/MidasWeb/documentos/referencias/actualizarDocumentosReferencias.action");
	documentoAnexoTipoReferenciaProcessor.setUpdateMode("off");
	documentoAnexoTipoReferenciaProcessor.enableDataNames(true);
	documentoAnexoTipoReferenciaProcessor.setTransactionMode("POST");
	documentoAnexoTipoReferenciaProcessor.defineAction("mensaje", response);
	documentoAnexoTipoReferenciaProcessor.init(documentosAnexosRefGrid);
}
function traerListaDocumentosReferencias() {
	refrescarGridDocumentosAnexos(null,null,null,null);
}
function refrescarGridDocumentosAnexos(sid,action,tid,node){
	obtenerDocumentosReferencias();
	return true; 
}
function response(node){
	//parent.guardarDocReferencia(node);
	obtenerDocumentosReferencias();
return false;
}

function descargarDocumentosReferencias(id) {
	window.open('/MidasWeb/documentosreferencias/descargarArchivoFortimax.do?id=' + id, 'download');
}