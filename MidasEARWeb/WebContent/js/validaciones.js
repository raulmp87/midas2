
/* 
 * Esta funcion solo admite Letras y las transfrma a Mayusculas.
 * NOTA: ESTA FUNCION NO AVENTARA ERROR SI LE DAS UN CAMPO VACIO.
 * @param mayus Este booleano indica si se convertira en mayusculas todos los char introducidos por el usuario. 
*/ 
function soloLetrasM1(event, ctrlAllow){
	var resultado = false;		//Resultado de la funcion
	var e = event;	//capturamos la tecla presionada.
	var ctrl = ctrlAllow; //si es permitido el Ctrl.
	var code;
	 
	if (!e) e=window.event;
	
	if(e.ctrlKey && !ctrl) {return false;} //asi evitamos el Ctrl v o ctrl C
	
	if ((e.charCode) && (e.keyCode==0))
		code = e.charCode
	else
		code = e.keyCode;
//	
//	if (e.keyCode)	code = e.keyCode;
//	else if (e.which) code = e.which; 
	var character = String.fromCharCode(code);
	
	if (/^([a-z]+)?$/gi.test(character)) 
		resultado = true; //comprobacion RegEx que solo admite letras en el input
	
	//llaves de control {delete,supr,intro,tab}
	if ((code==null) || (code==0) || (code==8) || (code==9) || (code==27) ) {resultado = true;}
	
	if ((code == 13))  {return false;} //es un enter
	
	
//	alert(inputActual.value);
	return resultado;
}

/* 
 * Esta funcion solo admite Letras y las transfrma a Mayusculas.
 * NOTA: ESTA FUNCION NO AVENTARA ERROR SI LE DAS UN CAMPO VACIO.
 * @param mayus Este booleano indica si se convertira en mayusculas todos los char introducidos por el usuario. 
 * @DEPRECATED USE MEJOR soloAlfanumericosM1
*/ 
function soloLetrasYNumerosM1(event, ctrlAllow){
//	usar solo con keydown
	var resultado = false;		//Resultado de la funcion
	var e = event;		//capturamos la tecla presionada.
	var ctrl = ctrlAllow;
	
	if (!e) e=window.event;
	
	if(e.ctrlKey && !ctrl) {return false;} //asi evitamos el Ctrl v o ctrl C
	
	if ((e.charCode) && (e.keyCode==0))
		code = e.charCode
	else
		code = e.keyCode;
	
	var character = String.fromCharCode(code);
	
	if (/(\w+)/g.test(character)) 
		resultado = true; //comprobacion RegEx que solo admite letras y numeros en el input
	
	if ((code==null) || (code==0) || (code==8) || (code==9) || (code==27) ) 
		resultado = true; //llaves de control {delete,supr,intro,tab}
	
	if (/(-|_)+/g.test(character)) 
		resultado = false; //comprobacion RegEx que solo admite letras y numeros en el input
	
	if ((code == 13))  {return false;} //es un enter
	return resultado;
}

/* 
 * Esta funcion solo admite que el input solo admita Alfanumericos (letras y numeros) a la hora de un evento onchange.
 * @param event Este booleano indica si se convertira en mayusculas todos los char introducidos por el usuario.
 * @param extra Es un Booleano que representa si quieres caracteres adicionales como enies y letras con acentos. 
 * @param mayus Este booleano indica si se convertira en mayusculas todos los char introducidos por el usuario. 
*/ 
function soloAlfanumericosM1(event, extra, ctrlAllow){
	    var resultado = false;		//Resultado de la funcion
		var e = event;		//capturamos la tecla presionada.
		var ctrl = ctrlAllow;
		
		if (!e) e=window.event;
		
		if(e.ctrlKey && !ctrl) {return false;} //asi evitamos el Ctrl v o ctrl C
		
		if ((e.charCode) && (e.keyCode==0))
			code = e.charCode
		else
			code = e.keyCode;
		
		var character = String.fromCharCode(code);
		
		if (/^([a-z0-9]+)?$/gi.test(character)) resultado = true; //comprobacion RegEx que solo admite letras en el input
		
		if (extra){ // estra comprobacion permite la insercion de espacios y letras especiales como acentuadas o enies.
			if (/^([\w���������\s\.\d:;,']+)?$/gi.test(character)) 
				resultado = true; 
		}
		
		if ((code==null) || (code==0) || (code==8) || (code==9) || (code==27) ) {resultado = true;}	//llaves de control {delete,supr,intro,tab}
		
		if ((code == 13))  {return false;} //es un enter
		
		return resultado;
}

function soloAlfanumericos(el, e, dec) {
	
		var key;
		var keychar;
		
		if (window.event) {
			key = window.event.keyCode;
			keyevent = event;
		} else if (e) {
			key = e.which;
			keyevent = e;
		} 
		if ((key == 13))  {return false;}

		if ((key==null) || (key==0) || (key==8) || 		// control keys
				(key==9) || (key==13) || (key==27) ) {
				return true;
		}
		/**
		 * Dentro de este IF se remueven todos los caracteres no alphanumericos,
		 * se ejecuta solo una vez, cuando el campo pierde el foco, (Con esto se evita que peguen cosas raras).
		 *
		 **/
		if(!dec){
			if(key==0 || !e){
				if(el.type == 'textarea')
					el.value = el.value.replace(/([^\w�-��-�,;\._\s])+/g,'');
				else
					el.value = el.value.replace(/([^\/\^\w�-��-�\s])+/g,'');
				el.value = trim(el.value);
				return true;
			}
		}		
		keychar = String.fromCharCode(key);

		//Caracteres agregados a peticion del usuario �,�,vocales acentuadas , ya sean mayusculas y minusculas y u con dieresis
		if(caracterPedidoPorUsuario(key)){
			return true;
		}

		if(el.type=='textarea'){
			if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789�,;. ").indexOf(keychar) > -1)) { 	// numbers
				if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
/*					if(window.event)
						keyevent.keyCode = key-0x20;	*/				
				}
				return true;
			} 
		}else{
			
			if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789� ").indexOf(keychar) > -1)) { 	// numbers
				if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
		/*			if(window.event)
						keyevent.keyCode = key-0x20;	*/
				}
				return true;
			}
		}
		if (dec && (keychar == ".")) {		// decimal point jump
			// myfield.form.elements[dec].focus();
			return true;
		} else if (dec && (keychar == "_")){ //Se agrego para las busquedas en el folio
			return true;
		} else if (dec && (keychar == "#")){ //Se agrego para las busquedas en el folio
			return true;
		} else if (dec && (keychar == ",")){ //Se agrego para las busquedas en el folio
			return true;
		}else if (dec && (keychar == "&")){ //Se agrego para las busquedas en el folio
			return true;
		}else if (dec && (keychar == "$")){ //Se agrego para las busquedas en el folio
			return true;
		}else if (dec && (keychar == "-")){ //Se agrego para las busquedas en el folio
			return true;
		}else if (dec && (keychar == "%")){ //Se agrego para las busquedas en el folio
			return true;
		}else if (dec && (keychar == "+")){ //Se agrego para las busquedas en el folio
			return true;
		}
		
		else {
			return false;
		}
}//end soloAlfanumericos()


function soloAlfanumericosException(el, e, dec) {
	
	var key;
	var keychar;
	
	if (window.event) {
		key = window.event.keyCode;
		keyevent = event;
	} else if (e) {
		key = e.which;
		keyevent = e;
	} 
	/**
	 * Dentro de este IF se remueven todos los caracteres no alphanumericos,
	 * se ejecuta solo una vez, cuando el campo pierde el foco, (Con esto se evita que peguen cosas raras).
	 *
	 **/
	if(!dec){
		if(key==0 || !e){
			if(el.type == 'textarea')
				el.value = el.value.replace(/([^\w�-��-�,;\._\s])+/g,'');
			else
				el.value = el.value.replace(/([^\/\^\w�-��-�\s])+/g,'');
			el.value = trim(el.value);
			return true;
		}
	}		
	keychar = String.fromCharCode(key);

	//Caracteres agregados a peticion del usuario �,�,vocales acentuadas , ya sean mayusculas y minusculas y u con dieresis
	if(caracterPedidoPorUsuario(key)){
		return true;
	}

	
	
	if ((key==null) || (key==0) || (key==8) || 		// control keys
		(key==9) || (key==13) || (key==27) ) {
		return true;
	}
	if(el.type=='textarea'){
		if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789�,;. -").indexOf(keychar) > -1)) { 	// numbers
			if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
/*					if(window.event)
					keyevent.keyCode = key-0x20;	*/				
			}
			return true;
		} 
	}else{
		
		if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789� -").indexOf(keychar) > -1)) { 	// numbers
			if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
	/*			if(window.event)
					keyevent.keyCode = key-0x20;	*/
			}
			return true;
		}
	}
	if (dec && (keychar == ".")) {		// decimal point jump
		// myfield.form.elements[dec].focus();
		return true;
	} else if (dec && (keychar == "_")){ //Se agrego para las busquedas en el folio
		return true;
	} else if (dec && (keychar == "#")){ //Se agrego para las busquedas en el folio
		return true;
	} else if (dec && (keychar == ",")){ //Se agrego para las busquedas en el folio
		return true;
	}else if (dec && (keychar == "&")){ //Se agrego para las busquedas en el folio
		return true;
	}else if (dec && (keychar == "$")){ //Se agrego para las busquedas en el folio
		return true;
	}else if (dec && (keychar == "-")){ //Se agrego para las busquedas en el folio
		return true;
	}else if (dec && (keychar == "%")){ //Se agrego para las busquedas en el folio
		return true;
	}else if (dec && (keychar == "+")){ //Se agrego para las busquedas en el folio
		return true;
	}
	
	else {
		return false;
	}
}//end soloAlfanumericosException()

function personaMoral(el, e, dec) {
	
	var key;
	var keychar;
	
	if (window.event) {
		key = window.event.keyCode;
		keyevent = event;
	} else if (e) {
		key = e.which;
		keyevent = e;
	}
	keychar = String.fromCharCode(key);

	//Caracteres agregados a peticion del usuario �,�,vocales acentuadas , ya sean mayusculas y minusculas y u con dieresis
	if(caracterPedidoPorUsuario(key)){
		return true;
	}

	
	
	if ((key==null) || (key==0) || (key==8) || 		// control keys
		(key==9) || (key==13) || (key==27) ) {
		return true;
	}
	if(el.type=='textarea'){
		if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789�,;. &").indexOf(keychar) > -1)) { 	// numbers
			if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
/*					if(window.event)
					keyevent.keyCode = key-0x20;	*/				
			}
			return true;
		} 
	}else{
		if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789� &").indexOf(keychar) > -1)) { 	// numbers
			if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
	/*			if(window.event)
					keyevent.keyCode = key-0x20;	*/
			}
			return true;
		}
	}
}//end personaMoral()

//Caracteres agregados a peticion del usuario �,�,vocales acentuadas , ya sean mayusculas y minusculas y u con dieresis
function caracterPedidoPorUsuario(key){
	
	//�,�, Vocales Acentuadas Minusculas y mayusculas y U con dieresis minuscula y Mayuscula
	
	if((key==225 || key==193)|| (key==233 || key==201) || 
			(key==237 || key==205)|| (key==243 || key==211)||
			(key==250 || key==218)||(key==252 || key==220)||
			(key==209 || key==241)){
		return true
	}else{
		return false;
	}
	
	
	
}

function soloNumerosM2(el, e, dec) {
	var key;
	var keychar;
	
	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} 
	
	if (key == 13)  {return false;}
	keychar = String.fromCharCode(key);

    if (key> 31 && (key <48 || key> 57)) {
 	    return false; 
	 }else{
	 	return true;
	 }
}//end soloNumeros()

/**
 * @param num objeto html
 * @param e evento
 * @param decimal numero de decimales
 * @returns
 */
function formatCurrencyCustom(num,e,decimal) {
	if(soloNumerosM2(null,e,null)){
		num = num.value;
		num = isNaN(num) || num === '' || num === null ? 0.00 : num;
		return parseFloat(num).toFixed(decimal);
	}
}
/**
 * Esta funcion te permite solo agregar numeros al input que estas modificando y puedes incluso darle formato a un decimal. 
 * @param el {Object} Es el input de entrada.
 * @param e {Event} es el evento disparado.
 * @param dec {Boolean} si admite decimales o no.
 * @param signo {Boolean} si admite signo de negativos.
 * @param enteros {Integer} es la cantidad de enteros que quieres que tenga tu decimal.
 * @param decimales {Integer} es la cantidad de decimales que quieres que tenga tu decimal.
 * @returns {Boolean} es el resultado de la validacion, si no es satisfactoria te dara un falso.
 * @author atavera (si falla algo, ya saben quien tiene la culpa.)
 */
function soloNumeros(el, e, dec, signo, enteros,decimales){	
	var resultado = false;
	var code;
	
	if (!e) e = window.event;
	if (e.keyCode) code = e.keyCode;
	else if (e.which) code = e.which;
	
	//Solo en Firefox el keycode de las flechas debe ser permitido para mover las fechas.
	if (jQuery.browser.mozilla){
		if((e.keyCode==37)|| (e.keyCode==38)|| (e.keyCode==39)|| (e.keyCode==40)|| (e.keyCode==46)){
			return true;
		}
	}
	
	//llaves de control {delete,supr,intro,tab y flechas}
	if ((code==null) || (code==0) || (code==8) || (code==9) || (code==27)) {return true;}
	if ((code == 13))  {return false;} //si tecleo enter lo ignorara.
	
	var character = String.fromCharCode(code);
	var texto = el.value+character;
	
	//Validacion para evitar que se colen valores de los codigos
	// permitidos de las flechas
	
	
	if(enteros==undefined && decimales==undefined){
		//comprobacion que solo admite numeros en el input
		if (("0123456789").indexOf(character) > -1 )
			return true;
		
		//comprobacion para Signo Negativo, no comprueba formato.
		if(signo && character == "-"){
			if((el.value).indexOf("-") > -1){
				return false;
			}else{
				return true;
			}
		}
		//comprobacion para decimales, no comprueba formato.
		if(dec && character == "."){
			if((el.value).indexOf(".") > -1){
				return false;
			}else{
				return true;
			}
		}
	}
	if(dec && enteros>=0 && decimales>=0){
		var resSigno = "";
		if(signo){
			resSigno = "-?"
		}
		var reText = "^("+resSigno+"\\d{0,"+enteros+"}(\\.\\d{0,"+decimales+"})?)?$"
		var re = new RegExp(reText, "g");
		if(re.test(texto))
		resultado = true;
	}
			
	return resultado;
}

function soloFecha(el, e, dec) {
	var key;
	var keychar;

	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} 
	/**
	 * Dentro de este IF se remueven todos los caracteres no date,
	 * se ejecuta solo una vez, cuando el campo pierde el foco, 
	 * (Con esto se evita que peguen cosas raras).
	 *
	 **/
	if(key==0 || !e){
		el.value = el.value.replace(/([^0-9\/])+/g,'');
		el.value = trim(el.value);
		return true;
	}
	
	keychar = String.fromCharCode(key);

	
	if ((key==null) || (key==0) || (key==8) || 		// control keys
		(key==9) || (key==13) || (key==27) ) {
		return true;
	} else if ((("0123456789/").indexOf(keychar) > -1)) { 	// numbers
		return true;
	} else if (dec && (keychar == ".")) {		// decimal point jump
		// myfield.form.elements[dec].focus();
		return true;
	} else {
		return false;
	}
}//end soloFecha()

function esFechaValida(fecha){
    if (fecha != undefined && fecha.value != "" ){
        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha.value)){
            alert("formato de fecha no v\xE1lido (dd/mm/aaaa)");
            fecha.value = '';
            return false;
        }
        var dia  =  parseInt(fecha.value.substring(0,2),10);
        var mes  =  parseInt(fecha.value.substring(3,5),10);
        var anio =  parseInt(fecha.value.substring(6),10);
 
    switch(mes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numDias=31;
            break;
        case 4: case 6: case 9: case 11:
            numDias=30;
            break;
        case 2:
            if (comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
            break;
        default:
            alert("La fecha introducida err\xF3nea");
        	fecha.value = '';
            return false;
    }
 
		if (dia>numDias || dia==0){
		    alert("La fecha introducida err\xF3nea");
		    fecha.value = '';
		    return false;
		}
		return true;
    }
}

function soloLetras(el, e, dec) {
	var key;
	var keychar;
	var keyevent;

	if (window.event) {
		key = window.event.keyCode;
		keyevent = event;
	} else if (e) {
		key = e.which;
		keyevent = e;
	}

	/**
	 * Dentro de este IF se remueven todos los caracteres no alpha,
	 * se ejecuta solo una vez, cuando el campo pierde el foco, 
	 * (Con esto se evita que peguen cosas raras).
	 *
	 **/
	if(key==0 || !e){
		el.value = el.value.replace(/([^\/\^\w�-��-�\s])+/g,'');
		return true;
	}
	
	keychar = String.fromCharCode(key);

	
	if ((key==null) || (key==0) || (key==8) || 		// control keys
		(key==9) || (key==13) || (key==27) ) {
		return true;
	} else if ((("����������abcdefghijklm�nopqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ� ").indexOf(keychar) > -1)) { 	// numbers
		if (((key > 0x60) && (key < 0x7B)) ||((key > 0xE0) && (key < 0xFB))){
			if(window.event)
				keyevent.keyCode = key-0x20;					
		}
		return true;
	} else if (dec && (keychar == ".")) {		// decimal point jump
		// myfield.form.elements[dec].focus();
		return true;
	} else {
		return false;
	}
}//end soloLetras()

function setDecimalForLength(obj, intv) {
	var intValue = removeCurrency(obj.value);

	if ((intValue.toString().length >= intv) && (obj.value.toString().lastIndexOf(".") == -1)) {
		obj.value = obj.value.toString().concat(".");
	}
}
function removeCurrency(strValue) {
	var objRegExp = /\(/;
	var strMinus = "";
	if (objRegExp.test(strValue)) {
		strMinus = "-";
	}
	objRegExp = /\)|\(|[,]/g;
	strValue = strValue.replace(objRegExp, "");
	if (strValue.indexOf("$") >= 0) {
		strValue = strValue.substring(1, strValue.length);
	}
	return strMinus + strValue;
}

function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g, "");
	if (isNaN(num)) {
		num = "0";
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10) {
		cents = "0" + cents;
	}
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
		num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
	}
	return (((sign) ? "" : "-") + "$" + num + "." + cents);
}

function formatCurrency4Decs(num) {
	num = num.toString().replace(/\$|\,/g, "");
	if (isNaN(num)) {
		num = "0";
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 10000 + 0.50000000001);
	
	cents = num % 10000;
	num = Math.floor(num / 10000).toString();
	if (cents < 1000 && cents >= 100) {
		cents = "0" + cents;
	}
	if (cents < 100 && cents >= 10) {
		cents = "00" + cents;
	}
	if (cents < 10) {
		cents = "000" + cents;
	}
	
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
		num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
	}
	return (((sign) ? "" : "-") + "$" + num + "." + cents);
}

function fechaMenorIgualQue(fechaMenor, fechaMayor){
	if(fechaMenor!==null && fechaMenor!=='' && fechaMayor!==null && fechaMayor!==''){
	
		fechaMenorArr = fechaMenor.split("/");	
		fechaMayorArr = fechaMayor.split("/");
	
		diaMenor = fechaMenorArr[0];
		mesMenor = fechaMenorArr[1];
		anioMenor = fechaMenorArr[2];
	
		diaMayor = fechaMayorArr[0];
		mesMayor = fechaMayorArr[1];
		anioMayor = fechaMayorArr[2];
		
		if(anioMenor<anioMayor)
			return true;
					
		if(anioMenor>anioMayor)			
			return false;
		
		if(mesMenor<mesMayor)
			return true;
		
		if(mesMenor>mesMayor)			
			return false;
		
		if(diaMenor>diaMayor)
			return false;
		else
			return true;	
	}
}

function fechaMenorQue(fechaMenor, fechaMayor){
	if(fechaMenor!==null && fechaMenor!=='' && fechaMayor!==null && fechaMayor!==''){
	
		fechaMenorArr = fechaMenor.split("/");	
		fechaMayorArr = fechaMayor.split("/");
	
		diaMenor = fechaMenorArr[0];
		mesMenor = fechaMenorArr[1];
		anioMenor = fechaMenorArr[2];
	
		diaMayor = fechaMayorArr[0];
		mesMayor = fechaMayorArr[1];
		anioMayor = fechaMayorArr[2];
		
		if(anioMenor<anioMayor)
			return true;
					
		if(anioMenor>anioMayor)			
			return false;
		
		if(mesMenor<mesMayor)
			return true;
		
		if(mesMenor>mesMayor)			
			return false;
		
		if(diaMenor<diaMayor)
			return true;
	}
}

function validarFechas(fechaMenor, fechaMenorNombre, fecha, fechaNombre, fechaMayor, fechaMayorNombre){
	ok = true;
	mensaje = '';
	if(fecha!==''){
		if(fechaMenor!=='' && !fechaMenorIgualQue(fechaMenor,fecha)){			
			ok = false;
			if(fechaMayor===''){
				alert('La fecha '+fechaNombre+' debe ser mayor o igual que la fecha '+fechaMenorNombre+'.');
				return ok;
			}
		}
			
		if(fechaMayor!=='' && !fechaMenorIgualQue(fecha, fechaMayor)){			
			ok = false;
			if(fechaMenor===''){				
				alert('La fecha '+fechaNombre+' debe ser menor o igual que la fecha '+fechaMayorNombre+'.');
				return ok;
			}
		}
	}
	
	if(!ok)
		alert('La fecha '+fechaNombre+' debe ser mayor o igual que la fecha '+fechaMenorNombre+' y menor o igual que la fecha '+fechaMayorNombre+'.');
	
	return ok;	
}

function validarFechasSiniestros(){
	if(validarFechas($('fechaTerminadoSiniestro').value,'de terminado el siniestro',$('fechaCerradoSiniestro').value,'de cerrado el siniestro','',''))
		if(validarFechas($('fechaInformeFinal').value,'de informe final',$('fechaTerminadoSiniestro').value,'de terminado el siniestro',$('fechaCerradoSiniestro').value,'de cerrado el siniestro'))
			if(validarFechas($('fechaDocumento').value,'de documento',$('fechaInformeFinal').value,'de informe final',$('fechaTerminadoSiniestro').value,'de terminado el siniestro'))
				if(validarFechas($('fechaPreliminar').value,'preliminar',$('fechaDocumento').value,'de documento',$('fechaInformeFinal').value,'de informe final'))
					if(validarFechas($('fechaInspeccionAjustador').value,'de inspecci\u00f3n del ajustador',$('fechaPreliminar').value,'preliminar',$('fechaDocumento').value,'de documento'))
						if(validarFechas($('fechaLlegadaAjustador').value,'de llegada del ajustador',$('fechaInspeccionAjustador').value,'de inspecci\u00f3n del ajustador',$('fechaPreliminar').value,'preliminar'))
							if(validarFechas($('fechaContactoAjustador').value,'de contacto del ajustador',$('fechaLlegadaAjustador').value,'de llegada del ajustador',$('fechaInspeccionAjustador').value,'de inspecci\u00f3n del ajustador'))
								if(validarFechas($('fechaAsignacionAjustador').value,'de asignaci\u00f3n del ajustador',$('fechaContactoAjustador').value,'de contacto del ajustador',$('fechaLlegadaAjustador').value,'de llegada del ajustador'))
									if(validarFechas($('fechaSiniestro').value,'del siniestro',$('fechaAsignacionAjustador').value,'de asignaci\u00f3n del ajustador',$('fechaContactoAjustador').value,'de contacto del ajustador'))
										if(validarFechas('','',$('fechaSiniestro').value,'del siniestro',$('fechaReporte').value,'del reporte'))										
											return true;	
	return false;
}

function validarRango(numero,minimo,maximo){
	return (numero>=minimo && numero<=maximo);		
}

function validarPorcentaje(porcentaje){	
	if(!validarRango(porcentaje,0,100)){
		alert("Los porcentajes deben ser mayores o iguales que 0.00% y menores o iguales que 100%");
		return false;
	}
	return true;
}


function validarDecimal(fieldName, fieldValue, enteros, decimales) {
	if (isNaN(fieldValue) || fieldValue == "") {
		alert("Debe introducir un valor num\u00E9rico.");
//		fieldName.select(); //los comente porque estaban causando problemas con IE8
//		fieldName.focus();
	}
	else {
		if (fieldValue.indexOf('.') == -1)
			fieldValue += ".";
		
		decText = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
		if (decText.length > decimales){
			alert ("Debe introducir un numero con " + decimales + " decimales maximo.");
//			fieldName.select();
//			fieldName.focus();
		 }
		else {
			intText = fieldValue.substring (0,fieldValue.indexOf('.'));
			if (intText.length > enteros){
				alert ("Debe introducir un numero con " + enteros + " enteros maximo.");
//				fieldName.select(); 
//				fieldName.focus();
			 }
		 }
	}
}

/* Este metodo es un metodo generico de validacion
 * que recibe una expresion regular para hacer una validacion de 
 * la tecla que se esta presionando es decir el onKeyPress.
 * Ejemplo:
 * e: event
 * regularExpression: /[A-z0-9\-]/ 
 * Permite letras de la a a la z minusculas y mayusculas, 
 * numeros del 0 al 9 y guiones (-).
 */

function teclaEsIgualALaExpresionRegular(e, regularExpression) {
	var valorDeRetorno;
    var charCode = (e.which) ? e.which : window.event.keyCode; 
	if (charCode <= 13) {
		valorDeRetorno = true;
	}
    else {
		var keyChar = String.fromCharCode(charCode); 
		valorDeRetorno = regularExpression.test(keyChar);
	}
	return valorDeRetorno;
}

/*
 * Se utiliza en el evento onkeypress, de la siguiente manera onkeypress="return limiteMaximoCaracteres(this.value, event, 300)"
 * En especial para la etiqueta html:textArea o midas:areaTexto
 */
function limiteMaximoCaracteres(texto, e, max){
	var key;
	
	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} 
	
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==27))
		return true;
	
	if (texto.length >= parseInt(max) || (key==13))
		return false;
	
	return true;
}

function isFloat(value){
	if(isNaN(value) || value.indexOf(".")<0){
	     return false;
	} else {
	    if(parseFloat(value)) {
	       return true;
	    } else {
	        return false;
	    }
	}
}

function isFloat0(value) {
	if (!/^[0-9]*(\.[0-9]*)?$/.test(value)){
		return false;
	}
	return true;
} 

function isInteger(value){
	if (!/^([0-9])*$/.test(value)){
		return false;
	}
	return true;
}

function esNumeroNegativo(numero){
	var resultado = true;
	if (!/^(-){1}([0-9])+(.)?([0-9])*$/.test(numero))
		resultado = false;
	return resultado;
	
}

function isNumeric(sText){
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
}

function teclaPresionadaEsNumerica (e, sText) {
	var valorDeRetorno = true;
	var ValidChars = "0123456789.";
	var Char;
	var puntosDecimales = 0;

    var charCode = (e.which) ? e.which : window.event.keyCode; 
	if (charCode <= 13) {
		valorDeRetorno = true;
	}
    else {
		var keyChar = String.fromCharCode(charCode); 
		sText = sText + keyChar;
		
		for (i = 0; i < sText.length && valorDeRetorno == true; i++) 
	      { 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1) 
	         {
	    	  valorDeRetorno = false;
	         }
	      
	      if (Char == '.') {
	    	  puntosDecimales = puntosDecimales + 1;
	    	  if (puntosDecimales > 1) {
	    		 valorDeRetorno = false;
	    	  }
	      }
	      
	      }
	}
	return valorDeRetorno;
}
function valEmail(valor){
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(valor))    {
    	alert("La direcci�n de email es incorrecta.");
        return false;
    }else{
        return true;
    }
}

function maximaLongitud(texto,maxlong) {
	var in_value, out_value;
	if (texto.value.length > maxlong) {
		in_value = texto.value;
		out_value = in_value.substring(0,maxlong);
		texto.value = out_value;
		return false;
	}
	return true;
}

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return "$" + s.join(dec);
}

function upperCase(e) { 
	  tecla = (document.all) ? e.keyCode : e.which; 
	  if(tecla == 9 || tecla == 0) return true; 
		if(/\d/.test(String.fromCharCode(tecla))) return false;
	  if(tecla == 8) return true; 
	  if(window.Event){ 
	      var pst = e.currentTarget.selectionStart; 
	      var string_start = e.currentTarget.value.substring(0,pst); 
	      var string_end = e.currentTarget.value.substring(pst ,e.currentTarget.value.length); 
	      e.currentTarget.value = string_start+ String.fromCharCode(tecla).toUpperCase()+ string_end; 
	      e.currentTarget.selectionStart = pst + 1; 
	      e.currentTarget.selectionEnd = pst + 1; 
	      e.stopPropagation(); 
	      return false; 
	  } 
	  else { 
	    te = String.fromCharCode(tecla); 
	    te = te.toUpperCase(); 
	    num = te.charCodeAt(0); 
	    e.keyCode = num; 
	  } 
	}

function evaluarCadena(fieldName, fieldValue){
	if (fieldValue == "" || fieldValue == " " ) {
		alert("Debe escribir Solo Letras.");
	}else{
		if(fieldValue.match(/^[a-zA-Z]+$/)){
			return true;
		} else {
			alert("Debe introducir Solo Letras.");
			fielName.setFocus();
		}
	}
}
function upperCaseAlfaNumerico(e) { 
	  tecla = (document.all) ? e.keyCode : e.which; 
	  if(tecla == 9 || tecla == 0) return true; 
//		if(/\d/.test(String.fromCharCode(tecla))) return false;
	  if(tecla == 8) return true; 
	  if(window.Event){ 
	      var pst = e.currentTarget.selectionStart; 
	      var string_start = e.currentTarget.value.substring(0,pst); 
	      var string_end = e.currentTarget.value.substring(pst ,e.currentTarget.value.length); 
	      e.currentTarget.value = string_start+ String.fromCharCode(tecla).toUpperCase()+ string_end; 
	      e.currentTarget.selectionStart = pst + 1; 
	      e.currentTarget.selectionEnd = pst + 1; 
	      e.stopPropagation(); 
	      return false; 
	  } 
	  else { 
	    te = String.fromCharCode(tecla); 
	    te = te.toUpperCase(); 
	    num = te.charCodeAt(0); 
	    e.keyCode = num; 
	  } 
	}
/* valida que un rango sea valido */
function validaRango(valor,min,max){
	if(valor > max){
		alert("El valor m\u00e1ximo permitido es: " + max);
	}else if(valor < min){
		alert("El valor m\u00ednimo permitido es: " + min);
	}
}

/* Sort date para dhtmlgrid*/
function date_custom(a,b,order){ 
    a=a.split("/")
    b=b.split("/")
    if (a[2]==b[2]){
        if (a[1]==b[1])
            return (a[0]>b[0]?1:-1)*(order=="asc"?1:-1);
        else
            return (a[1]>b[1]?1:-1)*(order=="asc"?1:-1);
    } else
         return (a[2]>b[2]?1:-1)*(order=="asc"?1:-1);
}

function dateTime_custom(a,b,order){
	var ad = a.split(' ');
	var bd = b.split(' ');

    a=ad[0].split("/")
    b=bd[0].split("/")
    
    if (a[2]==b[2]){
        if (a[1]==b[1]){
        	if(a[0]==b[0]){
        		var ah = ad[1].split(":");
        		var bh = bd[1].split(":");
                if (ah[0]==bh[0])
                    return (ah[1]>bh[1]?1:-1)*(order=="asc"?1:-1);
                else
                    return (ah[0]>bh[0]?1:-1)*(order=="asc"?1:-1);
        	}else
        		return (a[0]>b[0]?1:-1)*(order=="asc"?1:-1);
        }else
            return (a[1]>b[1]?1:-1)*(order=="asc"?1:-1);
    } else
         return (a[2]>b[2]?1:-1)*(order=="asc"?1:-1);
    
}

/**
 * Permite validar el número de póliza. 
 * @param poliza {String} Es el número de póliza que se quiere validar.
 * @returns {Boolean} es el resultado de la validacion, si no es satisfactoria te dara un falso.
 */

function esPolizaValida(poliza){
	var valida = false;
	var parts = poliza.split('-');
	if(parts.length == 3){
		if((parts[0].length <= 5) && (parts[0].length >= 3) && (parts[1].length == 10) && (parts[2].length == 2)){
			if(/^([0-9])*$/.test(parts[0]) && /^([0-9])*$/.test(parts[1]) && /^([0-9])*$/.test(parts[2])){
				valida = true;
			}
		}
	}
	return valida;
}
/**
 * Valida el porcentaje capturado
  @eventos valida el valor del elemento para las reglas aplicadas(porcentaje),
  @eventos asigna el valor procesado al elemento que lo mando llamar
 */
function validaPorcentaje(idActual) {
	var id = jQuery("#" + idActual);
	var valor = jQuery.trim(id.val());		
	if(valor <= 100 && valor >= 0)
		id.val(checkDecimales(valor,3));
	else
		id.val('');
}

/**
 * Valida cuantos decimales debe de contener la cantidad a evaluar
  @parametro valor,variable con el texto a procesar  
  @parametro limite,variable con el numero de decimales a limitar
  @regreso nResultado, numero con los decimales segun el limite
 */
function checkDecimales(valor,limite){
	var indicePunto = valor.indexOf('.');
	var nOriginal = 0.0;
	var nResultado = valor;
	if(indicePunto >= 0){
		var decimales = valor.substring(indicePunto, valor.length);
		if(decimales.length > limite){
			nOriginal = parseFloat(valor);
			nResultado = nOriginal.toFixed(limite);
		}
	}
	return nResultado;	
}

/**
	Valida filtros de fechas de consulta, si una fecha inicial es mayor a una fecha final
	@parametro fecha, fecha inicial
	@parametro fecha2, fecha final
	@regreso {Boolean} Regresa true si la fecha inicial es mayor a la final
*/
function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}

/**
	Valida filtros de fechas de consulta, si el periodo a consultar es superior a 1 año
	@parametro fecha, fecha inicial
	@parametro fecha2, fecha final
	@parametro filtroRestrictivo1, si este parámetro es diferente de vacio no toma en cuenta la restricción del periodo
	@parametro filtroRestrictivo2, si este parámetro es diferente de vacio no toma en cuenta la restricción del periodo
	@regreso {Boolean} Regresa true si los filtros restrictivos son vacios y el periodo entre las fechas es superior a 1 año
*/
function compare_dates_period(fecha1, fecha2) {
	var fechaIni = (fecha1=='')? new Date().toLocaleDateString() : fecha1;
	var fechaFin = (fecha2=='')? new Date().toLocaleDateString() : fecha2;

	var fechaInicial = fechaIni.split('/');
	var fechaFinal = fechaFin.split('/');
	var year = parseInt(fechaInicial[2]);
	var addYear = year + 1;	
	var fechaAux = new Date(addYear, fechaInicial[1]-1, fechaInicial[0]);
	fechaFin = new Date(fechaFinal[2], fechaFinal[1]-1, fechaFinal[0]);
	
	if(fechaFin > fechaAux) {
		return(true);
	} else {
		return(false);
	}
}

/**
	Valida filtros de fechas de consulta, si son vacias y no se captura filtro restrictivo setearles valores por default con diferfencia de 1 año
	@parametro idFecha, id del filtro de fecha inicial
	@parametro idFecha2, id del filtro de fecha final
*/
function definirFechasDefault(idFecha1, idFecha2) {
	fecha1=dwr.util.getValue(idFecha1);
	fecha2=dwr.util.getValue(idFecha2);
	var fechaFin = (fecha2=='')? new Date() : fecha2;
	if(fecha2 == '') {	// Si la fecha final es vacía, se arma fechaFin con funciones de Date(), caso contrario se utiliza fecha 2 que ya esta formateada dd/mm/yyyy en string
		fechaFin = String(fechaFin.getDate() + "/" + (fechaFin.getMonth()+1) + "/" + fechaFin.getFullYear());
	}
	
	var fechaFinal = fechaFin.split('/');
	var year = parseInt(fechaFinal[2]);
	var subYear = year - 1;	
	var fechaInicial = ("0" + fechaFinal[0]).slice(-2) + "/" + ("0" + fechaFinal[1]).slice(-2) + "/" + subYear;	// Formato dd/mm/yyyy	
	
	if(fecha1.trim() == '' && fecha2.trim() == '') {	// Fecha final e inicial vacias		
		fechaFin = ("0" + fechaFinal[0]).slice(-2) + "/" + ("0" + fechaFinal[1]).slice(-2) + "/" + fechaFinal[2];	// Formato dd/mm/yyyy
		
		/* Cambiar valores de filtros de fechas */
		document.getElementById(idFecha1).value = fechaInicial;
		document.getElementById(idFecha2).value = fechaFin;
		mostrarMensajeInformativo("No se capturó fecha inicial y final. Por validación se realizará búsqueda con fecha inicial " + fechaInicial 
			+ " y fecha final " + fechaFin, 10, null);
	} else if(fecha1.trim() == '' && fecha2.trim() != '') {	// Solo fecha inicial vacia
		/* Cambiar valores de filtros de fechas */
		document.getElementById(idFecha1).value = fechaInicial;
		mostrarMensajeInformativo("No se capturó fecha inicial. Por validación se realizará búsqueda con fecha inicial " + fechaInicial, 10, null);
	}
}

/**
	Valida filtros de texto para capturar al menos 3 caracteres
	@parametro filtro, id del filtro
	@parametro valor, valor del filtro
	@regreso {Boolean} regresa false si se capturan 1 o 2 caracteres y setea el filtro en vacio, de otra manera regresa true
*/
function validaLongitud(filtro, valor) {
	var numCaracteres = valor.length;
	if(numCaracteres > 0 && numCaracteres < 3) {
		mostrarMensajeInformativo("Se debe capturar un mínimo de 3 caracteres.", 10, null);
		document.getElementById(filtro).value = "";
		return (false);
	} else {
		return (true);
	}
}