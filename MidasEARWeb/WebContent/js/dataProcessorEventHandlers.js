var errorActualizacion = false;

function iniciarManejoEventosDataProcessor(dataProcessor, afterSuccess) {
	
	var rowsActualizados = 0;
	var rowsEnviados = 0;
	var idActualizados = new Array();
	var encontrado = false;

	
	
	dataProcessor.attachEvent("onRowMark", function (id,state,mode,invalid){
		
		encontrado = false;
		
		for (var i=0;i < rowsActualizados; i++){
			
			if (idActualizados[i] == id) {
				encontrado = true;
				break;
			}
		}
		
		if (!encontrado) {
			idActualizados[rowsActualizados] = id;
			rowsActualizados = rowsActualizados + 1;
		}
		
				
		return true;
	});
	
	dataProcessor.attachEvent("onAfterUpdateFinish",function (){
		
		rowsEnviados = rowsEnviados + 1;
		
		obtenerRespuestaPreeliminar();
				
		if(rowsEnviados >= rowsActualizados && dataProcessor.getSyncState) {
			rowsEnviados = 0;
			
			if (!errorActualizacion) {
				obtenerMensajeRespuesta(afterSuccess);
			} else {
				mostrarVentanaMensaje("10", "Algunos datos no se guardaron correctamente");
				errorActualizacion = false;
				eval(afterSuccess);
			}
			
		}
		
	});
	
	
}


function obtenerRespuestaPreeliminar(){
	new Ajax.Request('/MidasWeb/mensaje/verMensaje.do', {
		method : "post",
		asynchronous : false,
		//parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			var items = transport.responseXML.getElementsByTagName("item");
			var item = items[0];
			var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			if (tipoMensaje != "30") {
				errorActualizacion = true;
			}
		} // End of onSuccess
		
	});
	
	
}



