var calendario;

function iniciar() {
	cambiarTamanio();
	agregarEvento(window, 'resize', cambiarTamanio);
}

function agregarEvento(objeto, tipo, funcion) {
	if (objeto.attachEvent) {
		objeto['e' + tipo + funcion] = funcion;
		objeto[tipo + funcion] = function() {
			objeto['e' + tipo + funcion](window.event);
		}
		objeto.attachEvent('on' + tipo, objeto[tipo + funcion]);
	} else
		objeto.addEventListener(tipo, funcion, false);
}

function cambiarTamanio() {
	var altoDelEncabezado = $("encabezado").offsetHeight;
	var altoDelMenu = $("menu").offsetHeight;
	var altoDelPieDePagina = $("pieDePagina").offsetHeight;
	var altoTotalSinContenido = altoDelEncabezado + altoDelMenu + altoDelPieDePagina;
	//alert(altoTotalSinContenido + " " + altoDelEncabezado + " " + altoDelMenu + " " + altoDelPieDePagina);
	$("pagina").style.height = getAltoDeLaVentanaDelCliente() - altoTotalSinContenido - 10 + "px";
	var altoDeLaPagina = $("pagina").offsetHeight;

	var altoDeBordes = $("pagina").offsetHeight;
	$("central").style.height =  (altoDeBordes - 14) + "px";
	$("contenido").style.height =  (altoDeBordes - 14) + "px";
	$("bordeMedioIzquierdo").style.height = (altoDeBordes - 14) + "px";	
	$("bordeMedioDerecho").style.height = (altoDeBordes - 14) + "px";
}

function getAnchoDeLaVentanaDelCliente() {
	return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth
			: document.body.clientWidth;
}

function getAltoDeLaVentanaDelCliente() {
	return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight
			: document.body.clientHeight;
}
function setMenuStyle(sUlId, eLink, sSelectedStyle) {
	var ul = document.getElementsByTagName("ul");
	for (i = 0; i < ul.length; i=i+1) {
		var eUl = ul[i];
		var sId = eUl.id;
		if (sId.indexOf(sUlId) == 0) {
			var allLinks = ul[i].getElementsByTagName("a");
			for ( var j = 0; j < allLinks.length; j=j+1) {
				allLinks[j].className = "";
			} // End of for
			if (eLink !== null) {
				eLink.className = sSelectedStyle;
			} // End of if
		}
	}
}

function sendRequestContratoCuotaAparte(fobj, actionURL, targetId, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
				true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
			if (transport.responseText.indexOf('<!--123456789-->') != -1) {
				alert('popup');
				popUp('asignarReaseguradoCorredor');				
			};
		}, // End of onComplte
		onSuccess : function(transport) {
			$(targetId).innerHTML = transport.responseText;
			if (pNextFunction !== null && pNextFunction !== '') {
				eval(pNextFunction);
			} // End of if
		} // End of onSuccess
	});
}

function getCalendario(evento, objetivo){
	saveCursorPos(evento);
	calendario = null;
	calendario = new Epoch("epoch_popup", "popup", $(objetivo), false);
	calendario.toggle($(objetivo));
}

function mesajeGlobal(puntoDo,area) {
	var textObj = $("mensajeGlobal");
	var divObj = $("mensajeImg");
	var botonObj = $("mensajeBoton");
	
	var mensaje = $('mensaje');
	var tipoMensaje = $('tipoMensaje');
	
	if (puntoDo === null && area === null){
		if(textObj.style.dyplay === 'none') {
			  textObj.style.dyplay = 'block';
			} // End of if
	}

	if (mensaje !== null && mensaje.value !== ""){

		textObj.innerHTML = mensaje.value;
		if (tipoMensaje.value === "30"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_ok.png'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\"mesajeGlobal(null,null);sendRequest(null,'"+ puntoDo +"','"+ area +"',null);\">Aceptar</a></div>";
		}else if (tipoMensaje.value === "20"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_info.jpg'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" mesajeGlobal(null,null);\">Aceptar</a></div>";			
		}else if (tipoMensaje.value === "10"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_no.jpg'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" mesajeGlobal(null,null);\">Aceptar</a></div>";	
		}

		if(textObj.style.dyplay === 'none') {
			  textObj.style.dyplay = 'block';
			} // End of if
		
		popUp("mensaje_popup", "400", "300");
		backgroundFilter("blockWindow");
	}

}

function existenErrores(nextFunction){
	var errores = $("errores");
	if (errores === null || errores.innerHTML === ""){
		eval(nextFunction);
	
	}else{
		return true;
	}
}


function validaGuardarTipoPoliza(){
	var errores = $("errores");	
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")==''){
			parent.mostrarVentanaMensaje ('30', 'Guardado Exitosamente','refreshTree()');
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada verifique la informaci&oacute;n','null'));		
	}	
}

function validaGuardarModificarM1(idTipoPoliza){
	var errores = $("errores");	
	//errores = null;
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")=='' ){
			if(idTipoPoliza == null	|| idTipoPoliza == ''){
				mostrarMensajeInformativo("Acci\u00F3n realizada correctamente", "30",null, null);
			}else {
				mostrarMensajeInformativo("Acci\u00F3n realizada correctamente", "30", "onChangeComboTipoPoliza("+idTipoPoliza+");", null);
			}
			
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada verifique la informaci&oacute;n','null'));		
	}
}


function validaGuardarModificarM(){
	var errores = $("errores");	
	if (errores === null || errores.innerHTML === ""){
    	parent.mostrarVentanaMensaje ('30', 'Guardado Exitosamente','null')
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada verifique la informaci&oacute;n','null'));		
	}
}



function validaBorrar(){
	var errores = $("errores");	
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")==''){
			parent.mostrarVentanaMensaje ('30', 'Borrado Exitosamente','refreshTree()')
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada','null'));		
	}
}

function validaBorrarM1(){
	var errores = $("errores");	
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")==''){
			parent.mostrarVentanaMensaje ('30', 'Borrado Exitosamente','null')
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada','null'));		
	}
}

function validaGuardarSecciones(){
	var errores = $("errores");
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")==''){
			parent.mostrarVentanaMensaje ('30', 'Guardado Exitosamente','refreshTree()');
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada verifique la informaci&oacute;n','null'));		
	}	
}

function crearCalendario(){
	calendario = new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
	calendario.setDateFormat(formatoFechaCalendario());
		
	calendario.leftCalendar.attachEvent("onClick",function(fecha){
 		$('fechaInicial').value=calendario.leftCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
 			$('fechaFinal').value = '';
 		}
	});
	
 	calendario.rightCalendar.attachEvent("onClick",function(fecha){
 		$('fechaFinal').value=calendario.rightCalendar.getFormatedDate(formatoFechaCalendario(),fecha);
 		if(!fechaMayorOIgualQue($('fechaFinal').value,$('fechaInicial').value)){
			$('fechaInicial').value = '';
		}
	});
 	calendario.hide();
}

function mostrarCalendario(){
	if (calendario!=null){
		if (calendario.isVisible()){
			calendario.hide();
		}else{
			calendario.show();
		}
	}
}

function validaCopiarNegocio(){
	var errores = $("errores");	
	if (errores === null || errores.innerHTML === ""){
		if(dwr.util.getValue("mensaje")==null || dwr.util.getValue("mensaje")==''){
			
			parent.mostrarVentanaMensaje ('30', 'El Negocio se ha copiado Exitosamente','null')
		}
		else{
			existenErrores(parent.mostrarVentanaMensaje ($('tipoMensaje').value, $('mensaje').value,'null'));
		}
	}else{
		existenErrores(parent.mostrarVentanaMensaje ('20','La acci&oacute;n no puede ser completada verifique la informaci&oacute;n','null'));		
	}
}

function validarCorreoNoVacio(){	
	if(document.getElementById("correo").value==null || document.getElementById("correo").value==''){
	  alert('Es requerido al menos un correo para ser enviado');
	      return false;
	}else{
		return true;
	}
}

function setidTcRamo(){
	document.getElementById("idTcRamo").value="4";	
	idTcRamo[idTcRamo.selectedIndex].value="4";
	getSubRamos(idTcRamo,'idTcSubRamo');
}

