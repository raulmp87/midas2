

function agregarCorreo(){
	var correo = jQuery("#txtCorreo").val()
	var nombre = jQuery("#txtNombre").val()
	var gerenciaID = jQuery("#cbGerencia").val()
	if((correo != null && correo.length > 0) && (nombre != null && nombre.length > 0)){
		sendRequestJQ(null, config.contextPath + "/enlace/guardarCorreo.action?correo=" + correo +"&nombre=" + nombre + "&gerenciaID=" + gerenciaID
				, targetWorkArea, null, "html", defaultContentType);
	}
}


function eliminarCorreo(idCorreo){
	if(idCorreo > 0){
		sendRequestJQ(null, config.contextPath + "/enlace/eliminarCorreo.action?idCorreo=" + idCorreo, targetWorkArea, null, "html", defaultContentType);
	}
}

function mostrarDetalle( usuario ){
	sendRequestJQ(null, config.contextPath + "/enlace/verDetalleDescargas.action?usuario=" + usuario, targetWorkArea, null, "html", defaultContentType);
}

function bloquearUsuario(usuario){
	sendRequestJQ(null, config.contextPath + "/enlace/bloquearUsuario.action?usuario=" + usuario, targetWorkArea, null, "html", defaultContentType);
}

function deBloquearUsuario(usuario){
	sendRequestJQ(null, config.contextPath + "/enlace/deBloquearUsuario.action?usuario=" + usuario, targetWorkArea, null, "html", defaultContentType);
}

