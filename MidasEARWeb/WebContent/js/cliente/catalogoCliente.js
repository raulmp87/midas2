var camposObligatoriosPersonaFisica=new Array();
var camposObligatoriosPersonaMoral=new Array();
var cobranzaGrid=null;
function mostrarDetalleCliente(idCliente){
	var form=jQuery("#consultaClientesForm");
	var url="/MidasWeb/catalogoCliente/mostrarDetalleCliente.action";
	if(idCliente){
		url+="?cliente.idCliente="+idCliente;
	}
	//form.attr("action",url);
	//form.attr("onsubmit",null);
	//form.submit();
	sendRequestJQ(null,url,'contenido',null);
}

function obtenerCamposObligatorios(idTipoPersona){
	var idNegocio=jQuery("#cliente\\.idNegocio").val();
	var negocio=jQuery("#idNegocio").val();
	idNegocio=(negocio!=null)?negocio:idNegocio;
	var url="/MidasWeb/cliente/catalogoClienteJson/cargarCamposObligatorios.action";
	var data={"cliente.claveTipoPersona":idTipoPersona,"cliente.idNegocio":idNegocio};
	//url+="?cliente.claveTipoPersona="+idTipoPersona+"&cliente.idNegocio="+idNegocio;
	jQuery.ajax({
		url: url,
		dataType: 'json',
		async:false,
		type:"POST",
		data: data,
		success: function(json){
			var campos=json.camposRequeridos;
			if(idTipoPersona==1){
				camposObligatoriosPersonaFisica=campos;
			}else{
				camposObligatoriosPersonaMoral=campos;
			}
		}
	});
}

function rescribirEtiquetas(idTipoPersona){
	var fieldNames=null;
	if(idTipoPersona==1){
		fieldNames=camposObligatoriosPersonaFisica;
	}else{
		fieldNames=camposObligatoriosPersonaMoral;
	}
	if(fieldNames){
		for(var i=0;i<fieldNames.length;i++){
			var fieldName=fieldNames[i];
			var etiqueta=jQuery("label[for='"+fieldName+"']");
			var elemento=document.getElementsByName(fieldName);
			var elem=(elemento!=null)?elemento[0]:null;
			var campoHTML=null;
			if(elem!=null){
				campoHTML=jQuery(elem);
			}
			//alert("Campo HTML:"+campoHTML);
			if(etiqueta!=null){
				var text=etiqueta.html();
				//Si no contiene ya el asterisco, entonces se agrega, sino ya lo trae.
				if(text!=null && text.indexOf("*")==-1){
					etiqueta.html(text+"*");
				}
			}
			if(campoHTML!=null){
				jQuery(campoHTML).addClass("jQrequired");
			}
		}
	}
}

function guardarClienteDuplicados(divCarga){
	var idDiv=(divCarga!=null)?divCarga:'contenido';
	var url="/MidasWeb/catalogoCliente/guardarCliente.action";
	var data=jQuery("#clientForm").serialize();
	//sendRequestJQ(null,url+"?"+data,idDiv,null);
	var rfc=jQuery("#cliente\\.codigoRFC").val();
	var booleanTipoPersonaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
	var booleanTipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
	var tipoPersona=null;
	tipoPersona=(booleanTipoPersonaMoral)?2:1;
	var rfcValido=validarRFC();
	if(validateForId('clientForm', true)){
		if(tipoPersona==1){
			if(jQuery.isValid(rfc) /*&& rfcValido*/){
				if((esMayorEdadServidor(jQuery("#cliente\\.fechaNacimiento").val()))){
					
					if (confirm("¿Está seguro que desea guardar el registro aunque exista uno similar? Se guardará historíco del registro."))
					{operacionGenericaConParamsRendererIntoDiv(url+"?"+data+"&divCarga="+idDiv,null,idDiv);}
					
				}
			}else{
				alert("RFC inválido!");
			}
		}else if(jQuery.isValid(rfc) && tipoPersona==2){
			if (confirm("¿Está seguro que desea guardar el registro aunque exista uno similar? Se guardará historíco del registro."))
			{	
				operacionGenericaConParamsRendererIntoDiv(url+"?"+data+"&divCarga="+idDiv,null,idDiv);
				
			}
		}else{
			alert("RFC invalido, favor de capturar el RFC y verificar el tipo de persona jurídica");
		}
	}
}


function agregarDomicilio(){
	
	
	jQuery("#cliente\\.domicilioTemp\\.cveEstado").val(jQuery("#cliente\\.idEstadoDirPrin").val());
	jQuery("#cliente\\.domicilioTemp\\.cveCiudad").val(jQuery("#cliente\\.idMunicipioDirPrin").val());
	jQuery("#cliente\\.domicilioTemp\\.calle").val(jQuery("#cliente\\.nombreCalle").val());
	jQuery("#cliente\\.domicilioTemp\\.numero").val(jQuery("#cliente\\.numeroDom").val());
	jQuery("#cliente\\.domicilioTemp\\.codigoPostal").val(jQuery("#cliente\\.codigoPostal").val());
	jQuery("#cliente\\.domicilioTemp\\.colonia").val(jQuery("#cliente\\.nombreColonia").val());
	jQuery("#cliente\\.domicilioTemp\\.cvePais").val(jQuery("#cliente\\.idPaisString").val());
	
	jQuery("#cliente\\.idPaisString").val("PAMEXI");
	jQuery("#cliente\\.idEstadoDirPrin").val("");
	jQuery("#cliente\\.idMunicipioDirPrin").val("");
	jQuery("#cliente\\.nombreColonia").val("");
	jQuery("#cliente\\.codigoPostal").val("");
	jQuery("#cliente\\.numeroDom").val("");
	jQuery("#cliente\\.nombreCalle").val("");
	jQuery("#cliente\\.nombreColoniaDiferente").val("");
	jQuery("#bttnDomAceptar").hide();
	jQuery("#bttnDomCancelar").show();
}

function cancelarDomicilio(){
	
	jQuery("#bttnDomAceptar").show();
	jQuery("#bttnDomCancelar").hide();
	

	jQuery("#cliente\\.idPaisString").val(jQuery("#cliente\\.domicilioTemp\\.cvePais").val());
	onChangePais('cliente.idEstadoDirPrin','cliente.idMunicipioDirPrin','cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idPaisString');
	jQuery("#cliente\\.idEstadoDirPrin").val(jQuery("#cliente\\.domicilioTemp\\.cveEstado").val());
	onChangeEstadoGeneral('cliente.idMunicipioDirPrin','cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idEstadoDirPrin');
	jQuery("#cliente\\.idMunicipioDirPrin").val(jQuery("#cliente\\.domicilioTemp\\.cveCiudad").val());
	onChangeCiudad('cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idMunicipioDirPrin');
	jQuery("#cliente\\.nombreColonia").val(jQuery("#cliente\\.domicilioTemp\\.colonia").val());
	onChangeColonia('cliente.codigoPostal','cliente.nombreCalle', 'cliente.nombreColonia','cliente.idMunicipioDirPrin');
	jQuery("#cliente\\.numeroDom").val(jQuery("#cliente\\.domicilioTemp\\.numero").val());
	jQuery("#cliente\\.nombreCalle").val(jQuery("#cliente\\.domicilioTemp\\.calle").val());
	jQuery("#cliente\\.codigoPostal").val(jQuery("#cliente\\.domicilioTemp\\.codigoPostal").val());
	
}
function guardarCliente(divCarga){
	var idDiv=(divCarga!=null)?divCarga:'contenido';
	var url="/MidasWeb/catalogoCliente/guardarCliente.action";
	var data=jQuery("#clientForm").serialize();
	//sendRequestJQ(null,url+"?"+data,idDiv,null);
	var rfc=jQuery("#cliente\\.codigoRFC").val();
	var booleanTipoPersonaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
	var booleanTipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
	var tipoPersona=null;
	tipoPersona=(booleanTipoPersonaMoral)?2:1;
	var rfcValido=validarRFC();
	validacionesPrimaMayor();
	if(validateForId('clientForm', true)){
		if(tipoPersona==1){
			if(jQuery.isValid(rfc) /*&& rfcValido*/){
				if((esMayorEdadServidor(jQuery("#cliente\\.fechaNacimiento").val()))){
					
					
					operacionGenericaConParamsRendererIntoDiv(url+"?"+data+"&divCarga="+idDiv,null,idDiv);
				}
			}else{
				alert("RFC inválido!");
			}
		}else if(jQuery.isValid(rfc) && tipoPersona==2){
			
			operacionGenericaConParamsRendererIntoDiv(url+"?"+data+"&divCarga="+idDiv,null,idDiv);
		}else{
			alert("RFC invalido, favor de capturar el RFC y verificar el tipo de persona jurídica");
		}
	}
}

/**
 * Verifica si aplican las validaciones de prima mayor a 2,500 usd
 * @returns {Boolean}
 */

function validacionesPrimaMayor(){
	//Validaciones Cliente Único
	var idCliente = jQuery("#cliente\\.idCliente").val();
	var booleanTipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
	var tipoPersona=null;
	tipoPersona=(booleanTipoPersonaMoral)?2:1;
	
	
	var $giroCNSF;
	var $paisNacimiento;
	if(tipoPersona == 1){
		$paisNacimiento = jQuery("#cliente\\.idPaisNacimiento");
		$giroCNSF = jQuery("#cliente\\.ocupacionCNSF");
	}else{
		$paisNacimiento = jQuery("#cliente\\.idPaisConstitucion");
		$giroCNSF = jQuery("#cliente\\.cveGiroCNSF");
	}
	
	var cveGiroCNSF = $giroCNSF.val();
	var idPaisNacimiento = $paisNacimiento.val();
	
	
	if(idCliente){
		
		if(!idPaisNacimiento || !cveGiroCNSF){
			var valPrimaMayor = consultarPrima();
			
			if(valPrimaMayor){
				
				$giroCNSF.addClass("jQrequired");
				$paisNacimiento.addClass("jQrequired");
			
			}else{
				
				$giroCNSF.removeClass("jQrequired");
				$paisNacimiento.removeClass("jQrequired");
				
				if(!idPaisNacimiento){
					$paisNacimiento.css({'display':'none'});
					jQuery('label[for="' + $paisNacimiento.attr('id') +  '"]').css({'visibility':'hidden'});
				}
				
				
				if(!cveGiroCNSF){
					$giroCNSF.css({'display':'none'});
					jQuery('label[for="' + $giroCNSF.attr('id') +  '"]').css({'visibility':'hidden'});
				}
			}
			
		}
		
	}else{
		$giroCNSF.removeClass("jQrequired");
		$paisNacimiento.removeClass("jQrequired");
		$paisNacimiento.css({'visibility':'hidden'});
		$giroCNSF.css({'visibility':'hidden'});
				
		jQuery('label[for="' + $paisNacimiento.attr('id') +  '"]').css({'visibility':'hidden'});
		jQuery('label[for="' + $giroCNSF.attr('id') +  '"]').css({'visibility':'hidden'});
		
		$paisNacimiento.val('');
		
	}
}


/**
 * Consultar ws prima mayor
 * @returns {Boolean}
 */
function consultarPrima(){
	if(resultadoWSPrima == null){
		var url="/MidasWeb/catalogoCliente/validateClientePrima.action";
		var data={"cliente.idClienteUnico":jQuery("#cliente\\.idClienteUnico").val()};
		//url+="?cliente.claveTipoPersona="+idTipoPersona+"&cliente.idNegocio="+idNegocio;
		jQuery.ajax({
			url: url,
			dataType: 'json',
			async:false,
			type:"GET",
			data: data,
			success: function(params){
				try{
					primaJSON = JSON.parse(params.primaJSON);
					resultadoWSPrima = primaJSON.value;
				}catch(e){
					alert("Error al validar la prima acumulada.");
				}
			}
		});
	}
	
	return resultadoWSPrima;
}

/**
 * Indica si es igual o no el RFC esperado
 * @returns {Boolean}
 */
function validarRFC(){
	 var nombre=jQuery("#cliente\\.nombre").val();
	 var apPaterno=jQuery("#cliente\\.apellidoPaterno").val();
	 var apMaterno=jQuery("#cliente\\.apellidoMaterno").val();
	 var fechaNac=jQuery("#cliente\\.fechaNacimiento").val();
	 var rfcActual=jQuery("#cliente\\.codigoRFC").val();
	 var RFC=getMidasRFCByFields(nombre, apPaterno,apMaterno,fechaNac);
	 return RFC==rfcActual;
}

function switchPorTipoPersona(idTipoPersona,divCarga){
	var idDiv=(divCarga!=null)?divCarga:'contenido';
	var params=jQuery("#clientForm").serialize()+"&divCarga="+idDiv;
	sendRequestJQ(
			null,
			'/MidasWeb/catalogoCliente/switchTipoPersona.action?tipoPersona='+idTipoPersona+"&"+params,
			'workAreaClientes',
			'dhx_init_tabbars();jQuery("#btnGuardar").bind("click",guardarCliente);rescribirEtiquetas('+idTipoPersona+');'
	);
}

function enableDisableField(idField,disabled,display){
	if(idField){
		var field=jQuery("#"+idField);
		if(field){
			field.attr("disabled",disabled);
			field.css("display",display);
		}
	}
}
/**
 * Funcion que hace el switch cuando se palomea que es colonia diferente o no.
 * @param idCheckbox
 * @param idComboColonia
 * @param idCampoColoniaDiferente
 */
function switchColonias(idCheckbox,idComboColonia,idCampoColoniaDiferente){
	var esColoniaDiferente=jQuery("#"+idCheckbox);
	alert("Campo:"+esColoniaDiferente+"|Valor:"+esColoniaDiferente.attr("checked"));
	if(esColoniaDiferente!=null && (esColoniaDiferente.attr("checked")==true)){
		alert("Parte verdadera");
		jQuery("#"+idComboColonia).attr("disabled",true);
		alert("Combo colonia:"+jQuery("#"+idComboColonia));
		jQuery("#"+idCampoColoniaDiferente).attr("disabled",false);
		alert("Colonia diferente:"+jQuery("#"+idCampoColoniaDiferente).attr("disabled"));
	}else{
		jQuery("#"+idCampoColoniaDiferente).attr("disabled",true);
		jQuery("#"+idComboColonia).attr("disabled",false);
	}
}
function copiarDatosGenerales(){
	var tipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
	if(tipoPersonaMoral){
		dwr.util.setValue("cliente.razonSocialFiscal",dwr.util.getValue("cliente.razonSocial"));
		dwr.util.setValue("cliente.fechaNacimientoFiscal",dwr.util.getValue("cliente.fechaConstitucion"));		
	}else{
		dwr.util.setValue("cliente.nombreFiscal",dwr.util.getValue("cliente.nombre"));
		dwr.util.setValue("cliente.apellidoPaternoFiscal",dwr.util.getValue("cliente.apellidoPaterno"));	
		dwr.util.setValue("cliente.apellidoMaternoFiscal",dwr.util.getValue("cliente.apellidoMaterno"));
	}
	
	var idPais=dwr.util.getValue("cliente.idPaisString");
	var idEstado=dwr.util.getValue("cliente.idEstadoDirPrin");
	var idCiudad=dwr.util.getValue("cliente.idMunicipioDirPrin");
	var idColonia=dwr.util.getValue("cliente.nombreColonia");
	var calleNumero=dwr.util.getValue("cliente.nombreCalle");
	var codPostal=dwr.util.getValue("cliente.codigoPostal");
	var esNuevaColonia=jQuery("#idColoniaCheck").attr("checked");
	var nuevaColonia=dwr.util.getValue("cliente.nombreColoniaDiferente");
	var numero=dwr.util.getValue("cliente.numeroDom");
	if(calleNumero!='' && (idColonia!=''|| (nuevaColonia!='' && esNuevaColonia==true)) && codPostal!=''){
		var estadoName='cliente.idEstadoFiscal';		
		var ciudadName='cliente.idMunicipioFiscal';
		var coloniaName='cliente.nombreColoniaFiscal';
		var calleNumeroName = 'cliente.nombreCalleFiscal';
		var codigoPostalName = 'cliente.codigoPostalFiscal';		
		var coloniaDiferenteName='cliente.nombreColoniaDiferenteFiscal';
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, idColonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);		
		dwr.util.setValue(codigoPostalName, codPostal);
		jQuery('#idColoniaCheckFiscal').attr("checked",esNuevaColonia);
		dwr.util.setValue(coloniaDiferenteName, nuevaColonia);
		dwr.util.setValue('cliente.numeroDomFiscal', numero);
		}
		else{
			alert('El domicilio a copiar esta incompleto, favor de verificar.');
		}
}

function ajaxTimeout(callback){
	jQuery.ajax({
		url: '',dataType: 'json',async:true,
		type:"GET",contentType:'application/json',
		success:callback,
		error:callback
	});
}

function cargarComboDatosGenerales(){
	alert("Cargando datos del cliente");
}

function cargarComboDatosContacto(){
	alert("Cargando datos del contacto");
}

function cargarComboDatosCobranza(){
	alert("Cargando datos de cobranza");
}

function cargarComboDatosFiscales(){
	alert("Cargando datos fiscales");
}

function cargarComboAvisoSiniestro(){
	alert("Cargando datos de aviso de siniestro");
}

function cargarComboCanalVentas(){
	alert("Cargando datos de canal de ventas");
}

function guardarContactoAviso(){
	var url="/MidasWeb/catalogoCliente/guardarContactoAvisoSiniestro.action";
	var data=jQuery("#clientForm").serialize();
	jQuery.ajax({
		url: url,
		dataType: 'json',
		async:true,
		type:"POST",
		data: data,
		success: mostrarMensaje
	});
}


function guardarDatosCobranza(){
	if (dwr.util.getValue("cliente.mesFechaVencimiento") > 12) {
		mostrarMensajeInformativo("El mes de vencimiento es invalido","30", null,null);
		return false;
	}
	if(dwr.util.getValue("cliente.idTipoConductoCobro")!=''&&
			dwr.util.getValue("cliente.nombreTarjetaHabienteCobranza")!=''&&
			dwr.util.getValue("cliente.codigoRFCCobranza")!=''&&
			dwr.util.getValue("cliente.nombreColoniaCobranza")!=''&&
			dwr.util.getValue("cliente.nombreCalleCobranza")!=''&&
			dwr.util.getValue("cliente.codigoPostalCobranza")!=''&&
			dwr.util.getValue("cliente.idEstadoCobranza")!=''&&
			dwr.util.getValue("cliente.idMunicipioCobranza")!=''&&			
			dwr.util.getValue("cliente.idBancoCobranza")!=''&&
			dwr.util.getValue("cliente.numeroTarjetaCobranza")!=''&&			
			(dwr.util.getValue("cliente.idTipoTarjetaCobranza")!=''&&
			dwr.util.getValue("cliente.codigoSeguridadCobranza")!=''&&
			dwr.util.getValue("cliente.mesFechaVencimiento")!=''&&
			dwr.util.getValue("cliente.anioFechaVencimiento")!=''&&
			dwr.util.getValue("cliente.diaPagoTarjetaCobranza")!=''||
			dwr.util.getValue("cliente.idTipoConductoCobro")==8)){
	var url="/MidasWeb/cliente/catalogoClienteJson/guardarDatosCobranza.action";
	var data=jQuery("#clientForm").serialize();
	jQuery.ajax({
		url: url,
		dataType: 'json',
		async:true,
		type:"POST",
		data: data,
		success:function(json){
			processJsonResponse(json);
		}
	});
	}
	else{
		var camposFaltantes='Los siguientes campos son requeridos: ';	
				if(dwr.util.getValue("cliente.idTipoConductoCobro")==''){
					camposFaltantes=camposFaltantes+' Medio de Pago,';
				}
				if(dwr.util.getValue("cliente.nombreTarjetaHabienteCobranza")==''){
					camposFaltantes=camposFaltantes+' Nombre Tarjetahabiente,';
				}
				if(dwr.util.getValue("cliente.codigoRFCCobranza")==''){
					camposFaltantes=camposFaltantes+' RFC,';
				}				
				if(dwr.util.getValue("cliente.idEstadoCobranza")==''){
					camposFaltantes=camposFaltantes+' Estado,';
				}
				if(dwr.util.getValue("cliente.idMunicipioCobranza")==''){
					camposFaltantes=camposFaltantes+' Municipio,';
				}
				if(dwr.util.getValue("cliente.nombreColoniaCobranza")==''){
					camposFaltantes=camposFaltantes+' Colonia,';
				}
				if(dwr.util.getValue("cliente.codigoPostalCobranza")==''){
					camposFaltantes=camposFaltantes+' Codigo Postal,';
				}
				if(dwr.util.getValue("cliente.nombreCalleCobranza")==''){
					camposFaltantes=camposFaltantes+' Calle y Número,';
				}								
				if(dwr.util.getValue("cliente.idBancoCobranza")==''){
					camposFaltantes=camposFaltantes+' Institución Bancaria,';
				}
				if(dwr.util.getValue("cliente.numeroTarjetaCobranza")==''){
					camposFaltantes=camposFaltantes+' Numero de cuenta o CLABE,';
				}				
				if(dwr.util.getValue("cliente.idTipoConductoCobro")!=8){
					if(dwr.util.getValue("cliente.idTipoTarjetaCobranza")==''){
						camposFaltantes=camposFaltantes+' Tipo de Tarjeta,';
					}
					if(dwr.util.getValue("cliente.codigoSeguridadCobranza")==''){
						camposFaltantes=camposFaltantes+' Código de Seguridad,';
					}
					if(dwr.util.getValue("cliente.mesFechaVencimiento")==''){
						camposFaltantes=camposFaltantes+' Mes Vencimiento,';
					}
					if(dwr.util.getValue("cliente.anioFechaVencimiento")==''){
						camposFaltantes=camposFaltantes+' Año Vencimiento,';
					}
					if(dwr.util.getValue("cliente.diaPagoTarjetaCobranza")==''){
						camposFaltantes=camposFaltantes+' Día de Pago,';
					}					
				}
				parent.mostrarMensajeInformativo(camposFaltantes.substring(0, camposFaltantes.length-1),10);
	}
}

function processJsonResponse(json){
	if(json){
		//Indica que ocurrió un error
		if(json.tipoMensaje==10){
			parent.mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);			
			//Indica que es un exito
		}else if(json.tipoMensaje==30){
			parent.mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);	
			limpiarDatos();
		}
	}
	cargarMediosPagoGrid();
}

function mostrarMensaje(json){
	if(json){
		alert(json.mensaje);
	}else{
		alert("No se generó ningun resultado!");
	}
}

function inicializarGridCobranza(){
	cobranzaGrid = new dhtmlXGridObject("clientesGrid");
	cobranzaGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	cobranzaGrid.setSkin("light");
	//mostrarIndicadorCarga('indicador');	
	cobranzaGrid.attachEvent("onXLE", function(grid){
		//ocultarIndicadorCarga('indicador');
    });			
	cobranzaGrid.init();
	cobranzaGrid.load(urlConsultaMediosPago);
}

function cargarMedioPagoPorIdMedioPago(rowId,columnId){
	var cell=cobranzaGrid.cellById(rowId,1);
	alert(cell.getValue());
}

function habilitarPorMedioPagoDomiciliacion(idTipoDomi){
	//Si es credito
	if(idTipoDomi!=null && (idTipoDomi==4 || idTipoDomi!=8)){
		jQuery("#idBancoCobranza").attr("disabled",false);
		jQuery("#idTipoTarjetaCobranza").attr("disabled",false);
		jQuery("#numeroTarjetaCobranza").attr("disabled",false);
		jQuery("#codigoSeguridadCobranza").attr("disabled",false);
		jQuery("#mesFechaVencimiento").attr("disabled",false);
		jQuery("#anioFechaVencimiento").attr("disabled",false);
		jQuery("#diaPagoTarjetaCobranza").attr("disabled",false);
		if(idTipoDomi==4){
			jQuery("label[for='numeroTarjetaCobranza']").html("Número de tarjeta");
		}else{
			jQuery("label[for='numeroTarjetaCobranza']").html("Número de cuenta");
		}
	}else if(idTipoDomi==8){//Si es domiciliacion
		jQuery("#idBancoCobranza").attr("disabled",false);
		jQuery("#numeroTarjetaCobranza").attr("disabled",false);
		jQuery("#idTipoTarjetaCobranza").attr("disabled",true);
		jQuery("#codigoSeguridadCobranza").attr("disabled",true);
		jQuery("#codigoSeguridadCobranza").val("");
		jQuery("#mesFechaVencimiento").attr("disabled",true);
		jQuery("#mesFechaVencimiento").val("");
		jQuery("#anioFechaVencimiento").attr("disabled",true);
		jQuery("#anioFechaVencimiento").val("")
		jQuery("#diaPagoTarjetaCobranza").attr("disabled",true);
		jQuery("#diaPagoTarjetaCobranza").val("");
		jQuery("label[for='numeroTarjetaCobranza']").html("CLABE");
		jQuery("#idTipoTarjetaCobranza").val("");
		jQuery("#codigoSeguridadCobranza").val("");
		jQuery("#mesFechaVencimiento").val("");
		jQuery("#anioFechaVencimiento").val("");
		jQuery("#diaPagoTarjetaCobranza").val("");
	}
}

function regresarListarClientes(divCarga,tipoAccion,idCliente, tipoRegreso){	
	//var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?tipoBusqueda=1";
	if(parent.dwr.util.getValue("moduloOrigen")==null || parent.dwr.util.getValue("moduloOrigen")==''){
	var idDiv=(divCarga!=null)?divCarga:'contenido';
	var params="idField=idClienteResponsable";
	params+="&tipoBusqueda=1";
	if(jQuery.isValid(idDiv)){
		params+="&divCarga="+idDiv;
	}
	if(jQuery.isValid(tipoAccion)){
		params+="&tipoAccion="+tipoAccion;
	}
	if(jQuery.isValid(idCliente)){
		params+="&filtroCliente.idCliente="+idCliente;
	}
	var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?"+params;
	if(tipoRegreso == 1){
		var idNegocio = jQuery("#idNegocio").val();
		if(idNegocio != null && idNegocio != undefined){
			url="/MidasWeb/negocio/verDetalle.action?claveNegocio=A&id="+idNegocio+"&tabActiva=clientes";
		}else{
			url="/MidasWeb/negocio/listar.action?claveNegocio=A";
		}
		divCarga = 'contenido';
	}
	if(tipoRegreso == 2){
		var idToCotizacion = jQuery("#idToCotizacion").val();
		if(idToCotizacion != null && idToCotizacion != undefined){
			url="/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId="+idToCotizacion;
		}else{
		}
		divCarga = 'contenido';
	}
	if(tipoRegreso == 3){
		var idToPoliza = jQuery("#idToPoliza").val();
		var validoEn = jQuery("#validoEn").val();
		var validoEnMillis = jQuery("#validoEnMillis").val();
		var recordFrom = jQuery("#recordFrom").val();
		var recordFromMillis = jQuery("#recordFromMillis").val();
		var claveTipoEndoso = jQuery("#claveTipoEndoso").val();
		
		url = '/MidasWeb/poliza/verContenedorPoliza.action?id='+idToPoliza;
		if (validoEn != null) {
			url+= "&validoEn=" +validoEn;
		}
		if (validoEnMillis != null) {
			url+= "&validoEnMillis=" + validoEnMillis;
		}
		if (recordFrom != null) {
			url+= "&recordFrom=" + recordFrom;
		}
		if (recordFromMillis != null) {
			url+= "&recordFromMillis=" + recordFromMillis;
		}
		
		divCarga = 'contenido';
	}

	if(tipoRegreso == 4){
		var idToCotizacion = jQuery("#idToCotizacion").val();
		if(idToCotizacion != null && idToCotizacion != undefined){
			url="/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrarContenedor.action?tabActiva=complementar&idToCotizacion="+idToCotizacion;
		}else{
		}
		divCarga = 'contenido';
	}
	
	operacionGenericaConParamsRendererIntoDiv(url,null,divCarga);
	}
	else{
		var path= "/MidasWeb/fuerzaventa/agente/verDetalle.action";
		sendRequestJQ(null, path + "?tipoAccion=4&tabActiva=carteraClientes&agente.id="+parent.dwr.util.getValue("idAgente"), targetWorkArea, 'dhx_init_tabbars();');
	}
	//sendRequestJQ(null,url,'contenido',null);
}

function ocultarCamposTipoPersona(tipoPersona){
	if(tipoPersona==1){
		var trs=jQuery(".pf");
		if(trs!=null && trs.length>0){
			for(var i=0;i<trs.length;i++){
				var row=trs[i];
				jQuery(row).css("display","");
				jQuery(".pm").css("display","none");
			}
		}
		jQuery(".pm").find("input").val("");
	}else if(tipoPersona==2){
		jQuery(".pf").css("display","none");
		jQuery(".pf").find("input").val("");
		jQuery(".pm").css("display","");
	}
	
	jQuery("#clienteGrid").html("");
//	jQuery("#pagingArea").html("");
	jQuery("#infoArea").html("");
	jQuery("#pagingArea_Clientes").html("");
	
}

function filtrarClientes(){
	var filtroCliente=jQuery("#filtroIdCliente");
	var idCliente=null;
	if(filtroCliente){
		idCliente=filtroCliente.val();
	}
	if(validateForId('clientForm', true,'search')){
		listarFiltradoGenerico(listarFiltradoClientePath, 'clienteGrid', document.clienteForm, '<s:property value="idField"/>', 'clienteModal')
	}else{
		false
	}
}

function setTabActiva(divTab){
	jQuery("#tabActiva").val(divTab);
}

function regresarAgente() {	
	var path= "/MidasWeb/fuerzaventa/agente/verDetalle.action";
	sendRequestJQ(null, path + "?tipoAccion="+parent.dwr.util.getValue("tipoAccionAgente")+"&tabActiva=carteraClientes&idTipoOperacion=50&agente.id="+parent.dwr.util.getValue("idAgente")+"&idRegistro="+parent.dwr.util.getValue("idAgente"), targetWorkArea, 'dhx_init_tabbars();');	
}

function copiarValor(txt1, txt2){
	dwr.util.setValue(txt2,dwr.util.getValue(txt1));
	}

function limpiarDatos(){
	dwr.util.setValue("cliente.idTipoConductoCobro","");	
	dwr.util.setValue("cliente.idBancoCobranza","");
	dwr.util.setValue("cliente.idTipoTarjetaCobranza","");
	dwr.util.setValue("cliente.numeroTarjetaCobranza","");
	dwr.util.setValue("cliente.codigoSeguridadCobranza","");
	dwr.util.setValue("cliente.mesFechaVencimiento","");
	dwr.util.setValue("cliente.anioFechaVencimiento","");
	dwr.util.setValue("cliente.diaPagoTarjetaCobranza","");		
}

function limpiarDatosTarjetaHabiente(){
	dwr.util.setValue("cliente.nombreTarjetaHabienteCobranza","");
	dwr.util.setValue("cliente.codigoRFCCobranza","");
	dwr.util.setValue("cliente.telefonoCobranza","");
	dwr.util.setValue("cliente.emailCobranza","");
	dwr.util.setValue("cliente.idEstadoCobranza","");
	dwr.util.setValue("cliente.idMunicipioCobranza","");
	dwr.util.setValue("cliente.nombreColoniaCobranza","");
	dwr.util.setValue("cliente.codigoPostalCobranza","");
	dwr.util.setValue("cliente.nombreCalleCobranza","");	
}