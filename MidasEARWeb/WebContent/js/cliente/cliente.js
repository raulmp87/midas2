
var mascaraRFC="";
var tipoPersona=0;
function validaRFC(obj){
	 var cadena =obj.value;
	 if(cadena.length==0){
		 return false;
	 }
	 if(tipoPersona==0){
		 var personaFisica = document.getElementsByName('claveTipoPersona')[0].checked;
			if(personaFisica) {
				tipoPersona=1;
				}	
				else{
				tipoPersona=2;
				}
		}
	 if(tipoPersona==1){
		 if(cadena.length<13){
			 parent.mostrarVentanaMensaje("20","Se debe cumplir con el formato de RFC para persona fisica: AAAANNNNNNAAA, verifique", null);
			 obj.value=obj.value.toUpperCase();
			 obj.focus();
			 return false;
		 }
		 if(validaRFCPersonaFisica(cadena)){
			 obj.value=obj.value.toUpperCase();
			 return true;
		 }else{
			 parent.mostrarVentanaMensaje("20","Se debe cumplir con el formato de RFC para persona fisica: AAAANNNNNNAAA, verifique", null);
			 obj.value=obj.value.toUpperCase(); 
			 obj.focus();
			 return false;
		 }
	 }else if(tipoPersona==2){
		 if(cadena.length<12){
			 parent.mostrarVentanaMensaje("20","Se debe cumplir con el formato de RFC para persona moral: AAANNNNNNAAA, verifique", null);
			 obj.value=obj.value.toUpperCase(); 
			 obj.focus();
			 return false;
		 }
		 if(validaRFCPersonaMoral(cadena)){
			 obj.value=obj.value.toUpperCase();
			 return true;
		 }else{
			 parent.mostrarVentanaMensaje("20","Se debe cumplir con el formato de RFC para persona moral: AAANNNNNNAAA, verifique", null);
			 obj.value=obj.value.toUpperCase(); 
			 obj.focus();
			 return false;
		 }
	 }
	

}

function validaRFCPersonaFisica(cadena){
	var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|(&)*|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
	var validRfc=new RegExp(valid);
	var matchArray=cadena.match(validRfc);
	if (matchArray==null) {
	
		return false;
		
	}
	else
	{ 
		
		return true;
	}
}

function validaRFCPersonaMoral(cadena){
	var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|(&)*|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
	var validRfc=new RegExp(valid);
	var matchArray=cadena.match(validRfc);
	if (matchArray==null) {
		
		return false;
		
	}
	else
	{ 
		
		return true;
	}
}



function agregarMascaraRFC(obj){
	
	if($('claveTipoPersona').value='1'){
		return new Mask(mascaraRFC,  'string').attach(obj);
	}else if($('claveTipoPersona').value='2'){
		return new Mask('***######***',  'string').attach(obj);
	}
}
