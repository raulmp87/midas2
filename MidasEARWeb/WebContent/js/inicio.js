var cometd = $.cometd;
cometd.websocketEnabled = false;
(function($)
		{
		    $(document).ready(function()
		    {
		    	// Function invoked when first contacting the server and
		        // when the server has lost the state of this client
		        function _metaHandshake(handshake)
		        {
		            if (handshake.successful === true)
		            {
		            	cometdUtil.handshakeResubscribe();
		            }
		        }

		        // Disconnect when the page unloads
		        $(window).unload(function()
		        {
		            cometd.disconnect(true);
		        });

		        var cometUrl = config.baseUrl + "/cometd";
		        cometd.configure({
		            url: cometUrl,
		            logLevel: 'debug'
		        });

		        cometd.addListener('/meta/handshake', _metaHandshake);
		        cometd.handshake();
		    });
		})(jQuery);


soundManager.setup({
	  url: config.baseUrl + '/js/soundmanager2',
	  onready: function() {
	  },
	  ontimeout: function() {		  
	  }
});