/** Inicio de Funciones para CRUD de Tarifas **/
var dhxWins=null;
var contVentanas=0;
var ventanaTarifa=null;
var ventanaParticipacionCorredor = null;
var ventanaParticipacionFacultativo = null;
var ventanaCotizacion=null;
var ventanaAsignaSolicitudEndoso=null;

function obtenerContenedorVentanas(){
	if(dhxWins == null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	return dhxWins;
}

function cerrarContenedorVentanas(id){
	if(parent.dhxWins == null){
		parent.cerrarVentanaModal(id);
	}else{
		if(parent.dhxWins.window(id) == null){
			parent.cerrarVentanaModal(id);
		}else{
			parent.dhxWins.window(id).setModal(false);
			parent.dhxWins.window(id).hide();
			parent.dhxWins.window(id).close();
		}
	}
}

function closeDhtmlxWindows(id){
	if(parent.dhxWins == null){
		parent.cerrarVentanaModal(id);
	}else{
		if(parent.dhxWins.window(id) == null){
			parent.cerrarVentanaModal(id);
		}else{
			parent.dhxWins.window(id).setModal(false);
			parent.dhxWins.window(id).close();	
		}
	}
}

function mostrarCancelarCotizacion(idToCotizacion){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	if (ventanaCotizacion==null || parent.dhxWins.window("Cotizacion").isHidden()){
		ventanaCotizacion = dhxWins.createWindow("Cotizacion", 400, 320, 610, 260);
		ventanaCotizacion.setText("Cancelar/Rechazar Cotizaci&oacute;n.");
		ventanaCotizacion.setModal(true);
		ventanaCotizacion.attachURL("/MidasWeb/cotizacion/cotizacion/mostrarCancelarCotizacion.do?idToCotizacion="+idToCotizacion);
		ventanaCotizacion.button("close").disable();
		ventanaCotizacion.attachEvent("onClose", cerrarVentanaOT);
	}
}
function mostrarImprimirEndosoPoliza(idToPoliza){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaCotizacion = dhxWins.createWindow("OrdenTrabajo", 400, 320, 610, 260);
	ventanaCotizacion.setText("Impresi&oacute;n de endoso.");
	ventanaCotizacion.setModal(true);
	ventanaCotizacion.center();
	ventanaCotizacion.attachURL("/MidasWeb/poliza/mostrarImprimirEndoso.do?idToPoliza="+idToPoliza + "&accion=imprimir");
}

function mostrarConsultaEndosoPoliza(idToPoliza){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaCotizacion = dhxWins.createWindow("OrdenTrabajo", 400, 320, 610, 260);
	ventanaCotizacion.setText("Consulta de endosos.");
	ventanaCotizacion.setModal(true);
	ventanaCotizacion.center();
	ventanaCotizacion.attachURL("/MidasWeb/poliza/mostrarImprimirEndoso.do?idToPoliza="+idToPoliza + "&accion=consultar");
}

function mostrarCancelarEndosoPoliza(idToPoliza){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaCotizacion = dhxWins.createWindow("OrdenTrabajo", 300, 320, 920, 260);
	ventanaCotizacion.setText("Cancelaci&oacute;n de endosos.");
	ventanaCotizacion.setModal(true);
	ventanaCotizacion.center();
	ventanaCotizacion.attachURL("/MidasWeb/endoso/cancelacion/listar.do?idToPoliza="+idToPoliza);
}

function mostrarRehabilitarEndosoPoliza(idToPoliza){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaCotizacion = dhxWins.createWindow("OrdenTrabajo", 300, 320, 920, 260);
	ventanaCotizacion.setText("Rehabilitaci&oacute;n de endosos.");
	ventanaCotizacion.setModal(true);
	ventanaCotizacion.center();
	ventanaCotizacion.attachURL("/MidasWeb/endoso/rehabilitacion/listar.do?idToPoliza="+idToPoliza);
}

function cancelarEndosoPoliza(idToPoliza, numeroEndoso, idFecha){
	var actionConfirmar = '/MidasWeb/endoso/cancelacion/validarEndosoCancelable.do?idToPoliza=' + idToPoliza 
				+ '&numeroEndoso=' + numeroEndoso
	new Ajax.Request(actionConfirmar, {
		method : "post",
		encoding : "UTF-8",
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;	
			showIndicatorFromChild();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicatorFromChild();
		}, // End of onComplete
		onSuccess : function(transport) {
			var items = transport.responseXML.getElementsByTagName("item");
			var item = items[0];
			var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			
			if(confirm(mensaje)){
				var fecha = document.getElementById(idFecha.id).value;
		
				sendRequestCancelarRehabilitarEndoso('/MidasWeb/endoso/cancelacion/cancelarEndoso.do?idToPoliza=' + idToPoliza 
					+ '&numeroEndoso=' + numeroEndoso + '&fechaIniVigencia=' + fecha,
					'parent.dhxWins.window(\'OrdenTrabajo\').close();');
			}
		} // End of onSuccess
	});
}

function rehabilitarEndosoPoliza(idToPoliza, numeroEndoso, idFecha){
	if (confirm('\u00bfRealmente deseas rehabilitar el endoso ' + numeroEndoso + '?')){
		
		var fecha = document.getElementById(idFecha.id).value;
		
		sendRequestCancelarRehabilitarEndoso('/MidasWeb/endoso/rehabilitacion/rehabilitarEndoso.do?idToPoliza=' + idToPoliza 
				+ '&numeroEndoso=' + numeroEndoso + '&fechaIniVigencia=' + fecha,
				'parent.dhxWins.window(\'OrdenTrabajo\').close();');
	}
}


function sendRequestCancelarRehabilitarEndoso(actionURL, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;	
			showIndicatorFromChild();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicatorFromChild();
		}, // End of onComplete
		onSuccess : function(transport) {
		
		//alert (transport.responseXML);
			procesarRespuestaXml(transport.responseXML, pNextFunction);
		} // End of onSuccess
	});
	
}

function showIndicatorFromChild() {
	if (totalRequests === 1 && parent.$("blockWindow") !== null) {
		backgroundFilterFromChild("blockWindow");
		var centralIndicator = parent.$("central_indicator");

		var pageW = (isIE) ? parent.document.body.offsetWidth - 20 : (parent.innerWidth - 50);
		var pageH = (isIE) ? parent.document.body.offsetHeight : (parent.innerHeight);
		imageX = 235;
		imageY = 135;
		pageX = (pageH / 2) - (imageY);
		pageY = (pageW / 2) - (imageX / 2);
		centralIndicator.style.top = pageX + 'px';
		centralIndicator.style.left = pageY + 'px';
		centralIndicator.style.display = 'block';
	} // End of if
}

function hideIndicatorFromChild() {
	if (totalRequests === 0 && parent.$("blockWindow") !== null) {
		backgroundFilterFromChild("blockWindow");
		var centralIndicator = parent.$("central_indicator");
		centralIndicator.style.display = "none";
	} // End of if
}


function cerrarVentanaCot(){
	parent.dhxWins.window("Cotizacion").setModal(false);
	parent.dhxWins.window("Cotizacion").hide();
	parent.ventanaOrdenTrabajo=null;
	delay(500);
	//sendRequest(null,"/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do", "contenido",null);
}
function ocultarVentanaCot(){
	parent.dhxWins.window("Cotizacion").setModal(false);
	parent.dhxWins.window("Cotizacion").hide();	
}

function cerrarVentanaCopiarInciso(fobj) {
	if(document.getElementById('origen').value == 'ODT') {
		sendRequest(fobj,'/MidasWeb/cotizacion/inciso/copiar.do','contenido_resumenIncisos','creaArbolOrdenesDeTrabajo(' + fobj.idCotizacion.value + '); mostrarVentanaMensajeInciso();');
	} else {
		sendRequest(fobj,'/MidasWeb/cotizacion/inciso/copiar.do','contenido_resumenIncisos','creaArbolCotizacion(' + fobj.idCotizacion.value + '); mostrarVentanaMensajeInciso();');
	}
	dhxWins.window('copiarInciso').close();
}

function mostrarVentanaMensajeInciso(afterClose) {
	mostrarVentanaMensaje($('tipoMensajeInciso').value, $('mensajeInciso').value, afterClose);
}

function cerrarVentanaCopiarIncisoEndoso(fobj) {
	if(document.getElementById('origen').value == 'ODT') {
		sendRequest(fobj,'/MidasWeb/cotizacion/endoso/inciso/copiar.do','contenido_resumenIncisos','creaArbolCotizacionEndoso(' + fobj.idCotizacion.value + ', '+ fobj.claveTipoEndoso.value +');procesarRespuesta(null);');
	} else {
		sendRequest(fobj,'/MidasWeb/cotizacion/endoso/inciso/copiar.do','contenido_resumenIncisos','creaArbolCotizacionEndoso(' + fobj.idCotizacion.value + ', '+ fobj.claveTipoEndoso.value +');procesarRespuesta(null);');
	}
	dhxWins.window('copiarInciso').close();
}

function mostrarCopiarInciso(idCotizacion,numeroInciso){
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var copiarInciso = dhxWins.createWindow("copiarInciso", 34, 100, 600, 150);
	copiarInciso.setText("Copiar Inciso");
	copiarInciso.button("minmax1").hide();
	copiarInciso.button("park").hide();
	copiarInciso.setModal(true);
	copiarInciso.center();
	copiarInciso.denyResize();
	copiarInciso.attachURL("/MidasWeb/cotizacion/inciso/copiar.do?idCotizacion=" + idCotizacion + "&numeroInciso=" + numeroInciso);
}
function mostrarCopiarIncisoEndoso(idCotizacion,numeroInciso){
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var copiarInciso = dhxWins.createWindow("copiarInciso", 34, 100, 600, 150);
	copiarInciso.setText("Copiar Inciso");
	copiarInciso.button("minmax1").hide();
	copiarInciso.button("park").hide();
	copiarInciso.setModal(true);
	copiarInciso.center();
	copiarInciso.denyResize();
	copiarInciso.attachURL("/MidasWeb/cotizacion/endoso/inciso/copiar.do?idCotizacion=" + idCotizacion + "&numeroInciso=" + numeroInciso);
}

function mostrarBuscarPoliza() {
	if(document.getElementById('apartirPoliza').checked) {
		document.getElementById('buscarPoliza').style.display = '';
		document.getElementById('idToProducto').disabled = 'disabled';
	} else {
		document.getElementById('buscarPoliza').style.display = 'none';
		document.getElementById('idToProducto').disabled = '';
	}
}

function limpiarCamposSolicitud() {
	document.getElementById('idToProducto').value = '';
	document.getElementById('numPoliza').value = '';
	document.getElementById('idPoliza').value = '';
}

function mostrarTipoPersonaSolicitud() {
	var personaFisica = document.getElementsByName('tipoPersona')[0].checked;
	if(personaFisica) {
		document.getElementById('nombreDiv').style.display = '';
		document.getElementById('razonSocialDiv').style.display = 'none';
		document.getElementsByName('razonSocial')[0].value = '';
	} else {
		document.getElementById('nombreDiv').style.display = 'none';
		document.getElementById('razonSocialDiv').style.display = '';
		document.getElementsByName('nombres')[0].value = '';
		document.getElementsByName('apellidoPaterno')[0].value = '';
		document.getElementsByName('apellidoMaterno')[0].value = '';
	}
}

function sendRequestSolicitud(fobj, url, target, nextFunction, accion) {
	var contenido = document.getElementById(target);
	var fobj = document.solicitudForm;
	if(accion == "3") {
		sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);
		eval(nextFunction);
	} else {
		new Ajax.Request(url, {
			method : "post",
			encoding : "UTF-8",
			parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
					true) : null,
			onCreate : function(transport) {
				totalRequests = totalRequests + 1;
				showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				totalRequests = totalRequests - 1;
				hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
				contenido.innerHTML = transport.responseText;
				dhxWins.window('accionesSolicitud').close();
				if(accion == "1") {
					dhx_init_tabbars();
					var idCotizacion = document.getElementById("idToCotizacion").value;
					creaArbolOrdenesDeTrabajo(idCotizacion);
					inicializaObjetosEdicionOT(idCotizacion);
				} else {
					mostrarVentanaMensaje('30', 'La solicitud se registr&oacute; exitosamente.');
				}
				//parent.dhxWins.window('accionesSolicitud').close();
				//eval(nextFunction);
			} // End of onSuccess
		});
	}
}

function mostrarAccionesSolicitud(){
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var accionesSolicitud = dhxWins.createWindow("accionesSolicitud", 34, 100, 600, 150);
	accionesSolicitud.setText("Solicitud");
	accionesSolicitud.button("minmax1").hide();
	accionesSolicitud.button("park").hide();
	accionesSolicitud.button("close").hide();
	accionesSolicitud.setModal(true);
	accionesSolicitud.center();
	accionesSolicitud.denyResize();
	accionesSolicitud.attachURL("/MidasWeb/solicitud/mostrarAcciones.do");
}

function mostrarAsignarSolicitudCotizacion(idToPoliza, idToSolicitud){
	instanciarContenedorVentanas();
	if (ventanaCotizacion==null || parent.dhxWins.window("Cotizacion")==null || parent.dhxWins.window("Cotizacion") == undefined || parent.dhxWins.window("Cotizacion").isHidden()){
		ventanaCotizacion = dhxWins.createWindow("Cotizacion", 400, 320, 610, 190);
		ventanaCotizacion.setText("Asignar Cotizaci&oacute;n.");
		ventanaCotizacion.setModal(true);
		ventanaCotizacion.button("close").disable();		
		ventanaCotizacion.attachURL("/MidasWeb/cotizacion/cotizacion/mostrarAsignarSolicitudCotizacion.do?idToPoliza="+idToPoliza + "&idToSolicitud=" + idToSolicitud);
		ventanaCotizacion.attachEvent("onClose", cerrarVentanaCot);
	}
}

function asignarSolicitudCotizacion(fobj){
	var codigo = document.getElementById('idCodigoUsuario');
	if(codigo != undefined && codigo != null){
		if(codigo.selectedIndex != 0) {
			parent.showIndicatorSimple();
			new Ajax.Request('/MidasWeb/cotizacion/cotizacion/asignarSolicitudCotizacion.do', {
				method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
				onFailure : function(resp) {
					parent.hideIndicator();
					mostrarVentanaMensaje("10", "Ocurrio un error al asignar la cotizacion.", null);
					return;
					},
				onSuccess : function(transport) {
						parent.hideIndicator();
						if($('claveTipoEndoso') != null && $('claveTipoEndoso').value > 0){
							parent.listarCotizacionesEndoso();
							procesarRespuestaXml(transport.responseXML, "cerrarVentanaCot();");
						}else{
							parent.listarCotizaciones();
							procesarRespuestaXml(transport.responseXML, "cerrarVentanaCot();");
						}
					}
			});
		}
		else{
			alert("Seleccione un usuario para asignar la cotizacion.")
		}
	}
}

function mostrarAsignarCotizacion(idToCotizacion, idToSolicitud){
	instanciarContenedorVentanas();
	if (ventanaCotizacion==null || parent.dhxWins.window("Cotizacion") == null || parent.dhxWins.window("Cotizacion").isHidden()){
		ventanaCotizacion = dhxWins.createWindow("Cotizacion", 400, 320, 610, 190);
		ventanaCotizacion.setText("Asignar Cotizaci&oacute;n.");
		ventanaCotizacion.setModal(true);
		ventanaCotizacion.button("close").disable();		
		ventanaCotizacion.attachURL("/MidasWeb/cotizacion/cotizacion/mostrarAsignarCotizacion.do?idToCotizacion="+idToCotizacion + "&idToSolicitud=" + idToSolicitud);
		ventanaCotizacion.attachEvent("onClose", cerrarVentanaCot);
	}
}
function asignarCotizacion(fobj){
	var codigo = document.getElementById('idCodigoUsuario');
	if(codigo != undefined && codigo != null){
		if(codigo.selectedIndex != 0) {
			parent.showIndicatorSimple();
			new Ajax.Request('/MidasWeb/cotizacion/cotizacion/asignarCotizacion.do', {
				method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
				onFailure : function(resp) {
					parent.hideIndicator();
					mostrarVentanaMensaje("10", "Ocurrio un error al asignar la cotizacion.", null);
					return;
					},
				onSuccess : function(transport) {
						parent.hideIndicator();
						if($('claveTipoEndoso') != null && $('claveTipoEndoso').value > 0){
							parent.listarCotizacionesEndoso();
							procesarRespuestaXml(transport.responseXML, "cerrarVentanaCot();");
						}else{
							parent.listarCotizaciones();
							procesarRespuestaXml(transport.responseXML, "cerrarVentanaCot();");
						}
					}
			});
		}
		else{
			alert("Seleccione un usuario para asignar la cotizacion.")
		}
	}
}

function listarCotizaciones(){
	parent.sendRequest(document.cotizacionForm,"/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do","contenido",null);
}

function mostrarAsignarOrdenTrabajo(idToCotizacion, idToSolicitud){
	instanciarContenedorVentanas();
	if (ventanaOrdenTrabajo==null || parent.dhxWins.window("OrdenTrabajo")==null || parent.dhxWins.window("OrdenTrabajo") == undefined || parent.dhxWins.window("OrdenTrabajo").isHidden()){
		ventanaOrdenTrabajo = dhxWins.createWindow("OrdenTrabajo", 400, 320, 610, 190);
		ventanaOrdenTrabajo.setText("Asignar Orden de Trabajo.");
		ventanaOrdenTrabajo.setModal(true);
		ventanaOrdenTrabajo.attachURL("/MidasWeb/cotizacion/mostrarAsignarOrdenTrabajo.do?idToCotizacion="+idToCotizacion + "&idToSolicitud=" + idToSolicitud);
		ventanaOrdenTrabajo.attachEvent("onClose", cerrarVentanaOT);
	}
}
function mostrarResumen(idToSolicitud){
	if(dhxWins != null) {
		dhxWins.unload();
	}	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	
	var ventanaResumen = dhxWins.createWindow("Resumen", 400, 320, 610, 380);
	ventanaResumen.setText("Resumen General");
	ventanaResumen.center();
	ventanaResumen.setModal(true);
	ventanaResumen.attachURL("/MidasWeb/solicitud/mostrarResumen.do?id="+ idToSolicitud);
	//ventanaResumen.attachEvent("onClose", cerrarVentanaOT);
}
function asignarOrdenTrabajo(fobj){
	new Ajax.Request('/MidasWeb/cotizacion/asignarOrdenTrabajo.do', {
		method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			if(fobj.idToSolicitud == null || fobj.idToSolicitud == undefined || fobj.idToSolicitud.value == "undefined") {
				procesarRespuestaXml(transport.responseXML, "cerrarVentanaOT();");
			} else {
				procesarRespuestaXml(transport.responseXML, "cerrarVentanaSOL();");
			}
		}
		//procesarRespuestaOT(transport.responseXML);}
	});
}

function instanciarContenedorVentanas(){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
}

function mostrarAgregarTarifa(idToRiesgo, idConcepto, version){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
	}
	
	if (ventanaTarifa==null || parent.dhxWins.window("Tarifa").isHidden()){
		ventanaTarifa = dhxWins.createWindow("Tarifa", 400, 190, 610, 310);
		ventanaTarifa.center();
		ventanaTarifa.setText("Agregar Tarifa.");
		ventanaTarifa.setModal(true);
		ventanaTarifa.attachURL("/MidasWeb/tarifa/configuracion/mostrarAgregar.do?idToRiesgo="+idToRiesgo+"&idConcepto="+idConcepto+"&version="+version);
		ventanaTarifa.button("close").disable();
		
	}
}


function mostrarAgregarParticipacionFacultativa(idTdContratoFacultativo, id){
	var d=new Date();
	id = null;
	divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
 	 if (divCorredorBoton.style.display == "none"){
      	if (dhxWins==null){
	    	dhxWins = new dhtmlXWindows();
	    }
  	   if (ventanaParticipacionFacultativo==null || parent.dhxWins.window("participacionFacultativa") == null ||parent.dhxWins.window("participacionFacultativa").isHidden()){
  		 ventanaParticipacionFacultativo = dhxWins.createWindow("participacionFacultativa", 400, 190, 910, 310);
  		ventanaParticipacionFacultativo.center();
  		ventanaParticipacionFacultativo.setText("Agrega Participacion");
  		ventanaParticipacionFacultativo.setModal(true);
  		ventanaParticipacionFacultativo.attachURL("/MidasWeb/contratofacultativo/participacionFacultativa/mostrarAgregar.do?id="+id+"&idTdContratoFacultativo="+idTdContratoFacultativo)+"&fecha="+ d.getTime();
  		ventanaParticipacionFacultativo.button("close").disable();
		//ventanaParticipacionCorredor.attachEvent("onClose", closeAgregarParticipacionFacultativa);
	  }
 	}else{
 		if (dhxWins==null){
	    	dhxWins = new dhtmlXWindows();
	    }
  	   if (ventanaParticipacionCorredor==null || parent.dhxWins.window("participacionFacultativaCorredor") == null || parent.dhxWins.window("participacionFacultativaCorredor").isHidden()){
		  ventanaParticipacionCorredor = dhxWins.createWindow("participacionFacultativaCorredor", 400, 190, 710, 310);
		  ventanaParticipacionCorredor.center();
	      ventanaParticipacionCorredor.setText("Agrega Participacion Corredor");
		  ventanaParticipacionCorredor.setModal(true);
		  ventanaParticipacionCorredor.attachURL("/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarAgregar.do?id="+id+"&fecha="+ d.getTime());
		  ventanaParticipacionCorredor.button("close").disable();
		//ventanaParticipacionCorredor.attachEvent("onClose", closeAgregarParticipacionFacultativa);
	  }
    }  
}

function mostrarModificarParticipacionFacultativa(idTmContratoFacultativo,idTdContratoFacultativo, id){
	divCorredorBoton = document.getElementById("botonMostrarFacultativoCorredor");
 	 if (divCorredorBoton.style.display == "none"){
      	if (dhxWins==null){
	    	dhxWins = new dhtmlXWindows();
	    }

      	if (ventanaParticipacionFacultativo==null || parent.dhxWins.window("participacionFacultativa") == null || parent.dhxWins.window("participacionFacultativa").isHidden()){
      		ventanaParticipacionFacultativo = dhxWins.createWindow("participacionFacultativa", 400, 190, 910, 310);
      		ventanaParticipacionFacultativo.center();
      		ventanaParticipacionFacultativo.setText("Modificar Participacion");
      		ventanaParticipacionFacultativo.setModal(true);  
      		ventanaParticipacionFacultativo.attachURL("/MidasWeb/contratofacultativo/participacionFacultativa/mostrarModificar.do?id="+id+"&fecha="+(new Date()));
      		ventanaParticipacionFacultativo.button("close").disable();
      		//ventanaParticipacionCorredor.attachEvent("onClose", closeAgregarParticipacionFacultativa);
      	}
 	}else{
 		if (dhxWins==null){
	    	dhxWins = new dhtmlXWindows();
	    }
 	   if (ventanaParticipacionCorredor==null || parent.dhxWins.window("participacionFacultativaCorredor").isHidden()){
 		   ventanaParticipacionCorredor = dhxWins.createWindow("participacionFacultativaCorredor", 400, 190, 710, 310);
		  ventanaParticipacionCorredor.center();
	      ventanaParticipacionCorredor.setText("Modificar Participacion Corredor");
		  ventanaParticipacionCorredor.setModal(true);
		  ventanaParticipacionCorredor.attachURL("/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarModificar.do?id="+id);
		  ventanaParticipacionCorredor.button("close").disable();
		//ventanaParticipacionCorredor.attachEvent("onClose", closeAgregarParticipacionFacultativa);
	  }
    }  
}

function agregarParticipacionFacultativa(form,idTmContratoFacultativo, idTdContratoFacultativo){
	if ($('tipoParticipante').value != '' && $('participante').value != '' && $('cuentaBancoPesos').value != '' && $('cuentaBancoDolares').value != '' && $('porcentajeParticipacion').value != ''){
		if(validarPorcentaje($('porcentajeParticipacion').value)){ 
			closeAgregarParticipacionFacultativa();
			sendRequestFromAccordion(form,'/MidasWeb/contratofacultativo/participacionFacultativa/agregar.do?id='+idTdContratoFacultativo, 'gridboxFacultativo',
					'validarDetalleParticipacionFacultativa(),mostrarGridParticipacionFacultativo('+idTdContratoFacultativo+'),recargaCoberturasFacultativaTree('+idTmContratoFacultativo+','+ idTdContratoFacultativo+ ')');
		}
	}else{
		alert("Los datos marcados con * son requeridos");
	}
}


function modificarParticipacionFacultativa(form,idTmContratoFacultativo, idTdContratoFacultativo, idTdParticipacionFacultativo){
	if(validarPorcentaje($('porcentajeParticipacion').value)){ 
		closeAgregarParticipacionFacultativa();
		sendRequestFromAccordion(form,'/MidasWeb/contratofacultativo/participacionfacultativa/modificar.do?id='+idTdParticipacionFacultativo, 
				'gridboxFacultativo',
				'validarDetalleParticipacionFacultativa(),mostrarGridParticipacionFacultativo('+idTdContratoFacultativo+'),recargaCoberturasFacultativaTree('+idTmContratoFacultativo+','+ idTdContratoFacultativo+ ')');
	}
}

function agregarParticipacionFacultativaCorredor(form,idTmContratoFacultativo,idTdContratoFacultativo,id){
	if(parseFloat(form.porcentajeParticipacion.value) > parseFloat(form.totalReasegurador.value)){ 
	 alert("El porcentaje se sobrepasa");
    }else{	   
  	 if(validarPorcentaje($('porcentajeParticipacion').value)){
		if(form.idParticipante.value != ''){
  		 closeAgregarParticipacionFacultativaCorredor();
		 sendRequestFromAccordion(form,'/MidasWeb/contratofacultativo/participacionFacultativaCorredor/agregar.do?id='+id, 'gridboxFacultativoCorredor','mostrarGridParticipacionFacultativoCorredor('+id+'),recargaCoberturasFacultativaTree('+idTmContratoFacultativo+','+idTdContratoFacultativo+')');
		}else{
			alert("Selecciona reasegurador");
		}
 	  }
   }
}


function modificarParticipacionFacultativaCorredor(form,idTmContratoFacultativo,idTdContratoFacultativo,id){
var sumaPorcentaje =   parseFloat(form.porcentajeParticipacion.value);
var sumaPorcentajeTotal = parseFloat(form.totalReasegurador.value) + parseFloat(form.valorOriginalPorcentaje.value); 
if(sumaPorcentaje > sumaPorcentajeTotal){ 
		 alert("El porcentaje se sobrepasa");
	   }else{
    	   if(validarPorcentaje($('porcentajeParticipacion').value)){ 
		   closeAgregarParticipacionFacultativaCorredor();
		   sendRequestFromAccordion(form,'/MidasWeb/contratofacultativo/participacionFacultativaCorredor/modificar.do?id='+id, 'gridboxFacultativoCorredor','mostrarGridParticipacionFacultativoCorredor('+id+'),recargaCoberturasFacultativaTree('+idTmContratoFacultativo+','+idTdContratoFacultativo+')');
	      }
	   } 	
}


function closeAgregarParticipacionFacultativaCorredor(){
 	 parent.dhxWins.window("participacionFacultativaCorredor").setModal(false);
	 parent.dhxWins.window("participacionFacultativaCorredor").hide();
	 ventanaParticipacionFacultativo = null;  
     contVentanas=0;
}

function closeAgregarParticipacionFacultativa(){
 	 parent.dhxWins.window("participacionFacultativa").setModal(false);
	 parent.dhxWins.window("participacionFacultativa").hide();
	 ventanaParticipacionCorredor = null;  
     contVentanas=0;
}



function mostrarBorrarTarifa(idToRiesgo, idConcepto, idBase1, idBase2, idBase3, idBase4, version, valor){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	if (contVentanas==0){
		ventanaTarifa = dhxWins.createWindow("Tarifa", 400, 320, 610, 310);
		ventanaTarifa.setText("Borrar Tarifa.");
		ventanaTarifa.setModal(true);
		ventanaTarifa.attachURL("/MidasWeb/tarifa/configuracion/mostrarBorrar.do?idToRiesgo="+idToRiesgo+"&idConcepto="+idConcepto+"&idBase1="+idBase1+"&idBase2="+idBase2+"&idBase3="+idBase3+"&idBase4="+idBase4+"&version="+version+"&valor="+valor);
		ventanaTarifa.button("close").disable();
		ventanaTarifa.attachEvent("onClose", cerrarVentanaTarifa);
	}
}

function mostrarModificarTarifa(idToRiesgo, idConcepto, idBase1, idBase2, idBase3, idBase4, version, valor){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	if (contVentanas==0){
		ventanaTarifa = dhxWins.createWindow("Tarifa", 400, 320, 610, 310);
		ventanaTarifa.setText("Modificar Tarifa.");
		ventanaTarifa.setModal(true);
		ventanaTarifa.attachURL("/MidasWeb/tarifa/configuracion/mostrarModificar.do?idToRiesgo="+idToRiesgo+"&idConcepto="+idConcepto+"&idBase1="+idBase1+"&idBase2="+idBase2+"&idBase3="+idBase3+"&idBase4="+idBase4+"&version="+version+"&valor="+valor);
		ventanaTarifa.button("close").disable();
		//ventanaTarifa.attachEvent("onClose", cerrarVentanaTarifa);
	}
}

function agregarTarifa(fobj){
	if (!validarTarifa()) {
	new Ajax.Request('/MidasWeb/tarifa/configuracion/agregar.do', {
		method : "post",
		asynchronous : false,
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML,'cerrarVentanaTarifa();');
		} // End of onSuccess
		
	});
	}
	else {
		mostrarVentanaMensaje('10', 'Algunos datos no se han capturado.');
	}
}
function validarTarifa() {
	var errores = false;
	var base1 = document.getElementsByName('idBase1')[0];
	var base2 = document.getElementsByName('idBase2')[0];
	var base3 = document.getElementsByName('idBase3')[0];
	var base4 = document.getElementsByName('idBase4')[0];
	var valor = document.getElementsByName('valor')[0];
	
	if ($F(base1) == "") {
		errores = true;
	}
	if ($F(base2) == "") {
		errores = true;
	}
	if ($F(base3) == "") {
		errores = true;
	}
	if ($F(base4) == "") {
		errores = true;
	}
	if ($F(valor) == "") {
		errores = true;
	}
	
	return errores
}

function validarTarifa2() {
	var errores = false;
	var valor = document.getElementsByName('valor')[0];
	if ($F(valor) == "") {
		errores = true;
	}
	return errores;
}

function borraTarifa(fobj){	
	if (confirm('\u00bfRealmente deseas borrar el registro seleccionado?')){
		new Ajax.Request('/MidasWeb/tarifa/configuracion/borrar.do', {
			method : "post",
			asynchronous : false,
			parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
			onSuccess : function(transport) {
				procesarRespuestaXml(transport.responseXML,'cerrarVentanaTarifa();');
			} // End of onSuccess
			
		});
	}	
}

function modificaTarifa(fobj){
	//sendRequest(fobj,'/MidasWeb/tarifa/configuracion/modificar.do','contenido',null);
	if (!validarTarifa2()) {
	new Ajax.Request('/MidasWeb/tarifa/configuracion/modificar.do', {
		method : "post",
		asynchronous : false,
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML,'cerrarVentanaTarifa();');
		} // End of onSuccess
	});
	}
	else {
		mostrarVentanaMensaje('10', 'Algunos datos no se han capturado.');
	}
}

function cerrarVentanaTarifa(){
	contVentanas=0;
	parent.dhxWins.window("Tarifa").setModal(false);
	parent.dhxWins.window("Tarifa").hide();
	delay(500);
	new Ajax.Request("/MidasWeb/tarifa/configuracion/listar.do", {
		method : "post",
		encoding : "UTF-8",
		parameters : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
			parent.document.getElementById('detalleTarifa').innerHTML = transport.responseText;
		} // End of onSuccess
		
	});
	//parent.dhxWins.window("Tarifa").close();
	ventanaTarifa=null;
}

function delay(millis) {
	var date = new Date();
	var curDate = null;
	do { curDate = new Date(); } 
		while(curDate-date < millis);
} 

/** FIN de Funciones para CRUD de Tarifas **/

/** Inicio de Funciones para Orden de trabajo **/
var ventanaOrdenTrabajo=null;


function mostrarCancelarOrdenTrabajo(idToCotizacion){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	if (ventanaOrdenTrabajo==null || parent.dhxWins.window("OrdenTrabajo").isHidden()){
		ventanaOrdenTrabajo = dhxWins.createWindow("OrdenTrabajo", 400, 320, 610, 260);
		ventanaOrdenTrabajo.setText("Cancelar/Rechazar Orden de Trabajo.");
		ventanaOrdenTrabajo.setModal(true);
		ventanaOrdenTrabajo.attachURL("/MidasWeb/cotizacion/mostrarCancelarOrdenTrabajo.do?idToCotizacion="+idToCotizacion);
		ventanaOrdenTrabajo.button("close").disable();
		ventanaOrdenTrabajo.attachEvent("onClose", cerrarVentanaOT);
	}
}
function cancelarCotizacion(fobj){
	var idMotivo = document.getElementById("idMotivoRechazo");
	if (idMotivo != null){
		idMotivo = idMotivo.value;
		if (idMotivo == "")
			alert("Seleccion el motivo del rechazo o cancelacion");
		else{
			new Ajax.Request('/MidasWeb/cotizacion/cotizacion/cancelarCotizacion.do', {
				method : "post",
				asynchronous : false,
				parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
				onSuccess : function(transport) {
					procesarRespuestaXml(transport.responseXML, 'cerrarVentanaCot()');}
			});
		}
	}
}

function procesarRespuestaCancelarCOT(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if (value==2){
		alert(text);
	}
	else{
		alert(text);
		cerrarVentanaCot();
	}
}
function cancelarOrdenTrabajo(fobj){
	var idMotivo = document.getElementById("idMotivoRechazo");
	if (idMotivo != null){
		idMotivo = idMotivo.value;
		if (idMotivo == "")
			alert("Seleccion el motivo del rechazo o cancelacion");
		else{
			new Ajax.Request('/MidasWeb/cotizacion/cancelarOrdenTrabajo.do', {
				method : "post",
				asynchronous : false,
				parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
				onSuccess : function(transport) {
					procesarRespuestaCancelarOT(transport.responseXML);}
			});
		}
	}
}

function procesarRespuestaCancelarOT(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if (value==2){
		alert(text);
	}
	else{
		alert(text);
		parent.dhxWins.window("OrdenTrabajo").close();
	}
}

function actualizarListaOT(obj){
	if (obj.selectedIndex > 0) {
		sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajoFiltrado.do?claveEstatus='+obj[obj.selectedIndex].value, 'contenido', null);
	}
}

function actualizarListaCotizaciones(obj){
	if (obj.selectedIndex > 0) {
		sendRequest(null,'/MidasWeb/cotizacion/cotizacion/listarCotizacionesFiltrado.do?claveEstatus='+obj[obj.selectedIndex].value, 'contenido', null);
	}
}

function cerrarVentanaOT(){
	parent.dhxWins.window("OrdenTrabajo").setModal(false);
	parent.dhxWins.window("OrdenTrabajo").hide();
	ventanaOrdenTrabajo=null;
	delay(500);
	parent.sendRequest(document.cotizacionForm,"/MidasWeb/cotizacion/listarOrdenesTrabajo.do", "contenido",null);
}

function cerrarVentanaSOL(){
	parent.dhxWins.window("OrdenTrabajo").setModal(false);
	parent.dhxWins.window("OrdenTrabajo").hide();
	ventanaOrdenTrabajo=null;
	delay(500);
	parent.sendRequest(document.solicitudForm,"/MidasWeb/solicitud/listar.do", "contenido",null);
}

function ocultarVentanaOT(){
	parent.dhxWins.window("OrdenTrabajo").setModal(false);
	parent.dhxWins.window("OrdenTrabajo").hide();
}
/** Fin de Funciones para Orden de trabajo **/

/* Ventana mensajes pendientes */
var mensajesPendientesWindow;
var daniosProcessor;
var reaseguroProcessor;
var siniestrosProcessor;

function mostrarMensajesPendientesWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");

	mensajesPendientesWindow = dhxWins.createWindow("mensajesPendientesWindow", 34, 100, 640, 400);
	mensajesPendientesWindow.setText("Pendientes: ");
	mensajesPendientesWindow.button("minmax1").hide();

	var accordion = mensajesPendientesWindow.attachAccordion();
	accordion.addItem("0", "Modulo Da&#241;os");
	accordion.addItem("1", "Modulo Reaseguro");
	accordion.addItem("2", "Modulo Siniestros");
	accordion.setEffect(true);
	
	var danios = accordion.cells("0").attachGrid();
	danios.setHeader("Fecha, Mensaje");
	danios.setColumnIds("fecha,mensaje");
	danios.setInitWidths("80,*");
	danios.setColAlign("center,left");
	danios.setColSorting("date,str");
	danios.setColTypes("ro,link"); 
	danios.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	danios.setSkin("light");
	danios.enableLightMouseNavigation(true);
	danios.enableMultiline(true);
	danios.init();
	danios.load('/MidasWeb/sistema/gestionPendientes/listarPendientes.do?tipoLista=2', null, 'json');
	
 	var reaseguro = accordion.cells("1").attachGrid();
	reaseguro.setHeader("Fecha, Mensaje");
	reaseguro.setColumnIds("fecha,mensaje");
	reaseguro.setInitWidths("80,*");
	reaseguro.setColAlign("center,left");
	reaseguro.setColSorting("date,str");
	reaseguro.setColTypes("ro,link");
	reaseguro.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	reaseguro.setSkin("light");
	reaseguro.enableLightMouseNavigation(true);
	reaseguro.enableMultiline(true);
	reaseguro.init();
 
	var siniestros = accordion.cells("2").attachGrid();
	siniestros.setHeader("Fecha, Mensaje,Asegurado,No Reporte");
	siniestros.setColumnIds("fecha,mensaje,mensaje,mensaje");
	siniestros.setInitWidths("80,230,*,90");
	siniestros.setColAlign("center,left,left,left");
	siniestros.setColSorting("date,str,str,str");
	siniestros.setColTypes("ro,link,ro,ro");
	siniestros.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	siniestros.setSkin("light");
	siniestros.enableLightMouseNavigation(true);
	siniestros.enableMultiline(true);
	siniestros.init();
	siniestros.load('/MidasWeb/sistema/gestionPendientes/listarPendientes.do?tipoLista=1', null, 'json');
	
	mensajesPendientesWindow.show();
}

function mostrarAcercaDeWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var acercaDeWindow = dhxWins.createWindow("acercaDeWindow", 34, 100, 600, 345);
	acercaDeWindow.setText("Acerca de...");
	acercaDeWindow.button("minmax1").hide();
	acercaDeWindow.button("park").hide();
	acercaDeWindow.setModal(true);
	acercaDeWindow.center();
	acercaDeWindow.denyResize();
	acercaDeWindow.attachURL("/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do");
}



function mostrarAdjuntarArchivoSolicitudWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var acercaDeWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	acercaDeWindow.setText("Adjuntar archivos");
	acercaDeWindow.button("minmax1").hide();
	acercaDeWindow.button("park").hide();
	acercaDeWindow.setModal(true);
	acercaDeWindow.center();
	acercaDeWindow.denyResize();
	acercaDeWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        sendRequest(null, '/MidasWeb/solicitud/listarDocumentos.do', 'resultadosDocumentos', null);
        parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "0");
}

/**
 * Seccion de ventanas de mensaje (de error y de transaccion exitosa)
 */

function obtenerMensajeRespuesta(afterClose){
	new Ajax.Request('/MidasWeb/mensaje/verMensaje.do', {
		method : "post",
		asynchronous : false,
		//parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML, afterClose);
		} // End of onSuccess
		
	});
	
	//parent.dhxWins.window("Tarifa").close();
}


function procesarRespuestaXml(xmlDoc, afterClose){
 	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var mensaje = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	
	mostrarVentanaMensaje(tipoMensaje, mensaje, afterClose);

}

function procesarRespuesta(afterClose) {
	var mensaje = $('mensaje');
	var tipoMensaje = $('tipoMensaje');
	
	mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, afterClose);
	
}
function procesarRespuestaEmision(afterClose) {
	var mensaje = $('mensaje');
	var idToCotizacion = $("idToCotizacion").value;
	var idToPoliza = $("idToPoliza").value;
	var noEndoso = $("noEndoso").value;
	var fechaCreacionPoliza = $("fechaCreacionPoliza").value;
	var tipoMensaje = $('tipoMensaje');	
//	var notificaReaseguro = "notificaReaseguro("+idToCotizacion+","+idToPoliza+","+noEndoso+",'"+fechaCreacionPoliza+"');"
	if(tipoMensaje.value === "30"){
		mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, "listarEmisiones();"/*+notificaReaseguro*/);
	}else{
		if(noEndoso.value == '0' || noEndoso.value == 0){
			mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, "listarCotizaciones();");
		}else{
			mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, "listarCotizacionesEndoso();");
		}	
		
	}	
	
}
function notificaReaseguro(idToCotizacion,idToPoliza,noEndoso,fechaCreacionPoliza ){
	new Ajax.Request("/MidasWeb/notificarEmision.do", {
		method : "post",
		asynchronous : false,
		parameters : "idToCotizacion=" + idToCotizacion + "&idToPoliza="+ idToPoliza+ "&noEndoso="+noEndoso+"&fechaCreacionPoliza="+fechaCreacionPoliza,
		onSuccess : function(transport) {
			//Solo si se requiere mas adelante notificar si fue exitoso o no la llamada a reaseguro
		} // End of onSuccess
	});	
}
function mostrarVentanaMensaje(id, mensaje, afterClose) {
	
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;
	
	var html = "<div class=\"mensaje_encabezado\"></div>"
		html = html  + "<div class=\"mensaje_contenido\">"
		html = html  + "<div id=\"mensajeImg\">"
		if (id === "30"){
			html = html  + "<img src='/MidasWeb/img/b_ok.png'>";
		}else if (id === "20"){
			html = html  + "<img src='/MidasWeb/img/b_info.jpg'>";
			afterClose = null;
		}else if (id === "10"){
			html = html  + "<img src='/MidasWeb/img/b_no.jpg'>";
			afterClose = null;
		}
		html = html  + "</div>"	
		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\">"
		html = html  + mensaje;
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">"
		html = html  + "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" cerrarVentanaMensaje();\">Aceptar</a></div>";
		html = html  + "</div>"
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_pie\"></div>"
		
		if (parent.dhxWins==null){
			parent.dhxWins = new dhtmlXWindows();
		}
				
		if (parent.dhxWinsMsg==null){
			parent.dhxWinsMsg = new parent.dhtmlXWindows();
			parent.dhxWinsMsg.setSkin("midas_message");
		}
		
		mensajeAUsuario = parent.dhxWinsMsg.window("mensajeAUsuario");
		if (mensajeAUsuario!=null && parent.dhxWinsMsg.window("mensajeAUsuario").isHidden()) {
			parent.dhxWinsMsg.unload();
			parent.dhxWinsMsg = new parent.dhtmlXWindows();
			parent.dhxWinsMsg.setSkin("midas_message");
			mensajeAUsuario=null;
		}
		
		
		if (mensajeAUsuario==null){
			var browserName=navigator.appName; 
			
			if (browserName=="Microsoft Internet Explorer")
				mensajeAUsuario = parent.dhxWinsMsg.createWindow("mensajeAUsuario", 500, 340, 433, 190);
			else
				mensajeAUsuario = parent.dhxWinsMsg.createWindow("mensajeAUsuario", 500, 340, 420, 157);
			mensajeAUsuario.center();
						
			for (var a in parent.dhxWins.wins) {
				if (parent.dhxWins.wins[a].isModal()) {
					ventanaInvocadora = parent.dhxWins.wins[a];
					ventanaInvocadora.setModal(false);
					zIndexInvocadora = ventanaInvocadora.style.zIndex;
					ventanaInvocadora.style.zIndex = 0;
				}
			}
			
			mensajeAUsuario.setModal(true);
			mensajeAUsuario.attachHTMLString(html);
			mensajeAUsuario.attachEvent("onClose", function(win){
				mensajeAUsuario =null;
				
				if(ventanaInvocadora != null){
					if(jQuery('#ventanaDetalleDinamico #tipoMensaje').html()=="10")
						ventanaInvocadora.setModal(true);
					ventanaInvocadora.style.zIndex = zIndexInvocadora;
				}
				
				if (afterClose) {
					eval(afterClose);
				}
			});
		}
		
}

function cerrarVentanaMensaje() {
	parent.dhxWinsMsg.window("mensajeAUsuario").setModal(false);
	parent.dhxWinsMsg.window("mensajeAUsuario").hide();
	parent.dhxWinsMsg.window("mensajeAUsuario").close();
	parent.dhxWinsMsg.unload();
	parent.dhxWinsMsg = null;
	
}

/**
 * Fin de Seccion de ventanas de mensaje (de error y de transaccion exitosa)
 */

/**
 * Inicio ventana de agregar archivos anexos a productos
 */
var anexarArchivoWindow;
function mostrarAnexarArchivoProductoWindow(idToProducto) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.onUploadComplete = function(files) {
//Inicio.Funciona bien, pero no muestra mensaje de exito.
//        sendRequest(null, '/MidasWeb/catalogos/producto/listarDocumentosAnexos.do?', 'contenido_documentos', 'mostrarDocumentosProductoGrid()');
//        parent.dhxWins.window("adjuntarArchivoSolicitud").close();
//Fin.Funciona bien, pero no muestra mensaje de exito.
    	//Inicio. Muestra mensaje de exito al adjuntar un archivo PDF
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "mostrarDocumentosProductoGrid();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);
    	//Fin. Muestra mensaje de exito al adjuntar un archivo PDF
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "1");
}
/**
 * Fin ventana de agregar archivos anexos a productos
 */

 /**
 * Inicio ventana de agregar archivos anexos a tipo poliza
 */
function mostrarAnexarArchivoTipoPolizaWindow(idToTipoPoliza) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
  //Inicio.Funciona bien, pero no muestra mensaje de exito.
//    vault.onUploadComplete = function(files) {
//        sendRequest(null, '/MidasWeb/configuracion/tipopoliza/listarDocumentosAnexos.do?', 'contenido_documentos', 'mostrarDocumentosTipoPolizaGrid()');
//        parent.dhxWins.window("adjuntarArchivoSolicitud").close();
//    };
  //Fin.Funciona bien, pero no muestra mensaje de exito.

    //Inicio. Muestra mensaje de exito al adjuntar un archivo PDF
    vault.onUploadComplete = function(files) {
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "mostrarDocumentosTipoPolizaGrid();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);    	    	
    };
  //Fin. Muestra mensaje de exito al adjuntar un archivo PDF
    vault.create("vault");
    vault.setFormField("claveTipo", "2");
}
/**
 * Fin ventana de agregar archivos anexos a tipo poliza
 */

/**
 * Inicio ventana de agregar archivos anexos a CG/NivelSeccion
 */
function mostrarAnexarArchivoCgWindow(idToSeccion) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    
    vault.onAddFile = function(fileName) {
    	var ext = this.getFileExtension(fileName);
    	if (ext != "pdf" && ext != "PDF"){
    		mostrarVentanaMensaje('10','¡ Solo es posible cargar archivos PDF (.pdf) !, Intente de nuevo.',null);
    		return false;
    	} else 
    		return true;
    };
    
    vault.onUploadComplete = function(files) {
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "mostrarDocumentosCgGrid();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);    	    	
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "23");
}
/**
 * Fin ventana de agregar archivos anexos a CG/NivelSeccion
 */

/**
 * Inicio ventana de agregar archivos anexos a cobertura
 */
//var anexarArchivoWindow;
function mostrarAnexarArchivoCoberturaWindow(idToCobertura) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    //Inicio.Funciona bien, pero no muestra mensaje de exito.
//    vault.onUploadComplete = function(files) {
//        sendRequest(null, '/MidasWeb/catalogos/cobertura/listarDocumentosAnexos.do?', 'contenido_documentos', 'mostrarDocumentosCoberturaGrid()');
//        parent.dhxWins.window("adjuntarArchivoSolicitud").close();
//    };
    //Fin.Funciona bien, pero no muestra mensaje de exito.
    
    //Inicio. Muestra mensaje de exito al adjuntar un archivo PDF
    vault.onUploadComplete = function(files) {
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "mostrarDocumentosCoberturaGrid();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);    	    	
    };
  //Fin. Muestra mensaje de exito al adjuntar un archivo PDF
    vault.create("vault");
    vault.setFormField("claveTipo", "3");//3 = Cobertura
}
/**
 * Fin ventana de agregar archivos anexos a cobertura
 */

/**
 * Inicio ventana de agregar archivos anexos a SLIP
 */
function mostrarAdjuntarArchivoSlipWindow(idToSlip) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var acercaDeWindow = dhxWins.createWindow("adjuntarArchivoSlip", 34, 100, 440, 265);
	acercaDeWindow.setText("Adjuntar archivos");
	acercaDeWindow.button("minmax1").hide();
	acercaDeWindow.button("park").hide();
	acercaDeWindow.setModal(true);
	acercaDeWindow.center();
	acercaDeWindow.denyResize();
	acercaDeWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        sendRequest(null, '/MidasWeb/contratofacultativo/slip/listarDocumentos.do?idToSlip='+idToSlip, 'resultados', null);
        parent.dhxWins.window("adjuntarArchivoSlip").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "5"); //5 = documentos para slip
}
/**
 * Fin ventana de agregar archivos anexos a SLIP
 */
 
function mostrarAdjuntarDocumentoDigitalComplementarioWindow(idToCotizacion) {
	if(dhxWins != null) 
		dhxWins.unload();
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoDigitalComplementario", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar archivos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        sendRequest(null, '/MidasWeb/cotizacion/listarDocumentosDigitalesComplementarios.do?id='+idToCotizacion,'contenido_documentosDigitalesComplementarios',null);
        parent.dhxWins.window("adjuntarDocumentoDigitalComplementario").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "4");
    vault.setFormField("idToCotizacion", idToCotizacion);
}

function mostrarAdjuntarDocumentoAnexoInspeccionIncisoCotizacionWindow(idToInspeccionIncisoCotizacion) {
	if(dhxWins != null) 
		dhxWins.unload();
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoAnexoInspeccionIncisoCotizacion", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documentos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        sendRequest(null, '/MidasWeb/cotizacion/inspeccion/listarDocumentosAnexosInspeccionIncisoCotizacion.do?id='+idToInspeccionIncisoCotizacion,'tablaDocumentosAnexosInspeccion',null);
        parent.dhxWins.window("adjuntarDocumentoAnexoInspeccionIncisoCotizacion").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "6");
    vault.setFormField("idToInspeccionIncisoCotizacion", idToInspeccionIncisoCotizacion);
}

/**
 * Ventana para adjuntar archivo de registro CNSF a la cobertura
 */
function mostrarAdjuntarDocumentoCNSFCoberturaWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoCNSFCoberturaWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento CNSF");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('resultados_documento');
    		    	var html; 
    		    	html = "<table id='desplegarDetalle'><tr><td><div class='etiqueta'>Archivo:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoRegistroCNSF' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarAdjuntarDocumentoCNSFCoberturaWindow()' >Actualizar documento CNSF</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("adjuntarDocumentoCNSFCoberturaWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "7");
}

/**
 * Ventana para adjuntar archivo de registro CNSF al producto.
 */
function mostrarAdjuntarArchivoRegistroCNSFProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("AdjuntarArchivoRegistroCNSFProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento registro CNSF");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoRegistroCNSF');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo Registro CNSF:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoRegistroCNSF' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarAdjuntarArchivoRegistroCNSFProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("AdjuntarArchivoRegistroCNSFProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "8");
}

function mostrarAdjuntarArchivoCaratulaPolizaProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("AdjuntarArchivoCaratulaPolizaProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar car&aacute;tula p&oacute;liza");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoCaratulaPoliza');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo Car&aacute;tula Poliza:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoCaratulaPoliza' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarAdjuntarArchivoCaratulaPolizaProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("AdjuntarArchivoCaratulaPolizaProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "9");
}

function mostrarArchivoCondicionesProductoProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("ArchivoCondicionesProductoProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento de condiciones del producto");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoCondicionesProducto');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo Condiciones del producto:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoCondicionesProducto' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarArchivoCondicionesProductoProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("ArchivoCondicionesProductoProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "10");
}

function mostrarArchivoNotaTecnicaProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("ArchivoNotaTecnicaProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento nota t&eacute;cnica");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do","/MidasWeb/sistema/vault/getInfoHandler.do","/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoNotaTecnica');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo Nota Tecnica:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoNotaTecnica' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarArchivoNotaTecnicaProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("ArchivoNotaTecnicaProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "11");
}

function mostrarArchivoAnalisisCongruenciaProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("ArchivoAnalisisCongruenciaProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento de an&aacute;lisis de congruencia");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do", "/MidasWeb/sistema/vault/getInfoHandler.do", "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoAnalisisCongruencia');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo de An&aacute;lisis Congruencia:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoAnalisisCongruencia' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarArchivoAnalisisCongruenciaProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("ArchivoAnalisisCongruenciaProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "12");
}

function mostrarArchivoDictamenJuridicoProductoWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("ArchivoDictamenJuridicoProductoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento dict&aacute;men jur&iacute;dico");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do", "/MidasWeb/sistema/vault/getInfoHandler.do", "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById('ArchivoDictamenJuridico');
    		    	var html; 
    		    	html = "<table><tr><td><div class='etiqueta'>Archivo Dict&aacute;men Juridico:</div></td><td>" +
    		    			"<input class='cajaTexto'type='text' disabled value='" +nombreArchivo+"'></td><td>";
    		    	html = html+"<input type='hidden' name='idControlArchivoDictamenJuridico' value='"+idToControlArchivo+"'/></td>";
    		    	html = html+"<td><a href='javascript:void(0);' onclick='mostrarArchivoDictamenJuridicoProductoWindow()' >Actualizar documento</a></td>";
    		    	div.innerHTML = html;
    			}
    		} // End of onSuccess
    		
    	});
        parent.dhxWins.window("ArchivoDictamenJuridicoProductoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "13");
}


function mostrarArchivoDocumentoSiniestroWindow() {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("ArchivoDocumentoSiniestroWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento escaneado");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do","/MidasWeb/sistema/vault/getInfoHandler.do","/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
        parent.dhxWins.window("ArchivoDocumentoSiniestroWindow").close();
        var funcionRedirecciona = "listarReportesSiniestro();";
        mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "14");
    //Se agregan los datos capturados por el usuario
    vault.setFormField("nombreDocumento", document.forms[0].nombreDocumento.value);
    //Se checa si el select esta deshabilitado
    var elemento=document.getElementById("idTipoDocumento");
    if(elemento.getAttribute("disabled")=="disabled"){
    	 vault.setFormField("idTipoDocumento", document.getElementsByName("idTipoDocumento")[0].value);
    }else{
    	vault.setFormField("idTipoDocumento", document.forms[0].idTipoDocumento.value);
    }
    vault.setFormField("idReporteSiniestro", document.forms[0].idReporteSiniestro.value);
    vault.setFormField("idDocumentoSiniestro", document.forms[0].idDocumentoSiniestro.value);
}

/**
 * Ventana para capturar datos de la relacion entre riesgo y cobertura.
 */
var ventanaRiesgoCobertura=null;
function mostrarEditarRiesgoCoberturaWindow(idToRiesgo,idToCobertura,idToSeccion) {
	if(dhxWins == null)
		dhxWins = new dhtmlXWindows();
	
		ventanaRiesgoCobertura = dhxWins.createWindow("ventanaRiesgoCobertura", 400, 360, 610, 310);
		ventanaRiesgoCobertura.center();
		ventanaRiesgoCobertura.setText("Editar Asociaci&oacute;n Riesgo-Cobertura.");
		ventanaRiesgoCobertura.setModal(true);
		ventanaRiesgoCobertura.attachURL("/MidasWeb/configuracion/cobertura/mostrarEditarRelacionRiesgoCobertura.do?idToRiesgo="+idToRiesgo+"&idToCobertura="+idToCobertura+"&idToSeccion="+idToSeccion);
}

function guardarRiesgoCobertura(fobj){
	sendRequest(fobj, '/MidasWeb/configuracion/cobertura/editarRelacionRiesgoCobertura.do','ventana',null);
	/*
	new Ajax.Request('/MidasWeb/configuracion/cobertura/editarRelacionRiesgoCobertura.do', {
		method : "post", asynchronous : false, parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			procesarRespuestaXml(transport.responseXML,null);
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var tipoMensaje = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			if (tipoMensaje.value != 2)
				if (parent.dhxWins.window("ventanaRiesgoCobertura") != null && parent.dhxWins.window("ventanaRiesgoCobertura") != undefined)
				parent.dhxWins.window("ventanaRiesgoCobertura").close();
		} // End of onSuccess
		
		
	});
	*/
}

/**
 * Ventana para egresos
 */
var ventanaRI1 = null;
function registrarEgresos(){
	
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	
		if (ventanaOrdenTrabajo==null || dhxWins==null){
			ventanaOrdenTrabajo = dhxWins.createWindow("EgresoCancel", 420, 360, 650, 290);
			ventanaOrdenTrabajo.setText("Cancelar/Egreso.");
			ventanaOrdenTrabajo.setModal(true);
			ventanaOrdenTrabajo.attachURL("/MidasWeb/reaseguro/egresos/egresoCancel.do");
			ventanaOrdenTrabajo.attachEvent("onClose", cierraVentanaRegistrarEgresosReaseguro);
		}
	
}	

function cierraVentanaRegistrarEgresosReaseguro(){
	parent.dhxWins.window("EgresoCancel").setModal(false);
	parent.dhxWins.window("EgresoCancel").hide();
	parent.ventanaOrdenTrabajo =null;
	parent.dhxWins.unload();
	parent.dhxWins = null;
	//delay(500);
	//sendRequest(null,"/MidasWeb/reaseguro/egresos/mostrarEgresoCancel.do","contenido","registrarIngresosGrid()");
}


/**
 * Ventana para registrar ingresos
 */
var ventanaRI = null;
function registrarIngresos(){
	if(dhxWins == null)
		dhxWins = new dhtmlXWindows();
	ventanaRI = dhxWins.createWindow("Ingreso", 420, 360, 410, 340);
	ventanaRI.center();
	dhxWins.attachEvent("onContentLoaded", function(win){
		for(var i = 1; i <= 4; i++)
			ventanaRI._frame.contentWindow.document.getElementById('agregarRI' + i + '').style.display = '';
		ventanaRI._frame.contentWindow.document.getElementById('agregarRIb').style.display = '';
		ventanaRI._frame.contentWindow.document.getElementById('agregarRIb2').style.display = '';
		
    });
	ventanaRI.setText("Registrar un Ingreso");
	ventanaRI.setModal(true);
	ventanaRI.attachURL("/MidasWeb/contratos/ingresos/registrarIngresosReaseguro.do?refreshFrame=" + new Date());
	dhxWins.attachEvent("onClose", function(win){
		cierraVentanaRegistrarIngresoReaseguro();
	});
	
}

function cierraVentanaRegistrarIngresoReaseguro(){
	parent.dhxWins.window("Ingreso").setModal(false);
	parent.dhxWins.window("Ingreso").hide();
	parent.ventanaRI =null;
	parent.dhxWins.unload();
	parent.dhxWins = null;
	delay(500);
	sendRequest(null,"/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do","contenido","registrarIngresosGrid()");
}

var ventanaMRI = null;
function modificarRegistrarIngresos(id, monto, moneda, referencia, numCuenta, fecha){
	if(dhxWins == null)
		dhxWins = new dhtmlXWindows();
	var ventanaMRI = dhxWins.createWindow("ModificarIngreso", 420, 360, 410, 340);
	ventanaMRI.center();
	dhxWins.attachEvent("onContentLoaded", function(win){
		ventanaMRI._frame.contentWindow.document.getElementById('idIngresoReaseguro').value = id;
		for(var i = 1; i <= 4; i++)
			ventanaMRI._frame.contentWindow.document.getElementById('modificarRI' + i + '').style.display = '';
		ventanaMRI._frame.contentWindow.document.getElementById('montoIngresoM').value = formatCurrency4Decs(monto);
		
		ventanaMRI._frame.contentWindow.document.getElementById('referencia').value = referencia;
		ventanaMRI._frame.contentWindow.document.getElementById('numeroCuenta').value = numCuenta;
		ventanaMRI._frame.contentWindow.document.getElementById('modificarRIb').style.display = '';
		ventanaMRI._frame.contentWindow.document.getElementById('modificarRIb2').style.display = '';
		ventanaMRI._frame.contentWindow.document.getElementById('fechaIngreso').value = fecha;
		
	});
	ventanaMRI.setText("Modificar Ingreso");
	ventanaMRI.setModal(true);
	ventanaMRI.attachURL("/MidasWeb/contratos/ingresos/registrarIngresosReaseguro.do?idIngresoReaseguro=" + id +"&refreshFrame=" + new Date());
	dhxWins.attachEvent("onClose", function(win){
		cierraVentanaModificarIngresoReaseguro();
	});
}
function cierraVentanaModificarIngresoReaseguro(){
	parent.dhxWins.window("ModificarIngreso").setModal(false);
	parent.dhxWins.window("ModificarIngreso").hide();
	parent.ventanaMRI =null;
	parent.dhxWins.unload();
	parent.dhxWins = null;
	delay(500);
	sendRequest(null,"/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do","contenido","registrarIngresosGrid()");
}

function guardarPrevencionOperacionesArchivo(claveTipoDocumento) {
	var idToControl; //Este es la referencia al objeto de la forma.
	var idToCotizacion = $(complementarCotizacionForm).idToCotizacion.value;
	var idDiv; //El id del elemento en donde se desea agregar el html generado.
	var nombreDelDocumento;
	var nombreDelInput;

	switch (claveTipoDocumento) {
		case 1: //Identificacion del Asegurado
			idToControl =  $(complementarCotizacionForm).identificacionAseguradoIdArchivo;
			idDiv = "identificacionAsegurado";
			nombreDelDocumentoHtml = "Identificacion";
			nombreDelInput = "identificacionAseguradoArchivo";
			break;
		case 2: //RFC del Asegurado
			idToControl =  $(complementarCotizacionForm).rfcAseguradoIdArchivo;
			idDiv = "rfcAsegurado";
			nombreDelDocumentoHtml = "RFC";
			nombreDelInput = "rfcAseguradoArchivo";
			break;
		case 3: //Comprobante domicilio del asegurado
			idToControl =  $(complementarCotizacionForm).comprobanteDomicilioAseguradoIdArchivo;
			idDiv = "comprobanteDomicilioAsegurado";
			nombreDelDocumentoHtml = "Comprobante domicilio";
			nombreDelInput = "comprobanteDomicilioAseguradoArchivo";
			break;
		case 4: //Acta constitutiva de la empresa 
			idToControl =  $(complementarCotizacionForm).actaConstitutivaEmpresaIdArchivo;
			idDiv = "actaConstitutiva";
			nombreDelDocumentoHtml = "Copia de acta constitutiva";
			nombreDelInput = "actaConstitutivaEmpresaArchivo";
			break;
		case 5: //Poder del representante legal
			idToControl =  $(complementarCotizacionForm).poderRepresentanteEmpresaIdArchivo;
			idDiv = "poderRepresentante";
			nombreDelDocumentoHtml = "Poder del representante legal";
			nombreDelInput = "poderRepresentanteEmpresaArchivo";
			break;
		case 6: //Identificacion del representante legal
			idToControl =  $(complementarCotizacionForm).identificacionRepresentanteEmpresaIdArchivo;
			idDiv = "identificacionRepresentante";
			nombreDelDocumentoHtml = "Identificacion del representante legal";
			nombreDelInput = "identificacionRepresentanteEmpresaArchivo";
			break;
		case 7: //Comprobante domicilio empresa
			idToControl =  $(complementarCotizacionForm).comprobanteDomicilioEmpresaIdArchivo;
			idDiv = "comprobanteDomicilioEmpresa";
			nombreDelDocumentoHtml = "Comprobante de domicilio";
			nombreDelInput = "comprobanteDomicilioEmpresaArchivo";
			break;
		case 8: //Cedula fiscal de la empresa
			idToControl =  $(complementarCotizacionForm).cedulaFiscalEmpresaIdArchivo;
			idDiv = "cedulaFiscal";
			nombreDelDocumentoHtml = "Cedula fiscal";
			nombreDelInput = "cedulaFiscalEmpresaArchivo";
			break;
	}
	
	mostrarVentanaAdjuntarArchivoPrevencion(idToCotizacion, idToControl, claveTipoDocumento,
			idDiv, nombreDelDocumentoHtml, nombreDelInput);
}
function mostrarVentanaAdjuntarArchivoPrevencion(idToCotizacion, idToControlInput, claveTipoDocumento,
		idDiv, nombreDelDocumentoHtml, nombreDelInput) {
	var claveTipo = "15";
	var idToControl = idToControlInput.value;
	var nombreVentana = "adjuntarArchivoPrevencion";
	
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow(nombreVentana, 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento " + nombreDelDocumentoHtml);
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				var div = document.getElementById(idDiv);
    		    	var html; 
    		    	html = "&nbsp;" + nombreDelDocumentoHtml + "<input class=\"cajaTexto2\" type=\"text\" readonly=\"true\" value=\"" + 
    		    		nombreArchivo + "\" size=\"30\" name=\"" + nombreDelInput + 
    		    		"\"/> <a onclick=\"guardarPrevencionOperacionesArchivo(" + 
    		    		claveTipoDocumento + 
    		    		")\" href=\"javascript: void(0);\"> Actualizar documento </a>";
    		    	div.innerHTML = html;
    		    	idToControlInput.value = idToControlArchivo; //Actualizar el valor del hidden
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window(nombreVentana).close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", claveTipo);
    vault.setFormField("idToCotizacion", idToCotizacion);
    vault.setFormField("idTipoDocumento", claveTipoDocumento);
    vault.setFormField("idToControlArchivo", idToControl);   
}

function mostrarCargaMasivaIncisosWindow(idToCotizacion, origen) {
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaIncisos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva de Incisos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			var nextFunction;
    				if(origen == 'ODT') {
    					nextFunction = 'creaArbolOrdenesDeTrabajo(' + idToCotizacion + '); mostrarVentanaMensajeInciso();';
    				} else {
    					nextFunction = 'creaArbolCotizacion(' + idToCotizacion + '); mostrarVentanaMensajeInciso();';
    				}
        			parent.sendRequest(parent.document.incisoCotizacionForm, "/MidasWeb/cotizacion/inciso/cargaMasiva.do?idToCotizacion=" + idToCotizacion + "&idToControlArchivo=" + idToControlArchivo + "&origen=" + origen, "contenido_resumenIncisos", nextFunction);
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaIncisos").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

function mostrarVentanaMensajeAclaraciones(afterClose) {
	mostrarVentanaMensaje($('tipoMensajeAclaraciones').value, $('mensajeAclaraciones').value, afterClose);
}

function menuAyuda(){
	sendRequest(null,'/MidasWeb/sistema/mensajePendiente/mostrarHelp.do',null,'abrirAyuda(transport.responseText)');
}

function abrirAyuda (ruta){
	window.open(ruta,'Ayuda','left=100,top=100,width=700,height=700');	
}

function copiarParticipacionesFacultativoCobertura(idTmContratoFacultativo, idTdContratoFacultativo) {
	var permitirCopiar = true;
	
	var cantidadParticipaciones = cantidadElementosGridFacultativo();
	if (cantidadParticipaciones > 0) {
		var mensaje = "Ya existen participaciones configuradas para esta cobertura. " +
				"Si continua se borraran las actuales participaciones.";
		permitirCopiar = confirm(mensaje);
	}
	
	if (permitirCopiar) {
		copiarParticipacionesFacultativoCoberturaEndosoAnterior(idTdContratoFacultativo, 
				function(response) { 
			procesarRespuestaXml(response.responseXML);
			mostrarGridParticipacionFacultativo(idTdContratoFacultativo); 
			recargaCoberturasFacultativaTree(idTmContratoFacultativo, idTdContratoFacultativo);
		});
	}
}

function copiarParticipacionesFacultativoCoberturaEndosoAnterior(idTdContratoFacultativo, onSuccessFunction) {
	var url = '/MidasWeb/contratofacultativo/participacionfacultativa/copiarParticipacionesCoberturaEndosoAnterior.do'
	var parameter = 'id=' + idTdContratoFacultativo
	url = url + '?' + parameter;
	new Ajax.Request(url, {
		onSuccess: onSuccessFunction,
		asynchronous: false
	})
}

function ventanaAceptarCancelar() {
	dhxWins = new dhtmlXWindows();
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var ventanaAceptarCancelar = dhxWins.createWindow("aceptarCancelar", 20, 30, 320, 240);
	ventanaAceptarCancelar.center();
	ventanaAceptarCancelar.button("close").disable();
}

function cantidadElementosGridFacultativo() {
	return facultativoCoberturaGrid.getRowsNum();
}


/**
 * mostrarCatalogoGenerico sirve para mostrar el Contenido de la pagina que quieres reenderear, solo debes darle el Path del action que necesitas.
 * @autor atavera
 * @param path 
 * @param target
 */
function mostrarCatalogoGenerico(path,target,forma) {
	if(target != undefined){
		targetWorkArea = target;
	}
	var formaSerializada = (forma!=undefined)? "?"+ forma.serialize() : "" ;
	
	sendRequestJQ(null, path+formaSerializada, targetWorkArea, null);
}
/**
 * operacionGenerica sirve para agregar, editar o borrar, solo debes darle el path del action y el tipo de accion a realizar.
 * @autor atavera
 * @param path
 * @param tipoAccion
 * @param id
 */
function operacionGenerica(path, tipoAccion, objName, id, pNext){
	var Accion = "?tipoAccion=" + tipoAccion
	var Selected = (id!=undefined)? "&"+objName+".id="+id:"";
	var sFuncion = (pNext!=undefined)? pNext:null;
	sendRequestJQ(null, path + Accion + Selected, targetWorkArea, sFuncion);
}

function operacionGenericaConParametros(path, tipoAccion, objName,idProperty,id){
	var Accion = "?tipoAccion=" + tipoAccion
	var Selected = (id!=undefined)? "&"+objName+"."+idProperty+"="+id:"";
	sendRequestJQ(null, path + Accion + Selected, targetWorkArea, null);
}

function operacionGenericaConParams(path, tipoAccion,jsonParam){
	var Accion = "?tipoAccion=" + tipoAccion;
	var params = (jsonParam!=undefined)?"&"+jQuery.param(jsonParam):"";
	sendRequestJQ(null, path + Accion + params, targetWorkArea, null);
}

function operacionGenericaConParamsHistorial(path, tipoAccion,jsonParam,divToRenderer){
	
	mostrarIndicadorCarga('indicador');
	if(!divToRenderer){
		divToRenderer=targetWorkArea;
	}
	var Accion = "?tipoAccion=" + tipoAccion;
	var params = (jsonParam!=undefined)?"&"+jQuery.param(jsonParam):"";
	sendRequestJQ(null, path + Accion + params, divToRenderer, null);
	
}

/**
 * Funcion que se pasa la url, parametros en formato Json y lo renderea el resultado en un div especifico.
 * @param path
 * @param jsonParam
 * @param divToRenderer
 */
function operacionGenericaConParamsRendererIntoDiv(path,jsonParam,divToRenderer){
	if(!divToRenderer){
		divToRenderer=targetWorkArea;
	}
	var params = (jsonParam!=undefined)?"?"+jQuery.param(jsonParam):"";
	sendRequestJQ(null, path + params, divToRenderer, null);
}
/**
 * 
 * @param path
 * @param form
 * @param pNext
 * @param confirm
 */
function realizarOperacionGenerica(path, forma, pNext, alert){
	var formaSerializada = (forma!=undefined)? "?"+ jQuery(forma).serialize() : "" ;
	var siguienteFuncion = (pNext==undefined)? null: pNext;
	if(alert == undefined){
		alert = true;
	}
	if(alert){
		if(confirm('\u00BFSeguro que deseas continuar?')){
			sendRequestJQ(null, path + formaSerializada, targetWorkArea, siguienteFuncion);
		}	
	}else{
		sendRequestJQ(null, path + formaSerializada, targetWorkArea, siguienteFuncion);
	}
	
	
}

/**
 * listarFiltradoGenerico contesta con un XML de la url(Action path) y lo carga al GridId
 * que le especifiques, si agregas una forma serializa esa forma al url que diste.   
 * @autor atavera
 * @param url
 * @param gridId
 * @param forma
 */
 var grid;
function listarFiltradoGenerico(url, gridId, forma,idField, idModal,divCarga,enableGetRow){
	var loadDiv=(divCarga!=null)?divCarga:"divCarga";
	grid = new dhtmlXGridObject(gridId);
	grid.attachEvent("onRowDblClicked", function(){
		doOnRowDblClicked(grid,idField, idModal,enableGetRow,gridId);
	});
	grid.attachEvent("onXLS", function(grid,count){
		mostrarIndicadorCarga(loadDiv); 
	 });
	(forma!=null)?grid.load(url+"?"+jQuery(forma).serialize()):grid.load(url);
	/*******************************************************************
	** Si es el catalogo de autorizacion de agentes aplicar las siguientes
	** funciones que se encuentran en autorizacionagentes.js
	*****************************************************************/
	if(gridId == "autorizacionAgentesGrid"){
	 	grid.attachEvent("onXLE", function(grid,count){	 
	 		ocultarIndicadorCarga(loadDiv);	
		 	jQuery("#checkAll").attr("checked","checked");	 	
		 	ocultarBtnRechazar();
		 });
	 	
	 	grid.attachEvent("onCheck",ocultarBtnRechazar);	 	
	 }else{
		 grid.attachEvent("onXLE", function(grid,count){
			 ocultarIndicadorCarga(loadDiv);
		 }); 
	 }
	return grid;
}

function initPagingGrid(url, gridId, forma,idField, idModal,divCarga,enableGetRow,pageCount){
	var loadDiv=(divCarga!=null)?divCarga:"divCarga";
	grid = new dhtmlXGridObject(gridId);
	//grid.rowsBuffer.length=100;
	grid.attachEvent("onRowDblClicked", function(){
		doOnRowDblClicked(grid,idField, idModal,enableGetRow);
	});
	grid.attachEvent("onXLS", function(grid,count){
		mostrarIndicadorCarga(loadDiv); 
	 });
	 /*grid.attachEvent("onPaging", function(count){
		 //var divPaginadoActivo=jQuery("div.dhx_page_active_light>div.dhx_page_active_light");
		 //var paginaActual=divPaginadoActivo[0].innerHTML;
		 //alert("Conteo:"+count+"|Pagina Actual:"+paginaActual);
	 });*/
	(forma!=null)?grid.load(url+"?"+jQuery(forma).serialize()):grid.load(url);
	/*******************************************************************
	** Si es el catalogo de autorizacion de agentes aplicar las siguientes
	** funciones que se encuentran en autorizacionagentes.js
	*****************************************************************/
	if(gridId == "autorizacionAgentesGrid"){
	 	grid.attachEvent("onXLE", function(grid,count){	 		 	
		 	ocultarIndicadorCarga(loadDiv);		
		 	checkUncheckAll();		 	
		 });
	 	
	 	grid.attachEvent("onCheck",ocultarBtnRechazar);	 	
	 }else{
		 grid.attachEvent("onXLE", function(grid,count){
			 ocultarIndicadorCarga(loadDiv);
		 }); 
	 }
}

function doOnRowDblClicked(grid,idField, idModal,enableGetRow,gridId) {
	var id=grid.getSelectedId();
	var idValue="";
	if (gridId=="agenteGrid" && idField == "idNuevoAgente")
	{
		var ids = gridAgentes.getCheckedRows(0);
		if (ids!="")
		{
			var countSelected = ids.split(",");
			if (countSelected.length > 0) {
				if(confirm('\u00BFEsta seguro de relacionar al negocio?')){
					for (i = 1; i <= countSelected.length; i++) {
						if (i != countSelected.length) {
							idValue += grid.cellById(countSelected[i - 1], 1)
							.getValue()
							+ ",";
						} else {
							idValue += grid.cellById(countSelected[i - 1], 1)
							.getValue();
						}
					}
				}
			}
		}
		else {
			alert("No existe ningun registro marcado");
		}
	}
	else {
		idValue=grid.cellById(id, 0).getValue();
	}
	//Obtiene idDomicilio 
	var selectedId=grid.getSelectedRowId();
	//var cellObj = grid.cellById(selectedId, 1);
	//var idDom = cellObj.getValue();
	var idDom = null;
	var nombre = grid.cellById(selectedId, 1).getValue();
	if(enableGetRow){
		var cell1=grid.cellById(id, 0);
		var cell2=grid.cellById(id, 1);
		var val1=(cell1!=null)?cell1.getValue():null;
		var val2=(cell2!=null)?cell2.getValue():null;
		idValue=val1+"_"+val2;
	}
	/*jQuery.each(jQuery(".jsSearchResultField"),function(index,){
	});*/
	if(idField!=null){
		var componentSearch=jQuery.getObject(idField);
		var nombreField = "nombre"+idField;
		var domField = "dom"+idField;
		var nombreComponentSearch=jQuery.getObject(nombreField);
		var domComponentSearch=jQuery.getObject(domField);
		if(idField == "idAsegurado"){
			parent.accederVentanaModalComplementarAsegurado("ventanaAsegurado", idValue, idDom);
		}else{
			if(jQuery.getObject(idField).html() != null){
				componentSearch.val(idValue);
				nombreComponentSearch.val(nombre);
				domComponentSearch.val(idDom);
				//alert("Se ha seleccionado el registro con la clave:"+id);
				componentSearch.change();
			}else{			
				parent.jQuery.getObject(nombreField).val(nombre);
				parent.jQuery.getObject(domField).val(idDom);
				parent.jQuery.getObject(idField).val(idValue);
				parent.jQuery.getObject(idField).change();
			}
		}
		if(idModal){
			closeDhtmlxWindows(idModal);
		}
	}
	//jQuery("#"+idField).val(id);	
	//return confirm("Row with ID " + rowId + " was double clicked. Do you want to proceed?");
}

function seleccionCatalogoCliente(id,idField, idModal, nombre) {
	if(idField!=null){
		var componentSearch=jQuery.getObject(idField);
		var nombreField = "nombre"+idField;
		var nombreComponentSearch=jQuery.getObject(nombreField);
		if(idField == "idAsegurado"){
			parent.accederVentanaModalComplementarAsegurado("ventanaAsegurado", id);
		}else{
			if(jQuery.getObject(idField).html() != null){
				componentSearch.val(id);
				nombreComponentSearch.val(nombre);
				//alert("Se ha seleccionado el registro con la clave:"+id);
				componentSearch.change();
			}else{			
				parent.jQuery.getObject(nombreField).val(nombre);
				parent.jQuery.getObject(idField).val(id);
				parent.jQuery.getObject(idField).change();
			}
		}
		if(idModal)
		cerrarContenedorVentanas(idModal);
	}
}

function doOnRowSelected(rowId) {
	return null;
}

/**
 *  mostrarVentanaGenerica lanza una ventana generica donde puedes modificar el URL a renderear, Texto del Titulo, ancho y altura.
 * @autor atavera
 * @param url
 * @param text
 * @param width
 * @param height
 */
function mostrarVentanaGenerica(url,text,width,height, forma){
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	(forma==undefined)? forma="":forma= "?"+jQuery(forma).serialize();
	if(width==undefined) width=600;
	if(height==undefined) height=400;
	
	oWin = dhxWins.createWindow("ventanaGenerica",0,0,width,height);
	oWin.setText(text);
	oWin.setModal(true);
	oWin.center();
	oWin.attachURL(url+forma);
}

/*
 * Descarga documentos en configuracion de productos 
 */
function descargarDocumentoProducto(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function AdjuntarArchivoRCSWindow(claveTipo){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    				cargarRangos(idToControlArchivo);
        		}
    		} 
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}


function AdjuntarArchivoREASWindow(claveTipo){
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;

        		    sendRequestM(null,'/MidasWeb/danios/reportes/reporteRCS/cargaEsquemasREAS.do?idToControlArchivo='+ idToControlArchivo, 'resultadosDocumentos', null,'El proceso ha terminado.');
        		}
    		} 
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

function AdjuntarArchivoReaseguradoresCNSFWin(claveTipo){
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;

        		    sendRequestM(null,'/MidasWeb/catalogos/reaseguradoresCNSF/cargaEsquemasReaseguradoras.do?idToControlArchivo='+ idToControlArchivo, 'resultadosDocumentos', null,'El proceso ha terminado.');
        		}
    		} 
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

/******************************** INICIAN FUNCIONES PARA ENVIAR FACTURAS XML A AXOSNET ****************/
function mostrarVentanaFacturaXML(idOrigenEnvio, origenEnvio, tipo, cuentaAfectada, hideBotonCargar, positionX, positionY, width, height){	
	var url = "/MidasWeb/envioxml/mostrar.action?envioFactura.origenEnvio=" + origenEnvio + "&envioFactura.idOrigenEnvio=" + idOrigenEnvio + "&envioFactura.tipo=" + 
		tipo + "&envioFactura.cuentaAfectada=" + cuentaAfectada + "&hideCargar="+hideBotonCargar;
	parent.mostrarVentanaModal(origenEnvio, "Facturas XML", positionX?positionX:200, positionY?positionY:320, width?width:1010, height?height:460, url, null);
}

function mostrarVentanaDetalleEnvio(idEnvio){
	var pathId = "?envioFactura.idEnvio="+idEnvio;
	var url = "/MidasWeb/envioxml/listardet.action"+pathId;
	id = "respuestaEnvio";
	text = "Detalle envio";
	positionX= 200;
	positionY=320;
	width= 650;
	height=250;

	if(mainDhxWindow == null){
		mainDhxWindow = new dhtmlXWindows();	
		mainDhxWindow.enableAutoViewport(true);
		mainDhxWindow.setImagePath("/MidasWeb/img/dhxwindow/");
		mainDhxWindow.attachEvent("onContentLoaded", function(){
			unblockPage();
		});
	}
	
	var win = mainDhxWindow.window(id);
	
	if(mainDhxWindow != null){
		for (var a in mainDhxWindow.wins) {
			if (mainDhxWindow.wins[a].isModal()) {
				parentWindow = mainDhxWindow.wins[a];
				parentWindow.setModal(false);
				zIndexParentWindow = parentWindow.style.zIndex;
				parentWindow.style.zIndex = 1;
			}
		}
	}	
	win = mainDhxWindow.createWindow(id, positionX, positionY, width, height);
	win.setText(text);
	win.center();
	win.setModal(true);
	win.button("close").attachEvent("onClick", function(){
		cerrarVentanaModal(id);
	});	
	win.button("minmax1").hide();	
	win.button("close").show();
	win.button("park").hide();
	var grid = win.attachGrid();
	grid.loadXML(url);
}

function mostrarCargarFile(fobj, urlProcesaArchivo, parameters, target, codigoTipoDocumento, functionOnAddFile, functionOnBeforeUpload ) {
	
	var form = fobj;
	
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	
	var cargaOfFile = dhxWins.createWindow("cargaOfFile", 34, 100, 440, 265);
	cargaOfFile.setText("Factura XML");
	cargaOfFile.button("minmax1").hide();
	cargaOfFile.button("park").hide();
	cargaOfFile.setModal(true);
	cargaOfFile.center();
	cargaOfFile.denyResize();
	cargaOfFile.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setFilesLimit(1);
    vault.create("vault");
    vault.setFormField("claveTipo", "17");
    var fName;
    vault.onAddFile = function(fileName) { 
    	fName = fileName; 
        return true;
     }; 
    
    vault.onBeforeUpload = function(files){
    	jQuery("#file1").attr("name", "envioFactura.facturaXml");
    	
    	if(jQuery("#file1").val() != ''){
    		totalRequests = totalRequests + 1;
			showIndicator();
    	}
    }
    
    var idTipo = jQuery(form).find('input#idOrigenEnvio').val();
    var tipo = jQuery(form).find('input#origenEnvio').val();
    var hideBotonCargar = jQuery(form).find('input#hideCargar').val();
    var tipoCheqTR = jQuery(form).find('input#tipo').val();
    var cuentaAfectada = jQuery(form).find('input#cuentaAfectada').val();
    
    jQuery("#buttonUploadId").unbind("click");
    jQuery("#buttonUploadId").click(function(){
    	//Se valida que se eligio archivo para subir
    	var url = "/MidasWeb/envioxml/cargar.action?envioFactura.idOrigenEnvio=" + idTipo + "&envioFactura.origenEnvio=" + tipo + "&envioFactura.tipo=" +
    		tipoCheqTR + "&envioFactura.cuentaAfectada=" + cuentaAfectada + "&hideCargar=" + hideBotonCargar + "&fileName=" + fName;
        if(jQuery("#file1").val() != ''){
			jQuery.ajaxFileUpload({
				url: url,
				secureuri:false,
				fileElementId: "file1",
				dataType: 'text',
				success: function(data, estatus){
				 	parent.dhxWins.window("cargaOfFile").close();
				 	$(jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('body')[0]).innerHTML = data;
					loadListFacturas(tipo);
				},
				complete : function(data, estatus) {
						totalRequests = totalRequests - 1;
						hideIndicator();
				},
				error: function(data, estatus){
					alert('Ocurrio un error inesperado');
				}
			});
		}
	});    
}


function loadListFacturas(tipo){

	var mensaje = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#mensaje').val();
	if(mensaje){
		var tipoMensaje = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#tipoMensaje').val();
		mostrarVentanaMensaje(tipoMensaje, mensaje, null);
		jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#mensaje').val('');
		jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#tipoMensaje').val('');
	}
	
	jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#listaFacturasGrid').html('');
	
	var solicitudDocumentosGrid = new dhtmlXGridObject(jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#listaFacturasGrid')[0]);
	var idOrigenEnvio = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#idOrigenEnvio').val();
	var origenEnvio = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#origenEnvio').val();
	var tipoCheqTR = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#tipo').val();
	var cuentaAfectada = jQuery(mainDhxWindow.window(tipo)).find('iframe').contents().find('#cuentaAfectada').val();	
	
	var pathId = "?envioFactura.idOrigenEnvio="+idOrigenEnvio+ "&envioFactura.origenEnvio="+origenEnvio + "&envioFactura.tipo=" + tipoCheqTR +
		"&envioFactura.cuentaAfectada=" + cuentaAfectada;
		
	var url = "/MidasWeb/envioxml/listar.action"+pathId;
	solicitudDocumentosGrid.load(url);
}

jQuery.extend({
    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout		
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})

function filtrarFacturas(){
	
	var solicitudDocumentosGrid = new dhtmlXGridObject('listaFacturasGrid');
	
	var pathId = "?"+jQuery('#envioFacturaForm').serialize();
		
	var url = "/MidasWeb/envioxml/listar.action"+pathId;
	solicitudDocumentosGrid.load(url);
}

function mostrarPaginaProsa( URLProsa){

	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	
	ventanaCotizacion = dhxWins.createWindow("PROSA", 400, 320, 760, 500);
	ventanaCotizacion.btns["minmax1"].hide(); 
	ventanaCotizacion.btns["minmax2"].hide();
	ventanaCotizacion.btns["park"].hide(); 
	ventanaCotizacion.setText("Pago de Recibos");
	ventanaCotizacion.setModal(true);
	ventanaCotizacion.centerOnScreen();
	
	ventanaCotizacion.attachURL(URLProsa);
}

/******************************** TERMINA FUNCIONES PARA ENVIAR FACTURAS XML A AXOSNET ****************/

function mostrarAnexarDocumentosReferencias(idToSeccion) {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    
    vault.onUploadComplete = function(files) {
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "traerListaDocumentosReferencias();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "37");
}

