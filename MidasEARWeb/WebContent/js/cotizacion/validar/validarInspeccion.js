var accordionVI;
var autorizacionesPendientesGrid;
var accordionValidarCot = null;

function mostrarAutorizacionesInspeccion() {
	var url = "/MidasWeb/cotizacion/validar/inspeccion/mostrarValidarInspeccionAutorizar.do?idToCotizacion="
			+ parent.document.getElementById('idToCotizacion').value;
		parent.accordionValidarCot.cells("cell1").attachURL(url);
}
function mostrarInspeccion(){
	var url = "/MidasWeb/cotizacion/validar/inspeccion/mostrarValidarInspeccion.do?idToCotizacion="
		+ parent.document.getElementById('idToCotizacion').value+"&uid="+(new Date()).valueOf();
	parent.accordionValidarCot.cells("cell1").attachURL(url);	
}
function inicializaAccordionValidarCotizacion(idCotizacion,cell,mostrarMensaje){
	if(mostrarMensaje) {
		var mensaje = document.getElementById("mensajeValidar");
		var tipoMensaje = document.getElementById("tipoMensajeValidar");
		parent.mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, null);
	}
	parent.totalRequests = 1;
	parent.showIndicator();
	accordionValidarCot = new dhtmlXAccordion("accordionValidarCot");
	accordionValidarCot.addItem("cell1", "Inspecciones");
	accordionValidarCot.addItem("cell2", "Reaseguro Facultativo");
	accordionValidarCot.addItem("cell3", "Autorizaciones Pendientes");
	accordionValidarCot.cells("cell2").attachURL("/MidasWeb/cotizacion/validar/mostrarValidarReaseguroFacultativo.do?idToCotizacion="+idCotizacion);
	accordionValidarCot.cells("cell3").attachURL("/MidasWeb/cotizacion/validar/mostrarAutorizacionesPendientesPorCotizacion.do?id="+idCotizacion);
	accordionValidarCot.cells("cell1").attachURL("/MidasWeb/cotizacion/validar/inspeccion/mostrarValidarInspeccion.do?idToCotizacion="+idCotizacion+"&uid="+(new Date()).valueOf());
	
	accordionValidarCot.setEffect(false);
	accordionValidarCot.openItem(cell);
}

//datagrid para accordion de reaseguro facultativo.
var dataGridPoliza;
var dataGridInciso;
var dataGridSubInciso;


function initDataGridReaseguroFacultativo(){
	
	dataGridPoliza = new dhtmlXGridObject('cumuloPorPolizaGrid');
	dataGridPoliza.setHeader("Linea,Suma asegurada,Status,Acciones,#cspan");
	dataGridPoliza.setColumnIds("linea,sumaAsegurada,estado,accion1,accion2");
	dataGridPoliza.setInitWidths("*,150,100,35,35");
	
	dataGridPoliza.setColAlign("left,right,left,center,center");
	dataGridPoliza.setColSorting("str,na,str,str,str");
	dataGridPoliza.setColTypes("ro,edn,ro,img,img");
	dataGridPoliza.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	dataGridPoliza.setSkin("light");
	dataGridPoliza.enableLightMouseNavigation(true);
	
	dataGridPoliza.setNumberFormat("$0,000.00",1);
	dataGridPoliza.attachEvent("onRowCreated",function(rowId, rowObj) {
		dataGridPoliza.cellById(rowId, 1).setDisabled(true);
		return true;
	});
	
	dataGridPoliza.init();
	dataGridPoliza.setCSVDelimiter("\t");
	dataGridPoliza.loadCSVString(document.getElementById('registrosCumuloPoliza').innerHTML);
//	dataGridPoliza.load("/MidasWeb/cotizacion/validar/obtenerListaCumuloPoliza.do?tipoCumulo=1", null, 'json');
	
	dataGridInciso = new dhtmlXGridObject('cumuloPorIncisoGrid');
	dataGridInciso.setHeader("Inciso,Linea,Suma asegurada,Status,Acciones,#cspan");
	dataGridInciso.setColumnIds("inciso,linea,sumaAsegurada,estado,accion1,accion2");
	dataGridInciso.setInitWidths("100,*,150,100,35,35");
	
	dataGridInciso.setColAlign("left,left,right,left,center,center");
	dataGridInciso.setColSorting("str,str,na,str,str,str");
	dataGridInciso.setColTypes("ro,ro,edn,ro,img,img");
	dataGridInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	dataGridInciso.setSkin("light");
	dataGridInciso.enableLightMouseNavigation(true);
	
	dataGridInciso.setNumberFormat("$0,000.00",2);
	
	dataGridInciso.attachEvent("onRowCreated",function(rowId, rowObj) {
		dataGridInciso.cellById(rowId, 2).setDisabled(true);
		return true;
	});
	
	dataGridInciso.init();
	dataGridInciso.setCSVDelimiter("\t");
	dataGridInciso.loadCSVString(document.getElementById('registrosCumuloInciso').innerHTML);
//	dataGridInciso.load("/MidasWeb/cotizacion/validar/obtenerListaCumuloPoliza.do?tipoCumulo=2", null, 'json');
	
	
	dataGridSubInciso = new dhtmlXGridObject('cumuloPorSubIncisoGrid');
	dataGridSubInciso.setHeader("Inciso,Subinciso,Linea,Suma asegurada,Status,Acciones,#cspan");
	dataGridSubInciso.setColumnIds("inciso,subinciso,linea,sumaAsegurada,estado,accion1,accion2");
	dataGridSubInciso.setInitWidths("70,70,*,150,100,35,35");
	
	dataGridSubInciso.setColAlign("left,left,left,right,left,center,center");
	dataGridSubInciso.setColSorting("str,str,str,str,str,str,str");
	dataGridSubInciso.setColTypes("ro,ro,ro,ro,ro,img,img");
	dataGridSubInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	dataGridSubInciso.setSkin("light");
	dataGridSubInciso.enableLightMouseNavigation(true);
	
	dataGridSubInciso.setNumberFormat("$0,000.00",3);
	dataGridSubInciso.attachEvent("onRowCreated",function(rowId, rowObj) {
		dataGridSubInciso.cellById(rowId, 3).setDisabled(true);
		return true;
	});
	
	dataGridSubInciso.init();
	dataGridSubInciso.setCSVDelimiter("\t");
	dataGridSubInciso.loadCSVString(document.getElementById('registrosCumuloSubInciso').innerHTML);
//	dataGridSubInciso.load("/MidasWeb/cotizacion/validar/obtenerListaCumuloPoliza.do?tipoCumulo=3", null, 'json');
	parent.totalRequests = 0;
	parent.hideIndicator();
}

function mostrarReaseguroFacultativo(idLineaSoporte,idCotizacion,cumulo){
	
	if (parent.dhxWins==null){
		parent.dhxWins = new parent.dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaPersona = parent.dhxWins.createWindow("reaseguroFacultativo", 400, 320, 710, 360);
	parent.ventanaPersona.setText("Cotizaci&oacute;n de Reaseguro Facultativo.");
	parent.ventanaPersona.center();
	parent.ventanaPersona.setModal(true);
	parent.ventanaPersona.attachURL("/MidasWeb/cotizacion/validar/mostrarCotizacionReaseguroFacultativo.do?idLinea="+idLineaSoporte+"&idCotizacion="+idCotizacion+"&tipoCumulo="+cumulo);
	parent.ventanaPersona.button("minmax1").hide();
}

function aceptarFacultativo(fobj) {
	parent.sendRequest(fobj, '/MidasWeb/cotizacion/validar/aceptarFacultativo.do', 'contenido_validar', 'ventanaPersona.close(); inicializaAccordionValidarCotizacion(' + fobj.idToCotizacion.value + ', \&#39;cell2\&#39;);');
}

function cerrarVentanaReaseguroFacultativo(){
	if (parent.ventanaPersona != null && parent.ventanaPersona != undefined){
		parent.ventanaPersona.close();
	}
}

function solicitarReaseguroFacultativo(idLineaSoporte,idCotizacion,cumulo){
	new Ajax.Request('/MidasWeb/cotizacion/validar/solicitarReaseguroFacultativo.do',{
		method : "post",
		asynchronous : false,
		parameters : '?idLinea='+idLineaSoporte+'&idCotizacion='+idCotizacion+'&tipoCumulo='+cumulo,
		onSuccess : function(transport) {
			var doc = transport.responseXML;
			var estatus = doc.getElementsByTagName("estatus")[0].firstChild.nodeValue;
			var accion1 = doc.getElementsByTagName("accion1")[0].firstChild.nodeValue;
			var accion2 = doc.getElementsByTagName("accion2")[0].firstChild.nodeValue;
			if(estatus != null && estatus != undefined){
				if(accion1 != null && accion1 != undefined){
					if (accion2 != null && accion2 != undefined){
						if (cumulo == 1){
							dataGridPoliza.cellById(idLineaSoporte,2).setValue(estatus);
							dataGridPoliza.cellById(idLineaSoporte,3).setValue(accion1);
							dataGridPoliza.cellById(idLineaSoporte,4).setValue(accion2);
						} else if (cumulo ==2){
							dataGridInciso.cellById(idLineaSoporte,3).setValue(estatus);
							dataGridInciso.cellById(idLineaSoporte,4).setValue(accion1);
							dataGridInciso.cellById(idLineaSoporte,5).setValue(accion2);
						} else if (cumulo==3){
							dataGridSubInciso.cellById(idLineaSoporte,4).setValue(estatus);
							dataGridSubInciso.cellById(idLineaSoporte,5).setValue(accion1);
							dataGridSubInciso.cellById(idLineaSoporte,6).setValue(accion2);
						}
					}
				}
			}
		} // End of onSuccess
	});	
}

function cancelarReaseguroFacultativo(idLineaSoporte,idCotizacion,cumulo){
	new Ajax.Request('/MidasWeb/cotizacion/validar/cancelarReaseguroFacultativo.do',{
		method : "post",
		asynchronous : false,
		parameters : '?idLinea='+idLineaSoporte+'&idCotizacion='+idCotizacion+'&tipoCumulo='+cumulo,
		onSuccess : function(transport) {
			var doc = transport.responseXML;
			var estatus = doc.getElementsByTagName("estatus")[0].firstChild.nodeValue;
			var accion1 = doc.getElementsByTagName("accion1")[0].firstChild.nodeValue;
			var accion2 = doc.getElementsByTagName("accion2")[0].firstChild.nodeValue;
			if(estatus != null && estatus != undefined){
				if(accion1 != null && accion1 != undefined){
					if (accion2 != null && accion2 != undefined){
						if (cumulo == 1){
							dataGridPoliza.cellById(idLineaSoporte,2).setValue(estatus);
							dataGridPoliza.cellById(idLineaSoporte,3).setValue(accion1);
							dataGridPoliza.cellById(idLineaSoporte,4).setValue(accion2);
						} else if (cumulo ==2){
							dataGridInciso.cellById(idLineaSoporte,3).setValue(estatus);
							dataGridInciso.cellById(idLineaSoporte,4).setValue(accion1);
							dataGridInciso.cellById(idLineaSoporte,5).setValue(accion2);
						} else if (cumulo==3){
							dataGridSubInciso.cellById(idLineaSoporte,4).setValue(estatus);
							dataGridSubInciso.cellById(idLineaSoporte,5).setValue(accion1);
							dataGridSubInciso.cellById(idLineaSoporte,6).setValue(accion2);
						}
					}
				}
			}
		} // End of onSuccess
	});	
}

function initAccordionVI(idToCotizacion){
	accordionVI = new dhtmlXAccordion("accordionVI");
	accordionVI.addItem("a1", "Inspecciones");
	accordionVI.addItem("a2", "Reaseguro Facultativo");
	accordionVI.addItem("a3", "Autorizaciones");
	accordionVI.cells("a1").attachURL("/MidasWeb/cotizacion/validar/inspeccion/mostrarValidarInspeccion.do?idToCotizacion="+idToCotizacion+"&uid="+(new Date()).valueOf());
	//accordionVI.cells("a2").attachURL();
	//accordionVI.cells("a3").attachURL();
	
	
	accordionVI.openItem("a1");
	accordionVI.setEffect(true);
}

//Datagrid en tablaAutorizarOmisionInspeccion.jsp, CU Da�os > Registrar Cotizacion > Validar > Inspecci�n > Autorizar omisi�n de inspecci�n
function mostrarAutorizacionesPendientesYRechazadas(idToCotizacion){
	autorizacionesPendientesGrid = new dhtmlXGridObject('autorizacionesPendientesYRechazadas');
	autorizacionesPendientesGrid.setHeader("Inciso,Ubicaci&oacute;n,,");
	autorizacionesPendientesGrid.setColumnIds("inciso,ubicacion,icono,checked");
	autorizacionesPendientesGrid.setInitWidths("50,400,50,50");
	autorizacionesPendientesGrid.setColAlign("center,center,center,center");
	autorizacionesPendientesGrid.setColSorting("str,str,str,str");
	autorizacionesPendientesGrid.setColTypes("ro,ro,img,ch");
	autorizacionesPendientesGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	autorizacionesPendientesGrid.setSkin("light");
	autorizacionesPendientesGrid.enableDragAndDrop(false);	
	autorizacionesPendientesGrid.enableLightMouseNavigation(false);
	autorizacionesPendientesGrid.init();
	autorizacionesPendientesGrid.load('/MidasWeb/cotizacion/validar/inspeccion/cargarAutorizacionesPendientes.do?idToCotizacion='+idToCotizacion, null, 'json');
	/*processor.enableDataNames(true);
	processor.setTransactionMode("POST"); 
	processor.setUpdateMode("off");
	processor.init(autorizacionesPendientesGrid);*/	
}

function actualizarAutorizacionesPendientes(idToCotizacion, mensaje,nuevoEstatus, mensajeConfirmacion){
	var checkIds = autorizacionesPendientesGrid.getCheckedRows(3);
	 if(checkIds.length > 0){
		 if(confirm(mensajeConfirmacion)){
		 new Ajax.Request('/MidasWeb/cotizacion/validar/inspeccion/autorizar/actualizarAutorizacionesPendientes.do?ids='+checkIds+'&nuevoEstatus='+nuevoEstatus, {
				method : "post",
				encoding : "UTF-8",
				parameters : null,
				onCreate : function(transport) {
					totalRequests = totalRequests + 1;
					showIndicator();
				},
				onComplete : function(transport) {
					totalRequests = totalRequests - 1;
					hideIndicator();
				},
				onSuccess : function(transport) {
				mostrarAutorizacionesPendientesYRechazadas(idToCotizacion);
				}
			});
		 }
		 //sendRequest(inspeccionForm,'/MidasWeb/cotizacion/validar/inspeccion/autorizar/actualizarAutorizacionesPendientes.do?ids='+checkIds+'&nuevoEstatus='+nuevoEstatus,null,null);
	 }
	 else
		 alert(mensaje);
}

function mostrarAdjuntarDocumentoAnexoInspeccionIncisoCotizacionWindow(idToInspeccionIncisoCotizacion) {
	if(dhxWins != null) 
		dhxWins.unload();
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoAnexoInspeccionIncisoCotizacion", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documentos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        parent.dhxWins.window("adjuntarDocumentoAnexoInspeccionIncisoCotizacion").close();
        recargarTablaDocumentosAnexosInspeccion(idToInspeccionIncisoCotizacion);
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "6");
    vault.setFormField("idToInspeccionIncisoCotizacion", idToInspeccionIncisoCotizacion);
}

function recargarTablaDocumentosAnexosInspeccion(idToInspeccionIncisoCotizacion){
	//alert('sending request');
	sendRequest(null, '/MidasWeb/cotizacion/validar/inspeccion/listarDocumentosAnexosInspeccionIncisoCotizacion.do?id='+idToInspeccionIncisoCotizacion,'tablaDocumentosAnexosInspeccion',null);
}

function recargarAcordeonValidarCotizacion(itemNumber, idToCotizacion){
	if (itemNumber == 1)
		sendRequest(null,'/MidasWeb/cotizacion/validar/mostrarValidar.do?id=' + idToCotizacion, 'contenido_validar', 'inicializaAccordionValidarCotizacion(' + idToCotizacion + ', \'cell1\');');
		//parent.accordionVI.cells("a1").attachURL("/MidasWeb/cotizacion/validar/inspeccion/mostrarValidarInspeccion.do?idToCotizacion="+idToCotizacion+"&uid="+(new Date()).valueOf());
}
	
function sendRequestFromAccordion(fobj, actionURL, targetId, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
				true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		},
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		},
		onSuccess : function(transport) {
			if (targetId != null)
				parent.document.getElementById(targetId).innerHTML = transport.responseText;
			
			if (pNextFunction !== null && pNextFunction !== '') {
				parent.eval(pNextFunction);
			} 
		}
		
	});
}

var fechaDeLaInspeccion;
var fechaElaboracionDocumento;

function cargarCalendariosReporteInspeccion(){
	if (fechaDeLaInspeccion == null){
		fechaDeLaInspeccion = new dhtmlxCalendarObject("fechaDeLaInspeccionDiv");
		fechaDeLaInspeccion.hide();
		fechaDeLaInspeccion.attachEvent("onClick",function(fecha){
			$('fechaDeLaInspeccion').value=fechaDeLaInspeccion.getFormatedDate(formatoFechaCalendario(),fecha);
			mostrarCalendariosReporteInspeccion('fechaDeLaInspeccion');
		});
	}
	if (fechaElaboracionDocumento == null){
		fechaElaboracionDocumento = new dhtmlxCalendarObject("fechaElaboracionDocumentoDiv");
		fechaElaboracionDocumento.hide();
		fechaElaboracionDocumento.attachEvent("onClick",function(fecha){
			$('fechaElaboracionDocumento').value=fechaElaboracionDocumento.getFormatedDate(formatoFechaCalendario(),fecha);
			mostrarCalendariosReporteInspeccion('fechaElaboracionDocumento');
		});
	}
}

function destruirCalendarioReporteInspeccion(){
	fechaDeLaInspeccion=null;
	fechaElaboracionDocumento=null;
}

function mostrarCalendariosReporteInspeccion(calendario){
	cargarCalendariosReporteInspeccion();
	
	switch (calendario){
	case 'fechaDeLaInspeccion':
			if (fechaDeLaInspeccion!==null){
				if (fechaDeLaInspeccion.isVisible()){
					fechaDeLaInspeccion.hide();
				}else{
					fechaDeLaInspeccion.show();
					fechaElaboracionDocumento.hide();
				}
			}
			break;
	case 'fechaElaboracionDocumento':
			if (fechaElaboracionDocumento!==null){
				if (fechaElaboracionDocumento.isVisible()){
					fechaElaboracionDocumento.hide();
				}else{
					fechaElaboracionDocumento.show();
					fechaDeLaInspeccion.hide();
				}
			}
			break;
	}
				
}

function mostrarTablaArchivosAdjuntosInspeccion(){
	if ($('idToInspeccionIncisoCotizacion').value > 0){
		document.getElementById('botonesIniciales').style.display='none';
		document.getElementById('documentosAnexos').style.display='block';
	}
}


function validaMensajeErrorCumulo(){
	var errorCumulo= document.getElementById("mensajeErrorCumulo").value;
	
	errorCumulo= trim(errorCumulo);
	
	if(errorCumulo!="0"){
		if(errorCumulo.length>0)
		parent.mostrarVentanaMensaje("20", errorCumulo, null);
	}
	
	
}

function validaMensajeErrorIgualacion(){
	var errorIgualacion= document.getElementById("mensajeErrorIgualacionPrima").value;
	errorIgualacion= trim(errorIgualacion);
	if(errorIgualacion == "0"){
		var mensaje = document.getElementById("mensaje").value;
		var tipoMensaje = document.getElementById("tipoMensaje").value;
		if(mensaje) {
			parent.mostrarVentanaMensaje(tipoMensaje, "Error" + '<br />'+ mensaje, null);
		}
	} else {
		if(errorIgualacion.length>0){
			parent.mostrarVentanaMensaje("10", "Las Siguientes Coberturas Presentan Prima Neta en Ceros: " + '<br />' + errorIgualacion, null);
		}
	}
}

function ltrim(s) {
	return s.replace(/^\s+/, "");
}

function rtrim(s) {
	return s.replace(/\s+$/, "");
}

function trim(s) {
	return rtrim(ltrim(s));
}


