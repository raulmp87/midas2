var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";

function setTotalPrimerRiesgo(indexVar){
	var target = $('totalPrimerRiesgo['+indexVar+']');

	if (target.innerHTML==="" || target.innerHTML==="$0.00"){
		target.innerHTML = formatCurrency($('valorSumaAsegurada['+indexVar+']').value);
	}else{
		target.innerHTML = "$0.00";
	}
	actualizaTotales();
}

function actualizaTotales(){
	var size = document.getElementById('size').value;
	var totalPrimerRiesgo = $('totalSeccionesPrimerRiesgo');
	var total=parseInt(0);
	if (size !== null){
		var check;
		for(var k=0; k<size; k++){
			check = $("checkbox[" + k + "]");
			if (check.checked){
				total = parseFloat(total) + parseFloat($("valorSumaAsegurada[" + k + "]").value);
			} 
		}
		if(total === 0){
			document.getElementById('botonCalcular').style.display = 'none';
		}else{
			document.getElementById('botonCalcular').style.display = 'block';
		}
		totalPrimerRiesgo.innerHTML = formatCurrency(total);
	}
}

function mostrarDiv(){
	var size = $('size').value;
	if (size !== null){
		var check;
		for(var k=0; k<size; k++){
			check = $("checkbox[" + k + "]");
			if (check.checked){
				$('botonCalcular').style.display = 'block';
				return true;
			} 
		}
	}
}

function mostrarDivLUC(){
	var size = $('size').value;
	if (size !== null){
		var check;
		for(var k=0; k<size; k++){
			check = $("checkbox[" + k + "]");
			if (check.checked){
				$('botonCalcular').style.display = 'block';
				return true;
			} 
		}
	}
	$('botonCalcular').style.display = 'none';
}

function validarSeccionesAPrimerRiesgo(){
	var size = $('size').value;
	if (size !== null){
		var check;
		var dependeDeSeccion;
		var dependeDeSeccionAux;
		for(var k=0; k<size; k++){
			check = $("checkbox[" + k + "]");
			dependeDeSeccion = $("claveDependenciaOtrasPr[" + k + "]");
			if (check.checked){
				if(dependeDeSeccion.value == 0){
					return true;
				}else{
					for(var j=0; j<size; j++){
						dependeDeSeccionAux = $("claveDependenciaOtrasPr[" + j + "]");
						check = $("checkbox[" + j + "]");
						if (check.checked && dependeDeSeccionAux.value == 0 ){
							return true;
						}
					}					
				}
			}
		}
	}
	return false;
}
function validaSAPR(){
	var totalPrimerRiesgo = $('totalSeccionesPrimerRiesgo');
	var sumaAseguradaPrimerRiesgo = $('sumaAseguradaPrimerRiesgo');
	var totalF = removeCurrency(totalPrimerRiesgo.value);
	if(parseFloat(totalF) > parseFloat(sumaAseguradaPrimerRiesgo.value)){
		return true;
	}else{
		return false;
	}
}
function validaSecciones(){
	if (validarSeccionesAPrimerRiesgo()){
		//if(validaSAPR()){
			//sendRequest(document.primerRiesgoLUCForm,'/MidasWeb/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do', 'contenido_primerRiesgoLUC','mesajeGlobal(null,null);');
			agregarPrimerRiesgo(document.primerRiesgoLUCForm);
		//}else{
			//alert('El Valor Total de a 1er Riesgo debe ser Mayor que la Suma Asegurada a 1er Riesgo');
		//}	
	}else{
		alert('Las secciones seleccionadas requieren de otra para poder calcular el primer riesgo');
	}
}

function mesajeGlobalChild(puntoDo,area) {
	var textObj = parent.document.getElementById("mensajeGlobal");
	var divObj = parent.document.getElementById("mensajeImg");
	var botonObj = parent.document.getElementById("mensajeBoton");
	
	var mensaje = $('mensaje');
	var tipoMensaje = $('tipoMensaje');
	
	if (puntoDo === null && area === null){
		if(textObj.style.dyplay === 'none') {
			  textObj.style.dyplay = 'block';
			} // End of if
	}
		
	if (mensaje !== null && mensaje.value !== ""){

		textObj.innerHTML = mensaje.value;
		if (tipoMensaje.value === "30"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_ok.png'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\"backgroundFilter('blockWindow');closeWindow('mensaje_popup','400','300');\">Aceptar</a></div>";
		}else if (tipoMensaje.value === "20"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_info.jpg'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" backgroundFilter('blockWindow');closeWindow('mensaje_popup','400','300');\">Aceptar</a></div>";			
		}else if (tipoMensaje.value === "10"){
			divObj.innerHTML = "<img src='/MidasWeb/img/b_no.jpg'>";
			botonObj.innerHTML = "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" backgroundFilter('blockWindow');closeWindow('mensaje_popup','400','300');\">Aceptar</a></div>";	
		}

		if(textObj.style.dyplay === 'none') {
			  textObj.style.dyplay = 'block';
			} // End of if
		
		popUpFromChild("mensaje_popup", "400", "300");
		backgroundFilterFromChild("blockWindow");
	}
}


function agregarPrimerRiesgo(primerRiesgoLUCForm){
	parent.showIndicatorSimple();
	var tipoMensaje = null;
	var mensaje = null;
	new Ajax.Request('/MidasWeb/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do',{
		method : "post", asynchronous : false,
		parameters : (primerRiesgoLUCForm !== undefined && primerRiesgoLUCForm !== null) ? $(primerRiesgoLUCForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
			if (navigator.appName.indexOf("Microsoft") != -1) {
				var formPrimerRiesgo = parent.document.createElement("DIV");
				formPrimerRiesgo.innerHTML = transport.responseText; 
				var respuesta = formPrimerRiesgo.document.childNodes[0].childNodes[0].childNodes[0];

				parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = respuesta.innerHTML;
				
				tipoMensaje = formPrimerRiesgo.document.getElementById("tipoMensaje").value;
				mensaje = formPrimerRiesgo.document.getElementById("mensaje").value;
				
			} else {
				parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;	
				
				tipoMensaje = parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("tipoMensaje").value;
				mensaje = parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("mensaje").value;
			}
			
			parent.hideIndicator();
//			mesajeGlobalChild(null,null);
			parent.mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			
		}
	});
}

function agregarLUC(primerRiesgoLUCForm){
	parent.showIndicatorSimple();
	var tipoMensaje = null;
	var mensaje = null;
	new Ajax.Request('/MidasWeb/cotizacion/primerRiesgoLUC/calcularLUCODT.do',{
		method : "post", asynchronous : false,
		parameters : (primerRiesgoLUCForm !== undefined && primerRiesgoLUCForm !== null) ? $(primerRiesgoLUCForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
			
			if (navigator.appName.indexOf("Microsoft") != -1) {
				var formLUC = parent.document.createElement("DIV");
				formLUC.innerHTML = transport.responseText; 
				var respuesta = formLUC.document.childNodes[0].childNodes[0].childNodes[0];

				parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = respuesta.innerHTML;
				
				tipoMensaje = formLUC.document.getElementById("tipoMensaje").value;
				mensaje = formLUC.document.getElementById("mensaje").value;
				
			} else {
				parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;	
				
				tipoMensaje = parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("tipoMensaje").value;
				mensaje = parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("mensaje").value;
			}
			
			//parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;
		    parent.hideIndicator();
			//mesajeGlobalChild(null,null);	
			parent.mostrarVentanaMensaje(tipoMensaje, mensaje, null);	
		}
	});
}
function eliminarLUC(idCotizacion, numeroAgrupacion, idSeccion){
	parent.showIndicatorSimple();
	var tipoMensaje = null;
	var mensaje = null;
	new Ajax.Request('/MidasWeb/cotizacion/primerRiesgoLUC/eliminarLUC.do?idToCotizacion='+idCotizacion+"&numeroAgrupacion="+numeroAgrupacion+"&idToSeccion="+idSeccion,{
		method : "post", asynchronous : false,
		parameters : (primerRiesgoLUCForm !== undefined && primerRiesgoLUCForm !== null) ? $(primerRiesgoLUCForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
			
			if (navigator.appName.indexOf("Microsoft") != -1) {
				var formLUC = parent.document.createElement("DIV");
				formLUC.innerHTML = transport.responseText; 
				var respuesta = formLUC.document.childNodes[0].childNodes[0].childNodes[0];

				parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = respuesta.innerHTML;
				
				tipoMensaje = formLUC.document.getElementById("tipoMensaje").value;
				mensaje = formLUC.document.getElementById("mensaje").value;
				
			} else {
				parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;	
				
				tipoMensaje = parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("tipoMensaje").value;
				mensaje = parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("mensaje").value;
			}
			
			//parent.accordionPrimerRiesgoLUC.cells("a2").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;
			parent.hideIndicator();
			//mesajeGlobalChild(null,null);	
			parent.mostrarVentanaMensaje(tipoMensaje, mensaje, null);	
		}
	});	
}
function eliminarPRR(idCotizacion, numeroAgrupacion, idSeccion){
	parent.showIndicatorSimple();
	var tipoMensaje = null;
	var mensaje = null;
	new Ajax.Request('/MidasWeb/cotizacion/primerRiesgoLUC/eliminarPRR.do?idToCotizacion='+idCotizacion+"&numeroAgrupacion="+numeroAgrupacion+"&idToSeccion="+idSeccion,{
		method : "post", asynchronous : false,
		parameters : (primerRiesgoLUCForm !== undefined && primerRiesgoLUCForm !== null) ? $(primerRiesgoLUCForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
			
			if (navigator.appName.indexOf("Microsoft") != -1) {
				var formPrimerRiesgo = parent.document.createElement("DIV");
				formPrimerRiesgo.innerHTML = transport.responseText; 
				var respuesta = formPrimerRiesgo.document.childNodes[0].childNodes[0].childNodes[0];

				parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = respuesta.innerHTML;
				
				tipoMensaje = formPrimerRiesgo.document.getElementById("tipoMensaje").value;
				mensaje = formPrimerRiesgo.document.getElementById("mensaje").value;
				
			} else {
				parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;	
				
				tipoMensaje = parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("tipoMensaje").value;
				mensaje = parent.accordionPrimerRiesgoLUC.cells("a1").win._frame.contentWindow.document.getElementById("mensaje").value;
			}
			
			parent.hideIndicator();
			//mesajeGlobalChild(null,null);	
			parent.mostrarVentanaMensaje(tipoMensaje, mensaje, null);
		}
	});	
}
/*
 * Grid de coberturas
 */
var cotizacionCoberturaGrid;
var coberturaProcessor;

function mostrarCoberturasPorSeccion(idToCotizacion,numeroInciso,idToSeccion){

	var div = $('cotizacionCoberturasGrid');
	if(div == null) {
		initAutorizacionCoberturaGrid(idToCotizacion,numeroInciso,idToSeccion);
		return true;
	}	
	cotizacionCoberturaGrid = new dhtmlXGridObject("cotizacionCoberturasGrid");
	cotizacionCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionCoberturaGrid.setEditable(true);
	cotizacionCoberturaGrid.setSkin("light");

	cotizacionCoberturaGrid.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible,A/R/D,,,");
	cotizacionCoberturaGrid.setInitWidths("0,0,0,0,0,0,0,0,200,80,150,50,100,80,80,30,80,30,60,40,40,60,60,0,0,0");
	cotizacionCoberturaGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,edn,ro,co,img,co,img,coro,edn,edn,coro,img,ro,ro,ro");
	cotizacionCoberturaGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,left,left,left,left,center,center,center");
	cotizacionCoberturaGrid.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int,int");
	cotizacionCoberturaGrid.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
	cotizacionCoberturaGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,tipoDeducible,minimo,maximo,tipoLimiteDeducible,img3,desglosaRiesgo,claveSubIncisos,claveIgualacion");		
	cotizacionCoberturaGrid.setColumnHidden(0,true);
	cotizacionCoberturaGrid.setColumnHidden(1,true);
	cotizacionCoberturaGrid.setColumnHidden(2,true);
	cotizacionCoberturaGrid.setColumnHidden(3,true);
	cotizacionCoberturaGrid.setColumnHidden(4,true);
	cotizacionCoberturaGrid.setColumnHidden(5,true);
	cotizacionCoberturaGrid.setColumnHidden(6,true);
	cotizacionCoberturaGrid.setColumnHidden(7,true);
	cotizacionCoberturaGrid.setColumnHidden(23,true);
	cotizacionCoberturaGrid.setColumnHidden(24,true);
	cotizacionCoberturaGrid.setColumnHidden(25,true);	
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",10);
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",12);
	cotizacionCoberturaGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = cotizacionCoberturaGrid.cellById(rId, 4);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 9) {
			var existeObligatoriaParcialContratada = false;
			cotizacionCoberturaGrid.forEachRow(function(id){
				var obligatoriedadCurrentRow = cotizacionCoberturaGrid.cellById(id, 4);
				var claveContrato = cotizacionCoberturaGrid.cellById(id, 9);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Cobertura no puede ser descontratada.\nDebe contratar al menos una Cobertura Obligatoria Parcial.");
				return false;
			}
		}
		if(stage == 2 && (cInd == 14 || cInd == 16) ) {
			var coaseguro = cotizacionCoberturaGrid.cellById(rId, cInd);
			var combo = cotizacionCoberturaGrid.getCustomCombo(rId, cInd);
			combo.put(coaseguro.getValue(), coaseguro.getValue() + '%');
			combo.save();
		}
		if(stage == 2 && cInd == 9 ){
			seleccionarCobertura(stage,rId,cInd,nValue,oValue);
		}
		return true;
	});	
	cotizacionCoberturaGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		cotizacionCoberturaGrid.cellById(rowId, 12).setDisabled(true);
		var tipoSumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 5);
		var desglosaRiesgo = cotizacionCoberturaGrid.cellById(rowId, 23);
		var requiereSubIncisos = cotizacionCoberturaGrid.cellById(rowId, 24);
		var obligatoriedad = cotizacionCoberturaGrid.cellById(rowId, 4);
		var sumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 10);
		var claveContrato = cotizacionCoberturaGrid.cellById(rowId, 9);
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 14);
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 16);
		var cellARD = cotizacionCoberturaGrid.cellById(rowId, 22);
		var claveIgualacion = cotizacionCoberturaGrid.cellById(rowId, 25);
		
		if(claveContrato.getValue()=='1'){
			var idToCotizacion =  cotizacionCoberturaGrid.cellById(rowId, 0).getValue();
			var numeroInciso =  cotizacionCoberturaGrid.cellById(rowId, 1).getValue();
			var idToSeccion =  cotizacionCoberturaGrid.cellById(rowId, 2).getValue();
			var idToCobertura =  cotizacionCoberturaGrid.cellById(rowId, 3).getValue();
			if(claveIgualacion.getValue() == "1") {
				var cadena = "javascript: sendRequest(null,&#39;/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion="+idToCotizacion;
				cadena = cadena +"&numeroInciso="+numeroInciso+"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura;
				cadena = cadena +"&#39;,&#39;configuracion_detalle&#39;,null);";
				cellARD.setValue("/MidasWeb/img/details.gif^Aumentos/Recargos/Descuentos^"+cadena+"^_self");
			} else {
				cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
				cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			}
		}else{
			cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");			
			//cotizacionCoberturaGrid.setCellExcellType(rowId,18,"ro");
			//cotizacionCoberturaGrid.setCellExcellType(rowId,21,"ro");
			
			//Agregado por CesarMorales..
			if(tipoSumaAsegurada.getValue()=='1' || tipoSumaAsegurada.getValue()=='3'){
				cotizacionCoberturaGrid.cellById(rowId, 10).setValue('0.00');
			}
			/////
			cotizacionCoberturaGrid.cellById(rowId, 11).setValue('0.0000');
			cotizacionCoberturaGrid.cellById(rowId, 12).setValue('0.0');
			cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 17).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 15).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 18).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 21).setValue('');

		}
		if(tipoSumaAsegurada.getValue() == '2' || desglosaRiesgo.getValue() == '1' || requiereSubIncisos.getValue() == '1'){
			sumaAsegurada.setDisabled(true);
		}else{			
			cotizacionCoberturaGrid.setCellExcellType(rowId,10,"edn");		
		}
		
		if(desglosaRiesgo.getValue() === '1'){				
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 18).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 19).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 20).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 21).setDisabled(true);
		}else{		
			var idToCobertura = cotizacionCoberturaGrid.cellById(rowId,3);		
			var coaseguroCombo = cotizacionCoberturaGrid.getCustomCombo(rowId,14);
			var deducibleCombo = cotizacionCoberturaGrid.getCustomCombo(rowId,16);			
			
			getCoasegurosDeducibles("C", idToCobertura.getValue(),coaseguroCombo);				
			getCoasegurosDeducibles("D", idToCobertura.getValue(),deducibleCombo);		

	
		}
		if(obligatoriedad.getValue() === '3'){
			claveContrato.setDisabled(true);
		}
		if (claveContrato.getValue() === '0'){
			sumaAsegurada.setDisabled(true);
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);			
		}
		
		return true;
	});	
	var tipoDeducible = cotizacionCoberturaGrid.getCombo(18); 
	var tipoLimiteDeducible = cotizacionCoberturaGrid.getCombo(21);

	getCoasegurosDeducibles("TD", idToCotizacion,tipoDeducible);	
	getCoasegurosDeducibles("LD", idToCotizacion,tipoLimiteDeducible);

	cotizacionCoberturaGrid.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacion/cobertura/mostrarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	cotizacionCoberturaGrid.load(cotizacionCoberturaPath,null, 'json');	
	/*Descomentar para mostrar agrupacion de coberturas
	cotizacionCoberturaGrid.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
	cotizacionCoberturaGrid.customGroupFormat=function(name,count){
		var titulo;
		if(name == "0") {
			titulo = "Coberturas no Contratadas";
		} else {
			titulo = "Coberturas Contratadas";
		}
        return titulo + " (" + count + ")";
    }*/
	coberturaProcessor = new dataProcessor("/MidasWeb/cotizacion/cobertura/guardarCobertura.do");
	
	coberturaProcessor.enableDataNames(true);
	coberturaProcessor.setTransactionMode("POST");
	coberturaProcessor.setUpdateMode("off");
	coberturaProcessor.setVerificator(10,mayorA0);
	coberturaProcessor.setVerificator(14,mayorA0);
	coberturaProcessor.setVerificator(16,mayorA0);
	coberturaProcessor.attachEvent("onAfterUpdateFinish", function(){
		
		if(coberturaProcessor.getSyncState()){
			creaArbolCotizacion(idToCotizacion);
			mostrarCoberturasPorSeccion(idToCotizacion,numeroInciso,idToSeccion);
			hideIndicator();
		}
		return true;
	});
	coberturaProcessor.attachEvent("onBeforeDataSending",function(id){
		showIndicatorSimple();
		return true;
	});
	coberturaProcessor.init(cotizacionCoberturaGrid);	
}

/*
 * Grid de coberturas para endoso
 */

function mostrarCoberturasPorSeccionEndoso(idToCotizacion,numeroInciso,idToSeccion,tipoEndoso){

	var div = $('cotizacionCoberturasGrid');
	if(div == null) {
		initAutorizacionCoberturaGrid(idToCotizacion,numeroInciso,idToSeccion);
		return true;
	}	
	cotizacionCoberturaGrid = new dhtmlXGridObject("cotizacionCoberturasGrid");
	cotizacionCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionCoberturaGrid.setEditable(true);
	cotizacionCoberturaGrid.setSkin("light");
	cotizacionCoberturaGrid.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible,A/R/D,,,");
	cotizacionCoberturaGrid.setInitWidths("0,0,0,0,0,0,0,0,200,80,150,50,100,80,80,30,80,30,60,40,40,60,60,0,0,0");
	cotizacionCoberturaGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,edn,ro,co,img,co,img,coro,edn,edn,coro,img,ro,ro,ro");
	cotizacionCoberturaGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,left,left,left,left,center,center,center");
	cotizacionCoberturaGrid.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int,int");
	cotizacionCoberturaGrid.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
	cotizacionCoberturaGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,tipoDeducible,minimo,maximo,tipoLimiteDeducible,img3,desglosaRiesgo,claveSubIncisos,claveIgualacion");		
	cotizacionCoberturaGrid.setColumnHidden(0,true);
	cotizacionCoberturaGrid.setColumnHidden(1,true);
	cotizacionCoberturaGrid.setColumnHidden(2,true);
	cotizacionCoberturaGrid.setColumnHidden(3,true);
	cotizacionCoberturaGrid.setColumnHidden(4,true);
	cotizacionCoberturaGrid.setColumnHidden(5,true);
	cotizacionCoberturaGrid.setColumnHidden(6,true);
	cotizacionCoberturaGrid.setColumnHidden(7,true);
	cotizacionCoberturaGrid.setColumnHidden(23,true);
	cotizacionCoberturaGrid.setColumnHidden(24,true);
	cotizacionCoberturaGrid.setColumnHidden(25,true);	
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",10);
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",12);
	cotizacionCoberturaGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = cotizacionCoberturaGrid.cellById(rId, 4);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 9) {
			var existeObligatoriaParcialContratada = false;
			cotizacionCoberturaGrid.forEachRow(function(id){
				var obligatoriedadCurrentRow = cotizacionCoberturaGrid.cellById(id, 4);
				var claveContrato = cotizacionCoberturaGrid.cellById(id, 9);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Cobertura no puede ser descontratada.\nDebe contratar al menos una Cobertura Obligatoria Parcial.");
				return false;
			}
		}
		if(stage == 2 && (cInd == 14 || cInd == 16) ) {
			var coaseguro = cotizacionCoberturaGrid.cellById(rId, cInd);
			var combo = cotizacionCoberturaGrid.getCustomCombo(rId, cInd);
			combo.put(coaseguro.getValue(), coaseguro.getValue() + '%');
			combo.save();
		}
		if(stage == 2 && cInd == 9 ){
			seleccionarCobertura(stage,rId,cInd,nValue,oValue);
		}
		return true;
	});	
	cotizacionCoberturaGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		cotizacionCoberturaGrid.cellById(rowId, 12).setDisabled(true);
		var tipoSumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 5);
		var desglosaRiesgo = cotizacionCoberturaGrid.cellById(rowId, 23);
		var requiereSubIncisos = cotizacionCoberturaGrid.cellById(rowId, 24);
		var obligatoriedad = cotizacionCoberturaGrid.cellById(rowId, 4);
		var sumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 10);
		var claveContrato = cotizacionCoberturaGrid.cellById(rowId, 9);
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 14);
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 16);
		var cellARD = cotizacionCoberturaGrid.cellById(rowId, 22);
		var claveIgualacion = cotizacionCoberturaGrid.cellById(rowId, 25);
		
		if(claveContrato.getValue()=='1'){
			var idToCotizacion =  cotizacionCoberturaGrid.cellById(rowId, 0).getValue();
			var numeroInciso =  cotizacionCoberturaGrid.cellById(rowId, 1).getValue();
			var idToSeccion =  cotizacionCoberturaGrid.cellById(rowId, 2).getValue();
			var idToCobertura =  cotizacionCoberturaGrid.cellById(rowId, 3).getValue();
			if(claveIgualacion.getValue() == "1") {
				var cadena = "javascript: sendRequest(null,&#39;/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion="+idToCotizacion;
				cadena = cadena +"&numeroInciso="+numeroInciso+"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura;
				cadena = cadena +"&#39;,&#39;configuracion_detalle&#39;,null);";
				cellARD.setValue("/MidasWeb/img/details.gif^Aumentos/Recargos/Descuentos^"+cadena+"^_self");
			} else {
				cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
				cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			}
		}else{
			cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");			
			//cotizacionCoberturaGrid.setCellExcellType(rowId,18,"ro");
			//cotizacionCoberturaGrid.setCellExcellType(rowId,21,"ro");
			
			//Agregado por CesarMorales..
			if(tipoSumaAsegurada.getValue()=='1' || tipoSumaAsegurada.getValue()=='3'){
				cotizacionCoberturaGrid.cellById(rowId, 10).setValue('0.00');
			}
			/////
			cotizacionCoberturaGrid.cellById(rowId, 11).setValue('0.0000');
			cotizacionCoberturaGrid.cellById(rowId, 12).setValue('0.0');
			cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 17).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 15).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 18).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 21).setValue('');

		}

		if(tipoSumaAsegurada.getValue() == '2' || desglosaRiesgo.getValue() == '1' || requiereSubIncisos.getValue() == '1'){
			sumaAsegurada.setDisabled(true);
		}else{			
			cotizacionCoberturaGrid.setCellExcellType(rowId,10,"edn");		
		}
		
		if(desglosaRiesgo.getValue() === '1'){				
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 18).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 19).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 20).setDisabled(true);
			cotizacionCoberturaGrid.cellById(rowId, 21).setDisabled(true);
		}else{		
			var idToCobertura = cotizacionCoberturaGrid.cellById(rowId,3);		
			var coaseguroCombo = cotizacionCoberturaGrid.getCustomCombo(rowId,14);
			var deducibleCombo = cotizacionCoberturaGrid.getCustomCombo(rowId,16);			
			
			getCoasegurosDeducibles("C", idToCobertura.getValue(),coaseguroCombo);				
			getCoasegurosDeducibles("D", idToCobertura.getValue(),deducibleCombo);		

	
		}
		if(obligatoriedad.getValue() === '3'){
			claveContrato.setDisabled(true);
		}
		if (claveContrato.getValue() === '0'){
			sumaAsegurada.setDisabled(true);
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);			
		}
		
		return true;
	});	
	var tipoDeducible = cotizacionCoberturaGrid.getCombo(18); 
	var tipoLimiteDeducible = cotizacionCoberturaGrid.getCombo(21);

	getCoasegurosDeducibles("TD", idToCotizacion,tipoDeducible);	
	getCoasegurosDeducibles("LD", idToCotizacion,tipoLimiteDeducible);

	cotizacionCoberturaGrid.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacion/endoso/cobertura/mostrarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	cotizacionCoberturaGrid.load(cotizacionCoberturaPath,null, 'json');	
	/*
	cotizacionCoberturaGrid.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
	cotizacionCoberturaGrid.customGroupFormat=function(name,count){
		var titulo;
		if(name == "0") {
			titulo = "Coberturas no Contratadas";
		} else {
			titulo = "Coberturas Contratadas";
		}
        return titulo + " (" + count + ")";
    }*/
	coberturaProcessor = new dataProcessor("/MidasWeb/cotizacion/cobertura/guardarCobertura.do");
	
	coberturaProcessor.enableDataNames(true);
	coberturaProcessor.setTransactionMode("POST");
	coberturaProcessor.setUpdateMode("off");
	coberturaProcessor.setVerificator(10,mayorA0);
	coberturaProcessor.setVerificator(14,mayorA0);
	coberturaProcessor.setVerificator(16,mayorA0);
	coberturaProcessor.attachEvent("onAfterUpdateFinish", function(){
		
		if(coberturaProcessor.getSyncState()){
			creaArbolCotizacionEndoso(idToCotizacion,tipoEndoso);
			mostrarCoberturasPorSeccionEndoso(idToCotizacion,numeroInciso,idToSeccion, tipoEndoso)
			hideIndicator();			
		}
		return true;
	});
	coberturaProcessor.attachEvent("onBeforeDataSending",function(id){
		showIndicatorSimple();
		return true;
	});
	coberturaProcessor.init(cotizacionCoberturaGrid);	
}

function seleccionarCobertura(stage,rowId,cellIndex,newValue,oldValue) {
	if(stage==0)
		return true;				
	if (stage == 1){		
		col = cotizacionCoberturaGrid.getColIndexById('claveContrato'); 
		if (col==cellIndex){
			var estanSeleccionadasRequeridas = true;
			var estanSeleccionadasExcluidas = false;
			var indexRequeridas = cotizacionCoberturaGrid.getColIndexById('listaCoberturaRequeridas');
			var indexExcluidas = cotizacionCoberturaGrid.getColIndexById('listaCoberturaExcluidas');
			var arrayRequeridas = cotizacionCoberturaGrid.cellById(rowId,indexRequeridas).getValue();
			var arrayExcluidas = cotizacionCoberturaGrid.cellById(rowId,indexExcluidas).getValue();
			var requeridas = arrayRequeridas.split('_');
			var excluidas = arrayExcluidas.split('_');

			for (var i = 0; i < requeridas.length && arrayRequeridas!='' && cotizacionCoberturaGrid.cells(rowId,cellIndex).isChecked(); i++){
				if (cotizacionCoberturaGrid.getRowIndex(requeridas[i])>-1 && !cotizacionCoberturaGrid.cellById(requeridas[i],cellIndex).isChecked()){
					estanSeleccionadasRequeridas = false;
				}
			}
			
			if (!estanSeleccionadasRequeridas)	{
				coberturaProcessor.setUpdated(rowId,false,"update");	
				cotizacionCoberturaGrid.cells(rowId,cellIndex).setValue(oldValue);
				return false;
			}
			
			for (var i = 0; i < excluidas.length && arrayExcluidas!='' && cotizacionCoberturaGrid.cells(rowId,cellIndex).isChecked(); i++){
				if (cotizacionCoberturaGrid.getRowIndex(excluidas[i])>-1 && cotizacionCoberturaGrid.cellById(excluidas[i],cellIndex).isChecked()){
					estanSeleccionadasExcluidas = true;
				}
			}
			
			if (estanSeleccionadasExcluidas)	{
				alert('No se puede seleccionar porque esta seleccionada una cobertura que la excluye');
				cotizacionCoberturaGrid.cells(rowId,cellIndex).setValue(oldValue);
				return false;
			}
/*
			var idToCotizacion = cotizacionCoberturaGrid.cells(rowId,cotizacionCoberturaGrid.getColIndexById('idToCotizacion')).getValue();
			var numeroInciso = cotizacionCoberturaGrid.cells(rowId,cotizacionCoberturaGrid.getColIndexById('numeroInciso')).getValue();
			var idToSeccion = cotizacionCoberturaGrid.cells(rowId,cotizacionCoberturaGrid.getColIndexById('idToSeccion')).getValue();
			var idElement = idToCotizacion + '_' + numeroInciso + '_' + idToSeccion;
			
			if(cotizacionCoberturaGrid.cells(rowId,cellIndex).isChecked()){
				processor.sendData(rowId);
				processor.setUpdated(rowId,false,"update");
				creaArbolOrdenesDeTrabajo(idToCotizacion);
				ordenTrabajoTreeAbreElementById = idElement;
			}else{
				processor.sendData(rowId);
				processor.setUpdated(rowId,true,"update");
				creaArbolOrdenesDeTrabajo(idToCotizacion);
				ordenTrabajoTreeAbreElementById = idElement;
			}
*/			
		}
	}		
/*	
	col = cotizacionCoberturaGrid.getColIndexById('valorSumaAsegurada');
	if (stage==2 && col==cellIndex){
		var valor = cotizacionCoberturaGrid.cellById(rowId,cellIndex).getValue();
		if ( valor>=0 ){
			processor.sendData(rowId);
			processor.setUpdated(rowId,false,"update");
		}else{
			return false;
		}
	}
*/	
	return true;
}

/*
 * Grid de riesgos
 */
var cotizacionRiesgoGrid;
var cotizacionRiesgoPath;
var riesgoProcessor;
function initCotizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura) {
	cotizacionRiesgoPath = '/MidasWeb/cotizacion/riesgo/listarRiesgos.do';
	cotizacionRiesgoPath += '?idToCotizacion='+idToCotizacion;
	cotizacionRiesgoPath += '&numeroInciso='+numeroInciso;
	cotizacionRiesgoPath += '&idToSeccion='+idToSeccion;
	cotizacionRiesgoPath += '&idToCobertura='+idToCobertura;

	var div = $('cotizacionRiesgosGrid');
	if(div == null) {
		initAutorizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura);
		return true;
	}
	cotizacionRiesgoGrid = new dhtmlXGridObject("cotizacionRiesgosGrid");
	cotizacionRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionRiesgoGrid.setSkin("light");
	cotizacionRiesgoGrid.setHeader("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"Riesgo,Contratado,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible");
	cotizacionRiesgoGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"riesgo,contratado,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,claveSubIncisos," +
			"tipoDeducible,minimo,maximo,tipoLimiteDeducible");
	cotizacionRiesgoGrid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*");
	cotizacionRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,edn,ro,edn,ro,co,img,co,img,ro,coro,edn,edn,coro");
	cotizacionRiesgoGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,center,left,center,center,left,left,left,left");
	cotizacionRiesgoGrid.setColSorting("na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na");
	cotizacionRiesgoGrid.enableResizing("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	cotizacionRiesgoGrid.setColumnHidden(0, true);
	cotizacionRiesgoGrid.setColumnHidden(1, true);
	cotizacionRiesgoGrid.setColumnHidden(2, true);
	cotizacionRiesgoGrid.setColumnHidden(3, true);
	cotizacionRiesgoGrid.setColumnHidden(4, true);
	cotizacionRiesgoGrid.setColumnHidden(5, true);
	cotizacionRiesgoGrid.setColumnHidden(6, true);
	cotizacionRiesgoGrid.setColumnHidden(7, true);
	cotizacionRiesgoGrid.setColumnHidden(18, true);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",10);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",12);	
	var combo = cotizacionRiesgoGrid.getCombo(14);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(16);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(19);
	getCoasegurosDeducibles("TD", 0, combo);
	combo = cotizacionRiesgoGrid.getCombo(22);
	getCoasegurosDeducibles("LD", 0, combo);

	cotizacionRiesgoGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		if(stage == 2 && cInd == 14) {
			var coaseguro = cotizacionRiesgoGrid.cellById(rId, cInd);
			//coaseguro.setAttribute("title", coaseguro.getValue() + "%");
			var combo = cotizacionRiesgoGrid.getCustomCombo(rId, cInd);
			combo.put(coaseguro.getValue(), coaseguro.getValue() + '%');
			combo.save();
		}
		return true;
	});
	cotizacionRiesgoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		inicializaRowRiesgo(rowId, rowObj);
		return true;
	});
	cotizacionRiesgoGrid.enableLightMouseNavigation(true);
	cotizacionRiesgoGrid.init();
	cotizacionRiesgoGrid.load(cotizacionRiesgoPath, null, 'json');
	/*cotizacionRiesgoGrid.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan"]);
	cotizacionRiesgoGrid.customGroupFormat=function(name,count){
		var titulo;
		if(name == "0") {
			titulo = "Riesgos no Contratados";
		} else {
			titulo = "Riesgos Contratados";
		}
        return titulo + " (" + count + ")";
    }*/
	riesgoProcessor = new dataProcessor("/MidasWeb/cotizacion/riesgo/guardarRiesgo.do");
	riesgoProcessor.enableDataNames(true);
	riesgoProcessor.setTransactionMode("POST");
	riesgoProcessor.setUpdateMode("off");
	riesgoProcessor.setVerificator(10,greater_0);
	riesgoProcessor.setVerificator(14,greater0);
	riesgoProcessor.setVerificator(16,greater0);
	riesgoProcessor.attachEvent("onAfterUpdateFinish",function(){
		if(riesgoProcessor.getSyncState()) {
			creaArbolCotizacion(idToCotizacion);
			initCotizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura);
			hideIndicator();
		}
		return true;
	});
	riesgoProcessor.attachEvent("onBeforeDataSending",function(id){
		showIndicatorSimple();
		return true;
	});
	/*riesgoProcessor.attachEvent("onAfterUpdate",function(sid,action,tid,xml_node){
        initCotizacionRiesgoGrid(cotizacionRiesgoGrid.cellById(sid,0).getValue(),cotizacionRiesgoGrid.cellById(sid, 1).getValue(),cotizacionRiesgoGrid.cellById(sid, 2).getValue(),cotizacionRiesgoGrid.cellById(sid, 3).getValue()) 
	});*/
	riesgoProcessor.init(cotizacionRiesgoGrid);
}
function initCotizacionEndosoRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura,tipoEndoso) {
	cotizacionRiesgoPath = '/MidasWeb/cotizacion/riesgo/listarRiesgos.do';
	cotizacionRiesgoPath += '?idToCotizacion='+idToCotizacion;
	cotizacionRiesgoPath += '&numeroInciso='+numeroInciso;
	cotizacionRiesgoPath += '&idToSeccion='+idToSeccion;
	cotizacionRiesgoPath += '&idToCobertura='+idToCobertura;

	var div = $('cotizacionRiesgosGrid');
	if(div == null) {
		initAutorizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura);
		return true;
	}
	cotizacionRiesgoGrid = new dhtmlXGridObject("cotizacionRiesgosGrid");
	cotizacionRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionRiesgoGrid.setSkin("light");
	cotizacionRiesgoGrid.setHeader("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"Riesgo,Contratado,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible");
	cotizacionRiesgoGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"riesgo,contratado,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,claveSubIncisos," +
			"tipoDeducible,minimo,maximo,tipoLimiteDeducible");
	cotizacionRiesgoGrid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*");
	cotizacionRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,edn,ro,edn,ro,co,img,co,img,ro,coro,edn,edn,coro");
	cotizacionRiesgoGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,center,left,center,center,left,left,left,left");
	cotizacionRiesgoGrid.setColSorting("na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na");
	cotizacionRiesgoGrid.enableResizing("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	cotizacionRiesgoGrid.setColumnHidden(0, true);
	cotizacionRiesgoGrid.setColumnHidden(1, true);
	cotizacionRiesgoGrid.setColumnHidden(2, true);
	cotizacionRiesgoGrid.setColumnHidden(3, true);
	cotizacionRiesgoGrid.setColumnHidden(4, true);
	cotizacionRiesgoGrid.setColumnHidden(5, true);
	cotizacionRiesgoGrid.setColumnHidden(6, true);
	cotizacionRiesgoGrid.setColumnHidden(7, true);
	cotizacionRiesgoGrid.setColumnHidden(18, true);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",10);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",12);	
	var combo = cotizacionRiesgoGrid.getCombo(14);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(16);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(19);
	getCoasegurosDeducibles("TD", 0, combo);
	combo = cotizacionRiesgoGrid.getCombo(22);
	getCoasegurosDeducibles("LD", 0, combo);

	cotizacionRiesgoGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		if(stage == 2 && cInd == 14) {
			var coaseguro = cotizacionRiesgoGrid.cellById(rId, cInd);
			//coaseguro.setAttribute("title", coaseguro.getValue() + "%");
			var combo = cotizacionRiesgoGrid.getCustomCombo(rId, cInd);
			combo.put(coaseguro.getValue(), coaseguro.getValue() + '%');
			combo.save();
		}
		return true;
	});
	cotizacionRiesgoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		inicializaRowRiesgo(rowId, rowObj);
		return true;
	});
	cotizacionRiesgoGrid.enableLightMouseNavigation(true);
	cotizacionRiesgoGrid.init();
	cotizacionRiesgoGrid.load(cotizacionRiesgoPath, null, 'json');

	riesgoProcessor = new dataProcessor("/MidasWeb/cotizacion/riesgo/guardarRiesgo.do");
	riesgoProcessor.enableDataNames(true);
	riesgoProcessor.setTransactionMode("POST");
	riesgoProcessor.setUpdateMode("off");
	riesgoProcessor.setVerificator(10,greater_0);
	riesgoProcessor.setVerificator(14,greater0);
	riesgoProcessor.setVerificator(16,greater0);
	riesgoProcessor.attachEvent("onAfterUpdateFinish",function(){
		if(riesgoProcessor.getSyncState()) {
			creaArbolCotizacionEndoso(idToCotizacion,tipoEndoso);
			initCotizacionEndosoRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura);
			hideIndicator();
		}
		return true;
	});
	riesgoProcessor.attachEvent("onBeforeDataSending",function(id){
		showIndicatorSimple();
		return true;
	});
	/*riesgoProcessor.attachEvent("onAfterUpdate",function(sid,action,tid,xml_node){
        initCotizacionRiesgoGrid(cotizacionRiesgoGrid.cellById(sid,0).getValue(),cotizacionRiesgoGrid.cellById(sid, 1).getValue(),cotizacionRiesgoGrid.cellById(sid, 2).getValue(),cotizacionRiesgoGrid.cellById(sid, 3).getValue()) 
	});*/
	riesgoProcessor.init(cotizacionRiesgoGrid);
}

function greater_0(value){
	return value > 0;
}
function greater0(value){
	return value >= 0;
}

function mayorA0(value){
	if (value == 'Amparada' || value == 'Sublimite'){
		return true;
	}else if (value == 'Segun Riesgo'){
		return true;
	}else{
		return value >= 0;
	}
}

function inicializaRowRiesgo(rowId, rowObj) {
	var primaNeta = cotizacionRiesgoGrid.cellById(rowId, 12);
	primaNeta.setDisabled(true);
	var claveObligatoriedad = cotizacionRiesgoGrid.cellById(rowId, 6);
	var contratado = cotizacionRiesgoGrid.cellById(rowId, 9);
	var claveSubIncisos = cotizacionRiesgoGrid.cellById(rowId, 18);
	if(claveObligatoriedad.getValue() == '0' || claveObligatoriedad.getValue() == '1') {
		contratado.setDisabled(false);
	} else if(claveObligatoriedad.getValue() == '3') {
		contratado.setDisabled(true);
		contratado.setChecked(true);
	}
	var claveTipoSumaAsegurada = cotizacionRiesgoGrid.cellById(rowId, 7);
	var sumaAsegurada = cotizacionRiesgoGrid.cellById(rowId, 10);
	if(claveTipoSumaAsegurada.getValue() == 2 || claveSubIncisos.getValue() == '1') {
		sumaAsegurada.setDisabled(true);
	}

	var idToSeccion = cotizacionRiesgoGrid.cellById(rowId,2);;
	var idToCobertura = cotizacionRiesgoGrid.cellById(rowId,3);
	var idToRiesgo = cotizacionRiesgoGrid.cellById(rowId,4);
	var coaseguroCombo = cotizacionRiesgoGrid.getCustomCombo(rowId, 14);
	var deducibleCombo = cotizacionRiesgoGrid.getCustomCombo(rowId, 16);

	getCoasegurosDeduciblesRiesgo("C", idToSeccion.getValue(), idToCobertura.getValue(), idToRiesgo.getValue(), coaseguroCombo);		
	getCoasegurosDeduciblesRiesgo("D", idToSeccion.getValue(), idToCobertura.getValue(), idToRiesgo.getValue(), deducibleCombo);
}

function getCoasegurosDeducibles(tipo, idToCobertura,combo){
	new Ajax.Request("/MidasWeb/getValoresCoaseguroDeducible.do", {
		method : "post",
		asynchronous : false,
		parameters :"tipo="+ tipo + "&id=" + idToCobertura ,
		onSuccess : function(transport) {
			cargaComboGrid(combo,transport.responseXML);
		} // End of onSuccess
	});			
}
function getCoasegurosDeduciblesRiesgo(tipo,idToSeccion,idToCobertura,idToRiesgo,combo){
	new Ajax.Request("/MidasWeb/cotizacion/riesgo/getValoresCoaseguroDeducible.do", {
		method : "post",
		asynchronous : false,
		parameters :"tipo="+ tipo + "&idToSeccion=" + idToSeccion + "&idToCobertura=" + idToCobertura + "&idToRiesgo=" + idToRiesgo ,
		onSuccess : function(transport) {
			cargaComboGrid(combo,transport.responseXML);
		} // End of onSuccess
	});			
}

function cargaComboGrid(combo,doc){
	var items = doc.getElementsByTagName("item");
	for ( var x = 0; x < items.length; x++) {
		var item = items[x];
		var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		combo.put(value,text);
	} // End of for	
	combo.save();
}
var riesgoAutorizadoProcessor;
var riesgoRechazadoProcessor;
function initAutorizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura) {
	cotizacionRiesgoPath = '/MidasWeb/cotizacion/riesgo/listarAutorizacionRiesgos.do';
	cotizacionRiesgoPath += '?idToCotizacion='+idToCotizacion;
	cotizacionRiesgoPath += '&numeroInciso='+numeroInciso;
	cotizacionRiesgoPath += '&idToSeccion='+idToSeccion;
	cotizacionRiesgoPath += '&idToCobertura='+idToCobertura;

	cotizacionRiesgoGrid = new dhtmlXGridObject("autorizacionCotizacionRiesgosGrid");
	cotizacionRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionRiesgoGrid.setSkin("light");
	cotizacionRiesgoGrid.setHeader("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo," +
			"Riesgo,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,");
	cotizacionRiesgoGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo," +
			"riesgo,sumaAsegurada,cuota,primaNeta,comision,coaseguro,coaseguroAutorizado,deducible,deducibleAutorizado");
	cotizacionRiesgoGrid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*,*,*,*");
	cotizacionRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,edn,ro,edn,ro,ro,ro,ro,ro");
	cotizacionRiesgoGrid.setColAlign("center,center,center,center,center,center,left,left,left,left,left,left,center,left,center");
	cotizacionRiesgoGrid.setColSorting("na,na,na,na,na,na,na,na,na,na,na,na,na,na,na");
	cotizacionRiesgoGrid.enableResizing("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	cotizacionRiesgoGrid.setColumnHidden(0, true);
	cotizacionRiesgoGrid.setColumnHidden(1, true);
	cotizacionRiesgoGrid.setColumnHidden(2, true);
	cotizacionRiesgoGrid.setColumnHidden(3, true);
	cotizacionRiesgoGrid.setColumnHidden(4, true);
	cotizacionRiesgoGrid.setColumnHidden(5, true);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",7);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",9);		
	cotizacionRiesgoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		cotizacionRiesgoGrid.cellById(rowId, 7).setDisabled(true);
		cotizacionRiesgoGrid.cellById(rowId, 9).setDisabled(true);
		var coaseguroAutorizado = cotizacionRiesgoGrid.cellById(rowId, 12);
		if(coaseguroAutorizado.getValue() == 1) {
			coaseguroAutorizado.setValue('')
			cotizacionRiesgoGrid.setCellExcellType(rowId,12,"ch");
			coaseguroAutorizado.setDisabled(false);
			coaseguroAutorizado.setChecked(false);
		} else {
			coaseguroAutorizado.setValue('')
		}
		var deducibleAutorizado = cotizacionRiesgoGrid.cellById(rowId, 14);
		if(deducibleAutorizado.getValue() == 1) {
			deducibleAutorizado.setValue('')
			cotizacionRiesgoGrid.setCellExcellType(rowId,14,"ch");
			deducibleAutorizado.setDisabled(false);
			deducibleAutorizado.setChecked(false);
		} else {
			deducibleAutorizado.setValue('')
		}
		return true;
	});
	cotizacionRiesgoGrid.enableLightMouseNavigation(true);
	cotizacionRiesgoGrid.init();
	cotizacionRiesgoGrid.load(cotizacionRiesgoPath, null, 'json');

	riesgoAutorizadoProcessor = new dataProcessor("/MidasWeb/cotizacion/riesgo/guardarRiesgoAutorizado.do");
	riesgoAutorizadoProcessor.enableDataNames(true);
	riesgoAutorizadoProcessor.setTransactionMode("POST");
	riesgoAutorizadoProcessor.setUpdateMode("off");
	riesgoAutorizadoProcessor.defineAction("coaseguroActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionRiesgoGrid.setCellExcellType(rowId,12,"ro");
		var coaseguro = cotizacionRiesgoGrid.cellById(rowId, 12);
		coaseguro.setValue('');
		if(extra == "deducibleActualizado") {
			cotizacionRiesgoGrid.setCellExcellType(rowId,14,"ro");
			var deducible = cotizacionRiesgoGrid.cellById(rowId, 14);
			deducible.setValue('');
		}
		//riesgoAutorizadoProcessor.setUpdated(rowId, false);
		return true;
	});
	riesgoAutorizadoProcessor.defineAction("deducibleActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionRiesgoGrid.setCellExcellType(rowId,14,"ro");
		var deducible = cotizacionRiesgoGrid.cellById(rowId, 14);
		deducible.setValue('');
		if(extra == "coaseguroActualizado") {
			cotizacionRiesgoGrid.setCellExcellType(rowId,12,"ro");
			var coaseguro = cotizacionRiesgoGrid.cellById(rowId, 12);
			coaseguro.setValue('');
		}
		return true;
	});
	riesgoAutorizadoProcessor.init(cotizacionRiesgoGrid);

	riesgoRechazadoProcessor = new dataProcessor("/MidasWeb/cotizacion/riesgo/guardarRiesgoRechazado.do");
	riesgoRechazadoProcessor.enableDataNames(true);
	riesgoRechazadoProcessor.setTransactionMode("POST");
	riesgoRechazadoProcessor.setUpdateMode("off");
	riesgoRechazadoProcessor.defineAction("coaseguroActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionRiesgoGrid.setCellExcellType(rowId,12,"ro");
		var coaseguro = cotizacionRiesgoGrid.cellById(rowId, 12);
		coaseguro.setValue('');
		if(extra == "deducibleActualizado") {
			cotizacionRiesgoGrid.setCellExcellType(rowId,14,"ro");
			var deducible = cotizacionRiesgoGrid.cellById(rowId, 14);
			deducible.setValue('');
		}
		return true;
	});
	riesgoRechazadoProcessor.defineAction("deducibleActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionRiesgoGrid.setCellExcellType(rowId,14,"ro");
		var deducible = cotizacionRiesgoGrid.cellById(rowId, 14);
		deducible.setValue('');
		if(extra == "coaseguroActualizado") {
			cotizacionRiesgoGrid.setCellExcellType(rowId,12,"ro");
			var coaseguro = cotizacionRiesgoGrid.cellById(rowId, 12);
			coaseguro.setValue('');
		}
		return true;
	});
	riesgoRechazadoProcessor.init(cotizacionRiesgoGrid);
}
//############ funciones para ARDS #########################//

function regresarACoberturas(){
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	var idToSeccion = $('idToSeccion').value;
	var claveTipoEndoso = $('claveTipoEndoso');
	if(claveTipoEndoso != null && claveTipoEndoso.value != ''){
		sendRequest(null,'/MidasWeb/cotizacion/endoso/cobertura/listarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion ,'configuracion_detalle', 'mostrarCoberturasPorSeccionEndoso('+idToCotizacion+','+numeroInciso+','+idToSeccion+','+claveTipoEndoso.value+')');		
	}else{
		sendRequest(null,'/MidasWeb/cotizacion/cobertura/listarCoberturas.do?idToCotizacion='+ idToCotizacion +'&numeroInciso='+ numeroInciso + '&idToSeccion='+ idToSeccion, 'configuracion_detalle','mostrarCoberturasPorSeccion('+idToCotizacion+','+numeroInciso+','+idToSeccion+')');		
	}	
	
}

function setIcon(index, type){
	var target = 'autorizacion'+type+'['+index+']';
	var valor = 'valor'+type+'['+index+']';
	
	var divIcon = $(target);
	var textValor = $(valor);
	var icon = "<a href=\"void(0);\"><img alt=\"Autorizaci\u00f3n Solicitada\" title=\"Autorizaci\u00f3n Solicitada\" src=\"/MidasWeb/img/ico_yel.gif\"></a>";
	
	if(textValor.value > 0 ){
		divIcon.innerHTML =  icon;
		saveARD(index, type, 1);
	}
}

function hideIcon(index, type){
	var target = 'autorizacion'+type+'['+index+']';		
	var divIcon = $(target);		
	divIcon.innerHTML =  "";
}

function saveARD(index, type, autorizacion){
	var checkId = 'checkbox'+type+'['+index+']';
	var checkObj = $(checkId);
	var valueId = 'valor'+type+'['+index+']';
	var valueObj = $(valueId);
	
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	var idToCobertura = $('idToCobertura').value;
	var idARD = "";
	if(type == "A") {
		idARD = $('idAumento[' + index + ']').value;
	}
	if(type == "R") {
		idARD = $('idRecargo[' + index + ']').value;
	}
	if(type == "D") {
		idARD = $('idDescuento[' + index + ']').value;
	}
	var claveAutorizacion = "";
	if(autorizacion == 1) {
		claveAutorizacion = 1;
	}
	if(checkObj.checked){
		valueObj.disabled = false;
		valueObj.readOnly = false;
		new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarARD.do", {
			method : "post",
			asynchronous : false,
			parameters : "valor=" + valueObj.value + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&contrato=1"+"&tipo="+type+"&idARD="+idARD+"&claveAutorizacion="+claveAutorizacion,
			onSuccess : function(transport) {
				//loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});		

	}else{
		valueObj.disabled = true;
		valueObj.readOnly = true;
		valueObj.value = "0.00";
		hideIcon(index, type);
		claveAutorizacion = 0;
		new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarARD.do", {
			method : "post",
			asynchronous : false,
			parameters : "valor=" + valueObj.value + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&contrato=0"+"&tipo="+type+"&idARD="+idARD+"&claveAutorizacion="+claveAutorizacion,
			onSuccess : function(transport) {
				//loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});		
	}	
}

function autorizarARD(){
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	var idToCobertura = $('idToCobertura').value;	
	var aumentosSize = $('aumentosSize').value;
	var recargosSize = $('recargosSize').value;
	var descuentosSize = $('descuentosSize').value;	
	var tipo;
	if (aumentosSize !== null){
		var checkA;
		tipo = 'A';
		for(var k=0; k<aumentosSize; k++){
			checkA = $("checkboxAutA[" + k + "]");
			if (checkA != null && checkA.checked){
				var idARD = $('idAumento[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarAutorizarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});					
			} 
		}
	}

	if (recargosSize !== null){
		var checkR;
		tipo = 'R';		
		for(var k=0; k<recargosSize; k++){
			checkR = $("checkboxAutR[" + k + "]");
			if (checkR != null && checkR.checked){
				var idARD = $('idRecargo[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarAutorizarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});	
			} 
		}
	}	

	if (descuentosSize !== null){
		var checkD;
		tipo = 'D';
		for(var k=0; k<descuentosSize; k++){
			checkD = $("checkboxAutD[" + k + "]");
			if (checkD != null && checkD.checked){
				var idARD = $('idDescuento[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarAutorizarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});	
			} 
		}
	}	
	
}
function rechazarARD(){
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	var idToCobertura = $('idToCobertura').value;	
	var aumentosSize = $('aumentosSize').value;
	var recargosSize = $('recargosSize').value;
	var descuentosSize = $('descuentosSize').value;	
	var tipo;
	if (aumentosSize !== null){
		var checkA;
		tipo = 'A';
		for(var k=0; k<aumentosSize; k++){
			checkA = $("checkboxAutA[" + k + "]");
			if (checkA != null && checkA.checked){
				var idARD = $('idAumento[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarRechazarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});					
			} 
		}
	}

	if (recargosSize !== null){
		var checkR;
		tipo = 'R';		
		for(var k=0; k<recargosSize; k++){
			checkR = $("checkboxAutR[" + k + "]");
			if (checkR != null && checkR.checked){
				var idARD = $('idRecargo[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarRechazarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});	
			} 
		}
	}	

	if (descuentosSize !== null){
		var checkD;
		tipo = 'D';
		for(var k=0; k<descuentosSize; k++){
			checkD = $("checkboxAutD[" + k + "]");
			if (checkD != null && checkD.checked){
				var idARD = $('idDescuento[' + k + ']').value;
				//Autorizar
				new Ajax.Request("/MidasWeb/cotizacion/cobertura/guardarRechazarARD.do", {
					method : "post",
					asynchronous : false,
					parameters : "tipo=" + tipo + "&idToCotizacion=" + idToCotizacion +"&numeroInciso="+numeroInciso +"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura+"&idARD="+idARD,
					onSuccess : function(transport) {
					} // End of onSuccess
				});	
			} 
		}
	}
}

//############## AUTORIZAR O RECHAZAR COASEGUROS Y DEDUCIBLES DE COBERTURAS ###########################

var coberturaAutorizadoProcessor;
var coberturaRechazadoProcessor;

function initAutorizacionCoberturaGrid(idToCotizacion,numeroInciso,idToSeccion){

	cotizacionCoberturaGrid = new dhtmlXGridObject("autorizacionCotizacionCoberturasGrid");
	cotizacionCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionCoberturaGrid.setEditable(true);
	cotizacionCoberturaGrid.setSkin("light");
	cotizacionCoberturaGrid.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,A/R/D,,");
	cotizacionCoberturaGrid.setInitWidths("0,0,0,0,0,0,0,0,200,80,150,50,100,80,80,30,80,30,60,0,0");
	cotizacionCoberturaGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,edn,ro,edn,ro,co,ro,co,ro,img,ro,ro");
	cotizacionCoberturaGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,center,center");
	cotizacionCoberturaGrid.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int");
	cotizacionCoberturaGrid.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,false,false");
	cotizacionCoberturaGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,coaseguroAutorizado,deducible,deducibleAutorizado,img3,desglosaRiesgo,claveSubIncisos");	
	cotizacionCoberturaGrid.setColumnHidden(0,true);
	cotizacionCoberturaGrid.setColumnHidden(1,true);
	cotizacionCoberturaGrid.setColumnHidden(2,true);
	cotizacionCoberturaGrid.setColumnHidden(3,true);
	cotizacionCoberturaGrid.setColumnHidden(4,true);
	cotizacionCoberturaGrid.setColumnHidden(5,true);
	cotizacionCoberturaGrid.setColumnHidden(6,true);
	cotizacionCoberturaGrid.setColumnHidden(7,true);
	cotizacionCoberturaGrid.setColumnHidden(18,true);
	cotizacionCoberturaGrid.setColumnHidden(19,true);
	cotizacionCoberturaGrid.setColumnHidden(20,true);	
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",10);
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",12);	
	cotizacionCoberturaGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		cotizacionCoberturaGrid.cellById(rowId, 12).setDisabled(true);
		
		var sumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 10);
		var claveContrato = cotizacionCoberturaGrid.cellById(rowId, 9);
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 14);
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 16);
		var coaseguroAutorizado = cotizacionCoberturaGrid.cellById(rowId, 15);
		var deducibleAutorizado = cotizacionCoberturaGrid.cellById(rowId, 17);
		
		sumaAsegurada.setDisabled(true);
		coaseguro.setDisabled(true);
		deducible.setDisabled(true);
		claveContrato.setDisabled(true);
		
		if(coaseguroAutorizado.getValue() == 1) {
			coaseguroAutorizado.setValue('')
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ch");
			coaseguroAutorizado.setDisabled(false);
			coaseguroAutorizado.setChecked(false);
		} else {
			coaseguroAutorizado.setValue('')
		}		

		if(deducibleAutorizado.getValue() == 1) {
			deducibleAutorizado.setValue('')
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ch");
			deducibleAutorizado.setDisabled(false);
			deducibleAutorizado.setChecked(false);
		} else {
			deducibleAutorizado.setValue('')
		}		
		return true;
	});	
	cotizacionCoberturaGrid.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacion/cobertura/listarAutorizacionCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	cotizacionCoberturaGrid.load(cotizacionCoberturaPath,null, 'json');	
	/*cotizacionCoberturaGrid.groupBy(9,["","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#title","#cspan","#cspan","#cspan","#stat_total","","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan","#cspan"]);
	cotizacionCoberturaGrid.customGroupFormat=function(name,count){
		var titulo;
		if(name == "0") {
			titulo = "Coberturas no Contratadas";
		} else {
			titulo = "Coberturas Contratadas";
		}
        return titulo + " (" + count + ")";
    }*/
	coberturaAutorizadoProcessor = new dataProcessor("/MidasWeb/cotizacion/cobertura/guardarCoberturaAutorizado.do");
	coberturaAutorizadoProcessor.enableDataNames(true);
	coberturaAutorizadoProcessor.setTransactionMode("POST");
	coberturaAutorizadoProcessor.setUpdateMode("off");
	coberturaAutorizadoProcessor.defineAction("coaseguroActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 15);
		coaseguro.setValue('');
		if(extra == "deducibleActualizado") {
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
			var deducible = cotizacionCoberturaGrid.cellById(rowId, 17);
			deducible.setValue('');
		}
		return true;
	});
	coberturaAutorizadoProcessor.defineAction("deducibleActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 17);
		deducible.setValue('');
		if(extra == "coaseguroActualizado") {
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");
			var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 15);
			coaseguro.setValue('');
		}
		return true;
	});
	coberturaAutorizadoProcessor.init(cotizacionCoberturaGrid);

	coberturaRechazadoProcessor = new dataProcessor("/MidasWeb/cotizacion/cobertura/guardarCoberturaRechazado.do");
	coberturaRechazadoProcessor.enableDataNames(true);
	coberturaRechazadoProcessor.setTransactionMode("POST");
	coberturaRechazadoProcessor.setUpdateMode("off");
	coberturaRechazadoProcessor.defineAction("coaseguroActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 15);
		coaseguro.setValue('');
		if(extra == "deducibleActualizado") {
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
			var deducible = cotizacionCoberturaGrid.cellById(rowId, 17);
			deducible.setValue('');
		}
		return true;
	});
	coberturaRechazadoProcessor.defineAction("deducibleActualizado", function(node){
		var rowId = node.getAttribute("sid");
		var extra = node.getAttribute("extra");
		cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 17);
		deducible.setValue('');
		if(extra == "coaseguroActualizado") {
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");
			var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 15);
			coaseguro.setValue('');
		}
		return true;
	});
	coberturaRechazadoProcessor.init(cotizacionCoberturaGrid);	

}
/**
 * Funcion usada para indicar al usuario si se eliminar�n los incisos de la cotizaci�n al cambiar la p�liza
 */ 
function validarCambioEnTipoPoliza(){
	var permiteCambiarPoliza = document.getElementById("permiteCambiarPoliza");
	if (permiteCambiarPoliza != null && permiteCambiarPoliza != undefined){
		permiteCambiarPoliza = permiteCambiarPoliza.value;
		if (permiteCambiarPoliza == 'true')
			return true;
		else
			if (confirm("Si cambia el tipo de p\u00f3liza se eliminar\u00e1n los incisos y comisiones de la Orden de trabajo.\n Desea continuar?") )
				return true;
			else{
				var selectTipoPoliza = document.getElementById("selectTipoPoliza");
				var polizaOriginal = document.getElementById("poliza").value;
				selectTipoPoliza.value = polizaOriginal;
				return false;
			}
	}
	return true;
}

function reporteResumenCotizacion(tipoReporte) {
	var idToCotizacion = document.getElementById('idToCotizacion');
	var url = '/MidasWeb/cotizacion/resumen/generarReporteResumenCotizacion.do?id='+idToCotizacion.value+"&tipoReporte="+tipoReporte;
	window.open(url, 'download');
}

function reporteLineasNegociacion(tipoReporte) {
	var fechaInicial = trim($('fechaInicial').value);
	var fechaFinal = trim($('fechaFinal').value);
	if (fechaInicial.length != 0)
		fechaInicial += ' ' +trim($('horaInicial').value);
	if (fechaFinal.length != 0)
		fechaFinal += ' ' + trim($('horaFinal').value); 
	var url = '/MidasWeb/contratos/linea/generarListaFiltradoLineaNegociacion.do?fi='+
	fechaInicial+'&ff='+fechaFinal+	
	'&estatus='+$('estatus').value+"&idTcRamo="+$('idTcRamo').value+"&tipoReporte="+tipoReporte;
	window.open(url, 'download');
}

function imprimirReporteBasesEmision(form){
	var fechaIncial = document.getElementById("fechaInicial").value;
	var fechaFinal = document.getElementById("fechaFinal").value;
	var idTcRamo = document.getElementById("idTcRamo").value;
	var idTcSubRamo = document.getElementById("idTcSubRamo").value;
	var idTcTipoReaseguro = document.getElementById("idTcTipoReaseguro").value;
	var nivelAgrupamiento = document.getElementById("nivelAgrupamiento").value;
	var location = "/MidasWeb/danios/reportes/reporteBasesEmision/generarReporteMovimientoEmision.do";
	location = location+"?nivelAgrupamiento="+nivelAgrupamiento;
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
		location = location+"&fechaInicio="+fechaIncial;
	if(fechaFinal != undefined && fechaFinal != null && fechaFinal != '' )
		location = location+"&fechaFin="+fechaFinal ;
	if(idTcRamo != undefined && idTcRamo != null && idTcRamo != '' )
		location = location+"&idTcRamo="+idTcRamo;
	if(idTcSubRamo != undefined && idTcSubRamo != null && idTcSubRamo != '' )
		location = location+"&idTcSubramo="+idTcSubRamo;
	if(idTcTipoReaseguro != undefined && idTcTipoReaseguro != null && idTcTipoReaseguro != '' )
		location = location+"&idTcTipoReaseguro="+idTcTipoReaseguro;
	window.open(location,"ReporteBasesEmision");
}

function mostrarImprimirReportePML(form){
	sendRequest(form,'/MidasWeb/danios/reportes/reportePML/validarDatosReportePML.do', 'contenido',"existenErrores('imprimirReportePML()')");
}

function imprimirReportePML(){
	var fechaCorte = document.getElementById("fecha").value;
	var tipoCambio = document.getElementById("tipoCambio").value;
	var tipoReporte = document.getElementById("tipoReporte").value;
	var claveTipoReporte = document.getElementById("comboClaveTipoReporte").value;
	var location = "/MidasWeb/danios/reportes/reportePML/generarReportePML.do?fechaCorte="+fechaCorte+"&tipoCambio="+tipoCambio+"&tipoReporte="+tipoReporte+"&claveTipoReporte="+claveTipoReporte;
	nextFunction = 'mostrarImprimirReportePMLEspecifico()';
	new Ajax.Request(location, {
				method : "post",
				asynchronous : false,
				parameters : null,
				onSuccess : function(transport) {
					procesarRespuestaXml(transport.responseXML, nextFunction);}
			});
	var obj = document.getElementById("divGenerarReporteEspecifico");
	obj.style.display = 'none';
}

function mostrarImprimirReportePMLEspecifico(){
	var obj = document.getElementById("divGenerarReporteEspecifico");
	obj.style.display = 'block';
	
	/*obj = document.getElementById("divGenerarReporte");
	obj.style.display = 'none';*/
}

function imprimirReportePMLEspecifico(tipoReporte){
	var comboClaveTipoRep = document.getElementById("comboClaveTipoReporte");
	var location = "/MidasWeb/danios/reportes/reportePML/obtenerReportePML.do?tipoReporte="+tipoReporte+"&claveTipoReporte="+comboClaveTipoRep.value;
	window.open(location,"ReportePML");
}

function imprimirCotizacion(idCotizacion) {
	var location ="/MidasWeb/danios/reportes/imprimirCotizacion.do?id=" + idCotizacion;
	window.open(location, "Cotizacion_COT" + idCotizacion);
}

function imprimirMemoriaCalculo(tipoReporte){
	var objSelectInciso = document.getElementById("numeroInciso");
	var numeroInciso = "-1";
	if(objSelectInciso != null && objSelectInciso != undefined){
		numeroInciso = objSelectInciso.value;
	}
	var idCotizacion = parent.document.getElementById("idToCotizacion").value;
	
	var location ="/MidasWeb/danios/reportes/imprimirMemoriaCalculoCotizacion.do?id=" + idCotizacion+
				"&numeroInciso="+numeroInciso+"&tipoReporte="+tipoReporte;
	window.open(location, "MemoriaCalculo_COT" + idCotizacion);
}

function imprimirEndosoPoliza(idPoliza,numeroEndoso) {
	var location ="/MidasWeb/danios/reportes/poliza/imprimirPoliza.do?id=" + idPoliza;
	if (numeroEndoso != null && numeroEndoso != undefined)
		location = location + "&numeroEndoso="+numeroEndoso;
	parent.window.open(location, "Poliza " + idPoliza+", Endoso "+numeroEndoso);
	parent.dhxWins.window('OrdenTrabajo').close();
}

function generarReporteEndososCancelables(){
	var fechaCorte = document.getElementById("fecha");
	if(fechaCorte != null){
		fechaCorte = fechaCorte.value;
		
		if(fechaCorte != ''){
			var location ="/MidasWeb/danios/reportes/imprimirEndososCancelables.do?fechaCorte=" + fechaCorte;
			window.open(location, "Endosos_Cancelables_Al" + fechaCorte);
		}
		else{
			parent.mostrarVentanaMensaje("10", "Seleccione una fecha de corte", null);
		}
		
	}
	sendRequest(form,'/MidasWeb/danios/reportes/reportePML/validarDatosReportePML.do', 'contenido',"existenErrores('imprimirReportePML()')");
}

function consultarEndosoPoliza(idToCotizacion, idToPoliza, numeroEndoso, tipoEndoso) {
	if(tipoEndoso == "3" || tipoEndoso == "1") {
		sendRequest(null,"/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do?id=" + idToCotizacion + "&consultaEndoso=true","contenido","creaArbolPolizaSoloLectura("+idToPoliza+","+numeroEndoso+",'consultaEndoso=true'); dhx_init_tabbars();");
	} else {
		sendRequest(null,'/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do?id=' + idToCotizacion,'contenido','creaArbolPolizaSoloLectura('+idToPoliza+','+numeroEndoso+'); dhx_init_tabbars();');
	}
	dhxWins.window('OrdenTrabajo').close();
}
function imprimirPoliza(idPoliza) {
	var location ="/MidasWeb/danios/reportes/poliza/imprimirPoliza.do?id=" + idPoliza;
	window.open(location, "Poliza" + idPoliza);
}
function mostrarResumenCotizacion(){
	sendRequest(null,'/MidasWeb/cotizacion/resumen/mostrarResumenCotizacion.do','contenido_resumenCotizacion', null);
}
function listarCotizaciones(){
	sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do', 'contenido',null);
}
function listarCotizacionesEndoso(){
	sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/listar.do', 'contenido',null);
}
function listarEmisiones(){
	sendRequest(null,'/MidasWeb/poliza/listar.do', 'contenido',null);
}
function listarOrdenesTrabajo(){
	sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajo.do', 'contenido',null);
}
function mostrarAutorizacionesCobertura(){
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	
	sendRequest(null,'/MidasWeb/cotizacion/cobertura/listarCoberturasAutorizar.do?idToCotizacion='+idToCotizacion +'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion ,'configuracion_detalle', 'mostrarCoberturasPorSeccion('+idToCotizacion+','+numeroInciso+','+idToSeccion+')');
}


function cambiarEstatusCotLiberada(idToCotizacion){
		parent.showIndicatorSimple();
		new Ajax.Request('/MidasWeb/cotizacion/cambiarEstatusEnProceso.do?idToCotizacion='+idToCotizacion, {
			method : "post",
			encoding : "UTF-8",
			onSuccess : function(transport) {
				parent.hideIndicator();
				document.cotizacionForm.idToCotizacion.value = idToCotizacion;
				procesarRespuestaXml(transport.responseXML, "listarCotizaciones();");
				}
				
		});
	
}
 function regresarListado(){
	 if($('claveEstatus').value>9){
		 	 sendRequest(null,'/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do', 'contenido','cerrarObjetosEdicionOT();');
		}else{
			sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajo.do','contenido','cerrarObjetosEdicionOT();');;
		}
 }

 
 function mostrarAdjuntarArchivoSistema() {
		if(dhxWins != null) {
			dhxWins.unload();
		}
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var acercaDeWindow = dhxWins.createWindow("adjuntarArchivosSistema", 34, 100, 440, 265);
		acercaDeWindow.setText("Adjuntar archivos");
		acercaDeWindow.button("minmax1").hide();
		acercaDeWindow.button("park").hide();
		acercaDeWindow.setModal(true);
		acercaDeWindow.center();
		acercaDeWindow.denyResize();
		acercaDeWindow.attachHTMLString("<div id='vault'></div>");
		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setServerHandlers("/MidasWeb/sistema/vault/fileUploadAction.do",
	            "/MidasWeb/sistema/vault/getInfoHandler.do",
	            "/MidasWeb/sistema/vault/getIdHandler.do");
	    
	    vault.onUploadComplete = function(files) {
	        var s="";
	        for (var i=0; i<files.length; i++) {
	            var file = files[i];
	            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
	        }
	      parent.dhxWins.window("adjuntarArchivosSistema").close();
	      parent.mostrarAcercaDeWindow();
	    };
	    //acercaDeWindow.attachEvent("onClose",  parent.mostrarAcercaDeWindow());
	    vault.create("vault");
 }
 
 function mostrarAdjuntarArchivoSolicitudSistemaWindow() {
		if(dhxWins != null) {
			dhxWins.unload();
		}
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var acercaDeWindow = dhxWins.createWindow("adjuntarArchivoSolicitudSistema", 34, 100, 440, 265);
		acercaDeWindow.setText("Adjuntar Archivos a la Solicitud");
		acercaDeWindow.button("minmax1").hide();
		acercaDeWindow.button("park").hide();
		acercaDeWindow.setModal(true);
		acercaDeWindow.center();
		acercaDeWindow.denyResize();
		acercaDeWindow.attachHTMLString("<div id='vault'></div>");

		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setServerHandlers("/MidasWeb/sistema/vault/fileUploadAction.do",
	            "/MidasWeb/sistema/vault/getInfoHandler.do",
	            "/MidasWeb/sistema/vault/getIdHandler.do");

	    vault.onUploadComplete = function(files) {
	        var s="";
	        for (var i=0; i<files.length; i++) {
	            var file = files[i];
	            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
	        }
	       // sendRequest(null, '/MidasWeb/endoso/solicitud/listarDocumentos.do', 'resultadosDocumentosEndoso', null);
	        parent.dhxWins.window("adjuntarArchivoSolicitudSistema").close();
	        parent.mostrarAcercaDeWindow();
	    };
	    vault.create("vault");
	    vault.setFormField("claveTipo", "0");
	}
 
 function mostrarAdjuntarDocumentoDigitalComplementarioSistemaWindow() {
		if(dhxWins != null) 
			dhxWins.unload();
		
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoDigitalComplementarioSistema", 34, 100, 440, 265);
		adjuntarDocumento.setText("Adjuntar Archivos a la Cotizaci&oacute;n");
		adjuntarDocumento.button("minmax1").hide();
		adjuntarDocumento.button("park").hide();
		adjuntarDocumento.setModal(true);
		adjuntarDocumento.center();
		adjuntarDocumento.denyResize();
		adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setServerHandlers("/MidasWeb/sistema/vault/fileUploadAction.do",
	            "/MidasWeb/sistema/vault/getInfoHandler.do",
	            "/MidasWeb/sistema/vault/getIdHandler.do");

	    vault.onUploadComplete = function(files) {
	        var s="";
	        for (var i=0; i<files.length; i++) {
	            var file = files[i];
	            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
	        }
	        //sendRequest(null, '/MidasWeb/cotizacion/listarDocumentosDigitalesComplementarios.do?id='+idToCotizacion,'contenido_documentosDigitalesComplementarios',null);
	        parent.dhxWins.window("adjuntarDocumentoDigitalComplementarioSistema").close();
	        parent.mostrarAcercaDeWindow();
	    };
	    vault.create("vault");
	    vault.setFormField("claveTipo", "4");
	   // vault.setFormField("idToCotizacion", idToCotizacion);
	}
	   
/**
 * Funciones de Carga Masiva
 * @autor JORGEKNO
 */
var cargaMasivaGrid;	
var cargaMasivaProcessor;
var coloniasCombo;
var municipioEncotrado;
var estadoEncontrado;
/**
 * @param {Object} idToCotizacion
 * @param {Object} origen
 * @return {TypeName} 
 */
function mostrarGridCargaMasiva(idToCotizacion, origen){
	cargaMasivaGrid = new dhtmlXGridObject('cargaMasivaGrid');
	
	cargaMasivaGrid.setHeader("Estatus,#,Calle,No.Ext,No.Int,C.P.,Estado,Municipio,Colonia Original,Colonia V&aacute;lida,Mensaje Error,,");
	cargaMasivaGrid.setColumnIds("claveEstatus,numeroInciso,nombreCalle,numeroExterior,numeroInterior,codigoPostal,nombreEstado,nombreMunicipio,nombreColoniaOriginal,nombreColoniaValida,mensajeError,idColonia,idToCotizacion");
	cargaMasivaGrid.setInitWidths("60,25,150,60,60,70,150,150,150,150,150,0,0");
	cargaMasivaGrid.setColAlign("center,center,left,left,left,center,left,left,left,left,center,center,center");
	cargaMasivaGrid.setColSorting("na,int,string,string,string,int,string,string,string,na,string,string,string");
	cargaMasivaGrid.setColTypes("img,edtxt,edtxt,edtxt,edtxt,edtxt,ro,ro,ro,co,ro,ro,ro");
	cargaMasivaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cargaMasivaGrid.setSkin("light");
	cargaMasivaGrid.enableMultiline(true);
	cargaMasivaGrid.setColumnHidden(11,true);
	cargaMasivaGrid.setColumnHidden(12,true);
	cargaMasivaGrid.init();
	cargaMasivaGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		if(stage == 2 && cInd == 5) {
			var estado = cargaMasivaGrid.cellById(rId, 6);
			var municipio = cargaMasivaGrid.cellById(rId, 7);

			coloniasCombo = cargaMasivaGrid.getCustomCombo(rId, 9);	
			getColonias(nValue,coloniasCombo);

			if(estadoEncontrado != estado.getValue()){
				estado.setValue(estadoEncontrado);
			}
			if(municipioEncotrado != municipio.getValue()){
				municipio.setValue(municipioEncotrado);
			}
		}
		return true;
	});	
	cargaMasivaGrid.attachEvent("onRowDblClicked", function(rId,cInd){
		if(cInd == 9){
			coloniasCombo = cargaMasivaGrid.getCustomCombo(rId, 9);
			var codigoPostal = cargaMasivaGrid.cellById(rId, 5);
			getColonias(codigoPostal.getValue(),coloniasCombo);
		}
		return true;
	});	
	cargaMasivaGrid.load('/MidasWeb/cotizacion/inciso/getCargaMasiva.do?idToCotizacion='+idToCotizacion, null, 'json');
	cargaMasivaProcessor = new dataProcessor('/MidasWeb/cotizacion/inciso/guardarDetalleCargaMasiva.do?origen='+origen);
	cargaMasivaProcessor.enableDataNames(true);
	cargaMasivaProcessor.setTransactionMode("POST");
	cargaMasivaProcessor.setUpdateMode("off");
	cargaMasivaProcessor.attachEvent("onAfterUpdateFinish", function(){
		if(cargaMasivaProcessor.getSyncState()){
			mostrarGridCargaMasiva(idToCotizacion, origen);
		}
	});
	cargaMasivaProcessor.attachEvent("onBeforeDataSending",function(id){
		return true;
	});
	cargaMasivaProcessor.init(cargaMasivaGrid);	
}

/**
 * @param {Object} idToCotizacion
 * @param {Object} origen
 */
function mostrarCargaMasiva(idToCotizacion, origen){
	var nextFunction = "mostrarGridCargaMasiva(+"+idToCotizacion+",'"+origen+"');";
	sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/mostrarCargaMasiva.do?id='+idToCotizacion+'&orige='+origen, 'configuracion_detalle', nextFunction);
}
/**
 * @param {Object} codigoPostal
 * @param {Object} combo
 */
function getColonias(codigoPostal,combo){
	new Ajax.Request("/MidasWeb/codigoPostal.do", {
		method : "post",
		asynchronous : false,
		parameters :"id="+ codigoPostal,
		onSuccess : function(transport) {
			cargaComboColonia(combo,transport.responseXML);
		} // End of onSuccess
	});			
}
/**
 * @param {Object} combo
 * @param {Object} doc
 */
function cargaComboColonia(combo,doc){
	var items = doc.getElementsByTagName("colonies");
	for ( var x = 0; x < items.length; x++) {
		var item = items[x];
		var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		combo.put(value,text);
	} // End of for	
	
	var estado =  doc.getElementsByTagName("state");
	for ( var x = 0; x < estado.length; x++) {
		var item = estado[x];
		estadoEncontrado = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		break;
	}
	var municipio =  doc.getElementsByTagName("city");
	for ( var x = 0; x < municipio.length; x++) {
		var item = municipio[x];
		municipioEncotrado = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		break;
	}
	combo.save();
}

/**
 * @param {Object} idToCotizacion
 * @param {Object} origen
 */
function borrarCargaMasiva(idToCotizacion,origen){
	var nextFunction = "regresarAInciso("+idToCotizacion+",'"+origen+"');mostrarVentanaMensajeInciso();"
	sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/eliminarCargaMasiva.do?idToCotizacion='+idToCotizacion,'configuracion_detalle',nextFunction);
}
/**
 * @param {Object} idToCotizacion
 * @param {Object} origen
 */
function procesarCargaMasiva(idToCotizacion,origen){
	var nextFunction = "regresarAInciso("+idToCotizacion+",'"+origen+"');mostrarVentanaMensajeInciso();"
	sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/procesarCargaMasiva.do?idToCotizacion='+idToCotizacion,'configuracion_detalle',nextFunction);
}
/**
 * @param {Object} idToCotizacion
 * @param {Object} origen
 */
function confirmaBorrarCarga(idToCotizacion,origen){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el archivo de carga masiva\u003F')){
		borrarCargaMasiva(idToCotizacion,origen);
	}
}

function confirmaRenovacion(idToCotizacion){
	if(confirm('La p\u00f3liza a renovar tiene siniestros registrados, \u00BFDesea continuar con la cotizaci\u00f3n\u003F')){
		var nextFunction = "creaArbolCotizacion("+idToCotizacion+");dhx_init_tabbars();";
		sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=' + idToCotizacion +'&aceptoRenovacion=1','contenido',nextFunction);
	}	
}

function confirmaLiberacion(idToCotizacion, esEndoso){
	if($('correos').value == ''){
		if(confirm('Desea liberar la cotizaci\u00f3n sin agregar destinatarios de correo? ')){
			if(esEndoso == true){
				sendRequest(document.cotizacionReaseguroFacultativoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + idToCotizacion, 'contenido','mostrarVentanaMensajeValidar(); listarCotizacionesEndoso();');
			}else{
				sendRequest(document.cotizacionReaseguroFacultativoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + idToCotizacion, 'contenido','mostrarVentanaMensajeValidar(); listarCotizaciones();');
			}	
		}
	}else{
		if(esEndoso == true){
			sendRequest(document.cotizacionReaseguroFacultativoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + idToCotizacion, 'contenido','mostrarVentanaMensajeValidar(); listarCotizacionesEndoso();');
		}else{
			sendRequest(document.cotizacionReaseguroFacultativoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + idToCotizacion, 'contenido','mostrarVentanaMensajeValidar(); listarCotizaciones();');
		}	
	}	
}

function imprimirReporteRCS(form){
	var fechaIncial = document.getElementById("fechaInicial").value;
	var idTcRamo = document.getElementById("id_ramo").value;
	var numeroCortes = document.getElementById("numeroCortes").value;
	var nomenclatura = document.getElementById("nomenclatura").value;
	var tipoArchivo = 1;
	var cveNegocio = document.getElementById("cveNegocio").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/generarReporteRCS.do";
	
	if(cveNegocio == "4")
		location = "/MidasWeb/danios/reportes/reporteRCS/generarReporteRCSVida.do";
	
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
	{
		location = location+"?fechaInicio="+fechaIncial;
	}else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
		
	if(idTcRamo != undefined && idTcRamo != null && idTcRamo != '' )
		location = location+"&id_ramo="+idTcRamo;
	else{
		mostrarMensajeInformativo('No se ha seleccionado un ramo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(numeroCortes != undefined && numeroCortes != null && numeroCortes != '' )
	{	
		if(isNaN(numeroCortes))
		{
			mostrarMensajeInformativo('Numero corte debe ser numerico.', '20');
			jQuery("#nsOficina").focus();
			return;
		}else	
		location = location+"&numeroCortes="+numeroCortes;
	}
	else{
		mostrarMensajeInformativo('No se ha ingresado un numero corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(nomenclatura != undefined && nomenclatura != null && nomenclatura != '' )
		location = location+"&nomenclatura="+nomenclatura;
	else{
		mostrarMensajeInformativo('No se ha ingresado nomenclatura del archivo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	
	window.open(location+"&tipoArchivo="+tipoArchivo+"&claveNegocio="+cveNegocio,"ReporteRCS"); 
}

function imprimirBitacora(form){
	
	var location = "/MidasWeb/catalogos/reaseguradoresCNSF/mostrarBitacora.do";
	
	window.open(location,"ReporteRCS"); 
}

function detalleReporteRCS(form){
	var fechaIncial = document.getElementById("fechaInicial").value;
	var idTcRamo = document.getElementById("id_ramo").value;
	var numeroCortes = document.getElementById("numeroCortes").value;
	var nomenclatura = document.getElementById("nomenclatura").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/generarDReporteRCS.do";
	
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
	{
		location = location+"?fechaInicio="+fechaIncial;
	}else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
		
	if(idTcRamo != undefined && idTcRamo != null && idTcRamo != '' )
		location = location+"&id_ramo="+idTcRamo;
	else{
		mostrarMensajeInformativo('No se ha seleccionado un ramo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(numeroCortes != undefined && numeroCortes != null && numeroCortes != '' )
		location = location+"&numeroCortes="+numeroCortes;
	else{
		mostrarMensajeInformativo('No se ha ingresado un numero corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(nomenclatura != undefined && nomenclatura != null && nomenclatura != '' )
		location = location+"&nomenclatura="+nomenclatura;
	else{
		mostrarMensajeInformativo('No se ha ingresado nomenclatura del archivo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	window.open(location,"ReporteRCS"); 
}

function imprimirReporteREASRCS(form){
	var fechaIncial = document.getElementById("fechaInicial").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/generarReporteREASRCS.do";
	var numeroCortes = document.getElementById("numeroCortes").value;
	var nomenclatura = document.getElementById("nomenclatura").value;
	var tipoArchivo = 1;
	
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
	{
		location = location+"?fechaInicio="+fechaIncial;
	}else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(numeroCortes != undefined && numeroCortes != null && numeroCortes != '' )
		if(isNaN(numeroCortes))
		{
			mostrarMensajeInformativo('Numero de cortes, debe ser numerico.', '20');
			jQuery("#nsOficina").focus();
			return;
		}else
		location = location+"&numeroCortes="+numeroCortes;
	else{
		mostrarMensajeInformativo('No se ha ingresado un numero corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	if(nomenclatura != undefined && nomenclatura != null && nomenclatura != '' )
		location = location+"&nomenclatura="+nomenclatura;
	else{
		mostrarMensajeInformativo('No se ha ingresado nomenclatura del archivo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	window.open(location+"&tipoArchivo="+tipoArchivo,"ReporteREASRCS");
}

function procesoRCS(form, reproceso){
	var fechaIncial = document.getElementById("fechaInicial").value;
	var negocio = document.getElementById("negocio").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/iniciarProcesoRCS.do";
	
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
	{
		location = location+"?fechaInicio="+fechaIncial+"&negocio="+negocio+"&reproceso="+reproceso;
		sendRequestM(null, location, 'resultadosDocumentos', null,'El proceso ha terminado.');
	}else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}

function eliminarRangos(form){
	
	var idTcRamo = document.getElementById("id_ramo").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/eliminaRangos.do";
	var cveNegocio = document.getElementById("cveNegocio").value;
	
	if(idTcRamo != undefined && idTcRamo != null && idTcRamo != '' )
	{
		location = location+"?idTcRamo="+idTcRamo+"&claveNegocio="+cveNegocio;
		sendRequestM(null, location, null, "alert('Se han eliminado los rangos.')",'Se han eliminado los rangos.');
		
		return;
	}  
	else{
		mostrarMensajeInformativo('No se ha ingresado un ramo.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}


function consultaRangos(form){
	
	var location = "/MidasWeb/danios/reportes/reporteRCS/consultaRangos.do";
	window.open(location,"Consulta Rangos");
}


function rangosPorDuracion(){
	 
	
	var location = "/MidasWeb/danios/reportes/reporteRCS/rangosPorDuracion.do";
	
	var nomenclatura = document.getElementById("nomenclatura").value;
	var numeroCortes = document.getElementById("numeroCortes").value;
	
	if(nomenclatura != undefined && nomenclatura != null && nomenclatura != '' )
	{
		location = location+"?nomenclatura="+nomenclatura+"&numeroCortes="+numeroCortes;;
		sendRequestM(null, location, null, "alert('Se han actualizado los rangos.')",'Se han actualizado los rangos.');
		return; 
	}  
	else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}

function actualizaRangos(form){
	
	var fechaIncial = document.getElementById("fechaInicial").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/actualizaRangos.do";
	var cveNegocio = document.getElementById("cveNegocio").value;
	
	if(fechaIncial != undefined && fechaIncial != null && fechaIncial != '' )
	{
		location = location+"?fechaInicio="+fechaIncial+"&claveNegocio="+cveNegocio;
		sendRequestM(null, location, null, "alert('Se han actualizado los rangos.')",'Se han actualizado los rangos.');
		return;
	}  
	else{
		mostrarMensajeInformativo('No se ha ingresado una fecha de corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}

function cargarRangos(idToControlArchivo){
	
	sendRequestM(null,'/MidasWeb/danios/reportes/reporteRCS/cargaRangos.do?idToControlArchivo='+ idToControlArchivo, null, "alert('El proceso ha terminado.')",'El proceso ha terminado.');
}

function eliminarContratos(){
	
	var anioContrato = dwr.util.getValue("fechaCorte");
	var cve_negocio = 2;
	var location = "/MidasWeb/danios/reportes/reporteRCS/eliminarContratos.do";
	var tipoArchivo = dwr.util.getValue("tipoArchivo");
	
	if (tipoArchivo == 0) {
		mostrarMensajeInformativo('Por favor elija una plantilla v\xE1lida.', '10', null,null);
	}
	
	if(anioContrato != undefined && anioContrato != null && anioContrato != '' ) {		
		location = location+"?anioContrato="+anioContrato+"&cve_negocio="+tipoArchivo;
		jQuery.ajax({
		    type: "POST",
		    url: location,
		    data: null,
		    dataType: null,
		    async: true,
		    contentType: defaultContentType,
		    success : function(data) {
		    	jQuery("#mensaje_popup").html(data);
		    	mostrarMensajeInformativo(document.getElementById("msgDocumentos").textContent, '20');
		    }
		});
		jQuery("#nsOficina").focus();
		return;
	} else{
		mostrarMensajeInformativo('No se ha ingresado una Fecha.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}

 
function actualizarCalificaciones(){
	
	var anioCorte = dwr.util.getValue("fechaCorte");
	var cve_negocio = 2;
	var location = "/MidasWeb/danios/reportes/reporteRCS/actualizaCalificaciones.do";
	document.getElementById("resultadosDocumentos").innerHTML = ""; 
	
	if(anioCorte != undefined && anioCorte != null && anioCorte != '' )
	{
		
		location = location+"?anioCorte="+anioCorte+"&cve_negocio="+cve_negocio;
		sendRequest(null, location, null, null);
		mostrarMensajeInformativo('Se han actaulizado las calificaciones.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
	else{
		mostrarMensajeInformativo('No se ha ingresado una Fecha.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}

function consultaProceso(){
	
	var fechaInicial = document.getElementById("fechaInicial").value;
	var negocio = document.getElementById("negocio").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/consultaProcesoRCS.do";
	
	if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '' )
	{
		location = location+"?fechaInicial="+fechaInicial+"&negocio="+negocio;
		
		sendRequest(null, location, 'resultadosDocumentos', null);
		
		consultaValidaciones();
		return;
	}
	else{
		mostrarMensajeInformativo('No se ha ingresado una fecha Corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}


function consultaValidaciones(){
	
	var fechaInicial = document.getElementById("fechaInicial").value;
	var negocio = document.getElementById("negocio").value;
	var location = "/MidasWeb/danios/reportes/reporteRCS/consultaValidacionesRCS.do";
	
	if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '' )
	{ 
		location = location+"?fechaInicial="+fechaInicial+"&negocio="+negocio;
		window.open(location,"ReporteRCS");
		return;
	}
	else{
		mostrarMensajeInformativo('No se ha ingresado una fecha Corte.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}
