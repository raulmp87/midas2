
var accordionAnexos = null;
var soloLectura = false;

/**
 * Inicio ventana de agregar archivos anexos de reaseguro a cotizacion
 */
var anexarArchivoWindow;
function mostrarAnexarArchivoReaseguroWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.onUploadComplete = function(files) {
    	iniciaItemDocAnexosReaseguro($('idToCotizacion').value);
        parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    };
    vault.onAddFile = function(fileName) { 
    	var ext = this.getFileExtension(fileName); 
    	if (ext != "pdf" && ext != "PDF") { 
    		alert("Solo se pueden anexar Archivos PDF"); 
    		return false; 
    	} else 
    		return true; 
    }; 
    
    vault.create("vault");
    vault.setFormField("claveTipo", "17"); //17: Anexos reaseguro
}


/* Codigo documentos anexos de reaseguro en danios->cotizacion->documentosAnexos*/
	
var documentoAnexoReaseguroProcessor=null;
var documentoAnexoReaseguroSecuenciaError=false;
var documentoAnexoReaseguroDescripcionError=false;
//var documentos;
var cotizacionId=null;
function mostrarDocumentosReaseguroGrid(idCot) {
	documentos= new dhtmlXGridObject('documentosAnexosReaseguroGrid');
	documentos.setHeader("idToCotizacion,Archivo,# Secuencia, Descripcion");
	documentos.setColumnIds("idToCotizacion,nombreArchivo,numeroSecuencia,descripcionDocumento");
	documentos.setInitWidths("2,220,120,290");
	documentos.setColAlign("left,left,left,left");
	
	if (soloLectura) {
		documentos.setColTypes("ro,ro,ro,ro");
	} else {
		documentos.setColTypes("ro,ro,ed,ed");
	}
	
	documentos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	documentos.setSkin("light");
	
	documentos.setColumnHidden(0, true);
	documentos.attachEvent("onRowCreated",function(rowId) {
		var secuencia = documentos.cellById(rowId, 2);
		if(secuencia.getValue() == "0"){
			documentoAnexoReaseguroProcessor.setUpdated(rowId, true, "updated");
		}
	});
	
	
	documentos.init();
	if (idCot!=undefined)	cotizacionId=idCot;
	else	idCot=cotizacionId;
	documentos.load('/MidasWeb/cotizacion/documento/docAnexoCot/mostrarDocAnexosReaseguro.do?id='+ idCot, null, 'json');
	
	documentoAnexoReaseguroProcessor = new dataProcessor("/MidasWeb/cotizacion/documento/docAnexoCot/guardarDocumentoAnexoReaseguro.do");
	documentoAnexoReaseguroProcessor.enableDataNames(true);
	documentoAnexoReaseguroProcessor.setTransactionMode("POST");
	documentoAnexoReaseguroProcessor.setUpdateMode("off");
	
	documentoAnexoReaseguroProcessor.setVerificator(2, function(value,colName){
		documentoAnexoReaseguroSecuenciaError = false;
		if(value > 0){
			return true;
	    } else {
	    	documentoAnexoReaseguroSecuenciaError = true;
	        return false;
	    }
		
	});
	documentoAnexoReaseguroProcessor.setVerificator(3, function(value,colName){
		
		var val = value.toString()._dhx_trim();
		
		if(val.length==0) {
			documentoAnexoReaseguroSecuenciaError = true;
			return false;
		}
	    else {
	    	return true;
	    }
		
	});
	documentoAnexoReaseguroProcessor.init(documentos);
}

function iniciaItemDocAnexosPolizaEndoso() {
	
	var url = null;
	
	if (soloLectura) {
		url = "/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarDocAnexo.do";
	} else {
		url = "/MidasWeb/cotizacion/documento/docAnexoCot/mostrarDocAnexo.do";
	}
	
	
	new Ajax.Request( url, {
		method : "post",
		asynchronous : false,
		onSuccess : function(transport) {
			$("item1").innerHTML = transport.responseText;
			
			accordionAnexos.cells("a1").attachObject("item1");
			if (soloLectura) {
				cargaComponentesDocAnexosSoloLectura();
			} else {
				cargaComponentesDocAnexos();
			}
			//renderDatosSubInciso(transport.responseXML);
		} // End of onSuccess
	});	
}


function iniciaItemDocAnexosReaseguro(idToCotizacion) {
	
	var url = null;
	
	if (soloLectura) {
		url = "/MidasWeb/cotizacionsololectura/docAnexoCot/cargarDocAnexosReaseguro.do";
	} else {
		url = "/MidasWeb/cotizacion/documento/docAnexoCot/cargarDocAnexosReaseguro.do";
	}
	
	new Ajax.Request( url, {
		method : "post",
		asynchronous : false,
		parameters : "id=" + idToCotizacion,
		onSuccess : function(transport) {
			$("item2").innerHTML = transport.responseText;
			
			accordionAnexos.cells("a2").attachObject("item2");
			mostrarDocumentosReaseguroGrid(idToCotizacion);
			//renderDatosSubInciso(transport.responseXML);
		} // End of onSuccess
	});	
}



function inicializaObjetosAnexosCotizacion(idToCotizacion){
	accordionAnexos = new dhtmlXAccordion("accordionAnexos");
	accordionAnexos.addItem("a1", "Documentos Anexos");
	accordionAnexos.addItem("a2", "Documentos Anexos Reaseguro");
	
	iniciaItemDocAnexosPolizaEndoso();
	
	iniciaItemDocAnexosReaseguro(idToCotizacion);
	
	accordionAnexos.openItem("a1");
	accordionAnexos.setEffect(true);
	
}


function cargaTabAnexosCot(paramSoloLectura){
	//sendRequest(null,'/MidasWeb/cotizacion/documento/docAnexoCot/mostrarDocAnexo.do','contenido_documentosAnexos','cargaComponentesDocAnexos()');true;
	soloLectura = paramSoloLectura;
	sendRequest(null,'/MidasWeb/cotizacion/documento/docAnexoCot/mostrarAnexos.do','contenido_documentosAnexos','inicializaObjetosAnexosCotizacion(' + $('idToCotizacion').value + ')');
}


