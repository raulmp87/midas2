var PersonaFisicaRFC	    = 0;
var PersonaMoralRFC		    = 1;
var HomoclaveRFC			= 2;
var nombre, s,apePat, apeMat, curp, cadena, y, caracter1,caracter2,indice, palabra, edos, caracter3,letra, edo, sex,i, consonante1, consonante2, consonante3, strLetra1, strLetra2, strLetra3, strLetra4, strClvAlf, strComplet, strRegistro;

 var aDelPerMoral=new Array(30);
 var aDelApe=new Array(40);
 var estados=new Array(32); 
 var gender=new Array(2); 

 var ANT=new Array(45); 
 var conso;
/* 
1 AGUASCALIENTES 2 BAJA CALIFORNIA NORTE 3 BAJA CALIFORNIA SUR 4 CAMPECHE 5 COAHUILA 6 COLIMA 7 CHIAPAS 
8 CHIHUAHUA 9 DISTRITO FEDERAL 10 DURANGO 11 GUANAJUATO 12 GUERRERO 13 HIDALGO 14 JALISCO 
15 MEXICO 16 MICHOACAN 17 MORELOS 18 NAYARIT 19 NUEVO LEON 20 OAXACA 21 PUEBLA 
22 QUERETARO 23 QUINTANA ROO 24 SAN LUIS POTOSI 25 SINALOA 26 SONORA 27 TABASCO 28 TAMAULIPAS 
29 TLAXCALA 30 VERACRUZ 31 YUCATAN 32 ZACATECAS 

 estados["AGS"]="AS"; estados["BCN"]="BC"; estados["BCS"]="BS"; estados["CAM"]="CC"; estados["CHI"]="CH"; estados["CHS"]="CS"; estados["COA"]="CL";
 estados["COL"]="CM"; estados["DF"]="DF";  estados["DGO"]="DG"; estados["GRO"]="GR"; estados["GTO"]="GT"; estados["HGO"]="HG"; estados["JAL"]="JC"; 
 estados["MIC"]="MN"; estados["MOR"]="MS"; estados["EM"]="MC";  estados["NAY"]="NT"; estados["NL"]="NL";  estados["OAX"]="OC"; estados["PUE"]="PL";
 estados["QR"]="QR";  estados["QRO"]="QT"; estados["SIN"]="SL"; estados["SLP"]="SP"; estados["SON"]="SR"; estados["TAB"]="TC"; estados["TAM"]="TS"; 
 estados["TLA"]="TL"; estados["VER"]="VZ"; estados["YUC"]="YN"; estados["ZAC"]="ZS";  //estados[32]="NE";
*/

 estados["01000"]="AS"; estados["02000"]="BC"; estados["03000"]="BS"; estados["04000"]="CC"; estados["05000"]="CL"; estados["06000"]="CM"; estados["07000"]="CS"; 
 estados["08000"]="CH"; estados["09000"]="DF";  estados["10000"]="DG"; estados["11000"]="GT"; estados["12000"]="GR"; estados["13000"]="HG"; estados["14000"]="JC"; 
 estados["15000"]="MC"; estados["16000"]="MN"; estados["17000"]="MS"; estados["18000"]="NT"; estados["19000"]="NL";  estados["20000"]="OC"; estados["21000"]="PL";
 estados["22000"]="QR";  estados["23000"]="QT"; estados["24000"]="SP"; estados["25000"]="SL"; estados["26000"]="SR"; estados["27000"]="TC"; estados["28000"]="TS"; 
 estados["29000"]="TL"; estados["30000"]="VZ"; estados["31000"]="YN"; estados["32000"]="ZS";

gender["M"]="H";
gender["F"]="M";
 
ANT[0]="BUEI"; ANT[1]="BUEY"; ANT[2]="CACA"; ANT[3]="CACO"; ANT[4]="CAGA"; ANT[5]="CAGO"; ANT[6]="CAKA";
ANT[7]="CAKO"; ANT[8]="COGE"; ANT[9]="COGA"; ANT[10]="COJE"; ANT[11]="COJI"; ANT[12]="COJO"; ANT[13]="CULO";
ANT[14]="FETO"; ANT[15]="GUEY"; ANT[16]="JOTO"; ANT[17]="KACA"; ANT[18]="KAKO"; ANT[19]="KAGA"; ANT[20]="KAGO";
ANT[21]="KOGE"; ANT[22]="KOJO"; ANT[23]="KAKA"; ANT[24]="KULO"; ANT[25]="LOCA"; ANT[26]="LOCO"; ANT[27]="LOKA";
ANT[28]="LOKO"; ANT[29]="MAME"; ANT[30]="MAMO"; ANT[31]="MEAR"; ANT[32]="MEAS"; ANT[33]="MEON"; ANT[34]="MION";
ANT[35]="MOCO"; ANT[36]="MULA"; ANT[37]="PEDA"; ANT[38]="PEDO"; ANT[39]="PENE"; ANT[40]="PUTA"; ANT[41]="PUTO";
ANT[42]="QULO"; ANT[43]="RATA"; ANT[44]="RUIN";

var codigo=new Array(34);
codigo[0]="1"; codigo[1]="2"; codigo[2]="3"; codigo[3]="4"; codigo[4]="5"; codigo[5]="6"; codigo[6]="7";
codigo[7]="8"; codigo[8]="9"; codigo[9]="A"; codigo[10]="B"; codigo[11]="C"; codigo[12]="D"; codigo[13]="E";
codigo[14]="F"; codigo[15]="G"; codigo[16]="H"; codigo[17]="I"; codigo[18]="J"; codigo[19]="K"; codigo[20]="L";
codigo[21]="M"; codigo[22]="N"; codigo[23]="P"; codigo[24]="Q";  codigo[25]="R"; codigo[26]="S"; codigo[27]="T";
codigo[28]="U"; codigo[29]="V"; codigo[30]="W"; codigo[31]="X"; codigo[32]="Y"; codigo[33]="Z";   

//Arreglo para la persona moral
aDelPerMoral[1] = " SA DE CV";
aDelPerMoral[2] = " S.A. DE C.V.";
aDelPerMoral[3] = " S.A DE C.V";
aDelPerMoral[4] = " S EN C";
aDelPerMoral[5] = " S EN C POR A";
aDelPerMoral[6] = " S DE RL";
aDelPerMoral[7] = " S DE RL DE IP";
aDelPerMoral[8] = " SNC";
aDelPerMoral[9] = " SNC ";
aDelPerMoral[10] = " SOFOL";
aDelPerMoral[11] = " SOFOL ";
aDelPerMoral[12] = " AFORE";
aDelPerMoral[13] = " AFORE ";
aDelPerMoral[14] = " SIEAFORE";
aDelPerMoral[15] = " SIEAFORE ";
aDelPerMoral[16] = " S DE RL MI";
aDelPerMoral[17] = " SA";
aDelPerMoral[18] = " SA ";
aDelPerMoral[19] = " CV";
aDelPerMoral[20] = " CV ";
aDelPerMoral[21] = " AC";
aDelPerMoral[22] = " AC ";
aDelPerMoral[23] = " SC";
aDelPerMoral[24] = " SC ";
aDelPerMoral[25] = " AF";
aDelPerMoral[26] = " AF ";
aDelPerMoral[27] = " AP";
aDelPerMoral[28] = " AP ";
aDelPerMoral[29] = " AR";
aDelPerMoral[30] = " AR ";

aDelApe[1] = " DE ";
aDelApe[2] = " DEL ";
aDelApe[3] = " LA ";
aDelApe[4] = " LOS ";
aDelApe[5] = " Y ";
aDelApe[6] = " . ";
aDelApe[7] = " LAS ";
aDelApe[8] = " MC ";
aDelApe[9] = " MAC ";
aDelApe[10] = " VON ";
aDelApe[11] = " VAN ";
aDelApe[12] = " DA ";
aDelApe[13] = " DI ";
aDelApe[14] = " DEGLI ";
aDelApe[15] = " DELLA ";
aDelApe[16] = " D ";
aDelApe[17] = " DES ";
aDelApe[18] = " DU ";
aDelApe[19] = " VANDEN ";
aDelApe[20] = " VANDER ";
aDelApe[21] = ". ";
aDelApe[22] = " CON ";
aDelApe[23] = " A ";
aDelApe[24] = " ANTE ";
aDelApe[25] = " BAJO ";
aDelApe[26] = " CABE ";
aDelApe[27] = " CONTRA ";
aDelApe[28] = " DESDE ";
aDelApe[29] = " EN ";
aDelApe[30] = " ENTRE ";
aDelApe[31] = " HACIA ";
aDelApe[32] = " HASTA ";
aDelApe[34] = " PARA ";
aDelApe[35] = " POR ";
aDelApe[36] = " SEGUN ";
aDelApe[37] = " SIN ";
aDelApe[38] = " SI ";
aDelApe[39] = " SOBRE ";
aDelApe[40] = " TRAS ";

 strLetra1 = "";
 strLetra2 = "";
 strLetra3 = "";
 strLetra4 = "";
 strClvAlf = "";
 strComplet = "";
 strRegistro = "";

function calcularCURP(pate,mate,nombreC,estado,nac,sexo)
{
	apePat = pate;
	apeMat = mate;
	nombre = nombreC;
	edos = estado;
	nac = nac;
	sex = sexo;
	
	//alert("Entrando a curp");
        cadena=PrepararCadena(apePat);
  	checaApellidos(cadena);
  	caracter1=cadena.substring(i, i+1);
        y=i;
        consonantes(cadena);
        consonante1=cadena.charAt(y); 
	//consonante1=conso;
        var bandera=true;    
        while (bandera)
         {   
            i++; 
            letra=cadena.substring(i,i+1);
              
             if ((letra=="A") ||(letra=="E")||(letra=="I")||(letra=="O")||(letra=="U")){bandera=false;}
             if(cadena.length < i)
             	break;
        }
        caracter1+=letra; 

   
    	if(apeMat=="")		
        {
         caracter2="X";
         consonante2="X";
         //consonante2=conso;
        }
       else
       {  

       cadena=PrepararCadena(apeMat);
       checaApellidos(cadena);
       caracter2=cadena.substring(i, i+1)
       y=i;
       consonantes(cadena);
       consonante2=cadena.charAt(y); 
	//consonante2=conso;
    }
    cadena=PrepararCadena(nombre);
    checaNombres(cadena);
    caracter3=cadena.substring(i, i+1);
    y=i;
    consonantes(cadena);
    consonante3=cadena.charAt(y);      
    //consonante3=conso;
    curp=caracter1+caracter2+caracter3;
    palabra=curp;
    altisonante();
    curp=caracter1+caracter2+caracter3;
    fechas(nac);
    //SEXO();
    curp+=caracter1+caracter2+caracter3+gender[sex];
    edo=estados[edos];
	curp+=edo+consonante1+consonante2+consonante3;
    return curp;
   
}//fin funcion curp


function calcularRFCMoral(nombre,nac)
{
var intJ = 0;
//Se quitan las palabras no deseadas de la cadena
for(intI = 1; intI <= 30; intI++)  //  Len(aDelPerMoral)
  {	
	intJ = nombre.indexOf(aDelPerMoral[intI]);
	if(intJ!=0)
	{
		nombre = nombre.replace(aDelPerMoral[intI],"");
	}
	if(intI<=22)
	{
		intJ = nombre.indexOf(aDelPerMoral[intI]);
		if(intJ!=0)
		{
			nombre = nombre.replace(aDelPerMoral[intI],"");
		}
	}
  }
  nombre = Trim(nombre) + " ";
  strRegistro = nombre;

  if (nombre.indexOf(" ") == 0) {
	alert("No se puede calcular el RFC de una persona moral con los datos capturados");

  RFC = "";
  return RFC;
}
//Se obtiene el primer segmento de la cadena
strLetra1 = Trim(strRegistro.substr(0, strRegistro.indexOf(" ") - 1));
strRegistro = Trim(strRegistro.substr((strRegistro.indexOf(" ") + 1), strRegistro.length));


//Se obtiene el segundo segmento de la cadena
if (strRegistro.indexOf(" ") != -1) {
	strLetra2 = strRegistro.substr(0, strRegistro.indexOf(" ") - 1);
	strRegistro = strRegistro.substr((strRegistro.indexOf(" ") + 1), strRegistro.length);
}
else
{
	strLetra2 = strRegistro.substr(0,2);
	strRegistro = "";
	strLetra3 = "";	
}

//Se obtiene el tercer segmento de la cadena
if(strRegistro.length > 0) 
	strLetra3 = strRegistro.substr(0, 1);

//Si hay tres palabras
if(strLetra3 != "" && strLetra2 != "" && strLetra1 != "")
   strRegistro = strLetra1.substr(0, 1) + strLetra2.substr(0, 1) + strLetra3.substr(0, 1);

//Si hay dos palabras
if(strLetra3 == "" && strLetra2 != "" && strLetra1 != "")
   strRegistro = strLetra1.substr(0, 1) + strLetra2.substr(0, 2);

//Si hay solo una palabra
if(strLetra3 == "" && strLetra2 == "" && strLetra1 != "")
   strRegistro = strLetra1.substr(0, 3);

//Se concatena la fecha de RFC

    RFC = strRegistro
    fechas(nac); 
    RFC+=caracter1+caracter2+caracter3;
    return RFC;

}

function checaApellidos(cadena)
{
	//alert("entrando checa apellidos");
i=0;
  if ((cadena.substring(0,4)=="DEL ")||(cadena.substring(0,4)=="VAN ") ||(cadena.substring(0,4)=="VON " )||(cadena.substring(0,4)=="MAC " )){i=4; }

  if (cadena.substring(0,6)=="DE LA " ){i=6;}
  else{
        if ((cadena.substring(0,7)=="DE LAS ")||(cadena.substring(0,7)=="DE LOS ")){i=7;}
        else{
            if ((cadena.substring(0,3)=="DE ")||(cadena.substring(0,3)=="MC ")){i=3;}  
        }
   }    
   if (cadena.substring(0,2)=="Y "){ i=2; }  
}
  
function checaNombres()
{
	//alert("entrando checa nombres");
   i=0;
   
   if (cadena.substring(0,6)=="MARIA ")
   {
       i=6;
        if (cadena.substring (6,10)=="DEL "){i=10;} 
        else{
             if ((cadena.substring(6,13)=="DE LOS ")||(cadena.substring(6,13)=="DE LAS ")){i=13;}
             else{
                 if (cadena.substring(6,9)=="DE "){i=9;}  
                 }
           }
   }

   if (cadena.substring(0,5)=="JOSE ")
   {
       i=5;
       if (cadena.substring(0,8)=="JOSE DE "){i=8;}
   }

   if (cadena.substring(0,4)=="MA. "){i=4;}
   
   if (cadena.substring(0,3)=="MA "){i=3;}
    
}

function fechas(nac)
{
	//alert("entrando fechas");
   //var nac=formulario.nacimiento.value;
    caracter3=nac.substring(0,2);
   caracter2=nac.substring(3,5);
    caracter1=nac.substring(8,11);
}

function SEXO(){ 
    var i; 
    for (i=0;i<document.formulario.sexos.length;i++){ 
       if (document.formulario.sexos[i].checked)
       {
        sex= document.formulario.sexos[i].value; 
          break; 
        }
    } 
} 


function consonantes(cadena){
	//alert("entrando consonantes");
   bandera=true;
   while (bandera)
         {   
             y++; 
            letra=cadena.charAt(y); 
             if ((letra!="A") && (letra!="E")&&(letra!="I")&&(letra!="O")&&(letra!="U"))
             {  bandera=false;}
             if(cadena.length < y)
             	break;
         }
}

/*
function consonantes(cadena){
    bandera=true;
   for (j=y+1; j<cadena.length; j++)
   {    
          letra=cadena.charAt(j); 
             if (((letra!="A") && (letra!="E")&&(letra!="I")&&(letra!="O")&&(letra!="U"))&&(bandera))
             {  bandera=false;
                y=j;}
   } 

   if(bandera){ conso="X"} 
   else{
     conso=cadena.charAt(y);
     if (conso.charCodeAt(0)==209){conso="X";}
    }

}
*/
function RTrim(s){
	//alert("entrando rTrim");
        var i=0;
	var j=0;
	for(var i=s.length-1; i>-1; i--)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(0, j+1);
}

function Trim(s){
	//alert("entrando TRim");
return LTrim(RTrim(s));}

function LTrim(s){
	//alert("entrando LTrim");
	// Devuelve una cadena sin los espacios del principio
	var i=0;
	var j=0;
	
	// Busca el primer caracter <> de un espacio
	for(i=0; i<=s.length-1; i++)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(j, s.length);
}

function PrepararCadena(evaluada)
{

	//alert("entrando checa preparacadena");
    var cad="";
    var s=evaluada;
    var letra;
    espacio=true;
    evaluada=Trim(s);
    evaluada=evaluada.toUpperCase();
    
    
    
    for (i=0; i<evaluada.length; i++)
    {
        letra=evaluada.charAt(i);
        if (letra!=" " ) 
        {
             /*
	     if(letra=="A"){letra="A";}
             if(letra=="E"){letra="E";}
             if(letra=="I"){letra="I";}
             if(letra=="O"){letra="O";}
             if(letra=="U"){letra="U";}
             if(letra=="U"){letra="U";}
		*/
	         if(letra.charCodeAt(0)==193){letra="A";}
             if(letra.charCodeAt(0)==201){letra="E";}
             if(letra.charCodeAt(0)==205){letra="I";}
             if(letra.charCodeAt(0)==211){letra="O";}
             if(letra.charCodeAt(0)==218){letra="U";}
             if(letra.charCodeAt(0)==220){letra="U";}

             cad+=letra;
             espacio=true;
           
        }
       if((letra==" ")&&(espacio))
        {
           cad+=letra;
           espacio=false; 
 
        }
    }
	
    return cad;

}

function altisonante()
{
	//alert("entrando altisonante");
   for(j=0; j<ANT.length; j++)
   {
       if (palabra==ANT[j]){ caracter3="X";}
   }
}

function calcularRFC(paterno,materno,nombreC,nac)
{  
        
	//apePat = eval(apePat);
	//apeMat = eval(apeMat);
	//nombre = eval(nombre);
	//nac = eval(nac);
	//alert(paterno);
    cadena=PrepararCadena(paterno);
    apePat=cadena;
  	checaApellidos(cadena);
  	caracter1=cadena.substring(i, i+1);
    
    var bandera=true; 
    var existeVocal = false;
    
    //Validar si la cadena contiene alg?n caracter vocal.
    if ( cadena.length == 1 )
		return 'Error en RFC, Apellido Paterno No v?lido'
    
    for(i=1; i<=cadena.length-1; i++)
		if ((cadena.substring(i,i+1) == "A") ||(cadena.substring(i,i+1) == "E")||(cadena.substring(i,i+1) == "I")||(cadena.substring(i,i+1) == "O")||(cadena.substring(i,i+1) == "U"))
		{
			existeVocal = true;
			break;
		}       
	
	if ( ! existeVocal )
		return 'Error en RFC, No se encontr? ninguna vocal'
	
	i = 0;
    while (bandera && i <= cadena.length-1)
    {   
		i++; 
        letra=cadena.substring(i,i+1);
        if ((letra=="A") ||(letra=="E")||(letra=="I")||(letra=="O")||(letra=="U")){bandera=false;}
    }
	
    caracter1+=letra; 
    
    if(materno=="")
    {
        caracter2="X";      
    }
    else
    {  
        cadena=PrepararCadena(materno);
        apeMat=cadena;
  		checaApellidos(cadena);
        caracter2=cadena.substring(i, i+1);  
     }
    
    
    cadena=PrepararCadena(nombreC);
    
	
	    
    nombre=cadena;
    checaNombres(cadena);
    caracter3=cadena.substring(i, i+1);        
    RFC=caracter1+caracter2+caracter3; 
    palabra=RFC;
    altisonante();
    RFC=caracter1+caracter2+caracter3;
    fechas(nac); 
    RFC+=caracter1+caracter2+caracter3;
    completo=apePat+" "+apeMat+" "+nombre;
    homoclaveRFC();    
    
    
    
	return RFC;
   
}//fin funcion rfc



function homoclaveRFC()
{

     temp="0";
     indice=0;
     while (indice<completo.length)
     {
          car=completo.charAt(indice);
          if ((car=="0")||(car=="1")||(car=="2")||(car=="3")||(car=="4")||(car=="5")||(car=="6")||(car=="7")||(car=="8")||(car=="9"))
         {
              car=car.charCodeAt(0)-48;}
          else{
                 if (car==" "){ car=0;}
                 else{
                     if(car.charCodeAt(0)==209){car=10;}
                     else{
                         if(car<"J"){ car=car.charCodeAt(0)-54;}
                         else{
                             if (car<"S"){car=car.charCodeAt(0)-53;}
                             else {  
                                  car=car.charCodeAt(0)-51;
                             }
                         }
                     }
                 }
           }
           car+=100;
           car=""+car;
          car=car.substring(1,3);
          temp+=car;
          indice++;
         
     }
      
     
      
     res=0;
     indice=0;
     while (indice<temp.length-1)
     {
	pri=parseInt(temp.substring(indice, indice+2));
        seg=parseInt(temp.substring(indice+1, indice+2));
        res+=(pri*seg);
         indice++;
     }
       


     pri=Math.floor((res % 1000)/34);
     seg=Math.floor((res % 1000)%34);
       car=codigo[pri];
       RFC+=car;
       car=codigo[seg];
       RFC+=car;


    
      
      temp=" ";
      seg=0;
      indice=0;//probablemente sea 0
      while (indice<RFC.length)
      {
          car=RFC.charAt(indice);
           // car=RFC.substring(indice, indice+1);
           if ((car=="0")||(car=="1")||(car=="2")||(car=="3")||(car=="4")||(car=="5")||(car=="6")||(car=="7")||(car=="8")||(car=="9"))
            {   pri=car.charCodeAt(0)-48;}
            else
            {
             if (car==" "){pri=37;}
              else {
                  if (car.charCodeAt(0)==209){pri=24;}
                      else{
	                  if ((car=="A")||(car=="B")||(car=="C")||(car=="D")||(car=="E")||(car=="F")||(car=="G")||(car=="H")||(car=="I")||(car=="J")||(car=="K")||(car=="L")||(car=="M")||(car=="N"))
                             {pri=car.charCodeAt(0)-55;}
                             else{ pri=car.charCodeAt(0)-54;}
                          }
                  }
           }
          seg+=(pri*(13-indice));
          indice++;
       }
      
       

       pri=seg % 11;
       
       if (pri==0){RFC+="0"}
       else{
           pri=11-pri;
           
           if(pri==10){ RFC+="A";}
           else{
               if(pri==0){RFC+="0"}
               else{RFC+=""+pri;  }
           }
       }

 
} //fin funcion homoclave
function ValidateRFC(dataRFC, tipoValidacion)
{
	
	var CadenaValida  = dataRFC, CaracterValido = true;
	var ControlString;
	var i;
		
	if ( tipoValidacion == PersonaFisicaRFC )	
		ControlString = "??ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		
	else if ( tipoValidacion == PersonaMoralRFC )
			ControlString = "&??ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		 else
			ControlString="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
	
	for(i=0; i<parseInt(CadenaValida.length); i++)
	{
			CaracterValido = false;
			for (j=0;j<ControlString.length;j++)
			{
				if (CadenaValida.charAt(i) ==  ControlString.charAt(j))
					CaracterValido = true;
			}
			if (!CaracterValido)
				break;
	}	
	
	if (!CaracterValido)
		return false;			
	else
		return true;			
}

function ValidateFechaRFC(dia,mes,anio)
{
//alert(dia+"-"+mes+"-"+anio);
	var result = "";
	if(isNaN(anio) || anio.length<4)
	{ 
			result = "false";
	}
	else {
	 if ((",01,02,03,04,05,06,07,08,09,10,11,12,".indexOf(","+ mes +",") == -1))
			result = "false";
		else
		{	
			if ((",01,03,05,07,08,10,12,".indexOf(","+ mes +",") > -1) && (parseInt(dia)>31))
				result = "false";
			else
		    {
				if ((",04,06,09,11,".indexOf(","+ mes +",")> -1) && (parseInt(dia)>30))
   				result = "false";
 				if (parseInt(mes)==2) {
 					if ((parseInt(anio) % 4)==0 && (parseInt(dia)>29)) {
       						result = "false";
					}
     				else {
						if ((parseInt(anio) % 4)!=0 && (parseInt(dia)>28))
							result = "false";
					}
	  			}
 			}
		}

	}
	
  	if (result == "false" ) 
    {		
            return false;
 	}
  	else
    {
        return true;
    }
}
/*******************************************************************************************************/
/*
* @parameter who, person to generate RFC
* @parameter place, attribute to contain the RFC
*/
function generarRFC(who, obj){
		showIndicator(); 
		var personData = new Array(6); 
		var propertyName = obj.getAttribute('name');							
		personData = knowWho(who);
		var rfc;


		if (evaluateNotNull(personData,'rfc', who)){
			rfc = calcularRFC(personData[1], personData[2], personData[0], personData[3]);
		}else{
			alert("El nombre completo y la fecha de nacimiento de la persona son requeridos");
			rfc = "";
			hideIndicator();
		}
		hideIndicator();		
		return obj.value = rfc;	
}//end generarRFC()

function generarCURP(who, obj){
		showIndicator(); 
		var personData = new Array(6); 
		personData = knowWho(who);
		var curp;

		if (evaluateNotNull(personData,'curp', who)){
			curp = calcularCURP(personData[1], personData[2], personData[0], personData[4],  personData[3], personData[5]);
		}else{
			alert("El nombre completo, sexo, fecha y lugar de nacimiento de la persona son requeridos");
			curp = "";
			hideIndicator();
		}
		hideIndicator();
		return obj.value = curp;	
}//end generarCURP()

/*
* @parameter who, help to know what information is neccesary to read in order to generate the RFC
*/
function knowWho(who){
		var data=new Array(6); 
/*
		var targetName = getElementsByAttribute("name", who+'.firstName');
		var targetFLastName = getElementsByAttribute("name", who+'.fatherLastName');
		var targetMLastName = getElementsByAttribute("name", who+'.motherLastName');
		var targetBirthDate = getElementsByAttribute("name", who+'.birthDateString');
		var targetBirthState = getElementsByAttribute("name", who+'.birthStateId');
		var tagetGenderId = getElementsByAttribute("name", who+'.genderId');
	
		data[0] = targetName[0].value;
		data[1] = targetFLastName[0].value;
  	    data[2] = targetMLastName[0].value;
		data[3] = targetBirthDate[0].value;
		data[4] = targetBirthState[0].value;	
*/
		var targetName = $(who+'.firstName');
		var targetFLastName = $(who+'.fatherLastName');
		var targetMLastName = $(who+'.motherLastName');
		var targetBirthDate = $(who+'.dateOfBirth');
		var targetBirthState = $(who+'.stateOfBirthId');
		
		var male = $$('#'+who.replace(/\./g,'')+'genero .male');
		var female = $$('#'+who.replace(/\./g,'')+'genero .female');
		
		data[0] = targetName.value;
		data[1] = targetFLastName.value;
  	    data[2] = targetMLastName.value;
		data[3] = targetBirthDate.value;
		data[4] = targetBirthState.value;	
					  	    
		if (male[0].checked == true){
			data[5] = male[0].value;	
		}else if (female[0].checked == true){
			data[5] = female[0].value;	
		}else{
			data[5] = null;	
		}	
		
		return data;
}//end knowWho()



/*******************************************************************************************************/

/*
* @parameter who, person to generate RFC
* @parameter place, attribute to contain the RFC
*/
function generateRFC(who, form){
		var personData = new Array(6);
		///Devuelve un array que contiene los datos de la persona, incluyendo c�digo de g�nero, estado de necimiento y fecha.
		personData = knowWhoByForm(who, form);
		var rfc;
			//Verifica si los datos obtenidos del "who" son v�lidos, en el caso de RFC s�lo checa los primeros 4
		if (evaluateNotNull(personData,'rfc', who)){
			rfc = calcularRFC(personData[1], personData[2], personData[0], personData[3]);
		}else{
			rfc = "";
			//alert('Para generar autom�ticamente el RFC, \nel nombre completo y fecha de nacimiento \nde la persona son requeridos');
		}
		return rfc;	
}//end generateRFC()

function formatRFC(who, form){
	//calcula el RFC enviandole los dos par�metros
   var rfc = generateRFC(who, form);
   if(rfc!=''){  
   	   var flag = true;
	   var dateRFC = rfc.match(/\B\d{6}/);  
	   var firstIndex = rfc.indexOf(dateRFC); 
	   var lastIndex = firstIndex + 6;     	 
	   var singleRFC = rfc.substr(0,firstIndex);   
	   var homoclaveRFC = rfc.substr(lastIndex ,rfc.length - lastIndex);
	   if(isNaN(parseInt(dateRFC))){
	   	dateRFC = "";
		flag = false;
	   }	   	
	   if(homoclaveRFC.length > 3){
	   	homoclaveRFC = "";
	   	flag = false;
	   }
	   if(singleRFC.length > 4){
	   	singleRFC = "";
	   	flag = false;
	   }
	   
	   /*var targetSingles = $(who+'.singlesRFC');
	   var targetDate = $(who+'.dateRFC');
	   var targetHomoclave = $(who+'.homoclaveRFC');	   
	   targetSingles.value = singleRFC;
	   targetDate.value = dateRFC;
	   targetHomoclave.value = homoclaveRFC;*/
	   
	   if(flag === false)		  	  
	   	//alert("Los datos proporcionados son inv�lidos para la generaci\xf3n de RFC");
		   document.getElementById('rfc').value = "";
	   else
		   document.getElementById('rfc').value = rfc;
	      
	   //document.getElementById('rfc').setText(rfc);
	   //alert("RFC: "+rfc);
   }   
  }
  
function generateCURP(who, form, place){
		var personData = new Array(6); 
		personData = knowWhoByForm(who, form);
		var curp;

		if (evaluateNotNull(personData,'curp', who)){
			curp = calcularCURP(personData[1], personData[2], personData[0], personData[4],  personData[3], personData[5]);
		}else{
			//alert("Para generar autom�ticamente el CURP, \nel nombre completo, sexo, fecha y lugar \nde nacimiento de la persona son requeridos");
			curp = "";
		}
		if(curp.length > 18)
			curp = "NA";
		return place.value = curp;	
}//end generateCURP()

/*
* @parameter who, help to know what information is neccesary to read in order to generate the RFC
*/
function knowWhoByForm(who, form){
		/*var data=new Array(6); 		
		var targetName = $(who+'.firstName');
		var targetFLastName = $(who+'.fatherLastName');
		var targetMLastName = $(who+'.motherLastName');
		var targetBirthDate = $(who+'.dateOfBirthString');
		var targetBirthState = $(who+'.stateOfBirthId');
		var targetGenderId = $(who+'.genderId')*/
	var data=new Array(6);
	var targetName = $(who+'.nombre');
	var targetFLastName = $(who+'.apellidoPaterno');
	var targetMLastName = $(who+'.apellidoMaterno');
	var targetBirthDate = $(who+'.fechaNacimiento');
	var targetBirthState = $(who+'.stateOfBirthId');
	var targetGenderId = $(who+'.genderId')	
	//Estos datos son s�lo para generar el RFC, en caso de necesitar CURP se deber�n implementar los otros campos
	data[0]= who.nombre.value;/*firstName*/
	data[1]= who.apellidoPaterno.value;/*lastName*/
	data[2]= who.apellidoMaterno.value;/*secondLastName*/
	data[3]= who.fechaNacimiento.value;/*birthDate*/
		//data[0]= targetName.value;/*firstName*/
		//data[1]= targetFLastName.value;/*lastName*/
		//data[2]= targetMLastName.value;/*secondLastName*/
		//data[3]= targetBirthDate.value;/*birthDate*/		
		/*data[4]= targetBirthState.options[targetBirthState.selectedIndex].value;/*birthState
		data[5]= targetGenderId.options[targetGenderId.selectedIndex].value;/*gender*/

		return data;
}//end knowWhoByForm()




/*
* @parameter personData, array with the information to generate RFC and CURP
* @parameter type, is the flag  to know how to behave: 'curp' or 'rfc'
*/
function evaluateNotNull(personData, type){
		var data = new Array(6); 
		var dataLength;
		var i=0;
		var valid = true;
		var tmpField = '';
		
		data = personData;
		dataLength = data.length;
		
		if (type != 'curp'){
			dataLength = 4;
		}
		
		for(i=0;i<dataLength;i++){
			//eval('tmpField = ' + data[i]);
			tmpField = data[i];
			if(tmpField == null || tmpField.length == 0){
				valid = false;
			}
		}		
		return valid;	
}//end evaluateNotNull()
