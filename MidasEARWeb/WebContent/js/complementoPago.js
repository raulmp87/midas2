//COMPLEMENTO PAGO FUNCTIONS//
function buscarPorFiltros(){

   var claveAgente = jQuery("#claveAgente").val();
   var rfc = jQuery("#clienteRfc").val();
   var poliza = jQuery("#numPolizaComplemento").val();


  if (claveAgente=="" && rfc=="" && poliza=="") {
    if (validarFechasComplementoPago()) {
      mostrarGrid();
    }else {
      alert("Es necesario ingresar al menos: RFC, POLIZA, CLAVE DE AGENTE O FECHA EXPEDICION INICIAL Y FECHA EXPEDICION FINAL");
    }
   }else {
      mostrarGrid();
   }
}

function validarFechasComplementoPago(){

  var fechaExpedicionInicial = jQuery("#fechaExpedicionInicial").val();
  var fechaExpedicionFin = jQuery("#fechaExpedicionFinal").val();

  if (fechaExpedicionInicial==""&&fechaExpedicionFin=="") {
    return false;
  }else {
    if (compare_dates_period(fechaExpedicionInicial,fechaExpedicionFin)){
      alert("El periódo entre la fecha de expedicion inicial y la fecha de expedicion final no debe ser mayor a 1 año. Verificar.")
      return false;
    }else if (compare_dates(fechaExpedicionInicial,fechaExpedicionFin)) {
      alert("Error en las fechas")
      return false;
    }else{
      return true;
    }
  }


}

function ejecutarProceso(){
  sendRequest(null,'/MidasWeb/cobranza/pagos/complementopago/execService.action' ,null, null);
}

function envioNotificacion(){
	sendRequest(null,'/MidasWeb/cobranza/pagos/complementopago/execNotificacion.action' ,null, null);
}

//metodo que muestra el grid
function mostrarGrid(){
  var tipoAccion = '<s:property value="tipoAccion"/>';
  var varModal = 'CPModal';
  var idFieldAgente = '<s:property value ="idField"/>';
  urlFil = listarGridVacioPath;
  listarFiltradoGenerico(urlFil, "complementoGrid", jQuery("#complementoForm"), idFieldAgente, varModal);
}

function exportarPDF(folioFiscal){
  var url='/MidasWeb/cobranza/pagos/complementopago/exportarPDF.action?folioFiscal='+folioFiscal;
	window.open(url, "PDF");
}

function exportarXML(folioFiscal){
	var url='/MidasWeb/cobranza/pagos/complementopago/exportarXML.action?folioFiscal='+folioFiscal;
	window.open(url, "XML");
}
//FIN COMPLEMENTO PAGO FUNCTIONS//

//MONITOR COMPLEMENTO PAGO//
function validarFechasErrores(){
     var fechaPagoInicio = jQuery("#fechaPagoInicio").val();
     var fechaPagoFin = jQuery("#fechaPagoFin").val();

     if (fechaPagoInicio==""&&fechaPagoFin=="") {
       alert("Debes ingresar las fechas requeridas.")
       return false;
     }else {
       if (compare_dates_period(fechaPagoInicio,fechaPagoFin)){
         alert("El periódo entre la fecha de pago inicial y la fecha de pago final no debe ser mayor a 1 año. Verificar.")
         return false;
       }else if (compare_dates(fechaPagoInicio,fechaPagoFin)) {
         alert("Error en las fechas")
         return false;
       }else{
         return true;
       }
     }
}

function buscarErroresPorFiltros(){
  if (validarFechasErrores()) {
    urlFil = listarGridErrores;
    listarFiltradoGenerico(urlFil, "monitorCPGrid", jQuery("#monitorForm"), null, null);
  }
}


//FIN MONITOR COMPLEMENTO PAGO//
