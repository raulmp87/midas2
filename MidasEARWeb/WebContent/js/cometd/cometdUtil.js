/**
 * Nos sirve para tener un control sobre las suscripciones que estan activas en la aplicación y evitar que se pueda registrar mas una suscripcion
 * por canal, este caso podría suceder cuando una persona recarga varias veces una pagina usando AJAX, en este caso el evento window unload no se ejecuta
 * lo cual ocasionaria problemas.
 */
var cometdUtil = {
		subscriptions:{},
		subscribe: function (channel, callback) {
			this.unsubscribe(channel);
			var cometdSubscription = cometd.subscribe(channel, callback);
			this.subscriptions[channel] = {cometdSubscription: cometdSubscription, callback: callback};
			return cometdSubscription;
		},
		unsubscribe: function(channel) {
			var subscription = this.subscriptions[channel]
			if (subscription) {
				delete this.subscriptions[channel];
				cometd.unsubscribe(subscription.cometdSubscription);
			}
		},
		handshakeResubscribe: function() {
			var oldSubscriptions = this.subscriptions;
			this.subscriptions = {};
			//TODO:Investigar como podemos implementar batch aqui. Esta apareciendo el siguiente error:
			//TypeError: this.subscribe is not a function
			/*
				try
				{
					delegate.method.call(delegate.scope);
					this.endBatch();
				}
				catch (x)
				{
					this._debug('Exception during execution of batch', x);
					this.endBatch();
					throw x; 
			 */
//       		cometd.batch(function() {
       			for (var channel in oldSubscriptions) {
       				if (oldSubscriptions.hasOwnProperty(channel)) {
       					var oldSubscription = oldSubscriptions[channel];
           				this.subscribe(oldSubscription.cometdSubscription[0], oldSubscription.callback);
       				}
       			}  			
//       		});
		}
};