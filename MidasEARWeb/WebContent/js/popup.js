isIE = document.all;
isNN = !document.all && document.getElementById;
isN4 = document.layers;
isHot = false;
windowPopUp = null;

// (C) Stephen Daly
// www.stephendaly.org
// Date: 11/3/2008

// Checks if the browsers is IE or another.
// document.all will return true or false depending if its IE
// If its not IE then it adds the mouse event
// if (!document.all)
// document.captureEvents(Event.MOUSEMOVE)

// On the move of the mouse, it will call the function getPosition
// document.onmousemove = getPosition;

// These varibles will be used to store the position of the mouse
var X = 0
var Y = 0

// This is the function that will set the position in the above varibles
function getPosition(args) {
	// Gets IE browser position
	if (document.all) {
		X = event.clientX + document.body.scrollLeft
		Y = event.clientY + document.body.scrollTop
	}

	// Gets position for other browsers
	else {
		X = args.pageX
		Y = args.pageY
	}

	alert("X " + X);
	alert("Y " + Y);
}
function backgroundFilter(id) {
	var div;
	if (document.getElementById)
		// Standard way to get element
		div = document.getElementById(id);
	else if (document.all)
		// Get the element in old IE's
		div = document.all[id];

	// if the style.display value is blank we try to check it out here
	if (div.style.display == '' && div.offsetWidth != undefined
			&& div.offsetHeight != undefined) {
		div.style.display = (div.offsetWidth != 0 && div.offsetHeight != 0) ? 'block'
				: 'none';
	}

	// If the background is hidden ('none') then it will display it ('block').
	// If the background is displayed ('block') then it will hide it ('none').
	div.style.display = (div.style.display == '' || div.style.display == 'block') ? 'none'
			: 'block';
}
function backgroundFilterFromChild(id) {
	var div;
	if (parent.document.getElementById)
		// Standard way to get element
		div = parent.document.getElementById(id);
	else if (parent.document.all)
		// Get the element in old IE's
		div = parent.document.all[id];

	// if the style.display value is blank we try to check it out here
	if (div.style.display == '' && div.offsetWidth != undefined
			&& div.offsetHeight != undefined) {
		div.style.display = (div.offsetWidth != 0 && div.offsetHeight != 0) ? 'block'
				: 'none';
	}

	// If the background is hidden ('none') then it will display it ('block').
	// If the background is displayed ('block') then it will hide it ('none').
	div.style.display = (div.style.display == '' || div.style.display == 'block') ? 'none'
			: 'block';
}
function popUp(id, l, t) {
	var div;
	if (document.getElementById)
		// Standard way to get element
		div = document.getElementById(id);
	else if (document.all)
		// Get the element in old IE's
		div = document.all[id];

	// if the style.display value is blank we try to check it out here
	if (div.style.display == '' && div.offsetWidth != undefined
			&& div.offsetHeight != undefined) {
		div.style.display = (div.offsetWidth != 0 && div.offsetHeight != 0) ? 'block'
				: 'none';
	}

	// If the PopUp is hidden ('none') then it will display it ('block').
	// If the PopUp is displayed ('block') then it will hide it ('none').
	div.style.display = (div.style.display == '' || div.style.display == 'block') ? 'none'
			: 'block';

	if (div.style.display == 'block')
		initDrag(id);
	else
		finishDrag(id);

	// Off-sets the X position by 15px
	// X = X + 15;
	// var l = 145;
	// var t = 105;

	// Sets the position of the DIV
	div.style.left = l + 'px';
	div.style.top = t + 'px';
}
function popUpFromChild(id, l, t) {
	var div;
	if (parent.document.getElementById)
		// Standard way to get element
		div = parent.document.getElementById(id);
	else if (parent.document.all)
		// Get the element in old IE's
		div = parent.document.all[id];

	// if the style.display value is blank we try to check it out here
	if (div.style.display == '' && div.offsetWidth != undefined
			&& div.offsetHeight != undefined) {
		div.style.display = (div.offsetWidth != 0 && div.offsetHeight != 0) ? 'block'
				: 'none';
	}

	// If the PopUp is hidden ('none') then it will display it ('block').
	// If the PopUp is displayed ('block') then it will hide it ('none').
	div.style.display = (div.style.display == '' || div.style.display == 'block') ? 'none'
			: 'block';

	if (div.style.display == 'block')
		initDrag(id);
	else
		finishDrag(id);

	// Off-sets the X position by 15px
	// X = X + 15;
	// var l = 145;
	// var t = 105;

	// Sets the position of the DIV
	div.style.left = l + 'px';
	div.style.top = t + 'px';
}
function closeWindow(id, l, h) {
	popUp(id, l, h);
	//collapse();
	var obj = $('searchBackground');
	if (obj !== null && obj.style.display === 'block') {
		backgroundFilter("searchBackground");
	}
}
/*
 * ============================================================== Script:
 * Amazing Draggable Layer
 * 
 * Functions: This script implements a draggable layer that can be used much
 * like a popup window... but with- out the usual focus problems that popups
 * often imply. Also included are simple controls to show or hide the draggable
 * layer. Compatible with NS4-7 & IE.
 * 
 * Comments: The script is in two parts. A JavaScript <script> to be placed in
 * the <head> of the page; and a <div> layer that should be placed either
 * immediately after the <body> tag or immediately before the </body> tag.
 * 
 * There are *no* changes or setups required in the JavaScript script.
 * 
 * Positioning, width, height, colors, fonts, etc., as well as initial
 * visibility are set in the layer portion of the script, in the body.
 * 
 * Notes: Only a single instance of the script is allowed per page.
 * 
 * Browsers: NS4-7 & IE4 and later
 * 
 * Author: etLux ==============================================================
 */

function ddInit(e) {
	topDog = isIE ? "BODY" : "HTML";
	whichDog = isIE ? eval("document.all." + windowPopUp) : document
			.getElementById(windowPopUp);
	hotDog = isIE ? event.srcElement : e.target;
	while (hotDog.id != "topLeft" && hotDog.tagName != topDog) {
		hotDog = isIE ? hotDog.parentElement : hotDog.parentNode;
	}
	if (hotDog.id == "topLeft") {
		offsetx = isIE ? event.clientX : e.clientX;
		offsety = isIE ? event.clientY : e.clientY;
		nowX = parseInt(whichDog.style.left);
		nowY = parseInt(whichDog.style.top);
		ddEnabled = true;
		document.onmousemove = dd;
	}
}

function dd(e) {
	if (!ddEnabled)
		return;
	whichDog.style.left = isIE ? nowX + event.clientX - offsetx : nowX
			+ e.clientX - offsetx;
	whichDog.style.top = isIE ? nowY + event.clientY - offsety : nowY
			+ e.clientY - offsety;
	return false;
}

function ddN4(whatDog) {
	if (!isN4)
		return;
	N4 = eval(whatDog);
	N4.captureEvents(Event.MOUSEDOWN | Event.MOUSEUP);
	N4.onmousedown = function(e) {
		N4.captureEvents(Event.MOUSEMOVE);
		N4x = e.x;
		N4y = e.y;
	}
	N4.onmousemove = function(e) {
		if (isHot) {
			N4.moveBy(e.x - N4x, e.y - N4y);
			return false;
		}
	}
	N4.onmouseup = function() {
		N4.releaseEvents(Event.MOUSEMOVE);
	}
}

function hideMe() {
	if (isIE || isNN)
		whichDog.style.visibility = "hidden";
	else if (isN4)
		document.theLayer.visibility = "hide";
}

function showMe() {
	if (isIE || isNN)
		whichDog.style.visibility = "visible";
	else if (isN4)
		document.theLayer.visibility = "show";
}

function initDrag(id) {
	windowPopUp = id;
	document.onmousedown = ddInit;
	document.onmouseup = Function("ddEnabled=false");
}

function finishDrag(id) {
	windowPopUp = null;
	document.onmousedown = ''
	document.onmouseup = ''
}
