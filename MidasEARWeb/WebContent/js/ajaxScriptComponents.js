/**
Funciones para TreeView
**/

var GLOBAL_VER_ELEMENTOS_INACTIVOS = false;

var GLOBAL_NEGOCIO = "D";

function dhtmlXTreeFromHTML(obj){
	if (typeof(obj)!="object")
	 obj=document.getElementById(obj);
	var n=obj;var id=n.id;
	var cont="";
	for (var j=0;j<obj.childNodes.length;j++)
		if (obj.childNodes[j].nodeType=="1"){
			if (obj.childNodes[j].tagName=="XMP"){
				var cHead=obj.childNodes[j];
				for (var m=0;m<cHead.childNodes.length;m++)cont+=cHead.childNodes[m].data
			}else if (obj.childNodes[j].tagName.toLowerCase()=="ul")
				cont=dhx_li2trees(obj.childNodes[j],new Array(),0);
				break;
			};
			obj.innerHTML="";
			var t=new dhtmlXTreeObject(obj,"100%","100%",0);
			var z_all=new Array();
			for ( b in t )z_all[b.toLowerCase()]=b;
			var atr=obj.attributes;
			for (var a=0;a<atr.length;a++)
				if ((atr[a].name.indexOf("set")==0)||(atr[a].name.indexOf("enable")==0)){
					var an=atr[a].name;
					if (!t[an])an=z_all[atr[a].name];
					t[an].apply(t,atr[a].value.split(","));
				};if (typeof(cont)=="object"){
					t.XMLloadingWarning=1;
					for (var i=0;i<cont.length;i++){
						var n=t.insertNewItem(cont[i][0],cont[i][3],cont[i][1]);
						if (cont[i][2])t._setCheck(n,cont[i][2])
						};
						t.XMLloadingWarning=0;t.lastLoadedXMLId=0;
						t._redrawFrom(t)
					}else
						t.loadXMLString("<tree id='0'>"+cont+"</tree>");
				window[id]=t;
				return t
};

function dhx_init_trees(){
	var z=document.getElementsByTagName("div");
	for (var i=0;i<z.length;i++)
		if (z[i].className=="dhtmlxTree")dhtmlXTreeFromHTML(z[i])
};
	
function dhx_li2trees(tag,data,ind){
	for (var i=0;i<tag.childNodes.length;i++){
		var z=tag.childNodes[i];
		if ((z.nodeType==1)&&(z.tagName.toLowerCase()=="li")){
			var c="";var ul=null;var check=z.getAttribute("checked");
			for (var j=0;j<z.childNodes.length;j++){
				var zc=z.childNodes[j];
				if (zc.nodeType==3)c+=zc.data;
				else if (zc.tagName.toLowerCase()!="ul") c+=dhx_outer_html(zc);
				else ul=zc};data[data.length]=[ind,c,check,(z.id||(data.length+1))];
				if (ul)data=dhx_li2trees(ul,data,(z.id||data.length))}};
				return data
};

function dhx_outer_html(node){
	if (node.outerHTML)return node.outerHTML;
	var temp=document.createElement("DIV");
	temp.appendChild(node.cloneNode(true));temp=temp.innerHTML;
	return temp
};
if (window.addEventListener)window.addEventListener("load",dhx_init_trees,false);
else if (window.attachEvent)window.attachEvent("onload",dhx_init_trees);
	

function doLog(str){
	var log = document.getElementById("catalogo");
	log.innerHTML = log.innerHTML+str+"</br>"
	log.scrollTop = log.scrollHeight;
}
function tonclick(id){
	creaTreeItemLinkConfProd(id);
};

function tonclickfacultativo(id){
	creaTreeItemLinkConfFacultativa(id);
};

function tondblclick(id){
	doLog("Item "+tree.getItemText(id)+" was doubleclicked");
};			
function tondrag(id,id2){
	return confirm("Do you want to move node "+tree.getItemText(id)+" to item "+tree.getItemText(id2)+"?");
};
function tonopen(id,mode){
	return confirm("Do you want to "+(mode>0?"close":"open")+" node "+tree.getItemText(id)+"?");
};

function limpiarDiv(divName){
	var divObj = document.getElementById(divName);
	if (divObj != null)
		divObj.innerHTML='';
}

function toncheck(id,state){
	doLog("Item "+tree.getItemText(id)+" was " +((state)?"checked":"unchecked"));
};


function refreshTree () {
	refreshTree(null, false);
}


function refreshTree (claveNegocio, verInactivos) {
	
//	if (claveNegocio != null && claveNegocio != undefined) {
//		claveNegocio = "'" + claveNegocio + "'";
//	}
	if (claveNegocio == null && GLOBAL_NEGOCIO != null) {
		claveNegocio = GLOBAL_NEGOCIO;
	}
	sendRequest(null,  '/MidasWeb/sistema/configuracion/listar.do' ,  "contenido" , "cargandoTree('configuracionProducto','treeboxbox_tree','" + claveNegocio + "'," + verInactivos + ")");
}


function cargandoTree(contextoMenu, treeBoxId){
	cargandoTree(contextoMenu, treeBoxId, null, false);
}
var tree = null;
function cargandoTree(contextoMenu, treeBoxId, claveNegocio, verInactivos ){
	tree=new dhtmlXTreeObject(treeBoxId,"100%","100%",0);
	tree.setImagePath("/MidasWeb/img/csh_winstyle/");
	tree.enableCheckBoxes(0);
	tree.enableDragAndDrop(0);
		
	tree.attachEvent("onXLS",function(id,m){
		mostrarIndicadorCargaComps();
	}); 
    tree.attachEvent("onXLE",function(id,m){ 
    	ocultarIndicadorCargaComps();
    });
	
	/**
		El manejador de este evento podria hacerse variable dependiendo del
		contexto
	**/
	if (contextoMenu == 'configuracionProducto') {//Producto
			
		if(verInactivos == null) {
			verInactivos = false;
		}
		
		GLOBAL_NEGOCIO = claveNegocio;
		
		if (jQuery("#verInactivos").val() == null || jQuery("#verInactivos").val() == undefined) {
			jQuery("#divSuperior").html("");
			jQuery("#divSuperior").html("<div style='font-size:10px;'>Mostrar Inactivos<input type='checkbox' onclick=\"refreshTree('" + claveNegocio +"'," + !verInactivos + ");\" id='verInactivos' name='verInactivos' /></div>");
		}
		jQuery("#divSuperior").show();
		
		jQuery("#verInactivos")[0].checked = verInactivos;
		
		var sVerInactivos = (jQuery("#verInactivos")[0].checked?"true":"false");
		
		GLOBAL_VER_ELEMENTOS_INACTIVOS = verInactivos;
		
		tree.setOnClickHandler(tonclick);
		
		/** Otros eventos
			tree.setOnOpenHandler(tonopen);
			tree.attachEvent("onOpenEnd",function(nodeId, event){doLog("An id of open item is "+nodeId);});
			tree.setOnCheckHandler(toncheck);
			tree.setOnDblClickHandler(tondblclick);
			tree.setDragHandler(tondrag);
		**/
		tree.enableMultiLineItems("true");
		tree.setXMLAutoLoading("/MidasWeb/configuracion/producto/poblarTreeView.do?menu=" + contextoMenu + "&negocio=" + claveNegocio + "&verInactivos=" + sVerInactivos);
		tree.loadXML("/MidasWeb/configuracion/producto/poblarTreeView.do?menu=" + contextoMenu + "&negocio=" + claveNegocio + "&verInactivos=" + sVerInactivos + "&id=root");
	} else if (contextoMenu == 'configuracionTarifaA') {//Tarifas Autos
		GLOBAL_NEGOCIO = claveNegocio;
		tree.setOnClickHandler(tonclickTarifa);
		tree.loadXML("/MidasWeb/img/TreeTarifasAuto.xml");
    }  else if (contextoMenu == 'configuracionTarifaD') {//Tarifas Danios
    	GLOBAL_NEGOCIO = claveNegocio;
		tree.setOnClickHandler(tonclickTarifa);
		tree.loadXML("/MidasWeb/img/TreeTarifas.xml");
    } else if (contextoMenu == 'configuracionFacultativa0' || contextoMenu == 'configuracionFacultativa1' ||
    		   contextoMenu == 'configuracionFacultativa2' || contextoMenu == 'configuracionFacultativa3' ||
    		   contextoMenu == 'configuracionFacultativa4') {//Facultativo
        tree.setOnClickHandler(tonclickfacultativo);
		tree.enableMultiLineItems(false);
	 	tree.setXMLAutoLoading("/MidasWeb/configuracion/producto/poblarTreeView.do?menu=" + contextoMenu.substr(0,24));//dependiendo el contexto carga el arbol
		var estatus = "";
	 	if (contextoMenu.substr(24,25) == "0"){
	  	      estatus = "estatus=0";
	  	      mostrarTituloFacultativo(0);
		   }
		if (contextoMenu.substr(24,25) == "1"){
			  estatus = "estatus=1";
			  mostrarTituloFacultativo(1);
		   }
		if (contextoMenu.substr(24,25) == "2"){
			  estatus = "estatus=2";
			  mostrarTituloFacultativo(2);
		  }
		if (contextoMenu.substr(24,25) == "3"){
			  estatus = "estatus=3";
			  mostrarTituloFacultativo(3);
		  }
		if (contextoMenu.substr(24,25) == "4"){
			  estatus = "estatus=4";
			  mostrarTituloFacultativo(4);
		  }
 		tree.loadXML("/MidasWeb/configuracion/producto/poblarTreeView.do?menu=" + contextoMenu.substr(0,24) + "&id=root&"+ estatus);
	}

};

function mostrarIndicadorCargaComps(){
		var newHtml = '<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Cargando la informaci&oacute;n, espere un momento por favor...</font>';
		if (document.getElementById('loadingIndicatorComps') !== null){
			if (document.getElementById('loadingIndicatorComps').innerHTML === ''){
				document.getElementById('loadingIndicatorComps').innerHTML = newHtml;
				document.getElementById('loadingIndicatorComps').style.display='block';
			}
		}
}
function ocultarIndicadorCargaComps(){
	if(document.getElementById('loadingIndicatorComps') !== null)
		document.getElementById('loadingIndicatorComps').style.display='none';
}

function creaTreeItemLinkConfProd(id){
 	var divDestino = 'configuracion_detalle';
	var idEntidad = "0";
	var idPadre = "0";
	var nivelOrigen = "root";
	if (id != "root") {
		nivelOrigen = id.substr(0,id.lastIndexOf("_"));
		idEntidad = id.substr(id.lastIndexOf("_") + 1, id.lastIndexOf("(") - (id.lastIndexOf("_") + 1));
		idPadre = id.substr(id.lastIndexOf("(") + 1, id.lastIndexOf(")") - (id.lastIndexOf("(") + 1));
		var url = "/MidasWeb/configuracion/" + nivelOrigen + "/mostrarDetalle.do?id=" + idEntidad + "&idPadre=" + idPadre;
		var nextFunction = 'dhx_init_tabbars(); cargaDataGrid'+nivelOrigen+'Hijos(\'' + idEntidad + '\',\'' + idPadre + '\');'
		sendRequest(null,  url ,  divDestino , nextFunction);
	}	
}

function creaTreeItemLinkConfFacultativa(id){
   	var divDestino = 'configuracion_detalle';
	var idEntidad = "0";
	var idPadre = "0";
	var nivelOrigen = "root";
	  if (id != "root" && id.substr(0,10) != "cotizacion" ) {
	 	  nivelOrigen = id.substr(0,id.lastIndexOf("_"));
		  idEntidad = id.substr(id.lastIndexOf("_") + 1, id.lastIndexOf("(") - (id.lastIndexOf("_") + 1));
		  idPadre = id.substr(id.lastIndexOf("(") + 1, id.lastIndexOf(")") - (id.lastIndexOf("(") + 1));
		  var url = "/MidasWeb/contratofacultativo/" + nivelOrigen + "/mostrarDetalle.do?id=" + idEntidad + "&idPadre=" + idPadre;
		  if (nivelOrigen == "cobertura"){
			   sendRequest(null,  url ,  divDestino , 'mostrarGridParticipacionFacultativo('+idEntidad+'),formatearMontosRegistrarCotizacionFacultativa(),mostrarGridPlanPagosFacultativos('+idEntidad+')');
		   }else if (nivelOrigen == "slip"){ 
			sendRequest(null,  url + "creaCalendars('fechaContraroFacultativo,dobleCal#mCalIzq#mCalDer#btnDobleCal','SD')"  ,  divDestino , 'dhx_init_tabbars()');  
  		  }	else{
  			sendRequest(null,  url ,  divDestino , null);  
  		  }
  	  } 
}

var treeAction;
function recargaCoberturasFacultativaTree(idTmContratoFacultativo, idTdContratoFacultativo){
	var node = "cobertura_"+idTdContratoFacultativo+"("+idTmContratoFacultativo+")";
	var parent = tree.getParentId(node);
	var grandParent = tree.getParentId(parent);
	if(parent != null && parent != undefined && parent != '' && grandParent != null && grandParent != undefined && grandParent != ''){
		tree.refreshItem(grandParent);
		treeAction = "refreshGrandParent";
		tree.attachEvent("onXLE",function(id){
			if (treeAction == "refreshGrandParent"){
				tree.refreshItem(parent);
				treeAction = "refreshNode";
			}else{
				if (treeAction == "refreshParent"){
					if (parent)
						tree.openItem(parent);
					treeAction = "success";
				}
			}
		});
	}
}

function tonclickTarifa(id){

	var divDestino = "configuracion_detalle";
	var nivelOrigen = "root";

 	if (id != "root") {
		nivelOrigen = id.substr(0,id.lastIndexOf("_"));

		if (nivelOrigen == "Concepto") {

			sendRequestJQTarifa(null,  "/MidasWeb/tarifaversion/mostrarContenedor.action" ,  divDestino , "blockPage();iniciaTarifas('" + id.substr(9,id.length) +"');");

		}
	}
};

/**
Fin de Funciones para TreeView
**/

/**
 * Funciones para Accordion
 */
function cargandoAccordion(){
	dhxAccord = new dhtmlXAccordion("accordObj");
	dhxAccord.addItem("a1", "Catalogos tipo a");
	dhxAccord.addItem("a2", "Catalogos tipo b");
	dhxAccord.addItem("a3", "Catalogos tipo c");
	dhxAccord.cells("a1").attachObject("catalogos1");
	dhxAccord.cells("a2").attachObject("catalogos2");
	dhxAccord.cells("a3").attachObject("catalogos3");
	dhxAccord.openItem("a1");
	dhxAccord.setEffect(true);
	
	dhx_init_tabbars();
	
}

/**
 * Fin de Funciones para Accordion
 */

/**
 * Funciones para Menu
 */
function escribeMenu () {
	
//	<div class="boton">
//	<div class="leyendaDeBoton">
//	<script type="text/javascript">alert("jalo");</script>
//		<table id="botonesMenu" class="botonesMenu">
//			<tr>
//				<td>
//					<a href="javascript: void(0);" id="menu_1" onclick="javascript: sendRequest(null,'/MidasWeb/sistema/catalogos/listar.do', 'contenido', 'cargandoAccordion()'); setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');">Cat&aacute;logos</a>												
//				</td>
//				<td>
//					<a href="javascript: void(0);" id="menu_2" onclick="javascript: sendRequest(null,'/MidasWeb/sistema/configuracion/listar.do', 'contenido', 'cargandoTree(\'configuracionProducto\',\'treeboxbox_tree\')');setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');">Configuraci&oacute;n</a>
//				</td>
//				<td>
//					<a href="javascript: void(0);" id="menu_3" onclick="javascript: sendRequest(null,'/MidasWeb/sistema/siniestros/listar.do', 'contenido', null);setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');">Siniestros</a>
//				</td>
//				<td>
//					<a href="javascript: void(0);" id="menu_4" onclick="javascript: sendRequest(null,'/MidasWeb/sistema/contratos/listar.do', 'contenido', null);setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');">Contratos</a>
//				</td>
//				<td>
//					<a href="javascript: void(0);" id="menu_5" onclick="javascript: mesajeGlobal('')">Mensaje Error</a>
//				</td>									
//			</tr>
//		</table>
//	</div>
//</div>
	
	var menuOpcion1 = new Array(3); 
	menuOpcion1[0] = "menu_1";
	menuOpcion1[1] = "javascript: sendRequest(null,'/MidasWeb/sistema/catalogos/listar.do', 'contenido', 'cargandoAccordion()'); setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');";
	menuOpcion1[2] = "Cat&aacute;logos";
	
	var menuOpcion2 = new Array(3); 
	menuOpcion2[0] = "menu_2";
	menuOpcion2[1] = "javascript: sendRequest(null,'/MidasWeb/sistema/configuracion/listar.do', 'contenido', 'cargandoTree(\\\'configuracionProducto\\\',\\\'treeboxbox_tree\\\')');setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');";
	menuOpcion2[2] = "Configuraci&oacute;n";
	
	var menuOpcion3 = new Array(3); 
	menuOpcion3[0] = "menu_3";
	menuOpcion3[1] = "javascript: sendRequest(null,'/MidasWeb/sistema/siniestros/listar.do', 'contenido', null);setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');";
	menuOpcion3[2] = "Siniestros";
	
	var menuOpcion4 = new Array(3); 
	menuOpcion4[0] = "menu_4";
	menuOpcion4[1] = "javascript: sendRequest(null,'/MidasWeb/sistema/contratos/listar.do', 'contenido', null);setMenuStyle('ulMenuPrincipal', this, 'selectedMenu');";
	menuOpcion4[2] = "Contratos";
	
	var menuOpcion5 = new Array(3); 
	menuOpcion5[0] = "menu_5";
	menuOpcion5[1] = "javascript: mesajeGlobal('')";
	menuOpcion5[2] = "Mensaje Error";
	
	var menuOpciones = new Array(5);
	menuOpciones[0] = menuOpcion1;
	menuOpciones[1] = menuOpcion2;
	menuOpciones[2] = menuOpcion3;
	menuOpciones[3] = menuOpcion4;
	menuOpciones[4] = menuOpcion5;
	
	var target = document.getElementById("botonesMenu");
	var html = "<tr>"
	
	for ( var x = 0; x < menuOpciones.length; x++) {
		
		var menuOpcion = menuOpciones[x];
		
		html = html + "<td>";
		html = html + "<a href=\"javascript: void(0);\" ";
		html = html + "id = \""+ menuOpcion[0] +"\" ";
		html = html + "onclick = \""+ menuOpcion[1] +"\" ";
		html = html + ">"+ menuOpcion[2] +"</a>";
		html = html + "</td>";
	
	} // End of for	
	html = html + "</tr>";
	
	target.innerHTML = html;
	
}

function seleccionaItemMenu(menu, id) {
	if (menu.getUserData(id, "ejecutar") != null) {
		
		var parametrosAccion = trim(menu.getUserData(id, "ejecutar")).split("|");
				
		/**
		 * Los parametros son:
		 * 0. El url destino
		 * 1. El id del contenedor destino
		 * 2. La cadena con la siguiente funcion javascript a ejecutar (null si no hay tal)
		 */
		if(parametrosAccion[0] != "null") { 
			if (parametrosAccion[0].lastIndexOf(".action",parametrosAccion[0].lenght) != -1  ) {
				sendRequestJQ(null, parametrosAccion[0],parametrosAccion[1], parametrosAccion[2]);
			} else {
				sendRequest(null, parametrosAccion[0],parametrosAccion[1], parametrosAccion[2]);
			}
			
			
		} else {
			eval(parametrosAccion[2]);
		}
	}
}

function cargandoMenu() {
	
	var menu = new dhtmlXMenuObject("menuObj","clear_green");
	menu.setImagePath("/MidasWeb/img/"); //dhxmenu_clear_green/
	menu.setIconsPath("/MidasWeb/img/menu_icons/");
	menu.loadXML("/MidasWeb/cargaMenu.do");
	 menu.setWebModeTimeout(1500);
	// attaching onClick event handler
	menu.attachEvent("onClick", function(id) {
		seleccionaItemMenu(menu, id);
	});
	   
}

/**
 * Fin de Funciones para Menu
 */

/**
 * Funciones para Calendar
 */

function creaCalendars(calendarIds, calendarTypes) {
	
	/**
	 * calendarIds : cadena con los id de los calendarios a agregar (separados por coma ',')
	 * calendarTypes : cadena con el tipo de calendario a agregar [S: Sencillo, D: Doble (para rangos de fechas)]
	 * 					Estos deben coincidir en numero y orden con sus respectivos calendarIds.
	 */
	var calendars;
	
	calendars = calendarIds.split(',');
	calendarSingles = '';
	calendarDoubles = '';
	
	for (i=0;i<calendars.length;i++){
		if (calendarTypes!=null  && calendarTypes.length == calendars.length && calendarTypes.charAt(i) != null) {
			
			switch (calendarTypes.charAt(i).toUpperCase())
			{
				case 'S':
					calendarSingles = calendarSingles + calendars[i] + ',';
					break;
				case 'D':
					calendarDoubles = calendarDoubles + calendars[i] + ',';
					break;
				default:
					calendarSingles = calendarSingles + calendars[i] + ',';
			}
			
		} else { //default
			calendarSingles = calendarSingles + calendars[i] + ',';
		}
	}
	
	if (calendarSingles != '') {
		calendarSingles = calendarSingles.substring(0, calendarSingles.lastIndexOf(","));
		creaSingleCalendars(calendarSingles);
	}
	
	if (calendarDoubles != '') {
		calendarDoubles = calendarDoubles.substring(0, calendarDoubles.lastIndexOf(","));
		creaDoubleCalendars(calendarDoubles);
	}
	
	return true;
	
}

function creaSingleCalendars(calendarIds) {
	var form;
	var menu;
	var evento = "focus";	
	form = document.forms[0];
	menu = document.getElementById("menu");
	
	var calendars;
	calendars = calendarIds.split(',');
	
	var calendarObjects = new Array(calendars.length);
	
	for (i=0;i<calendars.length;i++){
		calendarObjects[i] = new dhtmlxCalendarObject(calendars[i], true, {isMonthEditable: true, isYearEditable: true});
	}
	
	for (j=0;j<form.elements.length;j++){
		if(document.all) { //ie
			form.elements[j].attachEvent("on" + evento, function(event){
				for (k=0;k<calendars.length;k++){
					if (event.srcElement != null) {
						if (calendarObjects[k].isVisible && calendars[k]!= event.srcElement.name) 
							calendarObjects[k].hide();
					}
				}
				return false;
			
			});
		} else { //W3C
			form.elements[j].addEventListener(evento, function(event){
				for (k=0;k<calendars.length;k++){
					if (calendarObjects[k].isVisible && calendars[k]!= event.target.name) 
						calendarObjects[k].hide();
				}
				return false;
			
			}, false);
		}
	}
	
	if(document.all) { //ie
		menu.attachEvent("onmouseover", function(event){
			
			for (k=0;k<calendars.length;k++){
				if (calendarObjects[k].isVisible) 
					calendarObjects[k].hide();
			}
			return false;
		
		});
	} else { //W3C
		menu.addEventListener("mouseover", function(event){
			for (k=0;k<calendars.length;k++){
				if (calendarObjects[k].isVisible) 
					calendarObjects[k].hide();
			}
			return false;
		
		}, false);
	}
	
}

function creaDoubleCalendars(vcalendarIds) {
	
	var vcalendars;
	vcalendars = vcalendarIds.split(',');
	
	var vcalendarObjects = new Array(vcalendars.length);
		
	for (i=0;i<vcalendars.length;i++){
		
		calElements = vcalendars[i].split('#');
				
		vcalendarObjects[i] = new dhtmlxDblCalendarObject(calElements[0], true, {isMonthEditable: true, isYearEditable: true});
		
		dobleCalendario = document.getElementById(calElements[0]);
		botonDobleCalendario = document.getElementById(calElements[3]);
		
		if(document.all) { //ie
			
			botonDobleCalendario.attachEvent("onClick", function(event){
				if (dobleCalendario.style.display =="block") {
					dobleCalendario.style.display =="none"
				} else {
					dobleCalendario.style.display =="block"
				}
				return false;
			});
			
			
		} else { //W3C
			
			botonDobleCalendario.addEventListener("click", function(event){
				if (dobleCalendario.style.display =="block") {
					dobleCalendario.style.display ="none"
				} else {
					dobleCalendario.style.display ="block"
				}
				return false;
			}, false);
			
		}
		
		vcalendarObjects[i].setOnClickHandler(function(date,cal, side){
			if (side == "left") {
				document.getElementById(calElements[1]).innerHTML = this.leftCalendar.getFormatedDate("%d/%m/%Y", date);
			}
			else {
				document.getElementById(calElements[2]).innerHTML = this.rightCalendar.getFormatedDate("%d/%m/%Y", date);
			}
		
		});
		
		
	}
	
}


/**
 * Fin de Funciones para Calendar
 */