function inicializarListaSiniestroMovil() {
	reporteSiniestroMovilGrid = new dhtmlXGridObject(
			"reporteSiniestroMovilGrid");
	
	if (paginationEnabled) {
		reporteSiniestroMovilGrid.enablePaging(true, 10, 10, "pagingArea", true, "infoArea");
		reporteSiniestroMovilGrid.setPagingSkin("bricks");
	}
	
	jQuery("#asignadosAMiCheckbox").click(function() {
		reporteSiniestroMovilGrid.clearAndLoad(reporteSiniestroMovilDhtmlxListUrl + "&asignadosAMi=" + jQuery("#asignadosAMiCheckbox").is(":checked") + "&clearAndLoad=true");
	});

	reporteSiniestroMovilGrid.init();
	
	reporteSiniestroMovilGrid.load(reporteSiniestroMovilDhtmlxListUrl);

	if (reporteSiniestroMovilListaEstatusId == 1) {
		
		var notificacionSound = soundManager.createSound({
		      url: config.baseUrl + '/audio/notificacion/sounds-960-no-trespassing.mp3'
		    });		
		cometd.batch(function() {
			var nuevoReporteChannel = "/siniestro/reporte-movil/nuevo/";
			var asignadoReporteChannel = "/siniestro/reporte-movil/asignado/";
			
			if (restringirCabina == "true") {
				nuevoReporteChannel += cabinaId; //subscribirse solo al canal con los nuevos reportes de la cabina que tiene el usuario.
				asignadoReporteChannel += cabinaId;
			} else {
				nuevoReporteChannel += "*";
				asignadoReporteChannel += "*";
			}
			
			cometdUtil.subscribe(nuevoReporteChannel, function(message) {
				if (jQuery('#reporteSiniestroMovilGrid').length == 1 && reporteSiniestroMovilListaEstatusId == 1) {
					var reporteSiniestroMovil = jQuery.parseJSON(message.data);
					var updateUrl = reporteSiniestroMovilDhtmlxListUrl + "&id="
							+ reporteSiniestroMovil.id;
					reporteSiniestroMovilGrid.updateFromXML(updateUrl);
					loopSound(notificacionSound, 7);
				} else {
					//Se cancela la suscripcion cuando se detecta que ya no esta el elemento de nuestro interes en el DOM.
					cometdUtil.unsubscribe(nuevoReporteChannel);
				}
			});
			cometdUtil.subscribe(asignadoReporteChannel,function(message) {
				if (jQuery('#reporteSiniestroMovilGrid').length == 1 && reporteSiniestroMovilListaEstatusId == 1) {
					var reporteSiniestroMovil = jQuery
							.parseJSON(message.data);
					reporteSiniestroMovilGrid
							.deleteRow(reporteSiniestroMovil.id);
				} else {
					//Se cancela la suscripcion cuando se detecta que ya no esta el elemento de nuestro interes en el DOM.
					cometdUtil.unsubscribe(asignadoReporteChannel);
				}
			});
		});
	}	
}