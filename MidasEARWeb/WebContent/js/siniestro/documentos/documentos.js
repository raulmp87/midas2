function validaFormaDocumentoSiniestro(forma){
	if(forma.nombreDocumento.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"El nombre del documento no puede estar vacio.", null);
	}else{
		mostrarArchivoDocumentoSiniestroWindow();
	}
}

function redireccionaCartaRechazoAprobada(){
//	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta('listarReportesSiniestro()');
}

function redireccionaCartaRechazoRechazada(){
//	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta('listarReportesSiniestro()');
}

function redireccionaCartaRechazoImpresa(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta('listarReportesSiniestro()');
}

function mostrarDocumentosAsociadosReporteSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do?idReporteSiniestro='+idToReporteSiniestro,'contenido',null);
}

function mostrarAgregarDocumentosSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/documentos/mostrarAgregarDocumentoSiniestro.do?idReporteSiniestro='+idToReporteSiniestro,'contenido',null);
}