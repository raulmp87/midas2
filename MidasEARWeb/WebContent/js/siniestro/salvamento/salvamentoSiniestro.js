/********************INICIO SALVAMENTO SINIESTRO*****************************/
var SALVAMENTO_SINIESTRO_AGREGAR=1;
var SALVAMENTO_SINIESTRO_MODIFICAR=2;
var SALVAMENTO_SINIESTRO_BORRAR=3;
var SALVAMENTO_SINIESTRO_DETALLE=4;
var SALVAMENTO_SINIESTRO_VENDER=5;

function mostrarSalvamentosPorReporteSiniestro(idToReporteSiniestro){
	if (idToReporteSiniestro){
		var url="/MidasWeb/siniestro/salvamento/listar.do?idToReporteSiniestro="+idToReporteSiniestro;
		sendRequest(null,url,'contenido',null);
	}
}

function habilitaCamposSalvamento(obj, idPorcentaje, idMontos){
	$(idPorcentaje).readOnly = !obj.checked;
	
	if (obj.checked){
		$(idPorcentaje).focus();
		$(idPorcentaje).select();
	}else{
		$(idPorcentaje).value = "0.0";		
		$(idMontos).value = "0.0";
	}
}

function calculaMontoPorcentajeSalvamentos(objPorcentaje, idCampoMonto){
	var valor = $('valorEstimado').value;
	valor = removeCurrency(valor);
	if (objPorcentaje && objPorcentaje.value && valor!='' && objPorcentaje.value>0.0 && objPorcentaje.value<=100.0){
		var montoPorcentaje = objPorcentaje.value*valor/100.0;
		$(idCampoMonto).value = formatCurrency(montoPorcentaje);
	}else{
		objPorcentaje.value="0.0";
		$(idCampoMonto).value = "$0.00";
	}
}


function calculaMontosSalvamentoSiniestro(objCantidad){	
	if (objCantidad.value==''){
		var tieneIVA = document.getElementsByName('tieneIVA')[0];
		var tieneIVARet = document.getElementsByName('tieneIVARet')[0];
		var tieneISR = document.getElementsByName('tieneISR')[0];
		var tieneISRRet = document.getElementsByName('tieneISRRet')[0];
		var tieneOtros = document.getElementsByName('tieneOtros')[0];
		tieneIVA.checked = false;
		tieneIVARet.checked = false;
		tieneISR.checked = false;
		tieneISRRet.checked = false;
		tieneOtros.checked = false;
		habilitaCamposSalvamento(tieneIVA, 'porcentajeIVA', 'montoIVA');
		habilitaCamposSalvamento(tieneIVARet, 'porcentajeIVARet', 'montoIVARet');
		habilitaCamposSalvamento(tieneISR, 'porcentajeISR', 'montoISR');
		habilitaCamposSalvamento(tieneISRRet, 'porcentajeISRRet', 'montoISRRet');
		habilitaCamposSalvamento(tieneOtros, 'porcentajeOtros', 'montoOtros');
	}else{
		calculaMontoPorcentajeSalvamentos($('porcentajeIVA'),'montoIVA');
		calculaMontoPorcentajeSalvamentos($('porcentajeIVARet'),'montoIVARet');
		calculaMontoPorcentajeSalvamentos($('porcentajeISR'),'montoISR');
		calculaMontoPorcentajeSalvamentos($('porcentajeISRRet'),'montoISRRet');
		calculaMontoPorcentajeSalvamentos($('porcentajeOtros'),'montoOtros');
	}
}

function  tienePerecederosSalvamentoSiniestro(tienePerecederos){
	$('observaciones').readOnly = !tienePerecederos.checked; 
	if (tienePerecederos.checked){
		$('observaciones').focus();
		$('observaciones').select();
	}else{
		$('observaciones').value="";
	}
}

function borrarSalvamentoSiniestro(idToSalvamentoSiniestro){
	if (trim($('observaciones').value).length>0){
		sendRequest(document.salvamentoSiniestroForm,'/MidasWeb/siniestro/salvamento/borrar.do', 'contenido',null);
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar las observaciones para eliminar", null);
	}
}

function regresarSalvamentoSiniestroDesdeIngresos(){
	var varIdToSalvamentoSiniestro = $('idToSalvamentoSiniestro').value;
	mostrarVenderSalvamentoSiniestro(varIdToSalvamentoSiniestro, 'agregarIngresoSiniestro');
}

function inicializaComponentesSalvamentoSiniestro(formularioOrigen){
	creaCalendars('fechaProvision','S');
	calculaMontosSalvamentoSiniestro($('valorEstimado'));
	
	if (formularioOrigen==SALVAMENTO_SINIESTRO_MODIFICAR){
		habilitaTextoSalvamentoParaCheck('tieneIVA','porcentajeIVA');
		habilitaTextoSalvamentoParaCheck('tieneIVARet','porcentajeIVARet');
		habilitaTextoSalvamentoParaCheck('tieneISR','porcentajeISR');
		habilitaTextoSalvamentoParaCheck('tieneISRRet','porcentajeISRRet');
		habilitaTextoSalvamentoParaCheck('tieneOtros','porcentajeOtros');
		habilitaTextoSalvamentoParaCheck('tienePerecederos','observaciones');
	}	
	if (formularioOrigen==SALVAMENTO_SINIESTRO_AGREGAR){
		habilitaTextoSalvamentoParaCheck('tienePerecederos','observaciones');
	}
}

function habilitaTextoSalvamentoParaCheck(nameCheck,idTexto){
	var obj = document.getElementsByName(nameCheck)[0];
	$(idTexto).readOnly = !obj.checked;
}

function mostrarAgregarIngresosParaSalvamento(idToReporteSiniestro){
	var valor = $('valorVenta').value;
	valor = removeCurrency(valor);
	
	if (valor>0.0){
		sendRequest(document.salvamentoSiniestroForm,'/MidasWeb/siniestro/salvamento/guardarSalvamentoSession.do',null,
			'mostrarFormAgregarIngresosParaSalvamento('+idToReporteSiniestro+');');
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar el valor de venta", null);
	}
}

function mostrarFormAgregarIngresosParaSalvamento(idToReporteSiniestro){
	var valorVenta = $('valorVenta').value;
	sendRequest(document.salvamentoSiniestroForm,"/MidasWeb/siniestro/finanzas/ingresos.do?id="+idToReporteSiniestro
			,"contenido","inicializaComponentesAgregarIngresosParaSalvamento('"+valorVenta+"');");
}

function inicializaComponentesAgregarIngresosParaSalvamento(valorVenta){
	$('idMontoIngreso').value=valorVenta;
	creaCalendars('fechaCobro','S');
}

function mostrarVenderSalvamentoSiniestro(idToSalvamentoSiniestro, formularioOrigen){
	var url = "/MidasWeb/siniestro/salvamento/mostrarVender.do?idToSalvamentoSiniestro="
		+ idToSalvamentoSiniestro + "&formularioOrigen="+formularioOrigen;	
	sendRequest(document.salvamentoSiniestroForm, url,'contenido','inicializaComponentesSalvamentoSiniestro(SALVAMENTO_SINIESTRO_VENDER)');
}

function venderSalvamentoSiniestro(idToReporteSiniestro){
	var url='/MidasWeb/siniestro/salvamento/vender.do';
	sendRequestProcesaRespuestaXML(document.salvamentoSiniestroForm, url, 'contenido', 'mostrarSalvamentosPorReporteSiniestro('+idToReporteSiniestro+');');
}

/********************FIN SALVAMENTO SINIESTRO*****************************/