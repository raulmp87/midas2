function guardarIngresos(formIngresos){
	if(validaCamposRequeridos()){
		sendRequest(formIngresos,'/MidasWeb/siniestro/finanzas/guardarIngresosSinietro.do', 'contenido','redireccionaGuardarIngresos()');
	}
}

//Esta funcion es similar a guardar ingresos solo que redirecciona a salvamento
//favor de avisar si se realiza algun cambio para sincronizarlas
function guardarIngresosSalvamento(formIngresos){
	if(validaCamposRequeridos()){		
		sendRequest(formIngresos,"/MidasWeb/siniestro/finanzas/guardarIngresosSinietro.do", null,
				"mostrarVenderSalvamentoSiniestro(" + $('idToSalvamentoSiniestro').value + ", 'agregarIngresoSiniestro');"
		);
	}
}

function habilitaCamposPorcentaje(objCheck){
	var isChecado = objCheck.checked;
	var valor = objCheck.value;
	switch (parseInt(valor)){
	case 1:
		if(isChecado){
			$('porcentajeIva').disabled=false;
			$('porcentajeIva').focus();
			$('indIva').value = true;
		}else{
			$('porcentajeIva').disabled=true;
			$('porcentajeIva').value='';
			$('indIva').value = false;
			$('montoIva').value='';
		}
		break;
	case 2:
		if(isChecado){
			$('porcentajeIvaRetencion').disabled=false;
			$('porcentajeIvaRetencion').focus();
			$('indIvaRet').value = true;
		}else{
			$('porcentajeIvaRetencion').disabled=true;
			$('porcentajeIvaRetencion').value='';
			$('indIvaRet').value = false;
			$('montoIvaRetencion').value='';
		}
		break;
	case 3:
		if(isChecado){
			$('porcentajeIsr').disabled=false;
			$('porcentajeIsr').focus();
			$('indIsr').value = true;
		}else{
			$('porcentajeIsr').disabled=true;
			$('porcentajeIsr').value='';
			$('indIsr').value = false;
			$('montoIsr').value='';
		}
		break;
	case 4:
		if(isChecado){
			$('porcentajeIsrRetencion').disabled=false;
			$('porcentajeIsrRetencion').focus();
			$('indIsrRet').value = true;
		}else{
			$('porcentajeIsrRetencion').disabled=true;
			$('porcentajeIsrRetencion').value='';
			$('indIsrRet').value = false;
			$('montoIsrRetencion').value= '';
		}
		break;
	case 5:
		if(isChecado){
			$('porcentajeOtros').disabled=false;
			$('porcentajeOtros').focus();
			$('indOtros').value = true;
		}else{
			$('porcentajeOtros').disabled=true;
			$('porcentajeOtros').value='';
			$('indOtros').value = false;
			$('montoOtros').value= '';
		}
		break;
	}
}

function calculaMontoPorcentaje(campoPorcentaje, campo, campoMonto){
	valorCampoPorcentaje = campoPorcentaje.value;
	if(valorCampoPorcentaje != null && valorCampoPorcentaje != ''){
		if(valorCampoPorcentaje < 100){
			var porcentaje = parseFloat(valorCampoPorcentaje)/100;
			valorMonto = $(campoMonto).value;
			valorMonto = removeCurrency(valorMonto);
			if(valorMonto !== null && valorMonto !==''){
				$(campo).value =  formatCurrency(valorMonto * porcentaje);
			}else{
				mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el monto',null);
				$(campo).value ='';
			}
		}else{
			campoPorcentaje.value='';
		}
	}else{
		campoPorcentaje.value='';
		campoMonto.value='';
	}
}

function recalcularPorcentajes(valorMontoIngreso){
	var monto = removeCurrency(valorMontoIngreso);
	
	if(!$('porcentajeIva').disabled ){
		if($('porcentajeIva').value !=''){
			porcentajeIva = parseFloat($('porcentajeIva').value)/100;
			$('montoIva').value =  formatCurrency(monto * porcentajeIva);
		}else{
			$('montoIva').value ='';
		}
	}
	if(!$('porcentajeIvaRetencion').disabled){
		if($('porcentajeIvaRetencion').value !=''){
			porcentajeRetIva = parseFloat($('porcentajeIvaRetencion').value)/100;
			$('montoIvaRetencion').value =  formatCurrency(monto * porcentajeRetIva);
		}else{
			$('montoIvaRetencion').value ='';
		}
	}
	if(!$('porcentajeIsr').disabled){
		if($('porcentajeIsr').value !=''){
			porcentajeIsr = parseFloat($('porcentajeIsr').value)/100;
			$('montoIsr').value =  formatCurrency(monto * porcentajeIsr);
		}else{
			$('montoIsr').value ='';
		}
	}
	if(!$('porcentajeIsrRetencion').disabled){
		if($('porcentajeIsrRetencion').value !=''){
			porcentajeRetIsr = parseFloat($('porcentajeIsrRetencion').value)/100;
			$('montoIsrRetencion').value =  formatCurrency(monto * porcentajeRetIsr);
		}else{
			$('montoIsrRetencion').value ='';
		}
	}
	if(!$('porcentajeOtros').disabled ){
		if($('porcentajeOtros').value !=''){
			porcentajeOtros = parseFloat($('porcentajeOtros').value)/100;
			$('montoOtros').value =  formatCurrency(monto * porcentajeOtros);
		}else{
			$('montoOtros').value ='';
		}
	}
}

function validaCamposRequeridos(){
	if($('indIva').value == 'true'  && $('porcentajeIva').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA',null);
		$('porcentajeIva').focus();
		return false;
	}
	if($('indIvaRet').value == 'true' && $('porcentajeIvaRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA Retencion',null);
		$('porcentajeIvaRetencion').focus();
		return false;
	}
	if($('indIsr').value == 'true' && $('porcentajeIsr').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR',null);
		$('porcentajeIsr').focus();
		return false;
	}
	if($('indIsrRet').value == 'true' && $('porcentajeIsrRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR retencion',null);
		$('porcentajeIsrRetencion').focus();
		return false;
	}
	if($('indOtros').value == 'true' && $('porcentajeOtros').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % otros',null);
		$('porcentajeOtros').focus();
		return false;
	}
	if($('descripcionIngreso').value == null || $('descripcionIngreso').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar la descripcion del ingreso',null);
		return false;
	}
	return true;
}

// Funciones para Modifcar Ingresos
function seleccionaIndicadores(){
	if($('indIva').value == 'true'){
		$('porcentajeIva').disabled=false;
		$('idIndIva').checked=true;
	}
	if($('indIvaRet').value == 'true'){
		$('porcentajeIvaRetencion').disabled=false;
		$('idIndIvaRet').checked=true;
	}
	if($('indIsr').value == 'true'){
		$('porcentajeIsr').disabled=false;
		$('idIndIsr').checked=true;
	}
	if($('indIsrRet').value == 'true'){
		$('porcentajeIsrRetencion').disabled=false;
		$('idIndIsrRet').checked=true;
	}
	if($('indOtros').value == 'true'){
		$('porcentajeOtros').disabled=false;
		$('idIndOtros').checked=true;
	}
}

function guardarModificarIngresos(formIngresos){
	if(validaCamposRequeridosModificar()){
//		sendRequest(formIngresos,'/MidasWeb/siniestro/finanzas/guardarModificarIngresosSinietro.do', 'contenido','procesarRespuesta(\'sendRequest(null,\\\'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro='+$('idToReporteSiniestro').value+'\\\', \\\'contenido\\\',null);\');');
		sendRequest(formIngresos,'/MidasWeb/siniestro/finanzas/guardarModificarIngresosSinietro.do', 'contenido','redireccionaModificarIngresos()');
	}													   
}

function validaCamposRequeridosModificar(){
	if($('indIva').value == 'true'  && $('porcentajeIva').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA',null);
		$('porcentajeIva').focus();
		return false;
	}
	if($('indIvaRet').value == 'true' && $('porcentajeIvaRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA Retencion',null);
		$('porcentajeIvaRetencion').focus();
		return false;
	}
	if($('indIsr').value == 'true' && $('porcentajeIsr').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR',null);
		$('porcentajeIsr').focus();
		return false;
	}
	if($('indIsrRet').value == 'true' && $('porcentajeIsrRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR retencion',null);
		$('porcentajeIsrRetencion').focus();
		return false;
	}
	if($('indOtros').value == 'true' && $('porcentajeOtros').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % otros',null);
		$('porcentajeOtros').focus();
		return false;
	}
	if($('descripcionIngreso').value == null || $('descripcionIngreso').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar la descripcion del ingreso',null);
		return false;
	}
	return true;
}
//Fin de Funciones para Modifcar Ingresos

//Funciones para Listar Ingresos Ingresos
function funcionesModificarIngresos(){
	seleccionaIndicadores();
	creaCalendars('fechaCobro,dobleCal#mCalIzq#mCalDer#btnDobleCal','SD');
}

function regresarListadoIngresos(){
	var varIdToReporteSiniestro = $('idToReporteSiniestro').value;
	mostrarListarIngresosSiniestro(varIdToReporteSiniestro);
}

function eliminarIngresos(){
		if(validarEliminarCancelar('eliminarIngreso','cancelarIngreso')){
//			if(confirm('Realmente desea Eliminar los Ingresos ')){
//				sendRequest(varForma,'/MidasWeb/siniestro/finanzas/eliminarIngresos.do', 'contenido','redireccionaEliminarIngresos()');
//			}
			var url = "sendRequest(document.ingresosForm,'/MidasWeb/siniestro/finanzas/eliminarIngresos.do', 'contenido','redireccionaEliminarIngresos()');";
			ventanaConfirmacionSiniestros('\u00BFRealmente desea Eliminar los Ingresos ?',url);
		}else{
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'No se pueden seleccionar ingresos para cancelar y eliminar',null);
		}
}

function cancelarIngresos(){
		if(validarEliminarCancelar('eliminarIngreso','cancelarIngreso')){
//			if(confirm('Realmente desea Cancelar los Ingresos ')){
//				sendRequest(varForma,'/MidasWeb/siniestro/finanzas/cancelarIngresos.do', 'contenido','redireccionaCancelarIngresos()');
//			}
			var url = "sendRequest(document.ingresosForm,'/MidasWeb/siniestro/finanzas/cancelarIngresos.do', 'contenido','redireccionaCancelarIngresos()');";
			ventanaConfirmacionSiniestros('\u00BFRealmente desea Cancelar los Ingresos?',url);
		}else{
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'No se pueden seleccionar ingresos para cancelar y eliminar',null);
		}
}

function validarEliminarCancelar(chkEliminar, chkCancelar){
	listaElim = document.getElementsByName(chkEliminar);
	listaCancel = document.getElementsByName(chkCancelar);
	
	var contElim = 0;
	var contCancel = 0;
	
	for(i=0; i < listaElim.length; i++){
		if(listaElim[i].checked){
			contElim++;
		}
		if(listaCancel[i].checked){
			contCancel++;
		}
	}
	
	if(contElim > 0 && contCancel > 0){
		return false;
	}
	
	return true
}

function redireccionaGuardarIngresos(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	mostrarVentanaMensaje(MOSTRAR_MENSAJE_OK_SINIESTRO,'El ingreso se guardo con \u00e9xito', funcionRedirecciona);
}
function redireccionaModificarIngresos(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	mostrarVentanaMensaje(MOSTRAR_MENSAJE_OK_SINIESTRO,'El ingreso se modifico con \u00e9xito', funcionRedirecciona);
}
function redireccionaEliminarIngresos(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	mostrarVentanaMensaje(MOSTRAR_MENSAJE_OK_SINIESTRO,'El ingreso se elimino con \u00e9xito', funcionRedirecciona);
}
function redireccionaCancelarIngresos(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	mostrarVentanaMensaje(MOSTRAR_MENSAJE_OK_SINIESTRO,'El ingreso se cancelo con \u00e9xito', funcionRedirecciona);
}

function creaCalendarsX(objectId){
	var calendars = objectId.split(',');	
	var fechitas = new Array(calendars.length);
	var i=0;
	
	while (i<calendars.length){
		fechitas[i] = new dhtmlxCalendarObject(calendars[i], true, {isMonthEditable: true, isYearEditable: true});
		fechitas[i].setDateFormat(formatoFechaCalendario());
		i++;		
	}	
}

function elementosSeleccionados(objElemento){
	var cuantos = 0;
	elementos = document.getElementsByName(objElemento);
	
	for(i = 0; i < elementos.length; i++){
		if(elementos[i].checked){
			cuantos ++;
		}
	}
	return cuantos;
}

function mostrarListarIngresosSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToReporteSiniestro='+idToReporteSiniestro, 'contenido',null);
}

function mostrarAgregarIngresos(idToReporteSiniestro){
	sendRequest(null,"/MidasWeb/siniestro/finanzas/ingresos.do?idToReporteSiniestro="+idToReporteSiniestro
			,"contenido","creaCalendars('fechaCobro','S')");
}

function mostrarAgregarIngresos(idToReporteSiniestro){
    sendRequest(null,"/MidasWeb/siniestro/finanzas/ingresos.do?idToReporteSiniestro="+idToReporteSiniestro
    		,"contenido","creaCalendars('fechaCobro','S')");

}



function listarIngresosPorAplicar(){
    sendRequest(null,'/MidasWeb/siniestro/finanzas/mostrarListarIngresosPorAplicar.do', 'contenido', "creaCalendars('fechaCobro','S')");
    
}



function mostrarIngresoPorAplicar(varIdToIngresoSiniestro){
	var noReporte = $('idToReporteSiniestro').value;
	var fecha = $('fechaCobro').value;
	var monto = $('montoIngreso').value;
	var conceptoIngreso = $('conceptoIngreso').value;
	var url = generaUrIngresoAplicar(varIdToIngresoSiniestro);
	sendRequest(null,url, 'contenido', null);
}

function aplicarIngresoSiniestro(){
//	var funcionRedirecciona = 'mostrarListaIngresosPorAplicarGrids('+$("idToReporteSiniestro").value+')';
	var selectReferenciaExterna = $('idToIngresoSiniestro').value;
	if(idReferenciaExterna == null || idReferenciaExterna == undefined || idReferenciaExterna == '' ||idReferenciaExterna == '-1'){
		alert('Debe seleccionar una referencia externa.');
	}
	else{
		var funcionRedirecciona = 'regresarMostrarListaIngresosPorAplicar()';
		var url=' /MidasWeb/siniestro/finanzas/aplicarIngreso.do?';
		url += '&idToIngresoSiniestro='+$('idToIngresoSiniestro').value;
		if($('observaciones').value != ""){
			url += '&observaciones='+$('observaciones').value;
		}
		if($('referencias').value != ""){
			url += '&referencias='+$('referencias').value;
		}
		sendRequestProcesaRespuestaXML(document.aplicarIngresosForm,url, null, funcionRedirecciona);
	}
}

function cancelarIngresoSiniestro(){
//	var funcionRedirecciona = 'mostrarListaIngresosPorAplicarGrids('+$("idToReporteSiniestro").value+')';
	var funcionRedirecciona = 'regresarMostrarListaIngresosPorAplicar()';
	var url=' /MidasWeb/siniestro/finanzas/cancelarIngreso.do?';
	url += '&idToIngresoSiniestro='+$('idToIngresoSiniestro').value;
	if($('observaciones').value != ""){
		url += '&observaciones='+$('observaciones').value;
	}
	if($('referencias').value != ""){
		url += '&referencias='+$('referencias').value;
	}
	sendRequestProcesaRespuestaXML(null,url, null, funcionRedirecciona);
}

function regresarMostrarListaIngresosPorAplicar(){
	noReporte = $('idToReporteSiniestroFiltro').value;
	fecha = $('fechaCobroFiltro').value;
	monto = $('montoIngresoFiltro').value;
	conceptoIngreso = $('conceptoIngresoFiltro').value;
	if(noReporte==""){
		noReporte=null;
	}else{
		noReporte= "'"+noReporte+"'";
	}
	if(fecha == ""){
		fecha = null
	}else{
		fecha= "'"+fecha+"'";
	}
	if(monto == ""){
		monto = null
	}else{
		monto= "'"+monto+"'";
	}
	if(conceptoIngreso == ""){
		conceptoIngreso = null
	}else{
		conceptoIngreso= "'"+conceptoIngreso+"'";
	}
	
//	listarIngresosPorAplicar();
	var funcionRedirect = "creaCalendars('fechaCobro','S')";
	var redirect2= ';mostrarGridListaIngresosPorAplicar('+noReporte+','+fecha+','+monto+','+conceptoIngreso+')';
	funcionRedirect += redirect2;
	sendRequest(document.ingresosForm,'/MidasWeb/siniestro/finanzas/mostrarListarIngresosPorAplicar.do', 'contenido', funcionRedirect);
//	mostrarGridListaIngresosPorAplicar(noReporte,fecha,monto,conceptoIngreso)
}

function mostrarGridListaIngresosPorAplicar(varNoReporte,varFecha,varMonto,varConceptoIngreso){
	if(varNoReporte != null){
		$('idToReporteSiniestro').value = varNoReporte;
	}
	if(varFecha != null){
		$('fechaCobro').value = varFecha;
	}
	if(varMonto != null){
		$('montoIngreso').value = varMonto;
	}
	if(varConceptoIngreso != null){
		$('conceptoIngreso').value = varConceptoIngreso;
	}
	mostrarListaIngresosPorAplicarGrids();
}

var listaIngresosPorAplicarPath;
var listaIngresosPorAplicarGrid;
function mostrarListaIngresosPorAplicarGrids(){
	if(validaIngresosAplicar()){
		listaIngresosPorAplicarPath = generaUrlBuscarIngresosAplicar();
		listaIngresosPorAplicarGrid = new dhtmlXGridObject('listaIngresosGrid');							  
		listaIngresosPorAplicarGrid.setHeader("Fecha,Concepto,Estatus,Nombre Asegurado,Numero Poliza,Numero Reporte,Monto Ingreso,",null,["text-align:center;","text-align:center","text-align:center","text-align:center","text-align:center","text-align:center","text-align:center"]);
		listaIngresosPorAplicarGrid.setColumnIds("fecha,conceptoIngreso,estatus,nombreAsegurado,numeroPoliza,numeroReporte,montoIngreso");
		listaIngresosPorAplicarGrid.setInitWidths("80,110,100,230,120,120,103,30");
		listaIngresosPorAplicarGrid.setColAlign("center,center,center,left,center,center,right,center");
		listaIngresosPorAplicarGrid.setColSorting("str,str,str,str,str,str,str,str");
		listaIngresosPorAplicarGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,img");
		listaIngresosPorAplicarGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		listaIngresosPorAplicarGrid.setSkin("light");
		listaIngresosPorAplicarGrid.enableDragAndDrop(false);
		listaIngresosPorAplicarGrid.enableLightMouseNavigation(false);
		
		listaIngresosPorAplicarGrid.enablePaging(true,10,5,"pagingArea",true,"infoArea");
		listaIngresosPorAplicarGrid.setPagingSkin("bricks");
		listaIngresosPorAplicarGrid.init();
		mostrarIndicadorCargaGenerico('indicadorIngresosPorAplicar');
		listaIngresosPorAplicarGrid.load(listaIngresosPorAplicarPath, ocultarIndicadorListaIngresosPorAplicarGrids, 'json');
	}
}

function ocultarIndicadorListaIngresosPorAplicarGrids(){
	ocultarIndicadorCargaGenerico('indicadorIngresosPorAplicar');
}

function generaUrlBuscarIngresosAplicar(){
	var noReporte = $('idToReporteSiniestro').value;
	var fecha = $('fechaCobro').value;
	var monto = $('montoIngreso').value;
	var conceptoIngreso = $('conceptoIngreso').value;
	var url ="/MidasWeb/siniestro/finanzas/listarIngresosPorAplicar.do?"
	var tmp = "";
	
	if(noReporte != ""){
		tmp=tmp+"&idToReporteSiniestro="+noReporte;
	}
	if(fecha != ""){
		tmp=tmp+"&fechaCobro="+fecha;
	}
	if(monto != ""){
		tmp=tmp+"&montoIngreso="+monto;
	}
	if(conceptoIngreso != ""){
		tmp=tmp+"&conceptoIngreso="+conceptoIngreso;
	}
	url = url +tmp.substring(1, tmp.length);
	return url;
}

function generaUrIngresoAplicar(varIdToIngresoSiniestro){
	var noReporte = $('idToReporteSiniestro').value;
	var fecha = $('fechaCobro').value;
	var monto = $('montoIngreso').value;
	var conceptoIngreso = $('conceptoIngreso').value;
	var url ="/MidasWeb/siniestro/finanzas/mostrarIngresoPorAplicar.do?idToIngresoSiniestro="+varIdToIngresoSiniestro;
	var tmp = "";
	if(noReporte != ""){
		tmp=tmp+"&idToReporteSiniestroFiltro="+noReporte;
	}
	if(fecha != ""){
		tmp=tmp+"&fechaCobroFiltro="+fecha;
	}
	if(monto != ""){
		tmp=tmp+"&montoIngresoFiltro="+monto;
	}
	if(conceptoIngreso != ""){
		tmp=tmp+"&conceptoIngresoFiltro="+conceptoIngreso;
	}
	url = url +tmp;
	return url;
}

function validaIngresosAplicar(){
	var noReporte = $('idToReporteSiniestro').value;
	var fecha = $('fechaCobro').value;
	var monto = $('montoIngreso').value;
	var conceptoIngreso = $('conceptoIngreso').value;
	if(noReporte === "" && fecha === "" && monto === "" && conceptoIngreso === ""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe introducir almenos un criterio de busqueda', null);
		return false;
	}
	return true;
}
