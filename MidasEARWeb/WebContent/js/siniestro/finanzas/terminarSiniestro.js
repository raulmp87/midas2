function terminarSiniestro(terminarSiniestroForm){
	if(validarTerminacion()){
		sendRequest(terminarSiniestroForm,'/MidasWeb/siniestro/finanzas/terminarReporteSiniestro.do', 'contenido', 'notificarTerminacionExitosa()');
	}	
}

function cancelarSiniestro(){
//	sendRequest(terminarSiniestroForm,'/MidasWeb/siniestro/finanzas/cancelarReporteSiniestro.do', 'contenido','null');
	listarReportesSiniestro();
}

function validarTerminacion(forma){
	if($('idTerminoAjuste').value == null || $('idTerminoAjuste').value == '0'){
		alert('El T\u00e9rmino de Ajuste es requerido');
		$('idTerminoAjuste').focus();
		return false;
	}else if($('descripcionTerminoAjuste').value == null || $('descripcionTerminoAjuste').value == ''){
		alert('El campo Descripcion del T\u00e9rmino es requerido');
		$('descripcionTerminoAjuste').focus();
		return false;
	}else	
		return true;	
}

function notificarTerminacionExitosa(){
//	var xredirect = "sendRequest(null,'/MidasWeb','contenido',null);";	
	mostrarVentanaMensaje('30','El reporte se termino con \u00e9xito', 'listarReportesSiniestro()');
}