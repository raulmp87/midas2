
function mostrarConvenioFiniquitoParaReporteSiniestro(idToReporteSiniestro){
	if (idToReporteSiniestro){
		var url="/MidasWeb/siniestro/finanzas/conveniofiniquito/mostrarGenerarConvenioFiniquito.do?idToReporteSiniestro="+idToReporteSiniestro;
		sendRequest(null,url,'contenido','inicializaComponentesConvenioFiniquito()');
	}
}

function inicializaComponentesConvenioFiniquito(){
	creaCalendars('fechaConvenio','S');
	var valorPagoConCheque = $('valorPagoConCheque').value;
	var cheque = $('pagoConCheque');
	
	if(valorPagoConCheque == "1"){
		cheque.checked = true;
	}else{
		cheque.checked = false;
	}
	isPagoChequeConvenio();
}

function inicializaRecibirConvenioFiniquito(){
	var valorPagoConCheque = $('valorPagoConCheque').value;
	var cheque = $('pagoConCheque');
	
	if(valorPagoConCheque == "1"){
		cheque.checked = true;
	}else{
		cheque.checked = false;
	}
}

function mostrarPDFConvenioFiniquito(){
	var location='/MidasWeb/siniestro/finanzas/conveniofiniquito/mostrarPDFConvenioFiniquito.do';
	window.open(location, "ReporteConvenioFiniquito");
}

function generarConvenioFiniquito(idToReporteSiniestro){
	var url="/MidasWeb/siniestro/finanzas/conveniofiniquito/generarConvenioFiniquito.do";
	sendRequestProcesaRespuestaXML(document.convenioFiniquitoForm, url, 'contenido', 'regresarListaReportes()');	
}

function mostrarRecibirConvenioFirmadoParaReporteSiniestro(idToReporteSiniestro){
	if (idToReporteSiniestro){
		var url="/MidasWeb/siniestro/finanzas/conveniofiniquito/mostrarRecibirConvenioFirmado.do?idToReporteSiniestro="+idToReporteSiniestro;
		sendRequest(null,url,'contenido','inicializaRecibirConvenioFiniquito()');
	}
}

function recibirConvenioFirmado(idToReporteSiniestro){
	if(validaPagoCheque()){
		var url="/MidasWeb/siniestro/finanzas/conveniofiniquito/recibirConvenioFirmado.do";
		sendRequestProcesaRespuestaXML(document.convenioFiniquitoForm, url, 'contenido', 'regresarListaReportes()');
	}
}


function isPagoChequeConvenio(){
	var radios = document.getElementsByName("tipoCuenta");
	var cheque = $('pagoConCheque');
	
	if(cheque.checked){
		$('valorPagoConCheque').value=1;
		for(var i = 0 ; i < radios.length; i++) {
			radios[i].disabled = true;
		}
		$('numeroCuenta').value='';
		$('numeroCuenta').disabled = true;
		$('bancoReceptor').value='';
		$('bancoReceptor').disabled = true;
		$('lugarConvenio').value='';
		$('lugarConvenio').disabled = true;
		$('nombreBeneficiario').value='';
		$('nombreBeneficiario').disabled = true;
	}else{
		$('valorPagoConCheque').value=0;
		for(var i = 0 ; i < radios.length; i++) {
			radios[i].disabled = false;
		}
		$('numeroCuenta').disabled = false;
		$('bancoReceptor').disabled = false;
		$('lugarConvenio').disabled = false;
		$('nombreBeneficiario').disabled = false;
	}
}


function validaPagoCheque(){
	var cheque = $('pagoConCheque');
	if(!cheque.checked){
		if($('numeroCuenta').value === null || $('numeroCuenta').value === ''){
			mostrarVentanaMensaje('10','El numero de cuenta es requerido',null);
			return false;
		}
		if($('bancoReceptor').value === null || $('bancoReceptor').value === ''){
			mostrarVentanaMensaje('10','El Banco Receptor es requerido',null);
			return false;
		}
		if($('lugarConvenio').value === null || $('lugarConvenio').value === ''){
			mostrarVentanaMensaje('10','El Lugar del convenio es requerido',null);
			return false;
		}
		if($('nombreBeneficiario').value === null || $('nombreBeneficiario').value === ''){
			mostrarVentanaMensaje('10','El nombre del beneficiario es requerido',null);
			return false;
		}
	}
	return true;
}

