
function realizaSumaEstimada(){
	var sumafinal = 0;
	var listaValoresEstimacion = document.getElementsByName('estimacion');
	for(i=0; i < listaValoresEstimacion.length;i++){
		if(listaValoresEstimacion[i].value != null && listaValoresEstimacion[i].value != ''){
			sumafinal = sumafinal + Number(removeCurrency(listaValoresEstimacion[i].value));
		}
	}
	if(isNaN(sumafinal)){
		$('idTotalEstimacionInicial').value = '';
	}else{
		$('idTotalEstimacionInicial').value = formatCurrency(sumafinal);
	}
}

function validarInformacionReserva(){
	
	sumaReservaInicial = $('idTotalEstimacionInicial').value;
	totalSumaAsegurada = $('totalSumaAsegurada').value;
	if(!validaEstimacionInicial(sumaReservaInicial)){
		mostrarVentanaMensaje('10','Los Valores de estimacion son Requeridos',null);
		return false;
	}
	if(!validaResevaInicialMenorSumaAsegurada(sumaReservaInicial,totalSumaAsegurada)){
		mostrarVentanaMensaje('10','La reserva inicial especificada debe ser menor o igual que la suma asegurada',null);
		return false;
	}
	//sendRequestProcesaRespuestaXML(document.reservaForm,'/MidasWeb/siniestro/finanzas/guardarEstimacion.do', 'contenido','mostrarMsjReserva()');
	sendRequestProcesaRespuestaXML(document.reservaForm,'/MidasWeb/siniestro/finanzas/guardarEstimacion.do', 'contenido','listarReportesSiniestro()');
}

function mostrarMsjReserva(){
	//mostrarVentanaMensaje('30','La reserva se Guardo con \u00e9xito', 'listarReportesSiniestro()');
}

function validaResevaInicialMenorSumaAsegurada(varReservaInicial,varSumaAsegurada){
	if(Number(removeCurrency(varReservaInicial)) <= Number(removeCurrency(varSumaAsegurada))){
		return true
	}
	return false;
}

function validaEstimacionInicial(sumaReservaInicial){
	var listaValoresEstimacion = document.getElementsByName('estimacion');
	for(i=0; i < listaValoresEstimacion.length;i++){
		if(listaValoresEstimacion[i].value === null || listaValoresEstimacion[i].value === ''){
			return false;
		}
	}
	if(sumaReservaInicial === null || sumaReservaInicial === ''){
		return false;
	}
	return true;
}

function muestraDetalleHistorial(){
	document.getElementById("detalleHistorialRes").style.display='block';
}

function realizaSumaReserva(){
	var sumafinal = 0;
	var listaValoresEstimacion = document.getElementsByName('reserva');
	for(i=0; i < listaValoresEstimacion.length;i++){
		if(listaValoresEstimacion[i].value != null && listaValoresEstimacion[i].value != ''){
			sumafinal = sumafinal + parseInt(listaValoresEstimacion[i].value);
		}
	}
	if(isNaN(sumafinal)){
		$('idTotalEstimacionInicial').value = '';
	}else{
		$('idTotalEstimacionInicial').value = sumafinal;
	}
}

function modificaReserva(){
	var listValoresReservas = document.getElementsByName('reservas');
	for(i=0; i < listValoresReservas.length;i++){
		if(listValoresReservas[i].value === null || listValoresReservas[i].value === ''){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Los valores de reserva son requeridos", null);
			return false;
		}
	}
	
	if(huboCambiosEnEstimaciones()){
		sendRequestProcesaRespuestaXML(document.reservaForm,'/MidasWeb/siniestro/finanzas/agregarReserva.do', 'contenido','listarReportesSiniestro()');
	}else{
		mostrarVentanaMensaje('10','No hay cambios por registrar', null);
	}	
}
function mostrarMsjModificarReserva(){
	mostrarVentanaMensaje('30','La reserva se Guardo con \u00e9xito', 'listarReportesSiniestro()');
}
function realizaSumaReservaModificar(){
	var sumafinal = 0;
	var listValoresReservas = document.getElementsByName('reservas');
	for(i=0; i < listValoresReservas.length;i++){
		if(listValoresReservas[i].value != null && listValoresReservas[i].value != ''){
			sumafinal = sumafinal + Number(removeCurrency(listValoresReservas[i].value));
		}
	}
	if(isNaN(sumafinal)){
		$('totalReservaPorAutorizar').value = '';
	}else{
		$('totalReservaPorAutorizar').value = formatCurrency(sumafinal);
	}
}

function colocaValorEstimaciones(){
	var listaReservas = document.getElementsByName('reservas');
	var valoresEstimacion = document.getElementsByName('estimacion');
	for(i=0; i < valoresEstimacion.length;i++){
		listaReservas[i].value = valoresEstimacion[i].value;
//		if(listaValoresEstimacion[i].value != null && listaValoresEstimacion[i].value != ''){
//			sumafinal = sumafinal + parseInt(listaValoresEstimacion[i].value);
//		}
	}
}

function autorizarReserva(){
	ventanaConfirmacionSiniestros("\u00BFEsta seguro de autorizar la reserva ?", 'accionAutorizarReserva()');
}
function accionAutorizarReserva(){
	sendRequest(document.reservaForm,'/MidasWeb/siniestro/finanzas/autorizarReserva.do', 'contenido','mostrarMsjReservaAutorizada()');
}
function mostrarMsjReservaAutorizada(){
	mostrarVentanaMensaje('30','La reserva se Autorizo con \u00e9xito', 'listarReportesSiniestro()');
}


function mostrarHistorialReserva(idToReporteSiniestro,varPantallaOrigen){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/listarReserva.do?idToReporteSiniestro='+idToReporteSiniestro+'&pantallaOrigen='+varPantallaOrigen,'contenido',null);
}

function mostrarModificarHistorialReserva(idToReporteSiniestro){
	 sendRequest(null,'/MidasWeb/siniestro/finanzas/mostrarModificarReserva.do?idToReporteSiniestro='+idToReporteSiniestro+'&origen=informeFinal','contenido',null);
}

function modificarReservaDesdeListado(idToReporteSiniestro){
	 sendRequest(null,'/MidasWeb/siniestro/finanzas/mostrarModificarReserva.do?idToReporteSiniestro='+idToReporteSiniestro+'&origen=pendiente','contenido',null);
}

function mostarDetalleHistorialReserva(){
	sendRequest(document.historialReservaForm,'/MidasWeb/siniestro/finanzas/listarReservaDetalle.do','detalleHistorialRes','muestraDetalleHistorial()');
}
function huboCambiosEnEstimaciones() {
	var estimaciones = document.getElementsByName('reservas');
	var nuevasEstimaciones = document.getElementsByName('reservaDetalleDTO.estimacion');
	var diferencias = false;
	
	for(i = 0; i < estimaciones.length; i++){
		var e = parseFloat(removeCurrency(estimaciones[i].value));
		var f = parseFloat(removeCurrency(nuevasEstimaciones[i].value));
		
		if((e * 100) != (f * 100)){
			diferencias = true;
			break;
		}
	}
	
	return diferencias;	
}