
function guardarOrdenDePago(){
//	if (confirm("Desea generar la orden de pago?")){
//		guardarOrdenDePagoAjax();
//	}
	ventanaConfirmacionSiniestros("\u00BFDesea generar la orden de pago?", 'guardarOrdenDePagoAjax();');
}

function guardarOrdenDePagoAjax() {	
		document.getElementById("b_guardar").style.display='none';
		new Ajax.Request("/MidasWeb/siniestro/finanzas/guardarOrdenDePago.do", {
			method : "post",
			asynchronous : false,
			parameters : "numeroAutorizacionTecnica="+$('idToAutorizacionTecnica').value,
			onSuccess : function(transport) {
			mostrarOrdenDePago(transport.responseXML);
			} // End of onSuccess
		});
}

function mostrarOrdenDePago(doc){
	var numeroOrden = doc.getElementsByTagName("valor")[0].firstChild.nodeValue;		
//	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarIngresos.do?idToIngresoSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	if(numeroOrden > 0){
		mostrarVentanaMensaje('30','Se genero el Numero de Orden '+numeroOrden+' con \u00e9xito', 'listarReportesSiniestro()');
	}else{
		mostrarVentanaMensaje('10','No se pudo generar la orden de pago. Contacte a Sistemas', 'listarReportesSiniestro()');
	}
}

function regresarListaOrdenesDePago(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do?idToReporteSiniestro='+$('idToReporteSiniestro').value, 'contenido',null);
}

function regresarListaOrdenesDePagoPorReporteSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do?idToReporteSiniestro='+idToReporteSiniestro, 'contenido',null);
}

function visualizarATGasto(){
	var varId = $('id').value;
	var varIdToAutorizacionTecnica = $('idToAutorizacionTecnica').value;
	var url = '/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do?id='+varId+'&numeroAutorizacion='+varIdToAutorizacionTecnica+'&pantalla=1';
	sendRequest(null,url, 'contenido',null);
}

function visualizarATIndemnizacion(){
	var varId = $('id').value;
	var varIdToAutorizacionTecnica = $('idToAutorizacionTecnica').value;
	var url = '/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do?id='+varId+'&numeroAutorizacion='+varIdToAutorizacionTecnica+'&pantalla=1';
	sendRequest(null,url, 'contenido',null);
}

// Funciones para el Listar Ordenes de Pago
function cancelarOrdenDePago(){
	ventanaConfirmacionSiniestros("\u00BFDesea cancelar la orden de pago?", 'ejecutaCancelarOrdenDePago()');
}

function ejecutaCancelarOrdenDePago(){
	listaCancel = document.getElementsByName('idCancelar');
	var i = 0;
	var lstValoresSel;
	var valorSel;
	var ban = false;
	while(i < listaCancel.length){ 
		if(listaCancel[i].checked){
			lstValoresSel = listaCancel[i].value;
			i = listaCancel.length-1;
			ban=true;
		}
		i++;
	}
	if(i <= listaCancel.length && ban==true){
		valorSel = lstValoresSel.split("#");
		if(valorSel[1] != "2"){
			cancelarOrdenDePagoAjax(valorSel[0]);
		}else{
			cerrarVentanaConfirmacion();
			mostrarVentanaMensaje('10','No se puede cancelar la orden seleccionada',null);
		}
	}else{
		cerrarVentanaConfirmacion();
		mostrarVentanaMensaje('10','Debe seleccionar una Orden de Pago',null);
	}
}

function cancelarOrdenDePagoAjax(idOrdenPago) {	
		new Ajax.Request("/MidasWeb/siniestro/finanzas/cancelarOrdenDePago.do", {
			method : "post",
			asynchronous : false,
			parameters : "idOrdenPago="+idOrdenPago+'&idToReporteSiniestro='+$('idToReporteSiniestro').value,
			onSuccess : function(transport) {
			mostrarCancelarOrdenDePago(transport.responseXML);
			} // End of onSuccess
		});
}
function mostrarCancelarOrdenDePago(doc){
	var numeroOrden = doc.getElementsByTagName("valor")[0].firstChild.nodeValue;		
//	mostrarVentanaMensaje('30','Se cancelo el Numero de Orden '+numeroOrden, regresarListaOrdenesDePago());
	if(numeroOrden == 0){
		mostrarVentanaMensaje('10','No se puede cancelar la orden',null);
	}else{
		mostrarVentanaMensaje('30','Se cancelo el Numero de Orden '+numeroOrden, regresarListaOrdenesDePago());
	}
}

function salirOrdenDePago(){
	listarReportesSiniestro();
}

function visualizarATLista(idAutorizacion){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/inicio.do', 'contenido',null);
}

function visualizarATGastoLista(varIdLista,varIdToAutorizacionTecnicaLista){
//	var varId = $('id').value;
//	var varIdToAutorizacionTecnica = $('idToAutorizacionTecnica').value;
	var url = '/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do?id='+varIdLista+'&numeroAutorizacion='+varIdToAutorizacionTecnicaLista+'&pantalla=2';
	sendRequest(null,url, 'contenido',null);
}

function visualizarATIndemnizacionLista(varIdLista,varIdToAutorizacionTecnicaLista){
//	var varId = $('id').value;
//	var varIdToAutorizacionTecnica = $('idToAutorizacionTecnica').value;
	var url = '/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do?id='+varIdLista+'&numeroAutorizacion='+varIdToAutorizacionTecnicaLista+'&pantalla=2';
	sendRequest(null,url, 'contenido',null);
}

function verificarEstatusOP(idToOrdenPago, idReporteSiniestro){
	var redirect = "regresarListaOrdenesDePagoPorReporteSiniestro("+idReporteSiniestro+")";	
	sendRequest(null,'/MidasWeb/siniestro/finanzas/revisarEstatusOrdenDePago.do?idOrdenPago='+idToOrdenPago, 'contenido', redirect);
}
/**
 * Grid de Autorizaciones Tecnicas (Modulo Siniestros)
 * @autor JORGEKNO
 */
var autorizacionesTecnicasGrid;
var autorizacionesTecnicasProcessor;
function poblarAutorizacionesTecnicas(form){
	
	var urlListarAutorizaciones = '/MidasWeb/siniestro/finanzas/listarAutorizaciones.do?';
	urlListarAutorizaciones += form.serialize();
	autorizacionesTecnicasGrid = new dhtmlXGridObject('autorizacionesTecnicasGrid');
	autorizacionesTecnicasGrid.load(urlListarAutorizaciones);
}
var ventanaOrdenDePago=null;
function mostrarVentanaOrdenDePago(ids,tipo){ 
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaOrdenDePago = parent.dhxWins.createWindow("ordenDePago", 400, 320, 610, 410);
	parent.ventanaOrdenDePago.setText("Orden de Pago.");
	parent.ventanaOrdenDePago.center();
	parent.ventanaOrdenDePago.setModal(true);
	if(tipo == 'generar'){
		parent.ventanaOrdenDePago.attachURL("/MidasWeb/siniestro/finanzas/mostrarGenerarOrdenDePago.do?ids="+ids+"&tipo="+tipo);	
	}else if(tipo == 'detalle'){
		parent.ventanaOrdenDePago.attachURL("/MidasWeb/siniestro/finanzas/mostrarDetalleOrdenDePago.do?ids="+ids+"&tipo="+tipo);
	}else if(tipo == 'cancelar'){
		parent.ventanaOrdenDePago.attachURL("/MidasWeb/siniestro/finanzas/mostrarDetalleOrdenDePago.do?ids="+ids+"&tipo="+tipo);
	}
	
	parent.ventanaOrdenDePago.button("minmax1").hide();

}
function generarOrdenDePago(tipo){
	var ids = autorizacionesTecnicasGrid.getCheckedRows(0);
	if(ids != null && ids.length > 0){
		mostrarVentanaOrdenDePago(ids, tipo);
	}else{
		mostrarVentanaMensaje("20", "Seleccione por lo menos una Autorizaci\u00f3n.");
	}
}
function confirmarGuardarOrdenDePago(ids){
	if(confirm("\u00BFDesea generar la orden de pago?")){
		parent.sendRequest(null,'/MidasWeb/siniestro/finanzas/generarOrdenPagoAgrupado.do?ids='+ids, 'contenido_sin_autorizaciones','ventanaOrdenDePago.close();procesarRespuesta("poblarAutorizacionesTecnicas(document.soportePagosForm)");');
	}
}
function confirmarCancelarOrdenDePago(ids){
	if(confirm("\u00BFDesea cancelar la orden de pago?")){
		parent.sendRequest(null,'/MidasWeb/siniestro/finanzas/cancelarOrdenPagoAgrupado.do?ids='+ids, 'contenido_sin_pagos','ventanaOrdenDePago.close();procesarRespuesta("poblarPagos(document.soportePagosForm)");');
	}
}
function mostrarDetalleVentanaOrdenDePago(ids){
	mostrarVentanaOrdenDePago(ids,'detalle');
}
function mostrarCancelacionVentanaOrdenDePago(ids){
	mostrarVentanaOrdenDePago(ids,'cancelar');
}
/**
 * Grid de Pagos (Modulo Siniestros)
 * @autor JORGEKNO
 */
var pagosGrid;
function poblarPagos(form){
	
	var urlListarPagos = '/MidasWeb/siniestro/finanzas/listarPagos.do?';
	urlListarPagos += form.serialize();
	pagosGrid = new dhtmlXGridObject('pagosGrid');
	pagosGrid.load(urlListarPagos);
	
}
function limpiarDivsPagos(){
	limpiarDiv('contenido_sin_pagos');
	limpiarDiv('contenido_sin_autorizaciones');
}
function imprimirOrdenDePago(idToOrdenPago){
	var ruta = "/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do?id="+idToOrdenPago+"&numeroAutorizacion=0&tipoAutorizacionTecnica=Gasto";
	newwindow = window.open(ruta,"AutorizacionOrdenPago");
}
