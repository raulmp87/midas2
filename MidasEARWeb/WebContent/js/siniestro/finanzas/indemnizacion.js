function generaPendienteVariosPagos(){
	var funcionRedirecciona = "recargaInicio();";
	procesarRespuesta(funcionRedirecciona);
}

function validaAgregarIndemnizacion(forma){
	if(forma.fechaPago.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar la fecha.", null);
		forma.fechaPago.focus();
		return false;
	}else{
		var fa = new Date();
		var fechaActual = fa.getDate()+"/"+ (fa.getMonth()+1) + "/" +fa.getFullYear();
		if(!fechaMenorIgualQue(forma.fechaPago.value, fechaActual)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"La fecha debe ser mayor o igual a la fecha actual.", null);
			forma.fechaPago.focus();
		}
	}
	if(forma.beneficiario.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el nombre del beneficiario.", null);
		forma.beneficiario.focus();
		return false;
	}
	if(forma.comentarios.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el comentario.", null);
		forma.comentarios.focus();
		return false;
	}
	//Validar que al menos una perdida sea mayor que cero
	var unaPerdidaMayorCero = false;
	var listaInputs = document.getElementsByName("perdidas");
	for(var i=0; i<listaInputs.length; i++){
		if(Number(removeCurrency(listaInputs[i].value)) > 0){
			unaPerdidaMayorCero = true;
			break;
		}
	}
	if(unaPerdidaMayorCero == false){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Al menos una perdida debe ser mayor a cero.", null);
		return false;
	}

	sendRequest(forma,"/MidasWeb/siniestro/finanzas/indemnizacion/agregarIndemnizacion.do", "contenido", "redireccionaAgregarIndemnizacion();");
}

function redireccionaAgregarIndemnizacion(){
	var funcionRedirecciona = "recargaInicio();";
	procesarRespuesta(funcionRedirecciona);
}

function redireccionaAutorizarIndemnizacion(){
	var funcionRedirecciona = "recargaInicio();";
	procesarRespuesta(funcionRedirecciona);
}

function redireccionaCancelarIndemnizacion(){
	var funcionRedirecciona = "recargaInicio();";
	procesarRespuesta(funcionRedirecciona);
}

function cancelarPagoParcial_CB(){
	var funcionRedirecciona = "recargaInicio();";
	procesarRespuesta(funcionRedirecciona);
}

function validaAgregarPagoParcial(forma){
	if(forma.fechaPago.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar la fecha.", null);
		forma.fechaPago.focus();
		return false;
	}else{
		var fechaActual = fechaActualSiniestros();
		if(!comparaFechas(fechaActual,forma.fechaPago.value)){
//		if(!fechaMenorIgualQue(forma.fechaPago.value, fechaActual)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"La fecha debe ser mayor o igual a la fecha actual.", null);
			forma.fechaPago.focus();
			return false;
		}
	}
	if(forma.beneficiario.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el nombre del beneficiario.", null);
		forma.beneficiario.focus();
		return false;
	}
	if(forma.comentarios.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el comentario.", null);
		forma.comentarios.focus();
		return false;
	}
	//Validar que al menos una perdida sea mayor que cero
	var unaPerdidaMayorCero = false;
	var listaInputs = document.getElementsByName("perdidas");
	var listaReservas = document.getElementsByName("estimacionesReserva");
	for(var i=0; i<listaInputs.length; i++){
		if(Number(removeCurrency(listaInputs[i].value)) > 0){
			unaPerdidaMayorCero = true;
		}
		if(calculaIndemnizacion(i)==false){
			return false;
		}
//		if(Number(removeCurrency(listaInputs[i].value)) > Number(removeCurrency(listaReservas[i].value))){
//			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"El monto de la indemnizaci\u00f3n no puede ser mayor que la reserva.", null);
//			listaInputs[i].focus();
//			return false;
//		}
	}
	if(unaPerdidaMayorCero == false){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Al menos una perdida debe ser mayor a cero.", null);
		return false;
	}
	sendRequest(forma,"/MidasWeb/siniestro/finanzas/indemnizacion/agregarPagoParcial.do", "contenido", "redireccionaAgregarPagoParcial();");
}

function redireccionaAgregarPagoParcial(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta(funcionRedirecciona);
}

function eliminarPagoParcial_CB(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta(funcionRedirecciona);
}

function mostrarModificarPagoParcial_CB(){
	toggleIVA();
	toggleIVARet();
	toggleISR();
	toggleISRRet();
	toggleOtros();
	toggleDeducibles();
	toggleCoaseguros();
}

function validaModificarPagoParcial(forma){
	if(forma.fechaPago.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar la fecha.", null);
		forma.fechaPago.focus();
		return false;
	}
	if(forma.beneficiario.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el nombre del beneficiario.", null);
		forma.beneficiario.focus();
		return false;
	}
	if(forma.comentarios.value==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de especificar el comentario.", null);
		forma.comentarios.focus();
		return false;
	}
	//Validar que al menos una perdida sea mayor que cero
	var unaPerdidaMayorCero = false;
	var listaInputs = document.getElementsByName("perdidas");
	var listaReservas = document.getElementsByName("estimacionesReserva");
	for(var i=0; i<listaInputs.length; i++){
		if(Number(removeCurrency(listaInputs[i].value)) > 0){
			unaPerdidaMayorCero = true;
		}
		if(calculaIndemnizacion(i)==false){
			return false;
		}
//		if(Number(removeCurrency(listaInputs[i].value)) > Number(removeCurrency(listaReservas[i].value))){
//			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"El monto de la indemnizaci\u00f3n no puede ser mayor que la reserva.", null);
//			listaInputs[i].focus();
//			return false;
//		}
	}
	if(unaPerdidaMayorCero == false){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Al menos una perdida debe ser mayor a cero.", null);
		return false;
	}
	if(document.getElementById("ultimoPago").checked){
		validarUltimoPagoIndemnizacion();
	}else{
		modificarPagoParcialIndemnizacion();
	}
	
}

function validarUltimoPagoIndemnizacion() {	
	new Ajax.Request("/MidasWeb/siniestro/finanzas/indemnizacion/validarUltimoPago.do", {
		method : "post",
		asynchronous : false,
		parameters : "idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+'&idIndemnizacion='+document.forms[0].idIndemnizacion.value,
		onSuccess : function(transport) {
			var idRespuesta = transport.responseXML.getElementsByTagName("id")[0].firstChild.nodeValue;	
			var msj = transport.responseXML.getElementsByTagName("description")[0].firstChild.nodeValue
			if(idRespuesta == 0){
				mostrarVentanaMensaje('10',msj,null);
			}else{
				modificarPagoParcialIndemnizacion();
			}
		} // End of onSuccess
	});
}
function modificarPagoParcialIndemnizacion(){
	sendRequest(document.indemnizacionForm,"/MidasWeb/siniestro/finanzas/indemnizacion/modificarPagoParcial.do", "contenido", "modificarPagoParcial_CB();");
}

function modificarPagoParcial_CB(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do?idReporteSiniestro="+document.forms[0].idReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta(funcionRedirecciona);
}

function seleccionaPagoParcial(estatus,ultimoPago){
	sendRequest(document.forms[0],'/MidasWeb/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion.do','detalleIndemnizacion',null);
	//Si el estatus es abierto
	document.getElementById("b_modificar").style.display='';
	if(estatus==1 && ultimoPago=="false"){
		document.getElementById("b_borrar").style.display='';
//		document.getElementById("b_modificar").style.display='';
		document.getElementById("b_guardar").style.display='none';
	//Si el estatus es autorizado
	}else if(estatus==3 || estatus==2 || estatus==5){
		document.getElementById("b_borrar").style.display='none';
//		document.getElementById("b_modificar").style.display='';
		document.getElementById("b_guardar").style.display='';
	}else if(estatus==4){
		document.getElementById("b_modificar").style.display='none';
		document.getElementById("b_guardar").style.display='none';
	}else{
		document.getElementById("b_borrar").style.display='none';
//		document.getElementById("b_modificar").style.display='';
		document.getElementById("b_guardar").style.display='none';
	}
}

function redireccionaMostrarAgregarIndemnizacion(){
	creaCalendars('fechaPago','S');
	var listaInputs = document.getElementsByName("perdidas");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("deducibles");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("coaseguros");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("indemnizaciones");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	document.getElementById("totalPerdidas").value=formatCurrency(0);
	document.getElementById("totalDeducibles").value=formatCurrency(0);
	document.getElementById("totalCoaseguros").value=formatCurrency(0);
	document.getElementById("totalIndemnizaciones").value=formatCurrency(0);
	document.getElementById("porcentajeIVA").value=0;
	document.getElementById("montoIVA").value=formatCurrency(0);
	document.getElementById("porcentajeIVARet").value=0;
	document.getElementById("montoIVARet").value=formatCurrency(0);
	document.getElementById("porcentajeISR").value=0;
	document.getElementById("montoISR").value=formatCurrency(0);
	document.getElementById("porcentajeISRRet").value=0;
	document.getElementById("montoISRRet").value=formatCurrency(0);
	document.getElementById("porcentajeOtros").value=0;
	document.getElementById("montoOtros").value=formatCurrency(0);
}

function mostrarAgregarPagoParcial(){
	creaCalendars('fechaPago','S');
	var listaInputs = document.getElementsByName("perdidas");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("deducibles");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("coaseguros");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	listaInputs = document.getElementsByName("indemnizaciones");
	for(var i=0; i<listaInputs.length; i++){
		listaInputs[i].value=formatCurrency(0);
	}
	document.getElementById("totalPerdidas").value=formatCurrency(0);
	document.getElementById("totalDeducibles").value=formatCurrency(0);
	document.getElementById("totalCoaseguros").value=formatCurrency(0);
	document.getElementById("totalIndemnizaciones").value=formatCurrency(0);
	document.getElementById("porcentajeIVA").value=0;
	document.getElementById("montoIVA").value=formatCurrency(0);
	document.getElementById("porcentajeIVARet").value=0;
	document.getElementById("montoIVARet").value=formatCurrency(0);
	document.getElementById("porcentajeISR").value=0;
	document.getElementById("montoISR").value=formatCurrency(0);
	document.getElementById("porcentajeISRRet").value=0;
	document.getElementById("montoISRRet").value=formatCurrency(0);
	document.getElementById("porcentajeOtros").value=0;
	document.getElementById("montoOtros").value=formatCurrency(0);
}

function toggleIVA(){
	if(document.getElementById("habilitaIVA").checked){
		document.getElementById("porcentajeIVA").disabled=false;
		document.getElementById("montoIVA").disabled=false;
		calculaMontoIva();
	}else{
		document.getElementById("porcentajeIVA").disabled=true;
		document.getElementById("montoIVA").disabled=true;
	}
}

function toggleIVARet(){
	if(document.getElementById("habilitaIVARet").checked){
		document.getElementById("porcentajeIVARet").disabled=false;
		document.getElementById("montoIVARet").disabled=false;
		calculaMontoIvaRet();
	}else{
		document.getElementById("porcentajeIVARet").disabled=true;
		document.getElementById("montoIVARet").disabled=true;
	}
}

function toggleISR(){
	if(document.getElementById("habilitaISR").checked){
		document.getElementById("porcentajeISR").disabled=false;
		document.getElementById("montoISR").disabled=false;
		calculaMontoIsr();
	}else{
		document.getElementById("porcentajeISR").disabled=true;
		document.getElementById("montoISR").disabled=true;
	}
}

function toggleISRRet(){
	if(document.getElementById("habilitaISRRet").checked){
		document.getElementById("porcentajeISRRet").disabled=false;
		document.getElementById("montoISRRet").disabled=false;
		calculaMontoIsrRet();
	}else{
		document.getElementById("porcentajeISRRet").disabled=true;
		document.getElementById("montoISRRet").disabled=true;
	}
}

function toggleOtros(){
	if(document.getElementById("habilitaOtros").checked){
		document.getElementById("porcentajeOtros").disabled=false;
		document.getElementById("montoOtros").disabled=false;
		calculaMontoOtros();
	}else{
		document.getElementById("porcentajeOtros").disabled=true;
		document.getElementById("montoOtros").disabled=true;
	}
}

function toggleDeducibles(){
	var listaInputs = document.getElementsByName("deducibles");
	if(document.getElementById("habilitaDeducible").checked){
		for(var i=0; i<listaInputs.length; i++){
			listaInputs[i].disabled=false;
		}
		document.getElementById("totalDeducibles").disabled=false;
	}else{
		for(var i=0; i<listaInputs.length; i++){
			listaInputs[i].value= "$0.00";
			listaInputs[i].disabled=true;			
		}
		document.getElementById("totalDeducibles").disabled=true;
	}
	reCalculaIndemnizaciones();
	recalculaImpuestos();
}

function toggleCoaseguros(){
	var listaInputs = document.getElementsByName("coaseguros");
	if(document.getElementById("habilitaCoaseguro").checked){
		for(var i=0; i<listaInputs.length; i++){
			listaInputs[i].disabled=false;
		}
		document.getElementById("totalCoaseguros").disabled=false;
	}else{
		for(var i=0; i<listaInputs.length; i++){
			listaInputs[i].value= "$0.00";
			listaInputs[i].disabled=true;
		}
		document.getElementById("totalCoaseguros").disabled=true;
	}
	reCalculaIndemnizaciones();
	recalculaImpuestos();
}

function calculaSumaPerdidas(indice){
	var listaInputs = document.getElementsByName("perdidas");
	var suma=0;
	//Se checa si la lista tiene mas de 1 elemento
	if(listaInputs.length > 1){
		for(var i=0; i<listaInputs.length; i++){
			suma=suma+Number(removeCurrency(listaInputs[i].value));
		}
	}else{
		suma=Number(removeCurrency(document.indemnizacionForm.perdidas.value));
	}
	document.getElementById("totalPerdidas").value=formatCurrency(suma);
	calculaIndemnizacion(indice);
	calculaSumaIndemnizaciones();
	recalculaImpuestos();
}

function calculaSumaDeducibles(indice){
	var listaInputs = document.getElementsByName("deducibles");
	var suma=0;
	//Se checa si la lista tiene mas de 1 elemento
	if(listaInputs.length > 1){
		for(var i=0; i<listaInputs.length; i++){
			suma=suma+Number(removeCurrency(listaInputs[i].value));
		}
	//Si solo tiene 1 registro, entonces no es un array
	}else{
		suma=Number(removeCurrency(document.indemnizacionForm.deducibles.value));
	}
	document.getElementById("totalDeducibles").value=formatCurrency(suma);
	calculaIndemnizacion(indice);
	calculaSumaIndemnizaciones();
	recalculaImpuestos();
}

function calculaSumaCoaseguros(indice){
	var listaInputs = document.getElementsByName("coaseguros");
	var suma=0;
	//Se checa si la lista tiene mas de 1 elemento
	if(listaInputs.length > 1){
		for(var i=0; i<listaInputs.length; i++){
			suma=suma+Number(removeCurrency(listaInputs[i].value));
		}
	//Si solo tiene 1 registro, entonces no es un array
	}else{
		suma=Number(removeCurrency(document.indemnizacionForm.coaseguros.value));
	}
	document.getElementById("totalCoaseguros").value=formatCurrency(suma);
	calculaIndemnizacion(indice);
	calculaSumaIndemnizaciones();
	recalculaImpuestos();
}

function calculaSumaIndemnizaciones(){
	var listaInputs = document.getElementsByName("indemnizaciones");
	var suma=0;
	//Se checa si la lista tiene mas de 1 elemento
	if(listaInputs.length > 1){
		for(var i=0; i<listaInputs.length; i++){
			suma=suma+Number(removeCurrency(listaInputs[i].value));
		}
	//Si solo tiene 1 registro, entonces no es un array
	}else{
		suma=Number(removeCurrency(document.indemnizacionForm.indemnizaciones.value));
	}
	document.getElementById("totalIndemnizaciones").value=formatCurrency(suma);
}

function calculaIndemnizacion(indice){
	var listaInputs = document.getElementsByName("indemnizaciones");
	//Se checa si la lista tiene mas de 1 elemento
	var formulario = (document.indemnizacionForm)?document.indemnizacionForm:$('indemnizacionForm');
	if(listaInputs.length > 1){
		var indemnizacion=Number(removeCurrency(formulario.perdidas[indice].value));
		var perdida=Number(removeCurrency(formulario.perdidas[indice].value));
		if(document.getElementById("habilitaDeducible").checked){
			indemnizacion = indemnizacion - Number(removeCurrency(formulario.deducibles[indice].value));
		}
		if(document.getElementById("habilitaCoaseguro").checked){
			indemnizacion = indemnizacion - Number(removeCurrency(formulario.coaseguros[indice].value));
		}
		if(perdida > 0){
			if(indemnizacion > 0){
				formulario.indemnizaciones[indice].value = formatCurrency(indemnizacion);	
			}else{
				formulario.indemnizaciones[indice].value =  "$0.00";
				mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"La Indemnizacion no puede ser menor o igual a 0", null);
				return false;
			}
		}else{
			formulario.indemnizaciones[indice].value = formatCurrency(indemnizacion);
		}
	//Si solo tiene 1 registro, entonces no es un array
	}else{
		var indemnizacion=Number(removeCurrency(formulario.perdidas.value));
		var perdida=Number(removeCurrency(formulario.perdidas.value));
		if(document.getElementById("habilitaDeducible").checked){
			indemnizacion = indemnizacion - Number(removeCurrency(formulario.deducibles.value));
		}
		if(document.getElementById("habilitaCoaseguro").checked){
			indemnizacion = indemnizacion - Number(removeCurrency(formulario.coaseguros.value));
		}
		if(perdida > 0){
			if(indemnizacion > 0){
				formulario.indemnizaciones.value = formatCurrency(indemnizacion);	
			}else{
				formulario.indemnizaciones[indice].value =  "$0.00";
				mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"La Indemnizacion no puede ser menor a 0", null);
				return false;
			}
		}else{
			formulario.indemnizaciones.value = formatCurrency(indemnizacion);
		}
	}
	return true;
}

function reCalculaIndemnizaciones(){
	for(var i=0; i<document.getElementsByName("indemnizaciones").length; i++){
		calculaIndemnizacion(i);
	}
	calculaSumaIndemnizaciones();
}

function calculaMontoIva(){
	var monto = Number(removeCurrency(document.getElementById("totalIndemnizaciones").value)) * Number(document.getElementById("porcentajeIVA").value) / 100;
	document.getElementById("montoIVA").value = formatCurrency(monto);
}

function calculaMontoIvaRet(){
	var monto = Number(removeCurrency(document.getElementById("totalIndemnizaciones").value)) * Number(document.getElementById("porcentajeIVARet").value) / 100;
	document.getElementById("montoIVARet").value = formatCurrency(monto);
}

function calculaMontoIsr(){
	var monto = Number(removeCurrency(document.getElementById("totalIndemnizaciones").value)) * Number(document.getElementById("porcentajeISR").value) / 100;
	document.getElementById("montoISR").value = formatCurrency(monto);
}

function calculaMontoIsrRet(){
	var monto = Number(removeCurrency(document.getElementById("totalIndemnizaciones").value)) * Number(document.getElementById("porcentajeISRRet").value) / 100;
	document.getElementById("montoISRRet").value = formatCurrency(monto);
}

function calculaMontoOtros(){
	var monto = Number(removeCurrency(document.getElementById("totalIndemnizaciones").value)) * Number(document.getElementById("porcentajeOtros").value) / 100;
	document.getElementById("montoOtros").value = formatCurrency(monto);
}

function recalculaImpuestos(){
	if(document.getElementById("habilitaIVA").checked){
		calculaMontoIva();
	}
	if(document.getElementById("habilitaIVARet").checked){
		calculaMontoIvaRet();
	}
	if(document.getElementById("habilitaISR").checked){
		calculaMontoIsr();
	}
	if(document.getElementById("habilitaISRRet").checked){
		calculaMontoIsrRet();
	}
	if(document.getElementById("habilitaOtros").checked){
		calculaMontoOtros();
	}
}

function recargaInicio(){
	document.location.reload();
}

function redireccionaIndemnizacionesDeReporteSiniestro(idToReporteSiniestro){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do?idReporteSiniestro="+idToReporteSiniestro+"','contenido',null);";
	eval(funcionRedirecciona);
}
function activaFechaIndemnizacion(){
	creaCalendars('fechaPago','S');
}



