function crearAutorizacionTecnicaGasto(){
//	if(confirm('Esta seguro de generar la autorizaci\u00f3n t\u00e9cnica de este gasto')){
//		sendRequest(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaGasto.do', 'contenido', 'mensajeCrearAutorizacion()');
//	}
	var url = "sendRequest(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaGasto.do', 'contenido', 'mensajeCrearAutorizacion()');";
	ventanaConfirmacionSiniestros('\u00BFEsta seguro de generar la autorizaci\u00f3n t\u00e9cnica de este gasto?',url);
}

function crearAutorizacionTecnicaIngreso(form){
//	if(confirm('Esta seguro de generar la autorizaci\u00f3n t\u00e9cnica de este ingreso')){
//		sendRequest(form,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIngreso.do', 'contenido', 'mensajeCrearAutorizacion()');
//	}
	var url = "sendRequest(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIngreso.do', 'contenido', 'mensajeCrearAutorizacion()');";
	ventanaConfirmacionSiniestros('\u00BFEsta seguro de generar la autorizaci\u00f3n t\u00e9cnica de este ingreso?',url);
}

function crearAutorizacionTecnicaIndemnizacion(form){
//	if(confirm('Esta seguro de generar la autorizaci\u00f3n t\u00e9cnica de esta indemnizaci\u00f3n')){
//		sendRequest(form,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIndemnizacion.do', 'contenido', 'mensajeCrearAutorizacion()');
//	}
	var url = "sendRequest(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIndemnizacion.do', 'contenido', 'mensajeCrearAutorizacion()');";
	ventanaConfirmacionSiniestros('\u00BFEsta seguro de generar la autorizaci\u00f3n t\u00e9cnica de esta indemnizaci\u00f3n?',url);
}

function mensajeCrearAutorizacion(){
	mostrarVentanaMensaje('30','La autorizaci\u00f3n t\u00e9cnica se creo con \u00e9xito', 'listarReportesSiniestro()');
}

function enviarAutorizacionGasto(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do?id=81', 'contenido', null);	
}

function enviarAutorizacionIngreso(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIngreso.do?id=258', 'contenido', null);	
}

function enviarAutorizacionIndemnizacion(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do?id=1', 'contenido', null);	
}

function listarAutorizacionesPorCancelar(){
		sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesPorCancelar.do?idReporteSiniestro=383', 'contenido', null);	
}

function listarAutorizacionesTecnicas(idReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesTecnicas.do?idReporteSiniestro='+idReporteSiniestro, 'contenido', null);		
}

function mostrarAutorizacionTecnicaLigas(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarAutorizacionTecnica.do?idAutorizacionTecnica=11', 'contenido', null);
}

function autorizarAutorizacionTecnica(){
	var estatus = $('estatusAT').value;
	var idReporteSiniestro = $('idToReporteSiniestro').value;
	
	var redirect = "'mensajeAutorizarAutorizacionTecnica("+idReporteSiniestro+")'"; 
	
	if(estatus >= 0){	
		if(estatus == 1){
			mostrarVentanaMensaje('10','La autorizaci\u00f3n t\u00e9cnica ya esta autorizada', null);
		}else if(estatus == 2){
			mostrarVentanaMensaje('10','La autorizaci\u00f3n t\u00e9cnica ya fue cancelada', null);
//		}else if(confirm('Esta seguro de efectuar la autorizaci\u00f3n')){	
//			sendRequest(form,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizarAutorizacionTecnica.do', 'contenido', redirect);
		}else{
			var url = "sendRequest(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizarAutorizacionTecnica.do', 'contenido', "+redirect+");";
			ventanaConfirmacionSiniestros('\u00BFEsta seguro de efectuar la autorizaci\u00f3n?',url);
		}
	}else{
		mostrarVentanaMensaje('10','Favor de Seleccionar una Autorizaci\u00f3n T\u00e9cnica', null);
	}	
}

function mensajeAutorizarAutorizacionTecnica(idReporteSiniestro){
	mostrarVentanaMensaje('30','La autorizaci\u00f3n t\u00e9cnica se autorizo \u00e9xito', null);
	listarAutorizacionesTecnicas(idReporteSiniestro);
}

function cancelarAutorizacionTecnica(){
	var estatus = $('estatusAT').value;
	var idReporteSiniestro = $('idToReporteSiniestro').value;
	
	var redirect = "mensajeCancelarAutorizacionTecnica("+idReporteSiniestro+")"; 
	
	if(estatus >= 0){	
		if(estatus == 2){
			mostrarVentanaMensaje('10','La autorizaci\u00f3n t\u00e9cnica ya fue cancelada', null);
//		}else if(confirm('Esta seguro de efectuar la cancelaci\u00f3n')){
//			sendRequestProcesaRespuestaXML(form,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/cancelarAutorizacionTecnica.do', 'contenido', redirect);
		}else{
			var url = "sendRequestProcesaRespuestaXML(document.autorizacionTecnicaForm,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/cancelarAutorizacionTecnica.do', 'contenido', "+redirect+");";
			ventanaConfirmacionSiniestros('\u00BFEsta seguro de efectuar la cancelaci\u00f3n?',url);
		}
	}else{
		mostrarVentanaMensaje('10','Favor de Seleccionar una Autorizaci\u00f3n T\u00e9cnica', null);
	}	
}

function mensajeCancelarAutorizacionTecnica(idReporteSiniestro){
	mostrarVentanaMensaje('30','La autorizaci\u00f3n t\u00e9cnica se cancel\u00f3 \u00e9xito', null);
	listarAutorizacionesTecnicas(idReporteSiniestro);	
}

function regresarAOrdenDePago(form){
	var idReporteSiniestro = $('idToReporteSiniestro').value;	
	var idAutorizacionTecnica = $('numeroAutorizacionTecnica').value;
	
	url = '/MidasWeb/siniestro/finanzas/generarOrdenPago.do?' + 
		  'idToAutorizacionTecnica=' + idAutorizacionTecnica +
		  '&idToReporteSiniestro=' + idReporteSiniestro;
	
	sendRequest(form, url, 'contenido', null);
}

function regresarAListaOrdenesDePago(form){
	sendRequest(form,'/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do', 'contenido', null);
}

function regresarAAutorizacionTecnica(form){
	var idAutorizacionTecnica = $('idToAutorizacionTecnica').value;
	
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarAutorizacionTecnica.do?idAutorizacionTecnica=' + idAutorizacionTecnica, 'contenido', null);
}

function mostrarAutorizacionTecnica(){
	var tipoAutorizacion = $('idTipoAutorizacionTecnica').value;	
	var numeroAutorizacion = $('numeroAutorizacionTecnica').value;
	var pantalla = $('pantalla').value;
	var id = $('id').value;
	
	var parametros = "?id="+id+"&numeroAutorizacion="+numeroAutorizacion+"&pantalla="+pantalla;
	
	switch(tipoAutorizacion){
		case "1":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do' + parametros, 'contenido', null);
			break;
		}
		case "2":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIngreso.do' + parametros, 'contenido', null);
			break;
		}
		case "3":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do' + parametros, 'contenido', null);
			break;
		}
	}		
}

function goHome(){
	window.location = '/MidasWeb/sistema/inicio.do';		
}

function showLigasX(){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/showLigas.do', 'contenido', null);	
}

function asignaEstatusAT(estatus){
	var objEstatus = document.getElementById("estatusAT");
	objEstatus.value = estatus;
}

function verDetalleATecnica(tipoAutorizacion, id, numeroAutorizacion){	
	var pantalla = $('pantalla').value;
	
	var parametros = "?id="+id+"&numeroAutorizacion="+numeroAutorizacion+"&pantalla="+pantalla;
	
	switch(tipoAutorizacion){
		case "1":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do' + parametros, 'contenido', null);
			break;
		}
		case "2":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIngreso.do' + parametros, 'contenido', null);
			break;
		}
		case "3":{
			sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do' + parametros, 'contenido', null);
			break;
		}
	}		
}

function regresarAListadoDeAT(form){
	var idReporteSiniestro = $('idToReporteSiniestro').value;
	sendRequest(null,'/MidasWeb/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesTecnicas.do?idReporteSiniestro='+idReporteSiniestro, 'contenido', null);	
} 

