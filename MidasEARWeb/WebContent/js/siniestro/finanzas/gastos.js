function guardarGastoSiniestro(formGastos){	
	if(validarAltaGastoSiniestro()){
		var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarGastos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
		sendRequestProcesaRespuestaXML(formGastos,'/MidasWeb/siniestro/finanzas/guardarGastoSiniestro.do', null,funcionRedirecciona);
	}
}

function guardarCambiosGastoSiniestro(formGastos){
	if(validarCambioGastoSiniestro()){
		var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarGastos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
		sendRequestProcesaRespuestaXML(formGastos,'/MidasWeb/siniestro/finanzas/guardarCambiosGastoSiniestro.do', null, funcionRedirecciona);
		// sendRequest(formGastos,'/MidasWeb/siniestro/finanzas/guardarCambiosGastoSiniestro.do', 'contenido', null);
	}
}

function eliminarGastosSiniestro(){		
	var gastosPorElimininar = elementosSeleccionados('eliminarGasto');
	
	if(gastosPorElimininar > 0){
//		if(confirm('Esta seguro de eliminar los Gastos')){
//			sendRequest(formGastos,'/MidasWeb/siniestro/finanzas/eliminarGastos.do', 'contenido', null);
//		}		
		var url = "sendRequest(document.gastosForm,'/MidasWeb/siniestro/finanzas/eliminarGastos.do', 'contenido', 'redireccionaEliminarGastosSin()')";
		ventanaConfirmacionSiniestros('\u00BFEsta seguro de eliminar los Gastos?',url);
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe seleccionar al menos un gasto a eliminar');
	}			
}

function redireccionaEliminarGastosSin(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarGastos.do?idToReporteSiniestro="+$('idToReporteSiniestro').value+"','contenido',null);";
	mostrarVentanaMensaje(MOSTRAR_MENSAJE_OK_SINIESTRO,'El gasto se elimino con \u00e9xito', funcionRedirecciona);
}

function cancelarGastosSiniestro(){	
	var gastosPorCancelar = elementosSeleccionados('cancelarGasto');
	
	if(gastosPorCancelar > 0){
//		if(confirm('Esta seguro de cancelar los Gastos')){
//			sendRequest(formGastos,'/MidasWeb/siniestro/finanzas/cancelarGastos.do', 'contenido', null);
//		}	
		var url = "sendRequest(document.gastosForm,'/MidasWeb/siniestro/finanzas/cancelarGastos.do', 'contenido', null);";
		ventanaConfirmacionSiniestros('\u00BFEsta seguro de cancelar los Gastos?',url);
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe seleccionar al menos un gasto a cancelar');
	}	
}

function validarAltaGastoSiniestro(){	
	if($('conceptoGasto').value == null || $('conceptoGasto').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El campo Concepto del Gasto es requerido');
		$('conceptoGasto').focus();
		return false;
	}else if($('prestadorServicios').value == null || $('prestadorServicios').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El campo Prestador de Servicios es requerido');
		$('prestadorServicios').focus();
		return false;
	}else if($('montoGasto').value == null || $('montoGasto').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El monto del Gasto es requerido');
		$('montoGasto').focus();
		return false;
	}else if($('fechaGasto').value == null || $('fechaGasto').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'La fecha del gasto es requerida');
		$('fechaGasto').focus();
		return false;
	}else if($('descripcionGasto').value == null || $('descripcionGasto').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'La descripci\u00f3n del gasto es requerida');
		$('descripcionGasto').focus();
		return false;
	}else if(!comparaFechas($('fechaGasto').value,$('fechaEstimacionPago').value)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"La fecha de estimacion debe ser mayor a la fecha del gasto.", null);
			return false;
		}else{
		return validarIndicadoresImpuestos();
		}
}

function validarCambioGastoSiniestro(){	
//	var cheque = $('numeroCheque').value;
//	var noTransferencia = $('numeroTransferencia').value;
//	
//	if(cheque == "" && noTransferencia == ""){
//		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe especificar n\u00famero de cheque o n\u00famero de transferencia');
//	}else 
		return validarAltaGastoSiniestro();
	
/*	if($('numeroCheque').value == null || $('numeroCheque').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El n\u00f3mero de Cheque es requerido');
		$('numeroCheque').focus();
		return false;
	}if($('numeroTransferencia').value == null || $('numeroTransferencia').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El n\u00f3mero de transferencia es requerida');
		$('numeroTransferencia').focus();
		return false;		
	} else 
		return validarAltaGastoSiniestro();
*/		 	
}

function creaCalendarios(){
	//creaCalendars('fechaGasto,fechaRecepcion,fechaEstimacionPago,fechaEntrega,dobleCal#mCalIzq#mCalDer#btnDobleCal','SD');
	creaCalendarsX('fechaGasto,fechaRecepcion,fechaEstimacionPago,fechaEntrega');
}

function inicializaPaginaDeEdicion(){
	seleccionaIndicadores();
	//creaCalendars('fechaGasto,fechaRecepcion,fechaEstimacionPago,fechaEntrega,dobleCal#mCalIzq#mCalDer#btnDobleCal','SD');
	creaCalendarsX('fechaGasto,fechaRecepcion,fechaEstimacionPago,fechaEntrega');
}

function validarIndicadoresImpuestos(){
	if($('indIva').value == 'true'  && $('porcentajeIva').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA');
		$('porcentajeIva').focus();
		return false;
	}
	if($('indIvaRet').value == 'true' && $('porcentajeIvaRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % IVA Retencion');
		$('porcentajeIvaRetencion').focus();
		return false;
	}
	if($('indIsr').value == 'true' && $('porcentajeIsr').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR');
		$('porcentajeIsr').focus();
		return false;
	}
	if($('indIsrRet').value == 'true' && $('porcentajeIsrRetencion').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % ISR retencion');
		$('porcentajeIsrRetencion').focus();
		return false;
	}
	if($('indOtros').value == 'true' && $('porcentajeOtros').value == ''){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe capturar el % otros');
		$('porcentajeOtros').focus();
		return false;
	}
	return true;
}

function notificarGuardado(){
	var funcionRedirecciona = "sendRequest(null,'/MidasWeb/siniestro/finanzas/listarGastos.do?idReporteSiniestro="+document.forms[0].idToReporteSiniestro.value+"','contenido',null);";
	procesarRespuesta(funcionRedirecciona);
}

function mostrarAgregarGastos(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/gastosAjuste.do?idToReporteSiniestro='+idToReporteSiniestro,'contenido','creaCalendarios()');
}

function listarGastosSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/finanzas/listarGastos.do?idToReporteSiniestro='+idToReporteSiniestro,'contenido',null);
}