function mostrarReporteSPSinParametrosExcel(nombrePlantilla){
	var location ='/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do?nombrePlantilla='+nombrePlantilla+"&tipoArchivo=TIPO_XLS";
	window.open(location, "ReporteSiniestro");
}


function mostrarReporteSPSinParametros(nombrePlantilla){
	var location ='/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do?nombrePlantilla='+nombrePlantilla;
	window.open(location, "ReporteSiniestro");
}

function mostrarReporteSiniestralidadYAnexo(nombrePlantilla, fechaInicial, fechaFinal, nombreDelAgente, numeroDePoliza, nombreDelAsegurado){
	var location ='/MidasWeb/siniestro/reportes/mostrarReporteSiniestralidadYAnexo.do?nombrePlantilla='+nombrePlantilla 
		+  '&fechaInicial='+ fechaInicial + '&fechaFinal='+ fechaFinal + '&nombreDelAgente='+ nombreDelAgente 
		+ '&numeroDePoliza=' + numeroDePoliza + '&nombreDelAsegurado=' + nombreDelAsegurado;;
	window.open(location, "ReporteSiniestro");
}

function mostrarReporteSiniestrosRRCSONORv7(nombrePlantilla, fechaInicial, fechaFinal){
	var location ='/MidasWeb/siniestro/reportes/mostrarReporteSiniestrosRRCSONORv7.do?nombrePlantilla='+nombrePlantilla 
		+  '&fechaInicial='+ fechaInicial + '&fechaFinal='+ fechaFinal;;
	window.open(location, "ReporteSiniestro");
}

function mostrarReporteGastosSiniestro(){
	mostrarReporteSPSinParametrosExcel('ReporteGastosSiniestro');
}

function mostrarReporteInventarioSalvamento(){
	mostrarReporteSPSinParametrosExcel('ReporteInventarioSalvamento');
}

function mostrarReporteMovimientosSiniestrosConsolidado(){
//	mostrarReporteSPSinParametrosExcel('ReporteMovimientosSiniestrosConsolidado');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteMovimientosSiniestrosConsolidado','contenido','crearCalendario()');
}

function mostrarReporteMovimientosSiniestrosDetalle(){
//	mostrarReporteSPSinParametrosExcel('ReporteMovimientosSiniestrosDetalle');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteMovimientosSiniestrosDetalleContabilidad','contenido','crearCalendario()');
}

function mostrarReporteSiniestrosAjustador(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosAjustador');
}

function mostrarReporteSiniestrosAntiguedad(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosAntiguedad');
}

function mostrarReporteSiniestrosConReservaPendiente(){
//	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosConReservaPendiente');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteSiniestrosConReservaPendiente','contenido','crearCalendario()');
}


function mostrarReporteSiniestrosConReservaPendienteAnexo(){
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteSiniestroConReservaPendienteAnexo','contenido','crearCalendario()');
}

function mostrarReporteSiniestrosSinReserva(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosSinReserva');
}

function mostrarReporteSiniestrosTerminadosConReserva(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosTerminadosConReserva');
}

function mostrarReporteSiniestrosTerminadosIndocumentados(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosTerminadosIndocumentados');
}

function mostrarReporteVentaSalvamentos(){
	mostrarReporteSPSinParametrosExcel('ReporteVentaSalvamentos');
}

function mostrarCaratulaSiniestroPDF(){
	var ruta = "/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestroPDF.do";
	window.open(ruta,"CaratulaSiniestro");
}

function mostrarReporteInventarioSalvamentoAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteInventarioSalvamentoAnexo');
}

function mostrarReporteMovimientosSiniestrosConsolidadoAnexo(){
//	mostrarReporteSPSinParametrosExcel('ReporteMovimientosSiniestrosConsolidadoAnexo');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteMovimientosSiniestrosConsolidadoAnexo','contenido','crearCalendario()');
}

function mostrarReporteMovimientosSiniestrosDetalleAnexo(){
//	mostrarReporteSPSinParametrosExcel('ReporteMovimientosSiniestrosDetalleAnexo');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteMovimientosSiniestrosDetalleAnexo','contenido','crearCalendario()');
}

function mostrarReporteSiniestrosAjustadorAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosAjustadorAnexo');
}

function mostrarReporteSiniestrosTerminadosConReservaAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosTerminadosConReservaAnexo');
}

function mostrarReporteSiniestrosSinReservaAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosSinReservaAnexo');
}

function mostrarReporteVentaSalvamentoAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteVentaSalvamentoAnexo');
}

function mostrarReporteSiniestrosAntiguedadAnexo(){
	mostrarReporteSPSinParametrosExcel('ReporteSiniestrosAntiguedadAnexo');
}

function mostrarReporteGeneralSiniestrosReportados(){
//	mostrarReporteSPSinParametrosExcel('ReporteGeneralSiniestrosReportados');
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteGeneralSiniestrosReportados','contenido','crearCalendario()');
}

function mostrarReporteAutorizacionTecnicaGastos(idToGasto,numeroAutorizacion){
	var ruta = "/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do?id="+idToGasto+"&numeroAutorizacion="+numeroAutorizacion+"&tipoAutorizacionTecnica=Gasto";
	window.open(ruta,"AutorizacionTecnicaGastos");
}

function mostrarReporteAutorizacionTecnicaIndemnizacion(idToIndemnizacion,numeroAutorizacion){
	var ruta = "/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do?id="+idToIndemnizacion+"&numeroAutorizacion="+numeroAutorizacion+"&tipoAutorizacionTecnica=Indemnizacion";
	
	window.open(ruta,"AutorizacionTecnicaIndemnizacion");
}

function imprimirAgruparATGasto(idToOrdenPago){
	var ruta = "/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do?id="+idToOrdenPago+"&numeroAutorizacion=0&tipoAutorizacionTecnica=AgruparAT";
	mostrarIndicadorCargaGenerico('indicadorImprimirATOrdenPago');
	newwindow = window.open(ruta,"AutorizacionOrdenPago");
	ocultarIndicadorCargaGenerico('indicadorImprimirATOrdenPago');
}

function mostrarReporteSPFechas(){
	var nombrePlantilla = $('nombrePlantilla').value;
	var fechaInicial = $('fechaInicial').value;
	var fechaFinal = $('fechaFinal').value;
	
	var location ='/MidasWeb/siniestro/reportes/mostrarReporteSPFechas.do?nombrePlantilla='+nombrePlantilla 
		+  '&fechaInicial='+ fechaInicial + '&fechaFinal='+ fechaFinal;;
	window.open(location, "ReporteSiniestro");
}

function imprimirOrdenDePagoV2(ordenDePago){
	var ruta = "/MidasWeb/siniestro/finanzas/imprimirOrdenDePago.do?id="+ordenDePago+"&tipoAutorizacionTecnica=Gasto";
	newwindow = window.open(ruta,"OrdenPago");
}