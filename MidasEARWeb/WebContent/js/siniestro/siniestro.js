var MOSTRAR_MENSAJE_OK_SINIESTRO="30";
var MOSTRAR_MENSAJE_ALERT_SINIESTRO="20";
var MOSTRAR_MENSAJE_ERROR_SINIESTRO="10";

var ROL_CABINERO = "Rol_Cabinero";
var ROL_COORDINADOR_SINIESTROS = "Rol_Coord_Siniestros";
var ROL_COORDINADOR_SINIESTROS_FACULTATIVO = "Rol_Coord_Sin_Facult";

function sendRequestProcesaRespuestaXML(fobj, actionURL, targetId, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
				true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		}, // End of onComplete
		onSuccess : function(transport) {
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			if (items){
				var item = items[0];
				var id = item.getElementsByTagName("id")[0].firstChild.nodeValue;
				if (id==0){
					procesarRespuestaXml(transport.responseXML,null);
				}else{
					procesarRespuestaXml(transport.responseXML, pNextFunction);
				}
			}else{
				if (targetId && trim(targetId)>0)
					$(targetId).innerHTML=transport.responseXML;
				if (pNextFunction && trim(pNextFunction)>0)
					eval(pNextFunction);
			}
		} // End of onSuccess
	});
}

function mostrarAprobarInformeFinal(){
	var idToReporteSiniestro = $('idToReporteSiniestro').value;
	sendRequest(null, '/MidasWeb/siniestro/finanzas/presentarInformeFinal.do?idToReporteSiniestro='+idToReporteSiniestro,'contenido',null);
}


function soloDecimales(oControl, evt){   
    var charCode = (evt.which) ? evt.which : window.event.keyCode;

    if (String.fromCharCode(charCode)=="."){
		if (oControl.value == "") 
			return false; 
		if (oControl.value.indexOf('.') > 0) 
			return false;
    }
    if (charCode <= 13){
    	return true;
    }else{
    	var keyChar = String.fromCharCode(charCode);
    	var re = /[0-9.]/
    	return re.test(keyChar);
    }   
} 

function removeCurrency(strValue) {
	var objRegExp = /\(/;
	var strMinus = "";
	
	if (objRegExp.test(strValue)) {
		strMinus = "-";
	}
	
	objRegExp = /\)|\(|[,]/g;
	strValue = strValue.replace(objRegExp, "");
	
	if (strValue.indexOf("$") >= 0) {
		strValue = strValue.substring(1, strValue.length);
	}
	
	return strMinus + strValue;
}

function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g, "");
	
	if (isNaN(num)) {
		num = "0";
	}
	
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	
	if (cents < 10) {
		cents = "0" + cents;
	}
	
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
		num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
	}
	
	return (((sign) ? "" : "-") + "$" + num + "." + cents);
}

//var afterCloseVentanaConfirmacionSiniestros;
//function ventanaConfirmacionSiniestros(mensaje, afterClose) {
//	var mensajeAUsuario = null;
//	var ventanaInvocadora = null;
//	var zIndexInvocadora = 10;
//	
//	afterCloseVentanaConfirmacionSiniestros = afterClose;
//	alert('mugreritooo');
//	var html = "<div class=\"mensaje_encabezado\"></div>"
//		html = html  + "<div class=\"mensaje_contenido\" >"
//		html = html  + "<div id=\"mensajeImg\">"
//		html = html  + "<img src='/MidasWeb/img/question2.png'>";
//		html = html  + "</div>"	
//		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\">"
//		html = html  + mensaje;
//		html = html  + "</div>"
//		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">"
//		html = html  + "<table><tr><td width=\"80%\">&nbsp;</td><td width=\"50%\"><div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" aceptarVentanaConfirmacionSiniestro();\">Aceptar</a> </td> <td><div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" cerrarVentanaConfirmacion();\">Cancelar</a></div></td></tr></table>";
//		html = html  + "</div>"
//		html = html  + "</div>"
//		html = html  + "<div class=\"mensaje_pie\"></div>"
//		
//		var skin = "ventanas_midas";
//		alert('mugreritooo 2');
//		if (parent.dhxWins==null){
//			parent.dhxWins = new dhtmlXWindows();
//		}
//		alert('mugreritooo 3');		
//		if (parent.dhxWinsMsg==null){
//			parent.dhxWinsMsg = new parent.dhtmlXWindows();
//			parent.dhxWinsMsg.setSkin(skin);
//		}
//		alert('mugreritooo 4');
//		mensajeAUsuario = parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros");
//		if (mensajeAUsuario!=null && parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").isHidden()) {
//			parent.dhxWinsMsg.unload();
//			parent.dhxWinsMsg = new parent.dhtmlXWindows();
//			parent.dhxWinsMsg.setSkin(skin);
//			parent.dhxWinsMsg.hideHeader();
//			mensajeAUsuario=null;
//		}
//		alert('mugreritooo 7');
//		
//		if (mensajeAUsuario==null){
//			alert('mugreritooo 71');
//			var browserName=navigator.appName; 
//			alert('mugreritooo 72');
////			if (browserName=="Microsoft Internet Explorer"){
//				mensajeAUsuario = parent.dhxWinsMsg.createWindow("ventanaConfirmacionSiniestros", 500, 340, 420, 157);
////			}else{
////				mensajeAUsuario = parent.dhxWinsMsg.createWindow("ventanaConfirmacionSiniestros", 500, 340, 420, 157);
////			}
//			alert('mugreritooo 73');
//			mensajeAUsuario.center();
//			alert('mugreritooo 74');
//			mensajeAUsuario.hideHeader();
//			alert('mugreritooo 8');
//						
//			for (var a in parent.dhxWins.wins) {
//				if (parent.dhxWins.wins[a].isModal()) {
//					ventanaInvocadora = parent.dhxWins.wins[a];
//					ventanaInvocadora.setModal(false);
//					zIndexInvocadora = ventanaInvocadora.style.zIndex;
//					ventanaInvocadora.style.zIndex = 0;
//				}
//			}
//			alert('mugreritooo 9');
//			mensajeAUsuario.setModal(true);
//
//			mensajeAUsuario.attachHTMLString(html);
//			mensajeAUsuario.attachEvent("onClose", function(win){
//				mensajeAUsuario =null;
//				if(ventanaInvocadora != null) {
//					ventanaInvocadora.setModal(true);
//					ventanaInvocadora.style.zIndex = zIndexInvocadora;
//				}
//				});
//		}
//		alert('mugreritooo 10');
//		
//}
//
//function aceptarVentanaConfirmacionSiniestro() {
//	cerrarVentanaConfirmacion();
//	eval(afterCloseVentanaConfirmacionSiniestros);
//	
//}
//
//function cerrarVentanaConfirmacion() {
//	parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").setModal(false);
//	parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").hide();
//	parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").close();
//	parent.dhxWinsMsg.unload();
//	parent.dhxWinsMsg = null;
//}

var afterCloseVentanaConfirmacionSiniestros;
function ventanaConfirmacionSiniestros(mensaje, afterClose) {
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;
	
	afterCloseVentanaConfirmacionSiniestros = afterClose;
	var html = "<div class=\"mensaje_encabezado\"></div>"
		html = html  + "<div class=\"mensaje_contenido\" style=\"height: 110px; \">"
		html = html  + "<div id=\"mensajeImg\">"
		html = html  + "<img src='/MidasWeb/img/question2.png'>";
		html = html  + "</div>"	
		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\">"
		html = html  + mensaje;
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">"
		html = html  + "<table><tr><td width=\"80%\">&nbsp;</td><td width=\"50%\"><div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" aceptarVentanaConfirmacionSiniestro();\">Aceptar</a> </td> <td><div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" cerrarVentanaConfirmacion();\">Cancelar</a></div></td></tr></table>";
		html = html  + "</div>"
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_pie\"></div>"
		
		var skin = "ventanas_midas";
		
		if (parent.dhxWins==null){
			parent.dhxWins = new dhtmlXWindows();
		}
				
		if (parent.dhxWinsMsg==null){
			parent.dhxWinsMsg = new parent.dhtmlXWindows();
			parent.dhxWinsMsg.setSkin(skin);
		}
		
		mensajeAUsuario = parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros");
		if (mensajeAUsuario!=null && parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").isHidden()) {
			parent.dhxWinsMsg.unload();
			parent.dhxWinsMsg = new parent.dhtmlXWindows();
			parent.dhxWinsMsg.setSkin(skin);
			parent.dhxWinsMsg.hideHeader();
			mensajeAUsuario=null;
		}
		
		
		if (mensajeAUsuario==null){
			var browserName=navigator.appName; 
			
			if (browserName=="Microsoft Internet Explorer")
				mensajeAUsuario = parent.dhxWinsMsg.createWindow("ventanaConfirmacionSiniestros", 500, 340, 420, 157);
			else
				mensajeAUsuario = parent.dhxWinsMsg.createWindow("ventanaConfirmacionSiniestros", 500, 340, 420, 157);
			mensajeAUsuario.center();
			mensajeAUsuario.hideHeader();
						
			for (var a in parent.dhxWins.wins) {
				if (parent.dhxWins.wins[a].isModal()) {
					ventanaInvocadora = parent.dhxWins.wins[a];
					ventanaInvocadora.setModal(false);
					zIndexInvocadora = ventanaInvocadora.style.zIndex;
					ventanaInvocadora.style.zIndex = 0;
				}
			}
			
			mensajeAUsuario.setModal(true);

			mensajeAUsuario.attachHTMLString(html);
			mensajeAUsuario.attachEvent("onClose", function(win){
				mensajeAUsuario =null;
				if(ventanaInvocadora != null) {
					ventanaInvocadora.setModal(true);
					ventanaInvocadora.style.zIndex = zIndexInvocadora;
				}
				});
		}
		
}

function aceptarVentanaConfirmacionSiniestro() {
	cerrarVentanaConfirmacion();
	eval(afterCloseVentanaConfirmacionSiniestros);
	
}

function cerrarVentanaConfirmacion() {
	if(parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros") != null ){
		parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").setModal(false);
		parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").hide();
		parent.dhxWinsMsg.window("ventanaConfirmacionSiniestros").close();
		parent.dhxWinsMsg.unload();
		parent.dhxWinsMsg = null;
	}
}


function fechaActualSiniestros(){
	var fa = new Date();
	var dia = fa.getDate();
	var mes = (fa.getMonth()+1);
	var anio = fa.getFullYear();
	if(dia < 10){
		dia = '0'+ dia;
	}
	if(mes < 10){
		mes = '0'+ mes;
	}
	var fechaActual = dia+"/"+ mes + "/" +anio;
	return fechaActual;
}

