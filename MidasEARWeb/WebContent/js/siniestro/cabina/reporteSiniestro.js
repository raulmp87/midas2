function mostrarAjustadorNombradoSiniestros(){
	var varTipoNegocio = $('idTipoNegocio').value;
	var divIdTituloAjustadorNombrado = $('idTituloAjustadorNombrado');
	var divIdAjustadorNombrado = $('idAjustadorNombrado');
	if(varTipoNegocio == 95 || varTipoNegocio == 96  || varTipoNegocio == 97 ){ 		
		divIdTituloAjustadorNombrado.style.display='';
		divIdAjustadorNombrado.style.display='';
	}
	
}

function ocultaAjustadorNombradoSiniestros(){
	var divIdTituloAjustadorNombrado = $('idTituloAjustadorNombrado');
	var divIdAjustadorNombrado = $('idAjustadorNombrado');
	divIdAjustadorNombrado.style.display='none';
	divIdTituloAjustadorNombrado.style.display='none';
}
	
	
	
function validaPermiteAsignarAjustador(){
	var varTipoNegocio = $('idTipoNegocio').value;
	var rolesUser = $('rolesUsuario').value;
	if((varTipoNegocio !='' && varTipoNegocio != 99 && permitidoUsuarioSiniestro(ROL_COORDINADOR_SINIESTROS_FACULTATIVO))
		||((varTipoNegocio =='' || varTipoNegocio == 99) && (permitidoUsuarioSiniestro(ROL_COORDINADOR_SINIESTROS) || permitidoUsuarioSiniestro(ROL_CABINERO)))	
	){ 		
		comboAjustador = $('idAjustador');
		comboAjustador.disabled = false;
	}else{
		comboAjustador = $('idAjustador');
		comboAjustador.disabled = true;
		comboAjustador.value='';
	}
}

function permitidoUsuarioSiniestro(varRol){
	var rolesUser = $('rolesUsuario').value.split(",");	
	for(i=0;i<rolesUser.length;i++){
		if(varRol == rolesUser[i]){
			return true;
		}
	}
	return false;
}

function mostrarSeccionBusquedaPoliza(){
	var varIdToPoliza = $('idToPoliza').value;
	if(varIdToPoliza != null && varIdToPoliza != ""){
		var divSeccionDatosPoliza = $('seccionDatosPolizaCrear');
		divSeccionDatosPoliza.style.display='none';
	}else{
		if (divSeccionDatosPoliza)
			divSeccionDatosPoliza.style.display='';
	}
}
function crearReporteSiniestroNumero(idReporte, nombrePersonaReporta, telefonoPersonaReporta, fecha, hora) {	
	if(nombrePersonaReporta !== null && telefonoPersonaReporta !== null){
		new Ajax.Request("/MidasWeb/siniestro/cabina/crearReporte.do", {
			method : "post",
			asynchronous : false,
			parameters : "id="+idReporte+"&nombre="+nombrePersonaReporta + 
						 "&telefono="+telefonoPersonaReporta+"&fecha="+fecha+"&hora="+hora+"&numeroPoliza="+$('numeroPoliza').value,
			onSuccess : function(transport) {
				imprimeNumeroReporteSiniestro(transport.responseXML);
			} // End of onSuccess
		});
	}
}

function imprimeNumeroReporteSiniestro(doc){
	var numero = doc.getElementsByTagName("numero")[0].firstChild.nodeValue;		
	$('numeroReporteDiv').innerHTML = numero;	
	$('numeroReporte').value = numero;
	var id = doc.getElementsByTagName("id")[0].firstChild.nodeValue;		
	$('idToReporteSiniestro').value = id;
}
function validarAsignarCoordinadorXZona(){
	if($('numeroPoliza').value == null || $('numeroPoliza').value == ''){
		return true;
	}else if($('tipoNegocio').value == null || $('tipoNegocio').value == '' || $('tipoNegocio').value == 99 ){
		return true;
	}
	return false;		
} 

function asignarCoordinadorPorZona(idEstado) {	
	if(validarAsignarCoordinadorXZona()){
		new Ajax.Request("/MidasWeb/siniestro/cabina/asignarCoordinadorPorZona.do", {
			method : "post",
			asynchronous : false,
			parameters : "idEstado="+idEstado,
			onSuccess : function(transport) {
			asignarValorCoordinadorPorZona(transport.responseXML);
			} // End of onSuccess
		});
	}
}

function asignarValorCoordinadorPorZona(doc){
	var idCoordinador = doc.getElementsByTagName("id")[0].firstChild.nodeValue;		
	$('idCoordinador').value = idCoordinador;
}


function validarFechaOcurrenciaSiniestro(fechaOcurrencia,fechaReporte){
	ok = true;
	if(fechaOcurrencia!=='' && fechaReporte !==''){
		if( !fechaMenorIgualQue(fechaOcurrencia, fechaReporte)){	
			mostrarVentanaMensaje('10','La fecha del siniestro debe ser menor o igual que la fecha del reporte.',null);
			ok = false;
		}
	}
	return ok;	
}

function validarFechaReporteSiniestro(fechaReporte,fechaAsignacionAjustador){
	ok = true;
	if(fechaReporte!=='' && fechaAsignacionAjustador !==''){
		if( !fechaMenorIgualQue(fechaReporte, fechaAsignacionAjustador)){	
			mostrarVentanaMensaje('10','La fecha del reporte debe ser menor o igual que la fecha de asignaci&oacute;n del ajustador.',null);
			ok = false;
		}
	}
	return ok;	
}

//Funcion Principal de Validacion

function validaGuardarReporteSiniestro(variableReporteSiniestroForm){
	varFechaSiniestro = $('fechaSiniestro').value;
	varFechaReporte =  $('fechaReporte').value;
	varFechaAsignacionAjustador = $('fechaAsignacionAjustador').value;

	if(!validarFechaOcurrenciaSiniestro(varFechaSiniestro,varFechaReporte)){
		return false;
	}
	if(!validarFechaReporteSiniestro(varFechaReporte,varFechaAsignacionAjustador)){
		return false;
	}
	if(!validaTelefonoReporta()){
		return false;
	}
	if(!validaTelefonoContacto()){
		return false;
	}
	if(!validaCodigoPostalSiniestro()){
		return false;
	}
	if(!validaFechaAsignoPoliza()){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'La fecha del siniestro no es igual a la fecha con la que se asigno la poliza: '+$('fechaAsignoPoliza').value ,null);
		return false;
	}
//		sendRequest(document.reporteSiniestroForm,'/MidasWeb/siniestro/cabina/guardarReporte.do', 'contenido',null);
	reporteSiniestroGuardar(variableReporteSiniestroForm);
}

function validaFechaAsignoPoliza(){
	varFechaSiniestro = $('fechaSiniestro').value;
	varFechaAsignoPoliza = $('fechaAsignoPoliza').value;
	if(varFechaSiniestro === varFechaAsignoPoliza){
		return true;
	}
	return false;
}

function validaTelefonoReporta(){
	var telefonoRep = $('telefonoPersonaReporta').value;
	if(telefonoRep.length > 19){
		mostrarVentanaMensaje('10','El telefono de la persona que reporta no puede ser mayor a 19 digitos',null);
		return false;
	}
	return true;
}	
function validaTelefonoContacto(){
	var telefonocont= $('telefonoContacto').value;
	if(telefonocont.length > 19){
		mostrarVentanaMensaje('10','El telefono de la persona de contacto no puede ser mayor a 19 digitos',null);
		return false;
	}
	return true;
}

function validaCodigoPostalSiniestro(){
	var telefonocont= $('codigoPostal').value;
	if(telefonocont.length > 10){
		mostrarVentanaMensaje('10','El codigo postal no puede ser mayor a 19 digitos',null);
		return false;
	}
	return true;
}

// funciones Generales

function comparaFechas(varFecha1,varFecha2){

	if(varFecha1!=null && varFecha1!='' && varFecha2!=null && varFecha2!=''){
		var fechaMenorArr = varFecha1.split("/");	
		var fechaMayorArr = varFecha2.split("/");
	
		diaMenor = fechaMenorArr[0];
		mesMenor = fechaMenorArr[1];
		anioMenor = fechaMenorArr[2];
	
		diaMayor = fechaMayorArr[0];
		mesMayor = fechaMayorArr[1];
		anioMayor = fechaMayorArr[2];
		
		if(anioMenor > anioMayor){
			return false;
		}		
		if(mesMenor > mesMayor){
			return false;
		}
		if(diaMenor > diaMayor){
			return false;
		}
		return true ;	
	}
	
}
//Termina funciones Generales

function habilitaTabPoliza(valorIdPoliza, valorIdAjustador){
	if($('fechaSiniestro').value != '' ){
		$('idToPoliza').value = valorIdPoliza; 
		$('idAjustador').value = valorIdAjustador;
		crearReporteSiniestro.enableTab('datosPoliza',true);
		crearReporteSiniestro.setTabActive('datosPoliza');
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Debe capturar la fecha del siniestro para asignar poliza", null);
	}
}

function datosPolizaTab(){
	var idToPoliza=$('idToPoliza').value;
	var varFechaSiniestro = $('fechaSiniestro').value;
	sendRequest(null,'/MidasWeb/siniestro/cabina/datosPolizaCrear.do?idToPoliza='+idToPoliza+'&fechaSiniestro='+varFechaSiniestro, 
			'contenido_datosPoliza',"muestraListaIncisosPolizaCrear();");
}

function datosPolizaTabModificar(){
	var idToPoliza=$('idToPoliza').value;
	var idToReporteSiniestro = $('idToReporteSiniestro').value
	var varFechaSiniestro = $('fechaSiniestro').value;
	sendRequest(null,'/MidasWeb/siniestro/cabina/reportePoliza.do?idToPoliza='+idToPoliza+'&idToReporteSiniestro='+idToReporteSiniestro+'&fechaSiniestro='+varFechaSiniestro, 
			'contenido_datosPoliza',"muestraListaIncisosPolizaCrear();");
}

function datosEstadoSiniestro(estadosiniestroForm){	
	sendRequest(estadosiniestroForm,'/MidasWeb/siniestro/cabina/reporteEstadoSiniestro.do?idToReporteSiniestro='+$('idToReporteSiniestro').value, 'contenido_estadoSiniestro', 'creaCalendariosBitacora();');
}

function datosEstadoSiniestroDesplegar(estadosiniestroForm){	
	sendRequest(estadosiniestroForm,'/MidasWeb/siniestro/cabina/desplegarDatosEstadoSiniestro.do?idToReporteSiniestro='+$('idToReporteSiniestro').value, 'contenido_estadoSiniestro', null);
}
function creaCalendariosBitacora(){
	creaCalendars('fechaAsignacion,fechaContactoAjustador,fechaInspeccionAjustador,fechaPreliminar,fechaDocumento,fechaInformeFinal,fechaTerminacionSiniestro,fechaCerrado','S,S,S,S,S,S,S,S')
}



function datosTransporte(transporteForm){
	sendRequest(null,'/MidasWeb/siniestro/cabina/reporteTransportes.do?idToReporteSiniestro='+$('idToReporteSiniestro').value, 'contenido_transportes', null);
}

function asignarPolizaModificar(){
	crearReporteSiniestro.enableTab('coberturaRiesgo',true);
	if($('productoTransportePoliza').value == "true" ){
		crearReporteSiniestro.enableTab('transportes', true);
	}else{
		crearReporteSiniestro.disableTab('transportes',true);
	}
	var varNumeroPoliza = $('idNumeroPolizaDatos').value;
	var varTipoNegocio = $('idCodigoTipoNegocioDatos').value;
	crearReporteSiniestro.setTabActive('reporteSiniestro');
	divSeccionDatosPoliza = $('seccionDatosPolizaCrear');
	divSeccionDatosPoliza.style.display='none';
	divIdDireccionPolizaCrear = $('idDireccionPolizaCrear');
	divIdDireccionPolizaCrear.style.display='';
	$('numeroPoliza').value = varNumeroPoliza ;
	$('idTipoNegocio').value = varTipoNegocio ;
	$('tipoNegocio').value  = varTipoNegocio ;
//	Agregados de Poliza para reportes
	$('numeroEndoso').value 	= $('polizaNumeroEndoso').value;
	$('idAgente').value 		= $('polizaIdAgente').value;
	$('nombreAgente').value 	= $('polizaNombreAgente').value;
	$('nombreOficina').value 	= $('polizaNombreOficina').value;
	$('nombreAsegurado').value 	= $('polizaNombreAsegurado').value;
	$('idCliente').value 		= $('polizaIdCliente').value;
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
	guardarAsignacionPolizaModificar();
	llenarComboCoordinadores(varTipoNegocio);
	validaPermiteAsignarAjustador();
	mostrarAjustadorNombradoSiniestros();
//	if(varTipoNegocio !='' && varTipoNegocio != 99){ // OJO posible bug		
//		comboAjustador = $('idAjustador');
//		comboAjustador.disabled = true;
//		comboAjustador.value='';
//	}
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
	
	
}

function guardarAsignacionPolizaModificar() {	
		new Ajax.Request("/MidasWeb/siniestro/cabina/guardarAsignacionPolizaModificar.do", {
			method : "post",
			asynchronous : false,
//			parameters :'?'+document.reporteSiniestroForm.serialize(),
			parameters : (document.reporteSiniestroForm !== undefined && document.reporteSiniestroForm !== null) ? $(document.reporteSiniestroForm).serialize(true) : null, asynchronous : false,
			onSuccess : function(transport) {
				mostrarVentanaMensaje('30','La poliza se asigno de forma correcta', null);
			} // End of onSuccess
		});
}

function iniciaModificarReporteSinietro(){
	mostrarAjustadorNombradoSiniestros();
	llenarComboCoordinadores($('idTipoNegocio').value);
	validaPermiteAsignarAjustador();
	//Recarga el combo con los ajustadores de la ciudad
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
	//Establece manualmente el ajustador seleccionado
	
//	($('idAjustador')){
//		var idAjustadorSel = $('idAjustador').value;
//		$('idAjustador').value = idAjustadorSel;
//	}
	creaCalendars('fechaSiniestro','S');
	
}

function asignarPoliza(varNumeroPoliza,varTipoNegocio){
	crearReporteSiniestro.setTabActive('reporteSiniestro');
	divSeccionDatosPoliza = $('seccionDatosPolizaCrear');
	divSeccionDatosPoliza.style.display='none';
	divIdDireccionPolizaCrear = $('idDireccionPolizaCrear');
	divIdDireccionPolizaCrear.style.display='';
	
		$('numeroPoliza').value = varNumeroPoliza ;
		$('idTipoNegocio').value = varTipoNegocio ;
		$('tipoNegocio').value = varTipoNegocio ;
//	Agregados de Poliza para reportes
		$('numeroEndoso').value 	= $('polizaNumeroEndoso').value;
		$('idAgente').value 		= $('polizaIdAgente').value;
		$('nombreAgente').value 	= $('polizaNombreAgente').value;
		$('nombreOficina').value 	= $('polizaNombreOficina').value;
		$('nombreAsegurado').value 	= $('polizaNombreAsegurado').value;
		$('idCliente').value 		= $('polizaIdCliente').value;
	
	llenarComboCoordinadores(varTipoNegocio);
	validaPermiteAsignarAjustador();
	mostrarAjustadorNombradoSiniestros();
	$('fechaAsignoPoliza').value = $('fechaSiniestro').value;
//	if(varTipoNegocio !='' && varTipoNegocio != 99){		
//		comboAjustador = $('idAjustador');
//		comboAjustador.disabled = true;
//		comboAjustador.value='';
//	}
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
	mostrarVentanaMensaje('30','La poliza se asigno de forma correcta', null);
}
function desasignarPoliza(){
	
	crearReporteSiniestro.setTabActive('reporteSiniestro');
	var divSeccionDatosPoliza = $('seccionDatosPolizaCrear');
	divSeccionDatosPoliza.style.display='';
	var divIdDireccionPolizaCrear = $('idDireccionPolizaCrear');
	divIdDireccionPolizaCrear.style.display='none';

	$('idToPoliza').value='';
	$('numeroPoliza').value = '' ;
	$('idTipoNegocio').value = '' ;
	$('idCoordinador').value = '' ;		
	$('tipoNegocio').value = '' ;
	$('idAjustador').value='';
	$('numeroEndoso').value 	= '' ;
	$('idAgente').value 		= '' ;
	$('nombreAgente').value 	= '' ;
	$('nombreOficina').value 	= '' ;
	$('nombreAsegurado').value 	= '' ;
	$('idCliente').value 		= '' ;
	$('fechaAsignoPoliza').value = $('fechaSiniestro').value;
	crearReporteSiniestro.disableTab('datosPoliza',true);
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
	llenarComboCoordinadores($('idTipoNegocio').value);
	validaPermiteAsignarAjustador();
	ocultaAjustadorNombradoSiniestros();
	guardarDesAsignacionPoliza();
	
}

function guardarDesAsignacionPoliza() {	
	new Ajax.Request("/MidasWeb/siniestro/cabina/guardarAsignacionPolizaModificar.do", {
		method : "post",
		asynchronous : false,
//		parameters :'?'+document.reporteSiniestroForm.serialize(),
		parameters : (document.reporteSiniestroForm !== undefined && document.reporteSiniestroForm !== null) ? $(document.reporteSiniestroForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
			mostrarVentanaMensaje('30','La poliza se desasigno de forma correcta', null);
		} // End of onSuccess
	});
}

function cargarDireccionPoliza(objCheck){
	if(objCheck.checked){
		new Ajax.Request('/MidasWeb/siniestro/cabina/direccionPoliza.do', {
			method : "post",
			parameters : "idToPoliza=" + +$('idToPoliza').value,
			asynchronous : true,
			onCreate : function(transport) {
				mostrarIndicadorCargaGenerico("procesandoSiniestros");
			},
			onSuccess : function(transport) {
			loadDireccionPoliza(transport.responseXML);
			} // End of onSuccess
		});
	}else{
		$('calle').value='';
		$('codigoPostal').value='';
		$('idEstado').value='';
		$('idCiudad').value='';
		$('idColonia').value='';
	}
}
function loadDireccionPoliza(doc) {
		var items = doc.getElementsByTagName("item");
		var item = items[0];
		var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		var valores = text.split(",");
//		var colo='20270-3';
		$('calle').value = valores[0];
		$('codigoPostal').value = valores[1];
		ocultarIndicadorCargaGenerico("procesandoSiniestros");
		getColoniasPorCP($('codigoPostal').value, 'idColonia','idCiudad','idEstado');
		
//		$('idEstado').value = valores[2];
//		getCiudades($('idEstado'),'idCiudad');
//		$('idCiudad').value = valores[3];
//		getColonias($('idCiudad'),'idColonia');
//		setTimeout(null, 3000);
//		$('idColonia').value = valores[4];
}

function iniciaFuncionesReporte(){
	var varTipoNegocio = $('idTipoNegocio').value;
	creaCalendars('fechaSiniestro','S');
	if(varTipoNegocio ==''){	
		crearReporteSiniestro.disableTab('datosPoliza',true);
	}else{
		llenarComboCoordinadores(varTipoNegocio);
	}
}

function llenarComboCoordinadores(tipoNegocio) {
	var url = '/MidasWeb/siniestro/cabina/comboCoordinadores.do';
	
	if(tipoNegocio != null){
		url = url + '?tipoNegocio=' + tipoNegocio;
	}
			
	new Ajax.Request(url, {
		method : "post",
		asynchronous : false,
		onSuccess : function(transport) {
		loadComboCoordinadores( transport.responseXML);
		} // End of onSuccess
	});
}

function loadComboCoordinadores(doc) {
	var selectedCombo = $('idCoordinador');
	var valorCoordinador = selectedCombo.value; 
	
	if (selectedCombo != null) {
		removeAllOptionsExt(selectedCombo);
		var items = doc.getElementsByTagName("item");
		//valorCoordinador = items[0].getElementsByTagName("id")[0].firstChild.nodeValue;
		for ( var x = 0; x < items.length; x++) {
			var item = items[x];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(selectedCombo, text, value);
		} // End of for
	} // End of if
	
	$('idCoordinador').value= valorCoordinador;
}



function reporteSiniestroGuardar(varReporteSiniestroForm){
	new Ajax.Request('/MidasWeb/siniestro/cabina/guardarReporte.do',{
		method : "post", asynchronous : false,
		parameters : (varReporteSiniestroForm !== undefined && varReporteSiniestroForm !== null) ? $(varReporteSiniestroForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
//			document.getElementById("crearReporteSiniestro").innerHTML = transport.responseText;
//			if (_isIE) {
			var temporal = transport.responseText;
			var valorDiv = "contenido";
			if(temporal.indexOf('/siniestro/cabina/listar.do')!= -1){
				mostrarVentanaMensaje('30','El reporte de siniestro se guard\u00f3 con \u00e9xito', 'listarReportesSiniestro()');
			}else{
				document.getElementById(valorDiv).innerHTML = transport.responseText;
				dhx_init_tabbars();
				iniciaFuncionesReporte();
			}
		}
	});
}


//Funciones para el Caso de Uso de Asignar Ajustador

//function llenarComboAjustadores(varTipoNegocio,varCiudad) {
////	varTipoNegocio = $('tipoNegocio').value;
////	varCiudad = $('idCiudad').value;
//	new Ajax.Request('/MidasWeb/siniestro/cabina/obtenerListaAjustadores.do', {
//		method : "post",
//		parameters : "ciudad=" + varCiudad +"&tipoNegocio=" + varTipoNegocio,
//		asynchronous : false,
//		onSuccess : function(transport) {
//		loadComboAjustadores( transport.responseXML);
//		} // End of onSuccess
//	});
//}
//
//function loadComboAjustadores(doc) {
//	var selectedCombo = $('idAjustador');
//	var valorAjustador;
//	if (selectedCombo != null) {
//		removeAllOptionsExt(selectedCombo);
//		var items = doc.getElementsByTagName("item");
//		valorAjustador = items[0].getElementsByTagName("id")[0].firstChild.nodeValue;
//		for ( var x = 1; x < items.length; x++) {
//			var item = items[x];
//			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
//			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
//			addOption(selectedCombo, text, value);
//		} // End of for
//	} // End of if
//	$('idAjustador').value= valorAjustador;
//}

function asignaAjustador() {
	var varIdAjustador = $('idAjustador').value;
	//Se verifica que se haya asignado un ajustador
	if(varIdAjustador==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de seleccionar un ajustador", null);
		return false;
	}
	var varIdToReporteSiniestro = $('idToReporteSiniestro').value;
	new Ajax.Request('/MidasWeb/siniestro/cabina/asignarAjustador.do', {
		method : "post",
		parameters : "idAjustador=" + varIdAjustador +"&idToReporteSiniestro=" + varIdToReporteSiniestro,
		asynchronous : false,
		onSuccess : function(transport) {
//			alert('termina');
//			var items = transport.responseXML.getElementsByTagName("item");
//			var ajustadorConfirmado = items[0].getElementsByTagName("id")[0].firstChild.nodeValue;
//			alert('El Ajustador ha sido asignado');
			document.getElementById("idAsignarAjustador").disabled=true;
			mostrarVentanaMensaje('30','El Ajustador ha sido asignado', null);
		} // End of onSuccess
	});
}

function confirmarAjustadorReporte() {
	var varIdAjustador = $('idAjustador').value;
	//Se verifica que se haya asignado un ajustador
	if(varIdAjustador==""){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Favor de seleccionar un ajustador", null);
		return false;
	}
	var varIdToReporteSiniestro = $('idToReporteSiniestro').value;
	new Ajax.Request('/MidasWeb/siniestro/cabina/confirmarAjustador.do', {
		method : "post",
		parameters : "idAjustador=" + varIdAjustador +"&idToReporteSiniestro=" + varIdToReporteSiniestro,
		asynchronous : false,
		onSuccess : function(transport) {
//			alert('termina');
//			var items = transport.responseXML.getElementsByTagName("item");
//			var ajustadorConfirmado = items[0].getElementsByTagName("id")[0].firstChild.nodeValue;
//			alert('El Ajustador ha sido confirmado');
			mostrarVentanaMensaje('30','El Ajustador ha sido confirmado', null);
			desabilitaBotonesAjustador();
		} // End of onSuccess
	});
}

function rechazarAjustador() {
	var varIdToReporteSiniestro = $('idToReporteSiniestro').value;
	new Ajax.Request('/MidasWeb/siniestro/cabina/rechazarAjustador.do', {
		method : "post",
		parameters : "idToReporteSiniestro=" + varIdToReporteSiniestro,
		asynchronous : false,
		onSuccess : function(transport) {
//			alert('termina');
//			var items = transport.responseXML.getElementsByTagName("item");
//			var ajustadorConfirmado = items[0].getElementsByTagName("id")[0].firstChild.nodeValue;
//			alert('El Ajustador ha sido rechazado');
			mostrarVentanaMensaje('30','El Ajustador ha sido rechazado', null);
			desabilitaBotonesAjustador();
		} // End of onSuccess
	});
}

function desabilitaBotonesAjustador(){
	document.getElementById("idConfirmarAjustador").disabled=true;
	document.getElementById("idRechazaAjustador").disabled = true;
}
function regresarListaReportes(){
	listarReportesSiniestro();
}

function guardarReporteModificar(varReporteSiniestroForm){
	varFechaSiniestro = $('fechaSiniestro').value;	
	varFechaReporte =  $('fechaReporte').value;	
	varFechaAsignacionAjustador = $('fechaAsignacionAjustador').value;

	if(!validarFechaOcurrenciaSiniestro(varFechaSiniestro,varFechaReporte)){
		return false;
	}
	if(!validarFechaReporteSiniestro(varFechaReporte,varFechaAsignacionAjustador)){
		return false;
	}
	if(!validaTelefonoReporta()){
		return false;
	}
	if(!validaTelefonoContacto()){
		return false;
	}
	if(!validaCodigoPostalSiniestro()){
		return false;
	}
	if(!validaFechaAsignoPoliza()){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'La fecha del siniestro no es igual a la fecha con la que se asigno la poliza: '+$('fechaAsignoPoliza').value ,null);
		//		ventanaConfirmacionSiniestros('Si modifica la fecha del sinietro se va a desasignar la poliza','desasignarPoliza()');
		return false;
	}
	
	 saveModificationReport(varReporteSiniestroForm)
}

function saveModificationReport(varReporteSiniestroForm){
	new Ajax.Request('/MidasWeb/siniestro/cabina/repoGuadSini.do',{
		method : "post", asynchronous : false,
		parameters : (varReporteSiniestroForm !== undefined && varReporteSiniestroForm !== null) ? $(varReporteSiniestroForm).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
//			document.getElementById("crearReporteSiniestro").innerHTML = transport.responseText;
//			if (_isIE) {
				var temporal = transport.responseText;
				var valorDiv = "contenido";
				if(temporal.indexOf('/siniestro/cabina/listar.do')!= -1){
					mostrarVentanaMensaje('30','El reporte de siniestro se guard\u00f3 con \u00e9xito', 'listarReportesSiniestro()');
				}else{
					document.getElementById(valorDiv).innerHTML = transport.responseText;
					dhx_init_tabbars();
					iniciaModificarReporteSinietro();
					validaInformacionADesplegar();
					mostrarSeccionBusquedaPoliza();
				}
		}
	});
	
}

function buscarListaPolizas(){
	varNumeroPoliza = $('numeroPolizaBuscar').value;
	varNombreAsegurado = $('idNombreAsegurado').value;
	if(validaFormatoPolizaCompletoSiniestro(varNumeroPoliza)){
		mostrarPolizaPorReporteSiniestroGrids(varNumeroPoliza,varNombreAsegurado);
	}
}

function listarReporteSiniestro_CB(){
	creaCalendars('fechaReporte','S');
	var valorTipoPersona=0;
	for(var i=0; i < document.reporteSiniestroForm.tipoPersona.length;i++){
		if(document.reporteSiniestroForm.tipoPersona[i].checked){
			valorTipoPersona=document.reporteSiniestroForm.tipoPersona[i].value;
		}
	}
	if(valorTipoPersona==1){
		setPersonaFisica();
	}else if(valorTipoPersona==2){
		setPersonaMoral();
	}
}

function setPersonaFisica(){
	document.getElementById("primerNombre").readOnly=false;
	document.getElementById("segundoNombre").readOnly=false;
	document.getElementById("apellidoPaterno").readOnly=false;
	document.getElementById("apellidoMaterno").readOnly=false;
	document.getElementById("razonSocial").readOnly=true;
	document.getElementById("razonSocial").value='';
	document.getElementById("primerNombre").focus();
}

function setPersonaMoral(){
	document.getElementById("primerNombre").value='';
	document.getElementById("primerNombre").readOnly=true;
	document.getElementById("segundoNombre").value='';
	document.getElementById("segundoNombre").readOnly=true;
	document.getElementById("apellidoPaterno").value='';
	document.getElementById("apellidoPaterno").readOnly=true;
	document.getElementById("apellidoMaterno").value='';
	document.getElementById("apellidoMaterno").readOnly=true;
	document.getElementById("razonSocial").readOnly=false;
	document.getElementById("razonSocial").focus();
}



function listarReportesSiniestro(){
	sendRequest(null,"/MidasWeb/siniestro/cabina/listar.do","contenido",'iniciaComponentesListarReportesSiniestro()');
}
function iniciaComponentesListarReportesSiniestro(){
	creaCalendars("fechaReporte","S");
	listarTodosReportesSiniestroGrid();
}

function mensajeProcedeAnalisisInterno(){
	mostrarVentanaMensaje('30','El analisis interno procedio con \u00e9xito', 'listarReportesSiniestro()');
}

function mensajeNoProcedeAnalisisInterno(){
	mostrarVentanaMensaje('30','El analisis interno No procedio', 'listarReportesSiniestro()');
}

function mensajeProcedeIndemnizacion(){
	mostrarVentanaMensaje('30','El informe se envio a Indemnizaci\u00f3n \u00e9xito', 'listarReportesSiniestro()');
}

function mensajeNoProcedeIndemnizacion(){
	mostrarVentanaMensaje('30','El informe No se envio Indemnizaci\u00f3n', 'listarReportesSiniestro()');
}

var polizaPorReporteSiniestroPath;
var polizaPorReporteSiniestroGrid;
function mostrarPolizaPorReporteSiniestroGrids(varNumeroPoliza,varNombreAsegurado){
	var polizaPorReporteSiniestroDiv = 'idListaPolizas';
	$(polizaPorReporteSiniestroDiv).style.height='250px';
	polizaPorReporteSiniestroPath = '/MidasWeb/siniestro/cabina/listarPolizasSiniestro.do?numeroPoliza='+varNumeroPoliza+'&nombreAsegurado='+varNombreAsegurado;
	polizaPorReporteSiniestroGrid = new dhtmlXGridObject(polizaPorReporteSiniestroDiv);							  
	polizaPorReporteSiniestroGrid.setHeader(",No. Poliza,Asegurado,Producto,Tipo Poliza,Tipo Negocio");
	polizaPorReporteSiniestroGrid.setColumnIds("acciones,numPoliza,asegurado,producto,tipoPoliza,tipoNegocio");
	polizaPorReporteSiniestroGrid.setInitWidths("30,150,*,100,100,100");
	polizaPorReporteSiniestroGrid.setColAlign("center,center,center,center,center,center");
	polizaPorReporteSiniestroGrid.setColSorting("str,str,str,str,str,str");
	polizaPorReporteSiniestroGrid.setColTypes("img,ro,ro,ro,ro,ro");
	polizaPorReporteSiniestroGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	polizaPorReporteSiniestroGrid.setSkin("light");
	polizaPorReporteSiniestroGrid.enableDragAndDrop(false);
	polizaPorReporteSiniestroGrid.enableLightMouseNavigation(false);
	
	polizaPorReporteSiniestroGrid.enablePaging(true,9,3,"pagingArea",true,"infoArea");
	polizaPorReporteSiniestroGrid.setPagingSkin("bricks");
	polizaPorReporteSiniestroGrid.init();
	mostrarIndicadorCargaGenerico('indicadorCargaPolizas');
	polizaPorReporteSiniestroGrid.load(polizaPorReporteSiniestroPath, ocultarIndicadorPolizaPorReporteSiniestroGrids, 'json');
}

function ocultarIndicadorPolizaPorReporteSiniestroGrids(){
	ocultarIndicadorCargaGenerico('indicadorCargaPolizas');
}

//cambiar el path para que funcione
var listaReporteSiniestroPath;
var listaReporteSiniestroGrid;

function listarTodosReportesSiniestroGrid(){
	listaReporteSiniestroPath = '/MidasWeb/siniestro/cabina/listarTodos.do';
	mostrarListaReporteSiniestroGrids(false);
}

function listarFiltradoReportesSiniestroGrid(){
	var numeroPolizaBuscar = $('numeroPolizaBuscar').value;
	if(validaFormatoPolizaCompletoSiniestro(numeroPolizaBuscar)){
		if (_isIE){
			listaReporteSiniestroPath = '/MidasWeb/siniestro/cabina/listarFiltrado.do?'+$('reporteSiniestroForm').serialize();
		}else{
			listaReporteSiniestroPath = '/MidasWeb/siniestro/cabina/listarFiltrado.do?'+document.reporteSiniestroForm.serialize();
		}
		mostrarListaReporteSiniestroGrids(true);
	}
}

function mostrarListaReporteSiniestroGrids(mostrarMensajeSinRegistros){
	var resultadosReporteSiniestroDiv = 'resultadosListaSiniestros';
	listaReporteSiniestroGrid = new dhtmlXGridObject(resultadosReporteSiniestroDiv);	
	listaReporteSiniestroGrid.setHeader("Num. Reporte,Fecha,Num Poliza,Nombre/Raz&oacute;n social ," +
										", , , , , , , , , , ,");
	listaReporteSiniestroGrid.setColumnIds("numReporte,fecha,numPoliza,nombre," +
		"caratula,reporte,salvamento,ingresos,gastos,modificarReserva,ordenesPago,documentos,autorizacionesTecnicas,finiquito,indemnizacion,listarReservas");
	listaReporteSiniestroGrid.setInitWidths("100,100,125,*," +
			"25,25,25,25,25,25,25,25,25,25,25,25");
	listaReporteSiniestroGrid.setColAlign("center,center,center,center," +
		"center,center,center,center,center,center,center,center,center,center,center,center");
	listaReporteSiniestroGrid.setColSorting("str,str,str,str," +
			"str,str,str,str,str,str,str,str,str,str,str,str");
	listaReporteSiniestroGrid.setColTypes("ro,ro,ro,ro," +
		"img,img,img,img,img,img,img,img,img,img,img,img");		
	listaReporteSiniestroGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	listaReporteSiniestroGrid.setSkin("light");
	listaReporteSiniestroGrid.enableDragAndDrop(false);
	listaReporteSiniestroGrid.enableLightMouseNavigation(false);
	listaReporteSiniestroGrid.setColumnHidden(listaReporteSiniestroGrid.getColIndexById('finiquito'),true);
	listaReporteSiniestroGrid.enablePaging(true,7,10,"pagingArea",true,"infoArea");
	listaReporteSiniestroGrid.setPagingSkin("bricks");
	
	listaReporteSiniestroGrid.init();
	mostrarIndicadorCargaGenerico('indicadorListaSiniestros');
	
	listaReporteSiniestroGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorListaReporteSiniestroGrid();
			
			if(grid.getRowsNum() == 0){
				
				if(mostrarMensajeSinRegistros == undefined || mostrarMensajeSinRegistros == null ||
						mostrarMensajeSinRegistros == true){
					mostrarVentanaMensaje('20','No se encontraron registros.\n Introduzca criterios de b&uacute;squeda v&aacute;lidos.',null);
				}
			}
	    });
	
	//03/06/2011 Se remueve la consulta masiva de registros para minimizar el consumo de memoria en el servidor.
	listaReporteSiniestroGrid.load(listaReporteSiniestroPath, null, 'json');
}

function ocultarIndicadorListaReporteSiniestroGrid(){
	ocultarIndicadorCargaGenerico('indicadorListaSiniestros');
}

//var detallePolizaPorReporteSiniestroPath;
//var detallePolizaPorReporteSiniestroGrid;
//function mostrarDetallePolizaPorReporteSiniestroGrids(idToPoliza){
//	var polizaPorReporteSiniestroDiv = 'detallePolizaGrid';
//	$(polizaPorReporteSiniestroDiv).style.height='150px';
//	detallePolizaPorReporteSiniestroPath = '/MidasWeb/siniestro/cabina/mostrarListaReporteDetallePoliza.do?idToPoliza='+idToPoliza;
//	detallePolizaPorReporteSiniestroGrid = new dhtmlXGridObject(polizaPorReporteSiniestroDiv);							  
//	detallePolizaPorReporteSiniestroGrid.setHeader("Inciso,SubInciso,Secci&oacute;n,Cobertura,Riesgo,Suma Asegurada");
//	detallePolizaPorReporteSiniestroGrid.setColumnIds("inciso,subInciso,seccion,cobertura,riesgo,sumaAsegurada");
//	detallePolizaPorReporteSiniestroGrid.setInitWidths("80,80,200,200,200,150");
//	detallePolizaPorReporteSiniestroGrid.setColAlign("center,center,center,center,center,center");
//	detallePolizaPorReporteSiniestroGrid.setColSorting("str,str,str,str,str,str");
//	detallePolizaPorReporteSiniestroGrid.setColTypes("ro,ro,ro,ro,ro,ro");
//	detallePolizaPorReporteSiniestroGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
//	detallePolizaPorReporteSiniestroGrid.setSkin("light");
//	detallePolizaPorReporteSiniestroGrid.enableDragAndDrop(false);
//	detallePolizaPorReporteSiniestroGrid.enableLightMouseNavigation(false);
//	
//	detallePolizaPorReporteSiniestroGrid.enablePaging(true,5,3,"pagingAreaDetallePoliza",true,"infoAreaDetallePoliza");
//	detallePolizaPorReporteSiniestroGrid.setPagingSkin("bricks");
//	detallePolizaPorReporteSiniestroGrid.init();
//	mostrarIndicadorCargaGenerico('indicadorCargaDetallePoliza');
//	detallePolizaPorReporteSiniestroGrid.load(detallePolizaPorReporteSiniestroPath, ocultarIndicadorDetallePolizaPorReporteSiniestroGrids, 'json');
//}

function ocultarIndicadorDetallePolizaPorReporteSiniestroGrids(){
	ocultarIndicadorCargaGenerico('indicadorCargaDetallePoliza');
}

function mostrarDetalleParaReporteSiniestro(idToReporteSiniestro){
	 sendRequest(null,"/MidasWeb/siniestro/cabina/desplegarDatosReporteSiniestro.do?idToReporteSiniestro="+ idToReporteSiniestro
			 , "contenido","dhx_init_tabbars();validaInformacionADesplegar();");
}

function mostrarModificarParaReporteSiniestro(idToReporteSiniestro){
	sendRequest(null,"/MidasWeb/siniestro/cabina/reporteSiniestro.do?idToReporteSiniestro="	+ idToReporteSiniestro
				, "contenido","dhx_init_tabbars();iniciaModificarReporteSinietro();validaInformacionADesplegar();mostrarSeccionBusquedaPoliza();");
}

function mostrarCaratulaParaReporteSiniestro(idToReporteSiniestro){
	sendRequest(null,'/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do?idReporteSiniestro='
			+ idToReporteSiniestro ,'contenido',null);
}

function procedeAnalisisInternoInformePreliminar(){
	var validoOptionButton = document.getElementsByName('valido');
	if (!validoOptionButton[0].checked && !validoOptionButton[1].checked){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Debe seleccionar si es v\u00e1lido o no el informe", null);
		return;
	}
	
//	var listaCoberturas = document.getElementsByName('coberturasRiesgo');
//	var coberturaRiesgoCheck = null; 
//	for (var i=0;i<listaCoberturas.length;i+=1){
//		coberturaRiesgoCheck = listaCoberturas[i];
//		if (coberturaRiesgoCheck.checked){
//			esValidoParaAnalisis = true;
//		}
//	}
//	
//	if (!esValidoParaAnalisis){
//		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,"Debe seleccionar al menos un riesgo", null);
//		return;
//	}
		sendRequest(document.validarInformePreliminarForm, '/MidasWeb/siniestro/finanzas/procedeElAnalisisInterno.do', 
				'contenido', 'mensajeProcedeAnalisisInterno()');	
}

function muestraListaIncisosPolizaCrear(){
	var idPoliza = $('idToPoliza').value;
	var numEndoso = $('polizaNumeroEndoso').value;
	sendRequest(null, '/MidasWeb/siniestro/cabina/listarIncisosPoliza.do?numeroPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso,'listIncisos', null);
	
}

function muestraListaSeccionSubIncisos(varNumeroInciso){
	var idPoliza = $('idToPoliza').value;
	var numEndoso = $('polizaNumeroEndoso').value;
	$('idNumeroInciso').value = varNumeroInciso;
	$('idBotonBuscaCoberturas').style.display = "";
	$('listSeccionSubIncisos').innerHTML="";
	$('listCoberturasRiesgo').innerHTML="";
	sendRequest(null, '/MidasWeb/siniestro/cabina/listarSeccionSubIncisos.do?numeroPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso+'&numeroInciso='+varNumeroInciso,'listSeccionSubIncisos', null);
}

function muestraListaCoberturasRiesgoPoliza(varDatospolizaForm){
	var idPoliza = $('idToPoliza').value;
	var numEndoso = $('polizaNumeroEndoso').value;
	var varIdNumeroInciso = $('idNumeroInciso').value;
	sendRequest(varDatospolizaForm, '/MidasWeb/siniestro/cabina/listarCoberturasRiesgo.do?numeroPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso+'&numeroInciso='+varIdNumeroInciso,'listCoberturasRiesgo', null);
	
}
function setCodigoPostalSiniestro(idColonia,campo){
	document.getElementById(campo).value = idColonia.substring(0,5);
}

function muestraListarIncisosUbicacionPoliza(){
	var idPoliza = $('idToPoliza').value;
	var numEndoso = $('polizaNumeroEndoso').value;
	if (_isIE){
		var urlListaIncisos = '/MidasWeb/siniestro/cabina/listarIncisosUbicacionPoliza.do?numeroPoliza='+idPoliza+'&'+$('datospolizaForm').serialize();
	}else{
		var urlListaIncisos = '/MidasWeb/siniestro/cabina/listarIncisosUbicacionPoliza.do?numeroPoliza='+idPoliza+'&'+document.datospolizaForm.serialize();
	}
	$('idBotonBuscaCoberturas').style.display = "none";
	$('listSeccionSubIncisos').innerHTML="";
	$('listCoberturasRiesgo').innerHTML="";
	sendRequest(null,urlListaIncisos ,'listIncisos', null);
	
}
function activaTabReporteSiniestro(){
	crearReporteSiniestro.setTabActive('reporteSiniestro');
}

//##### Seccion de Coberturas Riesgo

function afectarCoberturaRiesgo(){
	var numEndoso = $('numeroEndoso').value;
	sendRequest(null,'/MidasWeb/siniestro/poliza/afectarCoberturaRiesgo.do?numeroPoliza='+$('numeroPoliza').value+'&idToPoliza='+$('idToPoliza').value+'&idToReporteSiniestro='+$('idToReporteSiniestro').value+'&analisisInterno='+$('analisisInterno').value+'&polizaNumeroEndoso='+numEndoso, 'contenido_coberturaRiesgo', 'muestraListarIncisosCoberturaRiesgo();');
}

function muestraListarIncisosCoberturaRiesgo(){
	var idPoliza = $('idToPolizaCR').value;
	var numEndoso = $('idPolizaNumeroEndosoCR').value;
	var idToReporteSiniestro = $('idToReporteSiniestroCR').value;
	sendRequest(null, '/MidasWeb/siniestro/cabina/listarIncisosCoberturaRiesgo.do?idToPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso+'&idToReporteSiniestro='+idToReporteSiniestro,'listIncisosRiesgosAfectados', 'validaIncisoSeleccionado()');
	
}

function validaIncisoSeleccionado(){
	var listaIncisosSel = document.getElementsByName('idInciso');
	var incisoSeleccionado = null; 
	var i=0;
	while (i<listaIncisosSel.length){
		incisoSeleccionado = listaIncisosSel[i];
		if (incisoSeleccionado.checked){
			i=listaIncisosSel.length;
			muestraListaSeccionSubIncisosCoberturasRiesgo(incisoSeleccionado.value);
		}
		i++;
	}
}

function validaSubIncisoSeccionSeleccionado(){
	var listaSubIncisoSeccionSel = document.getElementsByName('subIncisoSeccion');
	var varSubIncisoSeccion = null; 
	var i=0;
	while (i<listaSubIncisoSeccionSel.length){
		varSubIncisoSeccion = listaSubIncisoSeccionSel[i];
		if (varSubIncisoSeccion.checked){
			i=listaSubIncisoSeccionSel.length;
			muestraListaCoberturasRiesgoAAfectar(document.coberturaRiesgoForm);
		}
		i++;
	}
}

function muestraListaSeccionSubIncisosCoberturasRiesgo(varNumeroInciso){
	var idPoliza = $('idToPolizaCR').value;
	var numEndoso = $('idPolizaNumeroEndosoCR').value;
	var idToReporteSiniestro = $('idToReporteSiniestroCR').value;
	$('idNumeroIncisoCR').value = varNumeroInciso;
	$('idBotonBuscaRiesgosAfectados').style.display = "";
	$('listSeccionSubIncisosRiesgosAfectados').innerHTML="";
	$('listCoberturasRiesgoAfectar').innerHTML="";
	sendRequest(null, '/MidasWeb/siniestro/cabina/listarSeccionSubIncisosCoberturasRiesgo.do?idToPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso+'&numeroInciso='+varNumeroInciso+'&idToReporteSiniestro='+idToReporteSiniestro,'listSeccionSubIncisosRiesgosAfectados', 'validaSubIncisoSeccionSeleccionado();');
}

function muestraListaCoberturasRiesgoAAfectar(varCoberturaRiesgoForm){
	var idPoliza = $('idToPolizaCR').value;
	var numEndoso = $('idPolizaNumeroEndosoCR').value;
	var varIdNumeroInciso = $('idNumeroIncisoCR').value;
	var idToReporteSiniestro = $('idToReporteSiniestroCR').value;
	sendRequest(varCoberturaRiesgoForm, '/MidasWeb/siniestro/cabina/listarCoberturasRiesgoAAfectar.do?idToPoliza='+idPoliza+'&polizaNumeroEndoso='+numEndoso+'&numeroInciso='+varIdNumeroInciso+'&idToReporteSiniestro='+idToReporteSiniestro,'listCoberturasRiesgoAfectar', null);
	
}
function muestraListarCoberturasRiesgoPorUbicacion(){
	var idPoliza = $('idToPolizaCR').value;
	var numEndoso = $('idPolizaNumeroEndosoCR').value;
	if (_isIE){
		var urlListaIncisos = '/MidasWeb/siniestro/cabina/listarCoberturasRiesgoPorUbicacion.do?numeroPoliza='+idPoliza+'&'+$('coberturaRiesgoForm').serialize();
	}else{
		var urlListaIncisos = '/MidasWeb/siniestro/cabina/listarCoberturasRiesgoPorUbicacion.do?numeroPoliza='+idPoliza+'&'+document.coberturaRiesgoForm.serialize();
	}
	$('idBotonBuscaRiesgosAfectados').style.display = "none";
	$('listSeccionSubIncisosRiesgosAfectados').innerHTML="";
	$('listCoberturasRiesgoAfectar').innerHTML="";
	sendRequest(null,urlListaIncisos ,'listIncisosRiesgosAfectados', 'validaIncisoSeleccionado()');
	
}

function validaMovimientosExistentes(variableCoberturaRiesgoForm) {	
	new Ajax.Request("/MidasWeb/siniestro/cabina/validaMovimientosExistentes.do", {
		method : "post",
		asynchronous : false,
		parameters : "idToReporteSiniestro="+ $('idToReporteSiniestroCR').value,
		onSuccess : function(transport) {
			var idRespuesta = transport.responseXML.getElementsByTagName("id")[0].firstChild.nodeValue;
			if(idRespuesta == 1){
				mostrarVentanaMensaje('10',transport.responseXML.getElementsByTagName("valor")[0].firstChild.nodeValue, null);
			}else{
				guardarAfectacionCoberturaRiesgo(variableCoberturaRiesgoForm);
			}
		} // End of onSuccess
	});
}


function guardarAfectacionCoberturaRiesgo(varCoberturaRiesgoForm){
	if(validaCoberturasRiesgoSeleccionados()){
		sendRequestProcesaRespuestaXML(varCoberturaRiesgoForm ,'/MidasWeb/siniestro/poliza/guardarCoberturaRiesgo.do' ,'contenido','despuesGuardarAfectacionCoberturaRiesgo()' );
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe seleccionar almenos un riesgo',null);
	}
}

function validaCoberturasRiesgoSeleccionados(){
	var riesgos = document.getElementsByName("coberturasRiesgoAfectas");
	var cont = 0;
	for(var i = 0 ; i < riesgos.length; i++) {
		if(riesgos[i].checked){
			cont++;
		}
	}
	if(cont > 0){
		return true;
	}
	return false;
}

function despuesGuardarAfectacionCoberturaRiesgo(){
	if($('pantallaOrigen').value == 'informePreliminar'){
		sendRequest(null,'/MidasWeb/siniestro/finanzas/presentarInformePreliminar.do?idToReporteSiniestro='+$('idToReporteSiniestroCR').value,'contenido', null);
	}else{
		activaTabReporteSiniestro();
	}
}

function cancelarCoberturaRiesgo(){
	if($('pantallaOrigen').value == 'informePreliminar'){
		sendRequest(null,'/MidasWeb/siniestro/finanzas/presentarInformePreliminar.do?idToReporteSiniestro='+$('idToReporteSiniestroCR').value,'contenido', null);
	}else{
		crearReporteSiniestro.setTabActive('reporteSiniestro');
	}
}

function seleccionaCoberturasRiesgo(){
	var valorSeleccionados = $('idSeleccionados').value;
	if(valorSeleccionados != null && valorSeleccionados != ''){
		var idsCoberturas = valorSeleccionados.split(',');
		i=0;
		listaCoberturasRiesgo = document.getElementsByName('coberturasRiesgo');
		while (i  < idsCoberturas.length-1){
			var idValor = idsCoberturas[i];
			listaCoberturasRiesgo[idValor].checked=true;
			i++;
		}
	}
}

function desplegarAfectarCoberturaRiesgo(){
	sendRequest(null,'/MidasWeb/siniestro/poliza/desplegarAfectarCoberturaRiesgo.do?idToReporteSiniestro='+$('idToReporteSiniestro').value+'&numeroPoliza='+$('numeroPoliza').value, 'contenido_coberturaRiesgo', null);
}

function modificarCoberturasRiesgoReporte(){
	if($('idToPolizaInformePreliminar').value != ""){
		var idToReporteSiniestroInformePreliminar = $('idToReporteSiniestro').value;
		var urlRiesgos = '/MidasWeb/siniestro/cabina/reporteSiniestro.do?idToReporteSiniestro='+idToReporteSiniestroInformePreliminar
		sendRequest(null,urlRiesgos, 'contenido','dhx_init_tabbars();iniciaModificarReporteSinietro();validaInformacionADesplegar();mostrarSeccionBusquedaPoliza();activaTabCoberturaRiesgo();');
	}else{
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'Debe asociar una poliza al reporte',null);
	}
	
}

function activaTabCoberturaRiesgo(){
	crearReporteSiniestro.setTabActive('coberturaRiesgo');
	setTimeout('asignaValorPantallaCoberturas();',2000);
}

function asignaValorPantallaCoberturas(){
	var pantOrig = 'informePreliminar';
	$('pantallaOrigen').value =  pantOrig;
}
// ##### Fin de Seccion de Coberturas Riesgo


// &&&&&& Funcion para la pestana de Transporte
//function guardarTransporteSinistros(varFormTransporte){
//	sendRequest(varFormTransporte,'/MidasWeb/siniestro/cabina/reporteTransportista.do', 'contenido',null);
//}

function guardarTransporteSinistros(varFormTransporte){
	new Ajax.Request("/MidasWeb/siniestro/cabina/reporteTransportista.do", {
		method : "post",
		asynchronous : false,
		parameters : (varFormTransporte !== undefined && varFormTransporte !== null) ? $(varFormTransporte).serialize(true) : null, asynchronous : false,
		onSuccess : function(transport) {
		mostrarVentanaMensaje('30','Las datos de transporte se guardaron con \u00e9xito', null);
				activaTabReporteSiniestro();
		} // End of onSuccess
	});
}

//&&&&&& Fin de Funcion para la pestana de Transporte


// @@@@@@@ Funcion para la pestana de Preguntas Especiales
function datosPreguntasEspeciales(preguntaespecialForm){
	sendRequest(preguntaespecialForm,'/MidasWeb/siniestro/cabina/reportePreguntasEspeciales.do?idToReporteSiniestro='+$('idToReporteSiniestro').value+'&noPoliza='+$('numeroPoliza').value, 'contenido_preguntasEspeciales', 'creaCalendars("fechaNotificacion,plazoReclamacion","S,S");');
}

function guardarPreguntasEspecialesSiniestro(varPreguntaespecialForm){
	sendRequestProcesaRespuestaXML(varPreguntaespecialForm,'/MidasWeb/siniestro/cabina/reporteGuardarEspeciales.do', 'contenido','activaTabReporteSiniestro();');
}

//@@@@@@@ Fin de Funcion para la pestana de Preguntas Especiales


//==============================Estado de siniestro============================================
var ESTATUS_PENDIENTE_RS = 1;
var ESTATUS_LEGAL_RS = 2;
var ESTATUS_CERRADO_RS = 3;
var SUBESTATUS_CERRADO_RS = 6;

function seleccionaEstatusEstadoSiniestro(){
	var comboEstatusSiniestro = $("idTcEstatusSiniestro");
	var textoFechaCerradoSiniestro = $("fechaCerrado");

	if (comboEstatusSiniestro.options[comboEstatusSiniestro.selectedIndex].value == ESTATUS_CERRADO_RS){
		if (!(textoFechaCerradoSiniestro && trim(textoFechaCerradoSiniestro.value).length >0)){
			comboEstatusSiniestro.selectedIndex = comboEstatusSiniestro.oldvalue;
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar la fecha de cierre", null);
		}
	}
	habilitaSubEstatusEstadoSiniestro();
}

function habilitaSubEstatusEstadoSiniestro(){
	var comboEstatusSiniestro = $("idTcEstatusSiniestro");
	var comboSubEstatusSiniestro = $("idTcSubEstatusSiniestro");
	var selectedItemComboEstatusSiniestro = comboEstatusSiniestro.options[comboEstatusSiniestro.selectedIndex];
	
	if (!(selectedItemComboEstatusSiniestro.value == ESTATUS_PENDIENTE_RS || selectedItemComboEstatusSiniestro.value == ESTATUS_LEGAL_RS)){
		comboSubEstatusSiniestro.selectedIndex = SUBESTATUS_CERRADO_RS;
	}
}

function seleccionaSubEstatusEstadoSiniestro(){
	var comboEstatusSiniestro = $("idTcEstatusSiniestro");
	var comboSubEstatusSiniestro = $("idTcSubEstatusSiniestro");
	var selectedItemComboEstatusSiniestro = comboEstatusSiniestro.options[comboEstatusSiniestro.selectedIndex];
	if (selectedItemComboEstatusSiniestro.value == ESTATUS_CERRADO_RS){
		comboSubEstatusSiniestro.selectedIndex = SUBESTATUS_CERRADO_RS;
	}
}

function seleccionaTerminoAjusteEstadoSiniestro(){
	var textoFechaTerminacionReporte = $('fechaTerminacionSiniestro'); 
	if (!(textoFechaTerminacionReporte && trim(textoFechaTerminacionReporte.value).length>0)){
		var comboTerminoAjuste = $("idTcTerminoAjuste");
		comboTerminoAjuste.selectedIndex = comboTerminoAjuste.oldvalue;
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar la fecha de terminaci\u00f3n del siniestro", null);
	}
}

function validarReporteEstadoSiniestro(){
	var esValidoParaCerrar = true;
	var comboEstatusSiniestro = $("idTcEstatusSiniestro");
	var comboTerminoAjuste = $("idTcTerminoAjuste");
	var textoFechaTerminacionReporte = $('fechaTerminacionSiniestro'); 
	var textoFechaCerradoSiniestro = $("fechaCerrado");
	
	
	if (comboEstatusSiniestro.options[comboEstatusSiniestro.selectedIndex].value == ESTATUS_CERRADO_RS){
		if (!(textoFechaTerminacionReporte && trim(textoFechaTerminacionReporte.value).length>0)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar la fecha de terminaci\u00f3n del siniestro", null);
			esValidoParaCerrar = false;
		}
		if (!(textoFechaCerradoSiniestro && trim(textoFechaCerradoSiniestro.value).length >0)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe especificar la fecha de cierre", null);
			esValidoParaCerrar = false;
		}
		if (!fechaMenorIgualQue(textoFechaTerminacionReporte.value, textoFechaCerradoSiniestro.value)){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"La fecha de terminaci\u00f3n debe ser menor o igual que la fecha de cierre", null);
			esValidoParaCerrar = false;
		}
		if (comboTerminoAjuste.options[comboTerminoAjuste.selectedIndex].value == 0){
			mostrarVentanaMensaje(MOSTRAR_MENSAJE_ALERT_SINIESTRO,"Debe seleccionar un termino de ajuste", null);
			esValidoParaCerrar = false;
		}
		
		if (esValidoParaCerrar){		
			ventanaConfirmacionSiniestros("\u00BFDesea cerrar el reporte?", "sendRequestProcesaRespuestaXML(document.estadosiniestroForm, " +
					"'/MidasWeb/siniestro/cabina/cerrarReporteSiniestro.do',"
					+"null, 'listarReportesSiniestro()')");
		}
	} else {
		guardarReporteEstadoSiniestro();
	}
}

function guardarReporteEstadoSiniestro(){
	sendRequestProcesaRespuestaXML(document.estadosiniestroForm,'/MidasWeb/siniestro/cabina/reporteGuardarSiniestro.do', 'contenido',null);
}

function validaFormatoPolizaCompletoSiniestro(varNoPoliza){
	if(varNoPoliza == ""){
		return true;
	}
	if(varNoPoliza.length < 14 ){
		mostrarVentanaMensaje(MOSTRAR_MENSAJE_ERROR_SINIESTRO,'El numero de la poliza no es correcto',null);
		return false;
	}
	return true;
}

function asignaFechaAsignoPolizaReporte(){
	if ($('idToPoliza').value == ''){
		$('fechaAsignoPoliza').value = $('fechaSiniestro').value;
	}
}
