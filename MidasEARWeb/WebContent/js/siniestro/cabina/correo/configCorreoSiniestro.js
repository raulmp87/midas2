
//############## CAT�LOGO DE CONFIGURACIOES DE CORREO PARA EL REGISTRO DE SINIESTROS POR TIPO DE NEGOCIO, POLIZA O AJUSTADOR ###########################

var configCorreosTipoNegocioGrid;
var configCorreosPolizaGrid;
var configCorreosAjustadorGrid;

	/*
	 * Grid de configuracion de correos por tipo de negocio
	 */
	function mostrarCorreosTipoNegocio(){
		var div = $('configCorreosTipoNegocioGrid');
		
		configCorreosTipoNegocioGrid = new dhtmlXGridObject("configCorreosTipoNegocioGrid");
		configCorreosTipoNegocioGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		configCorreosTipoNegocioGrid.setEditable(true);
		configCorreosTipoNegocioGrid.setSkin("light");
	
		configCorreosTipoNegocioGrid.setHeader("Tipo de Negocio,Correo,Usuario,Fecha,");
		configCorreosTipoNegocioGrid.setInitWidths("220,200,80,80,40");
		configCorreosTipoNegocioGrid.setColTypes("ro,edn,ro,ro,img");
		configCorreosTipoNegocioGrid.setColAlign("left,left,left,center,center");
		configCorreosTipoNegocioGrid.setColSorting("na,na,na,na,na");
		configCorreosTipoNegocioGrid.enableResizing("true,true,true,false,false");
		configCorreosTipoNegocioGrid.setColumnIds("tipoNegocio,correo,usuario,fecha,img1");		
		
		configCorreosTipoNegocioGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){	
			
			var idTipoNegocio = configCorreosTipoNegocioGrid.cellById(rId, 0).getValue();
			var correo = configCorreosTipoNegocioGrid.cellById(rId, 1).getValue();

			if( stage == 2 && cInd == 1 && nValue != oValue ) { // Si la columna es la del correo
				
				if( validationEmailGrid(correo )){
					sendRequest(null,'/MidasWeb/siniestro/cabina/correo/actualizar.do?idConfigTipoNegocio='+ rId +'&correo=' + correo,null,"mostrarCorreosTipoNegocioFiltrado( $('s_tiposNegocio') );");
				}else{
					mostrarVentanaMensaje("20","El correo electr\u00F3nico no es una direcci\u00F3n v\u00e1lida",null);
					configCorreosTipoNegocioGrid.cellById(rId, 1).setValue(oValue);
				}
			}
			
			return true;
		});		
			
		configCorreosTipoNegocioGrid.init();	
		
		var configCorreosTipoNegocioPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoTipoNegocio.do';	
		configCorreosTipoNegocioGrid.load(configCorreosTipoNegocioPath,null, 'json');	
	}
	
	/*
	 * Grid de configuracion de correos filtrado por tipo de negocio
	 */
	function mostrarCorreosTipoNegocioFiltrado( idTcTipoNegocio ){
		var div = $('configCorreosTipoNegocioGrid');
		var configCorreosTipoNegocioPath;
		if(div == null) {
			configCorreosTipoNegocioGrid.init();	
		}
		if( idTcTipoNegocio.getValue() != 0 ){
			configCorreosTipoNegocioPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoTipoNegocioFiltrado.do?idTcTipoNegocio='+idTcTipoNegocio.getValue();	
		}else{
			configCorreosTipoNegocioPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoTipoNegocio.do';
		}
		
		configCorreosTipoNegocioGrid.clearAll();
		configCorreosTipoNegocioGrid.load(configCorreosTipoNegocioPath,null, 'json');	
	}

	function guardarConfigTipoNegocio(){
		var validacionCombo = validationText( $('s_tiposNegocio') );
		var validacionTextCorreo = validationText( $('txt_correo') );
		if( validacionCombo && validacionTextCorreo ){
			sendRequest($('f_configTipoNegocio'),'/MidasWeb/siniestro/cabina/correo/guardar.do',null,"mostrarCorreosTipoNegocioFiltrado( $('s_tiposNegocio') );");
		}else{
			var msj = "";
			if( !validacionCombo && !validacionTextCorreo){
				msj += "Favor de llenar los campos requeridos";
			}else{
				msj += "Favor de llenar correctamente el campo requerido";
			}
			mostrarVentanaMensaje("20",msj,null);
			return;
		}
	}
	function eliminarConfigTipoNegocio( idTcTipoNegocio, idConfigTipoNegocio ){
		if( confirm("\u00BF Realmente desea eliminar el registro ?") ){	
			sendRequest(null,'/MidasWeb/siniestro/cabina/correo/eliminar.do?idConfigTipoNegocio=' + idConfigTipoNegocio,null,"mostrarCorreosTipoNegocioFiltrado( $('s_tiposNegocio') );");
		}
	}
	
	/*
	 * Grid de configuracion de correos por Poliza
	 */
	function mostrarCorreosPoliza(){
		var div = $('configCorreosPolizaGrid');
		
		configCorreosPolizaGrid = new dhtmlXGridObject("configCorreosPolizaGrid");
		configCorreosPolizaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		configCorreosPolizaGrid.setEditable(true);
		configCorreosPolizaGrid.setSkin("light");
	
		configCorreosPolizaGrid.setHeader("Numero de Poliza,Correo,Usuario,Fecha,");
		configCorreosPolizaGrid.setInitWidths("220,200,80,80,40");
		configCorreosPolizaGrid.setColTypes("ro,edn,ro,ro,img");
		configCorreosPolizaGrid.setColAlign("left,left,left,center,center");
		configCorreosPolizaGrid.setColSorting("na,na,na,na,na");
		configCorreosPolizaGrid.enableResizing("true,true,true,false,false");
		configCorreosPolizaGrid.setColumnIds("numeroPoliza,correo,usuario,fecha,img1");		
		
		configCorreosPolizaGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){	
			
			var correo = configCorreosPolizaGrid.cellById(rId, 1).getValue();

			if( stage == 2 && cInd == 1 && nValue != oValue ) { // Si la columna es la del correo
				if( validationEmailGrid(correo )){
					sendRequest(null,'/MidasWeb/siniestro/cabina/correo/poliza/actualizar.do?idConfigPoliza='+ rId +'&correo=' + correo,null,"mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );");
				}else{
					mostrarVentanaMensaje("20","El correo electr\u00F3nico no es una direcci\u00F3n v\u00e1lida",null);
					configCorreosPolizaGrid.cellById(rId, 1).setValue(oValue);
				}
			}
			
			return true;
		});		
			
		configCorreosPolizaGrid.init();	
		
		var configCorreosPolizaPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoPoliza.do';	
		configCorreosPolizaGrid.load(configCorreosPolizaPath,null, 'json');	
	}
		
	/*
	 * Grid de configuracion de correos filtrado por Poliza
	 */
	function mostrarCorreosPolizaFiltrado( numeroPoliza ){
		var div = $('configCorreosPolizaGrid');
		var configCorreosPolizaPath;
		if(div == null) {
			configCorreosPolizaGrid.init();	
		}
		if( numeroPoliza.getValue() != "" ){   
			configCorreosPolizaPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoPolizasFiltrado.do?numeroPoliza='+numeroPoliza.getValue();	
		}else{
			configCorreosPolizaPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoPoliza.do';
		}
		
		configCorreosPolizaGrid.clearAll();
		configCorreosPolizaGrid.load(configCorreosPolizaPath,null, 'json');	
	}

	function guardarConfigPoliza(){
		
		var fobj = $("f_configPoliza");
		var actionURL = "/MidasWeb/siniestro/cabina/correo/poliza/buscarGuardar.do";
		var actionURLsave = "/MidasWeb/siniestro/cabina/correo/poliza/guardar.do";
		var validacionNumeroPoliza = validationText( $('numeroPolizaBuscar') );
		var validacionTextCorreo = validationText( $('txt_correo') );
		var validaFormatoNumeroPoliza = validaFormatoPoliza($('numeroPolizaBuscar').value);
		var validaFormatoCorreo = validationEmail($('txt_correo'));
		var msj = "";
		
			
		if( !validacionNumeroPoliza && !validacionTextCorreo){

			mostrarVentanaMensaje("20","Favor de llenar los campos requeridos",null);
			return;
		}


		if( validaFormatoNumeroPoliza && validaFormatoCorreo ){
			
			new Ajax.Request(actionURL, {
				method : "post",
				encoding : "UTF-8",
				parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
				onCreate : function(transport) {
					totalRequests = totalRequests + 1;
					showIndicator();
				}, // End of onCreate
				onComplete : function(transport) {
					totalRequests = totalRequests - 1;
					hideIndicator();
				}, // End of onComplete
				onSuccess : function(transport) {
	
					if( transport.responseText == "true" ){
						$('txt_correo').value="";
						mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );
					}else{
						if( confirm("El n\u00FAmero de la p\u00F3liza no existe \u00BF Realmente desea guardarlo ?") ){
							sendRequest(fobj, actionURLsave, null, "mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );");

//							var redirect = "mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );";
//							var url = "sendRequest($('f_configPoliza'),'" + actionURLsave + "'," + null + ",'" + redirect +"');";
//							ventanaConfirmacionSiniestros('El n\u00FAmero de la p\u00F3liza no existe \u00BF Realmente desea guardarlo ?',url);
						}
					}
					
				} // End of onSuccess
				
			});
		}else{
			if( !validaFormatoNumeroPoliza ){
				msj += "El n\u00FAmero de la p\u00F3liza no es correcto ";
			}
			if( !validaFormatoCorreo ){
				if(msj!=""){
					msj += "<br/>";
				}
				msj += "El correo electr\u00F3nico no es una direcci\u00F3n v\u00e1lida";
			}
			mostrarVentanaMensaje("10",msj,null);
		}
	}
	
	function eliminarConfigPoliza( idConfigPoliza ){
		if( confirm("\u00BF Realmente desea eliminar el registro ?") ){	
			sendRequest(null,'/MidasWeb/siniestro/cabina/correo/poliza/eliminar.do?idConfigPoliza=' + idConfigPoliza,null,"mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );");
		}
//		var redirect = "mostrarCorreosPolizaFiltrado( $('numeroPolizaBuscar') );"	
//		var url      = "sendRequest(null,'/MidasWeb/siniestro/cabina/correo/poliza/eliminar.do?idConfigPoliza=" + idConfigPoliza + "',null," + redirect +");";
//			
//		ventanaConfirmacionSiniestros('\u00BF� Realmente desea eliminar el correo para la p�liza ?',url);
	}
	
	function validaFormatoPoliza( varNoPoliza ){

		if(varNoPoliza.length < 14 ){
			return false;
		}else{
			return true;
		}
	}
	
	/*
	 * Grid de configuracion de correos por ajustador
	 */
	function mostrarCorreosAjustador(){
		var div = $('configCorreosAjustadorGrid');
		
		configCorreosAjustadorGrid = new dhtmlXGridObject("configCorreosAjustadorGrid");
		configCorreosAjustadorGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		configCorreosAjustadorGrid.setEditable(true);
		configCorreosAjustadorGrid.setSkin("light");
	
		configCorreosAjustadorGrid.setHeader("Ajustador,Correo,Usuario,Fecha");
		configCorreosAjustadorGrid.setInitWidths("240,220,80,80");
		configCorreosAjustadorGrid.setColTypes("ro,edn,ro,ro");
		configCorreosAjustadorGrid.setColAlign("left,left,left,center");
		configCorreosAjustadorGrid.setColSorting("na,na,na,na");
		configCorreosAjustadorGrid.enableResizing("true,true,true,false");
		configCorreosAjustadorGrid.setColumnIds("ajustador,correo,usuario,fecha");
		
		configCorreosAjustadorGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){	
			
			var correo = configCorreosAjustadorGrid.cellById(rId, 1).getValue();

			if( stage == 2 && cInd == 1 && nValue != oValue ) { // Si la columna es la del correo
				if( validationEmailGrid(correo )){
					sendRequest(null,'/MidasWeb/siniestro/cabina/correo/ajustador/actualizar.do?idAjustador='+ rId +'&correo=' + correo,null,'mostrarCorreosAjustador();	');
				}else{
					mostrarVentanaMensaje("20","El correo electr\u00F3nico no es una direcci\u00F3n v\u00e1lida",null);
					configCorreosAjustadorGrid.cellById(rId, 1).setValue(oValue);
				}
			}
			
			return true;
		});		
			
		configCorreosAjustadorGrid.init();	
		
		var configCorreosAjustadorPath = '/MidasWeb/siniestro/cabina/correo/listarConfigCorreoAjustador.do';	
		configCorreosAjustadorGrid.load(configCorreosAjustadorPath,null, 'json');	
	}
	
//*****************  para guardar, actualizar  y eliminar	
//	if(value == "1") {
//		mostrarVentanaMensaje('30', "La colonia se actualiz&oacute; correctamente.", "parent.ventanaColonia.close()");
//	} else {
//		mostrarVentanaMensaje('10', text);
//	}

	/**
	 * 
	 * Si el valor del elemento es vacio
	 * se cambia la clase de la etiqueta de normal a error
	 *  
	 * @param element Elemento a evaluar
	 * @return
	 */
	function validationText( element ){ 

		var validation = true;
		if( element.value == "" || element.value == "0" ){
			cambiarError( element );
			
			validation = false;
			return validation;
		}
		
		if( element.id == "s_tiposNegocio" || element.id == "numeroPolizaBuscar" ){
			cambiarNormal( element );
		}else{
			validation = validationEmail( element );
			
		}
		return validation;
	}
	
	function validationEmail( correo ){

		var validation = true;
		expr=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	    if(!expr.exec( correo.value )){
	    	cambiarError( correo );
	    	validation = false;
	    }else{
	    	cambiarNormal( correo );
	    }
		return validation;
	}
	
	function validationEmailGrid( correo ){

		var validation = true;
		expr=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	    if(!expr.exec( correo )){
	    	validation = false;
	    }
		return validation;
	}

	function cambiarNormal( element ){

		if( navigator.appName == "Microsoft Internet Explorer"  ){
			
			element.parentNode.parentNode.firstChild.firstChild.firstChild.firstChild.firstChild.style.display="none";
			element.parentNode.parentNode.firstChild.firstChild.firstChild.setAttribute( "className", 'normal' );
			
		}else{
			
			element.parentNode.parentNode.firstDescendant().firstDescendant().firstDescendant().firstDescendant().firstDescendant().style.display="none";
			element.parentNode.parentNode.firstDescendant().firstDescendant().firstDescendant().removeClassName("error");
			element.parentNode.parentNode.firstDescendant().firstDescendant().firstDescendant().addClassName("normal");
		}
	}
	
	function cambiarError( element ){
	
		if( navigator.appName == "Microsoft Internet Explorer"  ){
			element.parentNode.parentNode.firstChild.firstChild.firstChild.setAttribute( "className", 'error' );
			
		}else{
			element.parentNode.parentNode.firstDescendant().firstDescendant().firstDescendant().removeClassName("normal");
			element.parentNode.parentNode.firstDescendant().firstDescendant().firstDescendant().addClassName("error");
		}
	}
