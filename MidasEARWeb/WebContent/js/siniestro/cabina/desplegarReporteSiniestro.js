function validaInformacionADesplegar(){
	var numeroPoliza = $('idToPoliza').value;
	if(numeroPoliza != null && numeroPoliza != ''){
		crearReporteSiniestro.enableTab('datosPoliza',true);
		if($('idTipoNegocio').value == 95){
			crearReporteSiniestro.enableTab('preguntasEspeciales',true);
		}else{
			crearReporteSiniestro.disableTab('preguntasEspeciales',true);
		}
		if($('productoTransporte').value == "true" ){
			crearReporteSiniestro.enableTab('transportes', true);
		}else{
			crearReporteSiniestro.disableTab('transportes',true);
		}
		crearReporteSiniestro.enableTab('coberturaRiesgo',true);		
	}else{
		crearReporteSiniestro.disableTab('datosPoliza',true);
		crearReporteSiniestro.disableTab('preguntasEspeciales',true);
		crearReporteSiniestro.disableTab('transportes',true);		
		crearReporteSiniestro.disableTab('coberturaRiesgo',true);		
	}
}

