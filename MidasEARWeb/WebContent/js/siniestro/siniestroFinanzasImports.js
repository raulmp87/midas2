/* IMPORTS de los distintos JS de reaseguro  */
var pathSiniestro = '../js/siniestro/';
function loadScriptSiniestro(url){
	 var script = document.createElement("script")
	 script.type = "text/javascript";
	 script.src = pathSiniestro + url;
	 document.getElementsByTagName("head")[0].appendChild(script);
}
/*Imports*/
loadScriptSiniestro('siniestro.js');
loadScriptSiniestro('finanzas/reserva.js');
loadScriptSiniestro('documentos/documentos.js');
loadScriptSiniestro('finanzas/gastos.js');
loadScriptSiniestro('finanzas/ingresos.js');
loadScriptSiniestro('finanzas/terminarSiniestro.js');
loadScriptSiniestro('reportes/reportesSiniestro.js');
loadScriptSiniestro('finanzas/indemnizacion.js');
loadScriptSiniestro('salvamento/salvamentoSiniestro.js');
loadScriptSiniestro('finanzas/convenioFiniquito.js');
loadScriptSiniestro('finanzas/ordenDePago.js');
loadScriptSiniestro('finanzas/autorizacionTecnica.js');
loadScriptSiniestro('eventocatastrofico.js');
