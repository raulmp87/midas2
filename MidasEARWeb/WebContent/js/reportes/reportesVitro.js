var idGrupo = '';
var idNegocio = 0;

jQuery(document).ready(function(){
	
	jQuery('#idNegocio').change(function(){
		idNegocio = jQuery(this).val();
		if(idNegocio !== null && typeof idNegocio !== 'undefined' && idNegocio > 0){
			jQuery.ajax({
				url : buscarClientes,
				type : 'POST',
				data : {'idNegocio' : idNegocio},
				datatype : 'json',
				success : function(json){
					jQuery('#dClietne').html(null);
					var cadenaDivCliente =crearCadenaDiv('cliente', 'generarDivGrupo();');
					
					for(var i = 0; i < json.clientes.length; i++){
						cadenaDivCliente += '<option value="' + json.clientes[i].dpnIdcliente + '">' +  json.clientes[i].dpnRazonsocial + '</option>';
					}
					cadenaDivCliente += "</select></div> </div>";
					jQuery('#dClietne').html(cadenaDivCliente);
				}
			});
		} else {
			limpiarDivs('idNegocio');
		}
	});
});

function generarDivGrupo(){
	var id = jQuery('#cliente').val();
	if(id !== null && typeof id !== 'undefined' && id > 0){
		jQuery.ajax({
			url : buscarGrupos,
			type : 'POST',
			datatype : 'json',
			success : function(json){
				jQuery('#dGrupo').html(null);
				var cadenaDivGrupo =crearCadenaDiv('idGrupo', 'generarDivFechas();');
				
				for(var i = 0; i < json.gruposVitro.length; i++){
					cadenaDivGrupo += '<option value="' + json.gruposVitro[i].dpnIdgrupo + '">' +  json.gruposVitro[i].dpnNombregrupo + '</option>';
				}
				
				cadenaDivGrupo += "</select></div> </div>";
				jQuery('#dGrupo').html(cadenaDivGrupo);
			}
		});
	} else {
		limpiarDivs('dClietne');
	}
}

function generarDivFechas(){
	idGrupo = jQuery('#idGrupo').val();
	if(idGrupo !== null && typeof idGrupo !== 'undefined' && idGrupo > 0){
		jQuery.ajax({
			url : fechasReportes,
			type : 'POST',
			data : {'idGrupo' : idGrupo},
			datatype : 'json',
			success : function(json){
				jQuery('#dFechaCorte').html(null);
				var cadenaSelectFecha =crearCadenaDiv('fechaPago','');
				
				for(var i = 0; i < json.cobros.length; i++){
					
					var strDat = json.cobros[i].split('T');
					var strFec = strDat[0].split('-');
					
					var fecha = new Date(strFec[0], (strFec[1] - 1), strFec[2]);
					
					cadenaSelectFecha += '<option value="' + formatDate(fecha) + '">' +  formatDate(fecha) + '</option>';
				}
				
				cadenaSelectFecha += "</select></div> </div>";
				jQuery('#dFechaCorte').html(cadenaSelectFecha);
			}
		});
	} else {
		limpiarDivs('dGrupo');
	}
}

function generarReporteVitro(){
	var fechaCorte = jQuery('#fechaPago').val();
	var idNegocio = jQuery('#idNegocio').val()
	var textoSelect = jQuery("#idGrupo option:selected").text();
	var idFormaPago = textoSelect.split('-');
	var idPago = idFormaPago[0].trim();
	if(typeof textoSelect !== 'undefined' && typeof fechaCorte !== 'undefined'){
		window.open(generarReporte + "?nombreGrupo=" + idFormaPago[1] +"&fechaCorte=" + fechaCorte+"&idFormaPago="+idPago+"&idNegocio="+idNegocio+"&fecha="+fechaCorte,"reporteVitro");
	} else {
		document.getElementById('mensajes').style.display = 'block';
		document.getElementById('textMessage').className = 'errorblock';
		jQuery("#textMessage").html('<p><b>No se puede generar el reporte si no ha seleccionado una fecha de corte</b></p>');
		setTimeout(closeMessageDiv, 4000);
	}
}

function formatDate(date) {
	
	var dia = date.getDate();
	var mes = date.getMonth()+1;
	var anio = date.getFullYear();
  return pad(dia, 2) + "/" + pad(mes, 2) + "/"  + date.getFullYear();
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function closeMessageDiv(){
	document.getElementById('mensajes').style.display = 'none';
	document.getElementById('textMessage').className = '';
	jQuery("#textMessage").html('');
}


function crearCadenaDiv(nombreDiv, functionChange){
	var cadena = "<div id='wwgrp_"+nombreDiv+"' class='wwgrp'> <div id='wwctrl_"+nombreDiv+"' class='wwctrl'>" +
					" <select id='"+nombreDiv+"' onchange='"+functionChange+"' class='cajaTexto'> <option selected='true' value='-1' >Seleccione ...</option>";
	return cadena;
}

function limpiarDivs(nombreDivCambio){
	switch(nombreDivCambio){
		case 'idNegocio' :
			jQuery('#dClietne').html(null);
			jQuery('#dGrupo').html(null);
			jQuery('#dFechaCorte').html(null);
			break;
		case 'dClietne' :
			jQuery('#dGrupo').html(null);
			jQuery('#dFechaCorte').html(null);
			break;
		case 'dGrupo' : 
			jQuery('#dFechaCorte').html(null);
			break;
	}
}