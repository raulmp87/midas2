function abrirModalCargaArchivo(){
	if(dhxWins != null){
		dhxWins.unload();
	}
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla para Reporte");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
	
	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			dwr.util.setValue("idToControlArchivo", idToControlArchivo);
        			window.open(uploadFileExcel +"?"+jQuery(document.generarReporte).serialize(), "download");
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("divContentForm").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

function generarReporteSerieInd(){
	var numeroSerie = jQuery('#numeroSerie').val();
	var numeroMidas = jQuery('#numeroPolizaMidas').val();
	var numeroSeycos = jQuery('#numeroPolizaSeycos').val();
	if((numeroSerie !== typeof 'undefined' && numeroSerie.trim() !== '') || 
			(numeroPolizaMidas !== typeof 'undefined' && numeroPolizaMidas.trim !== '') ||
			(numeroPolizaSeycos !== typeof 'undefined' && numeroPolizaSeycos.trim !== '')){
		window.open(generateReport + '?numeroSerie=' + numeroSerie + '&numeroPolizaMidas=' + numeroMidas + '&numeroPolizaSeycos=' + numeroSeycos,"reporteInd");
		cleanFilters();
	}
}

function cleanFilters(){
	jQuery('#numeroSerie').val('');
	jQuery('#numeroPolizaMidas').val('');
	jQuery('#numeroPolizaSeycos').val('');
}