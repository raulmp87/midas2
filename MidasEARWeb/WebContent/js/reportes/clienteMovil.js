/**
 * 
 */
var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";

function generarReporte(){
	var idTipoPol = dwr.util.getValue('idTipoPol');
	if(idTipoPol == undefined || idTipoPol == null || idTipoPol == '' ){
		alert('Se tiene que seleccionar un Tipo de Poliza.');
	} else {
		window.open(generarExcelURL + "?tipoPoliza="+idTipoPol, 'download');
	}
}



function sendRequest(param, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {
	$.ajax({
		    type: "POST",
		    url: actionURL,
		    data: (param !== undefined && param !== null) ? param : null,
		    dataType: dataTypeParam,
		    async: true,
		    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
		success : function(data) {		
	    	$('#searchForm').submit();
		} // End of onSuccess
		
	});
}
