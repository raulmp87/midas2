var formulario;

function generaReporteMonitoreoSol(myForm){
	formulario=myForm;
	//evaluaCrearReporte();
	sendRequest(formulario,'/MidasWeb/danios/reportes/validarDatosReporteMonitoreoSolicitudes.do', 'contenido','existenErrores("imprimirReporteMonitoreoSolicitudes()")');
	
}

function imprimirReporteMonitoreoSolicitudes(){
	formulario = (formulario)?formulario:$('reporteMonitoreoSolicitudesForm')
	var location = "/MidasWeb/danios/reportes/generarReporteMonitoreoSolicitudes.do?"+formulario.serialize();
	//window.open(location,"ReporteMonitoreoSolicitudes");
	
	var newwindow=window.open(location,'ReporteMonitoreoSolicitudes','height=240,width=540');
	if (window.focus) {newwindow.focus()}
	rellenaValoresFinalBusqueda(document.reporteMonitoreoSolicitudesForm.ejecutivos, "agentesFiltrado");
	
}


function cargaAgentesPorEjecutivo(obj, child){
	$('claveAgente').value="";
	document.reporteMonitoreoSolicitudesForm.agentesFiltrado.value=0;
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/danios/reportes/cargarAgentesPorEjecutivo.do", {
			method : "post",
			asynchronous : false,
			parameters : "idOficina=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
	
	
}
/*
function evaluaCrearReporte(){
	//alert("e");
	formulario = (formulario)?formulario:$('reporteMonitoreoSolicitudesForm')
	new Ajax.Request("/MidasWeb/danios/reportes/obtieneTotalFiltrado.do?"+formulario.serialize(), {
		method : "post",
		asynchronous : false,
		onSuccess : function(resp) { 
		//verificaResultado(resp.responseXML);
			}, 
		onFailure : function(resp) { 
			permiteCancelacion=false;
			return;
			},
		onComplete:function(resp){
				verificaResultado(resp.responseXML);
			}
	});
	
}

function verificaResultado(docXML){
	var items = docXML.getElementsByTagName("item");
	
	if(items!=null  && items.length>0){
		var item = items[0];
		if(item.getElementsByTagName("cuantos")[0].firstChild.nodeValue==0){
			mostrarVentanaMensaje("20", "No se encontraron registros con los parametros proporcionados.", null);
		}else if (item.getElementsByTagName("cuantos")[0].firstChild.nodeValue>0){
			imprimirReporteMonitoreoSolicitudes();
		}
		
			
	}
	
}*/

function rellenaValoresFinalBusqueda(obj, child){
	
	if (obj.selectedIndex > 0) {
			new Ajax.Request("/MidasWeb/danios/reportes/cargarAgentesPorEjecutivo.do", {
			method : "post",
			asynchronous : false,
			parameters : "idOficina=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
	
	if($('claveAgente').value.length>0){
		document.reporteMonitoreoSolicitudesForm.agentesFiltrado.value=$('claveAgente').value;
	}
	//presentaVentanaSinDatos();
}