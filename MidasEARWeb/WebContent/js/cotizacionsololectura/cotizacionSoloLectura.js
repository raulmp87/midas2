function creaArbolCotizacionSoloLectura(parametros){
	cotizacionSoloLectura = true;
	creaArbolCotizacionGeneral(parametros, null);
}


function manejadorArbolCotizacionSoloLectura(ids,level,onClickRequestParameters){
	switch(level){
		case 1:
			var url = '/MidasWeb/cotizacionsololectura/mostrarDetalle.do?id='+ids;
			url += '&' + onClickRequestParameters; 
			sendRequest(null,url,'configuracion_detalle','dhx_init_tabbars();');
			break;	
		case 2:
			sendRequest(null, '/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do?idToCotizacion='+ids[0],'configuracion_detalle',	'mostrarSeccionesPorIncisoCOTRO('+ids[1]+','+ids[0]+');');
			break;
		case 3:
			sendRequest(null,'/MidasWeb/cotizacionsololectura/listarCoberturas.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2] ,'configuracion_detalle', 'mostrarCoberturasPorSeccionCOTRO('+ids[0]+','+ids[1]+','+ids[2]+')');
			break;
		case 4:
			sendRequest(null,'/MidasWeb/cotizacionsololectura/mostrarRiesgosPorCobertura.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2]+'&idToCobertura='+ids[3] ,'configuracion_detalle', 'initCotizacionSoloLecturaRiesgoGrid('+ids[0]+','+ids[1]+','+ids[2]+','+ids[3]+')');
			break;
	}
}


//MOSTRAR DATOS GENERALES

function cargaTabDatosGeneralesCotSoloLectura(){
	sendRequest(null,'/MidasWeb/cotizacionsololectura/mostrarDatosGenerales.do?id='+$('idToCotizacion').value,'contenido_datosGenerales','inicializaObjetosEdicionCotizacionSoloLectura(' + $('idToCotizacion').value + ')');
}


var accordionCotizacion = null;
function inicializaObjetosEdicionCotizacionSoloLectura(idToCotizacion){
	accordionCotizacion = new dhtmlXAccordion("accordionCotizacion");
	accordionCotizacion.addItem("a1", "Datos del Asegurado");
	accordionCotizacion.addItem("a2", "Datos del Contratante");
	accordionCotizacion.cells("a1").attachURL("/MidasWeb/cotizacionsololectura/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3&origen=COT");
	accordionCotizacion.cells("a2").attachURL("/MidasWeb/cotizacionsololectura/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1&origen=COT");
	accordionCotizacion.openItem("a1");
	accordionCotizacion.setEffect(true);
}

function setTabIncisosCOTRO(){	
	dhx_init_tabbars();
	cotizaciontabbar.setTabActive('resumenIncisos');
}


function mostrarDatosIncisoSoloLectura(id, modificar, numeroInciso, soloLectura){
	// ,'datosInciso'
	new Ajax.Request("/MidasWeb/cotizacionsololectura/mostrarDatosInciso.do", {
		method : "post",
		asynchronous : false,
		parameters : "cotizacionId=" + id + "&modificar=" + modificar + "&numeroInciso=" + numeroInciso + "&soloLectura=" + soloLectura,
		onSuccess : function(transport) {
		renderDatosIncisoSoloLectura(transport.responseXML);
		} // End of onSuccess
	});	
}	
function renderDatosIncisoSoloLectura(doc){
	var data = doc.getElementsByTagName("datosinciso")[0].firstChild.nodeValue;
	var datosInciso = $('datosInciso');
	datosInciso.innerHTML = data;
}






function mostrarSeccionesPorIncisoCOTRO(numeroInciso,idToCotizacion){
	seccionesPorInciso = new dhtmlXGridObject('seccionesPorIncisoGrid');									  
	seccionesPorInciso.setHeader("Seccion,Contratada,Clave Obligatoriedad,Suma Asegurada,Prima Neta, Subincisos,puedeTenerSubIncisos");
	seccionesPorInciso.setColumnIds("seccion,claveContrato,claveObligatoriedad,sumaAsegurada,primaNeta,seleccionado,puedeTenerSubIncisos");
	seccionesPorInciso.setInitWidths("200,100,0,130,100,100,100");
	seccionesPorInciso.setColAlign("left,center,center,center,center,center,center");
	seccionesPorInciso.setColSorting("str,str,int,str,str,str,int");
	seccionesPorInciso.setColTypes("ro,ch,ro,edn,edn,ra,ro");
	seccionesPorInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesPorInciso.setColumnHidden(2, true);
	seccionesPorInciso.setColumnHidden(6, true);
	seccionesPorInciso.setNumberFormat("$0,000.00", 3);
	seccionesPorInciso.setNumberFormat("$0,000.00", 4);
	seccionesPorInciso.setSkin("light");
	seccionesPorInciso.attachEvent("onRowCreated", function(rowId, rowObj){
		var claveObligatoriedad = seccionesPorInciso.cellById(rowId, 2);
		seccionesPorInciso.cellById(rowId, 1).setDisabled(true);
	
		var puedeTenerSubIncisos = seccionesPorInciso.cellById(rowId, 6);
		if (puedeTenerSubIncisos.getValue() > 1){
			seccionesPorInciso.setCellExcellType(rowId,5,"ro");
			seccionesPorInciso.cellById(rowId, 5).setValue('');
		}
		seccionesPorInciso.cellById(rowId, 3).setDisabled(true);
		seccionesPorInciso.cellById(rowId, 4).setDisabled(true);
	});
	seccionesPorInciso.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var obligatoriedad = seccionesPorInciso.cellById(rId, 2);
		if(obligatoriedad.getValue() === '2' && stage == 0 && cInd == 1) {
			var existeObligatoriaParcialContratada = false;
			seccionesPorInciso.forEachRow(function(id){
				var obligatoriedadCurrentRow = seccionesPorInciso.cellById(id, 2);
				var claveContrato = seccionesPorInciso.cellById(id, 1);
				if(obligatoriedadCurrentRow.getValue() === '2' && rId != id && claveContrato.getValue() == '1') {
					existeObligatoriaParcialContratada = true;
				}
			});
			if(!existeObligatoriaParcialContratada) {
				alert("La Seccion no puede ser descontratada.\nDebe contratar al menos una Seccion Obligatoria Parcial.");
				return false;
			}
		}
		return true;
	});

	seccionesPorInciso.attachEvent("onCellChanged",function(rId,cInd,nValue){
		var claveContrato = seccionesPorInciso.cellById(rId, 1);
		if(cInd == 5 && nValue== 1 && claveContrato.getValue() == '0') {
			cellInTurn = seccionesPorInciso.cellById(rId,5);
			document.getElementById('resultados').style.display ="none";
			//document.getElementById('b_agregar').style.display ="none";
			processor.setUpdated(rId, false,'updated');
			return false;
		}
		cellInTurn = seccionesPorInciso.cellById(rId,6);
		if (cellInTurn.getValue() < 2){ 
			if (cInd == 5 && nValue== 1){
				cellInTurn = seccionesPorInciso.cellById(rId,1);
				ids=rId.split(',');
				$('idToCotizacion').value=ids[0];
				$('numeroInciso').value=ids[1];
				$('idToSeccion').value=ids[2];
				//document.getElementById('b_agregar').style.display ="block";
				document.getElementById('resultados').style.display ="block";
				sendRequest(null,'/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do?idSeccionCot=' + rId,'resultados',null);
				processor.setUpdated(rId, false,'updated');
			}
		}
	});
	
	seccionesPorInciso.attachEvent("onCheck",function(rId,cInd,state){
		if (cInd == 1){
			cellInTurn = seccionesPorInciso.cellById(rId,1);
			sendRequest(null,'/MidasWeb/cotizacion/modificarSeccion.do?idSeccionCot=' + rId + "&contratada=" + state + "&obligatoriedad=" + cellInTurn.getValue(),null,'creaArbolCotizacion('+idToCotizacion+')');
			processor.setUpdated(rId, false, 'updated');
			if(state == false) {
				document.getElementById('resultados').innerHTML = "";
				var sumaAsegurada = seccionesPorInciso.cellById(rId, 3);
				sumaAsegurada.setValue('0');
				var primaNeta = seccionesPorInciso.cellById(rId, 4);
				primaNeta.setValue('0');
			}
		}
	});
	
	seccionesPorInciso.enableDragAndDrop(false);	
	seccionesPorInciso.enableLightMouseNavigation(false);
	seccionesPorInciso.init();
	seccionesPorInciso.load('/MidasWeb/cotizacionsololectura/cargarSeccionesPorInciso.do?numeroInciso='+numeroInciso+'&idToCotizacion='+idToCotizacion, null, 'json');
	processor = new dataProcessor("/MidasWeb/cotizacion/modificarSeccion.do");
	processor.enableDataNames(true);
	processor.setTransactionMode("POST");
	processor.setUpdateMode("off");
	processor.init(seccionesPorInciso);		
	
}


function mostrarDatosSubIncisoSoloLectura(modificar, soloLectura){
	var idToCotizacion = document.getElementById("idToCotizacion").value;
	var numeroInciso = document.getElementById("numeroInciso").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	var numeroSubInciso = document.getElementById("numeroSubInciso").value;
	new Ajax.Request("/MidasWeb/cotizacionsololectura/mostrarDatosSubInciso.do", {
		method : "post",
		asynchronous : false,
		parameters : "idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso + "&idToSeccion=" + idToSeccion + 
		"&modificar=" + modificar + "&soloLectura=" + soloLectura + "&numeroSubInciso=" + numeroSubInciso,
		onSuccess : function(transport) {
			renderDatosSubIncisoSoloLectura(transport.responseXML);
		} // End of onSuccess
	});	
}	



function renderDatosSubIncisoSoloLectura(doc){
	var data = doc.getElementsByTagName("datossubinciso")[0].firstChild.nodeValue;
	var datosInciso = $('datosSubInciso');
	datosInciso.innerHTML = data;
}




var cotizacionCoberturaGrid;
var coberturaProcessor;

function mostrarCoberturasPorSeccionCOTRO(idToCotizacion,numeroInciso,idToSeccion){

	var div = $('cotizacionCoberturasGrid');
	if(div == null) {
		initAutorizacionCoberturaGrid(idToCotizacion,numeroInciso,idToSeccion);
		return true;
	}	
	cotizacionCoberturaGrid = new dhtmlXGridObject("cotizacionCoberturasGrid");
	cotizacionCoberturaGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionCoberturaGrid.setEditable(true);
	cotizacionCoberturaGrid.setSkin("light");

	cotizacionCoberturaGrid.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible,A/R/D,,,");
	cotizacionCoberturaGrid.setInitWidths("0,0,0,0,0,0,0,0,200,80,150,50,100,80,80,30,80,30,60,40,40,60,60,0,0,0");
	cotizacionCoberturaGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,edn,ro,co,img,co,img,coro,edn,edn,coro,img,ro,ro,ro");
	cotizacionCoberturaGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,left,left,left,left,center,center,center");
	cotizacionCoberturaGrid.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int,int");
	cotizacionCoberturaGrid.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
	cotizacionCoberturaGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,tipoDeducible,minimo,maximo,tipoLimiteDeducible,img3,desglosaRiesgo,claveSubIncisos,claveIgualacion");		
	cotizacionCoberturaGrid.setColumnHidden(0,true);
	cotizacionCoberturaGrid.setColumnHidden(1,true);
	cotizacionCoberturaGrid.setColumnHidden(2,true);
	cotizacionCoberturaGrid.setColumnHidden(3,true);
	cotizacionCoberturaGrid.setColumnHidden(4,true);
	cotizacionCoberturaGrid.setColumnHidden(5,true);
	cotizacionCoberturaGrid.setColumnHidden(6,true);
	cotizacionCoberturaGrid.setColumnHidden(7,true);
	cotizacionCoberturaGrid.setColumnHidden(23,true);
	cotizacionCoberturaGrid.setColumnHidden(24,true);
	cotizacionCoberturaGrid.setColumnHidden(25,true);	
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",10);
	cotizacionCoberturaGrid.setNumberFormat("$0,000.00",12);
	
	cotizacionCoberturaGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		cotizacionCoberturaGrid.cellById(rowId, 12).setDisabled(true);
		var tipoSumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 5);
		var desglosaRiesgo = cotizacionCoberturaGrid.cellById(rowId, 23);
		var requiereSubIncisos = cotizacionCoberturaGrid.cellById(rowId, 24);
		var obligatoriedad = cotizacionCoberturaGrid.cellById(rowId, 4);
		var sumaAsegurada = cotizacionCoberturaGrid.cellById(rowId, 10);
		var claveContrato = cotizacionCoberturaGrid.cellById(rowId, 9);
		var coaseguro = cotizacionCoberturaGrid.cellById(rowId, 14);
		var deducible = cotizacionCoberturaGrid.cellById(rowId, 16);
		var cellARD = cotizacionCoberturaGrid.cellById(rowId, 22);
		var claveIgualacion = cotizacionCoberturaGrid.cellById(rowId, 25);
		
		if(claveContrato.getValue()=='1'){
			var idToCotizacion =  cotizacionCoberturaGrid.cellById(rowId, 0).getValue();
			var numeroInciso =  cotizacionCoberturaGrid.cellById(rowId, 1).getValue();
			var idToSeccion =  cotizacionCoberturaGrid.cellById(rowId, 2).getValue();
			var idToCobertura =  cotizacionCoberturaGrid.cellById(rowId, 3).getValue();
			if(claveIgualacion.getValue() == "1") {
				var cadena = "javascript: sendRequest(null,&#39;/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion="+idToCotizacion;
				cadena = cadena +"&numeroInciso="+numeroInciso+"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura;
				cadena = cadena +"&#39;,&#39;configuracion_detalle&#39;,null);";
				cellARD.setValue("/MidasWeb/img/details.gif^Aumentos/Recargos/Descuentos^"+cadena+"^_self");
			} else {
				cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
				cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			}
		}else{
			cotizacionCoberturaGrid.setCellExcellType(rowId,22,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,17,"ro");
			cotizacionCoberturaGrid.setCellExcellType(rowId,15,"ro");			
			//cotizacionCoberturaGrid.setCellExcellType(rowId,18,"ro");
			//cotizacionCoberturaGrid.setCellExcellType(rowId,21,"ro");
			
			//Agregado por CesarMorales..
			if(tipoSumaAsegurada.getValue()=='1' || tipoSumaAsegurada.getValue()=='3'){
				cotizacionCoberturaGrid.cellById(rowId, 10).setValue('0.00');
			}
			/////
			cotizacionCoberturaGrid.cellById(rowId, 11).setValue('0.0000');
			cotizacionCoberturaGrid.cellById(rowId, 12).setValue('0.0');
			cotizacionCoberturaGrid.cellById(rowId, 22).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 17).setValue('');
			cotizacionCoberturaGrid.cellById(rowId, 15).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 18).setValue('');
			//cotizacionCoberturaGrid.cellById(rowId, 21).setValue('');

		}
			cotizacionCoberturaGrid.setCellExcellType(rowId,10,"ro");		
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);				
			claveContrato.setDisabled(true);
			sumaAsegurada.setDisabled(true);
					
		
		
		return true;
	});	
	cotizacionCoberturaGrid.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacionsololectura/mostrarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	cotizacionCoberturaGrid.load(cotizacionCoberturaPath,null, 'json');	
	
	
	
		
	coberturaProcessor.init(cotizacionCoberturaGrid);	
}

function regresarACoberturasSoloLectura(){
	var idToCotizacion = $('idToCotizacion').value;
	var numeroInciso = $('numeroInciso').value;
	var idToSeccion = $('idToSeccion').value;
	
	sendRequest(null,'/MidasWeb/cotizacionsololectura/listarCoberturas.do?idToCotizacion='+idToCotizacion +'&numeroInciso='+ numeroInciso + '&idToSeccion='+idToSeccion, 'configuracion_detalle','mostrarCoberturasPorSeccionCOTRO('+idToCotizacion+','+numeroInciso+','+idToSeccion+')');
}



function initCotizacionSoloLecturaRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura) {
	cotizacionRiesgoPath = '/MidasWeb/cotizacion/riesgo/listarRiesgos.do';
	cotizacionRiesgoPath += '?idToCotizacion='+idToCotizacion;
	cotizacionRiesgoPath += '&numeroInciso='+numeroInciso;
	cotizacionRiesgoPath += '&idToSeccion='+idToSeccion;
	cotizacionRiesgoPath += '&idToCobertura='+idToCobertura;

	var div = $('cotizacionRiesgosGrid');
	if(div == null) {
		initAutorizacionRiesgoGrid(idToCotizacion,numeroInciso,idToSeccion,idToCobertura);
		return true;
	}
	cotizacionRiesgoGrid = new dhtmlXGridObject("cotizacionRiesgosGrid");
	cotizacionRiesgoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	cotizacionRiesgoGrid.setSkin("light");
	cotizacionRiesgoGrid.setHeader("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"Riesgo,Contratado,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,,Tipo Deducible,Minimo, Maximo,T.Limite Deducible");
	cotizacionRiesgoGrid.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada," +
			"riesgo,contratado,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,claveSubIncisos," +
			"tipoDeducible,minimo,maximo,tipoLimiteDeducible");
	cotizacionRiesgoGrid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*,*");
	cotizacionRiesgoGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,edn,ro,edn,ro,co,img,co,img,ro,coro,edn,edn,coro");
	cotizacionRiesgoGrid.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,center,left,center,center,left,left,left,left");
	cotizacionRiesgoGrid.setColSorting("na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na");
	cotizacionRiesgoGrid.enableResizing("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	cotizacionRiesgoGrid.setColumnHidden(0, true);
	cotizacionRiesgoGrid.setColumnHidden(1, true);
	cotizacionRiesgoGrid.setColumnHidden(2, true);
	cotizacionRiesgoGrid.setColumnHidden(3, true);
	cotizacionRiesgoGrid.setColumnHidden(4, true);
	cotizacionRiesgoGrid.setColumnHidden(5, true);
	cotizacionRiesgoGrid.setColumnHidden(6, true);
	cotizacionRiesgoGrid.setColumnHidden(7, true);
	cotizacionRiesgoGrid.setColumnHidden(18, true);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",10);
	cotizacionRiesgoGrid.setNumberFormat("$0,000.00",12);	
	var combo = cotizacionRiesgoGrid.getCombo(14);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(16);
	combo.put("0.0","N/A");
	combo.save();
	combo = cotizacionRiesgoGrid.getCombo(19);
	getCoasegurosDeducibles("TD", 0, combo);
	combo = cotizacionRiesgoGrid.getCombo(22);
	getCoasegurosDeducibles("LD", 0, combo);
	cotizacionRiesgoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		inicializaRowRiesgoSoloLectura(rowId, rowObj);
		return true;
	});
	cotizacionRiesgoGrid.enableLightMouseNavigation(true);
	cotizacionRiesgoGrid.init();
	cotizacionRiesgoGrid.load(cotizacionRiesgoPath, null, 'json');
	
}

function inicializaRowRiesgoSoloLectura(rowId, rowObj) {
	var primaNeta = cotizacionRiesgoGrid.cellById(rowId, 12);
	primaNeta.setDisabled(true);
	var claveObligatoriedad = cotizacionRiesgoGrid.cellById(rowId, 6);
	var contratado = cotizacionRiesgoGrid.cellById(rowId, 9);
	var claveSubIncisos = cotizacionRiesgoGrid.cellById(rowId, 18);
	if(claveObligatoriedad.getValue() == '0' || claveObligatoriedad.getValue() == '1') {
		contratado.setDisabled(true);
		//contratado.setDisabled(false);
	} else if(claveObligatoriedad.getValue() == '3') {
		contratado.setDisabled(true);
		contratado.setChecked(true);
	}
	var claveTipoSumaAsegurada = cotizacionRiesgoGrid.cellById(rowId, 7);
	var sumaAsegurada = cotizacionRiesgoGrid.cellById(rowId, 10);
	
	sumaAsegurada.setDisabled(true);
	

	var idToSeccion = cotizacionRiesgoGrid.cellById(rowId,2);;
	var idToCobertura = cotizacionRiesgoGrid.cellById(rowId,3);
	var idToRiesgo = cotizacionRiesgoGrid.cellById(rowId,4);
	var coaseguroCombo = cotizacionRiesgoGrid.getCustomCombo(rowId, 14);
	var deducibleCombo = cotizacionRiesgoGrid.getCustomCombo(rowId, 16);

	getCoasegurosDeduciblesRiesgo("C", idToSeccion.getValue(), idToCobertura.getValue(), idToRiesgo.getValue(), coaseguroCombo);		
	getCoasegurosDeduciblesRiesgo("D", idToSeccion.getValue(), idToCobertura.getValue(), idToRiesgo.getValue(), deducibleCombo);
}



/* C�digo para textos adicionales danios->cotizacion->textosAdicionales*/
var texAdicionalPath;
var texAdicionalGrid;

function cargaComponentesTexAdicionalSoloLectura(){
	mostrarTexAdicionalSoloLecturaGrid();
}

function initTexAdicionalSoloLecturaGrid() {
	texAdicionalPath = '/MidasWeb/cotizacionsololectura/mostrarListarTextosAdicionales.do?idToCotizacion='+
					$('idToCotizacion').value;	
}

function mostrarTexAdicionalSoloLecturaGrid() {
	texAdicionalGrid = new dhtmlXGridObject('contenido_textosAdicionalesGrid');
	texAdicionalGrid.setHeader("Texto Adicional,Solicitado por,Autorizado/Rechazado por, Autorizaci&oacute;n, Fecha");
	texAdicionalGrid.setColumnIds("textAdicional,nombreUsuarioCreacion,nombreUsuarioModificacion,estatus,fechaCreacion");
	texAdicionalGrid.setInitWidths("300,180,180,100,120");
	texAdicionalGrid.setColAlign("left,center,center,center,center");
	texAdicionalGrid.setColSorting("str,str,str,str,str");
	texAdicionalGrid.setColTypes("ro,ro,ro,img,ro");
	texAdicionalGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	texAdicionalGrid.setSkin("light");		
	texAdicionalGrid.enableDragAndDrop(true);
	texAdicionalGrid.enableLightMouseNavigation(false);
	texAdicionalGrid.enableMultiline(true);

	texAdicionalGrid.init();	
	
	initTexAdicionalSoloLecturaGrid();
	texAdicionalGrid.load(texAdicionalPath, null, 'json');
}



//////////////////////////////////////


var docAnexoPath;
var docAnexoGrid;
var processor;

function cargaComponentesDocAnexosSoloLectura(){
	mostrarDocAnexoGridSoloLectura();
}

function mostrarDocAnexoGridSoloLectura() {docAnexoGrid = new dhtmlXGridObject('contenido_documentosAnexosGrid');
docAnexoGrid.setHeader(",,Seleccione,Nivel,Anexo,");
docAnexoGrid.setColumnIds("idToCotizacion,obligatoriedad,claveSeleccion,tipo,descripcion,download");
docAnexoGrid.setInitWidths("0,0,100,80,*,20");
docAnexoGrid.setColAlign("center,center,center,center,left,centar");
docAnexoGrid.setColSorting("int,int,int,str,str,na");
docAnexoGrid.setColTypes("ro,ro,ch,ro,ro,img");
docAnexoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
docAnexoGrid.setSkin("light");
docAnexoGrid.enableDragAndDrop(true);
docAnexoGrid.enableLightMouseNavigation(false);
docAnexoGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
	var obligatoriedad=docAnexoGrid.cellById(rowId, 1).getValue();
	var tipo=docAnexoGrid.cellById(rowId, 3).getValue();
		

		docAnexoGrid.cellById(rowId, 2).setDisabled(true);
	
});	

docAnexoGrid.setColumnHidden(0, true);
docAnexoGrid.setColumnHidden(1, true);
docAnexoGrid.init();	

docAnexoProcessor = new dataProcessor("/MidasWeb/cotizacion/documento/docAnexoCot/modificarDocAnexo.do");

docAnexoPath = '/MidasWeb/cotizacion/documento/docAnexoCot/mostrarListarAnexos.do?idToCotizacion='+$('idToCotizacion').value;
docAnexoGrid.load(docAnexoPath, null, 'json');	
docAnexoProcessor.enableDataNames(true);
docAnexoProcessor.setTransactionMode("POST");
//docAnexoProcessor.setUpdateMode("row");	
docAnexoProcessor.setUpdateMode("off");
docAnexoProcessor.init(docAnexoGrid);
	

}




function initDocAnexoGridSoloLectura() {
	docAnexoPath = '/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarListarAnexos.do?idToCotizacion='+
					$('idToCotizacion').value;	
}




function mostrarEdicionComisionesPorCotizacionGridSoloLectura(idCotizacion,tipoEdicion) {
	if (tipoEdicion != undefined && tipoEdicion != null){
		comisiones= new dhtmlXGridObject('editarComisionesCotizacionGrid');
		comisiones.setHeader("Ramo,SubRamo,Tipo %,Comision General, Comision de la cotizacion,Comision, Comision Cedida,Comision del Periodo,Comision RPF,#cspan,#cspan");
		comisiones.setColumnIds("descripcionRamo,descripcionSubRamo,tipoPorcentaje,comisionDefault,comisionCotizacion,comision,comisionCedida,comisionPorDevengar,comisionRPF,estado,autorizacion");
		comisiones.setInitWidths("100,160,40,140,125,100,100,100,100,20,20");
		comisiones.setColAlign("left,left,center,center,center,center,center,center,center,center,center");
		
		//comisiones.setHeader("Ramo,SubRamo,Tipo %,Comision General, Comision de la cotizacion,#cspan,#cspan");
		//comisiones.setColumnIds("descripcionRamo,descripcionSubRamo,tipoPorcentaje,comisionDefault,comisionCotizacion,estado,autorizacion");
		//comisiones.setInitWidths("100,160,40,140,125,20,20");
		//comisiones.setColAlign("left,left,center,center,center,center,center");
		comisiones.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		comisiones.setSkin("light");
		comisiones.enableMultiline(true);
		//comisiones.enableResizing("false,false,false,false,false,false,false");
		comisiones.enableResizing("false,false,false,false,false,false,false,false,false,false,false");
		if (tipoEdicion == 'comision'){
			//comisiones.setColSorting("str,str,str,int,int,str,str");
			//comisiones.setColTypes("ro,ro,ro,ro,ed,img,img");
			comisiones.setColSorting("str,str,str,int,int,str,str,str,str,str,str");
			comisiones.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img");		
			comisiones.setColumnHidden(9, true);
			//comisiones.attachEvent("onEditCell",validarAutorizacion);
			comisiones.init();
			comisiones.load("/MidasWeb/cotizacion/comision/mostrarComisionesCotizacion.do?id="+ idCotizacion+"&tipoEdicion="+tipoEdicion, null, 'json');
			
			//Processor para guardar el porcentaje de la comision
			comisionProcessor = new dataProcessor("/MidasWeb/cotizacion/comision/guardarComisionCotizacion.do");
			comisionProcessor.enableDataNames(true);
			comisionProcessor.setTransactionMode("POST");
			comisionProcessor.setUpdateMode("off");
			comisionProcessor.setVerificator(4, function(value,colName){
				var val = parseFloat(value.toString()._dhx_trim());
				if(isNaN(val)) {
					documentoAnexoProductoError = true;
					return false;}
			    if(val <= 0){
			    	documentoAnexoProductoError = true;
			        return false;
			    } else {
			    	documentoAnexoProductoError = false;
			        return true;}
			});
			comisionProcessor.init(comisiones);
		}
	}
}

