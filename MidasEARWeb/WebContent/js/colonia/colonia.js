var codigoValido=false;

function validaCodigoPostal(){
	codigoValido=false;
	if($('codigoPostal').value==""){
		return;
	}
	$('zipCodeUserId').value ="";
	
	new Ajax.Request("/MidasWeb/catalogos/colonia/validaCodigoPostal.do?codPostal="+$('codigoPostal').value, {
		method : "post",
		asynchronous : false,
		onSuccess : function(resp) { 
			
			}, 
		onFailure : function(resp) { 
			mostrarVentanaMensaje("10", "Ocurrio un error al buscar el codigo postal.", null);
			return;
			},
		onComplete:function(resp){
			 muestraDatosColonia(resp.responseXML);
			}
	});
	
}


function muestraDatosColonia(docXml){
	
	var items = docXml.getElementsByTagName("item");
	
	if(items!=null  && items.length>0){
		var item = items[0];
		$('zipCodeUserId').value = item.getElementsByTagName("idAsignar")[0].firstChild.nodeValue;
		$('codigoPostal').value =item.getElementsByTagName("codPostal")[0].firstChild.nodeValue;
		$('idColonia').value=$('codigoPostal').value+'-'+$('zipCodeUserId').value;
		$('idZonaHidro').value =item.getElementsByTagName("idZonaHidro")[0].firstChild.nodeValue;
		$('idZonaSismo').value =item.getElementsByTagName("idZonaSismo")[0].firstChild.nodeValue;
		if(item.getElementsByTagName("valorIVA")[0] != null) {
			$('valorIVA').value =item.getElementsByTagName("valorIVA")[0].firstChild.nodeValue;
		}
		codigoValido=true;
	
	}else{
		mostrarVentanaMensaje("20", "El codigo postal no se encuentra en el sistema,favor verifique", null);
		$('codigoPostal').focus();
		$('zipCodeUserId').value ="";
		codigoValido=false;
	}
}

function modificarCodigoPostalColonia(){
	if(validaFormulario(document.coloniaForm)){
		parent.showIndicatorSimple();
		new Ajax.Request('/MidasWeb/catalogos/colonia/modificar.do?'+document.coloniaForm.serialize(), {
			method : "post",
			encoding : "UTF-8",
			onSuccess : function(transport) {
				parent.hideIndicator();
				procesarRespuestaXml(transport.responseXML, "listarCodigosPostalesColonias();");
				}
				
		});
	}
}

function validaFormulario(form){

	
	var mensaje="";
	if(form.codigoPostal.value==""){
		mensaje+=" Falta capturar el codigo postal."+'\n';
		
	}
	
	if(form.descripcionColonia.value==""){
		mensaje+=" Falta capturar la descripcion de la colonia ."+'<br>';
		
	}
	
	if(form.idZonaHidro.value =='' || form.idZonaHidro.value.length==0){
		mensaje+=" Falta seleccionar la zona hidro ."+'<br>';
	}
	
	if(form.idZonaSismo.value =='' || form.idZonaSismo.value.length==0){
		mensaje+=" Falta seleccionar la zona sismo."+'<br>';
	}
	if(form.valorIVA.value =='' || form.valorIVA.value.length==0){
		mensaje+=" Falta capturar el valor del I.V.A.<br>";
	}
	if(mensaje.length>0){
		mostrarVentanaMensaje("20", "Se encontraron los siguientes errores: "+'<br>'+mensaje, null);
		return false
	}else{
		return true;
	}
}
function guardaCodigoPostalColonia(){
	if(codigoValido){
		if(validaFormulario(document.coloniaForm)){
			parent.showIndicatorSimple();
			new Ajax.Request('/MidasWeb/catalogos/colonia/agregar.do?'+document.coloniaForm.serialize(), {
				method : "post",
				encoding : "UTF-8",
				onSuccess : function(transport) {
					parent.hideIndicator();
					procesarRespuestaXml(transport.responseXML, "listarCodigosPostalesColonias();");
					}
					
			});
		}
	}
}

function listarCodigosPostalesColonias(){
	sendRequest(null,'/MidasWeb/catalogos/colonia/listarColonias.do', 'contenido',null);
}