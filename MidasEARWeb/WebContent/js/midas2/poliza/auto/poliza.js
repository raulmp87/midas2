var polizaGrid;
var bitacoraGuiaGrid;
var paginadoPolizaParamPath;
var currentNamespace = '/poliza';
var currentMainAction = 'listar';
var cveTipoEndosoCancelacion = 4;

function initPoliza(){
	//initOficina();
	//initGrid();
	polizaInitPaginado(1, true);
}

function limpiarFiltrosPoliza(){
	jQuery('#polizaForm').each (function(){
		  this.reset();
	});
	cleanInputDiv('negocioNombre');
	cleanInput('idNegocioPol');
	if (!agenteControlDeshabilitado) {
		cleanInputDiv('agenteNombre');
		cleanInput('idAgentePol');
	}
}

function polizaInitPaginado(page, nuevoFiltro){
	var posPath = 'posActual='+page+'&funcionPaginar='+'polizaInitPaginado'+'&divGridPaginar='+'polizaGrid';
	if(nuevoFiltro){
		paginadoPolizaParamPath = "";
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, "/MidasWeb/poliza/busquedaPaginada.action" + "?"+ posPath, 'gridPolizasPaginado', 'initGrid();');
}

function initGrid(){
	document.getElementById("polizaGrid").innerHTML = '';
	polizaGrid = new dhtmlXGridObject("polizaGrid");
	mostrarIndicadorCarga('indicador');	
	polizaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	polizaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	polizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	//polizaGrid.load("/MidasWeb/poliza/listarFiltrado.action");	
	polizaGrid.load("/MidasWeb/poliza/listarFiltrado.action" + "?" + posPath);
}

function buscarPolizaPaginado(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'buscarPolizaPaginado'+'&divGridPaginar='+'polizaGrid';
	if(nuevoFiltro){
		paginadoPolizaParamPath = jQuery(document.polizaForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, "/MidasWeb/poliza/busquedaPaginada.action?filtrar=true" + "&"+ paginadoPolizaParamPath + posPath, 'gridPolizasPaginado', 'buscarPolizas();');
}

function buscarPolizas(){
	document.getElementById("polizaGrid").innerHTML = '';
	polizaGrid = new dhtmlXGridObject("polizaGrid");
	mostrarIndicadorCarga('indicador');	
	polizaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	polizaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	polizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	//polizaGrid.load("/MidasWeb/poliza/listarFiltrado.action?filtrar=true" + "&"+ jQuery(document.polizaForm).serialize());
	polizaGrid.load("/MidasWeb/poliza/listarFiltrado.action?filtrar=true" + "&"+ paginadoPolizaParamPath + '&' +posPath);
}

/*function imprimirPoliza(idToPoliza,numPoliza,idToCotizacion){
	var location ="/MidasWeb/impresiones/poliza/imprimirPoliza.action?idToPoliza=" + idToPoliza+"&idToCotizacion="+idToCotizacion;
	window.open(location, "Poliza_" + numPoliza);
}
*/
function imprimePoliza(recordFromUltimoEndoso){
	var idToPoliza = jQuery("#idToPoliza").val();
	var validoEn = jQuery("#validoEn").val();
	var validoEnMillis = jQuery("#validoEnMillis").val();
	var recordFrom = jQuery("#recordFrom").val();
	var recordFromMillis = jQuery("#recordFromMillis").val();
	//alert(idToPoliza + " - " + validoEn + " - "+ validoEnMillis + " - "+ recordFrom + " - "+ recordFromMillis);

	mostrarContenedorImpresion(2,idToPoliza,-1,-1, validoEn, validoEnMillis, recordFromUltimoEndoso, recordFrom, 2);
}
function imprimirPoliza(idToPoliza, validoEn){
	
	var path = '/MidasWeb/impresiones/poliza/imprimirPoliza.action?idToPoliza='+idToPoliza;
	if (validoEn != null) {
		path+= "&validOn=" +validoEn;
	}
	//parent.sendRequestJQ(null, path, parent.targetWorkArea, null);
	window.open(path, "Poliza");
	parent.cerrarVentanaModal('impresionPoliza');
	
}

//function mostrarContenedorImpresion(2,67067,null,197411,'20/06/2012'){
function callMostrarContenedorImpresion(idToPoliza, validoEn){
	
}

function verDetalleImpresionPoliza(idToPoliza){
	mostrarVentanaModal("impresionPoliza","Impresi&oacute;n",200,320, 710, 350, "/MidasWeb/poliza/ventanaDetalleEndoso.action?id="+idToPoliza+"&accion=imprimir");

}

function verDetallePolizaActual(idToPoliza, tipoRegreso) {
	var fullDate = new Date();
	var currentDate = getDateFormat(fullDate);
	var dateMillis = fullDate.getTime(); 
	 verDetallePoliza(idToPoliza, currentDate, dateMillis, currentDate, dateMillis, tipoRegreso, null, true, true);	
}

function verDetallePoliza(idToPoliza, validoEn, validoEnMillis, recordFrom, recordFromMillis, tipoRegreso, claveTipoEndoso, mostrarCancelados, esSituacionActual) {
	
	var path = '/MidasWeb/poliza/verContenedorPoliza.action?id='+idToPoliza;

	if (validoEn != null) {
		path+= "&validoEn=" +validoEn;
	}
	if (validoEnMillis != null) {
		path+= "&validoEnMillis=" + validoEnMillis;
	}
	if (recordFrom != null) {
		path+= "&recordFrom=" + recordFrom;
	}
	if (recordFromMillis != null) {
		path+= "&recordFromMillis=" + recordFromMillis;
	}
	if (tipoRegreso != null && tipoRegreso != undefined) {
		path+= "&tipoRegreso=" + tipoRegreso;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != undefined) {
		path+= "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	if (mostrarCancelados) {
		path+= "&mostrarCancelados=1";
	}
	
	if (esSituacionActual) {
		path+= "&esSituacionActual=true";
	}
	
	//path += "&validoEnMillis=" + validoEnMillis + "&recordFrom=" + recordFrom + "&recordFromMillis=" + recordFromMillis;
	
	parent.sendRequestJQ(null, path, parent.targetWorkArea, null);
	parent.cerrarVentanaModal('consultaPoliza');
}

function verDetalleEndososPoliza(idToPoliza, tipoRegreso){
	var pathRegreso = "";
	if(tipoRegreso != null && tipoRegreso != undefined){
		pathRegreso = "&tipoRegreso="+tipoRegreso;
	}
	mostrarVentanaModal("consultaPoliza","Consulta",200,320, 710, 350, "/MidasWeb/poliza/ventanaDetalleEndoso.action?id="+idToPoliza+"&accion=consultar"+pathRegreso);
}

function obtenerResumenTotalesGrid(){
	document.getElementById("resumenTotalesGrid").innerHTML = '';
	sendRequestJQ(null,"/MidasWeb/poliza/mostrarResumenTotales.action?"+jQuery(document.polizaForm).serialize(), "resumenTotalesGrid", null);
}

function ventanaCorreo(idToPoliza, claveTipoEndoso){
	var validoEn = jQuery('#validoEn').val();
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	
	var path = "";
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" +validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	var url = '/MidasWeb/poliza/ventanaEmails.action?idToPoliza='+idToPoliza+"&claveTipoEndoso="+claveTipoEndoso+path;
	mostrarVentanaModal("correo", 'Capturar Datos Correo', 100, 300, 500, 220, url);	
}

function mostrarCaractVehiculo() {
	var url = '/MidasWeb/poliza/otrasCaractVehiculo.action';
	mostrarModal("correo", 'Capturar Datos Correo', 100, 300, 500, 800, url);
}

function realizaEndoso() {
	sendRequestJQ(null, '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/mostrarDefTipoModificacion.action',targetWorkArea, null);
}

function solicitarEndoso(idToPoliza,numeroPolizaFormateado)
{	
	var definirTipoModificacionPath = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/mostrarDefTipoModificacion.action";
	
	sendRequestJQ(null,definirTipoModificacionPath+'?namespaceOrigen='+currentNamespace+'&actionNameOrigen='+currentMainAction+'&idPolizaBusqueda='+idToPoliza+'&numeroPolizaFormateado='+numeroPolizaFormateado,targetWorkArea,null);
}


function seleccionarAgentePoliza(bandera){	
	mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,320, 710, 155, "/MidasWeb/poliza/ventanaAgentes.action?bandera="+bandera);	
}

function cargaAgentePoliza(id,nombre){
	if(nombre != "" && nombre != undefined){
		var campos = nombre.split("-");
		jQuery('#idAgentePol').val(id);
		var idAgente = jQuery.trim(campos[1]);
		jQuery('#idAgenteClavePol').val(idAgente);
		jQuery('#agenteNombre').text(campos[0]);
		cerrarVentanaModal("agente");
	}
}


function seleccionarNegocioPoliza(bandera){	
	mostrarVentanaModal("negocio","Selecci\u00F3n Negocio",200,320, 710, 155, "/MidasWeb/poliza/ventanaNegocios.action?bandera="+bandera);	
}

function cargaNegocioPoliza(id,descripcion){
	jQuery('#idNegocioPol').val(id);
	if (descripcion.length > 26){
		descripcion = descripcion.substring(0,26);
	}
	jQuery('#negocioNombre').text(descripcion);
	cerrarVentanaModal("negocio");
}

function regresarListadoPoliza() {
	
	var url = '/MidasWeb/poliza/listar.action?esRetorno=1';
	var nextFunction = "compareDates();";
	var tipoRegreso = jQuery("#tipoRegreso").val();
	if (tipoRegreso == 1) {
		url = '/MidasWeb/poliza/renovacionmasiva/listar.action?polizaDTO.numeroPoliza='+jQuery("#numeroPoliza").val()+"&esRetorno=1";
		nextFunction = null;
	} else if(tipoRegreso == 2) {
		url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/mostrarDefTipoModificacion.action?numeroPolizaFormateado='+jQuery("#numPolizaFormateada").val()+"&esRetorno=0";
		nextFunction = null;
	} else if(tipoRegreso == 3) {
		url = '/MidasWeb/endoso/cotizacion/auto/listar.action?esRetorno=1';
		nextFunction = "pageGridPaginado(1, true);";
	}	
	sendRequestJQ(null, url, targetWorkArea, nextFunction);
}

function desplegarDetallePoliza() {
	var idToPoliza = jQuery('#idToPoliza').val();
	var validoEn = jQuery('#validoEn').val();
	
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	var tipoRegreso = jQuery('#tipoRegreso').val();
	var claveTipoEndoso = jQuery('#claveTipoEndoso').val();
	var mostrarCancelados = jQuery('#mostrarCancelados').val();
	var esSituacionActual = jQuery('#esSituacionActual').val();
	
	var path = '/MidasWeb/poliza/verDetallePoliza.action?id='+idToPoliza;
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" +validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != "") {
		path += "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	if (tipoRegreso) {
		path += "&tipoRegreso=" + tipoRegreso;
	}
	
	if (mostrarCancelados) {
		path += "&mostrarCancelados=" + mostrarCancelados;
	}
	
	if (esSituacionActual) {
		path += "&esSituacionActual=" + esSituacionActual;
	}
	
	sendRequestJQ(null, path, 'contenido_detallePoliza', null);
}

function desplegarEmision() {
	var idToPoliza = jQuery('#idToPoliza').val();
	var validoEn = jQuery('#validoEn').val();
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	var claveTipoEndoso = jQuery('#claveTipoEndoso').val();
	
	
	var path = '/MidasWeb/poliza/emision/verEmision.action?polizaId='+idToPoliza;
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" +validoEn;
	}
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != "") {
		path += "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	sendRequestJQ(null, path, 'contenido_emision', null);
}

/**
 * @author 	Eduardo.Chavez
 * 			2015-09-23
 * 			Script que carga la Pestaña de Alertas SAP-AMIS de la pantalla Consulta Poliza
 */
function desplegarAlertasSAPAMIS() {
//	var idToPoliza = jQuery('#idToPoliza').val();
//	var validoEn = jQuery('#validoEn').val();
//	var validoEnMillis = jQuery('#validoEnMillis').val();
//	var recordFrom = jQuery('#recordFrom').val();
//	var recordFromMillis = jQuery('#recordFromMillis').val();
//	var claveTipoEndoso = jQuery('#claveTipoEndoso').val();
	
	
	var path = '/MidasWeb/poliza/emision/verEmision.action?polizaId='+idToPoliza;
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" +validoEn;
	}
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != "") {
		path += "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	sendRequestJQ(null, path, 'contenido_emision', null);
}


function verEmisionTest(idToPoliza) {
	
	var path = '/MidasWeb/poliza/emision/verEmision.action?polizaId='+idToPoliza;
	var validoEn= null;
	if (validoEn != null) {
		path+= "&validoEn=" +validoEn;
	}
	parent.sendRequestJQ(null, path, parent.targetWorkArea, null);
	parent.cerrarVentanaModal('consultaPoliza');
}

//mostrarVentanaModal
//function mostrarContenedorImpresion(tipoImpresion,idToPoliza,numeroPolizaFormateada,idCotizacion){
//    mostrarContenedorImpresion(tipoImpresion, idToPoliza, numeroPolizaFormateada, idCotizacion, null, null);
//}

function mostrarContenedorImpresion(tipoImpresion,idToPoliza,numeroPolizaFormateada,idCotizacion, validOn, validOnMillis, recordFromMillis, recordFrom, claveTipoEndoso, esSituacionActual){
	var url = '/MidasWeb/impresiones/componente/mostrarContenedorImpresion.action?id='+idCotizacion+ "&tipoImpresion="+tipoImpresion+"&idToPoliza="+idToPoliza;
	
	if(validOn){
		url = url + "&validOn=" + validOn;
	}
	
	if(validOnMillis){
		url = url + "&validOnMillis=" + validOnMillis;
	}
	
	if(recordFromMillis){
		url = url + "&recordFromMillis=" + recordFromMillis;
	}
	
	if(recordFrom){
		url = url + "&recordFrom=" + recordFrom;
	}
	
	if(claveTipoEndoso){
		url = url + "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	if (esSituacionActual) {
		url += "&esSituacionActual=" + esSituacionActual;
	}
	parent.mostrarVentanaModal("mostrarContenedorImpresion", 'Impresiones', 200, 320, 540, 485, url);
}

function mostrarContenedorImpresionIncisoPoliza(tipoImpresion,idToPoliza,numeroPolizaFormateada,idCotizacion, validOn, validOnMillis, recordFromMillis, recordFrom, claveTipoEndoso, esSituacionActual, txtIncios){
	var url = '/MidasWeb/impresiones/componente/mostrarContenedorImpresion.action?id='+idCotizacion+ "&tipoImpresion="+tipoImpresion+"&idToPoliza="+idToPoliza.val()+"&txtIncios="+txtIncios.val();
	
	if(validOn){
		url = url + "&validOn=" + validOn.val();
	}
	
	if(validOnMillis){
		url = url + "&validOnMillis=" + validOnMillis.val();
	}
	
	if(recordFromMillis){
		url = url + "&recordFromMillis=" + recordFromMillis.val();
	}
	
	if(recordFrom){
		url = url + "&recordFrom=" + recordFrom.val();
	}
	
	if(claveTipoEndoso){
		url = url + "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	if (esSituacionActual) {
		url += "&esSituacionActual=" + esSituacionActual;
	}
	parent.mostrarVentanaModal("mostrarContenedorImpresion", 'Impresiones', 200, 320, 540, 485, url);
}

function imprimirEndoso(idToCotizacion, recordFrom, recordFromMillis, claveTipoEndoso){
	var url = '/MidasWeb/impresiones/poliza/imprimirEndosoM2.action?idToCotizacion=' + idToCotizacion + "&recordFrom=" + recordFrom + "&recordFromMillis="+recordFromMillis + "&claveTipoEndoso=" + claveTipoEndoso;
	window.open(url);
}

function mostrarDatosCliente(personaContratanteId){
	var idToPoliza = jQuery("#idToPoliza").val();
	var validoEn = jQuery("#validoEn").val();
	var validoEnMillis = jQuery("#validoEnMillis").val();
	var recordFrom = jQuery("#recordFrom").val();
	var recordFromMillis = jQuery("#recordFromMillis").val();
	var claveTipoEndoso = jQuery("#claveTipoEndoso").val();
	
	var verDetalleClientePath = "/MidasWeb/catalogoCliente/loadById.action";
	operacionGenericaConParams(verDetalleClientePath,2,{'divCarga':'contenido','tipoRegreso':'3',
		'cliente.idCliente':personaContratanteId,'idToPoliza':idToPoliza,'validoEn':validoEn,'validoEnMillis':validoEnMillis,
		'recordFrom':recordFrom,'recordFromMillis':recordFromMillis,'claveTipoEndoso':claveTipoEndoso});
}

function cargarListadoIncisos(cotizacionContinuity, mostrarCancelados) {
	var validoEn = jQuery("#validoEn").val();
	var recordFrom = jQuery("#recordFrom").val();
	var aplicaFlotillas = jQuery("#aplicaFlotillas").val();
	var claveTipoEndoso = jQuery("#claveTipoEndoso").val();
	
	if (claveTipoEndoso != null && claveTipoEndoso== cveTipoEndosoCancelacion) {
		mostrarCancelados = 1;
		jQuery("#divRadioCancelados").css("display","none");
	}

	document.getElementById("listadoDeIncisos").innerHTML = '';
	sendRequestJQ(null,"/MidasWeb/poliza/inciso/mostrarIncisos.action?"+"cotizacionId=" + cotizacionContinuity + "&validoEn=" + validoEn
			+ "&recordFrom=" + recordFrom + "&mostrarCancelados=" + mostrarCancelados +  "&aplicaFlotillas=" + aplicaFlotillas
			,  "listadoDeIncisos", null);
}

function solicitarSeguroObligatorio(idToPoliza,numeroPolizaFormateado)
{	
	var seguroObligatorioPath = "/MidasWeb/poliza/seguroobligatorio/mostrar.action";
	mostrarVentanaModal("seguroObligatorio","Seguro Obligatorio",200,320, 710, 320, seguroObligatorioPath+"?idToPoliza="+idToPoliza);
}

function mostrarMovimientosEndoso(cotizacioncontinuityid){
	var movimientosEndosoPath = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/mostrarMovimientos.action";
	mostrarVentanaModal("movimientosEndoso","Movimientos Endoso",200,320, 710, 320, movimientosEndosoPath+"?cotizacion.continuity.id="+cotizacioncontinuityid);	
}

function verDetalleRecibosPoliza(idToPoliza){
	var mostrarRecibosPath = "/MidasWeb/impresiones/recibos/mostrarContenedorRecibos.action";
	mostrarVentanaModal("ventanaRecibosPoliza","Recibos",200,320, 710, 500, mostrarRecibosPath+"?polizaDTO.idToPoliza="+idToPoliza);	
}

function ventanBitacoraEnvioPoliza(idToPoliza, numeroPoliza){
	var mostrarBitacoraEnvioPath = "/MidasWeb/poliza/ventanaDetalleBitacoEnvio.action";
	mostrarVentanaModal("ventanBitacoraEnvio","Bitacora de Envio",200,320, 900, 500, mostrarBitacoraEnvioPath+"?polizaDTO.idToPoliza="+idToPoliza+"&numeroPolizaFormateada="+numeroPoliza);	
}

function cargarBitacoraEnvio(idToPoliza){
	jQuery("#bitacoraPolizaGrid").empty();
	bitacoraGuiaGrid = new dhtmlXGridObject("bitacoraPolizaGrid");
	mostrarIndicadorCarga('indicador');	
	bitacoraGuiaGrid.load("/MidasWeb/poliza/bitacoraEnvio.action?polizaDTO.idToPoliza="+idToPoliza);
}


function loadBuscarPolizaPag(){
	blockPage();
	var fecha1=dwr.util.getValue("polizaDTO.fechaCreacion");
	var fecha2=dwr.util.getValue("fechaFinal");
	var num_poliza_midas = dwr.util.getValue("polizaDTO.numeroPoliza");
	var num_poliza_seycos = dwr.util.getValue("polizaDTO.numeroPolizaSeycos");
	var num_serie = dwr.util.getValue("polizaDTO.numeroSerie");
	var varText = "";
	var folio_cotizacion = dwr.util.getValue("polizaDTO.cotizacionDTO.folio");
	
	if(validate_filters(fecha1, fecha2, num_poliza_midas, num_poliza_seycos)){
    	varText="Se debe seleccionar al menos 1 filtro para la búsqueda.";
    }else{
    	if (compare_dates(fecha1, fecha2)){  
    		varText="La fecha de Emision desde no debe ser mayor a la fecha hasta. Verificar."; 
    	}else if(compare_dates_period(fecha1, fecha2)) {
    		varText="El periódo entre la fecha de Emisión desde y la fecha hasta no debe ser mayor a 1 año. Verificar.";
    	}else {
	    	// Definir fechas por default cuando no se capture filtro restrictivo
	    	if(num_poliza_midas.trim() == '' && num_poliza_seycos.trim() == '' && num_serie.trim() == '' && folio_cotizacion.trim() == ''){
	    		definirFechasDefault("polizaDTO.fechaCreacion", "fechaFinal");
	    	}
    	}
    }
	
	if(varText==""){
		buscarPolizaPaginado(1,true);
	}else{
		mostrarMensajeInformativo(varText, 10, null);
	}
    unblockPage();
}

function validate_filters(fecha1, fecha2, num_poliza_midas, num_poliza_seycos) {
	
	var nombre_contratante = dwr.util.getValue("polizaDTO.cotizacionDTO.nombreContratante");
	var num_serie = dwr.util.getValue("polizaDTO.numeroSerie");
	var nombre_agente = dwr.util.getValue("agenteNombre");
	var nombre_negocio = dwr.util.getValue("negocioNombre");
	var conflicto_num_serie = dwr.util.getValue("conflictoNumeroSerie");	
	var ran_minimo = dwr.util.getValue("polizaDTO.limiteInferiorIncisos");
	var ran_maximo = dwr.util.getValue("polizaDTO.limiteSuperiorIncisos");
	var folio_cotizacion = dwr.util.getValue("polizaDTO.cotizacionDTO.folio");	
	
	var cve_aplica_flotillas = document.getElementById("polizaDTO.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas");
	var cve_flotillas = cve_aplica_flotillas.options[cve_aplica_flotillas.selectedIndex].text;
	
	if(num_poliza_midas.trim() != '' || fecha1.trim() != '' || fecha2.trim() != '' || num_poliza_seycos.trim() != '' || nombre_contratante.trim() != '' || num_serie.trim() != ''
		|| nombre_agente.trim() != '' || nombre_negocio.trim() != '' || conflicto_num_serie == true || ran_minimo.trim() != '' || ran_maximo.trim() != '' || folio_cotizacion.trim() != ''
		|| cve_flotillas.trim() != 'Seleccione ...') 
	{
		return (false);
	} else {
		return (true);
	}
}
