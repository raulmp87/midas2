/**
 * 
 */
function listarPolizaAnexa() {
	document.getElementById("polizaAnexaGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	var polizaAnexaGrid = new dhtmlXGridObject("polizaAnexaGrid");
	// deshabilita el doble click y el click en las celdas del grid
	polizaAnexaGrid.enableEditEvents(false, false);
	//polizaAnexaGrid.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp");
	mostrarIndicadorCarga('indicador');	
	polizaAnexaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	polizaAnexaGrid.load("/MidasWeb/poliza/seguroobligatorio/listar.action?idToPoliza="+ dwr.util.getValue("idToPoliza"));

}

function creaPolizaAnexas(){
	if(confirm('\u00BFEst\u00e1 seguro que desea crear la(s) Poliza(s) de Seguro Obligatorio?')){
		var seguroObligatorioPath = "/MidasWeb/poliza/seguroobligatorio/creaPolizaSeguroObligatorio.action";
		redirectVentanaModal("seguroObligatorio",seguroObligatorioPath+"?idToPoliza="+dwr.util.getValue("idToPoliza"),null);
	}
}

function verDetalleImpresionPolizaAnexa(idToPoliza){
	parent.mostrarVentanaModal("impresionPoliza","Impresi&oacute;n",200,320, 710, 350, "/MidasWeb/poliza/ventanaDetalleEndoso.action?id="+idToPoliza+"&accion=imprimir");
}