var mostrarBasesEmisionExt = "/MidasWeb/auto/reportes/mostrarReporteBasesEmisionAuto.action";
var generarReporteBasesEmisionExt = "/MidasWeb/auto/reportes/generarReporteBasesEmisionAuto.action";

function imprimirReporteBasesEmisionAuto(){
	var fechaInicial = dwr.util.getValue('fechaInicial');
	var fechaFinal = dwr.util.getValue('fechaFinal');
	var idRamo = dwr.util.getValue('idRamo');
	var subRamoId = dwr.util.getValue('subRamoId');
	var nivelAgrupamiento = dwr.util.getValue('nivelAgrupamiento');
	
	if(fechaInicial == undefined || fechaInicial == null || fechaInicial == '' ){
		alert('Se tiene que especificar la fecha Inicial.');
	}else if (fechaFinal == undefined || fechaFinal == null || fechaFinal == '') {
		alert('Se tiene que especificar la fecha Final.');
	}else if (nivelAgrupamiento == undefined || nivelAgrupamiento == null || nivelAgrupamiento == '') {
		alert('Se tiene que especificar un nivel de agrupamiento.');
	}
	else{
		var location = generarReporteBasesEmisionExt;
		location = location+"?nivelAgrupamiento="+nivelAgrupamiento;
		if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '' )
			location = location+"&fechaInicial="+fechaInicial;
		if(fechaFinal != undefined && fechaFinal != null && fechaFinal != '' )
			location = location+"&fechaFinal="+fechaFinal ;
		if(idRamo != undefined && idRamo != null && idRamo != '' )
			location = location+"&idRamo="+idRamo;
		if(subRamoId != undefined && subRamoId != null && subRamoId != '' )
			location = location+"&subRamoId="+subRamoId;
		window.open(location,"ReporteBasesEmisionAuto");
	}
	
	
}

function obtenerSubRamos(){
	var idTcRamo = dwr.util.getValue('idRamo');
	var fechaInicial = dwr.util.getValue('fechaInicial');
	var fechaFinal = dwr.util.getValue('fechaFinal');
	if(idTcRamo != null)
		sendRequestJQ(document.reporteBasesEmisionAutoForm, mostrarBasesEmisionExt+"?idRamo="+idTcRamo+
				"&fechaInicial="+fechaInicial+"&fechaFinal="+fechaFinal, targetWorkArea,null);
}
