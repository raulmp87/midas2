/**
 * 
 */

jQuery(document).ready(function() {	
	
	var idMesPrecargar = dwr.util.getValue('mesSeleccionado');
	if(idMesPrecargar == '')
	{
		idMesPrecargar = null;		
	}
	
	listadoService.getMapMonths(function(data) {
		addOptionsHeaderAndSelect("meses", data, idMesPrecargar, "", "Seleccione...");
	})
	
	var idAnioPrecargar = dwr.util.getValue('anioSeleccionado');
	if(idAnioPrecargar == '')
	{
		idAnioPrecargar = null;		
	}	
	
	listadoService.getMapYears(16, function(data) {
		addOptionsHeaderAndSelect("anios", data, idAnioPrecargar, "", "Seleccione...");
	});	
	
	
	var existenDatos = dwr.util.getValue('existenDatos');
	
	if(existenDatos == '' || existenDatos == 'true')
    {		
		jQuery('#botonGenerarDatos').attr("disabled", true);				
    }  
	else
	{ 
		jQuery('#botonGenerarDatos').removeAttr("disabled"); 
	}	
});


function validarDatos()
{
	var idMesSel = dwr.util.getValue("meses");
	var idAnioSel = dwr.util.getValue("anios");	
	
	if(idMesSel == '' || idAnioSel == '')
	{
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios ", "20", null);
		return false;
	}
	
    var serializedForm = jQuery(document.repRecibosProvForm).serialize();
	
	sendRequestJQ(null, validarExistenciaDatosPath + "?"+ serializedForm ,targetWorkArea, 'generarReporte(' + idMesSel + ',' + idAnioSel + ');');	
	
}

function generarReporte(idMesSel, idAnioSel)
{
	
	var existenDatos = dwr.util.getValue('existenDatos');	
	
	if(existenDatos == 'false')
	{
	    return false;
	    
	}else
	{
		var serializedForm = jQuery(document.repRecibosProvForm).serialize();			
		
		window.open(generarReportePath + "?"+ serializedForm + '&mes='+idMesSel +'&anio=' + idAnioSel,"Documento");		
	}	
}

function generarDatos()
{    
	var idMesSel = dwr.util.getValue("meses");
	var idAnioSel = dwr.util.getValue("anios");
	
	if(idMesSel == '' || idAnioSel == '')
	{
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios ", "20", null);
		return false;
	}
	
    var serializedForm = jQuery(document.repRecibosProvForm).serialize();
	
	sendRequestJQ(null, generarDatosPath + "?"+ serializedForm ,targetWorkArea, null);	
}