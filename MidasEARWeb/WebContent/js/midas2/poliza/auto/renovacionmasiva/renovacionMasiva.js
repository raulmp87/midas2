/**
 * 
 */
var renovacionesGrid;
var ordenRenovacionGrid;
var ordenRenovacionDetalleGrid;
var paginadoParamsRenPath;

function initRenovacion(){
	renovacionesGrid = new dhtmlXGridObject("polizaGrid");
	mostrarIndicadorCarga('indicador');	
	renovacionesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');		
    });	
	var pathBuscar = "?filtrar=false";
	if(jQuery("#numeroPoliza").val() != null && jQuery("#numeroPoliza").val() != undefined && jQuery("#numeroPoliza").val() != ""){
		pathBuscar =  "?filtrar=true&"+ jQuery(document.renovacionMasivaForm).serialize();
	}
	renovacionesGrid.load("/MidasWeb/poliza/renovacionmasiva/listarFiltrado.action" + pathBuscar);
	
	if (agenteControlDeshabilitado) {
		jQuery("#idAgenteClavePol").attr("readonly", true);
	}
	
	if (promotoriaControlDeshabilitado) {
		jQuery("#promotoriaId").attr("readonly", true);		
	}
}

function initOrdenRenovacion(){
	//listarOrdenesRenovacion(false);
	listarOrdenesRenovacionPaginado(1, true, false);
}

function initOrdenRenovacionDetalle(){
	ordenRenovacionDetalleGrid = new dhtmlXGridObject("ordenRenovacionDetalleGrid");
	mostrarIndicadorCarga('indicador');	
	ordenRenovacionDetalleGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	ordenRenovacionDetalleGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	ordenRenovacionDetalleGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	ordenRenovacionDetalleGrid.load("/MidasWeb/poliza/renovacionmasiva/listarOrdenRenovacionDetalle.action?filtrar=false" + "&idToOrdenRenovacion=" + jQuery("#idToOrdenRenovacion").val());	
}

function listarPolizasPaginado(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'listarPolizasPaginado'+'&divGridPaginar='+'polizaGrid';
	if(nuevoFiltro){
		paginadoParamsRenPath = jQuery(document.renovacionMasivaForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'listarPolizasRenovar(1);';
	
	mostrarIndicadorCarga('indicador');	
	sendRequestJQTarifa(null, "/MidasWeb/poliza/renovacionmasiva/buscarPolizasPaginado.action" + "?filtrar=true" + "&" + paginadoParamsRenPath + posPath, 'gridPolizasPaginado', nextFunc);
} 

function listarPolizasRenovar(esBusqueda){
	document.getElementById("polizaGrid").innerHTML = '';
	renovacionesGrid = new dhtmlXGridObject("polizaGrid");
	renovacionesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	renovacionesGrid.load("/MidasWeb/poliza/renovacionmasiva/listarFiltrado.action?filtrar=true" + "&" + paginadoParamsRenPath + "&"+ posPath);
	
	muestraLeyendaPolizaError(false);
	
	if(esBusqueda == 1)//1 = metodo invocado desde boton busqueda
	{		
		displayFiltersRenovacion();		
	}
	else //0 = metodo invocado desde paginacion
	{
		selectGridRenovacionesSize();		
	}	
	
}

function sizeGridRenovacion(){
	jQuery(document).ready(function(){
		jQuery('#polizaGrid').css('height','320px');
		jQuery('div[class=objbox]').css('height','275px');
	})
}

function reSizeGridRenovacion(){
	jQuery(document).ready(function(){
		jQuery('#polizaGrid').css('height','140px');
		jQuery('div[class=objbox]').css('height','100px');
	})
}

function displayFiltersRenovacion(){
	jQuery('#contenedorFiltros').toggle('fast', function() {
			if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
				jQuery('#mostrarFiltros').html('Mostrar Filtros')
				sizeGridRenovacion();
			} else {
				jQuery('#mostrarFiltros').html('Ocultar Filtros')
				reSizeGridRenovacion();
			}
		});
}

function selectGridRenovacionesSize()
{		
	if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
		jQuery('#mostrarFiltros').html('Mostrar Filtros')
		sizeGridCotizacion();
	} else {
		jQuery('#mostrarFiltros').html('Ocultar Filtros')
		reSizeGridCotizacion();
	}
}

function listarPolizasRenovarSinPaginado(){
	
	mostrarIndicadorCarga('indicador');	
	document.getElementById("polizaGrid").innerHTML = '';
	renovacionesGrid = new dhtmlXGridObject("polizaGrid");
	renovacionesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	renovacionesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	
	var posPath = jQuery(document.renovacionMasivaForm).serialize();;
	renovacionesGrid.load("/MidasWeb/poliza/renovacionmasiva/listarFiltrado.action?filtrar=true" + "&" + posPath);
	
	muestraLeyendaPolizaError(false);
}

function listarOrdenesRenovacionPaginado(page, nuevoFiltro, filtrar){
	var posPath = '&posActual='+page+'&funcionPaginar='+'listarOrdenesRenovacionPaginado'+'&divGridPaginar='+'ordenRenovacionGrid';
	if(nuevoFiltro){
		if(filtrar === null || filtrar === undefined || filtrar === true){
			paginadoParamsRenPath = jQuery(document.ordenRenovacionMasivaForm).serialize() +  "&filtrar=true";
		}else{
			paginadoParamsRenPath = "filtrar=false";
		}
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'listarOrdenesRenovacion();';
	
	mostrarIndicadorCarga('indicador');	
	sendRequestJQTarifa(null, "/MidasWeb/poliza/renovacionmasiva/buscarOrdenesRenovacionPaginado.action" + "?" + paginadoParamsRenPath + posPath, 'ordenRenovacionPaginadoGrid', nextFunc);
} 

function listarOrdenesRenovacion(){
	document.getElementById("ordenRenovacionGrid").innerHTML = '';
	ordenRenovacionGrid = new dhtmlXGridObject("ordenRenovacionGrid");
	mostrarIndicadorCarga('indicador');	
	ordenRenovacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	ordenRenovacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	ordenRenovacionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	//var filtros = "";
	//if(filtrar){
	//	filtros = "&"+ jQuery(document.ordenRenovacionMasivaForm).serialize();
	//}
	//ordenRenovacionGrid.load("/MidasWeb/poliza/renovacionmasiva/buscarOrdenesRenovacion.action?filtrar="+ filtrar + filtros);
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	ordenRenovacionGrid.load("/MidasWeb/poliza/renovacionmasiva/buscarOrdenesRenovacion.action?" + paginadoParamsRenPath + "&"+ posPath);
}

function seleccionarPromotoria(){
	parent.mostrarVentanaModal("promotoria","Selecci\u00F3n Promotoria",200,320, 510, 150, seleccionarPromotoriaPath);	
}

function cargarPromotoria(id,nombre){
	jQuery('#idPromotoria').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#descripcionBusquedaPromotoria').text(nombre);
	cerrarVentanaModal("promotoria");
}


function mostrarVentanaAccionRenovacion(){
	var idPoliza = cargaIdPolizas();
	if(idPoliza == ""){
		mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Renovar.");
	}else{
		parent.mostrarVentanaModal("accionRenovacion","Seleccione",200,320, 470, 320, renovarPath);
	}
}

function onChangeAccionRenovacion(value){
	if(value == 3){
		jQuery(".filtrosNegocio").show();
	}else{
		jQuery(".filtrosNegocio").hide();
	}
}

function renovarPolizas(){
	var form = jQuery('#accionRenovacionesForm')[0];
	var idPolizas = parent.cargaIdPolizas();
	creaIdPolizasEnVentana(idPolizas);
	var accionRenovacion = jQuery("input[@name=accionRenovacion]:checked").val();
	var idToNegSeccionPath = "";
	if(accionRenovacion == 3){
		var idToNegSeccion = jQuery("#secciones").val();		
		if(idToNegSeccion == '' || idToNegSeccion == undefined){
			parent.mostrarVentanaMensaje('10', "Seleccione una L\u00EDnea de Negocio.");
			return;
		}
		idToNegSeccionPath = "&idToNegSeccion=" + idToNegSeccion;
	}
	var validarPolizaPath = "/MidasWeb/poliza/renovacionmasiva/renovarPolizas.action";
	parent.redirectVentanaModal("accionRenovacion", validarPolizaPath, form);
}
function creaIdPolizasEnVentana(idPolizas){
	if(idPolizas != null && idPolizas != ""){
		var ids = idPolizas.split(",");
		var num = 0;
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#accionRenovacionesForm').append('<input type="hidden" name="polizaList['+ num+ '].idToPoliza" value="' + ids[i] + '" />');
	   	 	num = num + 1;		
		}
	}
}

function validaResultadoRenovar(){
	sendRequestJQ(null, mostrarOrdenesPath,"contenido", null);
}

function cargaIdPolizas(){
	var idPolizas = "";
	var num = 0;
	var idCheck = renovacionesGrid.getCheckedRows(0);
	if(idCheck != ""){
		return idCheck;
	}
	return idPolizas;
}

function cargaIdPolizasDetalle(){
	var idPolizas = "";
	var num = 0;
	var idCheck = ordenRenovacionDetalleGrid.getCheckedRows(0);
	if(idCheck != ""){
		return idCheck;
	}
	
	return idPolizas;
}

function creaIdPolizasEnVentanaDetalle(idPolizas){
	if(idPolizas != null && idPolizas != ""){
		var ids = idPolizas.split(",");
		var num = 0;
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#ordenRenovacionDetalleForm').append('<input type="hidden" name="polizaList['+ num+ '].idToPoliza" value="' + ids[i] + '" />');
	   	 	num = num + 1;		
		}
	}
}

function cancelarPolizasDetalleOrden(){
	var idPolizas = cargaIdPolizasDetalle();
	if(idPolizas === "" || idPolizas === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Cancelar.");
	}else{
		var form = jQuery('#ordenRenovacionDetalleForm')[0];
		creaIdPolizasEnVentanaDetalle(idPolizas);
		var validarPolizaPath = "/MidasWeb/poliza/renovacionmasiva/cancelarPolizasDetalle.action";
		form.setAttribute("action", "/MidasWeb/poliza/renovacionmasiva/cancelarPolizasDetalle.action");
		parent.redirectVentanaModal("consultaOrdenRenovacion", validarPolizaPath, form);
	}
}

function emitirPolizasDetalleOrden(){
	var idPolizas = cargaIdPolizasDetalle();
	if(idPolizas === "" || idPolizas === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Emitir.");
	}else{
		var form = jQuery('#ordenRenovacionDetalleForm')[0];
		creaIdPolizasEnVentanaDetalle(idPolizas);
		var validarPolizaPath = "/MidasWeb/poliza/renovacionmasiva/emitirPolizasDetalle.action";
		form.setAttribute("action", "/MidasWeb/poliza/renovacionmasiva/emitirPolizasDetalle.action");
		parent.redirectVentanaModal("consultaOrdenRenovacion", validarPolizaPath, form);
	}
}

function resultadoOrdenRenovacion(){
	var resultado = jQuery("#estatusRenovacion").val();
	if(resultado == 1){
		parent.cambiarVentanaOrden(jQuery("#idToOrdenRenovacion").val(), jQuery('#errores').val());
	}
}

function cambiarVentanaOrden(idOrden, errores){
	cerrarVentanaModal("accionRenovacion");
	if(typeof errores !== 'undefined' && errores !== null && errores.trim().length > 0){
		mostrarVentanaMensaje('30', "Se a generado una nueva orden por renovaci\u00F3n masiva con el identificador (" + idOrden + ").");
		sendRequestJQ(null, mostrarOrdenesPath + "?mensaje=" + errores + "&tipoMensaje=10","contenido", null);
	} else {
		mostrarVentanaMensaje('30', "Se a generado una nueva orden por renovaci\u00F3n masiva con el identificador (" + idOrden + ").");
		sendRequestJQ(null, mostrarOrdenesPath,"contenido", null);
	}
	
}

function salir(){
	alert("En construccion");
}

function mostrarVentanaConsultaOrden(idToOrdenRenovacion){
	parent.mostrarVentanaModal("consultaOrdenRenovacion","Consulta de Orden",200,200, 800, 620, consultaOrdenRenovacionPath + "?idToOrdenRenovacion=" + idToOrdenRenovacion, "listarOrdenesRenovacion(true);");
}

function editarCotizacion(idToCotizacion, claveEstatus){
	parent.verDetalleCotizacionOrden(idToCotizacion, claveEstatus);
}

function verCotizacion(idToCotizacion){
	parent.verConsultaCotizacionOrden(idToCotizacion);
}

function verDetalleCotizacionOrden(idToCotizacion, claveEstatus){
	cerrarVentanaModal("consultaOrdenRenovacion");
	if(claveEstatus == 10 || claveEstatus == 11){
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion,'contenido', null);
	}
	if(claveEstatus == 12){
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+idToCotizacion,'contenido', null);
	}
				
}

function verConsultaCotizacionOrden(idToCotizacion){
	cerrarVentanaModal("consultaOrdenRenovacion");
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion+"&soloConsulta=1",'contenido', null);
}

function cancelarOrdenRenovacion(idToOrdenRenovacion){
	if(confirm('\u00BFEst\u00E1 seguro que desea Cancelar la Orden de Renovacion?')){
		sendRequestJQ(null, '/MidasWeb/poliza/renovacionmasiva/cancelarOrdenRenovacion.action?idToOrdenRenovacion='+idToOrdenRenovacion,'contenido', null);
	}
}

function terminarOrdenRenovacion(idToOrdenRenovacion){
	if(confirm('\u00BFEst\u00E1 seguro que desea Terminar la Orden de Renovacion?')){
		sendRequestJQ(null, '/MidasWeb/poliza/renovacionmasiva/terminarOrdenRenovacion.action?idToOrdenRenovacion='+idToOrdenRenovacion,'contenido', null);
	}
}

function exportarDetalle(idToOrdenRenovacion){
	var location ="/MidasWeb/poliza/renovacionmasiva/descargarCotizacionesRenovacion.action?idToOrdenRenovacion=" + idToOrdenRenovacion;
	window.open(location, "Cotizacion_COT" + idToOrdenRenovacion);
}

function exportarXML(idToOrdenRenovacion){
	var location ="/MidasWeb/poliza/renovacionmasiva/descargarXMLRenovacion.action?idToOrdenRenovacion=" + idToOrdenRenovacion;
	window.open(location, "Cotizacion_COT" + idToOrdenRenovacion);
}

function exportarExcelComparacion(idToOrdenRenovacion){
	var location ="/MidasWeb/poliza/renovacionmasiva/descargarExcelRenovacion.action?idToOrdenRenovacion=" + idToOrdenRenovacion;
	window.open(location, "Cotizacion_COT" + idToOrdenRenovacion);
}

function cancelarCotizacionValidacion(){
	parent.cancelaYActualizaCotizacionRenovacion(jQuery("#accionRenovacionesForm"));
}

function cancelaYActualizaCotizacionRenovacion(form){
	//cerrarVentanaModal('accionRenovacion');
	var location ="/MidasWeb/poliza/renovacionmasiva/cancelarCotizacionValidacion.action";
	//sendRequestJQ(form, location, 'contenido', null);
	redirectVentanaModal("accionRenovacion", location, form);
}

function cerrarVentanaCancelacionValidacion(){
	parent.cierraVentanaCancelacionYAbreVentanaRenovacion();
}

function cierraVentanaCancelacionYAbreVentanaRenovacion(){
	cerrarVentanaModal('accionRenovacion');
	mostrarVentanaAccionRenovacion();
}

function limpiarFiltrosRenovacion(tipo){
	jQuery("input").each(function(){
		if(jQuery(this).attr("type") == "text"){
			if (jQuery(this).attr("id") == "idAgenteClavePol" && agenteControlDeshabilitado) {
				return;
			}
			jQuery(this).val('');
		}
	});
	if(tipo == 1 && !agenteControlDeshabilitado){
		cleanInputDiv("agenteNombre");
	}
	jQuery("select").each(function(){
		jQuery(this).val('');
		jQuery(this).change();
	});
}

function cerrarVentanaValidacionRenovacion(){
	var polizas = new Array();
	var count = 0;
	jQuery("#accionRenovacionesForm").find("input[type$='hidden']").each(function(){
		polizas[count] = jQuery(this).val();
		count = count + 1;
	});
	parent.marcaPolizasConError(polizas);	
}

function marcaPolizasConError(polizas){
	cerrarVentanaModal('accionRenovacion');
	for(var i = 0; i < polizas.length; i++){
		renovacionesGrid.setRowColor(polizas[i],"red");
	}
	muestraLeyendaPolizaError(true);
}

function muestraLeyendaPolizaError(mostrar){
	if(mostrar){
		jQuery("#leyendaPolizaErrorDiv").show();
	}else{
		jQuery("#leyendaPolizaErrorDiv").hide();
	}
}

function enviarProveedor(idToOrdenRenovacion){
	if(confirm('\u00BFEst\u00E1 seguro que desea Enviar la Orden de Renovacion al Proveedor?')){
		sendRequestJQ(null, '/MidasWeb/poliza/renovacionmasiva/enviarProveedorOrdenRenovacion.action?idToOrdenRenovacion='+idToOrdenRenovacion,'contenido', null);
	}
}

function obtenerOrdenProveedor(idToOrdenRenovacion){
	var location ="/MidasWeb/poliza/renovacionmasiva/obtenerOrdenRenovacionProveedor.action?idToOrdenRenovacion=" + idToOrdenRenovacion;
	window.open(location, "Cotizacion_COT" + idToOrdenRenovacion);
}

function verDetalleImpresionPoliza(idToPoliza){
	parent.mostrarVentanaModal("impresionPoliza","Impresi&oacute;n",200,320, 710, 350, "/MidasWeb/poliza/ventanaDetalleEndoso.action?id="+idToPoliza+"&accion=imprimir");
}

function cargarExcelNumeroGuias(){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("documentoNumeroGuias", 34, 100, 440, 265);
	adjuntarDocumento.setText("Excel Numero de Guias");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    				parent.mostrarErrores(idToControlArchivo);
    			}
    		} // End of onSuccess    		
    	});
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
        	mostrarMensajeInformativo("Solo puede importar archivos Excel (.xls). ", "20", null, null); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }
    vault.create("vault");
    vault.setFormField("claveTipo", "40");
}

function mostrarErrores(idToControlArchivo){
	new Ajax.Request(procesarCargarExcel, 
		{ 
			method       : "POST",
			asynchronous : true,
			parameters   : {
				'idToControlArchivo' : idToControlArchivo
				},
			onSuccess    : function(data) {
				var errores = data.responseJSON.errores[0];
			    dhxWins.window("documentoNumeroGuias").close();
				if(errores){
					 mostrarMensajeInformativo(errores, '10', null, null);
				}else{
					mostrarMensajeInformativo("Carga Exitosa", '30', null, null);
				    procesarRenovacionesEstatusPendientes();
				}
			}
		}
	);
}

function procesarRenovacionesEstatusPendientes(){
	new Ajax.Request(procesarEstatusPendientes, 
		{ 
			method       : "POST",
			asynchronous : false,
			parameters   : null,
			onSuccess    : function(data) {
				
			}
		}
	);
}

function reporteEfectividad(){
	var param =  "?"+ jQuery("#ordenRenovacionMasivaForm").serialize();
	window.open(reporteEfectividadEntrega+param, "Genera Reporte Efectividad");
}

function procesaTareaAction(){
	new Ajax.Request(procesaTarea, 
			{ 
				method       : "POST",
				asynchronous : false,
				parameters   : null,
				onSuccess    : function(data) {
					
				}
			}
		);
	}

function descargarPlantillaGuias(){
	window.open(descargaPlantilla);
}


function cargarListaOrdenesReno(){
    blockPage();
    var fecha1=dwr.util.getValue("ordenRenovacion.fechaCreacion");
    var fecha2=dwr.util.getValue("ordenRenovacion.fechaCreacionHasta");
    var fecha3 = dwr.util.getValue("ordenRenovacion.fechaModificacion");        
    var fecha4 = dwr.util.getValue("ordenRenovacion.fechaModificacionHasta");
    var num_renovacion=dwr.util.getValue("ordenRenovacion.idToOrdenRenovacion");
    var num_poliza=dwr.util.getValue("ordenRenovacion.numeroPoliza");
    var varText="";
    
    if(validateFiltersOrdenRen(fecha1, fecha2, fecha3, fecha4, num_renovacion, num_poliza)){
    	varText="Se debe seleccionar al menos 1 filtro para la b\u00fasqueda.";
    } else{
    	if(compare_dates_period(fecha1, fecha2)) {
    		varText="El periódo entre la fecha de orden inicial y la fecha de orden final no debe ser mayor a 1 a\u00f1o. Verificar.";
    	} else if(compare_dates_period(fecha3, fecha4)) {
    		varText="El periódo entre la fecha de estatus inicial y la fecha de estatus final no debe ser mayor a 1 a\u00f1o. Verificar.";
    	} else if (compare_dates(fecha1, fecha2)){  
    		varText="La fecha de orden inicial no debe ser mayor a la fecha de orden final. Verificar."; 
        } else if(compare_dates(fecha3, fecha4)) {
        	varText="La fecha de estatus inicial no debe ser mayor a la fecha de estatus final. Verificar.";    
        }else{
	        // Definir fechas por default cuando no se capture filtro restrictivo
	        if(num_renovacion.trim() == '' && num_poliza.trim() == '' && fecha4.trim() == ''){
	            definirFechasDefault("fechaCreacion", "fechaCreacionHasta");
	        }
        }
    }
    if(varText==''){
        listarOrdenesRenovacionPaginado(1, true, true);
    }else{
    	mostrarMensajeInformativo(varText, 10, null);
    }
    unblockPage();
}

function validateFiltersOrdenRen(fecha1, fecha2, fecha3, fecha4, num_renovacion, num_poliza) { 
    var clave_estatus = dwr.util.getValue("ordenRenovacion.claveEstatus");              
    var usuario_creacion = dwr.util.getValue("ordenRenovacion.nombreUsuarioCreacion");
    
    if(num_renovacion.trim() != '' || num_poliza.trim() != '' || fecha1.trim() != '' || fecha2.trim() != '' || clave_estatus.trim() != ''
        || fecha3.trim() != '' || fecha4.trim() != '' || usuario_creacion.trim() != '') 
    {
        return (false);
    } else {
        return (true);
    }
}

function listarRenovacionesMasivas(){
	blockPage();
	var fecha1=dwr.util.getValue("polizaDTO.cotizacionDTO.fechaFinVigencia");
	var fecha2=dwr.util.getValue("polizaDTO.cotizacionDTO.fechaFinVigenciaHasta");
	var numPoliza = dwr.util.getValue("polizaDTO.numeroPoliza");
	var numSerie = dwr.util.getValue("polizaDTO.numeroSerie");
	var numRenovacion = dwr.util.getValue("polizaDTO.numeroRenovacion");
	var negocio = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.negocio.idToNegocio");
	var nom_contratante = dwr.util.getValue("polizaDTO.cotizacionDTO.nombreContratante");
	var varText = "";
	if(validate_filters(fecha1, fecha2, numPoliza, numSerie)){
    	varText = "Se debe seleccionar al menos 1 filtro para la b\u00fasqueda.";
    } else{
    	if (compare_dates(fecha1, fecha2)){
    		varText = "La fecha de vigencia inicial no debe ser mayor a la fecha final. Verificar."; 
	    } else if(compare_dates_period(fecha1, fecha2)) {
	    	varText = "El periódo entre la fecha de vigencia inicial y la fecha final no debe ser mayor a 1 a\u00f1o. Verificar.";
	    } else{
	    	// Definir fechas por default cuando no se capture filtro restrictivo
	    	if(numPoliza.trim() == '' && numSerie.trim() == '' && negocio.trim() !=''){
	    		definirFechasDefault("polizaDTO.cotizacionDTO.fechaFinVigencia", "polizaDTO.cotizacionDTO.fechaFinVigenciaHasta");
	    	}
	    }
    }
	
	if (negocio.trim() === '' && numPoliza.trim() === '' && numSerie.trim() === '' && nom_contratante.trim() === ''){
		var textoNegocio = "El campo de negocio es Obligatorio.";
		if (varText.trim() === ''){
			varText = textoNegocio;
		}else{
			varText = textoNegocio + "<br>Adicional a esto " + varText.toLowerCase();
		}
	}
    
    if(varText==''){
        listarPolizasPaginado(1,true);
    }else{
    	mostrarMensajeInformativo(varText, 10, null);
    }
    unblockPage();
}

function validate_filters(fecha1, fecha2, numPoliza, numSerie) {
	var nom_contratante = dwr.util.getValue("polizaDTO.cotizacionDTO.nombreContratante");
	var fecha_desde = dwr.util.getValue("polizaDTO.cotizacionDTO.fechaFinVigencia");
	var fecha_hasta = dwr.util.getValue("polizaDTO.cotizacionDTO.fechaFinVigenciaHasta");		
	var numRenovacion = dwr.util.getValue("polizaDTO.numeroRenovacion");		
	var producto = dwr.util.getValue("polizaDTO.idToNegProducto");
	var tipo_poliza = dwr.util.getValue("polizaDTO.idToNegTipoPoliza");
	var seccion = dwr.util.getValue("polizaDTO.idToNegSeccion");
	var centro_emisor = dwr.util.getValue("polizaDTO.idCentroEmisor");
	var gerencia = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.ejecutivo.gerencia.id");
	var ejecutivo = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.codigoEjecutivo");
	var promotoria = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.id");
	var agente = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.idAgente");
	
	if(numPoliza.trim() != '' || numSerie.trim() != '' || nom_contratante.trim() != '' || fecha_desde.trim() != '' || fecha_hasta.trim() != '' 
		|| producto.trim() != '' || tipo_poliza.trim() != '' || seccion.trim() != '' || centro_emisor.trim() != '' 
		|| numRenovacion.trim() != '' || gerencia.trim() != '' || ejecutivo.trim() != '' || promotoria.trim() != '' || agente.trim() != '') 
	{
		return (false);
	} else {
		return (true);
	}
}