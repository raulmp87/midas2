
function verGeneracionLayoutRenovacion(){
	limpiarDivsGeneral();
	var path= '/MidasWeb/poliza/renovacionmasiva/mostrarGeneraLayout.action';
	sendRequestJQ(null, path,'contenido_generacionLayoutRenovacion',null);
}

function cargaProductosNegocio(){
	var idNegocio = jQuery('#idToNegocio').val();
	const ACTIVO = 1;
	if(idNegocio){
		listadoService.getMapProductos(idNegocio,ACTIVO,
				function(data){
			addOptions('idToProducto', data);
				});	
	}else{
		addOptions('idToProducto',null);
	}
}

function descargarLayoutRenovacion(){
	if (validarParams()){
		var fechaInicio = jQuery('#fechaCreacion').val();
		var fechaFin = jQuery('#fechaFinal').val();
		var idToNegocio = jQuery('#idToNegocio').val();
		var idToProducto = jQuery('#idToProducto').val();
		var path= '/MidasWeb/poliza/renovacionmasiva/descargarLayout.action?'+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&idToNegocio='+idToNegocio+'&idToProducto='+idToProducto;
		window.open(path, "Layout_Renovacion");
	}	
}

function validarParams(){
	var fechaInicio = jQuery('#fechaCreacion').val();
	if (fechaInicio != null && fechaInicio != ''){
		return true
	}
	else{
		mostrarMensajeInformativo('Favor de ingresar Fecha Inicio. NOTA: Fecha Fin no es obligatorio y su valor deafult es el ultimo dia del mes de Fecha Inicio',"20");
		return false;
	}	
}