var listadoSolicitudGrid;
var ventanaVehiculo;
var paginadoParamsPath;

function muestraDatosAdicionalesVehiculo(){
	console.log("muestraDatosAdicionalesVehiculo()");
	listadoService.obtieneNumeroDeRiesgos(dwr.util.getValue("incisoCotizacion.id.idToCotizacion"),dwr.util.getValue("incisoCotizacion.id.numeroInciso"),
			function(data){
				if(data == 0){
					jQuery('#btnDatosAdicionalesPaquete').css('display', 'block');
				}
	});
}
function pageGridPaginado(page, nuevoFiltro) {
	console.log("pageGridPaginado(" + page + "," + nuevoFiltro +")");
	var idToCotizacion = jQuery('#cotizacionId').val();
	var posPath = '&posActual=' + page + '&funcionPaginar='
			+ 'pageGridPaginadoIncisos' + '&divGridPaginar=' + 'listadoIncisos';
	if (nuevoFiltro) {
		paginadoParamsPath = jQuery(document.IncisoForm).serialize();
	} else {
		posPath = '&posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosPath + "?" + paginadoParamsPath
			+ posPath, 'gridListadoDeIncisos', 'mostrarListadoIncisos('
			+ idToCotizacion + ')');
}
function pageGridPaginadoIncisos(page, nuevoFiltro) {
	console.log("pageGridPaginadoIncisos(" + page + "," + nuevoFiltro +")");
	document.getElementById("contenido_emision").innerHTML = '';
	var idToCotizacion = jQuery('#cotizacionId').val();	
	var mostrarCancelados = jQuery('#mostrarCancelados').val();
	var posPath = 'posActual=' + page + '&funcionPaginar='
		+ 'pageGridPaginadoIncisos' + '&divGridPaginar=' + 'listadoIncisos'
		+ '&cotizacionId=' + idToCotizacion;
	
	posPath+= fillPath() + '&'	+ jQuery(document.incisosFiltroForm).serialize();
	
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize() 
	}
	
	sendRequestJQTarifa(null, "/MidasWeb/poliza/inciso/busquedaPaginada.action" + "?" + posPath,
				'gridListadoDeIncisos', 'mostrarListadoIncisos('+idToCotizacion+')');
	
	
}
function mostrarListadoIncisos(idToCotizacion) {
	document.getElementById("listadoIncisos").innerHTML = '';
	listadoSolicitudGrid = new dhtmlXGridObject('listadoIncisos');
	

	mostrarIndicadorCarga('indicador');
	listadoSolicitudGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		var count=listadoSolicitudGrid.getRowsNum();	
		var losVin = "";
		for(var i=0;i<count;i++){
			losVin += listadoSolicitudGrid.cells(i,5).getValue() + "|"	
		}
		jQuery("#noSerieVin").val(losVin.substring(0, losVin.length - 1));
		consultarCantidadAlertas(jQuery("#noSerieVin").val());
		var respuesta = consultarCantidadAlertas(jQuery("#noSerieVin").val());
		if(respuesta=="si"){
			jQuery('span:contains("Acciones Alertas SAP-AMIS")').css('color', 'red');
		}
	});
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() + '&'
																+ jQuery(document.incisosFiltroForm).serialize();
	
	var path = "/MidasWeb/poliza/inciso/buscarIncisos.action" + "?cotizacionId=" + idToCotizacion;
	
	path+= fillPath();
	listadoSolicitudGrid.load(path + "&" + posPath);
	
	
	
}

function pageGridPaginadoIncisosCancelados(page, nuevoFiltro) {
	console.log("pageGridPaginadoIncisosCancelados(" + page + "," + nuevoFiltro +")");
	var idToCotizacion = jQuery('#cotizacionId').val();	

	var posPath = 'posActual=' + page + '&funcionPaginar='
		+ 'pageGridPaginadoIncisosCancelados' + '&divGridPaginar=' + 'listadoIncisos'
		+ '&cotizacionId=' + idToCotizacion + '&mostrarCancelados=1';	

	posPath+= fillPath() + '&'	+ jQuery(document.incisosFiltroForm).serialize();
	
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize()
				;
	}
		
	sendRequestJQTarifa(null, "/MidasWeb/poliza/inciso/busquedaPaginadaCancelados.action" + "?" + posPath,
				'gridListadoDeIncisos', 'mostrarListadoIncisosCancelados('+idToCotizacion+')');
	
}

function mostrarListadoIncisosCancelados(idToCotizacion) {
	console.log("mostrarListadoIncisosCancelados(" + idToCotizacion + ")");
	document.getElementById("listadoIncisos").innerHTML = '';
	listadoSolicitudCanceladosGrid = new dhtmlXGridObject('listadoIncisos');	

	mostrarIndicadorCarga('indicador');
	listadoSolicitudCanceladosGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() + '&'
																+ jQuery(document.incisosFiltroForm).serialize();
	
	var path = "/MidasWeb/poliza/inciso/buscarIncisosCancelados.action" + "?cotizacionId=" + idToCotizacion + "&mostrarCancelados=1";	
	path+= fillPath();

	listadoSolicitudCanceladosGrid.load(path + "&" + posPath);
}

function obtenerIncisos(){
	console.log("obtenerIncisos()");
	var path = '/MidasWeb/vehiculo/inciso/listarIncisos.action';

		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoSolicitudGrid = new dhtmlXGridObject('listadoIncisos');
		mostrarIndicadorCarga('indicador');	
		listadoSolicitudGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
		listadoSolicitudGrid.load(path + "?" + jQuery(document.IncisoForm).serialize());
}

function obtenerCoberturaCotizaciones() {
	console.log("obtenerCoberturaCotizaciones()");
	var incisoId = jQuery('#incisoId').val();
	
	document.getElementById("coberturaCotizacionGrid").innerHTML = '';
	jQuery('#cargando').css('display','block');
	
	var path = "/MidasWeb/poliza/inciso/mostrarCorberturaCotizacion.action?incisoId=" + incisoId
	path+= fillPath();
	
	sendRequestJQTarifa(null, path , "coberturaCotizacionGrid", null);

	jQuery('#cargando').css('display','none');
}

function obtenerResumenTotalesGrid(){
	console.log("obtenerResumenTotalesGrid()");
	document.getElementById("resumenTotalesGrid").innerHTML = '';
	sendRequestJQ(null,"/MidasWeb/poliza/mostrarResumenTotales.action?"+jQuery(document.polizaForm).serialize(), "resumenTotalesGrid", null);
}


function CantidadRegistrosGrid(){
	console.log("CantidadRegistrosGrid()");
}

function mostrarDatosVehiculo(id){
	console.log("mostrarDatosVehiculo(" + id + ")");
	var url = '/MidasWeb/poliza/inciso/verDetalleInciso.action';
	mostrarVentanaModal("Datos Vehiculo", 'Datos Vehiculo', 50, 50, 800, 600, url);
}

function mostrarVentanaInciso(id){
	console.log("mostrarVentanaInciso(" + id + ")");
	var url = '/MidasWeb/poliza/inciso/verDetalleInciso.action' + "?incisoId=" + id
	url+= fillPath();
	mostrarVentanaModal("Datos Vehiculo", 'Datos Vehiculo', 50, 50, 800, 600, url);;
}

function mostrarCaractVehiculo(){
	console.log("mostrarCaractVehiculo()");
	var url = '/MidasWeb/poliza/inciso/verCaractVehiculo.action';
	parent.mostrarVentanaModal("Caract Vehiculo", 'Otras caracteristicas del Vehiculo', 50, 50, 600, 400, url);
}


function mostrarVentanaAsegurado(incisoContinuityId) {
	console.log("mostrarCaractVehiculo(" + incisoContinuityId + ")");
	var validoEn = jQuery('#validoEn').val();
	var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado/mostrarVentanaAsegurado.action"
		+ "?incisoContinuityId=" + incisoContinuityId + "&validoEn=" + validoEn;;
	mostrarVentanaModal('ventanaAsegurado', 'Complementar Datos Asegurado', 200, 400, 450, 240, url, null );	
}

function mostrarVentanaAseguradoConsulta(incisoContinuityId) {
	console.log("mostrarVentanaAseguradoConsulta(" + incisoContinuityId + ")");
	var url = '/MidasWeb/poliza/inciso/verDetalleAsegurado.action' + "?incisoId=" + incisoContinuityId;
	url+= fillPath();
	mostrarVentanaModal("datosAsegurado", 'Datos Asegurado', 50, 50, 400, 150, url);;
}


function remplazarCaracteresNumSerie(obj){
	console.log("remplazarCaracteresNumSerie(" + obj + ")");
	var pattN = /[�]/gi;
	var valN = 'N';
	var pattOQU = /[oq]/gi;
	var valOQU = '0';
	var pattI = /[i]/gi;
	var valI = '1';	
	var space = /[ ]/gi;
	var empty = '';
	var str = obj.value;
	str = str.replace(pattN, valN);
	str = str.replace(pattOQU, valOQU);
	str = str.replace(pattI, valI);
	str = str.replace(space, empty);
	obj.value = str.toUpperCase();
}

function mostrarControles() {
	console.log("mostrarControles()");
	var mostrarConductor = jQuery('#mostrarSoloConductor').val();
	
	if (mostrarConductor == 'false') {
		var accionEndoso = jQuery('#accionEndoso').val();
		var incisoId = jQuery('#incisoId').val();
		var validoEn = jQuery('#validoEn').val();
		var cotizacionId = jQuery('#cotizacionId').val();
		
	

		var path = null;
		if (accionEndoso = "4") {
			path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso/cargaControles.action?";
		} else {
			path = "/MidasWeb/poliza/inciso/cargaControlDinamicoRiesgo.action?";
		}
		
		path+= "incisoId=" + incisoId + "&cotizacionId=" + cotizacionId + fillPath();
		document.getElementById("controlesGrid").innerHTML = '';
		sendRequestJQ(null, path, "controlesGrid", null);
	}

}

function fillPath() {
	console.log("fillPath()");
	var path = "";
	var validoEn = jQuery('#validoEn').val();
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	var claveTipoEndoso = jQuery('#claveTipoEndoso').val();
	
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" + validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != "") {
		path += "&claveTipoEndoso=" + claveTipoEndoso;
	}
	return path;
}


function validaCiente(){
	console.log("validaCiente()");
	if(jQuery("#idCliente").val() > 0){
		jQuery("input[name$='copiarDatos']").removeAttr("disabled");
	}else{
		jQuery("input[name$='copiarDatos']").attr("disabled",true);		
	}
	if(jQuery("input[name$='copiarDatos']:checked").val() == 2){
		jQuery("#nombre").attr("disabled",true);
		jQuery("#paterno").attr("disabled",true);
		jQuery("#materno").attr("disabled",true);
	}
}

function copiarDatosUsuario(sel) {
	
	console.log("copiarDatosUsuario(" + sel +")");
	
	if (claveTipoPersona == 2 && sel == 2) {
		jQuery("#copiarDatos1").attr('checked', true);
		parent.mostrarMensajeInformativo("No se puede copiar una persona moral", "10",null, null);
		return false;
	}
	if (sel == 1) {
		jQuery("#nombre").removeAttr("disabled");
		jQuery("#paterno").removeAttr("disabled");
		jQuery("#materno").removeAttr("disabled");
	} else {		
		parent.recargarVentanaVehiculoListadoIncisos(jQuery("#incisoId").val(), jQuery("#validoEn").val(),jQuery("#cotizacionId").val(), sel, jQuery("#claveTipoEndoso").val());
	}
}

function limpiarFiltrosIncisos() {
	console.log("limpiarFiltrosIncisos()");
	if (jQuery('#incisosFiltroForm') != null && jQuery('#incisosFiltroForm').length > 0){
		jQuery('#incisosFiltroForm').each (function(){
			  this.reset();
		});		
	}	
	
	if (jQuery('#incisosFiltroEmisionForm') != null && jQuery('#incisosFiltroEmisionForm').length > 0){
		
		jQuery('#incisosFiltroEmisionForm').each (function(){
			  this.reset();
		});			  
	}	
}
