var listadoSolicitudEmisionGrid;
var ventanaVehiculo;
var cargaDatosCliente = 1;
var paginadoParamsPath;


function pageGridPaginadoIncisosEmision(page, nuevoFiltro) {
	document.getElementById("contenido_detallePoliza").innerHTML = '';
	var idToCotizacion = jQuery('#cotizacionId').val();
	var posPath = 'posActual=' + page + '&funcionPaginar='
			+ 'pageGridPaginadoIncisosEmision' + '&divGridPaginar=' + 'listadoIncisosEmision'
			+ '&cotizacionId=' + idToCotizacion;
	
	posPath+= fillPath() + '&'	+ jQuery(document.incisosFiltroEmisionForm).serialize();
	
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, "/MidasWeb/poliza/emision/busquedaPaginada.action" + "?" + posPath,
			'emision_gridListadoDeIncisos', 'mostrarListadoIncisosEmision('+idToCotizacion+')');
}

function mostrarListadoIncisosEmision(idToCotizacion) {

	var validoEn = jQuery('#validoEn').val();
	document.getElementById("listadoIncisosEmision").innerHTML = '';
	listadoSolicitudEmisionGrid = new dhtmlXGridObject('listadoIncisosEmision');
	mostrarIndicadorCarga('indicador');
	listadoSolicitudEmisionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize()
														+ '&'	+ jQuery(document.incisosFiltroEmisionForm).serialize();
	
	var path = "/MidasWeb/poliza/emision/buscarIncisos.action" + "?cotizacionId=" + idToCotizacion;
	
	path+= fillPath();

	listadoSolicitudEmisionGrid.load(path + "&" + posPath);
}

function obtenerIncisos(){
	var path = '/MidasWeb/vehiculo/inciso/listarIncisos.action';

		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoSolicitudEmisionGrid = new dhtmlXGridObject('emision_listadoIncisos');
		mostrarIndicadorCarga('indicador');	
		listadoSolicitudEmisionGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
		listadoSolicitudEmisionGrid.load(path + "?" + jQuery(document.IncisoForm).serialize());
}

function obtenerResumenTotalesGrid(){
	document.getElementById("resumenTotalesGrid").innerHTML = '';
	sendRequestJQ(null,"/MidasWeb/poliza/mostrarResumenTotales.action?"+jQuery(document.polizaForm).serialize(), "resumenTotalesGrid", null);
}


function mostrarVentanaVehiculo(incisoId) {
	var cotizacionId = jQuery('#cotizacionId').val();
	var url =  "/MidasWeb/poliza/emision/verDatosVehiculo.action" + "?incisoId=" + incisoId;
	url =  "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso/mostrar.action" 
		+ "?incisoId=" + incisoId + "&accionEndoso=3" + "&cotizacionId=" + cotizacionId;
	
	url+= fillPath();
	
	mostrarVentanaModal('ventanaVehiculo', 'Datos Complementarios Veh\u00edculo', 50, 50, 800, 450, url, null );
}

function mostrarVentanaAsegurado(numeroInciso,idToCotizacion){
	var url = "/MidasWeb/vehiculo/inciso/asegurado.mostrarVentanaAsegurado.action?incisoCotizacion.id.idToCotizacion="+idToCotizacion+"&incisoCotizacion.id.numeroInciso="+numeroInciso;
	mostrarVentanaModal('ventanaAsegurado', 'Complementar Datos Asegurado', 200, 400, 450, 240, url, null );	
}


function regresarListadoPoliza() {
	var url = "/MidasWeb/poliza/listar.action?esRetorno=1";
	var nextFunction = "compareDates();";
	var tipoRegreso = jQuery("#tipoRegreso").val();
	if(tipoRegreso == 1){
		url = '/MidasWeb/poliza/renovacionmasiva/listar.action?polizaDTO.numeroPoliza='+jQuery("#numeroPoliza").val()+"&esRetorno=0";
		nextFunction = null;
	}
	sendRequestJQ(null, url, targetWorkArea, nextFunction);
}

function fillPath() {
	var path = "";
	var validoEn = jQuery('#validoEn').val();
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	var claveTipoEndoso = jQuery('#claveTipoEndoso').val();
	
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" + validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	
	if (claveTipoEndoso != null && claveTipoEndoso != "") {
		path += "&claveTipoEndoso=" + claveTipoEndoso;
	}
	return path;
}


function ventanaCorreo(idToPoliza, claveTipoEndoso){
	var validoEn = jQuery('#validoEn').val();
	var validoEnMillis = jQuery('#validoEnMillis').val();
	var recordFrom = jQuery('#recordFrom').val();
	var recordFromMillis = jQuery('#recordFromMillis').val();
	
	var path = "";
	if (validoEn != null && validoEn != "") {
		path+= "&validoEn=" +validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		path += "&validoEnMillis=" + validoEnMillis;
	}
	
	if (recordFrom != null && recordFrom != "") {
		path += "&recordFrom=" + recordFrom;
	}
	
	if (recordFromMillis != null && recordFromMillis != "") {
		path += "&recordFromMillis=" + recordFromMillis;
	}
	var url = '/MidasWeb/poliza/ventanaEmails.action?idToPoliza='+idToPoliza+"&claveTipoEndoso="+claveTipoEndoso+path;
	mostrarVentanaModal("correo", 'Capturar Datos Correo', 100, 300, 500, 220, url);	
}




