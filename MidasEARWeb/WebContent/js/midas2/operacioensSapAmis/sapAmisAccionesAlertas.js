var clientesGrid;
var subGrids = [];
var urlConsulta = "/MidasWeb/sapAmis/emision/gridBitacoraEmision.action";

var recienAgregado = false;

function desplegarAccionesAlertasSAPAMIS() {
	limpiarDivsGeneral();
	//console.log("desplegarAccionesAlertasSAPAMIS");
	var path='/MidasWeb/sapAmis/emision/mostrarContenidoAccionesAlertas.action';
	sendRequestJQ(null, path, 'accionesAlertasSAP', null);
}
function initConsultaLineaGrid() {
	var urlConsulta = "/MidasWeb/sapAmis/emision/mostrarGridAccionesAlertas.action";
	clientesGrid = new dhtmlXGridObject("alertasGrid");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	clientesGrid.init();
}
function consultarAlertasLinea(){
	//console.log("consultarAlertasLinea");
	var consultaNuemeroSerie = jQuery('#noSerieVin').val();
	//console.log(consultaNuemeroSerie);
	if(consultaNuemeroSerie != ""){
		var urlConsulta = "/MidasWeb/sapAmis/emision/llenarGridAccionesAlertas.action";
		urlConsulta = urlConsulta + '?consultaNuemeroSerie=' + consultaNuemeroSerie;
		urlConsulta = urlConsulta + '&nuevoCotizador=0';
		urlConsulta = urlConsulta + '&idCotizacion=' + jQuery('#idToCotizacion').val();	
		console.log(urlConsulta);	
		clientesGrid = new dhtmlXGridObject("alertasGrid");
		mostrarIndicadorCarga('indicador');	
		clientesGrid.attachEvent("onXLE", function(grid){
			var xml = [];
			var i = 0;
			clientesGrid.forEachRow(function(id){
				xml[i] = clientesGrid.cells(id,3).getValue().replace("encabezado>","head><beforeInit><call command='enableMultiline'><param>true</param></call></beforeInit>").replace("encabezado","head");
				console.log(xml[i]);
				clientesGrid.cells(id,3).setValue("<div id='row" + id + "' style='margin-top:15px'></div>");
				i++;
			});
			clientesGrid.setColTypes("ro,ro,ro");
			for(var x=0; x<xml.length; x++){
				subGrids[x] = new dhtmlXGridObject("row" + (x+1));
				subGrids[x].attachEvent("onXLE", function(grid){	
					document.getElementById("row" + (x+1)).style.height = "auto";
					jQuery("#row" + (x+1) +" > .xhdr").css("height","0px");
					jQuery("#row" + (x+1) +" > .objbox").css("height","auto");
					jQuery("#row" + (x+1) +" > .objbox").css("background","transparent");
					var id = subGrids[x].cells("nueva",0).getValue();
					subGrids[x].cells("nueva",0).setValue("<input id='" + id + "' type='text' style='width:100%' placeholder='(Nueva)' />");
					
					
					subGrids[x].forEachRow(function(id) {
						if(id != "nueva") {
							var str = subGrids[x].cells(id,0).getValue();
							subGrids[x].cells(id,0).setValue("<div class='subgridAlertas'>" + str + "</div>");
						}
					});
					
				});
				subGrids[x].init();
				subGrids[x].enableColSpan(true);
				subGrids[x].parse(xml[x]);
				
			}
			ocultarIndicadorCarga('indicador');
	    });
		clientesGrid.init();
		clientesGrid.load(urlConsulta);
	}
}

function pintarDeRojo(){
    jQuery('span:contains("Acciones Alertas SAP-AMIS")').css("color","#FF0000");
}

function consultarCantidadAlertas(consultaNuemeroSerie){
	return obtenerAlertas(jQuery('#idToCotizacion').val(),consultaNuemeroSerie);
}

function obtenerAlertas(idToCotizacion, consultaNuemeroSerie){
	
	var urlConsulta='/MidasWeb/sapAmis/emision/consultarSiHayAlertas.action';
	urlConsulta = urlConsulta + '?consultaNuemeroSerie=' + consultaNuemeroSerie;
	urlConsulta = urlConsulta + '&idCotizacion=' + idToCotizacion;	
	urlConsulta = urlConsulta + '&nuevoCotizador=0';
	console.log(urlConsulta);
	sendRequestJQTarifa(null, urlConsulta, 'conenedorRespuestaAccioneSapAmis', null);
	var respuesta = jQuery("#conenedorRespuestaAccioneSapAmis").html().replace(/(?:\r\n|\r|\n)/g, '');
	jQuery("#conenedorRespuestaAccioneSapAmis").html("");
	console.log(respuesta);
	return respuesta;
}
function agregarAccion(id, position){
	if(document.getElementById('nuevaAccion'+id).value != "") {
		var urlConsulta='/MidasWeb/sapAmis/emision/agregarNuevaAccion.action';
		urlConsulta = urlConsulta + '?idRelacionSapAmis=' + id;
		urlConsulta = urlConsulta + '&descripcionAccionSapAmis=' + document.getElementById('nuevaAccion'+id).value;
		
		//console.log(urlConsulta);
		
		sendRequestJQTarifa(null, urlConsulta, 'conenedorRespuestaAccioneSapAmis', null);
		var idAccion = jQuery("#conenedorRespuestaAccioneSapAmis").html().replace(/(?:\r\n|\r|\n)/g, '');
		var newId = id+"|@|"+(position -1)	+"|@|"+document.getElementById('nuevaAccion'+id).value+"|@|"+idAccion;
		var data = [
			"<div class='subgridAlertas'>" + document.getElementById('nuevaAccion'+id).value + "</div>"
		];
		recienAgregado = true;
		subGrids[position - 1].addRow(newId,data,(subGrids[position - 1].getRowIndex("nueva")));
		subGrids[position - 1].setColspan(newId, 0, 2);
		recienAgregado =  false;
		document.getElementById('nuevaAccion'+id).value = '';
	} else {
		alert("Capture una accion.")
	}
}

function iniciarlizarPestania(){
	//console.log("1.-entra a la funcion datosVehiculoInputs");
	var noSerie = jQuery("#numSerieVin").html();
	//console.log("2.-noSerie="+noSerie);
	if(noSerie != null && noSerie != ""){
		//console.log("3.-entra al if="+noSerie);
		jQuery("#noSerieVin").val(noSerie);
		//console.log("4.-setea el valor="+jQuery("#noSerieVin").val());
	}
	//console.log("5.-getea el valor="+jQuery("#noSerieVin").val());
	if(jQuery("#noSerieVin").val() != null && jQuery("#noSerieVin").val() != ""){
		//console.log("6.-consultar alertas");
		var respuesta = consultarCantidadAlertas(jQuery("#noSerieVin").val());
		//console.log("7.-Pintar de Rojo?=" +  respuesta);
		if(respuesta == "si"){
			//console.log("8. antes de pintar de rjo");
			pintarDeRojo();
			//console.log("9. despues de pintar de rojo");
		}
		//console.log("10.-finish");
	}
}


function validaCP(idEstado){
	var idEstado=dwr.util.getValue(jQuery(idEstado).selector);
    var idCodigo=$('#idCodigo').val();
	if (idEstado =="" ){
		alert("No se ha seleccionado un estado");
		idCodigo=$('#idCodigo').val("");
	}else{
	    $.ajax({
	        type: "POST",
	        url:"/MidasWeb/componente/vehiculo/validaCodigoPostal.do",
	        async: true,
	        data:
	            {
	        	idEstado:     idEstado,
	        	codigoPostal: idCodigo
	            },
	        dataType: "text",
	        success: function (response){
	        	if ( response ==0 )
	        		{
	        		alert("El C\u00F3digo no existe en los rangos establecidos");
	        		idCodigo=$('#idCodigo').val("");
	        	}
	        }           
	      });
		
	}
		

}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;  

   return true;
}




