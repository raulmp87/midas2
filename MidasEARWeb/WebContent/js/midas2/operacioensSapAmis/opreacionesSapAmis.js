/**
 * Funciones del modulode SAP Sistema Alertas y Prvencion
 */

// Acciones de redireccion de los Tabs de Emision
var gridListadoLog ;

function getGridLogEjecucion()
{
   document.getElementById("logListadoGrid").innerHTML = '';
   gridListadoLog = new dhtmlXGridObject('logListadoGrid');
   var posPath = '/MidasWeb/sapAmis/emision/llenaListadoLog.action';
   

   gridListadoLog.loadXML(posPath,function(){ });

   ///gridListadoLog.setColSorting("int,na,na,na,na,na,na,na,na,na,na");
   //gridListadoLog.enablePaging(true,15,5,"pagingArea_log",true,"infoArea");
   gridListadoLog.init();
   
   //gridListadoLog.sortRows(0,"int","des");

 
}

function desplegarEjecucionesManuales() {
	
	var path = "/MidasWeb/sapAmis/emision/mostrarEjecucionesManuales.action";
	sendRequestJQ(null, path, 'contenido_ejecucionesManuales', null);
    }          	
	
function ejecucionAmisManual(fechaInicioEjecucionManual, fechaFinEjecucionManual, modulo) {
		var path = "/MidasWeb/sapAmis/emision/ejecucionAmisManual.action" +'?fechaInicioEjecucionManual='+fechaInicioEjecucionManual + '&fechaFinEjecucionManual='+fechaFinEjecucionManual + '&modulo='+modulo;
	//	alert(fechaInicioEjecucionManual);
		//	alert(fechaFinEjecucionManual);
		//	alert(modulo);
		
		
        if (modulo==2)
        	{
        	  alert("Debe Seleccionar el módulo a Ejecutar");
        	  return;
        	}
        else
        	{
        	
        	
        	if (fechaInicioEjecucionManual=="")
        	{
        	  alert("Debe Seleccionar la Fecha Inicial");
        	  return;
        	}
        	else
        		{
        		
        		
        		if (fechaFinEjecucionManual=="")
            	{
            	  alert("Debe Seleccionar la Fecha Fin");
            	  return;
            	}
            	else
            		{
        		alert("Proceso lanzado para su Ejecución.");
               	sendRequestJQasyncFalse(null, path, 'contenido_ejecucionesManuales', null);
            		}
            		}

        	}

	}
	


function ejecucionWebService(idsession){
	if (confirm("Se enviará información a SAP, puede demorar unos minutos. ¿Desea Continuar?")) {
		var path = "/MidasWeb/sapAmis/emision/ejecucionAmisManualWS.action" +'?idsession='+idsession;
	 	sendRequestJQasyncFalse(null, path, 'contenido_ejecucionesManuales', null);	
	 	alert("Proceso enviado. Favor de monitorear actualizando la lista.")
	}	
}

function desplegarBitacoraEmision() {
	
	var path = "/MidasWeb/sapAmis/emision/mostrarBitacoraEmision.action";
	sendRequestJQ(null, path, 'contenido_bitacoraEmision', null);

}



function desplegarBitacoraEmision() {
	
	var path = "/MidasWeb/sapAmis/emision/mostrarBitacoraEmision.action";
	sendRequestJQ(null, path, 'contenido_bitacoraEmision', null);

}

function desplegarBitacoraSiniestros() {
	
	
	var path = "/MidasWeb/sapAmis/emision/mostrarBitacoraSiniestros.action";
	sendRequestJQ(null, path, 'contenido_bitacoraSiniestros', null);
	
}

function desplegarConsultaAlertaEmision() {
	
	var path = "/MidasWeb/sapAmis/emision/mostrarConsultaAlertasEmision.action";
	sendRequestJQ(null, path, 'contenido_consAlertasEmision', null);
	

}

function desplegarDetalleAlertaEmision() {
	
	var path = "/MidasWeb/sapAmis/emision/mostrarDetalleAlertasEmision.action";
	sendRequestJQ(null, path, 'contenido_detalleAlertas', null);
	

}

// Funcion para inicializar el grid de la Bitacora de Emision
var clientesGrid;
var urlConsulta = "/MidasWeb/sapAmis/emision/gridBitacoraEmision.action" ;

function initBitacoraEmisionClientesGrid() {

	
	
	clientesGrid = new dhtmlXGridObject("emisionGrid");
	
	clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	clientesGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	clientesGrid.init();
	clientesGrid.load(urlConsulta);
	
}

function initBitacoraEmisionSiniestrosGrid() {
	
	var siniestroGrid;
	var urlConsulta = "/MidasWeb/sapAmis/emision/gridBitacoraSiniestros.action" ;
	
	siniestroGrid = new dhtmlXGridObject("siniestrosGrid");
	
	siniestroGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	siniestroGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	siniestroGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	siniestroGrid.init();
	siniestroGrid.load(urlConsulta);
	
}


function filtrarBitacoraEmision(poliza, vin, fechaEnvio, estatus) 
{	
	
	var cadena = '';
	var checkedValue = new Array(); 
	var inputElements = document.getElementsByTagName('input');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].className==="sisAlertas" && 
	         inputElements[i].checked){
	          cadena = cadena + inputElements[i].value + '=' + inputElements[i].value + '&';
	      }
	}
	var parametros = '?';
	
	if(poliza != ''){
		parametros = parametros + 'bitacoraPoliza='+poliza + '&';
	}
	if(vin != ''){
		parametros = parametros +'bitacoraVin='+vin + '&';
	}
	if(fechaEnvio != ''){
		parametros = parametros +'bitacoraFechaEnvio='+fechaEnvio + '&';
	}
	if(estatus != ''){
		parametros = parametros +'estatusEnvio='+estatus + '&';
	}
	parametros = parametros + cadena;
	parametros = parametros.substring(0, parametros.length - 1)

	
	var urlConsulta = "/MidasWeb/sapAmis/emision/gridBitacoraEmisionFiltrado.action" ;
	urlConsulta = urlConsulta + parametros ; 
	
	
	
	clientesGrid = new dhtmlXGridObject("emisionGrid");
	clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	clientesGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	clientesGrid.init();
	clientesGrid.load(urlConsulta);
	
	
}


function filtrarBitacoraSiniestros(poliza, vin, fechaEnvio, estatus) 
{	
	
	
	var cadena = '';
	var checkedValue = new Array(); 
	var inputElements = document.getElementsByTagName('input');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].className==="sisAlertas" && 
	         inputElements[i].checked){
	          cadena = cadena + inputElements[i].value + '=' + inputElements[i].value + '&';
	      }
	}
	var parametros = '?';
	
	if(poliza != ''){
		parametros = parametros + 'bitacoraPoliza='+poliza + '&';
	}
	if(vin != ''){
		parametros = parametros +'bitacoraVin='+vin + '&';
	}
	if(fechaEnvio != ''){
		parametros = parametros +'bitacoraFechaEnvio='+fechaEnvio + '&';
	}
	if(estatus != ''){
		parametros = parametros +'estatusEnvio='+estatus + '&';
	}
	parametros = parametros + cadena;
	parametros = parametros.substring(0, parametros.length - 1)

	
	var urlConsulta = "/MidasWeb/sapAmis/emision/gridBitacoraSiniestrosFiltrado.action" ;
	urlConsulta = urlConsulta + parametros ; 
	

	
	clientesGrid = new dhtmlXGridObject("siniestrosGrid");
	clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	clientesGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	clientesGrid.init();
	clientesGrid.load(urlConsulta);
	
	
}

function cargarDetalleAlertasBietacoraEmision(idEncabezadoAlertas){

	var path = "/MidasWeb/sapAmis/emision/mostrarDetalleAlertasBitacoraEmision.action?idEncabezadoAlertas=" + idEncabezadoAlertas;
	sendRequestJQ(null, path, 'contenido_bitacoraEmision', null);

}

function cargarDetalleAlertasBietacoraSiniestros(idEncabezadoAlertas){
	
	var path = "/MidasWeb/sapAmis/emision/mostrarDetalleAlertasBitacoraSiniestros.action?idEncabezadoAlertas=" + idEncabezadoAlertas;
	sendRequestJQ(null, path, 'contenido_bitacoraSiniestros', null);
	
}

function imprimirDetalleAlertasBitacoraEmision(idEncabezadoAlertas){
	
	var url = "/MidasWeb/sapAmis/emision/exportarToPDF.action?idEncabezadoAlertas=" + idEncabezadoAlertas;
	//sendRequestJQ(null, path, 'contenido_bitacoraSiniestros', null);
	blockPage();
	var win = window	
			.open(url, null,
					"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	var timer = setInterval(function() {
		if (win.closed) {
			clearInterval(timer);
			unblockPage();
		}
	}, 1000);
	
}

function imprimirDetalleAlertasEnLinea(consultaNuemeroSerie){
	
	 
	
	var url = "/MidasWeb/sapAmis/emision/exportarDetLineaToPDF.action?consultaNuemeroSerie=" + consultaNuemeroSerie;
	
	blockPage();
	var win = window	
			.open(url, null,
					"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	var timer = setInterval(function() {
		if (win.closed) {
			clearInterval(timer);
			unblockPage();
		}
	}, 1000);
	
}

function consultarAlertasLinea(consultaNuemeroSerie){
	

	var urlConsulta = "/MidasWeb/sapAmis/emision/consultaAlertasLinea.action" ;
	urlConsulta = urlConsulta + '?consultaNuemeroSerie=' + consultaNuemeroSerie ; 
	clientesGrid = new dhtmlXGridObject("alertasGrid");
	clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	clientesGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	clientesGrid.init();
	clientesGrid.load(urlConsulta);

	
}

function initConsultaLineaGrid() {
	var urlConsulta = "/MidasWeb/sapAmis/emision/gridConsultaLinea.action" ;
	clientesGrid = new dhtmlXGridObject("alertasGrid");
	clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	clientesGrid.setSkin("light");
	mostrarIndicadorCarga('indicador');	
	clientesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	clientesGrid.init();
	clientesGrid.load(urlConsulta);
}


function initGridAccionesAlertas(){
	var urlConsulta = "";
}

function consultarDetalleAlertasLinea(consultaNuemeroSerie){
	
	var path = "/MidasWeb/sapAmis/emision/buscaDetAlertLinea.action?consultaNuemeroSerie=" + consultaNuemeroSerie;
	sendRequestJQ(null, path, 'contenido_detalleAlertas', null);
	
}

function exportarExcelBitacoraEmision(poliza, vin, fechaEnvio, estatus){

	
	var cadena = '';
	var checkedValue = new Array(); 
	var inputElements = document.getElementsByTagName('input');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].className==="sisAlertas" && 
	         inputElements[i].checked){
	          cadena = cadena + inputElements[i].value + '=' + inputElements[i].value + '&';
	      }
	}
	

	
	var parametros = '?';
	
	if(poliza != ''){
		parametros = parametros + 'bitacoraPoliza='+poliza + '&';
	}
	if(vin != ''){
		parametros = parametros +'bitacoraVin='+vin + '&';
	}
	if(fechaEnvio != ''){
		parametros = parametros +'bitacoraFechaEnvio='+fechaEnvio + '&';
	}
	if(estatus != ''){
		parametros = parametros +'estatusEnvio='+estatus + '&';
	}
	parametros = parametros + cadena;
	parametros = parametros.substring(0, parametros.length - 1)
	


	
	var urlConsulta = "/MidasWeb/sapAmis/emision/exportarBitacoraEmisionFiltrado.action" ;
	urlConsulta = urlConsulta + parametros ; 
	

	
	blockPage();
	var win = window	
			.open(urlConsulta, null,
					"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	var timer = setInterval(function() {
		if (win.closed) {
			clearInterval(timer);
			unblockPage();
		}
	}, 1000);
	
	
}

function exportarExcelBitacoraSiniestros(poliza, vin, fechaEnvio, estatus){

	
	var cadena = '';
	var checkedValue = new Array(); 
	var inputElements = document.getElementsByTagName('input');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].className==="sisAlertas" && 
	         inputElements[i].checked){
	          cadena = cadena + inputElements[i].value + '=' + inputElements[i].value + '&';
	      }
	}
	

	
	var parametros = '?';
	
	if(poliza != ''){
		parametros = parametros + 'bitacoraPoliza='+poliza + '&';
	}
	if(vin != ''){
		parametros = parametros +'bitacoraVin='+vin + '&';
	}
	if(fechaEnvio != ''){
		parametros = parametros +'bitacoraFechaEnvio='+fechaEnvio + '&';
	}
	if(estatus != ''){
		parametros = parametros +'estatusEnvio='+estatus + '&';
	}
	parametros = parametros + cadena;
	parametros = parametros.substring(0, parametros.length - 1)
	


	
	var urlConsulta = "/MidasWeb/sapAmis/emision/exportarBitacoraSiniestrosFiltrado.action" ;
	urlConsulta = urlConsulta + parametros ; 
	

	
	blockPage();
	var win = window	
			.open(urlConsulta, null,
					"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	var timer = setInterval(function() {
		if (win.closed) {
			clearInterval(timer);
			unblockPage();
		}
	}, 1000);
	
}


function inicializarBotonSapAmis(){
	var iDiv = document.createElement('div');
	iDiv.id = 'botonSapAmis';
	document.getElementById('contenido').appendChild(iDiv);
	document.getElementById('botonSapAmis').innerHTML = "Pantalla SAP-AMIS";
	jQuery("#botonSapAmis").click(function() {
    	window.open("/MidasWeb/sapamis/index.action?modulo=SAPAMIS");
	});
}

function inicializarBotonRepuve(){
	var iDiv = document.createElement('div');
	iDiv.id = 'botonRepuve';
	document.getElementById('contenido').appendChild(iDiv);
	document.getElementById('botonRepuve').innerHTML = "Pantalla REPUVE";
	jQuery("#botonRepuve").click(function() {
    	window.open("/MidasWeb/sapamis/index.action?modulo=REPUVE");
	});
}