/**
 * 
 */
var flagCo = false;
var flagBo = false;
var idAgenteAdd = '';
var chkComision = 0;
var chkBono = 0;
var gridEx;
var accion=0;

jQuery(document).ready(function() {
	jQuery("#divAgregar").hide();
	limpiarFormBuscar();
	limpiarFormAgregar();	
});

/** Funciones Form Buscar Agente */
function agregarAgente() {
	jQuery("#dvExcluir").hide();
	jQuery("#divBuscar").hide();
	jQuery("#divAgregar").show();
	limpiarFormBuscar();
}

function limpiarFormBuscar() {
	jQuery("#idAgenteSear").val("");
	jQuery('input:checkbox').removeAttr('checked');
}

function btnBuscarAgente() {
	var idAgenteSear = jQuery("#idAgenteSear").val();
	var chkComisionSear = document.exluirForm.chkComisionSear.checked;
	var chkBonoSear = document.exluirForm.chkBonoSear.checked;
	var chkTodosSear = document.exluirForm.chkTodosSear.checked;

	if (idAgenteSear != "" || chkComisionSear != "" || chkBonoSear != ""
			|| chkTodosSear != "") {
		var path = "/MidasWeb/exclusionAgentes/agentes/listarFiltrado.action?idAgenteSear="
				+ idAgenteSear
				+ "&chkComisionSear="
				+ chkComisionSear
				+ "&chkBonoSear="
				+ chkBonoSear
				+ "&chkTodosSear="
				+ chkTodosSear;
		listarFiltradoGenerico(path, 'excluirGrid', null, null, null);

	} else {
		
		parent.mostrarMensajeInformativo("Favor de Capturar y/o Seleccionar una opci&oacute;n, \n para realizar una b&uacute;squeda.", "20");

	}

}

/** Funciones Form Agregar Agente */
function buscarAgente() {
	jQuery("#divAgregar").hide();
	jQuery("#divBuscar").show();
	limpiarFormAgregar();
}

function limpiarFormAgregar() {
	
	jQuery("#id").val("");
	jQuery("#idAgenteAdd").val("");
	jQuery('input:checkbox').removeAttr('checked');
}

/** Validacion para guardar agente */

function datosAgente() {
	idAgenteAdd = jQuery("#idAgenteAdd").val();
	id = jQuery("#id").val();
	alertaChecked();
	
		//Validacion al guardar informacion.
	if(idAgenteAdd != ""){
		if(chkComision == true || chkBono == true){
			saveAgente(idAgenteAdd, chkComision, chkBono,id);
		}else{
			
			parent.mostrarMensajeInformativo('Favor de Seleccionar una opcion Comisi&oacute;n / Bono',"20");
		}
	}else{
		
		parent.mostrarMensajeInformativo('Favor de capturar un Agente',"20");
	}
	
}

function alertaChecked() {
	/**
	 * regresa el valor true o false si esta o no seleccionado
	 * document.notificacionM.active.value Valida si tiene valor o no
	 */
	var iActiveCom = document.exluirForm.chkComision.checked
	var iActiveBon = document.exluirForm.chkBono.checked
	if (iActiveCom == true) {
		chkComision = true;
	} else {
		chkComision = false;
	}
	if (iActiveBon == true) {
		chkBono = true;
	} else {
		chkBono = false;
	}
}

function saveAgente(idAgenteAdd, chkComision, chkBono,id) {
	var path = "/MidasWeb/exclusionAgentes/agentes/guardar.action?idAgenteAdd="
			+ idAgenteAdd + "&chkComision=" + chkComision + "&chkBono="
			+ chkBono+"&id="+id;
	sendRequestJQ(null, path+ "&accion=" + accion, targetWorkArea, null);
	accion=0;
}

function verDetalleAgente(id) {
	
	limpiarFormAgregar();
    limpiarFormBuscar();
	var dExcl = grid.cellById(id, 0).getValue();
	var dAgen = grid.cellById(id, 1).getValue();
	var conF = grid.cellById(id, 2).getValue();
	var bon = grid.cellById(id, 3).getValue();
	accion=1;
	agregarAgente();

	jQuery("#dvExcluir").show();
	jQuery("#id").val(dExcl);
	jQuery("#idAgenteAdd").val(dAgen);
	if (conF == "Activo") {
		flagCo = true;
	}
	if (bon == "Activo") {
		flagBo = true;
	}
	CheckAction();
}

function CheckAction() {
	if (flagCo) {
		jQuery('input[name=chkComision]').attr('checked', true);
	}else{
		jQuery('input[name=chkComision]').attr('checked', false);
	}
	if (flagBo) {
		jQuery('input[name=chkBono]').attr('checked', true);
	}else{
		jQuery('input[name=chkBono]').attr('checked', false);
	}
	flagCo=false;
	flagBo=false;
}

function eliminarAgenteExcluir(id) {
	if (confirm("Realmente desea eliminarlo?")) {
		var path = "/MidasWeb/exclusionAgentes/agentes/eliminar.action?id="
				+ id;
		listarFiltradoGenerico(path, 'excluirGrid', null, null, null);

		parent.mostrarMensajeInformativo('Se elimin&oacute; correctamente',"30");
	}

}

document.getElementById('chkTodosSear').onclick = function() {
    // access properties using this keyword
    if ( this.checked ) {
        // if checked ...
        limpiarFormAgregar();
        limpiarFormBuscar();
        jQuery('input[name=chkTodosSear]').attr('checked', true);
        //alert( this.value );
    }
};