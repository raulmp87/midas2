/**
 * 
 */

function obtieneClaveAgente(id, idElemento, seleccion){
	if(id != null && id != ''){
		var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
		var params={"agente.id":id};
		if(seleccion == 2){
			params={"agente.id":id,"agente.tipoSituacion.idRegistro":1302};
		}
		jQuery.asyncPostJSON(url,params,function(data){
			if(data.agente != null){
				jQuery('#' + idElemento).val(data.agente.idAgente);		
			}else{
				jQuery('#' + idElemento).val("");
			}
		});	
	}else{
		jQuery('#' + idElemento).val("");
	}
}