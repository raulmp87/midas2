/**
 * Funciones para manipular combos
 * 
 */
function addOptions(target, map, frame) {
	
	if (target == null) {
		return false;
	}

	if(frame == null){
		target = validateTarget(target);
		dwr.util.removeAllOptions(target);
		size = sizeMap(map);
		if(size == 1){
			dwr.util.addOptions(target, map);
			excuteOnchange(target);
			return true;
		}
		if(size > 1){
			addSelectHeader(target);
			dwr.util.addOptions(target, map);
			return true;
		}	
		dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
	}else{
		frame.contentWindow.dwr.util.removeAllOptions(target);
		size = sizeMap(map);
		if(size == 1){
			frame.contentWindow.dwr.util.addOptions(target, map);
			//excuteOnchange(target);
			return true;
		}
		if(size > 1){
			addSelectHeader(target);
			frame.contentWindow.dwr.util.addOptions(target, map);
			return true;
		}	
		frame.contentWindow.dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
	}
}

/** Metodo para carga un combo y que ponga el seleccione 
	aun si solo tiene una opcion para seleccionar el combo
*/
function addOptionsConSeleccioneSiempre(target, map, frame) {
	if (target == null) {
		return false;
	}

	if(frame == null){
		target = validateTarget(target);
		dwr.util.removeAllOptions(target);
		size = sizeMap(map);
		if(size >= 1){
			addSelectHeader(target);
			dwr.util.addOptions(target, map);
			return true;
		}	
		dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
	}else{
		frame.contentWindow.dwr.util.removeAllOptions(target);
		size = sizeMap(map);
		if(size >= 1){
			addSelectHeader(target);
			frame.contentWindow.dwr.util.addOptions(target, map);
			return true;
		}	
		frame.contentWindow.dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
	}
}
/**
 * Obtiene el objeto html del target en caso
 * de que fuera un id
 * @param target
 * @returns objet html
 */
function validateTarget(target) {
	if (navigator.appName == "Microsoft Internet Explorer") {
		// IE FIX instanceof
		if (!(typeof target == "object")) {
			target = document.getElementById(target);
			return target;
		}
	} else {
		if (!(target instanceof Object)) {
			target = document.getElementById(target);
			return target;
		}
	}
	return target;
}
/**
* Metodo que llena un combo con los valores que estan dentro de un mapa. 
* Se puede poner un primer valor y descripcion (para remplazar el seleccione...) y 
* setear un valor incial si es que es diferente de <code>null</code>
*/
function addOptionsHeaderAndSelect(target, map, value, headerValue, headerDescripcion){
	dwr.util.removeAllOptions(target);
	if (headerDescripcion != null) {
		dwr.util.addOptions(target, [ {
			id : headerValue,
			value : headerDescripcion
		} ], "id", "value");
	}
	dwr.util.addOptions(target, map);

	if (value != null) {
		dwr.util.setValue(target, value);
	}
}


/**
* Reiniciar comobo eliminando todas sus opciones y dejando solo la opcion inicial
*/
function restartCombo(target, headerValue, headerDescription, idStr, valStr, frame){
	if(frame == null){
		dwr.util.removeAllOptions(target);
		dwr.util.addOptions(target, [{id: headerValue, value: headerDescription}], idStr, valStr);
	}else{
		frame.contentWindow.dwr.util.removeAllOptions(target);
		frame.contentWindow.dwr.util.addOptions(target, [{id: headerValue, value: headerDescription}], idStr, valStr);
	}	
}


/**
 * Llena un objeto y selecciona un valor de la lista
 * @param target input a llenar
 * @param map datos a insertar
 * @param value valor contenido en el map, que sera seleccionado
 * @author martin
 */
function addOptionAndSelect(target,map,value, frame){
	if(frame == null){			
		dwr.util.removeAllOptions(jQuery(target).selector);
	    addSelectHeader(target);
	    addOptions(jQuery(target).selector, map);
		dwr.util.setValue(jQuery(target).selector, value);
	}else{
		frame.contentWindow.dwr.util.removeAllOptions(jQuery(target).selector);		
	    addSelectHeader(target, frame);
	    addOptions(jQuery(target).selector, map, frame);
	    frame.contentWindow.dwr.util.setValue(jQuery(target).selector, value);
	}
}
/**
 * Asigna un valor a un objeto html
 * @param target
 * @param value
 * @autor martin
 */
function selectValue(target,value){
	try{
		target = validateTarget(target);
		dwr.util.setValue(target, value);
	}catch(e){
		mostrarMensajeInformativo(e.message, "10", null, null);
	}
}
function addSelectHeader(selectControl, frame) {
	if(frame == null){
		dwr.util.addOptions(selectControl, [{id: headerValue, value:"Seleccione ..." }], 
				"id", "value");
	}else{
		frame.contentWindow.dwr.util.addOptions(selectControl, [{id: headerValue, value:"Seleccione ..." }], 
				"id", "value");
	}	
}


/**
 * Metodo simple para obtener un combo de lisado service por id.
 * 
 * @author Daniel Contreras
 * @param value: valor del combo
 * @param targetId: El combo destino deber� tener un HTML ID 
 * 					para que se encontrado por jquery.
 * @param metodo:es metodo dentro de ListadoServiceDelegate
 */
function cargarComboSimpleDWR(value, targetId, metodo){	
	var targetIdS = targetId;
	targetId = '#' + targetId;
	if(value != null && (value != headerValue && value != '-1')){
		
		var code = "listadoService." + metodo + "('" + value + "', " + 
						"function(data){" +
							"addOptions(jQuery('"+targetId+"')[0],data);" +
						"});";		
		jQuery.globalEval(code);
		excuteOnchange(targetIdS);
	}else{
		removeAllOptionsAndSetHeaderDefault(targetIdS);
		excuteOnchange(targetIdS);
	}
}

function cargarComboSimpleDWR2(value, value2, targetId, metodo){
	var targetIdS = targetId;
	targetId = '#' + targetId;
	removeAllOptionsAndSetHeaderDefault(targetIdS);
	if(value != null && (value != headerValue && value != '-1')){			
		var code = "listadoService." + metodo + "('" + value + "', " + "'" + value2 + "', " + 
						"function(data){" +
							"addOptions(jQuery('"+targetId+"')[0],data);" +
						"});";		
		jQuery.globalEval(code);	
		excuteOnchange(targetIdS);
	}else{
		removeAllOptionsAndSetHeaderDefault(targetIdS);
		excuteOnchange(targetIdS);
	}
}
/**
 * Elimina todos las opciones de un combo 
 * y agrega el seleccione
 * @param target
 * @returns {Boolean}
 * @autor martin
 */
function removeAllOptionsAndSetHeaderDefault(target){
	if (target == null) {
		return false;
	}
	target = validateTarget(target);
	dwr.util.removeAllOptions(target);
	dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
}
/**
 * Agrega mas elementos a un combo cargado
 * @param target
 * @param value
 * @param description
 * @author martin
 */
function plusOption(target, value, description){
	dwr.util.addOptions(target, [{id: value, value: description}], 
			"id", "value");
}

function removeAllOptionsAndSetHeader(target, value, description){
	target = validateTarget(target);
	dwr.util.removeAllOptions(target);
	dwr.util.addOptions(target, [{id: value, value: description}], 
			"id", "value");
}
/**
 * Optioene el size de un mapa
 * 
 * @param map
 * @returns {Number}
 * @author martin
 */
function sizeMap(map){
	var count = 0;
	if(map == null){
		return count;
	}
	// Evalua que el mapa tenga mas de 1 registro
    for(var prop in map) {
            count++;
            if(count == 2){
            	return count;
            }
    }
    return count;
}
/**
 * Ejecuta el metodo onchange si es que lo tiene un
 * elemento de html
 * @param target puede ser el id o un objeto html
 * @autor martin
 */
function excuteOnchange(target){
	target = validateTarget(target);
	try{
		if(target.onchange){
			target.onchange();
		}
	}catch(e){
		
	}
}
/**
 * Elimina una opcion en un select
 * @param target puede ser el id o un objeto html
 * @param indexOption index de la opcion que se quiere eliminar
 * @autor martin
 */
function removeOption(target,indexOption){
	target = validateTarget(target);
	target.options.remove(indexOption);
}