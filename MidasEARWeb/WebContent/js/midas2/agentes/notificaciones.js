var gridDestinatarios;
var enviarA = new Array();
function onChangeProceso(target){
	var idProceso = jQuery("#procesos").val();
	if(idProceso != null  && idProceso != headerValue){		
		dwr.engine.beginBatch();
		listadoService.getMapMovimientosPorProceso(idProceso,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(target,"");
	}	
	jQuery("#proceso").val(jQuery("#procesos").val());
}

function onChangeMovimientos(){
	jQuery("#movimiento").val(jQuery("#movimientos").val());
	
	if (jQuery("#movimientos option:selected").text() =='GENERACION DE PREVIEW'  &&  jQuery("#procesos option:selected").text() =='PAGO DE COMISIONES'){
			opcionesValidasPreviewComisiones();
	}else{
		todasLAsOpciones();
	}
}

function onChangeOtros(){	
	if(jQuery(".checkOtro").attr('checked')==true){
		jQuery(".otros").css("display","block");
		jQuery(".attrOtros").each(function(i,elem){
			jQuery(this).attr('disabled',false);		
		});		
	}
	else{
		jQuery(".otros").css("display","none");
	}
}

function agregarDestinatarios(){
	jQuery("#accion").val(1);
	var tipoDestinatario=new Array();		
	var j=0,k=0,contTipoDest=0,contOtros=0;
	var validaciones='Los siguientes campos son requeridos:';
	if(jQuery("#procesos").val()==null||jQuery("#procesos").val()==''){
		validaciones+=' Proceso,';
	}
	if(jQuery("#movimientos").val()==null||jQuery("#movimientos").val()==''){
		validaciones+=' Movimiento,';
	}
	if(jQuery("#modoEnvio").val()==null||jQuery("#modoEnvio").val()==''){
		validaciones+=' Modo de Envio,';
	}
	jQuery(".tipDest").each(function(i,elem){
		if(jQuery(this).attr('checked') == true){			
			contTipoDest++;
			if(jQuery(this).attr('name')=='OTROS'){
				contOtros++;
			}
		}		
	});
	if(contTipoDest==0){
		validaciones+=' al menos un Tipo de Destinatario,';
	}
	if(contOtros>0){
		if(jQuery("#otrosPuesto").val()==null||jQuery("#otrosPuesto").val()==''){
			validaciones+=' Puesto,';
		}
		if(jQuery("#otrosNombre").val()==null||jQuery("#otrosNombre").val()==''){
			validaciones+=' Nombre,';
		}
		if(jQuery("#otrosCorreo").val()==null||jQuery("#otrosCorreo").val()==''){
			validaciones+=' Correo,';
		}
		else{
			 if (/^([\w\.\xF1\-]+@[\w\.\xF1\-]+\.[\w]{2,3})?$/.test(jQuery("#otrosCorreo").val())!=true){
				 validaciones+=' El formato del correo es incorrecto,';
			 }
		}
	}
	if(validaciones=='Los siguientes campos son requeridos:'){
	var correoDest;
	jQuery(".tCorreo").each(function(i,elem){
		if(jQuery(this).attr('checked') == true){			
			correoDest=jQuery(this).val();
		}
	});
	jQuery(".tipDest").each(function(i,elem){
		if(jQuery(this).attr('checked') == true){			
			tipoDestinatario.push({"idDest":jQuery(this).val(),"nameDest":jQuery(this).attr('name'),"correoDestino":correoDest});
		}
	});
	var dest={"idModoEnvio":jQuery("#modoEnvio").val(),"nameModoEnvio":jQuery("#modoEnvio option:selected").text(),"tipoDest":tipoDestinatario,
				   "puesto":jQuery("#otrosPuesto").val(),"nombre":jQuery("#otrosNombre").val(),"correo":jQuery("#otrosCorreo").val()};
	enviarA.push(dest);		
	crearTabla();
	jQuery("#otrosPuesto").val("");
	jQuery("#otrosNombre").val("");
	jQuery("#otrosCorreo").val("");
	onChangeOtros();
	}
	else{
		parent.mostrarMensajeInformativo(validaciones.substring(0,validaciones.length-1),"10");
	}
}

function eliminarDeTabla(id){
	var existenOtros=false;	
	enviarA.splice(id,1);
	crearTabla();
}

function crearTabla(){
	var existenOtros=false;
	var e=document.getElementById('tablaDinamica');
	e.innerHTML='';
	var c=new String();
	if(enviarA.length>0){
	for(j=0;j<enviarA.length;j++){
		for(k=0;k<enviarA[j].tipoDest.length;k++){	
			if(enviarA[j].tipoDest[k].nameDest=='OTROS'){
				existenOtros=true;
			}
		}
	}
	c+='<table name"tblDestinatarios" id="tblDestinatarios" width="98%" class="contenedorConFormato" align="center">';
	c+='<tr><td></td><td>DESTINATARIO(S)</td>';
	c+='<td>PUESTO</td><td>NOMBRE</td><td>CORREO</td>';
	c+='</tr>';
	for(j=0;j<enviarA.length;j++){
	c+='<tr>';
		c+='<td>'+enviarA[j].nameModoEnvio+':</td>';
		c+='<td>';
		for(k=0;k<enviarA[j].tipoDest.length;k++){		
			c+=enviarA[j].tipoDest[k].nameDest;
			if(k<enviarA[j].tipoDest.length-1){
				c+=','
			}
		}
		c+='</td>';
		for(k=0;k<enviarA[j].tipoDest.length;k++){			
			if(enviarA[j].tipoDest[k].nameDest=='OTROS'){
				c+='<td>'+enviarA[j].puesto+'</td><td>'+enviarA[j].nombre+'</td><td>'+enviarA[j].correo+'</td>';
			}
			else {
				c+='<td></td><td></td><td></td>';
			}
		}
		if(enviarA[j].tipoDest.length==0){
			c+='<td></td><td></td><td></td>';
		}
		if(jQuery("#accion").val()!=0){
			c+='<td><div class ="w100" align="right"><div class="btn_back w100"><a href="javascript: void(0);" onclick="eliminarDeTabla(';
			c+=j;
			c+=');">Eliminar</a></div></div></td>';
		}
	c+='</tr>';
	}
	c+='</table>';
	e.innerHTML=c;
	jQuery(".tipDest").each(function(i,elem){
		if(jQuery(this).attr('checked') == true){			
			jQuery(this).attr('checked',false);
		}
	});
	if(enviarA.length>0){
		jQuery("#proceso").val(jQuery("#procesos").val());
		jQuery("#movimiento").val(jQuery("#movimientos").val());
		jQuery("#procesos").attr("disabled", "disabled");
		jQuery("#movimientos").attr("disabled", "disabled");
		jQuery(".tCorreo").each(function(i,elem){
			if(jQuery(this).attr('checked') == true){			
				jQuery("#correoDestino").val(jQuery(this).val());
			}
		});		
	}
	}else{
		jQuery("#procesos").attr("disabled", false);
		jQuery("#movimientos").attr("disabled", false);		
		jQuery("#proceso").val("");
		jQuery("#movimiento").val("");
		jQuery("#correoDestino").val("");
	}
}

function guardarConfigNotif(guardar){
	var i=0,j=0;
	var contador=0;
	var url= new String;
	if(enviarA.length>0){	
	url="/MidasWeb/notificaciones/guardarConfiguracion.action?configNotif.idProceso.id="+jQuery("#proceso").val();
	url+="&configNotif.idMovimiento.id="+jQuery("#movimiento").val()+"&configNotif.idTipoCorreoEnvio.id="+jQuery("#correoDestino").val();	
	for(i=0;i<enviarA.length;i++){		
		for(j=0;j<enviarA[i].tipoDest.length;j++){
			url+="&configNotif.destinatariosList["+contador+"].idModoEnvio.id="+enviarA[i].idModoEnvio;
			url+="&configNotif.destinatariosList["+contador+"].idTipoDestinatario.id="+enviarA[i].tipoDest[j].idDest;
			url+="&configNotif.destinatariosList["+contador+"].idTipoCorreoEnvio.id="+enviarA[i].tipoDest[j].correoDestino;
			if(enviarA[i].tipoDest[j].nameDest=='OTROS'){
				url+="&configNotif.destinatariosList["+contador+"].puesto="+enviarA[i].puesto;
				url+="&configNotif.destinatariosList["+contador+"].nombre="+enviarA[i].nombre;
				url+="&configNotif.destinatariosList["+contador+"].correo="+enviarA[i].correo;
			}
			contador++;
		}
	}
//	if(dwr.util.getValue("configNotif.notas")!=''&&dwr.util.getValue("configNotif.notas")!=null){
//		url+="&configNotif.notas="+dwr.util.getValue("configNotif.notas");
//	}
	if(dwr.util.getValue("configNotif.id")!=''&&dwr.util.getValue("configNotif.id")!=null){
		url+="&configNotif.id="+dwr.util.getValue("configNotif.id");
	}
	var data={"configNotif.notas":dwr.util.getValue("configNotif.notas")};
	if(guardar==1){
		jQuery.asyncPostJSON(url,data,afterSaveAndCopy);
	}
	else{
		jQuery.asyncPostJSON(url,data,afterSave);
	}	
	}
	else{
		parent.mostrarMensajeInformativo('Es requerido minimo un destinatario','10');
	}	
}

function afterSave(json){
	var urlFiltro="/MidasWeb/notificaciones/gridNotificaciones.action";
	listarFiltradoGenerico(urlFiltro,"notificacionesGrid", null,null);
	parent.mostrarMensajeInformativo(json.mensaje,""+json.tipoMensaje+"");
	limpiarNotificaciones();
	enviarA= new Array();
}

function afterSaveAndCopy(json){
	var urlFiltro="/MidasWeb/notificaciones/gridNotificaciones.action";
	listarFiltradoGenerico(urlFiltro,"notificacionesGrid", null,null);
	parent.mostrarMensajeInformativo(json.mensaje,""+json.tipoMensaje+"");
	dwr.util.setValue("configNotif.id","");
}

function gridNotificaciones(){
	gridDestinatarios = new dhtmlXGridObject("notificacionesGrid");	
	gridDestinatarios.load("/MidasWeb/notificaciones/gridNotificaciones.action");	
}

function verConfigNotificaciones(id,accion){
	jQuery("#accion").val(accion);
	var path ="/MidasWeb/notificaciones/cargarConfiguracion.action?configNotif.id="+id;
	var data="";
	jQuery.asyncPostJSON(path,data,cargarConfiguracion);	
}

function cargarConfiguracion(json){
	enviarA=new Array();
	var conf = json.configNotif;
	dwr.util.setValue("configNotif.idProceso.id",conf.idProceso.id);
	onChangeProceso('movimientos');
	dwr.util.setValue("configNotif.idMovimiento.id",conf.idMovimiento.id);
	var listDest=conf.destinatariosList;
	var tipoDest=new Array();		
	var j=0,k=0;
	for(j=0;j<listDest.length;j++){	 
			tipoDest=new Array();
			tipoDest.push({"idDest":listDest[j].idTipoDestinatario.id,"nameDest":listDest[j].idTipoDestinatario.valor,"correoDestino":listDest[j].idTipoCorreoEnvio.id});		
	
	var dest={"idModoEnvio":listDest[j].idModoEnvio.id,"nameModoEnvio":listDest[j].idModoEnvio.valor,"tipoDest":tipoDest,
				   "puesto":listDest[j].puesto,"nombre":listDest[j].nombre,"correo":listDest[j].correo};
	enviarA.push(dest);
	}				
	crearTabla();
	dwr.util.setValue("configNotif.id",conf.id);
	dwr.util.setValue("configNotif.notas",conf.notas);
	if(jQuery("#accion").val()==0){
		jQuery(".tipDest").each(function(i,elem){					
				jQuery(this).attr('disabled',true);			
		});
		jQuery(".tCorreo").each(function(i,elem){					
			jQuery(this).attr('disabled',true);			
		});
		jQuery(".checkOtro").each(function(i,elem){					
			jQuery(this).attr('disabled',true);			
		});
		jQuery("#procesos").attr('disabled',true);	
		jQuery("#movimientos").attr('disabled',true);	
		jQuery("#modoEnvio").attr('disabled',true);	
		jQuery("#otrosPuesto").attr('disabled',true);	
		jQuery("#otrosNombre").attr('disabled',true);	
		jQuery("#otrosCorreo").attr('disabled',true);	
		jQuery(".btnH").css("display","none");	
		jQuery(".btnL").css("display","block");
	}
	else{
		jQuery(".tCorreo").each(function(i,elem){					
			jQuery(this).attr('disabled',false);			
		});
		jQuery(".tipDest").each(function(i,elem){					
			jQuery(this).attr('disabled',false);			
		});
		jQuery(".checkOtro").each(function(i,elem){					
			jQuery(this).attr('disabled',false);			
		});
		jQuery("#modoEnvio").attr('disabled',false);
		jQuery(".btnH").each(function(i,elem){					
			jQuery(this).css("display","block");	
		});
		jQuery(".btnH").css("display","block");	
		jQuery(".btnL").css("display","none");
	}
	jQuery(".otros").css("display","none");
	
	if (jQuery("#movimientos option:selected").text() =='GENERACION DE PREVIEW'  &&  jQuery("#procesos option:selected").text() =='PAGO DE COMISIONES'){
		opcionesValidasPreviewComisiones();
	}else{
		todasLAsOpciones();
	}
}

function limpiarNotificaciones(){
	dwr.util.setValue("configNotif.id","");
	jQuery("#otrosPuesto").val("");
	jQuery("#otrosNombre").val("");
	jQuery("#otrosCorreo").val("");
	jQuery("#accion").val("");
	jQuery("#procesos").val("");
	onChangeProceso('movimientos');
	jQuery("#movimientos").val("");
	jQuery("#modoEnvio").val("");
	jQuery("#notas").val("");
	jQuery("#procesos").attr("disabled", false);
	jQuery("#movimientos").attr("disabled", false);
	onChangeOtros();
	jQuery(".tCorreo").each(function(i,elem){
		jQuery(this).attr("disabled", false);
	});
	var e=document.getElementById('tablaDinamica');
	e.innerHTML='';
	jQuery(".tipDest").each(function(i,elem){					
		jQuery(this).attr('disabled',false);			
	});
	jQuery("#modoEnvio").attr('disabled',false);
	jQuery(".btnH").css("display","block");	
	jQuery(".btnL").css("display","none");
	enviarA=new Array();
}

function buscarNotificaciones(){
	var urlFiltro="/MidasWeb/notificaciones/buscarNotificaciones.action?configNotif.idProceso.id="+dwr.util.getValue("configNotif.idProceso.id")+"&configNotif.idMovimiento.id="+dwr.util.getValue("configNotif.idMovimiento.id");
	listarFiltradoGenerico(urlFiltro,"notificacionesGrid", null,null);
}

function eliminarNotificaciones(id){
//	parent.mostrarMensajeConfirm("Los campos obligatorios est\u00E1n completos, \u00BFDesea activar el registro?","20","sendRequestEliminar("+id+")",null,null,null);
	parent.mostrarMensajeConfirm("¿Esta seguro de eliminar esta notificación ?","20","sendRequestEliminar("+id+")",null,null,null);
}

function sendRequestEliminar(id){
	var urlFiltro="/MidasWeb/notificaciones/eliminarNotificacion.action?configNotif.id="+id;
	listarFiltradoGenerico(urlFiltro,"notificacionesGrid", null,null);
}

function opcionesValidasPreviewComisiones(){
	//desabilitamos el combo de modo de envio 
	jQuery("#modoEnvio option").each(function(i) {
	    if(i!=0 && i!=3){
		  jQuery(this).attr('disabled','disabled');
		}
	});
	//se desabilitan todos los check y radiobuton menos el check de otros
	jQuery('input[type=checkbox]').each(function(i){
		if(i>0){
			jQuery(this).attr('checked','').attr('disabled','disabled')
		}
	});
	jQuery('input[type=radio]').each(function(i){
				if(i==0){
					jQuery(this).attr('checked',true)
				}else{
					jQuery(this).attr('disabled','disabled')
				}
			});
	//se deshabilita el campo de notas 
	jQuery("#notas").attr('disabled','disabled');
}
function todasLAsOpciones(){
	jQuery("#modoEnvio option").each(function() {
	    jQuery(this).removeAttr('disabled','disabled');
	});
	jQuery('input[type=checkbox]').each(function(){
	    jQuery(this).attr('checked',false).removeAttr('disabled','disabled')
	});
	jQuery('input[type=radio]').each(function(i){
			jQuery(this).removeAttr('disabled','disabled')
	});
	
	jQuery("#notas").removeAttr('disabled','disabled');
}
