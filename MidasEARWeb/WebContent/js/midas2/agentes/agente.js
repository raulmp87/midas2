var agenteGrid, agenteCarteraClientesGrid;
var ventanaAfianzadora=null;
var tabInfo_general=null;
var tabDomicilios=null;
var tabDatosFiscales=null;
var tabDatosContables=null;
var tabDatosExtra=null;
var tabDocumentos=null;
var tabCarteraClientes=null;
var contenedorTab=null;
var grid=null;
var gridEntretenimientosAgente;
var gridHijosAgente;
var TIPOACCION_CONSULTAHISTORICO = "5";


function agregarAgente(tipoAccion) {
	if(tipoAccion==null || tipoAccion==''){
		tipoAccion=parent.dwr.util.getValue("tipoAccion");
	}
	var path= "/MidasWeb/fuerzaventa/agente/verDetalle.action";
	sendRequestJQ(null, path + "?tipoAccion=" + tipoAccion, targetWorkArea, 'dhx_init_tabbars();');	
}
function paginarAgente(){
	var url=listarFiltradoAgentePaginadoPath+"?funcionPaginar=paginarAgente&divGridPaginar=agenteGrid"
	var data=jQuery("#agenteForm").serialize();
	url+="&"+data;
	var funcionFiltrarAgente=jQuery("#agenteBuscar").attr("onclick");
	sendRequestJQ(null,url,"gridAgentePaginado",funcionFiltrarAgente);
}

function pageGridPaginadoAgente(page, nuevoFiltro,cargaInicial){
	
	var posPath = '&posActual='+page+'&funcionPaginar=pageGridPaginadoAgente&divGridPaginar=agenteGrid';

	var paginadoParamsPath ='';
	if(nuevoFiltro){
		var posActualFormAgente=jQuery("#agenteForm input[name='posActual']");
		if(posActualFormAgente){
			posActualFormAgente.remove();
		}
		jQuery("#txtTotalCount").val(null);
		paginadoParamsPath = jQuery(document.agenteForm).serialize();
	}else{
		posPath ='&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	posPath=(cargaInicial==true)?  posPath+"&totalCount=0":posPath;
	var url=listarFiltradoAgentePaginadoPath+ "?"+ paginadoParamsPath + posPath;
	url=jQuery.validateUrl(url);
	sendRequestJQ(null, url, 'gridAgentePaginado', 'listarAgente('+cargaInicial+');');
}

function verInfoGeneral() {
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		sendRequestJQ(null, mostrarInfoGralPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")+ 
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"),
				'contenido_info_general','limpiarDivsGeneral();');		
	}
	else
	{
		sendRequestJQ(null, mostrarInfoGralPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),
				'contenido_info_general','limpiarDivsGeneral();');		
	}	
}

function verDomicilioAgente() {
	var path= "/MidasWeb/fuerzaventa/agente/mostrarDomicilioAgente.action";
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		sendRequestJQ(null, mostrarDomicilioAgentePath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")+
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"),
				'contenido_domicilios','limpiarDivsDomicilio();');		
	}
	else
	{
		sendRequestJQ(null, mostrarDomicilioAgentePath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),
				'contenido_domicilios','limpiarDivsDomicilio();');		
	}
}

function verDatosFiscales() {
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		sendRequestJQ(null, mostrarDatosFiscalesPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")+
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"),
				'contenido_datosFiscales','limpiarDivsFiscales();');	
	}
	else
	{
		sendRequestJQ(null, mostrarDatosFiscalesPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),
				'contenido_datosFiscales','limpiarDivsFiscales();');		
	}
}

function verDatosContables() {
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		sendRequestJQ(null, mostrarDatosContablesPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")+
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"),
				'contenido_datosContables','limpiarDivsContables();');		
	}
	else
	{
		sendRequestJQ(null, mostrarDatosContablesPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+
				"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),
				'contenido_datosContables','limpiarDivsContables();');		
	}
}

function verDatosExtra() {
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		sendRequestJQ(null, mostrarDatosExtraPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+"&agente.id="+
				parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")+
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"),
				'contenido_datosExtra','limpiarDivsDatosExtra();');				
	}
	else
	{
		sendRequestJQ(null, mostrarDatosExtraPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),'contenido_datosExtra','limpiarDivsDatosExtra();');		
	}	
}

function verDocumentosAgente() {
	sendRequestJQ(null, mostrarDocumentosPath+'?tipoAccion='+dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+"&agente.id="+parent.dwr.util.getValue("agente.id")+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen"),'contenido_documentos','limpiarDivsDocumentos();');
}

function verCarteraClientes() {
	sendRequestJQ(null, mostrarCarteraClientesPath+'?moduloOrigen='+dwr.util.getValue("moduloOrigen")+'&tipoAccion='+dwr.util.getValue("tipoAccion"),'contenido_carteraClientes','null');//+"&moduloOrigen="+parent.dwr.util.getValue("moduloOrigen")
}

function onChangeGerencia(target,target2,value){
	idgerencia = value;
	if(idgerencia != null  && idgerencia != headerValue){
		listadoService.getMapEjecutivosPorGerencia(idgerencia,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});
	}else{
		addOptions(target,null);
	}
	var idEjecutivo = jQuery("#idEjecutivoCatalogo option:selected").val();
	if(idEjecutivo!=''){		
		addOptions(target2,null);
	}
}

function onChangeEjecutivo(target,value){
	var idEjecutivo = value;
	if(idEjecutivo != null  && idEjecutivo != headerValue){
		listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});	
	}else{
		addOptions(target,null);
	}
}

function guardarAgenteDatosGenerales() {
	if(validateAll(true)){
		var tipoAgente = jQuery("#txtTipoAgente option:selected").val();
		var clasificacionAgente = jQuery("#txtClasifAgente option:selected").text();
		var fecAlta = jQuery("#txtFechafechaAlta").val();
		var fecBaja = jQuery("#txtFechaBaja").val();
		if(compara_fecha(fecAlta,fecBaja)){
			alert("Fecha de vencimiento de cedula no puede ser menor a la Fecha de Autorizacion");
			jQuery("#txtFechaBaja").val("");
		}else if(clasificacionAgente=='ALTA POR INTERNET'){
			alert("La clasificación de Agente no puede ser ALTA POR INTERNET");
		}else{
			var generaChequeBoolean = (jQuery("#txtGeneraCheque").is(":checked")==true)?true:false;
			var contabilizaComisionBoolean = (jQuery("#txtContabiliza").is(":checked")==true)?true:false;
			var comisionBoolean = (jQuery("#txtComision").is(":checked")==true)?true:false;
			var imprimeEdoCtaBoolean = (jQuery("#txtEdoCuenta").is(":checked")==true)?true:false;
			var path= "/MidasWeb/fuerzaventa/agente/guardarGeneralAgente.action?"+jQuery("#guardarInfoGeneral").serialize()+
			"&agente.claveComisionBoolean="+comisionBoolean+
			"&agente.claveContabilizaComisionBoolean="+contabilizaComisionBoolean+
			"&agente.claveGeneraChequeBoolean="+generaChequeBoolean+
			"&agente.claveImprimeEstadoCtaBoolean="+imprimeEdoCtaBoolean+
			"&agente.idTipoAgente="+tipoAgente;
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
}

function guardarAgenteDatosFiscales(){
	if(validateAll(true)){
		var path="/MidasWeb/fuerzaventa/agente/guardarDatosFiscales.action?"+jQuery("#guardarDatosFiscalesForm").serialize();
		sendRequestJQ(null, path, targetWorkArea, null);
	}
}

function guardaDomicilioAgente() {
	if(validateAll(true)){
	var path= "/MidasWeb/fuerzaventa/agente/guardarDomicilioAgente.action?"+jQuery("#guardarDomicilioAgente").serialize();
	sendRequestJQ(null, path, targetWorkArea, null);	
	}
}

function onChangeBanco(){
	var idBanco=(dwr.util.getValue("idBanco")!='')?dwr.util.getValue("idBanco"):'-1';
	var target="idProductoBancario";
	var url="/MidasWeb/fuerzaventa/agente/listarProductosBancariosPorBanco.action";
	var params={"productoBancarioSeleccionado.banco.idBanco":idBanco};
	jQuery.asyncPostJSON(url,params,function(data){
		var dataCombo=new Array();
		var arr=data.productosBancarios;
		jQuery.fillCombo(target,data.productosBancarios,'id','descripcion',true);
		jQuery("#tipoProducto").val('');
	});
}

function onChangeProductoBancario(){
	var idProducto=dwr.util.getValue("idProductoBancario");
	var url="/MidasWeb/fuerzaventa/agente/cargarProductoBancario.action";
	var params={"productoBancarioSeleccionado.id":idProducto};
	limpiaCampos();
	jQuery.asyncPostJSON(url,params,function(data){
		var producto=data.productoBancarioSeleccionado;
		var claveCredito=producto.claveCredito;
		var claveTransfelectronica=producto.claveTransfelectronica;
		var tipoProducto="";
		var removerRequerido = ["montoCreditoLb","numeroPlazosLb","montoPagoLb","numeroCuentaLb","sucursalLb","plazaLb"];
		if(claveCredito==1){
			tipoProducto="Crédito";
			var fieldsEnable=["montoCredito","numeroPlazos","montoPago","sucursal","plaza"];
			var fieldsDisable=["numeroCuenta","numeroClabe","claveDefault"];
			setAttribute("disabled",false,fieldsEnable);
			isRequired(true,fieldsEnable);
			setAttribute("disabled",true,fieldsDisable);
			jQuery("#claveDefault").attr("checked",false);
			var agregarRequerido= ["montoCreditoLb","numeroPlazosLb","montoPagoLb","sucursalLb","plazaLb"];
			noEsRequerido(removerRequerido);
			esRequerido(agregarRequerido);
		}else if(claveTransfelectronica==1){
			tipoProducto="Transferencia Electrónica";
			var fieldsEnable=["claveDefault","numeroCuenta","numeroClabe","sucursal","plaza"];
			var fieldsDisable=["montoCredito","montoPago","numeroPlazos"];
			var requeridos = ["claveDefault","numeroCuenta","sucursal","plaza"];
			setAttribute("disabled",false,fieldsEnable);
			setAttribute("disabled",true,fieldsDisable);
			isRequired(true,requeridos);
			var agregarRequerido = ["numeroCuentaLb","sucursalLb","plazaLb"];
			noEsRequerido(removerRequerido);
			esRequerido(agregarRequerido);
		}
		jQuery("#tipoProducto").val(tipoProducto);
	});
}

function isRequired(isRequired,fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var elem=jQuery.getObject(id);
		if(elem!=null){
			if(isRequired){
				elem.addClass("jQrequired");
			}else{
				elem.removeClass("jQrequired");
			}
		}
	}
}

function enableDisable(enable,fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var elem=jQuery.getObject(id);
		if(elem!=null){
			elem.attr("readOnly",enable);
		}
	}
}

function setAttribute(attribute,value,fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var elem=jQuery.getObject(id);
		if(elem!=null){
			elem.attr(attribute,value);
		}
	}
}

function esRequerido(fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var texto =	jQuery("#"+id).text();
		jQuery.trim(texto);
		texto = texto+" *";
		jQuery("#"+id).text(texto);	
	}
}

function noEsRequerido(fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var tex = jQuery("#"+id).text();
		jQuery.trim(tex);
		tex.replace("*"," ");
		jQuery("#"+id).text(tex.replace("*"," "));	
	}
}


function mostrarModalResponsable(){
	var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=txtIdPersona";
	sendRequestWindow(null, url,obtenerVentanaResponsable);
}

function obtenerVentanaResponsable(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 900, 450);
	ventanaResponsable.center();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Consulta de Personas");
	return ventanaResponsable;
}

function findByIdResponsable(idResponsable){
	var url="/MidasWeb/fuerzaVenta/persona/findById.action";
	var data={"personaSeycos.idPersona":idResponsable};
	jQuery.asyncPostJSON(url,data,populatePersonaAgente);
}

function populatePersonaAgente(json){
	if(json){
		var idResponsable=json.personaSeycos.idPersona;
		var nombre=json.personaSeycos.nombreCompleto;
		var email=json.personaSeycos.email;
		var tipoPersona=json.personaSeycos.claveTipoPersona;
		jQuery("#txtIdPersona").val(idResponsable);
		jQuery("#txtNombreAgente").val(nombre);
		jQuery("#txtCorreoElectronico").val(email);
		jQuery("#claveTipoPersona").val(tipoPersona);
		
	}
}

function mostrarModalAfianzadora(){
	var url="/MidasWeb/fuerzaventa/afianzadora/mostrarContenedor.action?tipoAccion=consulta&idField=idAfianzadora";
	sendRequestWindow(null, url, obtenerVentanaAfianzadora);
}

function obtenerVentanaAfianzadora(){
	var wins = obtenerContenedorVentanas();
	ventanaAfianzadora= wins.createWindow("afianzadoraModal", 400, 320, 900, 450);
	ventanaAfianzadora.centerOnScreen();
	ventanaAfianzadora.setModal(true);
	ventanaAfianzadora.setText("Consulta de Afianzadoras");
	return ventanaAfianzadora;
}

function findByIdAfianzadora(idAfianzadora){
	var url="/MidasWeb/fuerzaventa/afianzadora/findById.action";
	var data={"afianzadora.id":idAfianzadora};
	jQuery.asyncPostJSON(url,data,populateAfianzadora);
}

function populateAfianzadora(json){
	if(json){
		var afianzadora=json.afianzadora;
		var idAfianzadora=afianzadora.id;
		jQuery("#idAfianzadora").val(idAfianzadora);
		var nombre=afianzadora.razonSocial;
		jQuery("#nombreAfianzadora").val(nombre);
	}
}

function agregarProducto(){
	if(validateForId("productosAgenteForm",true)){
		var url="/MidasWeb/fuerzaventa/agente/guardarProducto.action";
		var data=jQuery("#productosAgenteForm").serialize();
		jQuery.asyncPostJSON(url,data,function(json){
			parent.mostrarMensajeInformativo(json.mensaje,"30");
			var urlGrid="/MidasWeb/fuerzaventa/agente/listarProductosBancariosPorAgente.action?agente.id=33";
			gridProductosBancarios.loadXML(urlGrid);
		});
	}
}

function eliminarProductoBancario(idProducto){
	var url="/MidasWeb/fuerzaventa/agente/eliminarProductoBancario.action";
	var data={"producto.id":idProducto};
	if(idProducto==null){
		parent.mostrarMensajeInformativo("Favor de elegir un producto a eliminar","20");
	}else{
		if(confirm("Esta seguro de eliminar este producto?")){
			jQuery.asyncPostJSON(url,data,function(json){
				parent.mostrarMensajeInformativo(json.mensaje,"30");
				var urlGrid="/MidasWeb/fuerzaventa/agente/listarProductosBancariosPorAgente.action?agente.id=33";
				gridProductosBancarios.loadXML(urlGrid);
			});
		}
	}
}

function copiarDomicilioPersonal(){
	var estadoName="agente.persona.domicilios[0].claveEstado";
	var paisName="agente.persona.domicilios[0].clavePais";
	var ciudadName="agente.persona.domicilios[0].claveCiudad";
	var coloniaName="agente.persona.domicilios[0].nombreColonia";
	var calleNumeroName = "agente.persona.domicilios[0].calleNumero";
	var codigoPostalName = "agente.persona.domicilios[0].codigoPostal";
	var idEstado = jQuery('#persoEstado').val();
	var idPais = jQuery('#persoPais').val();
	var idCiudad = jQuery('#persoCiudad').val();
	var calleNumero=jQuery('#persoCalleNumero').val();
	var colonia=jQuery('#persoColonia').val();
	var codigoPostal = jQuery('#persoCP').val();
	dwr.util.setValue(paisName, idPais);	
	onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName, paisName);
	dwr.util.setValue(estadoName, idEstado);
	onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
	dwr.util.setValue(ciudadName, idCiudad);
	onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
	dwr.util.setValue(coloniaName, colonia);
	onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
	dwr.util.setValue(calleNumeroName, calleNumero);
	
	dwr.util.setValue(codigoPostalName, codigoPostal);
}

function populateAsociacion(json){
	if(json){
		var id=grid.getSelectedId();
		var idCliente=grid.cellById(id, 0).getValue();		
		var nombreCliente=grid.cellById(id, 1).getValue();
		var rfcCliente = grid.cellById(id, 2).getValue();
		var emailCliente = grid.cellById(id, 3).getValue();
		dwr.util.setValue("idCliente",idCliente);
		dwr.util.setValue("clienteAgente.cliente.idCliente",idCliente);
		dwr.util.setValue("nombreCliente",nombreCliente);
		dwr.util.setValue("rfcCliente",rfcCliente);
		dwr.util.setValue("emailCliente",emailCliente);
		var asociacionAgente=json.asociado.split('-');
		if(asociacionAgente.length>1){
			dwr.util.setValue("agente.idAgenteReclutador",asociacionAgente[1]);
		}
		else{
			dwr.util.setValue("agente.idAgenteReclutador","");
		}
	}
}

function callBackHijoAgente(JSON){
	listarFiltradoGenerico(listarFiltradoHijoAgentePath, 'agenteHijosGrid');
}

function callBackEntretenimientoAgente(JSON){
	listarFiltradoGenerico(listarFiltradoEntretenimientoAgentePath, 'agenteEntretenimientoGrid');
}


function borrarHijo(id){
	if(confirm("Seguro que desear Quitar este Hijo?")){
		genericPostWithParamsJsonReturn(eliminarDatosHijoExtraPath,{"hijoAgente.id":id},callBackHijoAgente);
	}
}

function borrarEntretenimiento(id){
	if(confirm("Seguro que desear quitar este entretenimiento?")){
		genericPostWithParamsJsonReturn(eliminarDatosEntretenimientoExtraPath,{"entretenimientoAgente.id":id},callBackEntretenimientoAgente);
	}
}

function generarLigaIfimax(){
	var url='/MidasWeb/fuerzaventa/agente/generarLigaIfimax.action?agente.id='+parent.dwr.util.getValue("agente.id")+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agente.persona.claveTipoPersona");
	window.open(url,'Fortimax');
}

function guardarDocumentosFortimax(){
	var url="/MidasWeb/fuerzaventa/agente/guardarDocumentos.action?"+jQuery("#agenteDocumentosForm").serialize();
	var data;
	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantes);
}

function populateDocumentosFaltantes(json){
	if(json){
		var docsFaltantes=json.documentosFaltantes;
		if(docsFaltantes!=null){
			parent.mostrarMensajeInformativo('Los siguientes archivos no han sido digitalizados: '+docsFaltantes+', favor de digitalizar',"10");
		}
		else{
			parent.mostrarMensajeInformativo('Todos los documentos han sido guardados exitosamente',"30");
			var path="/MidasWeb/fuerzaventa/agente/verDetalle.action";
			sendRequestJQ(null, path+"?agente.id="+parent.dwr.util.getValue("agente.id")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion")+"&tabActiva=carteraClientes",targetWorkArea, 'limpiarDivsCarteraClientes();dhx_init_tabbars();');		
		}
	}
}

function auditarDocumentos(){
	var url="/MidasWeb/fuerzaventa/agente/matchDocumentosAgente.action?"+jQuery("#agenteDocumentosForm").serialize();
	var data="";
	jQuery.asyncPostJSON(url,data,populateDocumentosFortimax);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimax(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#agenteDocumentosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
			});
		}
	}
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_documentos');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsDomicilio() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_documentos');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsFiscales() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_documentos');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsContables() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_documentos');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsDatosExtra() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_documentos');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsDocumentos() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_carteraClientes');
}

function limpiarDivsCarteraClientes() {
	limpiarDiv('contenido_info_general');
	limpiarDiv('contenido_domicilios');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContables');
	limpiarDiv('contenido_datosExtra');
	limpiarDiv('contenido_documentos');
}

function imprimirContratoAgente() {
	var location ="/MidasWeb/fuerzaventa/agente/imprimirContrato.action";
	window.open(location, "Contrato");
}

function ocultarMostrarBoton(div){
	
	if (div=="masFiltros"){
		jQuery("#masFiltros").css("display", "none");
		jQuery("#menosFiltros").css("display", "block");
				
	}else{
		jQuery("#masFiltros").css("display", "block");
		jQuery("#menosFiltros").css("display", "none");
	}
}

function guardarHijosAgenteExtra(){
	var cont =gridHijosAgente.getRowsNum()+1;
	var nombre = jQuery("#txtNombreHijos").val();
	var apellidoPaterno = jQuery("#txtApPaternoHijos").val();
	var apellidoMaterno = jQuery("#txtApMaternoHijos").val();
	var fechaNac = jQuery("#txtFechaNacimientoHijo").val();
	var ultimaEscolaridad = jQuery("#txtUltimaEscolaridad").val();

	gridHijosAgente.addRow(cont,[nombre, apellidoPaterno, apellidoMaterno, fechaNac, ultimaEscolaridad, ]);			
}

function populateGridHijos(){
				var cont =gridHijosAgente.getRowsNum()+1;
				var nombre = jQuery("#txtNombreHijos").val();
				var apellidoPaterno = jQuery("#txtApPaternoHijos").val();
				var apellidoMaterno = jQuery("#txtApMaternoHijos").val();
				var fechaNac = jQuery("#txtFechaNacimientoHijo").val();
				var ultimaEscolaridad = jQuery("#txtUltimaEscolaridad").val();
			
				gridHijosAgente.addRow(cont,[nombre, apellidoPaterno, apellidoMaterno, fechaNac, ultimaEscolaridad, ]);			
}

function mostrarHistoricoAgente(){
	var tipoOperacion = 50;
	var url = '/MidasWeb/fuerzaventa/agente/mostrarHistorial.action?idTipoOperacion='+tipoOperacion+ "&idRegistro="+parent.dwr.util.getValue("agente.id");
	mostrarModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}

function populateDomicilioAgente0(json){
	if(json){
		var estadoName="agente.persona.domicilios[0].claveEstado";
		var paisName="agente.persona.domicilios[0].clavePais";
		var ciudadName="agente.persona.domicilios[0].claveCiudad";
		var coloniaName="agente.persona.domicilios[0].nombreColonia";
		var calleNumeroName="agente.persona.domicilios[0].calleNumero";
		var codigoPostalName="agente.persona.domicilios[0].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function populateDomicilioAgente1(json){
	if(json){
		var estadoName="agente.persona.domicilios[1].claveEstado";
		var paisName="agente.persona.domicilios[1].clavePais";
		var ciudadName="agente.persona.domicilios[1].claveCiudad";
		var coloniaName="agente.persona.domicilios[1].nombreColonia";
		var calleNumeroName="agente.persona.domicilios[1].calleNumero";
		var codigoPostalName="agente.persona.domicilios[1].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}
/**
 * Funcion para inicializar tabs, si es una alta, deshabilita el resto de los tabs hasta que se guarde la primer pestania
 */
function incializarTabs(){
	var idAgente=jQuery("#agente\\.id").val();
	contenedorTab=window["configuracionNegocioTabBar"];
	//Nombre de todos los tabs
	var tabs=["info_general","domicilios","datosFiscales","datosContables","datosExtra","documentos","carteraClientes"];
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
	if(!jQuery.isValid(idAgente)){
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
	for(var i=0;i<tabs.length;i++){
		var tabName=tabs[i];
		var catalogoProveniente = jQuery("#moduloOrigen").val();
		var tab=contenedorTab[tabName];
		jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
			if(jQuery.isValid(idAgente)){
				if(dwr.util.getValue("tipoAccion")==1||dwr.util.getValue("tipoAccion")==4){
					event.stopPropagation();
					parent.mostrarMensajeConfirm("Está a punto de abandonar la sección, ¿Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}
				else{
					obtenerFuncionTab(event.data.value);
				}
			}
			else{
				parent.mostrarMensajeInformativo("Debe guardar datos generales","10");
			}
		});
	}
}

function obtenerFuncionTab(tab){
	var tipoOperac=50;
//	if(jQuery("#moduloOrigen").val()==1){
//		tipoOperac=120;
//	}
	if(tab==0){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=info_general&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==1){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=domicilios&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==2){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=datosFiscales&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==3){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=datosContables&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==4){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=datosExtra&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==5){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=documentos&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }else{
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva=carteraClientes&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('agente.id'),targetWorkArea,'dhx_init_tabbars();');
  }
}

function checkboxDatosAgente(){
	if(jQuery("#estatusAgenteSituacion option:selected").text()!='AUTORIZADO'){
		jQuery("#txtGeneraCheque").attr('checked',true);
		jQuery("#txtContabiliza").attr('checked',true);
		jQuery("#txtComision").attr('checked',true);
		jQuery("#txtEdoCuenta").attr('checked',true);
		var opcionSeleccionada = jQuery("#txtClasifAgente option:selected").text();
		if(opcionSeleccionada=='VENTA DIRECTA'||opcionSeleccionada=='NEGOCIOS ESPECIALES REASEGURO'||opcionSeleccionada=='NEGOCIOS ESPECIALES VIDA'||opcionSeleccionada=='BANCA DE GOBIERNO'||opcionSeleccionada=='BANCA SEGUROS'){//VENTAS DIRECTAS		
			jQuery("#txtGeneraCheque").attr('checked',false);
			jQuery("#txtContabiliza").attr('checked',false);
			jQuery("#txtTipoAgente option[text=VENTAS DIRECTAS]").attr("selected",true)//VENTAS DIRECTAS
		}else{
			var claveTipoPersona = jQuery("#claveTipoPersona").val();
			if (claveTipoPersona==1){
				jQuery("#txtTipoAgente option[text=PERSONA FÍSICA]").attr("selected",true);
			}else {
				jQuery("#txtTipoAgente option[text=PERSONA MORAL]").attr("selected",true);
			}
		}
	}
	
	
	if(jQuery("#txtAgenteReclutador").val()){
		findByIdAgenteReclutadorInit(jQuery("#txtAgenteReclutador").val());
	}	
}

function asignarClienteAAgente(){
	var agenteActual = dwr.util.getValue("agente.id");
	var agenteRelacionado = dwr.util.getValue("agente.idAgenteReclutador");
	var url="/MidasWeb/fuerzaventa/agente/guardarCarteraClientes.action";
	var data={"agente.id":dwr.util.getValue("agente.id"),"agente.idAgenteReclutador":dwr.util.getValue("agente.idAgenteReclutador"),"clienteAgente.cliente.idCliente":dwr.util.getValue("clienteAgente.cliente.idCliente")};
	var mensajeConfirmacion;
	var idCliente=dwr.util.getValue("clienteAgente.cliente.idCliente");
	var idAgente=dwr.util.getValue("agente.id");
	var idAgenteReclutador=dwr.util.getValue("agente.idAgenteReclutador");
	if(dwr.util.getValue("clienteAgente.cliente.idCliente")!=null&&dwr.util.getValue("clienteAgente.cliente.idCliente")!=''){
		if(agenteRelacionado==null||agenteRelacionado==''){			
			jQuery.asyncPostJSON(url,data,populateClientesAgente);
		}
		else if(agenteRelacionado!=agenteActual){
			mensajeConfirmacion='Este cliente est\u00E1 relacionado a un agente diferente, \u00BFconfirma relacionarlo tambi\u00E9n con el agente actual?';
			parent.mostrarMensajeConfirm(mensajeConfirmacion,"20","sendRequestCliente("+idAgente+","+idAgenteReclutador+","+idCliente+")",null,null,null);
//			if(confirm(mensajeConfirmacion)){
//				jQuery.asyncPostJSON(url,data,populateClientesAgente);
//			}
		}
		else{
			mensajeConfirmacion='Este cliente ya esta relacionado a este agente';
			parent.mostrarMensajeInformativo(mensajeConfirmacion,"10");
		}
	}
	else{
		parent.mostrarMensajeInformativo('Selecciones un cliente para relacionar',"10");
	}	
	dwr.util.setValue("idCliente","");
	dwr.util.setValue("clienteAgente.cliente.idCliente","");
	dwr.util.setValue("nombreCliente","");
	dwr.util.setValue("rfcCliente","");
	dwr.util.setValue("emailCliente","");
	dwr.util.setValue("agente.idAgenteReclutador","");
	
	jQuery("#txtNombreInstitucion").val("");
	jQuery("#txtApPaterno").val("");
	jQuery("#txtapMaterno").val("");
	jQuery(".dataGridGenericStyle #clientesGrid").html("");
	
}

function sendRequestCliente(idAgente,idAgenteReclutador,idCliente){
	var url="/MidasWeb/fuerzaventa/agente/guardarCarteraClientes.action";
	var data={"agente.id":idAgente,"agente.idAgenteReclutador":idAgenteReclutador,"clienteAgente.cliente.idCliente":idCliente};
	jQuery.asyncPostJSON(url,data,populateClientesAgente);
}

function populateClientesAgente(json){
	if(json["asociado"]=="exito"){
		dwr.engine.beginBatch();
		parent.mostrarMensajeInformativo("El Cliente ha sido asignado correctamente","30");
		dwr.engine.endBatch({async:false});
	} if (json["asociado"] == 'errorendoso') {
		dwr.engine.beginBatch();
		parent.mostrarMensajeInformativo("Error al generar el Endoso de Cambio de Agente","10");
		dwr.engine.endBatch({async:false});
	} 
	else{
		dwr.engine.beginBatch();
		parent.mostrarMensajeInformativo("Error al asociar el agente","10");
		dwr.engine.endBatch({async:false});
	}
	jQuery("#txtFechaHora").val(json["fechaHoraActualizFormatoMostrar"]);
	jQuery("#txtUsuario").val(json["usuarioActualizacion"]);
		listarFiltradoCarteraCliente();
		listarFiltradoCliente(false);
}

function validaNumeroAgente() {
	validacionService.numeroAgenteValido(dwr.util.getValue("agente.idAgente"),function(data){
		 if(!data){		
		 	parent.mostrarMensajeInformativo('El n\u00FAmero de agente '+dwr.util.getValue("agente.idAgente")+' es inv\u00E1lido o ya existente, favor de ingresar n\u00FAmero v\u00E1lido',"10");
		 	dwr.util.setValue("agente.idAgente","");
			 }
		 });
}

function listarFiltradoCarteraCliente(){
	gridAgenteCliente = new dhtmlXGridObject("agenteCarteraClientesGrid");
	var pathGrid="/MidasWeb/fuerzaventa/agente/listarFiltradoCarteraCliente.action";
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		gridAgenteCliente.load(pathGrid+"?agente.id="+dwr.util.getValue("agente.id")+"&tipoAccion="+dwr.util.getValue("tipoAccion") +
				"&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString"));
		
	}else
	{
		gridAgenteCliente.load(pathGrid+"?agente.id="+dwr.util.getValue("agente.id")+"&tipoAccion="+dwr.util.getValue("tipoAccion"));		
	}	
}

function listarFiltradoCliente(reload){
	var url="/MidasWeb/fuerzaventa/agente/listarFiltradoBusquedaCliente.action";	 
	var gridId = "clientesGrid";
	var forma = agenteCarteraClientesForm;
	var loadDiv="divCarga";
	grid = new dhtmlXGridObject(gridId);
	grid.attachEvent("onRowDblClicked", function(){
		var id=grid.getSelectedId();		
		var url="/MidasWeb/fuerzaventa/agente/clienteAsociado.action";
		var data={"agente.id":dwr.util.getValue("agente.id"),"clienteAgente.cliente.idCliente":grid.cellById(id, 0).getValue()};		
		jQuery.asyncPostJSON(url,data,populateAsociacion);		
	});
	grid.attachEvent("onXLS", function(grid,count){
		mostrarIndicadorCarga(loadDiv); 
	 });
	if (reload) {
		(forma!=null)?grid.load(url+"?"+jQuery("#agenteCarteraClientesForm").serialize()):grid.load(url);
	} else {
		grid.load(url);
	}
	
	grid.attachEvent("onXLE", function(grid,count){
		 ocultarIndicadorCarga(loadDiv);
	}); 
	 
}

function mostrarDetalleClienteAgente(){
	var url="/MidasWeb/catalogoCliente/mostrarDetalleCliente.action?moduloOrigen=1&tipoAccionAgente="+parent.dwr.util.getValue("tipoAccion")+"&idAgente="+parent.dwr.util.getValue("agente.id")+"&tipoAccion=1";
	sendRequestJQ(null,url,'contenido',null);
}

function autorizaAgente(){
	blockPage();
	var url="/MidasWeb/fuerzaventa/autorizacionagentes/autorizaAgente.action?idsAutorizar="+parent.dwr.util.getValue("agente.id");//agenteAutorizacion.id
	var data;
	jQuery.asyncPostJSON(url,data,populateAutorizaAgente);
	unblockPage();
}

function populateAutorizaAgente(json){
	if(json){
		var autorizacion=json.respuestaAutorizacion;
		if(autorizacion!=null){
			alert(autorizacion);
		}
		else{
			alert('Error al autorizar el agente, favor de intentarlo mas tarde');		
		}
	}
}

function findByIdAgenteReclutadorInit(idagente){
	var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
	var data={"agente.id":idagente};
	jQuery.asyncPostJSON(url,data,populateAgenteReclutadorInit);
}

function populateAgenteReclutadorInit(json){
	if(json){		
		var nombre = json.agente.persona.nombreCompleto;
		jQuery("#txtNombreReclutador").val(nombre);
	}
}

function atrasOSiguiente(tabActiva){
		if(dwr.util.getValue("tipoAccion")==1||dwr.util.getValue("tipoAccion")==4){
			parent.mostrarMensajeConfirm('Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?',"20","sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?idTipoOperacion=50&idRegistro='+parent.dwr.util.getValue('agente.id')+'&tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva="+tabActiva+"',targetWorkArea,'dhx_init_tabbars();')",null,null,null);
		}
		else{
			sendRequestJQ(null,'/MidasWeb/fuerzaventa/agente/verDetalle.action?idTipoOperacion=50&idRegistro='+parent.dwr.util.getValue('agente.id')+'&tipoAccion='+dwr.util.getValue('tipoAccion')+'&agente.id='+dwr.util.getValue('agente.id')+'&moduloOrigen='+parent.dwr.util.getValue('moduloOrigen')+'&tabActiva='+tabActiva,targetWorkArea,'dhx_init_tabbars();');
		}	
}

function guardarDatosExtraAgente(){	
	gridHijosAgente.setCSVDelimiter(',');	
	var listaHijosAgente=gridHijosAgente.serializeToCSV();
	
	gridEntretenimientosAgente.setCSVDelimiter(',');	
	var listaEntretenimientosAgente=gridEntretenimientosAgente.serializeToCSV();
	
	var path= "/MidasWeb/fuerzaventa/agente/guardarDatosExtra.action?"+jQuery("#guardarDatosExtraAgenteForm").serialize()+"&hijosAgente="+listaHijosAgente+"&entretenimientosAgente="+listaEntretenimientosAgente;
	sendRequestJQ(null, path, targetWorkArea, null);
}

function verClienteAgente(idCliente){
	var url="/MidasWeb/catalogoCliente/mostrarDetalleCliente.action?moduloOrigen=1&idAgente="+parent.dwr.util.getValue("agente.id")+"&cliente.idCliente="+idCliente+"&tipoAccion=2&tipoOperac="+parent.dwr.util.getValue("tipoAccion")+"&tipoAccionAgente="+parent.dwr.util.getValue("tipoAccion");
	sendRequestJQ(null,url,'contenido',null);
}

function editarClienteAgente(idCliente){
	var url="/MidasWeb/catalogoCliente/mostrarDetalleCliente.action?moduloOrigen=1&idAgente="+parent.dwr.util.getValue("agente.id")+"&cliente.idCliente="+idCliente+"&tipoAccion=4&tipoOperac="+parent.dwr.util.getValue("tipoAccion")+"&tipoAccionAgente="+parent.dwr.util.getValue("tipoAccion");
	sendRequestJQ(null,url,'contenido',null);
}

function salirAgente(){	
	var url = mostrarContenedorAgentePath;
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, url, targetWorkArea, null);
	}else{
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQ(null,'"+url+"',targetWorkArea,null)",null,null,null);
//		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
//			sendRequestJQAsync(null, url, targetWorkArea, null);
//		}
	}
}
function regresarAutorizacion(){	
	var url = mostrarAutorizacionAgentePath;
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, url, targetWorkArea, null);
	}else{
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQ(null,'"+url+"',targetWorkArea,null)",null,null,null);
//		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
//			sendRequestJQAsync(null, url, targetWorkArea, null);
//		}
	}
}


function eliminarAgente() {
//	if(confirm('¿Esta seguro de eliminar este agente?')){
	var path= "/MidasWeb/fuerzaventa/agente/eliminar.action?"+jQuery("#guardarInfoGeneral").serialize()+"&tipoAccion=3";
	parent.mostrarMensajeConfirm("¿Esta seguro de eliminar este agente?","20","sendRequestJQ(null,'"+path+"',targetWorkArea,null)");
//		sendRequestJQ(null, path, targetWorkArea, null);
//		}
	
}

//function quitarjQrequiredGenerales(){
//	jQuery("#txtCorreoElectronico").removeClass('jQrequired');
//	jQuery("#txtObservaciones").removeClass('jQrequired');
//	jQuery("#txtIdAgente").removeClass('jQrequired');
//	jQuery("#txtIdPersona").removeClass('jQrequired');
//	jQuery("#txtNombreAgente").removeClass('jQrequired');
//
//}
//function quitarjQrequiredDomicilio(){
//	jQuery("#agente.persona.domicilios[0].calleNumero").removeClass('jQrequired');
//	jQuery("#agente.persona.domicilios[0].codigoPostal").removeClass('jQrequired');
//	jQuery("#agente.persona.domicilios[1].calleNumero").removeClass('jQrequired');
//	jQuery("#agente.persona.domicilios[1].codigoPostal").removeClass('jQrequired');
//}

function cargaMasivaAgente(){
	var archivotxt = jQuery("#archivo").val();
	if(archivotxt.indexOf(".txt")!=-1) {
		document.formularioAltaMasivaAgente.submit();
	}else{
		parent.mostrarMensajeInformativo("Seleccione un archivo .txt","10");
	}
} 

function descargarArchivo(){
//	var path= "/MidasWeb/fuerzaventa/agente/downloadLogo.action";
//	sendRequestJQ(null, path, targetWorkArea, null);
	document.formularioDescargarLayout.submit();
}

function abrirLigaIfimax(){
    document.forms[0].action=dwr.util.getValue("urlIfimax");
    document.forms[0].submit();
}

function comparacionMidasMizar(){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/verComparacionImpComis.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}
function limpiaCampos(){
	jQuery("#tipoProducto").val("");
	jQuery("#numeroCuenta").val("");
	jQuery("#numeroClabe").val("");
	jQuery("#montoCredito").val("");
	jQuery("#numeroPlazos").val("");
	jQuery("#montoPago").val("");
	jQuery("#sucursal").val("");
	jQuery("#plaza").val("");
	jQuery("#claveDefault").removeAttr("checked");
}
function verPreviewBonos(){
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/verPreviewBonos.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function verProgramacionBonos(){
	var path ="/MidasWeb/fuerzaventa/programacionBono/mostrar.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function verConfigComisiones(){
	var path ="/MidasWeb/notificaciones/verNotificaciones.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function cargarValidacionManual(){
	var path ="/MidasWeb/recepcionDocs/verValidacionManual.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

//if (jQuery.browser.msie && jQuery.browser.version < 9){
//	
//	jQuery('select.wide').bind('focus mouseover', function() { 
//			jQuery(this).addClass('expand').removeClass('clicked'); 
//	}).bind('click', function() { 
//				jQuery(this).toggleClass('clicked');
//	
//	}).bind('mouseout', function() { 
//				if (!jQuery(this).hasClass('clicked')) { 
//						jQuery(this).removeClass('expand'); 
//				}	
//	}).bind('blur', function() {
//			 jQuery(this).removeClass('expand clicked'); 
//			 }); 
//}


function validaFechasDatosGenerales(){
	var fecAlta = jQuery("#txtFechafechaAlta").val();
	var fecBaja = jQuery("#txtFechaBaja").val();	
	if(fecAlta!="" && fecBaja!=""){
		if(compara_fecha(fecAlta,fecBaja)){
			alert("Fecha de vencimiento de cedula no puede ser menor a la Fecha de Autorizacion");
			jQuery("#txtFechaBaja").val("");
		}
	}	
}
function validaFechasDatosContables(){
	var fechaAutorizacion = jQuery("#fechaAutorizacion").val();
	var fechaVencimiento = jQuery("#fechaVencimiento").val();
	if(fechaAutorizacion!="" && fechaVencimiento!=""){
		if(compara_fecha(fechaAutorizacion,fechaVencimiento)){
			alert("Fecha de vencimiento no puede ser menor a la Fecha de Autorizacion ");
			jQuery("#fechaVencimiento").val("");
		}
	}	
}
function compara_fecha(fechaIncio,fechaFin){
	var fechaIncio1=new Date(fechaIncio.substring(6,10),fechaIncio.substring(3,5)-1,fechaIncio.substring(0,2));
	var anioAlta=fechaIncio1.getFullYear();
	var mesAlta =fechaIncio1.getMonth()+1;
	var diaAlta =fechaIncio1.getDate();
	
	var fechaFin1=new Date(fechaFin.substring(6,10),fechaFin.substring(3,5)-1,fechaFin.substring(0,2));
	var anioFin=fechaFin1.getFullYear();
	var mesFin =fechaFin1.getMonth()+1;
	var diaFin =fechaFin1.getDate();
	
	if(anioAlta>anioFin){
//		alert(anioAlta+">"+anioFin);
		return true;
	}
	if(anioAlta==anioFin && mesAlta>mesFin){
//		alert(anioAlta+"=="+anioFin +" && "+ mesAlta+">"+mesFin);
		return true;
	} 
	if(anioAlta==anioFin && mesAlta==mesFin){
//		alert(anioAlta+"=="+anioFin +" && "+ mesAlta+"=="+mesFin);
		if(diaAlta>diaFin){
//			alert(diaAlta+">"+diaFin);
			return true;
		}
	}
	return false;
}

function muestraSoloOpcionPermitidaAgente(){	
	var estatus = jQuery("#estatusAgenteSituacion option:selected").text();	  
	var url="/MidasWeb/fuerzaventa/agente/mostrarMotEstatusXSituacionDelAgente.action?situacionAgenteString="+estatus;
	var data="";
	jQuery.asyncPostJSON(url,data,returnMuestraSoloOpcionPermitidaAgente);
}
function returnMuestraSoloOpcionPermitidaAgente(json){	
	var opt="<option value=''>SELECCIONE...</option>";		
	var estatus = jQuery("#estatusAgenteSituacion option:selected").val();	
	if(estatus==""){
		jQuery("#idMotivoEstatusAgente").html(opt);
	}else{
		for(i=0;i<json.catalogoMotivoEstatus.length;i++){
			var jsonList=json.catalogoMotivoEstatus[i];
			var val=eval('jsonList.valor');
			var id=eval('jsonList.id');		
			opt+="<option value='"+id+"'>"+val+"</option>";
			jQuery("#idMotivoEstatusAgente").html(opt);
		}	
	}
	
}

function cambiarTipoAgente(){
	var claveTipoPersona = jQuery("#claveTipoPersona").val();
	var clasifAgenteSeleccionado = jQuery("#txtClasifAgente option:selected").html();
	if(clasifAgenteSeleccionado != "BANCA DE GOBIERNO" && clasifAgenteSeleccionado != "NEGOCIOS ESPECIALES VIDA" && clasifAgenteSeleccionado != "BANCA SEGUROS" && clasifAgenteSeleccionado != "NEGOCIOS ESPECIALES REASEGURO" &&  clasifAgenteSeleccionado != "VENTA DIRECTA"){
		if (claveTipoPersona==1){
			jQuery("#txtTipoAgente option[text=PERSONA FÍSICA]").attr("selected",true);
		}else {
			jQuery("#txtTipoAgente option[text=PERSONA MORAL]").attr("selected",true);
		}
	}
}

function deshabilitarClasificacionAgentes(){
	 var deshabilitarOption;
	 var opcionSeleccionada = jQuery("#txtClasifAgente option:selected").text();
	 if(jQuery("#estatusAgenteSituacion option:selected").text()=='AUTORIZADO'){
		if(opcionSeleccionada=='BANCA DE GOBIERNO' || opcionSeleccionada=='NEGOCIOS ESPECIALES VIDA'||opcionSeleccionada=='NEGOCIOS ESPECIALES REASEGURO' || opcionSeleccionada=='BANCA SEGUROS'||opcionSeleccionada=='VENTA DIRECTA'){
			deshabilitarOption = ['BAJA', 'AGENTE DE PROMOTORIA SP', 'AGENTE VENTA ESPECIAL', 'AGENTE LIDER DE PROMOTORIA DESARROLLO', 'AGENTE DE PROMOTORIA DESARROLLO', 'AGENTE LIDER DE PROMOTORIA', 'ALTA POR INTERNET', 'AGENTE DE PROMOTORIA','AGENTE ESPECIAL','AGENTE DIRECTO','Seleccione..'];
		}
		else{
			deshabilitarOption = ['BANCA DE GOBIERNO', 'NEGOCIOS ESPECIALES VIDA', 'NEGOCIOS ESPECIALES REASEGURO', 'BANCA SEGUROS', 'VENTA DIRECTA','Seleccione..'];
		}
		jQuery.each(deshabilitarOption , function(index, value) {
		      jQuery("#txtClasifAgente option[text="+this+"]").attr('disabled','disabled');
		   });
	}
}