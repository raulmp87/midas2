function onChangeIdAgente(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/fuerzaventa/reportePreviewBonos/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}else{
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#nombreAgente").val("");
	}
}

function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/reportePreviewBonos/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	var agente=json.agente;
	if(json){		
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreAgente = agente.persona.nombreCompleto;
		
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#nombreAgente").val(nombreAgente);
	}
}

function buscarAgentesParaGeneracionDocs(validarCampos)
{	
	
	mostrarIndicadorCarga('indicador');
	var fechaValidacion;	
	
	document.getElementById('agentesDocumentosGrid').innerHTML = '';	
	agentesDocumentosGrid = new dhtmlXGridObject('agentesDocumentosGrid');
		
	agentesDocumentosGrid.attachEvent("onXLE", function(agentesDocumentosGrid){
		ocultarIndicadorCarga('indicador');
    });		
		
	var serializedForm = jQuery(document.generarDocumentosForm).serialize();
	serializedForm = serializedForm.replace(/listaAgentes/g,'agenteList');
		
	var posPath = '&'+ serializedForm
	
	
	var url = buscarAgentesParaGenDocsPath + "?" + posPath;	
	
	agentesDocumentosGrid.load(url, function(){
		for (var i=0; i<agentesDocumentosGrid.getRowsNum(); i++) agentesDocumentosGrid.render_row(i);
	});	
}

function imprimirReporte(idAgente){
	var url="/MidasWeb/fuerzaventa/reporteAgenteResumenDetalleBonos/exportarToExcel.action?agente.idAgente="+idAgente;
	window.open(url, "Pagare");
}