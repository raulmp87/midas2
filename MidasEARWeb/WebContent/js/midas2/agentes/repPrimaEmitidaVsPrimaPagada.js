/**
 * Lista las gerencias por centro operacion
 * 
 * @param idCentroEmisor
 */
function loadGerenciasByCentroOperacion() {
	var idCombos="gerenciaList,ejecutivoList,promorotiaList";
	reiniciarCombos(idCombos);
	var idCentroOperacion = jQuery("#centroOperacion").val();
	listadoService
			.listarGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						llenarCombo(data, "gerenciaList",
								"gerenciasSeleccionadas", "descripcion",
								"gerenciaList");
						addToolTipOnchange("gerenciaList");
					});
}
function reiniciarCombos(idCombos){
	var i;
	var combo=idCombos.split(","); 
	var nCombos = combo.length;
	for(i=0; i<nCombos;i++){
			jQuery("#"+combo[i]).empty();
			jQuery("#"+combo[i]).append('<option value="" selected="selected">Seleccione..</option>');
		}
}
/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerencia() {
	var idCombos="ejecutivoList,promorotiaList";
	reiniciarCombos(idCombos);
	var idGerencias = jQuery("#gerenciaList").val();
	listadoService.listarEjecutivoPorGerencia(idGerencias, function(data) {
		llenarCombo(data, "ejecutivoList", "ejecutivosSeleccionados",
				"nombreCompleto", "ejecutivoList",
				"loadPromotoriaByEjecutivo()");
		addToolTipOnchange("ejecutivoList");
	});
}
/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivo() {
	var idCombos="promorotiaList";
	reiniciarCombos(idCombos);
	var idEjecutivos = jQuery("#ejecutivoList").val();
	listadoService.listarPromotoriaPorEjecutivo(idEjecutivos, function(data) {
		llenarCombo(data, "promorotiaList", "promotoriasSeleccionadas",
				"descripcion", "promorotiaList", null);
		addToolTipOnchange("promorotiaList");
	});

}

function llenarCombo(data, targetLoad, nameObj, property, selectedItems,propertyId) {
	if (data != null) {
		jQuery("#"+targetLoad).empty();
		jQuery("#"+targetLoad).append('<option value="" selected="selected">Seleccione....</option>');
		for ( var x = 0; x < data.size(); x++) {
		     if (property == "descripcion") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].descripcion+'</option>');
				}else if (property == "nombreCompleto") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].personaResponsable.nombreCompleto+'</option>');
				}else if (property == "valor") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].valor+'</option>');
				}
		    }
	}
}

function onChangeIdAgente(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/fuerzaventa/repPrimaPagadaVsPrimaEmitida/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}else{
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#nombreAgente").val("");
	}
}

function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/repPrimaPagadaVsPrimaEmitida/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/reportePreviewBonos/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	var agente=json.agente;
	if(json){		
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreAgente = agente.persona.nombreCompleto;
		
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#nombreAgente").val(nombreAgente);
	}
}