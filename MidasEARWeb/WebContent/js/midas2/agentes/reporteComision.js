/**
 * Lista las gerencias por centro operacion
 * 
 * @param idCentroEmisor
 */
function loadGerenciasByCentroOperacion() {
	var idCentroOperacion = checkInputs("operacionesList");
	listadoService
			.listarGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						makeCheck(data, "gerenciaList",
								"configComisiones.listaGerenciaView",
								"descripcion", "gerenciaList",
								"loadEjecutivoByGerencia();hiddenList('gerenciaList');");
					});
}

/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerencia() {
	var idGerencias = checkInputs("gerenciaList");
	listadoService.listarEjecutivoPorGerencia(idGerencias, function(data) {
		makeCheck(data, "ejecutivoList", "configComisiones.listaEjecutivoView",
				"nombreCompleto", "ejecutivoList",
				"loadPromotoriaByEjecutivo()");
	});
}

/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivo() {
	var idEjecutivos = checkInputs("ejecutivoList");
	listadoService.listarPromotoriaPorEjecutivo(idEjecutivos, function(data) {
		makeCheck(data, "promorotiaList",
				"configComisiones.listaPromotoriaView", "descripcion", "promorotiaList",
				null);
	});

}