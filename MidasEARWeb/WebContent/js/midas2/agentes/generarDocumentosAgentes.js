	var guiaHonorariosGrid;
	
	jQuery(document).ready(function(){
		
		validarSiEsPromotor();
		
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		});
		
		listadoService.getMapYears(2015,9999, function(data) {
			 addOptionOrderdesc("anios", data)
		});
		
		
	});
	
	
	function addOptionOrderdesc(target, map){
		jQuery("#"+target).append("<option value=''>Seleccione...</option>");
		var a = 0;
		var arr1 = new Array();
		for (var key in map) {
			arr1[a]= key;
			a++;
		}
		for(var b = arr1.length; b>0 ;b--){
			jQuery("#"+target).append("<option value="+arr1[b-1]+">"+arr1[b-1]+"</option>");
		}
	}

	function validarSiEsPromotor() {
		
		var isPromotor = (jQuery("#agentes").length > 0);
					
		if (isPromotor) {
			
			cargarCboAgentes();
			
		}

	}	
	
	function setAgente(id){
		jQuery("#idRegAgente").val(id);
	}

	function verDocumento(idAgente)
	{
		if(validarCamposMandatorios())
		{
			if(idAgente != null && idAgente != '' && idAgente != 0){
				var serializedForm = jQuery(document.generarDocumentosForm).serialize();	
				window.open(verDocumentoPath + "?"+ serializedForm +'&idAgenteSeleccionado=' + idAgente,"Documento");
			}else{
				mostrarMensajeInformativo("Favor de seleccionar el Agente", "20", null);			
			}
		}
	}	

	function enviarReportePorCorreo()
	{
		if(validarCamposMandatorios())
		{	
			var selectedRowsIds = jQuery("#idRegAgente").val(); 
			
			if (selectedRowsIds != null && selectedRowsIds != '' && selectedRowsIds != 0){
				var serializedForm = jQuery(document.generarDocumentosForm).serialize();
				sendRequestJQ(null, enviarDocsPath + "?"+ serializedForm +'&idsSeleccionados=' + selectedRowsIds,null,notificarEnvioCorreo());
			}else{
				mostrarMensajeInformativo("Favor de seleccionar el Agente", "20", null);			
			}
		}
	}	
	
	function validarCamposMandatorios()
	{
		var idSTipoDocSel = dwr.util.getValue("tiposDocs");
		var idMesSel = dwr.util.getValue("meses");
		var idAnioSel = dwr.util.getValue("anios");
		
		if(idSTipoDocSel == '' || idMesSel == '' || idAnioSel == '')
		{
			mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios ", "20", null);
			return false;
		}
		
		return true;	
	}	
	
	function notificarEnvioCorreo(){
		mostrarMensajeInformativo("El envío de correos se está procesando ", "20", null);
	}
	
	jQuery("#meses").change(function(){
		validateReglaDescargar();
	});
	
	function validateReglaDescargar(){
		var idMesSel = dwr.util.getValue("meses");
		var currentDate = new Date();
		
		var currentMonth = currentDate.getMonth() + 1;
		var currentDay = currentDate.getDate();
		var tipoDocumento = jQuery("#tiposDocs option:selected").text();
		
		if (currentMonth == idMesSel && currentDay < 28 && tipoDocumento == "Estado de Cuenta"){
			jQuery(".btn_back").hide();
		}else{
			jQuery(".btn_back").show();
		}
	}
	
	function ocultarFormatoSalida(){
		var tipoDocumento = jQuery("#tiposDocs option:selected").text();
		if (tipoDocumento == "Detalle de Primas"){
			jQuery("#txfFormatoSalida select").empty();
			jQuery("#txfFormatoSalida select").append(jQuery('<option>', {
			    value: 'xls',
			    text: 'Excel'
			}));	
			jQuery("#txfFormatoSalida select").append(jQuery('<option>', {
				value: 'pdf',
		     	text: 'PDF'
			}));
			
			jQuery(".btn_back").show();
		} else{
			jQuery("#txfFormatoSalida select").empty();
			jQuery("#txfFormatoSalida select").append(jQuery('<option>', {
				value: 'pdf',
				text: 'PDF'
			}));
			
			validateReglaDescargar();
		}
	}
	
	function validarTipoReciboHonorarios() {
		var tipoDocumento = jQuery("#tiposDocs option:selected").text();
		
		if (tipoDocumento == "Recibo de Honorarios") {
			document.getElementById('botonTxt').innerHTML = "Generar Documentos";
			document.getElementById('botonTxt').setAttribute('onclick','generarDocumentosReciboHonorarios();');
			document.getElementById('botonCorreoTxt').style.visibility = "hidden";
			document.getElementById('botonCorreo2Txt').style.visibility = "visible";
		} else {
			document.getElementById('botonTxt').innerHTML = "Generar Reporte";
			document.getElementById('botonTxt').setAttribute('onclick','verDocumento(document.getElementById("idRegAgente").value);');
			document.getElementById('botonCorreoTxt').style.visibility  = "visible";
			document.getElementById('botonCorreo2Txt').style.visibility = "hidden";
		}
		
	}
	
	function enviarRecibosHonorariosPorCorreo() {
		if(validarCamposMandatorios()){
			var selectedRowsIds = guiaHonorariosGrid.getCheckedRows(0);
			var tipoDoc = jQuery("#tiposDocs").val();
			var archivo = jQuery("#formatosal").val();
			var mes = jQuery("#meses").val();
			var anio = jQuery("#anios").val();
			var idAgente = jQuery("#agentes").val();
			
			sendRequestJQ(null, enviarDocsPath + "?"+ 'tipoDocumento=' + tipoDoc +'&tipoSalidaArchivo=' +archivo +'&mes='+mes +'&anio='+anio +"&idAgente=" +idAgente +'&idsSeleccionados=' + selectedRowsIds,null,mostrarMensajeInformativo("El envío de correos se está procesando ", "20", generarDocumentosReciboHonorarios()));
		}
	}
	
	function generarDocumentosReciboHonorarios() {
		
		if(!validarCamposMandatorios()) {
		    return false;	
		}
		
		mostrarIndicadorCarga('indicador');
		
		document.getElementById('guiaHonorariosGrid').innerHTML = '';	
		guiaHonorariosGrid = new dhtmlXGridObject('guiaHonorariosGrid');
			
		guiaHonorariosGrid.attachEvent("onXLE", function(guiaHonorariosGrid){
			ocultarIndicadorCarga('indicador');
	    });		
			
		var tipoDoc = jQuery("#tiposDocs").val();
		var archivo = jQuery("#formatosal").val();
		var mes = jQuery("#meses").val();
		var anio = jQuery("#anios").val();
		var idAgente = jQuery("#agentes").val();
		
		if (idAgente === undefined) {
			idAgente = 0;
		}
		
		var url;
        if ( idAgente == 0 ) {
            url = buscarAgentesParaGenDocsPath + "?" + 'tipoDocumento=' + tipoDoc +'&tipoSalidaArchivo=' +archivo +'&mes='+mes +'&anio='+anio;
        }else{
            url = buscarAgentesParaGenDocsPath + "?" + 'tipoDocumento=' + tipoDoc +'&tipoSalidaArchivo=' +archivo +'&mes='+mes +'&anio='+anio +"&agenteList[0].id=" +idAgente;
        }

		guiaHonorariosGrid.load(url, function(){
			for (var i=0; i<guiaHonorariosGrid.getRowsNum(); i++) guiaHonorariosGrid.render_row(i);
			if(guiaHonorariosGrid.getRowsNum() > 0){
				jQuery("#agentesDocumentosGrid input:checkbox").attr('checked',true);			
			}
			guiaHonorariosGrid.checkAll(true);
		});	
	}
	
	function verGuiaReciboHonorarios(solicitudChequeId) {
		var serializedForm = jQuery(document.generarDocumentosForm).serialize();	
		
		window.open(verDocumentoPath + "?"+ serializedForm +"&solicitudChequeId="+ solicitudChequeId);
	}