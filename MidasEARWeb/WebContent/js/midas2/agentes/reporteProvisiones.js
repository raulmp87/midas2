function onChangeLineaVenta(){
	var idSeleccionados="";
	jQuery("#ajax_listaLineaVenta input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});

		dwr.engine.beginBatch();
		listadoService.listaProducto(idSeleccionados,idNexList,
				function(data){
			comboChecksCreateCascade(data,'ajax_listaProductos','listaProductosSeleccionados','onChangeProducto',"idProducto");
				});
		dwr.engine.endBatch({async:false});
		onChangeProducto();
		
}

function onChangeProducto(){
	var idSeleccionados="";
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_listaRamos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();
		listadoService.listaRamo(idSeleccionados,idNexList,
				function(data){
				comboChecksCreateCascade(data,'ajax_listaRamos','listaRamosSeleccionados','onChangeRamo',"idRamo");
				});
		dwr.engine.endBatch({async:false});
		onChangeRamo();
}

function onChangeRamo(){
	var idSeleccionados="";
	jQuery("#ajax_listaRamos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_listaSubRamos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();
		listadoService.listaSubramo(idSeleccionados,idNexList,
				function(data){
					comboChecksCreateCascade(data,'ajax_listaSubRamos','listaSubRamosSeleccionados',null,"idSubramo");
				});
		dwr.engine.endBatch({async:false});
		onChangeRamo_LineaNegocio();
}

function onChangeRamo_LineaNegocio(){
	var idSeleccionados="";
	jQuery("#ajax_listaRamos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_lineaNegocio input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();
		listadoService.listaLineaNegocio(idSeleccionados,idNexList,
				function(data){
					comboChecksCreateCascade(data,'ajax_lineaNegocio','listaLineaNegocioSeleccionados','onChangeLineaNegocio',"idSeccion");
				});
		dwr.engine.endBatch({async:false});
		onChangeLineaNegocio();
}

function onChangeLineaNegocio(){
	var idSeleccionados="";
	jQuery("#ajax_lineaNegocio input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_listaCoberturas input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();
		listadoService.listaCoberturas(idSeleccionados,idNexList,
				function(data){
					comboChecksCreateCascade(data,'ajax_listaCoberturas','listaCoberturasSeleccionadas',null,"idCobertura");
				});
		dwr.engine.endBatch({async:false});
}

function comboChecksCreateCascade(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"();";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li><label for="'+key+'">';
					option+='<input type="checkbox" name="'+nameObj+'['+i+'].'+property+'" id="'+nameObj+'['+i+'].'+property+'" value="'+key+'" ';
					if(check==1){
						option+= "checked='checked'";
						}
					option+='onchange="'+fn+'" />';
					option+= value + '</label>';
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}


function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/reporteProvision/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	var agente=json.agente;
	if(json){		
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreAgente = agente.persona.nombreCompleto;
		
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#nombreAgente").val(nombreAgente);
	}
}
