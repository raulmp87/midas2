var confAgenteGrid;
var confPromotoriaGrid;

function requestConfigMediciones(actionURL, fobj, pNextFunctionSuccess, pNextFunctionComplete) {

	blockPage();
	jQuery.ajax({
	    type: "GET",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: "json",
	    async: true,
	    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
	    success: function(data) {
	    	if (data != null) {
		    	if (data.respuesta == "OK") {
		    		
		    		if (typeof pNextFunctionSuccess == "string") {
						eval(pNextFunctionSuccess);
					} else if (typeof pNextFunctionSuccess == "function") {
						pNextFunctionSuccess();
					}
		    		
		    	} else {
		    	
		    		alert(data.respuesta);
		    		
		    	}
	    	}
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    	
	    	if (typeof pNextFunctionComplete == "string") {
				eval(pNextFunctionComplete);
			} else if (typeof pNextFunctionComplete == "function") {
				pNextFunctionComplete();
			}

	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   	    
	});

}

function agregarConfAgente() {
	
	if (jQuery('#idAgente').val() == undefined || jQuery('#idAgente').val() == null  || jQuery.trim(jQuery('#idAgente').val()) == ''  
			|| jQuery('#emailAgente').val() == undefined || jQuery('#emailAgente').val() == null || jQuery.trim(jQuery('#emailAgente').val()) == '') {
		
		return alert("Tiene que llenar todos los campos");
		
	}
		
	var vUrl = urlAgregarAgente;
	
	requestConfigMediciones(vUrl, '#configMedicionesForm', agregarConfAgenteCallback, clearAgenteForm);
	
}

function agregarConfAgenteCallback() {
			
	confAgenteGrid = document.getElementById('confAgenteGrid').grid;
			
//	confAgenteGrid.addRow(confAgenteGrid.uid(),[jQuery('#idAgente').val(),jQuery('#nombreAgente').val(),jQuery('#emailAgente').val(), 1],0);
	
	//loadGrid('confAgenteGrid', confAgenteGrid);
	loadGrid('confAgenteGrid');
	
}

function clearAgenteForm() {
	
	//Limpia campos
	
	jQuery('#idAgente').val('');
	
	jQuery('#nombreAgente').val('');
		
	jQuery('#emailAgente').val('');
	
}

function agregarConfPromotoria() {
	
	if (jQuery('#idPromotoria').val() == undefined || jQuery('#idPromotoria').val() == null  || jQuery.trim(jQuery('#idPromotoria').val()) == ''  
			|| jQuery('#emailPromotoria').val() == undefined || jQuery('#emailPromotoria').val() == null || jQuery.trim(jQuery('#emailPromotoria').val()) == '') {
		
		return alert("Tiene que llenar todos los campos");
		
	}

	var vUrl = urlAgregarPromotoria;
	
	requestConfigMediciones(vUrl, '#configMedicionesForm', agregarConfPromotoriaCallback, clearPromotoriaForm);
	
}

function agregarConfPromotoriaCallback() {
	
	confPromotoriaGrid = document.getElementById('confPromotoriaGrid').grid;
	
//	confPromotoriaGrid.addRow(confPromotoriaGrid.uid(),[jQuery('#idPromotoria').val(),jQuery('#nombrePromotoria').val(),jQuery('#emailPromotoria').val(), 1],0);
		
	//loadGrid('confPromotoriaGrid', confPromotoriaGrid);
	loadGrid('confPromotoriaGrid');
		
}

function clearPromotoriaForm() {
	
	//Limpia campos
	
	jQuery('#idPromotoria').val('');
	
	jQuery('#nombrePromotoria').val('');
	
	jQuery('#emailPromotoria').val('');
	
}



function agregarCorreosAdicionales() {

	var vUrl = urlAgregarCorreosAdicionales;
	
	requestConfigMediciones(vUrl, '#configMedicionesForm');
	
}


function eliminarConfAgente(idAgente, rowId) {

	var vUrl = urlEliminarAgente + '?configAgente.agente.idAgente=' + idAgente;
	
	requestConfigMediciones(vUrl, null, 'eliminarConfAgenteCallback(' + rowId + ')');

	
	
}

function eliminarConfAgenteCallback(rowId) {
	
	confAgenteGrid = document.getElementById('confAgenteGrid').grid;
	
	confAgenteGrid.deleteRow(rowId);
	
}

function eliminarConfPromotoria(idPromotoria, rowId) {

	var vUrl = urlEliminarPromotoria + '?configPromotoria.promotoria.idPromotoria=' + idPromotoria;
	
	requestConfigMediciones(vUrl, null, 'eliminarConfPromotoriaCallback(' + rowId + ')');
	
	
}

function eliminarConfPromotoriaCallback(rowId) {
	
	confPromotoriaGrid = document.getElementById('confPromotoriaGrid').grid;
	
	confPromotoriaGrid.deleteRow(rowId);
	
}

function habilitarConfAgente_chk(rowId, colIndex, chkState) {
	
	confAgenteGrid = document.getElementById('confAgenteGrid').grid;
	
	var id = confAgenteGrid.cells(rowId, 0).getValue();
		
	var vUrl = urlHabilitarAgente + '?configAgente.agente.idAgente=' + id;
	
	requestConfigMediciones(vUrl);
			
}


function habilitarConfPromotoria_chk(rowId, colIndex, chkState) {
	
	confPromotoriaGrid = document.getElementById('confPromotoriaGrid').grid;
	
	var id = confPromotoriaGrid.cells(rowId, 0).getValue();
	
	var vUrl = urlHabilitarPromotoria + '?configPromotoria.promotoria.idPromotoria=' + id;
	
	requestConfigMediciones(vUrl);
	
}


function actualizarConfAgente(rowId, colIndex, newValue) {
	
	if (colIndex == 2) {
		
		confAgenteGrid = document.getElementById('confAgenteGrid').grid;
		
		var id = confAgenteGrid.cells(rowId, 0).getValue();
			
		var vUrl = urlActualizarAgente + '?configAgente.agente.idAgente=' + id + '&configAgente.email=' + newValue;
		
		requestConfigMediciones(vUrl);
		
	}
	
}

function actualizarConfPromotoria(rowId, colIndex, newValue) {
	
	if (colIndex == 2) {
		
		confPromotoriaGrid = document.getElementById('confPromotoriaGrid').grid;
		
		var id = confPromotoriaGrid.cells(rowId, 0).getValue();
		
		var vUrl = urlActualizarPromotoria + '?configPromotoria.promotoria.idPromotoria=' + id + '&configPromotoria.email=' + newValue;
		
		requestConfigMediciones(vUrl);
		
	}
	
}

function enviarCorreosReporteSemanal() {

	var vUrl = urlEnviarCorreosReporteSemanal;
	
	requestConfigMediciones(vUrl, null, enviarCorreosReporteSemanalCallback);
	
}

function enviarCorreosReporteSemanalCallback() {

	alert("Se ha ejecutado el proceso de enviado de correos electronicos");
	
}


////////////////////////////////////////////////////
//Modales de ayuda
////////////////////////////////////////////////////

var ventanaBusquedaAgente;

function pantallaModalBusquedaAgente() {
	
	//var url = "/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgenteBusqueda";
	var vUrl = urlContenedorAyudaAgentes + '?tipoAccion=consulta&idField=txtIdAgenteBusqueda';
	
	sendRequestWindow(null, vUrl, obtenerVentanaBusquedaAgente);
}


function obtenerVentanaBusquedaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaBusquedaAgente= wins.createWindow("agenteModal", 10, 320, 900, 450);
	ventanaBusquedaAgente.centerOnScreen();
	ventanaBusquedaAgente.setModal(true);
	ventanaBusquedaAgente.setText("Buscar Agente");
	return ventanaBusquedaAgente;
}

function addAgenteProduccion(){
	
	var busIdAgente = jQuery("#txtIdAgenteBusqueda").val();
	var data = {"agente.id":busIdAgente};
	
	//var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
	var vUrl = urlSeleccionarAgente;
	
	jQuery.asyncPostJSON(vUrl,data,responseAgenteProduccion);
	
}

function responseAgenteProduccion(json){
	
	//var idAgente = json.agente.id;
	var claveAgente = json.agente.idAgente;
	var nombreAgente = json.agente.persona.nombreCompleto;
	
	if(claveAgente != null){
		
		jQuery('#idAgente').val(claveAgente);
		
		jQuery('#nombreAgente').val(nombreAgente);
		
	}			
}

////////////////////////////////////////////////////

var ventanaPromotoria;

function pantallaModalBusquedaPromotoria(){
	
	//var url="/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdBeneficiarioPromotoria";
	var vUrl = urlContenedorAyudaPromotorias + '?tipoAccion=consulta&idField=txtIdBeneficiarioPromotoria';
	
	sendRequestWindow(null, vUrl,obtenerVentanaBusquedaPromotoria);
	
}

function obtenerVentanaBusquedaPromotoria(){
	var wins = obtenerContenedorVentanas();
	ventanaPromotoria= wins.createWindow("promotoriaModal", 10, 320, 900, 450);
	ventanaPromotoria.centerOnScreen();
	ventanaPromotoria.setModal(true);
	ventanaPromotoria.setText("Buscar Promotoria");
	return ventanaPromotoria;
}

function addPromotoriaBeneficiaria(){
	
	var idPromotoria = jQuery("#txtIdBeneficiarioPromotoria").val();
	var data = {"promotoria.id":idPromotoria};
	
	//var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPromotoria.action";
	var vUrl = urlSeleccionarPromotoria;

	jQuery.asyncPostJSON(vUrl,data,responsePromotoriaBenef);
}

function responsePromotoriaBenef(json){
	
	var idPromotoria = json.promotoria.idPromotoria;
	var promotoria = json.promotoria.descripcion;
	
	if(idPromotoria!=null) {
		
		jQuery('#idPromotoria').val(idPromotoria);
		
		jQuery('#nombrePromotoria').val(promotoria);
		
	}		
	
}

