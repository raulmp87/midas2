/**
 * 
 */

var agentesDocumentosGrid;
var TIPO_MENSAJE_EXITO= "30";
var TIPO_MENSAJE_WARNING = "20";
var TIPO_MENSAJE_ERROR = "10";

/**
 * Lista las gerencias por centro operacion
 */
function loadGerenciasByCentroOperacionCascada() {
	var idCentroOperacion = checkInputs("operacionesList");	
	listadoService
			.listarGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						makeCheck(data, "gerenciaList",
								"gerenciasSeleccionadas", "descripcion",
								"gerenciaList",
								"loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');");
					});
}

/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerenciaCascada() {	
	var idGerencias = checkInputs("gerenciaList");	
	listadoService.listarEjecutivoPorGerencia(idGerencias, function(data) {
		makeCheck(data, "ejecutivoList", "ejecutivosSeleccionados",
				"nombreCompleto", "ejecutivoList",
				"loadPromotoriaByEjecutivoCascada()");
	});
}

/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivoCascada() {
	var idEjecutivos = checkInputs("ejecutivoList");
	listadoService.listarPromotoriaPorEjecutivo(idEjecutivos, function(data) {
		makeCheck(data, "promotoriaList", "promotoriasSeleccionadas",
				"descripcion", "promotoriaList", null);
	});
}


//Cargar Combos Año y mes
jQuery(document).ready(function() {	
		
	var tipoUsuario = jQuery('#tipoUsuario').val();
	var numeroAnios = tipoUsuario == "ADMIN" ? 4:1;	
	
	listadoService.getMapMonths(function(data) {
		addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
	})
		
	listadoService.getMapYears(numeroAnios, function(data) {
		addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
	});
	
	ocultarMostrarComponentes();		
	
});


function buscarAgentesParaReporte(validarCampos)
{	
	if(validarCampos == 1)
	{
		if(!validarCamposMandatorios())
		{
		    return false;	
		}
	}
	
	mostrarIndicadorCarga('indicador');
	//var fechaValidacion;	
	
	document.getElementById('agentesDocumentosGrid').innerHTML = '';	
	agentesDocumentosGrid = new dhtmlXGridObject('agentesDocumentosGrid');
		
	agentesDocumentosGrid.attachEvent("onXLE", function(agentesDocumentosGrid){
		ocultarIndicadorCarga('indicador');
    });		
		
	var serializedForm = jQuery(document.generarDocumentosForm).serialize();
	serializedForm = serializedForm.replace(/listaAgentes/g,'agenteList');
		
	var posPath = '&'+ serializedForm
	
	
	var url = buscarAgentesReportePath + "?" + posPath;	
	
	agentesDocumentosGrid.load(url, function(){
		for (var i=0; i<agentesDocumentosGrid.getRowsNum(); i++) agentesDocumentosGrid.render_row(i);
		
		if(agentesDocumentosGrid.getRowsNum() > 0)
		{
			jQuery("#agentesDocumentosGrid input:checkbox").attr('checked',true);			
		}
		
		agentesDocumentosGrid.checkAll(true);			
	});	
}

function validarCamposMandatorios()
{
	var idTipoBeneficiarioSel = dwr.util.getValue("tiposBeneficiarios");
	var idMesSel = dwr.util.getValue("meses");
	var idAnioSel = dwr.util.getValue("anios");
	var tipoUsuario = jQuery('#tipoUsuario').val();
	
	if((tipoUsuario != 'AGENTE' && idTipoBeneficiarioSel == '') || idMesSel == '' || idAnioSel == '')
	{
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios ", "20", null);
		return false;
	}
	
	return true;	
}

function descargarReporte(idBeneficiario)
{
	if(validarCamposMandatorios())
	{
		var tipoUsuario = jQuery('#tipoUsuario').val();
		var tipoBeneficiario = tipoUsuario == "AGENTE" ? "":jQuery('#tiposBeneficiarios').val();
		var mes = dwr.util.getValue("meses");
		var anio = dwr.util.getValue("anios");
		
		if(tipoUsuario == 'AGENTE')
		{
			idBeneficiario = jQuery('#idAgenteUsuario').val();	
			
		}else if(tipoBeneficiario == "838" && tipoUsuario == 'PROMOTOR')
		{
			idBeneficiario = jQuery('#idPromotoria').val();			
		}
		
		window.open(descargarReportePath + '?' + 'idBeneficiario=' + idBeneficiario + '&tipoBeneficiario=' + tipoBeneficiario 
				+ '&anio=' + anio + '&mes=' + mes + '&tipoUsuario=' + tipoUsuario,"Reporte");	   	
	}
}

function ocultarMostrarComponentes()
{		
	var tipoBeneficiario = jQuery('#tiposBeneficiarios').val();
	var tipoUsuario = jQuery('#tipoUsuario').val();
	
	if(tipoUsuario == "ADMIN")
	{
		jQuery(".btnDescargar").hide();	
		
		if(tipoBeneficiario == "838")//Promotor
		{
			jQuery(".filtrosAplicablesAgente").hide();
			
		}else
		{
			jQuery(".filtrosAplicablesAgente").show();
			
		}
	}	
	else if(tipoUsuario == "PROMOTOR")
	{
		if(tipoBeneficiario == "839")//Agente
		{
			jQuery(".contenedorFormas tr")[2].show();
			jQuery(".btnBuscar").show();
			jQuery(".btnEnvCorreo").show();		
			jQuery(".btnDescargar").hide();
			
		}
		else
		{
			jQuery(".contenedorFormas tr")[2].hide(); 
			jQuery(".btnBuscar").hide();
			jQuery(".btnEnvCorreo").hide();	
			jQuery(".btnDescargar").show();
		}		
	}else if(tipoUsuario == "AGENTE")
	{
		jQuery(".btnBuscar").hide();
	}else
	{
		jQuery(".btnBuscar").hide();
		jQuery(".btnEnvCorreo").hide();		
		jQuery(".btnDescargar").hide();
		
	}
}

function enviarRepCalculoBonoMensualPorCorreo()
{	
		if(validarCamposMandatorios())//tipo, mes, año y ids agentes seleccionados
	    {	
		var tipoUsuario = jQuery('#tipoUsuario').val();
		var tipoBeneficiario = tipoUsuario == "AGENTE" ? "":jQuery('#tiposBeneficiarios').val();
		var mes = dwr.util.getValue("meses");
		var anio = dwr.util.getValue("anios");
		
        var selectedRowsIds = agentesDocumentosGrid.getCheckedRows(0);
	
		var url = enviarReportePath + '?' + 'idsSeleccionados=' + selectedRowsIds + '&tipoBeneficiario=' + tipoBeneficiario 
		+ '&anio=' + anio + '&mes=' + mes + '&tipoUsuario=' + tipoUsuario;
		    
        var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_EXITO)
 				{
					mostrarMensajeInformativo(JSON.parse(data).mensaje, TIPO_MENSAJE_EXITO);
 				}
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
					mostrarMensajeInformativo(JSON.parse(data).mensaje, TIPO_MENSAJE_ERROR);
					
 				}if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_WARNING)
 				{
 					mostrarMensajeInformativo(JSON.parse(data).mensaje, TIPO_MENSAJE_WARNING);
 				}
				
	        }
		});	
	    
	}
}
