var maxMes = 36;

/**
 * Lista las gerencias por centro operacion
 * 
 * @param idCentroEmisor
 */
function loadGerenciasByCentroOperacion() {
	var idCombos="gerenciaList,ejecutivoList,promorotiaList";
	reiniciarCombos(idCombos);
	var idCentroOperacion = jQuery("#centroOperacion").val();
	listadoService
			.listarGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						llenarCombo(data, "gerenciaList",
								"gerenciasSeleccionadas", "descripcion",
								"gerenciaList");
						addToolTipOnchange("gerenciaList");
					});
}

function reiniciarCombos(idCombos){
	var i;
	var combo=idCombos.split(","); 
	var nCombos = combo.length;
	for(i=0; i<nCombos;i++){
			jQuery("#"+combo[i]).empty();
			jQuery("#"+combo[i]).append('<option value="" selected="selected">Seleccione..</option>');
		}
}

/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerencia() {
	var idCombos="ejecutivoList,promorotiaList";
	reiniciarCombos(idCombos);
	var idGerencias = jQuery("#gerenciaList").val();
	listadoService.listarEjecutivoPorGerencia(idGerencias, function(data) {
		llenarCombo(data, "ejecutivoList", "ejecutivosSeleccionados",
				"nombreCompleto", "ejecutivoList",
				"loadPromotoriaByEjecutivo()");
		addToolTipOnchange("ejecutivoList");
	});
}

/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivo() {
	var idCombos="promorotiaList";
	reiniciarCombos(idCombos);
	var idEjecutivos = jQuery("#ejecutivoList").val();
	listadoService.listarPromotoriaPorEjecutivo(idEjecutivos, function(data) {
		llenarCombo(data, "promorotiaList", "promotoriasSeleccionadas",
				"descripcion", "promorotiaList", null);
		addToolTipOnchange("promorotiaList");
	});

}

function llenarCombo(data, targetLoad, nameObj, property, selectedItems,propertyId) {
	if (data != null) {
		jQuery("#"+targetLoad).empty();
		jQuery("#"+targetLoad).append('<option value="" selected="selected">Seleccione....</option>');
		for ( var x = 0; x < data.size(); x++) {
		     if (property == "descripcion") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].descripcion+'</option>');
				}else if (property == "nombreCompleto") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].personaResponsable.nombreCompleto+'</option>');
				}else if (property == "valor") {
					jQuery("#"+targetLoad).append('<option value="'+data[x].id+'">'+data[x].valor+'</option>');
				}
		    }
	}
}

function onChangeIdAgente(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(urlObtenerAgente,data,loadInfoAgente);
	}else{
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#nombreAgente").val("");
	}
}

function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(urlObtenerAgenteCargos,data,loadInfoAgente);
	}
}

function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url= urlMostrarContenedorAgente+"?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(urlObtenerAgente,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	if(json){
		var agente=json.agente;
		if(agente){
			var id=agente.id;
			var idAgen=agente.idAgente;
			var nombreAgente = agente.persona.nombreCompleto;
			
			jQuery("#id").val(id);
			jQuery("#idAgente").val(idAgen);
			jQuery("#nombreAgente").val(nombreAgente);	
		}		
	}
}

function validaFechasInicioFin(){
	var fechaIn = jQuery('#fechaInicio').val();
	var fechaFin = jQuery('#fechaFin').val();
	var estatus = false;
	
	if((fechaIn!=null || fechaIn!='') && (fechaFin!=null || fechaFin!='')){	
		var fechaFinSplit = fechaFin.split("");
		var fechaInSplit = fechaIn.split("");
		var fechaInFormat = '01/'+fechaInSplit[4]+fechaInSplit[5]+'/'+fechaInSplit[0]+fechaInSplit[1]+fechaInSplit[2]+fechaInSplit[3];
		var fechaFinFormat = '01/'+fechaFinSplit[4]+fechaFinSplit[5]+'/'+fechaFinSplit[0]+fechaFinSplit[1]+fechaFinSplit[2]+fechaFinSplit[3];
		estatus = fechaMenorQue(fechaFinFormat, fechaInFormat);
		if(estatus){
			alert('La fecha inicio no puede ser mayor a la fecha final');
			jQuery('#fechaFin').val("");
			jQuery('#fechaInicio').val("");
		}
		var numeroMeses = validarFechas(fechaInFormat, fechaFinFormat);
		if(numeroMeses>=maxMes){
			alert('La diferencia de meses entre las fechas es mayor a 36');
			jQuery('#fechaFin').val("");
			jQuery('#fechaInicio').val("");
		}
	}
	
	if(fechaIn!=null || fechaIn!=''){
		validaIngresoFecha(jQuery('#fechaInicio'), "Inicio");
	}
	if(fechaFin!=null || fechaFin!=''){
		validaIngresoFecha(jQuery('#fechaFin'), "Fin");
	}
	
}

function validaIngresoFecha(fecha, strigFecha){
	var fechaSplit = fecha.val();
	fechaSplit = fechaSplit.split("");
	var fechaFormat = fechaSplit[0]+fechaSplit[1]+fechaSplit[2]+fechaSplit[3];
	if(fechaFormat<2015){
		fecha.val("");
		alert('La fecha '+strigFecha+' no puede ser menor a 201501');
	}
}

function validaExporTo(){
	var fechaIn = jQuery('#fechaInicio').val();
	var fechaFin = jQuery('#fechaFin').val();
	
	if((fechaIn==null || fechaIn=='') || (fechaFin==null || fechaFin=='')){
		return alert('Los campos Fecha Corte inicio y Fecha Corte fin son obligatorias');
	}
	if(jQuery('#listStatus1')[0].checked ||jQuery('#listStatus0')[0].checked ||jQuery('#listStatus2')[0].checked){
		$_exportTo('excel');
	}else{
		return  alert('Debe seleccionar algun estatus');
	}		
}
