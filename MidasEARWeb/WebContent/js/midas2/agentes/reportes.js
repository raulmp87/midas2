/**
 * Revisa los checkbox seleccionados y retorna sus valores
 * 
 * @param target
 */
function checkInputs(target) {
	var id = "";
	jQuery("#" + target + " input:checked").each(function(index) {
		id += jQuery(this).val() + ",";
	});
	return id;
}
/**
 * Limpia el formulario (descheca todos los inputs etc)
 * 
 * @param idForm
 */
function limpiarForm(idForm) {
	jQuery("#" + idForm).each(function() {
		this.reset();
	});
	hiddenList('operacionesList');
}

/**
 * Contruye un listado de check box
 */
function makeCheck(data, targetLoad, nameObj, property, selectedItems,
		onclickFunction,propertyId) {
	var selectedItemsString = checkInputs(selectedItems);
	var selectedItemsArray = new Array();
	selectedItemsArray = selectedItemsString.split(",");
	var select = jQuery("#" + targetLoad);
	select.html("");
	if (data != null) {
		for ( var x = 0; x < data.size(); x++) {
			var option = '<li><label for="' + data[x].id + '">';
			option += '<input type="checkbox" name="' + nameObj + '[' + x;
					if (propertyId != null && propertyId != 'undefined') {
						option += '].'+propertyId + '" id="' + nameObj + '[' + x + '].' + propertyId;
					}else {
						option += '].id' + '" id="' + nameObj + '[' + x + '].id';
					}
					
					option += ' " value="' + data[x].id + '" ';
			for ( var y = 0; y < selectedItemsArray.size(); y++) {
				if (selectedItemsArray[y] == data[x].id) {
					option += "checked='checked'";
				}
			}
			option += 'onchange="' + onclickFunction + '" />';
			if (property == "descripcion") {
				option += data[x].descripcion + '</label>';
			} else if (property == "nombreCompleto") {
				option += data[x].personaResponsable.nombreCompleto
						+ '</label>';
			} else if (property == "valor") {
				option += data[x].valor + '</label>';
			}
			option += '</li>';
			select.append(option);
		}
	}
}
var ventanaBusquedaAgente;
function pantallaModalBusquedaAgente() {
	var url = "/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgenteBusqueda";
	sendRequestWindow(null, url, obtenerVentanaBusquedaAgente);
}

function obtenerVentanaBusquedaAgente() {
	var wins = obtenerContenedorVentanas();
	ventanaBusquedaAgente = wins.createWindow("responsableModal", 10, 320, 900,
			450);
	ventanaBusquedaAgente.centerOnScreen();
	ventanaBusquedaAgente.setModal(true);
	ventanaBusquedaAgente.setText("Buscar Agente");
	return ventanaBusquedaAgente;
}

function responseAgenteProduccion(json) {
	var idAgente = json.agente.id;
	var agente = json.agente.persona.nombreCompleto;
	var indice = jQuery("#listaProduccionAgentes li").length;
	if (idAgente != null) {
		var indicename = indice;
		var option = '<li id="'
				+ idAgente
				+ '"><img src="../img/icons/ico_eliminar.gif" onclick="delete_agente('
				+ idAgente + ');" style="cursor:pointer;"/>';
		option += '&nbsp;&nbsp;<label for="'
				+ idAgente
				+ '" />'
				+ agente
				+ '</label> <input type="text" style="display:none" name="listaAgentes['
				+ indicename + '].id" value="' + idAgente + '"/> </li>';

		jQuery("#listaProduccionAgentes").append(option);
	}
}

function responseAgenteProduccionIdAgente(json) {
	var idAgente = json.agente.idAgente;
	var agente = json.agente.persona.nombreCompleto;
	var indice = jQuery("#listaProduccionAgentes li").length;
	if (idAgente != null) {
		var indicename = indice;
		var option = '<li id="'
				+ idAgente
				+ '"><img src="../img/icons/ico_eliminar.gif" onclick="delete_agente('
				+ idAgente + ');" style="cursor:pointer;"/>';
		option += '&nbsp;&nbsp;<label for="'
				+ idAgente
				+ '" />'
				+ agente
				+ '</label> <input type="text" style="display:none" name="listaAgentes['
				+ indicename + '].id" value="' + idAgente + '"/> </li>';

		jQuery("#listaProduccionAgentes").append(option);
	}
}

function delete_agente(id) {
	jQuery("#listaProduccionAgentes #" + id).remove();
}

function addAgenteProduccion() {
	var busIdAgente = jQuery("#txtIdAgenteBusqueda").val();
	var data = {
		"agente.id" : busIdAgente
	};
	var url = "/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
	
	jQuery.asyncPostJSON(url, data, responseAgenteProduccion);
}

function addAgenteProduccionIdAgente() {
	var busIdAgente = jQuery("#txtIdAgenteBusqueda").val();
	var data = {
		"agente.id" : busIdAgente
	};
	var url = "/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
	
	jQuery.asyncPostJSON(url, data, responseAgenteProduccionIdAgente);
}

function exportTo(typeExport) {
	var isRepDetPrima=exportarToExcelUrl.split('/');
	if(isRepDetPrima[3]=='reporteDetallePrimas'){
		var f1 = jQuery("#fechaInicio").val();
		var f2 = jQuery("#fechaFinal").val();
		var numMeses = validarFechas(f1, f2);
		if(numMeses>1 && numMeses<=3){	
			var co = jQuery('#centroOperacion').val();
			var ta = jQuery('#tipoAgente').val();
			var idAgt = jQuery('#idAgente').val();
			if(co == ''&& ta==''&& idAgt ==""){
				 return alert('Para periodos mayores a 1 mes y menores a 3 meses debe seleccionar al menos un Centro de Operación ó un Tipo de Agente ó un Número de Agente ');
				}
			} else 
				if(numMeses>3 && numMeses<=12){
					
					var idAgt = jQuery('#idAgente').val();
					var ge = jQuery('#gerenciaList').val();
					if(idAgt =='' && ge=='' ){
						return alert('Para periodos mayores a 3 meses debe seleccionar al menos una Gerencia ó un Número de Agente');
					}
			} else if(numMeses!=1){
				return alert('No es posible procesar el reporte con rango de fechas mayor a 12 meses');
			}
		}
	
	if(validateAll(true,"submit") && $_validaPeriodo() && $_validaAnioMes()){
		parent
		.mostrarMensajeConfirm(
				"\u00BFEstá seguro de que desea imprimir el reporte \u003F Este proceso puede demorar algunos minutos.",
				"20", "$_exportTo('" + typeExport + "')", null, null);
	}
}
function $_exportTo(typeExport) {
	var url = typeExport == "pdf" ? exportarToPDFUrl : exportarToExcelUrl;
	url = url + "?" + jQuery(document.exportarToPDF).serialize();
	blockPage();
	var win = window	
			.open(url, null,
					"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	var timer = setInterval(function() {
		if (win.closed) {
			clearInterval(timer);
			unblockPage();
		}
	}, 1000);
}

/**
 * Ocula las listas del cascadeo
 * 
 * @param checkList
 *            lista actual
 */
function hiddenList(checkList) {
	var check = checkInputs(checkList);
	if (check == "") {
		if (checkList == "operacionesList") {
			jQuery("#gerenciaList").html("");
			jQuery("#ejecutivoList").html("");
			jQuery("#promotoriaList").html("");
		} else if (checkList == "gerenciaList") {
			jQuery("#ejecutivoList").html("");
			jQuery("#promotoriaList").html("");
		} else if (checkList == "lineaVentaList") {
			jQuery("#subRamoList").html("");
			jQuery("#productoList").html("");
			jQuery("#lineaNegocioList").html("");
			jQuery("#ramoList").html("");
			jQuery("#coberturasList").html("");
		} else if (checkList == "productoList") {
			jQuery("#ramoList").html("");
			jQuery("#subRamoList").html("");
			jQuery("#lineaNegocioList").html("");
			jQuery("#coberturasList").html("");
		} else if (checkList == "ramoList") {
			jQuery("#subRamoList").html("");
			jQuery("#lineaNegocioList").html("");
			jQuery("#coberturasList").html("");
		}
	}
}

function selectFin(anioInicio) {
	dwr.util.setValue('aniosFin',parseInt(anioInicio)+1);
	$_hidenOptionInt(parseInt(anioInicio)+1,'aniosFin');
}

/**
 * regresa el numero de meses entre dos fechas 
 * si la fecha 1 es mayor que la fecha 2 por 12 meses y un dia ya considera 13 meses
 * formato de fecha es dd/mm/yyyy
 **/
function validarFechas(f1, f2){
	   aF1 = f1.split("/");
	  aF2 = f2.split("/");
	  
	  numMeses = aF2[2]*12 + (aF2[1]-1) - (aF1[2]*12 + (aF1[1]-1));
	  if (aF2[0]>=aF1[0]){
	    numMeses = numMeses + 1;
	  }
	  return numMeses;
}

function validaPeriodoReportes(){
	var mes    = jQuery("#mes option:selected").val();
	var mesFin = jQuery("#mesFin option:selected").val();

	if (parseInt (mesFin)>=parseInt (mes)){
		exportTo('excel');
	}else{
	  alert('El Mes Inicio no puede ser mayor al Mes Fin');
	}
}