var ventanaRechazoRecibo;

function cargarValidacionManual(){
	var path ="/MidasWeb/recepcionDocs/verValidacionManual.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function cargarDocumentosFortimax(){
	if(dwr.util.getValue("agente.id")!=''&&dwr.util.getValue("agente.id")!=null){
		var path ="/MidasWeb/recepcionDocs/mostrarDocumentos.action?agente.id="+dwr.util.getValue("agente.id");
		sendRequestJQ(null, path, "documentos", null);
	}
	else{
		parent.mostrarMensajeInformativo('Favor de seleccionar un agente',"10");
	}
}

function generarLigaIfimaxRecibos(nombreDocumento){
	
	var nombreDoc = nombreDocumento.id;
	var url='/MidasWeb/recepcionDocs/generarLigaIfimax.action?agente.id='+dwr.util.getValue("agente.id")+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agente.persona.claveTipoPersona")+'&nombreDocumento='+nombreDoc;
	window.open(url,'Fortimax');
}

function guardarDocumentosFortimaxRecibos(){
	var url="/MidasWeb/recepcionDocs/guardarDocumentos.action?agente.id="+dwr.util.getValue("agente.id")+"&documentoFortimax.id="+dwr.util.getValue("documentoFortimax.id")+"&mesInicio="+dwr.util.getValue("mesInicio")+"&mesFin="+dwr.util.getValue("mesFin");
	var data;
	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantesRecibo);
}

function populateDocumentosFaltantesRecibo(json){
	if(json){
		var docsFaltantes=json.documentosFaltantes;
		if(docsFaltantes!=null&&docsFaltantes!=''){
			parent.mostrarMensajeInformativo('Los siguientes archivos no han sido digitalizados: '+docsFaltantes+', favor de digitalizar',"10");
		}
		else{
			parent.mostrarMensajeInformativo('Todos los documentos han sido guardados exitosamente, se realizo la autorización exitosamente',"30");		
		}
	}
}

function deshabilitarBusqueda(){
	jQuery(".btnAut").each(function(i,elem){
		jQuery(this).css("display","none");		
	});		
	parent.mostrarMensajeInformativo('Se requiere digitalizar el recibo para cerrar la autorización','30');
}

function mostrarModalRechazoRecibo(nombreDocumento){
	if(dwr.util.getValue("agente.id")!=''&&dwr.util.getValue("agente.id")!=null){
		var url="/MidasWeb/recepcionDocs/verRechazarRecibo.action?nombreDocumento="+nombreDocumento.id;
		sendRequestWindow(null, url,obtenerVentanaRechazoRecibo);
	}
	else{
		parent.mostrarMensajeInformativo('Favor de seleccionar un agente',"10");
	}
}

function obtenerVentanaRechazoRecibo(){
	var wins = obtenerContenedorVentanas();
	ventanaRechazoRecibo= wins.createWindow("rechazoReciboModal", 400, 320, 800, 250);
	ventanaRechazoRecibo.center();
	ventanaRechazoRecibo.setModal(true);
	ventanaRechazoRecibo.setText("Rechazar");
//	ventanaRechazoRecibo.button("park").hide();
//	ventanaRechazoRecibo.button("minmax1").hide();
	return ventanaRechazoRecibo;
}

function rechazarReciboAgente(){
	var nombreDocumento = dwr.util.getValue("nombreDocumento");
	var arr = nombreDocumento.split("_");
	var periodo = arr[1];
	var anio = periodo.substring(0,4);
	var mes = periodo.substring(4,6);
	if(dwr.util.getValue("recepDocs.motivoRechazo.id")!=''&&dwr.util.getValue("recepDocs.motivoRechazo.id")!=null&&dwr.util.getValue("recepDocs.descripcion")!=''&&dwr.util.getValue("recepDocs.descripcion")!=null){
		var path ="/MidasWeb/recepcionDocs/rechazarRecibo.action?recepDocs.idAgente="+dwr.util.getValue("agente.id")+"&mesInicio="+mes+"&mesFin="+mes+"&recepDocs.motivoRechazo.id="+dwr.util.getValue("recepDocs.motivoRechazo.id")+"&recepDocs.descripcion="+dwr.util.getValue("recepDocs.descripcion");
		sendRequestJQ(null, path, targetWorkArea, 'afterRechazo()');
	}
	else{
		parent.mostrarMensajeInformativo('El motivo de rechazo y la descripción son requeridos',"10");
	}
}

function afterRechazo(){
	ventanaRechazoRecibo.close();
	parent.mostrarMensajeInformativo("'"+parent.dwr.util.getValue("mensaje")+"'","'"+parent.dwr.util.getValue("tipoMensaje")+"'");
}

/*****************************************************************************************************************************/

function mostrarModalAgente(){
	if(jQuery("#claveAgente").val()!=""){
			findByClaveAgente();
		}else{
			var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=idagente";
			sendRequestWindow(null, url, obtenerVentanaAgente);
		}
}

function obtenerVentanaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 200, 200, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agente");
//	ventanaAgentes.button("park").hide();
//	ventanaAgentes.button("minmax1").hide();
	return ventanaAgentes;
}

function findByIdAgente(idagente){
	dwr.util.setValue("agente.id",idagente);
	var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
	var data={"agente.id":idagente,"agente.idAgente":""};
	jQuery.asyncPostJSON(url,data,populateAgente);
}

function findByClaveAgente(){
	var claveAgente = jQuery("#claveAgente").val();//dwr.util.getValue("agente.idAgente");
	 var esnumero =validarSiNumero(claveAgente);
	if(claveAgente!=""){
		if(esnumero=='si'){
			var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
			var data={"agente.idAgente":claveAgente,"agente.id":""};
			jQuery.asyncPostJSON(url,data,populateAgente);
		}else{
			clearForm();
		}
	}else{
		populateAgente(null);
		clearForm();
	}
}

function populateAgente(json){
	if(json!=null && json.agente!=null){			
		dwr.util.setValue("agente.persona.claveTipoPersona",json.agente.persona.claveTipoPersona);
		dwr.util.setValue("nombreAgente",json.agente.persona.nombreCompleto);
		dwr.util.setValue("rfcAgente",json.agente.persona.rfc);
		dwr.util.setValue("numFianza",json.agente.numeroFianza);
		dwr.util.setValue("numCedula",json.agente.numeroCedula);
		dwr.util.setValue("statusAgente",json.agente.tipoSituacion.valor);
		dwr.util.setValue("tipoAgente",json.agente.tipoAgente);
		dwr.util.setValue("venceFianza",json.agente.vencimientoFianzaString);
		dwr.util.setValue("venceCedula",json.agente.vencimientoCedulaString);
		dwr.util.setValue("promotoriaAgente",json.agente.promotoria.descripcion);
		dwr.util.setValue("ejecAgente",json.agente.promotoria.ejecutivo.personaResponsable.nombreCompleto);
		dwr.util.setValue("gerenciaAgente",json.agente.promotoria.ejecutivo.gerencia.descripcion);
		dwr.util.setValue("coAgente",json.agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion);
		dwr.util.setValue("domicilio",json.domicilioCompleto);
		dwr.util.setValue("agente.idAgente",json.agente.idAgente);
		dwr.util.setValue("agente.id",json.agente.id);
		jQuery("#claveAgente").val(json.agente.idAgente);
		
		var id= json.agente.id;
		var url="/MidasWeb/fuerzaventa/agente/mostrarNDocumentosMismo.action";
		var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"RECIBO","nombreCarpeta":"RECIBOS","nombreGaveta":"AGENTES"};
		//crearEstructuraAgente(url, data);
	}else{
//		dwr.util.setValue("agente.persona.claveTipoPersona","");
//		dwr.util.setValue("nombreAgente","");
//		dwr.util.setValue("rfcAgente","");
//		dwr.util.setValue("numFianza","");
//		dwr.util.setValue("numCedula","");
//		dwr.util.setValue("statusAgente","");
//		dwr.util.setValue("tipoAgente","");
//		dwr.util.setValue("venceFianza","");
//		dwr.util.setValue("venceCedula","");
//		dwr.util.setValue("promotoriaAgente","");
//		dwr.util.setValue("ejecAgente","");
//		dwr.util.setValue("gerenciaAgente","");
//		dwr.util.setValue("coAgente","");
//		dwr.util.setValue("domicilio","");
//		dwr.util.setValue("agente.idAgente","");
//		dwr.util.setValue("agente.id","");
		clearForm()
	}
}
function auditarDocumentos(){
	var url="/MidasWeb/recepcionDocs/matchDocumentosAgente.action?"+jQuery("#documentos").serialize();
	var data="";
	jQuery.asyncPostJSON(url,data,populateDocumentosFortimax);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimax(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#cargosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
			});
		}
	}
}


function validarSiNumero(numero){
	if (!/^([0-9])*$/.test(numero)){
		return 'no';
	}
	return 'si';
}

function clearForm(){
	jQuery("#claveAgente").val("");
	dwr.util.setValue("agente.persona.claveTipoPersona","");
	dwr.util.setValue("nombreAgente","");
	dwr.util.setValue("rfcAgente","");
	dwr.util.setValue("numFianza","");
	dwr.util.setValue("numCedula","");
	dwr.util.setValue("statusAgente","");
	dwr.util.setValue("tipoAgente","");
	dwr.util.setValue("venceFianza","");
	dwr.util.setValue("venceCedula","");
	dwr.util.setValue("promotoriaAgente","");
	dwr.util.setValue("ejecAgente","");
	dwr.util.setValue("gerenciaAgente","");
	dwr.util.setValue("coAgente","");
	dwr.util.setValue("domicilio","");
	dwr.util.setValue("agente.idAgente","");
	dwr.util.setValue("agente.id","");
}

function muestraDocumentosPorAnio(anio){
	//cargarDocumentosFortimax();
	var idAgente = jQuery("#claveAgente").val();
	var anio = jQuery("#anios").val();
	var url = "/MidasWeb/recepcionDocs/muestraDocumentosEntregadosMesAnio.action?anio="+anio+"&agente.idAgente="+idAgente;
	jQuery.asyncPostJSON(url,null,responseMuestraDocumentosEntregados);
}

function responseMuestraDocumentosEntregados(json){
	var datos = json.listaFacturasMeses;	
	var estructura = "<table class='contenedorConFormato w600'>";	
	var val;
	var entregado;
	for(i=0;i<datos.length;i++){				
		val=datos[i].valor;		
		if(datos[i].checado==0){
			entregado="NO DIGITALIZADO";
		}else{
			entregado="DIGITALIZADO";
		}
		estructura+="<tr><td>"+val+"</td><td>"+entregado+"</td>";
		estructura+="<td><div class='btn_back w180'><a href='javascript: void(0);' id='"+val+"' class='icon_imprimir' onclick='generarLigaIfimaxRecibos(this);'>Digitalizar Documento</a></div></td>";
		if(datos[i].checado==0){
			estructura+="<td><div class='btn_back w80'><a href='javascript: void(0);' id='"+val+"_RECHAZO'  onclick='mostrarModalRechazoRecibo(this);'>Rechazar</a></div></td>";
		}
		estructura+="</tr>";
	}
	estructura+="</table>";
//	alert(estructura);
	jQuery("#muestraFacturasMesAnio").html(estructura);
}


function auditarEntregaFacturas(){
	var anio = jQuery("#anios").val();
	var idAgente = jQuery("#agente_id").val();
	var anio = jQuery("#anios").val();
	var url = "/MidasWeb/recepcionDocs/auditarDocumentosEntregadosMesAnio.action?anio="+anio+"&agente.id="+idAgente;
	jQuery.asyncPostJSON(url,null,responseMuestraDocumentosEntregados);
}