var emptyMap = "";
/**
 * Lista las gerencias por centro operacion
 * 
 * @param idCentroEmisor
 */
function loadGerenciasByCentroOperacion(idCentroOperacion) {
	listadoService
			.getMapGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						if (sizeMap(data) > 0) { 
							addOptionsHeaderAndSelect("gerenciaList", data, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("ejecutivoList", null, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("promotoriaList", null, null, '', 'Seleccione...');
							addToolTipOnchange("gerenciaList");
						}else {
							addOptionsHeaderAndSelect("gerenciaList", null, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("ejecutivoList", null, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("promotoriaList", null, null, '', 'Seleccione...');
						}
					});
}

/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerencia(idGerencia) {	
	listadoService.getMapEjecutivosPorGerencia(idGerencia, function(data) {
		if (sizeMap(data) > 0) {
			addOptionsHeaderAndSelect("ejecutivoList", data, null, '', 'Seleccione...');
			addOptionsHeaderAndSelect("promotoriaList", null, null, '', 'Seleccione...');
			addToolTipOnchange("ejecutivoList");
		} else {
			addOptionsHeaderAndSelect("ejecutivoList", null, null, '', 'Seleccione...');
			addOptionsHeaderAndSelect("promotoriaList", null, null, '', 'Seleccione...');
		}
	});
}

/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivo(idEjecutivo) {
	listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo, function(data) {
		if (sizeMap(data) > 0) {
			addOptionsHeaderAndSelect("promotoriaList", data, null, '', 'Seleccione...');
			addToolTipOnchange("promotoriaList");
		}else {
			addOptionsHeaderAndSelect("promotoriaList", null, null, '', 'Seleccione...');
		}
	});
}

function cleanList(list){
	if (list == 'centroOperacionList') {
		dwr.util.removeAllOptions("gerenciaList");
		dwr.util.removeAllOptions("ejecutivoList");
		dwr.util.removeAllOptions("promotoriaList");
	} else if ('gerenciaList') {
		dwr.util.removeAllOptions("ejecutivoList");
		dwr.util.removeAllOptions("promotoriaList");
	}
	
}

function getAllAgentes(){
	var actionURL= "/MidasWeb/suscripcion/cotizacion/auto/agentes/buscarAgente.action?descripcionBusquedaAgente=";

	blockPage();
	jQuery.ajax({
	    type: "GET",
	    url: actionURL,
	    dataType: "XML",
	    async: false,
	    success: function(xml) {
			jQuery("#agentes").append('<option value="0">SELECCIONE ...</option>');
			jQuery(xml).find('item').each(function(){
			  var idAgente = jQuery(this).find('idAgente').text();
			  var id = jQuery(this).find('id').text();
			  var descripcion = jQuery(this).find('descripcion').text();
			  
			  jQuery("#agentes").append('<option value='+id+'>'+descripcion+'</option>');
			});
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            alert("Ocurrió un error al obtener los agentes");
			unblockPage();
        }   	    
	});
}

function cargarCboAgentes(){
	if (jQuery("#idProm").val() != null && jQuery("#idProm").val() != '' && jQuery("#idProm").val() > 0){
		listadoService.getMapAgentesPorPromotoria(jQuery("#idProm").val(), function(data) {
			if (sizeMap(data) > 0) {
				addOptionsHeaderAndSelect("agentes", data, null, '', 'Seleccione...');
				addToolTipOnchange("agentes");
			}else {
				addOptionsHeaderAndSelect("agentes", null, null, '', 'Seleccione...');
			}
		});	
	}else{
		getAllAgentes();
	}
}