var paqueteGrid;
var catalogoTipoSeguroList;
var elementHTMLTipoSerguroSapAmis = 'wwctrl_txtTipoSeguroSapAmis';

function listarPaquete() {
	document.getElementById("paqueteGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	paqueteGrid = new dhtmlXGridObject("paqueteGrid");
	paqueteGrid.load(listarPaquetePath, function(){desplegarSeleccionTipoSeguro(1, elementHTMLTipoSerguroSapAmis)});
}

function mostrarCatalogoPaquete() {
	sendRequestJQ(null, mostrarCatalogoPaquetePath, targetWorkArea, 'listarPaquete();');
}

function guardarPaquete(){
	sendRequestJQ(null, guardarPaquetePath+"?"+jQuery(document.paqueteForm).serialize(), targetWorkArea, 'listarPaquete();');
}

function eliminarPaquete() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarPaquetePath + "?"+jQuery(document.paqueteForm).serialize(), targetWorkArea, 'listarPaquete();');
	}
}

function verDetallePaquete (tipoAccion) {
	if(paqueteGrid.getSelectedId() != null){
		var idPaquete = getIdFromGrid(paqueteGrid, 0);
		sendRequestJQ(null, verDetallePaquetePath + "?tipoAccion=" + tipoAccion + "&id=" + idPaquete, targetWorkArea, null);
	}
}

function nuevoPaquete (tipoAccion) {
	sendRequestJQ(null, verDetallePaquetePath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}

function listarFiltradoPaquete(){
	paqueteGrid = new dhtmlXGridObject("paqueteGrid");
	var form = document.paqueteForm;
	if(form!=null){
		paqueteGrid.load(listarFiltradoPaquetePath+"?"+jQuery(document.paqueteForm).serialize());
	}else{
		paqueteGrid.load(listarFiltradoPaquetePath);
	}
}


function disableEnterKey(e)
{
     var key;

     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox

     if(key == 13)
          return false;
     else
          return true;
}

function desplegarSeleccionTipoSeguro(tipoAccion, idElementHTML){
	if(tipoAccion==1){
		obtenerCatalogoTipoSeguro(function(){
			crearEstructuraSelect(idElementHTML);
		});
	}
}

function crearEstructuraSelect(idElementHTML){
	document.getElementById(idElementHTML).innerHTML = '';
	var iElementSelect = document.createElement('select');
	    iElementSelect.id = 'selectTipoSeguroSapAmis';
	    iElementSelect.name = 'paquete.claveAmisTipoSeguro';
	    iElementSelect.className = 'cajaTexto';
	document.getElementById(idElementHTML).appendChild(iElementSelect);
	agregarOpcionesSelect(iElementSelect.id);
}

function agregarOpcionesSelect(idElementHTML){
	var iElementOption = document.createElement('option');
	    iElementOption.id = 'optionVacia';
	    iElementOption.value = '-1';
	    iElementOption.innerHTML = 'Seleccionar Opcion.';
	document.getElementById(idElementHTML).appendChild(iElementOption);
		iElementOption = document.createElement('option');
	    iElementOption.id = 'noHomologado';
	    iElementOption.value = '0';
	    iElementOption.innerHTML = 'Paquetes no homologados.';
	document.getElementById(idElementHTML).appendChild(iElementOption);
	for(var i=0; i<catalogoTipoSeguroList.length; i++){
		agregarOpcionSelect(idElementHTML, catalogoTipoSeguroList[i]);
	}
}

function agregarOpcionSelect(iElementSelect, catalogoTipoSeguro){
	var iElementOption = document.createElement('option');
	    iElementOption.id = 'option' + catalogoTipoSeguro.claveTipoValor;
	    iElementOption.value = catalogoTipoSeguro.claveTipoValor;
	    iElementOption.innerHTML = catalogoTipoSeguro.descripcionParametroGeneral;
	document.getElementById(iElementSelect).appendChild(iElementOption);
}

function obtenerCatalogoTipoSeguro(callBack) {
	var parametrosGenerales = {};
		parametrosGenerales.groupName = 'Tipo de Seguro';
	jQuery.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
	    url: '/MidasWeb/catalogos/parametroGeneral/listByGroup.action',
        cache: false,
        data: JSON.stringify(parametrosGenerales),
	    async: true,
	    success: function(cifrasResponse) {
	    	catalogoTipoSeguroList = cifrasResponse.listaParametroGeneral;
			callBack();
	    },
	    error:function () {
            callBack();
        }
	});
}