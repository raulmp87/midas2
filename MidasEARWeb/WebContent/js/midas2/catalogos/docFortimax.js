
function crearEstructuraAgente(url, data){
	jQuery.asyncPostJSON(url,data,$_populateDocumentosFortimax);
}

function crearEstructuraNAgente(url, data){
	jQuery.asyncPostJSON(url,data,$_populateDocumentosFortimax);
}

function $_populateDocumentosFortimax(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			var listDoc="<table class='contenedorFormas no-border'>";
			for(var i=0;i<lista.length;i++){
				var jsonObject=lista[i];
				var descripcion=jsonObject.catalogoDocumentoFortimax.descripcion;
				var existe=jsonObject.existeDocumento;
				var id=jsonObject.id;
				var nombreDocumento=jsonObject.catalogoDocumentoFortimax.nombreDocumento;
				var nombreDocumentoFortimax = jsonObject.catalogoDocumentoFortimax.nombreDocumentoFortimax;
				if(existe==1){
					listDoc = listDoc+"<tr><td><input type='checkbox' checked='checked' disabled='disabled'/>"+nombreDocumentoFortimax+"</td></tr>";
				}else{
					listDoc=listDoc+"<tr><td><input type='checkbox' disabled='disabled'/>"+nombreDocumentoFortimax+"</td></tr>";
				}
			}
			listDoc=listDoc+"</table>";			
			jQuery('#cargarCheck').html(listDoc);
			jQuery("#botonesFortimax").css("display","block");
		}
	}
}

//function guardarConfigCargo(url,data){
//		jQuery.asyncPostJSON(url,data,$_guardar);
//}	