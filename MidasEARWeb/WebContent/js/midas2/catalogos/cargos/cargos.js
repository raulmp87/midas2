
function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgente(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	if(json.agente==null){
//		alert("La clave del Agente no Existe ó no esta Autorizado "+json["mensajeError"]);
		alert(json["mensajeError"]);
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#tipoAgente").val("");
		jQuery("#numeroFianza").val("");
		jQuery("#venceFianza").val("");
		jQuery("#numeroCedula").val("");
		jQuery("#venceCedula").val("");
		jQuery("#estatusAgente").val("");
		jQuery("#descripcionCentroOperacion").val("");
		jQuery("#descripcionGerencia").val("");
		jQuery("#descripcionPromotoria").val("");
		jQuery("#nombreAgente").val("");
		jQuery("#rfcAgente").val("");
		jQuery("#ejecutivo").val("");
	}else{
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var tipoAgente = agente.clasificacionAgentes.valor;
		var numeroFianza = agente.numeroFianza;
		var venceFianza = agente.vencimientoFianzaString;
		var numeroCedula= agente.numeroCedula;
		var venceCedula =agente.vencimientoCedulaString;
		var estatus = agente.tipoSituacion.valor;
		var centroOperacion= agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion;
		var gerencia = agente.promotoria.ejecutivo.gerencia.descripcion;
		var promotoria = agente.promotoria.descripcion;
		var nombreAgente = agente.persona.nombreCompleto;
		var rfc = agente.persona.rfc;
		var ejecutivo = agente.promotoria.ejecutivo.personaResponsable.nombreCompleto;
		
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#tipoAgente").val(tipoAgente);
		jQuery("#numeroFianza").val(numeroFianza);
		jQuery("#venceFianza").val(venceFianza);
		jQuery("#numeroCedula").val(numeroCedula);
		jQuery("#venceCedula").val(venceCedula);
		jQuery("#estatusAgente").val(estatus);
		jQuery("#descripcionCentroOperacion").val(centroOperacion);
		jQuery("#descripcionGerencia").val(gerencia);
		jQuery("#descripcionPromotoria").val(promotoria);
		jQuery("#nombreAgente").val(nombreAgente);
		jQuery("#rfcAgente").val(rfc);
		jQuery("#ejecutivo").val(ejecutivo);
		
	}
}


var gridCargos;
function cargaGridCargos(){
	var url="/MidasWeb/cargos/cargosAgentes/cargarGridCargos.action?cargos.id="+dwr.util.getValue("cargos.id");
//			");
	gridCargos = new dhtmlXGridObject('gridCargos');
	gridCargos.load(url);
}


 /* Valores globales de año comercial*/
var diario = 1;
var semanal = 7;
var quincenal = 15;
var mensual = 30;
var bimestral = 60;
var trimestral = 90;
var semestral = 180;
var anual = 360;
var fechaHoy = fechaActual();

function calcularCargos(){
	var tipoCargo = jQuery("#tipoCargo").val();
	var importe = jQuery("#importe").val();
	var plazo = jQuery("#plazo").val();
	var fecIniCobro = jQuery("#fecIniCobro").val();
	var numeroCargos = jQuery("#numeroCargos").val();
	if(tipoCargo!='' && importe != 0.0 && plazo != '' && fecIniCobro != '' && numeroCargos != '' ){
		gridCargos.clearAll();
		var plazoText = jQuery("#plazo :selected").text();
		var plazo=0;
		var cont = 0;
		var importeOtorgado = jQuery("#importe").val();
		var fechaInicio = jQuery("#fecIniCobro").val();
		var numeroCargos = jQuery("#numeroCargos").val();
		
		if(plazoText == "Diario")     { plazo = diario    }
		if(plazoText == "Semanal")    { plazo = semanal   }
		if(plazoText == "Quincenal")  { plazo = quincenal }
		if(plazoText == "Mensual")    { plazo = mensual   }
		if(plazoText == "Bimestral")  { plazo = bimestral }
		if(plazoText == "Trimestral") { plazo = trimestral}
		if(plazoText == "Semestral")  { plazo = semestral }
		if(plazoText == "Anual")      { plazo = anual 	  }	
		
		var no_Cargo; 
		var fecha_cargo;
		var importe_Inicial = importeOtorgado;
		var importe_cada_cargo = importeOtorgado/numeroCargos;
		var estatus;
		var importe_Restante=importeOtorgado; 
		jQuery("#importeCargo").val(importe_cada_cargo.toFixed(2));
		mifecha = fechaInicio;
		for(cont; cont<numeroCargos; cont++){
			importe_Restante = importe_Restante-importe_cada_cargo;
			gridCargos.addRow(cont,[cont+1,mifecha,importe_Inicial,importe_cada_cargo.toFixed(2),importe_Restante.toFixed(2),'PENDIENTE',]);
			importe_Inicial = importe_Restante.toFixed(2);
			mifecha = recalcFechas(fechaInicio,plazo);
			fechaInicio = mifecha;
		}
		jQuery("#btnGuardar").css("display","block");
	}else{
		var camposFaltantes = '';
		if(tipoCargo=='' ){
			camposFaltantes=camposFaltantes+" Tipo de Cargo, ";
		}
		if(importe == 0.0){
			camposFaltantes=camposFaltantes+" Importe, ";
		}
		if(plazo == ''){
			camposFaltantes=camposFaltantes+" Plazo, ";
		} 
		if(fecIniCobro == ''){
			camposFaltantes=camposFaltantes+" Fecha de Inicio del Cobro, ";
		}
		if(numeroCargos == '' ){
			camposFaltantes=camposFaltantes+"  Número de Cargos, ";
		}
		parent.mostrarMensajeInformativo('Los siguientes campos son requeridos '+camposFaltantes ,'10');
	}	
}

function calculateFooterValues() {
    jQuery("#totIntOrd").text(sumColumn(4));   
    jQuery("#totIntMor").text(sumColumn(5));   
    jQuery("#totAbonoCap").text(sumColumn(6));   
    jQuery("#totIva").text(sumColumn(7));   
    jQuery("#totImpPago").text(sumColumn(8));    
}
function sumColumn(ind) {
    var out = 0;
    for (var i = 0; i < pagaresGrid.getRowsNum(); i++) {
        out += parseFloat(pagaresGrid.cells2(i, ind).getValue());
    }
    return roundNumber(out,2);
}


/**********************************************************************************************************************************
 *bloque de funciones para incrementar o decrementar dias a una fecha (en formato de año comercial todos los meses de 30 dias)
 *@cavalos
 *la funcion que debemos ejecutar en nuestros eventos es:
 *recalcFechas(fecInicio,increm)
 *donde.
 *fecInicio = fecha a la que se quiere incrementar /decrementar dias.
 *increm = valor numerico a incremetar o decrementar
 *(para decrementar indicar signo (-)negativo antes del valor)
********************************************************************/
function recalcFechas(fecInicio,increm){
   var  fec = addToDate(fecInicio, increm);
   return  fec;
}

//var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var aFinMes = new Array(30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30);
	

function finMes(nMes, nAno){
	return aFinMes[nMes - 1] + (((nMes == 2) && (nAno % 4) == 0)? 1: 0);
}

function padNmb(nStr, nLen, sChr){
    var sRes = String(nStr);
    for (var i = 0; i < nLen - String(nStr).length; i++)
     sRes = sChr + sRes;
    return sRes;
}

function makeDateFormat(nDay, nMonth, nYear){
    var sRes;
    sRes = padNmb(nDay, 2, "0") + "/" + padNmb(nMonth, 2, "0") + "/" + padNmb(nYear, 4, "0");
    return sRes;
}

function incDate(sFec0){
   var nDia = parseInt(sFec0.substr(0, 2), 10);
   var nMes = parseInt(sFec0.substr(3, 2), 10);
   var nAno = parseInt(sFec0.substr(6, 4), 10);
   nDia += 1;
   if (nDia > finMes(nMes, nAno)){
    nDia = 1;
    nMes += 1;
    if (nMes == 13){
     nMes = 1;
     nAno += 1;
    }
   }
   return makeDateFormat(nDia, nMes, nAno);
}

function decDate(sFec0){
   var nDia = Number(sFec0.substr(0, 2));
   var nMes = Number(sFec0.substr(3, 2));
   var nAno = Number(sFec0.substr(6, 4));
   nDia -= 1;
   if (nDia == 0){
    nMes -= 1;
    if (nMes == 0){
     nMes = 12;
     nAno -= 1;
    }
    nDia = finMes(nMes, nAno);
   }
   return makeDateFormat(nDia, nMes, nAno);
}

function addToDate(sFec0, sInc){
   var nInc = Math.abs(parseInt(sInc));
   var sRes = sFec0;
   if (parseInt(sInc) >= 0)
    for (var i = 0; i < nInc; i++) sRes = incDate(sRes);
   else
    for (var i = 0; i < nInc; i++) sRes = decDate(sRes);
   return sRes;
}
/********************************************************************************************************************************/
function fechaActual(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	fecha = dd+'/'+mm+'/'+yyyy; 
	return fecha;
}

function DiferenciaFechas (CadenaFecha1,CadenaFecha2) {  
	     
	    var nDia1 = Number(CadenaFecha1.substr(0, 2));
	   	var nMes1 = Number(CadenaFecha1.substr(3, 2));
	   	var nAno1 = Number(CadenaFecha1.substr(6, 4));
	   	
	   	var nDia2 = Number(CadenaFecha2.substr(0, 2));
	   	var nMes2 = Number(CadenaFecha2.substr(3, 2));
	   	var nAno2 = Number(CadenaFecha2.substr(6, 4));
	   
	   //Obtiene objetos Date  
	   var miFecha1 = new Date( nAno1, nMes1, nDia1 ); 
	   var miFecha2 = new Date( nAno2, nMes2, nDia2 );  
	  
	   //Resta fechas y redondea  
	   var diferencia = miFecha1.getTime() - miFecha2.getTime();  
	   var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));  
	   var segundos = Math.floor(diferencia / 1000);
//	   alert ('La diferencia es de ' + dias + ' dias,\no ' + segundos + ' segundos.')  
	     
	   return dias;
	}


function guardarConfigCargos(){
	
		gridCargos.setCSVDelimiter(',');	
		var listaCargos=gridCargos.serializeToCSV();
		var estatusCargo = jQuery("#estatusCargo").val();
		if(validateAll(true)){
			var url="/MidasWeb/cargos/cargosAgentes/guardar.action?"+jQuery("#cargosForm").serialize()+"&cargos.estatusCargo.id="+estatusCargo+"&tipoAccion"+dwr.util.getValue("tipoAccion")+"&detalleListaCargos="+listaCargos;
			var f = new Date();
			
			var fechaHoy = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
			
			gridCargos.selectRow(0);
			var id=gridCargos.getSelectedId();
			var fecha=gridCargos.cellById(id, 1).getValue();
			
			if (Date.parse(fecha) >= Date.parse(fechaHoy)){
				sendRequestJQ(null,url,targetWorkArea,null);
			}else{
				parent.mostrarMensajeInformativo('La fecha de Cargo no puede ser menor a la Fecha Actual, debe de cambiar la Fecha de Inicio del Cobro','20');
				jQuery("#btnAplicar").css("display","none");
			}
		}	
}

function dateFormatMex(fecha){
	if(fecha!=null){
		var date = new Date(fecha);
//		date=toDate(date);
		var d = (date.getDate()<10)?"0"+date.getDate():date.getDate();
		var m = ((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
		var y = date.getFullYear(); 
		return d+"/"+m+"/"+y;
	}else{
		return null;
	}
}


function imprimirComprobanteCargo(){
		var idCargo = dwr.util.getValue("cargos_id");
		var url="/MidasWeb/cargos/cargosAgentes/imprimirComprobanteCargo.action?cargos.id="+idCargo;
//		sendRequestWindow(null, url, obtenerVentanaTablaAmortizacion);
		window.open(url, "ComprobanteCargo");
}


function aplicarMovimiento(){
	var idAgente = jQuery("#id").val();	
	var idCargo = jQuery("#cargos_id").val();
	var url = "/MidasWeb/cargos/cargosAgentes/auditarDocumentosCargosDigitalizar.action?cargos.id="+idCargo+"&cargos.agente.id="+idAgente;
	jQuery.asyncPostJSON(url,null,validaAplicacionDeMovimiento);
	
}

function validaAplicacionDeMovimiento(json){

	var lista=json.listaDocumentosFortimaxView;
	
	var count = 0;	

	if(!jQuery.isEmptyArray(lista)){
		
		for(var i=0;i<lista.length;i++){
			if (lista[i].checado == 0){
				count = count+1;
			}
		}
		
		if (count==0){
			
				var f = new Date();
				
				var fechaHoy = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
				
				gridCargos.selectRow(0);
				var id=gridCargos.getSelectedId();
				var fecha=gridCargos.cellById(id, 1).getValue();
				
				if (Date.parse(fecha) >= Date.parse(fechaHoy)){
					var idMovimiento = jQuery("#cargos_id").val();
					var url="/MidasWeb/cargos/cargosAgentes/aplicarMovimiento.action?cargos.id="+idMovimiento;
					sendRequestJQ(null,url,targetWorkArea,null);
					imprimirComprobanteCargo();
				}else{
					parent.mostrarMensajeInformativo('La fecha de Cargo no puede ser menor a la Fecha Actual, debe de cambiar la Fecha de Inicio del Cobro','20');
					jQuery("#btnAplicar").css("display","none");
				}
		} else{
			parent.mostrarMensajeInformativo('Hay '+count+' documentos sin digitalizar',"10");
		}
	}else{
		parent.mostrarMensajeInformativo('Hay documentos sin digitalizar',"10");
	}	
}

function cancelarMovimiento(){
	var idMovimiento = jQuery("#cargos_id").val();
	if(confirm("Eliminar Movimiento?")){
		if(idMovimiento!=""){
			var url="/MidasWeb/cargos/cargosAgentes/cancelarMovimiento.action?cargos.id="+idMovimiento;
			sendRequestJQ(null,url,targetWorkArea,null);
		}else{
			urlDetalle = "/MidasWeb/cargos/cargosAgentes/mostrarContenedor.action";
			sendRequestJQ(null,urlDetalle,targetWorkArea,null);
		}
	}
}

function controlDeBotones(){
	var idMovimiento = jQuery("#cargos_id").val();
	var estatusMovimiento = jQuery("#estatusCargo :selected").text();
	if(idMovimiento != ""){
		if(estatusMovimiento == "PENDIENTE"){
			jQuery("#btnCalcularCargo").css("display","block");
			jQuery("#btnAplicar").css("display","block");
			jQuery("#btnGuardar").css("display","block");
			jQuery("#btnCancelar").css("display","block");
		}
		if(estatusMovimiento == "APLICADO"){
			jQuery("#btnTablaAmortizacion").css("display","block");
			jQuery("#btnCancelar").css("display","block");
		}
	}else{
		jQuery("#btnCalcularCargo").css("display","block");		
	}
	
}
//
//function activarPagare(rowId,columInd,state){
//	var idPagare=pagaresGrid.cellById(rowId, 1).getValue();
//	var idMovimiento = jQuery("#cargos_id").val();
//	var url="/MidasWeb/prestamos/prestamosAnticipos/activarPagare.action?prestamoAnticipo.id="+idMovimiento+"&pagarePrestamoAnticipo.id="+idPagare;
//	sendRequestJQ(null,url,targetWorkArea,null);	
//}


function guardarDocumentosFortimax(){
	var url="/MidasWeb/fuerzaventa/agente/guardarDocumentos.action?"+jQuery("#agenteDocumentosForm").serialize();
	var data;
	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantes);
}


function salirCargos(){
	
	var url = "/MidasWeb/cargos/cargosAgentes/mostrarContenedor.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}
}

function mostrarHistoricoCargos(tipoOperacion, idRegisto){
	var url = '/MidasWeb/fuerzaventa/agente/mostrarHistorial.action?idTipoOperacion='+tipoOperacion+ "&idRegistro="+idRegisto;
	mostrarModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
}

function botonPruebaMisar(){
	sendRequestJQ(null,"/MidasWeb/cargos/cargosAgentes/pruebaMisar.action",targetWorkArea,null);
}


function generaEstructuraFortimax(){

	var idAgente = jQuery("#id").val();	
	var idCargo = jQuery("#cargos_id").val();
	var tipoCargo = jQuery("#tipoCargo :selected").text();
	var nombreDocumento = "COMPROBANTE DE ACUERDO DE "+tipoCargo;
	
	var url="/MidasWeb/cargos/cargosAgentes/generarDocCargosEnFortimax.action";
	var data={"cargos.id":idCargo,"cargos.agente.id":idAgente,"cargos.tipoCargo.valor":nombreDocumento};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizar);	
	jQuery(".js_habilita").css("display","block");
}

function muestraDocumentosADigitalizar(){
	var idAgente = jQuery("#id").val();	
	var idCargo = jQuery("#cargos_id").val();
	var url="/MidasWeb/cargos/cargosAgentes/mostrarDocumentosADigitalizar.action";
	var data={"cargos.id":idCargo,"cargos.agente.id":idAgente};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizar);
}

function muestraHtmlDocDigitalizar(json){
	var datos = json.listaDocumentosFortimaxView;	
	var estructura = "<table class='contenedorConFormato w800'>";	
	var val;
	var entregado;
	for(i=0;i<datos.length;i++){				
		val=datos[i].valor;		
		if(datos[i].checado==0){
			entregado="NO DIGITALIZADO";
		}else{
			entregado="DIGITALIZADO";
		}
		estructura+="<tr><td>"+val+"</td><td>"+entregado+"</td>";
//		if(datos[i].checado==0){			
			estructura+="<td><div class='btn_back w180'><a href='javascript: void(0);' id='"+val+"' class='icon_imprimir btn_digitalizarDoc' onclick='generarLigaIfimaxCargosFortimax(this);'>Digitalizar Documento</a></div></td>";
//		}
		estructura+="</tr>";
	}
	estructura+="</table>";
//	alert(estructura);
	jQuery("#muestraDocumentosADigitalizar").html(estructura);
}

function generarLigaIfimaxCargosFortimax(nombreDocumento){
	var idAgente = jQuery("#id").val();	
	var nombreDoc = nombreDocumento.id;
	var url='/MidasWeb/cargos/cargosAgentes/generarLigaIfimaxCargos.action?cargos.agente.id='+idAgente+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agente.persona.claveTipoPersona")+'&nombreDocumento='+nombreDoc;
	window.open(url,'Fortimax');
}


function auditarDocumentosCargosDigitalizados(){
	var idAgente = jQuery("#id").val();	
	var idCargo = jQuery("#cargos_id").val();
	
	var url = "/MidasWeb/cargos/cargosAgentes/auditarDocumentosCargosDigitalizar.action?cargos.id="+idCargo+"&cargos.agente.id="+idAgente;
	jQuery.asyncPostJSON(url,null,muestraHtmlDocDigitalizar);
}

//if (jQuery.browser.msie && jQuery.browser.version < 9){
//
//	jQuery('select.wide').bind('focus mouseover', function() { 
//			jQuery(this).addClass('expand').removeClass('clicked'); 
//	}).bind('click', function() { 
//				jQuery(this).toggleClass('clicked');
//	
//	}).bind('mouseout', function() { 
//				if (!jQuery(this).hasClass('clicked')) { 
//						jQuery(this).removeClass('expand'); 
//				}	
//	}).bind('blur', function() {
//			 jQuery(this).removeClass('expand clicked'); 
//			 }); 
//}