var reaseguradorCnsfGrid;

function listarFiltradoReaseguradorCnsf(){
	reaseguradorCnsfGrid = new dhtmlXGridObject("reaseguradorCnsfGrid");
	var form = document.reaseguradorCnsfForm;
	if(form!=null){
		reaseguradorCnsfGrid.load(listarReaseguradorCnsfPath+"?"+jQuery(document.reaseguradorCnsfForm).serialize());
	}else{
		reaseguradorCnsfGrid.load(listarReaseguradorCnsfPath);
	}		
}

function verDetalleReaseguradorCnsf (tipoAccion) {
	if(reaseguradorCnsfGrid.getSelectedId() != null){
		var idColorVehiculo = getIdFromGrid(reaseguradorCnsfGrid, 0);
		sendRequestJQ(null, verDetalleReaseguradorCnsfPath + "?tipoAccion=" + tipoAccion + "&id=" + idColorVehiculo, targetWorkArea, null);
	}
}

function mostrarCatalogoReaseguradorCnsf() {
	sendRequestJQ(null, mostrarReaseguradorCnsfPath, targetWorkArea, 'listarFiltradoReaseguradorCnsf();');
}

function guardarCatalogoReaseguradorCnsf() {
	sendRequestJQ(null, guardarReaseguradorCnsfPath+"?"+jQuery(document.reaseguradorCnsfForm).serialize(), targetWorkArea, 'listarFiltradoReaseguradorCnsf();');
}

function eliminarCatalogoReaseguradorCnsf() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarReaseguradorCnsfPath + "?"+jQuery(document.reaseguradorCnsfForm).serialize(), targetWorkArea, 'listarFiltradoReaseguradorCnsf();');
	}
}

function nuevoCatalogoReaseguradorCnsf (tipoAccion) {
	sendRequestJQ(null, verDetalleReaseguradorCnsfPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}

function onChangeAgencia(){
	var idAgencia=(dwr.util.getValue("agenciasList")!='')?dwr.util.getValue("agenciasList"):'-1';
	var target="Catcalificacion";
	var url="/MidasWeb/catalogos/reaseguradorescnsf/mostrarCatalogoCalificaciones.action";
	var params={"reaseguradorMovs.idagencia":idAgencia};
	jQuery.asyncPostJSON(url,params,function(data){
		var dataCombo=new Array();
		var arr=data.listaCal;
		jQuery.fillCombo(target,data.listaCal,'id','calificacion',true);
	});
}

function exportarInfo(){
	
	var data = jQuery("#polizaForm").serialize();
	var url = exportarReaseguradorCnsfPath+"?"+jQuery(document.reaseguradorCnsfForm).serialize();  
	window.open(url, "Excel");
	
}