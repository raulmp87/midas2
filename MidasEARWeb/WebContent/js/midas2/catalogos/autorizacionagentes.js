var autorizacionAgentesGrid;

function ocultarBtnRechazar(){
	var ids=grid.getCheckedRows(1);
	var countSelected = ids.split(",");
	if(countSelected.length > 1 ){
		jQuery("#jQBtnRechazar a").css("display","none");
	}else if(countSelected.length ==1) {
		jQuery("#jQBtnRechazar a").css("display","block");
	}
	if(countSelected.length >= 1 ){
		jQuery("#btnAutorizar a").css("display","block");
	}
	if(ids==''){
		jQuery("#btnAutorizar a").css("display","none");
		jQuery("#jQBtnRechazar a").css("display","none");
	}
}

function checkUncheckAll(){
	var sel = jQuery("#checkAll").attr("checked");

	if(sel){
		reloadGrid();	
		grid.checkAll(true);			
	}else{
		grid.getCheckedRows(1);
		grid.checkAll(false);	
	}
	ocultarBtnRechazar();	
	
}

function allChecksSelected(){	
	var idchecked = grid.getCheckedRows(1);

	if(idchecked!=""){
		grid.filterBy(1,1);
	}else{
		alert("No existe ningun registro marcado");
	}
}

function reloadGrid(){	 
        grid.filterBy(1,0);		
}

function activaCheck(){
	//jQuery("#checkAll").attr("checked","checked");
}

var ventanaRechazar=null;
function mostrarModalRechazarAutorizacion(){
	var idchecked = grid.getCheckedRows(1);
	if(idchecked!=""){
		var idRow = grid.cellById(idchecked, 0).getValue();
		var url="/MidasWeb/fuerzaventa/autorizacionagentes/rechazarAutorizacion.action?agenteAutorizacion.id="+idRow;
		sendRequestWindow(null, url, ventanaRechazarAutorizacion);
		
	}else{
		alert("Seleccione el registro a eliminar");
	}
}
 function ventanaRechazarAutorizacion(){
		var wins = obtenerContenedorVentanas();
		ventanaRechazar= wins.createWindow("rechazar", 400, 320, 750, 280);
		ventanaRechazar.center();
		ventanaRechazar.setModal(true);
		ventanaRechazar.setText("Rechazar");
		return ventanaRechazar;
	}


function rechazarAutorizacion(){
	if (validateAll(true)){
		if(confirm("\u00BFEst\u00e1 seguro que desea rechazar el alta del agente?")){
			sendRequestJQAsync(null, "/MidasWeb/fuerzaventa/autorizacionagentes/eliminar.action?"
				+ jQuery(document.rechazoAgentesForm).serialize(),
				targetWorkArea, null);
			ventanaRechazar.close();
		}
	}	
}

function aceptarAutorizacion(){
	var idchecked = grid.getCheckedRows(1);
	

	if(idchecked!=""){
		allChecksSelected();
		var idRow="";
		var idArray = idchecked.split(",");
		for(i=1;i<=idArray.length;i++){
			if(i!=idArray.length){
				idRow += grid.cellById(idArray[i - 1],0).getValue() + ",";				
			}else{
				idRow += grid.cellById(idArray[i - 1],0).getValue();
			}
		}
		if(idRow!=""){	
			if(confirm("\u00BFEsta seguro de aplicar la autorizaci\u00F3n de agentes?")){
				sendRequestJQ(null, "/MidasWeb/fuerzaventa/autorizacionagentes/aprobarAgente.action?idsAutorizar="+idRow,
							targetWorkArea,null);
			}
		}
	}
	else{
		alert("Seleccione el registro a aprobar");
	}
}

function reloadAutorizacion(){
	sendRequestJQ(null,"/MidasWeb/fuerzaventa/autorizacionagentes/mostrarContenedor.action",targetWorkArea,null);
}

function onChangeGerencia(target,target2,value){
	var idgerencia = value;//dwr.util.getValue("idGerencia");
	if(idgerencia != null  && idgerencia != headerValue){
		listadoService.getMapEjecutivosPorGerencia(idgerencia,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});	
		onChangeEjecutivo('idPromotoria');
	}else{
		addOptions(target,null);
		onChangeEjecutivo('idPromotoria');
	}
	var idEjecutivo = jQuery("#idEjecutivo option:selected").val();
	if(idEjecutivo!=''){		
		addOptions(target2,null);
	}
}

function onChangeEjecutivo(target,value){
	var idEjecutivo =value;// dwr.util.getValue("idEjecutivo");
	if(idEjecutivo != null  && idEjecutivo != headerValue){
		listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});	
	}else{
		addOptions(target,null);
	}
}


dhtmlXGridObject.prototype._in_header_filter_checkbox=function(t,i,c){
 t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
 var self=this;
 t.firstChild.onclick=function(e){
   self.filterBy(i,this.checked?1:0);
  (e||event).cancelBubble=true;
 }
}

