/**	
 * 
 * 
 */
function populateDomicilio(json){
	if(json){
		var estadoName="ejecutivo.domicilio.claveEstado";
		var paisName="ejecutivo.domicilio.clavePais";
		var ciudadName="ejecutivo.domicilio.claveCiudad";
		var coloniaName="ejecutivo.domicilio.nombreColonia";
		var calleNumeroName="ejecutivo.domicilio.calleNumero";
		var codigoPostalName="ejecutivo.domicilio.codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/ejecutivo/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	mostrarModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}

//function obtenerHistorico(){
//	var url = '/MidasWeb/fuerzaventa/ejecutivo/loadHistory.action?idTipoOperacion='+30+ "&idRegistro="+dwr.util.getValue("idEjecutivo");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}

function salirEjecutivo(){
	if(confirm("Está a punto de abandonar esta sección, si no ha guardado se perderán sus datos. ¿Desea continuar?")){
	var path= "/MidasWeb/fuerzaventa/ejecutivo/mostrarContenedor.action";
	sendRequestJQAsync(null, path, targetWorkArea, null);
	}	
}

function guardarEjecutivo(){
	if(validateAll(true)){
		var calleNumero=dwr.util.getValue("ejecutivo.domicilio.calleNumero");
		var codigoPostal=dwr.util.getValue("ejecutivo.domicilio.codigoPostal");
		if(!jQuery.isValid(calleNumero)&&!jQuery.isValid(codigoPostal)){
			mostrarMensajeInformativo("La persona seleccionada no cuenta con un domicilio de oficina, no es posible guardar el ejecutivo","10");
		}
		else{
			sendRequestJQ(null, guardarEjecutivoPath+"?"+ jQuery(document.ejecutivoForm).serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion"),targetWorkArea);
		}
	}
}