
function buscaAgentePorIdAgente(){
	var idAgente = jQuery("#txtIdAgente").val();
	if(idAgente!=""){
		if(jQuery.isValid(idAgente)){
			var url="/MidasWeb/fuerzaventa/agente/findByClaveAgente.action";
			var data={"agente.idAgente":idAgente};
			jQuery.asyncPostJSON(url,data,loadInfoAgente);
		}
	}else{
		mostrarListadoAgentesSusp();
	}
}

function mostrarListadoAgentesSusp(){
	var idAgente = jQuery("#txtIdAgente").val();
	var field="id";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}
}

function buscaAgentePorId(){
	var id = jQuery("#id").val();
	
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
		var data={"agente.id":id};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}	
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	if(json.agente==null){
		alert("La clave del Agente no Existe");
		jQuery("#id").val("");
//		jQuery("#txtIdAgente").val("");
		jQuery("#estatusAgente").val("");
		jQuery("#descripcionCentroOperacion").val("");
		jQuery("#descripcionGerencia").val("");
		jQuery("#descripcionPromotoria").val("");
		jQuery("#nombreAgente").val("");
		jQuery("#rfcAgente").val("");
		jQuery("#ejecutivo").val("");
		jQuery("#tipoAgente").val("");
		jQuery("#numeroFianza").val("");
		jQuery("#venceFianza").val("");
		jQuery("#numeroCedula").val("");
		jQuery("#venceCedula").val("");
	}else{		
		var agente=json.agente;
		var id=agente.id;
		var idAgente=agente.idAgente;
		var estatus = agente.tipoSituacion.valor;
		var estatusAnterior = agente.tipoSituacion.id;
		var idMotivoEstatusAgente = agente.idMotivoEstatusAgente;
		var centroOperacion= agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion;
		var gerencia = agente.promotoria.ejecutivo.gerencia.descripcion;
		var promotoria = agente.promotoria.descripcion;
		var nombreAgente = agente.persona.nombreCompleto;
		var tipoPersona = agente.persona.claveTipoPersona;
		var rfc = agente.persona.rfc;
		var ejecutivo = agente.promotoria.ejecutivo.personaResponsable.nombreCompleto;
		var clasificacionAgente = agente.clasificacionAgentes.valor;
		var numeroFianza = agente.numeroFianza;
		var venceFianza = agente.vencimientoFianzaString;
		var numeroCedula= agente.numeroCedula;
		var venceCedula =agente.vencimientoCedulaString;
		
		jQuery("#id").val(id);
		jQuery("#txtIdAgente").val(idAgente);
		jQuery("#estatusAgente").val(estatus);
		jQuery("#descripcionCentroOperacion").val(centroOperacion);
		jQuery("#descripcionGerencia").val(gerencia);
		jQuery("#descripcionPromotoria").val(promotoria);
		jQuery("#nombreAgente").val(nombreAgente);
		jQuery("tipoPersona").val(tipoPersona);
		jQuery("#rfcAgente").val(rfc);
		jQuery("#ejecutivo").val(ejecutivo);
		jQuery("#tipoAgente").val(clasificacionAgente);
		jQuery("#numeroFianza").val(numeroFianza);
		jQuery("#venceFianza").val(venceFianza);
		jQuery("#numeroCedula").val(numeroCedula);
		jQuery("#venceCedula").val(venceCedula);
		jQuery("#estatusAgenteAnterior").val(estatusAnterior);
		jQuery("#motivoEstatusAnterior").val(idMotivoEstatusAgente);
		
		if(jQuery("#solicitante").val()==""){
			jQuery("#solicitante").val(jQuery("#nombreUsuarioActual").val());			
		}
//		var url="/MidasWeb/fuerzaventa/agente/mostrarDocGenerico.action";
//		var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"CARTA DE SUSPENSION","nombreCarpeta":"SUSPENSIONES","nombreGaveta":"AGENTES"};
//		crearEstructuraAgente(url, data);
		
	}
}

function dateFormatMex(fecha){
	if(fecha!=null){
		var date = new Date(fecha);
//		date=toDate(date);
		var d = (date.getDate()<10)?"0"+date.getDate():date.getDate();
		var m = ((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
		var y = date.getFullYear(); 
		return d+"/"+m+"/"+y;
	}else{
		return null;
	}
}

//function guardarSolicitudSuspension(){
//	
//	var id = jQuery("#id").val();
//	var idAgente = jQuery("#txtIdAgente").val();
//	var url="/MidasWeb/fuerzaventa/agente/matchDocumentosGenerico.action";
//	var data={"agente.idAgente":idAgente,"agente.id":id, "nombreDocumento":"CARTA DE SUSPENSION","nombreCarpeta":"SUSPENSIONES","nombreGaveta":"AGENTES"};
//	jQuery.asyncPostJSON(url,data,$_guardar);
//	crearEstructuraAgente
	
//	var estatusMovimiento = jQuery("#estatusMovimiento").val();
//	var estatusAgenteAnterior = jQuery("#estatusAgenteAnterior").val();
//	var motivoEstatusAnterior = jQuery("#motivoEstatusAnterior").val();
//	
//	var fechaSolicitud = jQuery("#fechaSolicitud").val();
//	if(validateAll(true)){
//		sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/guardar.action?"+jQuery(document.suspensionesForm).serialize()
//				+"&suspension.estatusMovimiento.id="+estatusMovimiento
//				+"&suspension.agente.tipoSituacion.id="+estatusAgenteAnterior
//				+"&suspension.agente.idMotivoEstatusAgente="+motivoEstatusAnterior,
//				targetWorkArea);
//	}	
//}

function $_guardar(json){
	if(json){
		var estatusMovimiento = jQuery("#estatusMovimiento").val();
		var estatusAgenteAnterior = jQuery("#estatusAgenteAnterior").val();
		var motivoEstatusAnterior = jQuery("#motivoEstatusAnterior").val();
		
		var fechaSolicitud = jQuery("#fechaSolicitud").val();
		var count = 0;
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
				for(var i=0;i<lista.length;i++){
					if (lista[i].existeDocumento == 0){
						count = count+1;
					}
				}
				if (count==0){
					if(validateAll(true)){
						sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/guardar.action?"+jQuery(document.suspensionesForm).serialize()
								+"&suspension.estatusMovimiento.id="+estatusMovimiento
								+"&suspension.agente.tipoSituacion.id="+estatusAgenteAnterior
								+"&suspension.agente.idMotivoEstatusAgente="+motivoEstatusAnterior,
								targetWorkArea);
				}
		}else{
			parent.mostrarMensajeInformativo('Hay '+count+' documentos sin digitalizar',"10");
		}
	}else{
		parent.mostrarMensajeInformativo('Hay documentos sin digitalizar',"10");
	}
}
}

function guardarSolicitudSuspension(){
	if(validateAll(true)){
		var id = jQuery("#id").val();
		var idAgente = jQuery("#txtIdAgente").val();
		var estatusMovimiento = jQuery("#estatusMovimiento").val();
		var estatusAgenteAnterior = jQuery("#estatusAgenteAnterior").val();
		var motivoEstatusAnterior = jQuery("#motivoEstatusAnterior").val();
//		alert("ok");
		sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/guardar.action?"+jQuery(document.suspensionesForm).serialize()
				+"&suspension.estatusMovimiento.id="+estatusMovimiento
				+"&suspension.agente.tipoSituacion.id="+estatusAgenteAnterior
				+"&suspension.agente.idMotivoEstatusAgente="+motivoEstatusAnterior,
				targetWorkArea);
	}
}

function regresarSuspensiones(){
	sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/mostrarContenedor.action",targetWorkArea);
}

function estatusMovDefault(){
	jQuery("#estatusMovimiento option").each(function(){
		if(jQuery(this).text() == "PENDIENTE"){
			jQuery(this).attr("selected","selected");
		}
	});		
}

function fechaActual(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	fecha = dd+'/'+mm+'/'+yyyy; 
	return fecha;
}

function autorizarSuspension(){
	auditarDocumentosSuspensionDigitalizados();
	var suspension_id = jQuery("#suspension_id").val();
	var agente_id = jQuery("#id").val();
	sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/autorizarSuspension.action?suspension.id="+suspension_id+"&suspension.agente.id="+agente_id,targetWorkArea);
}
function rechazarSuspension(){
	var suspension_id = jQuery("#suspension_id").val();
	sendRequestJQ(null, "/MidasWeb/fuerzaventa/suspensiones/rechazarSuspension.action?suspension.id="+suspension_id,targetWorkArea);
}

function ocultaBtnAutorizarYRechazar(){
	var estatusMovimiento = jQuery("#estatusMovimiento :selected").text();
	if(estatusMovimiento!="PENDIENTE"){
		jQuery("#btn_autorizar").css("display","none");
		jQuery("#btn_rechazar").css("display","none");		
	}
}

function generarLigaIfimax(){
	var idAgente = jQuery("#id").val();
	var url='/MidasWeb/fuerzaventa/suspensiones/generarLigaIfimax.action?suspension.agente.id='+idAgente;
	window.open(url,'Fortimax');
}

//function guardarDocumentosFortimax(){
//	var url="/MidasWeb/fuerzaventa/agente/guardarDocumentos.action?"+jQuery("#agenteDocumentosForm").serialize();
//	var data;
//	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantes);
//}
function auditarDocumentos(){
	var url="/MidasWeb/fuerzaventa/suspensiones/matchDocumentos.action?"+jQuery("#suspensionesForm").serialize();
	var data="";
	jQuery.asyncPostJSON(url,data,populateDocumentosFortimax);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimax(json){
	if(json){
		var listaDoc=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(listaDoc)){
			jQuery("#suspensionesForm input[type='checkbox']").each(function(i,elem){
				if(listaDoc[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
			});
		}
	}
}

function muestraSoloOpcionPermitida(){
	var estatus = jQuery("#estatusAgenteSituacion option:selected").text();	  
	var url="/MidasWeb/fuerzaventa/suspensiones/mostrarMotEstatusXSituacionAgente.action?situacionAgenteString="+estatus;
	var data="";
	jQuery.asyncPostJSON(url,data,returnMuestraSoloOpcionPermitida);
}
function returnMuestraSoloOpcionPermitida(json){	
	var opt="<option value=''>SELECCIONE...</option>";	
	for(i=0;i<json.listaMotivoEstatus.length;i++){
		var jsonList=json.listaMotivoEstatus[i];
		var val=eval('jsonList.valor');
		var id=eval('jsonList.id');		
		opt+="<option value='"+id+"'>"+val+"</option>";		
	}	
	jQuery("#motivoEstatus").html(opt);
	addToolTipOnchange("motivoEstatus");
}


function muestraDocumentosADigitalizarSusp(){
	var id = jQuery("#Id").val();	
	var idSuspension = jQuery("#suspension_id").val();
	var url="/MidasWeb/fuerzaventa/suspensiones/mostrarDocumentosADigitalizar.action";
	var data={"suspension.id":idSuspension,"suspension.agente.id":id};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizarSusp);
}

function muestraHtmlDocDigitalizarSusp(json){
	var datos = json.listaDocumentosFortimaxView;	
	var estructura = "<table class='contenedorConFormato w800'>";	
	var val;
	var entregado;
	for(i=0;i<datos.length;i++){				
		val=datos[i].valor;		
//		alert(datos[i].checado);
		if(datos[i].checado==0){
			entregado="NO DIGITALIZADO";
		}else{
			entregado="DIGITALIZADO";
		}
		estructura+="<tr><td>"+val+"</td><td>"+entregado+"</td>";
		if(datos[i].checado==0){			
			estructura+="<td><div class='btn_back w180'><a href='javascript: void(0);' id='"+val+"' class='icon_imprimir btn_digitalizarDoc' onclick='generarLigaIfimaxSuspencionesFortimax(this);'>Digitalizar Documento</a></div></td>";
		}
		estructura+="</tr>";
	}
	estructura+="<tr><td><div align='right' class='w80 inline '><div class='btn_back w80'><a onclick='auditarDocumentosSuspensionDigitalizados();' href='javascript:void(0);'>Auditar</a></div></div></td></tr>"
	estructura+="</table>";
//	alert(estructura);
	jQuery("#muestraDocumentosADigitalizar").html(estructura);
}

function generaEstructuraFortimax(){
	var idAgente = jQuery("#id").val();	
	var idSuspension = jQuery("#suspension_id").val();	
	var tipoPersona = jQuery("#tipoPersona").val();
	var url="/MidasWeb/fuerzaventa/suspensiones/generarDocSuspensionEnFortimax.action";
	var data={"suspension.id":idSuspension,"suspension.agente.id":idAgente,"suspension.agente.persona.claveTipoPersona":tipoPersona};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizarSusp);	
}

function generarLigaIfimaxSuspencionesFortimax(nombreDocumento){
	var idAgente = jQuery("#id").val();	
	var nombreDoc = nombreDocumento.id;
	var url='/MidasWeb/fuerzaventa/suspensiones/generarLigaIfimaxSuspension.action?suspension.agente.id='+idAgente+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agente.persona.claveTipoPersona")+'&nombreDocumento='+nombreDoc;
	window.open(url,'Fortimax');
}

function auditarDocumentosSuspensionDigitalizados(){
	var idAgente = jQuery("#id").val();
	var idSuspension = jQuery("#suspension_id").val();	
	
	var url = "/MidasWeb/fuerzaventa/suspensiones/auditarDocumentosSuspensionDigitalizar.action?suspension.id="+idSuspension+"&suspension.agente.id="+idAgente;
	jQuery.asyncPostJSON(url,null,muestraHtmlDocDigitalizarSusp);
}