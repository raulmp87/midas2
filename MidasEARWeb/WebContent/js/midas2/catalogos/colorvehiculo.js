var colorVehiculoGrid;

function mostrarCatalogoColorVehiculo() {
	sendRequestJQ(null, mostrarCatalogoColorVehiculoPath, targetWorkArea, 'listarFiltradoColorVehiculo();');
}

function guardarColorVehiculo() {
	sendRequestJQ(null, guardarColorVehiculoPath+"?"+jQuery(document.colorVehiculoForm).serialize(), targetWorkArea, 'listarFiltradoColorVehiculo();');
}


function eliminarColorVehiculo() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarColorVehiculoPath + "?"+jQuery(document.colorVehiculoForm).serialize(), targetWorkArea, 'listarFiltradoColorVehiculo();');
	}
}


function listarFiltradoColorVehiculo(){
	colorVehiculoGrid = new dhtmlXGridObject("colorVehiculoGrid");
	var form = document.colorVehiculoForm;
	if(form!=null){
		colorVehiculoGrid.load(listarFiltradoColorVehiculoPath+"?"+jQuery(document.colorVehiculoForm).serialize());
	}else{
		colorVehiculoGrid.load(listarFiltradoColorVehiculoPath);
	}
}

function verDetalleColorVehiculo (tipoAccion) {
	if(colorVehiculoGrid.getSelectedId() != null){
		var idColorVehiculo = getIdFromGrid(colorVehiculoGrid, 0);
		sendRequestJQ(null, verDetalleColorVehiculoPath + "?tipoAccion=" + tipoAccion + "&id=" + idColorVehiculo, targetWorkArea, null);
	}
}

function nuevoColorVehiculo (tipoAccion) {
	sendRequestJQ(null, verDetalleColorVehiculoPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}