var catalogoTipoServicioList;
var descripcionRegistroNoHomologado = 'Es requerido homologar catalogo.';
var tipoServicioSapAmisId = 'tipoServicioSapAmis';
var selectTipoServicioSapAmisId = 'selectTipoServicioSapAmis';
var grupoParametroGeneralTipoServicio = 'Tipo Servicio';

function actualizacionUnitaria(tipo){
	obtenerCatalogoTipoServicio(function(){
		if(tipo == 1){
			if(document.getElementById(tipoServicioSapAmisId).innerHTML.trim() == 0){
				document.getElementById(tipoServicioSapAmisId).innerHTML = descripcionRegistroNoHomologado;
			}else{
				for(var i=0; i<catalogoTipoServicioList.length; i++){
					if(document.getElementById(tipoServicioSapAmisId).innerHTML.trim() == catalogoTipoServicioList[i].claveTipoValor){
						document.getElementById(tipoServicioSapAmisId).innerHTML = catalogoTipoServicioList[i].claveTipoValor + ' - ' + catalogoTipoServicioList[i].descripcionParametroGeneral;
					}
				}
			}
		}else{
			crearEstructuraSelect(tipoServicioSapAmisId);
			obtenerDescripcionTipoServicio();
		}
	});
}

function obtenerDescripcionTipoServicio(){
	var iElementHTML = document.getElementById('resultados').getElementsByTagName('table')[0].getElementsByTagName('tbody')[0];
	var iElementHTML2 = iElementHTML.getElementsByTagName('tr');
	for(var c=0; c<iElementHTML2.length; c++){
		if(iElementHTML2[c].getElementsByTagName('td')[3].innerHTML.trim() == 0){
			iElementHTML2[c].getElementsByTagName('td')[3].innerHTML = descripcionRegistroNoHomologado;
		}else{
			for(var i=0; i<catalogoTipoServicioList.length; i++){
				if(iElementHTML2[c].getElementsByTagName('td')[3].innerHTML.trim() == catalogoTipoServicioList[i].valor){
					iElementHTML2[c].getElementsByTagName('td')[3].innerHTML = catalogoTipoServicioList[i].valor + ' - ' + catalogoTipoServicioList[i].descripcionParametroGeneral;
				}
			}
		}
	}
}

function crearEstructuraSelect(idElementHTML){
	document.getElementById(idElementHTML).innerHTML = '';
	var iElementSelect = document.createElement('select');
	    iElementSelect.id = selectTipoServicioSapAmisId;
	    iElementSelect.name = 'claveAmisTipoServicio';
	    iElementSelect.className = 'cajaTexto';
	document.getElementById(idElementHTML).appendChild(iElementSelect);
	agregarOpcionesSelect(iElementSelect.id);
}

function agregarOpcionesSelect(idElementHTML){
	var iElementOption = document.createElement('option');
	    iElementOption.id = 'optionVacia';
	    iElementOption.value = '0';
	    iElementOption.innerHTML = 'Seleccionar Opcion.';
	document.getElementById(idElementHTML).appendChild(iElementOption);
	for(var i=0; i<catalogoTipoServicioList.length; i++){
		agregarOpcionSelect(idElementHTML, catalogoTipoServicioList[i]);
	}
}

function agregarOpcionSelect(iElementSelect, catalogoTipoServicio){
	var iElementOption = document.createElement('option');
	    iElementOption.id = 'option' + catalogoTipoServicio.claveTipoValor;
	    iElementOption.value = catalogoTipoServicio.claveTipoValor;
	    iElementOption.innerHTML = catalogoTipoServicio.descripcionParametroGeneral;
	document.getElementById(iElementSelect).appendChild(iElementOption);
}

function obtenerCatalogoTipoServicio(callBack) {
	var parametrosGenerales = {};
		parametrosGenerales.groupName = grupoParametroGeneralTipoServicio;
	jQuery.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
	    url: '/MidasWeb/catalogos/parametroGeneral/listByGroup.action',
        cache: false,
        data: JSON.stringify(parametrosGenerales),
	    async: true,
	    success: function(cifrasResponse) {
	    	catalogoTipoServicioList = cifrasResponse.listaParametroGeneral;
			callBack();
	    },
	    error:function () {
            callBack();
        }
	});
}

function funcionGuardar(tipo){
	if(document.getElementById(selectTipoServicioSapAmisId).getValue() != null && document.getElementById(selectTipoServicioSapAmisId).getValue() > 0){
		if(tipo == 1){
			sendRequest(document.tipoUsoVehiculoForm,'/MidasWeb/catalogos/tipousovehiculo/agregar.do', 'contenido','funcionGuardar2()');
		} else {
			sendRequest(document.tipoUsoVehiculoForm,'/MidasWeb/catalogos/tipousovehiculo/modificar.do', 'contenido','funcionGuardar2()');
		}
	} else {
		alert('Es requerido seleccionar el Tipo de Servicio (SAP-AMIS) correspondiente al Tipo de Uso.');
	}
}

function funcionGuardar2(){
	validaGuardarModificarM1();
	actualizacionUnitaria(2);
}