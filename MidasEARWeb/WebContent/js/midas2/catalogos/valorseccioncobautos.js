var valorSeccionCobAutosGrid;

function mostrarCatalogoValorSeccionCobAutos() {
	sendRequestJQ(null, mostrarCatalogoValorSeccionCobAutosPath, targetWorkArea, 'initValorSeccionCobAutosGrid();');
}

function guardarValorSeccionCobAutos() {
	var tiposa = dwr.util.getValue("claveTipoLimiteSumaAseg");
	if(tiposa==="2"){
		$('valorSumaAseguradaMin').value="0";
		$('valorSumaAseguradaMax').value="0";
		$('valorSumaAseguradaDefault').value="0";
	}
	sendRequestJQ(null, guardarValorSeccionCobAutosPath + "?" + 
			jQuery(document.valorSeccionCobAutosForm).serialize(), targetWorkArea, 'listarFiltradoValorSeccionCobAutos();');
}

function eliminarValorSeccionCobAutos() {
	if(confirm("\u00bfEst\u00E1 Seguro de Eliminar Registro?")) {
		sendRequestJQ(null, eliminarValorSeccionCobAutosPath + "?"	+	
				jQuery(document.valorSeccionCobAutosForm).serialize(), targetWorkArea, 'listarFiltradoValorSeccionCobAutos();');
	}
}

function listarFiltradoValorSeccionCobAutos(){
	initValorSeccionCobAutosGrid();
    var idToSeccion = dwr.util.getValue("valorSeccionCobAutos.id.idToSeccion");
	if(idToSeccion != headerValue){
		valorSeccionCobAutosGrid.load(listarFiltradoValorSeccionCobAutosPath + "?" + jQuery(document.valorSeccionCobAutosForm).serialize());
	}
	
}

function initValorSeccionCobAutosGrid(){
	document.getElementById("valorSeccionCobAutosGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	valorSeccionCobAutosGrid = new dhtmlXGridObject("valorSeccionCobAutosGrid");
}

function verDetalleValorSeccionCobAutos (tipoAccion) {
	if(valorSeccionCobAutosGrid != null && valorSeccionCobAutosGrid.getSelectedId() != null){
		var idToSeccion = getIdFromGrid(valorSeccionCobAutosGrid, 0);
		var idToCobertura = getIdFromGrid(valorSeccionCobAutosGrid, 1);
		var idMoneda = getIdFromGrid(valorSeccionCobAutosGrid, 2);
		var idTcTipoUsoVehiculo = getIdFromGrid(valorSeccionCobAutosGrid, 3);
		
		sendRequestJQ(null, verDetalleValorSeccionCobAutosPath + "?tipoAccion=" + tipoAccion + 
				"&id.idToSeccion=" + idToSeccion + 
				"&id.idToCobertura=" + idToCobertura + 
				"&id.idMoneda=" + idMoneda + 
				"&id.idTcTipoUsoVehiculo=" + idTcTipoUsoVehiculo, targetWorkArea, null);
	}
}

function getCoberturasSeccionPorSeccion(lineaNegocioSelect, coberturaSelect) {
	var idToSeccion =  dwr.util.getValue(lineaNegocioSelect);
	if (idToSeccion != headerValue) {
		listadoService.getMapCoberturasSeccionPorSeccion(idToSeccion, function(coberturaSeccionMap) {
			addOptions(coberturaSelect, coberturaSeccionMap);
		});
	} else {
		addOptions(coberturaSelect, null);
	}
}

function getTipoUsoVehiculoPorSeccion(lineaNegocioSelect, tipoUsoVehiculoSelect) {
	var idToSeccion =  dwr.util.getValue(lineaNegocioSelect);
	if (idToSeccion != headerValue) {
		listadoService.getMapTipoUsoVehiculoPorSeccion(idToSeccion, function(tipoUsoVehiculoMap) {
			addOptions(tipoUsoVehiculoSelect, tipoUsoVehiculoMap);
		});
	} else {
		addOptions(tipoUsoVehiculoSelect, null);
	}
}

function nuevoValorSeccionCobAutos (tipoAccion) {
	sendRequestJQ(null, verDetalleValorSeccionCobAutosPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}

function validaSumaAsegurada(tiposumaasegurada){
	var tiposa=tiposumaasegurada.value;
	if(tiposa=="0"){
		$('valorSumaAseguradaMin').readOnly=true;
		$('valorSumaAseguradaMax').readOnly=true;
		$('valorSumaAseguradaDefault').readOnly=true;
		$('valorSumaAseguradaMin').value="0";
		$('valorSumaAseguradaMax').value="0";
		$('valorSumaAseguradaDefault').value="0";
	}
}

function ocultarSumaAsegurada(tiposumaasegurada){
	var tiposa = dwr.util.getValue("claveTipoLimiteSumaAseg");
	if(tiposa=="2" ||  tiposa==""){
		document.getElementById('tiposSumasAseguradas1').style.display = 'none'; 
		document.getElementById('tiposSumasAseguradas2').style.display = 'none';
		document.getElementById('tiposSumasAseguradas3').style.display = 'none';
		document.getElementById('tiposSumasAseguradas4').style.display = 'none';
		document.getElementById('tiposSumasAseguradas5').style.display = 'none';
		document.getElementById('tiposSumasAseguradas6').style.display = 'none';
		$('#valorSumaAseguradaMin').val="0";
		$('#valorSumaAseguradaMax').value="0";
		$('#valorSumaAseguradaDefault').value="0";
	}else {
		document.getElementById('tiposSumasAseguradas1').style.display = 'block'; 
		document.getElementById('tiposSumasAseguradas2').style.display = 'block'; 
		document.getElementById('tiposSumasAseguradas3').style.display = 'block'; 
		document.getElementById('tiposSumasAseguradas4').style.display = 'block'; 
		document.getElementById('tiposSumasAseguradas5').style.display = 'block'; 
		document.getElementById('tiposSumasAseguradas6').style.display = 'block'; 
	}
}

function limpiarValorSA(){
	$('valorSumaAseguradaMin').value="0";
	$('valorSumaAseguradaMax').value="0";
	$('valorSumaAseguradaDefault').value="0";
}

function number_format_simple (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}