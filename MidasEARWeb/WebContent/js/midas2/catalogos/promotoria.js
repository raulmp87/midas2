var promotoriaGrid;
var ventanaResponsable=null;

function listarFiltradoPromotoria(){	

	promotoriaGrid = new dhtmlXGridObject("promotoriaGrid");
	var form = document.promotoriaForm;
	if(form!=null){
		promotoriaGrid.load(listarFiltradoPromotoriaPath+"?"+jQuery("#promotoriaForm").serialize());
	}else{
		promotoriaGrid.load(listarFiltradoPromotoriaPath);
	}
}

function agregarPromotoria(tipoAccion) {
	sendRequestJQ(null, verDetallePromotoriaPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);	
	
}

function guardarPromotoria() {
		
	if(validateForId("promotoriaForm",true)){
		var tipoAccion = jQuery("#tipoAccion").val();
		sendRequestJQAsync(null, "/MidasWeb/fuerzaventa/promotoria/guardarPromotoria.action?"
			+ jQuery("#promotoriaForm").serialize()+ "&tipoAccion=" + tipoAccion,
			targetWorkArea);
	}
	
}


function eliminarPromotoria(){

	if(confirm("Esta seguro de Eliminar el Registo")){
		sendRequestJQAsync(null, "/MidasWeb/fuerzaventa/promotoria/eliminar.action?"
			+ jQuery("#promotoriaForm").serialize(),
			targetWorkArea, null);
	}
}

function mostrarHistoricoPromotoria(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/promotoria/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+ "&idRegistro="+idRegistro;
	mostrarModal("historicoGrid", 'Historial', 100, 50, 940, 500, url);
}
//function obtenerHistorico(){
//var url = '/MidasWeb/fuerzaVenta/persona/loadHistory.action?idTipoOperacion='+40+ "&idRegistro="+dwr.util.getValue("idPromotoria");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}

function salirDePromotoria(){
	var path= "/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, path, targetWorkArea, null);
	}else{
		if(confirm('Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?')){
			sendRequestJQAsync(null, path, targetWorkArea, null);		
		}		
	}
}

function populateDomicilio(json){
	if(json){
		/*var estadoName="promotoria.domicilio.claveEstado";
		var paisName="promotoria.domicilio.clavePais";
		var ciudadName="promotoria.domicilio.claveCiudad";
		var coloniaName="promotoria.domicilio.nombreColonia";
		var calleNumeroName="promotoria.domicilio.calleNumero";
		var codigoPostalName="promotoria.domicilio.codigoPostal";*/
		var estadoName="promotoria.personaResponsable.domicilios[0].claveEstado";
		var paisName="promotoria.personaResponsable.domicilios[0].clavePais";
		var ciudadName="promotoria.personaResponsable.domicilios[0].claveCiudad";
		var coloniaName="promotoria.personaResponsable.domicilios[0].nombreColonia";
		var calleNumeroName="promotoria.personaResponsable.domicilios[0].calleNumero";
		var codigoPostalName="promotoria.personaResponsable.domicilios[0].codigoPostal";
		var idDomicilioName="promotoria.personaResponsable.domicilios[0].idDomicilio.idDomicilio";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		var idDomicilioValue=json.idDomicilio.idDomicilio;
		dwr.util.setValue(idDomicilioName,idDomicilioValue);
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
		//Cambio para poner hidden los campos de domicilio
	}
}

/****Datos contables***********************************/
function onChangeBanco(){
	var idBanco=(dwr.util.getValue("idBanco")!='')?dwr.util.getValue("idBanco"):'-1';
	var target="idProductoBancario";
	var url="/MidasWeb/fuerzaventa/promotoria/listarProductosBancariosPorBanco.action";
	var params={"productoBancarioSeleccionado.banco.idBanco":idBanco};
	jQuery.asyncPostJSON(url,params,function(data){
		var dataCombo=new Array();
		var arr=data.productosBancarios;
		jQuery.fillCombo(target,data.productosBancarios,'id','descripcion',true);
		jQuery("#tipoProducto").val('');
	});
}

function onChangeProductoBancario(){
	var idProducto=dwr.util.getValue("idProductoBancario");	
	
	var url="/MidasWeb/fuerzaventa/promotoria/cargarProductoBancario.action";
	var params={"productoBancarioSeleccionado.id":idProducto};
	limpiaCampos();
	jQuery.asyncPostJSON(url,params,function(data){
		var producto=data.productoBancarioSeleccionado;
		var claveCredito=producto.claveCredito;
		var claveTransfelectronica=producto.claveTransfelectronica;
		var tipoProducto="";
		var removerRequerido = ["montoCreditoLb","numeroPlazosLb","montoPagoLb","numeroCuentaLb","numeroClabeLb"];
		
		if(claveCredito==1){			
			tipoProducto="Crédito";
			var fieldsEnable=["montoCredito","numeroPlazos","montoPago","sucursal","plaza"];
			var fieldsDisable=["numeroCuenta","numeroClabe","claveDefault"];
			setAttribute("disabled",false,fieldsEnable);
			isRequired(true,fieldsEnable);
			setAttribute("disabled",true,fieldsDisable);
			jQuery("#claveDefault").attr("checked",false);
			var agregarRequerido= ["montoCreditoLb","numeroPlazosLb","montoPagoLb"];
			noEsRequerido(removerRequerido);
			esRequerido(agregarRequerido);			
		}else if(claveTransfelectronica==1){	
			tipoProducto="Transferencia Electrónica";
			var fieldsEnable=["claveDefault","numeroCuenta","numeroClabe","sucursal","plaza"];
			var fieldsDisable=["montoCredito","montoPago","numeroPlazos"];
			var requeridos = ["claveDefault","numeroCuenta","sucursal","plaza"];
			setAttribute("disabled",false,fieldsEnable);
			setAttribute("disabled",true,fieldsDisable);
			isRequired(true,requeridos);
			var agregarRequerido = ["numeroCuentaLb"];
			noEsRequerido(removerRequerido);
			esRequerido(agregarRequerido);
			
		}
		jQuery("#tipoProducto").val(tipoProducto);
		
	});
}

function isRequired(isRequired,fields,accion){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var elem=jQuery.getObject(id);
		if(elem!=null){
			if(isRequired){
				elem.addClass("jQrequired");
			}else{
				elem.removeClass("jQrequired");
			}
		}
	}
}

function setAttribute(attribute,value,fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var elem=jQuery.getObject(id);
		if(elem!=null){
			elem.attr(attribute,value);
		}
	}
}

function esRequerido(fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var texto =	jQuery("#"+id).text();
		jQuery.trim(texto);
		texto = texto+" *";
		jQuery("#"+id).text(texto);	
	}
}

function noEsRequerido(fields){
	for(var i=0;i<fields.length;i++){
		var id=fields[i];
		var tex = jQuery("#"+id).text();
		jQuery.trim(tex);
		tex.replace("*"," ");
		jQuery("#"+id).text(tex.replace("*"," "));	
	}
}

function agregarProducto(){
	if(validateForId("productosPromotoriaForm",true)){
		var url="/MidasWeb/fuerzaventa/promotoria/guardarProductoBancario.action";
		var idBancos = jQuery("#idBanco").val();
		var nombreBancos = jQuery("#idBanco option:selected").html();
		var productosBancariosName = jQuery("#idProductoBancario option:selected").html();	
		var productosBancarios = jQuery("#idProductoBancario").val();		
		var tipoProducto = jQuery("#tipoProducto").val();
		var numeroCuenta = jQuery("#numeroCuenta").val();
		var numeroClabe = jQuery("#numeroClabe").val();
		var montoCredito = jQuery("#montoCredito").val();
		var numeroPlazos = jQuery("#numeroPlazos").val();
		var montoPago = jQuery("#montoPago").val();
		var sucursal = jQuery("#sucursal").val();
		var plaza = jQuery("#plaza").val();
//		var claveDefault=jQuery("#claveDefault").val();
		var claveDefault = jQuery("#claveDefault").attr("checked");
		var producto={
			"producto.productoBancario.banco.idBanco":idBancos,
			"producto.productoBancario.id":productosBancarios,
			"producto.claveDefaultBoolean":claveDefault,
			"producto.numeroCuenta":numeroCuenta,
			"producto.numeroClabe":numeroClabe,
			"producto.montoCredito":montoCredito,
			"producto.numeroPlazos":numeroPlazos,
			"producto.montoPago":montoPago,
			"producto.sucursal":sucursal,
			"producto.plaza":plaza
		};
				
		var count=0;
		var registros=gridProductosBancarios.getRowsNum();
		for(i=1;i<=registros;i++){
			var claveDefault=gridProductosBancarios.cellById(i, 7).getValue();
			if(claveDefault=="Si"){
				count++;
			}
		}	
		if(count>=1&&jQuery("#claveDefault").attr("checked")==true){
			mostrarMensajeInformativo("Ya existe un Producto Bancario marcado como clave de pago","10");
		}else{		
			jQuery.asyncPostJSON(url,producto,function(json){
			alert(json.mensaje);
			var idPromotoria=jQuery("#idPromotoria").val();
			var urlGrid="/MidasWeb/fuerzaventa/promotoria/cargarProductosBancariosPorPromotoria.action?promotoria.id="+idPromotoria;
			gridProductosBancarios.loadXML(urlGrid);
			if(json.tipoMensaje==30){
				cleanForm("productosPromotoriaForm");
				jQuery("#claveDefault").removeAttr("checked");
			}
		});
		}
	}
}

function cleanForm(form){
	var forma=jQuery("#"+form);
	if(forma){
		jQuery.clearForm(form);
		jQuery("#idProductoBancario").val("-1");
	}
}

function eliminarProductoBancario(idProducto){
	var url="/MidasWeb/fuerzaventa/promotoria/eliminarProductoBancario.action";
	var data={"producto.id":idProducto};
	if(idProducto==null){
		alert("Favor de elegir un producto a eliminar");
	}else{
		if(confirm("Esta seguro de eliminar este producto?")){
			jQuery.asyncPostJSON(url,data,function(json){
				alert(json.mensaje);
				var idPromotoria=jQuery("#idPromotoria").val();
				var urlGrid="/MidasWeb/fuerzaventa/promotoria/cargarProductosBancariosPorPromotoria.action?promotoria.id="+idPromotoria;
				gridProductosBancarios.loadXML(urlGrid);
			});
		}
	}
}

/**
 * Funcion para inicializar tabs, si es una alta, deshabilita el resto de los tabs hasta que se guarde la primer pestania
 */
function incializarTabs(tipoAccion){
	var idPromotoria=jQuery("#idPromotoria").val();
	contenedorTab=window["promotoriaTabBar"];
	//Nombre de todos los tabs
	var tabs=["datosPrincipalesPromotoria","datosContables"];
	var isDisabled=false;
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
	if(!jQuery.isValid(idPromotoria)){
		isDisabled=true;
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
	for(var i=0;i<tabs.length;i++){
		var tabName=tabs[i];
		var tab=contenedorTab[tabName];
		jQuery("div[tab_id*='"+tabName+"'] span").bind('click',function(event){
			if(!isDisabled){
				if(!confirm("Est\u00E1 a punto de abandonar la secci\u00F3n, \u00bfDesea continuar?")){
					//event.preventDefault();
					event.stopPropagation();
				}
			}
		});
	}
	if(parent.dwr.util.getValue("tipoAccion")==1 || parent.dwr.util.getValue("tipoAccion")==4){	
//		alert(parent.dwr.util.getValue("tipoAccion"));
		jQuery("#btnAtras").bind('click',function(event){
			if(!isDisabled){
				if(!confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
					//event.preventDefault();
					event.stopPropagation();
				}else{
					sendRequestJQ(null,verDetallePromotoriaPath+"?tabActiva=datosPrincipalesPromotoria&tipoAccion="+tipoAccion+"&idRegistro="+jQuery("#idPromotoria").val()+"&idTipoOperacion=40",targetWorkArea,null);
				}
			}
		});
		jQuery("#btnSiguiente").bind('click',function(event){
			if(!isDisabled){
				if(!confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
					//event.preventDefault();
					event.stopPropagation();
				}else{
					sendRequestJQ(null,verDetallePromotoriaPath+"?tabActiva=datosContables&tipoAccion="+tipoAccion,targetWorkArea,null);
				}
			}else{
				alert("Favor de registrar los datos generales de la promotor\u00EDa");
			}
		});
	}else{
		jQuery("#btnAtras").bind('click',function(event){
			if(!isDisabled){
					event.stopPropagation();
					sendRequestJQ(null,verDetallePromotoriaPath+"?tabActiva=datosPrincipalesPromotoria&tipoAccion="+tipoAccion+"&idRegistro="+jQuery("#idPromotoria").val()+"&idTipoOperacion=40",targetWorkArea,null);
				}
		});
		jQuery("#btnSiguiente").bind('click',function(event){
			if(!isDisabled){
					//event.preventDefault();
					event.stopPropagation();
					sendRequestJQ(null,verDetallePromotoriaPath+"?tabActiva=datosContables&tipoAccion="+tipoAccion,targetWorkArea,null);
			}else{
				alert("Favor de registrar los datos generales de la promotor\u00EDa");
			}
		});
	}
}

function cargarGridDatosBancarios(){
	var url="/MidasWeb/fuerzaventa/promotoria/cargarProductosBancariosPorPromotoria.action?tipoAccion = " +dwr.util.getValue("tipoAccion")+"&ultimaModificacion.fechaHoraActualizacionString="+dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");
	document.getElementById('productoBancario').innerHTML = '';
	gridProductosBancarios = new dhtmlXGridObject('productoBancario');
	gridProductosBancarios.load(url);
}

/***Modal de Persona responsable***************/
function mostrarModalResponsable(){
	var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
	sendRequestWindow(null, url,obtenerVentanaResponsable);
}

function obtenerVentanaResponsable(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 480);
	ventanaResponsable.center();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Consulta de Personas");
	ventanaResponsable.button("park").hide();
	ventanaResponsable.button("minmax1").hide();
	return ventanaResponsable;
}

function findByIdResponsable(idResponsable){
	var url="/MidasWeb/fuerzaVenta/persona/findById.action";
	var data={"personaSeycos.idPersona":idResponsable};
	jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	jQuery("#nombreResponsable").css("background-color","#FFFFFF");
}

function populatePersona(json){
	if(json){
		var idResponsable=json.personaSeycos.idPersona;
		var nombre=json.personaSeycos.nombreCompleto;
		jQuery("#idPersonaResponsable").val(idResponsable);
		jQuery("#nombreResponsable").val(nombre);
		jQuery("#descripcionPromotoria").val(nombre);
		jQuery("#txtCorreo").val(json.personaSeycos.email);
		var domicilios=json.personaSeycos.domicilios;
		var domicilioFiscal=null;
		if(!jQuery.isNull(domicilios)){
			for(var i=0;i<domicilios.length;i++){
				var domicilio=domicilios[i];
				var domicilioGral= domicilios[0];
				var tipoDomicilio=domicilio.tipoDomicilio;
				if("FISC"==tipoDomicilio){
					domicilioFiscal=domicilio;
					break;
				}
			}
		}
		if(!jQuery.isNull(domicilioFiscal)){
			populateDomicilio(domicilioFiscal);
		}else if(!jQuery.isNull(domicilioGral)){
			populateDomicilio(domicilioGral);
	}else{
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].clavePais","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveEstado","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveCiudad","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].nombreColonia","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].calleNumero","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].codigoPostal","");
		}
		/*if(!jQuery.isNull(domicilioFiscal)){
			
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].clavePais",domicilioFiscal.clavePais);
			
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveEstado",domicilioFiscal.claveEstado);
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveCiudad",domicilioFiscal.claveCiudad);
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].nombreColonia",domicilioFiscal.nombreColonia);
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].calleNumero",domicilioFiscal.calleNumero);
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].codigoPostal",domicilioFiscal.codigoPostal);
		}else{
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].clavePais","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveEstado","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].claveCiudad","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].nombreColonia","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].calleNumero","");
			dwr.util.setValue("promotoria.personaResponsable.domicilios[0].codigoPostal","");
		}*/
	}
}


function limpiaCampos(){
	jQuery("#tipoProducto").val("");
	jQuery("#numeroCuenta").val("");
	jQuery("#numeroClabe").val("");
	jQuery("#montoCredito").val("");
	jQuery("#numeroPlazos").val("");
	jQuery("#montoPago").val("");
	jQuery("#sucursal").val("");
	jQuery("#plaza").val("");
	jQuery("#claveDefault").removeAttr("checked");
}

function guardarProdBancario(){
	var count=0;
	var registros=gridProductosBancarios.getRowsNum();
	for(i=0;i<registros;i++){
		var claveDefault=(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),7).getValue()=='Si')?'Si':'No';
		if(claveDefault=="Si"){
			count++;
		}
	}
	if(registros==0){
		mostrarMensajeInformativo("No hay productos bancarios ha guardar","10");
	}
	else{
		if(count<1){
			mostrarMensajeInformativo("Es requerido un Producto Bancario marcado como clave de pago","10");
		}
		else{
			var arrProdBanc = new Array();
			for (var i=0;i<registros; i++){
				//"productoBancario.banco.idBanco":gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),2).getValue(),
				//productoBancario.id
			var elementosGrid = {
					"idProductoBancario":gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),2).getValue(),
					"claveDefault":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),7).getValue()=="Si")?1:0,
					"numeroCuenta":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),5).getValue()=="N/A")?null:gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),5).getValue(),
					"numeroClabe":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),6).getValue()=="N/A")?null:gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),6).getValue(),
					"montoCredito":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),8).getValue()=="N/A")?null:gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),8).getValue(),
					"numeroPlazos":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),9).getValue()=="N/A")?null:gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),9).getValue(),
					"montoPago":(gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),10).getValue()=="N/A")?null:gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),10).getValue(),
					"sucursal":gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),11).getValue(),
					"plaza":gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),12).getValue(),
					"promotoria.id":jQuery("#idPromotoria").val()
				};
				arrProdBanc.push(elementosGrid);
			}
			
			var url="/MidasWeb/fuerzaventa/promotoria/guardarProductoBancario.action";
			var data=jQuery.convertJsonArrayToParams(arrProdBanc,["idProductoBancario","claveDefault","numeroCuenta","numeroClabe","montoCredito","numeroPlazos","montoPago","sucursal","plaza"],"lista_ProdBancarios");//productoBancario.id
			jQuery.asyncPostJSON(url,data,function(json){
				mostrarMensajeInformativo(json.mensaje,"30");
				var idPromotoria=jQuery("#idPromotoria").val();
				var urlGrid="/MidasWeb/fuerzaventa/promotoria/cargarProductosBancariosPorPromotoria.action?promotoria.id="+idPromotoria;
				gridProductosBancarios.loadXML(urlGrid);
				if(json.tipoMensaje==30){
					cleanForm("productosPromotoriaForm");
					jQuery("#claveDefault").removeAttr("checked");
				}
				jQuery("#txtFechaHora").val(json.fechaHoraActualizFormatoMostrar);
				jQuery("#txtUsuario").val(json.usuarioActualizacion);
			});
		}
	}
}


function agregarProductoInFront(){
	if(validateForId("productosPromotoriaForm",true)){
		var idBancos = jQuery("#idBanco").val();
		var nombreBancos = jQuery("#idBanco option:selected").html();
		var productosBancariosName = jQuery("#idProductoBancario option:selected").html();	
		var productosBancarios = jQuery("#idProductoBancario").val();		
		var tipoProducto = jQuery("#tipoProducto").val();
		var numeroCuenta = (jQuery("#numeroCuenta").val()!='')?jQuery("#numeroCuenta").val():'N/A';
		var numeroClabe =  (jQuery("#numeroClabe").val()!='')?jQuery("#numeroClabe").val():'N/A';
		var montoCredito = jQuery("#montoCredito").val();
		var numeroPlazos = jQuery("#numeroPlazos").val();
		var montoPago =    jQuery("#montoPago").val();
		var sucursal =     jQuery("#sucursal").val();
		var plaza =        jQuery("#plaza").val();
		var claveDefault = (jQuery("#claveDefault").attr("checked"))?"Si":"No";
		
		var new_id = gridProductosBancarios.getRowsNum()+1;
		
		if(existeProdMarcadoClavePago()=='Si' && claveDefault=='Si'){
			mostrarMensajeInformativo("Ya existe un Producto Bancario marcado como clave de pago","10");				
		}else{	
			new_id=new_id+1;
			gridProductosBancarios.addRow(new_id,[new_id,tipoProducto,productosBancarios,productosBancariosName,nombreBancos,numeroCuenta,numeroClabe,claveDefault,montoCredito,numeroPlazos,montoPago,sucursal,plaza,"../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_row("+new_id+")^_self" ])
			cleanForm("productosPromotoriaForm");
			jQuery("#claveDefault").removeAttr("checked");
		}	
	}
}

function existeProdMarcadoClavePago(){
	var existeprodClavePago="No";
	var cont = gridProductosBancarios.getRowsNum();
	var i=0;
	if(cont>0){
		for(i;i<=cont-1;i++){
			existeprodClavePago = (gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),7).getValue()=="Si")?"Si":"No";
			if(existeprodClavePago=="Si"){
				return existeprodClavePago;
			}
		}
	}
	return existeprodClavePago;
}
function delete_row(Id_Row){
	if(confirm("Esta seguro de eliminar este registro?")){
		gridProductosBancarios.deleteRow(Id_Row);
		parent.mostrarMensajeInformativo("El registro ha sido eliminado exitosamente","30");
	}
}
//FUNCION PARA BUSCAR UN AGENTE 
function mostrarListadoAgentes(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}
function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgente(){
	var claveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(claveAgente)){
		var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
		var data={"agente.idAgente":claveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}else{
		jQuery("#id").val("");
		jQuery("#nombreAgente").val("");
		jQuery("#idAgente").val("");
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	
	//Se prefiltran datos para elegir al agente promotor
	prepareFiltrosAgentePromotor();
	
	return ventanaAgentes;
}

function onChangeTipoCapturaClave() {
		
	if (jQuery('#capturaManualClave').is(":checked")) {
	  
		jQuery("#clave").attr("disabled", false).addClass("jQrequired");
		
	} else {
				
		jQuery("#clave").val("").attr("disabled", true).removeClass("jQrequired").removeClass("errorField").attr("title", "");
		
	}
	
}

function loadInfoAgente(json){
	if(json.agente==null || json.agente.idAgente==null){
		alert("La clave del Agente no Existe ó no esta Autorizado ");
		jQuery("#id").val("");
		jQuery("#nombreAgente").val("");
		jQuery("#idAgente").val("");
	}else{
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreAgente = agente.persona.nombreCompleto;
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#nombreAgente").val(nombreAgente);
	}
}


function prepareFiltrosAgentePromotor() {
	
	var url = "/MidasWeb/fuerzaventa/promotoria/findById.action";
	
	var data = {"promotoria.id":jQuery("#idPromotoria").val()};
	
	jQuery.asyncPostJSON(url, data, setFiltrosAgentePromotor);
	
}

function addSingleOption(selectControl, key, value) {
	
	selectControl.append(jQuery("<option></option>").attr("value",key).text(value)); 
	
}

function createHidden (control) {
	
	var layout = "<input type='hidden' name='{0}' value='{1}'>";
		
	return layout.replace("{0}", control.attr("name")).replace("{1}", control.val());
		
}

function setFiltrosAgentePromotor(data) {

	var promotoria = data.promotoria;
	
	var situacionAutorizado = data.situacionAutorizado;
	
	
	//Se establecen los filtros dependientes
	
	addSingleOption(jQuery("#idEjecutivoCatalogo"), promotoria.ejecutivo.id, promotoria.ejecutivo.personaResponsable.nombreCompleto);
	
	addSingleOption(jQuery("#idPromotoriaCatalogo"), promotoria.id, promotoria.descripcion);
	
	
	jQuery("#idGerencia").attr("disabled", true).val(promotoria.ejecutivo.gerencia.id);
	
	jQuery("#idEjecutivoCatalogo").attr("disabled", true).val(promotoria.ejecutivo.id);

	jQuery("#idPromotoriaCatalogo").attr("disabled", true).val(promotoria.id);

	jQuery("#tipoSituacionAgente").attr("disabled", true).val(situacionAutorizado.id);

	
	//Se establecen hidden para que se tomen en cuenta los filtros deshabilitados en la busqueda
	
	jQuery("#agenteForm")
		.append(createHidden(jQuery("#idGerencia")))
		.append(createHidden(jQuery("#idEjecutivoCatalogo")))
		.append(createHidden(jQuery("#idPromotoriaCatalogo")))
		.append(createHidden(jQuery("#tipoSituacionAgente")));

}



