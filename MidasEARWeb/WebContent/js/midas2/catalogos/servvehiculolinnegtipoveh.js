var servVehiculoLinNegTipoVehGrid;

function mostrarCatalogoServVehiculoLinNegTipoVeh() {
	sendRequestJQ(null, mostrarCatalogoServVehiculoLinNegTipoVehPath, targetWorkArea, 'initServVehiculoLinNegTipoVehGrid();');
}

function guardarServVehiculoLinNegTipoVeh() {
	sendRequestJQ(null, guardarServVehiculoLinNegTipoVehPath+"?"+jQuery(document.servVehiculoLinNegTipoVehForm).serialize(), targetWorkArea, 'listarFiltradoServVehiculoLinNegTipoVeh();');
}


function eliminarServVehiculoLinNegTipoVeh() {
	if(confirm("\u00bfEst\u00E1 Seguro de Eliminar Registro?")) {
		sendRequestJQ(null, eliminarServVehiculoLinNegTipoVehPath + "?"+jQuery(document.servVehiculoLinNegTipoVehForm).serialize(), targetWorkArea, 'listarFiltradoServVehiculoLinNegTipoVeh();');
	}
}
	


function listarFiltradoServVehiculoLinNegTipoVeh(){
	initServVehiculoLinNegTipoVehGrid();
    var idToSeccion = dwr.util.getValue("servVehiculoLinNegTipoVeh.id.idToSeccion");
	if(idToSeccion != headerValue){
		servVehiculoLinNegTipoVehGrid.load(listarFiltradoServVehiculoLinNegTipoVehPath+"?"+jQuery(document.servVehiculoLinNegTipoVehForm).serialize());
	}
	
}

function initServVehiculoLinNegTipoVehGrid(){
	document.getElementById("servVehiculoLinNegTipoVehGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	servVehiculoLinNegTipoVehGrid = new dhtmlXGridObject("servVehiculoLinNegTipoVehGrid");
}


function verDetalleServVehiculoLinNegTipoVeh(tipoAccion) {
	if(servVehiculoLinNegTipoVehGrid != null && servVehiculoLinNegTipoVehGrid.getSelectedId() != null){
		var idToSeccion = getIdFromGrid(servVehiculoLinNegTipoVehGrid, 0);
		var idTcTipoVehiculo = getIdFromGrid(servVehiculoLinNegTipoVehGrid, 1);
		var idTcTipoServicioVehiculo = getIdFromGrid(servVehiculoLinNegTipoVehGrid, 2);
		
		sendRequestJQ(null, verDetalleServVehiculoLinNegTipoVehPath + "?tipoAccion=" + tipoAccion + "&id.idToSeccion=" + idToSeccion + "&id.idTcTipoVehiculo=" + idTcTipoVehiculo, targetWorkArea, null);
	}
}

function getTipoVehiculoPorSeccion(lineaNegocioSelect, tipoVehiculoSelect) {
	var idToSeccion =  dwr.util.getValue(lineaNegocioSelect);
	if (idToSeccion != headerValue) {
		listadoService.getMapTipoVehiculoPorSeccion(idToSeccion, function(tipoVehiculoMap) {
			addOptions(tipoVehiculoSelect, tipoVehiculoMap);
		});
	} else {
		addOptions(tipoVehiculoSelect, null);
		addOptions(tipoServicioVehiculoSelect, null);
	}
}


function getTipoServicioVehiculoPorSeccion(tipoVehiculoSelect, tipoServicioVehiculoSelect) {
	var idTcTipoVehiculo =  dwr.util.getValue(tipoVehiculoSelect);
	if (idTcTipoVehiculo != headerValue) {
		listadoService.getMapTipoServicioVehiculoPorTipoVehiculo(idTcTipoVehiculo, function(tipoServicioVehiculoMap) {
			addOptions(tipoServicioVehiculoSelect, tipoServicioVehiculoMap);
		});
	} else {
		addOptions(tipoServicioVehiculoSelect, null);
	}
}

function nuevoServVehiculoLinNegTipoVeh(tipoAccion) {
	sendRequestJQ(null, verDetalleServVehiculoLinNegTipoVehPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}