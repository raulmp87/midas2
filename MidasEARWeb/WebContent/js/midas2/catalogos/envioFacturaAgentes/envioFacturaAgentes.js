/**
 * 
 */
jQuery(function(){
	var urlFiltro="/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/listarFiltrado.action?idAgente="+jQuery("#hiddenIdAgente").val();
	listarFiltradoGenerico(urlFiltro,"divContentTable", null,null,null);
 	});

jQuery(document).ready(function(){
	var pix = null;
	
	if(jQuery.browser.msie){
		pix = 'px';
	}
	
	jQuery("#divContentModal").dialog({buttons: [
													{
														text: "Aceptar",
														click: function() {
															jQuery( this ).dialog( "close" );
															jQuery("#divContentTableDet").html('');
														}
													}
												],
										height: 350+pix,
										width: 570,
										autoOpen: false
									});
})

function init(){
	var url = "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/init.action?idAgente="+jQuery("#inputIdAgente").val();
	sendRequestJQ(null, url, 'contenido_comisiones', null);
}

function cargaDetalle(idEnvio){
	var urlFiltro="/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/listarDetalle.action?idEnvio="+idEnvio;
	listarFiltradoGenerico(urlFiltro,"divContentTableDet", null,null,null);	
	jQuery("#divContentModal").dialog("open");
}

function importarArchivo(){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga de Facturas");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
		

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setFilesLimit(1);
    
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xml") { 
           alert("Solo puede importar archivos XML (.xml). "); 
           return false; 
        }else{
        	return true; 
        }
     }; 
     
    vault.create("vault");
    vault.setFormField("claveTipo", "17");
        
    vault.onBeforeUpload = function(files){
    	jQuery("#file1").attr("name", "facturaXml");
    }
    
    jQuery("#buttonUploadId").unbind("click");
    jQuery("#buttonUploadId").click(function(){
        
    	blockPage();
		jQuery.ajaxFileUpload({
			url: "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/subirArchivo.action?idAgente="+jQuery("#hiddenIdAgente").val(),
			secureuri:false,
			fileElementId: "file1",
			dataType: 'text',
			success: function(data, estatus){
				unblockPage()
				parent.dhxWins.window("divContentForm").close();
				alert(data);
				var url = "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/init.action?idAgente="+jQuery("#hiddenIdAgente").val();
				sendRequestJQ(null, url, 'contenido_comisiones', null);
			},
			error: function(data, estatus){
				unblockPage()
				parent.dhxWins.window("divContentForm").close();
				alert(data);
				var url = "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/init.action?idAgente="+jQuery("#hiddenIdAgente").val();
				sendRequestJQ(null, url, 'contenido_comisiones', null);
			}
		});
	});
}

function setAgente(id){
	var url = "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/init.action?idAgente="+id;
	sendRequestJQ(null, url, 'contenido_comisiones', null);
}


function autorizaExcepcion() {
		
	blockPage();
	jQuery.ajax({
	    type: "POST",
	    url: "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/autorizaExcepcion.action?idAgente="+jQuery("#hiddenIdAgente").val(),
	    data: null,
	    async: true,
	    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
	    success: function(data) {
	    	checkboxCallback();
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
	    	unblockPage();
        }   
	});  
	
	
}

function checkboxCallback() {
	
	jQuery("#autorizacion1").attr("disabled", true);
	jQuery("#autorizacion2").attr("disabled", true);
	
}

function marcarFacturasAntiguas() {
	
	if (confirm('\u00BFEst\u00E1 seguro que el agente ya entreg\u00f3 TODAS las facturas que ten\u00EDa pendientes anteriores a enero de 2015?')) {
		
		var url = "/MidasWeb/fuerzaventa/agente/envioFacturaAgentes/marcarFacturasAntiguas.action?idAgente="+jQuery("#hiddenIdAgente").val();
		sendRequestJQ(null, url, 'contenido_comisiones', null);
		
	}
	
}


