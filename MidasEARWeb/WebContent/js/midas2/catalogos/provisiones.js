function buscaRamosPorFiltro(){
	var idProvision = jQuery("#provision_id").val();
	var idBono = jQuery("#idBono").val();
	var idPromotoria = jQuery("#idPromotoria").val();
	var idGerencia = jQuery("#idGerencia").val();
	var idGerente = jQuery("#idGerente").val();
	var idLineaVenta = jQuery("#idLineaVenta").val();
	var idEjecutivo = jQuery("#idEjecutivo").val();
	var idLineaNegocio = jQuery("#idLineaNegocio").val();
	var idProducto = jQuery("#idProducto").val();
	var idAgente = jQuery("#idAgente").val();
		
	var data="filtrosDetalleProvision.provisionAgentes.id="+idProvision;
	if(idBono!=null&&idBono!=""){
		data = data+"&filtrosDetalleProvision.configBono.id="+idBono;
	}
	if(idAgente!=null&&idAgente!=""){
		data = data+"&filtrosDetalleProvision.agenteOrigen.idAgente="+idAgente;
	}
	if(idPromotoria!=null&&idPromotoria!=""){
		data = data+"&filtrosDetalleProvision.promotoria.id="+idPromotoria;
	}
	if(idGerencia!=null&&idGerencia!=""){
		data = data+"&filtrosDetalleProvision.idGerencia="+idGerencia;
	}
	if(idGerente!=null&&idGerente!=""){
		data = data+"&filtrosDetalleProvision.idGerente="+idGerente;
	}
	if(idLineaVenta!=null&&idLineaVenta!=""){
		data = data+"&filtrosDetalleProvision.idLineaVenta="+idLineaVenta;
	}
	if(idEjecutivo!=null&&idEjecutivo!=""){
		data = data+"&filtrosDetalleProvision.ejecutivo.id="+idEjecutivo;
	}
	if(idLineaNegocio!=null&&idLineaNegocio!=""){
		data = data+"&filtrosDetalleProvision.idLineaNegocio="+idLineaNegocio;
	}
	if(idProducto!=null&&idProducto!=""){
		data = data+"&filtrosDetalleProvision.producto.idToProducto="+idProducto; 
	}
	
	var url ="/MidasWeb/fuerzaventa/provisiones/buscaRamosPorFiltro.action";	
	jQuery.asyncPostJSON(url,data,responseListRamos);
}

function responseListRamos(json){
	
	var arrLength = json.listaRamosProvision.length;	
	var i;
	var muestraHtml="<tr><th class='titulo' colspan='2'>Bono: Producción</th></tr>";
	jQuery("#importesXRamosXFiltros input").each(function(){
		jQuery(this).val(0);
	});
	
	for(i=0;i<arrLength;i++){
		var id = json.listaRamosProvision[i].id;
		var idRamo = json.listaRamosProvision[i].idRamo;
		var descripcion = json.listaRamosProvision[i].descripcion;
		var importe = json.listaRamosProvision[i].importe;
		var idProvision= json.listaRamosProvision[i].idProvision;
		muestraHtml += "<tr><td width='300px'>"+descripcion+"</td><td><input name='listaImporteDeRamos["+i+"].id' value='"+id+"' style='display:none'><input name='listaImporteDeRamos["+i+"].idRamo' value='"+idRamo+"' style='display:none'><input type='text' name='listaImporteDeRamos["+i+"].importe' value='"+importe+"' class='cajaTextoM2 w80'></td></tr>"
		
//		jQuery("#id_"+idRamo).val(id);
//		jQuery("#idRamo_"+idRamo).val(idRamo);		
//		jQuery("#importe_"+idRamo).val(importe);
//		
//		jQuery(".js_idProvision").val(jQuery("#provision_id").val());
	}
	jQuery("#ramosProvisionados").html(muestraHtml);
}

function guardaImportesDeRamos(){
	var idProvision = jQuery("#provision_id").val();
	var arrAjusteProvision=[{"provision.id":idProvision,"ramo.idTcRamo":1,"importe":jQuery("#incendioPuro").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":2,"importe":jQuery("#maritimoTransportes").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":3,"importe":jQuery("#diversos").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":4,"importe":jQuery("#automoviles").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":5,"importe":jQuery("#responsabilidadCivilGeneral").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":6,"importe":jQuery("#catastroficos").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":7,"importe":jQuery("#vidaGrupoColectivo").val()},
	                        {"provision.id":idProvision,"ramo.idTcRamo":8,"importe":jQuery("#vidaInd").val()}];
	var listaAjusteProvision=jQuery.convertJsonArrayToParams(arrAjusteProvision,["provision.id","ramo.idTcRamo","importe"],"listajusteProvison");
	var url="/MidasWeb/fuerzaventa/provisiones/guardar.action?"+listaAjusteProvision+"&idProvision="+idProvision;//jQuery("#provisionesForm").serialize()
	sendRequestJQ(null,url,targetWorkArea,null);
}
function autorizaImportesDeRamos(){
	var idProvision = jQuery("#provision_id").val();
	var url="/MidasWeb/fuerzaventa/provisiones/autorizar.action?"+jQuery("#provisionesForm").serialize()+"&idProvision="+idProvision;
	sendRequestJQ(null,url,targetWorkArea,null);
}
function regresarCatalogoProvisiones(){
	var url="/MidasWeb/fuerzaventa/provisiones/mostrarContenedor.action";
	sendRequestJQ(null,url,targetWorkArea,null);
}

/**cascadeo de los filtros**/

function loadEjecutivoByGerencias() {
	var idProvision = jQuery("#provision_id").val();
	var idGerencia = jQuery("#idGerencia").val();
	listadoService
			.getMapEjecutivosPorGerencia(
					idGerencia,idProvision,
					function(data) {
						if (sizeMap(data) > 0) { 
							addOptionsHeaderAndSelect("idEjecutivo", data, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("idPromotoria", null, null, '', 'Seleccione...');
//							addToolTipOnchange("gerenciaList");
						}else {
							addOptionsHeaderAndSelect("idEjecutivo", null, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("idPromotoria", null, null, '', 'Seleccione...');
						}
					});
}

function loadPromotoriaByEjecutivo() {
	var idProvision = jQuery("#provision_id").val();
	var idEjecutivo = jQuery("#idEjecutivo").val();
	listadoService
			.getMapPromotoriasPorEjecutivo(
					idEjecutivo,idProvision,
					function(data) {
						if (sizeMap(data) > 0) { 
							addOptionsHeaderAndSelect("idPromotoria", data, null, '', 'Seleccione...');
//							addToolTipOnchange("gerenciaList");
						}else {
							addOptionsHeaderAndSelect("idPromotoria", null, null, '', 'Seleccione...');
						}
					});
}

function loadProductoByLineaVenta() {
	var idProvision = jQuery("#provision_id").val();
	var idLineaVenta = jQuery("#idLineaVenta").val();
	listadoService
			.getMapProductoPorLineaVenta(
					idLineaVenta,idProvision,
					function(data) {
						if (sizeMap(data) > 0) { 
							addOptionsHeaderAndSelect("idProducto", data, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("idLineaNegocio", null, null, '', 'Seleccione...');
//							addToolTipOnchange("gerenciaList");
						}else {
							addOptionsHeaderAndSelect("idProducto", null, null, '', 'Seleccione...');
							addOptionsHeaderAndSelect("idLineaNegocio", null, null, '', 'Seleccione...');
						}
					});
}

function loadLineaNegocioByProducto() {
	var idProvision = jQuery("#provision_id").val();
	var idProducto = jQuery("#idProducto").val();
	listadoService
			.getMapLineaNegocioPorProducto(
					idProducto,idProvision,
					function(data) {
						if (sizeMap(data) > 0) { 
							addOptionsHeaderAndSelect("idLineaNegocio", data, null, '', 'Seleccione...');
//							addToolTipOnchange("gerenciaList");
						}else {
							addOptionsHeaderAndSelect("idLineaNegocio", null, null, '', 'Seleccione...');
						}
					});
}