/*
 * 
 */
function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/centrooperacion/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	mostrarModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}

//function obtenerHistorico(){
//	var url = '/MidasWeb/fuerzaventa/centrooperacion/loadHistory.action?idTipoOperacion='+10+ "&idRegistro="+dwr.util.getValue("idCentroOperacion");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}

function populateDomicilio(json){
	if(json){
		var estadoName="centroOperacion.domicilio.claveEstado";
		var paisName="centroOperacion.domicilio.clavePais";
		var ciudadName="centroOperacion.domicilio.claveCiudad";
		var coloniaName="centroOperacion.domicilio.nombreColonia";
		var calleNumeroName="centroOperacion.domicilio.calleNumero";
		var codigoPostalName="centroOperacion.domicilio.codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function guardarCentroOperacion(){
	var url = '/MidasWeb/fuerzaventa/centrooperacion/guardar.action?'+jQuery(document.centroOperacionForm).serialize();
	if(validateAll(true)){
		sendRequestJQ(null, url, targetWorkArea, null);
	}	
}
function salirCentroOperacion(){
	
	var url = "/MidasWeb/fuerzaventa/centrooperacion/mostrarContenedor.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}
		
}



