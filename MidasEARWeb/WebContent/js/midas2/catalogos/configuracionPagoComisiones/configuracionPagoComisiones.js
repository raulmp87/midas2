function listarFiltradoConfiguracionPagoComisiones(){
	pagoComicionesGrid = new dhtmlXGridObject("pagoComicionesGrid");
	var form = document.catalogoConfigComForm;
		if(form!=null){
			afianzadoraGrid.load(listarFiltradoConfigcomisionesPath+"?"+jQuery("#catalogoConfigComForm").serialize());
		}else{
			afianzadoraGrid.load(listarConfiguracionPagocomisionesPath);
		}
}

function GuardarConfigPagosComisiones() {
	var cont =0;

	var modoEjecucion=jQuery("#comboModoEjecu").val();
	var ageVa = jQuery("#agentes").val();
	if(ageVa == '' || ageVa!= ''){		
		valid = true;
	}
	jQuery('input[validar=true]').each(function(item){
	    if(jQuery(this).is(':checked')){
	    	cont++;                
	    }
	});
	
	if (valid == true) {
		if(validarRequeridos(true)&&(cont>0)){
			var path = "/MidasWeb/fuerzaventa/configuracionPagoComisiones/guardarConfigPagosComisiones.action?"
				+ jQuery("#configPagoComisionesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
			sendRequestJQ(null, path, targetWorkArea, null);
		}else{
			parent.mostrarMensajeInformativo("Existen campos obligatorios vacíos, favor de verificar la configuración de la producción seleccionada para el corte.","10");
		}
	}
	 else {
			parent.mostrarMensajeInformativo("Validar el numero capturado", "10");
		}
}

function validarRequeridos(alert,typeMessage){
	var result = true
	
	jQuery('input[type="text"]:enabled, select:enabled, textarea:enabled').each(function(){
		var input = jQuery(this);
		if (!checkForErrors(input)) {
			result = false;
		}
	});
	return result;
}


function chec(grupoChecks){
	if(jQuery("#checarTodos").attr("checked")){
		jQuery("."+grupoChecks).each(function(index){
			jQuery(this).attr("checked",true);
		});
	}else{
		jQuery("."+grupoChecks).each(function(index){
			jQuery(this).attr("checked",false);
		});
	}
}

function checkActivo(){
	if(dwr.util.getValue("configComisiones.activoBoolean")=="true"){
		jQuery("#inicioVigencia").attr("disabled",false);
		jQuery(".ui-datepicker-trigger").css("display","block");
		jQuery("#inicioVigencia").css("float","left");
		jQuery("#inicioVigencia").addClass("jQrequired");		
		
	}else{
		jQuery("#inicioVigencia").removeClass("jQrequired");
		jQuery("#inicioVigencia").removeClass("errorField");
		jQuery("#inicioVigencia").attr("disabled",true);
		jQuery("#inicioVigencia").val("");
		jQuery("#wwctrl_inicioVigencia > .ui-datepicker-trigger").css("display","none");
	}
}
function botonEjecutar(){
	
	if(jQuery("#comboModoEjecu option:selected").text()=='Automatico'){
		jQuery("#btEjecutar").css("display", "none");
		jQuery("#btMonitor").css("display", "none");		
		jQuery("#periodoEjec").attr("disabled",false);
		jQuery("#periodoEjec").attr("class", "cajaTextoM2 w150 jQrequired");
//		jQuery("#horario").attr("disabled",false);
	}else if(jQuery("#comboModoEjecu option:selected").text()=="Manual"){
		jQuery("#btEjecutar").css("display", "block");
		jQuery("#btMonitor").css("display", "block");		
		jQuery("#periodoEjec").attr("disabled",true);
		jQuery("#diaSemana").attr("disabled",true);
		jQuery("#periodoEjec").val("");
		jQuery("#diaSemana").val(0);
		jQuery("#periodoEjec").attr("class", "cajaTextoM2 w150");
//		jQuery("#horario").attr("disabled",true);
	}else{
		jQuery("#btEjecutar").css("display", "none");
		jQuery("#btMonitor").css("display", "none");		
		jQuery("#periodoEjec").attr("disabled",true);
		jQuery("#diaSemana").attr("disabled",true);
		jQuery("#periodoEjec").val("");
		//jQuery("#diaSemana").val(0);
//		jQuery("#horario").attr("disabled",true);
	}
}

function habilitarAgenteInicioFin(){
	if(dwr.util.getValue("configComisiones.todosLosAgentesBoolean")=="true"){
		
	}else{
		
	}
}

function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/afianzadora/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	parent.mostrarVentanaModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
}

function mostrarComboDias(){
	if(jQuery("#periodoEjec option:selected").html()=='SEMANAL'){
		jQuery("#diaSemana").attr("disabled",false);
		jQuery("#diaSemana").attr("class", "cajaTextoM2 w150 jQrequired");
	}else{
		jQuery("#diaSemana").attr("disabled",true);
		//jQuery("#diaSemana").val("0");
		jQuery("#diaSemana").attr("class", "cajaTextoM2 w150");
	}
}

function checarChec(elementoAchecar){
	jQuery("#"+elementoAchecar).attr("checked",true);
}

//function enableListChecs(grupoChecks){

function enableListChecs(){

	    jQuery(".js_checked").each(function(index){
			jQuery(this).attr("disabled",true);
		});
	    jQuery("#periodoEjec").attr("disabled",true);
	    jQuery("#diaSemana").attr("disabled",true);
	    jQuery("#checarTodos").attr("disabled",true);
}

function generarReporteComisiones(){	
		var location ="/MidasWeb/fuerzaventa/calculos/comisiones/generarReporte.action?calculo.id="+dwr.util.getValue("calculo.id");
		window.open(location, "ReporteComisiones");
}

function recalcularPagoComisiones(){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/recalcularComisiones.action?calculo.id="+dwr.util.getValue("calculo.id")+"&configuracion.id="+dwr.util.getValue("configuracion.id")+"&moduloOrigen="+dwr.util.getValue("moduloOrigen");
	sendRequestJQ(null, path, targetWorkArea, null);
}

function autorizarPagoComisiones(){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/autorizarPreview.action?calculo.id="+dwr.util.getValue("calculo.id")+"&moduloOrigen="+dwr.util.getValue("moduloOrigen");
	sendRequestJQ(null, path, targetWorkArea, null);
}

function verPreview(tipoAccion,idCalculo){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/verDetalle.action?moduloOrigen="+dwr.util.getValue("moduloOrigen")+"&idCalculoVerDetalle="+idCalculo+"&tipoAccion="+tipoAccion+"&configuracion.id="+dwr.util.getValue("configuracion.id");
	sendRequestJQ(null, path, targetWorkArea, null);
}

function eliminarPreview(idCalculo){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/cancelarPreview.action?calculo.id="+idCalculo;
	sendRequestJQ(null, path, targetWorkArea, null);
}

function ejecutarConfiguracion(){
	var configId = dwr.util.getValue("configComisiones.id");
	
	if(dwr.util.getValue("configComisiones.activoBoolean")=="true"){
		if(jQuery.isValid(configId)){
			var path ="/MidasWeb/fuerzaventa/calculos/comisiones/generarPreviewCalculo.action?configuracion.id="+configId+"&tipoAccion="+4+"&idRegistro="+configId+"&idTipoOperacion=60";
			sendRequestJQ(null, path, targetWorkArea, null);
		}
		else{
			parent.mostrarMensajeInformativo("Se requiere guardar la configuración para ejecutarla","10");
		}
	}else{
		parent.mostrarMensajeInformativo("Se requiere que la configuracion esta activa para poder ejecutarla.","10");
	}
	
	
}
/**
 * Funcion para mostrar la tabla comparativa una vez que ya fue autorizado el pago de comisiones.
 */
function compararPagoComisiones(){
	var path ="/MidasWeb/fuerzaventa/calculos/comisiones/verComparacionImpComis.action?calculo.id="+dwr.util.getValue("calculo.id");
	sendRequestJQ(null, path, targetWorkArea, null);
}
/**
 * Funcion para regresar a detalle de calculo
 */
function salirDetalleCalculoComisiones(){
	var path="/MidasWeb/fuerzaventa/calculos/comisiones/mostrarCalculosPendientesDePago.action";//listadoPreviewComisiones.action
	sendRequestJQ(null, path, targetWorkArea, null);
}
/**
 * Funcion para regresar al Listaco de Preview de Pago de Comisiones Procesados
 */

function regrearPreviewComisiones(){
	
	var moduloOrigen = dwr.util.getValue("moduloOrigen");
	var path="";
	if(moduloOrigen == "PREVIEW"){
	
		
		path="/MidasWeb/fuerzaventa/calculos/comisiones/listadoPreviewComisiones.action";
		sendRequestJQ(null, path, targetWorkArea, null);
	}else{
		
		
		salirDetalleCalculoComisiones();
	}
}
/**
 * Funcion para regresar a detalle de calculo de preview
 */
function salirDetalleCalculoComisionesPreview(){
	var path="/MidasWeb/fuerzaventa/calculos/comisiones/listadoPreviewComisiones.action";//listadoPreviewComisiones.action
	sendRequestJQ(null, path, targetWorkArea, null);
}

/**
 * Funcion para salir de la tabla de comparacion de detalle del calculo
 */
function salirComparacionDetalleCalculo(){
	var path=verDetallePagoComisionesPath+"?idCalculoVerDetalle="+dwr.util.getValue("calculo.id");
	sendRequestJQ(null, path, targetWorkArea, null);
}


function verMonitorDeCalculoComisiones(){
	var url = "/MidasWeb/fuerzaventa/calculos/comisiones/monitorCalculoComisiones.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function cargaMonitorComisionesGrid(){
	var url="/MidasWeb/fuerzaventa/calculos/comisiones/listarMonitorComisionesGrid.action";
	document.getElementById('monitorBonoGrid').innerHTML = '';
	monitorBonoGrid = new dhtmlXGridObject('monitorBonoGrid');
	monitorBonoGrid.load(url);
	
}

function regresarAConfigComisiones(){
	var url = "/MidasWeb/fuerzaventa/calculos/comisiones/listadoPreviewComisiones.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}


var rang = /^(\d{5}-\d{5})*(,\d{5})*(,\d{5}-\d{5})*$/;
var coma = /^(\d{5},*)*$/;
var valid = false;



function validateInfo(phoneInput) {
	if (!rang.exec(phoneInput)) {
		valid = false;
		if (!coma.exec(phoneInput)) {
			parent.mostrarMensajeInformativo("El numero ' " + phoneInput
					+ " ' capturado no es u valido", "10");

			valid = false;
		} else {
			valid = true;
		}
	} else {
		var numAgentes = phoneInput.split("-");
		var numI = numAgentes[0];
		var numF = numAgentes[1];
		if (numF <= numI) {
			parent.mostrarMensajeInformativo("El numero final  ' " + numF
					+ " ' del rango no puede ser menor al numero incial ' "
					+ numI + " ' ", "10");
			valid = false;
		} else {
			valid = true;
		}

	}
	
}

function fnValidaInput() {
	var input = jQuery("#agentes").val();
	validateInfo(input);
}