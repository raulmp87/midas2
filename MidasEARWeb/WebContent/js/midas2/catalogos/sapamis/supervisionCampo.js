var catSapAmisSupervisionCampoGrid;

function listarCatSapAmisSupervisionCampo() {
	document.getElementById("catSapAmisSupervisionCampoGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	catSapAmisSupervisionCampoGrid = new dhtmlXGridObject("catSapAmisSupervisionCampoGrid");
	catSapAmisSupervisionCampoGrid.load(listarCatSapAmisSupervisionCampoPath);
}

function mostrarCatalogoCatSapAmisSupervisionCampo() {
	sendRequestJQ(null, mostrarCatalogoCatSapAmisSupervisionCampoPath, targetWorkArea, 'listarCatSapAmisSupervisionCampo();');
}

function guardarCatSapAmisSupervisionCampo(){
	sendRequestJQ(null, guardarCatSapAmisSupervisionCampoPath+"?"+jQuery(document.catSapAmisSupervisionCampoForm).serialize(), targetWorkArea, 'listarCatSapAmisSupervisionCampo();');
}

function eliminarCatSapAmisSupervisionCampo() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarCatSapAmisSupervisionCampoPath + "?"+jQuery(document.catSapAmisSupervisionCampoForm).serialize(), targetWorkArea, 'listarCatSapAmisSupervisionCampo();');
	}
}

function verDetalleCatSapAmisSupervisionCampo (tipoAccion) {
	if(catSapAmisSupervisionCampoGrid.getSelectedId() != null){
		var idCatSapAmisSupervisionCampo = getIdFromGrid(catSapAmisSupervisionCampoGrid, 0);
		sendRequestJQ(null, verDetalleCatSapAmisSupervisionCampoPath + "?tipoAccion=" + tipoAccion + "&id=" + idCatSapAmisSupervisionCampo, targetWorkArea, null);
	}
}

function nuevoCatSapAmisSupervisionCampo (tipoAccion) {
	sendRequestJQ(null, verDetalleCatSapAmisSupervisionCampoPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
}

function listarFiltradoCatSapAmisSupervisionCampo(){
	catSapAmisSupervisionCampoGrid = new dhtmlXGridObject("catSapAmisSupervisionCampoGrid");
	var form = document.catSapAmisSupervisionCampoForm;
	if(form!=null){
		catSapAmisSupervisionCampoGrid.load(listarFiltradoCatSapAmisSupervisionCampoPath+"?"+jQuery(document.catSapAmisSupervisionCampoForm).serialize());
	}else{
		catSapAmisSupervisionCampoGrid.load(listarFiltradoCatSapAmisSupervisionCampoPath);
	}
}

function disableEnterKey(e) {
     var key;
     if(window.event)
          key = window.event.keyCode;
     else
          key = e.which;
     if(key == 13)
          return false;
     else
          return true;
}