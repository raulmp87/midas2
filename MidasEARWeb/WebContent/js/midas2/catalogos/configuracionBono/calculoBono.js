function verDetalleCalculoBono(idCalculo,tipoAccion){
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/verDetalleCalculoBonos.action?detalleCalculoBono.calculoBono.id="+idCalculo+"&tipoAccion="+tipoAccion;
	sendRequestJQ(null, path, targetWorkArea, null);
}

function verPreviewBonos(){
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/verPreviewBonos.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function autorizarPagoBonos(){
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/autorizarPagoBonos.action?calculoBono.id="+dwr.util.getValue("detalleCalculoBono.calculoBono.id")
		+"&tipoAccion="+ dwr.util.getValue("tipoAccion");
	sendRequestJQ(null, path, targetWorkArea, null);
}

function recalcularBono(){
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/recalcularBonos.action?calculoBono.id="+dwr.util.getValue("detalleCalculoBono.calculoBono.id");
	sendRequestJQ(null, path, targetWorkArea, null);
}

function eliminarCalculoBono(){
	var idCalculo = jQuery("#idCalculo").val();
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/eliminaCalculoBono.action?calculoBono.id="+idCalculo;
	sendRequestJQ(null, path, targetWorkArea,null);
}

function generarDetalleExcel(idCalculo,tipoReporte)
{
	var path ="/MidasWeb/fuerzaventa/calculos/bonos/generarDetalleExcel.action?calculoBono.id="+idCalculo+"&reporte="+tipoReporte+"&calculoBono.modoAplicacion.id="+jQuery("#modoAplicacionId").val();
	window.open(path,"DetalleCalculoPoliza");
}

function rehabilitarDetalleBono(idDetalle, idRow) {
	
	var rowCallback = function(idRow) {
	    return function(data, textStatus, jqXHR) {
	        	    	
	    	grid.cells(idRow, 5).setValue('PENDIENTE POR AUTORIZAR');
    	
	    	grid.cells(idRow, 6).setValue("");
	    	
	    };
	};
		
	blockPage();
	jQuery.ajax({
	    type: "POST",
	    url: rehabilitarDetalleBonoPath + "?detalleCalculoBono.id=" + idDetalle,
	    data: null,
	    async: true,
	    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
	    success: rowCallback(idRow),
	    complete: function(jqXHR) {
	    	unblockPage();
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
	    	unblockPage();
        }   
	});
	
	
}
