var gridAgente;
var negocioGrid;
var gridPoliza;
var rangoFactorGrid;
var gridCveAmis;

function cascadeoProductosXLineaVentas(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaLineaVenta input:checked").each(function(index){
		var clave={"clave":jQuery(this).val()};		
		arrSeleccionados.push(clave);		
		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["clave"],"listaLineaVentaSeleccionadas");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarProductosPorLineaDeVenta.action";
	
	jQuery.asyncPostJSON(url,data,responseProductos);
	
}

function responseProductos(json){
	comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","idProducto");	
}


function cascadeoRamosXProducto(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var id={"idProducto":jQuery(this).val()};		
		arrSeleccionados.push(id);		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["idProducto"],"listaProductosSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarRamosPorProducto.action";
	
	jQuery.asyncPostJSON(url,data,responseRamos);
	
}
function responseRamos(json){
	comboChecksCreate(json.listaRamos,"ajax_listaRamos","listaRamosSeleccionados","cascadeoSubramoXRamo","idRamo");	
}

function cascadeoSubramoXRamo(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaRamos input:checked").each(function(index){
		var id={"idRamo":jQuery(this).val()};		
		arrSeleccionados.push(id);		
		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["idRamo"],"listaRamosSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarSubramosPorRamoYLineaNegocioPorRamo.action";
	
	jQuery.asyncPostJSON(url,data,responseSubRamos);
}

function responseSubRamos(json){
	comboChecksCreate(json.listaSubRamos,"ajax_listaSubRamos","listaSubRamosSeleccionados",null,"idSubramo");
	comboChecksCreate(json.listaLineaNegocio,"ajax_lineaNegocio","listaLineaNegocioSeleccionados","cascadeoCoberturasXLineaNegocio","idSeccion");
}

function cascadeoCoberturasXLineaNegocio(){
	
	var arrSeleccionados = new Array();
	jQuery("#ajax_lineaNegocio input:checked").each(function(index){
		var id={"idSeccion":jQuery(this).val()};		
		arrSeleccionados.push(id);		
		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["idSeccion"],"listaLineaNegocioSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarCoberturasPorLineaNegocio.action";
	
	jQuery.asyncPostJSON(url,data,responseCoberturas);
}
function responseCoberturas(json){
	comboChecksCreate(json.listaCoberturas,"ajax_listaCoberturas","listaCoberturasSeleccionadas",null,"idCobertura");
}


//jsonList = lista devuelta en formato json
//idSelect = id del elemento select donde se mostrara la informacion
//nameObj = nombre que recibira el atributo name del select 
//onclickFunction = funcion que se ejecutara en el onclick de cada checkbox
function comboChecksCreate(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"('"+property.toString()+"');";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li><label for="'+key+'">';
					//option+='<input type="checkbox" name="'+nameObj+'['+i+'].id" id="'+nameObj+'['+i+'].id" value="'+key+'" ';
					option+='<input type="checkbox" name="'+nameObj+'['+i+'].'+property+'" id="'+nameObj+'['+i+'].'+property+'" value="'+key+'" ';
					if(check==1){
						option+= "checked='checked'";
						}
					option+='onchange="'+fn+'" />';
					option+= value + '</label>';
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}


function guardarConfiguracionBono(){
	var modoAplicacion = dwr.util.getValue("configuracionBono.modoAplicacion.id");
	var nivelSiniestralidad = jQuery( "#idNivelSiniestralidad option:selected" ).text();
	/*validacion para indicar un modo de aplicacion*/
	if(modoAplicacion==false){
		alert("Debe seleccionar un Modo de Aplicación");
		return false;
	}
	/*validacion para indicar un nivel de siniestralidad*/
	var existeSiniestralidad=0;
	if(nivelSiniestralidad!='Seleccione..'){
		if(nivelSiniestralidad =='LINEA DE NEGOCIO'){
			jQuery("#ajax_lineaNegocio input:checked").each(function(item){
				existeSiniestralidad++;
		    });
			if(existeSiniestralidad==0){
				alert("Debe seleccionar por lo menos una Línea de Negocio");
				return false;
			}
		}
		if(nivelSiniestralidad =='PRODUCTO'){
			jQuery("#ajax_listaProductos input:checked").each(function(item){ 
				existeSiniestralidad++;
		    });
			if(existeSiniestralidad==0){
				alert("Debe seleccionar por lo menos un Producto");
				return false;
			}
		}
		if(nivelSiniestralidad =='LINEA DE VENTA'){
			jQuery("#ajax_listaLineaVenta input:checked").each(function(item){
	            existeSiniestralidad++;
		    });
			if(existeSiniestralidad==0){
				alert("Debe seleccionar por lo menos una Línea de Venta");
				return false;
			}
		}
		if(nivelSiniestralidad =='SUBRAMO'){
			jQuery("#ajax_listaSubRamos input:checked").each(function(item){  
				existeSiniestralidad++;
		    });
			if(existeSiniestralidad==0){
				alert("Debe seleccionar por lo menos un Subramo");
				return false;
			}
		}
		if(nivelSiniestralidad =='RAMO'){
			jQuery("#ajax_listaRamos input:checked").each(function(item){     
				existeSiniestralidad++;
		    });
			if(existeSiniestralidad==0){
				alert("Debe seleccionar por lo menos un Ramo");
				return false;
			}
		}
	}
	
//	if(validarExisteRangoConfigurado()==true){
	jQuery("#txtFechaInicioVigencia1").val(jQuery("#txtInicioVigencia2").val());
	jQuery("#txtFechaFinVigencia1").val(jQuery("#txtFechaFinVigencia2").val());
	
	if(compara_fecha(jQuery("#txtFechaInicioVigencia1").val(),jQuery("#txtFechaFinVigencia1").val())){
		parent.mostrarMensajeInformativo("La 'fecha Fin de Vigencia' no puede ser menor a la fecha 'Inicio de Vigencia'","10");
		return false;
	}
	
		if(compara_fecha(jQuery("#txtFechaPeriodoInicial").val(),jQuery("#txtFechaPeriodoFinal").val())) { 
		parent.mostrarMensajeInformativo("Fecha Periodo Inicial no debe ser mayor a Fecha Periodo Final","10");
		return false; 
	}
	
	
	var tipoBeneficiarioString;
	var countAgtBenefEspecificos =jQuery("#listaBeneficiariosAgentes").find('li').length;
	var countPromoBenefEspecificas =jQuery("#listaPromotoriasBeneficiarias").find('li').length;
	var countPolizasProdEspecificas =jQuery("#listaPolizasProduccion").find('li').length;
	var countAgtProdEspecificas =jQuery("#listaProduccionAgentes").find('li').length;	
	
	/*validaciones para obligar la captura de almenos un tipo de beneficiario*/
	if(jQuery("#benefPromotoria").is(":checked")==false){
		jQuery(".js_promTodos").removeAttr("checked");
		jQuery("#listaPromotoriasBeneficiarias").html("");
		jQuery("#btnBenefProm").css("display","block");
	}
	if(jQuery("#benefAgente").is(":checked")==false){
		jQuery(".js_agtTodos").removeAttr("checked");
		jQuery("#listaBeneficiariosAgentes").html("");
		jQuery("#btnBenefAgt").css("display","block");
	}
	
	if(jQuery("#benefPromotoria").is(":checked") && !jQuery("#benefAgente").is(":checked")){
		tipoBeneficiarioString = "Promotoria";		
		if(jQuery(".js_promTodos").is(":checked")==false && countPromoBenefEspecificas==0){
			parent.mostrarMensajeInformativo("Marque 'Todos' o seleccione minimo una promotoria benficiaria","10");
			return false;
		}
	}else if(!jQuery("#benefPromotoria").is(":checked") && jQuery("#benefAgente").is(":checked")){
		tipoBeneficiarioString = "Agente";
		if(jQuery(".js_agtTodos").is(":checked")==false &&  countAgtBenefEspecificos==0){
			parent.mostrarMensajeInformativo("Marque 'Todos' o seleccione minimo un agente benficiario","10");
			return false;
		}
	}else if(jQuery("#benefPromotoria").is(":checked") && jQuery("#benefAgente").is(":checked")){
		tipoBeneficiarioString = "Ambos";
		if(jQuery(".js_promTodos").is(":checked")==false &&  countPromoBenefEspecificas==0){
			parent.mostrarMensajeInformativo("Marque 'Todos' o seleccione minimo una promotoria benficiaria","10");
			return false;
		}
		if(jQuery(".js_agtTodos").is(":checked")==false &&  countAgtBenefEspecificos==0){
			parent.mostrarMensajeInformativo("Marque 'Todos' o seleccione minimo un agente benficiario","10");
			return false;
		}
	}else{
		parent.mostrarMensajeInformativo("Es requerido al menos un tipo de beneficiario","10");
		return false;
	}
	
	if(validateAll(true)){
			var listadosEspecificos="";
			/****extrae los agentes que se agregaron en la configuracion de bonos***/
			if(countAgtProdEspecificas!=0){
				var arrAgente= new Array();
				jQuery("#listaProduccionAgentes li").each(function(index){
					var idAgente = {"id":jQuery(this).attr("id")};
					arrAgente.push(idAgente);              
			    });
				listadosEspecificos+="&"+jQuery.convertJsonArrayToParams(arrAgente,["id"],"lista_agentes");
			}
			
			/****extrae las polizas que se agregaron en la configuracion de bonos***/
			if(countPolizasProdEspecificas!=0){
				var arrPolizas= new Array();
				jQuery("#listaPolizasProduccion li").each(function(index){
					var idPoliza = {"idCotizacion":jQuery(this).attr("id")};
					arrPolizas.push(idPoliza);
			    });
				
				listadosEspecificos+="&"+jQuery.convertJsonArrayToParams(arrPolizas,["idCotizacion"],"listaConfigPolizas");
			}
			
			/****extrae las promotorias beneficiarias que se agregaron en la configuracion de bonos***/
			if(countPromoBenefEspecificas!=0 && jQuery("#benefPromotoria").is(":checked")==true){
				var arrPromotorias= new Array();
				jQuery("#listaPromotoriasBeneficiarias li").each(function(index){
					var idPromotoria = {"id":jQuery(this).attr("id")};
					arrPromotorias.push(idPromotoria);              
			    });
				
				listadosEspecificos+="&"+jQuery.convertJsonArrayToParams(arrPromotorias,["id"],"lista_AplicaPromotorias");
			}
			
			/****extrae la lista de los agentes beneficiarios****/			
			if(countAgtBenefEspecificos!=0 && jQuery("#benefAgente").is(":checked")==true){		
				var arrAgentesBenef= new Array();
				jQuery("#listaBeneficiariosAgentes li").each(function(index){
					var idAgenteBenef = {"id":jQuery(this).attr("id")};
					arrAgentesBenef.push(idAgenteBenef);              
			    });
				
				listadosEspecificos+="&"+jQuery.convertJsonArrayToParams(arrAgentesBenef,["id"],"lista_AplicaAgentes");
			}
			
			var path = "/MidasWeb/fuerzaventa/configuracionBono/guardarConfiguracionDeBonos.action?"			
					+"&clonar=0"
					+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion")
					+listadosEspecificos
					+"&tipoBeneficiarioString="+tipoBeneficiarioString;
					
					sendRequestJQ(document.configuracionBonoForm, path, targetWorkArea, null);				
			
		}else{
			parent.mostrarMensajeInformativo("Existen algunos errores en la captura","10");
		}
//	}
}

function mostrarModalPromotoria(){
	var url="/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action?tipoAccion=consulta&idField=idPersonaResponsable";
	sendRequestWindow(null, url,obtenerVentanaPromotoria);
}
var ventanaPromotoria=null;
function obtenerVentanaPromotoria(){
	var wins = obtenerContenedorVentanas();
	ventanaPromotoria= wins.createWindow("promotoriaModal", 400, 320, 900, 450);
	ventanaPromotoria.center();
	ventanaPromotoria.setModal(true);
	ventanaPromotoria.setText("Consulta de Promotoria");
	ventanaPromotoria.button("park").hide();
	ventanaPromotoria.button("minmax1").hide();
	return ventanaPromotoria;
}

function verConfiguracionBono() {	
	sendRequestJQ(null, verDetalleConfiguracionBonosPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id'),'contenido_configuracionBonos','limpiarDivsConfiguracionBonos();');
	
}

function verExcepciones() {
	sendRequestJQ(null, verExcepcionesPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id'),'contenido_excepciones','limpiarDivsExcepciones();');
}

function incializarTabs(){
	var idconfigBono=jQuery("#configuracionBono\\.id").val();
	contenedorTab=window["configuracionBonoTabBar"];
	//Nombre de todos los tabs
	var tabs=["configuracionBonos","excepciones"];
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
	if(!jQuery.isValid(idconfigBono)){
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
	for(var i=0;i<tabs.length;i++){
		var tabName=tabs[i];
		var tab=contenedorTab[tabName];
		jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
			if(jQuery.isValid(idconfigBono)){
				if(jQuery("#tipoAccion").val()!=2){
					event.stopPropagation();
					parent.mostrarMensajeConfirm("Está a punto de abandonar la sección, ¿Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}else{
					
				}
			}else{
				parent.mostrarMensajeInformativo("Debe guardar datos de la configuración","10");
			}
		});
	}
}
function irDetalleBonos(tipoAccion) {
//	if(tipoAccion==null || tipoAccion==''){
//		tipoAccion=parent.dwr.util.getValue("tipoAccion");
//	}
	tipoAccion = 1;
	var path= "/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action";
	sendRequestJQ(null, path + "?tipoAccion=" + tipoAccion, targetWorkArea, 'dhx_init_tabbars();');	
}

function limpiarDivsConfiguracionBonos() {
	limpiarDiv('contenido_excepciones');
}
function limpiarDivsExcepciones() {
	limpiarDiv('contenido_configuracionBonos');
}

function cargarGridCveAmis(){
	var idConfig = jQuery("#configuracionBono_id").val();
	document.getElementById('gridCveAmis').innerHTML = '';
	gridCveAmis = new dhtmlXGridObject('gridCveAmis');	
	gridCveAmis.attachEvent("onCellChanged", function(rId,cInd,nValue){
		if(cInd >=1 && cInd <=2){
				if(nValue!='&nbsp;'){
					var num = Number(nValue);
					if(num.length>5 || validateNum5Enteros(num)==false){
					parent.mostrarMensajeInformativo("El valor ingresado no puede ser mayor a 5 digitos","10");
					gridCveAmis.cellById(rId,cInd).setValue("");   	
				}
			} 
		}
	});
	if(idConfig!=null){
		var url ="/MidasWeb/fuerzaventa/configuracionBono/listarExclucionCveAmis.action?configuracionBono.id="+idConfig;
		gridCveAmis.load(url);
		
	}else{
		var url="/MidasWeb/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/claveAmisGrid.jsp";
		gridCveAmis.load(url);
	}
}

function cargarGridAgente(){
	var idConfig = jQuery("#configuracionBono_id").val();
	document.getElementById('gridAgente').innerHTML = '';
	gridAgente = new dhtmlXGridObject('gridAgente');	
	gridAgente.attachEvent("onCheck", activaInactivaSegunChecks);
	
	if(idConfig!=null){
		var url ="/MidasWeb/fuerzaventa/configuracionBono/listarExcepcionAgentes.action?configuracionBono.id="+idConfig;
		gridAgente.load(url);
	}else{
		var url="/MidasWeb/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/agenteGrid.jsp";
		gridAgente.load(url);
	}
}

function mostrarModalAgente(){
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgente&closeModal=No";
	sendRequestWindow(null, url,obtenerVentanaAgente);
}
function obtenerVentanaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("agenteModal", 10, 320, 900, 450);
	ventanaResponsable.centerOnScreen();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Agregar Agente");
	return ventanaResponsable;
}

function cargarGridNegocio(){
	var idConfig = jQuery("#configuracionBono_id").val();
	document.getElementById('negocioGrid').innerHTML = '';
	negocioGrid = new dhtmlXGridObject('negocioGrid');	
	
	if(idConfig!=null){
		var url ="/MidasWeb/fuerzaventa/configuracionBono/listarExcepcionNegocios.action?configuracionBono.id="+idConfig;
		negocioGrid.load(url);
	}else{
		var url="/MidasWeb/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/negocioGrid.jsp";
		negocioGrid.load(url);
	}
}

function mostrarModalNegocio(){
	var url="/MidasWeb/fuerzaventa/configuracionBono/agregarNegocio.action";//?tipoAccion=consulta&idField=txtIdRepresentanteFiscal
	sendRequestWindow(null, url,obtenerVentanaNegocio);
}
function obtenerVentanaNegocio(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("closeModalNegocioAsoc", 10, 320, 600, 450);
	ventanaResponsable.centerOnScreen();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Agregar Negocio");
	return ventanaResponsable;
}

function limpiarChec(grupoChecks){
	jQuery("."+grupoChecks).each(function(index){
		jQuery(this).attr("checked",false);
	});
}

function llenarGridAgengte(idAgente){
	
	var data = {"agente.id":idAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
	
	jQuery.asyncPostJSON(url,data,responseAgente);
	
}
function responseAgente(json){
	var Id_Row =gridAgente.getRowsNum()+1;
	var idAgente = json.agente.id; 
	var grid="gridAgente";
	gridAgente.addRow(Id_Row,[,idAgente,json.agente.persona.nombreCompleto,"","../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepAgente("+Id_Row+")^_self", ]);
//	alert("se Agrego el agente: "+json.agente.persona.nombreCompleto);
}
function delete_rowExcepAgente(Id_Row) {
	gridAgente.deleteRow(Id_Row);
	activaInactivaSegunChecks();
}

function llenarListaNegDes(){
	
	var iDnegocio = jQuery("#idNegocio").val();
	var claveEstatus = jQuery("#claveEstatus").val();
	var data = {"filtroNegocio.idNegocio":iDnegocio, "filtroNegocio.claveEstatus": claveEstatus};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/llenarListaNegDesasoc.action";
	
	jQuery.asyncPostJSON(url,data,responseListaNegDes);
}


function responseListaNegDes(json){
	var select= jQuery("#ajax_listaNegDesasoc");
	select.html("");
	for(var i=0;i<json.nuevaListaNeg.length;i++){
		var jsonList = json.nuevaListaNeg[i];
		var key=eval('jsonList.idNegocio');
		var value=eval('jsonList.descripcionNegocio');
		if(key!=null){
			var option='<li id="'+key+'"><label for="'+key+'">';
//			option+='<input type="text" name="nombre" id="id" value="'+key+'" />';
			option+= value + '</label>';
			option+='</li>';
			    select.append(option);		    	
		}
	}
}

function llenarGridNegocio(){
	 var idsRecuperados = "";
	 jQuery("#NegocioAsociado li").each(function(index){
		 idsRecuperados+=jQuery(this).attr("id")+","; 		
	 });
	 if(idsRecuperados==""){
		 alert("Debe seleccionar un negocio");
	 }else{
		 var arregloIds=new Array();
		 var mySplitResult = idsRecuperados.split(",");
		 var tamanioSplit = mySplitResult.length-1;
		 for(var i=0;  i!=tamanioSplit; i++){
			 var id = {"idNegocio":mySplitResult[i]};
			 arregloIds.push(id);
		 }
		 var data=jQuery.convertJsonArrayToParams(arregloIds,["idNegocio"],"obtenerNegocios");
		 var url ="/MidasWeb/fuerzaventa/configuracionBono/regresarObjetoNegocios.action";
		 jQuery.asyncPostJSON(url,data,responseNegocio);
	}
}
function responseNegocio(json){
	var grid="negocioGrid";
	var tamanio = json.nuevaListaNeg.length;
	for(var i =0; i<tamanio; i++){
		var Id_Row =negocioGrid.getRowsNum()+1;
		negocioGrid.addRow(Id_Row,[json.nuevaListaNeg[i].idNegocio,json.nuevaListaNeg[i].descripcionNegocio, , ,json.nuevaListaNeg[i].pcteDescuento ,"../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepNegocio("+Id_Row+")^_self",  ]);
		closeDhtmlxWindows('closeModalNegocioAsoc');
	}
}

jQuery("#chkActivo").click(function(){
	activaFechasSiEsActivo();
});

function activaFechasSiEsActivo(){
	if(jQuery("#chkActivo:checked").val()=="true"){	
		jQuery("#js_inicioVigencia1").css("display","none");
		jQuery("#js_finVigencia1").css("display","none");
		jQuery("#txtFechaInicioVigencia1").attr("display","none");
		jQuery("#txtFechaFinVigencia1").attr("display","none");
		
		jQuery("#js_inicioVigencia2").css("display","block");
		jQuery("#js_finVigencia2").css("display","block");
		jQuery("#txtInicioVigencia2").attr("display","block");
		jQuery("#txtFechaFinVigencia2").attr("display","block");
				
		jQuery("#txtInicioVigencia2").val(jQuery("#txtFechaInicioVigencia1").val());
		jQuery("#txtFechaFinVigencia2").val(jQuery("#txtFechaFinVigencia1").val());
		
	}else{
		jQuery("#js_inicioVigencia1").css("display","block");
		jQuery("#js_finVigencia1").css("display","block");
		jQuery("#txtFechaInicioVigencia1").attr("display","block");
		jQuery("#txtFechaFinVigencia1").attr("display","block");
		jQuery("#txtFechaInicioVigencia1").val(jQuery("#txtInicioVigencia2").val());
		jQuery("#txtFechaFinVigencia1").val(jQuery("#txtFechaFinVigencia2").val());
		
		jQuery("#js_inicioVigencia2").css("display","none");
		jQuery("#js_finVigencia2").css("display","none");
		jQuery("#txtInicioVigencia2").attr("display","none");
		jQuery("#txtFechaFinVigencia2").attr("display","none");
		
	}
}

jQuery("#js_ajusteOficina").click(function(){
	activaCamposSiEsAjusteOficina();
});
function activaCamposSiEsAjusteOficina(){
	if(jQuery("#js_ajusteOficina:checked").val()=="true"){
		jQuery("#sel_idBonoAjustar").removeAttr("disabled");
		jQuery("#sel_idBonoAjustar").removeClass("jQrequired");
//		jQuery("#sel_periodoAjuste").removeAttr("disabled");
	}else{
		jQuery("#sel_idBonoAjustar").attr("disabled","readOnly");
		jQuery("#sel_idBonoAjustar").addClass("jQrequired");
//		jQuery("#sel_periodoAjuste").attr("disabled","readOnly");
	}
}
jQuery("#js_global").click(function(){
	activaSiEsGlobal();
});

function activaSiEsGlobal(){
	if(jQuery("#js_global:checked").val()=="true"){	
		jQuery("#js_habilitaPeriodoInicial1").css("display","none");
		jQuery("#js_habilitaPeriodoFinal1").css("display","none");
		
		jQuery("#js_habilitaPeriodoInicial2").css("display","block");
		jQuery("#js_habilitaPeriodoFinal2").css("display","block");
		
		jQuery("#txtFechaPeriodoInicial2").attr("readOnly","readOnly");
		jQuery("#txtFechaPeriodoFinal2").attr("readOnly","readOnly");
		
		jQuery("#txtFechaPeriodoInicial").val("");
		jQuery("#txtFechaPeriodoFinal").val("");
		
	}else{
		jQuery("#js_habilitaPeriodoInicial1").css("display","block");
		jQuery("#js_habilitaPeriodoFinal1").css("display","block");
		
		jQuery("#js_habilitaPeriodoInicial2").css("display","none");
		jQuery("#js_habilitaPeriodoFinal2").css("display","none");
		
		jQuery("#txtFechaPeriodoInicial2").removeAttr("readOnly");
		jQuery("#txtFechaPeriodoFinal2").removeAttr("readOnly");
	}
}

function checkSegunElTipoModoAplicacion(){
	if(jQuery("#valorPorcentajeImporteDirecto").val()!=""){
		jQuery("#radioDirecto1").attr("checked","checked");
		jQuery("#btn_agregaFactores").css("display","none");
	}else{
		jQuery("#radioConfigRangos1").attr("checked","checked");
		jQuery("#valorPorcentajeImporteDirecto").attr("readOnly","readOnly");
	}
}
jQuery("#radioDirecto1").click(function(){
	jQuery("#valorPorcentajeImporteDirecto").removeAttr("readOnly");
	jQuery("#btn_agregaFactores").css("display","none");
});
jQuery("#radioConfigRangos1").click(function(){
	jQuery("#valorPorcentajeImporteDirecto").attr("readOnly","readOnly");
	jQuery("#valorPorcentajeImporteDirecto").val("");
	jQuery("#btn_agregaFactores").css("display","block");
});
/***SE AGREGA ESTE CAMBIO PARA TRUNCAR EL CAMPO DE MODO DE APLICACION SI ES
 * POR PORCENTAJE QUE TRUNQUE EL CAMPO A 4 ENTEROS Y 2 DECIMALES */ 
function modifCampo(obj){
	if (jQuery("#"+obj.id).next().text()=="Porcentaje"){
		jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","9");
//		jQuery("#valorPorcentajeImporteDirecto").removeAttr("onblur");
//		jQuery("#valorPorcentajeImporteDirecto").attr("onblur","validarEstandarEnModApl(this);");
		if(validateNum4Enteros(jQuery("#valorPorcentajeImporteDirecto").val())==true){
			jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 4 enteros y 2 decimales","10");
			jQuery("#valorPorcentajeImporteDirecto").val("");
		}
		var radioConfigRangos = jQuery("#radioConfigRangos1").is(':checked');
		if(radioConfigRangos==true){
			var idConfig = jQuery("#configuracionBono_id").val();
			if(idConfig!=''){
				dwr.engine.beginBatch();
				 validacionService.existeRangoConfigurado(idConfig, function(data){
						if(data==true){
							parent.mostrarMensajeInformativo("Favor de verificar la Configuración de los Factores campo Bonificación","20");
						}
					});
				 dwr.engine.endBatch({async:false});
			}
		}
	}else{
		jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","21");
//		jQuery("#valorPorcentajeImporteDirecto").removeAttr("onblur");
//		jQuery("#valorPorcentajeImporteDirecto").attr("onblur","validarEstandarEnCampos(this);");
		if(validateNumber(jQuery("#valorPorcentajeImporteDirecto").val())==true){
			 jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
			jQuery("#valorPorcentajeImporteDirecto").val("");
		}
	}
}
//jQuery("#modoAplicacion_id").click(function(){
//	alert("hola");
//});
var ventanaRangoFactores;
function mostrarModalRangoFactores(){

	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	
	if(configuracionBono_id != '')
	{		
		var url="/MidasWeb/fuerzaventa/configuracionBono/mostrarRangoFactores.action?configuracionBono.id="+configuracionBono_id+"&tipoAccion=" + jQuery("#tipoAccion").val();
		sendRequestWindow(null, url, obtenerVentanaRangoFactores);
		
	}else
	{
		mostrarMensajeInformativo("Debe guardar la configuraci\u00F3n de bono actual, antes de continuar con la configuraci\u00F3n de Rangos","20");		
	}
	
	
	
}
function obtenerVentanaRangoFactores(){
	var wins = obtenerContenedorVentanas();
	ventanaRangoFactores= wins.createWindow("rangoFactoresModal", 400, 320, 930, 450);
	ventanaRangoFactores.center();
	ventanaRangoFactores.setModal(true);
	ventanaRangoFactores.setText("Rangos");
	ventanaRangoFactores.button("park").hide();
	ventanaRangoFactores.button("minmax1").hide();
	ventanaRangoFactores.attachEvent("onClose", function(win){
		//existeCrecimiento();
		requeridoCrecimientoYSupMeta();
		return true;
    });
	return ventanaRangoFactores;
}

function guardarAgenteExcepcion(){
	var listaIds = gridAgente.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	var nulosFlag="correcto";
	
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();
		var aplicaDescuento="";
		var idAgente="";
		var monto="";
		var datos ="";
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				aplicaDescuento = gridAgente.cellById(valor,0).getValue();
				idAgente = gridAgente.cellById(valor,1).getValue();
				monto = gridAgente.cellById(valor,3).getValue();
				
				if(aplicaDescuento == 0 && monto==''){
					nulosFlag="error";
					break;
				}
				
				datos={"id":gridAgente.cellById(valor,5).getValue(),"agente.id":idAgente,"valorMontoPcteAgente":monto,"noAplicaDescuentoExcepcion":aplicaDescuento};
				arr.push(datos);			
		}
		if(nulosFlag != "error"){
			
			strParam=jQuery.convertJsonArrayToParams(arr,["id","agente.id","valorMontoPcteAgente","noAplicaDescuentoExcepcion"],"listaExcepxionesAgentes");				
			var url="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcepAgente.action";
			var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
			jQuery.asyncPostJSON(url,data,responseGuardadoExcepAgente);
			
		}
							
		}else{
			var url="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcepAgente.action";
			var data = "configuracionBono.id="+configuracionBono_id;
			jQuery.asyncPostJSON(url,data,responseGuardadoExcepAgente);	
		}
	
	return nulosFlag;
}
function responseGuardadoExcepAgente(json){
	//alert(json);
}


function guardarNegocioExcepcion(){
	
	var url="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcepNegocio.action";
	var listaIds = negocioGrid.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	var nulosFlag="correcto";
	
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();		
		var datos ="";
		
		
		
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				negocio = negocioGrid.cellById(valor,0).getValue();
				montoPorcentajeAgente = negocioGrid.cellById(valor,2).getValue();
				montoPorcentajePromotoria= negocioGrid.cellById(valor,3).getValue();
				
				if(montoPorcentajeAgente == '' && montoPorcentajePromotoria == ''){
					nulosFlag="error";
					break;
				}
				
				descuentoMaximo= negocioGrid.cellById(valor,4).getValue();
				datos={"id":negocioGrid.cellById(valor,6).getValue(),"negocio.idToNegocio":negocio,"valorMontoPcteAgente":montoPorcentajeAgente,"valorMontoPctePromotoria":montoPorcentajePromotoria,"descuentoMaximo":descuentoMaximo};
				arr.push(datos);
				
				
		}
			
		if(nulosFlag != "error"){
			strParam=jQuery.convertJsonArrayToParams(arr,["id","negocio.idToNegocio","valorMontoPcteAgente","valorMontoPctePromotoria","descuentoMaximo"],"listaExcepxionesNegocios");				
			var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
			jQuery.asyncPostJSON(url,data,responseGuardadoExcepNegocio);
		}			
		}else{
			var data = "configuracionBono.id="+configuracionBono_id;
			jQuery.asyncPostJSON(url,data,responseGuardadoExcepNegocio);	
		}
	
	return nulosFlag;
}
function responseGuardadoExcepNegocio(json){
	//alert(json);
}
function delete_rowExcepNegocio(Id_Row) {
	negocioGrid.deleteRow(Id_Row);
}

function agregarRangoFactor(){
	var Id_Row =rangoFactorGrid.getRowsNum()+1;
	rangoFactorGrid.attachEvent("onCellChanged",validateNum);
	rangoFactorGrid.attachEvent("onKeyPress",function(code,cFlag,sFlag){
		if (code!=188){
			return true;
	    }else{
	    	return false;
	    }
	});	
	rangoFactorGrid.addRow(Id_Row,[Id_Row,,,,,,,,,,,,"../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowRangoFactor("+Id_Row+")^_self" ]);
	
}
	function presionTecla(code,cFlag,sFlag){
//		alert(code);
		    if (code==188){
				window.event.keyCode = 188;
				window.event.returnValue=false;
//		      alert('No se puede ingresar espacios en blanco para un nombre de usuario '+code);
		    }
		    else    if (e) { alert(e.which);
		      var tecla=e.which;
//			  alert("2 - "+tecla)
		      if(tecla==44)  {
		        e.preventDefault();
//		        alert('No se puede ingresar espacios en blanco para un nombre de usuario');
		      }
		    }
	}
	 function validateNum (Id_Row, ind, value){
		 var esPorcentaje=jQuery("input:radio[name=configuracionBono.modoAplicacion.id]:checked").next("label").text();
	        if (ind <= 11 && !rangoFactorGrid._ignore_next){
	        	if (ind == 3 || ind == 4 && !rangoFactorGrid._ignore_next){
	        		if(!validateNumberNeg(value)){
		        		rangoFactorGrid._ignore_next = true;
		        		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
		        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
		        	}
	        	}else
        		if(ind == 11 && esPorcentaje=='Porcentaje' && !rangoFactorGrid._ignore_next){
//	        		alert(ind+" "+esPorcentaje+" "+validateNum4Enteros(value));
	        		if(validateNum4Enteros(value)==false){
		        		rangoFactorGrid._ignore_next = true;
		        		parent.mostrarMensajeInformativo("El valor ingresado debe de ser máximo 4 enteros y 2 decimales, campo bonificación","10");
		        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
	        		}
	        	}else
	        	if(!validateNumber(value)){
	        		rangoFactorGrid._ignore_next = true;
	        		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
	        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
	        	}
	        	rangoFactorGrid._ignore_next = false;
	        }
		}
 function validateNumberNeg(numStr){
		var expreg = /^-?(\d{1,14})(\.\d{1,2})?$/;//^-?(\d{0,14})(\.\d{1,2})?$/;//^(\d{0,14})(\.\d{1,2})?$
		if(numStr!='' && numStr!='&nbsp;'){	
			numStr = numStr.toString().replace(/,/g,'');
			if(numStr!=''){
					return expreg.test(numStr);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
function validateNumber(numStr){
	var expreg = /^(\d{0,14})(\.\d{1,2})?$/;//^(\d{0,14})(\.\d{1,2})?$
	if(numStr!='' && numStr!='&nbsp;'){
	    numStr=numStr.toString().replace(/,/g,'');
		if(numStr!=''){
			return expreg.test(numStr);			
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function validarEstandarEnCampos(obj){
	if(obj.id=='pctSiniestralidadMaxima'){
		if(validateNumberNeg(obj.value)==true){
			jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
			jQuery("#"+obj.id).val("");
		}		
	}else{
		if(validateNumber(obj.value)==true){
			 jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
			jQuery("#"+obj.id).val("");
		}				
	}
} 
function validarEstandarEnModApl(obj){
	if(validateNum4Enteros(obj.value)==true){
		 jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
	}else{
		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 4 enteros y 2 decimales","10");
		jQuery("#"+obj.id).val("");
	}				
} 

function delete_rowRangoFactor(Id_Row) {
	rangoFactorGrid.deleteRow(Id_Row);
	numerarRowsRangoFactor();
}

function numerarRowsRangoFactor() {
	var Id_Row = "";
	var listaIds = rangoFactorGrid.getAllRowIds(",");
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		for(i=0;i<idArray.length;i++){
			var valor = idArray[i];
			Id_Row = i + 1;
			rangoFactorGrid.cells(valor,0).setValue(Id_Row);
			rangoFactorGrid.cells(valor,12).setValue("../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowRangoFactor("+ Id_Row +")^_self");
			rangoFactorGrid.changeRowId(valor,Id_Row);
		}
	}
}
function guardarRangosFactores(){
	var listaIds = rangoFactorGrid.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();		
		var datos ="";
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				prima_de = rangoFactorGrid.cellById(valor,1).getValue();
				prima_Hasta = rangoFactorGrid.cellById(valor,2).getValue();
				siniest_inicial = rangoFactorGrid.cellById(valor,3).getValue();
				siniest_final= rangoFactorGrid.cellById(valor,4).getValue();
				crecimiento_inicial= rangoFactorGrid.cellById(valor,5).getValue();
				crecimiento_final= rangoFactorGrid.cellById(valor,6).getValue();
				supera_inicial= rangoFactorGrid.cellById(valor,7).getValue();
				supera_final= rangoFactorGrid.cellById(valor,8).getValue();
				poliza_de = rangoFactorGrid.cellById(valor,9).getValue();
				poliza_Hasta = rangoFactorGrid.cellById(valor,10).getValue();
				bonificacion = rangoFactorGrid.cellById(valor,11).getValue();
				idRecord = rangoFactorGrid.cellById(valor,13).getValue();
				datos={"importePrimaInicial":prima_de,
						"importePrimaFinal":prima_Hasta,
						"pctSiniestralidadInicial":siniest_inicial,
						"pctSiniestralidadFinal":siniest_final,
						"pctCrecimientoInicial":crecimiento_inicial,
						"pctCrecimientoFinal":crecimiento_final,
						"pctSupMetaInicial":supera_inicial,
						"pctSupMetaFinal":supera_final,
						"numPolizasEmitidasInicial":poliza_de,
						"numPolizasEmitidasFinal":poliza_Hasta,
						"valorAplicacion":bonificacion,
						"id":idRecord
						};
				arr.push(datos);			
		}
			strParam=jQuery.convertJsonArrayToParams(arr,["importePrimaInicial","importePrimaFinal","pctSiniestralidadInicial","pctSiniestralidadFinal"
                                              ,"pctCrecimientoInicial","pctCrecimientoFinal","pctSupMetaInicial","pctSupMetaFinal"
                                              ,"numPolizasEmitidasInicial","numPolizasEmitidasFinal","valorAplicacion","id"],"listRangoFacotres");				
			var url="/MidasWeb/fuerzaventa/configuracionBono/saveListRangoFactores.action";
			var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
			jQuery.asyncPostJSON(url,data,responseListRangoFactores);				
		}else{
			var url="/MidasWeb/fuerzaventa/configuracionBono/saveListRangoFactores.action";
			var data = "configuracionBono.id="+configuracionBono_id;
			jQuery.asyncPostJSON(url,data,responseListRangoFactores);	
		}
}
function responseListRangoFactores(json){
	parent.mostrarMensajeInformativo(json.mensaje,json.tipoMensaje);
	requeridoCrecimientoYSupMeta();
}


/**
 *saveConfigBonoExcluTipoBono 
 */

 function saveConfigBonoExcluTipoBono (){	
	var IdConfigBono = jQuery("#configuracionBono_id").val();
	
	var arregloIds = new Array();
	jQuery('#tipoBono input[validar=true]').each(function(item){
	    if(jQuery(this).is(':checked')){
	    	 var id = {"id":jQuery(this).val()};
	    	 arregloIds.push(id);              
	    }
	});
	var listaIds=jQuery.convertJsonArrayToParams(arregloIds,["id"],"obtenerListCatalogoTipoBono");
	var data="configuracionBono.id="+IdConfigBono+"&"+listaIds; 
	var url ="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcluTipoBono.action";
 
	jQuery.asyncPostJSON(url,data,responseConfigBonoExcluTipoBono);
}
 
function responseConfigBonoExcluTipoBono(json){
//	comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","idProducto");	

}

//*****
function saveConfigBonoExcluTipoCedula(){	
	var IdConfigBono = jQuery("#configuracionBono_id").val();
	
	var arregloIds = new Array();
	jQuery('#tipoCedula input[validar=true]').each(function(item){
	    if(jQuery(this).is(':checked')){
	    	 var id = {"id":jQuery(this).val()};
	    	 arregloIds.push(id); 
//	    	 alert(jQuery(this).val())
	    }
	});
	var listaIds=jQuery.convertJsonArrayToParams(arregloIds,["id"],"obtenerListCatalogoTipoCedula");
	var data="configuracionBono.id="+IdConfigBono+"&"+listaIds; 
	var url ="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcluTipoCedula.action";
 
	jQuery.asyncPostJSON(url,data,responseExcluTipoCedula);
}	
 
function responseExcluTipoCedula(json){
//	comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","idProducto");	
	
}

var ventanaBusquedaAgente;
function pantallaModalBusquedaAgente(){
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgenteBusqueda&closeModal=No";
	sendRequestWindow(null, url,obtenerVentanaBusquedaAgente);
	
}
function pantallaModalBusquedaAgenteBeneficiario(){

	var url="/MidasWeb/fuerzaventa/configuracionBono/abrirModalGirdAgente.action?&idField=txtIdAgenteBeneficiario";
	sendRequestWindow(null, url,obtenerVentanaBusquedaAgente);
	
}
function obtenerVentanaBusquedaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaBusquedaAgente= wins.createWindow("agenteModal", 10, 320, 900, 450);
	ventanaBusquedaAgente.centerOnScreen();
	ventanaBusquedaAgente.setModal(true);
	ventanaBusquedaAgente.setText("Buscar Agente");
	return ventanaBusquedaAgente;
}
function addAgenteProduccion(){
	var busIdAgente = jQuery("#txtIdAgenteBusqueda").val();
	var data = {"agente.id":busIdAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";

	jQuery.asyncPostJSON(url,data,responseAgenteProduccion);
}
function responseAgenteProduccion(json){
	
	var idAgente = json.agente.id;
	var claveAgente = json.agente.idAgente;
	var agente = json.agente.persona.nombreCompleto;
	var indice = jQuery("#listaProduccionAgentes li").length;
	if(idAgente!=null){
		var indicename = indice;
		var option='<li id="'+idAgente+'"><img src="../img/icons/ico_eliminar.gif" onclick="delete_agente('+idAgente+');" style="cursor:pointer;"/>';
		option+='&nbsp;&nbsp;<label for="'+idAgente+'" />'+claveAgente+" - "+agente+'</label> <s:hidden name="lista_Agentes['+indicename+'].agente.id"/> </li>';
		
		jQuery("#listaProduccionAgentes").append(option);
	}			
}
function delete_agente(id){
	jQuery("#listaProduccionAgentes #"+id).remove();
}
function deleteAgenteBenefi(id){
	jQuery("#listaBeneficiariosAgentes .benef"+id).remove();
}

function addAgenteBeneficiario(){
	var busIdAgente = jQuery("#txtIdAgenteBeneficiario").val();
	var data = {"agente.id":busIdAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";

	jQuery.asyncPostJSON(url,data,responseAgenteBeneficiario);
}
function responseAgenteBeneficiario(json){
	
	var idAgente = json.agente.id;
	var agente = json.agente.persona.nombreCompleto;
	if(idAgente!=null){
		var option='<li id="'+idAgente+'"><img src="../img/icons/ico_eliminar.gif" onclick="deleteAgenteBenef('+idAgente+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+agente+'" name="listaAgentes['+idAgente+'].id">'+agente+'</label></li>';
		
		jQuery("#listaBeneficiariosAgentes").append(option);
	}			
}

var ventanaPromotoria;
function pantallaModalPromotoria(){
	var url="/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdBeneficiarioPromotoria";
	sendRequestWindow(null, url,obtenerVentanaBusquedaPromotoria);
	
}

function obtenerVentanaBusquedaPromotoria(){
	var wins = obtenerContenedorVentanas();
	ventanaPromotoria= wins.createWindow("promotoriaModal", 10, 320, 900, 450);
	ventanaPromotoria.centerOnScreen();
	ventanaPromotoria.setModal(true);
	ventanaPromotoria.setText("Buscar Promotoria");
	return ventanaPromotoria;
}
function addPromotoriaBeneficiaria(){
	var idPromotoria = jQuery("#txtIdBeneficiarioPromotoria").val();
	var data = {"promotoria.id":idPromotoria};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPromotoria.action";

	jQuery.asyncPostJSON(url,data,responsePromotoriaBenef);
}
function responsePromotoriaBenef(json){
	
	var idPromotoria = json.promotoria.id;
	var promotoria = json.promotoria.descripcion;
	if(idPromotoria!=null){
		var option='<li id="'+idPromotoria+'"><img src="../img/icons/ico_eliminar.gif" onclick="deletePromotoriaBenef('+idPromotoria+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+promotoria+'" name="listaAgentes['+idPromotoria+'].id">'+promotoria+'</label></li>';
		
		jQuery("#listaPromotoriasBeneficiarias").append(option);
	}			
}
function deletePromotoriaBenef(id){
	jQuery("#listaPromotoriasBeneficiarias #"+id).remove();
}

var ventanaPolizaConfig;
function mostrarModalPolizaConfig(){
	var url="/MidasWeb/fuerzaventa/configuracionBono/agregarPoliza.action?tipoAccion=consulta&idField=txtIdPoliza";
	sendRequestWindow(null, url,obtenerVentanaPolizaConfig);
}
function obtenerVentanaPolizaConfig(){
	var wins = obtenerContenedorVentanas();
	ventanaPolizaConfig= wins.createWindow("modalPoliza", 10, 320, 700, 400);
	ventanaPolizaConfig.centerOnScreen();
	ventanaPolizaConfig.setModal(true);
	ventanaPolizaConfig.setText("Búsqueda de Póliza");
	return ventanaPolizaConfig;
}
function addPolizaConfig(){
	var busIdPoliza = jQuery("#txtIdPoliza").val();
	var data = {"filtroPoliza.idPoliza":busIdPoliza};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPoliza.action";
//	alert(busIdPoliza + url);
	jQuery.asyncPostJSON(url,data,responsePolizaConfig);
}
function responsePolizaConfig(json){
	var idPoliza = json.filtroPoliza.idPoliza;
	var numeroPoliza = json.filtroPoliza.numeroPolizaMidasString;
	if(idPoliza!=null){
		var option='<li id="'+idPoliza+'"><img src="../img/icons/ico_eliminar.gif" onclick="delete_poliza('+idPoliza+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+numeroPoliza+'" name="listaConfigPolizas['+idPoliza+'].id">&nbsp;&nbsp;'+numeroPoliza+'</label></li>';
		
		jQuery("#listaPolizasProduccion").append(option);
	}			
}
function delete_poliza(id){
	jQuery("#listaPolizasProduccion #"+id).remove();
}

function checarChec(elementoAchecar){
	jQuery("#"+elementoAchecar).attr("checked",true);
}


function salirConfigBono(){	
	var tipoAccion = jQuery("#tipoAccion").val();
	var url = mostrarContenedorConfiguracionBonosPath;
	if(tipoAccion != 2){
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQ(null,'"+url+"',targetWorkArea,null)",null,null,null);
	}else{
		sendRequestJQ(null, url , targetWorkArea, 'dhx_init_tabbars();');
	}
}

function mostrarHistoricoConfigBono(){
	var tipoOperacion = 70;
	var url = '/MidasWeb/fuerzaventa/agente/mostrarHistorial.action?idTipoOperacion='+tipoOperacion+ "&idRegistro="+parent.dwr.util.getValue("configuracionBono.id");
	mostrarModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
}

function atrasOSiguiente(tabActiva){
	var tipoOperac = 70;
	var idConfigBono=jQuery("#configuracionBono\\.id").val();//"+tabActiva+"
	var tipoAccion = jQuery("#tipoAccion").val();
	if(jQuery.isValid(idConfigBono)){
		if(tipoAccion != 2){	
			parent.mostrarMensajeConfirm('Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?',"20","sendRequestJQ(null,'/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id')+'&tabActiva="+tabActiva+"&idTipoOperacion="+tipoOperac+"&idRegistro='+dwr.util.getValue('configuracionBono.id'),targetWorkArea,'dhx_init_tabbars();')",null,null,null);
		}else{
			path = '/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id')+'&tabActiva='+tabActiva+'&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('configuracionBono.id');
			sendRequestJQ(null, path , targetWorkArea, 'dhx_init_tabbars();');		
		}
	}else{
		parent.mostrarMensajeInformativo("Debe guardar la configuracion de bonos","10");
	}
}


function obtenerFuncionTab(tab){
	var tipoOperac=70;
	if(tab==0){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id')+'&tabActiva=configuracionBonos&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('configuracionBono.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==1){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionBono.id='+dwr.util.getValue('configuracionBono.id')+'&tabActiva=excepciones&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('configuracionBono.id'),targetWorkArea,'dhx_init_tabbars();');
  }
}

function enableCheckBox(){
	jQuery(".js_checkEnable").each(function(index){		
		jQuery(this).attr("disabled",true);
	});
}

function checarChecksCascadeados(elementoAchecar){
	dwr.engine.beginBatch();
	jQuery("#"+elementoAchecar).attr("checked",true);
	dwr.engine.endBatch({async:false});
		checkCombosCascadeados();
	
}

function checkCombosCascadeados(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaLineaVenta input:checked").each(function(index){
		var clave={"clave":jQuery(this).val()};		
		arrSeleccionados.push(clave);
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["clave"],"listaLineaVentaSeleccionadas");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarProductosPorLineaDeVenta.action";
	
	jQuery.asyncPostJSON(url,data,loadChecksProductos);
}
function loadChecksProductos(json){
	comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","idProducto");	
}

function guardarExcepcion(){
	
	var banderaGardadoPol = "";
	var banderaGardadoNeg = "";
	var banderaGardadoAgt = "";
	var banderaClaveAmis = "";
	
	saveConfigBonoExcluTipoCedula();
	saveConfigBonoExcluTipoBono();
	banderaGardadoPol = guardarExcepcionPoliza();
	banderaGardadoNeg = guardarNegocioExcepcion();
	banderaGardadoAgt = guardarAgenteExcepcion();
	banderaClaveAmis = guardarExcepcionClaveAmis();
	
	var todasPolizas = 0;
	var contratadoConSobreComision = 0;
	if(jQuery("#todasPolizas").is(":checked")){
		var todasPolizas = 1;
	}
	
	if(jQuery("#contratadoConSobreComision").is(":checked")){
		 contratadoConSobreComision=1;
	 }
	
	
	
	if(banderaGardadoPol == "correcto" && banderaGardadoNeg== "correcto" && banderaGardadoAgt == "correcto" && banderaClaveAmis=="correcto"){
		var path = "/MidasWeb/fuerzaventa/configuracionBono/guardarExclusiones.action?"
			+"tipoAccion="+parent.dwr.util.getValue("tipoAccion")
		    +"&configuracionBono.id="+parent.dwr.util.getValue("configuracionBono.id")
		    +"&configuracionBono.conIncisosMayorIgual="+parent.dwr.util.getValue("configuracionBono.conIncisosMayorIgual")
		    +"&configuracionBono.conDescuentoMayorIgual="+parent.dwr.util.getValue("configuracionBono.conDescuentoMayorIgual")
		    +"&configuracionBono.TodasPolizas="+todasPolizas
		    +"&configuracionBono.ContratadoConSobreComision="+contratadoConSobreComision
		    +"&configuracionBono.tipoPersona="+parent.dwr.util.getValue("configuracionBono.tipoPersona")
		    +"&configuracionBono.fechaVencimientoCedula="+parent.dwr.util.getValue("configuracionBono.fechaVencimientoCedula")
		    +"&configuracionBono.diasAntesVencimiento="+parent.dwr.util.getValue("configuracionBono.diasAntesVencimiento");
			+"&tabActiva=excepciones";
			
		sendRequestJQ(null, path, targetWorkArea, null);
		
	}else{
		
		//alert('No se permiten valores nulos en los porcentages de las Excluciones seleccionadas');
		parent.mostrarMensajeInformativo("Las excepciones seleccionadas deben de aplicar en el porcentaje de descuento o contener montos en los porcentajes de bono. Favor de no dejar valores nulos.","10");
	}
	
}

function grabarYcopiar(){
	
	var banderaGardadoPol = "";
	var banderaGardadoNeg = "";
	var banderaGardadoAgt = "";
	var banderaGuardadoCveAmis = "";
	
	guardarExcepcion();
	saveConfigBonoExcluTipoCedula();
	saveConfigBonoExcluTipoBono();
	banderaGardadoPol = guardarExcepcionPoliza();
	banderaGardadoNeg = guardarNegocioExcepcion();
	banderaGardadoAgt = guardarAgenteExcepcion();
	banderaGuardadoCveAmis = guardarExcepcionClaveAmis();
	
	
	
	if(banderaGardadoPol == "correcto" && banderaGardadoNeg== "correcto" && banderaGardadoAgt == "correcto" && banderaGuardadoCveAmis=="correcto"){
		var path = "/MidasWeb/fuerzaventa/configuracionBono/grabarYcopiarConfigBonos.action?"
			+"tipoAccion="+parent.dwr.util.getValue("tipoAccion")
		    +"&configuracionBono.id="+parent.dwr.util.getValue("configuracionBono.id")
		    +"&tabActiva=excepciones";
		sendRequestJQ(null, path, targetWorkArea, null);
		
	}else{
		parent.mostrarMensajeInformativo("Las excepciones seleccionadas deben de aplicar en el porcentaje de descuento o contener montos en los porcentajes de bono. Favor de no dejar valores nulos.","10");
	}
	
}

//ventana y grid Polizas

var listarPolizaG;
var gridPoliza;
var listarPolizaGrid;

//funcion para abrir el popup de polizas
function mostrarModalPoliza(){
	var url="/MidasWeb/fuerzaventa/configuracionBono/agregarPoliza.action?idField=txt_IdPoliza&configuracionBono.id="+parent.dwr.util.getValue("configuracionBono.id");
	sendRequestWindow(null, url,obtenerVentanaPoliza);
}
function obtenerVentanaPoliza(){
	var wins = obtenerContenedorVentanas();
	ventanaPoliza= wins.createWindow("modalPoliza", 10, 320, 700, 400);
	ventanaPoliza.centerOnScreen();
	ventanaPoliza.setModal(true);
	ventanaPoliza.setText("Agregar Póliza");
	return ventanaPoliza;
}


function llenarGridPoliza(){
	var Id_Row =gridPoliza.getRowsNum()+1;
	 var idCell = listarPolizaG.getSelectedId();
	 var ClavePoliza = listarPolizaG.cellById(idCell,0).getValue();
	 var Numeropoliza = listarPolizaG.cellById(idCell,1).getValue();
	 
		 gridPoliza.addRow(Id_Row,[,ClavePoliza,Numeropoliza,,,,"../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepPoliza("+Id_Row+")^_self",]);
		 closeDhtmlxWindows('agregPoliza');
}

function cargarGridListadoPolizas(){
	var url="/MidasWeb/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/listarAgregarPolizaGrid.jsp";
	document.getElementById('listarPolizaGrid').innerHTML = '';
	listarPolizaGrid = new dhtmlXGridObject('listarPolizaGrid');
	listarPolizaGrid.load(url);
}

function listarPolizas(){
	listarPolizaG= new dhtmlXGridObject("listarPolizaGrid");
	var form = document.filtrarPoliza;
		if(form!=null){
			listarPolizaGrid.load(listarPolizasExcepcionesPath+"?"+jQuery("#filtrarPoliza").serialize());
		}else{
			listarPolizaGrid.load(listarPolizasExcepcionesPath);
		}
}
function cargarGridPolizalistado(){
	var url="/MidasWeb/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/polizaGrid.jsp?"+jQuery("#filtrarPoliza").serialize();
	document.getElementById('polizaGrid').innerHTML = '';
	gridPoliza = new dhtmlXGridObject('polizaGrid');
	gridPoliza.load(url);
}

function cargarGridPoliza(){
	var idConfig = jQuery("#configuracionBono_id").val();
	document.getElementById('gridPoliza').innerHTML = '';
	gridPoliza = new dhtmlXGridObject('gridPoliza');	
	gridPoliza.attachEvent("onCheck", activaInactivaSegunChecks);
	
	if(idConfig!=null){
		var url ="/MidasWeb/fuerzaventa/configuracionBono/listarExcepcionPoliza.action?configuracionBono.id="+idConfig;
		gridPoliza.load(url);
	}
}

function delete_rowExcepPoliza(Id_Row) {
	gridPoliza.deleteRow(Id_Row);
}

function guardarExcepcionPoliza(){

	var url="/MidasWeb/fuerzaventa/configuracionBono/saveConfigBonoExcepPoliza.action?tabActiva=excepciones";
	var listaIds = gridPoliza.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	var nulosFlag = "correcto";
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();
		var aplicaDescuento="";
		var idPoliza="";
		var monto="";
		var datos ="";
		
		
		for(i=0;i<idArray.length;i++){
			var valor = idArray[i];
			aplicaDescuento = gridPoliza.cellById(valor,0).getValue();
			idPoliza = gridPoliza.cellById(valor,1).getValue();
			montoPorcentajeAgente = gridPoliza.cellById(valor,3).getValue();
			montoPorcentajePromotoria= gridPoliza.cellById(valor,4).getValue();
			descuentoPoliza= gridPoliza.cellById(valor,5).getValue();
			
			
			
			if(montoPorcentajeAgente == '' && montoPorcentajePromotoria == '' && aplicaDescuento == 0){
				nulosFlag="error";
				break;
			}
			
			datos={"idExcepcionPoliza":gridPoliza.cellById(valor,7).getValue(),"idCotizacion":idPoliza,"valorMontoPcteAgente":montoPorcentajeAgente,"valorMontoPctePromotoria":montoPorcentajePromotoria,"descuentoPoliza":descuentoPoliza,"noAplicaDescuentoExcepcion":aplicaDescuento};
			arr.push(datos);
		}
		
		if(nulosFlag != "error"){
			strParam=jQuery.convertJsonArrayToParams(arr,["idExcepcionPoliza","idCotizacion","valorMontoPcteAgente","valorMontoPctePromotoria","descuentoPoliza","noAplicaDescuentoExcepcion"],"listabonoExcepcionPoliza");				
			var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
			jQuery.asyncPostJSON(url,data,responseGuardadoExcepPoliza);
			
		}
	}
	else{
		var data = "configuracionBono.id="+configuracionBono_id;
		jQuery.asyncPostJSON(url,data,responseGuardadoExcepPoliza);	
	}
	
	return nulosFlag;
}
function responseGuardadoExcepPoliza(json){
//	alert(json);
}
//polizas excepciones
function addPolizaExcepciones(){
	var busIdPoliza = jQuery("#txt_IdPoliza").val();
	var data = {"filtroPoliza.idPoliza":busIdPoliza};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPoliza.action";
//	alert(busIdPoliza + url);
	jQuery.asyncPostJSON(url,data,responsePolizaExcepcion);
}
function responsePolizaExcepcion(json){
	var Id_Row =gridPoliza.getRowsNum()+1;
	var idPoliza = json.filtroPoliza.idPoliza;
//	var numeroPoliza = json.filtroPoliza.numeroPoliza;
	var numeroPoliza = json.filtroPoliza.numeroPolizaMidasString;
	var descuentoPoliza=json.filtroPoliza.descuentoPoliza;
	if(idPoliza!=null){		 
		 if (idPoliza==null){
			 alert("Debe seleccionar una poliza");
		 }else{
			 gridPoliza.addRow(Id_Row,[,idPoliza,numeroPoliza,,,descuentoPoliza,"../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepPoliza("+Id_Row+")^_self",]);
			 closeDhtmlxWindows('agregPoliza');
		 }
	}			
}

//function modoAplicacion(){
//	if(jQuery('input:radio[name=configuracionBono.modoAplicacion.id]:checked + label').text()=="Porcentaje"){
//		jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","5");
//		jQuery("#valorPorcentajeImporteDirecto").val("");
//	}else{
//		jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","7");
//		jQuery("#valorPorcentajeImporteDirecto").val("");
//	}
//}


jQuery("#descuentoPoliza").change(function(){
	var valor = jQuery(this).val();
	if(valor > 0){
		jQuery("#todasPolizas").attr("checked","checked");
		activaInactivaSegunChecks();
	}else{
		jQuery("#todasPolizas").removeAttr("checked");
	}
});	


function activaInactivaSegunChecks(){
	var marcadosPoliza = gridPoliza.getCheckedRows(0);
	var marcadosAgente = gridAgente.getCheckedRows(0);
	var descuentoPoliza=jQuery("#descuentoPoliza").val();
	
	if(marcadosPoliza || marcadosAgente){		
		jQuery("#todasPolizas").removeAttr("checked");		
	}else{
		jQuery("#todasPolizas").attr("checked","checked");
		if(descuentoPoliza==""){
			jQuery("#todasPolizas").removeAttr("checked");
		}
	}
}

function existeCrecimiento(){
	var texto =	jQuery("#comparacionProduccionLbl").text();
	var idConf=dwr.util.getValue("configuracionBono.id");		
	if(idConf!=''&&idConf!=null){
		if(dwr.util.getValue("configuracionBono.porcentajeImporteDirecto")==null || dwr.util.getValue("configuracionBono.porcentajeImporteDirecto")==''){
			validacionService.validarExisteCrecimiento(idConf,function(data){
				if(data){
					jQuery("#periodoComparacion").removeClass("ErrorField");
					jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""));
					jQuery("#periodoComparacion").attr('disabled',true);
					jQuery("#periodoComparacion option[value='']").attr("selected",true);
				}
				else{
					jQuery("#periodoComparacion").attr('disabled',false);
				}
			});
		}
	}
	else{
		jQuery("#periodoComparacion").removeClass("jQrequired");
		jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""))
		jQuery("#periodoComparacion").attr('disabled',true);
		jQuery("#periodoComparacion option[value='']").attr("selected",true);
	}	
}

function checarTodasPolizas(){
	var descuentoPoliza = jQuery("#descuentoPoliza").val();
	if(descuentoPoliza!=""){
		jQuery("#todasPolizas").attr('checked', true);
	}else{
		jQuery("#todasPolizas").attr('checked', false);
	}
}

function activaBtnSeleccionarAgtBenef(){	
	if(jQuery("#todosAgentesBoolean").is(':checked')==true){
		jQuery("#btnBenefAgt").css("display","none");
		jQuery("#listaBeneficiariosAgentes").html("");
	}else{
		jQuery("#btnBenefAgt").css("display","block");
	}
}

function activaBtnSeleccionarPromoBenef(){	
	if(jQuery("#todasPromotoriasBoolean").is(':checked')==true){
		jQuery("#btnBenefProm").css("display","none");
		jQuery("#listaPromotoriasBeneficiarias").html("");
	}else{
		jQuery("#btnBenefProm").css("display","block");
	}
}

function validateGridFactores(){
	var listaIds = rangoFactorGrid.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();		
		var datos ="";
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				prima_de = rangoFactorGrid.cellById(valor,1).getValue();
				prima_Hasta = rangoFactorGrid.cellById(valor,2).getValue();
				siniest_inicial = rangoFactorGrid.cellById(valor,3).getValue();
				siniest_final= rangoFactorGrid.cellById(valor,4).getValue();
				crecimiento_inicial= rangoFactorGrid.cellById(valor,5).getValue();
				crecimiento_final= rangoFactorGrid.cellById(valor,6).getValue();
				supera_inicial= rangoFactorGrid.cellById(valor,7).getValue();
				supera_final= rangoFactorGrid.cellById(valor,8).getValue();
				poliza_de = rangoFactorGrid.cellById(valor,9).getValue();
				poliza_Hasta = rangoFactorGrid.cellById(valor,10).getValue();
				bonificacion = rangoFactorGrid.cellById(valor,11).getValue();
				datos={"importePrimaInicial":prima_de,
						"importePrimaFinal":prima_Hasta,
						"pctSiniestralidadInicial":siniest_inicial,
						"pctSiniestralidadFinal":siniest_final,
						"pctCrecimientoInicial":crecimiento_inicial,
						"pctCrecimientoFinal":crecimiento_final,
						"pctSupMetaInicial":supera_inicial,
						"pctSupMetaFinal":supera_final,
						"numPolizasEmitidasInicial":poliza_de,
						"numPolizasEmitidasFinal":poliza_Hasta,
						"valorAplicacion":bonificacion
						};
				arr.push(datos);		
		}
		
			strParam=jQuery.convertJsonArrayToParams(arr,["importePrimaInicial","importePrimaFinal","pctSiniestralidadInicial","pctSiniestralidadFinal"
                                              ,"pctCrecimientoInicial","pctCrecimientoFinal","pctSupMetaInicial","pctSupMetaFinal"
                                              ,"numPolizasEmitidasInicial","numPolizasEmitidasFinal","valorAplicacion"],"listRangoFacotres");				
			var url="/MidasWeb/fuerzaventa/configuracionBono/validarGridRanfoFactores.action";
			var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
			jQuery.asyncPostJSON(url,data,responseListRangoFactores);				
		}
}
function compara_fecha(fechaIncio,fechaFin){
	var fechaIncio1=new Date(fechaIncio.substring(6,10),fechaIncio.substring(3,5)-1,fechaIncio.substring(0,2));
	var anioAlta=fechaIncio1.getFullYear();
	var mesAlta =fechaIncio1.getMonth()+1;
	var diaAlta =fechaIncio1.getDate();
	
	var fechaFin1=new Date(fechaFin.substring(6,10),fechaFin.substring(3,5)-1,fechaFin.substring(0,2));
	var anioFin=fechaFin1.getFullYear();
	var mesFin =fechaFin1.getMonth()+1;
	var diaFin =fechaFin1.getDate();
	
	if(anioAlta>anioFin){
//		alert(anioAlta+">"+anioFin);
		return true;
	}
	if(anioAlta==anioFin && mesAlta>mesFin){
//		alert(anioAlta+"=="+anioFin +" && "+ mesAlta+">"+mesFin);
		return true;
	} 
	if(anioAlta==anioFin && mesAlta==mesFin){
//		alert(anioAlta+"=="+anioFin +" && "+ mesAlta+"=="+mesFin);
		if(diaAlta>diaFin){
//			alert(diaAlta+">"+diaFin);
			return true;
		}
	}
	return false;
}

function requeridoCrecimientoYSupMeta(){
	var checkDirecto = jQuery("#radioDirecto1").is(':checked');
	if(!checkDirecto){
			var idConf=dwr.util.getValue("configuracionBono.id");		
			if(idConf!=''&&idConf!=null){
			//validar crecimiento
				validacionService.validarExisteCrecimiento(idConf,function(data){
					var texto =	jQuery("#comparacionProduccionLbl").text();
					if(data==false){
						jQuery("#periodoComparacion").addClass("jQrequired");
						if(texto.indexOf("*")!=-1){
							jQuery("#comparacionProduccionLbl").text(texto + " *");
						}
					}
					else{
						jQuery("#periodoComparacion").removeClass("jQrequired");
						jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""));
					}
				});
				//validar superacion de meta
				validacionService.validaSupMetaXConfigBono(idConf,function(data){
					var texto =	jQuery("#produccionMinimaLbl").text();
					if(data==false){//alert('validaSupMetaXConfigBono '+data);
						jQuery("#produccionMinimaTxt").addClass("jQrequired");
						if(texto.indexOf("*")!=-1){
							jQuery("#produccionMinimaLbl").text(texto + " *");
						}
					}
					else{
						jQuery("#produccionMinimaTxt").removeClass("jQrequired");
						jQuery("#produccionMinimaLbl").text(texto.replace("*", ""));
					}
				});
			}
			jQuery("#valorPorcentajeImporteDirecto").removeClass("jQrequired");
	}else{
		var texto1 =	jQuery("#produccionMinimaLbl").text();
		var texto =	jQuery("#comparacionProduccionLbl").text();
		//SI LOS SIGUIENTES CAMPOS ESTAN MARCADOS COMO REQUERIDOS LO QUITA
		jQuery("#periodoComparacion").removeClass("jQrequired");
		jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""));
		jQuery("#produccionMinimaTxt").removeClass("jQrequired");
		jQuery("#produccionMinimaLbl").text(texto1.replace("*", ""));
		//SE MARCA COMO OBLIGATORIO EL CAMPO  Directo
		jQuery("#valorPorcentajeImporteDirecto").addClass("jQrequired");
	}
}

function guardarConfigBono_AndValidRangoConf(){
	var prodMinima=jQuery("#produccionMinimaTxt").val();
	var pctSinietMaxima= jQuery("#pctSiniestralidadMaxima").val();
	var pctImporteDirecto = jQuery("#valorPorcentajeImporteDirecto").val();
	if(prodMinima!=''&&prodMinima.indexOf(",")!=-1){
		jQuery("#produccionMinimaTxt").val(prodMinima.replace(/,/g,''));
	}
	if(pctSinietMaxima!='' && pctSinietMaxima.indexOf(",")!=-1){
		jQuery("#pctSiniestralidadMaxima").val(pctSinietMaxima.replace(/,/g,''))
	}
	if(pctImporteDirecto!='' && pctImporteDirecto.indexOf(",")!=-1){
		jQuery("#valorPorcentajeImporteDirecto").val(pctImporteDirecto.replace(/,/g,''))
	}
	var idConf=dwr.util.getValue("configuracionBono.id");
	var radioConfigRangos = jQuery("#radioConfigRangos1").is(':checked');
	if(radioConfigRangos && idConf!=''){
		dwr.engine.beginBatch();
		 validacionService.existeRangoConfigurado(idConf, function(data){
			if(data==false){
				parent.mostrarMensajeInformativo("Debe de Agregar Factores a la Configuracion para poder Guardar ","10");
			}else{
//				guardarConfiguracionBono();
				validaCampoBonif();
			}
		});
		dwr.engine.endBatch({async:false});		
	}else{
		guardarConfiguracionBono();
	}
}

/** Da formato a un número para su visualización 
 * numero (Number o String) - Número que se mostrará 
 * decimales (Number, opcional) - Nº de decimales (por defecto, auto) 
 * separador_decimal (String, opcional) - Separador decimal (por defecto, coma) 
 * separador_miles (String, opcional) - Separador de miles (por defecto, ninguno) 
 */ 
function formato_numero(numero, decimales, separador_decimal, separador_miles){
	var num = numero.indexOf(',');
	if (num !=-1){
		var i =0;
		var splitStr = numero.split('.');
		var splitLeft = splitStr[0];
		var entero = splitLeft.split(',');
		for (i =0; entero.size()-1>=i; i++) {
			if (i==0){
				if(entero[i].length>3){
					parent.mostrarMensajeInformativo("El valor Ingresado no es Correcto","10");
					return "";
				}
			}else{
				if(entero[i].length!=3){
					parent.mostrarMensajeInformativo("El valor Ingresado no es Correcto","10");
					return "";
				}
			}
		}
		if(separador_decimal!='' && numero.indexOf('.')==-1){
			numero = numero+'.00';
		}else if(numero.indexOf('.')!=-1){
			numero = numero.replace('.00','');
		}
			
		return numero;
	}
	// Convertimos el punto en separador_decimal  
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";     
	}
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         // Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");        
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
			}     
		}
		if(separador_decimal!='' && numero.indexOf('.')==-1){
			numero = numero+'.00';
		}else if(numero.indexOf('.')!=-1){
			numero = numero.replace('.00','');
		}
		return numero; 
	}
	function presionTecla(e){
	  if (window.event)  {
	    var tecla=window.event.keyCode;
//		alert("1 - "+tecla)
	    if (tecla==44)    {
	      window.event.returnValue=false;
//	      alert('No se puede ingresar espacios en blanco para un nombre de usuario');
	    }
	  }  else    if (e) { alert(e.which);
	      var tecla=e.which;
//		  alert("2 - "+tecla)
	      if(tecla==44)  {
	        e.preventDefault();
//	        alert('No se puede ingresar espacios en blanco para un nombre de usuario');
	      }
	    }
	}
	
	function validateNum4Enteros(numStr){
		var expreg = /^(\d{0,4})(\.\d{1,2})?$/;//^(\d{0,14})(\.\d{1,2})?$
		if(numStr!='' && numStr!='&nbsp;'){
		    numStr=numStr.toString().replace(/,/g,'');
			if(numStr!=''){
				return expreg.test(numStr);			
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	function validarCheckModoAplicacion(){		
		var texto = jQuery('input:radio[name=configuracionBono.modoAplicacion.id]:checked').next().text();
		if (texto == 'Porcentaje'){
			jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","9");
//			jQuery("#valorPorcentajeImporteDirecto").removeAttr("onblur");
//			jQuery("#valorPorcentajeImporteDirecto").attr("onBlur","validarEstandarEnModApl(this);");
			if(validateNum4Enteros(jQuery("#valorPorcentajeImporteDirecto").val())==true){
				jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
			}else{
				parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 4 enteros y 2 decimales","10");
				jQuery("#valorPorcentajeImporteDirecto").val("");
			}
		}else if(texto == 'Importe'){
			jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","21");
//			jQuery("#valorPorcentajeImporteDirecto").removeAttr("onblur");
//			jQuery("#valorPorcentajeImporteDirecto").attr("onBlur","validarEstandarEnCampos(this);");
			if(validateNumber(jQuery("#valorPorcentajeImporteDirecto").val())==true){
				jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
			}else{
				parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
				jQuery("#valorPorcentajeImporteDirecto").val("");
			}
		}
	}
	function validarCampoModoApl(obj){
		var esPorcen_importe=jQuery("input:radio[name=configuracionBono.modoAplicacion.id]:checked").next("label").text();
//		alert(esPorcen_importe);
		if(esPorcen_importe =='Importe'){
			validarEstandarEnCampos(obj);
		}else if(esPorcen_importe =='Porcentaje'){
			validarEstandarEnModApl(obj)			
		}
	}
	
	function validaCampoBonif(){
		var esPorcentaje=jQuery("input:radio[name=configuracionBono.modoAplicacion.id]:checked").next("label").text();
		var idConf=dwr.util.getValue("configuracionBono.id");
		if(esPorcentaje=='Porcentaje'){
			dwr.engine.beginBatch();
			 validacionService.validaTamanioCampoBonificacion(idConf, function(data){
				if(data==false){
					parent.mostrarMensajeInformativo("Favor de verificar la configuración de los factores campo Bonificación","10");
				}else{
					guardarConfiguracionBono();
				}
			});
			dwr.engine.endBatch({async:false});
		}else{
			guardarConfiguracionBono();
		}
	}
	
	function desavilitarEdisionSegunBenef(){
		
		alert("Ingreso a la funcion");
		
		var tipoBenefConfig = document.getElementById("configuracionBono_benef").value;
		var polPorBonAgt = document.getElementById("porBonoAgt");
		var polPorBonoProm = document.getElementById("porBonoProm");
		if(tipoBenefConfig == 838 ){
			alert('Promotoria');
			if(polPorBonAgt == null){
				
				alert('elemeto vacio');
			}else{
				
				alert('elemeto NO vacio');
			}
		}else if(tipoBenefConfig == 839){
			alert('Agente');
		}else if(tipoBenefConfig == 840){
			alert('Ambos');
		}
		
		
	}
//	validarEstandarEnModApl
	
//	valorPorcentajeImporteDirecto
//rangoFactorGrid.setEditable(false);
	
function add_RowCveAmis(){
	var Id_Row = gridCveAmis.getRowsNum()+1;
	var fila = Id_Row-1;
	if(fila>0){
		var cve_amis_ini = gridCveAmis.cellById(fila,1).getValue();
		var cve_amis_fin = gridCveAmis.cellById(fila,2).getValue();
		if((cve_amis_ini=='' && cve_amis_fin =='') || (Number(cve_amis_ini)>Number(cve_amis_fin))){
			 return parent.mostrarMensajeInformativo("Favor de verificar el Grid de claves amis","10");
		}else{
			gridCveAmis.addRow(Id_Row, ["","","","../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowCveAmis("+Id_Row+")^_self" ]);
		}
	}else{
		gridCveAmis.addRow(Id_Row, ["","","","../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowCveAmis("+Id_Row+")^_self" ]);			
	}
}

function delete_rowCveAmis(Id_Row){

	gridCveAmis.deleteRow(Id_Row);
}

function guardarExcepcionClaveAmis(){
	var listaIds = gridCveAmis.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	var nulosFlag="correcto";
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();
		var cve_amis_ini="";
		var clave_amis_fin="";
		
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				cve_amis_ini = pad(gridCveAmis.cellById(valor,1).getValue(),5);
				cve_amis_fin = pad(gridCveAmis.cellById(valor,2).getValue(),5);
				
				
				datos={"cve_amis_ini":cve_amis_ini,"cve_amis_fin":cve_amis_fin,"configBono.id":configuracionBono_id};
				arr.push(datos);			
		}
		
			
		strParam=jQuery.convertJsonArrayToParams(arr,["cve_amis_ini","cve_amis_fin","configBono.id"],"listaRangosClaveAmis");				
		var url="/MidasWeb/fuerzaventa/configuracionBono/saveListaExclucionCveAmis.action";
		var data = "configuracionBono.id="+configuracionBono_id+"&"+strParam;
		jQuery.asyncPostJSON(url,data,null);
			
		}else{
			var url="/MidasWeb/fuerzaventa/configuracionBono/saveListaExclucionCveAmis.action";
			var data = "configuracionBono.id="+configuracionBono_id;
			jQuery.asyncPostJSON(url,data,null);	
		}
	
	return nulosFlag;
}
function pad(n, width, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function validaGridAmisAndSaveExcepcion(){
	var listaIds = gridCveAmis.getAllRowIds(",");
	var configuracionBono_id = jQuery("#configuracionBono_id").val();
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		var strParam="";	
		var arr = new Array();		
		var datos ="";
		for(i=0;i<idArray.length;i++){
				var valor = idArray[i];
				cve_amis_ini = gridCveAmis.cellById(valor,1).getValue();
				cve_amis_fin = gridCveAmis.cellById(valor,2).getValue();
				if(cve_amis_ini=='' || cve_amis_fin ==''){
					return parent.mostrarMensajeInformativo("Favor de verificar el Grid de claves amis","10");
				}else{
					datos={"cve_amis_ini":cve_amis_ini,
							"cve_amis_fin":cve_amis_fin
							};
					arr.push(datos);
				}
		}
		
		dwr.engine.beginBatch();
		 validacionService.validaRangosGridAmis(arr, function(data){
			if(data==false){
				parent.mostrarMensajeInformativo("Favor de verificar el Grid de claves amis","10");
			}else{
				guardarExcepcion();
			}
		});
		dwr.engine.endBatch({async:false});
	}else{
		guardarExcepcion(); 
	}
}

function validateNum5Enteros(numStr){
	var expreg = /^(\d{0,5})?$/;
	if(numStr!='' && numStr!='&nbsp;'){
	    numStr=numStr.toString().replace(/,/g,'');
		if(numStr!=''){
			return expreg.test(numStr);			
		}else{
			return true;
		}
	}else{
		return true;
	}
}