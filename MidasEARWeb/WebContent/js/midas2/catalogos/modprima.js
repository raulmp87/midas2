var modprimaGrid;

function mostrarCatalogoModPrima() {
	//alert(mostrarCatalogoModprimaPath);
	sendRequestJQ(null, mostrarCatalogoModprimaPath, targetWorkArea,null);
}

function guardarModPrima() {
   //alert(guardarGpoVarModPrimaPath+"?"+jQuery(document.grupoVariablesModificacionPrimaForm).serialize());
	sendRequestJQ(null, guardarModprimaPath+"?"+jQuery(document.modPrimaForm).serialize(), targetWorkArea, 'getIdDetalle();');
}


function eliminarModPrima() {
	var idgpoVarModifPrima = getIdFromGrid(modprimaGrid, 0);
	sendRequestJQ(null, verDetalleModprimaPath + "?"+jQuery(document.modPrimaForm).serialize(), targetWorkArea, 'getIdDetalle();');
}

function listarModPrima(){
	
	document.getElementById("modprimaGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	gpoVarModifPrimaGrid = new dhtmlXGridObject("modprimaGrid");
	var form = document.modPrimaForm;
	if(form!=null){
		//alert(listarFiltradoGpoVarModPrimaPath+"?"+jQuery(document.grupoVariablesModificacionPrimaForm).serialize());
		modprimaGrid.load(listarModprimaPath+"?"+jQuery(document.modPrimaForm).serialize());
	}else{
		modprimaGrid.load(listarModprimaPath);
	}
}

function verDetalleModPrima (tipoAccion) {
	if(modprimaGrid.getSelectedId() != null){
		var idModPrima = getIdFromGrid(modprimaGrid, 0);
		sendRequestJQ(null, verDetalleModprimaPath + "?tipoAccion=" + tipoAccion + "&id=" + idgpoVarModifPrima, targetWorkArea, null);
	}else{
		sendRequestJQ(null, verDetalleModprimaPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
	}
}

function verDetalleModPrimaDos (tipoAccion) {
	if(modprimaGrid.getSelectedId() != null){
		var idModPrima = getIdFromGrid(modprimaGrid, 0);
		sendRequestJQ(null, verDetalleModprimaDosPath + "?tipoAccion=" + tipoAccion + "&id=" + idgpoVarModifPrima, targetWorkArea, null);
	}else{
		sendRequestJQ(null, verDetalleModprimaDosPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
	}
}
function getIdDetalle(id){
	//alert(obtenerClaveDetallePath +  "?modPrimaAction.id=" + dwr.util.getValue("idgrupo"));return;
	document.getElementById("modprimaGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
    modprimaGrid = new dhtmlXGridObject('modprimaGrid');
	modprimaGrid.load(obtenerClaveDetallePath +  "?modPrimaAction.id=" + dwr.util.getValue("idgrupo") );
}

function getIdDetalleDetalle(id){
	
	document.getElementById("modprimaGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
    modprimaGrid = new dhtmlXGridObject('modprimaGrid');
  
	modprimaGrid.load(obtenerClaveDetallePath +  "?modPrimaAction.id=" +id);
}
