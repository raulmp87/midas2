var coberturasObligatoriasGrid
var coberturasAdicionalesGrid;
var sumasAseguradasAdicionalesGrid;
var deduciblesAdicionalesGrid;

var coberturasAdicionalesProcessor;
var sumasAseguradasAdicionalesProcessor;
var deduciblesAdicionalesProcessor;

var idToNegAct = -1;
var idToNegProductoAct = -1;
var idToNegPaqSeccionAct = -1;
var idToNegTipoPolizaAct = -1;
var idToNegSeccionAct = -1;
var idMonedaAct = -1;
var idVigenciaAct = -1;
var idMunicipioAct = -1;


function obtenerProductos(idToNegocio){
	idToNegAct = idToNegocio;
	listadoService.getMapNegProductoPorNegocio(idToNegocio,function(data){
		var combo = document.getElementById('idToNegProducto');
		addOptions(combo,data);
	});
	estadosNegocio(idToNegocio);
	idToNegProductoAct = -1;
}

function obtenerTiposPoliza(idToNegProducto){
	idToNegProductoAct = idToNegProducto;
	listadoService.getMapNegTipoPolizaPorNegProducto(idToNegProducto,function(data){
		var combo = document.getElementById('idToTipoPoliza');
		addOptions(combo,data);
	});
	idToNegTipoPolizaAct = -1;
}

function obtenerLineasDeNegocio(idToTipoPoliza){
	idToNegTipoPolizaAct = idToTipoPoliza;
	listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza,function(data){
		var combo = document.getElementById('idToNegSeccion');
		addOptions(combo,data);
	});
	idToNegSeccionAct = -1;
}

function obtenerMonedas(idToTipoPoliza){
	listadoService.getMapMonedaPorNegTipoPoliza(idToTipoPoliza,function(data){
		var combo = document.getElementById('idTcMoneda');
		addOptions(combo,data);
	});
	idMonedaAct = -1;
}

function obtenerVigencias(){
	listadoService.getMapVigencias(function(data){
		var combo = document.getElementById('idTcVigencia');
		addOptions(combo,data);
	});
	idVigenciaAct = -1;
	validaCombos();
}

function obtenerPaquetes(idToNegSeccion){
	idToNegSeccionAct = idToNegSeccion;
	listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(
			idToNegSeccion,
			function(data){
				var combo = document.getElementById('idToNegPaqueteSeccion');
				addOptions(combo,data);
			});
	idToNegPaqSeccionAct = -1;
	validaCombos();
}

function estadosNegocio(idNegocio){
	listadoService.getEstadosPorNegocioId(idNegocio,false,function(data){
		var combo = document.getElementById('stateId');
		addOptions(combo,data);
    });
}

function obtieneMunicipio(idEstado){
	listadoService.getMapMunicipiosPorEstado(
			idEstado,
			function(data){
				var combo = document.getElementById('cityId');
				addOptions(combo,data);
			});
	idMunicipioAct = -1;
	validaCombos();
}

function onChangeComboMoneda(idMoneda){
	if(idMoneda == ""){
		idMoneda = -1;
	}
	idMonedaAct = idMoneda;
}

function onChangeComboVigencia(idVigencia){
	if(idVigencia == ""){
		idVigencia = -1;
	}
	idVigenciaAct = idVigencia;
	validaCombos();
}

function onChangeComboPaquete(idToNegPaqSeccion){
	if(idToNegPaqSeccion == ""){
		idToNegPaqSeccion = -1;
	}
	idToNegPaqSeccionAct = idToNegPaqSeccion;
	validaCombos();
}

function onChangeComboMunicipio(idMunicipio){
	idMunicipioAct = idMunicipio;
	validaCombos();
}

function validaCombos(){
	if(idToNegTipoPolizaAct > -1 && idToNegSeccionAct > -1 && idToNegPaqSeccionAct > -1  && idMonedaAct > -1 && idVigenciaAct > -1){
		jQuery("#stateId").removeAttr("disabled");
		jQuery("#cityId").removeAttr("disabled");
		refrescarGridsTarifaServicioPublico(null,null,null,null);
	}else{
		jQuery("#stateId").attr("disabled","true");
		jQuery("#cityId").attr("disabled","true");		
		document.getElementById('coberturasAdicionalesGrid').innerHTML='';
		coberturasAdicionalesGrid = new dhtmlXGridObject('coberturasAdicionalesGrid');
		document.getElementById('coberturasObligatoriasGrid').innerHTML='';
		coberturasObligatoriasGrid = new dhtmlXGridObject('coberturasObligatoriasGrid');
		document.getElementById('sumasAseguradasAdicionalesGrid').innerHTML='';
		sumasAseguradasAdicionalesGrid = new dhtmlXGridObject('sumasAseguradasAdicionalesGrid');
		document.getElementById('deduciblesAdicionalesGrid').innerHTML='';
		deduciblesAdicionalesGrid = new dhtmlXGridObject('deduciblesAdicionalesGrid');
	}
}

function obtenerCoberturasAdicionales(){
	coberturasAdicionalesGrid = new dhtmlXGridObject('coberturasAdicionalesGrid');
	
	coberturasAdicionalesGrid.load(obtenerCoberturasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + "&idToNegSeccion=" + idToNegSeccionAct);
		
	coberturasAdicionalesProcessor = new dataProcessor(guardarMontosCoberturasAdicionalesPath);	
	coberturasAdicionalesProcessor.enableDataNames(true)
	coberturasAdicionalesProcessor.setTransactionMode("POST");
	coberturasAdicionalesProcessor.setUpdateMode("cell");
	coberturasAdicionalesProcessor.attachEvent("onAfterUpdate",refrescarGridCoberturasAdicionales);
	//coberturasAdicionalesProcessor.defineAction("mensajeGenerico", response);
	coberturasAdicionalesProcessor.setVerificator(8, isFloat0);
	coberturasAdicionalesProcessor.init(coberturasAdicionalesGrid);
}

function response(node){
	parent.readResponseDataProcessor(node,initGridsTarifaServicioPublico);
}

function obtenerCoberturasObligatorias(){
	coberturasObligatoriasGrid = new dhtmlXGridObject('coberturasObligatoriasGrid');
	coberturasObligatoriasGrid.load(obtenerCoberturasObligatoriasGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId")+ "&idToNegSeccion=" + idToNegSeccionAct);
}

function refrescarGridsTarifaServicioPublico(sid,action,tid,node){
	obtenerCoberturasObligatorias();
	obtenerCoberturasAdicionales();
	obtenerSumasAseguradasAdicionales();
	obtenerDeduciblesAdicionales();
	return true;
}

function initGridsTarifaServicioPublico(){
	if(idToNegPaqSeccionAct != -1){
		refrescarGridsTarifaServicioPublico(null,null,null,null);
	}	
}

//COBERTURAS ADICIONALES
function refrescarGridCoberturasAdicionales(sid,action,tid,node){
	obtenerCoberturasAdicionales();
	return true;
}

//SA ADICIONALES
function obtenerSumasAseguradasAdicionales(){
	sumasAseguradasAdicionalesGrid = new dhtmlXGridObject('sumasAseguradasAdicionalesGrid');

	sumasAseguradasAdicionalesGrid.load(obtenerSumasAseguradasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId")+ "&idToNegSeccion=" + idToNegSeccionAct);

	
	sumasAseguradasAdicionalesProcessor = new dataProcessor(guardarMontosSumasAseguradasAdicionalesPath);	
	sumasAseguradasAdicionalesProcessor.enableDataNames(true)
	sumasAseguradasAdicionalesProcessor.setTransactionMode("POST");
	sumasAseguradasAdicionalesProcessor.setUpdateMode("cell");
	sumasAseguradasAdicionalesProcessor.attachEvent("onAfterUpdate",refrescarGridSumasAseguradasAdicionales);
	//sumasAseguradasAdicionalesProcessor.defineAction("mensajeGenerico", response);
	sumasAseguradasAdicionalesProcessor.setVerificator(9, isFloat0);
	sumasAseguradasAdicionalesProcessor.init(sumasAseguradasAdicionalesGrid);
}

function refrescarGridSumasAseguradasAdicionales(sid,action,tid,node){
	obtenerSumasAseguradasAdicionales();
	return true;
}

//DEDUCIBLES
function obtenerDeduciblesAdicionales(){
	deduciblesAdicionalesGrid = new dhtmlXGridObject('deduciblesAdicionalesGrid');

	deduciblesAdicionalesGrid.load(obtenerDeduciblesAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId")+ "&idToNegSeccion=" + idToNegSeccionAct);

	deduciblesAdicionalesGrid.groupBy(10);
	deduciblesAdicionalesProcessor = new dataProcessor(guardarMontosDeduciblesAdicionalesPath);	
	deduciblesAdicionalesProcessor.enableDataNames(true)
	deduciblesAdicionalesProcessor.setTransactionMode("POST");
	deduciblesAdicionalesProcessor.setUpdateMode("cell");
	deduciblesAdicionalesProcessor.attachEvent("onAfterUpdate",refrescarGridDeduciblesAdicionales);
	//deduciblesAdicionalesProcessor.defineAction("mensajeGenerico", response);
	deduciblesAdicionalesProcessor.setVerificator(11, isFloat0);
	deduciblesAdicionalesProcessor.setVerificator(12, isFloat0);
	deduciblesAdicionalesProcessor.setVerificator(13, isFloat0);
	deduciblesAdicionalesProcessor.init(deduciblesAdicionalesGrid);
}

function refrescarGridDeduciblesAdicionales(sid,action,tid,node){
	obtenerDeduciblesAdicionales();
	return true;
}