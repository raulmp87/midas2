var agrupadorSeccionGrid;
function listarAgrupadorSeccion(negocio) {
	initAgrupadorSeccionGrid()
	document.agrupadorSeccionForm.negocio.value = negocio;

	agrupadorSeccionGrid.load(listarFiltradoAgrupadorSeccionPath+"?negocio="+negocio);
	
}

function initAgrupadorSeccionGrid(){
	document.getElementById("agrupadorSeccionGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	agrupadorSeccionGrid = new dhtmlXGridObject("agrupadorSeccionGrid");
}

function listarFiltradoAgrupadorSeccion(){
	initAgrupadorSeccionGrid();
    var idToSeccion = dwr.util.getValue("agrupadorTarifaSeccion.id.idToSeccion");
    var idTcMoneda= dwr.util.getValue("agrupadorTarifaSeccion.id.idMoneda");
    var idToAgrupadorTarifa = dwr.util.getValue("agrupadorTarifaSeccion.id.idToAgrupadorTarifa");
    var negocio = document.agrupadorSeccionForm.negocio.value;
    
	if(idToSeccion != headerValue){
		agrupadorSeccionGrid.load(listarFiltradoAgrupadorSeccionPath+"?"+jQuery(document.agrupadorSeccionForm).serialize());
	}
}

function verDetalleAgrupadorSeccion (tipoAccion) {
	if(agrupadorSeccionGrid.getSelectedId() != null){
		var idAgrupadorSeccion = getIdFromGrid(agrupadorSeccionGrid, 0);
		sendRequestJQ(null, verDetalleAgrupadorSeccionPath + "?tipoAccion=" + tipoAccion + "&key=" + idAgrupadorSeccion + "&negocio=" + document.agrupadorSeccionForm.negocio.value, targetWorkArea, null);
	}else{
		sendRequestJQ(null, verDetalleAgrupadorSeccionPath + "?tipoAccion=" + tipoAccion + "&key=" + "&negocio=" + document.agrupadorSeccionForm.negocio.value, targetWorkArea, null);
	}
}

function mostrarCatalogoAgrupadorSeccion() {
	var negocio = document.agrupadorSeccionForm.negocio.value
	sendRequestJQ(null, mostrarCatalogoAgrupadorSeccionPath+"?negocio ="+negocio, targetWorkArea, 'listarAgrupadorSeccion("'+negocio+'");');
}

function guardarAgrupadorSeccion(){
	var negocio = document.agrupadorSeccionForm.negocio.value;
	sendRequestJQ(null, guardarAgrupadorSeccionPath+"?"+jQuery(document.agrupadorSeccionForm).serialize(), targetWorkArea, 'listarFiltradoAgrupadorSeccion();');
}

function eliminarAgrupadorSeccion() {
	var negocio = document.agrupadorSeccionForm.negocio.value
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarAgrupadorSeccionPath + "?"+jQuery(document.agrupadorSeccionForm).serialize(), targetWorkArea, 'listarFiltradoAgrupadorSeccion();');
	}
}

function getMoneda(monedaSelect){
	listadoService.getMapMonedas( function(monedaMap) {
		addOptions(monedaSelect, monedaMap);
	});
}

function getAgrupadoresPorMoneda(monedaSelect, agrupadorSelect) {
	var idMoneda =  dwr.util.getValue(monedaSelect);
	var negocio = document.agrupadorSeccionForm.negocio.value;
	if (idMoneda != headerValue) {
		listadoService.getMapAgrupadoresPorNegocio(negocio, idMoneda, function(agrupadoresMap) {
			addOptions(agrupadorSelect, agrupadoresMap);
		});
	} else {
		addOptions(agrupadorSelect, null);
	}
}

function getAgrupadoresPorMonedaPorSeccion(seccionSelect, monedaSelect, agrupadorSelect) {
	var idSeccion = dwr.util.getValue(seccionSelect);
	var idMoneda =  dwr.util.getValue(monedaSelect);
	var negocio = document.agrupadorSeccionForm.negocio.value;	
	if (idMoneda == null || idMoneda == '') {
		idMoneda=484;
	}
	listadoService.getMapAgrupadoresPorMonedaPorSeccion(negocio, idSeccion, idMoneda,  function(agrupadoresMap) {
		addOptions(agrupadorSelect, agrupadoresMap);
	});
}

function validaFiltro(){
	var filtro1 = dwr.util.getValue("agrupadorTarifaSeccion.id.idToSeccion");
	
	if(filtro1  != headerValue){
		document.getElementById('busquedaAgrupadorSeccion').style.display = 'block'; 
	}
}