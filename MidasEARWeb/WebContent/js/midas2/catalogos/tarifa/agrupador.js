var agrupadorGrid;
function listarAgrupador(negocio) {
	document.getElementById("agrupadorGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	agrupadorGrid = new dhtmlXGridObject("agrupadorGrid");
	agrupadorGrid.attachHeader("&nbsp;,#text_filter,#text_filter,&nbsp,&nbsp;,#text_filter,&nbsp;,&nbsp;,&nbsp,&nbsp");
	document.agrupadorForm.negocio.value = negocio;
	agrupadorGrid.load(listarFiltradoAgrupadorTarPath+"?negocio="+negocio);
}
function mostrarCatalogoAgrupador() {
	var negocio = document.agrupadorForm.negocio.value;
	sendRequestJQ(null, mostrarCatalogoAgrupadorTarPath+"?negocio = "+negocio, targetWorkArea, 'listarAgrupador("'+negocio+'");');
}

function nuevaVersionAgrupador(id){
	var negocio = document.agrupadorForm.negocio.value;
	sendRequestJQ(null, nuevaVersionAgrpadorTarPath+"?id="+id+"&negocio = "+negocio, targetWorkArea, 'listarAgrupador("'+negocio+'");');
}

function guardarAgrupador(){
	var negocio = document.agrupadorForm.negocio.value;
	sendRequestJQ(null, guardarAgrupadorTarPath+"?"+jQuery(document.agrupadorForm).serialize(), targetWorkArea, 'listarAgrupador("'+negocio+'");');
}
function eliminarAgrupador() {
//	var idAgrupador = getIdFromGrid(agrupadorGrid, 0);
	var negocio = document.agrupadorForm.negocio.value;
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarAgrupadorTarPath + "?"+jQuery(document.agrupadorForm).serialize(), targetWorkArea, 'listarAgrupador("'+negocio+'");');
	}
}

function verDetalleAgrupador (tipoAccion) {

	if(agrupadorGrid.getSelectedId() != null){
		var idAgrupador = getIdFromGrid(agrupadorGrid, 0);
		sendRequestJQ(null, verDetalleAgrupadorTarPath + "?tipoAccion=" + tipoAccion + "&id=" + idAgrupador + "&negocio=" + document.agrupadorForm.negocio.value, targetWorkArea, null);
	}else{
		sendRequestJQ(null, verDetalleAgrupadorTarPath + "?tipoAccion=" + tipoAccion + "&negocio=" + document.agrupadorForm.negocio.value, targetWorkArea, null);
	}
}



function disableEnterKey(e)
{
     var key;

     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox

     if(key == 13)
          return false;
     else
          return true;
}