/**
 * Relaciones de agrupador tarifa
 */
var tarifaAgrupadorTarifaAsociadasGrid;
var tarifaAgrupadorTarifaDisponiblesGrid;	
var tarifaAgrupadorTarifaProcessor;

/**
 * tarifa
 */

function obtenerTarifaAgrupadorTarifaAsociadas(){
	tarifaAgrupadorTarifaAsociadasGrid = new dhtmlXGridObject('tarifaAgrupadorTarifaAsociadasGrid');
	if(dwr.util.getValue("claveNegocio")=='D'){
		tarifaAgrupadorTarifaAsociadasGrid.load(obtenerTarifaAgrupadorTarifaAsociadasPath + 
				"?tarifaAgrupadorTarifa.idToAgrupadorTarifaFromString=" + dwr.util.getValue("idAgrupadorTarifaName") + 
				"&claveNegocio=" + dwr.util.getValue("claveNegocio"));
	}else{
		tarifaAgrupadorTarifaAsociadasGrid.load(obtenerTarifaAgrupadorTarifaAsociadasPath + 
				"?tarifaAgrupadorTarifa.idToAgrupadorTarifaFromString=" + dwr.util.getValue("idAgrupadorTarifaName") + 
				"&tarifaAgrupadorTarifa.id.idLineanegocio=" + dwr.util.getValue("idLineaNegocioName") + 
				"&tarifaAgrupadorTarifa.id.idMoneda=" + dwr.util.getValue("idMonedaName") + 
				"&claveNegocio=" + dwr.util.getValue("claveNegocio"));
	}
	
	//Creacion del DataProcessor
	tarifaAgrupadorTarifaProcessor = new dataProcessor(accionSobreTarifaAgrupadorTarifaAsociadasPath +"?tarifaAgrupadorTarifa.idToAgrupadorTarifaFromString=" + dwr.util.getValue("idAgrupadorTarifaName"));

	tarifaAgrupadorTarifaProcessor.enableDataNames(true);
	tarifaAgrupadorTarifaProcessor.setTransactionMode("POST");
	tarifaAgrupadorTarifaProcessor.setUpdateMode("cell");
	
	tarifaAgrupadorTarifaProcessor.attachEvent("onAfterUpdate",refrescarGridsTarifaAgrupadorTarifa);
	
	tarifaAgrupadorTarifaProcessor.init(tarifaAgrupadorTarifaAsociadasGrid);
}


function obtenerTarifaAgrupadorTarifaDisponibles(){
	tarifaAgrupadorTarifaDisponiblesGrid = new dhtmlXGridObject('tarifaAgrupadorTarifaDisponiblesGrid');
	if(dwr.util.getValue("claveNegocio")=='D'){
		tarifaAgrupadorTarifaDisponiblesGrid.load(obtenerTarifaAgrupadorTarifaDisponiblesPath + 
				"?tarifaAgrupadorTarifa.idToAgrupadorTarifaFromString=" + dwr.util.getValue("idAgrupadorTarifaName") + 
				"&claveNegocio=" + dwr.util.getValue("claveNegocio"));
	}else{
		tarifaAgrupadorTarifaDisponiblesGrid.load(obtenerTarifaAgrupadorTarifaDisponiblesPath + 
				"?tarifaAgrupadorTarifa.idToAgrupadorTarifaFromString=" + dwr.util.getValue("idAgrupadorTarifaName") + 
				"&tarifaAgrupadorTarifa.id.idLineanegocio=" + dwr.util.getValue("idLineaNegocioName") + 
				"&tarifaAgrupadorTarifa.id.idMoneda=" + dwr.util.getValue("idMonedaName")+ 
				"&claveNegocio=" + dwr.util.getValue("claveNegocio"));
	}
}

function iniciaGridsTarifaAgrupadorTarifa() {
	if(dwr.util.getValue("claveNegocio")=='D'){
		if(dwr.util.getValue("idAgrupadorTarifaName")!=headerValue){
			refrescarGridsTarifaAgrupadorTarifa(null,null,null,null);
		}else{
			 removeGridsTarifaAgrupadorTarifa();
		}
		
	}else{
		if(dwr.util.getValue("idMonedaName") != headerValue){
			refrescarGridsTarifaAgrupadorTarifa(null,null,null,null);
		}else{
			 removeGridsTarifaAgrupadorTarifa();
		}
	}
}


function refrescarGridsTarifaAgrupadorTarifa(sid,action,tid,node){

	obtenerTarifaAgrupadorTarifaAsociadas();
	obtenerTarifaAgrupadorTarifaDisponibles();
	return true; 
}


function resetGridsTarifaAgrupadorTarifa(){
	eraseGridsTarifaAgrupadorTarifa();
	tarifaAgrupadorTarifaDisponiblesGrid = new dhtmlXGridObject("tarifaAgrupadorTarifaDisponiblesGrid");
	tarifaAgrupadorTarifaAsociadasGrid = new dhtmlXGridObject("tarifaAgrupadorTarifaAsociadasGrid");
}

function eraseGridsTarifaAgrupadorTarifa(){
	document.getElementById("tarifaAgrupadorTarifaDisponiblesGrid").innerHTML = '';
	document.getElementById("tarifaAgrupadorTarifaAsociadasGrid").innerHTML = '';
}

function removeGridsTarifaAgrupadorTarifa(){
	 resetGridsTarifaAgrupadorTarifa();
	 eraseGridsTarifaAgrupadorTarifa();
}

function getAccionTarifaAgrupadorTarifa(){
	if(dwr.util.getValue("claveNegocio")=='D'){
		iniciaGridsTarifaAgrupadorTarifa();
	}else{
//		getLineaNegocioTarifaAgrupadorTarifa($('idAgrupadorTarifaName'),$('idLineaNegocioName'),$('idMonedaName'));
		getMonedaTarifaAgrupadorTarifa('idAgrupadorTarifaName', $('idMonedaName') );
	}
}

function getLineaNegocioTarifaAgrupadorTarifa(agrupadorTarifaSelect, lineaNegocioSelect, monedaSelect) {
	var idAgrupadorTarifa =  dwr.util.getValue(agrupadorTarifaSelect);
	if (idAgrupadorTarifa != headerValue) {
		listadoService.getMapLineaNegocioPorAgrupadorTarifa(idAgrupadorTarifa, function(lineas) {
			addOptions(lineaNegocioSelect, lineas);	
			if (jQuery(lineaNegocioSelect).find("option").size()<2){
				alert("No hay l\u00EDneas de negocio asociadas")
			}
		});		
	} else {
		addOptions(lineaNegocioSelect, null);
	}	
	addOptions(monedaSelect,null);
	removeGridsTarifaAgrupadorTarifa();
}

function getMonedaTarifaAgrupadorTarifa(agrupadorTarifaSelect, monedaSelect ){
	var idAgrupadorTarifa =  dwr.util.getValue(agrupadorTarifaSelect);
	if (idAgrupadorTarifa != headerValue) {
			listadoService.getMapMonedaPorAgrupadorTarifa(idAgrupadorTarifa, function(monedas) {
			addOptions(monedaSelect, monedas);
			if (jQuery(monedaSelect).find("option:selected").val() == "" && jQuery(monedaSelect).find("option").size() < 2){
				alert("No hay Monedas asociadas")
			}
		});
	} else {
		addOptions(monedaSelect, null);
	}
	removeGridsTarifaAgrupadorTarifa();	
}



