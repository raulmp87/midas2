function mostrarListadoAgentes_SaldoMov(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}
function obtenerVentanaAgentes(){
	
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function onChangeAgente_SaldoMov(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgt_SaldoMov(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function loadInfoAgente(json){
	if(json.agente==null){
		alert("La clave del Agente no Existe ");//ó no esta Autorizado
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#fechaEstatus").val("");
		jQuery("#motivoEstatusAgente").val("");
		jQuery("#tipoSituacion").val("");
	}else{
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreCompleto = agente.persona.nombreCompleto;
		var tipoSituacion = agente.tipoSituacion.valor;
		var motivoEstatus = agente.descripMotivoEstatusAgente;
		var fechaAlta= agente.fechaAltaString;
		
		jQuery("#nombreCompleto").val(nombreCompleto);
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#fechaEstatus").val(fechaAlta);
		jQuery("#motivoEstatusAgente").val(motivoEstatus);
		jQuery("#tipoSituacion").val(tipoSituacion);
	}	
	
}


function mostrarDetalleMovimiento(idMovimiento, anioMes)
{	
	var url = verDetalleMovimientoPath + "?filtroMovimientosDelSaldo.id=" + idMovimiento + "&filtroMovimientosDelSaldo.anioMes=" + anioMes;
	mostrarVentanaModal("DetMovGrid", 'Detalle de Movimientos', 100, 200, 910, 560, url);
}
