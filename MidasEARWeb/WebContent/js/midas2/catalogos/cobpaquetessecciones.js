/**
 * Relaciones de paquete
 */
var coberturaSeccionAsociadasGrid;
var coberturaSeccionDisponiblesGrid;	
var coberturaSeccionProcessor;

/**
 * CoberturasSeccion
 */

function obtenerCoberturaSeccionAsociadas(){
	coberturaSeccionAsociadasGrid = new dhtmlXGridObject('coberturaSeccionAsociadasGrid');
	
	coberturaSeccionAsociadasGrid.load(obtenerCoberturaSeccionAsociadasPath + "?cobPaquetesSeccion.id.idToSeccion=" + dwr.util.getValue("idToSeccionName")+"&cobPaquetesSeccion.id.idPaquete=" + dwr.util.getValue("idPaqueteName"));
	
	//Creacion del DataProcessor
	coberturaSeccionProcessor = new dataProcessor(accionSobreCoberturaSeccionAsociadasPath + "?cobPaquetesSeccion.id.idPaquete=" + dwr.util.getValue("idPaqueteName"));

	coberturaSeccionProcessor.enableDataNames(true);
	coberturaSeccionProcessor.setTransactionMode("POST");
	coberturaSeccionProcessor.setUpdateMode("cell");
	
	coberturaSeccionProcessor.attachEvent("onAfterUpdate",refrescarGridsCoberturaSeccion);
	
	coberturaSeccionProcessor.init(coberturaSeccionAsociadasGrid);
}


function obtenerCoberturaSeccionDisponibles(){
	
	coberturaSeccionDisponiblesGrid = new dhtmlXGridObject('coberturaSeccionDisponiblesGrid');
	
	coberturaSeccionDisponiblesGrid.load(obtenerCoberturaSeccionDisponiblesPath +  "?cobPaquetesSeccion.id.idToSeccion=" + dwr.util.getValue("idToSeccionName")+"&cobPaquetesSeccion.id.idPaquete=" + dwr.util.getValue("idPaqueteName"));
}

function iniciaGridsCoberturaSeccion() {
	refrescarGridsCoberturaSeccion(null,null,null,null);
}


function refrescarGridsCoberturaSeccion(sid,action,tid,node){

	obtenerCoberturaSeccionAsociadas();
	obtenerCoberturaSeccionDisponibles();
	return true; 
}


function getPaquetes(lineaNegocioSelect, paqueteSelect) {
	var idToSeccion =  dwr.util.getValue(lineaNegocioSelect);
	if (idToSeccion != headerValue) {
		listadoService.getMapPaquetesPorSeccion(idToSeccion, function(paquetes) {
			addOptions(paqueteSelect, paquetes);
		});
	} else {
		addOptions(paqueteSelect, null);
	}
}



