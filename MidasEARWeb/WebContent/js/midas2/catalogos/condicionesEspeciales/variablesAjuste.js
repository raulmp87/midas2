var varAsociadasGrid;
var varDisponiblesGrid;
var calendars = {};

function obtenerVariablesAsociadas(){
	document.getElementById("varAjustesAsociadas").innerHTML = '';
	varAsociadasGrid = new dhtmlXGridObject('varAjustesAsociadas');
	varAsociadasGrid.load( obtenerVariablesAsociadasPath + "?" + jQuery(document.variableAjusteForm).serialize());
	varAsociadasGrid.attachEvent("onRowSelect", 
			function(id,ind){
				limpiarDivValores();
				var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
				var consulta = jQuery('#consulta').val();
				sendRequestJQ(null, mostrarValorVariablePath + "?"  + "idCondicionEspecial=" + idCondicionEspecial 
						+ "&idVariableAjuste=" + id+ "&consulta=" + consulta,  'valorVariable', null);	
		 		return true;
	});	
	
	dataProcessorVariables = new dataProcessor(accionVariableAjustePath + "?" + jQuery(document.variableAjusteForm).serialize());
	dataProcessorVariables.enableDataNames(true);
	dataProcessorVariables.setTransactionMode("POST");
	dataProcessorVariables.setUpdateMode("cell");
	dataProcessorVariables.attachEvent("onAfterUpdate", iniciaGridsVariables);
	dataProcessorVariables.init(varAsociadasGrid);
}


function obtenerVariablesDisponibles(){
	document.getElementById("varAjustesDisponibles").innerHTML = '';
	varDisponiblesGrid = new dhtmlXGridObject('varAjustesDisponibles');	
	varDisponiblesGrid.load(obtenerVariablesDisponiblesPath + "?" + jQuery(document.variableAjusteForm).serialize());
	
}

function iniciaGridsVariables() {
	obtenerVariablesAsociadas();
	obtenerVariablesDisponibles();
	limpiarDivValores();
}


function handleCalendar(id) {
	var calendar =  new dhtmlxCalendarObject(id, true);
	var split = id.split('_');
	//var idCalendario = id.substring(id.indexOf('_') + 1 , id.length);
	
	var variableAjuste = split[1];
	var idCalendario = split[2];

	
	if (calendars[idCalendario] == null) {
		calendars[idCalendario] = calendar;
	}
	
	calendar.setDateFormat("%d/%m/%Y");
	
	calendar.attachEvent("onClick",function(fecha){
		var valor = calendar.getFormatedDate("%d/%m/%Y",fecha);
		jQuery('#' + id).value= valor;
 		actualizaValorVariable(variableAjuste, idCalendario, valor);
	});
	
	calendar.hide();

}


function showCalendar(idCalendario){
	
	if (calendars[idCalendario] != null) {
		var calendar = calendars[idCalendario];
		if (calendar.isVisible()){
			calendar.hide();
		} else {
			calendar.show();
		}
	} else{
		handleCalendar();
	}
}

function handleTime(id) {
	try {
		jQuery('#' + id).mask('00:00');
	} catch(err) {		
	}	
}


function inicializarComponente() {

	var dateElements= document.getElementsByClassName('varajustedate');
	
	if (dateElements != null) {
		for (var i = 0; i < dateElements.length; ++i) {
		    var item = dateElements[i]; 
		    handleCalendar(item.id);
		}
	}
	
	var timeElements= document.getElementsByClassName('varajustetime');
	
	if (timeElements != null) {
		for (var i = 0; i < timeElements.length; ++i) {
		    var item = timeElements[i];  
		    handleTime(item.id);
		}
	}	
}

function limpiarDivValores() {
	document.getElementById("valorVariable").innerHTML = '';
}

function actualizaValorVariableTime(idVariableAjuste, idValor, valor) {
	if (valor != "" && (/^(?:[01]\d|2[0-3]):(?:[0-5]\d)$/.test(valor))) {
		actualizaValorVariable(idVariableAjuste, idValor.substring(idValor.indexOf('_') + 1 , idValor.length), valor);
	} else {
		jQuery('#' + idValor).val(null);
		return false;
	}
}

function actualizaValorVariable(idVariableAjuste, idValor, valor) {	
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	
	sendRequestJQ(null, actualizarValorVariablePath + "?" + "idCondicionEspecial=" + idCondicionEspecial 
			+ "&idVariableAjuste=" + idVariableAjuste
			+ "&idValorVariableAjuste=" + idValor + "&valorAjuste=" + valor, 'valorVariable', null);	
}

function buscarVariableAjuste(){
	var variableAjuste = jQuery("#nombreVariable").val();
	var url = obtenerVariablesDisponiblesPath + "?nombreVariable=" + variableAjuste;	
	varDisponiblesGrid.load(url);	
}

function asociarTodas() {	
	sendRequestJQ(null, asociarTodasVariablesAjustePath + "?" + jQuery(document.variableAjusteForm).serialize(), 'contenido_variables', null);	
	
}

function borrarAsociadas() {
	sendRequestJQ(null, eliminarTodasVariablesAjustePath + "?" + jQuery(document.variableAjusteForm).serialize(), 'contenido_variables', null);
}

function salir() {
	sendRequestJQ(null, salirPath , targetWorkArea, null);		
}