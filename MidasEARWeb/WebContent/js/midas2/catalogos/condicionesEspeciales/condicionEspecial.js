/**
 * 
 */

function guardarCondicion() {
	var nombre = jQuery('#condicion_nombre').val();
	var descripcion = jQuery('#condicion_descripcion').val();
	var estatus = jQuery('#condicion_estatus').val();
	
	if (estatus == '0' ) {
		if (nombre == '') {
			mostrarMensajeInformativo('Favor de ingresar el nombre de la condición',"20");
		} else {
			var formParams = jQuery(document.condicionEspecialForm).serialize();
			sendRequestJQ(null, guardarCondicionPath + '?' + formParams, 'contenido', null);		
		}
	} else if (nombre == '' || descripcion == '' ) {
		mostrarMensajeInformativo('Favor de ingresar el nombre y descripción de la condición',"20");
	} else {
		mostrarMensajeConfirm('Se cambiará el estatus de la condición a ACTIVO, ¿Desea continuar?', '20', 
				'guardarCondicionConfirm()', null, null);
	}
}

function guardarCondicionConfirm() {
	var formParams = jQuery(document.condicionEspecialForm).serialize();
	sendRequestJQ(null, guardarCondicionPath + '?' + formParams, targetWorkArea, null);
}

function confirmGuardar() {
	var nombre = jQuery('#condicion_nombre').val();
	var descripcion = jQuery('#condicion_descripcion').val();
	var estatus = jQuery('#condicion_estatus').val();
	
	if (estatus == '0' ) {
		if (nombre == '') {
			mostrarMensajeInformativo('Favor de ingresar el nombre de la condición',"20");
			return false;
		} else {
			return true;	
		}
	} else if (nombre == '' || descripcion == '' ) {
		mostrarMensajeInformativo('Favor de ingresar el nombre y descripción de la condición',"20");
		return false;
	} else {
		if(confirm('Se cambiará el estatus de la condición a ACTIVO, ¿Desea continuar?')){
			return true;
		} else {
			return false;
		}
		mostrarMensajeConfirm('Se cambiará el estatus de la condición a ACTIVO, ¿Desea continuar?', '20', 
				'guardarCondicionConfirm()', null, null);
	}	
}

function salir() {
	sendRequestJQ(null, salirPath , targetWorkArea, null);		
}

function verTab(idTab) {
	limpiarDivsGeneral();	
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	var consulta = jQuery('#consulta').val();	
	var path;
	var contenedor;
	
	if(idTab == 1) {
		path = condicionPath;
		contenedor = 'contenido_condicion';
	} else if(idTab == 2) {
		path = clasificacionPath;
		contenedor = 'contenido_clasificacion';
	} else if(idTab == 3) {
		path = variablesPath;
		contenedor = 'contenido_variables';
	}	
	
	var params= '?idCondicionEspecial=' + idCondicionEspecial
	
	if (consulta != null) {
		params = params + '&consulta=' + consulta;
	}		
	
	sendRequestJQ(null, path + params, contenedor, null);
}


function limpiarDivsGeneral() {
	limpiarDiv('contenido_condicion');
	limpiarDiv('contenido_clasificacion');
	limpiarDiv('contenido_variables');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function imprimirCondicion(){
	var idCondicionEspecial = jQuery("#idCondicionEspecial").val();
	var url="/MidasWeb/catalogos/condicionespecial/imprimirCondicion.action?idCondicionEspecial="+idCondicionEspecial;
	window.open(url, "CondicionEspecial");
}

/*
 * ADJUNTAR ARCHIVOS CONDICIONES ESPECIALES
 */

function mostrarAdjuntarArchivos(idCondicionEspecial, consulta){
	if(idCondicionEspecial === null ||  idCondicionEspecial === ''){
		mostrarMensajeInformativo('Primero debe guardar una condición especial', '10');
	}else{
		var formParams = 'idCondicionEspecial='+idCondicionEspecial+'&consulta='+consulta;
		sendRequestJQ(null, mostrarAdjuntarArchivosPath + '?' + formParams, targetWorkArea, null);
	}
}

function iniciaArchivosAdjuntosGrid(){
	var idCondicionEspecial = jQuery("#idCondicionEspecial").val();
	document.getElementById("archivosAdjuntosGrid").innerHTML = '';
	cargaMasivasGrid = new dhtmlXGridObject("archivosAdjuntosGrid");
	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasGrid.load("/MidasWeb/catalogos/condicionespecial/catalogo/listarArchivosAdjuntos.action?idCondicionEspecial="+ idCondicionEspecial);
}

function regresarAdjuntarArchivos(idCondicionEspecial, consulta){
	var formParams = 'idCondicionEspecial='+idCondicionEspecial+'&consulta='+consulta;
	sendRequestJQ(null, mostrarCatalogoPath + '?' + formParams, targetWorkArea, null);	
}

function descargarArchivoAdjunto(idToControlArchivo) {	
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function confirmaEliminacionArchivoAdjunto(idToControlArchivo){
	mostrarMensajeConfirm('¿Está seguro que desea eliminar el archivo adjunto?', '20', 
			'eliminarArchivoAdjunto('+idToControlArchivo+')', null, null);
}

function eliminarArchivoAdjunto(idToControlArchivo){
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	var consulta = jQuery('#consulta').val();
	var formParams = 'idToControlArchivo=' + idToControlArchivo + '&idCondicionEspecial=' + idCondicionEspecial + '&consulta='+consulta;
	sendRequestJQ(null, eliminarCatalogoPath + '?' + formParams, targetWorkArea, null);	
}

function anexarArchivoAdjunto(){
	var idCondicionEspecial = jQuery("#idCondicionEspecial").val();
	var consulta = jQuery("#consulta").val();
	var parameters = 'idCondicionEspecial=' + idCondicionEspecial + '&consulta=' + consulta;
	mostrarVentanaVault('/MidasWeb/catalogos/condicionespecial/catalogo/cargarArchivoAdjunto.action', 
						parameters, targetWorkArea, '30', null, null);	
}


/*
 * FIN ADJUNTAR ARCHIVOS CONDICIONES ESPECIALES
 */

function mostrarCopiarCondicion(idCondicionEspecial, muestraCondiciones){
	var url="/MidasWeb/catalogos/condicionespecial/mostrarCopiarCondEspecial.action?idCondicionEspecial="+idCondicionEspecial+"&muestraCondiciones="+muestraCondiciones;
	mostrarVentanaModal("confCopiarCondicionEspecial", 'Confirmación Copia de Condición Especial', 100, 200, 500, 300, url);    
}
