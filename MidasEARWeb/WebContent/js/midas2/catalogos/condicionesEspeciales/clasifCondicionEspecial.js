var coberturasDisponiblesTree;
var coberturasAsociadasTree;
var consulta;

function iniciaGrids() {
	document.getElementById("coberturasAsociadasTree").innerHTML = '';
	document.getElementById("coberturasDisponiblesTree").innerHTML = '';
	consulta = jQuery('#consulta').val();
	obtenerCoberturasAsociadas();
	obtenerCoberturasDisponibles();
}

function obtenerCoberturasAsociadas(){
	document.getElementById("coberturasAsociadasTree").innerHTML = '';
	coberturasAsociadasTree = new dhtmlXTreeObject('coberturasAsociadasTree', "100%","100%", 0);
	coberturasAsociadasTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	coberturasAsociadasTree.enableTreeImages(false);	
	coberturasAsociadasTree.enableSmartXMLParsing(true);
	
	if (consulta == 'false') {
		coberturasAsociadasTree.enableDragAndDrop(true);
	
		coberturasAsociadasTree.attachEvent("onDrop", 
				function(sId,tId,id,sObject,tObject){
				var path = null;
				if (sId.substring(0, 3) == 'SEC') {
					var idSeccion = sId.substring(4, sId.length);
					
					if (idSeccion.indexOf('_') > -1) {
						idSeccion = idSeccion.substring(0, idSeccion.indexOf('_') );
					}
					
					var path = asociarCoberturaPath  + '?' +  jQuery(document.clasifCondicionEspecialForm).serialize();
					path = path + "&accion=asociaseccion"
					+ "&idSeccionCobertura=" + idSeccion;
				} else if (sId.substring(0, 3) == 'COB') {
					
					var idx = sId.indexOf("-SEC");
					var idCobertura = sId.substring(4, idx);
					var idSeccion = sId.substring(idx + 5, sId.length);
					var path = asociarCoberturaPath + '?' + jQuery(document.clasifCondicionEspecialForm).serialize();
					path = path + "&accion=asociacobertura"
					+ "&idCobertura=" + idCobertura + "&idSeccionCobertura=" + idSeccion;
				}				
				try {
					coberturasAsociadasTree.loadXML(path, iniciaGrids);		
				} catch(err) {
					iniciaGrids();
				}						
				return true;})
	}
	coberturasAsociadasTree.loadXML(obtenerAsociadasPath + '?' + jQuery(document.clasifCondicionEspecialForm).serialize());
}

function obtenerCoberturasDisponibles(){
	document.getElementById("coberturasDisponiblesTree").innerHTML = '';
	coberturasDisponiblesTree = new dhtmlXTreeObject('coberturasDisponiblesTree', "100%","100%", 0);
	coberturasDisponiblesTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	coberturasDisponiblesTree.enableTreeImages(false);	
	coberturasDisponiblesTree.enableSmartXMLParsing(true);
	
	if (consulta == 'false') {
		coberturasDisponiblesTree.enableDragAndDrop(true);
	
		coberturasDisponiblesTree.attachEvent("onDrop", 
				function(sId,tId,id,sObject,tObject){
					var path = null;
					if (sId.substring(0, 3) == 'SEC') {
						var idSeccion = sId.substring(4, sId.length);
						
						if (idSeccion.indexOf('_') > -1) {
							idSeccion = idSeccion.substring(0, idSeccion.indexOf('_') );
						}
						var path = eliminarCoberturaPath + '?' + jQuery(document.clasifCondicionEspecialForm).serialize();
						path = path + "&accion=eliminaseccion"
						+ "&idSeccionCobertura=" + idSeccion;
					} else if (sId.substring(0, 3) == 'COB') {					
						var idx = sId.indexOf("-SEC");
						var idCobertura = sId.substring(4, idx);
						var idSeccion = sId.substring(idx + 5, sId.length);
						var path = eliminarCoberturaPath + '?' + jQuery(document.clasifCondicionEspecialForm).serialize();
						path = path + "&accion=eliminacobertura"
						+ "&idCobertura=" + idCobertura + "&idSeccionCobertura=" + idSeccion;
					}					
					coberturasDisponiblesTree.loadXML(path, iniciaGrids );		
					
				return true;})
	}
	coberturasDisponiblesTree.loadXML(obtenerDisponiblesPath + '?' + jQuery(document.clasifCondicionEspecialForm).serialize());
}


function changeTabArea(idAreaImpacto) {
	document.getElementById("factores").innerHTML = '';
	document.getElementById("valorFactor").innerHTML = '';

	if (jQuery('#areaImpacto_' + idAreaImpacto).is(':checked')) {
		
		changeLinkColor('linkArea', idAreaImpacto);
		
		sendRequestJQ(null, obtenerFactoresPath + "?" + jQuery(document.clasifCondicionEspecialForm).serialize() 
				+ "&idAreaImpacto=" + idAreaImpacto , 'factores', null);
	}
	
}

function guardarAreaCondicion(idAreaImpacto) {
	document.getElementById("factores").innerHTML = '';
	document.getElementById("valorFactor").innerHTML = '';
	
	var checked = jQuery('#' + idAreaImpacto).is(':checked');
	idAreaImpacto = idAreaImpacto.substring(idAreaImpacto.indexOf('_') + 1 , idAreaImpacto.length);
	sendRequestJQ(null, guardarAreaPath + "?" + jQuery(document.clasifCondicionEspecialForm).serialize() 
			+ "&idAreaImpacto=" + idAreaImpacto + "&seleccionArea=" + checked, 'areasImpacto', null);
}

function guardarFactor(idAreaImpacto, idFactor) {
	document.getElementById("valorFactor").innerHTML = '';
	
	var checked = jQuery('#' + idFactor).is(':checked');
	idFactor = idFactor.substring(idFactor.indexOf('_') + 1 , idFactor.length);
	
	sendRequestJQ(null, guardarFactoresPath + "?" 
			+ jQuery(document.clasifCondicionEspecialForm).serialize() 
			+ "&idAreaImpacto=" + idAreaImpacto + "&idFactor=" + idFactor 
			+ "&seleccionFactor=" + checked, 'factores', null);
}

function mostrarValorFactor(idAreaImpacto, idFactor) {
	
	if (jQuery('#factor_' + idFactor).is(':checked')) {
		changeLinkColor('linkFactor', idFactor);
		
		sendRequestJQ(null, mostrarValorFactorPath + "?" + jQuery(document.clasifCondicionEspecialForm).serialize() 
				+ "&idAreaImpacto=" + idAreaImpacto + "&idFactor=" + idFactor , 'valorFactor', null);
	} else {
		return false;
	}	
}

function actualizaValorFactor(idAreaImpacto, idFactor, valor) {
	
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	
	sendRequestJQ(null, actualizaValorFactorPath + "?"
			+ "idCondicionEspecial=" + idCondicionEspecial + "&idAreaImpacto=" + idAreaImpacto 
			+ "&idFactor=" + idFactor 
			+ "&valorFactor=" + valor, 'valorFactor', null);	
}

function guardarNivelImpacto(value) {
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	sendRequestJQ(null, guardarNivelImpactoPath + "?"
			+ "idCondicionEspecial=" + idCondicionEspecial + "&condicion.nivelAplicacion=" + value , 'contenedorNivel', null);	
}

function guardarVIP(check) {
	var idCondicionEspecial = jQuery('#idCondicionEspecial').val();
	sendRequestJQ(null, guardarVIPPath + "?"
			+ "idCondicionEspecial=" + idCondicionEspecial + "&condicion.vip=" + check.checked , 'contenedorNivel', null);	
}

function salir() {
	sendRequestJQ(null, salirPath , targetWorkArea, null);		
}

function changeLinkColor(linkClass, id) {
	jQuery('.' + linkClass).each(function() {
		
		if (jQuery(this).attr('id') == linkClass + '_' + id) {
			jQuery(this).hover(
					  function () {
						  changeFont(this, "#a0ffa0", "bold", "11px");
					  }, 
					  function () {
						  changeFont(this, "#229A4C", "bold", "11px");
					  }
					);
		} else {
			changeFont(this, "#00a000", "normal", "9px");
			jQuery(this).hover(
					  function () {
						  changeFont(this, "#a0ffa0", "normal", "9px");
					  }, 
					  function () {
						  changeFont(this, "#00a000", "normal", "9px");
					  }
					);
		}
		
	});
	
	function changeFont(component, color, weight, size) {
		jQuery(component).css("color", color);
		jQuery(component).css("font-weight", weight);
		jQuery(component).css("font-size", size);
		
	}
}