/**
 * 
 */
function inicializarCondicionListado(){	
	mostrarListadoCondiciones();
}
function verTabCondiciones() {
	limpiarDivsGeneral();	
	sendRequestJQ(null, condicionesPath, 'contenido_condiciones',null);
}

function verTabRanking() {
	limpiarDivsGeneral();	
	sendRequestJQ(null, rankingPath, 'contenido_ranking',null);
}

function mostrarListadoCondiciones(idCondicionEspecial){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('listadoCondiciones');

	listadoGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		unblockPage();
    });	
	
	var formParams = null;
	
	if (idCondicionEspecial == null) {		
		formParams = jQuery(document.condicionEspecialForm).serialize();
	} else {
		
		formParams = "condicionEspecialFiltro.condicion.id=" + idCondicionEspecial + '&tipoVista=' + jQuery('#tipoVista').val();
	}	 
	
	listadoGrid.load(buscarCondicionesPath + '?' + formParams);
}

function crearCondicion() {
	sendRequestJQ(null, mostrarCatalogoPath, targetWorkArea, null);
}


function limpiarFiltros(){
	jQuery('#condicionEspecialForm').each (function(){
		  this.reset();
	});
}

function modificarEstatus(idCondicion, estatus) {
	
	var pregunta = "";
	if(estatus == 0 || estatus == 2){
		mostrarMensajeConfirm('Se cambiar\u00e1  el estatus de la Condicion Especial a ACTIVO. \u00BFDesea continuar\u003F ', '20', 
				'modificarEstatusConfirm(' + idCondicion + ',' + 1 + ')', null, null);

	}else if(estatus == 1){
		mostrarMensajeConfirm('Se cambiar\u00e1  el estatus de la Condicion Especial a INACTIVO. \u00BFDesea continuar\u003F ', '20', 
				'modificarEstatusConfirm(' + idCondicion + ',' + 2 + ')', null, null);
	}
}

function modificarEstatusConfirm(idCondicion, estatus) {
	sendRequestJQ(null, modificarEstatusPath + '?' + jQuery(document.condicionEspecialForm).serialize() 
				+ '&idCondicionEspecial=' + idCondicion + '&estatus=' + estatus, targetWorkArea, null);
}

function consultarCondicion(idCondicionEspecial) {
	sendRequestJQ(null, mostrarCatalogoPath + '?idCondicionEspecial=' + idCondicionEspecial + '&consulta=true', targetWorkArea, null);
}

function editarCondicion(idCondicionEspecial) {
	sendRequestJQ(null, mostrarCatalogoPath + '?idCondicionEspecial=' + idCondicionEspecial, targetWorkArea, null);
}

function mostrarAdjuntarArchivos(idCondicionEspecial){	
		var formParams = 'idCondicionEspecial='+idCondicionEspecial+'&consulta='+false;
		sendRequestJQ(null, mostrarAdjuntarArchivosPath + '?' + formParams, targetWorkArea, null);	
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_condiciones');
	limpiarDiv('contenido_ranking');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function mostrarCondicionesEspeciales() {
	sendRequestJQ(null, salirPath , targetWorkArea, null);		
}

function mostrarCopiarCondicionFromList(idCondicionEspecial, muestraCondiciones) {
	var url="/MidasWeb/catalogos/condicionespecial/mostrarCopiarCondEspecial.action?idCondicionEspecial="+idCondicionEspecial+"&muestraCondiciones="+muestraCondiciones;
	mostrarVentanaModal("confCopiarCondicionEspecial", 'Confirmación Copia de Condición Especial', 100, 200, 500, 300, url);
}

function imprimirCondicion(idCondicionEspecial){
	var url="/MidasWeb/catalogos/condicionespecial/imprimirCondicion.action?idCondicionEspecial="+idCondicionEspecial;
	window.open(url, "CondicionEspecial");
}
