/****Catalogo de domicilios para agentes y generico***********************/
var domicilioGrid;

function listarFiltradoDomicilio(){	
	domicilioGrid = new dhtmlXGridObject("domicilioGrid");
	var form = document.domicilioForm;
	if(form!=null){
		domicilioGrid.load(listarFiltradoDomicilioPath+"?"+jQuery("#domicilioForm").serialize());
	}else{
		domicilioGrid.load(listarFiltradoDomicilioPath);
	}
}

function agregarDomicilio(tipoAccion) {
	sendRequestJQ(null, verDetalleDomicilioPath + "?tipoAccion=" + tipoAccion, targetWorkArea, 'null');	
	
}

function guardarDomicilio() {
	if(validateAll(true)){
		sendRequestJQ(null, "/MidasWeb/catalogos/domicilio/guardarDomicilio.action?"
			+ jQuery("#domicilioForm").serialize(),
			targetWorkArea, 'null');
	}
}
function eliminarDomicilio(){

	if(confirm("Esta seguro de Eliminar el Registo")){
		sendRequestJQ(null, "/MidasWeb/catalogos/domicilio/eliminarDomicilio.action?"
			+ jQuery("#domicilioForm").serialize(),
			targetWorkArea, 'null')
	}
}
function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/catalogos/domicilio/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	mostrarModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
}

//function obtenerHistorico(){
//	var url = '/MidasWeb/catalogos/domicilio/loadHistory.action?idTipoOperacion='+100+ "&idRegistro="+dwr.util.getValue("domicilio.idDomicilio");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}

function salirDeDomicilio(){

	var url = initContenedorDomicilioPath;
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}
	}
}