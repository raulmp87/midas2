/**
 * 
 */
var programacionBonoGrid;
var monitorBonoGrid;
var programacionBonoProcessor;

function onchangeTipoBono(){
	var tipoBono = jQuery("#tipoBono").val();

	var url ="/MidasWeb/fuerzaventa/programacionBono/listarNegociosEspeciales.action?idTipoBono="+tipoBono;
	var url2 ="/MidasWeb/fuerzaventa/programacionBono/listarConfiguracionBono.action?idTipoBono="+tipoBono;
	var url3 ="/MidasWeb/fuerzaventa/programacionBono/obtenerProgramados.action?idTipoBono="+tipoBono;
	
	jQuery.asyncPostJSON(url,null,responseNegocioEspecial);
	jQuery.asyncPostJSON(url2,null,responseConfiguracionBono);
	jQuery.asyncPostJSON(url3,null,responseObtenerProgramados);
}

function responseNegocioEspecial(json){
	comboChecksCreate(json.listaNegociosEspeciales,"ajax_listaNegociosEspeciales","negociosEspecialesList",null,"idToNegocio");	
}

function responseConfiguracionBono(json){
	comboChecksCreate(json.listaConfiguracionBonos,"ajax_listaConfiguracionBonos","configuracionBonoList",null,"id");	
}

function responseObtenerProgramados(json){
	jQuery("#bonosProgramados").html(json.bonosProgramados);
	jQuery("#bonosPorProgramar").html(json.bonosPorProgramar);
}

function programarBono(){
	var hora = jQuery("#hora").val();
	if(hora < 0 || hora > 24){
		mostrarVentanaMensaje('10', "Horario debe estar entre 0 y 23.");
		return;
	}
	var path = "/MidasWeb/fuerzaventa/programacionBono/guardarProgramacionBono.action";
	if(validateForId('programacionBonosForm',true)){
		sendRequestJQ(jQuery(document.programacionBonosForm), path, "contenido", null);
	}
}

function cargaProgramacionBonoGrid(){
	var url="/MidasWeb/fuerzaventa/programacionBono/listar.action";
	document.getElementById('programacionBonoGrid').innerHTML = '';
	programacionBonoGrid = new dhtmlXGridObject('programacionBonoGrid');
	programacionBonoGrid.load(url);
	
	programacionBonoProcessor = new dataProcessor("/MidasWeb/fuerzaventa/programacionBono/activarProgramacionBono.action");
	
	programacionBonoProcessor.enableDataNames(true);
	programacionBonoProcessor.setTransactionMode("POST");
	programacionBonoProcessor.setUpdateMode("cell");
	programacionBonoProcessor.attachEvent("onAfterUpdate",cargaProgramacionBonoGrid);
	programacionBonoProcessor.init(programacionBonoGrid);
}

function changeModoEjecucion(){
	var value = jQuery("input[name$='modoEjecucion']:checked").val();
	if(value == 1){
		jQuery("#manualButton").show();
		jQuery("#automaticoButton").hide();
		jQuery("#periodoEjecucion").attr("disabled",true);
		jQuery("#fechaEjecucion").attr("disabled",true);
		jQuery("#hora").attr("disabled",true);
		//Se quita la validacion de los campos obligatorios.
		jQuery("#periodoEjecucion").removeClass("jQrequired");
		jQuery("#fechaEjecucion").removeClass("jQrequired");
		jQuery("#hora").removeClass("jQrequired");
	}
	if(value == 2){
		jQuery("#manualButton").hide();
		jQuery("#automaticoButton").show();
		jQuery("#periodoEjecucion").attr("disabled",false);
		jQuery("#fechaEjecucion").attr("disabled",false);
		jQuery("#hora").attr("disabled",false);
		//Se agrega la validacion de los campos obligatorios.
		jQuery("#periodoEjecucion").addClass("jQrequired");
		jQuery("#fechaEjecucion").addClass("jQrequired");
		jQuery("#hora").addClass("jQrequired");
	}
}

function ejecutarCalculo(){
	var path = "/MidasWeb/fuerzaventa/programacionBono/calculoBono.action";
	sendRequestJQ(jQuery(document.programacionBonosForm), path, "contenido", null);
}

function eliminarProgramacion(id){
	if (confirm('\u00BFEst\u00e1 seguro que desea Eliminar la programaci\u00F3n?')) {
		var path = "/MidasWeb/fuerzaventa/programacionBono/eliminarProgramacionBono.action?programacionBono.id="+id;
		sendRequestJQ(null, path, "contenido", null);
	}
}

function habilitarBusquedaPorDescripcion()
{
	if(document.getElementById("ajax_listaConfiguracionBonos").getElementsByTagName("li").length > 0 || 
			document.getElementById("ajax_listaNegociosEspeciales").getElementsByTagName("li").length > 0)
	{		
		document.getElementById('txtDescBonoBusqueda').disabled = false;		
	}
	else
	{
		document.getElementById('txtDescBonoBusqueda').value = '';
		document.getElementById('txtDescBonoBusqueda').disabled = true;		
	}
}

function filtrarBonosPorDescripcion(descripcionBono)
{	
	var tipoTexto = document.body.textContent?true:false;//Fix: inconsistencia entre navegadores Firefox e IE
	
	var ulBonos = document.getElementById("ajax_listaConfiguracionBonos");
	var ulNegociosEspeciales = document.getElementById("ajax_listaNegociosEspeciales");	
	var filtro = descripcionBono.toUpperCase();
	
	var items = ulBonos.getElementsByTagName("li");
	if(items.length>0){
		deselectAllChecks('ajax_listaConfiguracionBonos');
	}
	for (var i = 0; i < items.length; ++i) {
		
		//Fix: inconsistencia entre navegadores Firefox e IE
		var name;
		
		if(tipoTexto == true)
		{
			name = items[i].childNodes[0].textContent;			
		}else
		{
			name = items[i].childNodes[0].innerText;			
		}		
		
        if (name.toUpperCase().indexOf(filtro)!== -1) 
        	items[i].style.display = 'list-item';
        else
        	items[i].style.display = 'none';		
	}
	
	var itemsNegEspeciales = ulNegociosEspeciales.getElementsByTagName("li");	
	if(itemsNegEspeciales.length>0){
		deselectAllChecks('ajax_listaNegociosEspeciales');
	}
	for (var i = 0; i < itemsNegEspeciales.length; ++i) {
		
		//Fix: inconsistencia entre navegadores Firefox e IE
		var name;
		
		if(tipoTexto == true)
		{
			name = itemsNegEspeciales[i].childNodes[0].textContent;					
		}else
		{			
			name = itemsNegEspeciales[i].childNodes[0].innerText;			
		}		
		
        if (name.toUpperCase().indexOf(filtro)!== -1) 
        	itemsNegEspeciales[i].style.display = 'list-item';
        else
        	itemsNegEspeciales[i].style.display = 'none';		
	}	
}

function verMonitorDeCalculosBonos(){
	var url = "/MidasWeb/fuerzaventa/programacionBono/monitorCalculoBono.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function cargaMonitorBonoGrid(){
	var url="/MidasWeb/fuerzaventa/programacionBono/listarMonitorBonoGrid.action";
	document.getElementById('monitorBonoGrid').innerHTML = '';
	monitorBonoGrid = new dhtmlXGridObject('monitorBonoGrid');
	monitorBonoGrid.load(url);
	
}

function regresarAProgramacionBonos(){
	var url = "/MidasWeb/fuerzaventa/programacionBono/mostrar.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function selectAllChecksb(idUL)
{
	jQuery("#"+idUL +" input").each(function(index) {
		if( jQuery(this).is(":visible") ){
			jQuery(this).attr('checked',true);
		}
	});
}