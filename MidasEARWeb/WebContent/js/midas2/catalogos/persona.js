var personaGrid;
var historicoFuerzaVentaGrid;
var ventanaResponsable=null;
var TIPOACCION_CONSULTAHISTORICO = "5";
function listarFiltradoPersona(){
	personaGrid = new dhtmlXGridObject("personaGrid");
	var form = document.personaForm;
	if(form!=null){
		personaGrid.load(listarFiltradoPersonaPath+"?"+jQuery("#personaForm").serialize());
	}else{
		personaGrid.load(listarFiltradoPersonaPath);
	}
}

function agregarInciso(tipoAccion) {
	sendRequestJQ(null, verDetalleIncisoPath + "?tipoAccion=" + tipoAccion, targetWorkArea, 'null');	
}

function cargarDatosGralesPersona(){	
	if(dwr.util.getValue("personaSeycos.idPersona")!=null && dwr.util.getValue("personaSeycos.idPersona")!="" || parent.dwr.util.getValue("personaSeycos.idPersona")!=null && parent.dwr.util.getValue("personaSeycos.idPersona")!="")
	{
		var path= "/MidasWeb/fuerzaVenta/persona/mostrarPersonaDatosGrales.action?tipoAccion="+dwr.util.getValue("tipoAccion")+"&personaSeycos.idPersona="+dwr.util.getValue("personaSeycos.idPersona")+"&idTipoOperacion="+90+"&idRegistro="+dwr.util.getValue("personaSeycos.idPersona");

		if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
		{
			path = path + "&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
		}		
	}	
	else
	{
		var path= "/MidasWeb/fuerzaVenta/persona/mostrarPersonaDatosGrales.action?tipoAccion="+parent.dwr.util.getValue("tipoAccion");
	}
	
	sendRequestJQ(null,path,'contenido_datosGenerales','limpiarDivsGeneral();');
}

function cargarDatosFiscalesPersona(){	
	var path= "/MidasWeb/fuerzaVenta/persona/mostrarPersonaDatosFiscales.action?tipoAccion="+dwr.util.getValue("tipoAccion")+"&personaSeycos.idPersona="+dwr.util.getValue("personaSeycos.idPersona")+"&idTipoOperacion="+90+"&idRegistro="+dwr.util.getValue("personaSeycos.idPersona");	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		path = path + "&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
	}
	sendRequestJQ(null,path,'contenido_datosFiscales','limpiarDivsFiscal();');
}

function cargarDatosOficinaPersona(){		
	var path= "/MidasWeb/fuerzaVenta/persona/mostrarPersonaDatosOficina.action?tipoAccion="+dwr.util.getValue("tipoAccion")+"&personaSeycos.idPersona="+dwr.util.getValue("personaSeycos.idPersona")+"&idTipoOperacion="+90+"&idRegistro="+dwr.util.getValue("personaSeycos.idPersona");	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		path = path + "&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
	}
	sendRequestJQ(null,path,'contenido_datosOficina','limpiarDivsContacto();');	
}

function cargarDatosContactoPersona(){	
	var path= "/MidasWeb/fuerzaVenta/persona/mostrarPersonaDatosContacto.action?tipoAccion="+dwr.util.getValue("tipoAccion")+"&personaSeycos.idPersona="+dwr.util.getValue("personaSeycos.idPersona")+"&idTipoOperacion="+90+"&idRegistro="+dwr.util.getValue("personaSeycos.idPersona");	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		path = path + "&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
	}
	sendRequestJQ(null,path,'contenido_datosContacto','limpiarDivsOficina();');	
	
}

function cargarDatosPersona(tipoAccion,idPersona) {
	if(tipoAccion==''||tipoAccion==null)
		tipoAccion=parent.dwr.util.getValue("tipoAccion");
	if(idPersona!=null && idPersona!=""){
		var path= "/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion="+tipoAccion+"&personaSeycos.idPersona="+idPersona;
		sendRequestJQ(null, path, targetWorkArea, null);	
	}
	else{
		var path= "/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion="+tipoAccion;
		sendRequestJQ(null, path, targetWorkArea, null);	
	}
}
function validateAllTabsPers(){
	var url= "/MidasWeb/fuerzaVenta/persona/revisionPersonaCompleta.action";	
	var data={"personaSeycos.idPersona": dwr.util.getValue("personaSeycos.idPersona")};
	jQuery.asyncPostJSON(url,data,activaPersona);
}
function activaPersona(json){
	var esValido = json.isValidPersona;
	if(esValido == "ok"){				
		parent.mostrarMensajeConfirm("Los campos obligatorios est\u00E1n completos, \u00BFDesea activar el registro?","20","sendRequestActivaPersona()",null,null,null);
//		if(confirm("Los campos obligatorios est\u00E1n completos, \u00BFDesea activar registro?")){		
//			
//		}
	}else if(esValido == "nok"){
		//alert("Falta llenar informaci\u00F3n");
	}
}

function sendRequestActivaPersona(){
	var url= "/MidasWeb/fuerzaVenta/persona/activaPersonaCompleta.action";	
	var data={"personaSeycos.idPersona": dwr.util.getValue("personaSeycos.idPersona"),"tipoAccion":4};
	jQuery.asyncPostJSON(url,data,responseActivaPersona);
}

function responseActivaPersona(json){
//	alert(json.mensaje);
	var path=verDetallePersonaPath;
	path+="?tipoAccion=4";
	path+="&personaSeycos.idPersona="+dwr.util.getValue("personaSeycos.idPersona");
	path+="&idRegistro="+dwr.util.getValue("personaSeycos.idPersona");
	path+="&idTipoOperacion=90";
	sendRequestJQ(null, path, targetWorkArea, 'parent.mostrarMensajeInformativo("Acción realizada correctamente","30")');
}

function guardarDatosGeneralesPersona() {
	var rfcGenerated = "";
	var tipoPers=jQuery("#tipoPers").val();
	var rfcGenerado="";
	var rfcCapturado="";
	var txtRfcFecha="";
	var txtRfcSiglas="";
	var isAdministradorAgente = (jQuery("#isAdministradorAgente").val().toString()=='true')?1:0;
	
	//se valida el rfc
			if(tipoPers==1){
				var nombre =jQuery("#txtNombre").val();
				var apPaterno =jQuery("#txtApellidoPaterno").val();
				var apMaterno =jQuery("#txtApellidoMaterno").val();
				var fecha =jQuery("#fecha").val();
				txtRfcFecha=jQuery("#txtRfcFecha").val();
				txtRfcSiglas=jQuery("#txtRfcSiglas").val();
				rfcGenerated = midasGenerateRFC(nombre, apPaterno, apMaterno, fecha);
				rfcGenerado=rfcGenerated.substring(0, 4)+rfcGenerated.substring(4, 10);
				rfcCapturado=txtRfcSiglas+txtRfcFecha;
				if(rfcGenerado!=rfcCapturado && isAdministradorAgente!=1){
					parent.mostrarMensajeInformativo('El Rfc no corresponde con la información capturada',"10");
					return false;
				}
			}
			if(tipoPers==2){
				var txtRazonSocial = jQuery("#txtRazonSocial").val();
				var fecha2 = jQuery("#fecha2").val();		
				txtRfcFecha=jQuery("#txtRfcFecha").val();
				txtRfcSiglas=jQuery("#txtRfcSiglas").val();
				rfcGenerated = calcularRFCMoral(txtRazonSocial,fecha2);
				rfcGenerado=rfcGenerated.substring(0, 4)+rfcGenerated.substring(4, 10);
				rfcCapturado=txtRfcSiglas+txtRfcFecha;
				if(rfcGenerado!=rfcCapturado && !isAdministradorAgente){
					parent.mostrarMensajeInformativo('El Rfc no corresponde con la información capturada',"10");
					return false;
				}
			}
	
	
	//Comentar para pruebas, descomentar para PRODUCCION.
	var curpValido=false;
	var estadoNacimiento = jQuery('#personaSeycos\\.claveEstadoNacimiento option:selected').text();
	
	/* Se agrega parche para enviar la descripcion DISTRITO FEDERAL al validador de CURP */
	if(estadoNacimiento == 'CIUDAD DE MEXICO')
	{		
		estadoNacimiento = 'DISTRITO FEDERAL';		
	}
	
	var curp = dwr.util.getValue("personaSeycos.codigoCURP");
	var fechaNac = dwr.util.getValue("personaSeycos.fechaNacimiento");
	var nombre = dwr.util.getValue("personaSeycos.nombre");
	var apellidoPat = dwr.util.getValue("personaSeycos.apellidoPaterno");
	var apellidoMat = dwr.util.getValue("personaSeycos.apellidoMaterno");
	var sexo = dwr.util.getValue("personaSeycos.sexo");	
	var tipoPersona = dwr.util.getValue("personaSeycos.claveTipoPersona");
	if(tipoPersona==1){//Si es persona fisica se valida el curp
//		alert(estadoNacimiento+" "+curp+" "+fechaNac+" "+nombre+" "+apellidoPat+" "+apellidoMat+" "+sexo);
		if(tipoPersona ==1 && estadoNacimiento!=null && estadoNacimiento!='' && curp!=null && curp!='' && fechaNac!=null && fechaNac!='' && nombre!=null && nombre!='' && apellidoPat!=null && apellidoPat!='' && apellidoMat!=null && apellidoMat!='' &&sexo!=null && sexo!=''){
			var url="/MidasWeb/fuerzaVenta/persona/validarCurpPersona.action";
			var params="?estadoNacimientoCurp="+estadoNacimiento;
			params+="&codigoCURP="+curp;
			params+="&fechaNacimientoCurp="+fechaNac;
			params+="&nombreCurp="+nombre;
			params+="&apellidoPaternoCurp="+apellidoPat;
			params+="&apellidoMaternoCurp="+apellidoMat;
			params+="&sexoCurp="+sexo;
			params+="&claveTipoPersonaCurp="+tipoPersona;
			url+=params;
			//TODO descomentar
			if(isAdministradorAgente!=1){
				validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
	//			jQuery.asyncGetJSON(url,null,function(data){
	//				var valido=(data!=null && data["curpValido"]!=null)?data["curpValido"]:"0";
					/*if("1"=="1"){ if(valido=="1")*/ if(data==true){// TODO descomentar
						if((esMayorEdadServidor(jQuery("#fecha").val())) && validateAll(true)){
							/*if(dwr.util.getValue("personaSeycos.idPersona")=='' || dwr.util.getValue("personaSeycos.idPersona")==null){
								var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
								sendRequestJQAsync(null, path, targetWorkArea, null);	
							}
							else{*/
							var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
							sendRequestJQAsync(null, path, targetWorkArea, validateAllTabsPers);
							//}
							//validateAllTabsPers();
						}
					}else{
	//					alert(curp);
						parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");
					}
				});
			}else{
				if((esMayorEdadServidor(jQuery("#fecha").val())) && validateAll(true)){
					/*if(dwr.util.getValue("personaSeycos.idPersona")=='' || dwr.util.getValue("personaSeycos.idPersona")==null){
						var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
						sendRequestJQAsync(null, path, targetWorkArea, null);	
					}
					else{*/
					var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
					sendRequestJQAsync(null, path, targetWorkArea, validateAllTabsPers);
					//}
					//validateAllTabsPers();
				}
			}
			
		}else{
			parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");
		}
	}else if(tipoPersona==2){
		if(validateAll(true)){
			/*if(dwr.util.getValue("personaSeycos.idPersona")=='' || dwr.util.getValue("personaSeycos.idPersona")==null){
				var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
				sendRequestJQAsync(null, path, targetWorkArea, null);	
			}
			else{*/
			var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosGeneralesPersona.action?"+jQuery("#personaDatosGeneralesForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
			sendRequestJQAsync(null, path, targetWorkArea, validateAllTabsPers);
			//}
			//validateAllTabsPers();
		}
	}
}

function guardarDatosFiscalesPersona() {
	if(validateAll(true)){
		var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosFiscalesPersona.action?"+jQuery("#personaDatosFiscalesForm").serialize()+ "&personaSeycos.idPersona=" + dwr.util.getValue("personaSeycos.idPersona")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		sendRequestJQAsync(null, path, targetWorkArea, validateAllTabsPers);
		//validateAllTabsPers();
	}
}

function guardarDatosOficinaPersona() {
	if(validateAll(true)){
	var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosOficinaPersona.action?"+jQuery("#personaDatosOficinaForm").serialize()+ "&personaSeycos.idPersona=" + dwr.util.getValue("personaSeycos.idPersona")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
	sendRequestJQAsync(null, path, targetWorkArea, validateAllTabsPers);
	//validateAllTabsPers();
	}
}

function eliminarPersona() {
	parent.mostrarMensajeConfirm("\u00BFEsta seguro de la baja de esta persona?","20","sendRequestJQAsync(null,'/MidasWeb/fuerzaVenta/persona/eliminarPersona.action?personaSeycos.idPersona='+dwr.util.getValue('personaSeycos.idPersona'),targetWorkArea,null)",null,null,null);
//	if(confirm("\u00BFEsta seguro de la baja de esta persona?")){
//	var path= "/MidasWeb/fuerzaVenta/persona/eliminarPersona.action?personaSeycos.idPersona=" + dwr.util.getValue("personaSeycos.idPersona");
//	sendRequestJQAsync(null, path, targetWorkArea, null);
//	}
}

function guardarDatosContactoPersona() {
	if(validateAll(true)){
	var path= "/MidasWeb/fuerzaVenta/persona/guardarDatosContactoPersona.action?"+jQuery("#personaDatosContactoForm").serialize()+ "&personaSeycos.idPersona=" + dwr.util.getValue("personaSeycos.idPersona")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
	sendRequestJQ(null, path, targetWorkArea, 'validateAllTabsPers();parent.mostrarMensajeInformativo("Acción realizada correctamente","30")');
	//validateAllTabsPers();
	}
}

function salirTabsPersona(){
	var path= "/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, path, targetWorkArea, null);
	}else{		
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQAsync(null,'/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action',targetWorkArea,null)",null,null,null);
//		if(confirm('Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?')){
//			sendRequestJQAsync(null, path, targetWorkArea, null);		
//		}		
	}
}

function mostrarHistorico(){
	var url = '/MidasWeb/fuerzaVenta/persona/mostrarHistorial.action?idTipoOperacion='+90+ "&idRegistro="+parent.dwr.util.getValue("personaSeycos.idPersona");
	mostrarModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}

function obtenerHistorico(){
var url = '/MidasWeb/fuerzaVenta/persona/loadHistory.action?idTipoOperacion='+dwr.util.getValue("idTipoOperacion")+ "&idRegistro="+dwr.util.getValue("idRegistro");
	document.getElementById('historicoGrid').innerHTML = '';
	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
	historicoFuerzaVentaGrid.load(url);
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosOficina');
	limpiarDiv('contenido_datosContacto');
}

function limpiarDivsFiscal() {
	limpiarDiv('contenido_datosGenerales');
	limpiarDiv('contenido_datosOficina');
	limpiarDiv('contenido_datosContacto');
}

function limpiarDivsOficina() {
	limpiarDiv('contenido_datosGenerales');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosOficina');
}

function limpiarDivsContacto() {
	limpiarDiv('contenido_datosGenerales');
	limpiarDiv('contenido_datosFiscales');
	limpiarDiv('contenido_datosContacto');
}


function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function atrasOSiguiente(tabActiva){
	var path= "/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?personaSeycos.idPersona=" + dwr.util.getValue("personaSeycos.idPersona")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+tabActiva;
	if(parent.dwr.util.getValue("tipoAccion")==1 || parent.dwr.util.getValue("tipoAccion")==4){	
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQ(null,'"+path+"',targetWorkArea,'dhx_init_tabbars();')",null,null,null);
//		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?",
//				"sendRequestJQ(null, path, '/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?personaSeycos.idPersona=' + dwr.util.getValue('personaSeycos.idPersona')+'&tipoAccion='+parent.dwr.util.getValue('tipoAccion')+'&tabActiva='+tabActiva, null")){			
//			sendRequestJQ(null, path, targetWorkArea, null);	
//		}		
	}
	else{
		sendRequestJQ(null, path, targetWorkArea, null);
	}
}

function populateDomicilio(json){
	if(json){
		var estadoName="personaSeycos.domicilios[0].claveEstado";
		var paisName="personaSeycos.domicilios[0].clavePais";
		var ciudadName="personaSeycos.domicilios[0].claveCiudad";
		var coloniaName="personaSeycos.domicilios[0].nombreColonia";
		var calleNumeroName="personaSeycos.domicilios[0].calleNumero";
		var codigoPostalName="personaSeycos.domicilios[0].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function tipoPersonaEnableDisable(){
	if(jQuery('#personaSeycos\\.idPersona').val()==null||jQuery('#personaSeycos\\.idPersona').val()==''){
		jQuery("#btnSiguiente").css("display", "none");
		}
	var condicion=jQuery("#tipoPers").val();
	if(condicion==''||condicion==null)
		{
			condicion=1;
		}
	jQuery("#txtRfcSiglas").addClass("jQrequired");
	jQuery("#txtRfcFecha").addClass("jQrequired");
	jQuery("#txtRfcHomoclave").addClass("jQrequired");
	if(jQuery("#tipoAccion").val()!= 2 && jQuery("#tipoAccion").val()!= 3){
		if(condicion==1){			
			jQuery("#txtRazonSocial").attr("readonly", true);
			jQuery("#txtRazonSocial").removeClass("jQrequired");
			
			jQuery("#fechaPersonaFisica").css("display", "block");
			jQuery("#fechaPersonaMoral").css("display", "none");
			jQuery("#fecha").addClass("jQrequired");
			jQuery("#fecha2").removeClass("jQrequired");
			if(jQuery('#personaSeycos\\.idPersona').val()==null||jQuery('#personaSeycos\\.idPersona').val()==''){
			jQuery("#txtRazonSocial").val("");
			}
			jQuery("#txtNombre").attr("readonly", false);
			jQuery("#txtApellidoPaterno").attr("readonly", false);		
			jQuery("#txtApellidoMaterno").attr("readonly", false);
			jQuery("#txtNombre").addClass("jQrequired");
			jQuery("#txtApellidoPaterno").addClass("jQrequired");
			jQuery("#txtApellidoMaterno").addClass("jQrequired");
			jQuery("#estadoCivil").addClass("jQrequired");
			jQuery("#sexo").addClass("jQrequired");
			jQuery("#txtCURP").addClass("jQrequired");
			jQuery("#personaSeycos\\.claveEstadoNacimiento").addClass("jQrequired");
			jQuery("#personaSeycos.claveCiudadNacimiento").addClass("jQrequired");
			
		}
		else{
			jQuery("#fechaPersonaFisica").css("display", "none");
			jQuery("#fechaPersonaMoral").css("display", "block");
			jQuery("#fecha2").addClass("jQrequired");
			jQuery("#fecha").removeClass("jQrequired");
			jQuery("#txtRazonSocial").attr("readonly", false);
			jQuery("#txtRazonSocial").addClass("jQrequired");
			jQuery("#txtNombre").attr("readonly", true);
			jQuery("#txtApellidoPaterno").attr("readonly", true);		
			jQuery("#txtApellidoMaterno").attr("readonly", true);
			jQuery("#txtNombre").removeClass("jQrequired");
			jQuery("#txtApellidoPaterno").removeClass("jQrequired");
			jQuery("#txtApellidoMaterno").removeClass("jQrequired");
			jQuery("#estadoCivil").removeClass("jQrequired");			
			jQuery("#sexo").removeClass("jQrequired");
			jQuery("#txtCURP").removeClass("jQrequired");
			jQuery("#personaSeycos\\.claveEstadoNacimiento").removeClass("jQrequired");
			jQuery("#personaSeycos\\.claveCiudadNacimiento").removeClass("jQrequired");
			if(jQuery('#personaSeycos\\.idPersona').val()==null&&jQuery('#personaSeycos\\.idPersona').val()==''){
			jQuery("#txtNombre").val("");
			jQuery("#txtApellidoPaterno").val("");
			jQuery("#txtApellidoMaterno").val("");
			jQuery("#txtCURP").val("");
			}			
		}
	}	
	if(condicion==2){
		jQuery("#fechaPersonaFisica").css("display", "none");
		jQuery("#fechaPersonaMoral").css("display", "block");
		jQuery("#fecha2").addClass("jQrequired");
		jQuery("#fecha").removeClass("jQrequired");
//		jQuery("#fecha").attr("yearRange",null);
		jQuery("#txtSectorFinanciero").css("display", "block");		
		jQuery("#selSectorFinanciero").css("display", "block");	
		jQuery("#linkRenapo").css("display", "none");		
		jQuery("#tablaNacimiento").css("display", "none");	
		jQuery("#fNacimiento").css("display", "none");
		jQuery("#fConstitucion").css("display", "block");
		jQuery("#trRazonSocial").css("display", "block");
		jQuery("#trRazonSocial1").css("display", "block");
		jQuery("#lNombre").css("display", "none");
		jQuery("#tNombre").css("display", "none");
		jQuery("#lAPaterno").css("display", "none");
		jQuery("#tAPaterno").css("display", "none");
		jQuery("#lAMaterno").css("display", "none");
		jQuery("#tAMaterno").css("display", "none");
		jQuery("#lSexo").css("display", "none");
		jQuery("#tSexo").css("display", "none");
		jQuery("#lEdoCivil").css("display", "none");
		jQuery("#tEdoCivil").css("display", "none");
		jQuery("#lCURP").css("display", "none");
		jQuery("#tCURP").css("display", "none");
		jQuery("#btnValidarCurp").css("display", "none");
		jQuery("#linkRenapo").css("display", "none");		
		jQuery("#tablaNacimiento").css("display", "none");	
		jQuery("#txtRfcSiglas").removeClass("jQsiglasRFCFisica");
		jQuery("#txtRfcSiglas").addClass("jQsiglasRFCMoral");
		if(jQuery('#personaSeycos\\.idPersona').val()==null||jQuery('#personaSeycos\\.idPersona').val()==''){
		jQuery("#txtNombre").val("");
		jQuery("#txtApellidoPaterno").val("");
		jQuery("#txtApellidoMaterno").val("");
		jQuery("#txtCURP").val("");
		}			
	}
	else{
		jQuery("#fechaPersonaFisica").css("display", "block");
		jQuery("#fechaPersonaMoral").css("display", "none");
		jQuery("#fecha").addClass("jQrequired");
		jQuery("#fecha2").removeClass("jQrequired");
		jQuery("#txtSectorFinanciero").css("display", "none");		
		jQuery("#selSectorFinanciero").css("display", "none");	
		jQuery("#trRazonSocial").css("display", "none");
		jQuery("#trRazonSocial1").css("display", "none");
		jQuery("#fNacimiento").css("display", "block");
		jQuery("#fConstitucion").css("display", "none");		
		jQuery("#lNombre").css("display", "block");
		jQuery("#tNombre").css("display", "block");
		jQuery("#lAPaterno").css("display", "block");
		jQuery("#tAPaterno").css("display", "block");
		jQuery("#lAMaterno").css("display", "block");
		jQuery("#tAMaterno").css("display", "block");
		jQuery("#lSexo").css("display", "block");
		jQuery("#tSexo").css("display", "block");
		jQuery("#lEdoCivil").css("display", "block");
		jQuery("#tEdoCivil").css("display", "block");
		jQuery("#lCURP").css("display", "block");
		jQuery("#tCURP").css("display", "block");
		jQuery("#btnValidarCurp").css("display", "block");
		jQuery("#linkRenapo").css("display", "block");
		jQuery("#tablaNacimiento").css("display", "block");
		jQuery("#txtRfcSiglas").removeClass("jQsiglasRFCMoral");
		jQuery("#txtRfcSiglas").addClass("jQsiglasRFCFisica");
	}
	if(jQuery("#tipoAccion").val()== 3 && jQuery('#txtFechaBAja').val()!=null && jQuery('#txtFechaBAja').val()!=''){
		jQuery("#btnEliminar").css("display", "none");
	}
}

function botonSiguiente(){
	if(jQuery('#personaSeycos\\.idPersona').val()==null||jQuery('#personaSeycos\\.idPersona').val()==''){
	jQuery("#btnSiguiente").css("display", "none");
	}
}

function validarCurpJSON(){
	var estadoNacimiento = jQuery('#personaSeycos\\.claveEstadoNacimiento option:selected').text();
	var curp = dwr.util.getValue("personaSeycos.codigoCURP");
	var fechaNac = dwr.util.getValue("personaSeycos.fechaNacimiento");
	var nombre = dwr.util.getValue("personaSeycos.nombre");
	var apellidoPat = dwr.util.getValue("personaSeycos.apellidoPaterno");
	var apellidoMat = dwr.util.getValue("personaSeycos.apellidoMaterno");
	var sexo = dwr.util.getValue("personaSeycos.sexo");	
	var tipoPersona = dwr.util.getValue("personaSeycos.claveTipoPersona");
	if(tipoPersona ==1 && estadoNacimiento!=null && estadoNacimiento!='' && curp!=null && curp!='' && fechaNac!=null && fechaNac!='' && nombre!=null && nombre!='' && apellidoPat!=null && apellidoPat!='' && apellidoMat!=null && apellidoMat!='' &&sexo!=null && sexo!=''){
		parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");
	}else{
		var url="/MidasWeb/fuerzaVenta/persona/validarCurpPersona.action";
		var params="?estadoNacimientoCurp="+estadoNacimiento;
		params+="&codigoCURP="+curp;
		params+="&fechaNacimientoCurp="+fechaNac;
		params+="&nombreCurp="+nombre;
		params+="&apellidoPaternoCurp="+apellidoPat;
		params+="&apellidoMaternoCurp="+apellidoMat;
		params+="&sexoCurp="+sexo;
		params+="&claveTipoPersonaCurp="+tipoPersona;
		url+=params;
		jQuery.asyncGetJSON(url,null,function(data){
			var valido=(data!=null && data["curpValido"]!=null)?data["curpValido"]:"0";
			if(valido="1"){
				alert("Valido");
			}else{
				alert("Invalido");
			}
		});
	}
	
}

function validaCURPPersonaAgentesJS(){
	var resultado=false;
	var estadoNacimiento = jQuery('#personaSeycos\\.claveEstadoNacimiento option:selected').text();
	
	/* Se agrega parche para enviar la descripcion DISTRITO FEDERAL al validador de CURP */
	if(estadoNacimiento == 'CIUDAD DE MEXICO')
	{		
		estadoNacimiento = 'DISTRITO FEDERAL';		
	}
	
	var curp = dwr.util.getValue("personaSeycos.codigoCURP");
	var fechaNac = dwr.util.getValue("personaSeycos.fechaNacimiento");
	var nombre = dwr.util.getValue("personaSeycos.nombre");
	var apellidoPat = dwr.util.getValue("personaSeycos.apellidoPaterno");
	var apellidoMat = dwr.util.getValue("personaSeycos.apellidoMaterno");
	var sexo = dwr.util.getValue("personaSeycos.sexo");	
	var tipoPersona = dwr.util.getValue("personaSeycos.claveTipoPersona");
	if(tipoPersona ==1 &&
	   estadoNacimiento!=null && estadoNacimiento!='' &&
	   curp!=null 			  && curp!='' 			  &&
	   fechaNac!=null 		  && fechaNac!='' &&
	   nombre!=null 		  && nombre!='' &&
	   apellidoPat!=null 	  && apellidoPat!='' &&
	   apellidoMat!=null 	  && apellidoMat!='' &&
	   sexo!=null 			  && sexo!=''){		
	   validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
			 if(!data){
				 parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");	
				 return false;
			 }else{
				 if(tipoPersona==1){
					 parent.mostrarMensajeInformativo('CURP valida para los datos proporcionados',"30");				 
				 }			 
				 return true;
			 }
	   });
	}else{
		parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");	
		return false;
	}	
}

function validaCURPPersonaClientesJS(){
	var resultado=false;
	var estadoNacimiento = jQuery('#cliente\\.claveEstadoNacimiento option:selected').text();
	var curp = dwr.util.getValue("cliente.codigoCURP");
	var fechaNac = dwr.util.getValue("cliente.fechaNacimiento");
	var nombre = dwr.util.getValue("cliente.nombre");
	var apellidoPat = dwr.util.getValue("cliente.apellidoPaterno");
	var apellidoMat = dwr.util.getValue("cliente.apellidoMaterno");
	var sexo = dwr.util.getValue("cliente.sexo");	
	var tipoPersona = dwr.util.getValue("cliente.claveTipoPersona");
	if(tipoPersona ==1 &&
	   estadoNacimiento!=null && estadoNacimiento!='' &&
	   curp!=null 			  && curp!='' 			  &&
	   fechaNac!=null 		  && fechaNac!='' &&
	   nombre!=null 		  && nombre!='' &&
	   apellidoPat!=null 	  && apellidoPat!='' &&
	   apellidoMat!=null 	  && apellidoMat!='' &&
	   sexo!=null 			  && sexo!=''){		
	   validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
			 if(!data){
				 parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");	
				 return false;
			 }else{
				 if(tipoPersona==1){
					 parent.mostrarMensajeInformativo('CURP valida para los datos proporcionados',"30");				 
				 }			 
				 return true;
			 }
	   });
	}else{
		parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");	
		return false;
	}	
}

function mostrarOcultarCamposPersonaFiscal(){
	if(jQuery("#hiddenTipoPersona").val()==1){
		jQuery("#datosMoral").css("display", "none");
		jQuery("#botonRep").css("display", "none");		
		jQuery("#lRepLegal").css("display", "none");
		jQuery("#tRepLegal").css("display", "none");
		jQuery("#tRepLegalNombre").css("display", "none");
		jQuery("#lRSocial").css("display", "none");
		jQuery("#tRSocial").css("display", "none");
		jQuery("#fechaConstit").css("display", "none");
	}
	else{
		jQuery("#datosFisica").css("display", "none");
		jQuery("#lNombre").css("display", "none");
		jQuery("#tNombre").css("display", "none");
		jQuery("#lAPaterno").css("display", "none");
		jQuery("#tAPaterno").css("display", "none");
		jQuery("#lAMaterno").css("display", "none");
		jQuery("#tAMaterno").css("display", "none");
		jQuery("#txtIdRepresentanteFiscal").addClass("jQrequired");
	}
	
	if(jQuery("#txtIdRepresentanteFiscal").val()){
		findByIdResponsablePF(jQuery("#txtIdRepresentanteFiscal").val());
	}
}

function findByIdResponsablePF(idResponsable){
var url="/MidasWeb/fuerzaVenta/persona/findById.action";
var data={"personaSeycos.idPersona":idResponsable};
jQuery.asyncPostJSON(url,data,populatePersonaPF);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populatePersonaPF(json){
if(json){
	var idResponsable=json.personaSeycos.idPersona;
	var nombre=json.personaSeycos.nombreCompleto;
	jQuery("#txtIdRepresentanteFiscal").val(idResponsable);
	jQuery("#txtNombreRepresentanteFiscal").val(nombre);
}
}


function mostrarOcultarFechaInactivo(status){
	if(status==1){
		jQuery("#txtFechaBAja").val("");	
	}
	else{		
		jQuery("#txtFechaBAja").val(jQuery("#txtFechaActual").val());
	}
}

function incializarTabsPersona(){
	var idPersona=dwr.util.getValue("personaSeycos.idPersona");
	contenedorTab=window["configuracionPersonaTabBar"];
	//Nombre de todos los tabs
	var tabs=["datosGenerales","datosFiscales","datosOficina","datosContacto"];
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
	if(!jQuery.isValid(idPersona)){
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
	for(var i=0;i<tabs.length;i++){
		var tabName=tabs[i];
		var tab=contenedorTab[tabName];
		jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){		
			if(jQuery.isValid(idPersona)){
				if(parent.dwr.util.getValue("tipoAccion")==1 || parent.dwr.util.getValue("tipoAccion")==4){
				event.stopPropagation();				
				parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}
				else{
					obtenerFuncionTab(event.data.value);
				}
			}
			else{
				parent.mostrarMensajeInformativo("Debe guardar datos generales","10");
			}
		});
	}
}

function obtenerFuncionTab(tab){
	if(tab==0){
		sendRequestJQ(null,'/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&personaSeycos.idPersona='+dwr.util.getValue('personaSeycos.idPersona')+'&tabActiva=datosGenerales',targetWorkArea,'dhx_init_tabbars();');
	}else if(tab==1){
		sendRequestJQ(null,'/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&personaSeycos.idPersona='+dwr.util.getValue('personaSeycos.idPersona')+'&tabActiva=datosFiscales',targetWorkArea,'dhx_init_tabbars();');
	}else if(tab==2){
		sendRequestJQ(null,'/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&personaSeycos.idPersona='+dwr.util.getValue('personaSeycos.idPersona')+'&tabActiva=datosOficina',targetWorkArea,'dhx_init_tabbars();');
	}else{
		sendRequestJQ(null,'/MidasWeb/fuerzaVenta/persona/mostrarCapturaPersona.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&personaSeycos.idPersona='+dwr.util.getValue('personaSeycos.idPersona')+'&tabActiva=datosContacto',targetWorkArea,'dhx_init_tabbars();');
	}
}

function show_hide_BtnGenerarRFC(){
	var val = jQuery("#tipoPers").val();
	jQuery("#btnGenerarRFC").hide();
	if(val == 1){
		jQuery("#btnGenerarRFC").show();
		jQuery("#btnGenerarRFCMoral").hide();
	}
	if(val == 2){
		jQuery("#btnGenerarRFC").hide();
		jQuery("#btnGenerarRFCMoral").show();		
	}	
}

function copiarDomicilioPersona(indice,idPais,idEstado,idCiudad,colonia,calleNumero,codigoPostal){
	if(idPais!=''&&idEstado!=''&&idCiudad!=''&&calleNumero!=''&&colonia!=''&&codigoPostal!=''){
	var estadoName='personaSeycos.domicilios['+indice+'].claveEstado';
	var paisName='personaSeycos.domicilios['+indice+'].clavePais';
	var ciudadName='personaSeycos.domicilios['+indice+'].claveCiudad';
	var coloniaName='personaSeycos.domicilios['+indice+'].nombreColonia';
	var calleNumeroName = 'personaSeycos.domicilios['+indice+'].calleNumero';
	var codigoPostalName = 'personaSeycos.domicilios['+indice+'].codigoPostal';
	
	dwr.util.setValue(paisName, idPais);	
	onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName, paisName);
	dwr.util.setValue(estadoName, idEstado);
	onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
	dwr.util.setValue(ciudadName, idCiudad);
	onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
	dwr.util.setValue(coloniaName, colonia);
	onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
	dwr.util.setValue(calleNumeroName, calleNumero);
	
	dwr.util.setValue(codigoPostalName, codigoPostal);
	}
	else{
		parent.mostrarMensajeInformativo("El domicilio a copiar esta incompleto, favor de verificar","10");
	}
}

function populateDomicilioFiscal(json){
	if(json){
		var estadoName="personaSeycos.domicilios[1].claveEstado";
		var paisName="personaSeycos.domicilios[1].clavePais";
		var ciudadName="personaSeycos.domicilios[1].claveCiudad";
		var coloniaName="personaSeycos.domicilios[1].nombreColonia";
		var calleNumeroName="personaSeycos.domicilios[1].calleNumero";
		var codigoPostalName="personaSeycos.domicilios[1].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function populateDomicilioOficina(json){
	if(json){
		var estadoName="personaSeycos.domicilios[2].claveEstado";
		var paisName="personaSeycos.domicilios[2].clavePais";
		var ciudadName="personaSeycos.domicilios[2].claveCiudad";
		var coloniaName="personaSeycos.domicilios[2].nombreColonia";
		var calleNumeroName="personaSeycos.domicilios[2].calleNumero";
		var codigoPostalName="personaSeycos.domicilios[2].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function clearRfc(){ 
	jQuery("#txtRfcSiglas").val("");
	jQuery("#txtRfcFecha").val("");
	jQuery("#txtRfcHomoclave").val(""); 
}

function validaCURPPersonaAgentesInterJS(){
	var resultado=false;
	var estadoNacimiento = jQuery('#altaPorInternet\\.persona\\.claveEstadoNacimiento option:selected').text();
	var curp = dwr.util.getValue("altaPorInternet.persona.curp");
	var fechaNac = dwr.util.getValue("altaPorInternet.persona.fechaNacimiento");
	var nombre = dwr.util.getValue("altaPorInternet.persona.nombre");
	var apellidoPat = dwr.util.getValue("altaPorInternet.persona.apellidoPaterno");
	var apellidoMat = dwr.util.getValue("altaPorInternet.persona.apellidoMaterno");
	var sexo = dwr.util.getValue("altaPorInternet.persona.claveSexo");	
	var tipoPersona = dwr.util.getValue("altaPorInternet.persona.claveTipoPersona");
	if(tipoPersona ==1 &&
	   estadoNacimiento!=null && estadoNacimiento!='' &&
	   curp!=null 			  && curp!='' 			  &&
	   fechaNac!=null 		  && fechaNac!='' &&
	   nombre!=null 		  && nombre!='' &&
	   apellidoPat!=null 	  && apellidoPat!='' &&
	   apellidoMat!=null 	  && apellidoMat!='' &&
	   sexo!=null 			  && sexo!=''){		
	   validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
			 if(!data){
				 parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");	
				 return false;
			 }else{
				 if(tipoPersona==1){
					 parent.mostrarMensajeInformativo('CURP valida para los datos proporcionados',"30");				 
				 }			 
				 return true;
			 }
	   });
	}else{
		parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");	
		return false;
	}	
}
function quitaComa(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 44 || tecla==32 || tecla==13)
		return false;
}