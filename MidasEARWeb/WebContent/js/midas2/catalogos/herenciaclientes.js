var clientesCedidosGrid;
var clientesAcederGrid;
var ventanaAgentes=null;
function mostrarModalAgentes(){
	var idAgente = jQuery("#txtIdAgente").val();
	var isCheck = jQuery(".js_isCheck:checked").val();
	var field=(isCheck==1)?"txtIdHiddenOrigen":"txtIdHiddenDestino";
	limpiaListadosDeClientes();
	var idTipoSituacionAgenteAUTORIZADO=null;
	dwr.engine.beginBatch();
	listadoService.getIdElementoValorCatalogoAgentes("Estatus de Agente (Situacion)","AUTORIZADO",
			function(id){
				idTipoSituacionAgenteAUTORIZADO=id;
			});
	dwr.engine.endBatch({async:false});
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field+"&filtroAgente.tipoSituacion.id="+idTipoSituacionAgenteAUTORIZADO;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/agente/findByClaveAgente.action";
		var data={"agente.idAgente":idAgente};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}
function limpiaListadosDeClientes(){
	jQuery('#listaClientesCedidos').html("");
	jQuery('#listaClientes').html(""); 
					
}

function onChangeAgente(){
	var isCheck = jQuery(".js_isCheck:checked").val();
	var field=(isCheck==1)?"txtIdHiddenOrigen":"txtIdHiddenDestino";
	var idAgente = jQuery("#"+field).val();
	if(jQuery.isValid(idAgente)){
		var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
		var data={"agente.id":idAgente};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function loadInfoAgente(json){
	
	if(json.agente.persona){
		var isCheck = jQuery(".js_isCheck:checked").val();
		var agenteConsulta;
		
		var agente=json.agente;
		var id=agente.id;
		var idAgente=agente.idAgente;
		var nombreCompleto=agente.persona.nombreCompleto;
		var idCedula = agente.tipoCedula.id
		var tipoCedula = agente.tipoCedula.valor
//		var claveTipoPersona= (agente.persona.claveTipoPersona == 1) ? "FISICA" : "MORAL";
		var claveTipoPersona= agente.persona.claveTipoPersona;
		var idPrioridadAgente = agente.idPrioridadAgente;
		var curp=agente.persona.curp;
		var idMotivoEstatusAgente = agente.idMotivoEstatusAgente;
		var idTipoSituacion = agente.tipoSituacion.id;
//		alert("("+idMotivoEstatusAgente+" != 958 && "+idTipoSituacion+" != 1043) || ("+idTipoSituacion+" !=111) || ("+idTipoSituacion+" !=113) || ("+idTipoSituacion+" !=114)");
				
		if((idMotivoEstatusAgente == 958 && idTipoSituacion == 1043)){
			alert("La Situación del agente es BAJA y el Motivo Estatus AUDITORIA ");
		}else if((idTipoSituacion ==111)){
			alert("La Situación del agente es PENDIENTE POR AUTORIZAR");
		}else if(idTipoSituacion ==113){
			alert("La Situación del agente es RECHAZADO");
		}else if(idTipoSituacion ==114){
			alert("La Situación del agente es RECHAZADO CON EMISION");
		}else{
			if(isCheck == 1){
				agenteConsulta = "Origen";
				var url="/MidasWeb/fuerzaventa/agente/mostrarNDocumentosMismo.action";
				var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"COMPROBANTE DE CESION","nombreCarpeta":"HERENCIA DE CLIENTES","nombreGaveta":"AGENTES"};
				crearEstructuraAgente(url, data);
			}else{
				agenteConsulta = "Destino";
			}
			
			jQuery("#txtIdHidden" + agenteConsulta).val(id);
			jQuery("#txtIdAgenteHidden" + agenteConsulta).val(idAgente);
			
			jQuery("#txtIdAgente" + agenteConsulta).val(idAgente);		
			jQuery("#txtNombreCompleto" + agenteConsulta).val(nombreCompleto);
			jQuery("#txtIdCedula" + agenteConsulta).val(idCedula);
			jQuery("#txtTipoCedula" + agenteConsulta).val(tipoCedula);
			jQuery("#txtPersonalidad" + agenteConsulta).val(claveTipoPersona);
			jQuery("#txtIdPrioridadAgente" + agenteConsulta).val(idPrioridadAgente);
			jQuery("#txtCurp" + agenteConsulta).val(curp);
			
			
			var agenteOrigen = jQuery("#txtIdHiddenOrigen").val();
			var agenteDestino = jQuery("#txtIdHiddenDestino").val();
			if(agenteOrigen!="" && agenteDestino!=""){
				loadListClientes(agenteOrigen,agenteDestino);	
			}		
		}
	}else{
		alert("No existe el agente con clave " + json.agente.idAgente);
	}
}

function loadListClientes(agenteOrigen,agenteDestino){
	var txtIdCedulaOrigen = jQuery("#txtIdCedulaOrigen").val();
	var txtIdCedulaDestino = jQuery("#txtIdCedulaDestino").val();
	var url="/MidasWeb/fuerzaventa/herenciaclientes/listarClientesHeredar.action";
//	var data={"agenteOrigen.id":agenteOrigen,"agenteDestino.id" : agenteDestino,
//			  "agenteOrigen.tipoCedula.id" : txtIdCedulaOrigen,"agenteDestino.tipoCedula.id" : txtIdCedulaDestino};
	var data = "agenteOrigen.id="+agenteOrigen+"&agenteDestino.id="+agenteDestino+"&agenteOrigen.tipoCedula.id="+txtIdCedulaOrigen+"&agenteDestino.tipoCedula.id="+txtIdCedulaDestino;
	
//	jQuery.asyncPostJSON(url,data,response);
	clientesAcederGrid = listarFiltradoGenerico(url+"?"+data,'clientesAcederGrid',null,null,null);
}
function response(json){
	var listaClientes = json.listaClientes;	
	var clientesLength = json.listaClientes.length;	
	jQuery("#txtClientesACeder").val(clientesLength);
	jQuery.fillCombo('listaClientes',listaClientes,'id','cliente.nombre',false);

	if(json.mensaje !=null) {
		mostrarVentanaMensaje(json.tipoMensaje, json.mensaje, null);
	}
	
}

//function agregaOptionCliente(){
//	var clientesACeder = (jQuery("#txtClientesACeder").val()!="") ? jQuery("#txtClientesACeder").val() : 0;
//	var clientesCedidos = (jQuery("#clientesCedidos").val()!="") ? jQuery("#clientesCedidos").val() : 0;
//	var totalClientesCedidos=clientesCedidos;
//	var totalClientesACeder=clientesACeder;
//	
//	jQuery('#listaClientes option:selected').each(function(index,obj){
//		var optValSel = jQuery(obj).val();
//		var optText = jQuery(obj).text();
//		jQuery(obj).remove();
//		jQuery('#listaClientesCedidos').append('<option value="'+optValSel+'">'+optText+'</option>');
//		
//		totalClientesCedidos++;
//		jQuery("#clientesCedidos").val(totalClientesCedidos);
//		totalClientesACeder--;
//		jQuery("#txtClientesACeder").val(totalClientesACeder);
//	});
//	
//}

function agregaOptionCliente(){
	
	var rId=[];
	rId=clientesAcederGrid.getCheckedRows(0).split(",");
//    alert(rId.length+" - "+rId);
	if(rId != ""){		
		for(i=0; i<=rId.length-1; i++){
		    idField= rId[i];
//                    alert(idField);
			var idCliente = clientesAcederGrid.cellById(idField, 1).getValue();
			var nombre = clientesAcederGrid.cellById(idField, 2).getValue();
			clientesAcederGrid.deleteRow(idField);
			clientesCedidosGrid.setColTypes("ch,ro,ro");
			clientesCedidosGrid.addRow(idField,[0,idCliente, nombre]);
		}
	}else{
		alert("debe seleccionar almenos un cliente");
	}
}

function regresaOptionCliente(){
	
	var rId=[];
	rId=clientesCedidosGrid.getCheckedRows(0).split(",");
//    alert(rId.length+" - "+rId);
	if(rId != ""){		
		for(i=0; i<=rId.length-1; i++){
		    idField= rId[i];
//                    alert(idField);
			var idCliente = clientesCedidosGrid.cellById(idField, 1).getValue();
			var nombre = clientesCedidosGrid.cellById(idField, 2).getValue();
			clientesCedidosGrid.deleteRow(idField);
			clientesAcederGrid.setColTypes("ch,ro,ro");
			clientesAcederGrid.addRow(idField,[0,idCliente, nombre]);
		}
	}else{
		alert("debe seleccionar almenos un cliente");
	}
}

//function regresaOptionCliente(){
//	var clientesACeder = (jQuery("#txtClientesACeder").val()!="") ? jQuery("#txtClientesACeder").val() : 0;
//	var clientesCedidos = (jQuery("#clientesCedidos").val()!="") ? jQuery("#clientesCedidos").val() : 0;
//	var totalClientesCedidos=clientesCedidos;
//	var totalClientesACeder=clientesACeder;
//	jQuery('#listaClientesCedidos option:selected').each(function(index,obj){
//		var optValSel = jQuery(obj).val();
//		var optText = jQuery(obj).text();
//		jQuery(obj).remove();
//		jQuery('#listaClientes').append('<option value="'+optValSel+'">'+optText+'</option>');
//		
//		totalClientesCedidos--;
//		jQuery("#clientesCedidos").val(totalClientesCedidos);
//		totalClientesACeder++;
//		jQuery("#txtClientesACeder").val(totalClientesACeder);
//	});
//}

function guardarHerencia(){
	var id = jQuery("#id").val();
	var url="/MidasWeb/fuerzaventa/agente/matchNDocumentosMismo.action";
	var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"COMPROBANTE DE CESION","nombreCarpeta":"HERENCIA DE CLIENTES","nombreGaveta":"AGENTES"};
	jQuery.asyncPostJSON(url,data,$_guardar);
//	var clientesCedidos = jQuery("#clientesCedidos").val();
//			if(validateAll(true)){
//				var arr = new Array();
//				var strParam="";
//				jQuery('#listaClientesCedidos option').each(function(index){	
//					var cliente={"id":jQuery(this).val(),"cliente.nombre":jQuery(this).text(),"agente.id":jQuery("#txtIdHiddenOrigen").val()};
//					//var agente = {"agente.id" : jQuery("#txtIdAgenteOrigen").val()};
//					arr.push(cliente);
//					//arr.push(agente);
//				});
//				
//				var agenteOrigen = jQuery("#txtIdHiddenOrigen").val();
//				var agenteDestino = jQuery("#txtIdHiddenDestino").val();	
//				var idMotivoCesion = jQuery("#listMotivoCesion").val();
//				
//				strParam=jQuery.convertJsonArrayToParams(arr,["id","cliente.nombre","agente.id"],"listaClientesCedidos");
//				var dataHerencia = "&herenciaClientes.agenteOrigen.id="+agenteOrigen+"&herenciaClientes.agenteDestino.id="+agenteDestino+"&herenciaClientes.motivoCesion.id="+idMotivoCesion;
//				
//				var data="agenteOrigen.id="+agenteOrigen+"&agenteDestino.id="+agenteDestino+dataHerencia+"&"+strParam; 
//				
//				var url ="/MidasWeb/fuerzaventa/herenciaclientes/guardarHerencia.action";
//				
//				jQuery.asyncPostJSON(url,data,responseGuardado);
//			}
	
}

function $_guardar(json){
	if(json){
	var lista=json.listaDocumentosFortimax;
	var count = 0;
	if(!jQuery.isEmptyArray(lista)){
		var lista=json.listaDocumentosFortimax;
			for(var i=0;i<lista.length;i++){
				if (lista[i].existeDocumento == 0){
					count = count+1;
				}
			}if (count==0){
				var clientesCedidos = jQuery("#clientesCedidos").val();
				if(validateAll(true)){
					var arr = new Array();
					var strParam="";
					jQuery('#listaClientesCedidos option').each(function(index){	
						var cliente={"id":jQuery(this).val(),"cliente.nombre":jQuery(this).text(),"agente.id":jQuery("#txtIdHiddenOrigen").val()};
						//var agente = {"agente.id" : jQuery("#txtIdAgenteOrigen").val()};
						arr.push(cliente);
						//arr.push(agente);
					});
					
					 clientesCedidosGrid.forEachRow(function(id) {
					        var nombre =clientesCedidosGrid.cellById(id, 2).getValue();
					       // var id = clientesCedidosGrid.cellById(id, 1).getValue();
					        var cliente={"id":id,"cliente.nombre":nombre,"agente.id":jQuery("#txtIdHiddenOrigen").val()};
					        arr.push(cliente);
					});
					
					var agenteOrigen = jQuery("#txtIdHiddenOrigen").val();
					var agenteDestino = jQuery("#txtIdHiddenDestino").val();	
					var idMotivoCesion = jQuery("#listMotivoCesion").val();
					
					strParam=jQuery.convertJsonArrayToParams(arr,["id","cliente.nombre","agente.id"],"listaClientesCedidos");
					var dataHerencia = "&herenciaClientes.agenteOrigen.id="+agenteOrigen+"&herenciaClientes.agenteDestino.id="+agenteDestino+"&herenciaClientes.motivoCesion.id="+idMotivoCesion;
					
					var data="agenteOrigen.id="+agenteOrigen+"&agenteDestino.id="+agenteDestino+dataHerencia+"&"+strParam; 
					
					var url ="/MidasWeb/fuerzaventa/herenciaclientes/guardarHerencia.action";
					
					jQuery.asyncPostJSON(url,data,responseGuardado);
				}
			}else{
				parent.mostrarMensajeInformativo('Hay '+count+' documentos sin digitalizar',"10");
			}
		}else{
			parent.mostrarMensajeInformativo('Hay documentos sin digitalizar');			
		}
	}
}
function responseGuardado(json){
	dwr.util.setValue("herenciaClientes.status.valor",json.herenciaClientes.status.valor);
	mostrarOcultarDocumentos(json);
	getHistorico(json.herenciaClientes.id);
	jQuery("#btnActivar").css("display","block");
}

function generarLigaIfimaxHerencia(){
	var url='/MidasWeb/fuerzaventa/herenciaclientes/generarLigaIfimax.action?agenteOrigen.id='+parent.dwr.util.getValue("agenteOrigen.id")+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agenteOrigen.persona.claveTipoPersona")+'&herenciaClientes.id='+dwr.util.getValue("herenciaClientes.id");
	window.open(url,'Fortimax');
}

function auditarDocumentosHerencia(){
	var id = parent.dwr.util.getValue("agenteOrigen.id");
	var url="/MidasWeb/fuerzaventa/agente/matchNDocumentosMismo.action";
	var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"COMPROBANTE DE CESION","nombreCarpeta":"HERENCIA DE CLIENTES","nombreGaveta":"AGENTES"};
	jQuery.asyncPostJSON(url,data,$_populateDocumentosFortimax);
//	var url="/MidasWeb/fuerzaventa/herenciaclientes/matchDocumentosAgente.action?agenteOrigen.id="+parent.dwr.util.getValue("agenteOrigen.id")+'&herenciaClientes.id='+dwr.util.getValue("herenciaClientes.id");
//	var data="";
//	jQuery.asyncPostJSON(url,data,populateDocumentosFortimaxHerencia);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimaxHerencia(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#herenciaDocumentosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
				
			});
		}
	}
}

function guardarDocumentosFortimaxHerencia(){
	if(jQuery("#idHerencia").val()!=null){
		var url="/MidasWeb/fuerzaventa/herenciaclientes/guardarDocumentos.action?"+jQuery("#herenciaDocumentosForm").serialize()+'&herenciaClientes.id='+dwr.util.getValue("herenciaClientes.id")+'&herenciaClientes.agenteOrigen.id='+dwr.util.getValue("agenteOrigen.id");
		var data;
		jQuery.asyncPostJSON(url,data,populateDocumentosFaltantesHerencia);
	}else{
		
	}
}

function populateDocumentosFaltantesHerencia(json){
	if(json){
		var docsFaltantes=json.documentosFaltantes;
		if(docsFaltantes!=null){
			parent.mostrarMensajeInformativo('Es requerido digitalizar: '+docsFaltantes+' para activar la herencia',"10");
		}
		else{
			parent.mostrarMensajeInformativo('La herencia ha sido activada exitosamente',"30");
			document.getElementById("documentosHerencia").innerHTML="";
			jQuery("#btnGuardar").css("display", "none");
		}
	}
}

function onChangeGerencia(target, target2,value){
	var idgerencia = value;//dwr.util.getValue("idGerencia");
	if(idgerencia != null  && idgerencia != headerValue){
		listadoService.getMapEjecutivosPorGerencia(idgerencia,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});
	}else{
		addOptions(target,null);
	}

	var idEjecutivo = jQuery("#idEjecutivo option:selected").val();
	if(idEjecutivo!=''){		
		addOptions(target2,null);
	}
}

function onChangeEjecutivo(target,value){
	var idEjecutivo =value;// dwr.util.getValue("idEjecutivo");
	if(idEjecutivo != null  && idEjecutivo != headerValue){
		listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});	
	}else{
		addOptions(target,null);
	}
}

function mostrarHistorico(){
	var idTipoOperacion = 110;
	var idRegistro = jQuery("#idHerencia").val();
	if(idRegistro!=""){
		var url = '/MidasWeb/fuerzaventa/herenciaclientes/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
		parent.mostrarVentanaModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
	}
}

function mostrarOcultarDocumentos(json){
	if(json!=null){
		dwr.util.setValue("herenciaClientes.id",json.herenciaClientes.id);
	}
	if(dwr.util.getValue("herenciaClientes.id")!=''&&dwr.util.getValue("herenciaClientes.id")!=null&&dwr.util.getValue("herenciaClientes.status.valor")=='INACTIVO'){
		sendRequestJQ(null,'/MidasWeb/fuerzaventa/herenciaclientes/mostrarDocumentos.action?agenteDestino.id='+dwr.util.getValue("agenteDestino.id")+'&herenciaClientes.id='+dwr.util.getValue("herenciaClientes.id"),'documentosHerencia',null);
		jQuery("#btnGuardar").css("display", "none");
//		mostrarVentanaMensaje(null, "Es necesario digitalizar el comprobante de cesion para activar la herencia", "10");
	}
	else{
		document.getElementById("documentosHerencia").innerHTML="";
	}
}

function verDetalleHerenciaCliente(herenciaId, idRegistro, idTipoOperacion){
	sendRequestJQ(null,'/MidasWeb/fuerzaventa/herenciaclientes/verDetalle.action?herenciaClientes.id='+herenciaId+'&idRegistro='+idRegistro+'&idTipoOperacion='+idTipoOperacion+'&tipoAccion=2',targetWorkArea,'mostrarOcultarDocumentos()');
}

function getHistorico(idHerenciaCliente){
	 var idHerencia;
	 if(idHerenciaCliente!=null){
		 idHerencia = idHerenciaCliente;
	 }else{
		 idHerencia = jQuery("#idHerencia").val();
	 }
	
	if(idHerencia!=""){
		var data="idHerencia="+idHerencia; 
		var url ="/MidasWeb/fuerzaventa/herenciaclientes/getLastHistoricoUpdate.action";
		jQuery.asyncPostJSON(url,data,responseGetHistorico);
	}
}
function responseGetHistorico(json){
	if(json.detHistorial.fechaHoraActualizacionString!=null){
		var fecha =json.detHistorial.fechaHoraActualizacionString;
		var user = json.detHistorial.usuarioActualizacion;
		jQuery("#txtFechaHora").val(fecha);
		jQuery("#txtUsuario").val(user);
		jQuery("#mostrar_historico").css("display", "block");
	}
}

function crearGrid(){
	idHerencia = jQuery("#idHerencia").val();
	if(idHerencia==""){
		clientesCedidosGrid = new dhtmlXGridObject('clientesCedidosGrid');
		clientesCedidosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		clientesCedidosGrid.setHeader("#master_checkbox,id,Nombre(s)");
		clientesCedidosGrid.setInitWidths("30,*,*");
		clientesCedidosGrid.setColAlign("left,left,left");
		clientesCedidosGrid.setSkin("light");
		clientesCedidosGrid.enablePaging(true,13,5,"pagingArea2",true,"infoArea2");
		clientesCedidosGrid.setPagingSkin("bricks");
		clientesCedidosGrid.attachHeader(",#text_filter,#text_filter");
		clientesCedidosGrid.init();

		clientesAcederGrid = new dhtmlXGridObject('clientesAcederGrid');
		clientesAcederGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		clientesAcederGrid.setHeader("#master_checkbox,id,Nombre(s)");
		clientesAcederGrid.setInitWidths("30,*,*");
		clientesAcederGrid.setColAlign("left,left,left");
		clientesAcederGrid.setSkin("light");
		clientesAcederGrid.enablePaging(true,13,5,"pagingArea",true,"infoArea");
		clientesAcederGrid.setPagingSkin("bricks");
		clientesAcederGrid.attachHeader(",#text_filter,#text_filter");
		clientesAcederGrid.init();
	}else{
		loadListClientesCedidosGrid();
		var agenteDestino = jQuery("#txtIdHiddenDestino").val();
		var agenteOrigen = jQuery("#txtIdHiddenOrigen").val();
		loadListClientes(agenteOrigen,agenteDestino);
	}
}

function loadListClientesCedidosGrid(){
	var idHerencia = jQuery("#idHerencia").val();
	var agenteDestinoId = jQuery("#txtIdHiddenDestino").val();
	var url="/MidasWeb/fuerzaventa/herenciaclientes/listarClientesHeredadosGrid.action";
	var data = "herenciaClientes.id="+idHerencia+"&herenciaClientes.agenteDestino.idAgente="+agenteDestinoId;
	clientesCedidosGrid = listarFiltradoGenerico(url+"?"+data,'clientesCedidosGrid',null,null,null);
}