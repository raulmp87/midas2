var gpoVarModifPrimaGrid;

function mostrarCatalogoGpoVarModPrima() {
	sendRequestJQ(null, mostrarCatalogoGpoVarModPrimaPathPath, targetWorkArea, 
				'listarFiltradoGpoVarModPrima();');
}

function esconderCampo(){
	var idClaveTipoDetalle = dwr.util.getValue("idClaveTipoDetalle");
	if (idClaveTipoDetalle=="0"){
		document.getElementById('idDetalle').style.display = 'none'; 
		document.getElementById('idDetalleE').style.display = 'none'; 
		document.gpoVarModPrimaForm[5].value="";
	}
	if (idClaveTipoDetalle=="1"){
		document.getElementById('idDetalle').style.display = 'block'; 
		document.getElementById('idDetalleE').style.display = 'block';
	}
}

function guardarGpoVarModPrimaForm() {
	var idClaveTipoDetalle = document.getElementById("idClaveTipoDetalle").value;
	var descDetalle = document.getElementById("txtDescripcionDetalle").value;
//	alert(idClaveTipoDetalle);
	if (idClaveTipoDetalle == 0){
		document.getElementById("txtDescripcionDetalle").value=" ";
		sendRequestJQ(null, guardarGpoVarModPrimaPath + "?" + 
				jQuery(document.gpoVarModPrimaForm).serialize(), targetWorkArea, 
				'listarFiltradoGpoVarModPrima();');
	}else{
		if (descDetalle=="" || descDetalle==null ){
			alert("Cuando el Grupo es Lista de Rangos, Descripci\u00F3n Detalle es Requerido");
		}else{
			sendRequestJQ(null, guardarGpoVarModPrimaPath + "?" + 
					jQuery(document.gpoVarModPrimaForm).serialize(), targetWorkArea, 
					'listarFiltradoGpoVarModPrima();');
		}
	}
}

function eliminarGpoVarModPrima() {
	if(confirm("Esta Seguro de Eliminar Registro?")) {
		sendRequestJQ(null, eliminarGpoVarModPrimaPathPath + "?"+ 
				jQuery(document.gpoVarModPrimaForm).serialize(), targetWorkArea, 
				'listarFiltradoGpoVarModPrima();');
	}
}

function listarFiltradoGpoVarModPrima(){
	gpoVarModifPrimaGrid = new dhtmlXGridObject("gpoVarModifPrimaGrid");
	var form = document.gpoVarModPrimaForm;
	if(form!=null){
		gpoVarModifPrimaGrid.load(listarFiltradoGpoVarModPrimaPath + "?" + 
				jQuery(document.gpoVarModPrimaForm).serialize());
	}else{
		gpoVarModifPrimaGrid.load(listarFiltradoGpoVarModPrimaPath);
	}
}

function verDetalleGpoVarModPrima (tipoAccion) {
	if(gpoVarModifPrimaGrid.getSelectedId() != null){
		var idgpoVarModifPrima = getIdFromGrid(gpoVarModifPrimaGrid, 0);
		sendRequestJQ(null, verDetalleGpoVarModPrimaPath + "?tipoAccion=" + 
				tipoAccion + "&id=" + idgpoVarModifPrima, targetWorkArea, null);
	}
}

function nuevoGpoVarModPrima (tipoAccion) {
	sendRequestJQ(null, verDetalleGpoVarModPrimaPath + "?tipoAccion=" + 
			tipoAccion, targetWorkArea, null);
}