//guardarConfigPrestamoAnticipo();
function buscaAgentePorIdAgente(){
	var idAgente = jQuery("#txtIdAgente").val();
	if(idAgente!=""){
		if(jQuery.isValid(idAgente)){
			var url="/MidasWeb/fuerzaventa/agente/findByClaveAgente.action";
			var data={"agente.idAgente":idAgente};
			jQuery.asyncPostJSON(url,data,loadInfoAgente);
		}
	}else{
		mostrarListadoAgentesSusp();
	}
}

function mostrarListadoAgentesSusp(){
	var idAgente = jQuery("#txtIdAgente").val();
	var field="id";
	var idTipoSituacionAgenteAUTORIZADO=null;
	dwr.engine.beginBatch();
	listadoService.getIdElementoValorCatalogoAgentes("Estatus de Agente (Situacion)","AUTORIZADO",
			function(id){
				idTipoSituacionAgenteAUTORIZADO=id;
			});
	dwr.engine.endBatch({async:false});
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field+"&filtroAgente.tipoSituacion.id="+idTipoSituacionAgenteAUTORIZADO;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}
}

function buscaAgentePorId(){
	var id = jQuery("#id").val();
	
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
		var data={"agente.id":id};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}	
}




function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}
function limpiaFormaBusqueda(){
	jQuery("#id").val("");
//	jQuery("#txtIdAgente").val("");
	jQuery("#estatusAgente").val("");
	jQuery("#descripcionCentroOperacion").val("");
	jQuery("#descripcionGerencia").val("");
	jQuery("#descripcionPromotoria").val("");
	jQuery("#nombreAgente").val("");
	jQuery("#rfcAgente").val("");
	jQuery("#ejecutivo").val("");
	jQuery("#tipoAgente").val("");
	jQuery("#numeroFianza").val("");
	jQuery("#venceFianza").val("");
	jQuery("#numeroCedula").val("");
	jQuery("#venceCedula").val("");
}
function loadInfoAgente(json){
	if(json.agente==null){		
		parent.mostrarMensajeInformativo("La clave del Agente no Existe, no es posible realizarle un Préstamo o Anticipo","10");
		limpiaFormaBusqueda();
	}else{
		var agente=json.agente;
		var estatus = agente.tipoSituacion.valor;
		
		 if(json.agente.tipoSituacion.valor!="AUTORIZADO"){
			 parent.mostrarMensajeInformativo("La Situación del Agente no es Autorizado, no es posible realizarle un Préstamo o Anticipo","10");
			 limpiaFormaBusqueda();
		 }else{			
			var id=agente.id;
			var idAgente=agente.idAgente;		
			var centroOperacion= agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion;
			var gerencia = agente.promotoria.ejecutivo.gerencia.descripcion;
			var promotoria = agente.promotoria.descripcion;
			var nombreAgente = agente.persona.nombreCompleto;
			var rfc = agente.persona.rfc;
			var ejecutivo = agente.promotoria.ejecutivo.personaResponsable.nombreCompleto;
			var tipoAgente = agente.idTipoAgente;
			var numeroFianza = agente.numeroFianza;
			var venceFianza = agente.vencimientoFianzaString;
			var numeroCedula= agente.numeroCedula;
			var venceCedula =agente.vencimientoCedulaString;
			
			jQuery("#id").val(id);
			jQuery("#txtIdAgente").val(idAgente);
			jQuery("#estatusAgente").val(estatus);
			jQuery("#descripcionCentroOperacion").val(centroOperacion);
			jQuery("#descripcionGerencia").val(gerencia);
			jQuery("#descripcionPromotoria").val(promotoria);
			jQuery("#nombreAgente").val(nombreAgente);
			jQuery("#rfcAgente").val(rfc);
			jQuery("#ejecutivo").val(ejecutivo);
			jQuery("#tipoAgente").val(tipoAgente);
			jQuery("#numeroFianza").val(numeroFianza);
			jQuery("#venceFianza").val(venceFianza);
			jQuery("#numeroCedula").val(numeroCedula);
			jQuery("#venceCedula").val(venceCedula);
		}
	}
	
}

function cargaDomicilioDelAgente(idAgente){
	var url="/MidasWeb/fuerzaventa/agente/domicilioAgente.action";
	var data={"agente.idAgente":idAgente};
	jQuery.asyncPostJSON(url,data,loadInfoDomicilio);
}
function loadInfoDomicilio(json){
	
	var idDomicilio = json.datosDomicilio.idDomicilio;
	var calleNumero = json.datosDomicilio.calleNumero;
	var ciudad = json.datosDomicilio.ciudad;
	var codigoPostal = json.datosDomicilio.codigoPostal;
	var colonia = json.datosDomicilio.colonia;
	var estado = json.datosDomicilio.estado;
	
	var domicilioCompleto = calleNumero +" "+ colonia +" "+ ciudad +" "+ estado +" C.P. "+ codigoPostal;
	jQuery("#domicilioCompleto").text(domicilioCompleto);
}

var pagaresGrid;
function cargaPagaresGrid(){
	var url="/MidasWeb/prestamos/prestamosAnticipos/cargaPagaresGrid.action?prestamoAnticipo.id="+dwr.util.getValue("prestamoAnticipo.id");
	document.getElementById('pagaresGrid').innerHTML = '';
	pagaresGrid = new dhtmlXGridObject('pagaresGrid');
	var browserName=navigator.appName;
	if (browserName=="Microsoft Internet Explorer") {
		pagaresGrid.attachFooter("Totales,#cspan,#cspan,#cspan,<div id='totIntOrd'>0</div>,<div id='totIntMor'>0</div>,<div id='totAbonoCap'>0</div>,<div id='totIva'>0</div>,<div id='totImpPago'>0</div>,Por Pagar:,<div id='totImpPorPagar' style='color:green;'>0</div>,#cspan,#cspan", ["text-align:right;height:30px;"]);
	}else{
		pagaresGrid.attachFooter("Totales,#cspan,#cspan,<div id='totIntOrd'>0</div>,<div id='totIntMor'>0</div>,<div id='totAbonoCap'>0</div>,<div id='totIva'>0</div>,<div id='totImpPago'>0</div>,Por Pagar:,<div id='totImpPorPagar' style='color:green;'>0</div>,#cspan,#cspan", ["text-align:right;height:30px;"]);
	}
	
	pagaresGrid.attachEvent("onCheckbox", activarPagare);
	pagaresGrid.attachEvent("onXLE",funcionesTerminaCargarGrid);	
	pagaresGrid.load(url);
	
}

function funcionesTerminaCargarGrid(){
	//funcion para recalcular los pagos en caso de haberse pasado la fecha de pago se determinan los cargos moratorios y se pone estatus de vencido al pagare 
	recalculoAbonoXPagare();
	//funcion que calcula los totales que se encuentran en el footer del grid
	calculateFooterValues();	
	
	//funcione para deshabilitar los checkbox que ya se hayan marcado anteriormente esto para que no se puedan volver a desactivar/activar ya que ese pagare ya se encuentra registrado en estados de cuenta
	var checked=pagaresGrid.getCheckedRows(12);	
	if(checked){
		var arr = checked.split(",");
		for(i=0;i<arr.length;i++){
			pagaresGrid.cellById(arr[i],12).setDisabled(true);		
		}
	}
	
	var estatusMovimiento = jQuery("#estatusMovimiento :selected").text();
	if(estatusMovimiento=="CANCELADO"){
		pagaresDeshabilitados();
	}
	
}

function revisaCapturaCalculo(){
	var tipoMovimiento = jQuery("#tipoMovimiento").val();
	var importeOtorgado = jQuery("#importeOtorgado").val();
	var plazo = jQuery("#plazo").val();
	var fecIniCalculo = jQuery("#fecIniCalculo").val();
	var numeroPagos = jQuery("#numeroPagos").val();
	var pcteInteresOrdinario = jQuery("#pcteInteresOrdinario").val();
	var plazoInteres = jQuery("#plazoInteres").val();
	var interesMoratorio = jQuery("#interesMoratorio").val();
	var error="";
	
	if(tipoMovimiento=="" || tipoMovimiento==0){
		error+="Seleccione el Tipo de Movimiento \n ";
	}
	if(importeOtorgado=="" || importeOtorgado==0){
		error+="Capture el Importe Otorgado \n ";
	}
	if(plazo=="" || plazo==0){
		error+="Seleccione el Plazo \n ";
	}
	if(fecIniCalculo==""){
		error+="Capture la Fecha Inicio de Calculo \n ";
	}
	if(numeroPagos=="" || numeroPagos==0){
		error+="Capture el Número de Pagos \n ";
	}
	if(pcteInteresOrdinario>0 ){
		if(plazoInteres=="" || plazoInteres==0){
			error+="Seleccione el Plazo del Interes \n ";
		}
	}else{
		jQuery("#plazoInteres").val("");
	}

//	if(interesMoratorio==""){
//		error+="Capture el Interes Moratorio \n ";
//	}
	
	if(error!=""){
		alert("Indicaciones para Calculo \n" +error);
		return false;
	}else{
		return true;
	}
	
}

 /* Valores globales de año comercial*/
var diario = 1;
var semanal = 7;
var quincenal = 15;
var mensual = 30;
var bimestral = 60;
var trimestral = 90;
var semestral = 180;
var anual = 360;
var fechaHoy = fechaActual();

/*funcion que calcula los pagares*/
function calcularPagares(){	

	var idPrestamo=jQuery("#prestamoAnticipo_id").val();
	
	pagaresGrid.clearAll();
	if(revisaCapturaCalculo()){
		/*****************************************************
		   * Si la periodicidad del interés ordinario es diferente a la periodicidad del pago será necesario sacar la proporción del interés antes de calcular el importe del interés de la siguiente manera:
			% de Interés ordinario a aplicar por pagaré = i
			% de Interés ordinario ingresado = Y
			Plazo de Interés = P
			Plazos (de pago) = n
			i = ( Y  *  P ) / n
		 *****************************************************/
		var plazoText = jQuery("#plazo :selected").text();
		var plazoInteresText = jQuery("#plazoInteres :selected").text();
		var plazoInteres = 0;
		var plazo=0;		
		
		if(plazoInteresText == "Diario")     { plazoInteres = diario;    }
		if(plazoInteresText == "Semanal")    { plazoInteres = semanal;   }
		if(plazoInteresText == "Quincenal")  { plazoInteres = quincenal; }
		if(plazoInteresText == "Mensual")    { plazoInteres = mensual;   }
		if(plazoInteresText == "Bimestral")  { plazoInteres = bimestral; }
		if(plazoInteresText == "Trimestral") { plazoInteres = trimestral;}
		if(plazoInteresText == "Semestral")  { plazoInteres = semestral; }
		if(plazoInteresText == "Anual")      { plazoInteres = anual;  	 }
		
		if(plazoText == "Diario")     { plazo = diario    }
		if(plazoText == "Semanal")    { plazo = semanal   }
		if(plazoText == "Quincenal")  { plazo = quincenal }
		if(plazoText == "Mensual")    { plazo = mensual   }
		if(plazoText == "Bimestral")  { plazo = bimestral }
		if(plazoText == "Trimestral") { plazo = trimestral}
		if(plazoText == "Semestral")  { plazo = semestral }
		if(plazoText == "Anual")      { plazo = anual 	  }	
		/**************************************************************
		    Importe de Pago: El cálculo de la amortización se deberá ejecutar de la siguiente manera:
			Importe de Pago = A
			Importe Otorgado = VP
			Interés Ordinario = i
			Plazos = n
			Interés Moratorio = M
			Formula = A = [VP x [ i / [ 1 – ( 1 + i )^-n ] ]] + M + IVA
		 *************************************************************/	
		var importeOtorgado = jQuery("#importeOtorgado").val();
		var fechaInicio = jQuery("#fecIniCalculo").val();		
		var numeroPagos = jQuery("#numeroPagos").val();		
		var IVA = 16 / 100;
		
		var capitalInicial = importeOtorgado;
		var ivaXPagare = 0;
		var interesXPagare=0;	
		var importePago = 0;
	//	var importeInteresOrdinario = 0;	
		var abonoCapital = 0;
		var capitalFinal=0;
		var cargosMoratorios=0;
		var interesOrdinarioXPagare = 0;	
		var importeTotalDeInteres = 0;
		var importePagoTotal = 0;	
		var pcteInteresOrdinario=0;
		var interesMoratorio = 0;
		
		
		pcteInteresOrdinario = jQuery("#pcteInteresOrdinario").val() / 100;
		interesMoratorio = jQuery("#interesMoratorio").val() / 100;
		
		if(plazoText != plazoInteresText){				
//			interesXPagare = (pcteInteresOrdinario  *  plazoInteres ) / plazo;
			interesXPagare = (pcteInteresOrdinario  *  plazo ) / plazoInteres;
		}else{		
			interesXPagare = pcteInteresOrdinario;
		}	
		
		for(increm=1; increm<=numeroPagos; increm++){			
	
			mifecha = recalcFechas(fechaInicio,plazo);		
			
			if(interesXPagare>0){
				interesOrdinarioXPagare = capitalInicial * interesXPagare;
				// IVA = I * ( %IVA / 100 )
				ivaXPagare = interesOrdinarioXPagare * IVA;	
				
				importePago = (importeOtorgado) * ((interesXPagare) / (1 - Math.pow((1 + interesXPagare),-numeroPagos))) + ivaXPagare;
				
				//Abono a Capital =	Importe de Pago – Interés Ordinario
				abonoCapital = importePago - interesOrdinarioXPagare - ivaXPagare;		
				
			    // Capital Final =Capital Inicial – Abono al Capital
				capitalFinal = capitalInicial - abonoCapital;
			}else{
				importePago = importeOtorgado/numeroPagos;
				
				//Abono a Capital =	Importe de Pago – Interés Ordinario
				abonoCapital = importePago;		
				
			    // Capital Final =Capital Inicial – Abono al Capital
				capitalFinal = capitalInicial - abonoCapital;
			}
			
			
			
			
	//		var diasDiferencia = DiferenciaFechas(fechaHoy,mifecha);
	//		if(diasDiferencia > 0){
	//			// interes moratorio = (Capital vencido x días vencidos x tasa de interés moratoria) / 360
	//			cargosMoratorios = (importePago * diasDiferencia * interesMoratorio) / 360;
	//		}else{
	//			cargosMoratorios=0;
	//		}		
	//		importePago = importePago + cargosMoratorios;
			
			
			pagaresGrid.addRow(increm,[increm, ,mifecha,roundNumber(capitalInicial,2),roundNumber(interesOrdinarioXPagare,2),0.00,roundNumber(abonoCapital,2),roundNumber(ivaXPagare,2),roundNumber(importePago,2),roundNumber(capitalFinal,2),roundNumber(importePago,2),"PENDIENTE", ,"../img/b_printer.gif^Imprimir^javascript: imprimirPagare("+increm+")^_self", ]);
			pagaresGrid.cellById(increm,12).setDisabled(true);	
			fechaInicio = mifecha;
			capitalInicial = capitalFinal;
			
			importeTotalDeInteres += interesOrdinarioXPagare;
			importePagoTotal += importePago;
			
			if(idPrestamo==""){
				pagaresGrid.setColumnHidden(13,true);				
			}
		}
		jQuery("#importeInteresOrdinario").val(roundNumber(importeTotalDeInteres,2));
		jQuery("#importePago").val(roundNumber(importePagoTotal,2));	
		jQuery("#btnGuardar").css("display","block");
		calculateFooterValues();		
	}

}

function calculateFooterValues() {
    jQuery("#totIntOrd").text(sumColumn(4));   
    jQuery("#totIntMor").text(sumColumn(5));   
    jQuery("#totAbonoCap").text(sumColumn(6));   
    jQuery("#totIva").text(sumColumn(7));   
    jQuery("#totImpPago").text(sumColumn(8)); 
    jQuery("#totImpPorPagar").text(sumColumnTotalAPagar(10));
}
function sumColumn(ind) {
    var out = 0;
    for (var i = 0; i < pagaresGrid.getRowsNum(); i++) {
        out += parseFloat(pagaresGrid.cells2(i, ind).getValue());
    }
    return roundNumber(out,2);
}

function sumColumnTotalAPagar(ind) {
    var out = 0;
    for (var i = 0; i < pagaresGrid.getRowsNum(); i++) {
    	if(pagaresGrid.cells2(i, 11).getValue() == "VENCIDO" || pagaresGrid.cells2(i, 11).getValue() == "ACTIVO"){
    		out += parseFloat(pagaresGrid.cells2(i, ind).getValue());    		
    	}
    }
    return roundNumber(out,2);
}

function recalculoAbonoXPagare(){
	for (var i = 0; i < pagaresGrid.getRowsNum(); i++) {
		var out = 0;		
		var cargosMoratorios=0;
		var fechaPago = pagaresGrid.cells2(i, 2).getValue();
		var importePago = pagaresGrid.cells2(i, 8).getValue();
		var interesMoratorio = jQuery("#interesMoratorio").val() / 100;
		var estatus = pagaresGrid.cells2(i, 11).getValue();
		var fecha=fechaActual();
		if(estatus != "PAGADO"){		
			var diasDiferencia = DiferenciaFechas(fecha,fechaPago);
			if(diasDiferencia > 0){
				// interes moratorio = (Capital vencido x días vencidos x tasa de interés moratoria) / 360
				cargosMoratorios = (importePago * diasDiferencia * interesMoratorio) / 360;
				pagaresGrid.cells2(i, 11).setValue("VENCIDO");
			}
			
			out = parseFloat(importePago) + parseFloat(cargosMoratorios);		
			pagaresGrid.cells2(i,5).setValue(roundNumber(cargosMoratorios,2));
			pagaresGrid.cells2(i,10).setValue(roundNumber(out,2));
		}
	}
}

/**********************************************************************************************************************************
 *bloque de funciones para incrementar o decrementar dias a una fecha (en formato de año comercial todos los meses de 30 dias)
 *@cavalos
 *la funcion que debemos ejecutar en nuestros eventos es:
 *recalcFechas(fecInicio,increm)
 *donde.
 *fecInicio = fecha a la que se quiere incrementar /decrementar dias.
 *increm = valor numerico a incremetar o decrementar
 *(para decrementar indicar signo (-)negativo antes del valor)
********************************************************************/
function recalcFechas(fecInicio,increm){
   var  fec = addToDate(fecInicio, increm);
   return  fec;
}

//var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var aFinMes = new Array(30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30);
	

function finMes(nMes, nAno){
	return aFinMes[nMes - 1] + (((nMes == 2) && (nAno % 4) == 0)? 1: 0);
}

function padNmb(nStr, nLen, sChr){
    var sRes = String(nStr);
    for (var i = 0; i < nLen - String(nStr).length; i++)
     sRes = sChr + sRes;
    return sRes;
}

function makeDateFormat(nDay, nMonth, nYear){
    var sRes;
    sRes = padNmb(nDay, 2, "0") + "/" + padNmb(nMonth, 2, "0") + "/" + padNmb(nYear, 4, "0");
    return sRes;
}

function incDate(sFec0){
   var nDia = parseInt(sFec0.substr(0, 2), 10);
   var nMes = parseInt(sFec0.substr(3, 2), 10);
   var nAno = parseInt(sFec0.substr(6, 4), 10);
   nDia += 1;
   if (nDia > finMes(nMes, nAno)){
    nDia = 1;
    nMes += 1;
    if (nMes == 13){
     nMes = 1;
     nAno += 1;
    }
   }
   return makeDateFormat(nDia, nMes, nAno);
}

function decDate(sFec0){
   var nDia = Number(sFec0.substr(0, 2));
   var nMes = Number(sFec0.substr(3, 2));
   var nAno = Number(sFec0.substr(6, 4));
   nDia -= 1;
   if (nDia == 0){
    nMes -= 1;
    if (nMes == 0){
     nMes = 12;
     nAno -= 1;
    }
    nDia = finMes(nMes, nAno);
   }
   return makeDateFormat(nDia, nMes, nAno);
}

function addToDate(sFec0, sInc){
   var nInc = Math.abs(parseInt(sInc));
   var sRes = sFec0;
   if (parseInt(sInc) >= 0)
    for (var i = 0; i < nInc; i++) sRes = incDate(sRes);
   else
    for (var i = 0; i < nInc; i++) sRes = decDate(sRes);
   return sRes;
}
/********************************************************************************************************************************/
function fechaActual(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;//January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	fecha = dd+'/'+mm+'/'+yyyy; 
	return fecha;
}

function DiferenciaFechas (CadenaFecha1,CadenaFecha2) {  
	    var nDia1 = Number(CadenaFecha1.substr(0, 2));
	   	var nMes1 = Number(CadenaFecha1.substr(3, 2));
	   	var nAno1 = Number(CadenaFecha1.substr(6, 4));
	   	
	   	var nDia2 = Number(CadenaFecha2.substr(0, 2));
	   	var nMes2 = Number(CadenaFecha2.substr(3, 2));
	   	var nAno2 = Number(CadenaFecha2.substr(6, 4));
	   
	   //Obtiene objetos Date  
	   var miFecha1 = new Date( nAno1, nMes1, nDia1 ); 
	   var miFecha2 = new Date( nAno2, nMes2, nDia2 );  
	  
	   //Resta fechas y redondea  
	   var diferencia = miFecha1.getTime() - miFecha2.getTime();  
	   var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));  
	   var segundos = Math.floor(diferencia / 1000);
//	   alert ('La diferencia es de ' + dias + ' dias,\no ' + segundos + ' segundos.')  
	     
	   return dias;
	}  
	  
function roundNumber(num, dec) {
	var result = 0;
	if(num > 0 || num < 0){
		result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);		
	}
	return result;
}



function guardarConfigPrestamoAnticipo(){
	pagaresGrid.setCSVDelimiter(',');	
	var listaPagares=pagaresGrid.serializeToCSV();
	var estatus = jQuery("#estatusMovimiento").val();
	var numeroPagos = jQuery("#numeroPagos").val();
	var numPagares = pagaresGrid.getRowsNum();
	
	if(numeroPagos==numPagares){
		if(validateAll(true)){
			var url="/MidasWeb/prestamos/prestamosAnticipos/guardar.action?"+jQuery("#prestamosForm").serialize()+"&pagares="+listaPagares+"&prestamoAnticipo.estatus.id="+estatus;
			sendRequestJQ(null,url,targetWorkArea,null);
			
			jQuery("#btnAutorizar").css("display","block");
			jQuery("#btnTablaAmortizacion").css("display","block");
			jQuery("#btnPagarePrincipal").css("display","block");
			jQuery("#btnCancelar").css("display","block");	
		}
	}else{
		alert("El número de pagos no coincide con el número de pagares calculados");
	}
}

function $_autorizar(json){
	
	var idAgente = jQuery("#txtIdAgente").val();
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	var id = jQuery("#id").val();
	//tambien se activara el primer pagare
	var urlParams = activarPrimerPagare(1,11,true);
	if (idAgente != null) {
		var url="/MidasWeb/prestamos/prestamosAnticipos/autorizarMovimiento.action?prestamoAnticipo.agente.id=" + id + "&prestamoAnticipo.id="+idMovimiento + "&prestamoAnticipo.agente.idAgente=" + idAgente+urlParams;
	}else {
		var url="/MidasWeb/prestamos/prestamosAnticipos/autorizarMovimiento.action?prestamoAnticipo.agente.id=" + id + "&prestamoAnticipo.id="+idMovimiento+urlParams;
	}
	sendRequestJQ(null,url,targetWorkArea,null);
		
}

function dateFormatMex(fecha){
	if(fecha!=null){
		var date = new Date(fecha);
//		date=toDate(date);
		var d = (date.getDate()<10)?"0"+date.getDate():date.getDate();
		var m = ((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
		var y = date.getFullYear(); 
		return d+"/"+m+"/"+y;
	}else{
		return null;
	}
}


function mostrTablaAmortizacion(){
		var url="/MidasWeb/prestamos/prestamosAnticipos/imprimirTablaAmortizacion.action?prestamoAnticipo.id="+dwr.util.getValue("prestamoAnticipo.id");
//		sendRequestWindow(null, url, obtenerVentanaTablaAmortizacion);
		window.open(url, "Contrato");
}

//function obtenerVentanaTablaAmortizacion(){
//	var wins = obtenerContenedorVentanas();
//	ventanaTablaAmortizacion = wins.createWindow("agregPoliza", 10, 320, 950, 300);
//	ventanaTablaAmortizacion.centerOnScreen();
//	ventanaTablaAmortizacion.setModal(true);
//	ventanaTablaAmortizacion.setText("Tabla de Amortización");
//	return ventanaTablaAmortizacion;
//}
//
//var pagareTablaAmortizacion;
//function cargaPagaresGridTablaAmortizacion(){
//	var url="/MidasWeb/prestamos/prestamosAnticipos/cargaPagaresGrid.action?prestamoAnticipo.id="+dwr.util.getValue("prestamoAnticipo.id");
//	document.getElementById('pagareTablaAmortizacion').innerHTML = '';
//	pagareTablaAmortizacion = new dhtmlXGridObject('pagareTablaAmortizacion');
//	pagareTablaAmortizacion.load(url);
//}

function solicitarChequeDeMovimiento(){
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	if (idMovimiento != null) {
		var url="/MidasWeb/prestamos/prestamosAnticipos/solicitatChequeDeMovimiento.action?prestamoAnticipo.id="+idMovimiento;
		sendRequestJQ(null,url,targetWorkArea,null);
	}
}

function autorizarMovimiento(){	
//	var id = jQuery("#id").val();
//	var url="/MidasWeb/fuerzaventa/agente/matchNDocumentosMismo.action";//matchDocumentosGenerico
//	var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"SOLICITUD DE MOVIMIENTO","nombreCarpeta":"PRESTAMOS","nombreGaveta":"AGENTES"};
//	jQuery.asyncPostJSON(url,data,$_autorizar);	
	
	var id = jQuery("#id").val();
	var idPrestamo = jQuery("#prestamoAnticipo_id").val();	
	var url = "/MidasWeb/prestamos/prestamosAnticipos/auditarDocumentosPrestamosDigitalizar.action?prestamoAnticipo.id="+idPrestamo+"&prestamoAnticipo.agente.id="+id;
	jQuery.asyncPostJSON(url,null,$_autorizar);	
}

function cancelarMovimiento(){
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	if(confirm("Eliminar Movimiento?")){
		var url="/MidasWeb/prestamos/prestamosAnticipos/cancelarMovimiento.action?prestamoAnticipo.id="+idMovimiento;
		sendRequestJQ(null,url,targetWorkArea,null);
	}
}

function controlDeBotones(){
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	var estatusMovimiento = jQuery("#estatusMovimiento :selected").text();
	if(idMovimiento != ""){
		if(estatusMovimiento == "PENDIENTE"){
			jQuery("#btnCalcularPagares").css("display","block");
			jQuery("#btnAutorizar").css("display","block");
			jQuery("#btnTablaAmortizacion").css("display","block");
			jQuery("#btnPagarePrincipal").css("display","block");
			jQuery("#btnGuardar").css("display","block");
			jQuery("#btnCancelar").css("display","block");
			pagaresDeshabilitados();
			
		}
		if(estatusMovimiento == "AUTORIZADO"){			
			jQuery("#btnSolicitarCheque").css("display","block");
			jQuery("#btnTablaAmortizacion").css("display","block");
			jQuery("#btnPagarePrincipal").css("display","block");
		}
		if(estatusMovimiento == "APLICADA"){
			jQuery("#btnTablaAmortizacion").css("display","block");
			jQuery("#btnPagarePrincipal").css("display","block");
		}
	}else{
		jQuery("#btnCalcularPagares").css("display","block");		
	}
	
}

function activarPagare(rowId,columInd,state){
	
	if(state == true){
		pagaresGrid.cellById(rowId, 11).setValue("ACTIVO");
		jQuery("#totImpPorPagar").text(sumColumnTotalAPagar(10));
	}else{
		pagaresGrid.cellById(rowId, 11).setValue("PENDIENTE");
		jQuery("#totImpPorPagar").text(sumColumnTotalAPagar(10));
	}
	pagaresGrid.setCSVDelimiter(',');	
	var listaPagares=pagaresGrid.serializeToCSV();
	var abonoTotalPagare = jQuery("#totImpPorPagar").text();
	var idPagareActivado=pagaresGrid.cellById(rowId, 1).getValue();
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	var url="/MidasWeb/prestamos/prestamosAnticipos/activarPagare.action?prestamoAnticipo.id="+idMovimiento+"&pagares="+listaPagares+"&pagarePrestamoAnticipo.id="+idPagareActivado+"&pagarePrestamoAnticipo.abonoPorPagare="+abonoTotalPagare;
	sendRequestJQ(null,url,targetWorkArea,null);	
}

function activarPrimerPagare(rowId,columInd,state){
	
	if(state == true){
		pagaresGrid.cellById(rowId, 11).setValue("ACTIVO");
		jQuery("#totImpPorPagar").text(sumColumnTotalAPagar(10));
	}
	pagaresGrid.setCSVDelimiter(',');	
	var listaPagares=pagaresGrid.serializeToCSV();
	var abonoTotalPagare = jQuery("#totImpPorPagar").text();
	var idPagareActivado=pagaresGrid.cellById(rowId, 1).getValue();
	var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	var urlParams="&pagares="+listaPagares+"&pagarePrestamoAnticipo.id="+idPagareActivado+"&pagarePrestamoAnticipo.abonoPorPagare="+abonoTotalPagare;
	return	urlParams;
}


function generarLigaIfimaxPrestamo(){
	var idAgente = jQuery("#id").val();
	var url='/MidasWeb/prestamos/prestamosAnticipos/generarLigaIfimax.action?prestamoAnticipo.agente.id='+idAgente;
	window.open(url,'Fortimax');
}

//function guardarDocumentosFortimax(){
//	var url="/MidasWeb/fuerzaventa/agente/guardarDocumentos.action?"+jQuery("#agenteDocumentosForm").serialize();
//	var data;
//	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantes);
//}

function imprimirPagare(idPagare){
	var url="/MidasWeb/prestamos/prestamosAnticipos/imprimirPagare.action?pagarePrestamoAnticipo.id="+idPagare;
	window.open(url, "Pagare");
}

function imprimirPagarePrincipal(){
	//se crea estructura en fortimax para el documento CARTA DE ACUERDO
//	var url="/MidasWeb/fuerzaventa/agente/mostrarNDocumentosMismo.action";
//	var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"CARTA DE ACUERDO","nombreCarpeta":"PRESTAMOS","nombreGaveta":"AGENTES"};
//	crearEstructuraAgente(url, data);
    var idMovimiento = jQuery("#prestamoAnticipo_id").val();
	var url="/MidasWeb/prestamos/prestamosAnticipos/imprimirPagarePrincipal.action?prestamoAnticipo.id="+idMovimiento;	
	window.open(url, "Pagare_Principal");
	
}

function salirPrestamo(){
	
	var url = "/MidasWeb/prestamos/prestamosAnticipos/mostrarContenedor.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}
		
}


function auditarDocumentosPrestamo(){
//	var url="/MidasWeb/prestamos/prestamosAnticipos/matchDocumentos.action?"+jQuery("#prestamosForm").serialize();
//	var data="";
//	jQuery.asyncPostJSON(url,data,populateDocumentosFortimax);

	var id = jQuery("#id").val();
	var url="/MidasWeb/fuerzaventa/agente/matchNDocumentosMismo.action";
	var data={"agente.idAgente":"","agente.id":id, "nombreDocumento":"","nombreCarpeta":"CARGOS","nombreGaveta":"AGENTES"};
	jQuery.asyncPostJSON(url,data,$_populateDocumentosFortimax);
}

function populateDocumentosFortimax(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#prestamosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
			});
		}
	}
}

function generaEstructuraFortimax(){

	var id = jQuery("#id").val();
	var numPagares = pagaresGrid.getRowsNum();	
	var idPrestamo = jQuery("#prestamoAnticipo_id").val();
	var tipoMovimiento = jQuery("#tipoMovimiento").val();
	
	var url="/MidasWeb/prestamos/prestamosAnticipos/generarDocPrestamosEnFortimax.action";
	var data={"prestamoAnticipo.id":idPrestamo,"prestamoAnticipo.agente.id":id,"prestamoAnticipo.tipoMovimiento.id":tipoMovimiento,"numPagares":numPagares};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizar);	
	jQuery(".js_habilita").css("display","block");
}

function muestraDocumentosADigitalizar(){
	var id = jQuery("#id").val();	
	var idPrestamo = jQuery("#prestamoAnticipo_id").val();
	var url="/MidasWeb/prestamos/prestamosAnticipos/mostrarDocumentosADigitalizar.action";
	var data={"prestamoAnticipo.id":idPrestamo,"prestamoAnticipo.agente.id":id};
	jQuery.asyncPostJSON(url,data,muestraHtmlDocDigitalizar);
}

function muestraHtmlDocDigitalizar(json){
	var datos = json.listaDocumentosFortimaxView;	
	var estructura = "<table class='contenedorConFormato w800'>";	
	var val;
	var entregado;
	for(i=0;i<datos.length;i++){				
		val=datos[i].valor;		
		if(datos[i].checado==0){
			entregado="NO DIGITALIZADO";
		}else{
			entregado="DIGITALIZADO";
		}
		estructura+="<tr><td>"+val+"</td><td>"+entregado+"</td>";
//		if(datos[i].checado==0){			
			estructura+="<td><div class='btn_back w180'><a href='javascript: void(0);' id='"+val+"' class='icon_imprimir btn_digitalizarDoc' onclick='generarLigaIfimaxPrestamosFortimax(this);'>Digitalizar Documento</a></div></td>";
//		}
		estructura+="</tr>";
	}
	estructura+="</table>";
//	alert(estructura);
	jQuery("#muestraDocumentosADigitalizar").html(estructura);
}

function generarLigaIfimaxPrestamosFortimax(nombreDocumento){
	var id = jQuery("#id").val();
	var nombreDoc = nombreDocumento.id;
	var url='/MidasWeb/prestamos/prestamosAnticipos/generarLigaIfimaxPrestamos.action?prestamoAnticipo.agente.id='+id+'&agente.persona.claveTipoPersona='+dwr.util.getValue("agente.persona.claveTipoPersona")+'&nombreDocumento='+nombreDoc;
	window.open(url,'Fortimax');
}


function auditarDocumentosPrestamosDigitalizados(){
	var id = jQuery("#id").val();
	var idPrestamo = jQuery("#prestamoAnticipo_id").val();
	
	var url = "/MidasWeb/prestamos/prestamosAnticipos/auditarDocumentosPrestamosDigitalizar.action?prestamoAnticipo.id="+idPrestamo+"&prestamoAnticipo.agente.id="+id;
	jQuery.asyncPostJSON(url,null,muestraHtmlDocDigitalizar);
}

function pagaresDeshabilitados(){
	var checked=pagaresGrid.getAllRowIds(",");			
	if(checked){
		var arr = checked.split(",");
		for(i=0;i<arr.length;i++){
			pagaresGrid.cellById(arr[i],12).setDisabled(true);		
		}
	}
}


function noPermiteFechasPasadas(fec){
	var fecha = fec.value;
	var fechaHoy=new Date();	
	var dd = fechaHoy.getDate();
	var mm = fechaHoy.getMonth()+1;//January is 0!
	var yyyy = fechaHoy.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	var today=dd+'/'+mm+'/'+yyyy; 
	if(fecha<today){
		alert("Fecha invalida");
		jQuery("#fecIniCalculo").val("");
	}
}

