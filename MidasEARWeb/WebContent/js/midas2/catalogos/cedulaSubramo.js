/**
 *  Id's asociados a los Grids
 */
var ramoSubramoAsociadosGrid;
var ramoSubramoDisponiblesGrid;


function asociaRamosSubramos(){

	var idselected = grid.getSelectedRowId();
	if(idselected != null){
		sendRequestJQ(null, asociacionRamoSubramoPath+"?cedula.id=" + idselected+"&cedulaSubramo.tipoCedula.id="+idselected,targetWorkArea);
	}else{
		alert("Seleccione un Registro");
	}
}
function obtenerRamosAsociados(){
	var idCedula = jQuery("#txtIdCedula").val();
	if(idCedula == ""){
		idCedula = 0;
	}
	document.getElementById("ramoSubramoAsociadosGrid").innerHTML = '';
	ramoSubramoAsociadosGrid = new dhtmlXGridObject('ramoSubramoAsociadosGrid');
	ramoSubramoAsociadosGrid.load(obtenerRamosSubramosAsociadosPath + "?tipoCedula.id=" + idCedula);		

}

function loadSubramosPorRamos(idRamo){
	if(idRamo == null || idRamo == ""){
		idRamo = 0;
	}
	document.getElementById("ramoSubramoDisponiblesGrid").innerHTML = '';
	ramoSubramoDisponiblesGrid = new dhtmlXGridObject('ramoSubramoDisponiblesGrid');
	ramoSubramoDisponiblesGrid.load(obtenerSubramosPorRamoPath + "?ramo.idTcRamo=" + idRamo);
	noRepetirAsociacion();
}

function refrescarGrids(){
	obtenerRamosAsociados();
	loadSubramosPorRamos();
	return true;
}

function iniciaGrids(){	
	refrescarGrids();
}


function guardarAsociacionSubRamos(){
	var idCedula = jQuery("#txtIdCedula").val();
	var listaIdsSubRamos = ramoSubramoAsociadosGrid.getAllRowIds();
	
	var arr = new Array();
	var strParam="";
	var arrIdsSubRamos = listaIdsSubRamos.split(",");
		for(i=0;i<=arrIdsSubRamos.length;i++){
			var params = {"idTcSubRamo":arrIdsSubRamos[i]};
			arr.push(params);
		}	
		
	strParam = jQuery.convertJsonArrayToParams(arr,["idTcSubRamo"],"listaSubRamos");	
	var data = "tipoCedula.id="+idCedula+"&"+strParam;
	var url =guardarAsociacionCedulaSubRamosPath;
	
	jQuery.asyncPostJSON(url,data,responseAsociacion);	
	
}

function responseAsociacion(json){
	
	var mensaje = json.mensaje;
	alert(mensaje);	
}

function noRepetirAsociacion(){
	ramoSubramoDisponiblesGrid.attachEvent("onXLE", function(grid_obj,count){
		var subRamosAsociados = ramoSubramoAsociadosGrid.getAllRowIds();
		var arr = subRamosAsociados.split(",");
		for(i=0;i<=arr.length;i++){
			ramoSubramoDisponiblesGrid.setRowHidden(arr[i],true);
		}	
	}); 	
}

function guardarCedula() {
	if(validateAll(true)){
		sendRequestJQ(null, guardarCedulaPath+"?"+ jQuery(document.cedulaSubramoForm).serialize(),targetWorkArea);
	}	
}

function eliminarCedula(){
	if(confirm('Esta seguro de eliminar este tipo de cedula')){
		sendRequestJQ(null,eliminarCedulaSubRamosPath+"?"+ jQuery(document.cedulaSubramoForm).serialize(),targetWorkArea);
	}
}