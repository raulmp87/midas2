var afianzadoraGrid;

function salirAfianzadora(){
	
	var url = "/MidasWeb/fuerzaventa/afianzadora/mostrarContenedor.action";
//	if(parent.dwr.util.getValue("afianzadora.id")!=null && parent.dwr.util.getValue("afianzadora.id")!=''){
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}
		
}

function listarFiltradoAfianzadora(){	

	afianzadoraGrid = new dhtmlXGridObject("afianzadoraGrid");
	var form = document.afianzadoraForm;
	if(form!=null){
		afianzadoraGrid.load(listarFiltradoAfianzadoraPath+"?"+jQuery(document.afianzadoraForm).serialize());
	}else{
		afianzadoraGrid.load(listarFiltradoAfianzadoraPath);
	}
}

function agregarAfianzadora(tipoAccion) {
	sendRequestJQ(null, verDetalleAfianzadoraPath + "?tipoAccion=" + tipoAccion, targetWorkArea, 'null');	
	
}

function guardarAfianzadora() {
		if(validateAll(true)){
			sendRequestJQ(null, "/MidasWeb/fuerzaventa/afianzadora/guardarAfianzadora.action?"
					+ jQuery(document.afianzadoraForm).serialize(),
					targetWorkArea);
		}		
	
}
function eliminarAfianzadora(){

	if(confirm("Esta seguro de Eliminar el Registo")){
		sendRequestJQAsync(null, "/MidasWeb/fuerzaventa/afianzadora/eliminar.action?"
			+ jQuery(document.afianzadoraForm).serialize(),
			targetWorkArea, null);
		ocultaBtnEliminar();
	}
}

function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/afianzadora/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	parent.mostrarVentanaModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}
//function obtenerHistorico(){
//var url = '/MidasWeb/fuerzaVenta/persona/loadHistory.action?idTipoOperacion='+dwr.util.getValue("idTipoOperacion")+ "&idRegistro="+dwr.util.getValue("idRegistro");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}

function populateDomicilio(json){
	if(json){
		var estadoName="afianzadora.domicilio.claveEstado";
		var paisName="afianzadora.domicilio.clavePais";
		var ciudadName="afianzadora.domicilio.claveCiudad";
		var coloniaName="afianzadora.domicilio.nombreColonia";
		var calleNumeroName="afianzadora.domicilio.calleNumero";
		var codigoPostalName="afianzadora.domicilio.codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}


function mostrarOcultarFechaInactivo(){
	var situacion = jQuery("#situacion").val();
	var tipoAccion = jQuery("#tipoAccion").val();
	if(tipoAccion==3){
		jQuery("#txtFechaBaja").val(jQuery("#txtFechaActual").val());
	}else{
		if(situacion==1){
			jQuery("#txtFechaBaja").val("");	
		}
		else{		
			jQuery("#txtFechaBaja").val(jQuery("#txtFechaActual").val());
		}
	}
	
}

function validaTxtSiglasRfc(){
	var siglas = jQuery("#txtSiglasRfc").val();
	if(siglas.length < 3){
		jQuery("#txtSiglasRfc").addClass("errorField");
		jQuery("#txtSiglasRfc").addClass("jQrequired");
		
	}
}
function validaTxtFechaRfc(){
	var fecha = jQuery("#txtFechaRfc").val();
	if(fecha.length < 6){
		jQuery("#txtFechaRfc").addClass("errorField");
		jQuery("#txtFechaRfc").addClass("jQrequired");
	}
}

function validaTxtHomoclaveRfc(){
	var homoclave = jQuery("#txtHomoclaveRfc").val();	
	var tipoAccion = jQuery("#tipoAccion").val();
	if(homoclave.length < 3 && tipoAccion == 1 && tipoAccion == 4){
		jQuery("#txtHomoclaveRfc").addClass("errorField");
		jQuery("#txtHomoclaveRfc").addClass("jQrequired");
		}

}

function estatusDesahbilitaCampos(){
	var tipoAccion =jQuery("#tipoAccion").val();
	var estatus = jQuery("#situacion").val();
	if(tipoAccion == 4){
		if(estatus==0){
			jQuery(".contenedorFormas").contents().find("textarea,input").attr('readonly', 'readonly');
			jQuery(".contenedorFormas").contents().find("select").attr('disabled', 'readonly');
			jQuery("#btnDomicilio").css("display","none");
			jQuery("#btn_guardar").css("display","none");	
			jQuery("#situacion").removeAttr("disabled");
		}else{
			jQuery(".contenedorFormas").contents().find("textarea,input").removeAttr('readonly');
			jQuery(".contenedorFormas").contents().find("select").removeAttr('disabled');
			jQuery("#btnDomicilio").css("display","block");
			jQuery("#btn_guardar").css("display","block");
		}
	}	
}

function ocultaBtnEliminar(){
	var situacion = jQuery("#situacion").val();
	if(situacion == 1){
		jQuery("#btnEliminar").css("display","block");
	}else{
		jQuery("#btnEliminar").css("display","none");
	}
		
}