var gridProductosBancarios;
var gridEntretenimientos;
var gridHijos;
var numejecucion=0;
var TIPOACCION_CONSULTAHISTORICO = "5";

function guardarDatosExtraAgente(){	
	gridHijos.setCSVDelimiter(',');	
	var listaHijosAgente=gridHijos.serializeToCSV(true);
	
	gridEntretenimientos.setCSVDelimiter(',');	
	var listaEntretenimientosAgente=gridEntretenimientos.serializeToCSV(true);
	
	var path= "/MidasWeb/fuerzaventa/agente/guardarDatosExtra.action?"+jQuery("#guardarDatosExtraAgenteForm").serialize()+"&hijosAgente="+listaHijosAgente+"&entretenimientosAgente="+listaEntretenimientosAgente;
	sendRequestJQ(null, path, targetWorkArea, null);
}

function guardarDatosContables(){		
			gridProductosBancarios.setCSVDelimiter(',');	
			var cont =gridProductosBancarios.getRowsNum();
			var listaProductosBancarios=gridProductosBancarios.serializeToCSV();
			if(cont>0&&numejecucion==0){
				jQuery("#checarClaveDefault").val("Si");
			}
			 if(cont==0 ||jQuery("#checarClaveDefault").val()=="" ||jQuery("#checarClaveDefault").val()== null){
				 mostrarMensajeInformativo("Debe Seleccionar un Producto Bancario marcado como Clave de Pago","10");
			 }else{
					var path= "/MidasWeb/fuerzaventa/agente/guardarDatosContables.action?"+jQuery("#agenteDatosContablesForm").serialize()+"&productosBancariosAgregados="+listaProductosBancarios;
					sendRequestJQ(null, path, targetWorkArea, null);
			 }
}

function existeprodMarcadoClavePago(cont){
	var i=0;
	var existeprodClavePago= "No";
	if(cont>0){
		for(i; i<=cont-1; i++){
			existeprodClavePago = (gridProductosBancarios.cells(gridProductosBancarios.getRowId(i),7).getValue()=="Si")?"Si":"No";
			if(existeprodClavePago=="Si"){
				break;
			}
		}
	}
	return existeprodClavePago;
}

function guardarDatosContablesAgente(){		
	gridProductosBancarios.setCSVDelimiter('.|.');	
	var cont =gridProductosBancarios.getRowsNum();
	var listaProductosBancarios=gridProductosBancarios.serializeToCSV(true);
//	if(cont>0&&numejecucion==0){
//		jQuery("#checarClaveDefault").val("Si");
//	}

//	 if(cont==0 ||jQuery("#checarClaveDefault").val()=="" ||jQuery("#checarClaveDefault").val()== null)
	if(existeprodMarcadoClavePago(cont)=='No'){
		 mostrarMensajeInformativo("Debe Seleccionar un Producto Bancario marcado como Clave de Pago","10");
	 }else{
		 if(validateForId("datosAfianzadoraForm",true)){
				var fechaAutorizacion = jQuery("#fechaAutorizacion").val();
				var fechaVencimiento = jQuery("#fechaVencimiento").val();
					if(compara_fecha(fechaAutorizacion,fechaVencimiento)){
						alert("Fecha de vencimiento no puede ser menor a la Fecha de Autorizacion ");
						jQuery("#fechaVencimiento").val("");
					}else{
						var path= "/MidasWeb/fuerzaventa/agente/guardarDatosContables.action?"+jQuery("#agenteDatosContablesForm").serialize()+"&productosBancariosAgregados="+listaProductosBancarios;
						sendRequestJQ(null, path, targetWorkArea, null);
				}
		 }
	 }
}

//Entretenimiento
function cargarGridEntretenimiento(){
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		var url="/MidasWeb/fuerzaventa/agente/listarFiltradoEntretenimiento.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+
		"&tipoAccion="+dwr.util.getValue("tipoAccion")+ "&ultimaModificacion.fechaHoraActualizacionString=" +
		dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");	
	}
	else
	{
		var url="/MidasWeb/fuerzaventa/agente/listarFiltradoEntretenimiento.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+"&tipoAccion="+dwr.util.getValue("tipoAccion");		
	}	
	
	document.getElementById('gridEntretenimientosAgente').innerHTML = '';
	gridEntretenimientos = new dhtmlXGridObject('gridEntretenimientosAgente');
	gridEntretenimientos.load(url);
}

//hijos
function cargarGridHijos(){
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		var url="/MidasWeb/fuerzaventa/agente/listarFiltradoHijoAgente.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+
		"&tipoAccion="+dwr.util.getValue("tipoAccion")+ "&ultimaModificacion.fechaHoraActualizacionString=" +
		dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
	}
	else
	{
		var url="/MidasWeb/fuerzaventa/agente/listarFiltradoHijoAgente.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+"&tipoAccion="+dwr.util.getValue("tipoAccion");		
	}	
	
	document.getElementById('agenteHijosGrid').innerHTML = '';
	gridHijos = new dhtmlXGridObject('agenteHijosGrid');
	gridHijos.load(url);
}

function cargarGridDatosBancarios(){
	
	if(dwr.util.getValue("tipoAccion") == TIPOACCION_CONSULTAHISTORICO)
	{
		var url="/MidasWeb/fuerzaventa/agente/listarProductosBancariosPorAgente.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+"&tipoAccion="+
		dwr.util.getValue("tipoAccion")+ "&ultimaModificacion.fechaHoraActualizacionString=" + dwr.util.getValue("ultimaModificacion.fechaHoraActualizacionString");		
	}
	else
	{
		var url="/MidasWeb/fuerzaventa/agente/listarProductosBancariosPorAgente.action?tabActiva="+parent.dwr.util.getValue("tabActiva")+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");		
	}
	
	document.getElementById('productoBancario').innerHTML = '';
	gridProductosBancarios = new dhtmlXGridObject('productoBancario');
	gridProductosBancarios.load(url);
}

function populateDomicilio0(json){
	if(json){
		var estadoName="altaPorInternet.persona.domicilios[0].claveEstado";
		var paisName="altaPorInternet.persona.domicilios[0].clavePais";
		var ciudadName="altaPorInternet.persona.domicilios[0].claveCiudad";
		var coloniaName="altaPorInternet.persona.domicilios[0].nombreColonia";
		var calleNumeroName="altaPorInternet.persona.domicilios[0].calleNumero";
		var codigoPostalName="altaPorInternet.persona.domicilios[0].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}
function populateDomicilio1(json){
	if(json){
		var estadoName="altaPorInternet.persona.domicilios[1].claveEstado";
		var paisName="altaPorInternet.persona.domicilios[1].clavePais";
		var ciudadName="altaPorInternet.persona.domicilios[1].claveCiudad";
		var coloniaName="altaPorInternet.persona.domicilios[1].nombreColonia";
		var calleNumeroName="altaPorInternet.persona.domicilios[1].calleNumero";
		var codigoPostalName="altaPorInternet.persona.domicilios[1].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}
function populateDomicilio2(json){
	if(json){
		var estadoName="altaPorInternet.persona.domicilios[2].claveEstado";
		var paisName="altaPorInternet.persona.domicilios[2].clavePais";
		var ciudadName="altaPorInternet.persona.domicilios[2].claveCiudad";
		var coloniaName="altaPorInternet.persona.domicilios[2].nombreColonia";
		var calleNumeroName="altaPorInternet.persona.domicilios[2].calleNumero";
		var codigoPostalName="altaPorInternet.persona.domicilios[2].codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function mostrarModalContacto(){
	var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=txtIdRepresentanteFiscal";
	sendRequestWindow(null, url,obtenerVentanaContacto);
}

function obtenerVentanaContacto(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("responsableModal", 10, 320, 900, 450);
	ventanaResponsable.centerOnScreen();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Consulta de Personas");
	return ventanaResponsable;
}
function findByIdResponsable(idResponsable){
	var url="/MidasWeb/fuerzaVenta/persona/findById.action";
	var data={"personaSeycos.idPersona":idResponsable};
	jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populatePersona(json){
	if(json){
		var idResponsable=json.personaSeycos.idPersona;
		var nombre=json.personaSeycos.nombreCompleto;
		jQuery("#txtIdPersona").val(idResponsable);
		jQuery("#txtNombreAgente").val(nombre);
	}
}

function guardarAgenteInternet_a(){
//	if(jQuery("#tipoPers").val()==2||agenteEsMayorEdad()){
		if(validateAll(true)){
			var productoMarcadoClavePago =jQuery("#checarClaveDefault").val();
			gridProductosBancarios.setCSVDelimiter('.|.');	
			var listaProductosBancarios=gridProductosBancarios.serializeToCSV(true);
			var count=0;
			var registros=gridProductosBancarios.getRowsNum();
			for(i=1;i<=registros;i++){
				var claveDefault=gridProductosBancarios.cellById(i, 7).getValue();
				if(claveDefault=="Si"){
					count++;
				}
			}
			if(registros==0){
				mostrarMensajeInformativo("No hay productos bancarios ha guardar","10");
			}
			else{
				if(count<1){
					mostrarMensajeInformativo("Es requerido un Producto Bancario marcado como clave de pago","10");
				}
				else{
					gridHijos.setCSVDelimiter(',');	
					var listaHijosAgente=gridHijos.serializeToCSV();
					
					gridEntretenimientos.setCSVDelimiter(',');	
					var listaEntretenimientosAgente=gridEntretenimientos.serializeToCSV();
					var path= "/MidasWeb/fuerzaventa/agente/guardarDatosAgenteInternet.action?"+jQuery("#agenteInternetForm").serialize()+"&productosBancariosAgregados="+listaProductosBancarios+"&hijosAgente="+listaHijosAgente+"&entretenimientosAgente="+listaEntretenimientosAgente;
					sendRequestJQ(null, path, targetWorkArea, null);	
				}
			}
		}
//	}
}

function guardarAgenteInternet(){
	var rfcGenerated = "";
	var rfcGenerado="";
	var rfcCapturado="";
	var txtRfcFecha="";
	var txtRfcSiglas="";
	var tipoPersona = jQuery('input:radio[name=altaPorInternet\\.persona\\.claveTipoPersona]:checked').val();//jQuery("#tipoPers").val();
	if(tipoPersona==1){
		//se valida el rfc persona fisica
		var nombre =jQuery("#txtNombre").val();
		var apPaterno =jQuery("#txtApellidoPaterno").val();
		var apMaterno =jQuery("#txtApellidoMaterno").val();
		var fecha =jQuery("#txtFechaNacimiento").val();
		txtRfcFecha=jQuery("#txtfechaRfc").val();
		txtRfcSiglas=jQuery("#txtsiglasRfc").val();
		rfcGenerated = midasGenerateRFC(nombre, apPaterno, apMaterno, fecha);
		rfcGenerado=rfcGenerated.substring(0, 4)+rfcGenerated.substring(4, 10);
		rfcCapturado=txtRfcSiglas+txtRfcFecha;
		if(rfcGenerado!=rfcCapturado){
			parent.mostrarMensajeInformativo('El Rfc no corresponde con la información capturada',"10");
			return false;
		}
		var estadoNacimiento = jQuery('#altaPorInternet\\.persona\\.claveEstadoNacimiento option:selected').text();
		var curp = dwr.util.getValue("altaPorInternet.persona.curp");
		var fechaNac = dwr.util.getValue("altaPorInternet.persona.fechaNacimiento");
		var nombre = dwr.util.getValue("altaPorInternet.persona.nombre");
		var apellidoPat = dwr.util.getValue("altaPorInternet.persona.apellidoPaterno");
		var apellidoMat = dwr.util.getValue("altaPorInternet.persona.apellidoMaterno");
		var sexo = dwr.util.getValue("altaPorInternet.persona.claveSexo");
//		alert(estadoNacimiento+" "+curp+" "+fechaNac+" "+nombre+" "+apellidoPat+" "+apellidoMat+" "+sexo);
		if(estadoNacimiento!=null && estadoNacimiento!='' && curp!=null && curp!='' && fechaNac!=null && fechaNac!='' && nombre!=null && nombre!='' && apellidoPat!=null && apellidoPat!='' && apellidoMat!=null && apellidoMat!='' &&sexo!=null && sexo!=''){
			validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
				if(data==true){
						guardarAgenteInternet_a();
				}else{
					parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");
					return false;
				}
			});
		}else{
			parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");
			return false;
		}
	}else if (tipoPersona==2){
		//se valida el rfc persona fisica
		var txtRazonSocial = jQuery("#txtRazonSocial").val();
		var fecha2 = jQuery("#txtFechaConstitucion").val();		
		txtRfcFecha=jQuery("#txtfechaRfc").val();
		txtRfcSiglas=jQuery("#txtsiglasRfc").val();
		rfcGenerated = calcularRFCMoral(txtRazonSocial,fecha2);
		rfcGenerado=rfcGenerated.substring(0, 4)+rfcGenerated.substring(4, 10);
		rfcCapturado=txtRfcSiglas+txtRfcFecha;
		if(rfcGenerado!=rfcCapturado){
			parent.mostrarMensajeInformativo('El Rfc no corresponde con la información capturada',"10");
			return false;
		}
		guardarAgenteInternet_a();
	}
}
function copiarValor(txt1, txt2){
	jQuery("#"+txt2).val(jQuery("#"+txt1).val());
	}  


function tipoPersona(valor){
	if (jQuery(valor).val()==2){			
		jQuery("#txtTipoPersona1").removeClass("jQrequired");
		jQuery("#txtNombre").removeClass("jQrequired");
		jQuery("#txtApellidoPaterno").removeClass("jQrequired");
		jQuery("#txtApellidoMaterno").removeClass("jQrequired");
		jQuery("#sexo").removeClass("jQrequired");
		jQuery("#estadoCivil").removeClass("jQrequired");
		jQuery("#txtFechaNacimiento").removeClass("jQrequired");
		jQuery("#clienteNombreFiscal").removeClass("jQrequired");
		jQuery("#clienteApellidoPaternoFiscal").removeClass("jQrequired");
		jQuery("#clienteApellidoMaternoFiscal").removeClass("jQrequired");
		jQuery("#txtCURP").removeClass("jQrequired");		

		jQuery("#txtTipoPersona2").addClass("jQrequired");
		jQuery("#txtRazonSocial").addClass("jQrequired");		
		jQuery("#txtIdRepresentanteFiscal").addClass("jQrequired");
		jQuery("#txtFechaConstitucion").addClass("jQrequired");
		jQuery("#txtRazonSocialAfianzadora").addClass("jQrequired");
		jQuery("#txtIdRepresentanteFiscal").addClass("jQrequired");	
		jQuery("#txtNombreAgente").addClass("jQrequired");		

		jQuery("#textnombre1").css("display", "none");
		jQuery("#DatosPers").css("display", "none");
		jQuery("#fNacimiento").css("display", "none");
		jQuery("#tFechaNacimiento").css("display", "none");
		jQuery("#fiscalesFisica").css("display", "none");
		jQuery("#fiscalesNombreL").css("display", "none");
		jQuery("#fiscalesNombreT").css("display", "none");
		jQuery("#fiscalesNombreAPL").css("display", "none");
		jQuery("#fiscalesNombreAPT").css("display", "none");
		jQuery("#fiscalesNombreAML").css("display", "none");
		jQuery("#fiscalesNombreAMT").css("display", "none");
		jQuery("#btnGenerarRFC").css("display", "none");
		
		jQuery("#txfrazonsocial1").css("display", "block");
		jQuery("#tablaRazonSocial").css("display", "block");
		jQuery("#fConstitucion").css("display", "block");
		jQuery("#tFechaConstitucion").css("display", "block");
		jQuery("#fiscalesMoral").css("display", "block");
		jQuery("#fiscalesMoralL").css("display", "block");
		jQuery("#fiscalesMoralT").css("display", "block");
		jQuery("#lRepLegal").css("display", "block");
		jQuery("#tRepLegal").css("display", "block");
		jQuery("#botonRep").css("display", "block");
		jQuery("#nombreCompletoT").css("display", "block");
		jQuery("#btnGenerarRFCMoral").css("display", "block");
		jQuery("#displayCURP").css("display", "none");		
		
		jQuery("#tablaNacimiento").css("display", "none");
		jQuery("#altaPorInternet\\.persona\\.claveEstadoNacimiento").removeClass("jQrequired");
		jQuery("#altaPorInternet\\.persona\\.claveMunicipioNacimiento").removeClass("jQrequired");
		
		jQuery("#txtNombre").val("");
		jQuery("#txtApellidoPaterno").val("");
		jQuery("#txtApellidoMaterno").val("");
		jQuery("#txtCURP").val("");
	}else{
		jQuery("#txtTipoPersona2").removeClass("jQrequired");
		jQuery("#txtRazonSocial").removeClass("jQrequired");		
		jQuery("#txtIdRepresentanteFiscal").removeClass("jQrequired");
		jQuery("#txtFechaConstitucion").removeClass("jQrequired");
		jQuery("#txtRazonSocialAfianzadora").removeClass("jQrequired");
		jQuery("#txtIdRepresentanteFiscal").removeClass("jQrequired");	
		jQuery("#txtNombreAgente").removeClass("jQrequired");		
			
		jQuery("#txtTipoPersona1").addClass("jQrequired");
		jQuery("#txtNombre").addClass("jQrequired");
		jQuery("#txtApellidoPaterno").addClass("jQrequired");
		jQuery("#txtApellidoMaterno").addClass("jQrequired");
		jQuery("#sexo").addClass("jQrequired");
		jQuery("#estadoCivil").addClass("jQrequired");	
		jQuery("#txtFechaNacimiento").addClass("jQrequired");
		jQuery("#clienteNombreFiscal").addClass("jQrequired");
		jQuery("#clienteApellidoPaternoFiscal").addClass("jQrequired");
		jQuery("#clienteApellidoMaternoFiscal").addClass("jQrequired");
		
		jQuery("#nombreCompletoT").css("display", "none");
		jQuery("#txfrazonsocial1").css("display", "none");
		jQuery("#tablaRazonSocial").css("display", "none");	
		jQuery("#fConstitucion").css("display", "none");
		jQuery("#tFechaConstitucion").css("display", "none");
		jQuery("#fiscalesMoralL").css("display", "none");
		jQuery("#fiscalesMoralT").css("display", "none");
		jQuery("#lRepLegal").css("display", "none");
		jQuery("#tRepLegal").css("display", "none");
		jQuery("#botonRep").css("display", "none");
		jQuery("#nombreCompletoT").css("display", "none");
		jQuery("#btnGenerarRFCMoral").css("display", "none");
		
		jQuery("#displayCURP").css("display", "block");		
		jQuery("#btnGenerarRFC").css("display", "block");		
		jQuery("#textnombre1").css("display", "block");		
		jQuery("#DatosPers").css("display", "block");
		jQuery("#fNacimiento").css("display", "block");
		jQuery("#tFechaNacimiento").css("display", "block");
		jQuery("#fiscalesNombreL").css("display", "block");
		jQuery("#fiscalesNombreT").css("display", "block");
		jQuery("#fiscalesNombreAPL").css("display", "block");
		jQuery("#fiscalesNombreAPT").css("display", "block");
		jQuery("#fiscalesNombreAML").css("display", "block");
		jQuery("#fiscalesNombreAMT").css("display", "block");
		
		jQuery("#tablaNacimiento").css("display", "block");
		jQuery("#altaPorInternet\\.persona\\.claveEstadoNacimiento").addClass("jQrequired");
		jQuery("#altaPorInternet\\.persona\\.claveMunicipioNacimiento").addClass("jQrequired");
		jQuery("#txtRazonSocial").val("");
		jQuery("#txtFechaConstitucion").val("");
	}
	jQuery("#txtsiglasRfc").val("");
	jQuery("#txtfechaRfc").val("");
	jQuery("#txthomoclaveRfc").val("");
}

function agregarEntretenimientoTem(){	
	if(jQuery("#txtTipoEntretenimiento").val()!=''	&&	jQuery("#txtComentariosEntretenimiento").val()!=''){
	var cont =gridEntretenimientos.getRowsNum()+1;
	var textoEntretenimiento = jQuery("#txtTipoEntretenimiento option:selected").html().toUpperCase();
	var tipoEntretenimiento = jQuery("#txtTipoEntretenimiento").val();
	var comentarios = jQuery("#txtComentariosEntretenimiento").val();
	gridEntretenimientos.addRow(cont,[0,tipoEntretenimiento, textoEntretenimiento, comentarios,"<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdataEntretenimiento("+cont+",0);'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>" ]);	
	jQuery("#txtTipoEntretenimiento").val("");
	jQuery("#txtComentariosEntretenimiento").val("");
	}
	else{
		var camposFaltantes='';
		if(jQuery("#txtTipoEntretenimiento").val()==''){
			camposFaltantes=' Tipo Entretenimiento,';
		}
		if(jQuery("#txtComentariosEntretenimiento").val()==''){
			camposFaltantes=camposFaltantes+' Detalle,';
		}
		mostrarMensajeInformativo('Favor de capturar los siguientes campos para agregar un entretenimiento:'+camposFaltantes.substring(0, camposFaltantes.length-1),"10");
	}
}

function removeRowConfirmdataEntretenimiento(Id_Row,idEntretenimiento){
	if(confirm("Esta seguro de eliminar este registro?")){
		if(idEntretenimiento==0){
			gridEntretenimientos.deleteRow(Id_Row);
			parent.mostrarMensajeInformativo("El registro ha sido eliminado exitosamente","30");
		}
		else{
			var data = {"entretenimientoAgente.id":idEntretenimiento};
			var url ="/MidasWeb/fuerzaventa/agente/eliminarDatosEntretenimientoExtra.action";
			jQuery.asyncPostJSON(url,data,deleterowAfterDBEntretenimiento);
		}
	}

}

function deleterowAfterDBEntretenimiento(json){
	if(json.test=="1"){
		cargarGridEntretenimiento();
		parent.mostrarMensajeInformativo("El registro ha sido eliminado exitosamente","30");		
	}
	else{
		parent.mostrarMensajeInformativo("Error al eliminar el registro, intentelo mas tarde","10");
	}	
}

function agregarHijoTemp(){
	if(jQuery("#txtNombreHijos").val()!='' 			&&	jQuery("#txtApPaternoHijos").val()!=''		&&
	   jQuery("#txtApMaternoHijos").val()!=''		&&  jQuery("#txtFechaNacimientoHijo").val()!=''	&&  
	   jQuery("#txtUltimaEscolaridad").val()!=''){
	
		var cont =gridHijos.getRowsNum()+1;
		var nombre = jQuery("#txtNombreHijos").val();
		var apellidoPaterno = jQuery("#txtApPaternoHijos").val();
		var apellidoMaterno = jQuery("#txtApMaternoHijos").val();
		var fechaNac = jQuery("#txtFechaNacimientoHijo").val();
		var ultimaEscolaridad = jQuery("#txtUltimaEscolaridad").val();
		
		gridHijos.addRow(cont,[0,nombre, apellidoPaterno, apellidoMaterno, fechaNac, ultimaEscolaridad,"<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdataHijo("+cont+",0);'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>" ]);
		jQuery("#txtNombreHijos").val("");
		jQuery("#txtApPaternoHijos").val("");
		jQuery("#txtApMaternoHijos").val("");
		jQuery("#txtFechaNacimientoHijo").val("");
		jQuery("#txtUltimaEscolaridad").val("");
	}
	else{
		
		var camposFaltantes='';
		if(jQuery("#txtNombreHijos").val()==''){
			camposFaltantes=' Nombre,';
		}
		if(jQuery("#txtApPaternoHijos").val()==''){
			camposFaltantes=camposFaltantes+' Apellido Paterno,';
		}
		if(jQuery("#txtApMaternoHijos").val()==''){
			camposFaltantes=camposFaltantes+' Apellido Materno,';
		}
		if(jQuery("#txtFechaNacimientoHijo").val()==''){
			camposFaltantes=camposFaltantes+' Fecha Nacimiento,';
		}
		if(jQuery("#txtUltimaEscolaridad").val()==''){
			camposFaltantes=camposFaltantes+' Ultima Escolaridad,';
		}
		mostrarMensajeInformativo('Favor de capturar los siguientes campos para agregar un hijo:'+camposFaltantes.substring(0, camposFaltantes.length-1),"10");
	}
}

function removeRowConfirmdataHijo(Id_Row,idHijo){
	if(confirm("Esta seguro de eliminar este registro?")){
		if(idHijo==0){			
			gridHijos.deleteRow(Id_Row);
			parent.mostrarMensajeInformativo("El registro ha sido eliminado exitosamente","30");
		}
		else{
			var data = {"hijoAgente.id":idHijo};
			var url ="/MidasWeb/fuerzaventa/agente/eliminarDatosHijoExtra.action";
			jQuery.asyncPostJSON(url,data,deleterowAfterDBHijo);
		}
	}
}

function deleterowAfterDBHijo(json){
	if(json.test=="1"){
		cargarGridHijos();
		parent.mostrarMensajeInformativo("El registro ha sido eliminado exitosamente","30");		
	}
	else{
		parent.mostrarMEnsajeInformativo("Error al eliminar el registro, intentelo mas tarde","10");
	}
	
}

function agregarProductoTemp(){
	if(validateForId("agenteDatosContablesForm",true)){
		var check;
		var cont =gridProductosBancarios.getRowsNum();
	
//		if(cont!=0 && numejecucion==0){
//			jQuery("#checarClaveDefault").val("Si");
//		}
//		numejecucion++;
//		if(cont>=0 && jQuery("#checarClaveDefault").val()=="Si"&&jQuery("#claveDefault").attr("checked")==true)
		if(existeprodMarcadoClavePago(cont)=='Si' && jQuery("#claveDefault").attr("checked")==true){
			mostrarMensajeInformativo("Ya existe un Producto Bancario marcado como clave de pago","10");
		}else{
			if(jQuery("#idBanco").val()!=''			&&		jQuery("#tipoProducto").val()!=''		&&
			   jQuery("#sucursal").val()!=''		&&		jQuery("#plaza").val()!=''){
					
			if(jQuery("#claveDefault").attr("checked")==true){
				 check = 'Si';
				 var claveDefault = 1;
				 montoCredito=0;
				 jQuery("#checarClaveDefault").val(check);
			}else{
				check = 'No';
				 claveDefault = 0;
			}
				
			var cont =gridProductosBancarios.getRowsNum()+1;
			var idBancos = jQuery("#idBanco").val();
			var nombreBancos = jQuery("#idBanco option:selected").html();
			var productosBancariosName = jQuery("#idProductoBancario option:selected").html();
			var tipoProducto=jQuery("#tipoProducto").val();
			var productosBancarios = jQuery("#idProductoBancario").val();
			var tipoProducto = jQuery("#tipoProducto").val();
			var numeroCuenta = jQuery("#numeroCuenta").val();
			var numeroClabe = jQuery("#numeroClabe").val();
			var montoCredito = jQuery("#montoCredito").val();
			var numeroPlazos = jQuery("#numeroPlazos").val();
			var montoPago = jQuery("#montoPago").val();
			var sucursal = jQuery("#sucursal").val();
			var plaza = jQuery("#plaza").val();
				
			if(jQuery("#montoCredito").val()==''){
				montoCredito='N/A';
			}
			if(jQuery("#numeroPlazos").val()==''){
				numeroPlazos='N/A';
			}
			if(jQuery("#montoPago").val()==''){
				montoPago='N/A';
			}
			if(jQuery("#numeroCuenta").val()==''){
				numeroCuenta="N/A";
			}
			if(jQuery("#numeroClabe").val()==''){
				numeroClabe='N/A';
			}
			//gridProductosBancarios.addRow(cont,[productosBancarios,tipoProducto/*,idBancos*/,productosBancariosName,nombreBancos, numeroCuenta, numeroClabe,'Si',montoCredito , numeroPlazos, montoPago, sucursal, plaza," " ]);
			gridProductosBancarios.addRow(cont,["0",tipoProducto,productosBancarios,productosBancariosName,nombreBancos, numeroCuenta, numeroClabe,check,montoCredito , numeroPlazos, montoPago, sucursal, plaza,"<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdata("+cont+",0,"+claveDefault+");'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>"]);
			jQuery("#idBanco").val("");	
			jQuery("#idProductoBancario").val("");
			jQuery("#tipoProducto").val("");
			jQuery("#numeroCuenta").val("");
			jQuery("#numeroClabe").val("");
			jQuery("#montoCredito").val("");
			jQuery("#numeroPlazos").val("");
			jQuery("#montoPago").val("");
			jQuery("#sucursal").val("");
			jQuery("#plaza").val("");
			jQuery("#claveDefault").attr("disabled",false);
			jQuery("#claveDefault").attr("checked", false);
			}
			else{
				var camposFaltantes='';
				if(jQuery("#idProductoBancario option:selected").html()=='CREDITO'){
					if(jQuery("#idBanco").val()==''){
						camposFaltantes=' Banco,';
					}
					if(jQuery("#montoCredito").val()==''){
						camposFaltantes=camposFaltantes+' Monto Credito,';
					}
					if(jQuery("#numeroPlazos").val()==''){
						camposFaltantes=camposFaltantes+' Numero Plazos,';
					}		
					if(jQuery("#montoPago").val()==''){
						camposFaltantes=camposFaltantes+' Monto Pago,';
					}
					
					if(jQuery("#sucursal").val()==''){
						camposFaltantes=camposFaltantes+' Sucursal,';
					}		
					if(jQuery("#plaza").val()==''){
						camposFaltantes=camposFaltantes+' Plaza,';
					}
					mostrarMensajeInformativo('Favor de capturar los siguientes campos para agregar un producto bancario:'+camposFaltantes.substring(0, camposFaltantes.length-1),"10");
				}else{			
					if(jQuery("#numeroClabe").val()==''){
						camposFaltantes=camposFaltantes+' CLABE,';
					}	
					if(jQuery("#idBanco").val()==''){
						camposFaltantes=' Banco,';
					}
					if(jQuery("#idProductoBancario").val()==''){
						camposFaltantes=camposFaltantes+' Producto Bancario,';
					} 
					if(jQuery("#tipoProducto").val()==''){
						camposFaltantes=camposFaltantes+' Tipo de Producto,';
					}	
					if(jQuery("#sucursal").val()==''){
						camposFaltantes=camposFaltantes+' Sucursal,';
					}		
					if(jQuery("#plaza").val()==''){
						camposFaltantes=camposFaltantes+' Plaza,';
					}
					mostrarMensajeInformativo('Favor de capturar los siguientes campos para agregar un producto bancario:'+camposFaltantes.substring(0, camposFaltantes.length-1),"10");
				}
			}
		}
	}else{
		
	}
	var removerRequerido = ["tipoProducto","numeroCuenta","numeroClabe","montoCredito","numeroPlazos","montoPago","sucursal","plaza"];
	var removerLbRequerido = ["montoCreditoLb","numeroPlazosLb","montoPagoLb","numeroCuentaLb","sucursalLb","plazaLb"];
	noEsRequerido(removerLbRequerido);
	isRequired(false,removerRequerido);
	limpiaCampos();
	jQuery("#idProductoBancario > option[value=-1]").attr("selected", "selected");
}

function removeRowConfirmdata(Id_Row,idProducto,claveDefault){
	if(confirm("Esta seguro de eliminar este producto?")){
//		deleteInfoRowDatabase(Id_Row+","+idProducto+","+claveDefault);
		var data = {"idProductoBancario.id":idProducto, "Id_Row":Id_Row,"claveDefaultInt":claveDefault};
		var url ="/MidasWeb/fuerzaventa/agente/BorrarProductoBancario.action";
		jQuery.asyncPostJSON(url,data,deleterowAfterDB);
	}
//	parent.mostrarMensajeConfirm("Está a punto de borrar un Producto bancario, ¿Desea continuar?","20","deleteInfoRowDatabase("+Id_Row+","+idProducto+","+claveDefault+")",null,null,null);
}

function deleterowAfterDB(json){
	var Id_Row = json.regresarValores[0];
	var claveDeFault = json.regresarValores[1];
	var OK = json.regresarValores[2];
	if(OK == 1){
	    var index = gridProductosBancarios.getRowIndex(Id_Row);
	    gridProductosBancarios.deleteRow(gridProductosBancarios.getRowId(index));
//	    parent.mostrarMensajeInformativo('Se borro el Producto Bancario',"20",null,null);
	    alert('Se borro el Producto Bancario');
		if(claveDeFault==1){
			jQuery("#checarClaveDefault").val("No");
		}
	}else{
//		parent.mostrarMensajeInformativo('No se borro el Producto Bancario',"10",null,null);
		alert('No se borro el Producto Bancario');
	}
}

function initAltaAgenteInternet(){
	jQuery("#txtTipoPersona2").removeClass("jQrequired");
	jQuery("#txtRazonSocial").removeClass("jQrequired");		
	jQuery("#txtIdRepresentanteFiscal").removeClass("jQrequired");
	jQuery("#txtFechaConstitucion").removeClass("jQrequired");
	jQuery("#txtRazonSocialAfianzadora").removeClass("jQrequired");
	jQuery("#txtIdRepresentanteFiscal").removeClass("jQrequired");	
	jQuery("#txtNombreAgente").removeClass("jQrequired");	
	
	jQuery("#nombreCompletoT").css("display", "none");
	jQuery("#fiscalesMoralL").css("display", "none");
	jQuery("#fiscalesMoralT").css("display", "none");
	jQuery("#fiscalesNombreL").css("display", "block");
	jQuery("#fiscalesNombreT").css("display", "block");
	jQuery("#fiscalesNombreAPL").css("display", "block");
	jQuery("#fiscalesNombreAPT").css("display", "block");
	jQuery("#fiscalesNombreAML").css("display", "block");
	jQuery("#fiscalesNombreAMT").css("display", "block");
	jQuery("#fNacimiento").css("display", "block");
	jQuery("#fConstitucion").css("display", "none");
	jQuery("#tFechaConstitucion").css("display", "none");
	jQuery("#lRepLegal").css("display", "none");
	jQuery("#tRepLegal").css("display", "none");
	jQuery("#botonRep").css("display", "none");
	jQuery("#btnGenerarRFCMoral").css("display", "none");
		
//	jQuery("#txtNombre").addClass("jQrequired");
//	jQuery("#txtApellidoPaterno").addClass("jQrequired");
//	jQuery("#txtApellidoMaterno").addClass("jQrequired");
//	jQuery("#sexo").addClass("jQrequired");
//	jQuery("#estadoCivil").addClass("jQrequired");
//	
}

function copiarRFCFiscal(){
	jQuery("#siglasRFC").val(jQuery("#txtsiglasRfc").val());
	jQuery("#fechaRFC").val(jQuery("#txtfechaRfc").val());
	jQuery("#HomoClaveRFC").val(jQuery("#txthomoclaveRfc").val());
}

function copiarDomicilioAgente(indice,idPais,idEstado,idCiudad,colonia,calleNumero,codigoPostal,nuevaColonia,checkBox){
	if(idPais!=''&&idEstado!=''&&idCiudad!=''&&calleNumero!=''&& (colonia!=''|| nuevaColonia!='') &&codigoPostal!=''){
	var estadoName='altaPorInternet.persona.domicilios['+indice+'].claveEstado';
	var paisName='altaPorInternet.persona.domicilios['+indice+'].clavePais';
	var ciudadName='altaPorInternet.persona.domicilios['+indice+'].claveCiudad';
	var coloniaName='altaPorInternet.persona.domicilios['+indice+'].nombreColonia';
	var calleNumeroName = 'altaPorInternet.persona.domicilios['+indice+'].calleNumero';
	var codigoPostalName = 'altaPorInternet.persona.domicilios['+indice+'].codigoPostal';
	var nuevaColoniaName = 'altaPorInternet.persona.domicilios['+indice+'].nuevaColonia';
	var checkName='idColoniaCheck'+(indice+1);
	dwr.util.setValue(paisName, idPais);	
	onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName, paisName);
	dwr.util.setValue(estadoName, idEstado);
	onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
	dwr.util.setValue(ciudadName, idCiudad);
	onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
	dwr.util.setValue(coloniaName, colonia);
	onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
	dwr.util.setValue(calleNumeroName, calleNumero);
	//
	dwr.util.setValue(nuevaColoniaName, nuevaColonia);
	dwr.util.setValue(checkName, checkBox);
	document.getElementById(coloniaName).disabled=checkBox;
	jQuery("#"+checkName).attr("checked",checkBox);
	//
	dwr.util.setValue(codigoPostalName, codigoPostal);
	}
	else{
		mostrarMensajeInformativo("El domicilio a copiar esta incompleto, favor de verificar","10");
	}
}

function agenteEsMayorEdad(){	
		var fecha = jQuery("#txtFechaNacimiento").val();
		if(isEmpty(fecha)){
			return true;
		}
		var f=new Date();	
			
		var array_fecha = fecha.split("/"); 
	  	
	   	var year = parseInt(array_fecha[2]); 

	   	var mes = parseInt(array_fecha[1]); 

	   	var dia = parseInt(array_fecha[0]);	
		   
		if((f.getFullYear() - year) > 18)	
		return true;	
		else if((f.getFullYear() - year) < 18){
			mostrarMensajeInformativo("Fecha de nacimiento invalida, debe ser mayor de edad","10");
			return false;	
		}
		else {	
			if (f.getMonth() + 1 - mes > 0){ 
				 return true; 
			}
			else if ((f.getMonth() + 1 - mes) < 0){ 
				mostrarMensajeInformativo("Fecha de nacimiento invalida, debe ser mayor de edad","10");
				 return false; 
			}
			else{
				if ((f.getDate() - dia) >= 0){ 
					 return true; 
				}
				else if ((f.getDate() - dia) < 0) {
					mostrarMensajeInformativo("Fecha de nacimiento invalida, debe ser mayor de edad");
					return false;
				}
			}	 
		}	
}
/**
*Se vlida el campo Fianza solo reciba como maximo 8 Números y 2 decimales
*
**/
function validarCampoFianza(object){
	var expreg = /^(\d{0,8})(\.\d{1,2})?$/;
	if(expreg.test(object.value)){
		var contieneDecimal = object.value.split(".")
		if(contieneDecimal.length==1){
			jQuery("#"+object.id).val(object.value+'.00');
		}
	}else{
		jQuery("#"+object.id).val('');
		alert('El Monto de Fianza que ingreso no es correcto debe contener máximo 8 números enteros y 2 decimales');
	}
}
