function ocultarMostrarBoton(div){
	
	if (div=="masFiltros"){
		jQuery("#masFiltros").css("display", "none");
		jQuery("#menosFiltros").css("display", "block");
				
	}else{
		jQuery("#masFiltros").css("display", "block");
		jQuery("#menosFiltros").css("display", "none");
	}
}

function obtenerHistorico(){
	var url = '/MidasWeb/fuerzaVenta/persona/loadHistory.action?idTipoOperacion='+dwr.util.getValue("idTipoOperacion")+ "&idRegistro="+dwr.util.getValue("idRegistro");

	jQuery("#historicoGrid").html("");
	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
	historicoFuerzaVentaGrid.load(url);
}

function obtenerCondiciones(idField){
	
	var contador = 0;
	
	if(jQuery("#txtNombreLPers").val() != '')
		contador++;
	
	if(jQuery("#txtClaveTipoPersona").val() != '')
		contador++;
	if(jQuery("#txtRFC").val() != '')
		contador++;
	if(jQuery("#txtTelefonoCasa").val() != '')
		contador++;
	
	if(jQuery("#txtFechaInicio").val() != '')
		contador++;
	if(jQuery("#txtFechaFin").val() != '')
		contador++;
	
	if(dwr.util.getValue(jQuery("personaSeycos.domicilios[0].claveEstado").selector)!= '')
		contador++;
	
	if(dwr.util.getValue(jQuery("personaSeycos.domicilios[0].claveCiudad").selector)!= '')
		contador++;
	
	if(dwr.util.getValue(jQuery("personaSeycos.domicilios[0].nombreColonia").selector)!= '')
		contador++;
	
	if(dwr.util.getValue(jQuery("personaSeycos.domicilios[0].codigoPostal").selector)!= '')
		contador++;
	
		
	if(contador >= 2){
	 listarFiltradoGenerico(listarFiltradoPersonaPath, 'personaGrid', document.personaForm,idField,'responsableModal');
		
	}else{
		alert("Favor de selecionar al menos dos filtros.")
	}
	
	
	
}

