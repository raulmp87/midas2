/**
 * 
 */
var url="/MidasWeb/catalogos/parametroGeneral/lista.action";
var urlSave="/MidasWeb/catalogos/parametroGeneral/guardar.action";

function obtenerParametrosGenerales(){
	parametroGeneralGrid = new dhtmlXGridObject('parametroGeneralGrid');
	parametroGeneralGrid.load(url, function() {
		parametroGeneralGrid.sortRows(2, "str", "asc");
		
	});
	parametroGeneralGrid.groupBy(2);
	parametroGeneralProcessor = new dataProcessor(urlSave);	
	parametroGeneralProcessor.enableDataNames(true);
	parametroGeneralProcessor.setTransactionMode("POST");
	parametroGeneralProcessor.setUpdateMode("cell");
	parametroGeneralProcessor.attachEvent("onAfterUpdate",refrescarGridParametrosGenerales);	
	parametroGeneralProcessor.init(parametroGeneralGrid);	
}

function refrescarGridParametrosGenerales(sid,action,tid,node){
	//alert("akiii");
	obtenerParametrosGenerales();
	return true;
}