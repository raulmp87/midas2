/**
 * 
 * 
 */
function populateDomicilioGerencia(json){
	if(json){
		var estadoName="gerencia.domicilio.claveEstado";
		var paisName="gerencia.domicilio.clavePais";
		var ciudadName="gerencia.domicilio.claveCiudad";
		var coloniaName="gerencia.domicilio.nombreColonia";
		var calleNumeroName="gerencia.domicilio.calleNumero";
		var codigoPostalName="gerencia.domicilio.codigoPostal";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
	}
}

function mostrarHistorico(idTipoOperacion,idRegistro){
	var url = '/MidasWeb/fuerzaventa/gerencia/mostrarHistorial.action?idTipoOperacion='+idTipoOperacion+"&idRegistro="+idRegistro;
	mostrarModal("historicoGrid", 'Historial', 100, 200, 940, 500, url);
}

//function obtenerHistorico(){
//	var url = '/MidasWeb/fuerzaventa/gerencia/loadHistory.action?idTipoOperacion='+20+ "&idRegistro="+dwr.util.getValue("idGerencia");
//	document.getElementById('historicoGrid').innerHTML = '';
//	historicoFuerzaVentaGrid = new dhtmlXGridObject('historicoGrid');
//	historicoFuerzaVentaGrid.load(url);
//}
/*
var ventana=null;
function mostrarModalCentroOperacion(){
	var url="/MidasWeb/fuerzaventa/centrooperacion/mostrarContenedor.action?tipoAccion=consulta&idField=idCentroOperacion";
	sendRequestWindow(null, url, obtenerVentana);
}

function obtenerVentana(){
	var wins = obtenerContenedorVentanas();
	ventana= wins.createWindow("centroOperacionModal", 400, 320, 800, 450);
	ventana.center();
	ventana.setModal(true);
	ventana.setText("Consulta de centros de operacion");
	return ventana;
}*/
/*
function findByIdCentroOperacion(idCentroOperacion){
	var url="/MidasWeb/fuerzaventa/centrooperacion/findById.action";
	var data={"centroOperacion.id":idCentroOperacion};
	//url=url+"?"+jQuery.param(data);
	//sendRequestJQ(null,url,null,"populateDomicilio();");
	jQuery.asyncPostJSON(url,data,populateCentroOperacion);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateCentroOperacion(json){
	if(json){
		var centroOperacion=json.centroOperacion;
		var idCentroOperacion=centroOperacion.id;
		jQuery("#idCentroOperacion").val(idCentroOperacion);
		var nombre=centroOperacion.descripcion;
		jQuery("#descripcionCentroOperacion").val(nombre);	
	}
}*/

function salirDeGerencia(){

	var url = "/MidasWeb/fuerzaventa/gerencia/mostrarContenedor.action";
	if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
		sendRequestJQAsync(null, url, targetWorkArea, null);
	}else{
		if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}
	}
}
function guardarGerencia(){
	var path ="/MidasWeb/fuerzaventa/gerencia/guardar.action?"
		+ jQuery("#gerenciaForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
//	alert(path);
	if(validateAll(true)){
		sendRequestJQ(null, path, targetWorkArea, null);
	}
}