var varModifDescripcionGrid;

function mostrarCatalogoVarModifDescripcion() {
	sendRequestJQ(null, mostrarCatalogoVarModifDescripcionPath , targetWorkArea, 'listarVarModifDescripcion();');
}

function guardarVarModifDescripcion() {
	sendRequestJQ(null, guardarVarModifDescripcionPath+"?"+jQuery(document.varModifDescripcionForm).serialize(), targetWorkArea, "listarVarModifDescripcionGuardar("+ dwr.util.getValue("varModifDescripcion.idGrupo")+");");	 
}

function listarVarModifDescripcionGuardar(idGrupo){
	varModifDescripcionGrid = new dhtmlXGridObject("varModifDescripcionGrid");
	var form = document.varModifDescripcionForm;
	if(form!=null){
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath+ "?varModifDescripcion.idGrupo=" + idGrupo);
	}else{
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath);
	}
	document.varModifDescripcionForm.idGrupo.value=idGrupo;	
}

function eliminarVarModifDescripcion() {
	//var idVarModifDescripcion = getIdFromGrid(varModifDescripcionGrid, 0);
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sendRequestJQ(null, eliminarVarModifDescripcionPath+"?"+jQuery(document.varModifDescripcionForm).serialize(), targetWorkArea, "listarFiltradoVarModifDescripcion("+dwr.util.getValue("varModifDescripcion.idGrupo")+");");
	}
}

function mostrarCatalogoVarModifDescripcionDetalle(idGrupo) {
	if(idGrupo == null){
		idGrupo = 0;
	}
	 sendRequestJQ(null, mostrarCatalogoVarModifDescripcionPath , targetWorkArea, "listarFiltradoVarModifDescripcionDetalle("+idGrupo+");");	
}

function listarFiltradoVarModifDescripcionDetalle(idGrupo){
	varModifDescripcionGrid = new dhtmlXGridObject("varModifDescripcionGrid");
	varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath+ "?varModifDescripcion.idGrupo=" + idGrupo);
	document.varModifDescripcionForm.idGrupo.value=idGrupo;
}

function listarFiltradoVarModifDescripcion(idGrupo){
	varModifDescripcionGrid = new dhtmlXGridObject("varModifDescripcionGrid");
	var form = document.varModifDescripcionForm;
	if(form!=null){
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath+ "?varModifDescripcion.idGrupo=" + idGrupo);
	}else{
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath);
	}
	document.varModifDescripcionForm.idGrupo.value=idGrupo;	
}

function listarVarModifDescripcion(){
	varModifDescripcionGrid = new dhtmlXGridObject("varModifDescripcionGrid");
	var form = document.varModifDescripcionForm;
	if(form!=null){
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath+"?"+jQuery(document.varModifDescripcionForm).serialize());
	}else{
		varModifDescripcionGrid.load(listarFiltradoVarModifDescripcionPath);
	}	
}

function verDetalleVarModifDescripcion (tipoAccion) {
	if(varModifDescripcionGrid.getSelectedId() != null){
		var idVarModifDescripcion = getIdFromGrid(varModifDescripcionGrid, 0);
		sendRequestJQ(null, verDetalleVarModifDescripcionPath + "?tipoAccion=" + tipoAccion + "&id=" + idVarModifDescripcion, targetWorkArea, null);
	}
}

function nuevoVarModifDescripcion (tipoAccion) {
		if(dwr.util.getValue("varModifDescripcion.idGrupo") != -1){
			sendRequestJQ(null, verDetalleVarModifDescripcionPath + "?tipoAccion=" + tipoAccion + "&varModifDescripcion.idGrupo=" + dwr.util.getValue("varModifDescripcion.idGrupo"), targetWorkArea, null);
		}else {
			sendRequestJQ(null, verDetalleVarModifDescripcionPath + "?tipoAccion=" + tipoAccion, targetWorkArea, null);
		}
	
}