/**
 * Copyright (c) 2011 Enrique Mingorance Cano
 * Universidad de Murcia
 * Licensed under the GNU General Public License, version 3 (GPL-3.0)
 * http://www.opensource.org/licenses/GPL-3.0
 * 
 * jQuery charCounter v1.3
 * 
 * NOTA MNCS: Usar este plugin (charCounter) en lugar del viejo charscounter, que permanece para aplicaciones antiguas.
 * 
 * [¿Que es esto?]
 * 
 * Es un plugin que contabiliza los caracteres de un inputText de html o un textArea, incluyendo debajo
 * un contador con los caracteres restantes o informando del nnmero de caracteres que has incluido de mas.
 * Se pueden incluir rangos, con un numero minimo de caracteres y un maximo o directamente un miximo.
 * 
 * [Instrucciones de uso]
 * 
 * En cualquier componente textarea o inputText html podemos incluir una etiqueta title="count[x]" o title="count[y,x]".
 * Si no tenemos especificada la clase "errors" en nuestro css podemos anadirla y asi se mostrara de forma distinta cuando 
 * el numero de caracteres no esta dentro del rango. En el caso de FundeWEB (http://www.um.es/atica/que-es-fundeweb) esta
 * clase ya esta definida.
 * 
 * [Especificaciones]
 * jQuery 1.3.2 o superior (no se ha probado con versiones inferiores, pero no descartamos que funcione)
 * 
 *  
 * [Mejoras de la v1.1:]
 * 	- Contabiliza los saltos de linea y los suma a los caracteres, puesto que java as� lo hace con los Strings
 * [Mejoras de la v1.2:]
 * 	- Mejora la integraci�n al usar el script con varios componentes
 * [Mejoras de la v1.3:]
 * 	- A�adida funcionalidad en los mensajes
 */

jQuery(document).ready(function() {
	
    	//el objeto que tenga como atributo title="count["
	jQuery("[title^='count[']").each(function() {
		var elClass = jQuery(this).attr('title');
		var minChars = 0;
		var maxChars = 0;

		//alert(elClass);
		
		var limitMin="";
		var limitMax = "";

		var countControl = elClass.substring((elClass.indexOf('['))+1, elClass.lastIndexOf(']')).split(',');
		
		//Si se especifica un rango
		if(countControl.length > 1) {
			minChars = countControl[0];
			maxChars = countControl[1];
			//si es un rango se concatenar�n estos string que har� saber al usuario el rango
			limitMin = " (Min. "+minChars+" - ";
			limitMax = "Max. "+maxChars+")";
		} //o por el contrario solo se especifica el m�ximo 
		else {
			maxChars = countControl[0];
		}	

		//este c�digo comentado sirve por si queremos contabilizar las palabras
		//var numChars = jQuery.trim(jQuery(this).val()).length;
		
		
		//el n�mero de caracteres es la longitud del texto m�s el n�mero de retornos de carro
		var numChars = jQuery(this).val().length;
		//tenemos que tener en cuenta que los retornos de carro tambi�n los cuenta
		var lines = jQuery(this).val().split("\n");
		var linesLen = lines.length;
		numChars+=linesLen;

		//el mensaje es de caracteres restantes
		
		//le sumamos 2 porque al principio cuenta una linea como caracter
		if(numChars<minChars)
		{
			var msgNumChar = "<strong>"+(minChars-numChars+2)+"</strong> <span>Caracteres faltan</span>";
		} else if(numChars<=maxChars)
		{
			var msgNumChar = "<strong>"+(maxChars-numChars+2)+"</strong> <span>Caracteres restantes</span>";			    	
		}
		else{
			var msgNumChar = "<strong>"+(numChars-maxChars+2)+"</strong> <span>Caracteres sobran</span>";			    	
		}
		
		//contruimos un div con el id del textarea o input text concatenado con 'cC' para identificarlo posteriormente
		jQuery(this).after('<div id="'+jQuery(this).attr("id")+'cC" class="charCount">'+msgNumChar+limitMin+limitMax+'</div>');
		
		if(minChars > 0 && numChars < minChars) {
		    	//si no est� dentro del rango le a�adimos la clase al componente
			jQuery("#"+jQuery(this).attr("id")+"cC").addClass('errors');
		}	
		
		//repetimos la contabilizaci�n y la comprobaci�n del rango cada vez que salta
		//uno de los eventos especificados en el bind de jQuery
		jQuery(this).bind('keyup click blur focus change paste', function() {
			//alert(jQuery("#"+jQuery(this).attr("id")+"cC").attr("id"));
			//el n�mero de caracteres es la longitud del texto m�s el n�mero de retornos de carro
			var numChars = jQuery(this).val().length;
			//tenemos que tener en cuenta que los retornos de carro tambi�n los cuenta
			var lines = jQuery(this).val().split("\n");
			var linesLen = lines.length;
			numChars+=linesLen;
			
			if(jQuery(this).val() === '') {
				numChars = 0;
			}
			
			if(numChars<minChars)
			{
			    jQuery("#"+jQuery(this).attr("id")+"cC").children('strong').text(minChars-numChars);
				jQuery("#"+jQuery(this).attr("id")+"cC").children('span').text(" Caracteres faltan");
			} else if(numChars<=maxChars)
			{
			    jQuery("#"+jQuery(this).attr("id")+"cC").children('strong').text(maxChars-numChars);
				jQuery("#"+jQuery(this).attr("id")+"cC").children('span').text(" Caracteres restantes");			    	
			}
			else{
			    jQuery("#"+jQuery(this).attr("id")+"cC").children('strong').text(numChars-maxChars);
				jQuery("#"+jQuery(this).attr("id")+"cC").children('span').text(" Caracteres sobran");			    	
			}
			
			
			if(numChars < minChars || (numChars > maxChars && maxChars != 0)) {
				jQuery("#"+jQuery(this).attr("id")+"cC").addClass('errors');
			} else {
				jQuery("#"+jQuery(this).attr("id")+"cC").removeClass('errors');	
			}
		});
	});
});
