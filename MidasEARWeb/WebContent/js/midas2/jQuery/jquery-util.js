/**
 * Utilerias 
 * @vherrera
 */
(function(jQuery){
	jQuery.extend({
		/**
		 * Valida si una cadena esta vacia o nula
		 */
		isValid:function(val){
			return (val!=null && val.toString().length>0)?true:false;
		},
		/**
		 * Valida si un valor es nulo
		 */
		isNull:function(val){
			return (val!=null)?false:true;
		},
		/**
		 * Valida si un valor es numerico o no
		 */
		isNumber:function(val){
			return !isNaN(val);
		},
		/**
		 * Valida si un arreglo esta nulo o vacio
		 */
		isEmptyArray:function(arr){
			return (arr!=null && arr.length>0)?false:true;
		},
		/**
		 * Convierte un valor a entero, si es un String que contiene numeros, solo regresa los numeros , ejemplo:"Test1", regresa 1
		 */
		toInt:function(val){
			return (val!=null)?parseInt(val):null;
		},
		/***
		 * Convierte un valor a tipo decimal o flotante
		 */
		toFloat:function(val){
			return (val!=null)?parseFloat(val):null;
		},
		validMaxLength:function(val,length){
			var isValid=true;
			if(val!=null){
				var currentLength=val.toString().length;
				isValid=(currentLength>length)?false:true;
			}
			return isValid;
		},
		/**
		 * Convierte un string a tipo Date, si este parametro ya es tipo Date, ignora y regresa la fecha tipo Date
		 * @param strDate es el string tipo fecha
		 */
		toDate:function(strDate){
			if(typeof strDate.getMonth!='function'){//si no tiene ni mes o a�o o dia entonces no esta casteado a date
				if(new Date(strDate)=="NaN" || typeof new Date(strDate).getMonth!='function'){
					var arr=strDate.split("-");//formato yyyy-mm-dd
					if(arr.length<=1){
						arr=strDate.split("/");
						if(arr.length<=1){//formato mm/dd/yyyy
							strDate=parseDate("mm/dd/yyyy",strDate);
						}else{
							var mes=parseInt(arr[0])-1;
							var dia=parseInt(arr[1]);
							var anio=parseInt(arr[2]);
							var time=(arr[2]!=null)?arr[2].split("T"):null;
							var hora="00";
							var minuto="00";
							var segundo="00";
							if(time!=null && time.length>1){
								var tiempo=time[1];
								var fragmentos=tiempo.split(":");
								if(fragmentos[0]!=null){
									if(parseInt(fragmentos[0])<10){
										hora="0"+parseInt(fragmentos[0]);
									}else{
										hora=parseInt(fragmentos[0]);
									}
								}
								if(fragmentos[1]!=null){
									if(parseInt(fragmentos[1])<10){
										minuto="0"+parseInt(fragmentos[1]);
									}else{
										minuto=parseInt(fragmentos[1]);
									}
								}
								if(fragmentos[2]!=null){
									if(parseInt(fragmentos[2])<10){
										segundo="0"+parseInt(fragmentos[2]);
									}else{
										segundo=parseInt(fragmentos[2]);
									}
								}
							}
							mes=(mes!=null && mes<10)?"0"+mes:mes;
							dia=(dia!=null && dia<10)?"0"+dia:dia;
							strDate=new Date(anio,mes,dia,hora,minuto,segundo);
						}
					}else{
						var mes=parseInt(arr[1])-1;
						var dia=parseInt(arr[2]);
						var anio=parseInt(arr[0]);
						var time=(arr[2]!=null)?arr[2].split("T"):null;
						var hora="00";
						var minuto="00";
						var segundo="00";
						if(time!=null && time.length>1){
							var tiempo=time[1];
							var fragmentos=tiempo.split(":");
							if(fragmentos[0]!=null){
								if(parseInt(fragmentos[0])<10){
									hora="0"+parseInt(fragmentos[0]);
								}else{
									hora=parseInt(fragmentos[0]);
								}
							}
							if(fragmentos[1]!=null){
								if(parseInt(fragmentos[1])<10){
									minuto="0"+parseInt(fragmentos[1]);
								}else{
									minuto=parseInt(fragmentos[1]);
								}
							}
							if(fragmentos[2]!=null){
								if(parseInt(fragmentos[2])<10){
									segundo="0"+parseInt(fragmentos[2]);
								}else{
									segundo=parseInt(fragmentos[2]);
								}
							}
						}
						mes=(mes!=null && mes<10)?"0"+mes:mes;
						dia=(dia!=null && dia<10)?"0"+dia:dia;
						strDate=new Date(anio,mes,dia,hora,minuto,segundo);
					}
				}else{
					strDate=new Date(strDate);
				}
			}
			return strDate;
		},
		/**
		 * Formatea una fecha a formato mm/dd/yyyy 
		 * @param date es una fecha de tipo Date o String
		 */
		dateFormat:function(date){
			if(date!=null){
				date=toDate(date);
				var d = (date.getDate()<10)?"0"+date.getDate():date.getDate();
				var m = ((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
				var y = date.getFullYear(); 
				return m+"/"+d+"/"+y;
			}else{
				return null;
			}
		},
		/**
		 * convierte una fecha al formato de mm/dd/yyyy HH:MM:SS
		 * @param date es una fecha tipo Date de javascript o un String 
		 */
		dateTimeFormat:function(date){
			if(date!=null){
				date=toDate(date);
				var d = (date.getDate()<10)?"0"+date.getDate():date.getDate();
				var m = ((date.getMonth()+1)<10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
				var y = date.getFullYear();
				var h=(date.getHours()<10)?"0"+date.getHours():date.getHours();
				var min=(date.getMinutes()<10)?"0"+date.getMinutes():date.getMinutes();
				var seg=(date.getSeconds()<10)?"0"+date.getSeconds():date.getSeconds();
				return m+"/"+d+"/"+y+" "+h+":"+min+":"+seg;
			}else{
				return null;
			}
		},
		/**
		 * Limpia la forma, solo los inputs esto incluye: inputs textfield,hidden,select (setea en la primer opcion),textarea,checks y radios en default
		 */
		clearForm:function(idForm){
			if(jQuery('#'+idForm)!=null){
				jQuery.each(jQuery('#'+idForm+' :input'),function(i,field){
					var type=jQuery(field).attr('type');
					if(type!="button"){
						jQuery(field).val('');
					}
				});
			}
		},
		/**
		 * Busca un elemento en un catalogo de elementos con cierto valor.
		 * @param catalog, es la lista de elementos
		 * @param keyProperty es la propiedad por la cual se buscara dentro del catalog
		 * @param resultProperty es el valor de la propiedad que quiero rescatar si se encuentra el valor dentro del catalogo
		 */
		findElementInCatalog:function(catalog,keyProperty,resultProperty,value){
			var name=null;
			if(catalog!=null && catalog.length>0){
				for(var i=0;i<catalog.length;i++){
					var element=catalog[i];
					var val=element[keyProperty];
					if(val==value){//busca si el value esta en el catalogo,si esta entonces regresa el name del catalogo
						name=element[resultProperty];
						break;
					}
				}
			}
			return name;
		},
		/**
		 * Encuentra el label o name de un combo por su value
		 */
		findElementInCombo:function(idSelect,value){
			var name=null;
			if(jQuery('#'+idSelect)!=null){
				jQuery.each(jQuery('#'+idSelect+' option'),function(i,opt){
					var currentVal=jQuery(opt).val();
					if(currentVal==value){
						name=jQuery(opt).html();
					}
				});
			}
			return name;
		},
		getId:function(id){
			var arr=id.split(".");
			var selector=id;
			var component=null;
			if(arr!=null && arr.length>1){
			     selector="";
			     for(var i=0;i<arr.length;i++){
			          if(i<(arr.length-1)){
			               selector+=arr[i]+"\\.";
			          }else{
			               selector+=arr[i];
			          }
			     }
			}
			return selector;
		},
		create:function(options){
			var settings={
				tag:'div'
			};
			if(options){
				jQuery.extend(settings,options);
			}
			var element=jQuery(document.createElement(settings.tag));
			var defaultAttributes=["class","name","id","style","type"];
			var defaultAttributesFromJson=["cls","name","id","style","type"];
			for(var i=0;i<defaultAttributes.length;i++){
				var attribute=defaultAttributes[i];
				var property=defaultAttributesFromJson[i];
				var value=settings[property];
				if(value!=null && value.toString().length>0){
					jQuery(element).attr(attribute,value);
				}
			}
			return element;
		}
	});
})(jQuery);


(function(jQuery){
	jQuery.extend({
		/**
		 * Prepara la fecha para un formato correcto para ser enviado por la url
		 */
		encodeDate:function(date){
			var newDate=null;
			if(!jQuery.isNull(date)){
				newDate=date.replace("/","%2F");
				if(newDate.indexOf("/")>-1){
					newDate=jQuery.encodeDate(newDate);
				}
			}
			return newDate;
		},
		/**
		 * Regresa la posicion de el valor en un arreglo
		 * @return -1 si no se encontro, de lo contrario, regresa la posicion de ese elemento en el arreglo
		 */
		contains:function(value,arr){
			var index=-1;
			if(!jQuery.isValid(value) && !jQuery.isEmptyArray(arr)){
				index=jQuery.inArray(value,arr);
			}
			return index;
		},
		/**
		 * Obtiene los parametros de los hidden del listTable
		 */
		getParamsFromListTable:function(idForm){
			var params="";
			try{
				if(jQuery('#'+idForm)==null){
					throw "Empty";
				}
				jQuery.each(JQ('#'+idForm+' :input'),function(i,elem){
					var val=jQuery(elem).val();
					var type=jQuery(elem).attr('type');
					if(jQuery.isValid(val) && type=="hidden"){
						var name=jQuery(elem).attr('name');
						var strJson='{"'+name+'":"'+val+'"}';
						//var jsonParam=JSON.parse(strJson);
						var jsonParam=eval("("+strJson+")");
						var parametro=jQuery.param(jsonParam);
						if(i<(size-1)){
							params+=parametro+"&";
						}else{
							params+=parametro;
						}
					}
				});
			}catch(er){
				alert("No existe la forma con el id:"+idForm);
			}
			return params;
		},
		/**
		 * Obtiene los parametros de una forma
		 */
		getParamsForm:function(idForm){
			var params="";
			try{
				if(jQuery('#'+idForm)==null){
					throw "Empty";
				}
				jQuery.each(jQuery('#'+idForm+' :input'),function(i,elem){
					var val=jQuery(elem).val();
					var type=jQuery(elem).attr('type');
					if(jQuery.isValid(val) && (type!='button'&& type!='submit')){
						var name=jQuery(elem).attr('name');
						var strJson='{"'+name+'":"'+val+'"}';
						var jsonParam=eval("("+strJson+")");
						var parametro=jQuery.param(jsonParam);
						//if(i<(size-1)){
							params+=parametro+"&";
						/*}else{
							params+=parametro;
						}*/
					}
				});
				if(params.length>0){
					params=params.substring(0,params.length-1);
				}
			}catch(er){
				alert("No existe la forma con el id:"+idForm);
			}
			return params;
		},
		/**
		 * Realiza una llamada de getJson asincrona y sin cache
		 */
		asyncGetJSON:function(url,params,callBack,options){
			try{
				if(!jQuery.isValid(url)){
					throw "UrlException";
				}
				var settings={
					url:url,
					dataType:'json',
					success:callBack,
					cache:false,
					async:false
				};
				if(options){
					jQuery.extend(settings,options);
				}
				if(!jQuery.isNull(params)){
					settings["data"]=params;
				}
				jQuery.ajax(settings);
			}catch(er){
				if(er.name=="UrlException"){
					alert("Favor de proporcionar la url");
				}
			}
		},
		onCascade:function(callBack,timeout){
			var settings={
				url:"",
				type:"POST",
				dataType:'json',
				success:callBack,
				cache:false,
				async:false,
				timeout:((timeout!=null)?timeout:500)
			};
			jQuery.ajax(settings);
		},
		asyncPostJSON:function(url,params,callBack,options){
			try{
				if(!jQuery.isValid(url)){
					throw "UrlException";
				}
				var settings={
					url:url,
					type:"POST",
					dataType:'json',
					success:callBack,
					cache:false,
					async:false
				};
				if(options){
					jQuery.extend(settings,options);
				}
				if(!jQuery.isNull(params)){
					settings["data"]=params;
				}
				jQuery.ajax(settings);
			}catch(er){
				if(er.name=="UrlException"){
					alert("Favor de proporcionar la url");
				}
			}
		},
		asyncPost:function(url,params,callBack,options){
			try{
				if(!jQuery.isValid(url)){
					throw "UrlException";
				}
				var settings={
					url:url,
					type:"POST",
					dataType:'html',
					success:callBack,
					cache:false,
					async:false
				};
				if(options){
					jQuery.extend(settings,options);
				}
				if(!jQuery.isNull(params)){
					settings["data"]=params;
				}
				jQuery.ajax(settings);
			}catch(er){
				if(er.name=="UrlException"){
					alert("Favor de proporcionar la url");
				}
			}
		},
		jsonArrayToParam:function(jsonArray,parent,properties){
			var params="";
			if(properties!=null && properties.length!=0){
				for(var row=0;row<jsonArray.length;row++){
					for(var i=0;i<properties.length;i++){
						var property=properties[i];
						if(jsonArray[row][property]!=null && jsonArray[row][property].toString().length>0){
							params+=parent+"["+property+"]="+jsonArray[row][property];
							if(i<properties.length-1){
								params+="&";
							}
						}
					}
				}
			}
			return params;
		},
		/**
		 * Funcion que convierte un arreglo de objetos json a una lista para ser mandada como parametros a una liga
		 * @param arr es un arreglo de objetos json ,ej:[{id:x,nombre:y}]
		 * @param properties es un arreglo de las propiedades del objeto json , ej:["id","nombre"]
		 * @param baseObjectName es el nombre que recibira la lista para mandar los parametros, ej: "alumnos"
		 * @return una cadena ,ej: alumnos[0].id=x&alumnos[0].name=y& ...etc
		 */
		convertJsonArrayToParams:function(arr,properties,baseObjectName){
			var jsonParams=null;
			if(!jQuery.isEmptyArray(arr) && !jQuery.isEmptyArray(properties) && jQuery.isValid(baseObjectName)){
				jsonParams="";
				var str="";
				jQuery.each(arr,function(i,jsonObject){
					if(i>0){//la siguiente iteracion se le agrega un & para los demas parametros
						str+="&";
					}
					for(var j=0;j<properties.length;j++){
						var property=properties[j];
						var value=jsonObject[property];
						if(jQuery.isValid(value)){
							var partialParam=baseObjectName+"["+i+"]."+property+"="+value;
							var sufix=(j==(properties.length-1))?"":"&";
							str+=partialParam+sufix//le agrega el & cuando el indice no sea el ultimo
						}
					}
				});
				jsonParams=str;
			}
			return jsonParams;
		},
		/**
		 * Permite llenar un combo especificando el id del select y mandando la lista datos json.
		 * @param idSelect <select id="miCombo"></select>
		 * @param jsonList [{"idPais":1,"nombre":"Mexico"}, {"idPais":2,"nombre":"EUA"}] ->Lista de objetos json en pares (propiedad valor y propiedad llave)
		 * @param keyProperty En este ejemplo, nuestra propiedad llave es el "idPais" (lo que nos interesa al elegir una opcion, es el id del pais)
		 * @param valueProperty En este caso, es el nombre del pais (lo que se le muestra al usuario en la vista) que sería "nombre"
		 * @param enableEmptyValue Booleano que indica que se agregue el valor de seleccione o no. Si si, el valor al seleccionarlo es "-1"
		 * @param initValue Indica si una vez que ya se rendereo el combo aparezca seleccionado un valor inicial.Si le pongo initValue=1, aparecera seleccionado Mexico.
		 */
		fillCombo:function(idSelect,jsonList,keyProperty,valueProperty,enableEmptyValue,initValue){
			var select=jQuery("#"+idSelect);
			var DEFAULT_VALUE="Seleccione...";
			if(select){
				select.empty();
				if(enableEmptyValue){
					var option=jQuery.create({tag:"option",name:idSelect,value:"-1"});
					option.append(DEFAULT_VALUE);
					option.val(-1);
					select.append(option);
				}
				if(jsonList!=null && jsonList.length>0){
					for(var i=0;i<jsonList.length;i++){
						var json=jsonList[i];						
						//var key=json[keyProperty];
						var key=eval('json.'+keyProperty);
						//var value=json[valueProperty];
						var value=eval('json.'+valueProperty);
						if(key!=null){
							var option=jQuery.create({tag:"option",name:idSelect,value:key});
							option.append(value);
							option.val(key);
							select.append(option);
						}
						if(initValue){
							select.val(initValue);
						}
					}
				}
			}
		},
		showAlert:function(content,options,onSuccess){
			var settings={
				title:"Advertencia",
				closeText:"Aceptar",
				draggable:false,
				width:350,
				modal:true,
				dialogClass:"myBestDayDialog"
			};
			var div=jQuery.create({tag:"div",id:"_modal"});
			jQuery("body").append(div);
			if(options){
				jQuery.extend(settings,options);
			}
			//var modal=jQuery("#"+idModal);
			div.append((content!=null)?content:"");
			div.dialog(settings);
			div.dialog( "option", "buttons",{"Aceptar":function(){
				if(onSuccess){
					onSuccess();
				}
				jQuery(this).dialog("close");
			}});
		},
		showConfirm:function(content,options,onSuccess,onCancel){
			var settings={
					title:"Advertencia",
					closeText:"Aceptar",
					draggable:false,
					width:350,
					modal:true,
					dialogClass:"myBestDayDialog"
				};
				var div=jQuery.create({tag:"div",id:"_modal"});
				jQuery("body").append(div);
				if(options){
					jQuery.extend(settings,options);
				}
				//var modal=jQuery("#"+idModal);
				div.append((content!=null)?content:"");
				div.dialog(settings);
				div.dialog( "option", "buttons",{"Aceptar":function(){
					if(onSuccess){
						onSuccess();
					}
					jQuery(this).dialog("close");
				},
				"Cancelar":function(){
					if(onCancel){
						onCancel();
					}
					jQuery(this).dialog("close");
				}
				});
		},
		showCommonModal:function(content,options,onSuccess,onCancel){
			var settings={
				title:"Advertencia",
				closeText:"Aceptar",
				draggable:false,
				width:350,
				modal:true,
				dialogClass:"myBestDayDialog"
			};
			var div=jQuery.create({tag:"div",id:"_modal"});
			jQuery("body").append(div);
			if(options){
				jQuery.extend(settings,options);
			}
			//var modal=jQuery("#"+idModal);
			div.append((content!=null)?content:"");
			div.dialog(settings);
			div.dialog( "option", "buttons",{"Aceptar":function(){
				if(onSuccess){
					onSuccess();
				}
				jQuery(this).dialog("close");
			},
			"Cancelar":function(){
				if(onCancel){
					onCancel();
				}
				jQuery(this).dialog("close");
			}
			});
		},
		getObject:function(idField){
			var arr=idField.split(".");
			var selector=idField;
			var component=null;
			if(arr!=null && arr.length>1){
			     selector="";
			     for(var i=0;i<arr.length;i++){
			          if(i<(arr.length-1)){
			               selector+=arr[i]+"\\.";
			          }else{
			               selector+=arr[i];
			          }
			     }
			}
			if(selector){
				component=jQuery("#"+selector);
			}
			return component;
		},
		addToUrl:function(param,value,url){
			if(url!=null && jQuery.isValid(url)){
				var params=url.split("&");
				if(params!=null && params.length>0){
					for(var i=0;i<params.length;i++){
						var parameter=params[i];
						var info=parameter.split("=");
						if(info!=null && info.length>1){
							var field=info[0];
							var oldParameter=parameter;
							var newParameter=oldParameter;
							if(jQuery.isValid(param)&& field==param){
								newParameter=field+"="+value;
							}
							alert("Old url:"+url);
							url.replace(oldParameter,newParameter);
							alert("New url:"+url);
						}
					}
				}
			}
			return url;
		},
		validateUrl:function(url){
			var correctUrl="";
			var uri="";
			if(url!=null && jQuery.isValid(url)){
				var params=url.split("&");
				uri=params[0];
				correctUrl+=uri;
				if(params!=null && params.length>0){
					for(var i=0;i<params.length;i++){
						var parameter=params[i];
						var info=parameter.split("=");
						if(info!=null && info.length>1){
							var field=info[0]+"=";
							var posicion=correctUrl.indexOf(field);
							//Si es -1 es que no esta agregado a la url entonces se agrega
							if(posicion==-1){
								correctUrl+="&"+parameter;
							}
						}
					}
				}
			}
			return correctUrl;
		}
	});
})(jQuery);

/**
 * Requiere importar primero DHTMLX y dhtmlxGrid
 * For Midas 2
 * 
 * 
 * */
(function(jQuery){
	jQuery.extend({
		/**
		 * columns es el columnModel con formato de :{id:"propiedad de la columna del objetoJson",header:"",colType:"",colAlign:""}
		 */
		getJsonForDhtmlxGrid:function(jsonRows,columns,rowIdProperty){
			var json={};
			json["rows"]=new Array();
			var rows=new Array();
			if(jsonRows){
				for(var j=0;j<jsonRows.length;j++){
					var r=jsonRows[j];
					var row={};
					var rowId=eval('('+'r.'+rowIdProperty+')');
					row["id"]=rowId;
					var strData=new Array();
					for(var i=0;i<columns.length;i++){
						var property=columns[i].id;
						var value=null;
						/*if(property.indexOf(".")!=-1){//si tiene una propiedad anidada, es decir, "propiedad.subpropiedad"
							var parts=property.split(".");
							if(parts!=null && parts.length>1){//si si encontro propiedades, y me lo separo bien
								var baseObject=parts[0];
								var baseObjectProperty=parts[1];
								value=r[baseObject][baseObjectProperty];
							}
						}else{
							value=r[property];
						}
						value=r[property];*/
						value=eval('('+'r.'+property+')');
						value=(value!=null)?value.toString():"";
						strData.push(value);
					}
					row["data"]=strData;
					rows.push(row);
				}
				json["rows"]=rows;
			}
			return json;
		},
		getSelector:function(id){
			if(id!=null && id.indexOf(".")!=-1){
				var parts=id.split(".");
				if(parts!=null && parts.length>1){
					
				}
			}
		},
		getGrid:function(options){
			var settings={
				div:"",//div donde se rendereara
				imagePath:"",
				skin:"light",
				urlAction:"",
				rowIdProperty:"",
				baseProperty:null,
				columnModel:[],//arreglo de objetos json que representan el modelo con su {id:""} cada columna
				data:null,//Objeto json o string de parametros para la urlAction
				onRowSelect:null,
				onBeforeSelect:null,
				onRowDblClicked:null,
				onEditCell:null,
				onBeforeLoad:null,
				onAfterLoad:null,
				onSuccess:null,//despues de obtener los datos del objeto json de jquery.asyncPostJSON
				editable:false,
				jqOptions:null,//opciones de jquery ajax
				autoLoad:true//Indica si se carga desde el inicio
			};
			if(options){
				jQuery.extend(settings,options);
			}
			var _grid=new dhtmlXGridObject(settings.div);
			if(jQuery.isValid(settings.imagePath)){
				_grid.setImagePath(settings.imagePath);
			}
			_grid.setSkin(settings.skin);
			if(settings.onBeforeLoad){
				_grid.attachEvent("onXLS",settings.onBeforeLoad);
			}
			if(settings.onAfterLoad){
				_grid.attachEvent("onXLE",settings.onAfterLoad);
			}
			if(settings.onRowSelect){
				_grid.attachEvent("onRowSelect",settings.onRowSelect);
			}
			if(settings.onBeforeSelect){
				_grid.attachEvent("onBeforeSelect",settings.onBeforeSelect);
			}
			if(settings.onRowDblClicked){
				_grid.attachEvent("onRowDblClicked",settings.onRowDblClicked);
			}
			if(settings.onEditCell){
				_grid.attachEvent("onEditCell",settings.onEditCell);
			}
			if(!jQuery.isNull(settings.columnModel) && !jQuery.isEmptyArray(settings.columnModel)){
				var h="";
				var ct="";
				var ca="";
				var widths="";
				for(var i=0;i<settings.columnModel.length;i++){
					var column=settings.columnModel[i];
					var header=column.header;
					var colType=(column.colType!=null)?column.colType:"ro";
					var colAlign=(column.colAlign!=null)?column.colAlign:"left";
					var hidden=(column.hidden!=null && column.hidden)?"true":"false";
					var width=(column.width!=null)?column.width:"*";
					if(i!=(settings.columnModel.length-1)){
						h+=header+",";
						ct+=colType+",";
						ca+=colAlign+",";
						widths+=width+",";
					}else{
						h+=header;
						ct+=colType;
						ca+=colAlign;
						widths+=width;
					}
					_grid.setColumnHidden(i,hidden);
				}
				_grid.setColTypes(ct);
				_grid.setHeader(h);
				_grid.setColAlign(ca);
				_grid.setInitWidths(widths);
			}
			//Se agrega la funcion refresh que ejecuta de nuevo el action.
			var refresh=function(){
				if(jQuery.isValid(settings.urlAction)){
					jQuery.asyncPostJSON(settings.urlAction,settings.data,function(json){
						var obj=null;
						if(json!=null){
							if(settings.baseProperty!=null){
								obj=json[settings.baseProperty];
							}else{
								obj=json;
							}
						}
						var js=jQuery.getJsonForDhtmlxGrid(obj,settings.columnModel,settings.rowIdProperty);
						_grid.parse(js,"json");
						if(settings.onSuccess){
							settings.onSuccess();
						}
					},settings.jqOptions);
				}
			};
			_grid.init();
			if(jQuery.isValid(settings.urlAction)){
				jQuery.asyncPostJSON(settings.urlAction,settings.data,function(json){
					var obj=null;
					if(json!=null){
						if(settings.baseProperty!=null){
							obj=json[settings.baseProperty];
						}else{
							obj=json;
						}
					}
					var js=jQuery.getJsonForDhtmlxGrid(obj,settings.columnModel,settings.rowIdProperty);
					_grid.parse(js,"json");
					if(settings.onSuccess){
						settings.onSuccess();
					}
				},settings.jqOptions);
			}
			return _grid;
		},
		initTab:function(options){
			var settings={
				id:"dhtmlxTab",
				mode:"top",
				tabheight:20,
				imgpath:"",
				margin:"",
				align:"left",
				hrefmode:"",
				offset:"",
				tabstyle:"",
				select:"",
				skinColors:"default",
				style:"",
				onbeforeinit:"",
				oninit:""
			};
			if(options){
				jQuery.extend(settings,options);
			}
			jQuery("div[class*=dhtmlxTabBar]").each(function(i,element){
				var idTab=jQuery(element).attr("id");
				var _tab=new dhtmlXTabBar(idTab);
				_tab.setStyle('default');
				_tab.attachEvent("onSelect", function(id,last_id){
	            	idContenido = "contenido_" + id;
	            	var extraAction = document.getElementById(idContenido).getAttribute("extraAction");
	            	if (extraAction)
		            	eval(extraAction);
	                return true;
	            });
			});
		}
	});
})(jQuery);