/**
 * Subordinados de Seccion
 */

var derechosGrid;
var derechosProcessor;


function afterUpdateGridDerechos() {
	
	mostrarMensajeExitoYCambiarTab(configuracionSeccionTabBar, 'detalle');
}

function iniciaGridDerechos(nombreGrid, cargaElementosPath, accionSobreElementosPath) {
	
	derechosGrid = new dhtmlXGridObject(nombreGrid);
		
	
	derechosGrid.init();
	derechosGrid.load(cargaElementosPath + "?idSeccion=" + dwr.util.getValue("idSeccion"));
	
	//Creacion del DataProcessor
	derechosProcessor = new dataProcessor(accionSobreElementosPath + "?idSeccion=" + dwr.util.getValue("idSeccion"));

	derechosProcessor.enableDataNames(true);
	derechosProcessor.setTransactionMode("POST");
	derechosProcessor.setUpdateMode("cell");
	derechosProcessor.setVerificator(2, function(value,row){
		var valido = true;
		derechosGrid.forEachRow(function(id){ 
            // here id - id of the row
			var cellVal = derechosGrid.cellById(id, 2).getValue();
			//alert(cellVal + " - " + value + " - id - " + id + " - row - " + row);
			if(cellVal == value && id != row){
				mostrarVentanaMensaje('20', 'El valor ingresado esta duplicado');
				valido =  false;
				return;
			}
        });
		if(value <= 0){
			valido =  false;
		}
		return valido;
	});
	
	derechosProcessor.attachEvent("onAfterUpdate",afterUpdateGridDerechos);
	
	derechosProcessor.init(derechosGrid);
	
}

function agregarDerecho() {
	if (derechosGrid.getRowsNum() ==0 ) {
		derechosGrid.addRow(1,",,,0");
	} else {
		var secuencia = parseInt(derechosGrid.getRowId(derechosGrid.getRowsNum() - 1)) + 1;
		var valido = true;
		derechosGrid.forEachRow(function(id){ 
			var cellVal = derechosGrid.cellById(id, 2).getValue();
			if(cellVal == ""){
				valido =  false;
				return;
			}
        });
		if(valido){
			derechosGrid.addRow(secuencia,",,,0");
		}
	}	
}

function eliminarDerecho() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		derechosGrid.deleteSelectedItem();
	}
}


/**
 * Derechos de Endoso
 */

function iniciaGridDerechosEndoso() {
	
	iniciaGridDerechos('derechosEndosoGrid', obtenerDerechosEndosoPath, accionSobreDerechosEndosoPath);
	
}

/**
 * Derechos de Poliza
 */

function iniciaGridDerechosPoliza() {
	
	iniciaGridDerechos('derechosPolizaGrid', obtenerDerechosPolizaPath, accionSobreDerechosPolizaPath);
	
}


