// Actions sin input 
var mostrarConfiguracionURL;
var mostrarBusquedaURL;
var exportarConfiguracionURL;

function mostrarConfiguracionAction() {
	var url = mostrarConfiguracionURL;
	sendPeticion(url, "contenido");
}

function mostrarBusquedaAction() {
	var url = mostrarBusquedaURL;
	sendPeticion(url, "contenido");
}

// Actions con inputs
var salvarActualizarURL;
var validarListarConfiguracionURL;
var listarConfiguracionURL;
var borrarConfigURL;
var enviarPorCorreoURL;

// Divs de los inputs - forms
var divIdConfCamposIngreso;
var formIdConfig;
var divIdBusqFiltro;
var formIdFiltro;
var divIdEnvioCorreo;
var formEnvioCorreo;
var divIdLstDispGrid;

var gridMap = {};
var gridLstDisp;
var lstDispGridPagingArea = "pagingArea"
var indicadorId = "indicador";

var txtAgregarQuitar;

function salvarActualizarAction() {
	console.log("salvarActualizarAction");
	var data = jQuery("#" + formIdConfig).serialize();
	var url = salvarActualizarURL + "?" + data;

	//	
	// // Sacar valores y generar url para cargar busqueda/grid con info
	// var listaDeAjustadores = jQuery("#lstAjustadoresId").serialize();
	// var fechaInicial = jQuery("#fechaInicial").serialize();
	// var fechaFinal = jQuery("#fechaFinal").serialize();
	//
	// listaDeAjustadores = listaDeAjustadores.replace(/ajustadoresIds/g,
	// "horarioAjustadorFiltro.ajustadoresIds");
	// fechaInicial = fechaInicial.replace("fechaInicial",
	// "horarioAjustadorFiltro.fechaInicial");
	// fechaFinal = fechaFinal.replace("fechaFinal",
	// "horarioAjustadorFiltro.fechaFinal");
	//
	// var gridData = listaDeAjustadores + "&" + fechaInicial + "&" +
	// fechaFinal;

	// sendRequestJQ(null, url, divIdConfCamposIngreso);

	sendRequestJQ(null, url, divIdConfCamposIngreso, function() {
		listarConfiguracionConDatosDeConfig();
	});
}

function listarConfiguracionConDatosDeConfig() {
	console.log("listarConfiguracionConDatosDeConfig");

	var ajustadores = jQuery("#lstAjustadoresId").val();
	var fechaInicial = jQuery("#fechaInicial").val();
	var fechaFinal = jQuery("#fechaFinal").val();

	var url = listarConfiguracionURL + "?";

	for (i = 0; i < ajustadores.length; i++) {
		url += "horarioAjustadorFiltro.ajustadoresIds=" + ajustadores[i] + "&";
	}
	url += "horarioAjustadorFiltro.fechaInicial=" + fechaInicial + "&";
	url += "horarioAjustadorFiltro.fechaFinal=" + fechaFinal;
	reCargarGrid(url, gridLstDisp, true, true, deshabilitarCheckboxEliminar());
}

function validarListarConfiguracionAction() {
	console.log("validarListarConfiguracionAction");
	var data = jQuery("#" + formIdFiltro).serialize();
	var url = validarListarConfiguracionURL + "?" + data;
	var gridurl = validarListarConfiguracionURL + "?" + data;

	sendRequestJQ(null, url, divIdBusqFiltro, function() {
		reCargarGrid(gridurl, gridLstDisp);
	});

}

function listarConfiguracionAction() {
	console.log("listarConfiguracionAction");
	var data = jQuery("#" + formIdFiltro).serialize();
	var url = listarConfiguracionURL + "?" + data;
	reCargarGrid(url, gridLstDisp, true, true, deshabilitarCheckboxEliminar());
}

function borrarConfiguracionAction(fecha, ajustadorId, oficina, horarioLaboralId) {
	if(confirm("¿Esta seguro que desea eliminar el horario?")){
	console.log("borrarConfiguracionAction");
	var contenedor = divIdEnvioCorreo;	
	var data = 'horarioAjustadorId\.fecha='+ fecha + '&horarioAjustadorId\.ajustadorId='+ ajustadorId + 
		'&horarioAjustadorId\.oficinaId='+oficina+'&horarioAjustadorId\.horarioLaboralId='+horarioLaboralId;
	var gridurl = borrarConfigURL + "?" + data;
		reCargarGrid(gridurl, gridLstDisp, true, false);
	}
}

function enviarPorCorreoAction() {
	console.log("enviarPorCorreoAction");
	// checar que el listado de ajustadores tenga valores.
	if (jQuery("#lstAjustadoresId").value === 'undefined') {
		mostrarMensajeInformativo("Selecciona ajustadores a enviar correo",
				"10", null, null);
		return;
	}
	var dataCorreo = jQuery("#" + formEnvioCorreo).serialize();
	var dataFiltro = jQuery("#" + formIdFiltro).serialize();
	var url = enviarPorCorreoURL + "?" + dataCorreo + "&" + dataFiltro;
	var contenedor = divIdEnvioCorreo;
	// var callback = busqueda
	sendRequestJQ(null, url, contenedor);
}

function limpiarForma() {
	// jQuery(this).reset();
	jQuery(this).closest('form').reset();
}

// Actions de Grids
var listarConfiguracionURL;

function cargarListadoDeDisponibilidadGrid() {
	cargarGrid(listarConfiguracionURL, gridLstDisp);
}

function init() {
	initListadoDeDisponibilidadGrid();
	cargarListadoDeDisponibilidadGrid();

	jQuery.subscribe('onSelectFechaFinal', function(event, data) {
		jQuery("#fechaInicial").datepicker("option", "maxDate",
				event.originalEvent.dateText);
		// alert('Selected Date : ' + event.originalEvent.dateText);
	});

	jQuery.subscribe('onSelectFechaInicial', function(event, data) {
		jQuery("#fechaFinal").datepicker("option", "minDate",
				event.originalEvent.dateText);
		// alert('Selected Date : ' + event.originalEvent.dateText);
	});

	jQuery("#contenido").live("beforeunload", function() {
		alert("Goodbye")
	});

}

function initBusqueda() {
	hacerSeleccionarTodos('#correoRolesSeleccionados-1');
}

function initListadoDeDisponibilidadGrid() {
	gridLstDisp = simpleInitGrid(divIdLstDispGrid, indicadorId);
}

// Funciones "privadas"

function sendPeticion(url, contenedor, callback) {
	callback = (typeof callback === 'undefined') ? null : callback;
	sendRequestJQ(null, url, contenedor, callback);
}

function hacerSeleccionarTodos(jqControlId) {
	var chbxControl = jQuery(jqControlId);

	chbxControl.click(function(event) {
		var ctrlValue = this.checked;
		// Iterate each checkbox
		chbxControl.siblings().each(function() {
			this.checked = ctrlValue;
		});
	});

	chbxControl.siblings().click(function(event) {
		var sib = chbxControl.siblings().length;
		var sibsel = chbxControl.siblings(':checked').length;
		chbxControl.attr('checked', (sib == sibsel));
	});
}

function intentarSacarInfoDeSeleccion() {
	var ajustadores = jQuery("#lstAjustadoresId").val();
	var fechaInicial = jQuery("#fechaInicial").val();
	var fechaFinal = jQuery("#fechaFinal").val();

	if (ajustadores != null && fechaInicial != "" && fechaFinal != "") {
		listarConfiguracionConDatosDeConfig();
	}
}

// // Funciones del grid

function initGrid(gridDivId, indicador, paginationEnabled, pagingArea, infoArea) {
	// Default Values
	indicador = (typeof paginationEnabled === 'undefined') ? "indicador"
			: indicador;
	paginationEnabled = (typeof paginationEnabled === 'undefined') ? true
			: paginationEnabled;
	pagingArea = (typeof paginationEnabled === 'undefined') ? "pagingArea"
			: pagingArea;
	infoArea = (typeof paginationEnabled === 'undefined') ? "infoArea"
			: infoArea;

	var gridVar = simpleInitGrid(gridDivId)

	if (paginationEnabled == true && jQuery("#" + pagingArea).length != 0
			&& jQuery("#" + infoArea).length != 0) {
		// gridVar.enablePaging(true, 10, 10, pagingArea + gridDivId, true,
		// infoArea + gridDivId);
		gridVar.setPagingSkin("bricks");
	}
	gridVar.attachEvent("onXLS", function(grid_obj) {
		blockPage()
	});
	gridVar.attachEvent("onXLE", function(grid_obj) {
		unblockPage()
	});
	if (jQuery("#" + indicador).length != 0) {
		gridVar.attachEvent("onXLS", function(grid) {
			mostrarIndicadorCarga(indicador);
		});
		gridVar.attachEvent("onXLE", function(grid) {
			ocultarIndicadorCarga(indicador);
		});
	}
	return gridVar;
}

function simpleInitGrid(gridDivId, indicador) {
	document.getElementById(gridDivId).innerHTML = '';
	var gridVar = new dhtmlXGridObject(gridDivId);
	gridVar.setSkin("light");

	gridVar.setDateFormat("%d/%m/%Y");
	dhtmlxEvent(window, "resize", function() {
		gridVar.setSizes();
	});
	jQuery("#" + gridDivId).resize(function() {
		gridVar.setSizes();
	});
	gridVar.init();
	if (jQuery("#" + indicador).length != 0) {
		gridVar.attachEvent("onXLS", function(grid) {
			mostrarIndicadorCarga(indicador);
		});
		gridVar.attachEvent("onXLE", function(grid) {
			deshabilitarCheckboxEliminar();
			ocultarIndicadorCarga(indicador);
		});
	}else{
		gridVar.attachEvent("onXLE", function(grid) {
			deshabilitarCheckboxEliminar();
		});
	}
	gridMap[gridDivId] = gridVar;
	return gridVar;
}

/**
 * deshabilita los checkbox de eliminar en caso de que el horario sea menor a la fecha actual 
 * o no tenga horario asignado para ese tipo de disponibilidad.
 */
function deshabilitarCheckboxEliminar(){
    var validarFechaHoyOMayor;
//	TODO completar las diferentes disponibilidades
    
    //columnas del grid que contienen los checkbox
    var COLINDEX_CHECK = [4,6,8,10,12,14,16];
    //columna que contiene el horario en el grid, corresponde con la del check, ej. [COLINDEX_DESCRIPCION[1],COLINDEX_CHECK[1]] = [3,4] = horario y check de tipo de disponibilidad normal
    var COLINDEX_DESCRIPCION = [3,5,7,9,11,13,15];
    var rowsNum = gridLstDisp.getRowsNum();
    var count = 0;
    gridLstDisp.forEachRow(function(id){
    	validarFechaHoyOMayor = gridLstDisp.getUserData(id,"userData_validarFechaHoyOMayor");
		for(var i = 0; i < COLINDEX_CHECK.length; i++){
			if(!gridLstDisp.cellById(id,COLINDEX_DESCRIPCION[i]).getValue()
					|| validarFechaHoyOMayor == 'false'){
			        gridLstDisp.editStop(); 
		        	gridLstDisp.setCellExcellType(id,COLINDEX_CHECK[i],'ro');
		        	gridLstDisp.cellById(id,COLINDEX_CHECK[i]).setValue("");
			}
			else{
				gridLstDisp.setCellExcellType(id,COLINDEX_CHECK[i],'ch');
			}
		}
    });
}

function cargarGrid(urlListado, gridVar) {
	gridVar.load(urlListado);
}

function reCargarGrid(urlListado, gridVar, insert_new, del_missed, afterCall) {
	insert_new = (typeof insert_new === 'undefined') ? true : insert_new;
	del_missed = (typeof del_missed === 'undefined') ? true : del_missed;
	afterCall = (typeof afterCall === 'undefined') ? null : afterCall;
	// gridVar = new dhtmlXGridObject();
	// gridVar.clearAndLoad(urlListado);
	gridVar.updateFromXML(urlListado, insert_new, del_missed, afterCall);
}

var maxMap = {};
maxMap[divIdLstDispGrid] = false;
var minMap = {};
function maxminGrid(idGrid, dif) {
	console.log("maxminGrid");
	dif = (typeof dif === 'undefined') ? 120 : dif;

	if (typeof minMap[idGrid] === 'undefined') {
		var size = jQuery("#" + idGrid).height();
		minMap[idGrid] = size;
		maxMap[idGrid] = false;
	}

	var alto
	if (maxMap[idGrid]) {
		alto = minMap[idGrid];

	} else {
		alto = "+=" + dif;
	}

	jQuery("#" + idGrid).animate({
		height : alto
	}, 500, "linear", function() {
		grid = gridMap[idGrid];
		grid.setSizes();
	});

	maxMap[idGrid] = !maxMap[idGrid];

}

// // funciones de dropdowns
function onChangeOficina(target) {
	var lstOficina = jQuery("#lstOficinaId").val();
	if (lstOficina != null && lstOficina != headerValue) {
		dwr.engine.beginBatch();
		listadoService.getMapAjustadoresPorOficinaId(lstOficina,
				function(data) {
					addOptionsHeaderAndSelect(target, data);
				});
		dwr.engine.endBatch({
			async : false
		});
	} else {
		addOptions(target, "");
	}
	gridLstDisp.clearAll();
}

// funciones de UI

function actualizarFechasLimite() {
	var fechaInicial = jQuery("#fechaInicial");
	var fechaFinal = jQuery("#fechaFinal");
	fechaInicial.maxDate = fechaFinal.value();
	fechaFinal.minDate = fechaInicial.value();
}

function exportarExcel(){
	
	var data=jQuery("#formFiltro").serialize();
	var url=exportarConfiguracionURL + "?" + data;
	window.open(url, 'Listado_disponibilidad_ajustadores');

}

/**
 * Construye los parametros del url para las diferentes columnas de tipos de disponibilidad seleccionados en los checkbox para eliminar
 * @param columna
 * @param userDataName
 * @param ultimoIndice
 * @returns {String}
 */
function construirDatosUrlEliminar(columna, userDataName, ultimoIndice){
	var segmentoDatos = "";
	var columnaChecked = gridLstDisp.getCheckedRows(columna);
	if(columnaChecked){
		var columnaSplits = columnaChecked.split(",");
		for(var i = 0; i < columnaSplits.length; i++){
			segmentoDatos += "selected[" + ultimoIndice + "]=" + gridLstDisp.getUserData(columnaSplits[i],userDataName) + "&";
			ultimoIndice++;
		}
	}
	return segmentoDatos;
}

/**
 * Obtiene el ultimo indice de los parametros del url ya constuido
 * @param datosUrlEliminar
 * @returns {Number}
 */
function calcularUltimoIndiceDatosUrlEliminar(datosUrlEliminar){
	var datosSplit = datosUrlEliminar.split("&");
	return datosSplit.length - 1
}

/**
 * Dependiendo si es la pantalla de busqueda o la pantalla de configuracion de horarios
 * se toman los datos de diferentes variables
 * @returns
 */
function obtenerParametrosBusquedaParaDespuesDeEliminar(){
	var parametros = jQuery("#" + formIdFiltro).serialize();
	if(parametros){
		return parametros;
	}else{
		var ajustadores = jQuery("#lstAjustadoresId").val();
		var fechaInicial = jQuery("#fechaInicial").val();
		var fechaFinal = jQuery("#fechaFinal").val();

		for (i = 0; i < ajustadores.length; i++) {
			parametros += "horarioAjustadorFiltro.ajustadoresIds=" + ajustadores[i] + "&";
		}
		parametros += "horarioAjustadorFiltro.fechaInicial=" + fechaInicial + "&";
		parametros += "horarioAjustadorFiltro.fechaFinal=" + fechaFinal;
		return parametros;
	}
}

/**
 * Elimina las configuraciones de horario seleccionadas 
 */
function eliminarConfiguraciones() {
	//OBTENIENDO LOS DATOS DE LOS HORARIOS A ELIMINAR
	var datosUrl = "";
	datosUrl += construirDatosUrlEliminar(4,  "userData_dispNormal",      calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(6,  "userData_dispGuardia",     calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(8,  "userData_dispApoyo",       calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(10, "userData_dispDescanso", 	  calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(12, "userData_dispIncapacidad", calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(14, "userData_dispPermiso",     calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	datosUrl += construirDatosUrlEliminar(16, "userData_dispVacaciones",  calcularUltimoIndiceDatosUrlEliminar(datosUrl));
	
	//ELIMINANDO LOS DATOS
	if(datosUrl == ''){
		mostrarVentanaMensaje("20","Debe seleccionar al menos un horario a eliminar",null);
	}else{
		var filtro = obtenerParametrosBusquedaParaDespuesDeEliminar();
		var contenedor = divIdEnvioCorreo;
		var gridurl = borrarConfigURL + "?" + datosUrl + filtro;
		if(gridurl.length < 1990){
			if(confirm("\u00BFEst\u00E1 seguro que desea eliminar los horarios seleccionados\u003F")){
				//OBTENIENDO LOS DATOS PARA ACTUALIZAR LA LISTA DESPUES DE ELIMINAR
				reCargarGrid(gridurl, gridLstDisp, true, true, deshabilitarCheckboxEliminar());
			}
		}else{
			var maximoSeleccionados = gridurl.length - filtro.length - borrarConfigURL.length;
			maximoSeleccionados = Math.ceil(maximoSeleccionados/(calcularUltimoIndiceDatosUrlEliminar(datosUrl)+1));
			maximoSeleccionados = Math.floor((2000 - filtro.length - borrarConfigURL.length)/ maximoSeleccionados);
			mostrarVentanaMensaje("20","La cantidad m\u00E1xima de horarios a seleccionar es alrededor de " + maximoSeleccionados +", favor de seleccionar menos",null);
		}
	}
}