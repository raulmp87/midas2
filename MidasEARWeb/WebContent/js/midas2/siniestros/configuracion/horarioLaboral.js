var horarioLaboralGrid; 
var hLGridId = "horarioLaboralGrid";
var hLGridPagingArea = "pagingArea"
var indicadorId = "indicador";	

var listarCargaURL;
var mostrarURL;
var busquedaURL;
var mostrarAltaURL;
var verDetalleURL;
var salvarActualizarURL;

var guardarText;
var agregarText;

var guardarBtnId = "guardarBtnId";

function init() {
	initHorarioLaboralGrid();
	cargarHorarioLaboralGrid();
	console.log("init");
	initDetalle();
}

function initDetalle() {
	jQuery("select[name='horarioLaboral.estatus']").change(
			function() {
				if (jQuery(this).attr('value') == 1) {
					jQuery("input#predeterminado.cajaTextoM2.w120").removeAttr(
							"disabled");
				} else {
					jQuery("input#predeterminado.cajaTextoM2.w120").attr(
							"disabled", true);
				}
			});
}

function initHorarioLaboralGrid() {
	horarioLaboralGrid = simpleInitGrid(hLGridId, indicadorId);
}

function cargarHorarioLaboralGrid() {
	cargarGrid(listarCargaURL, horarioLaboralGrid);
}

function busquedaServicio() {
	var data = jQuery("#busqueda").serialize();
	//horarioLaboralGrid.loadXML
	cargarGrid(busquedaURL + "?" + data, horarioLaboralGrid);
}

function mostrarDetalleServicio(id) {
	var url = verDetalleURL + "?accion=2&horarioId=" + id;
	sendPeticion(url, "contenido");
}

function editarDetalleServicio(gid) {
	gid = (typeof gid === 'undefined') ? horarioLaboralGrid.getSelectedId()
			: gid;
	var url = verDetalleURL + "?accion=0&horarioId=" + gid;
	sendPeticion(url, "horarioLaboralDetalleDiv");
}

function agregarDetalleServicio() {
	var data = jQuery("#salvarActualizar").serialize();
	var url = salvarActualizarURL + "?" + data;
	sendPeticion(url, "horarioLaboralDetalleDiv", busquedaServicio);
}

function regresar() {
	var url = mostrarURL;
	sendPeticion(url, "contenido");
}

function muestraAlta() {
	var url = mostrarAltaURL + "?accion=1";
	sendPeticion(url, "contenido");
}

function sendPeticion(url, contenedor, callback) {
	callback = (typeof callback === 'undefined') ? null : callback;
	sendRequestJQ(null, url, contenedor, callback);
}

function deshabilitarComponentes() {
	jQuery("input,select").attr("disabled", true);
}

function enablePredeterminado(estatus) {
	jQuery("#predeterminado").disabled = (estatus.value == 1);
}

function initGrid(gridDivId, indicador, paginationEnabled, pagingArea, infoArea) {
	// Default Values
	indicador = (typeof paginationEnabled === 'undefined') ? "indicador"
			: indicador;
	paginationEnabled = (typeof paginationEnabled === 'undefined') ? true
			: paginationEnabled;
	pagingArea = (typeof paginationEnabled === 'undefined') ? "pagingArea"
			: pagingArea;
	infoArea = (typeof paginationEnabled === 'undefined') ? "infoArea"
			: infoArea;

	var gridVar = simpleInitGrid(gridDivId)

	if (paginationEnabled == true && jQuery("#" + pagingArea).length != 0
			&& jQuery("#" + infoArea).length != 0) {
		// gridVar.enablePaging(true, 10, 10, pagingArea + gridDivId, true,
		// infoArea + gridDivId);
		gridVar.setPagingSkin("bricks");
	}
	gridVar.attachEvent("onXLS", function(grid_obj) {
		blockPage()
	});
	gridVar.attachEvent("onXLE", function(grid_obj) {
		unblockPage()
	});
	if (jQuery("#" + indicador).length != 0) {
		gridVar.attachEvent("onXLS", function(grid) {
			mostrarIndicadorCarga(indicador);
		});
		gridVar.attachEvent("onXLE", function(grid) {
			ocultarIndicadorCarga(indicador);
		});
	}
	return gridVar;
}

function simpleInitGrid(gridDivId, indicador) {
	document.getElementById(gridDivId).innerHTML = '';
	var gridVar = new dhtmlXGridObject(gridDivId);
	gridVar.setSkin("light");
	gridVar.init();
	if (jQuery("#" + indicador).length != 0) {
		gridVar.attachEvent("onXLS", function(grid) {
			mostrarIndicadorCarga(indicador);
		});
		gridVar.attachEvent("onXLE", function(grid) {
			ocultarIndicadorCarga(indicador);
		});
	}
	return gridVar;
}

function cargarGrid(urlListado, gridVar) {
	gridVar.load(urlListado);
}
	