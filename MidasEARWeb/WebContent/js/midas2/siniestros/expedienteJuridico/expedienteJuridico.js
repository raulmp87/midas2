var listadoExpedientesJuridico;

var comentariosGrid;
function initGridJuridico(){
      document.getElementById("pagingArea").innerHTML = '';
      document.getElementById("infoArea").innerHTML = '';

      listadoExpedientesJuridico = new dhtmlXGridObject('listadoExpedientesJuridicoGrid');      
      listadoExpedientesJuridico.attachEvent("onXLS", function(grid_obj){blockPage()});
      listadoExpedientesJuridico.attachEvent("onXLE", function(grid_obj){unblockPage()});
      listadoExpedientesJuridico.attachEvent("onXLS", function(grid){
            mostrarIndicadorCarga("indicador");
      });
      listadoExpedientesJuridico.attachEvent("onXLE", function(grid){
                  ocultarIndicadorCarga('indicador');
      });
      listadoExpedientesJuridico.load('/MidasWeb/siniestros/expedientejuridico/mostrarListadoJuridico.action');
}


function buscarExpedienteJuridico(){
      
      if( validaFormaBusquedaExpedienteJuridico() ){
            var form = jQuery("#buscarExpedienteJuridicoForm").serialize();
          listadoExpedientesJuridico.load('/MidasWeb/siniestros/expedientejuridico/buscarExpedienteJuridico.action?'+form);
      }else{
            mostrarMensajeInformativo("Debe ingresar al menos un valor al filtro", '10');
      }
      
}

function validaFormaBusquedaExpedienteJuridico(){
      var cont = 0;
      jQuery(".obligatorioBusquedaJuridico").each(function(index, value) { 
          if( jQuery(this).val() != "" ){
            cont++;
          }
      });
      
      if( cont > 0 ){
            return true;
      }else{
            return false;
      }
}

function cerrar(){
	console.log('');
}


function guardar(){
	if(validaDatosRequeridos()){
		console.log('guardar');
		formParams = jQuery(document.expedienteJuridicoForm).serialize();
		var url = guardarPath+'?'+formParams;
		sendRequestJQ(null, url,"contenido", null);
	}
}



function consultar(idExpediente){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorExpedienteJuridico.action?expedienteJuridico.id="+idExpediente+"&esConsulta=true";
	sendRequestJQ(null, url,"contenido", null);
}

function editar(idExpediente){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorExpedienteJuridico.action?expedienteJuridico.id="+idExpediente+"&esConsulta=false";
	sendRequestJQ(null, url,"contenido", null);
}

 function mostrarBusqueda(){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/mostrarContenedorBusqueda.action";
	sendRequestJQ(null, url,"contenido", null);
}


function mostrarComentarios(){
	url = mostrarComentriosPath+'?expedienteJuridico.id='+ jQuery("#idExpedienteJuridico").val();
	console.log('mostrarComentrios:  '+url);
 	loadComentariosGrid(url);

}





function loadComentariosGrid(url){
 		 jQuery("#comentariosGrid").empty(); 
		 comentariosGrid = new dhtmlXGridObject('comentariosGrid');
		 comentariosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 comentariosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 comentariosGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicadorComentarios");
		    });
		 comentariosGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicadorComentarios');
		    });
		 comentariosGrid.load( url ) ;	
 }

function asignarSiniestro(){
	var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporteRecuperacion.action";
	mostrarVentanaModal("vm_busquedaRecuperacion", 'Busqueda', 200, 120, 1024, 650,url,null);
}

function validaModoConsulta(modoCOnsulta){
	if(modoCOnsulta){
		jQuery('.esConsulta').attr('disabled',true);	
	}
}

function consultarReporte(){
	var reporte = jQuery("#asignaNumSiniestro").val();
	if(reporte != ''){
		if (confirm('Al consultar el siniestro se perderan los datos no guardados')) {
			console.log(reporte);
			var res = reporte .split("-"); 
			console.log(res[0].trim());
			console.log(res[1].trim());
			console.log(res[2].trim());
			var params = "";
			params+="nsOficina="+res[0].trim()+"&";
			params+="nsConsecutivoR="+res[1].trim()+"&";
			params+="nsAnio="+res[2].trim();
			var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?"+params+'&vieneExpedienteJuridico=true&expedienteJuridico.id='+jQuery("#idExpedienteJuridico").val()+'&modoConsultaExpediente='+jQuery("#esConsulta").val();
			console.log('URL : '+url);
			sendRequestJQ(null, url,"contenido", "setConsultaReporte();");
		}
	}else{
		mostrarMensajeInformativo("Se debe asignar un siniestro", '10');	
	}
}


function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}

function validaVehiculoDetenido(){
	if(jQuery("#esConsulta").val() == 'false'){
		var vehiculoDetenido = jQuery("#s_vehiculoDetenido").val();
		if(vehiculoDetenido == '1'){
			jQuery("#s_estatusVehiculo").addClass("requerido");
			jQuery("#s_estatusVehiculo").removeAttr("disabled");
		}else{
			jQuery("#s_estatusVehiculo").attr("disabled","disabled");
			jQuery("#s_estatusVehiculo").removeClass("requerido");
			jQuery("#s_estatusVehiculo").attr("value","");
			
		}
		validaEstatusVehiculo();
	}
	
}

function validaEstatusVehiculo(){
	if(jQuery("#esConsulta").val() == 'false'){
		var estatusVehiculo = jQuery("#s_estatusVehiculo").val();
		if(estatusVehiculo == 'LIB'){
			jQuery("#dp_fechaLiberacionVehiculo").addClass("requerido");
			jQuery("#dp_fechaLiberacionVehiculo").removeAttr("disabled");
		}else{
			jQuery("#dp_fechaLiberacionVehiculo").attr("disabled","disabled");
			jQuery("#dp_fechaLiberacionVehiculo").removeClass("requerido");
			jQuery("#dp_fechaLiberacionVehiculo").attr("value","");
		}
	}
}

function validaTipoDeConclusion(esNuevo){
	if(esNuevo){
		jQuery("#s_tipoConclusion").attr("disabled","disabled");
		jQuery("#dp_fechaConclusionLegal").attr("disabled","disabled");
		jQuery("#dp_fechaConclusionAfirme").attr("disabled","disabled");
	}
}


function mostrarExpedienteJuridico(idExpediente){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorExpedienteJuridico.action?expedienteJuridico.id="+idExpediente;
	sendRequestJQ(null, url,"contenido", null);
}

function nuevo(){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorExpedienteJuridico.action?esConsulta=false";
	sendRequestJQ(null, url,"contenido", null);
}

function cerrar(){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorBusquedaExpedienteJuridico.action";
	sendRequestJQ(null, url,"contenido", null);
}

function exportarExcel(){
	var form = jQuery("#buscarExpedienteJuridicoForm").serialize();
	var url="/MidasWeb/siniestros/expedientejuridico/exportarExcel.action?" + form;
	window.open(url, "Expedientes_Juridicos");
}

function limpiarExpedienteJuridico(){
	jQuery('#buscarExpedienteJuridicoForm').each (function(){
		  this.reset();
	});
}