var listarRecuperacionCiaGrid;
var NAMESPACE_LISTAR_RECCOMPANIA 	= '/MidasWeb/siniestros/recuperacion/listado/recuperacion/compania/';
var ACTION_MOSTRAR_CONTENEDOR 		= NAMESPACE_LISTAR_RECCOMPANIA + 'mostrarContenedor.action';
var ACTION_LISTADO_GRID 			= NAMESPACE_LISTAR_RECCOMPANIA + 'buscar.action?';
var ACTION_LISTADO_EXPORTAR 		= NAMESPACE_LISTAR_RECCOMPANIA + 'exportarExcel.action?';
var ACTION_ASIGNAR_REFERENCIA 		= NAMESPACE_LISTAR_RECCOMPANIA + 'asignarReferencia.action?';
var ACTION_ELIMINAR_REFERENCIA 		= NAMESPACE_LISTAR_RECCOMPANIA + 'eliminarReferencia.action?';
var ACTION_ENTREGAR_CARTAS 			= NAMESPACE_LISTAR_RECCOMPANIA + 'entregarCartas.action?';
var ACTION_REASIGNAR_CARTAS 		= NAMESPACE_LISTAR_RECCOMPANIA + 'reasignarCartas.action?';
var	ACTION_REGISTRARENVIO_CARTAS	= NAMESPACE_LISTAR_RECCOMPANIA + 'registrarEnvioCartas.action?';
var ACTION_ELIMINARENVIO_CARTAS		= NAMESPACE_LISTAR_RECCOMPANIA + 'eliminarEnvioCartas.action?';
var ACTION_REGISTRARRECEP_CARTAS	= NAMESPACE_LISTAR_RECCOMPANIA + 'registrarRecepcionCartas.action?';
var ACTION_ELIMINARRECEP_CARTAS		= NAMESPACE_LISTAR_RECCOMPANIA + 'eliminarRecepcionCartas.action?';
var	MODULO_PROG_APLIC_INGRESOS		= 1;
var	MODULO_PROG_REG_ENT_CARTAS		= 2;
var	MODULO_PROG_REASIG_ELAB_CARTAS	= 3;
var	MODULO_PROG_ENV_REC_CARTAS		= 4;
var FECHAENVIO_COL = 10;
var FECHARECEPCION_COL = 12;
var OFICINAID_COL = 14;
var OFICINADESTINO_COL = 11;
var url;
var count;
var montoTotal;
var maxMap = {};
var minMap = {};
var gridMap = {};
var columnaMonto;
var oficinaSeleccion;
var fechaRecepcion;
var fechaEnvio;


maxMap[listarRecuperacionCiaGrid] = false;

function limpiarReasignacion(){
	jQuery('#divAcciones').hide();	
	jQuery('#cantidadRegistros').html('0');
	jQuery('#sumaRegistros').html('$0.00');
}

function init(){
	if(jQuery("#tipoModulo").val() == MODULO_PROG_APLIC_INGRESOS){
			columnaMonto = 12;
	}else if(jQuery("#tipoModulo").val() == MODULO_PROG_REG_ENT_CARTAS){
			columnaMonto = 16;
	}else if(jQuery("#tipoModulo").val() == MODULO_PROG_REASIG_ELAB_CARTAS){ 
			columnaMonto = 11;
	}else if(jQuery("#tipoModulo").val() == MODULO_PROG_ENV_REC_CARTAS){ 
			columnaMonto = 13;
	}
	onChangeCheckbox();
	jQuery("#lstTipoRecuperacion").val("CIA");
	jQuery("#lstMedioRecuperacion").val("REEMBOLSO");
}

function onChangeCheckbox(){
	var checked = listarRecuperacionCiaGrid.getCheckedRows(0);
	var count = 0;
	var montoTotal = 0;
	jQuery("#divAcciones").css('display','none');

	if(checked != ""){
		var a = checked.split(",");
		count = a.length;
		for(var i = 0; i < a.length; i++){
			montoTotal += parseFloat(listarRecuperacionCiaGrid.cells(a[i],columnaMonto).getValue());
		}
		jQuery("#divAcciones").css('display','block');
	}
	document.getElementById("cantidadRegistros").innerHTML = count;
	document.getElementById("sumaRegistros").innerHTML = montoTotal.formatMoney(2);

}

function validateCheckbox(rId, state){
	var error = false;
	if(jQuery("#tipoModulo").val() == MODULO_PROG_ENV_REC_CARTAS){
		if(state == true){
			//Validacion de oficina
			if(oficinaSeleccion == null || oficinaSeleccion == ''){
				oficinaSeleccion = listarRecuperacionCiaGrid.cells(rId, OFICINAID_COL).getValue();
			}else{
				if(oficinaSeleccion != listarRecuperacionCiaGrid.cells(rId, OFICINAID_COL).getValue()){
					mostrarMensajeInformativo('No es posible seleccionar el registro ya que la oficina es diferente a las seleccionadas', '20');
					listarRecuperacionCiaGrid.cells(rId,0).setValue(false);
					error = true;
				}
			}
			
			//Validacion de Cartas Enviadas. No es posible seleccionar si algunas tienen fecha de recepción y otras no.
			var fechaEnvioSel = listarRecuperacionCiaGrid.cells(rId, FECHAENVIO_COL).getValue();
			var fechaRecepcionSel = listarRecuperacionCiaGrid.cells(rId, FECHARECEPCION_COL).getValue();
			fechaEnvioSel = fechaEnvioSel == '' ? false : true;
			fechaRecepcionSel = fechaRecepcionSel == '' ? false : true;
			if(fechaEnvio == null || fechaEnvio == ''){
				fechaEnvio = fechaEnvioSel;
				fechaRecepcion = fechaRecepcionSel;
				fechaEnvio = fechaEnvio == '' ? false : true;
				fechaRecepcion = fechaRecepcion == '' ? false : true;				
			}else{
				if(fechaEnvio && fechaEnvioSel && fechaRecepcionSel != fechaRecepcion){
					mostrarMensajeInformativo('No es posible seleccionar el registro. Verifique que la seleccion coincida con la existencia de fechas en env\u00EDo y recepci\u00F3n', '20');
					listarRecuperacionCiaGrid.cells(rId,0).setValue(false);
					error = true;					
				}
				if(fechaEnvio != fechaEnvioSel){
					mostrarMensajeInformativo('No es posible seleccionar el registro. Verifique que la seleccion coincida con la existencia de fechas en env\u00EDo y recepci\u00F3n', '20');
					listarRecuperacionCiaGrid.cells(rId,0).setValue(false);
					error = true;						
				}
			}

			
			//R1
			
			
		}else{
			if(listarRecuperacionCiaGrid.getCheckedRows(0) == ""){
				resetGridValues();
			}
		}
		
	}
	return error;
}

function buscar(){
	 if ( validarBusqueda() == "s" ){
			removeCurrencyFormatOnTxtInput();
			var formParams = encodeForm(jQuery("#filtrosBusquedaListarRecuperacionCia"));
			url = ACTION_LISTADO_GRID;
			agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
			url += formParams ;
			console.log(url);
			limpiarReasignacion();
			getListarRecuperacionCiaGrid(url);
			initCurrencyFormatOnTxtInput();
	 }else{
		 mostrarMensajeInformativo('Se debe ingresar al menos un campo para realizar la búsqueda', '20');
	 }
}

function initGridListarRecuperacionCia(){
	url = ACTION_LISTADO_GRID;
	agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
	getListarRecuperacionCiaGrid(url);
}

function onCheckAll(){
	var error;
	for(var i = 0; i < listarRecuperacionCiaGrid.getRowsNum(); i++){
		listarRecuperacionCiaGrid.cellByIndex(i,0).setValue(jQuery("#chAll").attr("checked"))
		error = validateCheckbox(listarRecuperacionCiaGrid.getRowId(i), jQuery("#chAll").attr("checked"));
	}
	
	if (error){
		jQuery("#chAll").attr("checked", false);
	}
	onChangeCheckbox();
}

function getListarRecuperacionCiaGrid(url){
	listarRecuperacionCiaGrid = new dhtmlXGridObject('listarRecuperacionCiaGrid');
	listarRecuperacionCiaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	listarRecuperacionCiaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listarRecuperacionCiaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	listarRecuperacionCiaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	
	listarRecuperacionCiaGrid.attachEvent("onCheck", function(rId,cInd,state){
		onChangeCheckbox();
		validateCheckbox(rId, state);
	});

	listarRecuperacionCiaGrid.load(url);
	gridMap['listarRecuperacionCiaGrid'] = listarRecuperacionCiaGrid;
}

function asignarReferencia(){
	if(confirm("\u00BFSe asignar\u00E1 la referencia a todas las recuperaciones seleccionadas, desea continuar?")){
		url = ACTION_ASIGNAR_REFERENCIA;
		var ref = jQuery("#referenciaBancaria").val();
		if(ref == "" || ref == null || ref == undefined){
			mostrarMensajeInformativo('Debe capturar una referencia', '10');
		}else{
			agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
			agregarParametro('referenciaBancaria', ref);
			agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}	
}

function eliminarReferencia(){
	if(confirm("\u00BFSe eliminar\u00E1 la referencia de todas las recuperaciones seleccionadas, desea continuar?")){
		url = ACTION_ELIMINAR_REFERENCIA;
		agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
		agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
		sendRequestJQ(null, url, targetWorkArea, null);
	}	
}

function exportar(){
	var formParams = encodeForm(jQuery("#filtrosBusquedaListarRecuperacionCia"));
	url = ACTION_LISTADO_EXPORTAR;
	agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
	url += formParams;
	window.open(url, "RecuperacionesCompania");
}

function agregarParametro(param, val){
	url += param + "=" + val + "&";
}

function mostrarContenedor(){
	sendRequestJQ(null, ACTION_MOSTRAR_CONTENEDOR, targetWorkArea, null);
}

function currencyMask(el){
	console.log(el);
	 var ex = /^[0-9]+\.?[0-9]*$/;
	 console.log(el.value + ':' + ex.test(el.value));
	 if(ex.test(el.value)==false){
	   el.value = el.value.substring(0,el.value.length - 1);
	  }
}

function currencyMaskJQuery(el){
	el = jQuery(el);
	 var ex = /^[0-9]+\.?[0-9]*$/;
	 console.log(el.val() + ':' + ex.test(el.val()));
	 console.log(ex.test(el.val()));
	 if(ex.test(parseFloat(el.val()))==false){
		 console.log('yeaah');
	   el.val(el.val().substring(0,el.val().length - 1));
	  }
	 console.log(el.val());
}

function validarBusqueda(){
	
	var bandera = "n";
	jQuery(".cleaneable").each( function(){
		if( jQuery(this).val() != "" ){
			bandera = "s";
		}		
	});
	removeCurrencyFormatOnTxtInput();
	var montoInicial = parseFloat(jQuery("#montoInicial").val());
	var montoFinal = parseFloat(jQuery("#montoFinal").val());
	if(montoFinal < montoInicial){
		bandera = "n";
		mostrarMensajeInformativo('El rango de monto es incorrecto', '20');	
	}
	
	
    var fechaIni = stringToDate(jQuery("#fechaIniRegistro").val(), "dd/mm/yyyy", "/");
    var fechaFin = stringToDate(jQuery("#fechaFinRegistro").val(), "dd/mm/yyyy", "/");
    if(fechaFin < fechaIni){
    	bandera = "n";
    	mostrarMensajeInformativo('El rango de fechas es incorrecto', '20');
    }
	
	return bandera;
	
}

function reset(){
	jQuery(".cleaneable").each(
		function(){
			jQuery(this).val("");
		}
	);
	initGridListarRecuperacionCia();
}



function stringToDate(_date,_format,_delimiter)
{
            var formatLowerCase=_format.toLowerCase();
            var formatItems=formatLowerCase.split(_delimiter);
            var dateItems=_date.split(_delimiter);
            var monthIndex=formatItems.indexOf("mm");
            var dayIndex=formatItems.indexOf("dd");
            var yearIndex=formatItems.indexOf("yyyy");
            var month=parseInt(dateItems[monthIndex]);
            month-=1;
            var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
            return formatedDate;
}

function maxminGrid(idGrid, dif) {
	dif = (typeof dif === 'undefined') ? 120 : dif;
	if (typeof minMap[idGrid] === 'undefined') {
		var size = jQuery("#" + idGrid).height();
		minMap[idGrid] = size;
		maxMap[idGrid] = false;
	}
	var alto
	if (maxMap[idGrid]) {
		alto = minMap[idGrid];

	} else {
		alto = "+=" + dif;
	}
	jQuery("#" + idGrid).animate({
		height : alto
	}, 500, "linear", function() {
		grid = gridMap[idGrid];
		grid.setSizes();
	});
	maxMap[idGrid] = !maxMap[idGrid];

}

function entregarCartas(){
	if(confirm("\u00BFSe asignar\u00E1 la fecha de acuse a todas las recuperaciones seleccionadas, desea continuar?")){
		url = ACTION_ENTREGAR_CARTAS;
		var fechaAcuse = jQuery("#fechaAcuse").val();
		if(fechaAcuse == "" || fechaAcuse == null || fechaAcuse == undefined){
			mostrarMensajeInformativo('Debe capturar una fecha', '10');
		}else{
			agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
			agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
			agregarParametro('fechaAcuse', fechaAcuse);
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}		
}

function reasignarCartas(){
	if(confirm("\u00BFEst\u00E1 seguro de Reasignar las Recuperaciones seleccionadas para Elaboraci\u00f3n de las Cartas?")){
		url = ACTION_REASIGNAR_CARTAS;
		var oficinaReasignacion = jQuery("#listOficinasReasignacion").val();
		if(oficinaReasignacion == "" || oficinaReasignacion == null || oficinaReasignacion == undefined){
			mostrarMensajeInformativo('Debe capturar una oficina', '10');
		}else{
			agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
			agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
			agregarParametro('oficinaReasignacionId', oficinaReasignacion);
			sendRequestJQ(null, url, targetWorkArea, null);
		}
	}
}

function enviarRecibirCartas(){
	if(jQuery("#radioEnvioRecepcionE").attr('checked')){
		if(validaEnvioCartas()){
			url = ACTION_REGISTRARENVIO_CARTAS;
			var oficinaRecepcion = jQuery("#listOficinasRecepcion").val();
			if(oficinaRecepcion == "" || oficinaRecepcion == null || oficinaRecepcion == undefined){
				mostrarMensajeInformativo('Debe capturar una oficina', '10');
			}else{
				agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
				agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
				agregarParametro('oficinaRecepcion', oficinaRecepcion);
				sendRequestJQ(null, url, targetWorkArea, null);
			}
		}else{
			mostrarMensajeInformativo('No es posible enviar la informaci\u00F3n. Verifique que la seleccion coincida con la existencia de Fecha de Env\u00EDo y Oficina Recepci\u00F3n', '20');
		}
	}else{
		if(validaRecepcionCartas()){
			url = ACTION_REGISTRARRECEP_CARTAS;
			var personaRecepcion = jQuery("#personaRecepcion").val();
			if(personaRecepcion == "" || personaRecepcion == null || personaRecepcion == undefined){
				mostrarMensajeInformativo('Debe capturar una persona que recibe', '10');
			}else{
				agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
				agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
				agregarParametro('personaRecepcion', personaRecepcion);
				sendRequestJQ(null, url, targetWorkArea, null);
			}
		}else{
			mostrarMensajeInformativo('No es posible enviar la informaci\u00F3n. Verifique que la seleccion coincida con la existencia de Fecha de Recepci\u00F3n', '20');
		}			
	}
	resetGridValues();
}

function eliminarEnvio(){
	console.log("eliminarEnvio");
	if(validaEliminaEnvio()){
		console.log(true);
		url = ACTION_ELIMINARENVIO_CARTAS;
		agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
		agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
		sendRequestJQ(null, url, targetWorkArea, null);
		resetGridValues();		
	}else{
		mostrarMensajeInformativo('No es posible enviar la informaci\u00F3n. Verifique que la seleccion coincida con la existencia de Fecha de Env\u00EDo y Oficina Recepci\u00F3n', '20');
	}
}

function eliminarRecepcion(){
	console.log("eliminarRecepcion");
	if(validaEliminaRecepcion()){
		console.log(true);
		url = ACTION_ELIMINARRECEP_CARTAS;
		agregarParametro('tipoModulo', jQuery("#tipoModulo").val());
		agregarParametro('recuperacionesConcat', listarRecuperacionCiaGrid.getCheckedRows(0));
		sendRequestJQ(null, url, targetWorkArea, null);
		resetGridValues();
	}else{
		mostrarMensajeInformativo('No es posible enviar la informaci\u00F3n. Verifique que la seleccion coincida con la existencia de Fecha de Recepci\u00F3n', '20');
	}		
}

function onChangeEnvioRecepcion(val){
	if(val == 'E'){
		jQuery("#datosEnvio").css('display','table-cell');
		jQuery("#datosRecepcion").css('display','none');
	}else if (val == 'R'){
		jQuery("#datosEnvio").css('display','none');
		jQuery("#datosRecepcion").css('display','table-cell');
	}
}

function resetGridValues(){
	oficinaSeleccion = "";
	fechaRecepcion = "";
	fechaEnvio = "";
}

function validaEliminaEnvio(){
	var checked = listarRecuperacionCiaGrid.getCheckedRows(0);
	if(checked != ""){
		var a = checked.split(",");
		count = a.length;
		for(var i = 0; i < a.length; i++){
			if(listarRecuperacionCiaGrid.cells(a[i], OFICINADESTINO_COL).getValue() == "" ||
					listarRecuperacionCiaGrid.cells(a[i], FECHAENVIO_COL).getValue() == ""){
				return false;
			}
		}
	}
	return true;
}

function validaEliminaRecepcion(){
	var checked = listarRecuperacionCiaGrid.getCheckedRows(0);
	if(checked != ""){
		var a = checked.split(",");
		count = a.length;
		for(var i = 0; i < a.length; i++){
			if(listarRecuperacionCiaGrid.cells(a[i], FECHARECEPCION_COL).getValue() == ""){
				return false;
			}
		}
	}
	return true;
}

function validaEnvioCartas(){
	var checked = listarRecuperacionCiaGrid.getCheckedRows(0);
	if(checked != ""){
		var a = checked.split(",");
		count = a.length;
		for(var i = 0; i < a.length; i++){
			console.log(listarRecuperacionCiaGrid.cells(a[i], OFICINADESTINO_COL).getValue());
			console.log(listarRecuperacionCiaGrid.cells(a[i], FECHAENVIO_COL).getValue());
			if(listarRecuperacionCiaGrid.cells(a[i], OFICINADESTINO_COL).getValue() != "" ||
				listarRecuperacionCiaGrid.cells(a[i], FECHAENVIO_COL).getValue() != ""){
				return false;
			}
		}
	}
	return true;
}

function validaRecepcionCartas(){
	var checked = listarRecuperacionCiaGrid.getCheckedRows(0);
	if(checked != ""){
		var a = checked.split(",");
		count = a.length;
		for(var i = 0; i < a.length; i++){
			console.log(listarRecuperacionCiaGrid.cells(a[i], FECHARECEPCION_COL).getValue());
			if(listarRecuperacionCiaGrid.cells(a[i], FECHARECEPCION_COL).getValue() != ""){
				return false;
			}
		}
	}
	return true;
}