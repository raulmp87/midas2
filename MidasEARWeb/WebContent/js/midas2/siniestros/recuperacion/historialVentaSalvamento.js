var listadoSubastasSalvamentoGrid;

function iniGridSubastaSalvamento(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 listadoSubastasSalvamentoGrid = new dhtmlXGridObject('listadoSubastasSalvamentoGrid');	
	 listadoSubastasSalvamentoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoSubastasSalvamentoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoSubastasSalvamentoGrid.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 listadoSubastasSalvamentoGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });	
}

function cargaUrlSubastaSalvamento(){
	iniGridSubastaSalvamento();
	
	var recuperacionId = jQuery("#recuperacionId").val();
	if( recuperacionId != ""){
		listadoSubastasSalvamentoGrid.load('/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarHistorialDeSubastasGrid.action?recuperacionId='+recuperacionId);
	}

}


function cancelarAdjudicacion(){
	removeCurrencyFormatOnTxtInput();
	
	if( jQuery("#tieneVentaActiva").val() == "s" ){
		
		if( jQuery("#comentarioCancelacion").val() != "" ){
			var form = jQuery("#salvarSalvamentoForm").serialize();
			var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/cancelarAdjudicacion.action?"+form;
			
			if(confirm("¿Esta seguro que desea cancelar la venta?")){
				sendRequestJQ(null, url, targetWorkArea, null);
			}
		}else{
			mostrarMensajeInformativo("Ingrese el motivo de la cancelación", '10');
		}
		
	}else{
		mostrarMensajeInformativo("Para cancelar se requiere una venta activa", '10');
	}

}


function habilitarTextComentario(){
	jQuery("#comentarioCancelacion").attr("disabled",false);
}
