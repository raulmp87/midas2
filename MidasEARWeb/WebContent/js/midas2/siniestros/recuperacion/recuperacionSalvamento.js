

function guardarRecuperacionSVM(){
	if(validarGuardadoSalvamento() && validateAll(true) && confirm('\u00BFEst\u00E1 seguro que desea guardar la recuperaci\u00F3n?')){
		removeCurrencyFormatOnTxtInput();		
		sendRequestJQ(jQuery('#salvarSalvamentoForm')[0], '/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/salvarSalvamento.action',
				targetWorkArea, null);
	}
}

function validarGuardadoSalvamento(){		
	if(jQuery('#abandono').attr('checked')){
		jQuery('#fechaOcurrido').removeClass('jQrequired');
		if(jQuery('#motivoAbandonoIncosteable').val() == null || jQuery('#motivoAbandonoIncosteable').val() == ''){
			mostrarMensajeInformativo('Se requiere un motivo de abandono en caso de marcar dicha casilla', '10', null, null);
			return false;
		}
	}else{
		jQuery('#fechaOcurrido').addClass('jQrequired');
	}	
	return true;
}

function cancelarRecuperacionSalvamento(){	
	var motivo = jQuery('#motivoCancelacion').val();	
	if(motivo != null && motivo != ''){	
		if(estatusValidoParaCancelacion()){
			if(confirm('¿Está seguro que desea cancelar la recuperación?')){
				var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/cancelarSalvamento.action';
				removeCurrencyFormatOnTxtInput();
				sendRequestJQ(document.salvarSalvamento,ruta,targetWorkArea,null);
			}
		}else{
			mostrarMensajeInformativo('Solo se puede cancelar si el estatus es REGISTRADO o PENDIENTE.', '10', null, null);		
		}
	}else{
		mostrarMensajeInformativo('Debe proporcionar un motivo de cancelación', '10', null, null);
	}
}

function mostrarVentanaCancelarRecuperacionSalvamento(){
	var idRecuperacion = jQuery("#recuperacionId").val();
	console.log("idRecuperacion = " + idRecuperacion);
	if(estatusValidoParaCancelacion()){
		var formParams = jQuery(document.salvarSalvamento).serialize();
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarCancelarSalvamento.action?" + formParams;
		var TITULO = "Datos para Cancelaci\u00F3n";
		var cerrarVentana = "cerrarVentanaCancelarRecuperacion('vmCancelarRecueperacionSalvamento', " + idRecuperacion + ", '" + 
			jQuery('#estatusRecuperacion').val() + "',  'consultarRecuperacion(" + idRecuperacion + ")')";
		mostrarVentanaModal("vmCancelarRecueperacionSalvamento", TITULO,  1, 1, 660, 220, url, cerrarVentana);
	}else{
		mostrarMensajeInformativo('Solo se puede cancelar si el estatus es REGISTRADO o PENDIENTE.', '10', null, null);		
	}
}

function consultarRecuperacion(idRecuperacion){
	if(null!=idRecuperacion && idRecuperacion!=""){
		var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarContenedorRecuperacionSalvamento.action?soloConsulta=true&esNuevoRegistro=false&recuperacionId='+idRecuperacion;
		sendRequestJQ(null,ruta,targetWorkArea,null);
	}	
}

function estatusValidoParaCancelacion(){
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val();
	console.log(estatusRecuperacion);
	if(estatusRecuperacion == 'REGISTRADO' 
		|| estatusRecuperacion == 'PENDIENTE'){
		return true;
	}
	return false;
}


function cancelarRecuperacionSalvamentoVentana(){
	removeCurrencyFormatOnTxtInput();	
	var motivo = jQuery('#motivoCancelacion').val();
	if(motivo == null || motivo == ''){
		parent.mostrarMensajeInformativo('Se requiere un motivo para poder cancelar', '10', null, null);		
	}else if(confirm('\u00BFEst\u00E1 seguro que desea cancelar la recuperaci\u00F3n?')){
		parent.submitVentanaModal("vmCancelarRecueperacionSalvamento", document.cancelarRecuperacionForm);
	}	
}


function cargarCiudades(idEstado){
	 listadoService.getMapMunicipiosPorEstadoMidas(idEstado,
             function(data){
                addOptions(jQuery('#ciudadUbicacion')[0],data);
             });
	
}

function configurarVentanaCancelacion(){
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val();
	if(estatusRecuperacion != 'CANCELADO'){
		jQuery("#cancelarBtn").show();
	}else{
		jQuery("#motivoCancelacion").attr('disabled','true');
	}
}

function mostrarSiDatePickerSiVentaActiva(){
	if( jQuery("#tieneVentaActiva").val() == "s" ){
		jQuery("#fechaInicioSubastaPicker").hide();
		jQuery("#fechaInicioSubastaInput").show();
	}else{
		jQuery("#fechaInicioSubastaPicker").show();
		jQuery("#fechaInicioSubastaInput").hide();
	}
}