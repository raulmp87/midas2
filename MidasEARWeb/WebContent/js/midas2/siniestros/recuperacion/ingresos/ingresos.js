var listadoIngresosPendientesGrid;
var listadoIngresosReversaAplicarGrid;
var movtosManualesGrid;
function initListadoIngresosPendientesGrid(){  
      document.getElementById("pagingAreaIngresosPendientes").innerHTML = '';
      document.getElementById("infoAreaIngresosPendientes").innerHTML = '';
      jQuery("#listadoIngresosPendientesGrid").show();
      jQuery("#tituloListado").show();
      listadoIngresosPendientesGrid = new dhtmlXGridObject('listadoIngresosPendientesGrid');      
      listadoIngresosPendientesGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicador");});
      listadoIngresosPendientesGrid.attachEvent("onXLE", function(grid_obj){marcarIngresosPrevios();
                                                                                       deshabilitarCheckboxSiMedioEsIndemnizacion();
                                                                                       ocultarIndicadorCarga('indicador');unblockPage();});
      listadoIngresosPendientesGrid.attachEvent("onCheckbox",doOnCheckIngresos);
      listadoIngresosPendientesGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
            marcarIngresosPrevios();deshabilitarCheckboxSiMedioEsIndemnizacion();});
      var soloConsulta= jQuery("#soloConsulta").val();    
      if(soloConsulta!='true'){
            listadoIngresosPendientesGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
                     var self=this;
                     t.getElementsByTagName("input")[0].onclick=function(e){
                           if(this.checked){
                                listadoIngresosPendientesGrid.checkAll();
                                doOnCheckIngresos();
                           }else{
                                listadoIngresosPendientesGrid.uncheckAll();
                                doOnCheckIngresos();
                           }
                     }
                  }     
       }else {
            listadoIngresosPendientesGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
                     var self=this;
                  }
            listadoIngresosPendientesGrid.attachEvent("onXLE", function(grid_obj){
                  listadoIngresosPendientesGrid.checkAll();
                  jQuery("#ingresosConcat").val(listadoIngresosPendientesGrid.getCheckedRows(0));
                  calculaImportes();
                  listadoIngresosPendientesGrid.forEachRow(function(id){
                        listadoIngresosPendientesGrid.cells(id,0).setDisabled( true);
                        });
                  }
            );
      }    
}

function marcarIngresosPrevios(){
      var COLINDEX_CHECKSELECT = 0;
      var ingresosConcat = jQuery("#ingresosConcat").val();
      var arregloIngresos = ingresosConcat.split(",");     
      if(ingresosConcat){
            for (index = 0; index < arregloIngresos.length; index++) {
                  listadoIngresosPendientesGrid.forEachRow(function(id) {
                        if(id == arregloIngresos[index]){
                              listadoIngresosPendientesGrid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
                             return false;
                        }
                  });
            }
      }
}

function deshabilitarCheckboxSiMedioEsIndemnizacion(){
      var medioRecuperacion;
      var COLINDEX_CHECK = 0;
      var rowsNum = listadoIngresosPendientesGrid.getRowsNum();
      var count = 0;
      listadoIngresosPendientesGrid.forEachRow(function(id){
            medioRecuperacion = listadoIngresosPendientesGrid.getUserData(id,"medioRecuperacionHidden");
                  if(medioRecuperacion == 'INDEMNIZACION'){
                        listadoIngresosPendientesGrid.editStop(); 
                        listadoIngresosPendientesGrid.setCellExcellType(id,COLINDEX_CHECK,'ro');
                        listadoIngresosPendientesGrid.cellById(id,COLINDEX_CHECK).setValue("");
                  }});
}

function buscarIngresosPendientes(){
      var soloConsulta= jQuery("#soloConsulta").val();      
      if(soloConsulta=='true' || validarFormaIngresosPendientes() ){
            removeCurrencyFormatOnTxtInput();
            if(listadoIngresosPendientesGrid){
                  jQuery("#ingresosConcat").val(listadoIngresosPendientesGrid.getCheckedRows(0));
            }
            var form = jQuery("#buscarIngresosPendientesForm").serialize();
            initListadoIngresosPendientesGrid();
            var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarIngresosPendientes.action?'+form;
            console.log(url);
            listadoIngresosPendientesGrid.load(url);
      }else{
            mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
      }
}

function validarFormaIngresosPendientes(){
      var contadorElementos = 0;
      var elementos         = false;
      var rangoMonto        = false;
      var rangoFechaSol     = false;
      var rangoIniSini      = false;
      var rangoCancelacion  = false;
      if( convertirFecha(jQuery("#fechaRegistroDe").val()) > convertirFecha(jQuery("#fechaRegistroHasta").val()) ){
        	mostrarMensajeInformativo('La fecha inicial no puede ser mayor a la final', '10');
        	return false;
        }
      jQuery(".obligatorio").each(function(index, value) { 
          if( jQuery(this).val() != "" ){
            contadorElementos++;
          }
      });   
      if(contadorElementos > 0){
            elementos = true;
      }     
      if(elementos){
            return true;
      }else{
            return false;
      }
}

function validaCheckReferencias(elemento){
      if( jQuery(elemento).is(":checked") ){
            jQuery(elemento).attr("value",1);
      }else{
            jQuery(elemento).attr("value",0);
      }
}

function onClickHabilitarField(chckbox, txtfield, valorAlActivar){
	var esActivo = jQuery(chckbox).attr('checked');
	if(esActivo){
		jQuery(txtfield).val(valorAlActivar);
		jQuery(txtfield).removeAttr('disabled');
	}else{
		jQuery(txtfield).val(valorAlActivar);
		jQuery(txtfield).attr('disabled',true);
	}
}

function limpiarFormularioIngresosPendientes(){
      jQuery('#buscarIngresosPendientesForm').each (function(){
              this.reset();
      });
}

function doOnCheckIngresos(rowId,cellInd,state){
      jQuery("#ingresosConcat").val(listadoIngresosPendientesGrid.getCheckedRows(0));
      blockPage();
      calculaImportes();
      unblockPage();
      return true;
}
function doOnCheckAcreedores(rowId,cellInd,state){
      jQuery("#movsAcreedoresConcat").val(movsAcreedoresGrid.getCheckedRows(0));
      blockPage();
      calculaImportes();
      unblockPage();
      return true;
}
function doOnCheckDepositos(rowId,cellInd,state){
      jQuery("#depositosConcat").val(depositosBancariosGrid.getCheckedRows(0));
      blockPage();
      calculaImportes();
      unblockPage();
      return true;
}
function calculaImportes(){
      removeCurrencyFormatOnTxtInput();
      var depositos = '';
      var acreedores = '';
      var manuales = '';
      if(movsAcreedoresGrid){
            acreedores = movsAcreedoresGrid.getCheckedRows(0);
      }     
      if(depositosBancariosGrid){
            depositos=depositosBancariosGrid.getCheckedRows(0);
      }
      if(movtosManualesGrid){
    	  manuales=movtosManualesGrid.getCheckedRows(0);
      }
      var ingreso=jQuery("#ingresosConcat").val();
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var data="";
      jQuery.ajax({
            url: '/MidasWeb/siniestros/recuperacion/ingresos/calcularImportes.action?filtroIngresosPendientes.ingresosConcat='+ingreso
            	+'&filtroDepositos.depositosConcat='+depositos+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId  
            	+'&filtroMovAcreedor.movAcreedoresConcat='+acreedores +'&filtroMovManual.movManualConcat='+manuales  ,
            dataType: 'json',
            async:false,
            type:"POST",
            data: data,
            success: function(json){
                   var importe = json.importesDTO;
                 jQuery("#totalesIngresosAplicar").val(importe.totalesIngresosAplicar);
                 jQuery("#totalesDepositosBancarios").val(importe.totalesDepositosBancarios);
                 jQuery("#diferencia").val(importe.diferencia);
                 jQuery("#totalesMovimientosAcreedores").val(importe.totalesMovimientosAcreedores);
                 jQuery("#totalesMovimientosManuales").val(importe.totalesMovimientosManuales);    
            }
      });
      initCurrencyFormatOnTxtInput();
}

function limpiarDivsGeneral() {
      limpiarDiv('contenido_depositos');
      limpiarDiv('contenido_movimientosAcreedores');
      limpiarDiv('contenido_movimientosManuales');
}

function verTabDepositosBancarios(){
      limpiarDivsGeneral();
      var url = "/MidasWeb/siniestros/recuperacion/ingresos/mostrarDepositosBancarios.action";
      sendRequestJQ(null, url, 'contenido_depositos', null);
}

function verTabMovimientosAcreedores(){
      limpiarDivsGeneral();
      var url = "/MidasWeb/siniestros/recuperacion/ingresos/mostrarMovsAcreedores.action";
      sendRequestJQ(null, url, 'contenido_movimientosAcreedores', null);
}

function verTabMovimientosManuales(){
	limpiarDivsGeneral();
	var url = "/MidasWeb/siniestros/recuperacion/ingresos/mostrarMovsManuales.action";
	sendRequestJQ(null, url, 'contenido_movimientosManuales', null);
}

function onClickBuscarDepositosBancarios(){
      removeCurrencyFormatOnTxtInput();
      var consultaSinFiltro  =false;
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var depositos =  jQuery("#depositosConcat").val();   
      if( validateAll()){
            if( validaCamposObligatoriosBusquedaDepositos()){
                  if( validarFormaDepositosBancarios() ){
                        buscarDepositosBancarios(false);
                  }else{
                        mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
                  }
            }else{
                  mostrarMensajeInformativo('Capture los campos \"Nombre de Banco y Cuenta Bancaria\" o el campo \"Referencia Bancaria\"', '10');
            }
      }else{
            mostrarMensajeInformativo('Algunos de los parametros de busqueda ingresados es erroneo', '10');
      }     
}

function buscarDepositosBancarios(consultaSinFiltro){
      removeCurrencyFormatOnTxtInput();
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var depositos =  jQuery("#depositosConcat").val();   
      if(depositosBancariosGrid){
            jQuery("#depositosConcat").val(depositosBancariosGrid.getCheckedRows(0));
      }
      var form = jQuery("#buscarDepositosForm").serialize();
      initdepositosBancariosGrid();
      var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarDepositosBancarios.action?'+form+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId+'&consultaSinFiltro='+consultaSinFiltro;
      console.log(url);
      depositosBancariosGrid.load(url,function(){initCurrencyFormatOnTxtInput();});     
}

function validarFormaDepositosBancarios(){
      var contadorElementos = 0;
      var elementos         = false;
      var rangoMonto        = false;
      var rangoFechaSol     = false;
      var rangoIniSini      = false;
      var rangoCancelacion  = false;    
      if( convertirFecha(jQuery("#fechaInicio_t").val()) > convertirFecha(jQuery("#fechaFin_t").val()) ){
      	mostrarMensajeInformativo('La fecha inicial no puede ser mayor a la final', '10');
      	return false;
      }
      jQuery(".obligatorio").each(function(index, value) { 
          if( jQuery(this).val() != "" ){
            contadorElementos++;
          }
      });   
      if(contadorElementos > 0){
            elementos = true;
      }     
      if(elementos){
            return true;
      }else{
            return false;
      }
}

var depositosBancariosGrid;
function initdepositosBancariosGrid(){   
       document.getElementById("pagingAreaDepositos").innerHTML = '';
      document.getElementById("infoAreaDepositos").innerHTML = '';
      jQuery("#depositosBancariosGridContainer").show();
      jQuery("#tituloListadoDepositos").show();
      var soloConsulta= jQuery("#soloConsulta").val();
      depositosBancariosGrid = new dhtmlXGridObject('depositosBancariosGrid');   
      depositosBancariosGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorDepositos");});
      depositosBancariosGrid.attachEvent("onXLE", function(grid_obj){marcarDepositosPrevios();ocultarIndicadorCarga('indicadorDepositos');unblockPage();});
      depositosBancariosGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){marcarDepositosPrevios();});
      depositosBancariosGrid.attachEvent("onCheckbox",doOnCheckDepositos);
      if(soloConsulta!='true'){
            depositosBancariosGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
                     var self=this;
                     t.getElementsByTagName("input")[0].onclick=function(e){
                           if(this.checked){
                        	   var num =depositosBancariosGrid.getRowsNum();
                        	   if(num>100){
                        		   depositosBancariosGrid.uncheckAll();
                                   doOnCheckDepositos();
                                   this.checked=false;
                        		   mostrarMensajeInformativo('El total de depositos supera 100 Registros.', '10');
                        	   }else{
                        		   depositosBancariosGrid.checkAll();
                                   doOnCheckDepositos(); 
                        	   }
                           }else{
                                depositosBancariosGrid.uncheckAll();
                                doOnCheckDepositos();
                           }
                     }
                  }     
       }else {
            depositosBancariosGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
                     var self=this;
                  }
            depositosBancariosGrid.attachEvent("onXLE", function(grid_obj){
                  depositosBancariosGrid.checkAll();
                  jQuery("#depositosConcat").val(depositosBancariosGrid.getCheckedRows(0));
                  depositosBancariosGrid.forEachRow(function(id){
                        depositosBancariosGrid.cells(id,0).setDisabled( true);
                        });
                  }
            );
      }
}

function marcarDepositosPrevios(){
      var COLINDEX_CHECKSELECT = 0;
      var depositosConcat = jQuery("#depositosConcat").val();
      var arregloDepositos = depositosConcat.split(",");   
      if(depositosConcat){
            for (index = 0; index < arregloDepositos.length; index++) {
                  depositosBancariosGrid.forEachRow(function(id) {
                        if(id == arregloDepositos[index]){
                              depositosBancariosGrid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
                             return false;
                        }
                  });
            }
      }
}

function validaCamposObligatoriosBusquedaDepositos(){
      var referencia = jQuery("#referencia_t").val();
      var bancoId = jQuery("#bancos_s").val();
      var numeroCuenta = jQuery("#numeroCuenta_t").val();  
      if(referencia || 
                  bancoId && numeroCuenta){
            return true;
      }
      return false;
}

function limpiarFiltrosDepositosBancarios(){
      jQuery('#buscarDepositosForm').each (function(){
              this.reset();
      });
}

function limpiarFiltrosMovsAcreedores(){
      jQuery('#buscarMovsAcreedoresForm').each (function(){
              this.reset();
      });
}

function limpiarFiltrosMovsManuales(){
    jQuery('#buscarMovsManualesForm').each (function(){
            this.reset();
    });
}

var movsAcreedoresGrid;
function buscarMovsAcreedores(consultaSinFiltro){
      removeCurrencyFormatOnTxtInput();
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var movsAcreedoresConcat =  jQuery("#movsAcreedoresConcat").val(); 
                  if(movsAcreedoresGrid){
                        jQuery("#movsAcreedoresConcat").val(movsAcreedoresGrid.getCheckedRows(0));
                  }
                  var form = jQuery("#buscarMovsAcreedoresForm").serialize();
                  initMovsAcreedoresGrid();
                  var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarMovsAcreedores.action?'+form+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId+'&consultaSinFiltro='+consultaSinFiltro;
                  console.log(url);
                  movsAcreedoresGrid.load(url,function(){initCurrencyFormatOnTxtInput();});
}


function onClickBuscarMovsAcreedores(){
      removeCurrencyFormatOnTxtInput();
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var movsAcreedoresConcat =  jQuery("#movsAcreedoresConcat").val(); 
      if(validateAll()){
            if( validaFormaMovsAcreedores() ){
                  buscarMovsAcreedores(false);
            }else{
                  mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
            }
      }else{
            mostrarMensajeInformativo('Algunos de los parametros de busqueda ingresados es erroneo', '10');
      }
}


function eliminarMovimientoManual(idMovimiento){
	var url = '/MidasWeb/siniestros/recuperacion/ingresos/eliminarMovtoManual.action?filtroMovManual.idMovimiento=' + idMovimiento;
	if(confirm("\u00BFRealmente desea eliminar el registro?" )){
		movtosManualesGrid.cells(idMovimiento,0).setChecked(0);
		calculaImportes();
		sendRequestJQ(null, url, 'contenido_movimientosManuales', 'verTabMovimientosManuales()');
   }
	
}

function mostrarVentanaRegistrarMovManual(){
	var ingresoId = jQuery("#ingresoId").val();
	var url = '/MidasWeb/siniestros/recuperacion/ingresos/mostrarRegistrarMovtoManual.action?ingresoId=' + ingresoId;
	mostrarVentanaModal("vm_altaMovtoManual", 'Llene la informaci\u00F3n para el Movimiento Manual',  1, 1, 800, 400, url, null);
	
}

function onClickRegistrarMovManual(){
	if(validateAll()){
		removeCurrencyFormatOnTxtInput();
		parent.submitVentanaModal("vm_altaMovtoManual", document.movtoManualForm);
	}else{
		mostrarVentanaMensaje('20','Alguno de los campos capturados es err\u00F3neo', null);
    }
}

function onClickBuscarMovsManuales(){
	removeCurrencyFormatOnTxtInput();
    var soloConsulta= jQuery("#soloConsulta").val();
    var ingresoId= jQuery("#ingresoId").val();
    var movsManualesConcat =  jQuery("#movsManualesConcat").val();
    console.log("onClickBuscarMovsManuales: soloConsulta: " + soloConsulta + " ingresoId: " + ingresoId + " movsManualesConcat: " + movsManualesConcat);
    if(validateAll()){
             if( validaFormaMovsManuales() ){
            	 console.log("onClickBuscarMovsManuales: invocando buscarMovsManuales(false)");
                 buscarMovsManuales(false);
             }else{
                   mostrarMensajeInformativo('Seleccione algun parametro de busqueda', '10');
             }
    			
    }else{
          mostrarMensajeInformativo('Algunos de los parametros de busqueda ingresados es erroneo', '10');
    }
}


function buscarMovsManuales(consultaSinFiltro){
      removeCurrencyFormatOnTxtInput();
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var movsManualesConcat =  jQuery("#movsManualesConcat").val();
      console.log("buscarMovsManuales: soloConsulta: " + soloConsulta + " ingresoId: " + ingresoId + " movsManualesConcat: " + movsManualesConcat + " consultaSinFiltro: " + consultaSinFiltro);
                  if(movtosManualesGrid){
                	  console.log("buscarMovsManuales: obteniendo checked rows si grid no es nulo");
                        jQuery("#movsManualesConcat").val(movtosManualesGrid.getCheckedRows(0));
                  }
                  var form = jQuery("#buscarMovsManualesForm").serialize();
                  initMovsManualesGrid();
                  var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarMovsManuales.action?'+form+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId+'&consultaSinFiltro='+consultaSinFiltro;
                  console.log(url);
                  movtosManualesGrid.load(url,function(){initCurrencyFormatOnTxtInput();});
}

function initMovsManualesGrid(){
    document.getElementById("pagingAreaMovsManuales").innerHTML = '';
    document.getElementById("infoAreaMovsManuales").innerHTML = '';
    jQuery("#movsManualesGridContainer").show();
    jQuery("#tituloListadoMovsManuales").show();
    movtosManualesGrid = new dhtmlXGridObject('movtosManualesGrid');      
    movtosManualesGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorMovsManuales");});
    movtosManualesGrid.attachEvent("onXLE", function(grid_obj){marcarMovsManualesPrevios();ocultarIndicadorCarga('indicadorMovsManuales');unblockPage();});
    movtosManualesGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){marcarMovsManualesPrevios();});
    movtosManualesGrid.attachEvent("onCheckbox",doOnCheckManuales);
    var soloConsulta= jQuery("#soloConsulta").val();    
    if(soloConsulta!='true'){
    	movtosManualesGrid._in_header_master_checkbox=function(t,i,c){
                   t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
                   var self=this;
                   t.getElementsByTagName("input")[0].onclick=function(e){
                         if(this.checked){
                        	 movtosManualesGrid.checkAll();
                        	 doOnCheckManuales();
                         }else{
                        	 movtosManualesGrid.uncheckAll();
                        	 doOnCheckManuales();
                         }
                   }
                }     
     }else {
    	 movtosManualesGrid._in_header_master_checkbox=function(t,i,c){
                   t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
                   var self=this;
                }
    	 movtosManualesGrid.attachEvent("onXLE", function(grid_obj){
    		 movtosManualesGrid.checkAll();
                jQuery("#movsManualesConcat").val(movtosManualesGrid.getCheckedRows(0));
                calculaImportes();
                movtosManualesGrid.forEachRow(function(id){
                	movtosManualesGrid.cells(id,0).setDisabled( true);
                      });
                }
          );
    }
}

function doOnCheckManuales(rowId,cellInd,state){
    jQuery("#movsManualesConcat").val(movtosManualesGrid.getCheckedRows(0));
    blockPage();
    calculaImportes();
    unblockPage();
    return true;
}

function marcarMovsManualesPrevios(){
    var COLINDEX_CHECKSELECT = 0;
    var movsManualesConcat = jQuery("#movsManualesConcat").val();
    var arregloMovsManuales = movsManualesConcat.split(",");     
    if(ingresosConcat){
          for (index = 0; index < arregloMovsManuales.length; index++) {
        	  movtosManualesGrid.forEachRow(function(id) {
                      if(id == arregloMovsManuales[index]){
                    	  movtosManualesGrid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
                           return false;
                      }
                });
          }
    }
}

function initMovsAcreedoresGrid(){
      document.getElementById("pagingAreaMovsAcreedores").innerHTML = '';
      document.getElementById("infoAreaMovsAcreedores").innerHTML = '';
      jQuery("#movsAcreedoresGridContainer").show();
      jQuery("#tituloListadoMovsAcreedores").show();
      movsAcreedoresGrid = new dhtmlXGridObject('movsAcreedoresGrid');      
      movsAcreedoresGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorMovsAcreedores");});
      movsAcreedoresGrid.attachEvent("onXLE", function(grid_obj){marcarMovsAcreedoresPrevios();ocultarIndicadorCarga('indicadorMovsAcreedores');unblockPage();});
      movsAcreedoresGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){marcarMovsAcreedoresPrevios();});
      movsAcreedoresGrid.attachEvent("onCheckbox",doOnCheckAcreedores);
      var soloConsulta= jQuery("#soloConsulta").val();    
      if(soloConsulta!='true'){
            movsAcreedoresGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
                     var self=this;
                     t.getElementsByTagName("input")[0].onclick=function(e){
                           if(this.checked){
                                movsAcreedoresGrid.checkAll();
                                doOnCheckAcreedores();
                           }else{
                                movsAcreedoresGrid.uncheckAll();
                                doOnCheckAcreedores();
                           }
                     }
                  }     
       }else {
            movsAcreedoresGrid._in_header_master_checkbox=function(t,i,c){
                     t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
                     var self=this;
                  }
            movsAcreedoresGrid.attachEvent("onXLE", function(grid_obj){
                  movsAcreedoresGrid.checkAll();
                  jQuery("#movsAcreedoresConcat").val(movsAcreedoresGrid.getCheckedRows(0));
                  calculaImportes();
                  movsAcreedoresGrid.forEachRow(function(id){
                        movsAcreedoresGrid.cells(id,0).setDisabled( true);
                        });
                  }
            );
      }
}

function marcarMovsAcreedoresPrevios(){
      var COLINDEX_CHECKSELECT = 0;
      var movsAcreedoresConcat = jQuery("#movsAcreedoresConcat").val();
      var arregloMovsAcreedores = movsAcreedoresConcat.split(",");     
      if(ingresosConcat){
            for (index = 0; index < arregloMovsAcreedores.length; index++) {
                  movsAcreedoresGrid.forEachRow(function(id) {
                        if(id == arregloMovsAcreedores[index]){
                              movsAcreedoresGrid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
                             return false;
                        }
                  });
            }
      }
}

function validaFormaMovsAcreedores(){
      var contadorElementos = 0;
      var elementos         = false;
      var rangoMonto        = false;
      var rangoFechaSol     = false;
      var rangoIniSini      = false;
      var rangoCancelacion  = false;  
      
      if( convertirFecha(jQuery("#fechaInicioTraspaso_t").val()) > convertirFecha(jQuery("#fechaFinTraspaso_t").val()) ){
        	mostrarMensajeInformativo('La fecha inicial no puede ser mayor a la final', '10');
        	return false;
        }
      
      
      jQuery(".obligatorio").each(function(index, value) { 
          if( jQuery(this).val() != "" ){
            contadorElementos++;
          }
      });   
      if(contadorElementos > 0){
            elementos = true;
      }     
      if(elementos){
            return true;
      }else{
            return false;
      }
}


function validaFormaMovsManuales(){
    var contadorElementos = 0;
    var elementos         = false;
    var rangoMonto        = false;
    var rangoFechaSol     = false;
    var rangoIniSini      = false;
    var rangoCancelacion  = false;  
    
    if( convertirFecha(jQuery("#fechaInicioTraspaso_t").val()) > convertirFecha(jQuery("#fechaFinTraspaso_t").val()) ){
      	mostrarMensajeInformativo('La fecha inicial no puede ser mayor a la final', '10');
      	return false;
      }
    
    
    jQuery(".obligatorio").each(function(index, value) { 
        if( jQuery(this).val() != "" ){
          contadorElementos++;
        }
    });   
    if(contadorElementos > 0){
          elementos = true;
    }     
    if(elementos){
          return true;
    }else{
          return false;
    }
}



function cerrarDetalle(){
      var url                           = '/MidasWeb/siniestros/recuperacion/listado/ingresos/mostrarContenedor.action';
      if(confirm("¿Desea cerrar la pantalla? La información no almacenada se perderá." )){
           sendRequestJQ(null,url,targetWorkArea,null);
      }
      
}
function aplicarIngresos(){
	var ingreso=jQuery("#ingresosConcat").val();
    var variacion= parseFloat( jQuery("#variacion").val()) ; 
    var depositos=  '';
    var acreedores=  '';
    var manuales=  '';
	  if(movsAcreedoresGrid){
	        acreedores= movsAcreedoresGrid.getCheckedRows(0);
	  }     
	  
	  if(depositosBancariosGrid){
	        depositos=depositosBancariosGrid.getCheckedRows(0);
	  }
	  if(movtosManualesGrid){
    	  manuales=movtosManualesGrid.getCheckedRows(0);
      }
	  var url    =  '/MidasWeb/siniestros/recuperacion/ingresos/aplicarIngreso.action?filtroIngresosPendientes.ingresosConcat='
		  +ingreso+'&filtroDepositos.depositosConcat='+depositos+'&filtroMovAcreedor.movAcreedoresConcat='+acreedores+'&filtroMovManual.movManualConcat='+manuales ;
	  if(validarImportes()){
	        if(confirm("¿Desea Aplicar Ingresos?" )){
	                   removeCurrencyFormatOnTxtInput();
	                   sendRequestJQ(null,url,targetWorkArea,null);
	              } 
	   } 
}
function validarImportes(){
      removeCurrencyFormatOnTxtInput();
     var depositos=  '';
     var acreedores=  '';
     var manuales=  '';
     if(movsAcreedoresGrid){
	        acreedores= movsAcreedoresGrid.getCheckedRows(0);
	  }     
	  if(depositosBancariosGrid){
	        depositos=depositosBancariosGrid.getCheckedRows(0);
	  }
	 
	  if(movtosManualesGrid){
		  manuales=movtosManualesGrid.getCheckedRows(0);
	  }
      var ingreso=jQuery("#ingresosConcat").val();
      var variacion= parseFloat( jQuery("#variacion").val()) ;  
      var soloConsulta= jQuery("#soloConsulta").val();
      var ingresoId= jQuery("#ingresoId").val();
      var data="";
      var bandera=true;
      if(null==ingreso || ingreso ==''){
            mostrarMensajeInformativo('Selecccione el o los ingreso(s) a Aplicar', '10');
            return false;
      }
      
      if( (null==acreedores || acreedores=='') &&  (null==depositos || depositos=='') &&  (null==manuales || manuales=='')   ){
            mostrarMensajeInformativo('Selecccione los Depositos o Movimientos Acreedor/Manuales a Aplicar.', '10');
            return false;
      }
      jQuery.ajax({
            url: '/MidasWeb/siniestros/recuperacion/ingresos/validaAplicacionIngresos.action?filtroIngresosPendientes.ingresosConcat='
            	+ingreso+'&filtroDepositos.depositosConcat='+depositos+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId  
            	+'&filtroMovAcreedor.movAcreedoresConcat='+acreedores+'&filtroMovManual.movManualConcat='+manuales,
            dataType: 'json',
          async:false,
          type:"POST",
          data: data,
          success: function(json){
             var mensaje = json.mensajeAplicacion;
             if (null!= mensaje  ||  mensaje !=''){
                   mostrarMensajeInformativo(mensaje, '10');
                   bandera= false;
             }else{
                   bandera= true;
             }
          }
    });
        /*  jQuery.ajax({
            url: '/MidasWeb/siniestros/recuperacion/ingresos/calcularImportes.action?filtroIngresosPendientes.ingresosConcat='+ingreso+'&filtroDepositos.depositosConcat='+depositos+'&soloConsulta='+soloConsulta+'&ingresoId='+ingresoId  +'&filtroMovAcreedor.movAcreedoresConcat='+acreedores   ,
            dataType: 'json',
            async:false,
            type:"POST",
            data: data,
            success: function(json){
                   var importe = json.importesDTO;
                   var totalesIngresosAplicar= parseFloat( importe.totalesIngresosAplicar ) ;   
                   var totalesDepositosBancarios= parseFloat(importe.totalesDepositosBancarios) ;   
                   var diferencia= parseFloat(importe.diferencia) ;   
                   var totalesMovimientosAcreedores= parseFloat(importe.totalesMovimientosAcreedores) ;   
                   var totalesMovimientosManuales= parseFloat(importe.totalesMovimientosManuales) ;                  
                   jQuery("#totalesIngresosAplicar").val(importe.totalesIngresosAplicar);
                 jQuery("#totalesDepositosBancarios").val(importe.totalesDepositosBancarios);
                 jQuery("#diferencia").val(importe.diferencia);
                 jQuery("#totalesMovimientosAcreedores").val(importe.totalesMovimientosAcreedores);
                 jQuery("#totalesMovimientosManuales").val(importe.totalesMovimientosManuales);                   
                   if (totalesIngresosAplicar<=0 || null== totalesIngresosAplicar  ||  totalesIngresosAplicar ==''){
                         mostrarMensajeInformativo('No es posible aplicar los ingresos, el monto de Ingreso debe ser Mayor a Cero', '10');
                         bandera= false;
                   }else if(diferencia>variacion ){
                         mostrarMensajeInformativo('No es posible aplicar los ingresos, la diferencia entre Monto Ingresos y Movimientos es mayor a $'+variacion+'.00 ', '10');
                         bandera= false;
                   }else{
                         bandera= true;
                   }
            }
      });
      */
      initCurrencyFormatOnTxtInput();
      return bandera;
}

function iniDetaAcreedor(){
      var soloConsulta= jQuery("#soloConsulta").val();
      var  ingresoId= jQuery("#ingresoId").val();
      if(soloConsulta=='true' && null!=ingresoId && ingresoId!=''){
            buscarMovsAcreedores(true);
            jQuery("#filtrosM2").hide();       
      }else{
            jQuery("#filtrosM2").show();
            if(movsAcreedoresGrid){
                  jQuery("#movsAcreedoresConcat").val(movsAcreedoresGrid.getCheckedRows(0));
            }
            var acreedores=jQuery("#movsAcreedoresConcat").val();
            if(null!=acreedores && acreedores !='' ){
                  buscarMovsAcreedores(true);
            }           
      }
}
function iniDetaDeposito(){
      var soloConsulta= jQuery("#soloConsulta").val();
      var  ingresoId= jQuery("#ingresoId").val();
      if(soloConsulta=='true' && null!=ingresoId && ingresoId!=''){
            buscarDepositosBancarios(true);
            jQuery("#filtrosM2").hide();       
      }else{
            jQuery("#filtrosM2").show();
            if(depositosBancariosGrid){
                  jQuery("#depositosConcat").val(depositosBancariosGrid.getCheckedRows(0));
            }
            var depositos =jQuery("#depositosConcat").val();
            if(null!=depositos && depositos !='' ){
                  buscarDepositosBancarios(true);
            }           
      }
}
function iniDetaManual(){
    var soloConsulta= jQuery("#soloConsulta").val();
    var  ingresoId= jQuery("#ingresoId").val();
    if(soloConsulta=='true' && null!=ingresoId && ingresoId!=''){
          buscarMovsManuales(true);
          jQuery("#filtrosM2").hide();       
    }else{
          jQuery("#filtrosM2").show();
          if(movtosManualesGrid){
                jQuery("#movsManualesConcat").val(movtosManualesGrid.getCheckedRows(0));
          }
          var manuales = jQuery("#movsManualesConcat").val();
          if(null!=manuales && manuales !='' ){
                buscarMovsManuales(true);
          }           
    }
}
function iniDetaingreso(){
      var soloConsulta= jQuery("#soloConsulta").val();
      var  ingresoId= jQuery("#ingresoId").val();
      if(soloConsulta=='true' && null!=ingresoId && ingresoId!=''){
            buscarIngresosPendientes();
            calculaImportes();
            jQuery("#divIngresos").hide();
            jQuery("#btnAplicar").hide();
            jQuery("#btnHistorico").show();
      }else{
            jQuery("#btnHistorico").hide();
            jQuery("#btnAplicar").show();
            if( null!=ingresoId && ingresoId!=''){
                  jQuery("#divIngresos").hide();
                  //jQuery("#ingresosConcat").val(ingresoId);          
                  var form = jQuery("#buscarIngresosPendientesForm").serialize();
                  initListadoIngresosPendientesGrid();
                  var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarIngresosPendientes.action?'+form;
                  console.log(url);
                  listadoIngresosPendientesGrid.load(url);
            }else{
                  jQuery("#divIngresos").show();
            }
      }
}

function consultarHistorico(){
      var idToReporte = jQuery("#idToReporte").val();
      var ingresoId = jQuery("#ingresoId").val();
      var soloConsulta = jQuery("#soloConsulta").val();
      var tipoCancelacion = jQuery("#tipoDeCancelacion").val();
      console.log(idToReporte + "-" + soloConsulta + "-" + ingresoId + "-" );
      soloConsulta = (soloConsulta=='true')? 1 : (soloConsulta == 'false')? 0 : 1;
      var PANTALLA_ORIGEN = 'INGRESOS';
      console.log(idToReporte + "-" + soloConsulta + "-" + ingresoId + "-" );
      var url = "/MidasWeb/siniestros/cabina/reportecabina/historicomovimientos/mostrarContenedor.action?idToReporte=" + idToReporte 
      + "&soloConsulta=" + soloConsulta + "&pantallaOrigen=" + PANTALLA_ORIGEN + "&ingresoId=" + ingresoId+"&tipoDeCancelacion=" + tipoCancelacion;
      console.log(url);
      sendRequestJQ(null, url, targetWorkArea, null);
}









function initIngresoCancelacion(){
      var tipoDeCancelacion = jQuery("#tipoDeCancelacion").val();
      
      if( tipoDeCancelacion == 3 ){
            // # LIMPIAR LA FORMA DE BÚSQUEDA YA QUE SE CARGA CON EL DATOS DEL INGRESO SELECCIONADO
            jQuery(".filtroReversa").val("");
      }
      
}

function buscarIngresosPendientesPorCancelar(){
      
      if( validarFormaIngresosPendientes() ){
            
            removeCurrencyFormatOnTxtInput();
            if(listadoIngresosReversaAplicarGrid){
                  jQuery("#ingresosConcatReversaCancelarTemporalGrid").val(listadoIngresosReversaAplicarGrid.getCheckedRows(0));
            }
            var form = jQuery("#buscarIngresosPorCancelarReversaForm").serialize();
            
            initListadoIngresosPendientesPorCancelarGrid();
            //var url = '/MidasWeb/siniestros/recuperacion/ingresos/buscarIngresosPendientes.action?'+form;
            var url = '/MidasWeb/siniestros/recuperacion/ingresoCancelacion/buscarIngresosPendientes.action?'+form;
            console.log(url);
            listadoIngresosReversaAplicarGrid.load(url);
      }else{
            mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
      }
}

function initListadoIngresosPendientesPorCancelarGrid(){
      
      document.getElementById("pagingArea").innerHTML = '';
      document.getElementById("infoArea").innerHTML = '';

      listadoIngresosReversaAplicarGrid = new dhtmlXGridObject('listadoIngresosReversaAplicarGrid');     
      listadoIngresosReversaAplicarGrid.attachEvent("onXLS", function(grid_obj){
            blockPage();
            mostrarIndicadorCarga("indicador");
      });
      
       listadoIngresosReversaAplicarGrid.attachEvent("onXLE", function(grid_obj){
            marcarIngresosPreviosReversaCancelacion();
            deshabilitarCheckboxSiMedioEsIndemnizacion();
            ocultarIndicadorCarga('indicador');unblockPage();
      });
      listadoIngresosReversaAplicarGrid.attachEvent("onCheckbox",doOnCheckCancelarReversa);

      listadoIngresosReversaAplicarGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
            marcarIngresosPrevios();
            deshabilitarCheckboxSiMedioEsIndemnizacion();}
      );
           
       
       listadoIngresosReversaAplicarGrid._in_header_master_checkbox=function(t,i,c){
            t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
            var self=this;
      }
      listadoIngresosReversaAplicarGrid.attachEvent("onXLE", function(grid_obj){
            jQuery("#ingresosConcatReversaCancelarTemporalGrid").val(listadoIngresosReversaAplicarGrid.getCheckedRows(0));
           }
      );
      
}

function marcarIngresosPreviosReversaCancelacion(){
      var COLINDEX_CHECKSELECT = 0;
      var ingresosConcat = jQuery("#ingresosConcatReversaCancelarTemporalGrid").val();
      var arregloIngresos = ingresosConcat.split(",");     
      if(ingresosConcat){
            for (index = 0; index < arregloIngresos.length; index++) {
                  listadoIngresosReversaAplicarGrid.forEachRow(function(id) {
                        if(id == arregloIngresos[index]){
                              listadoIngresosReversaAplicarGrid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
                             return false;
                        }
                  });
            }
      }
}

function doOnCheckCancelarReversa(rowId,cellInd,state){
      jQuery("#ingresosConcatReversaCancelarTemporalGrid").val(listadoIngresosPendientesGrid.getCheckedRows(0));
      return true;
}

function limpiarFormularioIngresosPendientesPorCancelar(){
      jQuery(".filtroReversa").val("");
}

function seleccionarCancelacionPendienteAplicado(){
      var keyIngreso = jQuery("#ingresoId").val();
      var tipoCancelacion = 0;
      
      // RECUPERA OPCION SELECCIONADA POR EL USUARIO
      jQuery(".tipoCancelacionIngresoAplicado").each(function(index, value) {
          if( jQuery(this).is(":checked") ){
            tipoCancelacion = jQuery(this).val();
          }
      });   
      
      return tipoCancelacion;
}

function aplicarCancelacionPorDevolucion(){
      
      var aplicarTodos = jQuery("input[name='todasRecuperaciones']:checked").val();
      var motivoCancelacionPorDevolucion = jQuery("#motivosCancelacion").val();
      var cuenta                   = jQuery("#cuentaAcredora").val();
      var comentario               = jQuery("#comentarios").val();
      var ingresoId                = jQuery("#ingresoId").val();
      
      if( motivoCancelacionPorDevolucion == "" ){
            mostrarMensajeInformativo('Seleccione un motivo de cancelación', '10');
      }else if( cuenta == "" ){
            mostrarMensajeInformativo('Seleccione una cuenta acreedora', '10');
      }else{
            var url = "/MidasWeb/siniestros/recuperacion/ingresoCancelacion/cancelacionPorDevolucion.action?"
                        +"motivoCancelacionPorDevolucion="+motivoCancelacionPorDevolucion
                        +"&noCuentaAcredora="+cuenta
                        +"&comentarioCancelacionPorDevolucion="+comentario
                        +"&aplicarTodosIngresos="+aplicarTodos
                        +"&ingresoId="+ingresoId
                        
            sendRequestJQ(null,url,targetWorkArea,null); 
      }
      
}


function aplicarCancelacionCuentaAcreedora(){
      
      var tipoCancelacionIngresoAplicado = seleccionarCancelacionPendienteAplicado();
      
      if( tipoCancelacionIngresoAplicado > 0 ){
            
            var aplicarTodos = jQuery("input[name='todasRecuperaciones']:checked").val();
            var motivoCancelacionPorDevolucion = jQuery("#motivosCancelacion").val();
            var cuenta                   = jQuery("#cuentaAcredora").val();
            var comentario               = jQuery("#comentarios").val();
            var ingresoId                = jQuery("#ingresoId").val();
      
            if( motivoCancelacionPorDevolucion == "" ){
                  mostrarMensajeInformativo('Seleccione un motivo de cancelación', '10');
            }else if( cuenta == "" ){
                  mostrarMensajeInformativo('Seleccione una cuenta acreedora', '10');
            }else{
                  var url = "/MidasWeb/siniestros/recuperacion/ingresoCancelacion/cancelarACuentaAcreedora.action?"
                              +"motivoCancelacionPorDevolucion="+motivoCancelacionPorDevolucion
                             +"&noCuentaAcredora="+cuenta
                             +"&comentarioCancelacionPorDevolucion="+comentario
                             +"&aplicarTodosIngresos="+aplicarTodos
                             +"&ingresoId="+ingresoId
                              +"&tipoCancelacionIngresoAplicado="+tipoCancelacionIngresoAplicado;
                  
                  sendRequestJQ(null,url,targetWorkArea,null); 
            }
      }else{
            mostrarMensajeInformativo('Seleccione una opción de cancelación', '10');
      }     
}

function aplicarCancelacionReversa(){
      
	  var ingresosConcatReversa = "";

	  if( listadoIngresosReversaAplicarGrid != null ){
		  ingresosConcatReversa = listadoIngresosReversaAplicarGrid.getCheckedRows(0);
	  }
      
      var tipoCancelacionIngresoAplicado = seleccionarCancelacionPendienteAplicado();
      
      if( ingresosConcatReversa != "" ){
            if( tipoCancelacionIngresoAplicado > 0 ){
                  
                  var aplicarTodos = jQuery("input[name='todasRecuperaciones']:checked").val();
                  var motivoCancelacionPorDevolucion = jQuery("#motivosCancelacion").val();
                  var cuenta                     = jQuery("#cuentaAcredora").val();
                  var comentario                 = jQuery("#comentarios").val();
                  var ingresoId                  = jQuery("#ingresoId").val();
                  
                  if( motivoCancelacionPorDevolucion == "" ){
                        mostrarMensajeInformativo('Seleccione un motivo de cancelación', '10');
                  }else if( cuenta == "" ){
                        mostrarMensajeInformativo('Seleccione una cuenta acreedora', '10');
                  }else{
                        
                        var url = "/MidasWeb/siniestros/recuperacion/ingresoCancelacion/cancelarPorReversaDeIngreso.action?"
                              +"motivoCancelacionPorDevolucion="+motivoCancelacionPorDevolucion
                             +"&noCuentaAcredora="+cuenta
                             +"&comentarioCancelacionPorDevolucion="+comentario
                             +"&aplicarTodosIngresos="+aplicarTodos
                             +"&ingresoId="+ingresoId
                             +"&idIngresosConcatPorAplicar="+ingresosConcatReversa
                              +"&tipoCancelacionIngresoAplicado="+tipoCancelacionIngresoAplicado;
                        
                        sendRequestJQ(null,url,targetWorkArea,null); 
                  }
            }else{
                  mostrarMensajeInformativo('Seleccione una opción de cancelación', '10');
            }
      }else{
            mostrarMensajeInformativo('Seleccione los ingresos pendientes por aplicar en la búsqueda', '10');
      }
}

function onChangeBancosDepositos(idBanco){
      listadoService.getMapCuentasBancoMidas(idBanco,
             function(data){
                addOptions(jQuery('#numeroCuenta_t')[0],data);
             });
      
      
}

function convertirFecha(fecha){
	 var d=new Date();
	 var fecha = fecha.split("/");
	 return new Date(fecha[2],fecha[1],fecha[0]);
}


function validaOrigenDeducibleRecuperacionIngreso(){
	var origenRecuperacionIngreso = jQuery("#origenRecuperacionDeducible").val();
	
	if(origenRecuperacionIngreso == "PASE"){
		// DESHABILITAR BOTON DE CANCELAR
		jQuery('.btnCancelarIngresoAplicado').hide();
//		mostrarMensajeInformativo('No es posible cancelar un ingreso asociado a una recuperación de deducible que ya fue cobrado', '10');
	}
}
