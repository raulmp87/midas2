var ingresoDevolucionGrid;
var facturaListadoGrid;
var NAMESPACE_ADMINISTRAR_SOLICITUDES = '/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/';
var	NAMESPACE_AUTORIZAR_SOLICITUDES = '/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/';
var ACTION_MOSTRAR_BUSQUEDA = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'mostrarBusqueda.action';
var ACTION_LISTADO_GRID = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'buscar.action?';
var ACTION_LISTADO_EXPORTAR = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'exportar.action?';
var ACTION_SOLICITAR_CHEQUE = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'solicitarCheque.action?';
var ACTION_CONSULTAR_SOLICITUD_CHEQUE = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'consultarSolicitudCheque.action?';
var ACTION_CANCELAR_SOLICITUD_CHEQUE = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'cancelarSolicitudCheque.action?';
var ACTION_IMPRIMIR_SOLICITUD_CHEQUE = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'imprimirSolicitudCheque.action?';
var ACTION_ENVIAR_SOLICITUD_CHEQUE = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'enviarSolicitudCheque.action?';
var ACTION_AUTORIZAR_SOLICITUD_CHEQUE = NAMESPACE_AUTORIZAR_SOLICITUDES + 'autorizarSolicitudCheque.action?';
var ACTION_RECHAZAR_SOLICITUD_CHEQUE = NAMESPACE_AUTORIZAR_SOLICITUDES + 'rechazarSolicitudCheque.action?';
var ACTION_CARGAR_FACTURA = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'cargarFactura.action?';
var ACTION_LISTADOFACTURA_GRID = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'listarFactura.action?';
var ACTION_DETALLEVALIDACIONFACTURA = "/MidasWeb/siniestros/pagos/facturas/recepcionFacturas/verDetalleValidacionFactura.action?";
var url;

var PANTALLA_ORIGEN__IMPRESION= "IMPRESION";
var PANTALLA_ORIGEN_LISTADO="LISTADO" ;

function init(flujo){
	if (flujo == 'SOL'){
		ACTION_MOSTRAR_BUSQUEDA = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'mostrarBusqueda.action';
		ACTION_LISTADO_GRID = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'buscar.action?';
		ACTION_LISTADO_EXPORTAR = NAMESPACE_ADMINISTRAR_SOLICITUDES + 'exportar.action?';
	}else if (flujo == 'AUT'){
		ACTION_MOSTRAR_BUSQUEDA = NAMESPACE_AUTORIZAR_SOLICITUDES + 'mostrarBusqueda.action';
		ACTION_LISTADO_GRID = NAMESPACE_AUTORIZAR_SOLICITUDES + 'buscar.action?';
		ACTION_LISTADO_EXPORTAR = NAMESPACE_AUTORIZAR_SOLICITUDES + 'exportar.action?';
	}
}

function buscar(){
	 if ( validarBusqueda() == "s" ){
			removeCurrencyFormatOnTxtInput();
			var formParams = encodeForm(jQuery("#filtrosBusquedaIngresoDevolucion"));
			var url = ACTION_LISTADO_GRID + formParams ;
			getIngresoDevolucionGrid(url);
			initCurrencyFormatOnTxtInput();
	 }else{
		 mostrarMensajeInformativo('Se debe ingresar al menos un campo para realizar la búsqueda', '20');
	 }
}

function initGridIngresoDevolucion(){
	getIngresoDevolucionGrid(ACTION_LISTADO_GRID);
}

function getIngresoDevolucionGrid(url){
	ingresoDevolucionGrid = new dhtmlXGridObject('ingresoDevolucionGrid');
	ingresoDevolucionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	ingresoDevolucionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	ingresoDevolucionGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	ingresoDevolucionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });

	ingresoDevolucionGrid.load(url);

}

function getFacturaGrid(){

	mostrarIndicadorCarga('indicador');
	var form = jQuery('#cargaFacturaIngresoDevolucionForm').serialize();	
	
	var url = ACTION_LISTADOFACTURA_GRID + form + "&ingresoDevolucionId=" + jQuery('#id').val();
	document.getElementById("facturaListadoGrid").innerHTML = '';	
	facturaListadoGrid = new dhtmlXGridObject('facturaListadoGrid');

	facturaListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	

	facturaListadoGrid.enableEditEvents(true); 
	
	facturaListadoGrid.load(url);
}

function solicitar(id){
	url = ACTION_SOLICITAR_CHEQUE;
	agregarParametro('ingresoDevolucionId', id);
	sendRequestJQ(null, url, targetWorkArea, null);	
	
}

function consultar(id){
	url = ACTION_CONSULTAR_SOLICITUD_CHEQUE;
	agregarParametro('ingresoDevolucionId', id);
	sendRequestJQ(null, url, targetWorkArea, null);
	
}

function cancelar(id){
	if(confirm("\u00BFDesea Cancelar la Solicitud de Cheque?")){
		url = ACTION_CANCELAR_SOLICITUD_CHEQUE;
		agregarParametro('ingresoDevolucionId', id);
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function autorizar(id, pantallaOrigen){
	if(confirm("\u00BFSe enviar\u00E1 Solicitud de Cheque a Mizar, desea continuar?")){
		url = ACTION_AUTORIZAR_SOLICITUD_CHEQUE;
		agregarParametro('ingresoDevolucionId', id);
		agregarParametro('pantallaOrigen', pantallaOrigen);
		sendRequestJQ(null, url, targetWorkArea, 'buscar');
	}	
}

function rechazar(id){
	if(confirm("\u00BFDesea Rechazar la Solicitud de Cheque?")){
		url = ACTION_RECHAZAR_SOLICITUD_CHEQUE;
		agregarParametro('ingresoDevolucionId', id);
		sendRequestJQ(null, url, targetWorkArea, 'buscar');
	}	
}

function autorizarSolicitudCheque(){
	autorizar(jQuery("#ingresoDevolucionId").val(),PANTALLA_ORIGEN__IMPRESION);
}

function rechazarSolicitudCheque(){
	rechazar(jQuery("#ingresoDevolucionId").val());
}

function imprimir(id){
	url = ACTION_IMPRIMIR_SOLICITUD_CHEQUE;
	agregarParametro(url, 'ingresoDevolucionId', id);
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarImprimirSolicitud(id){
	if(id){
		var url = '/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/consultarImpresion.action?ingresoDevolucionId=' + id;
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
	}
}

function imprimirSolicitudCheque(){
	var ingresoDevolucionId = jQuery("#ingresoDevolucionId").val();
	var ES_PREVIEW = false;
	var esImprimible = jQuery("#esImprimible").val();
	if(ingresoDevolucionId){
		if(esImprimible == 'true'){
			var urlImprimir =	"/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/imprimirSolicitudCheque.action?ingresoDevolucionId="+ingresoDevolucionId+"&esPreview="+ES_PREVIEW;
			console.log(urlImprimir);
			window.open(urlImprimir, "OrdenExpedicionCheque");
		}else{
			mostrarMensajeInformativo('La Solicitud de Cheque requiere al menos una Recuperaci\u00F3n para poder imprimirse.' , '20');
		}
	}else{
		mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de ingreso devoluc\u00F3n v\u00E1lido para poder imprimir' , '20');
	}
}

function imprimirSolicitudChequeId(id){
	var ES_PREVIEW = true;
	var urlImprimir =	"/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/imprimirSolicitudCheque.action?ingresoDevolucionId="+id+"&esPreview="+ES_PREVIEW;
	window.open(urlImprimir, "OrdenExpedicionCheque");
}

function cerrarConsultaSolicitud(){
	urlCerrerConsulta = '/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/mostrarBusqueda.action';
	sendRequestJQ(null, urlCerrerConsulta, targetWorkArea, null);
}

function mostrarObservaciones(){
	jQuery("#contenedorObservaciones").show();
}

function mostrarBotonesAutorizarRechazar(){
	if(jQuery("#estatusIngresoDev").val() == 'PA'){
		jQuery("#btn_rechazar").show();
		jQuery("#btn_autorizar").show();
	}
}

function actualizarObservaciones(){
	var ingresoDevolucionId = 1;
//	jQuery("#ingresoDevolucionId").val();
	var observaciones = jQuery("#observaciones").val();
	var urlActualizarObs = "/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/guardarComentariosConsulta.action?ingresoDevolucionId="+ingresoDevolucionId+"&observaciones="+observaciones;
	sendRequestJQ(null, urlActualizarObs, targetWorkArea, null);
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}

function enviar(){
	if(validateAll(true)){
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery("#informacionSolicitudCheque").serialize();
		var url = ACTION_ENVIAR_SOLICITUD_CHEQUE + formParams ;
		sendRequestJQ(null, url, targetWorkArea, null);		
	}
}

function exportar(){
	var formParams = jQuery("#filtrosBusquedaIngresoDevolucion").serialize();
	var url = ACTION_LISTADO_EXPORTAR + formParams;
		window.open(url, "SolicitudesCheque");
}

function agregarParametro(param, val){
	url += param + "=" + val + "&";
}

function mostrarBusqueda(){
	sendRequestJQ(null, ACTION_MOSTRAR_BUSQUEDA, targetWorkArea, null);
}

function onChangeSubtotal(element){
	var subtotal =  parseFloat(jQuery("#subtotalNew").val());
	console.log(subtotal);
	if(subtotal <= jQuery("#h_subtotal").val()){
		jQuery("#ivaNew").val(jQuery("#h_iva").val() * subtotal / jQuery("#h_subtotal").val());
		jQuery("#ivaRetenidoNew").val(jQuery("#h_ivaRetenido").val() * subtotal / jQuery("#h_subtotal").val());
		jQuery("#isrNew").val(jQuery("#h_isr").val() * subtotal / jQuery("#h_subtotal").val());
		
		var importeTotal = subtotal + parseFloat(jQuery("#ivaNew").val()) - parseFloat(jQuery("#ivaRetenidoNew").val()) - parseFloat(jQuery("#isrNew").val());
		jQuery("#importeTotalNew").val(importeTotal);
		jQuery("#importeCheque").val(importeTotal);

		var montoRestante = parseFloat(jQuery("#h_importeTotal").val()) - importeTotal;
		jQuery("#montoRestante").val(montoRestante);
		
		var cuentaAcreedoraList=jQuery("#cuentaAcreedoraList");
		if(montoRestante){
			jQuery(cuentaAcreedoraList).addClass("jQrequired");
		}else{
			jQuery(cuentaAcreedoraList).removeClass("jQrequired");
		}

		if(subtotal < jQuery("#h_subtotal").val()){
			jQuery("#tipoDevolucionT").attr('checked',false);
			jQuery("#tipoDevolucionP").attr('checked',true);
		}else{
			jQuery("#tipoDevolucionT").attr('checked',true);
			jQuery("#tipoDevolucionP").attr('checked',false);
		}

	}else{
		mostrarMensajeInformativo('El monto del cheque/transferencia, no puede ser mayor a ' + jQuery("#h_subtotal").val() , '10');
		jQuery("#subtotalNew").val(parseFloat(jQuery("#h_subtotal").val()));
		onChangeSubtotal(element);
	}
}

function currencyMask(el){
	console.log(el);
	 var ex = /^[0-9]+\.?[0-9]*$/;
	 console.log(el.value + ':' + ex.test(el.value));
	 if(ex.test(el.value)==false){
	   el.value = el.value.substring(0,el.value.length - 1);
	  }
}

function currencyMaskJQuery(el){
	el = jQuery(el);
	 var ex = /^[0-9]+\.?[0-9]*$/;
	 if(ex.test(parseFloat(el.val()))==false){
	   el.val(el.val().substring(0,el.val().length - 1));
	  }
}

//----- Inicia funcionalidad Cargar archivo ----

function cargarFactura(){
  
	try {

		var ext
	
		if(dhxWins != null) 
			dhxWins.unload();
	
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
		adjuntarDocumento.setText("Carga de Factura");
		adjuntarDocumento.button("minmax1").hide();
		adjuntarDocumento.button("park").hide();
		adjuntarDocumento.setModal(true);
		adjuntarDocumento.center();
		adjuntarDocumento.denyResize();
		adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
			
	
		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setFilesLimit(1);
	    
	    vault.onAddFile = function(fileName) { 
	    	ext = this.getFileExtension(fileName); 
	    	if (ext != "xml") { 
	    		mostrarMensajeInformativo("Solo se pueden cargar Archivos XML",'20'); 
	    		return false; 
	    	} else 
	    		return true; 
	        
	     }; 
	     
	    vault.create("vault");
	    vault.setFormField("claveTipo", "16");
	        
	    vault.onBeforeUpload = function(files){
	    	jQuery("#file1").attr("name", "facturaXml");
	    }
	    
	    jQuery("#buttonUploadId").unbind("click");
	    jQuery("#buttonUploadId").click(function(){

	    var factParams = jQuery("#cargaFacturaSolicitudCheque").serialize();
	    var urlCarga = ACTION_CARGAR_FACTURA + factParams  + "&ingresoDevolucionId=" + jQuery("#id").val();
	    	blockPage();
			jQuery.ajaxFileUpload({
				url: urlCarga,
				secureuri:false,
				fileElementId: "file1",
				dataType: 'text',					
				success: function(data){
					
					unblockPage();
					parent.dhxWins.window("divContentForm").close();
					
					var respuesta = JSON.parse(jQuery(data).text()); 
					
					if( respuesta.tipoMensaje == 10 ){
						mostrarMensajeInformativo(respuesta.mensaje, '10');
					}else if(respuesta.numFacturasCargadas == 0){
						mostrarMensajeInformativo("No se cargo ninguna factura en el sistema, " +
								"revise que el RFC de la factura es correcto o que la factura no han sido previamente registradas", '20');					
					}
						
//					var url = "/MidasWeb/siniestros/pagos/facturas/recepcionFacturas/mostrarRecepcionFacturas.action?idBatch=" + respuesta.idBatch+"&idPrestadorServicio=" 
//					+ jQuery("#idPrestadorServicio").val() + "&numFacturasCargadas=" + respuesta.numFacturasCargadas;
					
//					sendRequestJQ(null,url,targetWorkArea,null);
					getFacturaGrid();
				},
				error: function(data){
					unblockPage()
					parent.dhxWins.window("divContentForm").close();
					mostrarMensajeInformativo('Ha ocurrido un error al intentar subir el archivo xml', '20');			
				}
			});
		});
	
	}catch(err) {
	    //document.getElementById("demo").innerHTML = err.message;
	}
}


jQuery.extend({
    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout		
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})
//----- Termina funcionalidad Cargar archivo ----

function verDetalleValidacion(idValidacionFactura, numeroFactura)
{	
	var url = ACTION_DETALLEVALIDACIONFACTURA +  "idValidacionFactura=" + idValidacionFactura;
	
	mostrarVentanaModal("DetValidacionesFactura", 'Detalle Validaciones Factura No. ' + numeroFactura, 50, 200, 700, 300, url);
}

function validarBusqueda(){
	
	var bandera = "n";
	jQuery(".cleaneable").each( function(){
		if( jQuery(this).val() != "" ){
			bandera = "s";
		}		
	});
	
	return bandera;
	
}

function reset(){
	jQuery(".cleaneable").each(
		function(){
			jQuery(this).val("");
		}
	);
	initGridIngresoDevolucion();
}

function onChangeFormaPago(){
	var banco=jQuery("#banco");
	var clabe=jQuery("#clabe");
	if(jQuery("#formaPagoTRNBANC").attr('checked')){
		jQuery(banco).addClass("jQrequired");
		jQuery(clabe).addClass("jQrequired");
	}else{
		jQuery(banco).removeClass("jQrequired");
		jQuery(clabe).removeClass("jQrequired");
	}

}