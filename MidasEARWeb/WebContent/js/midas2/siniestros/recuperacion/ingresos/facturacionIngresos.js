var listadoIngresosGrid;

var listadoIngresosAFacturarGrid;
var cuentaCompleta;
var saved = true;
var emisionFacturaDB = null;
var autoInciso = null;
var ingresosFacturables;
var ingresoIdSelected;
var popUpReenvioFactura;

function getLast4Digits(cuentaCompleta){
	return (cuentaCompleta.substring(cuentaCompleta.length-4, cuentaCompleta.length));
}

function initListadoIngresosFacturablesGrid(){
	 
	 console.log("initListadoIngresosFacturablesGrid ");
	
	 listadoIngresosAFacturarGrid=null;
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';
	 jQuery("#listadoIngresosFacturablesGrid").show();

	 listadoIngresosGrid = new dhtmlXGridObject('listadoIngresosFacturablesGrid');	
	 listadoIngresosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoIngresosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoIngresosGrid.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 listadoIngresosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });
	 
	 listadoIngresosGrid.attachEvent("onRowSelect", function(id,ind){
		 //console.log("rowselectd id- ind" +id + "|" + ind);
		 
		     console.log("initListadoIngresosFacturablesGrid: click en row ");
		     
		 	 habilitDatosIngresoSeleccionado();
		 
			 var estatusFactura = listadoIngresosGrid.cells(id,4).getValue();
			 var tipoRecuperacion = listadoIngresosGrid.getRowAttribute(listadoIngresosGrid.getSelectedId(),"codeRecuperacionId");
			 
			 //OBTENER DATOS SALVAMENTO VEHICULO
			 if( tipoRecuperacion == 'SVM' ){
				 jQuery("#idTipoSalvamento").show();
				 obtenerDatosSalvamentoVehiculo(listadoIngresosGrid.getSelectedId());
			 }else{
				 jQuery("#idTipoSalvamento").hide();
			 }
			 
	
			 if(verificaEdicion()){
				 clearInformacionFactura();
			 	if(estatusFactura != ""){
			 		obtenerInformacionEmisionFactura();
			 		cuentaCompleta = jQuery("#idCuenta").val();
			 		var digitosCuenta = getLast4Digits(cuentaCompleta); //.substring(cuentaCompleta.length-4, cuentaCompleta.length);
			 		jQuery("#idCuenta").val(digitosCuenta);
			 		limpiarCombosPorTipoRecuperacion(tipoRecuperacion);
			 	}else{
			 		console.log("initListadoIngresosFacturablesGrid recuperacion: "+tipoRecuperacion);
			 		//obtenerInformacionFactura();
			 		if(tipoRecuperacion == "DED" || tipoRecuperacion == "CRU" || tipoRecuperacion == "JUR"){
			 			//jQuery("#listaTiposPersona").val("ASEG");
			 			//jQuery("#listaTiposPersona option[value='PROVEE']").remove();
			 			obtenerInformacionAsegurado("CRE");
			 			
			 		}else{
			 			//jQuery("#listaTiposPersona").val("PROVEE");
			 			//jQuery("#listaTiposPersona option[value='ASEG']").remove();
			 			//jQuery("#listaTiposPersona option[value='OTRAPER']").remove();
			 			obtenerInformacionProveedor();
			 			
			 		}
			 		
			 		limpiarCombosPorTipoRecuperacion(tipoRecuperacion);
			 	}
			 }else{
				 mostrarMensajeInformativo("Se perderán los cambios que haya realizado por favor presione guardar antes de seleccionar otro ingreso", '10');
				 listadoIngresosGrid.selectRowById(ingresoIdSelected);
			 } 
		 
	 });
	
	 //jQuery("#divExcelBtn").show();
}



function verificaEdicion(){
	var editionValues = [];
	if (emisionFacturaDB != null) {
		//console.log("eSTADO NAME: "+jQuery("#idEstadoName").val());
		//console.log("Emision DB: "+emisionFacturaDB.estado);
		editionValues = [[emisionFacturaDB.tipoPersona, 			"#listaTiposPersona"],
	                     [emisionFacturaDB.nombreRazonSocial, 		"#idNombrePersona"],
	                     [emisionFacturaDB.rfc, 					"#idRFC"],
	                     [emisionFacturaDB.calle, 					"#idCalle"],
	                     [emisionFacturaDB.telefono, 				"#idTelefono"],
	                     [emisionFacturaDB.email, 					"#idEmail"],
	                     [emisionFacturaDB.concepto, 				"#idConcepto"],
	                     [emisionFacturaDB.estadoMidas.id, 			"#idEstadoName"],
	                     [emisionFacturaDB.ciudadMidas.id, 			"#idCiudadName"],
	                     [emisionFacturaDB.coloniaMidas.id, 		"#idColoniaName"],
	                     [emisionFacturaDB.cp, 						"#cpName"],
	                     [emisionFacturaDB.descripcionFacturacion, 	"#idDescripcion"],
	                     [emisionFacturaDB.id, 						"#emisionFacturaId"],
	                     [emisionFacturaDB.metodoPago, 				"#listaMetodosPago"],
	                     [emisionFacturaDB.observacionesFacturacion, "#idObservaciones"],
	                     [emisionFacturaDB.digitos, "#idCuenta"]];
		
		console.log("DB FACTURA: "+emisionFacturaDB.toSource());
	}
	
	for(var i=0; i<editionValues.length; i++) {
		console.log(editionValues[i][1]+" = "+editionValues[i][0]+":"+jQuery(editionValues[i][1]).val());
		if((editionValues[i][0] == null || editionValues[i][0] == 'null') && jQuery(editionValues[i][1]).val() == ""){
			//console.log("-------"+editionValues[i][0]+" "+jQuery(editionValues[i][1]).val());
			//console.log("------- ES NULO");
		}else{
			if(editionValues[i][1] == "#idCuenta"){
				if(getLast4Digits(editionValues[i][0]+"") != getLast4Digits(jQuery(editionValues[i][1]).val()))
					return false;
				else
					return true;
			}
			if(editionValues[i][0] != jQuery(editionValues[i][1]).val()){
				//console.log( "SON DIFERENTES VALORES :"+editionValues[i][0]+": "+jQuery(editionValues[i][1]).val() );
				return false;
			}
				
		}
		
	}
	saved = true;
	return saved;
}



function obtenerInformacionEmisionFactura(){
	ingresoIdSelected = listadoIngresosGrid.getSelectedId();
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerEmisionFactura.action?ingresoIdSelected="+ingresoIdSelected+"";
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:false,
        type:"POST",
        data: "",
        success: function(json){
        	emisionFacturaDB = null;
        	emisionFacturaDB = json.emisionFactura;  
        	jQuery("#listaTiposPersona").val(emisionFacturaDB.tipoPersona);
        	jQuery("#idNombrePersona").val(emisionFacturaDB.nombreRazonSocial);
        	jQuery("#idRFC").val(emisionFacturaDB.rfc);
        	jQuery("#idCalle").val(emisionFacturaDB.calle);
        	jQuery("#idTelefono").val(emisionFacturaDB.telefono);
        	jQuery("#idEmail").val(emisionFacturaDB.email);
        	jQuery("#idImporte").val(emisionFacturaDB.importe);
        	jQuery("#idIVA").val(emisionFacturaDB.iva);
        	jQuery("#idTotal").val(emisionFacturaDB.totalFactura);        	
        	jQuery("#idConcepto").val(emisionFacturaDB.concepto);
        	setEmisionFacturaDireccion(emisionFacturaDB);
        	jQuery("#idDescripcion").val(emisionFacturaDB.descripcionFacturacion);
        	jQuery("#emisionFacturaId").val(emisionFacturaDB.id);
        	jQuery("#listaMetodosPago").val(emisionFacturaDB.metodoPago);
        	jQuery("#listaMetodosPago").change();
        	jQuery("#idObservaciones").val(emisionFacturaDB.observacionesFacturacion);
        	jQuery("#idCuenta").val(emisionFacturaDB.digitos);
        	
        	loaded = true;
        }
  });
}

function obtenerIngresosFacturables(){
	removeCurrencyFormatOnTxtInput();	
	var form = jQuery("#facturarIngresoForm").serialize();	
	ingresosFacturables = form.substring(form.indexOf("filtroIngresos=")+15,form.indexOf("&",form.indexOf("filtroIngresos="))).split("%2C");
	//console.log("ingresosFacturables: "+ingresosFacturables[0]);
	initListadoIngresosFacturablesGrid();
	var url = '/MidasWeb/siniestros/emision/factura/obtenerIngresosFacturables.action?'+form;
	listadoIngresosGrid.load(url);
	
}

function cerrar(){
	if(confirm("La Información NO almacenada se perder\u00E1 ¿Desea continuar? ")){
		var url = '/MidasWeb/siniestros/recuperacion/listado/ingresos/mostrarContenedor.action';
		sendRequestJQAsync(null, url, targetWorkArea, null);
	}
	
}

function setEmisionFacturaDireccion(emisionFactura){
	jQuery("#idCalle").val(emisionFactura.calle);
	console.log("Estado Midas: "+JSON.stringify(emisionFactura.estadoMidas));
	if(emisionFactura.estadoMidas != null){
		jQuery("#idEstadoName").val(emisionFactura.estadoMidas.id);
		listadoService.getMapMunicipiosPorEstadoMidas(emisionFactura.estadoMidas.id, function(data){
	                 addOptionOderByElementSin('idCiudadName',data);
	                 
	                 if(emisionFactura.ciudadMidas != null){
	                	jQuery("#idCiudadName").val(emisionFactura.ciudadMidas.id);
	             		listadoService.getMapColoniasPorCiudadMidas(emisionFactura.ciudadMidas.id,function(data2){
	             				addOptionsDireccionSin('idColoniaName',data2);
	             				
	             				if(emisionFactura.coloniaMidas != null){
	             					jQuery("#idColoniaName").val(emisionFactura.coloniaMidas.id);
	             					listadoService.getCodigoPostalPorColoniaMidas(emisionFactura.coloniaMidas.id,
	             						function(data3){
	             							addOptionsDireccionSin('cpName',data3);
	             							jQuery("#cpName").val(emisionFactura.cp);
	             							jQuery("#idColoniaName").val(emisionFactura.coloniaMidas.id);
	             			        	});
	             				}else{
	             					if(emisionFactura.cp != null){
	             						jQuery("#cpName").val(emisionFactura.cp);   
	             					}
	             				}
	                         });	
	             	}
	            });
	}

}

function setEmisionFactura(emisionFactura){
	console.log("Emision factura: "+JSON.stringify(emisionFactura));
	
	if( emisionFactura != null ){
		
		console.log("factura NO nula");
		jQuery("#idNombrePersona").val(emisionFactura.nombreRazonSocial);
		jQuery("#idRFC").val(emisionFactura.rfc);
		jQuery("#idTelefono").val(emisionFactura.telefono);
		jQuery("#idEmail").val(emisionFactura.email);
		jQuery("#idImporte").val(emisionFactura.importe);
		jQuery("#idIVA").val(emisionFactura.iva);
		jQuery("#idTotal").val(emisionFactura.totalFactura);        	
		jQuery("#idConcepto").val(emisionFactura.concepto);  	
		jQuery("#idDescripcion").val(emisionFactura.descripcionFacturacion);
		
		
		setEmisionFacturaDireccion(emisionFactura);
		
	}
}


function obtenerInformacionFactura(){
	
	ingresoIdSelected = listadoIngresosGrid.getSelectedId();
	
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerInformacionFactura.action?ingresoIdSelected="+ingresoIdSelected+"";
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:true,
        type:"POST",
        data: "",
        success: function(json){
        	var emisionFactura = json.emisionFactura; 
        	console.log("Emision Factura: "+emisionFactura);
        	setEmisionFactura(emisionFactura);
        	jQuery("#idImporte").val(emisionFactura.importe);
        	jQuery("#idIVA").val(emisionFactura.iva);
        	jQuery("#idTotal").val(emisionFactura.totalFactura);        	
        	jQuery("#idConcepto").val(emisionFactura.concepto);
        	jQuery("#idDescripcion").val(emisionFactura.descripcionFacturacion);
        }
	 });
}

/***
 * Origen CRE - desde la pantalla de búsqueda de ingresos y se crea o modifica la factura toma el ID del grid
 *        BUQ - desde la pantalla de búsqueda de facturas en la pantalla de edicion toma el ID de un hidden
 * @param origen
 */
function obtenerInformacionAsegurado(origen){
	
	if( origen == "CRE" ){
		ingresoIdSelected = listadoIngresosGrid.getSelectedId();
	}else{
		ingresoIdSelected =  jQuery("#ingresoId").val();
	}
	
	console.log("OBTENER INFORMACION Asegurado: "+origen+" INGRESO: "+ingresoIdSelected);
		
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerInformacionAsegurado.action?ingresoIdSelected="+ingresoIdSelected;
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:false,
        type:"POST",
        data: "",
        success: function(json){
        	console.log("OBTENER INFORMACION Asegurado: "+json);
        	var emisionFactura = json.emisionFactura; 
        	setEmisionFactura(emisionFactura);
        }
  });
}

function obtenerInformacionProveedor(){
	ingresoIdSelected = listadoIngresosGrid.getSelectedId();
	
	console.log("OBTENER INFORMACION PROVEEDOR : "+ingresoIdSelected);
	
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerInformacionProveedor.action?ingresoIdSelected="+ingresoIdSelected+"";
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:false,
        type:"POST",
        data: "",
        success: function(json){
        	var emisionFactura = json.emisionFactura;    
        	setEmisionFactura(emisionFactura);
        }
  });
}

function validaRequeridos(){
    var requeridos = jQuery(".requerido");
    var valido = true;
    requeridos.each(
          function(){
                var these = jQuery(this);
                if( isEmpty(these.val()) ){
                	  //console.log(these.attr('id'))
                      these.addClass("errorField");
                      valido = false;
                } else {
                      these.removeClass("errorField");
                }
          }
    );
    return valido;
}


function guardarInformacionFacturacion(){
	ingresoIdSelected = listadoIngresosGrid.getSelectedId();
	try {
		var emisionFacturaId = listadoIngresosGrid.getRowAttribute(ingresoIdSelected,"emisionFacturaId");
	}catch(err) {
		mostrarMensajeInformativo('Antes de guardar seleccione el ingreso y la persona a facturar', '10');
	}
	
	if(validaRequeridos()){

		var emisionFactura = {
				nombreRazonSocial: 	jQuery("#idNombrePersona").val(),
				rfc: 				jQuery("#idRFC").val(),
				calle: 				jQuery("#idCalle").val(),
				telefono: 			jQuery("#idTelefono").val(),
				email: 				jQuery("#idEmail").val(),
				importe: 			jQuery("#idImporte").val(),
				iva: 				jQuery("#idIVA").val(),
				totalFactura: 		jQuery("#idTotal").val(),
				concepto: 			jQuery("#idConcepto").val(),
				estado: 			jQuery("#idEstadoName").val(),
				ciudad: 			jQuery("#idCiudadName").val(),
				colonia: 			jQuery("#idColoniaName").val(),
				cp:					jQuery("#cpName").val(),
				descripcionFacturacion: jQuery("#idDescripcion").val(),
				metodoPago:			jQuery("#listaMetodosPago").val(),			
				ingresoId: 			ingresoIdSelected,
				observacionesFacturacion: jQuery("#idObservaciones").val(),
				tipoPersona:		jQuery("#listaTiposPersona").val(),
				digitos:			jQuery("#idCuenta").val(),
				emisionFacturaId:	emisionFacturaId};
	
			var data1 = JSON.stringify(emisionFactura);
			var vUrl = "/MidasWeb/siniestros/emision/factura/guardarInformacionFactura.action";
			jQuery.ajax({
		        url: vUrl,
		        dataType: 'json',
		        async:false,
		        type:"POST",
		        contentType: 'application/json',
		        data: data1, 
		        success: function(json){
		        	var errores = json.errores;
		        	console.log(errores);
		        	saved = true;
		        	mostrarMensajeInformativo('Emision realizada exitosamente.', '30');
		        	listadoIngresosGrid.setRowAttribute(listadoIngresosGrid.getSelectedId(),"emisionFacturaId",json.emisionFacturaId);
		        	var url = '/MidasWeb/siniestros/emision/factura/obtenerIngresosFacturables.action?filtroIngresos='+ingresosFacturables.join();
		        	listadoIngresosGrid.load(url, function(){
		        									listadoIngresosGrid.selectRowById(ingresoIdSelected
		        									);        									
		        									obtenerInformacionEmisionFactura();
		        								  }
		        	);
		        }
		  });
	
	}else{
	  mostrarMensajeInformativo("Faltan campos obligatorios", '10');	
  }
}

function clearInformacionFactura(){
	jQuery("#idNombrePersona").val("");
	jQuery("#idRFC").val("");
	jQuery("#idCalle").val("");
	jQuery("#idTelefono").val("");
	jQuery("#idEmail").val("");
	/*jQuery("#idImporte").val("");
	jQuery("#idIVA").val("");
	jQuery("#idTotal").val("");        	
	jQuery("#idConcepto").val("");*/
	jQuery("#idEstadoName").val("");
	jQuery("#idCiudadName").val("");
	jQuery("#idColoniaName").val("");
	jQuery("#cpName").val("");      	
	/*jQuery("#idDescripcion").val("");
	jQuery("#emisionFacturaId").val("");
	jQuery("#listaMetodosPago").val("");
	jQuery("#idObservaciones").val("");
	jQuery("#idCuenta").val("");*/
}

function facturarPorMetodoPago(){
	//console.log("listaMetodosPago: "+jQuery("#listaMetodosPago").val());
	if(jQuery("#listaMetodosPago").val() == "NOIDENT" || jQuery("#listaMetodosPago").val() == "EFECT"){
		jQuery("#idCuenta").hide();
		jQuery("#idCuentaLabel").hide();
		jQuery("#idCuenta").removeClass("requerido");
	}else{
		jQuery("#idCuenta").show();
		jQuery("#idCuentaLabel").show();
		jQuery("#idCuenta").addClass("requerido");
	}
	
}

function setRequeridosDatosPersona(requeridos){
	if(requeridos){
		jQuery("#idNombrePersona").addClass("requerido");
		jQuery("#idRFC").addClass("requerido");
		jQuery("#idCalle").addClass("requerido");
		jQuery("#idColoniaName").addClass("requerido");
		jQuery("#idEstadoName").addClass("requerido");
		jQuery("#idCiudadName").addClass("requerido");
		jQuery("#cpName").addClass("requerido");
		jQuery("#idTelefono").addClass("requerido");
		jQuery("#idEmail").addClass("requerido");
	}else{
		jQuery("#idNombrePersona").removeClass("requerido");
		jQuery("#idRFC").removeClass("requerido");
		jQuery("#idCalle").removeClass("requerido");
		jQuery("#idColoniaName").removeClass("requerido");
		jQuery("#idEstadoName").removeClass("requerido");
		jQuery("#idCiudadName").removeClass("requerido");
		jQuery("#cpName").removeClass("requerido");
		jQuery("#idTelefono").removeClass("requerido");
		jQuery("#idEmail").removeClass("requerido");
	}
}

/***
 * Origen CRE - desde la pantalla de búsqueda de ingresos y se crea o modifica la factura toma el ID del grid
 *        BUQ - desde la pantalla de búsqueda de facturas en la pantalla de edicion toma el ID de un hidden, si es OTRAPER y origen BUQ buscará los datos guardados en EmisionFactura
 * @param origen
 */
function facturarATipoPersona(origen){

	if(jQuery("#listaTiposPersona").val()=="OTRAPER"){
		
		if( origen == "CRE" ){
			clearInformacionFactura();
			setRequeridosDatosPersona(true);
		}else{
			// SI ORIGINALMENTE FUE CREADO CON ASEG Y DESPUÉS SELECCIONA OTRAPER DEBE LIMPIAR LOS CAMPOS PARA SU CAPTURA
			// SI ORIGINALMENTE FUE CREADO CON OTRAPER DEBE IR A LA TABLA DE EMISION POR LOS DADOS
			if ( jQuery("#tipoPersona").val() == "OTRAPER" ){
				obtenerInformacionFacturaByEmsision();
			}else{
				clearInformacionFactura();
			}
			
		}
		
		
	}else{
		if(jQuery("#listaTiposPersona").val()=="ASEG"){	
			setRequeridosDatosPersona(false);
			obtenerInformacionAsegurado(origen);
		}
	}
}
	
function exportarExcel(){
	if( validarForma() ){
	 removeCurrencyFormatOnTxtInput();
	 var form = jQuery("#buscarIngresosForm").serialize();
	 var url = '/MidasWeb/siniestros/recuperacion/listado/ingresos/buscarIngresos.action?'+form;
		if(jQuery("#servicioParticular").val() == 0){
			url += "&filtroIngreso.servicioParticular=" + jQuery("#servicioParticular").val();
		}
		if(jQuery("#servicioPublico").val() == 0){
			url += "&filtroIngreso.servicioPublico=" + jQuery("#servicioPublico").val();
		}
	 window.open(url, "Ingresos");
	}else{
		mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
	}
	 
}

function generarFacturaElectronica()
{
	var selectedRowsIds = "";
	
	selectedRowsIds = listadoIngresosGrid.getCheckedRows(3);
	
	if(selectedRowsIds != ""){
		
		var arraySeleccionados = selectedRowsIds.split(',');
		
		for(var i = 0; i< arraySeleccionados.length; i++)
		{	
			if(listadoIngresosGrid.cellById(arraySeleccionados[i],4).getValue() != 'TRAMITE')
			{
				mostrarMensajeInformativo('Revise que todos los ingresos seleccionados cuenten con Estatus Factura en TRAMITE' , '20');
				return;
			}		
		}
		
		var url = "/MidasWeb/siniestros/emision/factura/generarFacturaElectronica.action?listaIngresosAFacturarStr=" + 
		selectedRowsIds;
		
		if(confirm("¿Est \u00E1 seguro que desea facturar los ingresos seleccionados? ")){
			
			sendRequestJQ(null, url, targetWorkArea, null);			
		}		
		
	}else{
		mostrarMensajeInformativo('Debe seleccionar al menos un ingreso por facturar' , '20');
	}
}

function verDetalleErrores(numeroIngreso, folioFactura)
{	
	var url = "/MidasWeb/siniestros/emision/factura/mostrarDetalleError.action?folioFactura=" 
		+ folioFactura;
	
	mostrarVentanaModal("DetValidacionFactIngreso", 'Detalle Validacion Ingreso No. ' + numeroIngreso, 50, 200, 700, 300, url);
}

function mostrarReenvioFactura(idEmisionFactura, folioFactura, correoFactura)
{
	var url = "/MidasWeb/siniestros/emision/factura/mostrarReenvioFactura.action?emisionFactura.id=" 
		+ idEmisionFactura + "&folioFactura=" + folioFactura + "&destinatariosStr=" + correoFactura;
	
	mostrarVentanaModal("facturaFormatoXML", 'Factura No. ' + folioFactura, 50, 200, 1000, 400, url);
	
	popUpReenvioFactura = mainDhxWindow.window('facturaFormatoXML');
	
}


function verFacturaXML(idEmisionFactura, folioFactura)
{
	var url = "/MidasWeb/siniestros/emision/factura/mostrarPantallaPDF.action?emisionFactura.id=" 
		+ idEmisionFactura + "&folioFactura=" + folioFactura;
		
	window.open(url, "facturaFormatoXML");	
}

function reenviarFactura(idEmisionFactura, correos, observaciones)
{
	var url = "/MidasWeb/siniestros/emision/factura/reenviarFactura.action?emisionFactura.id=" +
	idEmisionFactura + "&destinatariosStr=" + correos + "&observacionesReenvio=" + observaciones;	
	
	jQuery.ajax({
		"url" : url,
		"async": false,
		"dataType" : "text json",
		"success": function(data) {			
			
			if(JSON.parse(data).tipoMensaje == "10")
			{
				mostrarMensajeInformativo(JSON.parse(data).mensaje, '10');
				
			}else
			{
				popUpReenvioFactura.close();
				mostrarMensajeInformativo(JSON.parse(data).mensaje, '30');									
			}			
		 }
		});
	
}


function reFacturarEmision(){
	
	var ingresoId = jQuery("#ingresoId").val();
	var url = "/MidasWeb/siniestros/emision/factura/generarFacturaElectronica.action?listaIngresosAFacturarStr=" + ingresoId;
	var pantallaOrigen = jQuery("#pantallOrigen").val();
	var mensaje = "los ingresos seleccionados";
	
	// SOLO LA PANTALLA QUE VIENE DE BUSQUEDA DE FACTURAS EN EL DETALLE TIENE LA VARIABLE DEFINIDA
	if (typeof pantallaOrigen != 'undefined'){
		mensaje = "";
		url+="&pantallOrigen=OBF";
	}else{
		url+="&pantallOrigen=OBI";
	}

	if(confirm("¿Est \u00E1 seguro que desea re facturar "+mensaje+" ? ")){
		sendRequestJQ(null, url, targetWorkArea, null);			
	}		
		
}


/***
 * Origen CRE - desde la pantalla de búsqueda de ingresos y se crea o modifica la factura toma el ID del grid
 *        BUQ - desde la pantalla de búsqueda de facturas en la pantalla de edicion toma el ID de un hidden
 * @param origen
 */
function obtenerInformacionFacturaByEmsision(origen){
	
	emisionId =  jQuery("#emisionId").val();
	
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerEmisionFacturaByEmision.action?emisionId="+emisionId;
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:false,
        type:"POST",
        data: "",
        success: function(json){
        	
        	console.log("json ++++ :");
        	console.log(json);
        	var emisionFactura = json.emisionFactura; 
        	console.log(emisionFactura);
        	setEmisionFacturaEdicion(emisionFactura);
        }
	 });
}


function setEmisionFacturaEdicion(emisionFactura){
	jQuery("#idNombrePersona").val(emisionFactura.nombreRazonSocial);
	jQuery("#idRFC").val(emisionFactura.rfc);
	setEmisionFacturaDireccion(emisionFactura);
	jQuery("#idTelefono").val(emisionFactura.telefono);
	jQuery("#idEmail").val(emisionFactura.email);        	  	
}



function obtenerDatosSalvamentoVehiculo(ingresoId){
	console.log("OBTEER DATOS SALVAMENTO: "+ingresoId);
	var vUrl = "/MidasWeb/siniestros/emision/factura/obtenerDatosVehiculoSalvamento.action?ingresoIdSelected="+ingresoId;
	jQuery.ajax({
        url: vUrl,
        dataType: 'json',
        async:false,
        type:"POST",
        data: "",
        success: function(json){
        	console.log(json);
        	autoInciso = json.autoInciso; 
        	console.log(autoInciso);
        	setVehiculoSalvamento(autoInciso);
        }
	 });
}

function setVehiculoSalvamento(autoInciso){
	console.log(autoInciso);
	jQuery("#svmMarca").text(autoInciso.descMarca);
	jQuery("#svmTipo").text(autoInciso.descTipoUso);
	jQuery("#svmModelo").text(autoInciso.modeloVehiculo);
	jQuery("#svmSerie").text(autoInciso.numeroSerie);
	jQuery("#svmMotor").text(autoInciso.numeroMotor);
	jQuery("#svmColor").text(autoInciso.descColor);
}


/**
 * Deshabilitar datos al inicializar la pantalla 
 */
function initEmisionFacturacion(){
	if(jQuery('#emisionFacturaId').val() != true ){
		jQuery('input[type="text"], select, textarea').each(function(index,data) {
			jQuery(this).attr('disabled', true);
		}); 	
	}
}

/**
 * Habilita los datos al capturar una vez seleccionado el ingreso
 */
function habilitDatosIngresoSeleccionado(){
	jQuery(".obligatorio").attr('disabled', false);
	jQuery(".setNew").attr('disabled', false);
	jQuery("#idObservaciones").attr('disabled', false);
	
}

function limpiarCombosPorTipoRecuperacion(tipoRecuperacion){
	if(tipoRecuperacion == "DED" || tipoRecuperacion == "CRU" || tipoRecuperacion == "JUR"){
			jQuery("#listaTiposPersona").val("ASEG");
			jQuery("#listaTiposPersona option[value='PROVEE']").remove();
		}else{
			jQuery("#listaTiposPersona").val("PROVEE");
			jQuery("#listaTiposPersona option[value='ASEG']").remove();
			jQuery("#listaTiposPersona option[value='OTRAPER']").remove();
		}
}

function initComboFacturarAPorTipoRecuperacion( tipoRecuperacion ){
	
	jQuery("#listaTiposPersona option[value='ASEG']").remove();
	jQuery("#listaTiposPersona option[value='OTRAPER']").remove();
	jQuery("#listaTiposPersona option[value='PROVEE']").remove();
	
	var opAseg = new Option("ASEGURADO", "ASEG");
	var opOtra = new Option("OTRA PERSONA", "OTRAPER");
	var opProv = new Option("PROVEEDOR", "PROVEE");
	
	// RDN: Obtener Información de Ingreso a Facturar
	// PARA SALVAMENTO, PROVEEDOR Y COMPAÑIA SOLO SE HABILITA PROVEEDOR
	
	if( tipoRecuperacion == "CIA" || tipoRecuperacion == "SVM" || tipoRecuperacion == "PRV" ){
		jQuery("#listaTiposPersona").append(opProv);
	}
	
	// PARA DEDUCIBLE, CRUCERO Y JURIDICO SOLO SE HABILITA CONTRATANTE(ASEGURADO) Y OTRA PERSONA
	if ( tipoRecuperacion == "DED" || tipoRecuperacion == "CRU"){
		jQuery("#listaTiposPersona").append(opAseg);
		jQuery("#listaTiposPersona").append(opOtra);
	}
}

