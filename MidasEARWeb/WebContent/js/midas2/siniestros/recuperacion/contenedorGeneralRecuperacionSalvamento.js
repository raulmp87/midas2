var historialSubastaGrid;

function initIdentificadorTabs(){
	
	var contenedorTab=window["recuperacionTabBar"];
	contenedorTab.attachEvent("onSelect", function(id, lastId){
	    jQuery("#pestaniaSeleccionada").attr("value",id);
	    
	    if( jQuery("#pestaniaSeleccionada").val() == "indicadores" ){
	    	jQuery("#btnSalvarSalvamento").hide();
	    }else{
	    	jQuery("#btnSalvarSalvamento").show();
	    }
	    
	    if( jQuery("#pestaniaSeleccionada").val() == "prorrogaSalvamento" ){
	    	if( jQuery("#confirmacionDepositoSalvamento").val() != "" ){
	    		//
	    		console.log(" ++++ SI TIENE CONFIRMACION DE DEPOSITO ++++ ");
	    		jQuery("#fechaLimiteProrroga").attr("disabled",true); 
	    		jQuery("#comentarios").attr("disabled",true); 
	    		jQuery("#btnSalvarSalvamento").hide();
	    	}else{
	    		//
	    		console.log(" ++++ NO TIENE CONFIRMACION DE DEPOSITO ++++ ");
	    		jQuery("#btnSalvarSalvamento").show();
	    		jQuery("#fechaLimiteProrroga").attr("disabled",false);
	    		jQuery("#comentarios").attr("disabled",false); 
	    	}
	    }
	    
	    return true;
	});
	
}


function initReadOnly(){
	
	//set readonly class
	if(jQuery('#soloConsulta').val() == true || jQuery('#soloConsulta').val() == 'true'){
		jQuery('input[type="text"], input[type="checkbox"], select, textarea').each(function(index,data) {
			jQuery(this).attr('disabled', true);
		}); 	
		jQuery("#fechaInicioSubastaPicker").hide();
		jQuery("#fechaInicioSubastaInput").show();
		
		jQuery("#cancelarAdjudicacion").hide();
		
		jQuery(".ui-datepicker-trigger").hide();
	}
	
	jQuery('input[type="text"]').each(function(index,data) {
		if(jQuery(this).attr('readonly') == true){
			jQuery(this).addClass('disabled');
		}else{
			jQuery(this).removeClass('disabled');
		}
	   
	}); 
	
	if(jQuery('#esAccionCancelar').val() == true || jQuery('#esAccionCancelar').val() == 'true'){
		jQuery('#motivoCancelacion').removeClass('disabled');
		jQuery('#motivoCancelacion').removeAttr('disabled');
	}
	
	// INACTIVAR PESTAÑAS DE VENTA SALVAMENTO SI NO EXISTE FECHA SUBASTA
	initPestaniasVentaSalvamento();

}


function initPestaniasVentaSalvamento(){
	if( jQuery("#fechaOcurrido").val() == "" ){
		var contenedorTab=window["recuperacionTabBar"];
		var tabs=["ventaSalvamento","confirmacionIngresos","prorrogaSalvamento"];
		
		for(var i=0;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
}

function guardarSVM(){
	
	var pestania = jQuery("#pestaniaSeleccionada").attr("value");
	
	if( pestania == "" || pestania == "recuperacionSalvamento" ){
		//INCLUIDO EN recuperacionSalvamento.js
		guardarRecuperacionSVM();
	}else if ( pestania == "ventaSalvamento"){
		//INCLUIDO EN recuperacionSalvamento.js
		guardarVentaSalvamento();
	}else if ( pestania == "prorrogaSalvamento" ){
		//INCLUIDO EN solicitudProrrogaSalvamento.js
		solicitarProrroga();
	}else if ( pestania == "confirmacionIngresos" ){
		//INCLUIDO EN confirmacionIngreso.js
		guardarConfirmacionIngreso();
	}else if ( pestania == "historialSubasta" ){
		// INCLUIDO EN historialVentaSalvamento.js
		cancelarAdjudicacion();
	}
	
	
}


function convertirFecha(fecha){
	 var d=new Date();
	 var fecha = fecha.split("/");
	 return new Date(fecha[2],fecha[1],fecha[0]);
}