var listadoNotasCreditoGrid;

var popUp;
var COLINDEX_CHECKSELECT = 0;
var COLINDEX_ESTATUSNCR = 4;
var COLINDEX_CHECKCANCELAROC = 6;
var COLINDEX_TOTALOC = 11;
var COLINDEX_TOTALNC = 9;
var ETIQUETA_CONCEPTO_NCR = "Nota(s) de Crédito";
var ETIQUETA_CONCEPTO_OC = "Orden(es) de Compra";

var numOCSeleccionadas;
var montoOCSeleccionadas;
var numNCRSeleccionadas;
var montoNCRSeleccionadas;
var initGridOC;
var initGridNC;
var OP_AGREGAR = 1;
var OP_QUITAR = -1;


function mostrarContenedorNotasDeCredito(){
	var idRecuperacion  = jQuery('#h_idRecuperacion').val();
	var soloLectura = jQuery('#h_soloLectura').val();
	console.log('mostrarContenedorNotasDeCredito de recuperacion con id :'+idRecuperacion);
 	var url = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/notasDeCredito/mostrarContenedor.action?recuperacion.id="+idRecuperacion+"&soloLectura="+soloLectura;
 	idBatch = jQuery('#h_batchId').val();
 	if(typeof idBatch !== 'undefined'){
   		url = url+'&batchId='+idBatch;
 	};	
 	console.log('URL mostrarContenedorNotasDeCredito '+url);
 	sendRequestJQ(null, url,"contenido", null);
}

function cerrarNotas(){
	var idRecuperacion  = jQuery('#h_idRecuperacion').val();
	var soloLectura = jQuery('#h_soloLectura').val();
	if(soloLectura){
   		consultarRecuperacion(idRecuperacion,'PRV');
 	}else{
 		editarRecuperacion(idRecuperacion,'PRV');
 	}	
}


function listarNotasCreditoPendientes(){
	/*idRecuperacion = jQuery('#h_idRecuperacion').val();
	esEditable = jQuery('#h_esEditable').val();*/
	var formSerialized = jQuery(document.recepcionNotaCreditoForm).serialize();
	var url = mostrarNotasPendientesPath + '?'+formSerialized;
	//var url = mostrarNotasPendientesPath + '?recuperacion.id='+idRecuperacion+'&batchId='+jQuery('#h_batchId').val();
	loadGrid(url);
}

function cancelarNC(idNotaCredito){
	var formSerialized = jQuery(document.recepcionNotaCreditoForm).serialize();
	var url = cancelarNotaCreditoPath + '?'+formSerialized+'&notaCreditoId='+idNotaCredito;
	sendRequestJQ(null, url,"contenido", null);
}

function verDetalleValidacionNC(idValidacionNC, numeroNC){	
	var url = '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/notasDeCredito/verMensajes.action?' + "idValidacionNotaCredito=" + idValidacionNC;
	mostrarVentanaModal("DetValidacionesNCR", 'Detalle Validaciones Factura No. ' + numeroNC, 50, 200, 700, 300, url);
}

function deshabilitarGridCheckboxes(grid)
{
	console.log('deshabilitarGridCheckboxes');
	var numElementos = 0;
	var montoTotal = 0;
	
	if(jQuery('#h_esEditable').val() == 'true'){
		grid.forEachRow(function(id) { 				
			if(grid.cells(id,COLINDEX_ESTATUSNCR).getValue() != 'PROCESADA'){
				grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);						
			}
		});	
	}else  {
			grid.forEachRow(function(id) {  
			
			grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);	
			numElementos ++;
			montoTotal = montoTotal + grid.cells(id,COLINDEX_TOTALNC).getValue();
			
		});	
		
		actualizarPieGrid(grid, numElementos, ETIQUETA_CONCEPTO_NCR, montoTotal, true);		
	}
		
	
}

function actualizarPieGrid(grid, numElementos, concepto, montoTotal, requiereDetach)
{
	if(!isNaN(montoTotal))
	{
		montoTotal = formatCurrency(montoTotal).replace(/,/g , "&#x002C;"); //Formatear el texto y reemplazar la comma por codigo hexa
	}	  
	
	footerLabel = "<div style='float: left; color: #00a000; font-size: 10;' id='divExcelBtn'>Monto total de " +
	"<span style='font-weight:bold;color:black;'>" + numElementos + "</span>. " + concepto +
	" seleccionada(s): <span style='font-weight:bold;color:black;'>" + montoTotal + "</span></div>,#cspan,#cspan,#cspan,#cspan";
	
	if(requiereDetach)
	{
		grid.detachFooter(0);		
	}
	
	grid.attachFooter(footerLabel);	
}


function validarRegistrarNC()
{
	var idsNotasCreditoAsociar = listadoNotasCreditoGrid.getCheckedRows(COLINDEX_CHECKSELECT);
	var formSerialized = jQuery(document.recepcionNotaCreditoForm).serialize();
	
	if(idsNotasCreditoAsociar != ""){
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/notasDeCredito/registrar.action?"+formSerialized+'&notasCreditoSeleccionadasConcat='+idsNotasCreditoAsociar;
		sendRequestJQ(null,url,targetWorkArea,null);
	}
	else{
		mostrarMensajeInformativo("Debe asociar por lo menos una nota de crédito a la recuperacion",'20'); 
	}
}

function loadGrid(url){

		 jQuery("#listadoNotasCreditoGrid").empty(); 
		 listadoNotasCreditoGrid = new dhtmlXGridObject('notasCreditoListadoGrid');
		 listadoNotasCreditoGrid.attachHeader("&nbsp,#text_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,&nbsp,&nbsp");
		 listadoNotasCreditoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 listadoNotasCreditoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 listadoNotasCreditoGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicador");
		    });
		 listadoNotasCreditoGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
				deshabilitarGridCheckboxes(grid);
				actualizaNumeroDeArchivosProcesados();
		    });
		 listadoNotasCreditoGrid.load( url ) ;	
 }


 function actualizaNumeroDeArchivosProcesados(){
 	var numRows = listadoNotasCreditoGrid.getRowsNum();
 	console.log('Numero de archivos procesados : '+numRows);
 	jQuery('#t_umNotasCargadas').val(numRows);
 }


 //----- Inicia funcionalidad Cargar Archivo ----

function cargarZipNotasCredito(){
  
	try {	
		
	var ext;	
	
			
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga de Facturas");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
		

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");  
    vault.setFilesLimit(1);
    
    vault.onAddFile = function(fileName) { 
    	ext = this.getFileExtension(fileName); 
    	if (ext != "zip" && ext != "xml") { 
    		mostrarMensajeInformativo("Solo se pueden cargar Archivos XML o ZIP",'20'); 
    		return false; 
    	} else 
    		return true; 
        
     }; 
     
    vault.create("vault");
    vault.setFormField("claveTipo", "16");
        
    vault.onBeforeUpload = function(files){
    	jQuery("#file1").attr("name", "archivoNotasCredito");
    }
    
    jQuery("#buttonUploadId").unbind("click");
    jQuery("#buttonUploadId").click(function(){
    	console.log('INICIA EL ajaxFileUpload');
    	urlCargaArchivo = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/notasDeCredito/cargarArchivo.action?" +"&recuperacion.id=" 
			+ jQuery("#h_idRecuperacion").val() + "&extensionArchivo=" + ext+ "&idProveedor=" + jQuery("#h_noProveedor").val();
		console.log('urlCargaArchivo : '+urlCargaArchivo);
        
    	blockPage();
		jQuery.ajaxFileUpload({
			url: urlCargaArchivo,
			secureuri:false,
			fileElementId: "file1",
			dataType: 'text',					
			success: function(data){
				
				unblockPage();
				parent.dhxWins.window("divContentForm").close();
				
				var respuesta = JSON.parse(jQuery(data).text()); 
				
				if(respuesta.numNotasCargadas == 0)
				{
					mostrarMensajeInformativo("No se cargo ninguna nota de crédito en el sistema, " +
							"revise que el RFC de las notas de crédito es correcto o que las notas de crédito no han sido previamente registradas", '20');					
				}
				console.log(' Antes de armar el URL  ');		
				var url = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/notasDeCredito/mostrarContenedor.action?recuperacion.id="+respuesta.recuperacion.id+'&batchId='+respuesta.batchId;		
				console.log(' URL mostrarNotasPendientesPath '+url);		
				
				if(respuesta.tipoMensaje == '10')//Si ocurrio un error
				{
					url = url + "&mensaje= " + respuesta.mensaje + "&tipoMensaje= " + respuesta.tipoMensaje; 
				}
				
				sendRequestJQ(null,url,targetWorkArea,null);				
			},
			error: function(data){
				unblockPage()
				parent.dhxWins.window("divContentForm").close();
				mostrarMensajeInformativo('Ha ocurrido un error al intentar subir el archivo ZIP', '20');			
			}
		});
	});
   console.log('TERMINA EL ajaxFileUpload');
    
	}catch(err) {
	    //document.getElementById("demo").innerHTML = err.message;
	}
}


jQuery.extend({
    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout		
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})
//----- Termina funcionalidad Cargar archivo ----