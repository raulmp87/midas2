var listadoGridRecuperaciones;
var formRecuperacion = "#buscarRecuperacionesForm";
var rutaCancelacion = '';
function buscarListaEnter(e) {
    if (e.keyCode == 13) {
    	buscarRecuperaciones();
    }
}

function initEstruccturaGrid(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 listadoGridRecuperaciones = new dhtmlXGridObject('listadoGridRecuperaciones');	
	 listadoGridRecuperaciones.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoGridRecuperaciones.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoGridRecuperaciones.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 listadoGridRecuperaciones.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });

	 jQuery("#divExcelBtn").show();
}

function nuevaRecuperacion(){
	var url = "/MidasWeb/siniestros/recuperacion/listado/recuperaciones/definirRecuperacion.action";
	mostrarVentanaModal("vm_nuevaRecuperacion", 'Nueva Recuperacion', 200, 120, 300, 200,url,null);
}

function reenviarRecuperacion(){
	var url = "";
	var tipoRecuperacion  = jQuery("#tipoRecuperacion").val();
	if( tipoRecuperacion != "" ){
		url = "/MidasWeb/siniestros/recuperacion/listado/recuperaciones/definirRecuperacion.action?tipoRecuperacion="+jQuery("#tipoRecuperacion").val();
		if(tipoRecuperacion=="PRV"){
			parent.cerrarVentanaModal("vm_nuevaRecuperacion",'nuevaRecuperacionPRV();');
		}else if(tipoRecuperacion=="CIA"){
			parent.cerrarVentanaModal("vm_nuevaRecuperacion",'nuevaRecuperacionCIA();');
	
		}
		//NOTA: de acuerdo a los Casos de uso no existe un flujo para crear recuperacion de deducibles
		// de forma manual
		/*else if(tipoRecuperacion=="DED") 
		{
			parent.cerrarVentanaModal("vm_nuevaRecuperacion",'nuevaRecuperacionDED();');			
		}*/
		else{
			sendRequestJQ(null,url,targetWorkArea,null);
		}
	}
}
function nuevaRecuperacionPRV(){
	sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=false&esNuevoRegistro=true',targetWorkArea,null);
}

function nuevaRecuperacionDED(){
	sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionDeducible/obtenerCoberturas.action?soloConsulta=false&tipoMostrar='U'",targetWorkArea,null);
}
function nuevaRecuperacionCIA(){
	jQuery("#url").val("NUEVO"); 
	var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarAlerta.action?url=NUEVO";
	mostrarVentanaModal("vm_alertaRecuperacionCIA", 'Alerta Recuperaci\u00F3n CIA', 550, 310, 1000, 550,ruta,null);
}

function buscarRecuperaciones(){
	removeCurrencyFormatOnTxtInput();
	if( validarForma() ){
		var form = jQuery(formRecuperacion).serialize();
		initEstruccturaGrid();
		listadoGridRecuperaciones.load('/MidasWeb/siniestros/recuperacion/listado/recuperaciones/buscarRecuperaciones.action?'+form);
	}else{
		mostrarMensajeInformativo('Seleccion algún parametro de busqueda y/o ingrese ambos rangos', '10');
	}
}

function exportarExcel(){
	 removeCurrencyFormatOnTxtInput();
	 var form = jQuery(formRecuperacion).serialize();
	 var url="/MidasWeb/siniestros/recuperacion/listado/recuperaciones/exportarRecuperaciones.action?"+form;
	 window.open(url, "Recuperaciones");
	 
}

function validarForma(){
	
	var contadorElementos = 0;
	var elementos         = false;
	var rangoMonto        = false;
	var rangoFechaAplicacion     = false;
	var rangoIniSol      = false;
	var rangoSini  = false;
	
	jQuery(".obligatorio").each(function(index, value) { 
	    if( jQuery(this).val() != "" ){
	    	contadorElementos++;
	    }
	});
	
	if(contadorElementos > 0){
		elementos = true;
	}
	
	if( jQuery("#montoDe").val() != "" && jQuery("#montoHasta").val() != "" ){
		if( parseFloat(jQuery("#montoDe").val()) > parseFloat(jQuery("#montoHasta").val()) ){
			mostrarMensajeInformativo('El monto inicial no debe ser mayor al final', '10');
			rangoMonto = false;
		}else{
			rangoMonto = true;
		}
	}
	
	if( jQuery("#fechaIniAplicacion").val() != "" && jQuery("#fechaFinAplicacion").val() != "" ){
		
		if( convertirFecha(jQuery("#fechaIniAplicacion").val()) > convertirFecha(jQuery("#fechaFinAplicacion").val()) ){
			mostrarMensajeInformativo('La fecha inicial no puede ser mayor a la final', '10');
			rangoFechaAplicacion = false;
		}else{
			rangoFechaAplicacion = true;
		}
	}
	
	if( jQuery("#fechaSolDe").val() != "" && jQuery("#fechaSolHasta").val() != "" ){
		if( convertirFecha(jQuery("#fechaSolDe").val()) > convertirFecha(jQuery("#fechaSolHasta").val()) ){
			mostrarMensajeInformativo('La fecha inicial del registro no puede ser mayor a la final', '10');
			rangoFechaAplicacion = false;
		}else{
			rangoIniSol = true;
		}
		
	}
	
	if( jQuery("#fechaIniSiniestro").val() != "" && jQuery("#fechaFinSiniestro").val() != "" ){
		if( convertirFecha(jQuery("#fechaIniSiniestro").val()) > convertirFecha(jQuery("#fechaFinSiniestro").val()) ){
			mostrarMensajeInformativo('La fecha inicial del siniestro no puede ser mayor a la final', '10');
			rangoFechaAplicacion = false;
		}else{
			rangoSini = true;
		}
		
	}
	
	if( (elementos) || ( rangoMonto || rangoFechaAplicacion || rangoIniSol || rangoSini ) ){
		return true;
	}else{
		return false;
	}
}

function consultarRecuperacion(idRecuperacion, tipo){
	if(null!=idRecuperacion && idRecuperacion!=""){
		if(tipo=="PRV"){
			var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=true&esNuevoRegistro=false&recuperacionId='+idRecuperacion;
			sendRequestJQ(null,ruta,targetWorkArea,null);
		}else if(tipo=="DED")
		{
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionDeducible/mostrarContenedor.action?recuperacion.id=" 
					+ idRecuperacion + "&tipoMostrar=R " ,targetWorkArea,null);			
		}else if(tipo=="SVM"){
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarContenedorRecuperacionSalvamento.action?soloConsulta=true&recuperacionId=" 
					+ idRecuperacion + "&tipoMostrar=R " ,targetWorkArea,null);
		}else if(tipo=="CIA"){
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action?soloConsulta=true&recuperacionId=" 
					+ idRecuperacion + "&tipoMostrar=R " ,targetWorkArea,null);
		}else if(tipo=="SIP"){
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionSipac/mostrarContenedor.action?recuperacion.id="
					+ idRecuperacion +"&tipoMostrar=R",targetWorkArea,null);
		}
	}
}

function editarRecuperacion(idRecuperacion,tipo){
	if(null!=idRecuperacion && idRecuperacion!=""){
		if(tipo=="PRV"){
			var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=false&esNuevoRegistro=false&recuperacionId='+idRecuperacion;
			sendRequestJQ(null,ruta,targetWorkArea,null);		
		}else if(tipo=="DED")
		{
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionDeducible/mostrarContenedor.action?recuperacion.id=" 
					+ idRecuperacion + "&tipoMostrar=U" ,targetWorkArea,null);			
		}else if(tipo=="SVM"){
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarContenedorRecuperacionSalvamento.action?soloConsulta=false&recuperacionId=" 
					+ idRecuperacion + "&tipoMostrar=U" ,targetWorkArea,null);
		}else if(tipo=="CIA"){	
			jQuery("#url").val("EDITAR"); 
			jQuery("#recuperacionId").val(idRecuperacion); 
			var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarAlerta.action?url=EDITAR"+"&recuperacionId="+idRecuperacion;
			mostrarVentanaModal("vm_alertaRecuperacionCIA", 'Alerta Recuperaci\u00F3n CIA', 550, 310, 1000, 550,ruta,null);
		}
	}
}




function cancelarRecuperacion(idRecuperacion, esAccionCancelar,tipo){
	if(null!=idRecuperacion && idRecuperacion!=""){
		if(tipo=="PRV"){
			var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=true&esNuevoRegistro=false&recuperacionId='+idRecuperacion+"&esAccionCancelar="+esAccionCancelar;
			sendRequestJQ(null,ruta,targetWorkArea,null);	
		}else if(tipo=="DED")
		{
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionDeducible/mostrarContenedor.action?recuperacion.id=" 
					+ idRecuperacion + "&tipoMostrar=C" ,targetWorkArea,null);			
		}else if(tipo=="SVM"){
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarContenedorRecuperacionSalvamento.action?soloConsulta=true&esNuevoRegistro=false&esAccionCancelar="+esAccionCancelar+"&recuperacionId=" 
					+ idRecuperacion + "&tipoMostrar=C " ,targetWorkArea,null);
		}else if(tipo=="CIA"){	
			sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action?soloConsulta=true&recuperacionId=" 
					+ idRecuperacion + "&tipoMostrar=C " ,targetWorkArea,null);
		}	
	}
}


function limpiarFormulario(){
	jQuery('#buscarRecuperacionesForm').each (function(){
		  this.reset();
	});
}

function habilitarDatosRecuperacion(){
	
	var elemento =jQuery("#lstTipoRecuperacion").val();
	if ( elemento != null ){
		if (elemento == 'SVM'){
			jQuery(".divDocumentada").show();
		}else{
			jQuery(".divDocumentada").hide();
		}
	}
}


function buscarRecuperacionEnter(e) {
    if (e.keyCode == 13) {
    	buscarRecuperaciones();
    }
}





function cerrarAlerta(){
	parent.cerrarVentanaModal("vm_alertaRecuperacionCIA",true);
}


function continuarCIA(){
	var recuperacionId  =	jQuery("#recuperacionId").val();
	parent.cerrarVentanaModal("vm_alertaRecuperacionCIA",'mostrarCIA();');
}	

function mostrarCIA(){
	if(jQuery("#url").val()=="EDITAR"){
		var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action?soloConsulta=false&esNuevoRegistro=false&recuperacionId='+jQuery("#recuperacionId").val()+ "&tipoMostrar=U"+"&editarDatosGenericos=true";
		sendRequestJQ(null,ruta,targetWorkArea,null);	
	}else if (jQuery("#url").val()=="NUEVO"){
		var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action?soloConsulta=false&tipoMostrar='U'&esNuevoRegistro=true";
		sendRequestJQ(null,ruta,targetWorkArea,null);
	}
}


function initComboTipoRecuperacion(){
	
	// ELIMINA DEL COMBO LA RECUPERACION ESPECIAL
	
	jQuery("#estatusLiquidaciones option[value='ESP']").remove();   // ELIMINADA
	
}


function convertirFecha(fecha){
	 var d=new Date();
	 var fecha = fecha.split("/");
	 return new Date(fecha[2],fecha[1],fecha[0]);
}


