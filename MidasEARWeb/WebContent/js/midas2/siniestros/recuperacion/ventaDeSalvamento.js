var listadoGridReferencias;

function iniGridReferencias(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 listadoGridReferencias = new dhtmlXGridObject('listadoGridReferencias');	
	 listadoGridReferencias.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoGridReferencias.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoGridReferencias.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 listadoGridReferencias.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });

	
}

function cargaUrlReferenciasBancarias(){
	iniGridReferencias();
	var recuperacionId = jQuery("#recuperacionId").val();
	if( recuperacionId != ""){
		listadoGridReferencias.load('/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/mostrarGridReferencias.action?recuperacionId='+recuperacionId);
	}
	
}


function obtenerCorreoPrestador(prestadorServicioId){
	
	if( prestadorServicioId != "" ){
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/obtenerCorreoDelComprador.action?prestadorServicioId="+prestadorServicioId;
		jQuery.ajax({
		    url: url ,
		    dataType: 'json',
		    async:false,
		    type:"POST",
		    success: function(json){
		    	jQuery("#correo").val(json.correoComprador);
		    }
		});
	
	}
}


function guardarVentaSalvamento(){
	
	removeCurrencyFormatOnTxtInput();	
	
	if( validarVentaSalvamento() ){
		var form = jQuery("#salvarSalvamentoForm").serialize();
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/guardarVentaDeSalvamento.action?"+form;
		
		sendRequestJQ(null, url, targetWorkArea, null);
	}

}


function recalcularVentaSalvamentoTotal(){
	
	removeCurrencyFormatOnTxtInput();
	
	var porBaseIva   = parseInt(1) + parseInt(jQuery("#porIva").val()) / parseInt(100);
	var subTotal     = parseFloat( jQuery("#totalVenta").val() ) / parseFloat(porBaseIva) ;
	var iva          = parseFloat( subTotal ) * parseFloat(parseInt(jQuery("#porIva").val()) / parseInt(100)); 
	
	
	
	jQuery("#subtotal").val(subTotal);
	jQuery("#iva").val(iva);
	
	initCurrencyFormatOnTxtInput();
}

function aplicarSoloConsulta(){
	if( jQuery("#soloConsulta").val() == "true" ){
		//alert("es solo consulta");
		jQuery(".modoConsulta").css("background-color","#EEEEEE");
		jQuery(".modoConsulta").attr("readonly", true);
		jQuery(".readOnlyCombo").attr("disabled", "disabled");
		
	}
}

function aplicarSoloEdicion(){
	if( jQuery("#esEdicionVentaRecuperacion").val() == "true" ){
		//alert("es edicion");
		jQuery(".modoEdicion").css("background-color","#EEEEEE");
		jQuery(".modoEdicion").attr("readonly", true);
		jQuery(".readOnlyCombo").attr("disabled", "disabled");
		
	}
}

function mostrarSiVentaActiva(){
	if( jQuery("#tieneVentaActiva").val() == "n" ){
		jQuery("#ventaActivaDiv").show();
	}
}

function validarVentaSalvamento(){
	var bandera = true;
	mensajeValidacion ="";

	
	if( jQuery("#compradores").val() == "" ){
		mensajeValidacion = "Seleccione el comprador\n ";
		bandera = false;
	}

	if ( !validationEmailGuardar( jQuery("#correo").val() ) ){
		mensajeValidacion += "Ingrese un correo valido\n ";
		bandera = false;
	}

	if( jQuery("#totalVenta").val() <= 0 ){
		mensajeValidacion += "El total de la venta debe ser mayor a 0\n";
		bandera = false;
	}

	if( jQuery("#totalVenta").val() == "" ){
		mensajeValidacion += "El total de la venta no puede ser nulo\n ";
		bandera = false;	
	}

	if ( jQuery("#noSubasta").val() == "" ){
		mensajeValidacion += "Ingrese el numero de subasta\n ";
		bandera = false;
	}

	if( jQuery("#fechaCierreSubasta").val() == "" ){
		mensajeValidacion += "Ingrese la fecha cierre de subasta\n ";
		bandera = false;
	}
	
	if( jQuery("#fechaCierreSubasta").val() != "" ){
		if( convertirFecha(jQuery("#fechaCierreSubasta").val()) < convertirFecha(jQuery("#salvarSalvamentoForm_recuperacion_fechaInicioSubasta").val())  ){
			mensajeValidacion += "La fecha cierre de subasta no puede ser menor a la fecha de Inicio \n ";
			bandera = false;
		} 	
	}

	if ( jQuery("#fechaFacura").val() != "" ){
		if( jQuery("#noFactura").val() == "" ){
			mensajeValidacion += "Ingrese el numero de factura \n ";
			bandera = false;
		}else if( convertirFecha(jQuery("#fechaFacura").val()) < convertirFecha(jQuery("#fechaAsignacion").val())  ){
			mensajeValidacion += "La fecha factura no puede ser menor a la fecha asignación \n ";
			bandera = false;
		} 	
	}

	if( jQuery("#noFactura").val() != "" ){
		if( jQuery("#fechaFacura").val() == "" ){
			mensajeValidacion += "Ingrese la fecha de la factura \n ";
			bandera = false;
		}
	}
	
	
	if(  jQuery("#fechaEntregaSalvamento").val() != "" ){
		
		if( convertirFecha(jQuery("#fechaEntregaSalvamento").val()) < convertirFecha(jQuery("#fechaAsignacion").val())  ){
			mensajeValidacion += "La fecha de entrega no puede ser menor a la de la venta   \n ";
			bandera = false;
		}
	}
		
	if( !bandera ){
		mostrarMensajeInformativo(mensajeValidacion, '10');
	}
	
	return bandera;
	
}



function validationEmailGuardar( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
    	return false;
    }else{
    	return true;
    }
}

