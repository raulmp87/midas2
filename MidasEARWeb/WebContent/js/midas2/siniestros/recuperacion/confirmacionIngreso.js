function guardarConfirmacionIngreso(){
	removeCurrencyFormatOnTxtInput();	
	
	if( jQuery("#tieneVentaActiva").val() == "s" ){
		
		var form = jQuery("#salvarSalvamentoForm").serialize();
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/guardarConfirmacionDeIngreso.action?"+form;
			
		if( validacionConfirmacionIngreso() ){
			sendRequestJQ(null, url, targetWorkArea, null);
		}
		
	}else{
		mostrarMensajeInformativo("Para confirmar el ingreso requiere una venta activa", '10');
	}
}


function initMostrarConfirmacionIngreso(){
	
	if( jQuery("#confirmacionDepositoSalvamento").val() != "" ){
		jQuery(".modoConsultaConfirmacion").css("background-color","#EEEEEE");
		jQuery(".modoConsultaConfirmacion").attr("readonly", true);
	}
}



function validacionConfirmacionIngreso(){
	
	var bandera = true;
	if( jQuery("#bancos").val() == "" ){
		mostrarMensajeInformativo("Seleccione un banco", '10');
		bandera = false;
	}else if( jQuery("#cuenta").val() == "" ){
		mostrarMensajeInformativo("Ingrese un numero de cuenta", '10');
		bandera = false;
	}else if( convertirFecha(jQuery("#fechaDepositoConfirmacionIngreso").val()) < convertirFecha(jQuery("#fechaAsignacion").val()) ){
		mostrarMensajeInformativo("La fecha de confirmacion de ingreso no puede ser menor a la Adjudicación", '10');
		bandera = false;
	}else if( jQuery("#montoConfirmacion").val() == "" ){
		mostrarMensajeInformativo("Ingrese el monto confirmado", '10');
		bandera = false;
	}
	
	return bandera;
}