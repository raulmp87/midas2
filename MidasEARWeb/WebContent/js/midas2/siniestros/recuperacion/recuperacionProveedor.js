
function verTabDevolucion(){
	var soloConsulta = jQuery("#soloConsulta").val() ;
	/*if(soloConsulta!=true){
		if(confirm("¿Desea Salir de la pestaña, la informacion no almacenada se perdera? ")){
			removeCurrencyFormatOnTxtInput();
			//var data=jQuery("#siniestroCabinaForm").serialize();
			var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
			//limpiarDivsGeneral();	
			 var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedorDevolucionPRV.action?'+ formParams;
			sendRequestJQ(null,  ruta, 'contenido_devolucionProveedor',null);
		}
	}else {*/
		removeCurrencyFormatOnTxtInput();
		//var data=jQuery("#siniestroCabinaForm").serialize();
		var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
		//limpiarDivsGeneral();	
		 var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedorDevolucionPRV.action?'+ formParams;
		sendRequestJQ(null,  ruta, 'contenido_devolucionProveedor',null);
	//}
	
}
function verTabVenta(){
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
		 var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedorVentaPRV.action?'+ formParams;
		sendRequestJQ(null,  ruta, 'contenido_ventaProveedor',null);
}

function incializarTabs(){
	var medio = jQuery("#lstMedioRecuperacion").val() ;
	var esRefaccion = jQuery("#esRefaccion").val() ;
	var soloConsulta = jQuery("#soloConsulta").val() ;
	if(  (   (medio=="VENTA" ||  medio=="REEMBOLSO" )&&  esRefaccion=="true") ||  ( soloConsulta=="true")){
		window["recuperacionTabBar"].enableTab("ventaProveedor");
	}else{
		window["recuperacionTabBar"].disableTab("ventaProveedor");
	}
}

function changeMedioRecuperaPRV(){
	incializarTabs();
}


function  nuevaRecuperacionPRV(){
	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=false&esNuevoRegistro=true';
	sendRequestJQ(null,ruta,targetWorkArea,null);
}

function consultarRecuperacion(idRecuperacion){
	if(null!=idRecuperacion && idRecuperacion!=""){
		var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarContenedor.action?soloConsulta=true&esNuevoRegistro=false&recuperacionId='+idRecuperacion;
		sendRequestJQ(null,ruta,targetWorkArea,null);
	}
	
}

function reiniciarDatosRefaccion(){
	jQuery("#nombreTaller").val("");
	jQuery("#marcaVehiculo").val("");
	jQuery("#tipoVehiculo").val("");
	jQuery("#modeloVehiculo").val("");
	//jQuery("#text_IvaRetenido").val(0);
	//jQuery("#text_Isr").val(0);
	
	
	
}

function checkRefaccion(){
	var refaccion =  jQuery("#ch_esRefaccion").attr('checked'); 
	var ordenCompraID = jQuery("#filtro_numOrdenCompra").val(); 
	var filtro_esRefaccion =jQuery("#filtro_esRefaccion").val(); 
	if(null==ordenCompraID|| ordenCompraID==""){
		mostrarMensajeInformativo('Debe Seleccionar Orden de Compra' , '20')
		jQuery("#esRefaccion").val(false);
		jQuery("#ch_esRefaccion").attr('checked', false);
	}else if(filtro_esRefaccion!="true"){
		mostrarMensajeInformativo('la Orden de Compra NO contiene por lo menos un concepto de Tipo "Refacción".' , '20')
		jQuery("#esRefaccion").val(false);
		jQuery("#ch_esRefaccion").attr('checked', false);
	}else{
		if(refaccion){
			jQuery("#esRefaccion").val(true);
		}else{
			jQuery("#esRefaccion").val(false);
		}
		esRefaccion();
	}
	
	
	
}

function esRefaccion(){
	var refaccion =jQuery("#esRefaccion").val(); 
	var esDanosMateriales = jQuery("#filtro_esDanosMateriales").val() ;
	var esResponsabilidadCivil = jQuery("#filtro_esResponsabilidadCivil").val() ;
	if(refaccion=='true' &&( esDanosMateriales=='true' || esResponsabilidadCivil=='true' )  ){	
		
		 jQuery("#nombreTaller").removeAttr("disabled");
		 jQuery("#marcaVehiculo").removeAttr("disabled");
		 jQuery("#tipoVehiculo").removeAttr("disabled");
		 jQuery("#modeloVehiculo").removeAttr("disabled");		
		
		
		var filtro_marcaVehiculo = jQuery("#filtro_marcaVehiculo").val() ;
		var filtro_tipoVehiculo = jQuery("#filtro_tipoVehiculo").val() ;
		var filtro_modeloVehiculo = jQuery("#filtro_modeloVehiculo").val() ;
		var filtro_nombreTaller = jQuery("#filtro_nombreTaller").val() ;
		
		jQuery("#nombreTaller").val(filtro_nombreTaller);
		jQuery("#marcaVehiculo").val(filtro_marcaVehiculo);
		jQuery("#tipoVehiculo").val(filtro_tipoVehiculo);
		jQuery("#modeloVehiculo").val(filtro_modeloVehiculo);
		
		
		
	}else{
		reiniciarDatosRefaccion();
		jQuery("#nombreTaller").attr('disabled','disabled');
		jQuery("#marcaVehiculo").attr('disabled','disabled');
		jQuery("#tipoVehiculo").attr('disabled','disabled');
		jQuery("#modeloVehiculo").attr('disabled','disabled');
	}
	
	if(refaccion=='true' ){
		jQuery("#text_IvaRetenido").val(0);
		jQuery("#text_Isr").val(0);
		jQuery("#text_Iva").val(0);
		jQuery("#text_IvaRetenido").attr('disabled','disabled');		 
		jQuery("#text_Isr").attr('disabled','disabled');
		jQuery("#text_Iva").attr('readonly', 'readonly');
	}else{
		jQuery("#text_Iva").val(0);
		jQuery("#text_Isr").removeAttr("disabled");
		jQuery("#text_IvaRetenido").removeAttr("disabled");
		jQuery("#text_Iva").removeAttr("readonly");
	}
	calculaTotalMontos();
	 
	

}


function buscarOrdenesCompra(){
	var reporteCabina = jQuery("#idReporteCabina").val() ;
	var coberturasSeleccionadas = jQuery("#coberturasSeleccionadas").val() ;
	var pasesSeleccionadas = jQuery("#pasesSeleccionadas").val() ;
	var tipo =jQuery("#tipoOC").val(); 
	if(null==reporteCabina || reporteCabina==""){
		mostrarMensajeInformativo('Debe Seleccionar Numero de Siniestro' , '20')
		return 0;
	}
	if(null==tipo || tipo==""){
		mostrarMensajeInformativo('Debe Seleccionar Tipo Orden Compra' , '20')
		return 0;
	}
	var tipoOC="";
	if(tipo=="AFECTACION"){
		tipoOC=1;
	}else if (tipo=="GASTO"){
		tipoOC=2;
	}else{
		tipoOC=3;
	}

	mostrarBuscarOrdenCompra  (reporteCabina,coberturasSeleccionadas, pasesSeleccionadas,tipoOC,'filtro_');
}
function redireccionaBusquedaOrdenCompra(){
	jQuery("#esRefaccion").val(false);
	jQuery("#ch_esRefaccion").attr('checked', false);
	esRefaccion();
}


function calculaTotalMontos(){
	removeCurrencyFormatOnTxtInput();	
	var  subtotal = jQuery("#text_subtotal").val();
	var  iva = jQuery("#text_Iva").val();	
	var  ivaRetenido = jQuery("#text_IvaRetenido").val();	
	var  isr = jQuery("#text_Isr").val();
	 
	 if(null==subtotal || subtotal ==""){
		 jQuery("#text_subtotal").val(0);
	 }
	 if(null==iva || iva ==""){
		 jQuery("#text_Iva").val(0);
	 }
	 if(null==ivaRetenido || ivaRetenido ==""){
		 jQuery("#text_IvaRetenido").val(0);
	 }
	 if(null==isr || isr ==""){
		 jQuery("#text_Isr").val(0);
	 }
	var refaccion =jQuery("#esRefaccion").val();
	if(refaccion=='true' ){
		jQuery("#text_Iva").val(parseFloat(subtotal)*parseFloat(0.16) );	
	}
	 
	  subtotal = jQuery("#text_subtotal").val();
	  iva = jQuery("#text_Iva").val();	
	  ivaRetenido = jQuery("#text_IvaRetenido").val();	
	  isr = jQuery("#text_Isr").val();

	var total= parseFloat(subtotal) + parseFloat(iva) - parseFloat(ivaRetenido) - parseFloat(isr);
	jQuery("#text_montoTotal").val(total);
	initCurrencyFormatOnTxtInput();
}

function calculaTotalMontosVenta(){
	removeCurrencyFormatOnTxtInput();		
	var  subtotal = jQuery("#text_subtotalVenta").val();
	var  iva = jQuery("#text_IvaVenta").val();	
	if(null==subtotal || subtotal ==""){
		 jQuery("#text_subtotalVenta").val(0);
	 }
	if(null==iva || iva ==""){
		 jQuery("#text_IvaVenta").val(0);
	 }
	jQuery("#text_IvaVenta").val(parseFloat(jQuery("#text_subtotalVenta").val())*parseFloat(0.16) );
	jQuery("#text_montoTotalVenta").val(parseFloat( jQuery("#text_subtotalVenta").val()) + parseFloat(jQuery("#text_IvaVenta").val()));
	initCurrencyFormatOnTxtInput();
}

function iniciaTotales(){
	removeCurrencyFormatOnTxtInput();
	jQuery("#text_subtotal").val(0);
	jQuery("#text_Iva").val(0);
	jQuery("#text_IvaRetenido").val(0);
	jQuery("#text_Isr").val(0);
	jQuery("#text_montoTotal").val(0);
	initCurrencyFormatOnTxtInput();
}


function validarTotal(tarjet){
	removeCurrencyFormatOnTxtInput();	
	 var monto =tarjet.value ;
	 if(null==monto || monto==""){
		 jQuery(tarjet).val(0);
	 }	 
	initCurrencyFormatOnTxtInput();
	calculaTotalMontos();

}

function validarTotalVenta(tarjet){
	removeCurrencyFormatOnTxtInput();	
	 var monto =tarjet.value ;
	 if(null==monto || monto==""){
		 jQuery(tarjet).val(0);
	 }	 
	initCurrencyFormatOnTxtInput();
	calculaTotalMontosVenta();

}


function buscarCuentasDevolucion(){	
	var esNuevoRegistro=  jQuery("#esNuevoRegistro").val();
	var recuperacionId =   jQuery("#recuperacionId").val();
	var soloConsulta =  jQuery("#soloConsulta").val();	
	document.getElementById("cuentaDevolucionGrid").innerHTML = '';
	gridDev = new dhtmlXGridObject("cuentaDevolucionGrid");
	//gridDev.attachEvent("onXLS", function(grid_obj){blockPage()});
	gridDev.attachEvent("onXLE", function(grid_obj){
		unblockPage();
	});
	gridDev.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	gridDev.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	

	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/buscarCuentasDevolucion.action?soloConsulta='+soloConsulta+'&esNuevoRegistro='+esNuevoRegistro+'&recuperacionId='+recuperacionId;
	gridDev.load( ruta);
	
}


function iniContenedorRecuperacionPRV(){	
	var idRecuperacion=jQuery("#idRecuperacion").val() ;
	var refaccion = jQuery("#esRefaccion").val() ;
	var esDanosMateriales = jQuery("#filtro_esDanosMateriales").val() ;
	var esResponsabilidadCivil = jQuery("#filtro_esResponsabilidadCivil").val() ;
	if(refaccion=='true'){		
		jQuery("#ch_esRefaccion").attr('checked', true);
	}else{
		jQuery("#ch_esRefaccion").attr('checked', false);
	}
	 var soloConsulta=jQuery("#soloConsulta").val() ;	
	 var esNuevoRegistro=jQuery("#esNuevoRegistro").val() ;	
	 changeMotivo();
	 
	 
	 if(esNuevoRegistro=='true'){
		 bloquearDevolucion(false);
		 jQuery("#guardarBtn").show();
		 jQuery("#notaCred").hide(); 
		 esRefaccion();	
		 if(refaccion==true &&( esDanosMateriales==true || esResponsabilidadCivil==true )  ){
				//jQuery("#divRefacciones").show();
				 jQuery("#filtro_marcaVehiculo").removeAttr("disabled");
				 jQuery("#filtro_tipoVehiculo").removeAttr("disabled");
				 jQuery("#filtro_modeloVehiculo").removeAttr("disabled");
			}else{
				reiniciarDatosRefaccion();
				//jQuery("#divRefacciones").hide();	
				jQuery("#filtro_marcaVehiculo").attr('disabled','disabled');
				jQuery("#filtro_tipoVehiculo").attr('disabled','disabled');
				jQuery("#filtro_modeloVehiculo").attr('disabled','disabled');
			}
		 
		
		 
		 
		 
	 }else if (soloConsulta=='true'){
		 bloquearDevolucion(true);
		 jQuery("#notaCred").show();
		 jQuery("#guardarBtn").hide();
	 }else{
		 bloquearDevolucion(true);
		 jQuery("#guardarBtn").hide(); 
		 jQuery("#notaCred").show(); 
	 }	
	 

}


function changeMotivo(){	
	var motivo =jQuery("#lstMotivo").val() ;
	 if(motivo=="OTROS"){
		 jQuery("#otroMotivoDevolucion").removeAttr("disabled");
	 }else {
		 jQuery("#otroMotivoDevolucion").attr('disabled','disabled');
		 jQuery("#otroMotivoDevolucion").val("");
	 }
}
function bloquearVenta(bandera){
	if(bandera==true){
		jQuery("#fechaRegistroVenta").attr('readonly', 'readonly');
		jQuery("#ubicacionVenta").attr('readonly', 'readonly');
		jQuery("#personaRecogeRefaccion").attr('readonly', 'readonly');
		jQuery("#personaEntregaRefaccion").attr('readonly', 'readonly');
		
		jQuery("#estatusPreventa").attr('readonly', 'readonly');
		jQuery("#comprador").attr('readonly', 'readonly');
		jQuery("#correoComprador").attr('readonly', 'readonly');
		jQuery("#telefonoComprador").attr('readonly', 'readonly');
		jQuery("#comentariosVenta").attr('readonly', 'readonly');
		jQuery("#text_subtotalVenta").attr('readonly', 'readonly');
		jQuery("#text_IvaVenta").attr('readonly', 'readonly');
		jQuery("#text_montoTotalVenta").attr('readonly', 'readonly');		
		/*jQuery("#estadosLis").attr('disabled','disabled');
		jQuery("#ciudadLis").attr('disabled','disabled');
		jQuery("#estatusVentaLis").attr('disabled','disabled');*/

		
	}else {
		jQuery("#fechaRegistroVenta").removeAttr("readonly");
		jQuery("#ubicacionVenta").removeAttr("readonly");
		jQuery("#personaRecogeRefaccion").removeAttr("readonly");
		jQuery("#personaEntregaRefaccion").removeAttr("readonly");
		jQuery("#estatusPreventa").removeAttr("readonly");
		jQuery("#comprador").removeAttr("readonly");
		jQuery("#correoComprador").removeAttr("readonly");
		jQuery("#telefonoComprador").removeAttr("readonly");
		jQuery("#comentariosVenta").removeAttr("readonly");
		jQuery("#text_subtotalVenta").removeAttr("readonly");
		jQuery("#text_IvaVenta").attr('readonly', 'readonly');
		jQuery("#text_montoTotalVenta").removeAttr("readonly");
		
		/*jQuery("#estadosLis").removeAttr("disabled");
		jQuery("#ciudadLis").removeAttr("disabled");
		jQuery("#estatusVentaLis").removeAttr("disabled");*/

	}
}


function bloquearDatosVenta(bandera){
	if(bandera==true){
		jQuery("#compradorId").val("");
		jQuery("#comprador").val("");
		jQuery("#correoComprador").val("");
		jQuery("#telefonoComprador").val("");
		jQuery("#comentariosVenta").val("");
		jQuery("#text_subtotalVenta").val(0);
		jQuery("#text_IvaVenta").val(0);
		jQuery("#text_montoTotalVenta").val(0);
		jQuery("#divDatosVenta").hide();
		
		/*jQuery("#comprador").attr('disabled','disabled');
		jQuery("#correoComprador").attr('disabled','disabled');
		jQuery("#telefonoComprador").attr('disabled','disabled');
		jQuery("#comentariosVenta").attr('disabled','disabled');
		jQuery("#text_subtotalVenta").attr('disabled','disabled');
		jQuery("#text_IvaVenta").attr('disabled','disabled');
		jQuery("#text_montoTotalVenta").attr('disabled','disabled');*/
	}else{
		jQuery("#divDatosVenta").show();
		/*jQuery("#comprador").removeAttr("disabled");
		jQuery("#correoComprador").removeAttr("disabled");
		jQuery("#telefonoComprador").removeAttr("disabled");
		jQuery("#comentariosVenta").removeAttr("disabled");
		jQuery("#text_subtotalVenta").removeAttr("disabled");
		jQuery("#text_IvaVenta").removeAttr("disabled");
		jQuery("#text_montoTotalVenta").removeAttr("disabled");*/
	}
	
}

function changeEstatusVenta(){
	var estatusVentaLis = jQuery("#estatusVentaLis").val() ;
	if(estatusVentaLis=="VENTA"){
		bloquearDatosVenta(false);
	}else{
		 var soloConsulta=jQuery("#soloConsulta").val() ;	
		if (soloConsulta=='true'){
			bloquearDatosVenta(false);

		}else{
			bloquearDatosVenta(true);

		}
	}
}






function bloquearDevolucion(bandera){
	if(bandera==true){
				jQuery("#btn_BusquedaOC").hide();
				jQuery("#filtro_numOrdenCompra").attr('readonly', 'readonly');
				jQuery("#filtro_fechaOrdenCompra").attr('readonly', 'readonly');
				jQuery("#filtro_montoOrdenCompra").attr('readonly', 'readonly');
				jQuery("#filtro_factura").attr('readonly', 'readonly');
				jQuery("#filtro_fechaOrdenPago").attr('readonly', 'readonly');
				jQuery("#filtro_montoOrdenPago").attr('readonly', 'readonly');
				jQuery("#datosRecuperacion.nombreProveedor").attr('readonly', 'readonly');
				jQuery("#filtro_telefonoProveedor").attr('readonly', 'readonly');
				jQuery("#filtro_correoProveedor").attr('readonly', 'readonly');
				jQuery("#filtro_numValuacion").attr('readonly', 'readonly');
				jQuery("#filtro_nombreValuador").attr('readonly', 'readonly');
				jQuery("#adminRefacciones").attr('readonly', 'readonly');
				jQuery("#ch_esRefaccion").attr('readonly', 'readonly');
				jQuery("#personaDevolucion").attr('readonly', 'readonly');
				jQuery("#conceptoDevolucion").attr('readonly', 'readonly');
				jQuery("#lstMotivo").attr('readonly', 'readonly');
				jQuery("#otroMotivoDevolucion").attr('readonly', 'readonly');
				jQuery("#text_subtotal").attr('readonly', 'readonly');
				jQuery("#text_Iva").attr('readonly', 'readonly');
				jQuery("#text_IvaRetenido").attr('readonly', 'readonly');
				jQuery("#text_Isr").attr('readonly', 'readonly');
				jQuery("#text_montoTotal").attr('readonly', 'readonly');
				jQuery("#filtro_nombreProveedor").attr('readonly', 'readonly');
				//
				jQuery("#nombreTaller").attr('readonly', 'readonly');				
				jQuery("#marcaVehiculo").attr('readonly', 'readonly');
				jQuery("#tipoVehiculo").attr('readonly', 'readonly');
				jQuery("#modeloVehiculo").attr('readonly', 'readonly');
				jQuery("#ch_esRefaccion").attr('disabled','disabled');
		
	}else	{
		jQuery("#btn_BusquedaOC").show();
		//jQuery("#filtro_numOrdenCompra").removeAttr("readonly");
		//jQuery("#filtro_fechaOrdenCompra").removeAttr("readonly");
		//jQuery("#filtro_montoOrdenCompra").removeAttr("readonly");
		//jQuery("#filtro_factura").removeAttr("readonly");
		//jQuery("#filtro_fechaOrdenPago").removeAttr("readonly");
		//jQuery("#filtro_montoOrdenPago").removeAttr("readonly");
		//jQuery("#datosRecuperacion.nombreProveedor").removeAttr("readonly");
		//jQuery("#filtro_telefonoProveedor").removeAttr("readonly");
		//jQuery("#filtro_correoProveedor").removeAttr("readonly");
		jQuery("#filtro_numValuacion").removeAttr("readonly");
		jQuery("#filtro_nombreValuador").removeAttr("readonly");
		jQuery("#adminRefacciones").removeAttr("readonly");
		jQuery("#ch_esRefaccion").removeAttr("readonly");
		jQuery("#personaDevolucion").removeAttr("readonly");
		jQuery("#conceptoDevolucion").removeAttr("readonly");
		jQuery("#lstMotivo").removeAttr("readonly");
		jQuery("#otroMotivoDevolucion").removeAttr("readonly");
		jQuery("#text_subtotal").removeAttr("readonly");
		jQuery("#text_Iva").removeAttr("readonly");
		jQuery("#text_IvaRetenido").removeAttr("readonly");
		jQuery("#text_Isr").removeAttr("readonly");
		jQuery("#text_montoTotal").attr('readonly', 'readonly');
		//jQuery("#filtro_nombreProveedor").removeAttr("readonly");	
		jQuery("#nombreTaller").removeAttr("readonly");	
		//
		jQuery("#marcaVehiculo").removeAttr("readonly");
		jQuery("#tipoVehiculo").removeAttr("readonly");
		jQuery("#modeloVehiculo").removeAttr("readonly");
		jQuery("#ch_esRefaccion").removeAttr("disabled");

		
	}
	
	
}






function validaGuardarRecuperacion(){
	
	var valida = true;
	var ordenCompra = jQuery("#filtro_numOrdenCompra").val() ;
	if(null==ordenCompra || ordenCompra==""){
		mostrarMensajeInformativo('Debe Seleccionar una Orden de Compra' , '20')
		return false;
	}
	
	var conceptoDevolucion = jQuery("#conceptoDevolucion").val() ;
	if(null==conceptoDevolucion || conceptoDevolucion=="" || conceptoDevolucion.length > 500 ){
		mostrarMensajeInformativo('Debe Capturar la  Descripción del Concepto de Devolución y debe ser menor a 500 caracteres' , '20')
		return false;
	}
	
	var text_subtotal = jQuery("#text_subtotal").val() ;
	if(null==text_subtotal || text_subtotal==""){
		mostrarMensajeInformativo('Debe Capturar Subtotal' , '20')
		return false;
	}	
	if( text_subtotal<=0){
    	mostrarMensajeInformativo('El Monto del Subtotal debe ser mayor a cero', '20');
    	return false;
	}
	
	var text_montoTotal = jQuery("#text_montoTotal").val() ;
	if(null==text_montoTotal || text_montoTotal==""){
		mostrarMensajeInformativo('Debe Capturar el Monto Total Original' , '20')
		return false;
	}	
	if( text_montoTotal<=0){
    	mostrarMensajeInformativo('El Monto del Total Original debe ser mayor a cero', '20');
    	return false;
	}
	var filtro_montoOrdenCompra = jQuery("#filtro_montoOrdenCompra").val() ;	
	
	if( parseFloat(text_montoTotal)>parseFloat(filtro_montoOrdenCompra)){
		mostrarMensajeInformativo ('El Monto Total Original: '+text_montoTotal+' es mayor al Monto Total de  la Orden de Compra:'+ filtro_montoOrdenCompra);
		return false;		
	}
	
	var lstMotivo = jQuery("#lstMotivo").val() ;
	if(null==lstMotivo || lstMotivo==""){
		mostrarMensajeInformativo('Debe Seleccionar el Motivo de Devolucion' , '20')
		return false;
	}
	
	var filtro_correoProveedor = jQuery("#filtro_correoProveedor").val() ;
	if(null==filtro_correoProveedor || filtro_correoProveedor==""){
		mostrarMensajeInformativo('Debe Capturar el correo electronico del proveedor' , '20');
		return false;
	}else{
		return validationCorreo(filtro_correoProveedor);
		
	}
	return true;
}


function validationCorreo( correo ){
	expr=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(correo !="" && !expr.test( correo )){
		mostrarMensajeInformativo("El correo electr\u00F3nico no es una direcci\u00F3n de correo v\u00e1lida", '20');
    	return false;
    }
	return true;
}



function guardarRecuperacionPRV(){	
	removeCurrencyFormatOnTxtInput();
	
	if (validaGuardarRecuperacion()){
		var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
		var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/guardar.action?'+ formParams;
		if(confirm("¿Desea Guardar la informacion? ")){
			bloquearDevolucion(false);
			sendRequestJQ(null,ruta,targetWorkArea,null);
		}
		
	}	

	initCurrencyFormatOnTxtInput();
}


function iniContenedorVentaPRV(){	
	var idRecuperacion=jQuery("#idRecuperacion").val() ;
	 var soloConsulta=jQuery("#soloConsulta").val() ;	
		if (soloConsulta=='true'){
			bloquearVenta(true);
		 }else{
			 bloquearVenta(false);
			 changeEstatusVenta();
			 
		 }	
		
		var estatusVentaLis = jQuery("#estatusVentaLis").val() ;
		if(estatusVentaLis=="VENTA"){
			jQuery("#divDatosVenta").show();
		}else{
			 var soloConsulta=jQuery("#soloConsulta").val() ;	
			if (soloConsulta=='true'){
				jQuery("#divDatosVenta").show();

			}else{
				jQuery("#divDatosVenta").hide();

			}
		}
		
	 

}


function buscarCuentasVenta(){	
	var esNuevoRegistro=  jQuery("#esNuevoRegistro").val();
	var recuperacionId =   jQuery("#recuperacionId").val();
	var soloConsulta =  jQuery("#soloConsulta").val();	
	document.getElementById("cuentaVentaGrid").innerHTML = '';
	gridVenta = new dhtmlXGridObject("cuentaVentaGrid");
	//gridDev.attachEvent("onXLS", function(grid_obj){blockPage()});
	gridVenta.attachEvent("onXLE", function(grid_obj){
		unblockPage();
	});
	gridVenta.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	gridVenta.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	

	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/buscarCuentasVenta.action?soloConsulta='+soloConsulta+'&esNuevoRegistro='+esNuevoRegistro+'&recuperacionId='+recuperacionId;
	gridVenta.load( ruta);
	
}




//CANCELACION RECUPERACION --------------------------------------------

function mostrarVentanaCancelarRecuperacionProveedor(){
	var idRecuperacion = jQuery("#recuperacionId").val();
	console.log("idRecuperacion = " + idRecuperacion);
	if(estatusValidoParaCancelacion()){
		var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarVentanaCancelar.action?" + formParams;
		var TITULO = "Datos para Cancelaci\u00F3n";
		mostrarVentanaModal("vm_cancelarRecuperacionProveedor", TITULO,  1, 1, 660, 220, url, 'consultarRecuperacion(' + idRecuperacion +');');
	}else{
		alert("Solo se puede cancelar si el estatus es REGISTRADO o PENDIENTE.");
	}
}

function cancelarRecuperacionProveedor(){
	removeCurrencyFormatOnTxtInput();
	
	if(validarMotivoCapturado()){
		parent.submitVentanaModal("vm_cancelarRecuperacionProveedor", document.cancelarRecuperacionForm);
	}else{
		alert("El campo Motivo de la Cancelaci\u00F3n es obligatorio");
	}
}

function estatusValidoParaCancelacion(){
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val();
	console.log(estatusRecuperacion);
	if(estatusRecuperacion == 'REGISTRADO' 
		|| estatusRecuperacion == 'PENDIENTE'){
		return true;
	}
	return false;
}

function validarMotivoCapturado(){
	var motivo = jQuery("#motivoCancelacion_a").val();
	if(motivo){
		return true;
	}
	return false;
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}

function cerrarVentanaCancelarRecuperacionProveedor(){
	var idRecuperacion = jQuery("#idRecuperacion").val();
	console.log("idRecuperacion="+idRecuperacion);
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val();
	console.log("estatusRecuperacion=" + estatusRecuperacion);
	if(estatusRecuperacion == 'CANCELADO'){
		parent.cerrarVentanaModal('vm_cancelarRecuperacionProveedor', 'consultarRecuperacion(' + idRecuperacion + ');');
		console.log("ESTATUS CANCELADO, ACTUALIZANDO PANTALLA CONSULTA");
	}else{
		parent.cerrarVentanaModal('vm_cancelarRecuperacionProveedor');
		console.log("ESTATUS REGISTRADO");
	}
}

function verTabCancelacion(){
	var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
	limpiarDivsGeneral();
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionProveedor/mostrarPestanaCancelar.action?" + formParams;
	sendRequestJQ(null, url, 'contenido_cancelar', null);
}

function configurarVentanaCancelacion(){
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val();
	if(estatusRecuperacion != 'CANCELADO'){
		jQuery("#cancelarBtn").show();
	}else{
		jQuery("#motivoCancelacion_a").attr('disabled','true');
	}
}



function changeEstados(){	

	var estadosLis =jQuery("#estadosLis").val();
	if(null ==estadosLis   || estadosLis==""){
		dwr.util.removeAllOptions("ciudadLis");		
	}else{
	listadoService.getMapMunicipiosPorEstado( estadosLis ,function(data){
		dwr.util.removeAllOptions("ciudadLis");
		dwr.util.addOptions("ciudadLis", [ {
				id : "",
				value : "Seleccione..."
			} ], "id", "value");
		dwr.util.addOptions("ciudadLis", data);
	});
	}
}


function mostrarComprador(){
	var estado =jQuery("#estadosLis").val();
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true&filtroCatalogo.estatus=1&filtroCatalogo.tipoPrestadorStr=COMP"  ; 
	mostrarVentanaModal("vm_asignarComprador", "Asignar Comprador", null, null, 690, 650, url , "");
}

function setPrestadorId( prestadorId, nombrePrestador  ){
	//var txtPrestadorId.val(prestadorId);
	 jQuery(comprador).val(nombrePrestador);
	 jQuery(compradorId).val(prestadorId);
	 
	parent.cerrarVentanaModal("vm_asignarComprador",true);
}



function guardarVentaPRV(){
	removeCurrencyFormatOnTxtInput();	
	
	//var lstMedioRecuperacion = jQuery("#lstMedioRecuperacion").val() ;
	var estatusVentaLis = jQuery("#estatusVentaLis").val() ;
	 if(null==estatusVentaLis || estatusVentaLis==""){
		mostrarMensajeInformativo('Seleccione el Estatus de Pre Venta' , '20')
	}
	else{
		var valida = false;
		if(estatusVentaLis=="ESPERA"){
			valida = validaGuardarPreVenta();
		}else if(estatusVentaLis=="VENTA"){
			valida=validaGuardarVenta();
		}
		if (valida){
			var formParams = jQuery(document.contenedorRecuperacionPRVForm).serialize();
			var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionProveedor/guardarVenta.action?'+ formParams;
			if(confirm("¿Desea Guardar la informacion de Venta? ")){
				sendRequestJQ(null,ruta,targetWorkArea,null);
			}
			
		}	
	}
	initCurrencyFormatOnTxtInput();
	
}

function validaGuardarPreVenta(){
	var valida = true;  estatusRecuperacion
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val() ;
	if(estatusRecuperacion !="PENDIENTE"){
		mostrarMensajeInformativo('El estatus de la Preventa es EN ESPERA DE VENTA , el estatus de la recuperacion no debe ser diferente de PENDIENTE POR RECUPERAR' , '20')
		return false;
	}
	return valida;
}

function validaGuardarVenta(){
	var valida = true;	
	var estatusRecuperacion = jQuery("#estatusRecuperacion").val() ;	
	if(estatusRecuperacion !="REGISTRADO"){
		mostrarMensajeInformativo('El estatus de la Preventa es VENTA , el estatus de la recuperacion no puede ser diferente de REGISTRADO' , '20')
		return false;
	}
	
	var compradorId = jQuery("#compradorId").val() ;	

	if(null==compradorId || compradorId==""){
		mostrarMensajeInformativo('Debe Especificar un Comprador' , '20')
		return false;
	}
	var text_montoTotalVenta = jQuery("#text_montoTotalVenta").val() ;	
	if(null==text_montoTotalVenta || text_montoTotalVenta=="" || text_montoTotalVenta<=0){
		mostrarMensajeInformativo('EL Monto Final Recuperado debe ser mayor a cero' , '20')
		return false;
	}
	var correoComprador = jQuery("#correoComprador").val() ;	
	if(null==correoComprador || correoComprador=="" ){
		mostrarMensajeInformativo('Capture el correo Electronico del Comprador' , '20')
		return false;
	}
	
	if(  !validationCorreo( correoComprador )  ){
		return false;
	}
	
	return valida;
}
