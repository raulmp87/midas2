var listadoIngresosGrid;
var depositosBancariosGrid=null;
var movsAcreedoresGrid=null;
var listadoIngresosPendientesGrid=null;
var listadoIngresosReversaAplicarGrid = null;
var movtosManualesGrid=null;

function initListadoIngresosGrid(){
	 depositosBancariosGrid=null;
	 movsAcreedoresGrid=null;
	 listadoIngresosPendientesGrid=null;
	 listadoIngresosReversaAplicarGrid = null;
	 movtosManualesGrid=null;
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';
	 jQuery("#listadoIngresosGrid").show();

	 listadoIngresosGrid = new dhtmlXGridObject('listadoIngresosGrid');	
	 listadoIngresosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoIngresosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoIngresosGrid.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 
	 listadoIngresosGrid.attachEvent("onXLE", function(grid_obj){
			ocultarIndicadorCarga('indicador');
			
           document.getElementById("mcheckbox").onchange=function() {
           	var estadoChk = document.getElementById("mcheckbox").checked;
           	listadoIngresosGrid.forEachRow(function(id) {
           		
           		var cell =  listadoIngresosGrid.cells(id,15);
           		if( estadoChk ){
           			if( !cell.isDisabled() ){
               			cell.setValue(1);
               		}
           		}else{
           			if( !cell.isDisabled() ){
               			cell.setValue(0);
               		}
           		}
           			
   			});
           	
           }

	 });
	 
	 listadoIngresosGrid.attachEvent("onPageChanged", function(){
			listadoIngresosGrid.forEachRow(function(id) {
				if(listadoIngresosGrid.getRowAttribute(id,"factura") == "0"){
					listadoIngresosGrid.cells(id, 15).setDisabled(true);
				}	
			});
	 });
	 
	 jQuery("#divExcelBtn").show();
}



function buscarIngresos(){
	if( validarForma() ){
		removeCurrencyFormatOnTxtInput();	
		var form = jQuery("#buscarIngresosForm").serialize();
		initListadoIngresosGrid();
		var url = '/MidasWeb/siniestros/recuperacion/listado/ingresos/buscarIngresos.action?'+form;
		if(jQuery("#servicioParticular").val() == 0){
			url += "&filtroIngreso.servicioParticular=" + jQuery("#servicioParticular").val();
		}
		if(jQuery("#servicioPublico").val() == 0){
			url += "&filtroIngreso.servicioPublico=" + jQuery("#servicioPublico").val();
		}
		listadoIngresosGrid.load(url, function(){
			listadoIngresosGrid.forEachRow(function(id) {
				if(listadoIngresosGrid.getRowAttribute(id,"factura") == "0"){
					listadoIngresosGrid.cells(id, 15).setDisabled(true);
				}	
			});
		});
	}else{
		mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
	}
}

function validarForma(){
	var contadorElementos = 0;
	var elementos         = false;
	var rangoMonto        = false;
	var rangoFechaSol     = false;
	var rangoIniSini      = false;
	var rangoCancelacion  = false;
	
	jQuery(".obligatorio").each(function(index, value) { 
	    if( jQuery(this).val() != "" ){
	    	contadorElementos++;
	    }
	});
	
	jQuery(".tipoServicio").each(function(index, value){
		 if( jQuery(this).is(":checked") ){
			 contadorElementos++;
		 }
	 });
	
	if(contadorElementos > 0){
		elementos = true;
	}
	
	if(elementos){
		return true;
	}else{
		return false;
	}
}
	
function exportarExcel(){
	if( validarForma() ){
	 removeCurrencyFormatOnTxtInput();
	 var form = jQuery("#buscarIngresosForm").serialize();
	 var url = '/MidasWeb/siniestros/recuperacion/listado/ingresos/exportarIngresos.action?'+form;
		if(jQuery("#servicioParticular").val() == 0){
			url += "&filtroIngreso.servicioParticular=" + jQuery("#servicioParticular").val();
		}
		if(jQuery("#servicioPublico").val() == 0){
			url += "&filtroIngreso.servicioPublico=" + jQuery("#servicioPublico").val();
		}
	 window.open(url, "Ingresos");
	}else{
		mostrarMensajeInformativo('Seleccione algun parametro de busqueda y/o ingrese ambos rangos', '10');
	}
	 
}

function validaCheck(elemento){
	
	 var i=0;
	 var tipo = "";
	 jQuery(".tipoServicio").each(function(index, value){
		 if( jQuery(this).is(":checked") ){
			 tipo  = jQuery(this).attr("title");
			 i++;
		 }
	 });
	 
	 if( i == 2 ){
		 jQuery("#hTipoServicio").attr("value",3);
	 }else if( i == 1 ){
		 if( tipo == "Publico" ){
			 jQuery("#hTipoServicio").attr("value",2);
		 }else if( tipo == "Privado" ){
			 jQuery("#hTipoServicio").attr("value",1);
		 }
	 }else if( i == 0){
		 jQuery("#hTipoServicio").attr("value",0);
	 }
	 
	 ///////////////////////////////////////////////
	 if( jQuery(elemento).is(":checked") ){
		 jQuery(elemento).attr("value",1);
	 }else{
		 jQuery(elemento).attr("value",0);
	 }
}

function limpiarFormularioIngresos(){
	jQuery('#buscarIngresosForm').each (function(){
		  this.reset();
	});
	
	 jQuery("#hTipoServicio").attr("value",0);
	 jQuery("#servicioPublico").attr("value",0);
	 jQuery("#servicioParticular").attr("value",0);
}

function mostrarAplicarIngresos(){
	var url = '/MidasWeb/siniestros/recuperacion/ingresos/mostrarContenedor.action?soloConsulta=false';
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarFacturarIngresos(){
	jQuery("#ingresosFacturablesConcat").val(listadoIngresosGrid.getCheckedRows(15));
	if(jQuery("#ingresosFacturablesConcat").val() == "")
		mostrarMensajeInformativo('Seleccione al menos un ingreso', '10');
	else{
		var url = '/MidasWeb/siniestros/emision/factura/mostrarIngresosFacturables.action?soloConsulta=false&filtroIngresos='+jQuery("#ingresosFacturablesConcat").val();
		sendRequestJQ(null, url, targetWorkArea, null);
	}
	
}

function consultarIngreso(ingresoId){
	var url = '/MidasWeb/siniestros/recuperacion/ingresos/mostrarContenedor.action?soloConsulta=true&ingresoId='+ingresoId;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function aplicarIngreso(ingresoId,tipo){
	var url = '/MidasWeb/siniestros/recuperacion/ingresos/mostrarContenedor.action?soloConsulta=false&ingresoId='+ingresoId;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarVentanaCancelacionIngresoPendientePorAplicar(keyIngreso,bandera,tipoRecuperacion){
	var url = '/MidasWeb/siniestros/recuperacion/ingresoCancelacion/mostrarVentanaCancelacionPendientePorAplicar.action?ingresoId='+keyIngreso;
    mostrarVentanaModal("vm_cancelacionPendientePorAplicar", 'Cancelar Pendiente por Aplicar', 200, 120, 300, 200, url,null);     

}


function guardarCancelancionIngresoPendientePorAplicar(){
	var motivo    = jQuery("#motivosDeCancelacion").val();
	var ingresoId = jQuery("#ingresoId").val();
	
	if( ingresoId == "" ){
		parent.mostrarMensajeInformativo('No se puede cancelar el ingreso - idIngreso nulo', '10');
	}else{
		var url = "/MidasWeb/siniestros/recuperacion/ingresoCancelacion/mostrarVentanaCancelacionPendientePorAplicar.action?"
			      +"motivoCancelacionPendientePorAplicar="+motivo+"&ingresoId="+ingresoId+"&guardarCancelacionPendientePorAplicar=1";
		
		parent.sendRequestJQ(null,  url, "vm_cancelacionPendientePorAplicar");
		parent.cerrarVentanaModal("vm_cancelacionPendientePorAplicar");

	}
}

/*
 * 
 function guardarCancelancionIngresoPendientePorAplicar(){
	var motivo    = jQuery("#motivosDeCancelacion").val();
	var ingresoId = jQuery("#ingresoId").val();
	
	if( ingresoId == "" ){
		parent.mostrarMensajeInformativo('No se puede cancelar el ingreso - idIngreso nulo', '10');
	}else{
		console.log("form: "+document.cancelacionPendientePorAplicar.toString());
		parent.submitVentanaModal('vm_cancelacionPendientePorAplicar',document.cancelacionPendientePorAplicar);
		parent.cerrarVentanaModal("vm_cancelacionPendientePorAplicar");
	}
}
 * */
/*function redirectIngresosBusqueda(){
	var url = "/MidasWeb/siniestros/recuperacion/listado/ingresos/mostrarContenedor.action";
	parent.sendRequestJQ(null,  url, targetWorkArea,null);
	parent.mostrarMensajeInformativo('Accion realizada correctamente', '30');
	parent.cerrarVentanaModal("vm_cancelacionPendientePorAplicar");
}*/

function mostrarOpcionesCancelarIngresoAplicado(keyIngreso,bandera,tipoRecuperacion){
	var url = '/MidasWeb/siniestros/recuperacion/ingresoCancelacion/mostrarVentanaCancelacionIngresoAplicado.action?ingresoId='+keyIngreso;
    mostrarVentanaModal("vm_cancelacionIngresoAplicado", 'Cancelar Ingreso Aplicado', 200, 120, 400, 300, url,null);  
}

function continuarCancelacionIngresoAplicado(){
	var url = "";
	var keyIngreso = jQuery("#ingresoId").val();
	var tipoCancelacion = 0;
	
	jQuery(".tipoCancelacion").each(function(index, value) {
	    if( jQuery(this).is(":checked") ){
	    	tipoCancelacion = jQuery(this).val();
	    }
	});
	
	if( tipoCancelacion > 0 ){
		url = "/MidasWeb/siniestros/recuperacion/ingresos/mostrarContenedor.action?ingresoId="+keyIngreso+"&soloConsulta=true&tipoDeCancelacion="+tipoCancelacion;
		parent.sendRequestJQ(null,  url, targetWorkArea,null);	
		parent.cerrarVentanaModal("vm_cancelacionIngresoAplicado");
	}else{
		parent.mostrarMensajeInformativo('Seleccione una opción', '10');
	} 
	
	
}

