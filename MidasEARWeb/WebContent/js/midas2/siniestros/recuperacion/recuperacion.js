var permiteSeleccionMultiple = true;
var tipoInput='checkbox';
function changeMedioRecupera(){
	
}




function limpiarDivsGeneral() {
	limpiarDiv('contenido_devolucionProveedor');
	limpiarDiv('contenido_cancelar');
}

function  changeMedio(){
	var lstMedioRecuperacion = jQuery("#lstMedioRecuperacion").val() ;
	jQuery("#medio").val(lstMedioRecuperacion);
}

function modalSiniestro(){
		var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporteRecuperacion.action";
		mostrarVentanaModal("vm_busquedaRecuperacion", 'Busqueda', 200, 120, 1024, 650,url,null);
	}


function redireccionaBusquedaRecuperacion(){
	var tipoOC =jQuery("#tipoOC").val() ;
	if(null!=tipoOC && tipoOC !=""){
		var target = "#tipo"+tipoOC;
		jQuery("#tipoOC"+tipoOC).attr('checked', false);
		jQuery("#tipoOC").val("") ;
	}
	vaciarCobertura() ;
	vaciarPases( );
}




function changeTipoOC(target){
	var reporteCabina = jQuery("#idReporteCabina").val() ;
	var tipoOC= target.value;
	
	if(null==reporteCabina || reporteCabina==""){
		target.checked = false; 
		mostrarMensajeInformativo('Debe Seleccionar Numero de Siniestro' , '20');		  
		return 0;
	}
	if(null!=tipoOC && tipoOC!="" ){
		vaciarCobertura() ;
		vaciarPases( );
		if(tipoOC=="AFECTACION" ||  tipoOC=="TODOS" ){	
			 blockPage();
			 listadoService.getCoberturasOrdenCompra(reporteCabina,"OC" ,
                     function(data){
                          addOptionsCobertura(data);
                     });
		}
		jQuery("#coberturasSeleccionadas").val( "");
		jQuery("#pasesSeleccionadas").val( "");
		jQuery("#tipoOC").val( tipoOC);
		
		
	}
}

function vaciarCobertura( ) {
	jQuery('#lisCoberturas').remove();
	var contenedor='';
	contenedor+= '<ul id="lisCoberturas" class="w270"';
	contenedor+= 'style="height: 100px; overflow-y: scroll; overflow-x: hidden;">';
	contenedor+= '<li><label for="">';	
	contenedor+='</label></li>';
	contenedor+='</ul>';
	jQuery("#regionCoberturas").append(contenedor);
}

function addOptionsCobertura( map) {
	var contenedor='';	
    if(map != null){
    	contenedor+= '<ul type="none" id="lisCoberturas" class="w270"';
    	contenedor+= 'style="height: 100px; overflow-y: scroll; overflow-x: hidden;">';
    	jQuery.each( map, function( key, value ) { 
    		var keyV=key;
        	var valueV=value;
    		contenedor+= '<li><label for="'+keyV+'">';
        	contenedor+=					'<input type="'+tipoInput+'" ';
        	contenedor+=					'class="desabilitable"';
        	contenedor+=					'name="coberturasConcat"';
        	contenedor+=					'id="tipoOrdenCompra'+keyV+'"';
        	contenedor+=					'onclick="OnChangeCheckboxCobertura (this)"';
        	contenedor+=					'value="'+keyV+'" class="js_checkEnable" ';
        	contenedor+=					'style=""/>';
        	contenedor+=					valueV+'</label></li>';
    		});
    	contenedor+='</ul>';
    	jQuery('#lisCoberturas').remove();
    	jQuery("#regionCoberturas").append(contenedor);
    	
    	
    	
    	
    	
    }
    unblockPage();
}


function iniContenedorRecuperacion(){
	var soloConsulta=jQuery("#soloConsulta").val() ;
	var esNuevoRegistro=jQuery("#esNuevoRegistro").val() ;
	var tipoRecuperacion=jQuery("#tipoRecuperacion").val() ;
	var editarDatosGenericos=jQuery("#editarDatosGenericos").val() ;
	
	if(esNuevoRegistro=='true'){
		jQuery("#btn_Busqueda").css("visibility","visible");	
	}else if (soloConsulta=='true'){
		jQuery("#btn_Busqueda").css("visibility","hidden");
	}else if(editarDatosGenericos=='true'){
		jQuery("#btn_Busqueda").css("visibility","visible");	
	}else{
		jQuery("#btn_Busqueda").css("visibility","hidden");
	}
	if(tipoRecuperacion=="CIA"){
		permiteSeleccionMultiple=false;
		tipoInput='radio';
	}else{
		permiteSeleccionMultiple=true;
		tipoInput='checkbox';
	}
	/*if(esNuevoRegistro=='true'){
		jQuery("#btn_Busqueda").show();		
	}else if (soloConsulta=='true'){
		jQuery("#btn_Busqueda").hide();
	}else{
		jQuery("#btn_Busqueda").hide();
	}*/	
	
}


function iniContenedorVentaPRV(){
	var soloConsulta=jQuery("#soloConsulta").val() ;
	if (soloConsulta=='true'){
		jQuery("#btn_Busqueda").css("visibility","hidden");
	}else{
		jQuery("#btn_Busqueda").css("visibility","hidden");
	}	
//	if (soloConsulta=='true'){
//		jQuery("#btn_Busqueda").hide();
//	}else{
//		jQuery("#btn_Busqueda").hide();
//	}	
	
}













var coberturasSeleccion 
function OnChangeCheckboxCobertura (checkbox) {
		var value = checkbox.value ;
		var idcompuesta = value;
		var split = idcompuesta.split('|');
		var id = split[0];
		var cve = split[1];		
    if (checkbox.checked) {
    	if(permiteSeleccionMultiple){
    		coberturasSeleccion= jQuery("#coberturasSeleccionadas").val() +  value+",";	
    	}else{
    		coberturasSeleccion=value;	
    	}
    	jQuery("#coberturasSeleccionadas").val( coberturasSeleccion);
        vaciarPases() 
		 blockPage();
	   	 utileriasService.getListasTercerosAfectadorPorCoberturas(coberturasSeleccion ,
	               function(data){
		 			addOptionsPases(data);
	   		 		var size = sizeMap(data);
		   		 	var tipoRecuperacion=jQuery("#tipoRecuperacion").val() ;
			   		if(tipoRecuperacion=="CIA" && null!=coberturasSeleccion && coberturasSeleccion!=''){
			   			if(size >=1){
			   				reiniciaInfoPase();
			   		 		mostrarMensajeInformativo("Seleccione Pase de Atenci\u00f3n."  , '20');
			   			}else{
			   				obtenerInfoAfectacion(coberturasSeleccion, '');
			   			}
			   			
			   		}
		   			
	     });
    }
    else {
    	var coberturasStr= jQuery("#coberturasSeleccionadas").val();
    	var str = coberturasStr.replace(value+',', ""); 
    	jQuery("#coberturasSeleccionadas").val( str);
    	var res = str.replace(value, "");
    	jQuery("#coberturasSeleccionadas").val( res);
        vaciarPases() 
		 blockPage();
	   	 utileriasService.getListasTercerosAfectadorPorCoberturas(res ,
	               function(data){
	   		 		addOptionsPases(data,res);
	     });
    }
}





function vaciarPases( ) {
	jQuery('#lisPases').remove();
	var contenedor='';
	contenedor+= '<ul id="lisPases" class="w270"';
	contenedor+= 'style="height: 100px; overflow-y: scroll; overflow-x: hidden;">';
	contenedor+= '<li><label for="">';	
	contenedor+='</label></li>';
	contenedor+='</ul>';
	jQuery("#regionPases").append(contenedor);
}



function addOptionsPases( map) {
	var contenedor='';
	var pases = jQuery("#pasesSeleccionadas").val( );
		var split = pases.split(',');
		
    if(map != null){
    	contenedor+= '<ol type="none" id="lisPases" class="w270"';
    	contenedor+= 'style="height: 100px; overflow-y: scroll; overflow-x: hidden;">';
    	var pasesExistentes = "";
    	jQuery.each( map, function( key, value ) { 
    		var keyV=key;
        	var valueV=value;
    		contenedor+= '<li><label for="'+valueV+'">'; 
    		
    		if( permiteSeleccionMultiple && contains(split, keyV) ){
    			contenedor+=					'<input type="'+tipoInput+'" checked ';
    			pasesExistentes+=keyV+",";
    		}else{
    			contenedor+=					'<input type="'+tipoInput+'"  ';
    		}
        	contenedor+=					'class="desabilitable"';
        	contenedor+=					'name="pasesConcat"';
        	contenedor+=					'id="pase'+keyV+'"';
        	contenedor+=					'onclick="OnChangeCheckboxPase(this); (this)"';
        	contenedor+=					'value="'+keyV+'" class="js_checkEnable" ';
        	contenedor+=					'style=""/>';
        	contenedor+=					valueV+'</label></li>';
    		});
    	contenedor+='</ol>';
    	jQuery('#lisPases').remove();
    	jQuery("#regionPases").append(contenedor);
    	jQuery("#pasesSeleccionadas").val( pasesExistentes);
    }
    unblockPage();
}




var paseSeleccion ;
function OnChangeCheckboxPase (checkbox) {	 
		var value = checkbox.value ;
    if (checkbox.checked) {
    	if(permiteSeleccionMultiple){
    		paseSeleccion= jQuery("#pasesSeleccionadas").val() +  value+",";	
    	}else{
    		paseSeleccion= value;	
    	}
    	jQuery("#pasesSeleccionadas").val( paseSeleccion);
    	var tipoRecuperacion=jQuery("#tipoRecuperacion").val() ;
		 	if(tipoRecuperacion=="CIA" && null!=paseSeleccion && paseSeleccion!=''){
		 		obtenerInfoAfectacion(coberturasSeleccion, paseSeleccion);
		 	}
    }
    else {
    	var pasesStr= jQuery("#pasesSeleccionadas").val();
    	var str = pasesStr.replace(value+',', ""); 
    	jQuery("#pasesSeleccionadas").val( str);
    	var res = str.replace(value, "");
    	jQuery("#pasesSeleccionadas").val( res);
    }
}

//contains function
function contains(arr, findValue) {
var i = arr.length;
while (i--) {
   if (arr[i] == findValue) return true;
}
return false;
}

function cerrarRecuperacion(){	 	
	var esConsulta = jQuery('#h_soloLectura').val();	
	if(esConsulta != null && (esConsulta == 'true' || esConsulta == true)){
		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/listado/recuperaciones/mostrarContenedor.action',targetWorkArea,null);
	}else{
		if(confirm("¿Desea cerrar la pantalla? La información no almacenada se perderá." )){
			sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/listado/recuperaciones/mostrarContenedor.action',targetWorkArea,null);
		}
	}	
}

function cerrarVentanaCancelarRecuperacion(idVentana, idRecuperacion, estatus, onClose){	
	if(estatusRecuperacion == 'CANCELADO'){
		parent.cerrarVentanaModal(idVentana, onClose);		
	}else{
		parent.cerrarVentanaModal(idVentana);		
	}
}




