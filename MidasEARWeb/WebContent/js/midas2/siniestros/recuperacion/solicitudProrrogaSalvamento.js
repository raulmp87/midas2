function initProrrogaAutorizada(){
	
	var isAutorizada = jQuery("#autorizacionProrroga").val();
	var fechaAutorizacionProrroga = jQuery("#fechaAutorizacionProrroga").val();
	var isProrroga = jQuery("#solicitudProrroga").val();
	
	if( isAutorizada == "true" ){
		jQuery(".prorrogaaAutorizada").css("background-color","#EEEEEE");
		jQuery(".prorrogaaAutorizada").attr("readonly", true);
		jQuery("#opcAutorizacion").attr("checked",true);
	}
	
	if( isProrroga == "true" ){
		jQuery(".solicitudEnviada").attr("readonly", true);
		jQuery(".solicitudEnviada").css("background-color","#EEEEEE");
	}
	
	
	if( fechaAutorizacionProrroga != "" ){
		jQuery("#opcAutorizacion").attr("disabled","disabled");
	}
	
	
}

function solicitarProrroga(){
	
	var mensaje             = "";
	var fechaAutProrroga    = jQuery("#fechaAutorizacionProrroga").val();
	var isSolicitudPro      = jQuery("#solicitudProrroga").val();
	var isAutorizada        = jQuery("#autorizacionProrroga").val();
	//var isUsuarioAutorizado = jQuery("#usuarioAutorizado").val();
	var autProrrogaComboSeleccionado = jQuery("#seleccionAutorizaSubasta").val();
	
	autProrrogaComboSeleccionado = (typeof autProrrogaComboSeleccionado == 'undefined' ? "":autProrrogaComboSeleccionado );
	
	removeCurrencyFormatOnTxtInput();
	
	if( jQuery("#tieneVentaActiva").val() == "s" ){
	
		var form = jQuery("#salvarSalvamentoForm").serialize();
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionSalvamento/guardarProrroga.action?"+form;
		
		
		if( fechaAutProrroga != "" ){
			mostrarMensajeInformativo("La prorroga no puede ser modificada", '10');
		}else{
			
			var fechaCierreSubasta = convertirFecha(jQuery("#fechaCierreSubasta").val());
			// SUMA 6 DIAS
			fechaCierreSubasta     = sumaDias(fechaCierreSubasta,5);
			
			
			// CREAR MODIFICAR PRORROGA
			if( fechaAutProrroga == "" && autProrrogaComboSeleccionado == "" ){
				if( jQuery("#fechaLimiteProrroga").val() == "" ){
					mostrarMensajeInformativo("Ingrese la fecha de la prorroga", '10');
				}else if(  jQuery("#comentarios").val() == "" ){
					mostrarMensajeInformativo("Ingrese los comentarios de la prorroga", '10');
				}else if ( convertirFecha(jQuery("#fechaLimiteProrroga").val()) < fechaCierreSubasta ){	
					mostrarMensajeInformativo("La fecha límite prorroga debe ser 5 días mayor al cierre de la subasta", '10');
				}else{
				 sendRequestJQ(null, url, targetWorkArea, null);
				}
				
			}else{
				// APROBAR - DENEGAR PRORROGA
				if ( isSolicitudPro == "true" && fechaAutProrroga == "" && autProrrogaComboSeleccionado != "" ){
					mensaje = ( autProrrogaComboSeleccionado == "s" ? "AUTORIZAR" : "RECHAZAR" );
					if(confirm("Esta seguro que desea "+mensaje+" la prorroga?")){
						sendRequestJQ(null, url, targetWorkArea, null);
					}
				}
			}
		}
		
	}else{
		mostrarMensajeInformativo("Para solicitar una prorroga se requiere venta activa", '10');
	}

}

function sumaDias(fecha,dias){
	return new Date(fecha.getTime() + (parseInt(dias) * 24 * 3600 * 1000));
}



function opcAutorizarProrroga(elemento){
	 if( jQuery(elemento).is(":checked") ){
		 jQuery("#seleccionAutorizaSubasta").attr("value","s");
	 }else{
		 jQuery("#seleccionAutorizaSubasta").attr("value","n");
	 }
}
