/**
 * 
 */

function verTabDetalleRecuperacion(){
	var soloConsulta = jQuery("#soloConsulta").val() ;
	//removeCurrencyFormatOnTxtInput();
	var form = jQuery('#contenedorRecuperacionDeducibleForm').serialize();
	var url= '/MidasWeb/siniestros/recuperacion/recuperacionDeducible/mostrar.action?'+ form;
	sendRequestJQ(null,  url, 'detalleRecupDeducible',null);
	
}

function guardarRecuperacionDeducible()
{

	if(validateAll(true)){
		var recuperacionId = jQuery("#idRecuperacion").val();
		var tipoMostrar = jQuery("#tipoMostrar").val();
		var tipoProceso = jQuery("#tipoProceso").val();
		var fechaIngreso = jQuery("#fechaIngreso").val();
		var fechaEntrega = jQuery("#fechaEntrega").val();	
	    var numeroValuacion = jQuery("#numeroValuacion").val();
	    var nombreValuador = jQuery("#nombreValuador").val();
	    var nombreAfectado = jQuery("#nombreAfectado").val();	
		    
	    removeCurrencyFormatOnTxtInput();
		var url= '/MidasWeb/siniestros/recuperacion/recuperacionDeducible/guardar.action?recuperacion.id=' + recuperacionId 
		+ '&recuperacion.tipoProceso=' + tipoProceso + '&recuperacion.fechaIngreso=' + fechaIngreso + '&recuperacion.fechaEntrega=' + fechaEntrega 
		+ '&recuperacion.numeroValuacion=' + numeroValuacion + '&recuperacion.nombreValuador=' + nombreValuador + '&recuperacion.afectado = ' + nombreAfectado
		+ '&tipoMostrar=' + tipoMostrar;
		sendRequestJQ(null,  url, targetWorkArea,null);
	}
}

function compararFechas(fechaIngreso, fechaEntrega){
	var dateIngreso = new Date(fechaIngreso);
	var dateEntrega = new Date(fechaEntrega);
	
	if(!fechaIngreso
			||!fechaEntrega 
			|| (fechaIngreso && fechaEntrega && (dateIngreso <= dateEntrega))){
		return true;
	}
}

function onChangeFechaEntrega(){
	var fechaIngreso = jQuery("#fechaIngreso").val();
	var fechaEntrega = jQuery("#fechaEntrega").val();
	if(!compararFechas(fechaIngreso, fechaEntrega)){
		mostrarMensajeInformativo('La fecha de entrega no puede ser menor que la de ingreso' , '20');
		jQuery("#fechaEntrega").val('');
	}
}

function onChangeFechaIngreso(){
	var fechaIngreso = jQuery("#fechaIngreso").val();
	var fechaEntrega = jQuery("#fechaEntrega").val();
	if(!compararFechas(fechaIngreso, fechaEntrega)){
		mostrarMensajeInformativo('La fecha de entrega no puede ser menor que la de ingreso' , '20');
		jQuery("#fechaIngreso").val('');
	}
}

function cancelarRecuperacionDeducible(motivoCancelacion)
{	
	if(confirm("¿Está seguro que desea cancelar la recuperación?")) 			
	{
		if(motivoCancelacion != null && motivoCancelacion != "")
		{			
			var recuperacionId = jQuery("#idRecuperacion").val();
			var usuarioCancelacion = jQuery("#usuarioCancelacion").val();
			var tipoMostrar = jQuery("#tipoMostrar").val();
			
			var url= '/MidasWeb/siniestros/recuperacion/recuperacionDeducible/cancelar.action?recuperacion.id=' + recuperacionId + '&recuperacion.motivoCancelacion=' + motivoCancelacion 
			+ '&recuperacion.codigoUsuarioCancelacion=' + usuarioCancelacion + '&tipoMostrar=' + tipoMostrar;
			sendRequestJQ(null,  url, targetWorkArea,null);				
		}
		else
		{
			mostrarMensajeInformativo('Debe Proporcionar un motivo de cancelación' , '20');		
		}		
	}	
}

function generarMovIngreso()
{	
	if(confirm("¿Está seguro que desea generar un movimiento de ingreso ? ")) 			
	{
		var recuperacionId = jQuery("#idRecuperacion").val();
			
		var url= '/MidasWeb/siniestros/recuperacion/recuperacionDeducible/generarMovimientoIngreso.action?recuperacion.id=' + recuperacionId;
		
		sendRequestJQ(null,  url, targetWorkArea,null);			
	}	
}

function imprimirInstructivoDeposito(){
	var idRecuperacion = jQuery("#idRecuperacion").val();
	console.log("imprimirInstructivo recuperacion: " + idRecuperacion);
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionDeducible/imprimirInstructivoDeposito.action?recuperacion.id=" + idRecuperacion ;
    window.open(url, "InstructivoDeposito");
}
