var informacionAfectacion; 
var listadoOrdenesCiaGrid;
var seguimientosCiaGrid;
var  mensajeInfoPase;
var fechaAcuseCia  = null;
var popUpRegistroCompl;
var TIPO_MENSAJE_ERROR = "10";

function cancelacionCia (){
	var estatus=jQuery("#estatus").val();
	var estatusCartaLis=jQuery("#estatusCartaLis").val();
	var recuperacionId   = jQuery("#recuperacionId").val();	
	if(estatus=='RECUPERADO'){
 		 mostrarMensajeInformativo('La recuperacion ya ha sido Recuperada', '10');
 		 return 0;
	}
	
	if(estatusCartaLis!='PEND_CANC'){
		 mostrarMensajeInformativo('el Estatus de la Carta es diferente de Pendiente por Cancelar ', '10');
		 return 0;
	}
	if(confirm(" ¿Esta seguro de Cancelar la Recuperaci\u00f3n de Compa\u00F1\u00EDa?  ")){
		var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/cancelarRecuperacion.action?recuperacionId="+jQuery("#recuperacionId").val();
		sendRequestJQ(null,ruta,targetWorkArea,null);
	}
}

function changeMotivoCancelacion (target){
	var motivo= target.value;  
	jQuery("#motivoCancelacion").val(motivo);
}

function cerrarSolCancela(){
	parent.cerrarVentanaModal("vm_SolicitudCancelacionCIA",true);
}
function continuarSolCancelaCIA(){
	var motivo = jQuery("#motivoCancelacion").val();
	var recuperacionId = jQuery("#recuperacionIdCia").val();
	if(null==motivo || motivo ==''){
		return 0;
	}else{
     parent.cerrarVentanaModal("vm_SolicitudCancelacionCIA",'solicitarCancelarCIA("'+motivo+'"  , '+ recuperacionId+'   );');
	}

}	

function solicitarCancelarCIA(motivo, recuperacionId ){	
	var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/solicitarCancelacion.action?recuperacionId="+recuperacionId+"&recuperacion.motivoCancelacion="+motivo;
	sendRequestJQ(null,ruta,targetWorkArea,null);
}


function solicitarCancelacion (){
	var recuperacionId   = jQuery("#recuperacionId").val();
	var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarSolCancela.action?url=EDITAR"+"&recuperacionId="+recuperacionId;
	mostrarVentanaModal("vm_SolicitudCancelacionCIA", 'Solicitud de Cancelaci\u00f3n', 550, 310, 600, 200,ruta,null);
}



function buscarRecuperacionesVencer(){
	document.getElementById("pagingArea").innerHTML = '';
 	document.getElementById("infoArea").innerHTML = '';
 	var recuperacionesVencerGrid = new dhtmlXGridObject('recuperacionesVencerGrid');
 	recuperacionesVencerGrid.attachEvent("onXLS", function(grid){	
 		blockPage();
     });
 	recuperacionesVencerGrid.attachEvent("onXLE", function(grid){		
 		unblockPage();
     });
 	
 	recuperacionesVencerGrid.attachEvent("onXLE", function(){
 	    if (!recuperacionesVencerGrid.getRowsNum())
 	    	recuperacionesVencerGrid.addRow(recuperacionesVencerGrid.uid(), "No hay Recuperaci\u00F3n por Vencer");
 	})
	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/buscarAlertas.action';
	recuperacionesVencerGrid.load( ruta);

}



function iniContenedorRecuperacionCIA(){	
	var esCartaCompl = jQuery("#esCartaComplemento").val() ;
	if(esCartaCompl=='true'){		
		jQuery("#ch_esCarta").attr('checked', true);
	}else{
		jQuery("#ch_esCarta").attr('checked', false);
	}	
	jQuery("#divElabora").hide();
	jQuery("#divImprime").hide();
	jQuery("#divEntrega").hide();
	var tipoMostrar=jQuery("#tipoMostrar").val() ;		
	var cartaEstatus=jQuery("#estatusCartaLis").val() ;	
	 var soloConsulta=jQuery("#soloConsulta").val() ;
	 var esNuevoRegistro=jQuery("#esNuevoRegistro").val() ;	
	 if (soloConsulta=='true' || tipoMostrar=='R' || tipoMostrar=='C' ){
		 bloquearDatosCIA(true);
		 jQuery("#guardarBtn").hide();
	 }else{
		  bloquearDatosCIA(false);
		 if(tipoMostrar=='U'){
			 if(cartaEstatus=='REGISTRADO' || cartaEstatus=='PEND_ELAB' ){
				 jQuery("#divElabora").show();
				 jQuery("#guardarBtn").show();
			 }else{
				 jQuery("#guardarBtn").hide();
				 bloquearDatosCIA(true);
				 if (cartaEstatus=='ELABORADO'){
					 jQuery("#divImprime").show();
				}else  if (cartaEstatus=='IMPRESO'){
					jQuery("#divImprime").show();
					jQuery("#divEntrega").show();
				}
			 }
		}
	 }
	
	 
	 
}



function incializarTabs(){

}

function checkCartaComp(){
	var esCarta =  jQuery("#ch_esCarta").attr('checked'); 	
		if(esCarta){
			jQuery("#esCartaComplemento").val(true);
		}else{
			jQuery("#esCartaComplemento").val(false);
		}
}

function buscarOrdenCompraCia(){
	var cartaEstatus=jQuery("#estatusCartaLis").val() ;	
	var esNuevoRegistro=  jQuery("#esNuevoRegistro").val();
	var recuperacionId =   jQuery("#recuperacionId").val();
	var coberturasSeleccion= jQuery("#coberturasSeleccionadas").val() ;
	var paseSeleccion= jQuery("#pasesSeleccionadas").val();
	var soloConsulta =  jQuery("#soloConsulta").val();	
	var editarDatosGenericos=  jQuery("#editarDatosGenericos").val();	
	var porcentajePart= jQuery("#porcParticipa").val();	
	var ordenesCompraConcat= jQuery("#ordenesCiaConcat").val();
	var reporteCabinaId= jQuery("#idReporteCabina").val();
	document.getElementById("pagingAreaOC").innerHTML = '';
 	document.getElementById("infoAreaOC").innerHTML = '';

 	listadoOrdenesCiaGrid = new dhtmlXGridObject('ordenesCiaGrid');
 	listadoOrdenesCiaGrid.attachEvent("onCheckbox",doOnCheckOC);
 	listadoOrdenesCiaGrid.attachEvent("onXLS", function(grid){	
 		blockPage();
     });
 	listadoOrdenesCiaGrid.attachEvent("onXLE", function(grid){		
 		unblockPage();
     });
 	
 	listadoOrdenesCiaGrid.attachEvent("onXLE", function(){ 	  
 		if(soloConsulta!='true'  &&  editarDatosGenericos!='false'   )
	    	calcularTotalOrdenesCia();
 	})

	 if(soloConsulta!='true' &&  editarDatosGenericos!='false'){
		 listadoOrdenesCiaGrid._in_header_master_checkbox=function(t,i,c){
			   t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
			   var self=this;
			   t.getElementsByTagName("input")[0].onclick=function(e){
				   if(this.checked){
					   listadoOrdenesCiaGrid.checkAll();
					   doOnCheckOC();
				   }else{
					   listadoOrdenesCiaGrid.uncheckAll();
					   doOnCheckOC();
				   }
			   }
			}	 
	 }else {
		 listadoOrdenesCiaGrid._in_header_master_checkbox=function(t,i,c){
			   t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
			   var self=this;
			}
		     listadoOrdenesCiaGrid.attachEvent("onXLE", function(grid_obj){
			 listadoOrdenesCiaGrid.checkAll();
			 jQuery("#ordenesCiaConcat").val(listadoOrdenesCiaGrid.getCheckedRows(0));
			 
			 listadoOrdenesCiaGrid.forEachRow(function(id){
			  listadoOrdenesCiaGrid.cells(id,0).setDisabled( true);
				});
			}
		 );
	 }
 	
	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/obtenerOrdenesCompraCia.action?soloConsulta='+soloConsulta+
			'&esNuevoRegistro='+esNuevoRegistro+'&recuperacionId='+recuperacionId+'&coberturasSeleccionadas='+coberturasSeleccion+'&pasesSeleccionadas='+
			paseSeleccion+'&porcentajeParticipacion='+porcentajePart+'&ordenesCiaConcat='+ordenesCompraConcat
			+'&editarDatosGenericos='+editarDatosGenericos+'&recuperacion.reporteCabina.id='+reporteCabinaId  ;
	
	listadoOrdenesCiaGrid.load( ruta);
}

function doOnCheckOC(rowId,cellInd,state){
	blockPage();
	validaOrdenesCompraGrua();
	calcularTotalOrdenesCia();	
	//validaMontoPiezas();
	unblockPage();
}

function validaOrdenesCompraGrua(){
	 var statusCombo; 
	 var totalGrua = 0, montoARecuperar = 0;
	 var tipo = "";
	 listadoOrdenesCiaGrid.forEachRow(function(id){
		 
		 statusCombo = listadoOrdenesCiaGrid.cells(id,0).isChecked();
		 montoARecuperar = listadoOrdenesCiaGrid.cells(id,7).getValue();
		 tipo = listadoOrdenesCiaGrid.cells(id,3).getValue();
		 
		 if( tipo == "Gasto Ajuste Grúa" ){
			 //console.log( "SE VALIDA GASTO AJUSTE GRÚA: "+id);
			 if( statusCombo ){
				 //console.log( "ID: "+id+" IS CHECKED");
				 totalGrua   += parseFloat(montoARecuperar);
			 }
		 }
		 
	 });
	 
	 //console.log("Total Grua: "+totalGrua);
	 jQuery("#montoGAGrua").val(totalGrua) ;
}


function validaMontoPiezas(){
	 var statusCombo; 
	 var totalPiezas = 0, montoSeleccionado = 0;
	 var tipo = "";
	 listadoOrdenesCiaGrid.forEachRow(function(id){
		 
		 statusCombo = listadoOrdenesCiaGrid.cells(id,0).isChecked();
		 montoSeleccionado = listadoOrdenesCiaGrid.cells(id,9).getValue();
		 alert('Obtengo el monto PRV (montoSeleccionado): '+montoSeleccionado);
		 alert('totalPiezas :'+totalPiezas);

		 if( statusCombo ){
				 totalPiezas   += parseFloat(montoSeleccionado);
		 }
	 });
	 jQuery("#montoPiezas").val(totalPiezas) ;
}
 
function calcularTotalOrdenesCia(){	
	
	var soloConsulta =  jQuery("#soloConsulta").val();	
	var editarDatosGenericos=  jQuery("#editarDatosGenericos").val();	

	if(soloConsulta!='true'  && editarDatosGenericos!='false'){
		removeCurrencyFormatOnTxtInput();	
		jQuery("#ordenesCiaConcat").val(listadoOrdenesCiaGrid.getCheckedRows(0));
		var paseSeleccionados    = jQuery("#pasesSeleccionadas").val();
		var ordenesSeleccionadas = jQuery("#ordenesCiaConcat").val() ;
		var porcParticipa        = jQuery("#porcParticipa").val() ;
		var montoGAGrua          = jQuery("#montoGAGrua").val() ;
		var montoSalvamento      = jQuery("#montoSalvamento").val() ;
		var montoPiezas      = jQuery("#montoPiezas").val() ;
		if(null == porcParticipa || porcParticipa==""){
			porcParticipa=0;
		}
		
		var data=jQuery("#contenedorRecuperacionCIAForm").serialize();
	    jQuery.ajax({
	          url: '/MidasWeb/siniestros/recuperacion/recuperacionCia/calcularImportes.action?ordenesCiaConcat='+ ordenesSeleccionadas+'&porcentajeParticipacion='+ porcParticipa+
	          				'&recuperacion.montoGAGrua='+ montoGAGrua+'&recuperacion.montoPiezas='+montoPiezas+'&recuperacion.montoSalvamento='+montoSalvamento+'&pasesSeleccionadas='+paseSeleccionados,
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: data,
	          success: function(json){
	        	unblockPage();
	          	var importe = json.infoImportes;
	          	jQuery("#montoIndemnizacion").val(importe.montoIndemniza);
	           	jQuery("#importeCobrar").val(importe.montoTotal);
	           	jQuery("#montoSalvamento").val(importe.montoSalvamento);
	           	jQuery("#montoGAGrua").val(importe.montoGrua);
	           	jQuery("#montoPiezas").val(importe.montoPiezas);
	           	
	          },
	          beforeSend: function(){
	        	  blockPage();
			  }	
	    });
	    initCurrencyFormatOnTxtInput();
	}
}

function guardarRecuperacionCIA(){
	removeCurrencyFormatOnTxtInput();	
	if (validaGuardarRecuperacion(false)){
		if(confirm("¿Desea Guardar la informacion? ")){
			jQuery("#ciaLis").val( parseInt(jQuery("#ciaLis").val())) ;   
			var formParams = jQuery(document.contenedorRecuperacionCIAForm).serialize();
			
			 var data="";
			var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/validarGuardarRecuperacion.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize();
			
			jQuery.ajax({
	        	url:ruta ,
	        	dataType: 'json',
	          async:false,
	          type:"POST",
	          data: data,
	          success: function(json){
	          	 var mensaje = json.mensaje;
	          	 if(  json.tipoMensaje=="10"){//Error
	          		 if (null!= mensaje  ||  mensaje !=''){
		          		 mostrarMensajeInformativo(mensaje, '10');
		          	 }else{
		          		mostrarMensajeInformativo('Error General del sistema', '10'); 
		          	 }
	          	 }else if ( json.tipoMensaje=="30"){//Exito
	          		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/guardarRecuperacion.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize(),targetWorkArea,null);
	          	 }
	          	 
	          	 
	          	
	          }
			});
		}
	}
	initCurrencyFormatOnTxtInput();
}

function anexarArchivo(){
	
	var idRecuperacion = jQuery("#h_idRecuperacion").val();
	if(idRecuperacion != null && idRecuperacion != "")
	{
		verLigaDocumento('RECUP_CIA','' , 'FORTIMAX', idRecuperacion, 'x', 'Anexo Rec Cia');
	}else
	{
		mostrarMensajeInformativo("Debe guardar la recuperaci\u00F3n antes de anexar el archivo." , '20');
	}
	
}

function validaPorcentaje(){
	var porcParticipa =parseFloat(jQuery("#porcParticipa").val()) ; 
	var editarDatosGenericos=  jQuery("#editarDatosGenericos").val();	
	var soloConsulta =  jQuery("#soloConsulta").val();	
	jQuery("#ordenesCiaConcat").val(listadoOrdenesCiaGrid.getCheckedRows(0));
	if(null==porcParticipa || porcParticipa=="" ){
		jQuery("#porcParticipa").val(0) ; 
		porcParticipa =parseFloat(jQuery("#porcParticipa").val()) ; 
	}
	if(soloConsulta!='true'  && editarDatosGenericos!='false'){
		buscarOrdenCompraCia();
	}
}

function mostrarCia(){
	var estado =jQuery("#estadosLis").val();
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true&filtroCatalogo.estatus=1&filtroCatalogo.tipoPrestadorStr=COMP"  ; 
	mostrarVentanaModal("vm_asignarComprador", "Asignar Compañia", null, null, 690, 650, url , "");
}


function obtenerInfoAfectacion(coberturaConcat,paseAtnConcat){
	  removeCurrencyFormatOnTxtInput();	
	  reiniciaInfoPase();
      var data="";
      jQuery.ajax({
            url: '/MidasWeb/siniestros/recuperacion/recuperacionCia/obtenerDatosPase.action?coberturasSeleccionadas='+ coberturaConcat+'&pasesSeleccionadas='+paseAtnConcat  ,
            dataType: 'json',
            async:false,
            type:"POST",
            data: data,
            success: function(json){
            	var informacionAfectacion = json.infoAfectacion;
            	mensajeInfoPase= 	informacionAfectacion.mensaje;
            	if(null==mensajeInfoPase || mensajeInfoPase==''){
            		 jQuery("#poliza").val(informacionAfectacion.numeroPoliza); 
	               	 jQuery("#estadoUbica").val(informacionAfectacion.estadoUbicacion);  
	               	 jQuery("#ciudadUbica").val(informacionAfectacion.ciudadUbicacion);  
	               	 jQuery("#lugarUbica").val(informacionAfectacion.lugarSiniestro);
	               	 jQuery("#fechaSin").val(informacionAfectacion.fechaSiniestroStr); 	               	 
	               	 jQuery("#tipoPerdida").val(informacionAfectacion.tipoPerdida);
	               	 jQuery("#vehiculoAsegurado").val(informacionAfectacion.vehiculoAsegurado); 
	               	 jQuery("#personaLesioada").val(informacionAfectacion.personaLesionada);
	               	 jQuery("#vehiculoTercero").val(informacionAfectacion.vehiculoTercero);
	               	jQuery("#conceptoCarta").val(informacionAfectacion.conceptoCarta);
	               	jQuery("#termioAjuste").val(informacionAfectacion.termioAjuste);
	               	jQuery("#polizaCia").val(informacionAfectacion.numPolizaCia);
	               	jQuery("#numSiniestroCia").val(informacionAfectacion.noSiniestrosCia);	
	               	jQuery("#porcParticipa").val(informacionAfectacion.porcParticipacion);	
	               	jQuery("#prestadorServicioId").val(informacionAfectacion.companiaId);
	               	jQuery("#nombreCia").val(informacionAfectacion.nombreCompania);	
	               	jQuery("#montoGAGrua").val(informacionAfectacion.montoGrua);
	               	jQuery("#montoSalvamento").val(informacionAfectacion.montoSalvamento);
	               	jQuery("#importeCobrar").val(informacionAfectacion.montoTotal);
	               	jQuery("#tipoPerdida").val(informacionAfectacion.tipoPerdida);
	               	jQuery("#idCobertura").val(informacionAfectacion.idCoberturaRepCabina);
	               	jQuery("#idPaseAtencion").val(informacionAfectacion.idEstimacionCoberturaRepCabina);
	               	jQuery("#reservaDisponible").val(json.reservaDisponible);
	                jQuery("#guardarBtn").show();
	               	buscarOrdenCompraCia();
            	}else{
            		mostrarMensajeInformativo(mensajeInfoPase , '20');
            		listadoOrdenesCiaGrid.clearAll();
            	}
            }
      });
      initCurrencyFormatOnTxtInput();
}

function reiniciaInfoPase(){
	
	jQuery("#termioAjuste").val("");
	jQuery("#poliza").val(""); 
  	 jQuery("#estadoUbica").val("");  
  	 jQuery("#ciudadUbica").val("");  
  	 jQuery("#lugarUbica").val("");
  	 jQuery("#fechaSin").val(""); 
  	 jQuery("#tipoPerdida").val("");
  	 jQuery("#vehiculoAsegurado").val(""); 
  	 jQuery("#personaLesioada").val("");
  	 jQuery("#vehiculoAsegurado.Tercero").val("");
  	//jQuery("#conceptoCarta").val("");
  	//jQuery("#polizaCia").val("");
  	//jQuery("#numSiniestroCia").val("");
  	jQuery("#montoGAGrua").val(0);
  	jQuery("#montoSalvamento").val(0);
  	jQuery("#importeCobrar").val(0);
  	jQuery("#tipoPerdida").val("");
  	jQuery("#idCobertura").val(0);
  	jQuery("#idPaseAtencion").val(0);
  	jQuery("#montoIndemnizacion").val(0);  	
  	jQuery("#ordenesCiaConcat").val("");
  	jQuery("#reservaDisponible").val(0);
  	
  	jQuery("#guardarBtn").hide();
  	
  	
}

function verTabCIA(){
		var soloConsulta = jQuery("#soloConsulta").val() ;
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery(document.contenedorRecuperacionCIAForm).serialize();
		 var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedorCompanias.action?'+ formParams;
		sendRequestJQ(null,  ruta, 'contenido_tabRecuperacionCIA',null);
}

function validaGuardarRecuperacion( validarMontoPorRecuperar ){
	var coberturasSeleccionadas= jQuery("#coberturasSeleccionadas").val( );
	var pasesSeleccionadas= jQuery("#pasesSeleccionadas").val( );
	var numSiniestroCia = jQuery("#numSiniestroCia").val() ;
	var ciaLis = jQuery("#ciaLis").val() ;
	var polizaCia = jQuery("#polizaCia").val() ;
	var porcParticipa = jQuery("#porcParticipa").val();
	var termioAjuste= jQuery("#termioAjuste").val();
	var conceptoCarta= jQuery("#conceptoCarta").val(); 
	var estatusCartaLis =  jQuery("#estatusCartaLis").val(); 
	var montoPorCobrarCia = jQuery("#importeCobrar").val();
	
	if(null==coberturasSeleccionadas ||  coberturasSeleccionadas==""){
		mostrarMensajeInformativo('Debe Seleccionar una Cobertura' , '20');
		return false;
	}
	
	if( jQuery.isEmptyObject(mensajeInfoPase )==false ){
		mostrarMensajeInformativo(mensajeInfoPase , '20');
		return false;
	}
	
	
	if(null==numSiniestroCia ||  numSiniestroCia==""){
		mostrarMensajeInformativo('Debe Capturar el numero de Siniestro Compa\u00f1ia.' , '20');
		return false;
	}
	if(null==polizaCia ||  polizaCia==""){
		mostrarMensajeInformativo('Debe Capturar el numero de Poliza Compa\u00f1ia.' , '20');
		return false;
	}
	
	
	if(null==conceptoCarta ||  conceptoCarta==""){
		mostrarMensajeInformativo('Debe Capturar Concepto de Recuperacion' , '20');
		return false;
	}
	
	if(null==estatusCartaLis ||  estatusCartaLis==""){
		mostrarMensajeInformativo('Seleccione el Estatus de la Carta ' , '20');
		return false;
	}

	
	if(null==porcParticipa ||  porcParticipa==""){
		mostrarMensajeInformativo('No existe porcentaje de participación' , '20');
		return false;
	}
	if(porcParticipa<1 ||  porcParticipa>100){
		mostrarMensajeInformativo('El Porcentaje de Participac\u00edn debe ser en un Rango de 0% a 100% ' , '20');
		return false;
	}
	
	if( validarMontoPorRecuperar ){
		
		if(montoPorCobrarCia <= 0){
			mostrarMensajeInformativo('No se puede elaborar una carta con un monto por cobrar menor o igual a 0 ' , '20');
			return false;
		}
	}
	
	return true;
}


function bloquearDatosCIA(bandera){
	if(bandera==true){
		jQuery("#causasNoElaboracion").attr('disabled','disabled');
		jQuery("#estatusCartaLis").attr('disabled','disabled');
		jQuery("#comentarios").attr('readonly', 'readonly');
		jQuery("#numSiniestroCia").attr('readonly', 'readonly');
		jQuery("#polizaCia").attr('readonly', 'readonly');
		jQuery("#montoGAGrua").attr('readonly', 'readonly');	
		jQuery("#conceptoCarta").attr('readonly', 'readonly');	
		jQuery("#causasNoElaboracion").addClass('disabledgray');
		jQuery("#comentarios").addClass('disabledgray');
		jQuery("#montoGAGrua").addClass('disabledgray');
		jQuery("#conceptoCarta").addClass('disabledgray');
		jQuery("#estatusCartaLis").addClass('disabledgray');
		jQuery("#numSiniestroCia").addClass('disabledgray');
		jQuery("#polizaCia").addClass('disabledgray');
		
	}else	{
		jQuery("#causasNoElaboracion").removeAttr("disabled");
		jQuery("#causasNoElaboracion").removeClass("disabledgray");
		jQuery("#comentarios").removeClass("disabledgray");
		jQuery("#montoGAGrua").removeClass("disabledgray");
		jQuery("#conceptoCarta").removeClass("disabledgray");
		jQuery("#estatusCartaLis").removeClass("disabledgray");
		jQuery("#numSiniestroCia").removeClass("disabledgray");
		jQuery("#polizaCia").removeClass("disabledgray");
	}
}
function elaborarCarta(){
	
	removeCurrencyFormatOnTxtInput();	
	if( validaGuardarRecuperacion(true)){
		if(confirm("¿Al generar la Carta ya no se podrán editar los datos, ¿está de acuerdo en generar la Carta?")){
	  		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/elaborarCarta.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize(),targetWorkArea,null);
	  		initCurrencyFormatOnTxtInput();
		}
	}
	
}

function reenviarImprimeCarta(){
	var tipoPase  = jQuery("#lstTipoPaseCia").val();
	var recuperacionId   = jQuery("#recuperacionId").val();
	if(null!=tipoPase && tipoPase!=""){
		var resultado = prepararImpresionCarta();
		var validacion =jQuery("#impresionPreparada").val();
		if(validacion == 'true'){
			if(tipoPase=="ORIGINAL"){
				var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/imprimirCarta.action?tipoPaseCia=ORIGINAL"+ '&recuperacionId='+recuperacionId;
//				parent.document.getElementById('urlImprimir').value = url;
				window.open(url,"CartaCompaniaOriginal");
			}else if(tipoPase=="COPIA"){
				var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/imprimirCarta.action?tipoPaseCia=COPIA"+ '&recuperacionId='+recuperacionId;
//				parent.document.getElementById('urlImprimir').value = url;
				window.open(url,"CartaCompaniaCopia");
			}
		}else{
			mostrarVentanaMensaje("20","No se pudo actualizar la informacion de la carta para la impresi\u00F3n",null);
		}
	}else{
		mostrarVentanaMensaje("20","Debe seleccionar el tipo de pase",null);
	}
}
function imprimirCartaCia(){
	var url = jQuery("#urlImprimir").val();
	parent.window.open(url, "CartaCompaniaOriginal");
}

function cerrarVentanaTipoPase(){
	parent.cerrarVentanaModal("vm_imprimeRecuperacionCia","actualizarAlCerrarImpresion();");
}

function cerrarVentanaImpresionRedoc(){
	parent.cerrarVentanaModal("vm_imprimeRecuperacionCiaRedoc", "actualizaAlCerrarImpresionRedoc()");
}

function actualizarAlCerrarImpresion(){
	var soloConsulta = jQuery("#soloConsulta").val();
	var esNuevoRegistro = jQuery("#esNuevoRegistro").val();
	var editarDatosGenericos = jQuery("#editarDatosGenericos").val();
	var cartaEstatus=jQuery("#estatusCartaLis").val() ;	
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action"
		+ "?soloConsulta=" + soloConsulta + "&esNuevoRegistro=" + esNuevoRegistro 
		+ "&recuperacionId="+jQuery("#recuperacionId").val()+ "&tipoMostrar=U&editarDatosGenericos=" + editarDatosGenericos;
	sendRequestJQ(null,url,targetWorkArea,null);
}

function actualizaAlCerrarImpresionRedoc(){
	var soloConsulta = jQuery("#soloConsulta").val();
	var esNuevoRegistro = jQuery("#esNuevoRegistro").val();
	var editarDatosGenericos = jQuery("#editarDatosGenericos").val();
	var cartaEstatus=jQuery("#estatusCartaLis").val() ;	
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action"
		+ "?soloConsulta=" + soloConsulta + "&esNuevoRegistro=" + esNuevoRegistro 
		+ "&recuperacionId="+jQuery("#recuperacionId").val()+ "&tipoMostrar=U&editarDatosGenericos=" + editarDatosGenericos;
	sendRequestJQ(null,url,targetWorkArea,null);
}

function imprimirCartaCiaCopia(){
	var idRecuperacion   =jQuery("#h_idRecuperacion").val();
	window.open(url, "CartaCompaniaCopia");
}
function imprimirCarta(){ 
	var idRecuperacion  = jQuery("#h_idRecuperacion").val();	
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/imprimeCarta.action?recuperacionId="+idRecuperacion;
	mostrarVentanaModal("vm_imprimeRecuperacionCia", 'Impresion', 200, 120, 350, 150,url,"actualizarAlCerrarImpresion();");
}

function imprimirCartaRedoc(){
	var idRecuperacion  = jQuery("#h_idRecuperacion").val();	
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarImprimeCartaRedoc.action?recuperacionId="+idRecuperacion;
	mostrarVentanaModal("vm_imprimeRecuperacionCiaRedoc", 'Impresion', 200, 120, 350, 200,url,"actualizaAlCerrarImpresionRedoc();");
}

function generarImpresionCartaRedoc(){
	var idRecuperacion  = jQuery("#recuperacionId").val();	
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/imprimeCartaRedoc.action?recuperacionId="+idRecuperacion;
	window.open(url, "CartaCompaniaRedoc");
}

function reimprimirCarta(idCartaCia){
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/reimprimirCarta.action?cartaCia.id="+idCartaCia;
	window.open(url, "CartaCompaniaRedoc");
}

function prepararImpresionCarta(){
	var tipoPase  = jQuery("#lstTipoPaseCia").val();
	var recuperacionId   = jQuery("#recuperacionId").val();
	if(tipoPase && recuperacionId){
		jQuery.ajax({
            url: '/MidasWeb/siniestros/recuperacion/recuperacionCia/prepararImpresion.action?recuperacionId='+recuperacionId+"&tipoPaseCia="+tipoPase,
            dataType: 'json',
            async:false,
            type:"POST",
            data: null,
            success: function(json){
            	 var result = json.impresionPreparada;
            	 jQuery("#impresionPreparada").val(result);
            	 if(result){
            		 return true;
            	 }else{
            		 return false;
            	 }
            }
      });
	}else{
		mostrarVentanaMensaje("20","No se pudo recuperar el tipo de pase o la id de la recuperaci\u00F3n",null);
		return false;
	}
}
function registarEntregaCarta(){ 
	var idRecuperacion  = jQuery("#h_idRecuperacion").val();	
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/entregaCarta.action?recuperacionId="+idRecuperacion;
		mostrarVentanaModal("vm_EntregaRecuperacionCia", 'Registrar Entrega de Carta', 200, 120, 300, 295,url,null);
}

function cerrarEntregaCIA (){
	parent.cerrarVentanaModal("vm_EntregaRecuperacionCia",true);
}

function reenviarEntregaCia (){	
	var url = '/MidasWeb/siniestros/recuperacion/recuperacionCia/entregaCarta.action?recuperacionId=' +  jQuery("#recuperacionId").val() + 
    "&fechaAcuse=" + jQuery("#fechaAcuseCia").val();
	console.log('URL  : '+url);
	parent.document.getElementById('urlRedirect').value = url;
	fechaAcuseCia = jQuery("#fechaAcuseCia").val();
	var recuperacionId   = jQuery("#recuperacionId").val();	
	if(null!=fechaAcuseCia && fechaAcuseCia!='' && fechaAcuseCia!='null'  &&  null!= recuperacionId  && recuperacionId!=''  &&  recuperacionId!='null'){
		 parent.cerrarVentanaModal("vm_EntregaRecuperacionCia","entregarCartaCia();");
	}
}

function entregarCartaCia(){	
	var url  = jQuery("#urlRedirect").val();	
	sendRequestJQ(null,url,targetWorkArea,null);
	
}
//------------REDOCUMENTACION 


function iniRedocumentacion(){
	buscarRedocumentaciones();
	var tipo = jQuery("#lstTipoRedocumentacion").val();		
	var lstEstatusCartaRedoc = jQuery("#lstEstatusCartaRedoc").val();	
	var tipoMostrar=jQuery("#tipoMostrar").val() ;	
	changeTipoRedocumentacion();
	if( tipoMostrar=='R' || tipoMostrar == 'C'){
		bloquearRedocumentacion(true);
		
		jQuery(".setConsulta").each( function(){
			jQuery(this).attr('disabled','disabled');	
		});

	}else if( lstEstatusCartaRedoc=='ENTREGADO' || lstEstatusCartaRedoc=='POR_REDOC'  ||   lstEstatusCartaRedoc=='RECHAZADO' ||   lstEstatusCartaRedoc=='REGISTRADO'   ){
		bloquearRedocumentacion(false);
	}else{
		bloquearRedocumentacion(true);
	}
	
	
}//   
function bloquearRedocumentacion(bandera){
	if(bandera==true){
		jQuery("#rechazoConsulta").show();
		jQuery("#rechazoCaptura").hide();
		jQuery("#fechaRechazoConsulta").attr('readonly', 'readonly');
		jQuery("#montoExclusion").attr('readonly', 'readonly');
		jQuery("#montoRegistado").attr('readonly', 'readonly');
		jQuery("#nuevoMontoRegistrar").attr('readonly', 'readonly');
		jQuery("#comentariosRedoc").attr('readonly', 'readonly');	
		
	}else	{
		jQuery("#rechazoConsulta").hide();
		jQuery("#rechazoCaptura").show();
		jQuery("#fechaRechazoCaptura").removeAttr("readonly");
		jQuery("#montoExclusion").removeAttr("readonly");
		jQuery("#montoRegistado").attr('readonly', 'readonly');
		jQuery("#nuevoMontoRegistrar").removeAttr("readonly");
		jQuery("#nuevoMontoRegistrar").attr('readonly', 'readonly');
		jQuery("#comentariosRedoc").removeAttr("readonly");
	}
}

function changeTipoRedocumentacion(){
	var tipo = jQuery("#lstTipoRedocumentacion").val();	
	if(tipo=='EXCLUSION'){
		jQuery("#rechazoConsulta").show();
		jQuery("#rechazoCaptura").hide();
		jQuery("#lstCausaRechazo").attr('disabled','disabled');
		jQuery("#lstTipoRechazoRedocumentacion").attr('disabled','disabled');
		jQuery("#fechaRechazoConsulta").attr('disabled','disabled');
		jQuery("#lstCausaRechazo").val("");
		jQuery("#lstTipoRechazoRedocumentacion").val("");
		jQuery("#fechaRechazoCaptura").val("");
		jQuery("#fechaRechazoConsulta").val("");
		
		jQuery("#lstMotivoExclusion").removeAttr("disabled");
		jQuery("#montoExclusion").removeAttr("disabled");
		jQuery("#montoRegistado").removeAttr("disabled");
		jQuery("#nuevoMontoRegistrar").removeAttr("disabled");
		
	}else if (tipo=='RECHAZO')	{
		jQuery("#rechazoConsulta").hide();
		jQuery("#rechazoCaptura").show();
		jQuery("#lstCausaRechazo").removeAttr("disabled");
		jQuery("#lstTipoRechazoRedocumentacion").removeAttr("disabled");
		jQuery("#fechaRechazoCaptura").removeAttr("disabled");
		
		jQuery("#lstMotivoExclusion").attr('disabled','disabled');
		jQuery("#montoExclusion").attr('disabled','disabled');
		jQuery("#montoRegistado").attr('disabled','disabled');
		jQuery("#nuevoMontoRegistrar").attr('disabled','disabled');
		jQuery("#lstMotivoExclusion").val("");		
		jQuery("#montoExclusion").val("");	
		jQuery("#nuevoMontoRegistrar").val(jQuery("#montoRegistado").val());	
		
	}else{
		jQuery("#rechazoConsulta").show();
		jQuery("#rechazoCaptura").hide();
		jQuery("#lstCausaRechazo").attr('disabled','disabled');
		jQuery("#lstTipoRechazoRedocumentacion").attr('disabled','disabled');
		jQuery("#fechaRechazoConsulta").attr('disabled','disabled');
		jQuery("#lstMotivoExclusion").attr('disabled','disabled');
		jQuery("#montoExclusion").attr('disabled','disabled');
		jQuery("#montoRegistado").attr('disabled','disabled');
		jQuery("#nuevoMontoRegistrar").attr('disabled','disabled');
	}
}

function elaborarCartaRedoc(){
	if (validaGuardarRedocumentacion()){
		if(confirm("¿Está de acuerdo en redocumentar la Carta?")){
			removeCurrencyFormatOnTxtInput();	
	  		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/elaborarCartaRedoc.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize(),targetWorkArea,null);
	  		initCurrencyFormatOnTxtInput();
		}
	}
		
}


function validaGuardarRedocumentacion (){
	var tipo = jQuery("#lstTipoRedocumentacion").val();	
	removeCurrencyFormatOnTxtInput();
	if(tipo=='RECHAZO'){
		var lstEstatusCartaRedoc = jQuery("#lstEstatusCartaRedoc").val();	
		/*if(lstEstatusCartaRedoc!='ENTREGADO'){
			mostrarMensajeInformativo('El estatus de la carta es diferente de Entregado' , '20');
			return false;
		}*/
		var lstCausaRechazo = jQuery("#lstCausaRechazo").val();
		if(null==lstCausaRechazo || lstCausaRechazo==""){
			mostrarMensajeInformativo('Seleccione una causa de Rechazo ' , '20');
			return false;
		}
		
		var lstTipoRechazoRedocumentacion = jQuery("#lstTipoRechazoRedocumentacion").val();
		if(null==lstTipoRechazoRedocumentacion || lstTipoRechazoRedocumentacion==""){
			mostrarMensajeInformativo('Seleccione Tipo de Rechazo ' , '20');
			return false;
		}
		
		var fechaRechazo = jQuery("#fechaRechazoCaptura").val();
		if(null==fechaRechazo || fechaRechazo==""){
			mostrarMensajeInformativo('Capture Fecha de Rechazo ' , '20');
			return false;
		}
		return true;

		
	}else if (tipo=='EXCLUSION'){
		var lstEstatusCartaRedoc = jQuery("#lstEstatusCartaRedoc").val();	
		/*if(lstEstatusCartaRedoc!='ENTREGADO'){
			mostrarMensajeInformativo('El estatus de la carta es diferente de Entregado' , '20');
			return false;
		}*/
		
		var lstMotivoExclusion = jQuery("#lstMotivoExclusion").val();
		if(null==lstMotivoExclusion || lstMotivoExclusion==""){
			mostrarMensajeInformativo('Seleccione Motivo Exclusion ' , '20');
			return false;
		}
		
		var montoExclusion = jQuery("#montoExclusion").val();
		if(null==montoExclusion || montoExclusion==""){
			mostrarMensajeInformativo('Capture Monto Exclusion ' , '20');
			return false;
		}
		
		var montoRegistado = jQuery("#montoRegistado").val();

		
		if(   parseFloat(montoExclusion) >=   parseFloat(montoRegistado) ){
			mostrarMensajeInformativo('Monto Exclusion debe ser menor a Monto Registrado ' , '20');
			return false;
		}
		return true;
	}else{
		mostrarMensajeInformativo('Seleccione tipo de Redocumentacion RECHAZO/EXCLUSION' , '20');
		return false;
	}
		initCurrencyFormatOnTxtInput();
}

function calcularMontoRecuperar(){
	  removeCurrencyFormatOnTxtInput();	
	  var lstEstatusCartaRedoc = jQuery("#lstEstatusCartaRedoc").val();	
	  if( lstEstatusCartaRedoc=='POR_REDOC'  ||  lstEstatusCartaRedoc=='REGISTRADO'   ||  lstEstatusCartaRedoc=='ENTREGADO' ){
		  var montoExclusion = jQuery("#montoExclusion").val();	
			var montoRegistado = jQuery("#montoRegistado").val();	
			if(null==montoExclusion|| montoExclusion==""){
				 jQuery("#montoExclusion").val(0);	
				 montoExclusion = 0;
			}
			var total = parseFloat(montoRegistado)-parseFloat(montoExclusion);
			jQuery("#nuevoMontoRegistrar").val(total);			
		}	  

	  
	
    initCurrencyFormatOnTxtInput();
}

function enableSeguimientoTab(flag){
	if(flag){
		jQuery("#fechaUsuarioDiv").hide();
		jQuery("#comentarioDiv").hide();
		jQuery("#guardarSeguimiento").hide();
	}else{
		jQuery("#fechaUsuarioDiv").show();
		jQuery("#comentarioDiv").show();
		jQuery("#guardarSeguimiento").show();
	}
}

function initSeguimientoTab(){	
	console.log("solo consulta: "+jQuery("#soloConsulta").val());
	var soloConsulta = jQuery("#soloConsulta").val();
	if(soloConsulta!= 'true'){
		enableSeguimientoTab(false);
	}else{
		enableSeguimientoTab(true);
	}
	var recuperacionId = jQuery("#h_idRecuperacion").val();	
	var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarSeguimiento.action?recuperacionId="+recuperacionId;
	var data = "";
	console.log("antes ajax");
	jQuery.ajax({
        url: ruta,
        dataType: 'json',
        async:false,
        type:"POST",
        data: data,
        success: function(json){
        	jQuery("#fechaSeguimiento").val(json.creationDateTxt);
        	jQuery("#usuario").val(json.userName);
        	jQuery("#userID").val(json.currentUser);
        }
  });
	console.log("despues ajax");
 
}

function buscarSeguimientoCia(){
	 document.getElementById("pagingAreaS").innerHTML = '';
	 document.getElementById("infoAreaS").innerHTML = '';
	 seguimientosCiaGrid = new dhtmlXGridObject('seguimientoCiaGrid');
	 var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/obtenerSeguimientos.action?recuperacionId='+jQuery("#h_idRecuperacion").val();
	 seguimientosCiaGrid.load( ruta);
}

function guardarSeguimiento(){
	if(confirm(" ¿Esta seguro de registrar el Comentario en la Bitacora de Seguimiento? ")){
		var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/guardarSeguimiento.action";
	
		jQuery.ajax({
			url: ruta,
			dataType: 'json',
			async:false,
			type:"POST",
			data: { 
				'recuperacionId' : jQuery("#h_idRecuperacion").val(),
				'currentUser': jQuery("#userID").val(),
				'comentarioSeguimiento': jQuery("#comentario").val()
			},

			success: function(json){
				console.log("Refresh grid seguimiento");
				jQuery("#comentario").val("");
				buscarSeguimientoCia();
			}
		});
	}
}

//joksrc
function capturarFechaAcuseCartasMigradas(){
	var codigoDeUsuarioCarta = jQuery("#codigoUusarioPCartasMigradas").val();
	var idRecuperacion  = jQuery("#h_idRecuperacion").val();
	var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/capturaFechaAcuseCartasMigradas.action?recuperacionId="+idRecuperacion+"&codeUserCartasMigradas="+codigoDeUsuarioCarta ;
	mostrarVentanaModal("vm_FechaAcuseCartasMigradas", 'Registrar Fecha Acuse Cartas Migradas', 200, 120, 300, 300,url,null);

}

//joksrc
function cerrarVentanaFechaAcuseCartMig(){
	parent.cerrarVentanaModal("vm_FechaAcuseCartasMigradas",true);
}

//joksrc
function registrarNuevaFechaAcuseCartMig(){
	var fechaAcuseNueva = jQuery("#fechaAcuseCia").val();
	var validacionFecha = "CARTAMIGRADA";
	var recId = jQuery('#recuperacionId').val();

	if(fechaAcuseNueva == null || fechaAcuseNueva ==""){
		alert('Capture la fecha y presione continuar');
	}else{			
		parent.sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/guardarRedocumentacion.action?'+ jQuery(parent.document.contenedorRecuperacionCIAForm).serialize()+"&fechaAcuseCartasMigradas="+fechaAcuseNueva+"&validaExclusionRechazo="+validacionFecha,parent.targetWorkArea,parent.cerrarVentanaModal("vm_FechaAcuseCartasMigradas",true));
	}
	
}

function  guardarRedocumentacion(){
	var validacionFecha = "n";
	var codigoUsuario = jQuery("#codigoUusarioPCartasMigradas").val();
	
	if (validaGuardarRedocumentacion() && codigoUsuario != 'MIGSINIE'){
		var tipo = jQuery("#lstTipoRedocumentacion").val();	
		if(tipo=='RECHAZO'){
			if(confirm("¿Está seguro de rechazar la Carta?")){
				
				if(confirm("¿Desea copiar la fecha de acuse?")){
					validacionFecha = "s";
				}
				blockPage();
		  		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/guardarRedocumentacion.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize()+"&validaExclusionRechazo="+validacionFecha,targetWorkArea,null);
			}
		}else{
			if(confirm("¿Está seguro de registrar una Exclusión a la Carta?")){
				
				if(confirm("¿Desea copiar la fecha de acuse?")){
					validacionFecha = "s";
				}
				
				blockPage();
		  		sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/recuperacionCia/guardarRedocumentacion.action?'+ jQuery(document.contenedorRecuperacionCIAForm).serialize()+"&validaExclusionRechazo="+validacionFecha,targetWorkArea,null);
			}
		}
	}else if(validaGuardarRedocumentacion() && codigoUsuario == 'MIGSINIE'){
		var tipoCartMig = jQuery("#lstTipoRedocumentacion").val();
		
		if(tipoCartMig=='RECHAZO'){
			if(confirm("Carta Migrada: ¿Está seguro de registrar un Rechazo a esta Carta?")){
				capturarFechaAcuseCartasMigradas();
			}
		}else{
			if(confirm("Carta Migrada: ¿Está seguro de registrar una Exclusión a esta Carta?")){
				capturarFechaAcuseCartasMigradas();
				//registrarNuevaFechaAcuseCartMig();
			}
		}
	}
}


function buscarRedocumentaciones(){
	var recuperacionId =   jQuery("#recuperacionId").val();
	var soloConsulta =  jQuery("#soloConsulta").val();	
	
	document.getElementById("pagingAreaR").innerHTML = '';
 	document.getElementById("infoAreaR").innerHTML = '';
 	var listadoRedocumentaGrid = new dhtmlXGridObject('redocumentaGrid');
 	listadoRedocumentaGrid.attachEvent("onXLS", function(grid){	
 		blockPage();
     });
 	listadoRedocumentaGrid.attachEvent("onXLE", function(grid){		
 		unblockPage();
     });
	var ruta= '/MidasWeb/siniestros/recuperacion/recuperacionCia/obtenerCartasRedoc.action?soloConsulta='+soloConsulta+'&recuperacionId='+recuperacionId  ;
	listadoRedocumentaGrid.load( ruta);
}


function imprimeCartaRedoc(){ 
	var idRecuperacion  = jQuery("#h_idRecuperacion").val();	
		var url = "/MidasWeb/siniestros/recuperacion/recuperacionCia/imprimeCartaRedoc.action?recuperacionId="+idRecuperacion;
  		sendRequestJQ(null,url,targetWorkArea,null);
}

/////

/*
function cerrarComplemento(){
	parent.cerrarVentanaModal("vm_registrarComplemento",true);
}

function continuarComplemento(){
	var recuperacionId  =	jQuery("#recuperacionId").val();
	parent.cerrarVentanaModal("vm_registrarComplemento",'registrarComplemento();');
}	
*/

function validarComplemento(){
	if(jQuery("#tieneComplemento").val()){
		 var ruta = "/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarAlerta.action?url=NUEVO";
			mostrarVentanaModal("vm_alertaRecuperacionCIA", 'Alerta Recuperaci\u00F3n CIA', 550, 310, 1000, 550,ruta,null); 
	}
}

function validarRegistroComplemento()
{
	if(jQuery("#tipoMostrar").val()== 'U' 
		&& jQuery("#tieneComplemento").val()== 'true')
	{
		mostrarRegistrarComplemento();
	}
}

function mostrarRegistrarComplemento()
{
	var url="/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarRegistrarComplemento.action?";
	mostrarVentanaModal("mostrarRegistrarComplemento", 'Recuperaci\u00F3n de Compa\u00F1ias', 100, 200, 700, 300, url);
	popUpRegistroCompl = mainDhxWindow.window('mostrarRegistrarComplemento');
}

function registrarRecuperacion(url, refrescarPantalla)
{	
	jQuery.ajax({
	"url" : url,
	"async": false,
	"dataType" : "text json",
	"success": function(data) {
		
		popUpRegistroCompl.close();
		
		if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
		{
			mostrarMensajeInformativo(JSON.parse(data).mensaje, '30');
			
		}else
		{
			mostrarMensajeInformativo(JSON.parse(data).mensaje, '30');
			if(refrescarPantalla)
			{				
				sendRequestJQ(null,"/MidasWeb/siniestros/recuperacion/recuperacionCia/mostrarContenedor.action?soloConsulta=true&recuperacionId=" 
						+ jQuery('#recuperacionId').val() + "&tipoMostrar=R " ,targetWorkArea,null);
			}						
		}			
	 }
	});
}

