/**
 * Guarda la informacion capturada en la pantalla de formato fechas PT, en la seccion de primas pendientes
 * @param idOrdenCompra
 */
function guardarPrimasPendientes(){
	removeCurrencyFormatOnTxtInput();
	var formParams = jQuery(document.formatoFechaForm).serialize();
	var url = guardarPrimasPendientesPath + '?' + formParams;
	sendRequestJQ(null, url, targetWorkArea, null);
}

/**
 * Exportar PDF de la pantalla FormatoFechasPT
 */
function imprimirFormatoFechasPT(){
	removeCurrencyFormatOnTxtInput();
	if( jQuery("#h_consulta").val() == 1 ){
		setHabilitaFormatoFechasPT();
	}
	
	var url = imprimirFormatoFechasPTPath + '?idOrdenCompra=' + jQuery("#h_idOrdenCompra").val();
	if( jQuery("#h_consulta").val() == 1  ){
		setHabilitaFormatoFechasPT();
	}
	window.open(url, "Formato_Fechas_Perdida_Total");
	if( jQuery("#h_consulta").val() == 1 ){
		setConsultaFormatoFechasPT();
	}
	initCurrencyFormatOnTxtInput();
}

function setConsultaFormatoFechasPT(){
	jQuery(".setDisabled").attr("disabled","true");
	jQuery(".setReadOnly").attr("readOnly","true");
	jQuery("#btn_guardar").remove();
}

function setHabilitaFormatoFechasPT(){
	jQuery(".setDisabled").attr("disabled","");
}

function cerrarFormatoFechasPT(){
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	var url = cerrarFormatoFechasPTPath + "?filtroPT.idIndemnizacion=" + idIndemnizacion;
	sendRequestJQ(null, url, targetWorkArea, 'buscarPerdidasTotales(false);');
}