/**
 * @author eftregue
 */

var pasesAtencionGrid;

var mostrarPasePath = '/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/mostrarPase.action';

var vieneDeReportes = false;

function getPasesAtencion(load){
	pasesAtencionGrid = new dhtmlXGridObject('siniestrosVehiculoGrid');
	var url = "/MidasWeb/siniestros/cabina/reportecabina/buscarPasesDeAtencion.action";

		pasesAtencionGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicadorListadoPase");
	    });
		pasesAtencionGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicadorListadoPase');
	    });
		
		if (load) {
			url+= "?" + jQuery("#formPasesAtencion").serialize();
		} 
		console.log("getPasesAtencion URL : "+url);
		pasesAtencionGrid.load( url  );
}


function exportarExcel(){
	var validador = validarListaExportar();
	
	if(validador == true){
		var params = jQuery("#formPasesAtencion").serialize();
		var url="/MidasWeb/siniestros/cabina/reportecabina/exportarPaseAtencion.action?" + params ;
		window.open(url, "Excel_Siniestros");
	}else{
		alert("Se requiere realizar una búsqueda para exportar a Excel");
	}
	
}

//joksrc funcion nueva
function obtenerFormatoFechaYdias(fechaString){
	//formatear fecha de javascript proveniente de datepicker
	var fechaDividida = fechaString.split("/");
	var fechaStringFormat = fechaDividida[1]+"/"+fechaDividida[0]+"/"+fechaDividida[2];
	//convertir a date
	var fechaDateFormat = new Date(fechaStringFormat);
	//convertir a dias la fecha nueva ya formateada
	var minutes = 1000 * 60;
	var hours = minutes * 60;
	var days = hours * 24;
	var years = days * 365;
	
	var dias = Math.round(fechaDateFormat/days);
	
	
	
	return dias;
}

function validarListaExportar(){
	var numRep = document.getElementById('txfNumRep').value;
	var numSin = document.getElementById('txfNumSini').value;
	var numFol = document.getElementById('txfNumFol').value;
	var cmbTPA = document.getElementById('tipoPasesAtencion').value;
	var cmbCOB = document.getElementById('tipoPases').value;
	var fechaIni = document.getElementById('fechaInicial').value;
	var fechaFin = document.getElementById('fechaFinal').value;

	var salida="";
	var a = new Array();
	a[0] = numRep;
	a[1] = numSin;
	a[2] = numFol;
	a[3] = cmbTPA;
	a[4] = cmbCOB;
	a[5] = fechaIni;
	a[6] = fechaFin;
	
	var contador=0;
	
	for (x=0; x< a.length; x++){
		if(a[x] != "")
		{
			contador++;
		}	
	}
	if(contador > 1)
	{
		//validar que al realizar búsqueda por fecha, sean capturadas las fechas inicial y terminal.
		if( (a[5] != "" && a[6] == "") || (a[5] == "" && a[6] != "") ){
			salida = false;
			//si las dos fechas no están vacías, comprobar que la resta de fechas no superen los tres meses
		}else if((a[5] != "") && (a[6] != "")){
			var diasFechaIni = obtenerFormatoFechaYdias(a[5]);
			var diasFechaFin = obtenerFormatoFechaYdias(a[6]);
			var diasPermitidos = parseInt(diasFechaFin) - parseInt(diasFechaIni);
			//si los días no superan los 3 meses entonces si entra
			if(diasPermitidos < 94){
				salida = true;	
			}else{
				salida = false;
			}
		}else{
			//true para avanzar, false para pruebas
			salida = true;
		}
		
		//salida = true;
	}else{
		salida = false;
	}
	return salida;
}

function validarDosCampos(){
	
	var numRep = document.getElementById('txfNumRep').value;
	var numSin = document.getElementById('txfNumSini').value;
	var numFol = document.getElementById('txfNumFol').value;
	var cmbTPA = document.getElementById('tipoPasesAtencion').value;
	var cmbCOB = document.getElementById('tipoPases').value;
	var fechaIni = document.getElementById('fechaInicial').value;
	var fechaFin = document.getElementById('fechaFinal').value;

	var a = new Array();
	a[0] = numRep;
	a[1] = numSin;
	a[2] = numFol;
	a[3] = cmbTPA;
	a[4] = cmbCOB;
	a[5] = fechaIni;
	a[6] = fechaFin;
	
	var contador=0;
	
	for (x=0; x< a.length; x++){
		if(a[x] != "")
		{
			contador++;
		}	
	}
	if(contador > 1)
	{
		//validar que al realizar búsqueda por fecha, sean capturadas las fechas inicial y terminal.
		if( (a[5] != "" && a[6] == "") || (a[5] == "" && a[6] != "") ){
			mostrarMensajeInformativo('No se permite buscar con una sola fecha. Debe capturar ambas.', '10');
			//si las dos fechas no están vacías, comprobar que la resta de fechas no superen los tres meses
		}else if((a[5] != "") && (a[6] != "")){
			var diasFechaIni = obtenerFormatoFechaYdias(a[5]);
			var diasFechaFin = obtenerFormatoFechaYdias(a[6]);
			var diasPermitidos = parseInt(diasFechaFin) - parseInt(diasFechaIni);
			//si los días no superan los 3 meses entonces si entra
			if(diasPermitidos < 94){
				getPasesAtencion(true);	
			}else{
				mostrarMensajeInformativo('No se permite buscar por un rango de fechas mayor a tres meses.', '10');
			}
		}else{
			//true para avanzar, false para pruebas
			getPasesAtencion(true);
		}
	}
	else
	{
		mostrarMensajeInformativo('Se requiere capturar dos campos.', '20');

	}
}

function buscarPaseAtencion(){
	validarDosCampos();	
}

function mostrarEstimacion( soloConsulta, idCoberturaReporteCabina, claveTipoCalculo, tipoEstimacion, reporteCabinaId , idEstimacion){

	var pasesAtencionInt;
	var funcionCallback = null;
	var isListadoPases = jQuery("#h_isListadoPases").val();

	if( tipoEstimacion == "GME" || tipoEstimacion == "GMC" || tipoEstimacion == "RCB" || tipoEstimacion == "RCP" || tipoEstimacion == "RCV" || tipoEstimacion == "RCJ" ){
		pasesAtencionInt = 1;
	}else{
		pasesAtencionInt = 0;
	}
	
	var url = mostrarEstimacionPath + '?idCoberturaReporteCabina=' + idCoberturaReporteCabina + 
    "&tipoCalculo=" + claveTipoCalculo + 
	"&tipoEstimacion=" + tipoEstimacion +
	"&reporteCabinaId=" + reporteCabinaId + 
	"&pasesAtencionInt=" + pasesAtencionInt + 
	"&soloConsulta=" + soloConsulta;

	if( tipoEstimacion == "ESD" ){
		url = mostrarPasePath + '?idCoberturaReporteCabina=' + idCoberturaReporteCabina + 
	    "&tipoCalculo=" + claveTipoCalculo + 
		"&tipoEstimacion=" + tipoEstimacion +
		"&reporteCabinaId=" + reporteCabinaId + 
		"&pasesAtencionInt=" + pasesAtencionInt + 
		"&idPaseDeAtencion=" + idEstimacion +
		"&soloConsulta=" + soloConsulta +
		"&isListadoPases=" + isListadoPases;
		console.log('Solo consulta : '+soloConsulta +' vieneDeReportes: '+vieneDeReportes);
		if(soloConsulta == 1){
			if(vieneDeReportes){
				console.log('Si vieneDeReportes');
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();setConsultaEstimacion();");
			}else{
				console.log('No vieneDeReportes');
				sendRequestJQ(null, url, targetWorkArea, "setReturn();setConsultaEstimacion();");	
			}
			
		}else{
			if(vieneDeReportes){
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();");
			}else{
				sendRequestJQ(null, url, targetWorkArea, "setReturn();");
			}
			
		}
	}else if( tipoEstimacion == "GME" || tipoEstimacion == "GMC" || tipoEstimacion == "RCB" || tipoEstimacion == "RCP" || tipoEstimacion == "RCV" || tipoEstimacion == "RCJ" ){
		url = mostrarPasePath + '?idCoberturaReporteCabina=' + idCoberturaReporteCabina + 
	    "&tipoCalculo=" + claveTipoCalculo + 
		"&tipoEstimacion=" + tipoEstimacion +
		"&reporteCabinaId=" + reporteCabinaId + 
		"&pasesAtencionInt=" + pasesAtencionInt +
		"&idPaseDeAtencion=" + idEstimacion +
		"&soloConsulta=" + soloConsulta +
		"&isListadoPases=" + isListadoPases;
		console.log('Solo consulta : '+soloConsulta +' vieneDeReportes: '+vieneDeReportes);
		if(soloConsulta == 1){
			if(vieneDeReportes){
				console.log('Si vieneDeReportes');
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();setConsultaEstimacion();");
			}else{
				console.log('Si vieneDeReportes');
				sendRequestJQ(null, url, targetWorkArea, "setReturn();setConsultaEstimacion();");	
			}
			
		}else{
			if(vieneDeReportes){
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();");
			}else{
				sendRequestJQ(null, url, targetWorkArea, "setReturn();");
			}
		}
	}else if( tipoEstimacion == "DMA" ){
		if(soloConsulta == 1){
			if(vieneDeReportes){
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();setConsultaEstimacion();");
			}else{
				sendRequestJQ(null, url, targetWorkArea, "setReturn();setConsultaEstimacion();");
			}
		}else{
			if(vieneDeReportes){
				sendRequestJQ(null, url, targetWorkArea, "setReturnToReport();");
			}else{
				sendRequestJQ(null, url, targetWorkArea, "setReturn();");
			}
		}
		
	}
}

//function consultarPaseAtencion( id , tipoCalculo , tipoEstimacion , idReporteCabina , idEst ){
//	var url = mostrarPasePath + '?idCoberturaReporteCabina=' + id + 
//    "&tipoCalculo=" + tipoCalculo + 
//	"&tipoEstimacion=" + tipoEstimacion	+
//	"&reporteCabinaId=" + idReporteCabina +
//	"&idPaseDeAtencion=" + idEst;
//	parent.document.getElementById('urlRedirect').value = url;
//	redireccionaConsulta();
//}
//
//function editarPaseAtencion( id , tipoCalculo , tipoEstimacion , idReporteCabina , idEst ){
//	var url = mostrarPasePath + '?idCoberturaReporteCabina=' + id + 
//    "&tipoCalculo=" + tipoCalculo + 
//	"&tipoEstimacion=" + tipoEstimacion	+
//	"&reporteCabinaId=" + idReporteCabina +
//	"&idPaseDeAtencion=" + idEst;
//	parent.document.getElementById('urlRedirect').value = url;
//	redireccionaEditar();
//}

function imprimirPaseAtencion( id ){
	var url="/MidasWeb/siniestros/cabina/reportecabina/imprimirPaseAtencion.action?idEstimacionCobertura="+id;
	window.open(url, "PaseAtencion");
}

function limpiarFormulario(){
	jQuery(".cleaneable").each(
		function(){
			jQuery(this).val("");
		}
	);
}

function redireccionaEditar(){
	var url = jQuery("#urlRedirect").val();
	sendRequestJQ(null, url, 'contenido', 'setReturn();');
}

function redireccionaConsulta(){
	var url = jQuery("#urlRedirect").val();
	sendRequestJQ(null, url, 'contenido', 'setConsultaEstimacion();setReturn();');
}


function setConsultaEstimacion(){
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
	jQuery(".setNew").removeClass("requerido");
	jQuery(".setNew").removeClass("errorField");
	jQuery("#btn_nuevo").remove();
	jQuery("#btn_guardar").remove();
	try{
		jQuery("#btn_buscar_seguroTercero").remove();
	}catch (e) {
	}
	try{
		jQuery("#btn_buscar_hospital").remove();
	}catch (e) {
	}
	try{
		jQuery("#btn_buscar_medico").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_buscarResponsableReparacion").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_imagenes").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_buscarCompaniaSeguros").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_hospital").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_medico").remove();
	}catch (e) {
	}
	
	try{
	   var tab = document.getElementById("td_pais").getElementsByTagName("TABLE")[0];
		
       var secondTR     = tab.getElementsByTagName("tbody")[0].getElementsByTagName("tr")[1];
       var estadoTD    = secondTR.getElementsByTagName("td")[1];
	   var estadoDIV   = estadoTD.getElementsByTagName("div")[1];
	   var estadoSelect = estadoDIV.getElementsByTagName("select")[0]; 
	   estadoSelect.disabled=true;

       var municipioTD    = secondTR.getElementsByTagName("td")[3];
	   var municipioDIV   = municipioTD.getElementsByTagName("div")[1];
	   var municipioSelect = municipioDIV.getElementsByTagName("select")[0]; 
	   municipioSelect.disabled=true;
	}catch (e) {
	}
	jQuery("#contenido").append("<div id='h_consulta' style='display:none;' >1</div>");
//	jQuery("input").attr('disabled','disabled');
}

function setReturn(){
	
	jQuery("#btn_cerrar").unbind("click");
	jQuery("#btn_cerrar").click(
		function(){
			url="/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoPasesAtencion.action";
			sendRequestJQ(null, url, 'contenido', '');
		}
	);
}

function regresaAListadoDeReporte(){
	var	url="/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action";
	sendRequestJQ(null, url, 'contenido', '');
}