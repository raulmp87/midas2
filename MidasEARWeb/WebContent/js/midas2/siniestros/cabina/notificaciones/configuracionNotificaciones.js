var destinatariosGrid;
var isOtros;
var notificacionesGrid;
var notificacionesProcessor;
var isEditar;

function onChangeProceso(target){
	var idProceso = jQuery("#procesos").val();
	if(idProceso != null  && idProceso != headerValue){		
		dwr.engine.beginBatch();
		listadoService.getMapMovimientosNotificaciones(idProceso,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(target,"");
	}	
	jQuery("#proceso").val(jQuery("#procesos").val());
}

function inicializarListadoConfiguraciones(){	
	cargarListadoConfiguraciones(obtenerConfiguraciones);
}

function cargarListadoConfiguraciones(urlListado){
	blockPage();
	document.getElementById('notificacionesGrid').innerHTML = '';	
	notificacionesGrid = new dhtmlXGridObject("notificacionesGrid");
	notificacionesGrid
	.attachHeader("&nbsp,&nbsp,#select_filter,#select_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp");
	notificacionesGrid.load(urlListado);
	
	var url = asignarEstatusPath;
	notificacionesProcessor = new dataProcessor(url);
	notificacionesProcessor.enableDataNames(true);
	notificacionesProcessor.setTransactionMode("POST");
	notificacionesProcessor.setUpdateMode("cell");
	notificacionesProcessor.attachEvent("onAfterUpdate",inicializarListadoConfiguraciones);
	notificacionesProcessor.init(notificacionesGrid);
	creaGrid();
	unblockPage();
}

function busquedaConfiguracion(){
	blockPage();
	var data=jQuery("#notificacionesForm").serialize();
	
	document.getElementById("notificacionesGrid").innerHTML = '';
	notificacionesGrid = new dhtmlXGridObject("notificacionesGrid");
	var buscarConfiguracionesScreen = true;
	notificacionesGrid.load(busquedaGeneralPath + "?" + data+ "&configuracionNotificacionDTO.esBusquedaPorPantalla=" + buscarConfiguracionesScreen);
	var url = asignarEstatusPath;
	notificacionesProcessor = new dataProcessor(url);
	notificacionesProcessor.enableDataNames(true);
	notificacionesProcessor.setTransactionMode("POST");
	notificacionesProcessor.setUpdateMode("cell");
	notificacionesProcessor.attachEvent("onAfterUpdate",busquedaConfiguracion);
	notificacionesProcessor.init(notificacionesGrid);
	if(destinatariosGrid){
		destinatariosGrid.clearAll();
	}
	unblockPage();
}

function cargarConfiguracionNotificacion(id, editar){
	isEditar = editar;
	blockPage();
	var data="";
	limpiarNotificaciones();
	if(editar){ //Otros
		jQuery("#btnGuardar").show();
		jQuery("#btnAgregar").show();
		jQuery("#btnAdjuntar").show();
		jQuery('#notas').attr('readonly', false);
	}else{
		jQuery("#btnGuardar").hide();
		jQuery("#btnAgregar").hide();
		jQuery("#btnAdjuntar").hide();
		jQuery('#notas').attr('readonly', true);
	}
	jQuery.asyncPostJSON(cargarConfiguracionPath + "?configuracionNotificacion.id=" + id,data,cargarConfiguracionCallBack);
	unblockPage();
}

function creaGrid(){
	destinatariosGrid = new dhtmlXGridObject('destinatariosGrid');
	destinatariosGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	destinatariosGrid.setHeader("ModoEnvio,TipoDestinatario,TipoCorreo,Modo de Envío,Tipo Destinatario,Tipo Correo,Puesto,Nombre,Correo,Acciones");
	destinatariosGrid.setInitWidths("*,*,*,120,250,150,*,250,*,*");
	destinatariosGrid.setColAlign("left,left,left,left,left,left,left,left,left,center")
	destinatariosGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	destinatariosGrid.setColumnHidden(0,true);
	destinatariosGrid.setColumnHidden(1,true);
	destinatariosGrid.setColumnHidden(2,true);
	destinatariosGrid.init();
	destinatariosGrid.setSkin("light");
	destinatariosGrid.clearAll();
}

function cargarConfiguracionCallBack(json){
	blockPage();
	var conf = json.configuracionNotificacion;
	var listDest=conf.destinatariosNotificacion;
	creaGrid();
	var modoEnvio = "";
	var tipoDestinatario = "";
	var tipoCorreo = "";
	var eliminar = "";
	jQuery("#oficina").val(conf.oficina.id);
	jQuery("#procesos").val(conf.procesoCabina.id);
	onChangeProceso('movimientos');	
	jQuery("#movimientos").val('00' + conf.movimientoProceso.id); //El cero es por hack de ordenamiento
	jQuery("#notas").val(conf.notas);
	jQuery("#agenteId").val(conf.agenteId);
	jQuery("#numPoliza").val(conf.numPoliza);
	jQuery("#numInciso").val(conf.numInciso);
	for(j=0;j<listDest.length;j++){	
		jQuery("#modoEnvio > option").each(function() {
		    if(listDest[j].modoEnvio == this.value){
		    	modoEnvio = this.text;
		    }
		});
		jQuery('input').each(function(){
		    var element = jQuery(this);
		    if(element.attr("class") == 'destinatario'){

		        if (element.val() != '' && listDest[j].tipoDestinatario == element.val()) {
		            var label = jQuery("label[for='"+this.id+"']")
		            tipoDestinatario = label.text();
		        }
		    }
		});
		jQuery('input').each(function(){
		    var element = jQuery(this);
		    if(element.attr("class") == 'correos'){

		        if (element.val() != '' && listDest[j].correosEnvio == element.val()) {
		            var label = jQuery("label[for='"+this.id+"']")
		            tipoCorreo = label.text();
		        }
		    }
		});
		if(isEditar){
			eliminar = "<a target='_self' href='javascript:eliminarDestinatario(" + j+ ");'><img src='/MidasWeb/img/icons/ico_eliminar.gif'></a>";
		}
		destinatariosGrid.addRow(j, [listDest[j].modoEnvio,listDest[j].tipoDestinatario,listDest[j].correosEnvio,modoEnvio,tipoDestinatario,tipoCorreo,listDest[j].puesto,listDest[j].nombre,listDest[j].correo,eliminar]);
		modoEnvio = "";
		tipoDestinatario="";
		tipoCorreo = "";
	}		
	jQuery("#configuracionId").val(conf.id);
	unblockPage();
}

function eliminarDestinatario(row){
	destinatariosGrid.deleteRow(destinatariosGrid.getRowId(row));
}

function agregarDestinatario(){
	blockPage();
	valida = validaCamposDestinatario();
	if(valida){
		if(destinatariosGrid == null){
			creaGrid();
		}
		var tipoDestinatario = "";
		var tipoDestinatarioId = "";
		var tipoCorreo = "";
		var tipoCorreoId = "";
		var puesto = "";
		var nombre = "";
		var correo = "";
		var modoEnvio = "";
		var modoEnvioId = jQuery("#modoEnvio").val();
		var row = destinatariosGrid.rowsCol.length;
		jQuery("#modoEnvio > option").each(function() {
		    if(modoEnvioId == this.value){
		    	modoEnvio = this.text;
		    }
		});
		var x = document.getElementsByName('configuracionNotificacionDTO.tipoDestinatarioId');
        for(var k=0;k<x.length;k++){
          if(x[k].checked){
        	  tipoDestinatarioId = x[k].value;
          }
      	}
		jQuery('input').each(function(){
		    var element = jQuery(this);
		    if(element.attr("class") == 'destinatario'){

		        if (element.val() != '' && tipoDestinatarioId == element.val()) {
		            var label = jQuery("label[for='"+this.id+"']")
		            tipoDestinatario = label.text();
		        }
		    }
		});
		var eliminar = "<a target='_self' href='javascript:eliminarDestinatario(" + row + ");'><img src='/MidasWeb/img/icons/ico_eliminar.gif'></a>";
		if (isOtros) {
			puesto = jQuery("#puesto").val();
			nombre = jQuery("#nombre").val();
			correo = jQuery("#correo").val();
		}else{
			var x = document.getElementsByName('configuracionNotificacionDTO.correoEnvio');
	        for(var k=0;k<x.length;k++){
	          if(x[k].checked){
	        	  tipoCorreoId = x[k].value;
	          }
	      	}
			jQuery('input').each(function(){
			    var element = jQuery(this);
			    if(element.attr("class") == 'correos'){

			        if (element.val() != '' && tipoCorreoId == element.val()) {
			            var label = jQuery("label[for='"+this.id+"']")
			            tipoCorreo = label.text();
			        }
			    }
			});
		}
		destinatariosGrid.addRow(row,[modoEnvioId,tipoDestinatarioId,tipoCorreoId,modoEnvio,tipoDestinatario,tipoCorreo,puesto,nombre,correo,eliminar]);
	}
	unblockPage();
}

function validaCamposNotificacion(){	
	var oficinaVal = jQuery('#oficina').val();
	if(oficinaVal == null || oficinaVal[0] == ''){
		mostrarMensajeInformativo('Oficina es requerido y diferente de Seleccione...','10');		
	}
		
	if(jQuery("#procesos").val()==''){
		mostrarMensajeInformativo('Proceso es requerido','10');
		return false;
	}
	if(jQuery("#movimientos").val()==''){
		mostrarMensajeInformativo('Movimiento es requerido','10');
		return false;
	}
	if(jQuery('#agenteId').val() != ''){
		var pattern = /[0-9]{5}/;
		var result = jQuery('#agenteId').val().match(pattern);	
		if(result == null || result == ''){
			mostrarMensajeInformativo('Número de Agente debe cumplir con el siguiente formato: 95423');
			return false;
		}
	}
	if(jQuery('#numInciso').val() != ''){
		var pattern = /\b(^([0-9]*[-][0-9]*)$|(^([0-9]+)((,(?=[0-9]))[0-9]+)*)$)\b/; //ej. 1-20 ó 1,5,8
		var result = jQuery('#numInciso').val().match(pattern);		
		if(result == null || result == ''){
			mostrarMensajeInformativo('Número de Incisos debe cumplir con el siguiente formato: 1-20 ó 1,3,5');
			return false;
		}		
	}
	if(jQuery('#numPoliza').val() != ''){
		var pattern = /\b(^([0-9]{4}[-][0-9]{1,10}[-][0-9]{2})$)\b/; //ej. 3201-155520-00 ó 155520
		var result = jQuery('#numPoliza').val().match(pattern);		
		if(result == null || result == ''){
			mostrarMensajeInformativo('Número de Póliza debe cumplir con el siguiente formato: 3201-155520-00');
			return false;
		}		
	}
	
	
	return true;
}

function validaCamposDestinatario(){
	if(jQuery("#modoEnvio").val()==''){
		mostrarMensajeInformativo('Modo de Envío es requerido','10');
		return false;
	}
	if (jQuery('input[name=configuracionNotificacionDTO.tipoDestinatarioId]:checked').length <= 0) {
		mostrarMensajeInformativo('Tipo destinatario es requerido','10');
		return false;
	}
	if (isOtros) {
		if(jQuery("#puesto").val()==''){
			mostrarMensajeInformativo('El puesto es requerido','10');
			return false;
		}
		if(jQuery("#nombre").val()==''){
			mostrarMensajeInformativo('El nombre es requerido','10');
			return false;
		}
		if(jQuery("#correo").val()==''){
			mostrarMensajeInformativo('El correo es requerido','10');
			return false;
		}
		if ( !validaEmail(jQuery("#correo").val()) ){
			mostrarMensajeInformativo('Email no valido','10');
			return false;
		}
	}else{
		if (jQuery('input[name=configuracionNotificacionDTO.correoEnvio]:checked').length <= 0) {
			mostrarMensajeInformativo('Tipo Correo es requerido','10');
			return false;
		}
	}
	return true;
}

function onChangeTipoDestinatario(val){
	if(val == '4'){ //Otros
		jQuery("#divTipoDestinatarioOtros").css("display","inline");
		jQuery("#divTipoDestinatario").css("display","none");
		isOtros = true;
	}else{
		jQuery("#divTipoDestinatarioOtros").css("display","none");
		jQuery("#divTipoDestinatario").css("display","inline");
		isOtros = false;
	}
}

function guardarConfiguracion(){
	if(validaCamposNotificacion()){
		if(destinatariosGrid.rowsCol.length>0){	
			blockPage();			
			url=guardarConfiguracionPath + "?configuracionNotificacion.procesoCabina.id="+jQuery("#procesos").val();			
			url+="&configuracionNotificacion.movimientoProceso.id="+jQuery("#movimientos").val();
			url+="&configuracionNotificacion.notas="+jQuery("#notas").val();
			url+="&configuracionNotificacion.agenteId="+jQuery("#agenteId").val();
			url+="&configuracionNotificacion.numPoliza="+jQuery("#numPoliza").val();
			url+="&configuracionNotificacion.numInciso="+jQuery("#numInciso").val();
			
			
			//multiple ... para no rehacer la forma en que se manda los parametros al action, solo se agrego esta parte
			var oficinas = jQuery('#oficina').val();						
			for(var i = 0; i < oficinas.length; i++){		
				if(oficinas[i] != null && oficinas[i].length > 0){
					url+="&configuracionNotificacionDTO.oficinasId["+i+"]="+oficinas[i];
				}
			}
			
			var rows = destinatariosGrid.getAllRowIds().split(",");
			
			for(var i = 0; i < rows.length; i++){
				url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].modoEnvio="+destinatariosGrid.cells(rows[i],0).getValue();
				url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].tipoDestinatario="+destinatariosGrid.cells(rows[i],1).getValue();
				if(destinatariosGrid.cells(rows[i],1).getValue()=='4'){ //Otros
					url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].puesto="+destinatariosGrid.cells(rows[i],6).getValue();
					url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].nombre="+destinatariosGrid.cells(rows[i],7).getValue();
					url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].correo="+destinatariosGrid.cells(rows[i],8).getValue();
				}else{
					url+="&configuracionNotificacion.destinatariosNotificacion["+rows[i]+"].correosEnvio="+destinatariosGrid.cells(rows[i],2).getValue();
				}
			}
			if(jQuery("#configuracionId").val()!='' && jQuery("#configuracionId").val()!=null){
				url+="&configuracionNotificacion.id="+jQuery("#configuracionId").val();
			}
			jQuery.asyncPostJSON(url,null,afterSave);
		}else{
			mostrarMensajeInformativo('Es requerido minimo un destinatario','10');
		}
	}
}

function afterSave(json){
	inicializarListadoConfiguraciones();
	mostrarMensajeInformativo(json.mensaje,""+json.tipoMensaje+"");
	limpiarNotificaciones();
}

function limpiarNotificaciones(){
	dwr.util.setValue("configuracionNotificacion.id","");
	jQuery("#puesto").val("");
	jQuery("#nombre").val("");
	jQuery("#correo").val("");
	jQuery("#oficina").val("");
	jQuery("#procesos").val("");
	onChangeProceso('movimientos');
	jQuery("#movimientos").val("");
	jQuery("#modoEnvio").val("");
	jQuery("#notas").val("");
	jQuery("#agenteId").val("");
	jQuery("#numPoliza").val("");
	jQuery("#numInciso").val("");
	jQuery('#notas').attr('readonly', false);
	limpiarRadios();
	if(destinatariosGrid){
		destinatariosGrid.clearAll();
	}
	jQuery("#btnGuardar").show();
	jQuery("#btnAgregar").show();
	jQuery("#btnAdjuntar").show();
}

function limpiarRadios(){
	jQuery('input:radio').each(function(){
	    var element = jQuery(this);
	    element.attr('checked', false);
	});
}

function eliminarConfiguracionNotificacion(id){
	mostrarMensajeConfirm("¿Esta seguro de eliminar esta notificación ?","20","sendRequestEliminar("+id+")",null,null,null);
}

function sendRequestEliminar(id){
	var urlFiltro= eliminarConfiguracionPath + "?configuracionNotificacion.id="+id;
	limpiarNotificaciones();
	cargarListadoConfiguraciones(urlFiltro);
}

function validaEmail(email){
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function ventanaAdjuntos(){
	var configuracionId = jQuery("#configuracionId").val();
	if (configuracionId && configuracionId!= ''){
		mostrarVentanaModal("vm_Adjuntos", "Notificaciones ", 100, 100, 480, 430, mostrarAdjuntos+"?configuracionNotificacion.id="+configuracionId , null);
	}else{
		mostrarMensajeInformativo("Debe seleccionar primero una configuración", "101", null, null);
	}
		
}

function confirmaEliminacionArchivoAdjunto(id){
	eliminarAdjunto(id);
}

function eliminarAdjunto(id){
	var configuracionId = jQuery("#configuracionId").val();
	var urlFiltro= "/MidasWeb/siniestros/cabina/notificaciones/eliminarAdjunto.action?archivoAdjunto.id="+id+"&configuracionNotificacion.id="+configuracionId;
	sendRequestJQ(null, urlFiltro , null, iniciaArchivosAdjuntosGrid);	
}

function afterDelete(json){
	iniciaArchivosAdjuntosGrid()
}

function iniciaArchivosAdjuntosGrid(){
	var configuracionId = jQuery("#configuracionId").val();
	document.getElementById("archivosAdjuntosGrid").innerHTML = '';
	cargaMasivasGrid = new dhtmlXGridObject("archivosAdjuntosGrid");
	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasGrid.load("/MidasWeb/siniestros/cabina/notificaciones/obtenerAdjuntos.action?configuracionNotificacion.id="+ configuracionId);
}

function cerrarAdjuntos(){
	parent.cerrarVentanaModal("vm_Adjuntos",true);
}
