function inicializarValeTaller(){
	var today = new Date(); 
	var dd    = today.getDate(); 
	var mm    = today.getMonth()+1; //Enero is 0
	var yyyy  = today.getFullYear(); 
	var hora  = today.getHours();
	var min   = today.getMinutes();
	var dn  = "AM"
		
	 if ( hora > 12 ) {
	     dn = "PM"
		 hora = Hora - 12
	 }
	 if (Hora == 0){
	   	 hora = 12
	 }
	 
	 /* Si la Hora o los Minutos son Menores o igual a 9, le aÃ±adimos un 0 */
	 if ( hora <= 9 ) {
		 hora = "0" + hora;
	 }
	 if ( min <= 9 ){
		 min = "0" + min;
	 }
	
	jQuery("#t_dia").val( dd );
	jQuery("#t_mes").val( mm );
	jQuery("#t_anio").val( yyyy );
	jQuery('input[name=valeTaller.amPm]:checked', '#valeTallerForm').val( dn );
	
}

function mostrarInfoValeTaller(){
	var tipoVehiculo = jQuery('input[name=valeTaller.tipoVehiculo]:checked', '#valeTallerForm').val();
	var url;
	if( tipoVehiculo == "1" ){		
		url = "/MidasWeb/siniestros/cabina/reporteCabina/valeTaller/obtenerDatosInciso.action";
		redirectVentanaModal('vm_valeTaller', url, document.valeTallerForm);
		jQuery("#nombreAsegurado").show();
		jQuery("#listadoPases").hide();
		jQuery('#terceroId').val('');
	
	} else {
		jQuery("#nombreAsegurado").hide();
		jQuery("#listadoPases").show();
		limpiarDatosVehiculo();
		
	}
	
}

function mostrarInfoPase(idTercero) {	
	if (idTercero != null && idTercero != '') {		
		url = "/MidasWeb/siniestros/cabina/reporteCabina/valeTaller/obtenerDatosPase.action";
		
		redirectVentanaModal('vm_valeTaller', url, document.valeTallerForm);
	} else {
		limpiarDatosVehiculo();
	}
	
}

function limpiarDatosVehiculo() {
	jQuery('.datosTaller').val('');
}

function imprimirVale(){
	var formParams = null;
	formParams = jQuery(document.valeTallerForm).serialize();
	
	var url="/MidasWeb/siniestros/cabina/reporteCabina/valeTaller/imprimirVale.action" + '?' + formParams;
	
	window.open(url, "Vale_Taller");
}

function cerrarVentanaVale(){
	parent.cerrarVentanaModal('vm_valeTaller');	
}