jQuery(window).load(
	function(){
		setValueTextoLibre();
//		jQuery("#valueTextoLibre").blur(function(){guardar();});
		
		if(parent.document.getElementById("h_soloConsulta").value == 1){
			
			jQuery("#valueTextoLibre").attr('disabled','disabled');
			jQuery("#b_guardar").remove();
		}
	}
);

function setValueTextoLibre(){
	var value = getParentObject( "target" ).value;
	var valueTextoLibre = parent.document.getElementById(value).value;
	jQuery("#valueTextoLibre").val(valueTextoLibre);
}

function guardar(){
	var valToSave = jQuery("#valueTextoLibre").val();
	var value = getParentObject( "target" ).value;
	parent.document.getElementById(value).value=valToSave;
}

function cerrar(){
//	guardar();
	parent.cerrarVentanaModal('textolibre');
}

function guardarTextoLibre(){
	var id = parent.document.getElementById("idToReporte").value;
	var value = parent.document.getElementById("target").value;
	var valToSave = jQuery("#valueTextoLibre").val();
	var target = getParentObject( "target" ).value;
	var url = null;
	if (target == "informacionAdicional") {
		url = '/MidasWeb/siniestros/cabina/reportecabina/salvarInformacionAdicional.action'; 
	} else if (target = "declaracionTexto") {
		url = '/MidasWeb/siniestros/cabina/reportecabina/salvarDeclaracionSiniestro.action'; 
	}
	

	if( value == "informacionAdicional" && valToSave == "" ){
		parent.mostrarMensajeInformativo('Este campo no puede ir en blanco.','20');
	} else {
		url+='?idToReporte='+id;
		url+='&'+value+'='+valToSave;
		sendRequestJQ(null, url, 'textolibre', "success();");
		guardar();
	}
}

function success(){
	parent.mostrarMensajeInformativo('Información guardada correctamente', '30');
}