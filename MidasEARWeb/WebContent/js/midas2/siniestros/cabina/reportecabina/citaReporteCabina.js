function guardarCita(){
	var mensaje = validaRequeridos();
	if( mensaje == "" ){
		mensaje = compareDates(); 
		if( mensaje == "" ){
			parent.submitVentanaModal("ventanaCita", document.complementarCitaForm);
		}else{
			parent.mostrarMensajeInformativo(mensaje, '20');
		}
	} else {
		parent.mostrarMensajeInformativo("Campos Requeridos:" + mensaje, '20');
	}
}

function actualizaValorVariableTime(idValor, valor) {
	jQuery('#' + idValor).mask('00:00');
}

function validaRequeridos(){
	var horaCita	= jQuery("#horaCita").val();
	var fechaCita	= jQuery("#fechaCita").val();
	var asegurado 	= jQuery("#asegurado").val();
	var mensaje 	= "";
	if(fechaCita == ""){
		mensaje += "<br>Fecha Cita";
	}
	if(horaCita == ""){
		mensaje += "<br>Hora Cita";
	}
	if(asegurado == ""){
		mensaje += "<br>Asegurado";
	}
	return mensaje;
}

function compareDates(){
	var mensaje = "";
	var fecha1		=	jQuery("#fechaCita").val();
	var	fecha2		=	jQuery("#fechaTermino").val();
	var horaCita	= 	jQuery("#horaCita").val();
	var horaTermino =	jQuery("#horaTermino").val();
	if(fecha2 != ""){
		if(fecha1 == fecha2){
			if(horaCita != "" && horaTermino != ""){
				var hora1=horaCita.substring(0, 2); 
				var minuto1=horaCita.substring(3, 5);
				var hora2=horaTermino.substring(0, 2); 
				var minuto2=horaTermino.substring(3, 5);
				if(hora1 > hora2){
					mensaje = "La hora termino debe ser mayor";
				}else{
					if(hora1 == hora2 && minuto1 >= minuto2){
						mensaje = "La hora termino debe ser mayor";
					}
				}
			}
		}else{
		    if (compare_dates(fecha1, fecha2)){  
		    	mensaje = "La fecha de Termino deber ser mayor. Verificar."; 
		    }
		}
	}
    return mensaje;
}