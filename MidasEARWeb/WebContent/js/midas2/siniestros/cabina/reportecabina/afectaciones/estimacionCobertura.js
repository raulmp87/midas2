
function validarVigenciaPoliza(soloConsulta, idCoberturaReporteCabina, claveTipoCalculo, tipoEstimacion, reporteCabinaId){
	var formParams = jQuery("#siniestroCabinaForm").serialize();
	var motivoFaltaPago = 'CANCELADA POR FALTA DE PAGO';
//						'CANCELADA A PETICION'; //BORRAR, PARA PRUEBAS
	jQuery.ajax({
          url: "/MidasWeb/siniestros/cabina/reportecabina/validarVigenciaPoliza.action?" + '?' + formParams,
          dataType: 'json',
          async:false,
          type:"POST",
          data: null,
          success: function(json){
        	 unblockPage();
        	 var detalleInciso = json.detalleInciso;
        	 var incisoAutorizado = json.incisoAutorizado;
        	 
        	 //VALIDACION DE VIGENCIA DE POLIZA
        	 //ESTATUS: 0 (VIGENTE), 1 (NO VIGENTE), 2 (CANCELADO)
        	    if ( (detalleInciso.estatus == 1 	//CUANDO EL INCISO ES NO VIGENTE
        	    		|| (detalleInciso.estatus == 2 && detalleInciso.motivo == motivoFaltaPago ) )   //O EL INCISO ESTA CANCELADO Y ES POR FALTA DE PAGO 
        	    		// VALIDANDO EL ESTATUS DE LA AUTORIZACION, CUANDO incisoAutorizado == 1 EXISTE LA AUTORIZACION Y ESTA APROBADA
        	    		&& (incisoAutorizado == null || incisoAutorizado == undefined || incisoAutorizado == 2 || incisoAutorizado == -1)) {
        			if(confirm("El estatus del inciso requiere autorizaci\u00F3n ¿Desea solicitarla?")){
        				 var url = solicitarAutorizacionVigencia + '?' + jQuery("#siniestroCabinaForm").serialize() + "&entidad.id=" + reporteCabinaId;
        				 console.log(url);
        				sendRequestJQ(null, url, 'contenido', null);
        			} 
        		} else if( ( detalleInciso.estatus == 1 && incisoAutorizado == 0 ) 
        				|| (detalleInciso.estatus == 2 && detalleInciso.motivo == motivoFaltaPago && incisoAutorizado == 0)){
        			mostrarMensajeInformativo('Autorice el reporte para poder continuar', '20');
        		} else if( (detalleInciso.estatus == 0)
        				|| ( detalleInciso.estatus == 1 && incisoAutorizado == 1 ) 
        				|| (detalleInciso.estatus == 2 && detalleInciso.motivo == motivoFaltaPago && incisoAutorizado == 1)){
        			mostrarEstimacionOPases( soloConsulta, idCoberturaReporteCabina, claveTipoCalculo, tipoEstimacion, reporteCabinaId);
        		} else{
        			mostrarMensajeInformativo('Error al validar la vigencia de la p\u00F3liza', '30');
        		}
         	
          },
          input: function(json){
        	  mostrarMensajeInformativo(json.mensaje, '20');
        	  unblockPage();
          },
          beforeSend: function(){
				blockPage();
			}	
    });
}

var listadoPasesGrid;

/**
 * Regresa la pantalla correspondiente al tipo de estimación
 * @param idCoberturaReporteCabina
 * @param claveTipoCalculo
 * @param tipoEstimacion
 */
console.log('Antes de mostrarEstimacionOPases');
function mostrarEstimacionOPases( soloConsulta, idCoberturaReporteCabina, claveTipoCalculo, tipoEstimacion, reporteCabinaId){
    console.log('Entra a mostrarEstimacionOPases');
    var pasesAtencionInt;
    var soloConsulta = jQuery("#h_soloConsulta").val();
    var funcionCallback = null;
    var mensajeValidacionPermisoParaCrearPasesDeAtencion = '';
	var idReporte = jQuery("#idToReporte").val();
    
    console.log('En mostrarEstimacion mensajeValidacionPermisoParaCrearPasesDeAtencion:'+mensajeValidacionPermisoParaCrearPasesDeAtencion);
    if(isEmpty(mensajeValidacionPermisoParaCrearPasesDeAtencion)){
                   console.log('Estuvo vacio');
                   if( tipoEstimacion == "GME" || tipoEstimacion == "GMC" || tipoEstimacion == "RCB" || tipoEstimacion == "RCP" || tipoEstimacion == "RCV" || tipoEstimacion == "RCJ" ){
                                   pasesAtencionInt = 1;
                   }else{
                                   pasesAtencionInt = 0;
                   }
                   
                   var url = mostrarEstimacionPath + '?idCoberturaReporteCabina=' + idCoberturaReporteCabina + 
        "&tipoCalculo=" + claveTipoCalculo + 
                   "&tipoEstimacion=" + tipoEstimacion +
                   "&reporteCabinaId=" + reporteCabinaId + 
                   "&pasesAtencionInt=" + pasesAtencionInt + 
                   "&soloConsulta=" + soloConsulta;
                    
                   if( tipoEstimacion == "ESD" ){
                                   mostrarVentanaModal( "vm_estimacion", "Estimación", null, null, 600, 530, url , null); 
                   }else if( tipoEstimacion == "GME" || tipoEstimacion == "GMC" || tipoEstimacion == "RCB" || tipoEstimacion == "RCP" || tipoEstimacion == "RCV" || tipoEstimacion == "RCJ" ){
                                   mostrarVentanaModal( "vm_pases_de_atencion", "Pases De Atención", null, null, 1050, 580, url , null);     
                   }else if( tipoEstimacion == "DMA" ){
                                   url = url + '&incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
                                     "&fechaReporteSiniestroMillis=" + jQuery("#h_fechaReporteSiniestroMillis").val()  +
									 "&validOnMillis=" + jQuery("#h_validOnMillis").val() + "" +
									 "&recordFromMillis=" + jQuery("#h_recordFromMillis").val() + 
                                     "&idToPoliza=" + jQuery("#h_idPoliza").val() + 
                                     "&siniestroDTO.recibidaOrdenCia=" + jQuery("#recibidaOrdenCia").val() +
                                     "&siniestroDTO.ciaSeguros=" + jQuery("#ciaSeguros").val()+'&estatusHabilitado=' +  1; 
                                     console.log('URL DMA : '+url);
                                   if(soloConsulta == 1){
                                                   sendRequestJQ(null, url, targetWorkArea, "setConsultaEstimacion();");             
                                   }else{
                                                   sendRequestJQ(null, url, targetWorkArea, null);             
                                   }
                                   
                   }              
    }else{
                   console.log('No Estuvo vacio');
                   mostrarMensajeInformativo(mensajeValidacionPermisoParaCrearPasesDeAtencion, '20');
    }

}

function consultaEstimacionDirecta(){
    jQuery("#txt_estimacionNueva").attr("disabled","disabled");
    jQuery("#s_causaMovimiento").attr("disabled","disabled");
    //jQuery("#txt_correo").attr("disabled","disabled");
    jQuery("#txt_sumaAseguradaAmparada").attr("disabled","disabled");
    jQuery("#txt_sumaAseguradaObtenida").attr("disabled","disabled");
    jQuery("#s_aplicaDeducible").attr("disabled","disabled");
    jQuery("#btn_nuevo").remove();
    jQuery("#btn_guardar").remove();
    jQuery("#b_copiarEstimacion").remove();
    jQuery("#emailNoProporcionado").attr("disabled","disabled");
    jQuery("#emailNoProporcionadoMotivo").attr("disabled","disabled");
    jQuery("#txt_correo").attr("disabled","disabled");
    
}

/**
 * Muestra el listado de las Condiciones Especiales asociadas a un Inciso de una Poliza 
 */
function inicializarListadoPasesAtencion(){
	var soloConsulta = jQuery("#h_soloConsulta").val();
	listadoPasesGrid = new dhtmlXGridObject('listadoPasesAtencionGrid');

	listadoPasesGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoPasesGrid.attachEvent("onXLE", function(grid){
		unblockPage();
		validaListadoVacio();
    });	
	
	listadoPasesGrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,&nbsp,&nbsp");
	
	var url = obtenerListadoPasesPath + "?idCoberturaReporteCabina=" + jQuery("#h_idCoberturaReporteCabina").val() + 
										"&tipoEstimacion=" + jQuery("#h_tipoEstimacion").val() + "&soloConsulta=" + soloConsulta;
	

	listadoPasesGrid.load( url );
}

function validaListadoVacio(){

	 var numFilas = listadoPasesGrid.getRowsNum();
	 
	 if( numFilas < 2 ){
		 jQuery("#d_boxLight").addClass("dhx_pbox_light");
		 jQuery("#d_pagerInfo").addClass("dhx_pager_info_light");
		 jQuery("#d_pagerInfo").html("<div>No se encontraron registros</div>");
	 }else{
		 jQuery("#d_boxLight").removeClass("dhx_pbox_light");
		 jQuery("#d_pagerInfo").removeClass("dhx_pager_info_light");
		 jQuery("#d_pagerInfo").html("");
	 }
}

function mostrarEditarPaseAtencion(id){
	
	var url = mostrarPasePath + '?idCoberturaReporteCabina=' + jQuery("#h_idCoberturaReporteCabina").val() + 
    "&tipoCalculo=" + jQuery("#h_tipoCalculo").val() + 
	"&tipoEstimacion=" + jQuery("#h_tipoEstimacion").val() +
	"&reporteCabinaId=" + jQuery("#h_reporteCabinaId").val() +
	"&idPaseDeAtencion=" + id +
	"&soloConsulta=" + 0 +
	"&estatusHabilitado=" + 1; 
	
	parent.document.getElementById('urlRedirect').value = url;
	parent.cerrarVentanaModal("vm_pases_de_atencion", "redireccionaEditar();");
}




function mostrarConsultaPaseAtencion(id){
	
	var url = mostrarPasePath + '?idCoberturaReporteCabina=' + jQuery("#h_idCoberturaReporteCabina").val() + 
    "&tipoCalculo=" + jQuery("#h_tipoCalculo").val() + 
	"&tipoEstimacion=" + jQuery("#h_tipoEstimacion").val() +
	"&reporteCabinaId=" + jQuery("#h_reporteCabinaId").val() +
	"&idPaseDeAtencion=" + id + 
	"&soloConsulta=" + jQuery("#h_soloConsulta").val()+
	"&modoConsultaDeListado=true";
	console.log('URL de mostrarConsultaPaseAtencion : '+url);
	parent.document.getElementById('urlRedirect').value = url;
	parent.cerrarVentanaModal("vm_pases_de_atencion", "redireccionaConsulta();");
}


function crearNuevoPase(){
	var url = crearNuevoPasePath + '?idCoberturaReporteCabina=' + jQuery("#h_idCoberturaReporteCabina").val() + 
    "&tipoCalculo=" + jQuery("#h_tipoCalculo").val() + 
	"&tipoEstimacion=" + jQuery("#h_tipoEstimacion").val() +
	"&reporteCabinaId=" + jQuery("#h_reporteCabinaId").val() +
	"&soloConsulta=" + 0;
	
	parent.document.getElementById('urlRedirect').value = url;
	
	parent.cerrarVentanaModal("vm_pases_de_atencion", "redirecciona();");
}

function redirecciona(){
	var url = jQuery("#urlRedirect").val();
	sendRequestJQ(null, url, 'contenido', null);
}

function redireccionaConsulta(){
	var url = jQuery("#urlRedirect").val();
	sendRequestJQ(null, url, 'contenido', 'setConsultaEstimacion();');
}

function redireccionaEditar(){
	var url = jQuery("#urlRedirect").val();
	sendRequestJQ(null, url, 'contenido', 'setEditarEstimacion();');
}

/**
 * Manda a guardar la estimacion a traves de pop up
 */
function guardarEstimacion(){
	var url = "/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/guardar.action";
	
	if( validacionEstimacionDirecta() ){
		//if (validationEmail( jQuery("#txt_correo").val() )) {
			removeCurrencyFormatOnTxtInput();
			parent.redirectVentanaModal('vm_estimacion', url, document.estimacionForm);
		//};		
		
	}
}

function validacionEstimacionDirecta(){
	var resultado = true;
	var claveFuenteSumaAsegurada = jQuery("#claveFuenteSumaAsegurada").val();
	
	if (claveFuenteSumaAsegurada == 1 || claveFuenteSumaAsegurada == 2 || claveFuenteSumaAsegurada == 9) {
		console.log('Valor : '+jQuery("#txt_sumaAseguradaObtenida").val());
		if( (typeof jQuery("#txt_sumaAseguradaObtenida").val() === 'undefined') || (jQuery("#txt_sumaAseguradaObtenida").val() == "") ) {
			console.log('Agrega la clase errorField');
			jQuery("#txt_sumaAseguradaObtenida").addClass("errorField"); 
			resultado= false;
		}else{
			console.log('Remover la clase errorField');
			jQuery("#txt_sumaAseguradaObtenida").removeClass("errorField"); 
		}
	}
	
	
	if( jQuery("#txt_estimacionNueva").val() == ""){
		jQuery("#txt_estimacionNueva").addClass("errorField"); 
		resultado =  false;
	}else{
		jQuery("#txt_estimacionNueva").removeClass("errorField"); 
	}
	
	if( jQuery("#s_causaMovimiento").val() == ""){
		jQuery("#s_causaMovimiento").addClass("errorField"); 
		resultado = false;
	}else{
		jQuery("#s_causaMovimiento").removeClass("errorField"); 
	}
	
	
	/*if( jQuery("#txt_correo").val() == ""){
		jQuery("#txt_correo").addClass("errorField"); 
		resultado= false;
	}else{
		jQuery("#txt_correo").removeClass("errorField"); 
	}*/
	return resultado;
}

/**
 * Manda a guardar la estimacion general (desde pantalla principal)
 */
function guardarEstimacionGral(){
	if( validaDatosRequeridos() && validaHospitalMedico() && validaTipoPaseAtencion() && validaEdoCiudad() && validaDeducible() ){
		validationEmailGuardar();
	}else{
		isError();
	}
}


function autorizarSolicitudDeReserva(){
	if( validaCamposParaAutorizar() ){
		var url = "/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/autorizarReserva.action";
		removeCurrencyFormatOnTxtInput();
	   	sendRequestJQ(document.estimacionForm, url,'contenido', "setEditarEstimacion();");
	}else{
		isError();
	}
}


function autorizarSolicitudDeReservaDesdeModal(){
	if( validaCamposParaAutorizar() ){
		var url = "/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/autorizarReserva.action";
		removeCurrencyFormatOnTxtInput();
	   	parent.redirectVentanaModal('vm_estimacion', url, document.estimacionForm);
	}else{
		isError();
	}
}

function validaCamposParaAutorizar(){
	var success = true;
	var classRequerido = 'requeridoParaAutorizar';
	var requeridos = jQuery('.'+classRequerido );
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.removeClass("deshabilitado");
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);	
	
	return success;
}


function validaDeducible(){
	removeCurrencyFormatOnTxtInput();
	if( jQuery("#s_aplicaDeducible").val() == "S" & parseFloat(jQuery("#montoDeducible").val()) <= 0 ){
		mostrarVentanaMensaje("20","El deducible debe ser mayor a 0",null);
		initCurrencyFormatOnTxtInput();
		return false;
	}else{
		return true;
	}
}

function validaEdoCiudad(){
	
	var bandera = true;
	
	var tipoEstimacion =  jQuery("#h_tipoEstimacion").val();
	
	if( tipoEstimacion == 'RCB' ){
		
		if( !jQuery(".jQ_estado").val() ){
			jQuery(".jQ_estado").addClass("errorField");
			bandera = false;
		}
		
		if( jQuery(".jQ_estado").val() && !jQuery(".jQ_ciudad").val() ){
			jQuery(".jQ_ciudad").addClass("errorField");
			bandera = false;
		} 
		
		// # LIMPIAR CSS DE ERROR
		if( bandera ){
			jQuery(".jQ_ciudad").removeClass("errorField");
			jQuery(".jQ_estado").removeClass("errorField");
		}
	}
	
	return bandera;
}
/**
 * Cierra el Pop-up, regresando a la pantalla anterior
 */
function cerrarEstimacionDirecta(){
	var idReporteCabina = jQuery("#h_idReporteCabina").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId=" + idReporteCabina + 
																				   "&soloConsulta=" + soloConsulta;
	
	parent.document.getElementById('urlRedirect').value = url;
	
	parent.cerrarVentanaModal("vm_estimacion", "redirecciona();");
}

function cerrarDM(){
	var idReporteCabina = jQuery("#h_idReporteCabina").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId=" + idReporteCabina + 
																				   "&soloConsulta=" + soloConsulta;
	sendRequestJQ(null, url, 'contenido', null);
}

function cerrarPaseAtencion(){
	var idReporteCabina = jQuery("#h_reporteCabinaId").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	
	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId=" + idReporteCabina  + 
	   																			  "&soloConsulta=" + soloConsulta;
	parent.document.getElementById('urlRedirect').value = url;
	
	parent.cerrarVentanaModal("vm_pases_de_atencion", "redirecciona();");
}

function cerrarPasesDeAtencion(){
	parent.cerrarVentanaModal("vm_pases_de_atencion", "reloadContenedorSiniestro();");
}

//function cerrarCoberturaGral(){
//	var idReporteCabina = jQuery("#h_idReporteCabina").val();
//	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId=" + idReporteCabina;
//	sendRequestJQ(null, url, 'contenido', null);
//	
//}


function cerrarCoberturaGral(){
	var idReporteCabina 			= jQuery("#h_idReporteCabina").val();
	var idCoberturaReporteCabina 	= jQuery("#h_idCoberturaReporteCabina").val();
	var tipoCalculo 				= jQuery("#h_tipoCalculo").val();
	var tipoEstimacion 				= jQuery("#h_tipoEstimacion").val();
	var soloConsulta 				= jQuery("#h_soloConsulta").val();
	var consulta					= jQuery("#h_consultaPase").val();
	

	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?" +
			"siniestroDTO.reporteCabinaId=" + idReporteCabina +
			"&idCoberturaReporteCabina=" + idCoberturaReporteCabina +
			"&tipoCalculo=" +  tipoCalculo +
			"&tipoEstimacion=" + tipoEstimacion + 
			"&soloConsulta=" + soloConsulta;
	
	var cerrar = true;

	
	if (consulta == null || consulta != 1) {
		if(!confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?')){
			cerrar = false;
		}
	}

	if( cerrar == true ){
		if( jQuery("#h_isListadoPases").val() != "1" && jQuery("#h_isListadoPases").val() != "2" ){
			sendRequestJQ(null, url, 'contenido', 'openPopUpPases();');
		}else{
			url="/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoPasesAtencion.action";
			sendRequestJQ(null, url, 'contenido', null);
		}
	}
	
}

function openPopUpPases(){
	
	var idReporteCabina 			= jQuery("#idToReporte").val();
	var idCoberturaReporteCabina 	= jQuery("#h_idCoberturaReporteCabina").val();
	var tipoCalculo 				= jQuery("#h_tipoCalculo").val();
	var tipoEstimacion 				= jQuery("#h_tipoEstimacion").val();
	var soloConsulta 				= jQuery("#h_soloConsulta").val();
	
	mostrarEstimacionOPases( soloConsulta,idCoberturaReporteCabina, tipoCalculo, tipoEstimacion, idReporteCabina );
}





function reloadContenedorSiniestro(){
	var idReporteCabina = jQuery("#idToReporte").val();
	var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId=" + idReporteCabina;
	sendRequestJQ(null, url, 'contenido', null);
}


function nuevo(){
	jQuery('#estimacionForm').each (function(){
		  this.reset();
	});
}

var txtPrestadorId;
var txtPrestadorGral;
function mostrarPrestadorServicio( tipoPrestador, hPrestador, txtPrestador  ){
	txtPrestadorId = hPrestador;
	txtPrestadorGral = txtPrestador;
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true&filtroCatalogo.estatus=1&filtroCatalogo.tipoPrestadorStr="+tipoPrestador;
	mostrarVentanaModal("vm_asignarPrestador", "Asignar Prestador de Servicio", null, null, 926, 559, url , ""); 
}


function mostrarPrestadorServicioByEstado( tipoPrestador, hPrestador, txtPrestador, estado ){
	if(estado == ""){
		mostrarVentanaMensaje("20","Se requiere elegir un estado");
	}else{
		txtPrestadorId = hPrestador;
		txtPrestadorGral = txtPrestador;
		var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true&filtroCatalogo.estatus=1&filtroCatalogo.tipoPrestadorStr="+tipoPrestador+
		"&filtroCatalogo.estado=" + estado; 
		mostrarVentanaModal("vm_asignarPrestador", "Asignar Prestador de Servicio", null, null, 926, 559, url , ""); 
	}
}

function setPrestadorId( prestadorId, nombrePrestador  ){
	txtPrestadorId.val(prestadorId);
	txtPrestadorGral.val(nombrePrestador);
	parent.cerrarVentanaModal("vm_asignarPrestador",true);
}

function onChangeEstadoFiltro(targetId, estadoSelectId){	
	var estadoValue = dwr.util.getValue(estadoSelectId);	
	if(estadoValue != null && estadoValue != ''){
		listadoService.getMapMunicipiosPorEstadoMidas(estadoValue,
				function(data){
					addOptionOderByElementSin(targetId,data);
				});
	}else{
		addOptionsDireccionSin(targetId,"");
	}
}




function isError(){
	mostrarVentanaMensaje("20","Favor de llenar los campos requeridos",null);
}

function validaDatosRequeridos(){
	var success = true;
	val = jQuery('#s_tipoPaseAtencion').val();
	var classRequerido = 'requerido'+val;
	console.log('classRequerido  :'+classRequerido );
	var requeridos = jQuery('.'+classRequerido );
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.removeClass("deshabilitado");
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);	
	if(!validaInfoCiaParaGuardar()){
		success = false;
	}
	
	return success;
}

function removeErrorFieldClass(oldValue){
	var classRequerido = 'requerido'+oldValue;
	jQuery(".requerido."+classRequerido).blur(
			function(){
				var these = jQuery(this);
				these.removeClass("errorField");
			}
	);

}

function onBlurRequeridos(){
	var val = jQuery('#s_tipoPaseAtencion').val();
	var classRequerido = '.requerido'+val;
	jQuery(".requerido"+classRequerido).blur(
			function(){
				var these = jQuery(this);
				if( isEmpty( these.val() ) ){
					these.addClass("errorField");
				} else {
					these.removeClass("errorField");
				}
			}
		);
}

function validationEmail( correo ){

	expr=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
	if(correo !="" && !expr.test( correo )){
    	parent.mostrarVentanaMensaje("20","El correo electr\u00F3nico no es una direcci\u00F3n de correo v\u00e1lida",null);
    	jQuery("#txt_email").focus();
    	return false;
    }
	return true;
}

function validationEmailGuardar(){
	var url = "/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/guardar.action";
	var correo = jQuery('#txt_email').val()
	expr=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(correo !="" && !expr.test( correo )){
    	parent.mostrarVentanaMensaje("20","El correo electr\u00F3nico no es una direcci\u00F3n de correo v\u00e1lida",null);
    	jQuery("#txt_email").focus();return;
    }else{
//    	jQuery("#s_tipoPaseAtencion").attr("disabled",false);
    	removeCurrencyFormatOnTxtInput();
    	sendRequestJQ(document.estimacionForm, url,'contenido', "setEditarEstimacion();");
    }
}

function setEditarEstimacion(){
	
	var tipoEstimacion = jQuery("#h_tipoEstimacion").val();

	if( tipoEstimacion == "GMC" || tipoEstimacion == "RCB" || tipoEstimacion == "RCP" || tipoEstimacion == "RCV" || tipoEstimacion == "RCJ" ){
//		jQuery("#s_tipoPaseAtencion").attr("disabled","disabled");
	}
	
	if( jQuery("#h_estatusHabilitado").val() == "1"){
		jQuery("#s_estatus").attr("disabled",false);
	}
}

function habilitaEstatusDMA(){
	if( jQuery("#h_estatusHabilitado").val() == "1" && jQuery("#txt_folio").val() != "" ){
		jQuery("#s_estatus").attr("disabled",false);
	}
}

//function iniciarEstimacion(){
//	var nuevo = true;
//
//	if(!confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?')){
//		nuevo = false;
//	}
//
//	if(nuevo){
//		if( jQuery("#h_tipoEstimacion").val() === "GME" ){
//			 jQuery("#s_tipoPaseAtencion").removeClass("setNew");
//			 jQuery(".setNew").attr("value","");
//			 jQuery("#s_tipoPaseAtencion").addClass("setNew");
//		} else if( jQuery("#h_tipoEstimacion").val() === "RCB" ){
//			 var tab = document.getElementById("td_pais").getElementsByTagName("TABLE")[0];
//			 var firstTR     = tab.getElementsByTagName("tbody")[0].getElementsByTagName("tr")[0];
//			 var paisTD    = firstTR.getElementsByTagName("td")[1];
//			 var paisDIV   = paisTD.getElementsByTagName("div")[1];
//			 var paisSelect = paisDIV.getElementsByTagName("select")[0];
//			 
//			 jQuery(".setNew").attr("value","");
//			 jQuery("#txt_responsableReparacion").attr("value","");
//			 paisSelect.value="PAMEXI";
//		}else if( jQuery("#h_tipoEstimacion").val() === "RCJ" ){
//			 jQuery("#txt_hospital").attr("value","");
//			 jQuery("#txt_medico").attr("value","");
//		}else{
//			jQuery(".setNew").attr("value","");
//		}
//	}
//}

function iniciarEstimacion() {
	var nuevo = true;

	if(!confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?')){
		nuevo = false;
	}
	if(nuevo){
		var url = crearNuevoPasePath + 
		'?idCoberturaReporteCabina=' + jQuery("#h_idCoberturaReporteCabina").val() + 
		"&tipoCalculo=" + jQuery("#h_tipoCalculo").val() + 
		"&tipoEstimacion=" + jQuery("#h_tipoEstimacion").val() + 
		"&reporteCabinaId=" + jQuery("#h_idReporteCabina").val() + 
		"&soloConsulta=" + 0;
		
		sendRequestJQ(null, url, 'contenido', null);
	}
}



function setConsultaEstimacion(){
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
	jQuery(".setNew").removeClass("requerido");
	jQuery(".setNew").removeClass("errorField");
	jQuery("#btn_nuevo").remove();
	jQuery("#btn_guardar").remove();
	jQuery("#btn_indemnizar").remove();
	jQuery("#b_copiarEstimacion").remove();
	jQuery("#h_consultaPase").val('1');
	try{
		jQuery("#btn_buscar_seguroTercero").remove();
	}catch (e) {
	}
	try{
		jQuery("#btn_buscar_hospital").remove();
	}catch (e) {
	}
	try{
		jQuery("#btn_buscar_medico").remove();
	}catch (e) {
	}
	try{
		jQuery("#btn_valuacion").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_buscarResponsableReparacion").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_imagenes").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_buscarCompaniaSeguros").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_hospital").remove();
	}catch (e) {
	}
	try{
		jQuery("#b_medico").remove();
	}catch (e) {
	}
	try{
		jQuery("#btnCondiciones").remove();
	}catch (e) {}
	try{
		jQuery("#btnBuscarCia").remove();
	}catch (e) {}
	try{
		jQuery("#btnBuscarTaller").remove();
	}catch (e) {
	}
	
	try{
	   var tab = document.getElementById("td_pais").getElementsByTagName("TABLE")[0];
		
       var secondTR     = tab.getElementsByTagName("tbody")[0].getElementsByTagName("tr")[1];
       var estadoTD    = secondTR.getElementsByTagName("td")[1];
	   var estadoDIV   = estadoTD.getElementsByTagName("div")[1];
	   var estadoSelect = estadoDIV.getElementsByTagName("select")[0]; 
	   estadoSelect.disabled=true;

       var municipioTD    = secondTR.getElementsByTagName("td")[3];
	   var municipioDIV   = municipioTD.getElementsByTagName("div")[1];
	   var municipioSelect = municipioDIV.getElementsByTagName("select")[0]; 
	   municipioSelect.disabled=true;
	}catch (e) {
	}
	jQuery("#contenido").append("<div id='h_consulta' style='display:none;' >1</div>");
//	jQuery("input").attr('disabled','disabled');
}



function onChangeTipoAtencion(){
	if( jQuery("#s_tipoAtencion").val() == "AMB" && jQuery("#s_tipoPaseAtencion").val() != "SRE"){
		jQuery("#s_tipoAtencion").removeClass("errorField");
		jQuery("#txt_estimacionNueva").val(1500);
		mostrarMensajeInformativo('El monto propuesto para esta opcion es de $1,500', '20');
	}else if( jQuery("#s_tipoAtencion").val() == "HOS" && jQuery("#s_tipoPaseAtencion").val() != "SRE" ){
		jQuery("#s_tipoAtencion").removeClass("errorField");
		jQuery("#txt_estimacionNueva").val(15000);
		mostrarMensajeInformativo('El monto propuesto para esta opcion es de $15,000', '20');
	}else{
		jQuery("#txt_estimacionNueva").val(0);
	}
}

function onChangeTipoPaseAtencion(){
	if( jQuery("#s_tipoPaseAtencion").val() == "RAC" ){
		jQuery("#tr_companiaSeguros").show("slow");
	}else{
		jQuery("#txt_companiaSegurosTercero").val("");
		jQuery("#tr_companiaSeguros").hide("slow");
	}
}

function onChangeTipoPaseAtencionRCBienes(){
	var tipoPase = jQuery("#s_tipoPaseAtencion").val();
	jQuery("#h_tipoPaseAtencion").val(tipoPase);
}

function onChangeTipoPaseAtencionRCPersona(){
	
	var tipoPase = jQuery("#s_tipoPaseAtencion").val();
	jQuery("#h_tipoPaseAtencion").val(tipoPase);

	if( tipoPase == "RAC" || tipoPase == "SRE"){
		jQuery("#tr_companiaSeguros").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#txt_medico").val("");
		jQuery("#h_medico").val("")
		jQuery("#ch_otroHospital").attr('checked', false);
		jQuery("#txt_otroHospital").val("");
		jQuery("#td_hospital").hide("slow");
		try{
			jQuery("#b_hospital").hide("slow");
		}catch (e) {
		}
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		
	}else{
		jQuery("#txt_companiaSegurosTercero").val("");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		jQuery("#td_otroHospital").show("slow");
		jQuery("#txt_otroHospital").val("");
		try{
			jQuery("#b_hospital").show("slow");
		}catch (e) {
		}
	}
}


function onChangeTipoPaseAtencionGastosMedicosConductor(){
	
	var tipoPase = jQuery("#s_tipoPaseAtencion").val();
	jQuery("#h_tipoPaseAtencion").val(tipoPase);

	if( tipoPase == "RAC" || tipoPase == "SRE"){
		jQuery("#tr_companiaSeguros").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#txt_medico").val("");
		jQuery("#h_medico").val("")
		jQuery("#ch_otroHospital").attr('checked', false);
		jQuery("#txt_otroHospital").val("");
		jQuery("#td_hospital").hide("slow");
		try{
			jQuery("#b_hospital").hide("slow");
		}catch (e) {
		}
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#td_numerosiniestro").show("slow");
		
	}else{
		jQuery("#txt_companiaSegurosTercero").val("");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		jQuery("#td_otroHospital").show("slow");
		jQuery("#txt_otroHospital").val("");
		try{
			jQuery("#b_hospital").show("slow");
		}catch (e) {
		}
		jQuery("#td_numerosiniestro").hide("slow");
	}
}

function onChangeOtroHospital(){
	if( jQuery("#ch_otroHospital").attr('checked') == true){
		jQuery("#td_nombreOtroHospital").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#td_hospital").hide("slow");
		jQuery("#ch_otroHospital").attr('checked', true);
		try{
			jQuery("#b_hospital").hide("slow");
		}catch (e) {
		}
	}else{
		jQuery("#td_hospital").show("slow");
		jQuery("#txt_otroHospital").val("");
		jQuery("#td_nombreOtroHospital").hide("slow");
		try{
			jQuery("#b_hospital").show("slow");
		}catch (e) {
		}
	}
}

function onChangeEstadoPersona(){
	if( jQuery("#s_estadoPersona").val() == "LES" ){
		jQuery("#td_tipoAtencion").show("slow");
		onBlurRequeridos();
	}else{
		jQuery("#s_tipoAtencion").val("");
		jQuery("#td_tipoAtencion").hide("slow");
	}
}

function onChangeTipoPaseAtencionRCV(){
	var paseAtencion = jQuery("#s_tipoPaseAtencion").val();
	if( paseAtencion == "RAC" || paseAtencion == "SRE"){
		jQuery("#td_numeroSiniestro").show("slow");
		jQuery("#td_ciaSeguros").show("slow");
		jQuery("#td_tieneCia").show("slow");
		jQuery("#btnBuscarCia").show("slow");
		jQuery("#td_estado").hide("slow");
		jQuery("#td_taller").hide("slow");
		jQuery("#s_estado").removeClass("requerido");
		jQuery("#txt_tallerAsignado").removeClass("requerido");
		jQuery("#btnBuscarTaller").hide("slow");
	}else if( paseAtencion == "PAT" ){
		jQuery("#td_numeroSiniestro").hide("slow");
		jQuery("#td_ciaSeguros").hide("slow");
		jQuery("#td_tieneCia").hide("slow");
		jQuery("#btnBuscarCia").hide("slow");
		jQuery("#td_estado").show("slow");
		jQuery("#td_taller").show("slow");
		jQuery("#btnBuscarTaller").show("slow");
		jQuery("#s_estado").addClass("requerido");
		jQuery("#txt_tallerAsignado").addClass("requerido");
		onBlurRequeridos();
	} 
}	

function aplicaSeleccionTipoPaseDeAtencion(selectElement){

	var permite = jQuery('#h_permiteCapturarCiaInfo').val();
	console.log('permite: '+permite);

	console.log('Elimina los classErrorField de : '+selectElement.oldValue);
	removeErrorFieldClass(selectElement.oldValue);
	jQuery('.elegible').hide();
	var val = jQuery('#s_tipoPaseAtencion').val();
	if(!isEmpty(val)){
		var classElegible = 'elegible'+val;
		if(permite == 'true'  &&  val == 'SRE'){
		  console.log('Con valor');
		  classElegible = classElegible+'CIA'
		}
		console.log('classElegible  :'+classElegible );
		jQuery('.'+classElegible ).show();
	}
	
}


function inicioPersona(){

	if( jQuery("#s_estadoPersona").val() == "LES" ){
		jQuery("#td_tipoAtencion").show("slow");
		jQuery("#s_tipoAtencion").addClass("requerido");
		onBlurRequeridos();
	}else{
		jQuery("#s_tipoAtencion").removeClass("requerido");
		jQuery("#s_tipoAtencion").removeClass("errorField");
		jQuery("#td_tipoAtencion").hide("slow");
	}
	
	if( jQuery("#txt_otroHospital").val() != "" ){
		jQuery("#td_nombreOtroHospital").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#td_hospital").hide("slow");
		
		jQuery("#ch_otroHospital").attr('checked', true);
	}else{
		jQuery("#td_hospital").show("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#ch_otroHospital").attr('checked', false);
	}
	
	if( jQuery("#h_id").val() == null || jQuery("#txt_hospital").val() != "" ){
		jQuery("#ch_otroHospital").attr('checked', false);
	}

	if( jQuery("#s_tipoAtencion").val() == "AMB" ){
		jQuery("#s_tipoAtencion").removeClass("errorField");
	}else if( jQuery("#s_tipoAtencion").val() == "HOS" ){
		jQuery("#s_tipoAtencion").removeClass("errorField");
	}

	if( jQuery("#s_tipoPaseAtencion").val() == "RAC" || jQuery("#s_tipoPaseAtencion").val() == "SRE"){
		jQuery("#div_companiaSeguros").show("slow");
		jQuery("#td_hospital").hide("slow");
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#tr_tieneCompaniaSeguros").show("slow");
		
	}else{
		jQuery("#tr_tieneCompaniaSeguros").hide("slow");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		if( jQuery("#ch_otroHospital").attr('checked') == true){
			jQuery("#td_nombreOtroHospital").show("slow");
		}else{
			jQuery("#td_nombreOtroHospital").hide("slow");
		}
	}
}

function inicioGME(){
	if( jQuery("#h_id").val() == null || jQuery("#txt_hospital").val() != "" ){
		jQuery("#ch_otroHospital").attr('checked', false);
	}

//	jQuery("#s_tipoPaseAtencion").attr("disabled","disabled");
	
	if( jQuery("#s_estadoPersona").val() == "LES" ){
		jQuery("#td_tipoAtencion").show("slow");
	}else{
		jQuery("#td_tipoAtencion").hide("slow");
	}
	
	if( jQuery("#txt_otroHospital").val() != "" ){
		jQuery("#td_nombreOtroHospital").show("slow");
		try{
			jQuery("#b_hospital").hide("slow");
		}catch (e) {
		}
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#td_hospital").hide("slow");
		jQuery("#ch_otroHospital").attr('checked', true);
	}else{
		try{
			jQuery("#b_hospital").show("slow");
		}catch (e) {
		}
		onBlurRequeridos();
		jQuery("#td_nombreOtroHospital").hide("slow");
	}
}

function initRCGMConductor(){
	if( jQuery("#h_id").val() == null || jQuery("#txt_hospital").val() != "" ){
		jQuery("#ch_otroHospital").attr('checked', false);
	}
	
	if( jQuery("#s_estadoPersona").val() == "LES" ){
		jQuery("#td_tipoAtencion").show("slow");
		jQuery("#s_tipoAtencion").addClass("requerido");
	}else{
		jQuery("#s_tipoAtencion").removeClass("requerido");
		jQuery("#s_tipoAtencion").removeClass("errorField");
		jQuery("#td_tipoAtencion").hide("slow");
	}
	
	
	if( jQuery("#s_tipoPaseAtencion").val() == "RAC" ){
		jQuery("#tr_companiaSeguros").show("slow");
		jQuery("#td_hospital").hide("slow");
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#tr_tieneCompaniaSeguros").show("slow");
		jQuery("#td_numerosiniestro").show("slow");
		
	}else{
		jQuery("#tr_tieneCompaniaSeguros").hide("slow");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		jQuery("#td_numerosiniestro").hide("slow");
		if( jQuery("#ch_otroHospital").attr('checked') == true){
			jQuery("#td_nombreOtroHospital").show("slow");
		}else{
			jQuery("#td_nombreOtroHospital").hide("slow");
		}
	}
	
	if( jQuery("#txt_otroHospital").val() != "" ){
		jQuery("#td_nombreOtroHospital").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#td_hospital").hide("slow");
		
		jQuery("#ch_otroHospital").attr('checked', true);
	}else if(jQuery("#s_tipoPaseAtencion").val() != "RAC"){
		jQuery("#td_hospital").show("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#ch_otroHospital").attr('checked', false);
	}
}

function onChangeApDeducible(tipoPase){
	
	if( jQuery("#s_aplicaDeducible").val() == "N" ){
		
		if( tipoPase == "DMA" ){
			jQuery("#btnCondiciones").show("slow");
		}
		
		jQuery(".montoDeducible").hide();
		
		// SI EL ID DE LA ESTIMACION ES NULO INDICA QUE PASE NO EXISTE Y SE ESCONDE EL COMBO DE MOTIVOS
		if( jQuery("#idEstimacionCoberturaReporteCabina").val() != "" && jQuery("#h_aplicaDeducible").val() != "N" ){
			jQuery(".ctgMotivoNoaplicaDeducible").show();
		}
		
	}else{
		
		if( tipoPase == "DMA" ){
			jQuery("#btnCondiciones").hide("slow");
		}
		
		// SI LA LLEGARA APLICAR A DEDUCIBLE SE CALCULA POR JS
		calcularDeducibleCristales();
		
		jQuery(".ctgMotivoNoaplicaDeducible").hide();
		jQuery(".montoDeducible").show();

	}
}

function inicializarCondicionesEspecialesFromSiniestro(tipoValidacion){
	
	var url = buscarCondicionesEspecialesIncisoPath + '?incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
												      "&idReporte=" + jQuery("#h_idReporteCabina").val() + 
													  "&fechaReporteSiniestroMillis=" + jQuery("#h_fechaReporteSiniestroMillis").val() + 
													  "&idToPoliza=" + jQuery("#h_idPoliza").val()+
													  "&tipoValidacion=" + tipoValidacion +
													  "&validOnMillis=" + jQuery("#h_validOnMillis").val() + "" +
													  "&recordFromMillis=" + jQuery("#h_recordFromMillis").val();
	
	mostrarVentanaModal( "vm_condicionesEspecialesInciso", "Consulta de Condiciones Especiales de un Inciso", null, null, 800, 490, url , null);
}

function onChangeTipoPaseAtencionRCViajero(){

	var tipoPase = jQuery("#s_tipoPaseAtencion").val();
	jQuery("#h_tipoPaseAtencion").val(tipoPase);
	
	if( jQuery("#s_tipoPaseAtencion").val() == "RAC" ){
		jQuery("#tr_companiaSeguros").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#txt_medico").val("");
		jQuery("#h_medico").val("");
		jQuery("#ch_otroHospital").attr('checked', false);
		jQuery("#txt_otroHospital").val("");
		jQuery("#td_hospital").hide("slow");
		try{
			jQuery("#b_hospital").hide("slow");
		}catch (e) {
		}
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#td_numerosiniestro").show("slow");
		
	}else{
		jQuery("#txt_companiaSegurosTercero").val("");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		jQuery("#td_otroHospital").show("slow");
		jQuery("#td_numerosiniestro").hide("slow");
		jQuery("#txt_otroHospital").val("");
		try{
			jQuery("#b_hospital").show("slow");
		}catch (e) {
		}
	}
}

function validaHospitalMedico(){
	var tipoEstimacion =  jQuery("#h_tipoEstimacion").val();
	var hospital = jQuery("#txt_hospital").val();
	var otroHospital = jQuery("#txt_otroHospital").val();
	var medico = jQuery("#txt_medico").val();
	var valida = false;
	var error = true;
	var mensaje = "Debe Ingresar Hospital, Otro Hospital o M\u00e9dico";
	
	if( tipoEstimacion == 'GME' ){
		valida = true;
	}else if( tipoEstimacion == 'RCP' || tipoEstimacion == 'GMC' || tipoEstimacion == 'RCJ' ){
		
		if( tipoEstimacion == 'GMC' &&  jQuery("#s_tipoPaseAtencion").val() == 'RAC' && jQuery("#s_estadoPersona").val() == "" ){
			mensaje = "Debe seleccionar el estado: Homicidio o Lesi\u00f3n";
			valida = true;
		}else if( jQuery("#s_tipoPaseAtencion").val() != 'RAC'){
			valida = true;
		}
	}
	
	if( hospital == "" && otroHospital == "" && medico == "" && valida == true && jQuery("#s_tipoPaseAtencion").val() != 'SRE' ){
		parent.mostrarVentanaMensaje("20",mensaje,null);
		error = false;
	}
	
	return error;
}

function validaTipoPaseAtencion() {
	var tipoEstimacion =  jQuery("#h_tipoEstimacion").val();
	var valida = true;
	if( tipoEstimacion == 'RCP' || tipoEstimacion == 'GME' || tipoEstimacion == 'GMC' || tipoEstimacion == 'RCJ' ){
		if(jQuery("#s_estadoPersona").val() == "LES"){
			if(isEmpty(jQuery("#s_tipoAtencion").val())){
				valida = false;
				mostrarVentanaMensaje("20","Debe seleccionar Tipo de Atenci\u00F3n",null);
			}
		}
	}
	
	return valida;
}



function onChangeEstatus(){
	jQuery("#h_estatus").attr("value",jQuery("#s_estatus").val());
}

function inicioViajero(){
	if( jQuery("#s_estadoPersona").val() == "LES" ){
		jQuery("#td_tipoAtencion").show("slow");
	}else{
		jQuery("#td_tipoAtencion").hide("slow");
	}
	
	if( jQuery("#s_tipoPaseAtencion").val() == "RAC" ){
		jQuery("#tr_companiaSeguros").show("slow");
		jQuery("#td_hospital").hide("slow");
		jQuery("#td_medico").hide("slow");
		jQuery("#td_otroHospital").hide("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#tr_tieneCompaniaSeguros").show("slow");
		jQuery("#td_numerosiniestro").show("slow");
		
	}else{
		jQuery("#tr_tieneCompaniaSeguros").hide("slow");
		jQuery("#tr_companiaSeguros").hide("slow");
		jQuery("#td_hospital").show("slow");
		jQuery("#td_medico").show("slow");
		if( jQuery("#ch_otroHospital").attr('checked') == true){
			jQuery("#td_nombreOtroHospital").show("slow");
		}else{
			jQuery("#td_nombreOtroHospital").hide("slow");
		}
		jQuery("#td_numerosiniestro").hide("slow");
	}
	
	if( jQuery("#txt_otroHospital").val() != ""  ){
		jQuery("#td_nombreOtroHospital").show("slow");
		jQuery("#txt_hospital").val("");
		jQuery("#h_hospital").val("");
		jQuery("#td_hospital").hide("slow");
		
		jQuery("#ch_otroHospital").attr('checked', true);
	}else if(jQuery("#s_tipoPaseAtencion").val() != "RAC"){
		jQuery("#td_hospital").show("slow");
		jQuery("#td_nombreOtroHospital").hide("slow");
		jQuery("#ch_otroHospital").attr('checked', false);
	}
	
}

function setReturn(){
	jQuery("#btn_cerrar").replaceWith('<a id="btn_cerrar" onclick="javascript: backToList();" href="javascript: void(0);">Cerrar<img border="0px" src="/MidasWeb/img/b_anterior.gif" title="Cerrar" alt="Cerrar"></a>');
}

function setReturnToReport(){
	jQuery("#btn_cerrar").replaceWith('<a id="btn_cerrar" onclick="javascript: backToListFromReport();" href="javascript: void(0);">Cerrar<img border="0px" src="/MidasWeb/img/b_anterior.gif" title="Cerrar" alt="Cerrar"></a>');
}

function backToList(){
	url="/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoPasesAtencion.action";
	sendRequestJQ(null, url, 'contenido', '');
}


function backToListFromReport(){
	var idReporte = jQuery("#h_reporteCabinaId").val();
	var url="/MidasWeb/siniestros/cabina/reportecabina/mostrarPasesDeAtencionDeReporte.action?paseAtencion.idReporteCabina="+idReporte;
	sendRequestJQ(null, url, 'contenido', '');
}



function recibidaOrdenCia(){
	if( jQuery("#h_recibidaOrdenCia").val() != null && jQuery("#h_recibidaOrdenCia").val() == "N" ){
		jQuery("#td_folioCiaSeguros").hide("slow");
		jQuery("#td_numeroSiniestro").hide("slow");	
	}
}

function validaNoSerie(){
	console.log('->validaNoSerie<-');
	var txtNoSerie = jQuery("#txt_noSerie").val();
	var idEstimacion = jQuery('#idEstimacionCoberturaReporteCabina').val();
	console.log('txtNoSerie = '+txtNoSerie);
	if(!isEmpty(txtNoSerie))	{
		console.log('Llama Metodo : estaNumeroDeSerieinvolucradoEnSiniestrosAntiguos');
		//var data=jQuery("#estimacionForm").serialize();		
		var data= 'idEstimacionCoberturaReporte='+idEstimacion+'&estimacionCoberturaSiniestro\.estimacionVehiculos\.numeroSerie='+txtNoSerie;
		console.log('data : '+data);
      jQuery.ajax({
            url: '/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/estaNumeroDeSerieinvolucradoEnSiniestrosAntiguos.action?',
            dataType: 'json',
            async:true,
            type:"POST",
            data: data,
            success: function(json){
            	 var result = json.tieneAntiguedadELNoSerie;
            	 console.log('Resultado de JSON :'+result);
            	 jQuery('#tieneAntiguedadELNoSerie').val(result);
            	 muestraMensajeNoSerie();
            }
      });
	}
}

function muestraMensajeNoSerie(){
	var tieneAntiguedad = jQuery('#tieneAntiguedadELNoSerie')?jQuery('#tieneAntiguedadELNoSerie').val():null;
	if(tieneAntiguedad && tieneAntiguedad == 'true'){
		jQuery('#mensajeNoSerie').show();
	}else{
		jQuery('#mensajeNoSerie').hide();
	}
}


function cambioTieneCiaSeguros(obj, textoCiaSeguro, idCiaSeguro, divCiaSeguro){
	var value = obj.value;
	console.log('obj.value : '+obj.value);
	if(value != null && (value == 'N' || value == '')){
		jQuery('#'+textoCiaSeguro).val('');
		jQuery('#'+idCiaSeguro).val('');
		jQuery('#'+divCiaSeguro).hide();
	}else if(value != null){
		jQuery('#'+divCiaSeguro).show();
	}
}

function cambioTieneCiaSegurosSre(){
	var divCiaSeguro = 'td_ciaSeguros';
	var value = jQuery('#s_tieneCiaSeguros').val();
	if(value != null && (value == 'N' || value == '')){
		jQuery('#'+divCiaSeguro).hide();
	}else if(value != null){
		jQuery('#'+divCiaSeguro).show();
	}
}


function initDeducible(){
	
	var aplicaDeducible = jQuery("#s_aplicaDeducible").val();
	
	// ESCRIBRE EN HIDDEN VALOR QUE VIENE POR DEFAULT
	jQuery("#h_aplicaDeducible").attr("value",aplicaDeducible);
	
	// NUEVO PASE
	if( aplicaDeducible == "" ){
		jQuery(".montoDeducible").hide();
		jQuery(".ctgMotivoNoaplicaDeducible").hide();
		
	}else{
		// PASE EXISTENTE
		if (aplicaDeducible == "S"){
			
			jQuery(".montoDeducible").show();
			jQuery(".ctgMotivoNoaplicaDeducible").hide();
			
		}else if (aplicaDeducible == "N"){
 
			jQuery(".montoDeducible").hide();
			
			// SI EL ID DE LA ESTIMACION ES NULO INDICA QUE PASE NO EXISTE Y SE ESCONDE EL COMBO DE MOTIVOS
			if( jQuery("#idEstimacionCoberturaReporteCabina").val()== "" ){
				jQuery(".ctgMotivoNoaplicaDeducible").hide();
			}
			
			if( jQuery("#h_motivoDeducible").val() != "" ){
				jQuery(".ctgMotivoNoaplicaDeducible").show();
			}
			
		}
	}
	
}


function calcularDeducibleCristales(){
	
	var porcentaje;
	var sumaDeducible;
	
	// VALIDAR SI APLICA COBERTURA CRISTALES
	if( jQuery("#h_estimacionCobertura").val() == "CS_5" && jQuery("#txt_estimacionNueva").val() != null && jQuery("#txt_estimacionNueva").val() != '' ){
		
		removeCurrencyFormatOnTxtInput();
		
		// SI EL PORCENTATE NO ES NULO INDICA QUE getClaveTipoDeducible ES 1 (PORCENTAJE) Y LA CAUSA DE SINIESTRO ES CRISTALES
		porcentaje = parseFloat(jQuery("#h_porDeducible").val());
		// OBTENER VALOR SE ESTIMACION CAPTURADA
		
		sumaDeducible = porcentaje * parseFloat(jQuery("#txt_estimacionNueva").val());
		
		jQuery("#montoDeducible").attr("value",sumaDeducible);
		
		initCurrencyFormatOnTxtInput();
		
		
	}
}


function calcularDeducibleValorComercial(){
	
	var porcentaje;
	var sumaDeducible;
	
	// SI h_porDeducible ES NULO INDICA QUE NO VIEDE DE LA COBERTURA DE CRISTALES
	if( jQuery("#h_porDeducible").val() == ""){
	
		removeCurrencyFormatOnTxtInput();
		// SI EL PORCENTATE NO ES NULO INDICA QUE getClaveTipoDeducible ES 1 (PORCENTAJE) Y LA CAUSA DE SINIESTRO ES CRISTALES
		porcentaje = parseFloat(jQuery("#h_porDeducibleValorComercial").val());
		
		// OBTENER VALOR SE ESTIMACION CAPTURADA
		sumaDeducible = porcentaje * parseFloat(jQuery("#txt_sumaAseguradaObtenida").val());
		
		jQuery("#montoDeducible").attr("value",sumaDeducible);
		initCurrencyFormatOnTxtInput();
	}
	
}

function copiarValorEstimacionActual(){
	if(jQuery("#txt_estimacionActual").val()){
		jQuery("#txt_estimacionNueva").val(jQuery("#txt_estimacionActual").val());
	}
}

function onChangeEmailNoProporcionado(){
    var emailNoProporcionado = jQuery("#emailNoProporcionado").attr('checked');
    var emailNoProporcionadoDisabled = jQuery("#emailNoProporcionado").attr('disabled');
    if(emailNoProporcionado){
          if(!emailNoProporcionadoDisabled){
                jQuery("#emailNoProporcionadoMotivo").removeAttr('disabled');
                jQuery("#txt_email").val('');
                jQuery("#txt_correo").val('');
          }     
    }else{
          jQuery("#emailNoProporcionadoMotivo").val('');
          jQuery("#emailNoProporcionadoMotivo").attr('disabled','disabled');
    }
}

function onBlurEmailInteresado(){
    var emailInteresado = jQuery("#txt_email").val();
    var emailInteresado2 = jQuery("#txt_correo").val();
    if(emailInteresado || emailInteresado2){
          var emailNoProporcionado = jQuery("#emailNoProporcionado").attr('checked',false);
          onChangeEmailNoProporcionado();
    }
}

function validaSiPermiteCapturarCiaInfo(){
	console.log('validaSiPermiteCapturarCiaInfo');
	var esPermitido = jQuery("#h_permiteCapturarCiaInfo").val();
	var val = jQuery('#s_tipoPaseAtencion').val();
	console.log('h_permiteCapturarCiaInfo: '+esPermitido);
	console.log('tipoPaseAtencion: '+val);
	if(esPermitido == 'true'  &&  val == 'SRE'){
		jQuery("#info_compania_id").hide();
		jQuery("#td_numeroSiniestro_estimacion").show();
		jQuery("#td_numeroSiniestro").hide();
		jQuery("#txt_noSiniestroTercero_estimacion").attr('disabled', false);
		jQuery("#txt_noSiniestroTercero").attr('disabled', true);
		jQuery("#td_ciaSeguros").show();
	}else{
		jQuery("#info_compania_id").show();
		jQuery("#td_numeroSiniestro_estimacion").hide();
		jQuery("#td_numeroSiniestro").show();
		jQuery("#txt_noSiniestroTercero_estimacion").attr('disabled', true);
		jQuery("#txt_noSiniestroTercero").attr('disabled', false);
		jQuery("#td_ciaSeguros").hide();
	}
	var esConsulta = jQuery('#h_consultaPase').val();
	if(esConsulta == 1){
		jQuery("#txt_noSiniestroTercero_estimacion").attr('disabled', true);
		jQuery("#txt_noSiniestroTercero").attr('disabled', true);
	}
	
	if(jQuery('#h_permiteCapturarNoSiniestroTercero').val() == "true"){
		jQuery("#td_numeroSiniestro").hide();
		jQuery("#txt_noSiniestroTercero").attr('disabled', true);
		jQuery("#td_numeroSiniestro_estimacion").show();		
		jQuery("#txt_noSiniestroTercero_estimacion").removeAttr('readonly')
		.attr('disabled', false).removeClass('elegible').removeClass('elegibleSRECIA').show();	
		 
	}
	
	if(val == 'SIPAC'){
		jQuery("#s_tieneCiaSeguros").addClass('requeridoSIPAC');
		jQuery("#s_tieneCiaSeguros").val('S');		
		cambioTieneCiaSeguros(jQuery("#s_tieneCiaSeguros"), 
				'txt_companiaSegurosTercero', 'txt_idcompaniaSegurosTercero', 'td_ciaSeguros');		
		jQuery("#txt_companiaSegurosTercero").addClass('requeridoSIPAC');
	} else {
		jQuery("#s_tieneCiaSeguros").removeClass('requeridoSIPAC');
		jQuery("#txt_companiaSegurosTercero").removeClass('requeridoSIPAC');
	}
	
}

function validaSiPermiteCapturarCiaInfoRCP(){
	console.log('validaSiPermiteCapturarCiaInfoRCP');
	var esPermitido = jQuery("#h_permiteCapturarCiaInfo").val();
	var val = jQuery('#s_tipoPaseAtencion').val();
	console.log('h_permiteCapturarCiaInfo: '+esPermitido);
	console.log('tipoPaseAtencion: '+val);
	if(esPermitido == 'true'  &&  val == 'SRE'){
		jQuery("#txt_numeroSiniestroTercero_sre").attr('disabled', false);
		jQuery("#txt_numeroSiniestroTercero").attr('disabled', true);

	}else{
		jQuery("#txt_numeroSiniestroTercero_sre").attr('disabled', true);
		jQuery("#txt_numeroSiniestroTercero").attr('disabled', false);
	}
	var esConsulta = jQuery('#h_consultaPase').val();
	if(esConsulta == 1){
		jQuery("#txt_numeroSiniestroTercero_sre").attr('disabled', true);
		jQuery("#txt_numeroSiniestroTercero").attr('disabled', true);
	}
}

function validaInfoCiaParaGuardar(){
	console.log('Entra validaInfoCiaParaGuardar');
	var res = true;
	var cia = "";
	var esPermitido = jQuery("#h_permiteCapturarCiaInfo").val();
	var val = jQuery('#s_tipoPaseAtencion').val();

	if(esPermitido == 'true'  &&  val == 'SRE'){
		var requiereCia = jQuery('#s_tieneCiaSeguros').val();
		if(requiereCia=='S'){
			cia = jQuery('#txt_companiaSegurosTercero_sre').val()
			if(cia == ''){
				jQuery("#txt_companiaSegurosTercero_sre").addClass("errorField"); 
				jQuery("#txt_companiaSegurosTercero_sre").removeClass("deshabilitado"); 	
				return false;
			}else{
				jQuery("#txt_companiaSegurosTercero_sre").removeClass("errorField"); 	
				jQuery("#txt_companiaSegurosTercero_sre").addClass("deshabilitado"); 
			}
		}

	}
	return res;
}

function validaNoSerieCESVI(){
	var txtNoSerie = jQuery("#txt_noSerie").val();
	var idEstimacion = jQuery('#idEstimacionCoberturaReporteCabina').val();
	if(!isEmpty(txtNoSerie))	{
		var data= 'idEstimacionCoberturaReporte='+idEstimacion+'&estimacionCoberturaSiniestro\.estimacionVehiculos\.numeroSerie='+txtNoSerie;
      jQuery.ajax({
            url: '/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/validarCESVI.action?',
            dataType: 'json',
            async:true,
            type:"POST",
            data: data,
            success: function(json){
            	 var result = json.reponseDeCESVI;
            	 muestraMensajeNoSerie(result);
            }
      });
	}
}

function muestraMensajeNoSerie(result){
	if(result && !isEmpty(result))	{
		jQuery('#mensajeNoSerieCESVI').show();
		jQuery("label[for='respuestaCESVI']").html("CESVI: "+result);
		console.log(result);
	}
}
	

function verificarTerminosAjuste() {
	var terminoAjuste = jQuery('#h_terminoAjuste').val();
	
	if (terminoAjuste == 'SRSIPAC' 
		|| terminoAjuste == 'SRSIPACYRC' 
			|| terminoAjuste == 'SESIPACYRC' 
				|| terminoAjuste == 'SEONASIPAC' 
					|| terminoAjuste == 'SEONASIPTR' 
						|| terminoAjuste == 'SEONASIOCT' ) {
			jQuery('.campoSIPAC').show();
			jQuery('.campoSIPAC').children().find('input[type=text]').addClass('requeridoPAT');
			jQuery('.campoSIPAC').children().find('select').addClass('requeridoPAT');
			jQuery('.campoSIPAC').children().find('input[type=text]').addClass('requeridoPDA');
			jQuery('.campoSIPAC').children().find('select').addClass('requeridoPDA');
			validarCampoCircunstancia('DM');
	} else {
		jQuery('.campoSIPAC').hide(); 
		jQuery('.campoSIPAC').children().find('input[type=text]').removeClass('requeridoPAT');
		jQuery('.campoSIPAC').children().find('select').removeClass('requeridoPAT');
		jQuery('.campoSIPAC').children().find('input[type=text]').removeClass('requeridoPDA');
		jQuery('.campoSIPAC').children().find('select').removeClass('requeridoPDA');
		
	}
}

function validarCampoCircunstancia(tipo) {
	
	var tiposPase = new Array();
	if(tipo=='DM'){
		tiposPase.push('PDA');
		tiposPase.push('PAT');
	}else if(tipo=='RCV'){
		tiposPase.push('SIPAC');
		tiposPase.push('SIPACSRE');
	}
		
	if( jQuery("#ckb_otraCircunstancia").is(':checked') ) 
	{    
		 //Cambiar los requerimientos de los campos relativos a las circunstancias
		 tiposPase.each(function(tipoPase, index){
			 jQuery("#txt_otraCircunstancia_detalle").addClass('requerido' + tipoPase);
			 jQuery("#s_circunstancia").removeClass('requerido' + tipoPase);
		 });
		 
		 jQuery("#txt_otraCircunstancia_detalle").show(); 
		 
		 
		 
		 //Habilitar las circunstancias introducidas por el usuario.
		 if (jQuery("#txt_otraCircunstancia_detalle").is(':disabled'))
		 {
			 jQuery("#txt_otraCircunstancia_detalle").attr('disabled',false);
		 }
		 
	}
	else 
	{
		 
		 //Deshabilitar las circunstancias introducidas por el usuario.
		 if ( !(jQuery("#txt_otraCircunstancia_detalle").is(':disabled')) ) 
		 {
			 jQuery("#txt_otraCircunstancia_detalle").attr('disabled',true);
	     }
		 
		 tiposPase.each(function(tipoPase, index){
			 jQuery("#txt_otraCircunstancia_detalle").removeClass('requerido' + tipoPase);
			 jQuery("#s_circunstancia").addClass('requerido' + tipoPase);
		 });
		 jQuery("#txt_otraCircunstancia_detalle").hide();
	}
}

function validarDanosPrexistentes() {
	
	if( !((jQuery("#ckb_danosPrexistentes").is(':checked'))) ) {
		jQuery("#area_danosPrexistentes").hide();
		jQuery("#ta_danosPreexistentes").hide();
	}
	else {
		jQuery("#area_danosPrexistentes").show();
		jQuery("#ta_danosPreexistentes").show();
	}
	
}


function obtenerEstimacionNueva(){
	
	var tipoPase = jQuery('#s_tipoPaseAtencion')?jQuery('#s_tipoPaseAtencion').val():null;
	
	if(tipoPase && tipoPase == 'SIPAC'){
		
		var params = 'estimacionCoberturaSiniestro.estimacionVehiculos.tipoTransporte=' + jQuery('#s_tipoTransporte').val();
		params += '&estimacionCoberturaSiniestro.estimacionVehiculos.tipoPaseAtencion=' + tipoPase;
		
		jQuery.ajax({
	          url: "/MidasWeb/siniestros/cabina/reporteCabina/estimacioncobertura/findEstimacionNueva.action?" + params,
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: {
	        	  tipoEstimacion:jQuery('#h_tipoEstimacion').val(),
	        	  idCoberturaReporteCabina:jQuery('#h_idCoberturaReporteCabina').val()
	          },
	          success: function(data){
	        	 unblockPage();
	        	 if(data.estimacionNueva){
	        		 jQuery('#txt_estimacionNueva').val(data.estimacionNueva);
	        		 mascaraDecimales('#txt_estimacionNueva',data.estimacionNueva)
	        	 }
	         	
	          },
	          input: function(json){
	        	  mostrarMensajeInformativo(json.mensaje, '20');
	        	  unblockPage();
	          },
	          beforeSend: function(){
					blockPage();
	          },
	          complete: function(){
	        	  unblockPage();
	          }
	    });
	}
}
		
		
