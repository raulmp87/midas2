var map;


function guardarLugarMidas( isConfirmarUbicacion , param ) {
	jQuery("#isConfirmarUbicacion").val(isConfirmarUbicacion);
	
	var referencia = jQuery("#lugarAtencionOcurrido\\.referencia").val(); 

	// ELIMINAR SALTOS DE LINEA
	referencia = referencia.replace(/(\r\n|\n|\r)/gm,"");
	
	if( referencia.length > 200  ){
		alert("La longitud de la referencia es demasiado larga, máxima permitida 200 caracteres, ingresados: "+referencia.length);
	}else{
		if( isConfirmarUbicacion ){
			if ( jQuery("#latitud").val() || jQuery("#longitud").val()) {
				if( confirm("\u00BFDesea guardar esta posici\u00F3n, como actual para el siniestro?") ){
					parent.submitVentanaModal('vm_LugarAttOcc', document.lugarForm);
				}
			} else {
				alert('Debe de guardar la direcci\u00F3n para poder confirmar la ubicaci\u00F3n.');
			}
		} else {
			parent.submitVentanaModal('vm_LugarAttOcc', document.lugarForm);
		}
	}
	

}

function cerrarVentanaLugar(){
	parent.cerrarVentanaModal('vm_LugarAttOcc');
}

function configurarTitulo(){
	var tipoLugar = jQuery('#tipoLugar').val();
	var idLugarAtencionOcurrido = jQuery('#idLugarAtencionOcurrido').val();
	if(tipoLugar == "AT"){
		jQuery('#tituloAtencion').show();
	}else{
		jQuery('#tituloOcurrido').show();
	}
	if (idLugarAtencionOcurrido == ''){
		jQuery('#zona_s').val('CD');	
	}
}

//Funcionalidad ejecutada al cambiar el combo de zona. 
function onChangeZona(){
	var zona = jQuery('#zona_s').val();
//		ESCONDER info que no sea compartidad por Ciudad y Carretera
		jQuery('#lugarAtencionOcurrido\\.calleNumero').parent().parent().parent().parent().hide();
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').parent().parent().parent().parent().hide();
//		jQuery('#dir\\.num').parent().parent().parent().parent().hide();
		jQuery('#codigoPostalOtraColonia_t').parent().parent().parent().parent().hide();
		jQuery('#kilometro_t').parent().parent().parent().parent().hide();
		jQuery('#nombreCarretera_t').parent().parent().parent().parent().hide();
	if( zona == 'CA'){
//		MOSTRAR info Carretera
		jQuery('#kilometro_t').parent().parent().parent().parent().show();
		jQuery('#nombreCarretera_t').parent().parent().parent().parent().show();
		jQuery('#tipoCarretera_s').parent().parent().show(); 
//		ESCONDER info Ciudad
		jQuery('#lugarAtencionOcurrido\\.calleNumero').parent().parent().parent().parent().hide();
		jQuery('#lugarAtencionOcurrido\\.calleNumero').val('');
		jQuery('#lugarAtencionOcurrido\\.otraColonia').val('');
		jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').val(false);
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').parent().parent().parent().parent().hide();
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').val('');
		jQuery('#lugarAtencionOcurrido\\.colonia\\.id').val('');
		jQuery('#codigoPostalOtraColonia_t').parent().parent().parent().parent().hide();
		jQuery('#codigoPostalOtraColonia_t').val('');
	}else if (zona == 'CD'){
//		ESCONDER info Carretera
		jQuery('#kilometro_t').parent().parent().parent().parent().hide();
		jQuery('#kilometro_t').val('');
		jQuery('#tipoCarretera_s').val('');
		jQuery('#nombreCarretera_t').val('');
		jQuery('#nombreCarretera_t').parent().parent().parent().parent().hide();
//		MOSTRAR info Ciudad
		jQuery('#lugarAtencionOcurrido\\.calleNumero').parent().parent().parent().parent().show();
		var coloniaCheck = jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked');
		if(coloniaCheck == true){
			jQuery('#codigoPostalOtraColonia_t').parent().parent().parent().parent().show();
		}else if(coloniaCheck == false){
			jQuery('#lugarAtencionOcurrido\\.otraColonia').attr('readonly',true);
			jQuery('#lugarAtencionOcurrido\\.codigoPostal').parent().parent().parent().parent().show();
		}
	}
}

// Cambia la funcionalidad del check, para ocultar y mostrar campos al momento de editar la colonia.
function changeColoniaCheckEvent(){
	jQuery( "#lugarAtencionOcurrido\\.idColoniaCheck" ).click(
			function() {
				onClickColoniaCheck();
		  }
		);
}

// Nueva funcionalidad del checkbox de Nueva Colonia
function onClickColoniaCheck(){
	var coloniaCheck = jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked');
	if( coloniaCheck == true){
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').val('');
		jQuery('#lugarAtencionOcurrido\\.colonia\\.id').val('');
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').parent().parent().parent().parent().hide();
		jQuery('#codigoPostalOtraColonia_t').parent().parent().parent().parent().show();
		jQuery('#lugarAtencionOcurrido\\.otraColonia').attr('readonly',false);
	}else if ( coloniaCheck == false){
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').val('');
		jQuery('#lugarAtencionOcurrido\\.codigoPostal').parent().parent().parent().parent().show();
		jQuery('#codigoPostalOtraColonia_t').val('');
		jQuery('#codigoPostalOtraColonia_t').parent().parent().parent().parent().hide();
		jQuery('#lugarAtencionOcurrido\\.otraColonia').attr('readonly',true);
	}
}

function limpiarFormularioLugar(){
    jQuery('#lugarForm').each (function(){    	
            this.reset();
    });
}

function setMarkerSiniestro( callback , isConfirmarUbicacion){

	marcador = new Object();
	marcador.titulo = "Ubicación Siniestro";
	marcador.draggable = true;
	marcador.icon = ICON_REPORTE;
	marcador.latitud = jQuery("#latitud").val();
	marcador.longitud = jQuery("#longitud").val();

	crearMarcadorSiniestro(marcador);
	
	//obtener coordenadas de contacto en caso de que existan y pintar marcador verde de ajustador
	crearMarcadorContacto();
	

	if( callback )
		callback( isConfirmarUbicacion , marcador );	
}

/**
 * Generar marcador de contacto por parte del ajustador obtenido de las ocordenadas actualizadas por el movil
 */
function crearMarcadorContacto(){	
	var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerCoordenadasContactoAtencion.action";		
	var id = jQuery('#idToReporte').val();	
	var tipoLugar = jQuery('#tipoLugar').val();
	jQuery.getJSON(url,
			{idToReporte : id,
			tipoLugarSiniestro : tipoLugar}
			,function(ajustador){
				if(ajustador != null){
					if(ajustador.latitud != null && ajustador.latitud != ''){
						crearMarcadorAjustador(ajustador, id );
					}
				}
			}
	);						
}

function crearMarcadorSiniestro(marcador){
	id = "#lugarAtencionOcurrido\\.pais\\.id";
	var pais = jQuery(id +' option:selected').val();
	
	id = "#lugarAtencionOcurrido\\.estado\\.id";
	var estado = jQuery(id +' option:selected').val();
	
	id = "#lugarAtencionOcurrido\\.ciudad\\.id";
	var municipio = jQuery(id +' option:selected').val();
	
	var codigoPostal = jQuery('#lugarAtencionOcurrido\\.codigoPostal').val();
	
	var calle = jQuery('#lugarAtencionOcurrido\\.calleNumero').val();

	if( !marcador.latitud  || !marcador.longitud ){
		if( pais &&  estado && municipio && codigoPostal && calle ){
			var address = getDireccionSiniestro();
			createMarkerFromAddress( address , ponerMarcadorDeSiniestro);
		} else if( pais &&  estado && municipio ){
			var address = getDireccionSiniestro();
			createMarkerFromAddress( address , ponerMarcadorDeSiniestro);
		}
	} else {
		var map = getMap();
		marker = createMarker(marcador , map);

		setEventAndLatLng( marker );
		
		if( map ){
			map.setZoom(15);
			map.setCenter(marker.getPosition());
		}
			
	}
	
//	alert('System Error: No se localizaron las coordenadas con los parametros capturados');
}

function ponerMarcadorDeSiniestro( results, status ){
	marcador = new Object();
	marcador.titulo = "Ubicaci\u00F3n Siniestro";
	marcador.draggable = true;
	marcador.icon = ICON_REPORTE;
	
	if(status == google.maps.GeocoderStatus.OK){
		var map = getMap();
		console.log(results);
		map.setCenter(results[0].geometry.location);
		if( jQuery('#lugarAtencionOcurrido\\.codigoPostal').val() || jQuery('#codigoPostalOtraColonia_t').val() ){
			map.setZoom(15);
		} else {
			map.setZoom(6);
		}

		marcador.position = results[0].geometry.location;
		marker = createMarker(marcador);
		setEventAndLatLng( marker);
	}else{
		alert('No fue posible ubicar la dirección a través de la geolocalización: ' + status);
	}
}

function actualizarPosicionMarcador(marcador){
	jQuery("#latitud").val(marcador.getPosition().lat());
	jQuery("#longitud").val(marcador.getPosition().lng());
	setEvent(marcador, 'dragend', function() {
		jQuery("#latitud").val(marcador.getPosition().lat());
		jQuery("#longitud").val(marcador.getPosition().lng());
	});
}

function setEventAndLatLng( marcador ){
	if (!jQuery("#latitud").val()){
		jQuery("#latitud").val(marcador.getPosition().lat());
	}
	if (!jQuery("#longitud").val()){
		jQuery("#longitud").val(marcador.getPosition().lng());
	}

	setEvent(marcador, 'dragend', function() {
		jQuery("#latitud").val(marcador.getPosition().lat());
		jQuery("#longitud").val(marcador.getPosition().lng());
	});
}

function getDireccionSiniestro(){
	var direccion="",id;
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + ", ";
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + ", ";
	}	
	direccion+=getColoniaCPMunicipioPais();

	return direccion; 
}

function getColoniaCPMunicipioPais(){
	var direccion="",field="",id;
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){
		
		if( jQuery('#lugarAtencionOcurrido\\.otraColonia').val() ){
			id = "#lugarAtencionOcurrido\\.otraColonia";
			field=jQuery(id+' option:selected').text();

			if( field.trim() != "Seleccione..." )
				direccion+=field+ ", ";
		}
		
		direccion+=jQuery('#codigoPostalOtraColonia_t').val()+ ", ";
		
		id = "#lugarAtencionOcurrido\\.ciudad\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.estado\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.pais\\.id";
		direccion+=jQuery(id +' option:selected').text() + " ";
		
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		
		id = "#lugarAtencionOcurrido\\.colonia\\.id";
		field=jQuery(id +' option:selected').text();

		if( field.toString() != "Seleccione ..." )
			direccion+=field+ ", ";
		
		if( !isEmpty(jQuery('#lugarAtencionOcurrido\\.codigoPostal').val()) )
			direccion+=jQuery('#lugarAtencionOcurrido\\.codigoPostal').val() + ", ";
		
		id = "#lugarAtencionOcurrido\\.ciudad\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.estado\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.pais\\.id";
		direccion+=jQuery(id +' option:selected').text();
	}
	
	return direccion;
}

function getDireccionSiniestro2(){
	var direccion="",id;

	id = "#lugarAtencionOcurrido\\.pais\\.id";
	direccion+=jQuery(id +' option:selected').text() + " ";
	
	id = "#lugarAtencionOcurrido\\.estado\\.id";
	direccion+=jQuery(id +' option:selected').text() + " ";
	
	id = "#lugarAtencionOcurrido\\.ciudad\\.id";
	direccion+=jQuery(id +' option:selected').text() + " ";

	if( jQuery('#lugarAtencionOcurrido\\.otraColonia').val() ){
		id = "#lugarAtencionOcurrido\\.otraColonia";
		field=jQuery(id+' option:selected').text();

		if( field.trim() != "Seleccione..." )
			direccion+=field+ " ";
		
	} else {
		id = "#lugarAtencionOcurrido\\.colonia\\.id";
		field=jQuery(id +' option:selected').text();

		if( field.toString() != "Seleccione ..." )
			direccion+=field+ " ";
		
	}
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + " ";
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + " ";
	}
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){		
		direccion+=jQuery('#codigoPostalOtraColonia_t').val()+ " ";
		
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		
		if( !isEmpty(jQuery('#lugarAtencionOcurrido\\.codigoPostal').val()) )
			direccion+=jQuery('#lugarAtencionOcurrido\\.codigoPostal').val() + " ";
	}
	
	return direccion; 
}

function readyMap() {
	var map = getMap();
	if (map) {
		setMarkerSiniestro(null,null);
	} else {
		setTimeout("readyMap()", '1000');
	}
}

function cerrarVentanaLugar() {
	var soloConsulta = parent.jQuery("#h_soloConsulta").val();

	if (soloConsulta == null || soloConsulta != 1) {
		if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){
			parent.cerrarVentanaModal('vm_LugarAttOcc');
		}
	} else{
		parent.cerrarVentanaModal('vm_LugarAttOcc');
	}
	
}

