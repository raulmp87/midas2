var reportesSiniestrosGrid;
var serializedForm;

jQuery(document).ready(
	function(){
		realizarBusqueda(true);
		jQuery(".numeric").keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});

	}
);


function sortGridOnServer(ind,gridObj,direct){
	alert(ind);
	alert(gridObj);
	alert(direct);
	reportesSiniestrosGrid.clearAll();
	reportesSiniestrosGrid.load(gridQString+(gridQString.indexOf("?")>=0?"&amp;":"?")
     +"orderby="+ind+"&amp;direct="+direct);
    mygrid.setSortImgState(true,ind,direct);
    return false;
}

function realizarBusqueda(mostrarBusquedaVacia){
	//
	var url = "/MidasWeb/siniestros/cabina/reportecabina/buscarReportes.action?";
	serializedForm = encodeForm(jQuery("#formReporteSiniestroDTO"));
	var busquedaValidada =  validarBusqueda();
	
	if(mostrarBusquedaVacia || busquedaValidada == "s"){
		if(reportesSiniestrosGrid){reportesSiniestrosGrid.destructor();}
		reportesSiniestrosGrid = new dhtmlXGridObject('listadoReportesSiniestrosGrid');
		reportesSiniestrosGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		reportesSiniestrosGrid.setSkin('light');
		reportesSiniestrosGrid.setHeader("Oficina, Fecha, Número Siniestro, Número Reporte,Nombre del Ajustator, Número Póliza, Estatus, Contratante(s)/Razón Social, Nombre quien Reporta, Nombre del Conductor, Número de Serie, Acciones, , , , , , ");
		reportesSiniestrosGrid.setInitWidths("150,100,100,100,200,100,100,200,200,200,100,50,50,50,50,50,50,50");
		reportesSiniestrosGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center");
		reportesSiniestrosGrid.setColSorting("server,server,server,server,server,server,server,server,server,server,server,na,na,na,na,na,na,na");
		reportesSiniestrosGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img,img,img,img,img,img");			
		reportesSiniestrosGrid.enableMultiline(true);
		reportesSiniestrosGrid.init();
		reportesSiniestrosGrid.splitAt("6");	
		reportesSiniestrosGrid.attachEvent("onBeforePageChanged",function(){
    		if (!this.getRowsNum()) return false;
    		return true;
    	});		
		reportesSiniestrosGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		reportesSiniestrosGrid.setPagingSkin("bricks");	
		reportesSiniestrosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		reportesSiniestrosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		reportesSiniestrosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		reportesSiniestrosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });		
		if(!mostrarBusquedaVacia && busquedaValidada == "s"){
			url += serializedForm; 
		}		
		reportesSiniestrosGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
			server = url;			
			reportesSiniestrosGrid.clearAll();
			reportesSiniestrosGrid.load(server+(server.indexOf("?")>=0?"&":"?")
		     +"orderBy="+ind+"&direct="+direct);			
			reportesSiniestrosGrid.setSortImgState(true,ind,direct);
		    return false;
		});
		reportesSiniestrosGrid.load( url );
	} else{
		 mostrarMensajeInformativo('Capture al menos dos campos para realizar la búsqueda', '20');
	 }
}

/**
 * Funcion JS para exportar el excel del listado de reportes de siniestros
 */
function exportarExcelReporteSiniestro(){
	var url = "/MidasWeb/siniestros/cabina/reportecabina/exportarExcelReporteSiniestro.action?";
	var countRC= reportesSiniestrosGrid.getRowsNum() ;

	if(countRC==0){
		alert('No hay registros para exportar');
	}else{
		url=url+serializedForm;
		window.open(url, "Excel");	
	}
}


function realizarBusquedaRecuperaciones(modoAsignar){
	var serializedForm = encodeForm(jQuery("#formReporteSiniestroDTO"));
	 if ( validarBusqueda() == "s" ){
		if(reportesSiniestrosGrid){reportesSiniestrosGrid.destructor();}
		reportesSiniestrosGrid = new dhtmlXGridObject('listadoReportesSiniestrosGrid');
		reportesSiniestrosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		reportesSiniestrosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		reportesSiniestrosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
		});
		reportesSiniestrosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
		});
		reportesSiniestrosGrid.attachEvent("onRowSelect", function(id,ind){
			jQuery("#asignaNumSiniestro",window.parent.document).attr("value",reportesSiniestrosGrid.cells(id,2).getValue());
		 	jQuery("#asignaNumReporte"  ,window.parent.document).attr("value",reportesSiniestrosGrid.cells(id,3).getValue());
		 	jQuery("#idReporteCabina"  ,window.parent.document).attr("value",reportesSiniestrosGrid.cells(id,10).getValue());
		 	parent.cerrarVentanaModal("vm_busquedaRecuperacion","redireccionaBusquedaRecuperacion();");
		});
		 
		 var url = "/MidasWeb/siniestros/cabina/reportecabina/buscarReportesAsignacion.action?"+serializedForm+"&modoAsignar="+modoAsignar;
		 reportesSiniestrosGrid.load( url) ;
	 }else{
		 mostrarMensajeInformativo('Se debe ingresar al menos un campo para realizar la búsqueda', '20');
	 }
}


function validarBusqueda(){
	var bandera = "n";
	var contador = 0;
	jQuery(".cleaneable").each( function(){
		if( jQuery(this).val() != "" ){
			contador++;
		}
		if(contador >1){
			bandera = "s";
		}
	});
	
	return bandera;
	
}

function limpiar(){
	jQuery(".cleaneable").each(
		function(){
			jQuery(this).val("");
		}
	);
	realizarBusqueda(true);
}

function consultarReporte( claveOficina , consecutivoReporte , anioReporte ){
	var params = "";
	params+="nsOficina="+claveOficina+"&";
	params+="nsConsecutivoR="+consecutivoReporte+"&";
	params+="nsAnio="+anioReporte;
	var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?"+params;
	sendRequestJQ(null, url,"contenido", "setConsultaReporte();");
}


function editarReporte( claveOficina , consecutivoReporte , anioReporte ){
	var params = "";
	params+="nsOficina="+claveOficina+"&";
	params+="nsConsecutivoR="+consecutivoReporte+"&";
	params+="nsAnio="+anioReporte + "&";
	params += "ventanaOrigen=LR";
	
	var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?"+params
	sendRequestJQ(null, url,"contenido", "");
}

function setConsulta(){
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
	jQuery("#cancelar").remove();
	jQuery("#nuevo").remove();
	jQuery("#guardar").remove();
	jQuery("#contenido").append("<div id='consulta' style='display:none;' >1</div>");
}


function afectarReservaVacio(){
	var params = "";
	var url = "/MidasWeb/siniestros/reservas/mostrarAjusteDeReserva.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function afectarReserva(numeroSiniestro,numeroReporte,numeroPoliza,numeroInciso,idReporteCabina){
	var params = "numeroSiniestro="+numeroSiniestro+"&numeroReporte="+numeroReporte+"&numeroPoliza="+numeroPoliza+"&numeroInciso="+numeroInciso+"&idReporteCabina="+idReporteCabina;
	var url = "/MidasWeb/siniestros/reservas/mostrarAjusteDeReserva.action?"+params;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function imprimirReporte( idReporte ){
	var url="/MidasWeb/siniestros/cabina/reportecabina/imprimirReporteCabina.action?idToReporte="+idReporte;
	window.open(url, "ReporteCabina");
}

function mostrarListadoDeReporte(idReporte){
	var url="/MidasWeb/siniestros/cabina/reportecabina/mostrarPasesDeAtencionDeReporte.action?paseAtencion.idReporteCabina="+idReporte;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarGastoAjuste( idReporteCabina ){
	var id = idReporteCabina;
    if( !isEmpty(id) || id != 0 ){
    	sendRequestJQ(null, '/MidasWeb/siniestros/valuacion/ordencompra/mostrar.action'+ '?idReporteCabina=' + id+'&tipo=GA'+  '&origen=LISTADOREPORTE',"contenido", "");

    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    	
    	}
}




function mostrarOrdenesCompra( idReporteCabina ){
	var id = idReporteCabina;
    if( !isEmpty(id) || id != 0 ){
    	sendRequestJQ(null, '/MidasWeb/siniestros/valuacion/ordencompra/mostrar.action'+ '?idReporteCabina=' + id+'&tipo=OC'+  '&origen=LISTADOREPORTE',"contenido", "");

    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    	
    	}
}



