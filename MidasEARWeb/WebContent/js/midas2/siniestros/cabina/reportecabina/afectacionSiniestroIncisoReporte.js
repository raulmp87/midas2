var referenciasBancariasGrid;
var companiaSiniestrosGrid;

function verTabInciso(){
	console.log("en verTabInciso");
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	limpiarDivsGeneral();	
	sendRequestJQ(null, incisoPath + '?' + data, 'contenido_inciso',"setConsulta()");
	initCurrencyFormatOnTxtInput();
}

function verTabCierre(){
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	limpiarDivsGeneral();	
	sendRequestJQ(null, cierrePath + '?' + data, 'contenido_cierre',"setConsulta()");
	initCurrencyFormatOnTxtInput();
}

function cargaCombos(){
	onChangeCausaSiniestro('tipoResponsabilidad');
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_inciso');
	limpiarDiv('contenido_cierre');
}

function onChangeCausaSiniestro(target){
	var idCausa = jQuery("#causaSiniestro").val();
//	if(idCausa != null  && idCausa != headerValue){		
//		dwr.engine.beginBatch();
//		listadoService.obtenerCatalogoValorFijoByStr('TIPO_RESPONSABILIDAD', null,
//				function(data){
//					addOptions(target,data);
//				});
//		dwr.engine.endBatch({async:false});
//	}else{
//		addOptions(target,"");
//	}
	onChangeResponsabilidad(terminoAjuste,terminoSiniestro);

	// MUESTRA BOTON LLEGADA OTRA CIA SI ES COLISION_CON_TERCERO CS3
	validaBtnLLegadaOtraCia(idCausa);
}

function onChangeResponsabilidad(terminoAjuste,terminoSiniestro){
	var idCausa = jQuery("#causaSiniestro").val();
	var idRespon = jQuery("#tipoResponsabilidad").val();
	if(idRespon != null  && idRespon != headerValue && idCausa != null  && idCausa != headerValue){		
		dwr.engine.beginBatch();
		listadoService.obtenerTerminosAjuste(idCausa, idRespon,
				function(data){
					addOptions(terminoAjuste,data);
					addOptions(terminoSiniestro,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(terminoAjuste,"");
		addOptions(terminoSiniestro,"");
	}
}

function onChangeTerminoDeAjuste(element){
	var terminoAjuste = jQuery(element).val();
	var terminoSinestro = jQuery("#terminoSiniestro").val();
	if(terminoAjuste == 'ROC' || terminoAjuste == 'SRSIPACYRC'){
		jQuery("#recibidaOrdenCia").val('S');
	}
	if( terminoSinestro == ''  ){
		jQuery("#terminoSiniestro").val(terminoAjuste);
	}
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function guardar(){
	var mensaje = validaCamposRequeridos(eval(document.getElementById("siniestroDTO.esSiniestro").value));
	
	if( jQuery("#txt_placas").val() == "" ){
		mensaje +=", Placas";
	}
	removeCurrencyFormatOnTxtInput();	
	if( validateAll(true) && mensaje == "" ){
		if(validaSiRequiereCia()){
			var cadena = "";
			if( typeof jQuery("#llegaOtraCia").val() != "undefined" ){
				cadena = "&siniestroDTO.recibidaOrdenCia="+jQuery("#llegaOtraCia").val();
			}
			var data=jQuery("#siniestroCabinaForm").serialize()+cadena ;
			
			sendRequestJQ(null, guardarPath + '?' + data ,'contenido', null);
			mostrarCoberturasAfectacionInciso();
		}
	}else{
		mostrarMensajeInformativo("Campos Requeridos:" + mensaje, '20');
	}
	initCurrencyFormatOnTxtInput();
}

function validaSiRequiereCia(){
	var tieneCia = jQuery("#h_tieneCiaAsociada").val();
	var causaSiniestro = jQuery("#causaSiniestro").val();
	if(causaSiniestro=="CS_3"){
			if(tieneCia == 'true'){
				return true;
			}else{
				console.log('No Tiene');
				if(confirm("No se est\u00E1 registrando la compañia del tercero. \u00BFDesea continuar?")){
					return true;
				}else{
					return false;
				}
			}	
	}else{
		return true;
	}
}


function actualizaBanderaDeCia(valor){
	jQuery("#h_tieneCiaAsociada").val(valor);
}

function convertirSiniestro(){
	var mensaje = validaCamposRequeridos(true);
	if( mensaje == "" ){
		removeCurrencyFormatOnTxtInput();
		var data=jQuery("#siniestroCabinaForm").serialize();
		sendRequestJQ(null, convertirSiniestroPath + '?' + data ,'contenido', null);
	}else{
		mostrarMensajeInformativo("Campos Requeridos:" + mensaje, '20');
	}
}

function envioAHGSOCRA(){
	var data=jQuery("#siniestroCabinaForm").serialize();
	sendRequestJQ(null, enviarHGSOCRAPath + '?' + data ,'contenido', null);
}

function ordenCompra(){ 
    var id = jQuery("#idToReporte").val();
    var validOnMillis = jQuery("#h_validOnMillis").val();
	var recordFromMillis = jQuery("#h_recordFromMillis").val();
	var h_soloConsulta = jQuery("#h_soloConsulta").val();
	var modoConsulta = false;
	if (h_soloConsulta=="1"){
		modoConsulta=true;
	}
    if( !isEmpty(id) || id != 0 ){
    	 var url = '/MidasWeb/siniestros/valuacion/ordencompra/mostrar.action'+ '?idReporteCabina=' + id+ '&validOnMillis=' + validOnMillis + '&recordFromMillis=' + recordFromMillis+ '&tipo=OC'+'&modoConsulta='+modoConsulta ;
    	 sendRequestJQ(null,url,targetWorkArea,null);
    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    	}
}





function validaCamposRequeridos(esSiniestro){
	var mensaje 	= "";
	var conductor = jQuery("#conductor").val();
	var edad	= jQuery("#edad").val();
	var ladaTelefono	= jQuery("#ladaTelefono").val();
	var telefono 	= jQuery("#telefono").val();
	var tipoCotizacion 	= jQuery("#h_tipoCotizacion").val();
	//var ladaCelular	= jQuery("#ladaCelular").val();
	//var celular	= jQuery("#celular").val();
	var curp 	= jQuery("#curp").val();
	var rfc 	= jQuery("#rfc").val();
	var terminoSiniestro= jQuery("#terminoSiniestro").val();
	/// Se agregegan las nuevas validaciones para Mejora Siniestros autos --start
	var d_montoDanos= jQuery("#d_montoDanos").val();
	var s_motivoRechazo= jQuery("#s_motivoRechazo").val();
	var s_rechazoDesistimiento= jQuery("#s_rechazoDesistimiento").val();
	var observacionesRechazo= jQuery("#observacionesRechazo").val();
	var rechazo= jQuery("#terminoAjuste").val();
	var colorVehiculo = jQuery("#color").val();	
	var genero	= jQuery("#genero").val();
	var tipoLicencia	= jQuery("#tipoLicencia").val();
	var claveAmisSupervisionCampo= jQuery("#claveAmisSupervisionCampo").val();
	

	if(rechazo == "PEND"){
		if(claveAmisSupervisionCampo == null || claveAmisSupervisionCampo == "" || claveAmisSupervisionCampo == "0"){
			mensaje += " Supervision de Campo,";
		}	
	}
	
	if (rechazo=="RECH"){
		if(d_montoDanos == ""){
			mensaje += " Monto valuado de los da\u00f1os,";
		}
		if(s_motivoRechazo == ""){
			mensaje += " Motivo del rechazo,";
		}
		if(s_rechazoDesistimiento == ""){
			mensaje += " Se obtuvo desistimiento,";
		}
		if(observacionesRechazo == ""){
			mensaje += " Observaciones del rechazo,";
		}
		
	}
    if(colorVehiculo == ""){
		mensaje +=" Color del Vehiculo,";
	}
	
	if(genero == ""){
		mensaje +=" G\u00E9nero,";
	}
	
	if(tipoLicencia == ""){
		mensaje +=" Tipo Licencia,";
	}
	/// --end 
	
	if(terminoSiniestro == ""){
		mensaje += " Termino de Siniestro,";
	}

	if(conductor == ""){
		mensaje += " Conductor,";
	}
	if(edad == ""){
		mensaje += " Edad,";
	}
	if(ladaTelefono == "" || telefono == ""){
		mensaje += " Telefono,";
	}
	/*if(ladaCelular == "" || celular == ""){
		mensaje += " Celular,";
	}*/
	
	if (tipoCotizacion != 'TCSP') {
		if(curp == ""){
			mensaje += " CURP,";
		}
		if(rfc == ""){
			//mensaje += " RFC,";
		}
	}
	
	var terminoAjuste	= jQuery("#terminoAjuste").val();
	if(terminoAjuste){
		 mensaje += validaciones_adicionales_SIPAC(terminoAjuste);	
	}
	
	var recibidaOrdenCia	= jQuery("#llegaOtraCia").val();

	if(recibidaOrdenCia == "S" && (jQuery("#ciaSeguros").val() == "" || jQuery("#t_numeroSin").val() == "" 
		|| jQuery("#t_porcentaje").val() == "" || jQuery("#t_poliza").val() == "")){
		mensaje += " Se debe especificar los datos de la Compa\u00F1\u00EDaa";
	} else if ( recibidaOrdenCia == "S" && (jQuery("#t_porcentaje").val() <= 0 || jQuery("#t_porcentaje").val() > 100)) {
		mensaje += " El porcentaje debe participacion debe estar entre un rango del 1% al 100%";
	}
	
	
	if(esSiniestro){
		
		var condMismoAsegurado	= jQuery("#condMismoAsegurado").val();
		if(condMismoAsegurado == ""){
			mensaje += " Mismo Asegurado,";
		}

		var causaSiniestro	= jQuery("#causaSiniestro").val();
		if(causaSiniestro == ""){
			mensaje += " Causa del Siniestro,";
		}
		var tipoResponsabilidad	= jQuery("#tipoResponsabilidad").val();
		if(tipoResponsabilidad == ""){
			mensaje += " Tipo Responsabilidad,";
		}
		var terminoAjuste	= jQuery("#terminoAjuste").val();
		if(terminoAjuste == ""){
			mensaje += " T\u00E9rmino de Ajuste,";
		}
		var fugoTerceroResponsable	= jQuery("#fugoTerceroResponsable").val();
		if(fugoTerceroResponsable == ""){
			mensaje += " Fug\u00F3 Tercero Responsable,";
		}
		
		if(recibidaOrdenCia == ""){
			mensaje += " Recibimos Orden de Compa\u00F1\u00EDa,";
		}
		/*
		 * Se reactivara en recuperaciones
		 * var ciaSeguros	= jQuery("#ciaSeguros").val();
		if(ciaSeguros == ""){
			mensaje += " Compañia de Seguros,";
		}*/
		var unidadEquipoPesado	= jQuery("#unidadEquipoPesado").val();
		if(unidadEquipoPesado == ""){
			mensaje += " Particip\u00F3 Unidad Equipo Pesado,";
		}
		var terminoSiniestro	= jQuery("#terminoSiniestro").val();
		if(terminoSiniestro == ""){
			mensaje += " T\u00E9rmino de Siniestro,";
		}
	}
	
	if(mensaje != null){
		mensaje = mensaje.substring(0,mensaje.length-1);
	}
	return mensaje;
}

var listadoGrid;

/**
 * Muestra el listado de las Coberturas asociadas a un Inciso de una Poliza
 */
function mostrarCoberturasAfectacionInciso(){
	
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('coberturasAfectacionIncisoGrid');

	listadoGrid.attachEvent("onXLS", function(grid){	
		mostrarIndicadorCarga('indicador');	
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		ocultarIndicadorCarga('indicador');
		listadoGrid.forEachRow(function(id){				
			var cveCalculo = listadoGrid.cells(id,7).getValue();
			if(cveCalculo == 'SIGA'){ //se desea identificar en rojo la cobertura de tipo SIGA							
				listadoGrid.setRowTextStyle(id,"background-color:#FFA8A8");
			}				
		})
    });	
	var reporteCabinaId = jQuery("#idToReporte").val();
	var causaSiniestro = jQuery("#causaSiniestro").val();
	var terminoAjuste = jQuery("#terminoAjuste").val();
	var tipoResponsabilidad = jQuery("#tipoResponsabilidad").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	var url = mostrarCoberturasAfectacionIncisoPath + '?siniestroDTO.reporteCabinaId=' + reporteCabinaId + 
													  '&siniestroDTO.causaSiniestro=' + causaSiniestro +
													  '&siniestroDTO.terminoAjuste=' + terminoAjuste +
													  '&siniestroDTO.tipoResponsabilidad=' + tipoResponsabilidad + 
													  '&soloConsulta=' + soloConsulta;

	if( reporteCabinaId != null && causaSiniestro != null && terminoAjuste != null && tipoResponsabilidad != null ){
		jQuery("#contenedorFiltros").show("slow");
		
		listadoGrid.load( url, function(){
			listadoGrid.forEachRow(function(id){
				listadoGrid.cells(id,2).cell.className='heightCell';
			})
			listadoGrid.setColumnHidden(4,true);
		});
	}

}

function mostrarDatosRiesgoCoberturaAfectacion(idCobertura) {	
	document.getElementById("datosRiesgoCobertura").innerHTML = '';
	jQuery("#divDatosRiesgo").show();
	sendRequestJQ(null, datosRiesgoCoberturaPath + "?"  + "idCobertura=" + idCobertura 
			+ "&" + jQuery(document.siniestroCabinaForm).serialize(), 'datosRiesgoCobertura', null);
}


function regresarReporteBusquedaAfectacion(){
	var idToReporte 		= jQuery("#idToReporte").val();
	var soloConsulta 		= jQuery("#h_soloConsulta").val();
	var url = regresarReporteCabina + "?idToReporte="+idToReporte;
	var cerrar = true;

	if(soloConsulta != 1 ){
		if(!confirm('La informaci\u00f3n no guardada se perder\u00E1. \u00BFDesea continuar?')){
			cerrar = false;
		}
	}
	if( cerrar ){
		if(soloConsulta == 1 ){
			sendRequestJQ(null, url,targetWorkArea, "setConsultaReporte();");
		}else{
			sendRequestJQ(null, url,targetWorkArea, null);
		}
	}
}

function setConsulta(){
	if(jQuery("#h_soloConsulta").val() == 1 ){
		jQuery(".setNew").attr("disabled","disabled");
		jQuery("#btn_guardar").remove();
		jQuery("#btn_curp").remove();
		jQuery("#btn_rfc").remove();
		jQuery("#btn_convertirSiniestro").remove();	
		jQuery("#btn_rechazaSiniestro").remove();
		jQuery("#btn_terminarSiniestro").remove();		
		jQuery("#btn_cancelarSiniestro").remove();
		jQuery("#btn_actualizaEstatusValuacion").remove();	
	}
}

function mostrarEnviarValuacion(){
	var numValuacion = jQuery("#numeroValuacion").val(); 
	if(numValuacion == ""){
		var data=jQuery("#siniestroCabinaForm").serialize();
		var url = mostrarSolicitudValuacionPath + "?" + data;
		mostrarVentanaModal("vm_enviarValuacion", 'Seleccionar Valuador para el Env\u00EDo de la Solicitud',  1, 1, 600, 150, url, null);
		ocultarBotonCerrarVentanaModal("vm_enviarValuacion");
	}else{
		mostrarMensajeInformativo("La valuaci\u00f3n ya ha sido asignada", '20');
	}
}

function enviarSolicitudValuacionCrucero(){
	var valuadorSeleccionado = jQuery("#codigoValuador_s").val();
	var idAjustadorReporte = jQuery("#idAjustadorReporte").val();
	if(idAjustadorReporte){
		if (valuadorSeleccionado != "") {
			var idToReporte = jQuery("#idToReporte").val();
			var url = enviarValuacionPath + '?siniestroDTO.reporteCabinaId='
					+ idToReporte + "&codigoValuador=" + valuadorSeleccionado;

			redirectVentanaModal('vm_enviarValuacion', url, null);
				
		} else {
			alert('Debe seleccionar un valuador');
		}
	}else{
		alert('No se puede enviar la solicitud. Primero debe asignar un ajustador al reporte cabina');
	}
}

function abrirFortimax(){
	var numReporte=jQuery('#siniestroDTO\\.numeroReporte').val();
	var idToReporte=jQuery('#idToReporte').val();
	if(null!=numReporte && numReporte!=''){		
		ventanaFortimax('DOCS_AJUSTE','' , 'FORTIMAX',idToReporte,numReporte);
	}
}


function redireccionaSolicitudValuacion(){
	var url = jQuery("#urlEnvioValuacion").val();
	sendRequestJQ(null, url, 'contenido', null);
}

function cancelarSolicitud(){
	parent.cerrarVentanaModal('vm_enviarValuacion');
}

function cerrarEnviarSolicitudValuacion(idValuacion){
	jQuery("#numeroValuacion").val(idValuacion);
	parent.cerrarVentanaModal('vm_enviarValuacion');
}

function inicializarCondicionesEspecialesFromSiniestro(tipoValidacion){
	var url = buscarCondicionesEspecialesIncisoPath + '?incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
												      "&idReporte=" + jQuery("#idToReporte").val() + 
													  "&fechaReporteSiniestroMillis=" + jQuery("#h_fechaReporteSiniestroMillis").val() + 
													  "&idToPoliza=" + jQuery("#h_idPoliza").val()+
													  "&soloConsulta=" + jQuery("#h_soloConsulta").val()+
													  "&tipoValidacion=" + tipoValidacion;

	mostrarVentanaModal( "vm_condicionesEspecialesInciso", "Consulta de Condiciones Especiales de un Inciso", null, null, 800, 490, url , null);
}

function validaComboSeRecibioOrdenCia(){
	var termino = jQuery('#terminoSiniestro').val();
	if(termino == 'ROC'  || termino == 'SRSIPACYRC'){
		jQuery('#llegaOtraCia').val('S');
	}else{
		jQuery('#llegaOtraCia').val('N');
	}
	mostrarOcultarEnviarValuacion();
	mostrarOcultarCompanias();	
	mostrarOcultarSipac();
}

function mostrarOcultarEnviarValuacion(){
	
	var recibidaOrdenCia	= jQuery("#llegaOtraCia").val();
	var tipoResponsabilidad	= jQuery("#tipoResponsabilidad").val();
	var esSiniestro			= jQuery("#esSiniestro").val();
	
	if(tipoResponsabilidad == 'AFCT'){
		jQuery("#solicitaValuacion").css('display','inline');
	}else{
		jQuery("#solicitaValuacion").css('display','none');
	}
}

function mostrarOcultarSipac(){
	if(jQuery("#terminoAjuste").val() == "SRSIPAC" || 
			jQuery("#terminoAjuste").val() == "SRSIPACYRC" ||
			jQuery("#terminoAjuste").val() == "SESIPAC" ||
			jQuery("#terminoAjuste").val() == "SESIPACYRC" ||
			jQuery("#terminoAjuste").val() == "SEONASIPAC" ||
			jQuery("#terminoAjuste").val() == "SEONASIPTR" ||
			jQuery("#terminoAjuste").val() == "SEONASIOCT" ||
			jQuery("#terminoAjuste").val() == "SEOSIOCT" ||
			jQuery("#terminoAjuste").val() == "SESIPACTER" ||
			jQuery("#terminoAjuste").val() == "SESIPYRECM"
			) {
		document.getElementById("contenidoSIPAC").style.display = "table-row";
	}else{
		document.getElementById("contenidoSIPAC").style.display = "none";
	}
}

function validaciones_adicionales_SIPAC(terminoAjuste) {
	var mensajeSIPAC='';
	if	   (terminoAjuste == "SRSIPAC" || 
			terminoAjuste == "SRSIPACYRC" ||
			terminoAjuste == "SESIPAC" ||
			terminoAjuste == "SESIPACYRC" ||
			terminoAjuste == "SEONASIPAC" ||
			terminoAjuste == "SEONASIPTR" ||
			terminoAjuste == "SEONASIOCT" ||
			terminoAjuste == "SEOSIOCT" ||
			terminoAjuste == "SESIPACTER" ||
			terminoAjuste == "SESIPYRECM" ) {

	    var fechaExpedicion = jQuery("#fechaExpedicion").val();
		if (fechaExpedicion == "") {
			mensajeSIPAC += "Fecha expedición, ";
		}
					
		var origen = jQuery("#origenSiniestro").val();
		if (origen == ""){
			mensajeSIPAC += "Origen, ";
		} else {
			var fechaDictamen = jQuery("#fechaDictamen").val(); 
			if (origen == "J") {
				var fechaJuicio  = jQuery("#fechaJuicio").val();
						
				if (fechaDictamen == "" && fechaJuicio ==  "") {
					mensajeSIPAC += "Fecha Dictamen, Fecha Juicio, ";
				}
				else if (fechaDictamen == "" && !fechaJuicio == "" ) {
					mensajeSIPAC += "Fecha Dictamen, ";
				}
				else if (!fechaDictamen == "" && fechaJuicio == "") {
					mensajeSIPAC += "Fecha Juicio, ";
				}
														
			} else if (origen == "M" && fechaDictamen == "") {
				mensajeSIPAC += "Fecha Dictamen, ";
			}	
	    }
		if(terminoAjuste == "SRSIPAC" || terminoAjuste == "SRSIPACYRC"){
			if (jQuery("#ciaSeguros").val() == "" ){
				mensajeSIPAC += "Compañia de Seguros, ";
			}
			if (jQuery("#t_numeroSin").val() == ""){
				mensajeSIPAC += "No. Siniestro Compañía, ";
			} 
			if (jQuery("#t_poliza").val() == ""){
				mensajeSIPAC += "No. Póliza de Compañía, ";
			}
			if (jQuery("#t_inciso").val() == ""){
				mensajeSIPAC += "No. Inciso de Compañía, ";
			}
		}
	}
	return mensajeSIPAC;		
}

function validaBtnLLegadaOtraCia(tipo){

	if( tipo == "CS_3" ){
		jQuery("#div_llegadaOtraCia").show();
	}else{
		jQuery("#div_llegadaOtraCia").hide();
	}
	/*var participoOtraCia = jQuery("#llegaOtraCia").val();
	if(participoOtraCia == 'S'){
		jQuery("#div_llegadaOtraCia").show();
	}else{
		jQuery("#div_llegadaOtraCia").hide();	
	}*/
}

function capturaLlegadaOtraCia(){
	var url = mostrarContenedorLlegadaOtraCiaPath + "?idReporteCabina=" + jQuery("#idToReporte").val()+"&soloConsulta="+jQuery("#h_soloConsulta").val();
	mostrarVentanaModal( "vm_llegadaOtraCia", "Captura llegada Otras Cias.", null, null, 800, 490, url , null);	
}

function cerrarVentanaLlegadaCia(){
	parent.cerrarVentanaModal('vm_llegadaOtraCia');
}

function agregarLlegadaOtraCia(){
	if(validaRequeridos()){
		parent.submitVentanaModal("vm_llegadaOtraCia", document.llegadaCiaForm);
	}	
}

function validaRequeridos(){
    var requeridos = jQuery(".requerido");
    var esValido = true;
    requeridos.each(
          function(){
                var these = jQuery(this);
                if( isEmpty(these.val()) ){
                      these.addClass("errorField");
                      esValido = false;
                } else {
                      these.removeClass("errorField");
                }
          }
    );
    return esValido;
}

var llegadasCiaGrid;

function cargaLlegadaCiaGrid(){
	var reporteCabinaId = jQuery("#h_idReporteCabina").val(); 
	var soloConsulta = jQuery("#h_soloConsulta").val();
	var url = cargaLlegadaCiaGridPath+"?idReporteCabina="+reporteCabinaId+"&saltaPrepare=true"+"&soloConsulta="+soloConsulta;
	loadLlegadasCiaGrid(url);
}

function loadLlegadasCiaGrid(url){
		 jQuery("#llegadasCiaGrid").empty(); 
		 llegadasCiaGrid = new dhtmlXGridObject('llegadasCiaGrid');
		 llegadasCiaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 llegadasCiaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 	 llegadasCiaGrid.attachEvent("onXLS", function(grid){
		 	mostrarIndicadorCarga("indicador");
	     });
		 llegadasCiaGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
			validaSiTieneCia();
	     });
		 llegadasCiaGrid.load( url ) ;	
}

function eliminarLlegadaCia(llegadaCiaId){
	var url=eliminarLlegadaCiaPath+"?idLlegadaCia="+llegadaCiaId+"&saltaPrepare=true";
	sendRequestJQ(null, url, 'contenido', 'cargaLlegadaCiaGrid();');
}

function validaSiTieneCia(){
	var filas = llegadasCiaGrid.getRowsNum();
	console.log('El numero de filas de cia es: '+filas);
	if(filas>0){
		parent.actualizaBanderaDeCia(true);
	}else{
		parent.actualizaBanderaDeCia(false);
	}
}


function ocultarBotonCerrarVentanaModal(id){
	var win = mainDhxWindow.window(id);
	win.button("close").hide();
}


function onChangeCondMismoAsegurado(value) {
	if (value == 'S') {
		jQuery("#rfc").val(jQuery("#rfcAsegurado").val());
		jQuery("#curp").val(jQuery("#curpAsegurado").val());
	} else {
		jQuery("#rfc").val(null);
		jQuery("#curp").val(null);
	}
}

function mostrarOcultarValuacionRecuperacion(){
	if(jQuery("#terminoSiniestro").val() == "REC" ||
			jQuery("#terminoSiniestro").val() == "REJ" ||
			jQuery("#terminoSiniestro").val() == "RTC" ||
			jQuery("#terminoSiniestro").val() == "RTJ"){
		document.getElementById("detalleValuacion").style.display = "table-row";
		document.getElementById("detalleRecuperacion").style.display = "table-row";
		document.getElementById("referenciasBancarias").style.display = "table-row";
	}else{
		document.getElementById("detalleValuacion").style.display = "none";
		document.getElementById("detalleRecuperacion").style.display = "none";
		document.getElementById("referenciasBancarias").style.display = "none";
	}
}

function mostrarOcultarCompanias(){
	
	var participoOtraCia = jQuery("#llegaOtraCia").val();
	if(participoOtraCia == 'S' || (jQuery("#terminoAjuste").val() == 'SRSIPAC' ||  jQuery("#terminoAjuste").val() == 'SRSIPACYRC')){
		document.getElementById("contenidoCompania").style.display = "table-row";
	}else{
		document.getElementById("contenidoCompania").style.display = "none";
	}
	
}

function mostrarOcultarDanosRechazo(){
	
	if(jQuery("#terminoAjuste").val() == "RECH") {
		document.getElementById("contenidoValuacionDanos").style.display = "table-row";
		document.getElementById("contenidoValuacionDanos2").style.display = "table-row";
	}else{
		document.getElementById("contenidoValuacionDanos").style.display = "none";
		document.getElementById("contenidoValuacionDanos2").style.display = "none";
	}
	
}

function buscarReferenciasBancarias(){	
	var recuperacionId =   jQuery("#recuperacionId").val();
	var terminoSiniestro =   jQuery("#terminoSiniestro").val();
	document.getElementById("referenciasBancariasGrid").innerHTML = '';
	referenciasBancariasGrid = new dhtmlXGridObject("referenciasBancariasGrid");
	referenciasBancariasGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
	});
	referenciasBancariasGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	referenciasBancariasGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	

	var url= buscarReferenciasBancariasPath + '?siniestroDTO.recuperacionId='+recuperacionId + '&siniestroDTO.terminoSiniestro=' + terminoSiniestro;
	referenciasBancariasGrid.load(url);
}

function generarReferenciaRecuperacion(){
	removeCurrencyFormatOnTxtInput();
	var numSiniestro = document.getElementById("siniestroDTO.numeroSiniestro").value;
	var esSiniestro = document.getElementById("siniestroDTO.esSiniestro").value;
	var montoRecuperado = document.getElementById("montoRecuperado").value;
	if(numSiniestro == "" || esSiniestro == false){
			mostrarMensajeInformativo("Es necesario convertir a siniestro antes de generar la Recuperación", '20');
	}else{
		if(montoRecuperado != "" && montoRecuperado > 0){
			var mensaje = validaCamposRequeridos(false);
			var data=jQuery("#siniestroCabinaForm").serialize();
			sendRequestJQ(null, generarReferenciasRecuperacionPath + '?' + data ,'contenido', 'buscarReferenciasBancarias');
		}else{
			mostrarMensajeInformativo("Capture un monto final recuperado mayor a $0.00", '20');
		}
	
	}


}

function actualizaEstatusValuacion(){	
	var id = jQuery("#numeroValuacion").val();
	jQuery.ajax({
          url: actualizarEstatusValuacionPath + '?valuacionId='+id,
          dataType: 'json',
          async:false,
          type:"POST",
          data: null,
          success: function(json){
        	 unblockPage();
          	 var estatus = json.estatusValuacion;
             jQuery("#estatusValuacion").val(estatus);
          },
          beforeSend: function(){
				blockPage();
			}	
    });
}


/*function buscarCompaniasSiniestros(){	
	var idReporteCabina =   jQuery("#idToReporte").val();
	
	document.getElementById("siniestrosCiaGrid").innerHTML = '';
	
	
	companiaSiniestrosGrid = new dhtmlXGridObject("siniestrosCiaGrid");
	companiaSiniestrosGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
	});
	companiaSiniestrosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	companiaSiniestrosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	

	var url= buscarSiniestrosCiaPath + '?idReporteCabina='+idReporteCabina;
	companiaSiniestrosGrid.load(url);
}


function guardarSiniestrosCia(){

			var data=jQuery("#siniestroCabinaForm").serialize();
			sendRequestJQ(null, guardarSiniestrosCiaPath + '?' + data ,'contenido', 'buscarCompaniasSiniestros');
		
	}


function guardarSiniestroCia(){	
	jQuery.ajax({
          url: guardarSiniestroCiaPath + '?'  +jQuery("#siniestroCabinaForm").serialize(),
          dataType: 'json',
          async:false,
          type:"POST",
          data: null,
          success: function(json){
        	
        	 unblockPage();
        	 var mensaje = json.mensaje;
          	 if(  json.tipoMensaje=="10"){//Error
          		 if (null!= mensaje  ||  mensaje !=''){
	          		 mostrarMensajeInformativo(mensaje, '10');
	          	 }else{
	          		mostrarMensajeInformativo('Error General del sistema', '10'); 
	          	 }
          	 }
        	 buscarCompaniasSiniestros();
        	 jQuery(".companiaData").val("");
          },
          beforeSend: function(){
				blockPage();
			}	
    });
}

function editarSiniestroCia (idCompaniaSiniestro) {

	jQuery.ajax({
        url: editarSiniestroCiaPath + '?idCompaniaSiniestro='  + idCompaniaSiniestro,
        dataType: 'json',
        async:false,
        type:"POST",
        data: null,
        success: function(json){
	        unblockPage();
	      	var companiaSinId = json.companiaSiniestro.id;
	      	var coberturaId = json.companiaSiniestro.coberturaId;
	      	var folio = json.companiaSiniestro.folioCia;
	      	var numeroSiniestro = json.companiaSiniestro.numeroSiniestro;
	      	var porcentaje = json.companiaSiniestro.porcentaje;
	      	var companiaId = json.companiaSiniestro.compania.id
	
	      	 
	      	 jQuery("#s_cias").val(companiaId);
	         jQuery("#s_coberturas").val(coberturaId);
	         jQuery("#t_folio").val(folio);
	         jQuery("#t_numeroSin").val(numeroSiniestro);
	         jQuery("#t_porcentaje").val(porcentaje);
	         jQuery("#h_idCompaniaSiniestro").val(companiaSinId);
	    },
        beforeSend: function(){
				blockPage();
			}	
  });
}*/


function rechazarSiniestro() {
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	sendRequestJQ(null, rechazarSiniestroPath + '?' + data ,'contenido', null);
}

function terminarSiniestro() {	
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	sendRequestJQ(null, termiinarSiniestroPath + '?' + data ,'contenido', null);	
}

function cancelarSiniestro() {	
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	sendRequestJQ(null, cancelarSiniestroPath + '?' + data ,'contenido', null);	
}

function reaperturarReporte() {	
	removeCurrencyFormatOnTxtInput();
	var data=jQuery("#siniestroCabinaForm").serialize();
	sendRequestJQ(null, reaperturarReportePath + '?' + data ,'contenido', null);
	
}



function validarPendientesReporte(accion){	
	removeCurrencyFormatOnTxtInput();
	jQuery.ajax({
          url: validarPendientesReportePath + '?'  +jQuery("#siniestroCabinaForm").serialize(),
          dataType: 'json',
          async:false,
          type:"POST",
          data: null,
          success: function(json){
        	
        	 unblockPage();
        	 var estatus = json.estatusPendienteReporte;
          	 if (estatus == 0) {
          		if (accion == 1) {
          			rechazarSiniestro();
          		} else if (accion == 2) {
          			cancelarSiniestro();
          		} else if (accion == 3) {
      				terminarSiniestro();
          		}
          	 } else if (estatus == 1) {
          		 mostrarMensajeInformativo(json.mensaje, '10');
          	 } else if (estatus == 2) {
          		if(confirm('El reporte cuenta con reserva pendiente. \u00BFDesea depurar la reserva?')){
          			if (accion == 1) {
              			rechazarSiniestro();
              		} else if (accion == 2) {
              			cancelarSiniestro();
              		} else if (accion == 3) {
          				terminarSiniestro();
              		}
        		}
          	 }
         	
          },
          beforeSend: function(){
				blockPage();
			}	
    });
}

function mostrarMontosCoberturasRechazo(){
	var idToReporte = jQuery("#idToReporte").val();
	var montoRechazo = jQuery("#t_montoDanos").val();
	var url = mostrarCoberturasRechazoPath + "?idReporteRechazo=" + idToReporte + "&siniestroDTO.montoDanos=" + montoRechazo;
	mostrarVentanaModal("vm_coberturasRechazo", 'Listar Coberturas para Rechazo',  1, 1, 700, 500, url, 'initCurrencyFormatOnTxtInput();');
}

function guardarMontosCoberturasRechazo(){
	var idToReporte = jQuery("#idReporteRechazo").val();
	var params = obtenerMontosCoberturas();
	jQuery("#coberturasRechazoForm").attr("action","guardarCoberturasRechazo.action?"+ params);
	var actionString = jQuery("#coberturasRechazoForm").attr("action");
	parent.submitVentanaModal("vm_coberturasRechazo", document.coberturasRechazoForm);
}

function obtenerMontosCoberturas(){
	var reservas = "";
	var contador = 0;
      
	contenedorCoberturasRechazoGrid.forEachRow(function(id){
		var montoRechazo = contenedorCoberturasRechazoGrid.cells(id,1).getValue();
		if(!montoRechazo){montoRechazo = 0;}
		reservas = reservas.concat("&listAfectacionCoberturaSiniestroDTO[" + contador + "].coberturaReporteCabina.id=" + contenedorCoberturasRechazoGrid.getUserData(id,"coberturaReporteCabinaIdCell")
				+ "&listAfectacionCoberturaSiniestroDTO[" + contador + "].coberturaReporteCabina.claveTipoCalculo=" + contenedorCoberturasRechazoGrid.getUserData(id,"claveTipoCalculoCell")
				+ "&listAfectacionCoberturaSiniestroDTO[" + contador + "].configuracionCalculoCobertura.cveSubTipoCalculoCobertura=" + contenedorCoberturasRechazoGrid.getUserData(id,"claveSubTipoCalculoCell")
				+ "&listAfectacionCoberturaSiniestroDTO[" + contador + "].montoRechazo=" + montoRechazo);
		contador ++;
	});
	return reservas;
}

var contenedorCoberturasRechazoGrid;
function buscarMontosCoberturasRechazo(){
		parent.document.getElementById('t_montoDanos').value = jQuery("#montoRechazoTotal").val();
		parent.document.getElementById('d_montoDanos').value = jQuery("#montoRechazoTotal").val();
		removeCurrencyFormatOnTxtInput();
		jQuery("#contenedorCoberturasRechazoGrid").empty(); 
		
		contenedorCoberturasRechazoGrid = new dhtmlXGridObject('contenedorCoberturasRechazoGrid');
		contenedorCoberturasRechazoGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorCoberturasRechazo");});
		contenedorCoberturasRechazoGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorCoberturasRechazo");});
		contenedorCoberturasRechazoGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		    if(stage == 0) jQuery("#btn_guardarMontosRechazo").hide();
		    if(stage == 2) jQuery("#btn_guardarMontosRechazo").show();
		    return true;
		});
		
		formParams = jQuery(document.coberturasRechazoForm).serialize();
		var url = buscarCoberturasRechazoPath  + '?' +  formParams;
		console.log("URL buscar MONTOS COBERTURAS RECHAZO: " + url);
		contenedorCoberturasRechazoGrid.load( url );
}

function cerrarVentanaCoberturasRechazo(){
	parent.cerrarVentanaModal('vm_coberturasRechazo','initCurrencyFormatOnTxtInput();');
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}
