var map,marker,
	markers = [],
	siniestro, 
	monitor = false;

var GLOBAL_INTERVAL = 30000;



/**
 * Inicializar mapa para monitor de ajustadores
 */
function initialize(){	
	var zoomMapa = 11;
	var idContenedorMapa = "map";
	var url = "/MidasWeb/siniestros/catalogo/servicio/mostrarContenedorMapa.action?mostrarOficinas=true";
	blockPage();
	map = initMap( idContenedorMapa , zoomMapa );
	unblockPage();
	if( map ){		
		jQuery("#ofinasActivas").change(
			function(){		
				blockPage();				
				getAjustadores(true);	
				unblockPage();
			}
		);		
		if(jQuery('#ofinasActivas').val()){
			getAjustadores(true);
		}
	} else {
		sendRequestJQ(null, url,"contenido", "init();");
	}
	
}

/**
 * Obtener ajustadores para la oficina seleccionada
 */
function getAjustadores(centrarOficina){		
	var idOficina = jQuery("#ofinasActivas :selected").val();
	if( idOficina ){
		if(centrarOficina){
			setMapaCoordenadasOficina(idOficina);
		}
		var url = "/MidasWeb/siniestros/catalogo/servicio/cargarUbicacionAjustadoresPorOficina.action?oficinaId="+idOficina;
		var data="";	
		jQuery.asyncPostJSON(url,data,function(response){
			setUbicacionAjustadores(response);
			obtenerReportesPorOficina(idOficina);
		});
	} else {
		limpiarElementosMapa();
	}
}

/**
 * Obtener ubicaciones historicas del dia para un ajustador
 * @param idAjustador
 */
function getHistoricoDelDia(idAjustador){	
	limpiarIntervaloActual();
	blockPage();
	setTimeout(
		function(){
			if( idAjustador ){
				var url = "/MidasWeb/siniestros/catalogo/servicio/cargarHistoricoAjustador.action";
				jQuery.ajax({
						 dataType: "json",
						 url: url,
						 async: false,
						 data: {idAjustador   : idAjustador},
						 beforeSend: function(){									
							 blockPage();
								},
						 success: function(response){
							 setUbicacionesHistoricas(response);							 
							 }
						 ,
						 complete: function(){
							 unblockPage();
						 }
					});
			}
		}, 100);
}

/**
 * Pintar ubicaciones historicas de un ajustdor. Callback para getHistoricoDelDia.
 * @param json
 */
function setUbicacionesHistoricas(json){
	var ajustadores = "";
	ajustadores = json.listaAjustadores;
	var i;
	var divHtml="";
	limpiarElementosMapa();
	for( i=0; i < ajustadores.length; i++ ){	
		crearMarcadorAjustador(ajustadores[i]);			
	}
}


/**
 * Pintar marcadores para ajustadores. Callback para cargarUbicacionAjustadoresPorOficina.
 * @param json
 */
function setUbicacionAjustadores(json){
	var ajustadores = "";
	ajustadores = json.listaAjustadores;
	var i;
	var divHtml="";
	var div="";

	limpiarElementosMapa();
	if(ajustadores == null || ajustadores.length == 0){
		divHtml = setListadoVacio();
		jQuery("#listadoAjustadores").html(divHtml);		
	}else{
		jQuery("#infoAlertContenedor").hide();
		jQuery("#infoAlert").html("");
		for( i=0; i < ajustadores.length; i++ ){			
			if( !jQuery("#mostrarInformacionReporte").val() ){
				map.setCenter( new google.maps.LatLng( ajustadores[0].latitud , ajustadores[0].longitud ));
			}			
			if( jQuery("#mostrarListadoAjustadores").val() == 'true' && jQuery("#mostrarInformacionReporte").val() == 'true'){
				div = setListadoAjustadores( ajustadores[i] );
				divHtml+=div;
			}
			if( jQuery("#mostrarListadoAjustadores").val() == 'true' && jQuery("#mostrarInformacionReporte").val() == 'false'){				
				div = setListadoAjustadoresMonitor( ajustadores[i] );
				divHtml+=div;
			}
			crearMarcadorAjustador(ajustadores[i]);
			jQuery("#listadoAjustadores").html(divHtml);
			
			//draw reportes
			var reportes = ajustadores[i].reportesAsignados;
			if(reportes != null && reportes.length > 0){				
				for(var j = 0; j < reportes.length; j++){						
					reportes[j].idControl=i;
					reportes[j].tipo = TIPO_REPORTE;							
					reportes[j] = determinarIcono(reportes[j]);								
					var reporte = crearMarcadorReporte(reportes[j]);					
					var reportesSinCoordenadas = [];
					
					if(reporte == null){ //no coords
						reporte = {
								id: reportes[j].reporteId,							
								title: reportes[j].numeroReporte + ' : ' + reportes[j].estatus,
							    tipo: reportes[j].tipo,
							    numReporte : reportes[j].numeroReporte,
							    fechaAlta: reportes[j].fechaReporteStr,
							    fechaAsignacion: reportes[j].fechaAsignacionStr,
							    fechaContacto: reportes[j].fechaContactoStr,
							    cliente: reportes[j].cliente,
							    numPoliza: reportes[j].numPoliza,
							    asignadoA: reportes[j].asignadoA
						};
						reportesSinCoordenadas.push(reporte);							
					}
					if(reportes[j].coordenadas != null){
						var pathObj = new Object();
						var latlngAjustador = new google.maps.LatLng(ajustadores[i].latitud,ajustadores[i].longitud);
						var latlngReporte = new google.maps.LatLng(ajustadores[i].reportesAsignados[j].coordenadas.latitud,ajustadores[i].reportesAsignados[j].coordenadas.longitud);
						var waypoint = [latlngAjustador, latlngReporte];						
						pathObj.path = trazarLineaEntreCoordenadas(waypoint, PATH_COLOR[i % PATH_COLOR.length], 3);
						pathObj.idAjustador = ajustadores[i].idAjustador;
						pathObj.idReporte = reportes[j].reporteId;
						paths.push(pathObj);
					}
				}
			}
		}
	}
	activarDesactivarWayPoint();
}

/**
 *  
 * Obtener reportes por una oficina sin asignacion o que no tengan coordenadas confirmadas y pintar sus marcadores
 * @param oficinaId
 */
function obtenerReportesPorOficina(oficinaId){
	var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerReportesPorOficina.action";	
	limpiarMarcador(TIPO_REPORTE_NOCOORDS, null);
	jQuery.getJSON(url,
			{oficinaId : oficinaId}
			,function(reponse){
				reportes = reponse.listaReportes;
				var i;	
				
				if( reportes != null && reportes.length > 0 ){
					var marcaReporte;
					var reportesSinCoordenadasGlobal = [];;
					for(var i=0; i < reportes.length; i++ ){
						reportes[i].idControl=i;
						reportes[i].tipo = TIPO_REPORTE;							
						reportes[i] = determinarIcono(reportes[i]);								
						var reporte = crearMarcadorReporte(reportes[i]);
						if(reporte == null){ //no coords
							reporte = {
									id: reportes[i].reporteId,							
									title: reportes[i].numeroReporte + ' : ' + reportes[i].estatus,
								    tipo: reportes[i].tipo,
								    numReporte : reportes[i].numeroReporte,
								    fechaAlta: reportes[i].fechaReporteStr,
								    fechaAsignacion: reportes[i].fechaAsignacionStr,
								    fechaContacto: reportes[i].fechaContactoStr,
								    cliente: reportes[i].cliente,
								    numPoliza: reportes[i].numPoliza,
								    asignadoA: reportes[i].asignadoA
							};							
							reportesSinCoordenadasGlobal.push(reporte);							
						}
					}
					if(reportesSinCoordenadasGlobal != null && reportesSinCoordenadasGlobal.length > 0){
						noCoordMarker = new google.maps.Marker(
								{
									id: 'noCoordMarker',
									position: map.getCenter(),
									map: map,								
								    draggable: false,
								    title: 'Sin confirmación de coordenadas y sin ajustador',
								    animation: google.maps.Animation.DROP,
								    tipo : TIPO_REPORTE_NOCOORDS				
								});
						markers.push(noCoordMarker);
						google.maps.event.addListener(noCoordMarker, 'click', (function(noCoordMarker) {
					        return function() {
					          infowindow.setContent(generarInfoWindowNoCoordenadas(reportesSinCoordenadasGlobal));
					          infowindow.open(map, noCoordMarker);
					        }
					      })(noCoordMarker));									
					}
				}
			});
}

/**
 * Limpiar seccion de ajustadores
 * @returns {String}
 */
function setListadoVacio(){
	var itemAjustador="";
	var back="";	
	itemAjustador+='<div '+back+' class="ajustadorItem googleStyle">';
	itemAjustador+='<div style="float: left;text-align: left;margin-top: 2%;"><b>No hay ajustadores para mostrar en esta oficina el día de hoy o las coordenadas de su ubicaci\u00F3n no han sido reportadas.</b></div>';
	itemAjustador+='</div>';
	
	return itemAjustador;
}

/**
 * Pintar listado de ajustadores en visualizador
 * @param ajustador
 * @returns {String}
 */
function setListadoAjustadoresMonitor( ajustador ){
	var itemAjustador="";
	var back="";
	
	if( ajustador.estaAsignado ){
		back='style="background-color:#F4DC53 !important;"';
	}
	itemAjustador+='<div '+back+' class="ajustadorItem googleStyle">';
	itemAjustador+='<div style="width: 100%;height: 10px;position:relative;" >';
	itemAjustador+='<div style="height:35px;float: left;text-align: left;margin-top: 2px;"><b>Nombres:</b> '+ajustador.nombreAjustador+'</div>';
	itemAjustador+='</div>';
	
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 2%;"><b>Fecha Posici&oacute;n:</b> '+ajustador.fechaCoordenadas+'</div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;"><b>Horario Disponibilidad:</b> '+ajustador.disponibilidad+' </div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;"><b>Reportes Asignados:</b> '+ajustador.numeroReportesAsignados+' </div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;">';
	itemAjustador+='<div class="btn_back w60" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2%;">';
	itemAjustador+='<a href="javascript: void(0);" alt="Hist&oacute;rico del d&iacute;a" title="Hist&oacute;rico del d&iacute;a" onclick="getHistoricoDelDia('+ajustador.idAjustador+');">Hist&oacute;rico</a>';
	itemAjustador+='</div></div>';
	itemAjustador+='</div>';
	
	return itemAjustador;
}

/**
 * Refrescar el mapa y realizar la carga de nuevo
 * @param oficinaId
 */
function refrescar(oficinaId){
	limpiarIntervaloActual();
	var url = '/MidasWeb/siniestros/catalogo/servicio/mostrarContenedorMapa.action?&mostrarOficinas=true&oficinaId='+oficinaId;
	sendRequestJQ(null,url,targetWorkArea,null);
}

/**
 * Activar monitoreo constante de ajustadores y reportes cada X tiempo, definido por la constante GLOBAL_INTERVAL
 * @param obj
 */
function monitorear(obj){
	if (obj.interval){
	        clearInterval(obj.interval);
	        obj.interval = null;
	        jQuery('#accionMonitor')[0].src = BTN_OFF;
	    } else {
	    	 obj.interval = setInterval(function(){	        	
	        		if(jQuery('#accionMonitor')[0]){
	        			getAjustadores(false);
	        		}else{
	        			clearInterval(obj.interval);
	        		}
	        	}
	        ,GLOBAL_INTERVAL);	      
	    	 jQuery('#accionMonitor')[0].src = BTN_ON;
	    }	
}

/**
 * Desactivar el intervalo de monitoreo en caso de estar activo
 */
function limpiarIntervaloActual(){
	if(jQuery('#accionMonitor')[0].interval){
		clearInterval(jQuery('#accionMonitor')[0].interval);
		jQuery('#accionMonitor')[0].src = BTN_OFF;
	}
}

/**
 * Limpiar ajustadores, reportes y waypoints del mapa
 */
function limpiarElementosMapa(){
	limpiarMarcador(TIPO_AJUSTADOR, null);
	limpiarMarcador(TIPO_REPORTE, null);
	limpiarWayPoints();
}

function eliminarHistorial(){
	if(confirm('¿Está seguro que desea eliminar el historial del mes anterior de los ajustadores, este proceso es irreversible?')){
		var url = "/MidasWeb/siniestros/catalogo/servicio/eliminarHistorialAjustadores.action";
		jQuery.getJSON(url,
				{}
				,function(json){						
					mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);
				});
	}
}



