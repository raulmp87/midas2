/**
 * 
 */
// Inicializa el Greid de la lista de eventos
function iniContenedorGrid() {
	dwr.util.removeAllOptions("s_evento");  
	buscarListaEventos();
} 

function onChangeModulo(){
	var modulo = dwr.util.getValue("s_modulo");	
	if(null ==modulo   || modulo==""){
		dwr.util.removeAllOptions("s_evento");		
	}else{
	listadoService.obtenerCatalogoValorFijoByStr("TIPO_EVENTO", modulo ,function(data){
		dwr.util.removeAllOptions("s_evento");
		dwr.util.addOptions("s_evento", [ {
				id : "",
				value : "Seleccione..."
			} ], "id", "value");
		dwr.util.addOptions("s_evento", data);
	});
	}
	
}



function addOptionsEvento(target, map) {
	dwr.util.removeAllOptions(jQuery(target).selector);
	addSelectHeader(target);
	dwr.util.addOptions(jQuery(target).selector, map);
}

function validaNombre(target, map) {
	var modulo = dwr.util.getValue("nombreUsu");
	var n = modulo.length;
	if(n<3){
		mostrarMensajeInformativo('Para filtrar por nombre debe proporcionar al menos 3 caracteres', '20');
		dwr.util.setValue( "nombreUsu","" ); 
	}
}


function buscarListaEventos(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	jQuery("#observacion").val( '');
	listadoGrid = new dhtmlXGridObject('eventoGrid');
	
	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
		mostrarIndicadorCarga("indicadorBitacora");
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
		ocultarIndicadorCarga('indicadorBitacora');
    });	
	listadoGrid.attachEvent("onRowSelect", function(id,ind){
 
		 jQuery("#observacion").val( listadoGrid.cells(listadoGrid.getSelectedId(),4).getValue() );
        
	  });
	
	listadoGrid.attachEvent("onXLE", function(){
	    if (!listadoGrid.getRowsNum())
	    	listadoGrid.addRow(listadoGrid.uid(), "No Encontró Resultados.");
	})
	
	var formParam = null;
	formParam = jQuery(document.contenedorForm).serialize();
	listadoGrid.load( buscarEventosPath + '?' + formParam );
}

 function exportarExcel(){
	 var url="/MidasWeb/siniestros/cabina/reporteCabina/bitacoraEvento/exportar.action?" + jQuery(document.contenedorForm).serialize();
	 	window.open(url, "Listado_Eventos");
	 }

 function cerrar(){
	 
	 var id = jQuery("#reporteCabinaId").val();
	 var soloConsulta = jQuery("#h_soloConsulta").val();
	 
	 sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?idToReporte='+id,
			 		targetWorkArea, soloConsulta==1?"setConsultaReporte();":null);
	 	
 }


