var siniestrosVehiculoGrid;

jQuery(document).ready(
	function(){
		getSiniestrosVehiculo();
	}
);

function getSiniestrosVehiculo(){
	var numeroSerie = jQuery("#numeroSerie").val();
	siniestrosVehiculoGrid = new dhtmlXGridObject('siniestrosVehiculoGrid');

	if( numeroSerie != null && numeroSerie != 0 ){
		siniestrosVehiculoGrid.load( "/MidasWeb/siniestros/cabina/reportecabina/buscarSiniestro.action?numeroSerie=" + numeroSerie );
	} else {
		siniestrosVehiculoGrid.load( "/MidasWeb/siniestros/cabina/reportecabina/buscarSiniestro.action" );
	}	
}

function filterText(node){
    var text = node.previousSibling.value;
    if (!text) return grid.filterBy();

    grid.filterBy(function(data){  //grid is a dhtmlxGrid instance
        return data == text;

    })
}

function exportarExcel(){
	var numeroSerie = jQuery("#numeroSerie").val();
	var url="/MidasWeb/siniestros/cabina/reportecabina/exportarListado.action?numeroSerie=" + numeroSerie;
	window.open(url, "Listado_Siniestros");
}
