function inicializarListadoGastoAjuste(){	
	mostrarListadoGastoAjuste();
}

function mostrarListadoGastoAjuste(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('gastoAjusteGrid');

	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });	
	var formParams = null;
	
	formParams = jQuery(document.gastoAjusteForm).serialize();	 
	listadoGrid.load( mostrarGastoAjustePath + '?' + formParams );
}

function editarGastoAjuste(idGastoAjuste) {
	parent.redirectVentanaModal('mostrarGastoAjuste', mostrarGastoPath + '?gastoAjusteDTO.id=' + idGastoAjuste, null);
}

function buscarDatosPrestador(prestadorId){
	var data=jQuery("#gastoAjusteForm").serialize();
	jQuery.ajax({
		url: buscarDatosPrestadorPath,
		dataType: 'json',
		async:false,
		type:"POST",
		data: data,
		success: function(json){
			buscarDatosPrestadorCallBack(json);
		}
	});
	//jQuery.asyncPostJSON(buscarDatosPrestadorPath + '?' + data,null,buscarDatosPrestadorCallBack);
}

function buscarDatosPrestadorCallBack(json){
	var gasto = json.gastoAjusteDTO;
	jQuery("#txt_rfc").val(gasto.rfc);
	jQuery("#txt_curp").val(gasto.curp);
	jQuery("#origen").val(gasto.origen);
	jQuery("#prestadorId").val(gasto.prestadorAjusteId);
}

function nuevoGastoAjuste(id) {
	if(confirm("Si continua, los datos que se guardaron, se perderan.")){
		parent.redirectVentanaModal('mostrarGastoAjuste', nuevoGastoPath + '?idToReporte=' + id, null);
	}
}

function cerrarVentanaAjuste(){
	if(confirm("Si continua, los datos que se guardaron, se perderan.")){
		parent.cerrarVentanaModal('mostrarGastoAjuste');
	}
}

function guardarAjuste(gastoId){
	
	if(gastoId == ""){
		
		if(jQuery("#motivoSituacion").val() == ""){
			alert('Seleccione Motivo de la Situación');
			return false;
		}
		
		if(jQuery("#tipoPrestador").val() == ""){
			alert('Seleccione Tipo Prestador');
			return false;
		}
		
		if(jQuery("#prestadorOrigen").val() == ""){
			alert('Seleccione Prestador');
			return false;
		}
		
		if(jQuery("#mostrarSeccionGrua").val() == 'true'){
			if(jQuery("#tallerAsignadoId").val() == ""){
				alert('Seleccione Taller Asignado');
				return false;
			}
		}
	}
	parent.submitVentanaModal("mostrarGastoAjuste", document.gastoAjusteForm);
}

function onChangeTipoPrestador(tipoPrestadorId, id){
	var data=jQuery("#gastoAjusteForm").serialize();
	parent.redirectVentanaModal('mostrarGastoAjuste', onChangeTipoPrestadorPath + '?tipoPrestadorId=' +  tipoPrestadorId + '&idToReporte=' + id, null);
}

function setPrestador(id, valor){
	var arr = valor.split(',');
	data=jQuery("#" + id).val(arr[1]);
}

function imprimirValeTaller(){

	var reporteCabinaId = jQuery('#reporteCabinaId').val();
	var prestadorId = jQuery('#prestadorId').val();
	var url = "/MidasWeb/siniestros/cabina/reporteCabina/valeTaller/mostrarVale.action?idReporteCabina=" 
		+ reporteCabinaId + "&idGrua=" + prestadorId;

 parent.mostrarVentanaModal("vm_valeTaller", "Vale de Servicio de Gr\u00DAa", null, null, 1000, 550, url, "");   
}

function redirecciona(){
	redirectVentanaModal("vm_valeTaller", jQuery("#urlRedirect").val(), null);
}

function setConsulta() {
	jQuery("#b_guardar").remove();
	jQuery("#b_nuevaVersion").remove();
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
}
