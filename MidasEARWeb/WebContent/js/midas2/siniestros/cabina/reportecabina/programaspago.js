function blockTextfields(){
	jQuery('#numeroPoliza_t').attr('readonly',true);
	jQuery('#numeroInciso_t').attr('readonly',true);
	jQuery('#contratante_t').attr('readonly',true);
	jQuery('#moneda_t').attr('readonly',true);
}

function initConfiguration(){
	blockTextfields();
	jQuery("#tablaRecibos").hide();
}

function regresarPantallaBusqueda(){
	var url="/MidasWeb/siniestros/cabina/reportecabina/mostrarContenedor.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

var programasPagoGrid;
function initProgramasPagoGrid(){
	jQuery("#programasPagoGrid").empty();
	
	var fechaReporteSiniestro = jQuery("#fechaReporteSiniestro_h").val();
	var incisoContinuityId = jQuery("#incisoContinuityId_h").val();
	var validOnMillis = jQuery("#h_validOnMillis").val();
	var recordFromMillis = jQuery("#h_recordFromMillis").val();
	
	programasPagoGrid = new dhtmlXGridObject('programasPagoGrid');
	programasPagoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	programasPagoGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
		if(grid_obj.getRowsNum() > 0){
			jQuery("#numeroPrograma_h").val(grid_obj.getRowId(0));
			jQuery("#idCotizacionSeycos_h").val(grid_obj.getUserData(grid_obj.getRowId(0),"idCotizacionSeycosCell"));
		}
		});
	
	var urlProgramasPago = "/MidasWeb/siniestros/cabina/reportecabina/poliza/programapago/obtenerProgramasPago.action" +
				"?fechaReporteSiniestro=" + fechaReporteSiniestro +
				"&incisoContinuityId=" + incisoContinuityId +
				"&validOnMillis=" + validOnMillis +
				"&recordFromMillis=" + recordFromMillis;
	programasPagoGrid.load(urlProgramasPago, initRecibosProgramaPago);
	
	programasPagoGrid.attachEvent("onRowSelect", 
			function(id){
			jQuery("#tablaRecibos").show();
			var idCotizacionSeycos 	= programasPagoGrid.getUserData(id,"idCotizacionSeycosCell");
			var validOnMillis 		= jQuery("#h_validOnMillis").val();
			var recordFromMillis 	= jQuery("#h_recordFromMillis").val();
			var fechaReporteSiniestro 	= jQuery('#fechaReporteSiniestro_h').val();
			var url 					= "/MidasWeb/siniestros/cabina/reportecabina/poliza/programapago/obtenerRecibos.action" +
											 "?numeroPrograma=" + id
											 + "&idCotizacionSeycos=" + idCotizacionSeycos
											 + "&fechaReporteSiniestro=" + fechaReporteSiniestro 
											 + "&validOnMillis=" + validOnMillis 
											 + "&recordFromMillis=" + recordFromMillis;
			
			getRecibosProgramaPago(url);
			return true;
	});	
}

var recibosProgramaPagoGrid;
function getRecibosProgramaPago(url){
	jQuery("#recibosProgramaPagoGrid").empty(); 
	
	recibosProgramaPagoGrid = new dhtmlXGridObject('recibosProgramaPagoGrid');
	recibosProgramaPagoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	recibosProgramaPagoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	recibosProgramaPagoGrid.load( url );
}

function initRecibosProgramaPago(){
	if(programasPagoGrid.getRowsNum() > 0){
		var fechaReporteSiniestro = jQuery("#fechaReporteSiniestro_h").val();
		var validOnMillis = jQuery("#h_validOnMillis").val();
		var recordFromMillis = jQuery("#h_recordFromMillis").val();
		var numeroPrograma = jQuery("#numeroPrograma_h").val();
		var idCotizacionSeycos = jQuery("#idCotizacionSeycos_h").val();
		
		var urlRecibosProgramasPago = "/MidasWeb/siniestros/cabina/reportecabina/poliza/programapago/obtenerRecibos.action"
					 + "?numeroPrograma=" + numeroPrograma
					 + "&idCotizacionSeycos=" + idCotizacionSeycos
					 + "&fechaReporteSiniestro=" + fechaReporteSiniestro 
					 + "&validOnMillis=" + validOnMillis 
					 + "&recordFromMillis=" + recordFromMillis;
		
		jQuery("#tablaRecibos").show();

		getRecibosProgramaPago(urlRecibosProgramasPago);
	}
}

function setConsulta(){
	jQuery("#contenido").append("<div id='consulta' style='display:none;' >1</div>");
}

