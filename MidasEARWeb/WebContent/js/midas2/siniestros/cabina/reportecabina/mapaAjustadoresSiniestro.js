var 
//google,maps,
map,marker,markers = [],siniestro,directionsDisplay,
directionsService,ajustadores=[];

jQuery(document).ready(
	function(){		
		setTimeout("init();", "1200");
		jQuery("#ofinasActivas").change(
			function(){
				getAjustadores();
				if( getDirectionsDisplay() ){
					var d = getDirectionsDisplay();
					d.setDirections({routes: []});
				}
			}
		);
	}
);

function init(){
	var zoomMapa = 12;
	var idContenedorMapa = "map";
	map = initMap( idContenedorMapa , zoomMapa );	
	setTimeout("obtenerMarcadorSiniestro()",'500');
}

function getAjustadores(){
	var idOficina = jQuery("#ofinasActivas :selected").val();
	var idToReporte = jQuery("#idToReporte").val();
	if( idOficina && idToReporte ){
		setMapaCoordenadasOficina(idOficina);
		var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerAjustadoresParaCalcularRuta.action";

		jQuery.post(
			url, 
			{
				oficinaId   : idOficina,
				idToReporte : idToReporte
			} ,
			function(response){
				cargarUbicacionesAjustadores(response);
				//setUbicacionAjustadores(response);
				getMap().setZoom(11);
			}
		);
	} else {
		clearMarkers(TIPO_AJUSTADOR);
	}
}

function setUbicacionAjustadores(json){
	ajustadores = json.listaAjustadores;
	var i;
	var div="";
	var divHtml="";
	
	jQuery("#listadoAjustadores").empty();
	clearMarkers();

	if( ajustadores.length > 0 ){
		for( i=0; i < ajustadores.length; i++ ){
			ajustadores[i].idControl=i;
			if( !jQuery("#mostrarInformacionReporte").val() ){
				map.setCenter( new google.maps.LatLng( ajustadores[0].latitud , ajustadores[0].longitud ));
			}			
			setAjustadorMarker( ajustadores[i] );
			setListadoAjustadoresMapa( ajustadores[i] );
		}
	} else {
		jQuery("#listadoAjustadores").html(
				"<div id='listadoAjustadoresTxt'>No hay ajustadores para mostrar en esta oficina, no existen ajustadores en el horario actual de servicio o las coordenadas de su ubicaci\u00F3n no han sido reportadas.</div>")
		jQuery("#listadoAjustadoresTxt").css({"text-align":"left","font-size":"10pt"});
		jQuery("#listadoAjustadoresTxt").hide();
		jQuery("#listadoAjustadoresTxt").fadeIn( 400 );
	}
}

function setAjustadorMarker( ajustador ){
	var div="";
	var marker = "";

	if( ajustador.estaAsignado ){
		parent.document.getElementById("ajustadorIdTcAjustador").value=ajustador.id;
		parent.document.getElementById("ajustadorNombre").value=ajustador.nombreAjustador;
		ajustador.icon = ICON_AJUSTADOR_ASIGNADO;
	} else {
		ajustador.icon = ICON_AJUSTADOR_DISPONIBLE;
	}
	
	if( ajustador.calcularRuta ){
		consultaGoogle( ajustador );
	} 
	
	if( !ajustador.calcularRuta ) {
		marker = new google.maps.Marker(
				{
					id:ajustador.id,
					position: new google.maps.LatLng( ajustador.latitud , ajustador.longitud ),
					map: map,
					icon:ajustador.icon,
					title: ajustador.nombreAjustador,
				    animation: google.maps.Animation.DROP,
				    calcularRuta:ajustador.calcularRuta,
				    distancia:ajustador.distanciaSiniestroKM,
				    tipo: "ajustador"
				}
			);
		markers.push(marker);
		
		google.maps.event.addListener(marker, 'click', (function(marker) {
	        return function() {
	          infowindow.setContent(generarInfoWindowAjustador(marker));
	          infowindow.open(map, marker);
	        }
	      })(marker));
	}
	
}

function consultaGoogle( ajustador , resetAjustadorData ){
	var dS = getDirectionsService();
	var dD = getDirectionsDisplay();
	var request = {
		origin:new google.maps.LatLng( ajustador.latitud , ajustador.longitud ),
		destination:siniestro.getPosition(),
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		durationInTraffic: true
	};
	
	dS.route(
		request, 
		function(result, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				var distancia = result.routes[0].legs[0].distance.value/1000;
				var tiempo = result.routes[0].legs[0].duration;
				ajustador.distanciaSiniestroKM=distancia;
				ajustador.duracion=tiempo.text;
				ajustadores[ajustador.idControl].distanciaSiniestroKM;
				ajustadores[ajustador.idControl].duracion;
				marker = new google.maps.Marker(
					{
						id:ajustador.id,
						position: new google.maps.LatLng( ajustador.latitud , ajustador.longitud ),
						map: map,
						icon:ajustador.icon,
						title: ajustador.nombreAjustador,
					    animation: google.maps.Animation.DROP,
					    calcularRuta:ajustador.calcularRuta,
					    distancia:distancia,
					    duracion:ajustador.duracion,
					    disponibilidad:ajustador.disponibilidad,
					    tipo: "ajustador"
					}
				);
				
				setListadoAjustadoresMapa( ajustador , resetAjustadorData )
				
				markers.push(marker);
				
				google.maps.event.addListener(marker, 'click', (function(marker) {
			        return function() {
			          infowindow.setContent(generarInfoWindowAjustador(marker));
			          infowindow.open(map, marker);
			        }
			      })(marker));
			}
		}
	);
}
/*
function setListadoAjustadores( ajustador , resetAjustadorData ){
	var itemAjustador="";
	var back="";
	var distance=ajustador.distanciaSiniestroKM.toString().slice(0,5);
	if( !ajustador.calcularRuta ){
		ajustador.distanciaSiniestroKM="Distancia a calcular";
	} else {
		ajustador.distanciaSiniestroKM=distance+" Km.";
	}
	
	
	if( ajustador.estaAsignado ){
		back='style="background-color:#F4DC53 !important;"';
	}
	itemAjustador+='<div id="ajustador'+ajustador.id+'" '+back+' class="ajustadorItem googleStyle">';
	
	itemAjustador+='<div style="width: 240px;height: 10px;position:relative;" >';
	itemAjustador+='<div style="height:10px;float: left;text-align: left;margin-top: 2px;"><b>Nombre:</b> '+ajustador.nombreAjustador+'</div>';
	itemAjustador+='</div>';
	
	itemAjustador+='<div style="width: 240px;height: 10px;float:left;position:relative;" >';
	itemAjustador+='<div style="height:10px;float: left;text-align: left;margin-top: 3px;"><b>Distancia:</b> '+ajustador.distanciaSiniestroKM +'</div>';
	itemAjustador+='</div>';
	
	itemAjustador+='<div style="width: 240px;height: 10px;float:left;position:relative;" >';
	itemAjustador+='<div style="height:11px;float: left;text-align: left;margin-top: 3px;vertical-align:center;"><b>Disponibilidad:</b> '+ ajustador.disponibilidad +'</div>';
	itemAjustador+='</div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;"><b>Reportes Asignados:</b> '+ajustador.numeroReportesAsignados+' </div>';
	itemAjustador+='<div style="width: 240px;height: 19px;margin-top:17px;float:left;position:relative;" >';
	itemAjustador+='<div class="btn_back w60" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2px;">';
	itemAjustador+='<a href="javascript: void(0);" onclick="asignarAjustador('+ajustador.idAjustador+');">Asignar</a>';
	itemAjustador+='</div>';
	itemAjustador+='<div class="btn_back w30" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2px;">';
	itemAjustador+='<a href="javascript: void(0);" class="icon_ruta" onclick="obtenerRuta('+ajustador.latitud+','+ajustador.longitud+', true , '+ajustador.idControl+');"></a>';
	itemAjustador+='</div>';
	itemAjustador+='</div>';
	
	itemAjustador+='</div>';

	jQuery("#listadoAjustadores").append(itemAjustador);
}*/

/**
 * Alimentar listado de ajustadores en mapa de asignacion
 * @param ajustador
 * @param resetAjustadorData
 */
function setListadoAjustadoresMapa( ajustador , resetAjustadorData ){
	var itemAjustador="";
	var back="";
	var distance=ajustador.distanciaSiniestroKM.toString().slice(0,5);
	if( !ajustador.calcularRuta && !resetAjustadorData ){
		ajustador.distanciaSiniestroKM="Distancia a calcular";
	} else {
		ajustador.distanciaSiniestroKM=distance+" Km.";
	}	
	
	if( ajustador.estaAsignado ){		
		if(contieneReporte(ajustador, jQuery('#idToReporte').val())){			
			back='style="background-color:#1caabd !important;"'; //45B300
		}else{
			back='style="background-color:#F4DC53 !important;"';
		}
	}
	
	
	
	itemAjustador+='<div id="ajustador'+ajustador.id+'" '+back+' class="ajustadorItem googleStyle">';
	
	
	
	itemAjustador+='<div style="width: 100%;height: 10px;position:relative;" >';
	itemAjustador+='<div style="height:35px;float: left;text-align: left;margin-top: 2px;"><b>Nombres:</b> '+ajustador.nombreAjustador+'</div>';
	itemAjustador+='</div>';
	
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 2%;"><b>Distancia:</b> '+ajustador.distanciaSiniestroKM+'</div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 2%;"><b>ETA:</b> '+ (ajustador.duracion ?  ajustador.duracion +' aprox.' : 'No identificado')  +'</div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 2%;"><b>Fecha Posici&oacute;n:</b> '+ajustador.fechaCoordenadas+'</div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;"><b>Horario Disponibilidad:</b> '+ajustador.disponibilidad+' </div>';
	itemAjustador+='<div style="width:100%;height: 10px;float: left;text-align: left;margin-top: 3%;"><b>Reportes Asignados:</b> '+ajustador.numeroReportesAsignados+' </div>';
	
	itemAjustador+='<div style="width: 240px;height: 19px;margin-top:17px;float:left;position:relative;" >';
	itemAjustador+='<div class="btn_back w60" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2px;">';
	itemAjustador+='<a href="javascript: void(0);" onclick="asignarAjustador('+ajustador.idAjustador+');">Asignar</a>';
	itemAjustador+='</div>';
	
	itemAjustador+='<div class="btn_back w30" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2px;">';
	itemAjustador+='<a href="javascript: void(0);" class="icon_ruta" onclick="obtenerRuta('+ajustador.latitud+','+ajustador.longitud+', true , '+ajustador.idControl+');"></a>';
	itemAjustador+='</div>';
	
	if( ajustador.estaAsignado ){
		itemAjustador+='<div class="btn_back w60" style="display: inline; float: left; vertical-align: bottom; position: relative; margin-top: 2px;">';
		itemAjustador+='<a href="javascript: void(0);" onclick="verReportes('+ajustador.idAjustador+','+ jQuery('#idToReporte').val() + ');">Reportes</a>';
		itemAjustador+='</div>';
	}
	itemAjustador+='</div>';
	
	itemAjustador+='</div>';

	if( !resetAjustadorData ){
		 jQuery("#listadoAjustadores").append(itemAjustador);
	} else if( jQuery("#ajustador"+ajustador.id).length > 0 ) {
		jQuery("#ajustador"+ajustador.id).replaceWith(itemAjustador);
	}
}

function asignarAjustador( idAjustador ){
	if(confirm('\u00BFEst\u00E1 seguro que desea asignar este ajustador al reporte?')){
		var nombreAjustador = parent.document.getElementById("ajustadorNombre").value;
		parent.setAjustador(idAjustador, nombreAjustador);
	}
}

function clearMarkers(tipo){
	for (var i = 0; i < markers.length; i++) {
		if( markers[i].tipo== tipo )
			markers[i].setMap(null);
	}
}


function obtenerRutas(){
	for (var i = 0; i < markers.length; i++) {
		if( markers[i].tipo=="ajustador" && markers[i].calcularRuta.toString() == "true" ){
			setRoutes( siniestro.getPosition() , markers[i].getPosition() , resetAjustadorData , markers[i] );
		}
	}
}

function obtenerRuta( origen_latitud , origen_longitud , resetAjustadorData ,idAjustador ){
	try{		
		var origin = new google.maps.LatLng( origen_latitud , origen_longitud );
		var ajustador = ajustadores[idAjustador];
		setRoutes( origin , siniestro.getPosition() , resetAjustadorData , ajustador, map );
	}catch(ex){
		alert('La ruta no pudo ser obtenido debido a un error: ' + ex.message);
	}
}

function setMarcadorSiniestro(){	
	var map = getMap();
	if (map) {
		if(jQuery("#mostrarListadoAjustadores").val() )
			jQuery("#map").width(900);
		
		marcador = new Object();
		marcador.titulo = "Ubicación Siniestro";
		marcador.draggable = false;
		marcador.latitud = jQuery("#latitud").val();
		marcador.longitud = jQuery("#longitud").val();

		crearMarcadorSiniestro(marcador);		
		getAjustadores();
	} else {
		setTimeout("setMarcadorSiniestro()", '300');
	}
}

function crearMarcadorSiniestro(marcador){
		var mark = createMarker(marcador , getMap());
		
		google.maps.event.addListener(mark, 'click', (function(mark) {
	        return function() {
	          infowindow.setContent(generarInfoWindowReporte(mark));
	          infowindow.open(map, mark);
	        }
	      })(mark));
		
		setEventAndLatLng( mark );
		siniestro = mark;
		if( map ){
			map.setZoom(11);
			map.setCenter(mark.getPosition());
	}

}

function ponerMarcadorDeSiniestro( results, status ){
	marcador = new Object();
	marcador.titulo = "Ubicaci\u00F3n Siniestro";
	marcador.draggable = true;
	marcador.icon = ICON_REPORTE;
	
	if(status == google.maps.GeocoderStatus.OK){
		var map = getMap();
		map.setCenter(results[0].geometry.location);
		if( jQuery('#lugarAtencionOcurrido\\.codigoPostal').val() || jQuery('#codigoPostalOtraColonia_t').val() ){
			map.setZoom(15);
		} else {
			map.setZoom(6);
		}

		marcador.position = results[0].geometry.location;
		var marcadorCreado = createMarker(marcador);
		setEventAndLatLng( marcadorCreado );
	}
	
}

function setEventAndLatLng( marcador ){
	if (!jQuery("#latitud").val()){
		jQuery("#latitud").val(marcador.getPosition().lat());
	}
	if (!jQuery("#longitud").val()){
		jQuery("#longitud").val(marcador.getPosition().lng());
	}

	setEvent(marcador, 'dragend', function() {
		jQuery("#latitud").val(marcador.getPosition().lat());
		jQuery("#longitud").val(marcador.getPosition().lng());
	});
}

function getDireccionSiniestro(){
	var direccion,id;
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + ", ";
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		direccion+=jQuery('#lugarAtencionOcurrido\\.calleNumero').val() + ", ";
	}	
	direccion+=getColoniaCPMunicipioPais();

	return direccion; 
}

function getColoniaCPMunicipioPais(){
	var direccion,id;
	
	if( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == true){
		
		if( jQuery('#lugarAtencionOcurrido\\.otraColonia').val() ){
			id = "#lugarAtencionOcurrido\\.otraColonia";
			direccion+=jQuery(id+' option:selected').text()+ " ";
		}
		
		direccion+=jQuery('#codigoPostalOtraColonia_t').val()+ ", ";
		
		id = "#lugarAtencionOcurrido\\.municipio\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.estado\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.pais\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
	}else if ( jQuery('#lugarAtencionOcurrido\\.idColoniaCheck').attr('checked') == false){
		
		id = "#lugarAtencionOcurrido\\.colonia\\.id";
		direccion+=jQuery(id +' option:selected').text() + " ";
		
		direccion+=jQuery('#lugarAtencionOcurrido\\.codigoPostal').val() + ", ";
		
		id = "#lugarAtencionOcurrido\\.municipio\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.estado\\.id";
		direccion+=jQuery(id +' option:selected').text() + ", ";
		
		id = "#lugarAtencionOcurrido\\.pais\\.id";
		direccion+=jQuery(id +' option:selected').text();
	}

	return direccion;
}


/***
 * Obtener reportes donde un ajustador esta asignado y mostrarlos en mapa
 * @param ajustadorId
 */
function verReportes(ajustadorId, idToReporte){
	var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerReportesPorAjustador.action";
	jQuery.post(
		url, 
		{
			idAjustador   : ajustadorId,
			idToReporte 	  : idToReporte
		} ,
		function(response){
			clearMarkers(TIPO_REPORTE_ALTERNO);
			setUbicacionReportes(response, ajustadorId);			
		});
}

/**
 * Iterar listado de reportes y crear un marcador para cada uno. Sirve de respuesta para la consulta de reportes alternos para un ajustador
 * @param json
 * @param map
 */
function setUbicacionReportes(json, ajustadorId){
	reportes = json.listaReportes;
	var i;	
	var reportesSinCoordenadas = [];
	if( reportes.length > 0 ){
		var marcaReporte;
		for(var i=0; i < reportes.length; i++ ){
			reportes[i].idControl=i;
			reportes[i].tipo = TIPO_REPORTE_ALTERNO;
			reportes[i].icono = ICON_REPORTE_ALTERNO;
			var reporte = crearMarcadorReporte(reportes[i]);
			if(reporte == null){ //no coords
				reporte = {
						id: reportes[i].reporteId,							
						title: reportes[i].numeroReporte + ' : ' + reportes[i].estatus,
					    tipo: reportes[i].tipo,
					    numReporte : reportes[i].numeroReporte,
					    fechaAlta: reportes[i].fechaReporteStr,
					    fechaAsignacion: reportes[i].fechaAsignacionStr,
					    fechaContacto: reportes[i].fechaContactoStr,
					    cliente: reportes[i].cliente,
					    numPoliza: reportes[i].numPoliza
				};
				reportesSinCoordenadas.push(reporte);
				
			}
		}
	} 
	if(reportesSinCoordenadas.length > 0){		
		infowindow.setContent(generarInfoWindowNoCoordenadas(reportesSinCoordenadas));
		infowindow.open(map, obtenerMarcador(TIPO_AJUSTADOR, ajustadorId));
	}
}

/**
 * Refrescar pantalla de asignacion de ajustadores
 * @param reporteId
 */
function refrescar(reporteId){	
	var url = '/MidasWeb/siniestros/catalogo/servicio/mostrarMapaAjustadoresSiniestro.action?idToReporte='+
		reporteId+'&mostrarOficinas=true&mostrarInformacionReporte=true&mostrarListadoAjustadores=true';
	parent.redirectVentanaModalWithBlocking('vm_asignarAjustador', url, null);
}


/**
 * Consultar posicion ajustador a traves de DirectionService para ver distancia al objetivo y tiempo de llegada
 * @param ajustador
 * @param resetAjustadorData
 */
function consultarPosicionAjustador(ajustador , resetAjustadorData ){
	var dS = getDirectionsService();
	var dD = getDirectionsDisplay();
	var request = {
		origin:new google.maps.LatLng( ajustador.latitud , ajustador.longitud ),
		destination:siniestro.getPosition(),
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		durationInTraffic: true
	};
	
	dS.route(
		request, 
		function(result, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				var distancia = result.routes[0].legs[0].distance.value/1000;
				var tiempo = result.routes[0].legs[0].duration;
				ajustador.distanciaSiniestroKM=distancia;
				ajustador.duracion=tiempo.text;
				ajustadores[ajustador.idControl].distanciaSiniestroKM;
				ajustadores[ajustador.idControl].duracion;
				crearMarcadorAjustador(ajustador, jQuery('#idToReporte').val());	
				setListadoAjustadoresMapa( ajustador, true);		
			}
		}
	);
}


/**
 * Iterar listado de ajustadores y crear un marcador para cada uno. Sirve de respuesta para la consulta de ajustadores en asignacion de ajustador
 * @param json
 */
function cargarUbicacionesAjustadores(json){	
	var i;
	ajustadores = json.listaAjustadores;	
	jQuery("#listadoAjustadores").empty();
	clearMarkers(TIPO_AJUSTADOR);
	if( ajustadores.length > 0 ){
		for( i=0; i < ajustadores.length; i++ ){			
			ajustadores[i].idControl=i;
			setListadoAjustadoresMapa( ajustadores[i], false);	
			if(ajustadores[i].calcularRuta){
				consultarPosicionAjustador(ajustadores[i]);
			}else{
				crearMarcadorAjustador(ajustadores[i], jQuery('#idToReporte').val());
			}						
		}
	} else {
		jQuery("#listadoAjustadores").html(
				"<div id='listadoAjustadoresTxt'>No hay ajustadores para mostrar en esta oficina, no existen ajustadores en el horario actual de servicio o las coordenadas de su ubicaci\u00F3n no han sido reportadas.</div>")
		jQuery("#listadoAjustadoresTxt").css({"text-align":"left","font-size":"10pt"});
		jQuery("#listadoAjustadoresTxt").hide();
		jQuery("#listadoAjustadoresTxt").fadeIn( 400 );
	}
}

/**
 * 
 */
function obtenerMarcadorSiniestro(){
	var map = null;
	var reporte = null;
	while(map == null){
		map = getMap();
	}
	if(jQuery("#mostrarListadoAjustadores").val() ){
		jQuery("#map").width(900);
	}
	var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerInformacionReporte.action";	
	jQuery.getJSON( url ,
					{idToReporte : jQuery('#idToReporte').val()},
					function(response){
						var reporte = response;
						reporte.idControl=0;
						reporte.tipo = TIPO_REPORTE;
						reporte = determinarIcono(reporte);	
						if(reporte.asignadoA != 'Sin ajustador'){ //patch para cambio de color
							reporte.icono = ICON_REPORTE_ASIGNADO_REPORTE_ACTUAL;
						}
						siniestro = crearMarcadorReporte(reporte);												
						getAjustadores();
						map.setCenter(siniestro.getPosition());
					});	
}
