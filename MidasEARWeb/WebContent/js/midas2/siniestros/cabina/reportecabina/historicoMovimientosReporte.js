function mostrarHistoricoMovimientos(){
	formParams = jQuery(document.busquedaHistoricoMovimientos).serialize();
	var url = mostrarHistoricoMovimientosPath + '?' +  formParams ;
	sendRequestJQ(null, url, targetWorkArea, 'buscarHistoricoMovimientos(true);');
}

var historicoMovimientosGrid;
function buscarHistoricoMovimientos(mostrarSoloEncabezados){
	jQuery("#tituloListado").show();
	jQuery("#historicoMovimientosGrid").empty();
	jQuery("#mostrarSoloEncabezados").val(mostrarSoloEncabezados);
	
	historicoMovimientosGrid = new dhtmlXGridObject('historicoMovimientosGrid');
	historicoMovimientosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	historicoMovimientosGrid.attachEvent("onXLE", function(grid_obj){actualizarResumenMovimientos();unblockPage();});
	
	formParams = jQuery(document.busquedaHistoricoMovimientos).serialize();
	var url = buscarHistoricoMovimientosPath + '?' +  formParams ;
	historicoMovimientosGrid.load( url );
}

function limpiarFiltros(){
	jQuery('#busquedaHistoricoMovimientos').each (function(){
		  this.reset();
	});
}

function regresarReporteBusqueda(){
	var pantallaOrigen = jQuery("#pantallaOrigen").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	if(pantallaOrigen=='ORDENPAGOS'){
		var accionV = jQuery("#accion").val();
		var idOrdenCompraV = jQuery("#idOrdenCompra").val();
		var idOrdenPagoV = jQuery("#idOrdenPago").val();
		if(accionV=='consultar'){
			consultarOrdenPago(idOrdenPagoV,idOrdenCompraV,"consultar");
		}else if(accionV=='editar'){
			editarOrdenPago(idOrdenPagoV,idOrdenCompraV,"editar");
		}else{
			var url = regresarOrdenPago;
			sendRequestJQ(null, url,targetWorkArea, null);
		}
		
	}else if(pantallaOrigen == 'INGRESOS'){
		var tipoCancelacion = jQuery("#tipoDeCancelacion").val();
		var ingresoId = jQuery("#ingresoId").val();
		soloConsulta = (soloConsulta==1)? 'true' : (soloConsulta == 0)? 'false' : 'true';
		var url = '/MidasWeb/siniestros/recuperacion/ingresos/mostrarContenedor.action?soloConsulta=' + soloConsulta + '&ingresoId='+ingresoId + "&tipoDeCancelacion=" + tipoCancelacion;
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		var idToReporte 		= jQuery("#idToReporte").val();
		var url = regresarReporteCabina + "?idToReporte="+idToReporte;
		sendRequestJQ(null, url,targetWorkArea, soloConsulta==1?"setConsultaReporte();":null);
	}
}

function limpiarDivsGeneralHistoricoMovimientos() {
	limpiarDiv('contenido_afectacionReserva');
	limpiarDiv('contenido_gastosAjuste');
}

function verTabAfectacionReserva(){
	var idToReporte = jQuery("#idToReporte").val();
	var soloConsulta =  jQuery("#h_soloConsulta").val();
	var tipoCancelacion = jQuery("#tipoDeCancelacion").val();
	var esPantallaGA = false;
	limpiarDivsGeneralHistoricoMovimientos();
	var url = mostrarHistoricoMovimientosPath + "?idToReporte=" + idToReporte + "&soloConsulta=" + soloConsulta + "&esPantallaGA=" + esPantallaGA +"&tipoDeCancelacion=" + tipoCancelacion;;
	sendRequestJQ(null, url, 'contenido_afectacionReserva', 'buscarHistoricoMovimientos(true);');
}

function verTabGastosAjuste(){
	var idToReporte = jQuery("#idToReporte").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	var tipoCancelacion = jQuery("#tipoDeCancelacion").val();
	var esPantallaGA = true;
	limpiarDivsGeneralHistoricoMovimientos();
	var url = mostrarHistoricoMovimientosPath + "?idToReporte=" + idToReporte + "&soloConsulta=" + soloConsulta + "&esPantallaGA=" + esPantallaGA +"&tipoDeCancelacion=" + tipoCancelacion;;
	sendRequestJQ(null, url, 'contenido_gastosAjuste', 'buscarHistoricoMovimientos(true);');
}

function actualizarResumenMovimientos(){
	var esPantallaGA = jQuery("#esPantallaGA").val();
	if(esPantallaGA == 'true'){
		var totalGasto = historicoMovimientosGrid.getUserData("","userData_totalGastos");
		jQuery("#totalGastos_t").val(totalGasto);
		var totalRecuperacionGasto = historicoMovimientosGrid.getUserData("","userData_totalRecuperacionGastos");
		jQuery("#totalRecuperacionGastos_t").val(totalRecuperacionGasto);
	}else if(esPantallaGA = 'fasle'){
		var totalEstimado = historicoMovimientosGrid.getUserData("","userData_totalEstimado");
		jQuery("#totalEstimado_t").val(totalEstimado);
		var totalPagado = historicoMovimientosGrid.getUserData("","userData_totalPagado");
		jQuery("#totalPagado_t").val(totalPagado);
		var reservaPendiente = historicoMovimientosGrid.getUserData("","userData_reservaPendiente");
		jQuery("#reservaPendiente_t").val(reservaPendiente);
		var totalIngresos = historicoMovimientosGrid.getUserData("","userData_totalIngresos");
		jQuery("#totalIngresos_t").val(totalIngresos);
	}
	initCurrencyFormatOnTxtInput();
}

/**
 * @deprecated ahora se calcula los montos desde el action, debido a que solo calcula los montos para la primera pagina del grid
 */
function calcularResumenMovimientos(){
	var esPantallaGA = jQuery("#esPantallaGA").val();
	var TIPODOCTO_ESTIMACION = 'ESTIMACION';
	var TIPODOCTO_PAGO = 'PAGO';
	var TIPODOCTO_INGRESO = 'INGRESO';
	var COLUMNA_TIPODOCUMENTO_INDEX = 2;
	var COLUMNA_IMPORTE_INDEX = 8;
	//GastosAjuste
	var totalGasto = 0;	var totalRecuperacionGasto = 0;
	//AfectacionReserva
	var totalEstimado = 0; var pagado = 0; var reservaPendiente = 0; var ingresos = 0;
	if(historicoMovimientosGrid){
		historicoMovimientosGrid.forEachRow(function(id) {
			var tipoDocumento = historicoMovimientosGrid.cells(id,COLUMNA_TIPODOCUMENTO_INDEX).getValue();
			console.log("TIPO DE DOCUMENTO=" + tipoDocumento);
			var importe = historicoMovimientosGrid.cells(id,COLUMNA_IMPORTE_INDEX).getValue();
			console.log("IMPORTE=" + importe);
			if(isNaN(importe)){importe = 0};
			console.log("IMPORTE DESPUES NAN=" + importe);
			if(esPantallaGA == 'true'){		//GastosAjuste
				if(tipoDocumento == TIPODOCTO_INGRESO){
					totalRecuperacionGasto += importe;
				}else if(tipoDocumento == TIPODOCTO_PAGO){
					totalGasto += importe;
				}
			}else if(esPantallaGA == 'false'){
				if(tipoDocumento == TIPODOCTO_ESTIMACION){
					totalEstimado += importe;
				}else if(tipoDocumento == TIPODOCTO_PAGO){
					pagado += importe;
				}else if(tipoDocumento == TIPODOCTO_INGRESO){
					ingresos += importe;
				}
			}
		});
		if(esPantallaGA == 'true'){
			console.log("TOTAL GASTOS=" + totalGasto + " - TOTAL RECUPERACIONES=" + totalRecuperacionGasto);
			jQuery("#totalGastos_t").val(totalGasto);
			jQuery("#totalRecuperacionGastos_t").val(totalRecuperacionGasto);
		}else if(esPantallaGA == 'false'){
			reservaPendiente = totalEstimado - pagado;
			console.log("RESERVA PENDIENTE = " + reservaPendiente + " - TOTAL ESTIMADO=" + totalEstimado+ " - PAGADO=" + pagado + " - INGRESOS=" + ingresos);
			jQuery("#totalEstimado_t").val(totalEstimado);
			jQuery("#totalPagado_t").val(pagado);
			jQuery("#reservaPendiente_t").val(reservaPendiente);
			jQuery("#totalIngresos_t").val(ingresos);
		}
		
	}
}