
/**
 * Asocia la solicitud al reporte
 */
function asignarCartaCobertura(){
	var url = asignarCartaCoberturaPath;
	parent.redirectVentanaModal('vm_solicitud', url, cartaCoberturaForm);	
}
/**
 * Muestra documento de carta cobertura
 */
function mostrarDocumentoCartaCobertura(idToControlArchivo){
	if(null==idToControlArchivo || idToControlArchivo==""){
		alert("No cuenta con Carta Cobertura Asociada ");
		
	}else{
		descargarDocumento(idToControlArchivo);
	}
}

/**
 * Cierra el Pop-up, regresando a la pantalla anterior
 */
function cerrar(){
	parent.cerrarVentanaModal( "vm_solicitud", null );
}