jQuery(document).ready(
	function(){
		jQuery('#nsOficina').focus();
		if( jQuery("#id").val() ){
			jQuery("#ofinasActivas :not(:selected)").hide();
		}
		jQuery(".readOnly").attr("readonly","true");
		jQuery("#idToReporte").val(jQuery('#id').val());
		jQuery(".numeric").keyup(function () {
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});

		jQuery(".requerido").blur(
			function(){
				var these = jQuery(this);
				if( isEmpty( these.val() ) ){
					these.addClass("errorField");
				} else {
					these.removeClass("errorField");
				}
			}
		);
		
		jQuery("#ofinasActivas").change(function(){jQuery(this).blur();});
//		jQuery('#horaOcurrido').mask('00:00');
//		jQuery('#horaReporte').mask('00:00');
//		jQuery('#horaAsignacion').mask('00:00');
//		jQuery('#horaContacto').mask('00:00');
//		jQuery('#horaTerminacion').mask('00:00');
		
	}
);

function viewWindow( idVentana , url , title ){
	//jQuery(".requerido").blur();
	var message ='En caso de que sea Reporte Movil y antes de asignar el ajustador: <br/><br/>1: Asegurese de tener el reporte completo (Nombres, Poliza, Lugar de Atencion, oficina) <br/><br/>2: Terminar el reporte Movil con el numero de reporte cabina';
	var id = jQuery("#id").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
	if( isEmpty(id) ){
		mostrarMensajeInformativo('Debe haber guardado el reporte previamente para abrir esta opción', '20');
		return;
	}	
	if( jQuery(".errorField").length <= 0 || title == 'Lugar Ocurrido' ){
		switch (idVentana) {
		case "textolibre":		
			if( jQuery("#consulta").length ){
				mostrarVentanaModal(idVentana, title, 200, 120, 450, 250,url,'setConsulta();');
			} else {
				var win = viewAndGetModalWindow(idVentana, title, 200, 120, 450, 250,url,'');
				win.attachEvent(
					"onClose", 
					function(win){
						if( confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?') ){
							return true;
						} else {
							return false;
						}
					}
				);
			}			
			break;
		case "mostrarCitaRep":
			 mostrarVentanaModal(idVentana, title, 100, 200, 1000, 300, url, '');
			 jQuery("#target").val(target);
			break;
		case "vm_LugarAttOcc":	
			mostrarVentanaModal(idVentana, title,  1, 1, 1020, 650, url, null);
			break;
		case "mostrarGastoAjuste":
			mostrarVentanaModal(idVentana, title, 100, 100, 1000, 450, 
					url + '?idToReporte=' + id + "&soloConsulta=" + soloConsulta, null);			
			break;
		case "mostrarAjustadorMapa":
			mostrarMensajeAsignarAjustador(message, '20', 
					'mostrarAjustadorEnElMapa()', null, null);
			break;
		case "asignarAjustador":
				mostrarMensajeAsignarAjustador(message, '20', 
								'asignarAjustador()', null, null);
			break;
		case "mostrarHistoricoMovimientos":
			mostrarHistoricoMovimientos();
			break;
		default:
			mostrarMensajeInformativo('System Error.', '10');
			break;
		}
	} else {
		mostrarMensajeInformativo('Debe de capturar previamente los datos obligatorios y guardar el reporte para abrir esta opción', '20');
	}
}

function agregarTextoLibre( action , title, target, actionToSave  ){
	jQuery("#target").val(target);
	jQuery("#target").attr("actionToSave",actionToSave);
	var targetId = jQuery("#target").val();
	var valueTextoLibre = jQuery("#"+targetId).val();
	var url = '/MidasWeb/siniestros/cabina/reportecabina/'+action+'.action';
	viewWindow( "textolibre", url, title );
}

function mostrarCitaReporte(action , title, target ){
	var idToReporte = jQuery("#idToReporte").val();
    var url = '/MidasWeb/siniestros/cabina/reportecabina/' + action + '.action?idToReporte='+idToReporte;
    viewWindow( "mostrarCitaRep", url, "Citas" );
}

function buscarReporte(){
	var numeroSiniestro = jQuery("#nsOficina").val()+ "-" + jQuery("#nsConsecutivoR").val() + "-" + jQuery("#nsAnio").val();
	
	if( !isEmpty(jQuery("#nsOficina").val()) && !isEmpty(jQuery("#nsConsecutivoR").val()) && !isEmpty(jQuery("#nsAnio").val()) ){
		var params = "";
		params+="nsOficina="+jQuery("#nsOficina").val()+"&";
		params+="nsConsecutivoR="+jQuery("#nsConsecutivoR").val()+"&";
		params+="nsAnio="+jQuery("#nsAnio").val();
		
		sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?"+params,"contenido_reportecabina", "validaEncontrarReporte();");
	} else {
		mostrarMensajeInformativo('No se ha ingresado un número de reporte completo.', '20');
		jQuery("#nsOficina").focus();
	}
}

function salvarReporte(desplegarMensajeExito){
	validaRequeridos();
	validaFechaOcurrido();
	//validaFechaReporteOcurrido(); //no se requiere ya que se valida a nivel servicio
	
	if( jQuery(".errorField").length == 0 ){
		
		var formParams = jQuery("#reporteCabinaForm").serialize();
		formParams = formParams.concat('&despliegaMensajeExito=' + desplegarMensajeExito);
		var url = "/MidasWeb/siniestros/cabina/reportecabina/salvarReporte.action?" + formParams;
		
		sendRequestJQ(null, url, 'contenido_reportecabina', '');
	} else {
		mostrarMensajeInformativo('Favor de llenar los campos obligatorios.', '20');
	}
}

function validaFechaReporteOcurrido(){
	var fechaReporte  = jQuery("[name='entidad.fechaHoraReporte']").val();
	var horaReporte   = jQuery("#horaReporte").val();
	var fechaOcurrido = jQuery("#fechaOcurrido").val();
	var horaOcurrido  = jQuery("#horaOcurrido").val();
	
	if( ( (fechaReporte.length > 0) && (horaReporte.length > 0) ) && ( (fechaOcurrido.length > 0) && (horaOcurrido.length > 0 ) ) ){
		
		var aFechaReporte  = fechaReporte.split("/");
		var aHoraReporte   = horaReporte.split(":");
		
		var aFechaOcurrido = fechaOcurrido.split("/");
		var aHoraOcurrido  = horaOcurrido.split(":");
		
		var dateReporte  = new Date(aFechaReporte[2] ,parseInt(aFechaReporte[1])-1 ,aFechaReporte[0] ,aHoraReporte[0] ,aHoraReporte[1] ,0,0);
		var dateOcurrido = new Date(aFechaOcurrido[2],parseInt(aFechaOcurrido[1]-1),aFechaOcurrido[0],aHoraOcurrido[0],aHoraOcurrido[1],0,0);
		
		if ( dateOcurrido > dateReporte ){
			mostrarMensajeInformativo('La fecha de ocurrido no puede ser mayor a la del reporte.', '20');
			jQuery("#horaOcurrido").addClass("errorField");
			jQuery("#fechaOcurrido").addClass("errorField");
		}else{
			jQuery("#fechaOcurrido").removeClass("errorField");
			jQuery("#horaOcurrido").removeClass("errorField");
		}
	}
}

/***
 * Valida si el usuario selecciona una fecha ingresa la hora o viceversa
 */
function validaFechaOcurrido(){
	var fechaOcurrido = jQuery("#fechaOcurrido").val();
	var horaOcurrido  = jQuery("#horaOcurrido").val();	

	if( fechaOcurrido.length > 0 & horaOcurrido.length == 0 ){
		mostrarMensajeInformativo('Ingrese la hora de ocurrido.', '20');
		jQuery("#horaOcurrido").addClass("errorField");
	}else if( fechaOcurrido.length == 0 & horaOcurrido.length > 0 ){
		mostrarMensajeInformativo('Ingrese la fecha de ocurrido.', '20');
		jQuery("#fechaOcurrido").addClass("errorField");
	}else{
		jQuery("#fechaOcurrido").removeClass("errorField");
		jQuery("#horaOcurrido").removeClass("errorField");
	}
}

function validaRequeridos(){
	var requeridos = jQuery(".requerido");
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField");
			} else {
				these.removeClass("errorField");
			}
		}
	);
}

/*function cerrarVentanaReporte(){
	var url = "";
	if(jQuery(".consulta").length > 0){
		url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action";
		sendRequestJQ(null, url,"contenido", "");
	} else {
		if(confirm("Si continua, los datos que no se guardaron, se perderan.")){
			if (jQuery("#ventanaOrigen").val() == "LR"){
				url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action";
				
			}else if (jQuery("#ventanaOrigen").val() == "MR"){
				url = "/MidasWeb/siniestros/cabina/monitor/mostrarMonitor.action";
				
			}else{
				url = "/MidasWeb/siniestros/cabina/mostrarContenedor.action";
			}
			sendRequestJQ(null, url,"contenido", "");
		
		}
	}
}*/

function cerrarVentanaReporte() {
	var url = "";
	if(jQuery(".consulta").length > 0){
		url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action";
		sendRequestJQ(null, url,"contenido", "");
	} else {
		if(confirm("Si continua, los datos que no se guardaron, se perderan.")){
			url = "/MidasWeb/siniestros/cabina/reportecabina/cerrarVentanaReporte.action";
			sendRequestJQ(null, url,"contenido", "");
		}
	}
}

function validar(){
	var id = jQuery("#id").val();
	if( !isEmpty(id) ){
		mostrarMensajeInformativo('Registro guardado exitosamente.', '30');
	} else {
		mostrarMensajeInformativo('Error al guardar.', '10');
	}
}

function mostrarGastoAjuste(action , title, target ){
	/*var url = '/MidasWeb/siniestros/reporte/gastoAjuste/' + action + '.action';
    viewWindow( "mostrarGastoAjuste", url, "Gastos de Ajuste" );*/
	var id = jQuery("#idToReporte").val();
	var h_soloConsulta = jQuery("#h_soloConsulta").val();
	var modoConsulta = false;
	if (h_soloConsulta=="1"){
		modoConsulta=true;
	}
    if( !isEmpty(id) || id != 0 ){

    	sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/mostrar.action'+ '?idReporteCabina=' + id+'&tipo=GA'+'&modoConsulta='+modoConsulta,targetWorkArea,null);
    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    	
    	}
}

function validaEncontrarReporte(){
	var id = jQuery("#id").val();
	if( isEmpty(id) ){
		mostrarMensajeInformativo('No se encontrarón resultados.', '20');
	}
}

function mostrarLugarOcurrido(idVentana , url , title){
	mostrarVentanaModal(idVentana, title, null, null, 1020, 500, url, null);
}

function guardarReporteYMostrarLugarAtencion(){
	validaRequeridos();
	if( jQuery(".errorField").length == 0 ){	
		if(  jQuery("#idToReporte").val() == ""){
			var formParams = jQuery("#reporteCabinaForm").serialize();
			var funcionMostrarLugar = "mostrarLugarReporte('Lugar Atenci\u00F3n','AT');";
			sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/salvarReporte.action?" + formParams, 'contenido_reportecabina', funcionMostrarLugar);
		} else {
			mostrarLugarReporte('Lugar Atenci\u00F3n','AT');
		}
	} else {
		mostrarMensajeInformativo('Debe de capturar previamente los datos obligatorios', '20');
	}
}

function mostrarLugarReporte(tituloVentana, tipoLugar){
	var idReporteCabina = jQuery("#idToReporte").val();
	var url = '/MidasWeb/siniestros/cabina/reportecabina/mostrarLugar.action?idToReporte=' + idReporteCabina + '&tipoLugar=' + tipoLugar + '&tituloVentana=' + tituloVentana;
	viewWindow("vm_LugarAttOcc", url, tituloVentana);
}

function mismoConductor(){
	jQuery("#conductor").val(jQuery("#reporta").val());
}

function mostrarReferenciasBancarias(){
	var reporteCabinaId = jQuery("#id").val();
	if (reporteCabinaId) {
		sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/referenciasbancarias/mostrarContenedor.action"+"?entidad.id=" + reporteCabinaId, "contenido","");	
	} else {
		mostrarMensajeInformativo("Se requiere hacer una busqueda primero.", '20');
	}
}


function setNew(){
	if(confirm("Si continua, los datos que no se guardaron, se perderan.")){
		sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/mostrarContenedor.action", "contenido","");
	}
}

function cancelarReporte(){
	if(confirm("¿Desea cancelar el Reporte?")){
		var formParams = jQuery("#reporteCabinaForm").serialize();
		
		jQuery.ajax({
	          url: validarPendientesReporte + '?' + formParams,
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: null,
	          success: function(json){
	        	
	        	 unblockPage();
	        	 var estatus = json.estatusPendienteReporte;
	          	 if (estatus == 0) {	          	
	          		sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/cancelarReporte.action?" + formParams, 'contenido_reportecabina', null);	          		
	          	 } else if (estatus == 1) {
	          		 mostrarMensajeInformativo(json.mensaje, '10');
	          	 } else if (estatus == 2) {
	          		if(confirm('El reporte cuenta con reserva pendiente. \u00BFDesea depurar la reserva?')){
	          			sendRequestJQ(null, "/MidasWeb/siniestros/cabina/reportecabina/cancelarReporte.action?" + formParams, 'contenido_reportecabina', null);
	        		}
	          	 }
	         	
	          },
	          beforeSend: function(){
					blockPage();
				}	
	    });
		
	}
}

function validacionEstatusReporte(){
	var estatus = jQuery("#entidad_estatus").val();
	var noEditableParaRol = jQuery("#noEditableParaRol").val();
	if(noEditableParaRol == null || noEditableParaRol == ''){
		noEditableParaRol = 'false';
	}
	if(estatus == 2 || noEditableParaRol == 'true'){
		setConsultaReporte();
	} else if(estatus == 3 || estatus == 7){
		setConsultaReporteTerminado();
	} 

	if(estatus == 2 || estatus == 4 || estatus == 5) {
		jQuery("#btn_reaperturar").show();
	} else{
		jQuery("#btn_reaperturar").hide();
	}
}

function setConsultaReporte(){
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
	jQuery("#cancelar").remove();
	jQuery("#nuevo").remove();
	jQuery("#guardar").remove();
	jQuery("#btn_Busqueda").remove();
	jQuery("#btn_BusquedaPoliza").remove();
	jQuery("#btn_asignarAjustador").remove();
	jQuery("#contenido").append("<div id='consulta' style='display:none;' >1</div>");
	jQuery("#b_generar").remove();
	jQuery("#h_soloConsulta").val(1);
	jQuery("#btn_generaFechaTermino").remove();
	jQuery("#btn_copiarPersona").remove();
	jQuery("#div_asignacionAjustador").remove();
	jQuery(".ui-datepicker-trigger").remove();
//	jQuery("input").attr('disabled','disabled');
}

function setConsultaReporteTerminado(){
	jQuery("#btn_BusquedaPoliza").remove();
	jQuery("#btn_asignarAjustador").remove();
	jQuery("#btn_generaFechaTermino").remove();
	jQuery("#btn_generaFechaContacto").remove();
	jQuery("#div_asignacionAjustador").remove();
	jQuery(".ui-datepicker-trigger").remove();
}

function isSupervisorCabina(){
	
	if(jQuery("#isEditable").val() == "SI")
	{	
		
		jQuery("#fechaTerminacion").attr("disabled",false);
		jQuery("#horaTerminacion").attr("disabled",false);
		jQuery("#horaContacto").attr("disabled",false);
		jQuery("#entidad\\.fechaHoraContacto").attr("disabled",false);
		jQuery("#btnActualizaFechas").show();
	}else
	{
		jQuery("#fechaTerminacion").attr("disabled",true);
		jQuery("#horaTerminacion").attr("disabled",true);
		jQuery("#horaContacto").attr("disabled",true);
		jQuery("#entidad\\.fechaHoraContacto").attr("disabled",true);
		jQuery("#btnActualizaFechas").hide();
	}
}

function actualizaFechas()
{
	var fechaTerminacion  = jQuery("#fechaTerminacion").val();
	var horaTerminacion   = jQuery("#horaTerminacion").val();
	var horaContacto      = jQuery("#horaContacto").val();
	var fechaHoraContacto = jQuery("#entidad\\.fechaHoraContacto").val();
	
	if( fechaHoraContacto != '')
		if(fechaValida(fechaHoraContacto) != false){
			if(horaValida(horaContacto) == null)
				alert("Hora Contacto.El formato de hora es incorrecto.");
			else
				generarFechaHoraContacto();
		}
		else
			alert("Fecha Contacto.El formato de fecha es incorrecto.");
	
	if( fechaTerminacion != '')
		if(fechaValida(fechaTerminacion) != false){
			if(horaValida(horaTerminacion) == null)
			{		
				alert("Hora Terminacion.El formato de hora es incorrecto.");
			}
			else
			{
				generarFechaHoraTerminoAjustador();
			}
		}else
			alert("Fecha Termino.El formato de fecha es incorrecto.");
	
}

function fechaValida(texto) {
    let partes = /^(\d{1,2})[/](\d{1,2})[/](\d{3,4})$/.exec(texto);
    if (!partes) return false; //no coincide el regex

    //Obtener las partes
    let d = parseInt(partes[1], 10),
        m = parseInt(partes[2], 10),
        a = parseInt(partes[3], 10);

    //Validar manualmente
    if (!a || !m || m > 12 || !d) return false;

    let diasPorMes = [31,28,31,30,31,30,31,31,30,31,30,31 ];

    //Si es bisiesto, febrero tiene 29
    if (m == 2 && (a % 4 == 0 && a % 100 != 0) || a % 400 == 0)
        diasPorMes[1] = 29;

    //Que no tenga más días de los permitidos en el mes
    if (d > diasPorMes[m - 1]) return false;
    
    //Fecha válida
    return new Date(a,m,d);
}

function horaValida(texto) {
	
    return /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/.exec(texto);
   
}

//function mostrarLugarReporte(tituloVentana, tipoLugar){
//	var idReporteCabina = jQuery("#idToReporte").val();
//	var url = '/MidasWeb/siniestros/cabina/reportecabina/mostrarLugar.action?idToReporte=' + idReporteCabina + '&tipoLugar=' + tipoLugar + '&tituloVentana=' + tituloVentana;
//	viewWindow("vm_LugarAttOcc", url, tituloVentana);
//}

function asignarAjustador(){
	var idReporteCabina=jQuery("#idToReporte").val();
	var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/mostrarContenedorDeAsignacion.action?modoAsignar=true&reporteCabinaId=" + idReporteCabina+ "&tipoServicioOperacion=1" ;
	mostrarVentanaModal("vm_asignarAjustador", "Asignar Ajustador", null, null, 1169, 650, url , "");
	

}

function setAjustador(ajustadorId,ajustadorNombre){
	jQuery("#ajustadorIdTcAjustador").val(ajustadorId);
	jQuery("#ajustadorNombre").val(ajustadorNombre);
	parent.cerrarVentanaModal("vm_asignarAjustador",true);
	salvarReporte(true);
}


function getFechaActual(){
	var date 	= new Date();
	var mes 	= date.getMonth()+ 1;
	
	var fecha = date.getDate()+ "/" + mes + "/" + date.getFullYear(); 
	
	return fecha;
}

function getHoraActual(){
	var date 	= new Date();
	var mes 	= date.getMonth()+ 1;
	
	var hr = date.getHours()+":"+date.getMinutes(); 
	
	return hr;
}

function getFechaActualMillis(){
	var date 	= new Date();
	
	return date.getTime();
}

/**
 * Limpia el DIV de la informacion de Poliza
 */
function limpiarDivDatosVehiculo() {
	document.getElementById("contenedorPoliza").innerHTML = '';
}

/**
 * Prueba para cargar los datos de poliza.
 */
function cargarDatosPoliza(){
	var incisoContinuityId 			= jQuery("#h_incisoContinuityId").val();
	var fechaReporteSiniestro 		= jQuery("#h_fechaReporteSiniestro").val();
	
	var url = mostrarDatosIncisoPoliza 
				+"?incisoContinuityId="+incisoContinuityId
				+"&fechaReporteSiniestro="+fechaReporteSiniestro;
	
	sendRequestJQ(null, url, 'contenedorPoliza', null);
}



function mostrarBitacora(action  ){ 
    var id = jQuery("#idToReporte").val();
    var soloConsulta = jQuery("#h_soloConsulta").val();
    if( !isEmpty(id) || id != 0 ){
    	 var url = '/MidasWeb/siniestros/cabina/reporteCabina/bitacoraEvento/mostrarContenedor.action'
    		 + '?reporteCabinaId=' + id + "&soloConsulta=" + soloConsulta; 
    	 sendRequestJQ(null,url,targetWorkArea,null);
    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    	
    	}
}

function mostrarAfectacionInciso(){
	var idReporteCabina = jQuery("#idToReporte").val();
    if( !isEmpty(idReporteCabina) || idReporteCabina != 0 ){
    	
    	irAfectacionInciso(idReporteCabina);
    	
    }else{
    	mostrarMensajeInformativo('Debe contar con número de reporte para abrir esta opción', '20');
    }
}

function irAfectacionInciso(idReporteCabina) {
	var urlPath = '/MidasWeb/siniestros/cabina/siniestrocabina/puedeVerAfectacionInciso.action?siniestroDTO.reporteCabinaId=' + idReporteCabina +'&vieneExpedienteJuridico=true&expedienteJuridico.id='+jQuery("#idExpedienteJuridico").val()+'&modoConsultaExpediente='+jQuery("#h_modoConsultaExpediente").val();		
	jQuery.ajax({
		url: urlPath,
		dataType: 'json',
		async:false,
		type:"GET",
		data: null,
		success: function(json){
			unblockPage();
			mostrarAfectacionIncisoCallBack(json);
		},
		beforeSend: function(){
			blockPage();
		}		
	});
}

/* (NOTA) la funcion regresarAfectacionIncisoCallBack del JS  ordenesCompra.js esta basado en este. 
 * si esta funcion se cambia, realizar el ajuste en dicha funcion .
 * */
function mostrarAfectacionIncisoCallBack(json){
	var idReporteCabina = jQuery("#idToReporte").val();
	var validOnMillis = jQuery("#h_validOnMillis").val();
	var recordFromMillis = jQuery("#h_recordFromMillis").val();
	
	var soloConsulta = null;
	
	if( jQuery("#consulta").length ){
		soloConsulta = 1;
	} else {
		soloConsulta = 0;
	}
	
	var puedeVerAfetacion = json.verAfectacionInciso;
	var expedienteJuridico = json.expedienteJuridico;
	if(puedeVerAfetacion){
		var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId="+ idReporteCabina + '&soloConsulta=' + soloConsulta + '&validOnMillis=' + validOnMillis + '&recordFromMillis=' + recordFromMillis;
		sendRequestJQ(null, url, 'contenido', null);
	}else{
		mostrarMensajeInformativo('Es necesario guardar previamente la información del inciso', '20');
	}
}


function CheckTime(str)
{
hora=str.value
if (hora=='') {return}
if (hora.length>5) {alert("Introdujo una cadena mayor a 8 caracteres");return}
if (hora.length!=5) {alert("Introducir HH:MM");return}
a=hora.charAt(0) //<=2
b=hora.charAt(1) //<4
c=hora.charAt(2) //:
d=hora.charAt(3) //<=5
if ((a==2 && b>3) || (a>2)) { mostrarMensajeInformativo('El valor que introdujo en la Hora no corresponde, introduzca un digito entre 00 y 23', '20');$('#horaOcurrido').focus();return}
if (d>5) {mostrarMensajeInformativo("El valor que introdujo en los minutos no corresponde, introduzca un digito entre 00 y 59",'20');$('#horaOcurrido').focus();return}
if (c!=':') {mostrarMensajeInformativo("Introduzca el caracter ':' para separar la hora, los minutos",'20');$('#horaOcurrido').focus();return}
}

function copiarEnter(e) {
    if (e.keyCode == 13) {
    	mismoConductor(); 
    }
}

function setConsultaLugar(){
	
	jQuery(".setNew").attr("disabled","disabled");
	jQuery(".setNew").addClass("consulta");
}

function validateBotonConsulta(){
	var incisoContinuityId 		= jQuery("#h_incisoContinuityId").val();
	
	if(incisoContinuityId != ""){
		jQuery("#btn_ConsultaPoliza").css('display','inline');
	}
}

function validateFechaHoraTerminacion(){
	/*var idToSolicitud 			= jQuery("#h_idToSolicitud").val();
	var incisoContinuityId 		= jQuery("#h_incisoContinuityId").val();
	
	if(incisoContinuityId != ""){
		jQuery("#fechaTerminacionEdicion").css('display','block');
		jQuery("#fechaTerminacionConsulta").css('display','none');
	}
	else if(idToSolicitud != ""){
		jQuery("#fechaTerminacionEdicion").css('display','block');
		jQuery("#fechaTerminacionConsulta").css('display','none');
	}else{
		jQuery("#fechaTerminacionEdicion").css('display','none');
		jQuery("#fechaTerminacionConsulta").css('display','block');
	}*/
}


function validateFechaHoraOcurrido(){
	var validacionFechaHoraOcurrido 			= jQuery("#h_validacionFechaHoraOcurrido").val();

		if(validacionFechaHoraOcurrido == 'true'){
			mostrarMensajeInformativo('Se guardo la fecha y hora de ocurrido, esta ya no se podra modificar. Para modificarlo se tiene que cancelar este reporte y empezar uno nuevo', '30');
		}

		
	}

function mostrarAjustadorEnElMapa(){
	var idLugarAtencionOcurrido = jQuery("#idLugarAtencion").val();
	var latitudLugar = jQuery("#latitudLugar").val();
 	var longitudLugar = jQuery("#longitudLugar").val();
	
	if( idLugarAtencionOcurrido && latitudLugar && longitudLugar ){
		var id = jQuery("#idToReporte").val();
		var url = '/MidasWeb/siniestros/catalogo/servicio/mostrarMapaAjustadoresSiniestro.action?idToReporte='+id+'&mostrarOficinas=true&mostrarInformacionReporte=true&mostrarListadoAjustadores=true';
		mostrarVentanaModal("vm_asignarAjustador", "Ajustadores en mapa", 0, null, 1250, 600,url,'');
	} else {
		mostrarMensajeInformativo('Debe de guardar y confirmar previamente el lugar de atención del reporte, para abrir esta opción', '20');
	}
}

function buscarReporteEnter(e) {
    if (e.keyCode == 13) {
    	buscarReporte();
    }
}


function imprimirReporte(){
	var idToReporte = jQuery("#idToReporte").val();
	var url="/MidasWeb/siniestros/cabina/reportecabina/imprimirReporteCabina.action?idToReporte="+idToReporte;
	window.open(url, "ReporteCabina");
}

function mostrarHistoricoMovimientos(){
	var id = jQuery("#idToReporte").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();
  	var url = "/MidasWeb/siniestros/cabina/reportecabina/historicomovimientos/mostrarContenedor.action?idToReporte="
  		+id + "&soloConsulta=" + soloConsulta;
  	sendRequestJQ(null, url, targetWorkArea, null);
}

function actualizaValorTime(idValor, valor) {
	jQuery('#' + idValor).mask('00:00');
}

function generarFechaHoraContacto(){	
	
	var id = jQuery("#idToReporte").val();
	if(id == null || id == ''){
		mostrarMensajeInformativo('Debe existir un reporte para poder generar la fecha y hora de contacto.', '20');
		return;
	}	
	var fechaHoraAsignacion = jQuery('#fechaHoraAsignacion').val();
	if(fechaHoraAsignacion == null || fechaHoraAsignacion === ''){
		mostrarMensajeInformativo('Debe existir una fecha de asignaci\u00F3n para poder generar la fecha y hora de contacto.', '20');
		return;
	}	
	if(confirm('\u00BFEst\u00E1 seguro que desea generar la fecha/hora de contacto? Una vez generada no ser\u00E1 modificable.')){
		jQuery.ajax({
	          url: '/MidasWeb/siniestros/cabina/reportecabina/generarFechaHoraContacto.action?idToReporte='+id+"&isEditable="+jQuery("#isEditable").val()+"&entidad.fechaHoraContacto="+jQuery("#entidad\\.fechaHoraContacto").val()+"&horaContacto="+jQuery("#horaContacto").val(),
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: null,
	          success: function(json){
	        	 unblockPage();
	          	 var fechaHora = json.fechaHoraDTO;
	          	 var descripcion = fechaHora.descripcion;
	          	 if(descripcion != null && descripcion !=''){
	          		mostrarMensajeInformativo(descripcion, '20');
	          	 }else{
	               jQuery("#entidad\\.fechaHoraContacto").val(fechaHora.fecha);
	               jQuery("#horaContacto").val(fechaHora.hora);
	          	 }
	          },
	          beforeSend: function(){
					blockPage();
				}	
	    });			    
	}
}

function regresaBandejaDeAutorizaciones(){
	var url = "/MidasWeb/siniestros/autorizacion/solicitudAutorizacionAntiguedad/mostrarContenedor.action?";
  	sendRequestJQ(null, url, targetWorkArea, null);
}

function regresaExpedienteJuridico(){
	var url = "/MidasWeb/siniestros/expedientejuridico/mostrarContenedorExpedienteJuridico.action?expedienteJuridico.id="+jQuery("#idExpedienteJuridico").val()+"&esConsulta="+jQuery("#h_modoConsultaExpediente").val();
  	sendRequestJQ(null, url, targetWorkArea, null);
}

function generarFechaHoraTerminoAjustador(){	
	var id = jQuery("#idToReporte").val();
	if(id == null || id == ''){
		mostrarMensajeInformativo('Debe existir un reporte para poder generar la fecha y hora de terminaci\u00F3n.', '20');
		return;
	}	
	var horaContacto = jQuery('#horaContacto').val();
	if(horaContacto == null || horaContacto === ''){
		mostrarMensajeInformativo('Debe existir una fecha de contacto para poder generar la fecha y hora terminaci\u00F3n.', '20');
		return;
	}	
	if(confirm('\u00BFEst\u00E1 seguro que desea generar la fecha/hora de termino? Una vez generada no ser\u00E1 modificable.')){
		jQuery.ajax({
	          url: '/MidasWeb/siniestros/cabina/reportecabina/generarFechaHoraTermino.action?idToReporte='+id+"&isEditable="+jQuery("#isEditable").val()+"&entidad.fechaHoraTerminacion="+jQuery("#fechaTerminacion").val()+"&horaTerminacion="+jQuery("#horaTerminacion").val(),
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: null,
	          success: function(json){
	        	 unblockPage();
	          	 var fechaHora = json.fechaHoraDTO;
	          	 var descripcion = fechaHora.descripcion;
	          	 if(descripcion != null && descripcion !=''){
	          		mostrarMensajeInformativo(descripcion, '20');
	          	 }else{
	               jQuery("#fechaTerminacion").val(fechaHora.fecha);
	               jQuery("#horaTerminacion").val(fechaHora.hora);
	               validaBotonesAsignacionAjustador();
	          	 }
	          },
	          beforeSend: function(){
					blockPage();
				}	
	    });			    
	}
}

function validaBotonesAsignacionAjustador(){
	var fechaTerminacion = jQuery("#fechaTerminacion").val();
	if(fechaTerminacion != ''){
		jQuery("#div_asignacionAjustador").hide();
	}
}

function reaperturarReporte() {
	if(confirm('\u00BFDesea reaperturar el Reporte? ')){
		var url = "/MidasWeb/siniestros/cabina/reportecabina/reaperturar.action?" + jQuery("#reporteCabinaForm").serialize();
	  	sendRequestJQ(null, url, targetWorkArea, null);
	}
}


function mostrarMensajeAsignarAjustador(mensaje, id, method,methodClose,executeAfterCloseWithError){	
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;	
	var parent = null;
	var arregloVentanas = null;	
	var html = "<div class=\"mensaje_encabezado\" style=\"width: 100%;   background-size:  100%;\"></div>"
		html = html  + "<div class=\"mensaje_contenido\" style=\" height: 27%;\">"
		html = html  + "<div id=\"mensajeImg\">"
		if (id === "30"){
			html = html  + "<img src='/MidasWeb/img/b_ok.png'>";
		}else if (id === "20"){
			html = html  + "<img src='/MidasWeb/img/b_info.jpg'>";
		}else if (id === "10"){
			html = html  + "<img src='/MidasWeb/img/b_no.jpg'>";
		}
		html = html  + "</div>"	;
		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\" style=\"height: 180px;width:  88%;font-size:  15px\">";		
		html = html  + remplazarCaracteresFix(mensaje);
		html = html  + "</div>";
		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">";
		html = html  + "<div class=\"b_cancelar\"> <a href=\"javascript:void(0);\"onclick=";
		if(methodClose != null && methodClose != 'undefined'){
			html = html  +  methodClose + ";cerrarMensajeInformativo();>Cancelar</a></div>";
		}else{
			html = html  + "cerrarMensajeInformativo();>Cancelar</a></div>";
		}
	
		html = html  + "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=";
		html = html  + method +";cerrarMensajeInformativo();>Aceptar</a></div>";
		html = html  + "</div>"
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_pies\"></div>"				
		
		if (dhxWinsMessage==null){
			dhxWinsMessage = new dhtmlXWindows();
			dhxWinsMessage.setSkin("midas_message");
		}		
		mensajeAUsuario = dhxWinsMessage.window("mensajeAUsuario");
		if (mensajeAUsuario!=null && dhxWinsMessage.window("mensajeAUsuario").isHidden()) {
			mensajeAUsuario.close();
			mensajeAUsuario = null;
		}		
		if (mensajeAUsuario==null){
			var browserName=navigator.appName; 			
			if (browserName=="Microsoft Internet Explorer"){
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 433, 190);
			}else{
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 500, 757);
			}
			mensajeAUsuario.center();			
			if(mainDhxWindow != null){	
				var topWindow = mainDhxWindow.getTopmostWindow();
				if(topWindow != null){
					var ventanaPadreId = topWindow.getId();
					if(ventanaPadreId != null){
						ventanaInvocadora = mainDhxWindow.window(ventanaPadreId);			
						ventanaInvocadora.setModal(false);				
						//ventanaInvocadora.style.zIndex = 40;
					}else{
						ventanaInvocadora = null;
					}
				}
				var i = 0;
				for (var a in mainDhxWindow.wins) {	
					if(!mainDhxWindow.wins[a].isHidden()){
						arregloVentanas = new Array(++i);
						arregloVentanas[i - 1] = mainDhxWindow.wins[a].style.zIndex;
						mainDhxWindow.wins[a].style.zIndex = 40;
					}
				}
			}
			mensajeAUsuario.setModal(true);
			mensajeAUsuario.bringToTop();
			mensajeAUsuario.attachHTMLString(html);
			//mensajeAUsuario.style.zIndex = 100; //top of everything
			mensajeAUsuario.attachEvent("onClose", function(win){
				mensajeAUsuario =null;				
				if(ventanaInvocadora != null){					
					ventanaInvocadora.setModal(true);						
					if(arregloVentanas != null){
						i = 0;
						for (var a in mainDhxWindow.wins) {
							mainDhxWindow.wins[a].style.zIndex = arregloVentanas[i++];						
						}
					}
				}	
//				if(id == null || id != '10' || (id == '10' && executeAfterCloseWithError)){ //que no sea error
//					if (afterClose) {
//						try{
//							eval(afterClose);
//						}catch(e){}
//					}
//				}				
			});			
		}
}
