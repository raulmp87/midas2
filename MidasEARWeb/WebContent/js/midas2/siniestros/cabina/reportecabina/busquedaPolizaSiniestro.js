var listadoGrid;
var listadoCoberturasGrid;
var listadoCondicionesGrid;
var resultadoPolizaIncisoGrid;
var listadoDireccionesGrid;


/**
 * Al iniciar la pantalla consulta de poliza, ejecuta lo que se debe
 * mostrar desde el inicio.
 */
function inicializarPoliza(){
	
/*	if( jQuery("#h_incisoVigenteAutorizado").val() == 0 ){
		jQuery("#d_asignarInciso").hide();
	}*/
	mostrarCoberturasInciso();
}

/**
 * Muestra el listado de las Coberturas asociadas a un Inciso de una Poliza
 */
	function mostrarCoberturasInciso(){
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoCoberturasGrid = new dhtmlXGridObject('coberturasIncisoGrid');
	
		listadoCoberturasGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		listadoCoberturasGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
			listadoCoberturasGrid.forEachRow(function(id){				
				var cveCalculo = listadoCoberturasGrid.cells(id,5).getValue();
				if(cveCalculo == 'SIGA'){ //se desea identificar en rojo la cobertura de tipo SIGA							
					listadoCoberturasGrid.setRowTextStyle(id,"background-color:#FFA8A8");
				}				
			})
	    });	
		var formParams = null;
	
		listadoCoberturasGrid.attachEvent("onRowSelect", 
				function(id,ind){
					limpiarDivDatosRiesgo();
					sendRequestJQ(null, datosRiesgoCoberturaPath + "?"  + "idCobertura=" + id 
							+ "&" + jQuery(document.polizaSiniestroForm).serialize(), 'datosRiesgoCobertura', null);	
			 		return true;
		});	
	
		formParams = jQuery(document.polizaSiniestroForm).serialize();
		listadoCoberturasGrid.load( buscarCoberturasIncisoPath + '?' + formParams, function(){
//			listadoCoberturasGrid.forEachRow(function(id){
//				listadoCoberturasGrid.cells(id,2).cell.className='heightCell';
//			})
		});
	}
	
	function limpiarDivDatosRiesgo() {
		document.getElementById("datosRiesgoCobertura").innerHTML = '';
		jQuery("#divDatosRiesgo").show();
	}



function solicitarAutorizacion( ){
	
	var idReporte 		= jQuery('#h_idReporte').val();
	var idPoliza 		= jQuery('#h_idPoliza').val();
	var numeroInciso 	= jQuery('#h_numeroInciso').val();
	var estatusPoliza 	= jQuery('#h_descEstatus').val();
	var estatus			= jQuery('#h_incisoEstatus').val();
	
	var url = mostrarSolicitarAutorizacionPath+ '?idToPoliza=' + idPoliza+'&numeroInciso='+numeroInciso+
		"&idReporte="+idReporte+"&estatusPoliza="+estatusPoliza + "&codigoEstatusPoliza=" + estatus; 
	
	mostrarVentanaModal("vm_solicitarAutorizacion", "Solicitar Autorizacion", null, null, 690, 180, url , "");
}

function enviarSolicitudAutorizacion(){
	var url = solicitarAutorizacionPath; 
	redirectVentanaModal('vm_solicitarAutorizacion', url, document.formSolicitarAutorizacion);

}

	function probarEnviar(){
		
		var url = "/MidasWeb/siniestros/cabina/reporteCabina/poliza/pruebaSolicitudAutorizacion.action?numeroInciso=85"
			//+"?"+jQuery(document.formSolicitarAutorizacion).serialize(); 
		
		sendRequestJQ(null, url, 'botonAutorizar', null);	
		parent.limpiarDivBotonAutorizar();
		salirSolicitarAutorizacion();
	}


function salirSolicitarAutorizacion(){
	parent.cerrarVentanaModal("vm_solicitarAutorizacion",true);
}

function asignarPrestador(){
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true"; 
	mostrarVentanaModal("vm_asignarPrestador", "Asignar Prestador de Servicio", null, null, 690, 650, url , "");
}

function setPrestadorId(prestadorId, nombrePrestador){
	 
	jQuery("#txt_asigna_prestador").val(prestadorId);
	parent.cerrarVentanaModal("vm_asignarPrestador",true);
}


/**
 * Carga la pantalla principal de Busqueda de Poliza Inciso
 */
	function mostrarBusquedaIncisoPoliza(){
		var idToReporte 			= jQuery('#idToReporte').val();
		var fechaReporteSiniestro 	= jQuery('#h_fechaReporteSiniestro').val();
		var fechaOcurrido 			= jQuery('#fechaOcurrido').val();
		var horaOcurrido 			= jQuery('#horaOcurrido').val();
	
		
		if( idToReporte != ""){
			if( fechaOcurrido !="" && horaOcurrido!=""){
				jQuery.ajax({
			          url: '/MidasWeb/siniestros/cabina/reportecabina/validarExistenciaFechaHora.action',
			          dataType: 'json',
			          async:false,
			          type:"POST",
			          data: {idToReporte:idToReporte, tipoFechaHora:'OCURRIDO'},
			          success: function(json){
			          	 var fechaHora = json.fechaHoraDTO;			          	 
			          	 if(fechaHora.fecha != null && fechaHora.fecha !='' && fechaHora.hora != null && fechaHora.hora != ''){
			          		var url = "/MidasWeb/siniestros/cabina/reporteCabina/poliza/mostrarBusqueda.action?"
								+ "idReporte=" +idToReporte+"&fechaReporteSiniestro="+fechaOcurrido+"&horaOcurrido="+horaOcurrido;							
							sendRequestJQ(null, url,targetWorkArea,null);
			          	 }else{
			          		mostrarMensajeInformativo('Debe guardar el reporte para registrar la fecha y hora de ocurrido', '20');
			          	 }
			          }
			    });			 			
			}else{
				mostrarMensajeInformativo('Debe Proporcionar la Fecha y Hora de Ocurrido', '20');
			}
		}else{
			mostrarMensajeInformativo('Debe Guardar el Reporte, antes de buscar un Inciso/Poliza', '20');
		}
	}
	

	
	/**
	 * Carga la pantalla principal de Consulta de Poliza Inciso
	 */
		function mostrarConsultaIncisoPoliza(){			
			var incisoContinuityId 		= jQuery("#h_incisoContinuityId").val();
			var recordFromMillis 		= jQuery("#h_recordFromMillis").val();
			var validOnMillis			= jQuery("#h_validOnMillis").val();
			var fechaOcurrido 			= jQuery('#fechaOcurrido').val();
			var horaOcurrido 			= jQuery('#horaOcurrido').val();
			var idReporte 				= jQuery('#idToReporte').val();
			var soloConsulta 			= jQuery('#h_soloConsulta').val();
			 
			var url = mostrarDetalleInciso + '?' +
							'incisoContinuityId='+ incisoContinuityId +
							'&recordFromMillis=' + recordFromMillis +
							'&validOnMillis=' + validOnMillis +
							'&fechaReporteSiniestro='+fechaOcurrido +
							'&horaOcurrido='+horaOcurrido+
							'&idReporte='+idReporte+
							'&soloConsulta='+soloConsulta;
			
			if(incisoContinuityId != ""){
				sendRequestJQ(null, url, targetWorkArea, "setConsulta();");
			}
			
										
			//incisoContinuityId="+incisoContinuityId + "&fechaReporteSiniestro=" + fechaReporteSiniestro + "&idReporte="+idReporte;
			
			
//			if(solicitudPoliza == 'true'){
//				if(jQuery("#idToSolicitud").val() != ""){
//					var url = mostrarSolicitudPath + '?idToSolicitud=' + jQuery("#idToSolicitud").val()+ 
//				     								'&idReporteCabina=' + jQuery("#idReporte").val() +
//				     								'&serie=' + jQuery("#serie").val() 	
//				     mostrarVentanaModal( "vm_solicitud", "Consulta de Soicitud", null, null, 950, 250, url , null);
//				}else{
//					 mostrarMensajeInformativo('Seleccione primero una Carta Cobertura', '20')
//				}
//				
//			}else{
//				if(incisoContinuityId != ""){
//					var url = mostrarDetalleInciso + "?" + jQuery(document.busquedaIncisoPolizaForm).serialize()
//					//incisoContinuityId="+incisoContinuityId + "&fechaReporteSiniestro=" + fechaReporteSiniestro + "&idReporte="+idReporte;
//					sendRequestJQ(null, url, targetWorkArea, null);
//				}else{
//					 mostrarMensajeInformativo('Seleccione primero un Inciso a Consultar', '20')
//				}
//			}
		
		}
	
	
	/**
	 * Carga el grid de la Busqueda  de Poliza Inciso
	 */
	function getIncisosPoliza(){
		
		var incisoContinuityId 	= jQuery('#incisoContinuityId').val();
		
		jQuery("#resultadoPolizaIncisoGrid").empty();
		
		resultadoPolizaIncisoGrid = new dhtmlXGridObject('resultadoPolizaIncisoGrid');
		
		resultadoPolizaIncisoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		resultadoPolizaIncisoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});

		resultadoPolizaIncisoGrid.load( buscarPoliza  );
		
		
		resultadoPolizaIncisoGrid.attachEvent("onRowSelect", 
				function(id){
			
			if( validateIdError(id) ){
				var fechaReporteSiniestro 	    = jQuery('#fechaReporteSiniestro').val();
				var fechaReporteSiniestroMillis = jQuery('#fechaReporteSiniestroMillis').val();
				var incisoContinuityId		    = resultadoPolizaIncisoGrid.getUserData(id,"valorIncisoContinuityId") ;
				var idToSolicitud	 		    = resultadoPolizaIncisoGrid.getUserData(id,"valoridToSolicitudDataEnTramite");
				var validOn	 				    = resultadoPolizaIncisoGrid.getUserData(id,"valorValidOn");
				var recordFrom	 			    = resultadoPolizaIncisoGrid.getUserData(id,"valorRecordFrom");
				var validOnMillis	 		    = resultadoPolizaIncisoGrid.getUserData(id,"valorValidOnMillis");
				var recordFromMillis		    = resultadoPolizaIncisoGrid.getUserData(id,"valorRecordFromMillis") ;
				
				jQuery('#solicitudPoliza').val( solicitudPoliza ) ;
				jQuery('#incisoContinuityId').val( incisoContinuityId ) ;
				jQuery('#validOn').val( validOn ) ;
				jQuery('#recordFrom').val( recordFrom ) ;
				jQuery('#validOnMillis').val( validOnMillis ) ;
				jQuery('#recordFromMillis').val( recordFromMillis ) ;


				if(idToSolicitud != null && idToSolicitud != ""){					
					idToSolicitud = resultadoPolizaIncisoGrid.getUserData(id,"valoridToSolicitudDataEnTramite") ;	
				}
				
				var url = mostrarDatosVehiculo 
							+"?incisoContinuityId="+incisoContinuityId
							+"&fechaReporteSiniestro="+fechaReporteSiniestro
							+"&fechaReporteSiniestroMillis="+fechaReporteSiniestroMillis
							+"&validOnMillis="+validOnMillis
							+"&recordFromMillis="+recordFromMillis
							+"&idToSolicitud="+idToSolicitud;
				
				limpiarDivDatosVehiculo();
				var func=null;
				if (jQuery("#busquedaPolizas").val() != "true" ) {
					func = "ocultarBotones()"
				}
				sendRequestJQ(null, url, 'datosVehiculoSeleccionado', func);	
		 		return true;
			}
		});	
		
		
		if(incisoContinuityId !=""){
			realizarBusquedaIncisoPoliza();
			//cargarDatosVehiculo(incisoContinuityId);
		}
	}
	
	
	
	
	function cargarDatosVehiculo(id){
		
		if( validateIdError(id) ){
			
			var fechaReporteSiniestro 	= jQuery('#fechaReporteSiniestro').val();
			var solicitudPoliza	 		= jQuery('#solicitudPoliza').val();
			var validOnMillis	 		= jQuery('#validOnMillis').val(); 
			var recordFromMillis		= jQuery('#recordFromMillis').val();  
			
			if(solicitudPoliza == 'true'){
				jQuery('#idToSolicitud').val( resultadoPolizaIncisoGrid.getUserData(id,"valorIdToSolicitud") ) ;
				jQuery('#serie').val( resultadoPolizaIncisoGrid.getUserData(id,"valorNumeroSerie") ) ;
				id = resultadoPolizaIncisoGrid.getUserData(id,"valoridToSolicitudDataEnTramite") ;	
			}
			
			var url = mostrarDatosVehiculo 
						+"?incisoContinuityId="+id
						+"&fechaReporteSiniestro="+fechaReporteSiniestro
						+"&solicitudPoliza="+solicitudPoliza
						+"&validOnMillis="+validOnMillis
						+"&recordFromMillis="+recordFromMillis;
			
			limpiarDivDatosVehiculo();
			
			sendRequestJQ(null, url, 'datosVehiculoSeleccionado', null);	
	 		
		}
}
	
	/**
	 * Funcion que valida que el renglon seleccionado no sea un mensaje de error.
	 * @param id
	 * @returns {Boolean}
	 */
	function validateIdError( id ){
		
		if( id =="error1"){
			return false;
		}
		
		else if( id =="error2"){
			return false;	
		}
		
		else if( id=="error3"){
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * Realiza la busqueda de los Incisos Poliza
	 */
	function realizarBusquedaIncisoPoliza(){
		if(validateFormulario()){
			var formParams = encodeForm(jQuery(document.busquedaIncisoPolizaForm));
			
			 var url = buscarPoliza + "?" + formParams;
			 console.log(url);
			 resultadoPolizaIncisoGrid.load( url) ;
			 
		}
	}
	
	/**
	 * Limpia el formulario de Busqueda inciso Poliza
	 */
	function limpiarFormulario(){
		jQuery('#busquedaIncisoPolizaForm').each (function(){
			  this.reset();
		});
	}
	
	/**
	 * Limpia el DIV de la informacion del vehiculo en la pantalal de Busqueda Inciso Poliza
	 */
	function limpiarDivDatosVehiculo() {
		document.getElementById("datosVehiculoSeleccionado").innerHTML = '';
	}
	
	
	/**
	 * Limpia el DIV del boton Autorizar
	 */
	function limpiarDivBotonAutorizar() {
		document.getElementById("botonAutorizar").innerHTML = '';
	}
	
	/**
	 * Realiza las validaciones del formulario de Busqueda inciso Poliza
	 * @returns {Boolean}
	 */
	function validateFormulario(){
		var element =  document.getElementById('polizaMidas');
		var element2 =  document.getElementById('polizaSeycos');
		var continuar=false;
		if ((typeof(element) != 'undefined' && element != null) && (typeof(element2) != 'undefined' && element2 != null))
		{
			continuar=true;
		}

		 var numeroSerie 			= jQuery("#numeroSerie").val();
		 var servicioPublico 		= jQuery("#servicioPublico").attr("checked");
		 var placa 					= jQuery("#placa").val();
		 var asegurado 				= jQuery("#asegurado").val();
		 var servicioParticular 	= jQuery("#servicioParticular").attr("checked");
		 var poliza 				= jQuery("#poliza").val();
		 if(continuar)
		 {
			 var polizaMidas 			= jQuery("#polizaMidas").val();
		 var polizaSeycos 			= jQuery("#polizaSeycos").val();}
		 var motor 					= jQuery("#motor").val();
		 var txtFechaInicio			= jQuery("#txtFechaInicio").val();
		 var txtFechaFin 			= jQuery("#txtFechaFin").val();
		 var numeroInciso 			= jQuery("#numeroInciso").val();
		 var folioCarta				= jQuery("#idToSolicitud").val();
		 var contador 				= 0 ;
		 var validacionExtraMotor 	= false;
		 var validacionFechaIni		= false;
		 var validacionFechaFin		= false;
		 
		 var folioReexpedible 		= jQuery('#numeroFolio').val();
		 
		 if(folioCarta.length != 0){
			 var msjError = 'El folio de la carta debe tener formato SOL-234556 o ser completamente numérico (234556)';		
			 if(isNaN(folioCarta)){
				 if(folioCarta.indexOf("SOL-") ==  0){//formato SOL-234234
					 try{
					 	var folioCartaNumerico = folioCarta.split("-")[1];	
					 	if(isNaN(folioCartaNumerico)){					 		
					 		throw msjError;					 		
					 	}
					 	jQuery("#idToSolicitud").val(folioCartaNumerico);
					 }catch(err){						
						 mostrarMensajeInformativo(err,'20');
						 return false;
					 }			
				 }else{
					mostrarMensajeInformativo(msjError, '20');
					return false;
				 }
			 }		 
			 contador++;
		 }
		 
		 if(numeroSerie.length != 0){
			 contador ++;
			 if(numeroSerie.length <3 || numeroSerie.length > 17  ){
				 mostrarMensajeInformativo('El numero de Serie debe contener entre 3 y 17 caracteres', '20');
				 return false;
			 }
		 }
		 
		 /*if(servicioPublico == true ){
			 contador ++;
		 }*/
			 
		 if(placa.length != 0){
			 contador ++;
		 }
		 
		 if(asegurado.length != 0){
			 contador ++;
		 }
		 
		 /*if(servicioParticular == true){
			 contador ++;
		 }*/
		 
		 if(poliza.length != 0){
			 contador ++;
		 }
		 if (continuar){
		 if(polizaMidas.length != 0){
			 contador ++;
		 }
		 if(polizaSeycos.length != 0){
			 contador ++;
		 }
		 }
		 
		 if(numeroInciso.length != 0){
			 contador ++;
		 }
		 
		 if(txtFechaInicio.length != 0){
			 contador ++;
			 validacionFechaIni = true;
		 }
		 
		 if(txtFechaFin.length != 0){
			 contador ++;
			 validacionFechaFin = true;
		 }
		 
		 if(motor.length != 0){
			 contador ++;
			 if(contador == 1){
				 validacionExtraMotor = true;
			 }
		 }
		 
		 if(typeof(folioReexpedible)  != 'undefined' && folioReexpedible.trim().length > 0){
			 contador++;
		 } 
		 
		 if(validacionExtraMotor){
			 mostrarMensajeInformativo('Debe Proporcionar un campo de búsqueda adicional cuando se realiza la búsqueda por No de Motor', '20');
			 return false;
		 }
		 
		 if (validacionFechaIni || validacionFechaFin){
			 if(contador <3){
				 mostrarMensajeInformativo('Debe Proporcionar un campo de búsqueda adicional, cuando se realiza la búsqueda por Vigencia', '20');
				 return false;
			 }
		 }
		 
		 if(contador >0){
			 return true;
		 }else{
			 mostrarMensajeInformativo('Debe Proporcionar filtros de búsqueda adicionales', '20');
			 return false;
		 }
	 }

	
	/**
	 * Inicializa el listado de las Condiciones Especiales asociadas a un Inciso de una Poliza
	 */
	function inicializarCondicionesEspeciales(tipoValidacion){
		var url = buscarCondicionesEspecialesIncisoPath + '?incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
													      "&idReporte=" + jQuery("#h_idReporte").val() + 
														  "&fechaReporteSiniestroMillis=" + jQuery("#h_fechaReporteSiniestroMillis").val() + 
														  "&validOnMillis=" + jQuery("#h_validOnMillis").val() +
														  "&recordFromMillis=" + jQuery("#h_recordFromMillis").val() +
														  "&idToPoliza=" + jQuery("#h_idPoliza").val()+
														  "&tipoValidacion=" + tipoValidacion+
														  "&soloConsulta=" + jQuery("#h_soloConsulta").val();
	
		mostrarVentanaModal( "vm_condicionesEspecialesInciso", "Consulta de Condiciones Especiales de un Inciso", null, null, 800, 490, url , null);
	}

	/**
	 * Muestra el listado de las Condiciones Especiales asociadas a un Inciso de una Poliza 
	 */
	function mostrarCondicionesEspecialesInciso(){
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		var soloConsulta = jQuery("#h_soloConsulta").val();
		var incisosAsociadosReporte = jQuery("#h_incisosAsociadosReporte").val();	
		
		listadoCondicionesGrid = new dhtmlXGridObject('condicionesEspecialesIncisoGrid');
	
		listadoCondicionesGrid.attachEvent("onXLS", function(grid){	
			parent.blockPage();
	    });
		listadoCondicionesGrid.attachEvent("onXLE", function(grid){		
			parent.unblockPage();
	    });			
		
		var formParams = null;
		var url = buscarCondicionesEspecialesIncisoGridPath + '?incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
													      "&idReporte=" + jQuery("#h_idReporte").val() + 
														  "&fechaReporteSiniestroMillis=" + jQuery("#h_fechaReporteSiniestroMillis").val() + 
														  "&validOnMillis=" + jQuery("#h_validOnMillis").val() +
														  "&recordFromMillis=" + jQuery("#h_recordFromMillis").val() +
														  "&incisosAsociadosReporte=" + jQuery("#h_incisosAsociadosReporte").val();
		
		listadoCondicionesGrid.load( url );
		listadoCondicionesGrid.load(url, function() {
			if (incisosAsociadosReporte == 1 && soloConsulta == 1) {
				listadoCondicionesGrid.forEachRow(function(rId){
					listadoCondicionesGrid.cells(rId,1).setDisabled(true);
				   });
			}			
		});		
	}

/**
 * Asocia las Condiciones Especiales a un Inciso de una Poliza
 */
function guardarCondicionesEspecialesInciso(){
	var condicionesEspeciales = sumCondicionesEspeciales(1); 
	var url = guardarCondicionesEspecialesIncisoPath + '?idReporte=' + jQuery("#h_idReporte").val() + 
														   '&condicionesEspeciales=' + condicionesEspeciales +
														   '&tipoValidacion='+ jQuery("#h_tipoValidacion").val() + 
														   '&incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() + 
														   '&validOn=' + jQuery("#h_validOn").val() + 
														   '&incisosAsociadosReporte=' + jQuery("#h_incisosAsociadosReporte").val() +
		  												   '&fechaReporteSiniestroMillis=' + jQuery("#h_fechaReporteSiniestroMillis").val();
		parent.redirectVentanaModal('vm_condicionesEspecialesInciso', url, null);
		//sendRequestJQ(null, url, 'vm_condicionesEspecialesInciso', "cerrar();");
	
}

/**
 * Cierra el Pop-up, mostrando primero un mensaje de confirmacion
 */
function cerrarConfirmar(){
	var incisosAsociadosReporte = jQuery("#h_incisosAsociadosReporte").val();
	var soloConsulta = jQuery("#h_soloConsulta").val();	
	if (incisosAsociadosReporte == 1 && listadoCondicionesGrid.getRowsNum() > 0 && soloConsulta != 1) {
		if(confirm("Se perderán los cambios no guardados ¿Desea Continuar?")){
			cerrar();
		}
	} else {
		cerrar();
	}
}

/**
 * Despues de guardar,
 * cierra el Pop-up, mostrando primero un mensaje de confirmacion
 */
function cerrarGuardar(){
    parent.mostrarMensajeConfirm("Al guardar los datos se cerrará la pantalla ¿Desea Continuar?", "20", "cerrar();", null, null);

}

/**
 * Cierra el Pop-up, regresando a la pantalla anterior
 */
function cerrar(){
	parent.cerrarVentanaModal("vm_condicionesEspecialesInciso", "inicializarPoliza();");
}


/**
 * Valida las condiciones especiales que se encuentran seleccionadas y 
 * crea una cadena con todas las idCondicionEspecial que se quieren 
 * asociar al Inciso
 */
function sumCondicionesEspeciales( ind ) {
    var condicionesEspecialesChecked = "";
    var condicionesEspeciales = "";
    
    for (var i = 0; i < listadoCondicionesGrid.getRowsNum(); i++) {
    	condicionesEspecialesChecked = listadoCondicionesGrid.cells2(i, ind).getValue();
    	if( condicionesEspecialesChecked == 1 ){
    		condicionesEspeciales += listadoCondicionesGrid.cells2(i, 0).getValue()+",";
    	}
    }
    return condicionesEspeciales;
}

/**
 * Valida las condiciones especiales que se encuentran seleccionadas y 
 * crea una cadena con todas las idCondicionEspecial que se quieren 
 * asociar al Inciso
 */
function asignarInciso() {
	var asignar = true;
	var url = asignarIncisoPath + '?' + "idToPoliza=" + jQuery("#h_idPoliza").val() +
			  "&incisoVigenteAutorizado=" + jQuery("#h_incisoVigenteAutorizado").val() +		
			  "&"+ jQuery(document.historialBusquedaInciso).serialize();

	if(!confirm('El inciso seleccionado se asignar\u00E1 al reporte. \u00BFDesea continuar?')){
		asignar = false;
	}

	if(asignar){	
		sendRequestJQ(null, url,targetWorkArea,"regresarReporte();");
	}
}

/**
 * Muestra los datos del Cliente
 * @param idCliente
 */
function consultarAsegurado(  ){
	var idCliente  = jQuery("#h_idPersonaContratante").val();
	
	var url = mostrarDatosClientePath + '?idCliente=' + idCliente;
	mostrarVentanaModal( "vm_datosAsegurado", "Consulta de Asegurado", null, null, 1300, 500, url , null);
}

/**
 * Muestra listado con los datos de direcciones del cliente
 */
function inicializarListadoDatosCliente(){
	
	listadoDireccionesGrid = new dhtmlXGridObject('listadoDireccionesClienteGrid');

	var url = buscarDireccionesClientePath + '?idCliente=' + jQuery("#h_idCliente").val();
	listadoDireccionesGrid.clearAll();
	listadoDireccionesGrid.load( url );
}

/**
 * Regresa a la pantalla detalle Inciso
 */
function cerrarDatosAsegurado(){
	parent.cerrarVentanaModal("vm_datosAsegurado", null);
}

function mostrarProgramasPago(){
	
	var funcionScript 				= "";
	
	 var url = "/MidasWeb/siniestros/cabina/reportecabina/poliza/programapago/mostrar.action?"
		 			+ jQuery(document.historialBusquedaInciso).serialize();
	 
	 sendRequestJQ(null, url, targetWorkArea, funcionScript);

}

function validateInfo(incisoContinuityId, fechaReporteSiniestro){
	 if(incisoContinuityId != null &&  fechaReporteSiniestro != null){
			 if(!isEmpty(incisoContinuityId.toString()) && !isEmpty(fechaReporteSiniestro.toString())){
				 return true;
			 }else{
				 return false;
			 }
	 } else{
		 return false;
	 }
 }
	
	
	 /**
	  * Carga la pagina de Consulta DEtalle Inciso
	  */
	function consultaInciso(){
		var solicitudPoliza	 		    = jQuery("#idToSolicitud").val();		
		var incisoContinuityId 			= jQuery("#incisoContinuityId").val();
		var fechaReporteSiniestro 		= jQuery("#fechaReporteSiniestro").val();
		var fechaReporteSiniestroMillis = jQuery("#fechaReporteSiniestroMillis").val();
		var idReporte					= jQuery("#idReporte").val();

		if (typeof solicitudPoliza !== 'undefined' && solicitudPoliza != ""){
			var url = mostrarSolicitudPath + '?idToSolicitud=' + jQuery("#idToSolicitud").val()+ 
		     								'&idReporteCabina=' + jQuery("#idReporte").val() +
		     								'&serie=' + jQuery("#serieCartaCobertura").val() 	
		     mostrarVentanaModal( "vm_solicitud", "Consulta de Solicitud", null, null, 950, 250, url , null);
		} else if(incisoContinuityId != ""){
			var url = mostrarDetalleInciso + "?" + jQuery(document.busquedaIncisoPolizaForm).serialize()
			//incisoContinuityId="+incisoContinuityId + "&fechaReporteSiniestro=" + fechaReporteSiniestro + "&idReporte="+idReporte;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			 mostrarMensajeInformativo('Seleccione primero un Inciso a Consultar', '20')
		}
			
		
	}
	



	
	
	 /**
	  * Carga la pagina de Consulta DEtalle Inciso
	  */
	function cerrarProgramasPago(){
		var soloConsulta = jQuery("#h_soloConsulta").val();
		
		var url = mostrarDetalleInciso + "?" + jQuery(document.programasPagoForm).serialize();
		sendRequestJQ(null, url, targetWorkArea, soloConsulta==1?"setConsulta();":null);
	}
	
	
	
	/**
	  * Carga PopUp de Riesgos
	  */
	function viewDatosRiesgo(){
		 var url = mostrarDatosRiesgo  + "?" + jQuery(document.polizaSiniestroForm).serialize();
	     mostrarVentanaModal("vm_datosRiesgo", 'Datos Riesgo',null, null, 650, 250, url , null);    
	}
	
	
	 /**
	  * Carga la pagina de Consulta DEtalle Inciso
	  */
	function mostrarEndosos(){
		var incisoContinuityId 			= jQuery("#h_incisoContinuityId").val();
		var funcionScript 				= "";

		var url = mostrarEndoso + "?incisoContinuity="+incisoContinuityId 
					+"&"+ jQuery(document.historialBusquedaInciso).serialize();
		sendRequestJQ(null, url, targetWorkArea, null);
	}
	
	 /**
	  * Carga la pagina de Consulta DEtalle Inciso
	  */
	function cerrarEndosos(){
		var incisoContinuityId 			= jQuery("#incisoContinuity").val();
		var soloConsulta = jQuery("#h_soloConsulta").val();

		var url = mostrarDetalleInciso + "?incisoContinuityId="+incisoContinuityId
					+"&"+ jQuery(document.historialBusquedaInciso).serialize();
		
		sendRequestJQ(null, url, targetWorkArea, soloConsulta==1?"setConsulta();":null);
	}
	
	
	
	function listadoSiniestros(){
		var numeroSerie 			= jQuery("#h_numeroSerie").val();
		var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarListado.action?numeroSerie=" + numeroSerie;
		mostrarVentanaModal("listadoSiniestros", "Listado de Siniestros", 100, 200, 1200, 400, url, '');
	}

	function obtenerProrroga(){
		var url = mostrarDatosProrrogaPath + '?idToPoliza=' + jQuery("#h_idPoliza").val() + 
											 '&incisoContinuityId=' + jQuery("#h_incisoContinuityId").val() +
											 '&validOn=' + jQuery('#h_validOn').val();
		mostrarVentanaModal( "vm_datosProrroga", "Consulta de Prórroga", null, null, 700, 200, url , null);
	}

	function cerrarProrroga(){
		parent.cerrarVentanaModal("vm_datosProrroga", null);
	}
	
	
	function regresarReporte(){
		var idToReporte 		= jQuery("#h_idReporte").val();
		var soloConsulta = jQuery("#h_soloConsulta").val();
		if (idToReporte != ""){
		var url = regresarReporteCabina + "?idToReporte="+idToReporte;
		
		sendRequestJQ(null, url,targetWorkArea, soloConsulta==1?"setConsultaReporte();":null);
		} 
		else {
			var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarContenedor.action?ventanaOrigen=MN"
				sendRequestJQ(null, url,targetWorkArea, null);
		}
	}
	
	function regresarReporteBusqueda(){
		var idToReporte 		= jQuery("#idReporte").val();
		var url = regresarReporteCabina + "?idToReporte="+idToReporte;
		
		var r = confirm("¿Desea regresar a la pantalla del reporte? se perderan los datos encontrados");
		if (r == true) {
			sendRequestJQ(null, url,targetWorkArea, null);
		}
		
		
	}
	
	function regresarBusqueda(){
		var fechaReporteSiniestro 		= jQuery("#fechaReporteSiniestro").val();
		var idReporte					= jQuery("#idReporte").val();
		if (idReporte != "" ){
		var url = "/MidasWeb/siniestros/cabina/reporteCabina/poliza/mostrarBusqueda.action?"
			+ jQuery(document.historialBusquedaInciso).serialize();
		} else {
			var url = "/MidasWeb/siniestros/cabina/reporteCabina/poliza/mostrarBusquedaPolizas.action?"
				+ jQuery(document.historialBusquedaInciso).serialize();
		}
		sendRequestJQ(null, url,targetWorkArea, null);
	}
	
	
	function setConsulta(){
		jQuery(".setNew").attr("disabled","disabled");
		jQuery(".setNew").addClass("consulta");
		jQuery("#btn_regresarBusqueda").remove();
		jQuery("#d_asignarInciso").remove();
//		jQuery("#guardar").remove();
//		jQuery("#btn_BusquedaPoliza").remove();
//		jQuery("#btn_asignarAjustador").remove();
//		jQuery("#btn_declaracionSiniestro").remove();
		jQuery("#contenido").append("<div id='consulta' style='display:none;' >1</div>");
	}

	function abrirVentanaDetalleCondicion(id){		
		var url = "/MidasWeb/siniestros/cabina/reporteCabina/poliza/mostrarDetalleCondicion.action?condicionEspecialId="+id;				
		parent.mostrarVentanaModalMode("winCondicionDetalle", "Detalle de la condición", null, null, 700, 400, 'URL', url, null);		
	}	