var asignacionGrid;
var arriboGrid;
var terminoGrid;

// Seteados en JSP
var listarReportesAsignacion;
var listarReportesArribo;
var listarReportesTermino;
var exportarExcelUrl;
var cabinaId;
var paginationEnabled;
var alertasActivas;
// Seteados en JSP

// atributos para reloj
var attrTiempoTrans = "tiempoTrans";
var attrTiempoAlarma = "tiempoAlarma";
var attrTipoAlarma = "tipoAlarma";

var colAlign = "left, left, left, left, left, left, left, left, left, left, left, left, left, left";


var reporteCreadoChannel = "/reporte-cabina/reporte-creado/*";
var reporteAsignadoChannel = "/reporte-cabina/ajustador-asignado/*";
var reporteContactoChannel = "/reporte-cabina/ajustador-contacto/*";
var reporteTerminoChannel = "/reporte-cabina/ajustador-termino/*";
var reporteCitaChannel = "/reporte-cabina/cita-creada/*";
var reporteCanceladoChannel = "/reporte-cabina/reporte-cancelado/*";

var gridsLoading = 3;

function init() {
	inicializarGrids();
	
}

function inicializarGrids() {
	cargarAsignacionGrid();
	cargarArriboGrid();
	cargarTerminoGrid();
	subscribeCometd();
}

function habilitarSelect() {
	//console.log("load");
	gridsLoading--;
	if (gridsLoading == 0){
		jQuery("#lstOficinaId").removeAttr('disabled');
		unblockPage();
	}
}

function cargarAsignacionGrid() {
	
	asignacionGrid = cargarGrid(listarReportesAsignacion, "asignacionGrid", null, "indicadorAsignacion");

}

function cargarArriboGrid() {
	arriboGrid = cargarGrid(listarReportesArribo, "arriboGrid", null, "indicadorArribo");

}

function cargarTerminoGrid() {
	terminoGrid = cargarGrid(listarReportesTermino, "terminoGrid", null, "indicadorTermino");

}

function cargarGrid(urlListado, gridDivId,call, indicadorDiv) {
	// blockPage();

	document.getElementById(gridDivId).innerHTML = '';
	var gridVar = new dhtmlXGridObject(gridDivId);
	if (paginationEnabled == true) {
		gridVar.enablePaging(true, 10, 10, "pagingArea" + gridDivId, true,
				"infoArea" + gridDivId);
		gridVar.setPagingSkin("bricks");
	}
	// crearGrid(gridDivId, gridVar);
	gridVar.attachEvent("onXLS", function(grid_obj){blockPage()});
	gridVar.attachEvent("onXLE", function(grid_obj){unblockPage();habilitarSelect()});
	gridVar.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga(indicadorDiv);
    });
	gridVar.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga(indicadorDiv);
    });
	gridVar.attachEvent("onRowDblClicked",function(rId,cInd){
		unsubscribeCometd();
		editar(gridVar.cells(rId, 3).getValue(), gridVar.cells(rId, 4).getValue(), gridVar.cells(rId, 5).getValue());
	});
	gridVar.setDateFormat("%d/%m/%Y %H:%i") 
	gridVar.init();
	gridVar.setSkin("light");
	gridVar.load(urlListado, call);

	return gridVar;
	// unblockPage();
}


function editar(nsOficina, nsConsecutivoR, nsAnio) {
	var params = "";
	params += "nsOficina=" + nsOficina + "&";
	params += "nsConsecutivoR=" + nsConsecutivoR + "&";
	params += "nsAnio=" + nsAnio + "&";
	params += "ventanaOrigen=MR";
	
	sendRequestJQ(null,
			"/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?"
					+ params, "contenido");

}

function onChangeOficina() {
	key = jQuery("#lstOficinaId").val();
	if (key == "") {
		asignacionGrid.resetFilters();
		arriboGrid.resetFilters();
		terminoGrid.resetFilters();
	} else {
		var val = jQuery("#lstOficinaId option:selected").html();
		asignacionGrid.filterBy(2, val);
		arriboGrid.filterBy(2, val);
		terminoGrid.filterBy(2, val);
	}
}

function permutarAlertasActivas() {
	alertasActivas = !alertasActivas;
	// HACKME: guardar en cookies
	var texto = ""
	if (alertasActivas == true) {
		texto = "Desactivar Alertas";
	} else {
		texto = "Activar Alertas";
	}
	jQuery("#btnAlertasActivas").text(texto);
}

function subscribeCometd() {
	cometd.batch(function() {
		//console.log("cometdbatch");
		cometdUtil.subscribe(reporteCreadoChannel, function(message) {
			//console.log("cometd");
			var reporteCabina = jQuery.parseJSON(message.data);
			// Actualizar AssignacionGrid
			asignacionGrid.updateFromXML(listarReportesAsignacion + "?id="
					+ reporteCabina.id, true, true);
			//asignacionGrid.refreshFilters();
		});

		cometdUtil.subscribe(reporteAsignadoChannel, function(message) {
			var reporteCabina = jQuery.parseJSON(message.data);
			// Borrar de AssignacionGrid
			asignacionGrid.deleteRow(reporteCabina.id);

			// Actualizar ArriboGrid
			arriboGrid.updateFromXML(listarReportesArribo + "?id="
					+ reporteCabina.id, true, true);
			//arriboGrid.refreshFilters();
		});

		cometdUtil.subscribe(reporteContactoChannel, function(message) {
			var reporteCabina = jQuery.parseJSON(message.data);
			// Borrar de ArriboGrid
			arriboGrid.deleteRow(reporteCabina.id);

			// Actualizar TerminoGrid
			terminoGrid.updateFromXML(listarReportesTermino + "?id="
					+ reporteCabina.id, true, true);
			//terminoGrid.refreshFilters();
		});

		cometdUtil.subscribe(reporteTerminoChannel, function(message) {
			var reporteCabina = jQuery.parseJSON(message.data);
			// Borrar de TerminoGrid
			terminoGrid.deleteRow(reporteCabina.id);
			//terminoGrid.refreshFilters();
		});

		cometdUtil.subscribe(reporteCitaChannel, function(message) {
			// Actualizar AssignacionGrid
			var reporteCabina = jQuery.parseJSON(message.data);
			//asignacionGrid.deleteRow(reporteCabina.id);
			asignacionGrid.updateFromXML(listarReportesAsignacion, true, true);
			arriboGrid.updateFromXML(listarReportesArribo, true, true);

			//asignacionGrid.refreshFilters();
			//arriboGrid.refreshFilters();
		});

		cometdUtil.subscribe(reporteCanceladoChannel, function(message) {
			var reporteCabina = jQuery.parseJSON(message.data);

			// Actualizar AssignacionGrid
			try {
				asignacionGrid.deleteRow(reporteCabina.id);
			} catch(err) {}
			//asignacionGrid.updateFromXML(listarReportesAsignacion);

			// Actualizar ArriboGrid
			try {
				arriboGrid.deleteRow(reporteCabina.id);
			} catch(err) {}
			//arriboGrid.updateFromXML(listarReportesArribo);

			// Actualizar TerminoGrid
			try {
				terminoGrid.deleteRow(reporteCabina.id);
			} catch(err) {}
			//terminoGrid.updateFromXML(listarReportesTermino);

			//asignacionGrid.refreshFilters();
			//arriboGrid.refreshFilters();
			//terminoGrid.refreshFilters();
		});

	});
}

function aviso(){
	alert("Se ha desactivado el registro automático de reportes en el monitor. Para activarlo, ingrese nuevamente al menú Monitores.");
}

function unsubscribeCometd() {
	cometdUtil.unsubscribe(reporteCreadoChannel);
	cometdUtil.unsubscribe(reporteAsignadoChannel);
	cometdUtil.unsubscribe(reporteContactoChannel);
	cometdUtil.unsubscribe(reporteTerminoChannel);
	cometdUtil.unsubscribe(reporteCitaChannel);
	cometdUtil.unsubscribe(reporteCanceladoChannel);
}

function exportarExcel() {
	window.open('/MidasWeb/siniestros/cabina/monitor/exportarExcel.action',
			"Exportar_Reportes_Monitor");
}
