var notificacionSound = soundManager.createSound({
	url : config.baseUrl + '/audio/notificacion/sounds-960-no-trespassing.mp3'
});

function eXcell_conta(cell) {
	if (cell) {
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.edit = function() {
	}
	this.isDisabled = function() {
		return true;
	}
	this.setValue = function(val) {
		vals = val.split("|");
		tiempoTrans = vals[0];
		tiempoAlarma = vals[1];
		tipoAlarma = vals[2];
		divId = "cont" + this.cell.parentNode.idd;
		this.setCValue("<div id='" + divId + "' " + attrTiempoAlarma + "='"
				+ tiempoAlarma + "' " + attrTiempoTrans + "='" + tiempoTrans
				+ "' " + attrTipoAlarma + "='" + tipoAlarma + "'>"
				+ iniContador(tiempoTrans, divId) + "</div>");

	}
	this.getValue = function() {
		if (this.cell._clearCell)
			return "";
		return this.cell.innerHTML.toString()._dhx_trim();
	}

}
eXcell_conta.prototype = new eXcell;
var interval
function contador(divId) {
	var strFecha = jQuery("#" + divId).attr(attrTiempoTrans);
	if (jQuery("#" + divId) == []) {
		clearInterval(interval);
	}
	//console.log("strFecha " + strFecha);
	var strFechaAla = jQuery("#" + divId).attr(attrTiempoAlarma);
	var tipoAlarma = jQuery("#" + divId).attr(attrTipoAlarma);
	jQuery("#" + divId).text(sacarDifHorasAhora(strFecha));
	if (alertasActivas) {
		if (!(jQuery("#" + divId).hasClass(tipoAlarma))
				&& ponerAlarma(strFechaAla)) {
			jQuery("#" + divId).addClass(tipoAlarma);
			if (ie === undefined)
				loopSound(notificacionSound, 3);
		}
	} else {
		jQuery("#" + divId).removeClass(tipoAlarma);
	}
}

var ie = (function(){
	 
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
 
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
 
    return v > 4 ? v : undef;
 
}());

function ponerAlarma(strFechaAla) {
	return new Date() > new Date(strFechaAla);
}

function iniContador(strFecha, divId) {
	interval = setInterval(function() {
		contador(divId);
	}, 1000);
	return sacarDifHorasAhora(strFecha);
}

function sacarDifHorasAhora(strFecha) {
	dateA = new Date();
	dateB = new Date(strFecha);
	var timeDiff = new Date(Math.abs(dateA - dateB));
	return sacarHora(timeDiff);
}

function isLater(str1, str2)
{
    return new Date(str1) > new Date(str2);
}

function sacarHora(fecha) {
	var hora = Math.floor(fecha / 1000 / 60 / 60);
	var min = ("0" + fecha.getMinutes()).slice(-2);
	var seg = ("0" + fecha.getSeconds()).slice(-2);
	var tiempoString = hora + ':' + min + ':' + seg;
	return tiempoString;
}
