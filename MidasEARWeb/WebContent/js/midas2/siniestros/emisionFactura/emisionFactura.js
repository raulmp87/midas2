var listadoEmisionFacturas;
var listadoGridHistoricoEmisionFactura;

function buscarEmisionFactura(){
	
	if( validarForma() ){
		removeCurrencyFormatOnTxtInput();	
		initEstruccturaGrid();
		listadoEmisionFacturas.load('/MidasWeb/siniestros/emision/factura/buscarEmisionFacturasGrid.action?'+jQuery("#buscarEmisionFacturasForm").serialize());
	}else{
		mostrarMensajeInformativo('Seleccione algún parametro de busqueda y/o ingrese ambos rangos', '10');
	}
}



function initEstruccturaGrid(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 listadoEmisionFacturas = new dhtmlXGridObject('listadoEmisionFacturas');	
	 listadoEmisionFacturas.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoEmisionFacturas.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 listadoEmisionFacturas.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 listadoEmisionFacturas.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });

	 jQuery("#divExcelBtn").show();
}


function limpiarEmisionFactura(){
	jQuery('#buscarEmisionFacturasForm').each (function(){
		  this.reset();
	});
}

function exportarExcelEmisionFactura(){
	if( validarForma() ){
		removeCurrencyFormatOnTxtInput();	
		
		var form = jQuery("#buscarEmisionFacturasForm").serialize();
		var url="/MidasWeb/siniestros/emision/factura/exportarExcelEmisionFacturas.action?"+form;
		window.open(url, "Emisión Factura");
		
	}else{
		mostrarMensajeInformativo('Seleccione algún parametro de busqueda y/o rangos', '10');
	}
}

function verFactura(id){
	
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/mostrarDetalleFactura.action?emisionId='+id+'&modoConsultar=true',targetWorkArea,null);
}

function editarFactura(id){
	
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/mostrarDetalleFactura.action?emisionId='+id+'&modoConsultar=false',targetWorkArea,null);
}

/***
 * Muestra venta con combo y opciones de la cancelación
 */
function verCancelacionFacturaEmision(){
	var emisionId =  jQuery("#emisionId").val();
	var url = "/MidasWeb/siniestros/emision/factura/muestraCancelarFacturaMotivos.action?emisionId="+emisionId;
	mostrarVentanaModal("vm_cancelacionFacturaEmision", 'CANCELAR FACTURA', 550, 310, 400, 250,url,null);
}

function validaMotivoCancelacionFactura(){
	var sel = jQuery("#listaMotivosCancelacion").val();
	if ( sel == "OTR" ){
		jQuery("#motivoCancelacion").attr("disabled",false);
	}else{
		jQuery("#motivoCancelacion").attr("value","");
		jQuery("#motivoCancelacion").attr("disabled",true);
	}
}

function aplicarCancelacionEmisionFactura(){
	var sel = jQuery("#listaMotivosCancelacion").val();
	if( sel == "" ){
		parent.mostrarMensajeInformativo('Seleccione el motivo', '10');
	}else if( sel == "OTR" && jQuery("#motivoCancelacion").val() == "" ){
		parent.mostrarMensajeInformativo('Describa el motivo', '10');
	}else{
		var url = "/MidasWeb/siniestros/emision/factura/cancelarFactura.action?"
			+"emisionId="+jQuery("#emisionId").val()
			+"&motivoCancelacionCtg="+jQuery("#listaMotivosCancelacion").val()
			+"&motivoCancelacionTxt="+jQuery("#motivoCancelacion").val();
		
		parent.sendRequestJQ(null,url,targetWorkArea,null);
		
		parent.cerrarVentanaModal("vm_cancelacionFacturaEmision",null);
	}
	
}



function validarForma(){
	
	var contadorElementos = 0;
	var respuesta         = false;
	
	jQuery(".obligatorio").each(function(index, value) { 
	    if( jQuery(this).val() != "" ){
	    	contadorElementos++;
	    }
	});
	
	if(contadorElementos == 0){
		respuesta = false;
	}else{
		respuesta = true;
	}
	
	if( jQuery("#fechaFacturacionDe").val() != "" || jQuery("#fechaFacturacionHasta").val() != "" ){
		if( convertirFecha(jQuery("#fechaFacturacionDe").val()) > convertirFecha(jQuery("#fechaFacturacionHasta").val()) ){
			respuesta = false;
			mostrarMensajeInformativo('La fecha de facturación inicial no puede ser mayor a la final', '10');
		}
	}
	if( jQuery("#fechaCancelacionDe").val() != "" || jQuery("#fechaCancelacionHasta").val() != "" ){
		if( convertirFecha(jQuery("#fechaCancelacionDe").val()) > convertirFecha(jQuery("#fechaCancelacionHasta").val()) ){
			respuesta = false;
			mostrarMensajeInformativo('La fecha de cancelación inicial no puede ser mayor a la final', '10');
		}
	}
	
	return respuesta;
}


function cerrarFacturaEmision(){
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/contenedorBusquedaEmisionFactura.action',targetWorkArea,null);
}

function aplicarCancelarFacturaEmision(){
	
	var form = jQuery("#detalleFacturaEmision").serialize();
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/contenedorBusquedaEmisionFactura.action',targetWorkArea,null);
}

function cargarCiudades(idEstado){
	 listadoService.getMapMunicipiosPorEstadoMidas(idEstado,
            function(data){
               addOptions(jQuery('#idCiudadName')[0],data);
            });
	
}

function cargarColonias(idCiudad){
	
	 listadoService.getMapColoniasPorCiudadMidas(idCiudad,
            function(data){
               addOptions(jQuery('#idColoniaName')[0],data);
            });
	
}

function initDetalleEmisionFactura(){
	
	var tipoRecuperacion = jQuery("#tipoRecuperacion").val();
	var habilitarTipo    = "";

	// SE DESHABILITAN TODOS LOS TEXT DE LA FORMA
	jQuery('input[type="text"],select').each(function(index,data) {
				jQuery(this).attr('disabled', true);
	});
	 
	// SI ES MODO EDICION SOLO SE HABILITAN POR TIPO DE RECUPERACION
	if( jQuery("#modoConsultar").val() == "false" ){
			
		// SE HABILITAN BOTONES POR TIPO DE RECUPERACION 
		if( tipoRecuperacion == "CIA" || tipoRecuperacion == "SVM" || tipoRecuperacion == "PRV" ){
			habilitarTipo = ".tipoCiaSvmPrv";
			// ELIMINAR DEL COMBO ASEGURADO OTRA PERSONA
			jQuery("#listaTiposPersona option[value='ASEG']").remove();
 			jQuery("#listaTiposPersona option[value='OTRAPER']").remove();
		}else if ( tipoRecuperacion == "DED" || tipoRecuperacion == "CRU"){
			habilitarTipo = ".tipoDedCru";
			// ELIMINAR DEL COMBO PROVEEDOR
			jQuery("#listaTiposPersona option[value='PROVEE']").remove();
		}
		
		jQuery(habilitarTipo).attr("disabled",false);
	}
}

function initGridHistoricoEmisionFactura(){
	
	var emisionId = jQuery("#emisionId").val();
	
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';

	listadoGridHistoricoEmisionFactura = new dhtmlXGridObject('listadoGridHistoricoEmisionFactura');	
	listadoGridHistoricoEmisionFactura.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoGridHistoricoEmisionFactura.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listadoGridHistoricoEmisionFactura.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
	});
	listadoGridHistoricoEmisionFactura.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});
	listadoGridHistoricoEmisionFactura.load('/MidasWeb/siniestros/emision/factura/listarMovimientosHistoricos.action?emisionId='+emisionId);
}

function editarFacturaEmsion(){
	var tipoRecuperacion = jQuery("#tipoRecuperacion").val();
	var emisionId = jQuery("#emisionId").val();
	var form = jQuery("#detalleFacturaEmision").serialize();
	if ( validaEditarEmision(tipoRecuperacion) ){
		sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/modificarFacturaEmision.action?'+form+"&emisionId="+emisionId,targetWorkArea,null);
	}
	
}


function validaEditarEmision(tipoRecuperacion){
	
	 var contador = 0;
	
	if( tipoRecuperacion == "CIA" || tipoRecuperacion == "SVM" || tipoRecuperacion == "PRV" ){
		jQuery(".obligatorioCiaSvmPrv").each(function(index, value) { 
		    if( jQuery(this).val() == "" ){
		    	contador ++;
		    }
		});
	}else if ( tipoRecuperacion == "DED" || tipoRecuperacion == "CRU"){
		jQuery(".obligatorioDedCru").each(function(index, value) { 
			if( jQuery(this).val() == "" ){
		    	contador ++;
		    }
			// VALIDAR EMAIL
			if( contador == 0){
				if ( !validationEmailGuardar( jQuery("#idEmail").val() ) ){
					contador++;
					mostrarMensajeInformativo('Email no valido', '10', null, null);
				}
			}
		});
	}
	
	if( contador > 0 ){
		mostrarMensajeInformativo('Ingrese todos los datos', '10', null, null);
		return false;
	}else{
		return true;
	}
	
}


function validationEmailGuardar( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
    	return false;
    }else{
    	return true;
    }
}

function convertirFecha(fecha){
	 var d=new Date();
	 var fecha = fecha.split("/");
	 return new Date(fecha[2],fecha[1],fecha[0]);
}


function initComboIngreso(){
	jQuery("#listadoEstatusIngreso option[value='CANCPEND']").remove();
	jQuery("#listadoEstatusIngreso option[value='CANCDEV']").remove();
	jQuery("#listadoEstatusIngreso option[value='CANCENVAC']").remove();
	jQuery("#listadoEstatusIngreso option[value='REVERSA']").remove();
}

function initComboEstatusFactura(){
	jQuery("#lstEstatusFactura option[value='PROC']").remove();
}


/*function cerrarVentanaCancelacionEmision(){
	parent.cerrarVentanaModal("vm_cancelacionFacturaEmision", "mostrarPantallaEmisionFacturaInicial();");
}

function mostrarPantallaEmisionFacturaInicial(){
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/contenedorBusquedaEmisionFactura.action',targetWorkArea,null);
}*/

/*function testCancelacion(){
	sendRequestJQ(null,'/MidasWeb/siniestros/emision/factura/cancelarFactura.action',targetWorkArea,null);
}*/
