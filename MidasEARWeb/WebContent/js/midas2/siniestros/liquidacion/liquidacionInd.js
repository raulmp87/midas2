var jquery143;
var gridBusquedaLiquidacionInmdBeneficiario; 
var listaBeneficiarios;
var pagosRelacionadosGrid;
var pagosPorRelacionarGrid;
var cifrasPagoGrid;
var banderaAsociandoTodas = false;
var OP_ASOCIAR = "1";
var OP_DESASOCIAR = "-1";
var TIPO_MENSAJE_ERROR = "10";
var PANTALLA_AUTORIZACION_LIQUIDACION_BENEFICIARIO = 'BIA';
var PANTALLA_LIQUIDACION_EGRESO_BENEFICIARIO = 'BIB';

function initGridBeneficiarios(){
	var origenBusqueda = jQuery("#origenBusquedaLiquidaciones").val();
	// ELIMINA ELEMENTOS DEL COMBO SI ES AUTORIZACION | BUSQUEDA INDEMNIZACION AUTORIZACION
	if( origenBusqueda === "BIA" ){
		jQuery("#estatusLiquidaciones option[value='ELIM']").remove();   // ELIMINADA
		jQuery("#estatusLiquidaciones option[value='ENTRAM']").remove(); // EN TRAMITE
		jQuery("#estatusLiquidaciones option[value='LIBSNSOL']").remove(); // EN TRAMITE
		jQuery("#estatusLiquidaciones option[value='APLIC']").remove(); // APLIC
		jQuery("#estatusLiquidaciones option[value='CANC']").remove(); // CANC
		jQuery("#estatusLiquidaciones option[value='TRMXMIZAR']").remove(); // TRMXMIZAR
		jQuery("#estatusLiquidaciones option[value='TRMXRECH']").remove(); // TRAMITE POR RECHAZO
		jQuery("#estatusLiquidaciones option[value='LIBCNSOL']").remove(); // LIBERADA CON SOLICITUD
	}
	
	cargaInicialLiquidaciones(origenBusqueda);
}

function cargaInicialLiquidaciones(origenBusqueda){
	initEstruccturaGrid();
	 
	removeCurrencyFormatOnTxtInput();
	if( origenBusqueda === "BIA" ){
		gridBusquedaLiquidacionInmdBeneficiario.load('/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/buscarLiquidacionAutorizacionBeneficiario.action?origenBusquedaLiquidaciones='+ origenBusqueda);
	} 
}

function buscarLiquidaciones(){
	var origenBusqueda = jQuery("#origenBusquedaLiquidaciones").val();
	
	initEstruccturaGrid();
	removeCurrencyFormatOnTxtInput();
	
	 if ( validarBusqueda() ){
		 removeCurrencyFormatOnTxtInput();
		 var form = jQuery("#buscarLiquidacionesForm").serialize();
		 if( origenBusqueda === "BIA" ){
			 gridBusquedaLiquidacionInmdBeneficiario.load('/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/buscarLiquidacionAutorizacionBeneficiario.action?'+form+"&origenBusquedaLiquidaciones="+origenBusqueda );
		 }else{
			 gridBusquedaLiquidacionInmdBeneficiario.load('/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/buscarLiquidacionBeneficiario.action?'+form+"&origenBusquedaLiquidaciones="+origenBusqueda );
		 }
		 
	 }else{
		 
		 mostrarMensajeInformativo('Se requieren como mínimo tres datos para continuar con la búsqueda', '10');
	 }
	
}

function initEstruccturaGrid(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 gridBusquedaLiquidacionInmdBeneficiario = new dhtmlXGridObject('listadoGridLiquidaciones');
	 gridBusquedaLiquidacionInmdBeneficiario.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	 gridBusquedaLiquidacionInmdBeneficiario.setSkin("light");
	 gridBusquedaLiquidacionInmdBeneficiario.setHeader("Oficina,No. Liquidación, Estatus, Tipo de Liquidación, Tipo de Operación, Beneficiario, No. Siniestro, Solicitado por, Neto a Pagar, Fecha de Solicitud, Fecha de Autorización, Fecha de Elaboración Cheque/Transferencia, No. Solicitud de Cheque, No. Cheque/Referencia, Autorizado por");
	 gridBusquedaLiquidacionInmdBeneficiario.setColumnIds("oficina,liquidacion,estatus,tipoLiquidacion,tipoOperacion,beneficiario,siniestro,solicitadoPor,totalPagar,fechaSolicitud,fechaAutorizacion,fechaElab,noSolCheque,noCheque,autorizadaPor");
	 gridBusquedaLiquidacionInmdBeneficiario.setInitWidths("120,120,135,140,150,180,80,80,180,100,120,100,155,135,135,180");
	 gridBusquedaLiquidacionInmdBeneficiario.setColAlign("center,center,center,center,center,center,center,center");
	 gridBusquedaLiquidacionInmdBeneficiario.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,edn,ro,ro,ro,ro,ro,ro");
	 gridBusquedaLiquidacionInmdBeneficiario.init();
	 
	 gridBusquedaLiquidacionInmdBeneficiario.attachEvent("onXLS", function(grid_obj){blockPage()});
	 gridBusquedaLiquidacionInmdBeneficiario.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 gridBusquedaLiquidacionInmdBeneficiario.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 gridBusquedaLiquidacionInmdBeneficiario.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });

	 jQuery("#divExcelBtn").show();
}


function exportarExcelBusquedaLiquidaciones(){
	 
	 removeCurrencyFormatOnTxtInput();
	 var origenBusqueda = jQuery("#origenBusquedaLiquidaciones").val();
		
	 if ( validarBusqueda() ){
		 var form = jQuery("#buscarLiquidacionesForm").serialize();
		 var url="/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/exportarBusquedaBeneficiarioExcel.action?"+form+"&origenBusquedaLiquidaciones="+origenBusqueda;
		 window.open(url, "Liquidaciones");
	 }else{
		 mostrarMensajeInformativo('Se debe realizar previamente una búsqueda para hacer la importación del excel', '10');
	 }
}

 
 // JCV
 function validarBusqueda(){
	 
	 if( validaRequerido() & validaCombos() & validaRangos() ){
		 return true;
	 }else{
		 return false;
	 }
	 
 }
 
 
 function validaRequerido(){
	 var aValores     = new Array();
	 var contador = 0;
	
	 jQuery(".obligatorio").each(function(index, value){
		 if( jQuery(this).val() == "" || jQuery(this).val() == "0" ){
			 aValores.push(0); // no existe valor
		 }else{
			 aValores.push(1); // existe valor
		 }
	 });
	 for (x=0; x< aValores.length; x++){		 	
			if(aValores[x] != "")
			{				
				contador++;
			}	
		}
	 if(contador >2){
		 return true;
	 }else{
		 return false;
	 }
	 /*
	 var aValores     = new Array();
	 
	 jQuery(".obligatorio").each(function(index, value){
		 if( jQuery(this).val() == "" || jQuery(this).val() == "0" ){
			 aValores.push(0); // no existe valor
		 }else{
			 aValores.push(1); // existe valor
		 }
	 });
	 var pos = aValores.indexOf(1);
	 if( aValores.indexOf(1) > -1 ){
		 return true;
	 }else{
		 return false;
	 }*/
 }
 
 function validaCombos(){
	 var aCombo     = new Array();
	 
	 var contCombo    = 0;
	 var contValCombo = 0;
	 
	 jQuery(".validaCombo").each(function(index, value){
		 contValCombo++;
		 jQuery(".validaComboValor").each(function(index, value){
			 if( jQuery(this).val() != "" ){
				 contCombo++;			 
			 }
		 });
	 });
	 
	 if( contValCombo > 0){
		 if( contCombo == 0 ){
			 aCombo.push(0);
		 }else{
			 aCombo.push(1); // existe valor
		 }
		 
		 var pos = aCombo.indexOf(1);
		 if( aCombo.indexOf(1) > -1 ){
			 return true;
		 }else{
			 return false;
		 }
	 }else{
		 return true;
	 }
	 
 }
 
 function validaRangos(){
	 //var aRango     = new Array();
	 var contRango 	  = 0;
	 var contValRango = 0;
	 
	 //VALIDA RANGOS
	 jQuery(".requeridoRango").each(function(index, value){
		 contValRango++;
		 if( jQuery(this).val() != "" ){
			 contRango++;			 
		 }
	 });	 
	 
	 if( contValRango > 0 ){
		 if( contRango > 0 ){
			 if( contValRango == contRango ){
				 return true;
			 }else{
				 return false;
			 }
		 }
		 
	 }else{
		 return true;
	 }
	 
 }

 // JCV 
 function validaCondicion(input1,input2,elemento,radio){
	 
	 // RANGOS
	 jQuery(input1).removeClass("requeridoRango");
	 jQuery(input2).removeClass("requeridoRango");
	 
	 jQuery(input1).removeAttr('disabled');
	 jQuery(input1).addClass("obligatorio");
	 jQuery(input1).addClass("validaComboValor");
	 jQuery(input1).attr("value","");
	 
	 jQuery(input2).attr('disabled','disabled');
	 jQuery(input2).removeClass("obligatorio");
	 jQuery(input2).attr("value","");
	 
	 jQuery(radio).attr('checked',false);
	 jQuery(elemento).addClass("validaCombo");
	 
	 if( jQuery(elemento).val() == "" ){
		 jQuery(elemento).removeClass("obligatorio");
		 jQuery(elemento).removeClass("validaCombo");
		 jQuery(input1).attr('disabled','disabled');
		 jQuery(input1).removeClass("obligatorio");
		 jQuery(input1).removeClass("validaComboValor");
		 jQuery(input2).removeClass("obligatorio");
	 }
	 
 }
 
 // JCV
 function validaRango(input1,input2,elemento,select){
	 
	 jQuery(input1).removeClass("validaComboValor");
	 jQuery(input2).removeClass("validaComboValor");
	 
	 if( jQuery(elemento).is(":checked") ){
		 
		 jQuery(input1).removeAttr('disabled');
		 jQuery(input1).addClass("requeridoRango");
		 
		 jQuery(input2).removeAttr('disabled');
		 jQuery(input2).addClass("requeridoRango");
		 
		 jQuery(select).attr("value","");
		 jQuery(select).removeClass("validaCombo");
	 }else{
		 jQuery(input1).attr('disabled','disabled');
		 jQuery(input1).removeClass("requeridoRango");
		 jQuery(input1).attr("value","");
		 
		 jQuery(input2).attr('disabled','disabled');
		 jQuery(input2).removeClass("requeridoRango");
		 jQuery(input2).attr("value","");
		 
		 jQuery(select).attr("value","");
		 jQuery(select).removeAttr("validaCombo");
	 }

 }

 // JCV
 function validaCheck(elemento){
	 
	 var i=0;
	 var tipo = "";
	 jQuery(".tipoServicio").each(function(index, value){
		 if( jQuery(this).is(":checked") ){
			 tipo  = jQuery(this).attr("title");
			 i++;
		 }
	 });
	 
	 if( i == 2 ){
		 jQuery("#hTipoServicio").attr("value",3);
	 }else if( i == 1 ){
		 if( tipo == "Publico" ){
			 jQuery("#hTipoServicio").attr("value",2);
		 }else if( tipo == "Privado" ){
			 jQuery("#hTipoServicio").attr("value",1);
		 }
	 }else if( i == 0){
		 jQuery("#hTipoServicio").attr("value",3);
	 }
	 
	 /////////////////////////////////////
	 if( jQuery(elemento).is(":checked") ){
		 jQuery(elemento).attr("value",1);
	 }else{
		 jQuery(elemento).attr("value",0);
	 }
 }
 
 function nuevaLiquidacion(){
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarLiquidacion.action?origenBusquedaLiquidaciones=BIB',targetWorkArea,null);	
 }
 
 function consultarLiquidacion(idLiquidacion, soloLectura, pantallaOrigen){
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarLiquidacion.action?liquidacionSiniestro.id='+ idLiquidacion + '&soloLectura=' 
				+ soloLectura+"&pantallaOrigen="+pantallaOrigen,targetWorkArea,null);
	} 
 
//-------- Inicia funcionalidad Autcomplete---------

 function findItem (term, devices) {
 	
 	var items = [];
     for (var i=0;i<devices.length;i++) {
         var item = devices[i];
         for (var prop in item) {
             var detail = item[prop].toString().toLowerCase();           
             if (detail.indexOf(term)>-1) {
                 items.push(item);
                 break;               
             }
         }
     }
     return items;
 }

 jquery143(function(){
	 if (typeof listaBeneficiarios != 'undefined')
	 {
		 jquery143("#nombreBeneficiario").autocomplete({
         source: listaBeneficiarios.size() > 0 ? listaBeneficiarios : function(request, response){         	
         	
         },
         minLength: 3,
         delay: 500,
         select: function(event, ui) {
	        	 jquery143("#idBeneficiario").val(ui.item.id);
	        	 buscarOrdenesPagoDisponibles(); 
	        	 guardarLiquidacionAutomatico('A');
	        	 obtenerTiposLiquidacionDisponibles(jQuery('#idLiquidacion').val());
         },
         search: function(event, ui) {
         	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
             var busqueda = jQuery("#nombreBeneficiario").val();
              
             if(findItem(busqueda.toString().toLowerCase(), listaBeneficiarios).length == 0){
	             	mostrarMensajeInformativo(("Beneficiario ("+busqueda.toString().toUpperCase()+") no existe o no cuenta con ordenes de pago pendientes de relacionar"), '20');
             	jQuery("#nombreBeneficiario").val("");
             	return false;
             }
         }
     });
	 } 	
 });

 jquery143(function(){
	 jquery143("#nombreBeneficiario").bind("keypress", function(e) {
 	    if (e.keyCode == 13) {
 	        e.preventDefault();         
 	        return false;
 	    }
 	});
 });

 //-------- termina funcionalidad Autocomplete
 
 function guardarLiquidacion(estatus)
 {
 	var form = jQuery('#definirLiquidacionForm').serialize(); //TODO Revisar si se debe enviar la forma completa

 	sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/guardarLiquidacion.action?' + form,targetWorkArea,null);	
 }

 function guardarLiquidacionAutomatico(origen)
 {	 
 	//var form = jQuery('#definirLiquidacionForm').serialize();
 	var form = encodeForm(jQuery('#definirLiquidacionForm'));
 	var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/guardarLiquidacionAutomatico.action?' + form;	
 	
 	var ajaxresults = jQuery.ajax({
 		"url" : url,
 		"dataType" : "text json",
 		"success": function(data) {
 			
         }
 	});	 	
 } 
 
 function buscarOrdenesPagoDisponibles()
 {
	var idBeneficiario = jQuery('#idBeneficiario').val();	
	
 	//mostrarIndicadorCarga('indicadorPagosDisponibles');

 	document.getElementById("pagosPorRelacionarGrid").innerHTML = '';	
 	pagosPorRelacionarGrid = new dhtmlXGridObject('pagosPorRelacionarGrid');
 	
 	if(jQuery('#soloLectura').val() == 'true')
	 {
 		pagosPorRelacionarGrid.enableDragAndDrop(false);		 
	 }else
	 {
		 pagosPorRelacionarGrid.enableDragAndDrop(true);			 
	 } 	
 	
 	pagosPorRelacionarGrid.attachEvent("onXLE", function(grid){
 		ocultarIndicadorCarga('indicadorPagosDisponibles'); 		
 		
     });
 	
 	pagosPorRelacionarGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
 		blockPage();
 		asociarDesasociarOrdenesPago(this,sId, OP_DESASOCIAR, sObj); 		 	
	}); 	
 	
	 pagosPorRelacionarGrid.attachHeader("#text_filter,&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp#text_filter,&nbsp");
	 var encodedForm = encodeForm(jQuery("#definirLiquidacionForm"));
	 var urlBusqueda = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarListadoOrdenPago.action?" + encodedForm;
	 pagosPorRelacionarGrid.load(urlBusqueda);		
 	
 	pagosPorRelacionarGrid.objBox.style.overflowX = "hidden";
 	
 }
 
 function buscarOrdenesPagoRelacionadas()
 {
	 //mostrarIndicadorCarga('indicadorPagosRelacionados');
	 var idLiquidacion = jQuery('#idLiquidacion').val();	 
	 
	 document.getElementById("pagosRelacionadosGrid").innerHTML = '';	
	 pagosRelacionadosGrid = new dhtmlXGridObject('pagosRelacionadosGrid');
	 
	 if(jQuery('#soloLectura').val() == 'true')
	 {
		 pagosRelacionadosGrid.enableDragAndDrop(false);			 
	 }else
	 {
		 pagosRelacionadosGrid.enableDragAndDrop(true);			 
	 } 	 
	 
	 pagosRelacionadosGrid.attachEvent("onXLE", function(grid){
	 		//ocultarIndicadorCarga('indicadorPagosRelacionados');
	 	
		 	//Cargar los registros existentes al grid cifras por pago
	 		pagosRelacionadosGrid.forEachRow(function(id){
	 			
	 			actualizarImportesPorPago(grid,id,OP_ASOCIAR);	 			
	 	 		
	 	 		});		 	
	 });
	 													  
	 pagosRelacionadosGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){	
		 blockPage();
		 asociarDesasociarOrdenesPago(this,sId, OP_ASOCIAR,sObj);		 	
		});			 
	 
	 	pagosRelacionadosGrid.attachHeader("#text_filter,&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp#text_filter,&nbsp");
	 
		pagosRelacionadosGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarOrdenesPagoAsociadas.action?liquidacionSiniestro.id=" 
				+ idLiquidacion);
		
		pagosRelacionadosGrid.objBox.style.overflowX = "hidden";
 }
 
 function initGridCifrasPorPago()
 {
	//mostrarIndicadorCarga('indicador');
 	document.getElementById("cifrasPagoGrid").innerHTML = '';	
 	cifrasPagoGrid = new dhtmlXGridObject('cifrasPagoGrid');
 		
 	cifrasPagoGrid.attachEvent("onXLS", function(grid){
 		//ocultarIndicadorCarga('indicador');
     });
 	
 	cifrasPagoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
 	cifrasPagoGrid.setSkin("light");
 	cifrasPagoGrid.setHeader("No. Orden Pago, Subtotal,Deducible,Descuentos, IVA, IVA RET, ISR, Neto a Pagar, Importe Salvamento");
 	cifrasPagoGrid.setColumnIds("numeroOrdenPago,subtotal,deducible,descuentos,iva,ivaRet,isr,netoPagar,importeSalvamento");
 	cifrasPagoGrid.setInitWidths("120,120,120,120,120,120,120,120,120");
 	cifrasPagoGrid.setColAlign("center,center,center,center,center,center,center,center,center");
 	cifrasPagoGrid.setColTypes("ron,ron,ron,ron,ron,ron,ron,ron,ron");
 	cifrasPagoGrid.setNumberFormat("$0,000.00",1);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",2);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",3);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",4);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",5);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",6);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",7);
 	cifrasPagoGrid.setNumberFormat("$0,000.00",8);
 	cifrasPagoGrid.attachHeader("#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,&nbsp");
 	cifrasPagoGrid.setColSorting("int,na,na,na,na,na,na,int,na")
 	cifrasPagoGrid.init();
 	cifrasPagoGrid.objBox.style.overflowX = "hidden";
 	
 } 
 
 function asociarDesasociarOrdenesPago(targetGrid, rowId, operacion, sourceGrid)
 { 	
	var  mensajeValidaciones = "";

	var url; 	
	var contador = 0; 
	
	if(operacion == OP_ASOCIAR)
	{
		url = '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/asociarOrdenPago.action?liquidacionSiniestro.id='+ 
				jQuery('#idLiquidacion').val() + '&ordenPago.id=' +  rowId;
		
		mensajeValidaciones = validarAsociacion(rowId);
		
	}else if(operacion == OP_DESASOCIAR)
	{
		url = '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/desasociarOrdenPago.action?liquidacionSiniestro.id='+ 
				jQuery('#idLiquidacion').val() + '&ordenPago.id=' +  rowId; 			
	}
	
	if(mensajeValidaciones.length == 0)
	{
		var ajaxresults = jQuery.ajax({
 			"url" : url,
 			"async": false,
 			"dataType" : "text json",
 			"success": function(data) {
 				
 				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
 					//sourceGrid
 					manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, JSON.parse(data).mensaje); 	 					 	 					
 				}else
 				{
 					actualizarImportesPorPago(targetGrid,rowId,operacion);
 					actualizarImportesLiquidacion(JSON.parse(data));
 					obtenerTiposLiquidacionDisponibles(jQuery('#idLiquidacion').val());
 					seleccionarDeseleccionarPrimas(); 					
 				} 	 				
 				
 	        }
 		});	 			
	}else
	{
		//Procesar manejo del error
		manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, mensajeValidaciones); 
	} 
	
	unblockPage();
 }
 
 function validarAsociacion(rowId)
 {
	 var mensaje = "";	 
	 var op_new_idSiniestro = pagosRelacionadosGrid.cellById(rowId,0).getValue();	 
	 var contadorPT = 0;
	 var contadorFPTransferencia = 0;	 
	 
	 //No existe mas de una Orden Pago por Perdida Total
	 pagosRelacionadosGrid.forEachRow(function(id) {
		 
		 //Las ordenes no pertenecen al mismo siniestro.
		 if(op_new_idSiniestro != pagosRelacionadosGrid.cellById(id,0).getValue())
		 {
			 mensaje = "Error: La orden de pago que esta intentando asociar pertenece a un siniestro distinto a la(s) orden(s) ya asociada(s).";
			 return;
		 }	
		 
		 if(pagosRelacionadosGrid.cellById(id,1).getValue() == "TB")
		 {
			 contadorFPTransferencia ++;			 			 			 
		 }
		 
		 if(pagosRelacionadosGrid.cellById(id,2).getValue() == "true")
		 {
			 contadorPT ++; 
		 }		 
		 
	 });
	 
	 //Combinacion de Formas de pago de las ordenes relacionadas es: Nula para todas las ordenes, o 1 con Transferencia Bancaria y el resto null.
	 if(contadorFPTransferencia > 1)
	 {
		 mensaje = "Error: Ya existe una orden de pago con forma de pago 'Transferencia Bancaría' asociada a la Liquidación.";		
	 }
	 
	 if(contadorPT > 1)
	 {
		 mensaje = "Error: No puede asociar mas de una orden de pago por Pérdida a Total a la Liquidación.";		
	 }		 
	 
	 return mensaje;	 
 }
 
 function manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, mensajeError)
 {
	 targetGrid.moveRowTo(rowId,"","move","sibling",targetGrid,sourceGrid);
	 mostrarMensajeInformativo(mensajeError, '20');	
 }
 
 
 function actualizarImportesPorPago(grid, rowId, operacion)
 {	
 	if(operacion == OP_ASOCIAR)
 	{
 		var colIndex = 4;
		var montosOrdenCompra= '';		
		var concepto = '';
		//concatena el numero de orden de pago
		concepto = grid.cellById(rowId,3).getValue() == ''?'0' : grid.cellById(rowId,3).getValue();			
		montosOrdenCompra = montosOrdenCompra.concat(concepto + ',');
		//concatena los montos de los diferentes conceptos:subtotal,deducible,descuentos,iva,ivaRet,isr,total,salvamento
		while(colIndex < grid.getColumnsNum())
		{				
			concepto = grid.cellById(rowId,colIndex).getValue() == ''?'0' : grid.cellById(rowId,colIndex).getValue();
			montosOrdenCompra = montosOrdenCompra.concat(concepto + ',');
			colIndex++;
		}
		cifrasPagoGrid.addRow(rowId,montosOrdenCompra.substring(0, montosOrdenCompra.length - 1)); 	
 		
 	}else if(operacion == OP_DESASOCIAR)
 	{
 		cifrasPagoGrid.deleteRow(rowId);	
 	}
 	
 	this.habilitarDeshabilitarBeneficiario();
 	this.desplegarBotones();
 	ocultarIndicadorCarga("indicador");
 }
 
 function actualizarImportesLiquidacion(liquidacionSiniestroJson)
 {
 	 jQuery('#subtotal').val(liquidacionSiniestroJson.liquidacionSiniestro.subtotal);
 	 jQuery('#deducible').val(liquidacionSiniestroJson.liquidacionSiniestro.deducible);
 	 jQuery('#descuentos').val(liquidacionSiniestroJson.liquidacionSiniestro.descuento);
 	 jQuery('#iva').val(liquidacionSiniestroJson.liquidacionSiniestro.iva);
 	 jQuery('#ivaRetenido').val(liquidacionSiniestroJson.liquidacionSiniestro.ivaRet);
 	 jQuery('#isr').val(liquidacionSiniestroJson.liquidacionSiniestro.isr);
 	 jQuery('#total').val(liquidacionSiniestroJson.liquidacionSiniestro.total);
 	 jQuery('#salvamento').val(liquidacionSiniestroJson.liquidacionSiniestro.importeSalvamento);
 	 jQuery('#bancoReceptor').val(liquidacionSiniestroJson.liquidacionSiniestro.bancoBeneficiario);
 	 jQuery('#clabe').val(liquidacionSiniestroJson.liquidacionSiniestro.clabeBeneficiario);
 	 jQuery('#rfc').val(liquidacionSiniestroJson.liquidacionSiniestro.rfcBeneficiario);
 	 jQuery('#correo').val(liquidacionSiniestroJson.liquidacionSiniestro.correo);
	 jQuery('#telefonoLada').val(liquidacionSiniestroJson.liquidacionSiniestro.telefonoLada);
	 jQuery('#telefonoNumero').val(liquidacionSiniestroJson.liquidacionSiniestro.telefonoNumero);
	    
	 jQuery('#netoPagar').val(liquidacionSiniestroJson.liquidacionSiniestro.total);
	 jQuery('#netoPagarTxt').val(liquidacionSiniestroJson.liquidacionSiniestro.total);
	 jQuery('#primaPendientePagoConsulta').val(liquidacionSiniestroJson.primaPendientePagoConsulta);
	 jQuery('#primaPendientePagoConsultaTxt').val(liquidacionSiniestroJson.primaPendientePagoConsulta);	 
	 jQuery('#primaNoDevengadaConsulta').val(liquidacionSiniestroJson.primaNoDevengadaConsulta);
	 jQuery('#primaNoDevengadaConsultaTxt').val(liquidacionSiniestroJson.primaNoDevengadaConsulta);
	 
	 if(liquidacionSiniestroJson.primaPendientePagoConsulta > 0)
	 {
		 jQuery("#checkPrimaPendiente").attr('disabled', false);
	 }else
	 {
		 jQuery("#checkPrimaPendiente").attr('disabled', true);		 
	 }
	 
	 if(liquidacionSiniestroJson.primaNoDevengadaConsulta > 0)
	 {
		 jQuery("#checkPrimaNoDevengada").attr('disabled', false);
	 }else
	 {
		 jQuery("#checkPrimaNoDevengada").attr('disabled', true);		 
	 }
	 
 	 
 	 actualizarTotalAPagar();
 	 
 	 initCurrencyFormatOnTxtInput();
 }
 
 function actualizarTotalAPagar()
 {
	 var totalAPagar = 0;
	 
	 totalAPagar = Number(jQuery('#netoPagar').val()) - Number(jQuery('#primaPendientePago').val()) + Number(jQuery('#primaNoDevengada').val());
	 jQuery('#totalPagar').val(totalAPagar);
	 jQuery('#totalPagarTxt').val(totalAPagar);
	 
	 initCurrencyFormatOnTxtInput();    
 }
 
 function seleccionarDeseleccionarPrimas()
 {	 
	 var primaPdtPago = 0;
	 var primaNoDevengada = 0;	 
	
	 if(jQuery('#checkPrimaPendiente').is(":checked"))
	 {
		 primaPdtPago = Number(jQuery('#primaPendientePagoConsulta').val());
		 jQuery('#primaPendientePago').val(primaPdtPago);
		 jQuery('#primaPendientePagoTxt').val(primaPdtPago);		 
		 		 
	 }else
	 {
		 jQuery('#primaPendientePago').val(primaPdtPago);
		 jQuery('#primaPendientePagoTxt').val(primaPdtPago);		 
	 }	 
	 
	 if(jQuery('#checkPrimaNoDevengada').is(":checked"))
	 {
		 primaNoDevengada = Number(jQuery('#primaNoDevengadaConsulta').val());
		 jQuery('#primaNoDevengada').val(primaNoDevengada);
		 jQuery('#primaNoDevengadaTxt').val(primaNoDevengada);		 
		 		 
	 }else
	 {
		 jQuery('#primaNoDevengada').val(primaNoDevengada);
		 jQuery('#primaNoDevengadaTxt').val(primaNoDevengada);		 
	 }
	 
	 actualizarTotalAPagar();
	 guardarLiquidacionAutomatico('B');     
 }

 function habilitarDeshabilitarBeneficiario()
 {
 	//Se deshabilita el proveedor en caso de existir una orden de pago asociada.
 	if(jQuery('#soloLectura').val() == 'true' || cifrasPagoGrid.getRowsNum() > 0)
 	{
 		jQuery("#iconoBorrar").hide(); 
 		jQuery("#nombreBeneficiario").attr("disabled",true); 	
 	}
 	else
 	{
 		jQuery("#iconoBorrar").show(); 
 		jQuery("#nombreBeneficiario").attr("disabled",false);		
 	}	
 }
 
 function desplegarBotones(){
		
		//--Boton Datos Cuenta Bancaria	
		if(jQuery("#proveedorTieneInfoBancaria").val() == "true"){
			
			jQuery("#botonDatosCtaBancaria").show();
		}
		else
		{
			jQuery("#botonDatosCtaBancaria").hide();		
		}	
		
		//-- Boton solicitar Autorizacion
		if((jQuery('#estatusLiquidacion').val() == 'ENTRAM' || 
				jQuery('#estatusLiquidacion').val() == 'TRMXRECH' ||
				jQuery('#estatusLiquidacion').val() == 'TRMXMIZAR') && cifrasPagoGrid.getRowsNum() > 0 )
		{
			jQuery('#botonSolAutorizacion').show();
			
		}else
		{
			jQuery('#botonSolAutorizacion').hide();		
		}
		
		//-- Boton imprimir
		if(cifrasPagoGrid.getRowsNum() > 0 )
		{
			jQuery('#botonImprimir').show();
			
		}else
		{
			jQuery('#botonImprimir').hide();		
		}
		
		//-- Boton Reexpedicion
		if(jQuery('#estatusLiquidacion').val() == 'LIBSNSOL')
		{
			jQuery('#botonReexpedir').show();
			
		}else
		{
			jQuery('#botonReexpedir').hide();		
		}
		
		//-- Boton Cancelar
		if(jQuery('#estatusLiquidacion').val() == 'TRMXRECH' || 
				jQuery('#estatusLiquidacion').val() == 'LIBCNSOL' ||
				jQuery('#estatusLiquidacion').val() == 'LIBSNSOL' ||
				jQuery('#estatusLiquidacion').val() == 'TRMXMIZAR')
		{
			jQuery('#botonCancelar').show();
			
		}else
		{
			jQuery('#botonCancelar').hide();		
		}	
		
		//-- Boton Eliminar
		if(pagosRelacionadosGrid.getRowsNum() == 0 && 
				(jQuery('#estatusLiquidacion').val() == 'ENTRAM' || 						 
				jQuery('#estatusLiquidacion').val() == 'XAUT'))
		{
			jQuery('#botonEliminar').show();
			
		}else
		{
			jQuery('#botonEliminar').hide();		
		}
		
		if(jQuery("#soloLectura").val() == "true"){
			jQuery(".esconder").hide();
		}
	}
 
 jQuery(document).ready(function () {  	
	 
	 actualizarTotalAPagar();	 
	 
 }); 
 
 function cerrarDetalle(){	
	 var pantallaOrigen = jQuery("#origenBusquedaLiquidaciones").val();
	 var url = "";
	 if(pantallaOrigen == "BIB"){
		 url = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarBusquedaLiquidacionIndemBeneficiario.action?origenBusquedaLiquidaciones=BIB";
		 sendRequestJQ(null, url, targetWorkArea, null);
	 }else if(pantallaOrigen == "BIA"){
		 url = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarBusquedaAutorizacionLiquidacionBeneficiario.action?origenBusquedaLiquidaciones=BIA";
		 sendRequestJQ(null, url, targetWorkArea, null);
	 }	 
 }
 
 function consultarLiquidacion(idLiquidacion, soloLectura, pantallaOrigen){
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarLiquidacion.action?liquidacionSiniestro.id='+ idLiquidacion + '&soloLectura=' 
				+ soloLectura+"&origenBusquedaLiquidaciones="+pantallaOrigen,targetWorkArea,null);
	} 
 
 //TODO unificar metodos comunes
 
 function solicitarAutorizacionLiq(idLiquidacion)
 {
	sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/solicitarAutorizacion.action?liquidacionSiniestro.id='+ 
			idLiquidacion,targetWorkArea,null);	
 }
 
 function eliminarLiquidacion(idLiquidacion)
 {
 	sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/eliminarLiquidacion.action?liquidacionSiniestro.id='+ 
 			idLiquidacion,targetWorkArea,null);	
 }
 
 function reexpedirLiquidacion(idLiquidacion)
 {	
	sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/reexpedir.action?liquidacionSiniestro.id='+ 
	 		idLiquidacion,targetWorkArea,null);
 } 
 
 function mostrarImprimirLiquidacionBeneficiario(pantallaOrigen){
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		if(idLiquidacion){
			var url = '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarImprimirLiquidacionBeneficiario.action?liquidacionSiniestro.id=' + idLiquidacion + "&pantallaOrigen=" + pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	function mostrarImprimirLiquidacionBeneficiario(idLiqSiniestro, pantallaOrigen){
		if(idLiqSiniestro){
			var url = '/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarImprimirLiquidacionBeneficiario.action?liquidacionSiniestro.id=' + idLiqSiniestro + "&pantallaOrigen=" + pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	function cerrarImprimirLiquidacionBeneficiario(){
		var pantallaOrigen = jQuery("#pantallaOrigen").val();
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		
		var url = "";
		if(pantallaOrigen == PANTALLA_AUTORIZACION_LIQUIDACION_BENEFICIARIO){
			url = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarBusquedaAutorizacionLiquidacionBeneficiario.action?origenBusquedaLiquidaciones="+pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else if(pantallaOrigen == PANTALLA_LIQUIDACION_EGRESO_BENEFICIARIO){
			url = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarBusquedaLiquidacionIndemBeneficiario.action?origenBusquedaLiquidaciones="+pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('No se puede regresar a la pantalla anterior, regrese manualmente.' , '20');
		}
	}
	
	function imprimirOrdenExpedicionChequeBeneficiario(){
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		var ES_PREVIEW = false;
		var esImprimible = jQuery("#esImprimible").val();
		if(idLiquidacion){
			if(esImprimible == 'true'){
				var url =	"/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/imprimirOrdenExpedicionChequeBeneficiario.action?liquidacionSiniestro.id="+idLiquidacion+"&esPreview="+ES_PREVIEW;
				window.open(url, "OrdenExpedicionCheque");
			}else{
				mostrarMensajeInformativo('La liquidaci\u00F3n requiere al menos una Orden de Pago para poder imprimirse.' , '20');
			}
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	
	

	 function autorizarLiquidacion(){
		   var formParams = jQuery(document.impresionLiquidacionBeneficiarioForm).serialize();
		   var idLiquidacion = jQuery('#idLiqSiniestro').val();
		   var numeroLiquidacion = jQuery("#numeroLiquidacion").val();
		   if(null==idLiquidacion || idLiquidacion==''){
			   mostrarMensajeInformativo('Debe existir numero de liquidacion para invocar esta accion ' , '20')
		   }else{
			   if(confirm("¿Desea autorizar la liquidacion "+numeroLiquidacion+"?" )){
				sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/autorizarLiquidacion.action?'+ formParams ,targetWorkArea,null);
		   }
	} 
	} 


	function rechazarLiquidacion(){
		var formParams = jQuery(document.impresionLiquidacionBeneficiarioForm).serialize();
		var idLiquidacion = jQuery('#idLiqSiniestro').val();
		var numeroLiquidacion = jQuery("#numeroLiquidacion").val();
		   if(null==idLiquidacion || idLiquidacion==''){
			   mostrarMensajeInformativo('Debe existir numero de liquidacion para invocar esta accion ' , '20')
		   }else{
			   if(confirm("¿Desea rechazar la liquidacion "+numeroLiquidacion+"?")){
				sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/rechazarLiquidacion.action?'+ formParams ,targetWorkArea,null);
		   }
		   }
			
		}

	function asociarDesasociarTodas(gridOrigen, gridDestino)
	{   
		if(jQuery('#soloLectura').val() == 'false')
		{
			banderaAsociandoTodas = true;
			blockPage();
			
			gridOrigen.forEachRow(function(id) {  
				
				gridOrigen.moveRowTo(id,"","move","sibling",gridOrigen,gridDestino);
				
				asociarDesasociarOrdenesPago(gridDestino, id, (gridOrigen.entBox.id == "pagosPorRelacionarGrid") ? OP_ASOCIAR:OP_DESASOCIAR, gridOrigen);	
				
			});
				
			unblockPage();
			banderaAsociandoTodas = false;		
		}	
	}
	
	
	 function limpiarBusquedaLiquidaciones(){
		 jQuery('#buscarLiquidacionesForm').each (function(){
			 this.reset();
		 });
		 limpiarClassBusquedaLiquidaciones(); 	
	 }	
	
	function limpiarClassBusquedaLiquidaciones(){
		// SE APOYA EN LA CLASE txtfield PARA REMOVER TODOS LOS DATOS
		jQuery(".txtfield").removeClass("requeridoRango validaCombo validaComboValor");
		
		jQuery("#montoDe").attr('disabled','disabled');
		jQuery("#montoHasta").attr('disabled','disabled');
		jQuery("#fechaSolDe").attr('disabled','disabled');
		jQuery("#fechaSolHasta").attr('disabled','disabled');
		jQuery("#fechaAutDe").attr('disabled','disabled');
		jQuery("#fechaAutHasta").attr('disabled','disabled');
		jQuery("#fechaDeTransfer").attr('disabled','disabled');
		jQuery("#fechaDeTransHasta").attr('disabled','disabled');
		
		jQuery("#hTipoServicio").attr("value",3);
		jQuery("#servicioPublico").attr("value",0);
		 jQuery("#servicioParticular").attr("value",0);
	}
	
	function imprimirCartaFiniquito(){
		formParams = jQuery(document.impresionLiquidacionBeneficiarioForm).serialize();
		var url = imprimirCartaFiniquitoPath + '?' +  formParams ;
	    window.open(url, "CartaFiniquito");
	}
	
	function mostrarCancelarLiquidacion(idLiquidacion) {
		var url="/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarCancelarLiquidacion.action?liquidacionSiniestro.id="+idLiquidacion;
		mostrarVentanaModal("mostrarCancelarLiquidacion", 'Cancelación de Liquidación', 100, 200, 700, 300, url);
	}
	
	function cerrarVentanaCancelacionLiq(redirect){
		if (redirect) {
			parent.cerrarVentanaModal('mostrarCancelarLiquidacion', "redirectCancelacion()");
		} else {
			parent.cerrarVentanaModal('mostrarCancelarLiquidacion', null);
		}
		
	}

	function confirmarCancelacion() {
		var nextFunction = 'enviarCancelacion()';
		parent.mostrarMensajeConfirm('¿Está seguro que desea cancelar la liquidación?', '20', 
				nextFunction, null, null)
	}

	function enviarCancelacion(){
		var parentFrame = parent.getWindowContainerFrame('mostrarCancelarLiquidacion');
		var idLiquidacion = parentFrame.contentWindow.document.getElementById('idLiquidacion').value;
		
		var radios = parentFrame.contentWindow.document.getElementsByName('tipoCancelacion');
		var tipoCancelacion;

		for (var i = 0, length = radios.length; i < length; i++) {
		    if (radios[i].checked) {
		    	tipoCancelacion = radios[i].value;
		        break;
		    }
		}
			
		var url="/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/cancelacionLiquidacion.action?liquidacionSiniestro.id="
					+idLiquidacion+"&tipoCancelacion="+tipoCancelacion;
		
		parent.redirectVentanaModal('mostrarCancelarLiquidacion', url, null);
	}

	function redirectCancelacion() {	
		
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/mostrarBusquedaLiquidacionIndemBeneficiario.action?mensaje=Se ha enviado la Cancelacion&tipoMensaje=30&origenBusquedaLiquidaciones=BIB',targetWorkArea,null);
	}
	
	function obtenerTiposLiquidacionDisponibles(idLiquidacion)
	{
		var url;
		var listaTiposLiq;
		var tipoLiquidacionRegistrado="";
		var opcionTransferenciaDefault = false;
		
		url = "/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/obtenerTiposLiquidacionDisponibles.action?liquidacionSiniestro.id=" 
				+ idLiquidacion;
		
		var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				listaTiposLiq = (JSON.parse(data)).listaTiposLiquidacion;			
				tipoLiquidacionRegistrado = (JSON.parse(data)).liquidacionSiniestro.tipoLiquidacion; 				
				
				jQuery('#listaTipoLiquidaciones')
			    .find('option')
			    .remove();		

				for (var prop in listaTiposLiq) 
				{		       
					if(prop == 'TRNBANC')
					{
						opcionTransferenciaDefault = true;					
					}
					
					jQuery("#listaTipoLiquidaciones").append('<option value="'+prop+'">'+listaTiposLiq[prop]+'</option>');				
			    }
				
				if(opcionTransferenciaDefault)//Seleccionar por default si aun no ha sido seleccionado
				{	
					jQuery("#listaTipoLiquidaciones option[value='TRNBANC']").attr("selected",true);
				}else if(!opcionTransferenciaDefault && (tipoLiquidacionRegistrado == "" || tipoLiquidacionRegistrado == null))
				{	
					jQuery("#listaTipoLiquidaciones option[value='CHQ']").attr("selected",true);									
				}else
				{	
					jQuery("#listaTipoLiquidaciones option[value='"+tipoLiquidacionRegistrado+"']").attr("selected",true);	
				}
	        }
		});		
	}
	
	function limpiarBeneficiarioLiq()
	{
		jQuery('#nombreBeneficiario').val(''); 
		jQuery('#idBeneficiario').val('');
	}
	
	function deshabilitarCombosPrimasLiqAplic()
	{
		if(jQuery("#estatusLiquidacion").val() == 'APLIC')
		{
			jQuery(".checkboxLabel").hide();
			jQuery("#primaPendientePagoConsultaTxt").hide();
			jQuery("#checkPrimaPendiente").hide();
			jQuery("#checkPrimaNoDevengada").hide();
			jQuery("#primaNoDevengadaConsultaTxt").hide();
		}
	}
