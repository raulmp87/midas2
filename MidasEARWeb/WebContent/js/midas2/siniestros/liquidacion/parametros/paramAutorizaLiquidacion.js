
var solicitudAutLiquidacionGrid;
 
 
 function realizarBusquedaParamAutLiq(cargaFiltro){
 	console.log('realizarBusquedaParamAutLiq:'+cargaFiltro);
 	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/buscarParametros.action?";
 	if(cargaFiltro){
 		removeCurrencyFormatOnTxtInput(); // ELIMINAR FORMATO CURRENCY
// 		if(validaCamposDeBusqueda()){//Se quita la validacion de seleccion al menos de un campo, ya que siempre estan seleccionados los campos que dicen TODOS
 			if(validarRangosConAmbasFechas()){
		 		 formParams = jQuery(document.busquedaParamAutLiquidacionForm).serialize();
		 		 url+=formParams;
		 		 loadGrid(url);
 			}else{
 				mostrarVentanaMensaje("10","Debe seleccionar ambas fechas si quiere buscar por rango",null);
 			}
//	 	}
// 		else{
//	 		mostrarVentanaMensaje("10","Debe existir por lo menos un filtro capturado para poder buscar",null);
//	 	}
 	}
 	console.log('realizarBusquedaParamAutLiq - url :'+url);
 }

 function loadGrid(url){
 		 jQuery("#solicitudAutLiquidacionGrid").empty(); 
		 solicitudAutLiquidacionGrid = new dhtmlXGridObject('solicitudAutLiquidacionGrid');
		 solicitudAutLiquidacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 solicitudAutLiquidacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 solicitudAutLiquidacionGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicadorSolAutParamLiquidacion");
		    });
		 solicitudAutLiquidacionGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicadorSolAutParamLiquidacion');
		    });
		 solicitudAutLiquidacionGrid.load( url ) ;	
 }

 function mostrarBusqueda(){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/mostrarContenedorBusqueda.action";
	sendRequestJQ(null, url,"contenido", null);
}

function mostrarAlta(){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/mostrarContenedorAlta.action?esParametroSoloLectura=false";
	sendRequestJQ(null, url,"contenido", null);
}

function consultarParametro(parametroId){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/consultarParametro.action?esParametroSoloLectura=true&parametroId="+parametroId;
	sendRequestJQ(null, url,"contenido", null);
}

function editarParametro(parametroId){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/consultarParametro.action?esParametroSoloLectura=false&parametroId="+parametroId;
	sendRequestJQ(null, url,"contenido", 'activaEstatus();');
}

function activaEstatus(){
	var estatusParam = jQuery("#estatusParam").val();
	if(estatusParam == 'true'){
		jQuery('#s_estatus').removeAttr('disabled');
	}
	else{
		jQuery('#s_estatus').attr('disabled',true);
		jQuery('#b_buscar').hide();
	}
}


function limpiaTiposDeServicio(){
	jQuery('#ch_spublico').attr('checked',false);
	jQuery('#ch_sparticular').attr('checked',false);
	seleccionaCheckButton();
}

 function seleccionaCheckButton(){

	var publico = jQuery('#ch_spublico').is(':checked');
 	var privado = jQuery('#ch_sparticular').is(':checked');
 	console.log('Publico : '+publico);
 	console.log('Privado : '+privado);
	

	if(publico){
		console.log('Publico checked : '+jQuery('#ch_spublico').val());
		jQuery('#h_tipo_servicio').val(jQuery('#ch_spublico').val());
 	}else if(!privado){
 		console.log('Borra en publico');
 		jQuery('#h_tipo_servicio').val('');
 	}
 	if( privado){
 		jQuery('#h_tipo_servicio').val(jQuery('#ch_sparticular').val());
		console.log('Particular checked: '+jQuery('#ch_sparticular').val());
 	}else if( !publico ){
 		console.log('Borra en privado');
 		jQuery('#h_tipo_servicio').val('');
 	}

 	if(publico  && privado){
 		console.log('Ambos');
 		jQuery('#h_tipo_servicio').val(3);
 	} 
	console.log('Valor actual : '+jQuery('#h_tipo_servicio').val());
}


function guardarParametro(){
	removeCurrencyFormatOnTxtInput(); // ELIMINAR FORMATO CURRENCY
	if(validateAll(true)){
		if(validaCondicionCorrectaVigencia()){
			if(validaCondicionCorrectaMonto()){
				if(validarRangosCorrectosVigencia()){
					if(validarRangosCorrectosMonto()){
					 	console.log('guardarParametro:');
					 	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/guardarParametro.action?";
						formParams = jQuery(document.altaParamAutLiquidacionForm).serialize();
						url+=formParams;
						console.log('guardarParametro - url :'+url);
					 	sendRequestJQ(null, url,"contenido", 'activaEstatus();');
					}else{
						mostrarVentanaMensaje("10","Verifique que el rango ingresado en el MONTO A PAGAR sea correcto",null);
					}
				}else{
					mostrarVentanaMensaje("10","Verifique que el rango ingresado en la VIGENCIA sea correcto",null);
				}
			}else{
				mostrarVentanaMensaje("10","Verifique que haya capturado la condici\u00F3n de MONTO A PAGAR correctamente",null);
			}
		}else{
			mostrarVentanaMensaje("10","Verifique que haya capturado la condici\u00F3n de VIGENCIA correctamente",null);
		}
	}
 }

 function setModoConsulta(){
 	esModoConsulta = false;
 	parametroId = jQuery('#h_noParametro').val();
 	if( parametroId != ''){
 		esModoConsulta = true;
 	}
 	if(esModoConsulta){
 		jQuery('.desabilitable').attr('disabled','disabled');
 		jQuery(".controles").remove();
 	}else{
 		jQuery('.desabilitable').removeAttr('disabled');
 			
 	}
 	validaTipoDeServicio(esModoConsulta);
 }

 
function cargaMultiplesSelecciones(strConcat,nombreBase){
	strConcat = strConcat+'';
 	console.log('Recibe strConcat: '+strConcat);
 	console.log('Recibe nombreBase: '+nombreBase);
 	arrayOficinas = strConcat.split(',');
	for(var i = 0; i < arrayOficinas.length; i++){
	    compuesto = nombreBase.concat(arrayOficinas[i].trim());
	    console.log('Compuesto: '+compuesto);
	    jQuery("#"+compuesto.trim()).attr("checked","checked");
	}
 }

 function limpiarFormularioParamAutLiquidacion(){
	jQuery("#contenedor_fechaActivoFin").hide();
	jQuery("#contenedor_fechaInactivoFin").hide();
	jQuery('#busquedaParamAutLiquidacionForm').each (function(){
		  this.reset();
	});
}

function validaTipoDeServicio(soloConsulta){
	var tipoServicio = jQuery('#h_tipo_servicio').val();
	if(typeof tipoServicio != 'undefined' && !isEmpty(tipoServicio)){
		if( tipoServicio == 1){
			jQuery('#ch_sparticular').attr('checked',true);
		}
		if( tipoServicio == 2){
			jQuery('#ch_spublico').attr('checked',true);
		}
		if( tipoServicio == 3){
			jQuery('#ch_sparticular').attr('checked',true);
			jQuery('#ch_spublico').attr('checked',true);
		}
	}
	if(soloConsulta){
		jQuery('#ch_sparticular').attr('disabled',true);
		jQuery('#ch_spublico').attr('disabled',true);
	}
}

function validaCamposDeBusqueda(){
	var alMenosUnFiltro = false;
	jQuery(".filtroBusqueda").each(function(i){
		if(jQuery(this).val()){
			alMenosUnFiltro = true;
		}
	});
	return alMenosUnFiltro;
}

function cerrarAlta(){
	var url = "/MidasWeb/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro/mostrarContenedorBusqueda.action";
	sendRequestJQ(null, url,"contenido", 'realizarBusquedaParamAutLiq(false);');
}

function onChangeCondicionActivo(){
	var HEADER_VALUE = "";
	var condicionSeleccionada = jQuery("#s_condFechaActivo").val();
	if(condicionSeleccionada != null  
			&& condicionSeleccionada != HEADER_VALUE){
		jQuery("#c_condFechaActivo").removeAttr('checked');
		jQuery("#fechaActivoFin").val("");
		jQuery("#contenedor_fechaActivoFin").hide();
	}
}

function onChangeCondicionInactivo(){
	var HEADER_VALUE = "";
	var condicionSeleccionada = jQuery("#s_condFechaInactivo").val();
	if(condicionSeleccionada != null  
			&& condicionSeleccionada != HEADER_VALUE){
		jQuery("#c_condFechaInactivo").removeAttr('checked');
		jQuery("#fechaInactivoFin").val("");
		jQuery("#contenedor_fechaInactivoFin").hide();
	}
}

function onChangeRangoActivo(){
	var esRangoSeleccionado = jQuery("#c_condFechaActivo").attr('checked');
	if(esRangoSeleccionado){
		jQuery("#contenedor_fechaActivoFin").show();
		jQuery("#s_condFechaActivo").val("");
	}else{
		jQuery("#contenedor_fechaActivoFin").hide();
		jQuery("#dp_fechaActivoIni").val("");
		jQuery("#fechaActivoFin").val("");
	}
}

function onChangeRangoInactivo(){
	var esRangoSeleccionado = jQuery("#c_condFechaInactivo").attr('checked');
	if(esRangoSeleccionado){
		jQuery("#contenedor_fechaInactivoFin").show();
		jQuery("#s_condFechaInactivo").val("");
	}else{
		jQuery("#contenedor_fechaInactivoFin").hide();
		jQuery("#dp_fechaInactivoIni").val("");
		jQuery("#fechaInactivoFin").val("");
	}
}

function configurarParaCondicionVigencia(){
	var condicionSeleccionada = jQuery("#ch_condicionVigencia").attr('checked');
	if(condicionSeleccionada){
		jQuery("#ch_rangoVigencia").removeAttr('checked');
		jQuery("#fechaVigenciaHasta").val("");
		jQuery(".VigenciaHasta").hide();
		jQuery("#s_condicionVigencia").removeAttr('disabled');
	}
}

function configurarParaRangoVigencia(){
	var rangoSeleccionado = jQuery("#ch_rangoVigencia").attr('checked');
	if(rangoSeleccionado){
		jQuery("#ch_condicionVigencia").removeAttr('checked');
		jQuery(".VigenciaHasta").show();
		jQuery("#s_condicionVigencia").val("");
		jQuery("#s_condicionVigencia").attr("disabled",true);
	}
}

function configurarParaCondicionMonto(){
	var condicionSeleccionada = jQuery("#ch_condicionMonto").attr('checked');
	if(condicionSeleccionada){
		jQuery("#ch_rangoMontos").removeAttr('checked');
		jQuery("#txt_montoHasta").val("");
		jQuery("#contenedor_montoHasta").hide();
		jQuery("#s_condicionMonto").removeAttr('disabled');
	}
}

function configurarParaRangoMonto(){
	var rangoSeleccionado = jQuery("#ch_rangoMontos").attr('checked');
	if(rangoSeleccionado){
		jQuery("#ch_condicionMonto").removeAttr('checked');
		jQuery("#contenedor_montoHasta").show();
		jQuery("#s_condicionMonto").val("");
		jQuery("#s_condicionMonto").attr("disabled",true);
	}
}

function onChangeCondicionVigencia(){
	var seleccion = jQuery("#s_condicionVigencia").val();
	if(seleccion){
		jQuery("#ch_condicionVigencia").attr("checked",true);
		configurarParaCondicionFecha();
	}
}

function onChangeCondicionMonto(){
	var seleccion = jQuery("#s_condicionMonto").val();
	if(seleccion){
		jQuery("#ch_condicionMonto").attr("checked",true);
		configurarParaCondicionMonto();
	}
}

function configurarMostrarParametros(){
	var VigenciaHasta = jQuery("#fechaVigenciaHasta").val();
	var MontoHasta = jQuery("#txt_montoHasta").val();
	var condicionVigencia = jQuery("#s_condicionVigencia").val();
	var condicionMonto = jQuery("#s_condicionMonto").val();
	if(VigenciaHasta){
		jQuery(".VigenciaHasta").show();
		jQuery("#ch_rangoVigencia").attr("checked",true);
	}else if(condicionVigencia){
		jQuery("#ch_condicionVigencia").attr("checked",true);
	}
	if(MontoHasta){
		jQuery("#contenedor_montoHasta").show();
		jQuery("#ch_rangoMontos").attr("checked",true);
	}else if(condicionMonto){
		jQuery("#ch_condicionMonto").attr("checked",true);
	}
}

function validarRangosConAmbasFechas(){
	var rangoActivo = false;
	var rangoInactivo = false;
	var fechaActivoIni = jQuery("#dp_fechaActivoIni").val();
	var fechaActivoFin = jQuery("#fechaActivoFin").val();
	var fechaInactivoIni = jQuery("#dp_fechaInactivoIni").val();
	var fechaInactivoFin = jQuery("#fechaInactivoFin").val();
	var rangoActivoChecked = jQuery("#c_condFechaActivo").attr('checked');
	var rangoInactivoChecked = jQuery("#c_condFechaInactivo").attr('checked');
	
	if(!rangoActivoChecked){
		rangoActivo = true;
	}else if(fechaActivoIni && fechaActivoFin || !fechaActivoIni && !fechaActivoFin){
		rangoActivo = true;
	}
	
	if(!rangoInactivoChecked){
		rangoInactivo = true;
	}else if(fechaInactivoIni && fechaInactivoFin || !fechaInactivoIni && !fechaInactivoFin){
		rangoInactivo = true;
	}
	
	if(rangoActivo && rangoInactivo){
		return true;
	}else{
		return false;
	}
}

function validarRangosCorrectosVigencia(){
	var rangoSeleccionado = jQuery("#ch_rangoVigencia").attr('checked');
	var fechasValidas = false;
	if(rangoSeleccionado){
		var fechaInicio = jQuery("#fechaVigenciaDe").val();
		var fechaFin = jQuery("#fechaVigenciaHasta").val();
		if(fechaInicio && fechaFin){
			var fechaInicioSplit = fechaInicio.split("/");
			var fechaInicioDate = new Date(fechaInicioSplit[2], fechaInicioSplit[1]-1, fechaInicioSplit[0]);
			var fechaFinSplit = fechaFin.split("/");
			var fechaFinDate = new Date(fechaFinSplit[2], fechaFinSplit[1]-1, fechaFinSplit[0]);
			if(fechaInicioDate <= fechaFinDate){
				fechasValidas = true;
			}
		}
	}else{
		fechasValidas = true;
	}
	return fechasValidas;
}

function validarRangosCorrectosMonto(){
	var rangoSeleccionado = jQuery("#ch_rangoMontos").attr('checked');
	var montosValidos = false;
	if(rangoSeleccionado){
		var montoDesde = jQuery("#txt_montoDe").val();
		var montoHasta = jQuery("#txt_montoHasta").val();
		if(montoDesde && montoHasta){
			if(Number(montoDesde) <= Number(montoHasta)){
				montosValidos = true;
			}
		}
	}else{
		montosValidos = true;
	}
	return montosValidos;
}

function validaCondicionCorrectaVigencia(){
	var condicionSeleccionado = jQuery("#ch_condicionVigencia").attr('checked');
	var rangoSeleccionado = jQuery("#ch_rangoVigencia").attr('checked');
	var condicionCompleta = false;
	
	var condicionVigencia = jQuery("#s_condicionVigencia").val();
	var fechaVigenciaDe = jQuery("#fechaVigenciaDe").val();
	
	if(rangoSeleccionado){
		condicionCompleta = true;
	}else if(condicionSeleccionado){
		if(condicionVigencia && fechaVigenciaDe){
			condicionCompleta = true;
		}
	}else if(fechaVigenciaDe){
		if(condicionSeleccionado && condicionVigencia){
			condicionCompleta = true;
		}
	}else{
		condicionCompleta = true;
	}
	
	return condicionCompleta;
}

function validaCondicionCorrectaMonto(){
	var condicionSeleccionado = jQuery("#ch_condicionMonto").attr('checked');
	var rangoSeleccionado = jQuery("#ch_rangoMontos").attr('checked');
	var condicionCompleta = false;
	
	var condicionMonto = jQuery("#s_condicionMonto").val();
	var montoDe = jQuery("#txt_montoDe").val();
	if(rangoSeleccionado){
		condicionCompleta = true;
	}else if(condicionSeleccionado){
		if(condicionMonto && montoDe){
			condicionCompleta = true;
		}
	}else if(montoDe){
		if(condicionSeleccionado && condicionMonto){
			condicionCompleta = true;
		}
	}else{
		condicionCompleta = true;
	}
	
	return condicionCompleta;
}