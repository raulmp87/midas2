//var testVar;
var jquery143;
var listaPrestadoresServicio;// = new Array();
var gridBusquedaLiquidacion;
var gridInformacionBancariaLiquidaciones;
var PANTALLA_AUTORIZACION_LIQUIDACION = "AUT";
var PANTALLA_LIQUIDACION_EGRESO = "EGR";
var cifrasPagoGridGrid;
var ordenesDeCompraGrid;
var cifrasPagoNotaCreditoGrid;
var importesLiqGrid;
var pagosRelacionadosGrid;
var pagosPorRelacionarGrid;
var notasCreditoAsociadasGrid;
var notasCreditoPorRelacionarGrid;
var recuperacionesPendientesGrid;
var OP_ASOCIAR = "1";
var OP_DESASOCIAR = "-1";
var subGridOrdenesPago;
var banderaAsociandoTodas = false;
var TIPO_MENSAJE_ERROR = "10"
var requiereAsociarDesasociar=[]; //bandera determina cuando se debe invocar el metodo correspondiente para asocias/deasociar ordenes de compra
var requiereAsociarDesasociarNCRs=[];
var requiereAsociarDesasociarNCRsRec=[];
var requiereRecalculo = -1;
 
 function limpiar(){
		jQuery(document.bandejaLiqForm).each (function(){
			  this.reset();
		});
	} 
 
 
	function consultarLiquidacion(idLiquidacion, soloLectura, pantallaOrigen){
		//pantallaOrigen = PANTALLA_AUTORIZACION_LIQUIDACION = "AUT";
		//pantallaOrigen =  PANTALLA_LIQUIDACION_EGRESO = "EGR";
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarLiquidacion.action?liquidacionSiniestro.id='+ idLiquidacion + '&soloLectura=' 
				+ soloLectura+"&pantallaOrigen="+pantallaOrigen,targetWorkArea,null);
	} 
	
	 function cerrarDetalle(){	
		 var pantallaOrigen = jQuery("#pantallaOrigen").val();
		 var url = "";
		 if(pantallaOrigen == PANTALLA_AUTORIZACION_LIQUIDACION){
			 url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaAutorizacionLiquidacionProveedor.action";
			 sendRequestJQ(null, url, targetWorkArea, null);
		 }else if(pantallaOrigen == PANTALLA_LIQUIDACION_EGRESO){
			 url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaLiquidacionProveedor.action";
			 sendRequestJQ(null, url, targetWorkArea, null);
		 }
		 
	 }
	 
	 
	 
	function nuevaLiquidacion(){
		sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarLiquidacion.action?pantallaOrigen=EGR',targetWorkArea,null);	
	} 
	
	 
	function imprimirOrdenExpedicionCheque(){
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		var ES_PREVIEW = false;
		var esImprimible = jQuery("#esImprimible").val();
		if(idLiquidacion){
			if(esImprimible == 'true'){
				var url =	"/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/imprimirOrdenExpedicionCheque.action?liquidacionSiniestro.id="+idLiquidacion+"&esPreview="+ES_PREVIEW;
				window.open(url, "OrdenExpedicionCheque");
			}else{
				mostrarMensajeInformativo('La liquidaci\u00F3n requiere al menos una Orden de Pago para poder imprimirse.' , '20');
			}
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	function mostrarImprimirLiquidacion(pantallaOrigen){
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		if(idLiquidacion){
			var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarImprimirLiquidacion.action?liquidacionSiniestro.id=' + idLiquidacion + "&pantallaOrigen=" + pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	function mostrarImprimirLiquidacion(idLiqSiniestro, pantallaOrigen){
		if(idLiqSiniestro){
			var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarImprimirLiquidacion.action?liquidacionSiniestro.id=' + idLiqSiniestro + "&pantallaOrigen=" + pantallaOrigen;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir' , '20');
		}
	}
	
	function cerrarImprimirLiquidacion(){
		var pantallaOrigen = jQuery("#pantallaOrigen").val();
		var idLiquidacion = jQuery("#idLiqSiniestro").val();
		var url = "";
		if(pantallaOrigen == PANTALLA_AUTORIZACION_LIQUIDACION){
			url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaAutorizacionLiquidacionProveedor.action";
			sendRequestJQ(null, url, targetWorkArea, null);
		}else if(pantallaOrigen == PANTALLA_LIQUIDACION_EGRESO){
			url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaLiquidacionProveedor.action?liquidacionSiniestro.id=" +  idLiquidacion;
			sendRequestJQ(null, url, targetWorkArea, null);
		}else{
			mostrarMensajeInformativo('No se puede regresar a la pantalla anterior, regrese manualmente.' , '20');
		}
	}	

	function reexpedirLiquidacion(){		

		var form = jQuery('#definirLiquidacionForm').serialize();

		sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/reexpedir.action?'+ form,targetWorkArea,null);
	} 	
	 
 function iniciaContenedorLiquidacion(){
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoLiquidacionesGrid = new dhtmlXGridObject('liquidacionesGrid');	
		listadoLiquidacionesGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		listadoLiquidacionesGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
	    });	
		
		var formParams = jQuery(document.bandejaLiqForm).serialize();
		listadoLiquidacionesGrid.load( '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarListaVacia.action?'+ formParams); 
	
 }
  
 function changeTipoPago() {	
		var tipo = dwr.util.getValue("tipoPagoLis");
		if  (tipo=="PP"){
			jQuery("#divNomBenef").hide();
			jQuery("#divProvedor1").show();
			jQuery("#divProvedorNom1").show();
			jQuery("#nomBeneficiario").val("");
		}else {
			jQuery("#divNomBenef").show();
			jQuery("#divProvedor1").hide();
			jQuery("#divProvedorNom1").hide();
			jQuery("#tipoProveedorLis").val("");
			jQuery("#proveedorLis").val("");
		}
}  
 
 function changeTipoPrestador() {	
	 var tipo = dwr.util.getValue("tipoProveedorLis");	
	 if(null ==tipo   || tipo=="" ){
			dwr.util.removeAllOptions("proveedorLis");		
		}else{
		listadoService.getMapPrestadorPorTipo( tipo ,function(data){
			dwr.util.removeAllOptions("proveedorLis");
			dwr.util.addOptions("proveedorLis", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("proveedorLis", data);
		});
		}
 }

 // JCV
 function busquedaTempProveedor(){
	 sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaLiquidacionProveedor.action?origenBusquedaLiquidaciones=proveedor',targetWorkArea,null);
 }
 
 function busquedaTempIndemnizacion(){
	 sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaLiquidacionIndemnizacion.action?origenBusquedaLiquidaciones=indemnizacion',targetWorkArea,null);
 }
 
 // JCV
 function limpiarBusquedaLiquidaciones(tipo){
	 
	 if( tipo == "autorizacion" ){		 
		 jQuery('#buscarLiquidacionesAutorizacionForm').each (function(){
	 		  this.reset();
	 	});
		 limpiarClassBusquedaLiquidaciones(); 
	 }else{		
		 jQuery('#buscarLiquidacionesForm').each (function(){
	 		  this.reset();
	 	});
		 limpiarClassBusquedaLiquidaciones(); 
	 }	
 }

 
 // JCV
 function buscarLiquidacionesAutorizacion(){
	 
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 gridBusquedaLiquidacion = new dhtmlXGridObject('listadoGridLiquidaciones');
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid_obj){blockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });	

	 jQuery("#divExcelBtn").show();
	 //joksrc
	 if ( validarBusqueda() ){
		 removeCurrencyFormatOnTxtInput();
		 var form = jQuery("#buscarLiquidacionesAutorizacionForm").serialize();
		 gridBusquedaLiquidacion.load('/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/buscarLiquidacionAutorizacion.action?'+form );
	 }else{
		 mostrarMensajeInformativo('Capture tres datos para realizar la búsqueda', '10');
	 }
	 
 }
 
 // JCV
 function validarBusqueda(){
	 //if( validaRequerido()){
	 if( validaRequerido() & validaCombos() & validaRangos() ){
		 return true;
	 }else{
		 return false;
	 }
	 
 }
 
 //valida capturar 3 parámetros como obligatorios. joksrc
 function validaRequerido(){
	 var aValores     = new Array();
	 var contador = 0;
	
	 jQuery(".obligatorio").each(function(index, value){
		 if( jQuery(this).val() == "" || jQuery(this).val() == "0" ){
			 aValores.push(0); // no existe valor
		 }else{
			 aValores.push(1); // existe valor
		 }
	 });
	 for (x=0; x< aValores.length; x++){		 	
			if(aValores[x] != "")
			{				
				contador++;
			}	
		}
	 if(contador >2){
		 return true;
	 }else{
		 return false;
	 }
	 /*
	 jQuery(".obligatorio").each(function(index, value){
		 if( jQuery(this).val() == "" || jQuery(this).val() == "0" ){
			 aValores.push(0); // no existe valor
		 }else{
			 aValores.push(1); // existe valor
		 }
	 });
	 var pos = aValores.indexOf(1);
	 if( aValores.indexOf(1) > -1 ){
		 return true;
	 }else{
		 return false;
	 }*/
 }
 
 function validaCombos(){
	 var aCombo     = new Array();
	 
	 var contCombo    = 0;
	 var contValCombo = 0;
	 
	 jQuery(".validaCombo").each(function(index, value){
		 contValCombo++;
		 jQuery(".validaComboValor").each(function(index, value){
			 if( jQuery(this).val() != "" ){
				 contCombo++;			 
			 }
		 });
	 });
	 
	 if( contValCombo > 0){
		 if( contCombo == 0 ){
			 aCombo.push(0);
		 }else{
			 aCombo.push(1); // existe valor
		 }
		 
		 var pos = aCombo.indexOf(1);
		 if( aCombo.indexOf(1) > -1 ){
			 return true;
		 }else{
			 return false;
		 }
	 }else{
		 return true;
	 }
	 
 }
 
 function validaRangos(){
	 //var aRango     = new Array();
	 var contRango 	  = 0;
	 var contValRango = 0;
	 
	 //VALIDA RANGOS
	 jQuery(".requeridoRango").each(function(index, value){
		 contValRango++;
		 if( jQuery(this).val() != "" ){
			 contRango++;			 
		 }
	 });	 
	 
	 if( contValRango > 0 ){
		 if( contRango > 0 ){
			 if( contValRango == contRango ){
				 return true;
			 }else{
				 return false;
			 }
		 }
		 
	 }else{
		 return true;
	 }
	 
 }

 // JCV 
 function validaCondicion(input1,input2,elemento,radio){
	 
	 // RANGOS
	 jQuery(input1).removeClass("requeridoRango");
	 jQuery(input2).removeClass("requeridoRango");
	 
	 jQuery(input1).removeAttr('disabled');
	 jQuery(input1).addClass("obligatorio");
	 jQuery(input1).addClass("validaComboValor");
	 jQuery(input1).attr("value","");
	 
	 jQuery(input2).attr('disabled','disabled');
	 jQuery(input2).removeClass("obligatorio");
	 jQuery(input2).attr("value","");
	 
	 jQuery(radio).attr('checked',false);
	 jQuery(elemento).addClass("validaCombo");
	 
	 if( jQuery(elemento).val() == "" ){
		 jQuery(elemento).removeClass("obligatorio");
		 jQuery(elemento).removeClass("validaCombo");
		 jQuery(input1).attr('disabled','disabled');
		 jQuery(input1).removeClass("obligatorio");
		 jQuery(input1).removeClass("validaComboValor");
		 jQuery(input2).removeClass("obligatorio");
	 }
	 
 }
 
 // JCV
 function validaRango(input1,input2,elemento,select){
	 
	 jQuery(input1).removeClass("validaComboValor");
	 jQuery(input2).removeClass("validaComboValor");
	 
	 if( jQuery(elemento).is(":checked") ){
		 
		 jQuery(input1).removeAttr('disabled');
		 jQuery(input1).addClass("requeridoRango");
		 
		 jQuery(input2).removeAttr('disabled');
		 jQuery(input2).addClass("requeridoRango");
		 
		 jQuery(select).attr("value","");
		 jQuery(select).removeClass("validaCombo");
	 }else{
		 jQuery(input1).attr('disabled','disabled');
		 jQuery(input1).removeClass("requeridoRango");
		 jQuery(input1).attr("value","");
		 
		 jQuery(input2).attr('disabled','disabled');
		 jQuery(input2).removeClass("requeridoRango");
		 jQuery(input2).attr("value","");
		 
		 jQuery(select).attr("value","");
		 jQuery(select).removeAttr("validaCombo");
	 }

 }

 // JCV
 function validaCheck(elemento){
	 
	 var i=0;
	 var tipo = "";
	 jQuery(".tipoServicio").each(function(index, value){
		 if( jQuery(this).is(":checked") ){
			 tipo  = jQuery(this).attr("title");
			 i++;
		 }
	 });
	 
	 if( i == 2 ){
		 jQuery("#hTipoServicio").attr("value",3);
	 }else if( i == 1 ){
		 if( tipo == "Publico" ){
			 jQuery("#hTipoServicio").attr("value",2);
		 }else if( tipo == "Privado" ){
			 jQuery("#hTipoServicio").attr("value",1);
		 }
	 }else if( i == 0){
		 jQuery("#hTipoServicio").attr("value",3);
	 }
	 
	 
	 
	 /////////////////////////////////////
	 if( jQuery(elemento).is(":checked") ){
		 jQuery(elemento).attr("value",1);
	 }else{
		 jQuery(elemento).attr("value",0);
	 }

 }
 
 function initBusquedaLiquidacionesProveedor()
 {
	 gridBusquedaLiquidacion = new dhtmlXGridObject('listadoGridLiquidaciones');
	 gridBusquedaLiquidacion.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	 gridBusquedaLiquidacion.setSkin("light");
	 gridBusquedaLiquidacion.setHeader("Oficina,No. Liquidación, Estatus, Tipo de Liquidación, Tipo de Operación, No. Proveedor, Proveedor, No. Siniestro, No. Factura, Solicitado por, Neto a Pagar, Fecha de Solicitud, Fecha de Autorización, Fecha de Elaboración Cheque/Transferencia, No. Solicitud de Cheque, No. Cheque/Referencia, Autorizado por");
	 gridBusquedaLiquidacion.setColumnIds("oficina,liquidacion,estatus,tipoLiquidacion,tipoOperacion, No. Proveedor, Proveedor,siniestro,factura,solicitadoPor,totalPagar,fechaSolicitud,fechaAutorizacion,fechaElab,noSolCheque,noCheque,autorizadaPor");
	 gridBusquedaLiquidacion.setInitWidths("65,120,135,140,150,90,180,80,80,180,100,120,100,155,135,135,180");
	 gridBusquedaLiquidacion.setColAlign("center,center,center,center,center,center,center,center,center");
	 gridBusquedaLiquidacion.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,edn,ro,ro,ro,ro,ro,ro");
	 gridBusquedaLiquidacion.init();
	 
	 
	 // ELIMINAR DEL COMBO TIPO DE OPERACION
	 jQuery("#estatusTipoOperaciones option[value='NA']").remove();   // ELIMINADA
	 
 }
 
 // JCV
 function initBusquedaLiquidacionesAutorizacionProveedor(){	
	 
	 // ELIMINAR DEL COMBO ESTATUS
	 jQuery("#estatusLiquidaciones option[value='ELIM']").remove();   // ELIMINADA
	 jQuery("#estatusLiquidaciones option[value='ENTRAM']").remove(); // EN TRAMITE
	 jQuery("#estatusLiquidaciones option[value='LIBSNSOL']").remove(); // EN TRAMITE
	 jQuery("#estatusLiquidaciones option[value='APLIC']").remove(); // APLIC
     jQuery("#estatusLiquidaciones option[value='CANC']").remove(); // CANC
	 jQuery("#estatusLiquidaciones option[value='TRMXMIZAR']").remove(); // TRMXMIZAR
	 jQuery("#estatusLiquidaciones option[value='TRMXRECH']").remove(); // TRAMITE POR RECHAZO
	 jQuery("#estatusLiquidaciones option[value='LIBCNSOL']").remove(); // LIBERADA CON SOLICITUD
	 
	 // ELIMINAR DEL COMBO TIPO DE OPERACION
	 jQuery("#estatusTipoOperaciones option[value='NA']").remove();   // ELIMINADA
	 
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 gridBusquedaLiquidacion = new dhtmlXGridObject('listadoGridLiquidaciones');	
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid_obj){blockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });	

	 jQuery("#divExcelBtn").show();
	 
	 gridBusquedaLiquidacion.load('/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/buscarLiquidacionAutorizacion.action');
 }

 // JCV
 
 function buscarLiquidacionesEnter(e,el) {
	    
	    if (e.keyCode == 13) {
	    	buscarLiquidaciones();
	    }
	}
 function buscarLiquidaciones(){
	 
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 gridBusquedaLiquidacion = new dhtmlXGridObject('listadoGridLiquidaciones');	
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid_obj){blockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 gridBusquedaLiquidacion.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 gridBusquedaLiquidacion.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });

	 jQuery("#divExcelBtn").show();
	 
	 if ( validarBusqueda() ){
		 removeCurrencyFormatOnTxtInput();
		 var form = jQuery("#buscarLiquidacionesForm").serialize();
		 gridBusquedaLiquidacion.load('/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/buscarLiquidacion.action?'+form );
	 }else{
		 mostrarMensajeInformativo('Escriba o capture al menos tres datos para realizar la búsqueda', '10');
	 }
	 
 }
 
 
 // JCV
 function exportarExcelBusquedaLiquidaciones(origen){
	 
	 removeCurrencyFormatOnTxtInput();
	 elemento = "#buscarLiquidacionesForm";
	 
	 if( origen == "autorizaciones"){
		elemento = "#buscarLiquidacionesAutorizacionForm";
	 }
	 
	 if ( validarBusqueda() ){
		 var form = jQuery(elemento).serialize();
		 var url="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/exportarBusquedaExcel.action?" + form;
		 window.open(url, "Liquidaciones");
	 }else{
		 mostrarMensajeInformativo('Se debe realizar previamente una búsqueda para hacer la importación del excel', '10');
	 }
 }
 // JCV
 function mostrarDatosBancariosLiquidaciones(){
	 
	 var idPrestador = jQuery("#idPrestadorServicio").val();
	 if( idPrestador != "" ){
		 var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarDatosBancariosLiquidaciones.action?idPrestador='+idPrestador;
		 mostrarVentanaModal("mostrar", 'Datos Bancarios', 200, 120, 850, 550, url,null);     
	 }else{
		 mostrarMensajeInformativo('Seleccione un tipo de proveedor y proveedor', '10');
	 }
	 
 }
 
 // JCV
 function initGridInformacionBancariaLiquidaciones(){
	 
	 var idPrestador = jQuery("#idPrestador").val() ;
	 
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 gridInformacionBancariaLiquidaciones = new dhtmlXGridObject('listadoInformacionBancariaGridLiquidaciones');	
	 gridInformacionBancariaLiquidaciones.attachEvent("onXLS", function(grid_obj){blockPage()});
	 gridInformacionBancariaLiquidaciones.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 gridInformacionBancariaLiquidaciones.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 gridInformacionBancariaLiquidaciones.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
 
	 gridInformacionBancariaLiquidaciones.load('/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/gridDatosBancariosLiquidaciones.action?idPrestador='+idPrestador);	 
 }
 
 function autorizarLiquidacion(){
	   var formParams = jQuery(document.impresionLiquidacionForm).serialize();
	   var idLiquidacion = jQuery('#idLiqSiniestro').val();
	   if(null==idLiquidacion || idLiquidacion==''){
		   mostrarMensajeInformativo('Debe existir numero de liquidacion para invocar esta accion ' , '20')
	   }else{
		   if(confirm("¿Desea autorizar la liquidacion? ")){
			sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/autorizarLiquidacion.action?'+ formParams+'&idLiqSiniestro='+idLiquidacion ,targetWorkArea,null);
	   }
} 
} 


function rechazarLiquidacion(){
	var formParams = jQuery(document.impresionLiquidacionForm).serialize();
	var idLiquidacion = jQuery('#idLiqSiniestro').val();
	   if(null==idLiquidacion || idLiquidacion==''){
		   mostrarMensajeInformativo('Debe existir numero de liquidacion para invocar esta accion ' , '20')
	   }else{
		   if(confirm("¿Desea rechazar la liquidacion? ")){
			sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/rechazarLiquidacion.action?'+ formParams+'&idLiqSiniestro='+idLiquidacion ,targetWorkArea,null);
	   }
	   }
		
	}

 
 
 
 function buscarOrdenesPagoRelacionadas()
 { 
     var tipoNCR;
	 //mostrarIndicadorCarga('indicadorPagosRelacionados');
	 var idLiquidacion = jQuery('#idLiquidacion').val();	 
	 
	 document.getElementById("pagosRelacionadosGrid").innerHTML = '';	
	 pagosRelacionadosGrid = new dhtmlXGridObject('pagosRelacionadosGrid');
	 
	 	pagosRelacionadosGrid.attachEvent("onXLE", function(grid){
			desplegarBotones();
	    });
	 	pagosRelacionadosGrid.attachHeader("&nbsp,#text_filter,#text_filter,&nbsp,&nbsp");
		pagosRelacionadosGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarOrdenesPagoAsociadas.action?liquidacionSiniestro.id=" + idLiquidacion);
	
 }
 
 function initGridCifrasPorPago()
 {
 	//var form = jQuery('#listadoCitasJuridicoForm').serialize();
	//mostrarIndicadorCarga('indicador');
 	document.getElementById("cifrasPagoGrid").innerHTML = '';	
 	cifrasPagoGridGrid = new dhtmlXGridObject('cifrasPagoGrid');
 		
 	cifrasPagoGridGrid.attachEvent("onXLS", function(grid){
 		//ocultarIndicadorCarga('indicador');
 		//cifrasPagoGridGrid.enableAutoHeight(true, "150");
     });
 	
 	cifrasPagoGridGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
 	cifrasPagoGridGrid.setSkin("light");
 	cifrasPagoGridGrid.setHeader("No. Orden Compra, Subtotal,Deducible,Descuentos, IVA, IVA RET, ISR, Total, Importe Salvamento");
 	cifrasPagoGridGrid.setColumnIds("numeroOrdenPago,subtotal,deducible,descuentos,iva,ivaRet,isr,netoPagar,importeSalvamento");
 	cifrasPagoGridGrid.setInitWidths("120,120,120,120,120,120,120,120,120");
 	cifrasPagoGridGrid.setColAlign("center,center,center,center,center,center,center,center,center");
 	cifrasPagoGridGrid.setColTypes("ron,ron,ron,ron,ron,ron,ron,ron,ron");
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",1);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",2);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",3);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",4);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",5);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",6);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",7);
 	cifrasPagoGridGrid.setNumberFormat("$0,000.00",8);
 	cifrasPagoGridGrid.attachHeader("#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,&nbsp");
 	cifrasPagoGridGrid.setColSorting("int,na,na,na,na,na,na,int,na")
 	cifrasPagoGridGrid.init();
 	cifrasPagoGridGrid.objBox.style.overflowX = "hidden";

 }

 function initGridOrdenesDeCompra()
 {

 	document.getElementById("cifrasPagoGrid").innerHTML = '';	
 	ordenesDeCompraGrid = new dhtmlXGridObject('cifrasPagoGrid');
 	var idLiquidacion = jQuery('#idLiquidacion').val();	 
	ordenesDeCompraGrid.attachHeader("#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,&nbsp");
	ordenesDeCompraGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarOrdenesDeCompra.action?liquidacionSiniestro.id=" + idLiquidacion);
	initCurrencyFormatOnTxtInput();

 }
 
 function initGridCifrasPorNotaCredito()
 {
 	//var form = jQuery('#listadoCitasJuridicoForm').serialize();
	//mostrarIndicadorCarga('indicador');
 	document.getElementById("cifrasNotaCreditoGrid").innerHTML = '';	
 	cifrasPagoNotaCreditoGrid = new dhtmlXGridObject('cifrasNotaCreditoGrid');
 	var idLiquidacion = jQuery('#idLiquidacion').val();	 
 	cifrasPagoNotaCreditoGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarCifrasRecuperaciones.action?liquidacionSiniestro.id=" + idLiquidacion);

 	
 	/*cifrasPagoNotaCreditoGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
 	cifrasPagoNotaCreditoGrid.setSkin("light");
 	cifrasPagoNotaCreditoGrid.setHeader("&nbsp,Tipo,Subtotal, IVA, IVA RET, ISR, Total");
 	cifrasPagoNotaCreditoGrid.setColumnIds("idFactura,tipo,subtotal,iva,ivaRet,isr,netoPagar");
 	cifrasPagoNotaCreditoGrid.setInitWidths("135,135,135,135,135,135,138");
 	cifrasPagoNotaCreditoGrid.setColAlign("center,center,center,center,center,center,center");
 	cifrasPagoNotaCreditoGrid.setColTypes("ro,ro,ron,ron,ron,ron,ron");
 	cifrasPagoNotaCreditoGrid.setColumnHidden(0,true);
 	cifrasPagoNotaCreditoGrid.setNumberFormat("$0,000.00",2);
 	cifrasPagoNotaCreditoGrid.setNumberFormat("$0,000.00",3);
 	cifrasPagoNotaCreditoGrid.setNumberFormat("$0,000.00",4);
 	cifrasPagoNotaCreditoGrid.setNumberFormat("$0,000.00",5);
 	cifrasPagoNotaCreditoGrid.setNumberFormat("$0,000.00",6);
 	cifrasPagoNotaCreditoGrid.init();
 	cifrasPagoNotaCreditoGrid.objBox.style.overflowX = "hidden";*/


 }

 /*
  
 function initGridNotasCreditoAsociadas() //DEPRECATED
 {
 	document.getElementById("notasCreditoRelacionadasGrid").innerHTML = '';	
 	notasCreditoAsociadasGrid = new dhtmlXGridObject('notasCreditoRelacionadasGrid');
 		
 	notasCreditoAsociadasGrid.attachEvent("onXLS", function(grid){
 		//ocultarIndicadorCarga('indicador');
 		//cifrasPagoGridGrid.enableAutoHeight(true, "150");
     }); 
 	
 	notasCreditoAsociadasGrid.attachEvent("onSubGridCreated",function(sub,id,ind,value){			
		 
		 sub.callEvent("onGridReconstructed",[])
		     sub.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleNotaCredito.action?idFactura=" + id 
		    		 + "&tipoElemento=" + notasCreditoAsociadasGrid.cellById(id,3).getValue(),function(){
	    	 sub.callEvent("onGridReconstructed",[]);
	    	 sub.setSizes();
	    	 notasCreditoAsociadasGrid.setSizes();	 	    	 
	    	 blockPage();
	    	 //actualizarImportesPorNotaCredito(sub,OP_ASOCIAR); 
	    	 asociarDesasociarNotasCreditoRecuperacion(sub, id, OP_ASOCIAR, notasCreditoAsociadasGrid, notasCreditoPorRelacionarGrid);
         });
	  
	     return false;  
	 });
 	
 	if(jQuery('#soloLectura').val() == 'true')
	{
 		notasCreditoAsociadasGrid.enableDragAndDrop(false);		 
	}else
	{
		notasCreditoAsociadasGrid.enableDragAndDrop(true);			 
	}	
 	
 	notasCreditoAsociadasGrid.attachEvent("onBeforeDrag",function(id){
	  if (notasCreditoAsociadasGrid.cellById(id,3).getValue() == "FAC") //Si se trata de una factura entonces no será posible mover el elemento.  
	  {
		 return false; 
		  
	  }else
	  {
		 return true;  
	  }
	                                         
	});
 	
 	notasCreditoAsociadasGrid.attachEvent("onDragIn",function(sid,tid){
 		if(notasCreditoPorAsociar)
 		  return true;                                       
 		});
 	
 	notasCreditoAsociadasGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
 	notasCreditoAsociadasGrid.setSkin("light");
 	notasCreditoAsociadasGrid.setHeader("&nbsp,&nbsp,Numero,&nbsp,Tipo,Total");
 	notasCreditoAsociadasGrid.setColumnIds("detalle,tieneNotasCredito,numero,tipo,descripcion,total");
 	notasCreditoAsociadasGrid.setInitWidths("20,20,150,20,150,*");
 	notasCreditoAsociadasGrid.setColAlign("center,left,left,center,center,center");
 	notasCreditoAsociadasGrid.setColTypes("sub_row_grid,ro,ron,ron,ron,ron");
 	notasCreditoAsociadasGrid.setColumnHidden(1,true);
 	notasCreditoAsociadasGrid.setColumnHidden(3,true);
 	notasCreditoAsociadasGrid.setNumberFormat("$0,000.00",5);
 	notasCreditoAsociadasGrid.setColSorting("int,int,int,int,int,int"); 
 	notasCreditoAsociadasGrid.attachHeader("&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,#text_filter");
 	notasCreditoAsociadasGrid.init();
 	//notasCreditoAsociadasGrid.objBox.style.overflowX = "hidden";

 }
*/


 /*
 function doOnCheckPagosDisponibles(){
 	  jQuery("#pagosDisponiblesConcat").val(pagosPorRelacionarGrid.getCheckedRows(0));
      blockPage();
      unblockPage();
      return true;
 }*/
 
 function buscarOrdenesPagoDisponibles()
 {
	var idProveedor = jQuery('#idPrestadorServicio').val();	
 	//mostrarIndicadorCarga('indicadorPagosDisponibles');

 	document.getElementById("pagosDisponibles").innerHTML = '';	
 	pagosPorRelacionarGrid = new dhtmlXGridObject('pagosDisponibles');

 	//pagosPorRelacionarGrid.attachEvent("onCheckbox",doOnCheckPagosDisponibles);
 	
 	/*if(jQuery('#soloLectura').val() == 'true')
	 {
 		pagosPorRelacionarGrid.enableDragAndDrop(false);		 
	 }else
	 {
		 pagosPorRelacionarGrid.enableDragAndDrop(true);			 
	 }*/
 	
 	
 	
 	pagosPorRelacionarGrid.attachEvent("onXLE", function(grid){
 		ocultarIndicadorCarga('indicadorPagosDisponibles');
   	    // MOSTRAR SUBGRID ABIERTO AL CARGAR EL JSP
 		/*pagosPorRelacionarGrid.forEachRow(function(id){
 			pagosPorRelacionarGrid.cells(id,0).open();
	 	});*/ 
     });
 	
 	/*
 	pagosPorRelacionarGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
 		 blockPage(); 		
 		 pagosPorRelacionarGrid.cells(sId,0).open();
 		 pagosPorRelacionarGrid.cells(sId,0).close(); 		 
 		 
 		 if(sObj != tObj)
		 {
 			requiereAsociarDesasociar.push(1);	
 			requiereAsociarDesasociarNCRs.push(1);
		 }
		 	
		});
 	
 	pagosPorRelacionarGrid.attachEvent("onRowAdded", function(rId){ //Para funcionalidad asociar/desasociar todas las facturas
 		   if(banderaAsociandoTodas)
 		   {  
 			  pagosPorRelacionarGrid.cells(rId,0).open();
 			  pagosPorRelacionarGrid.cells(rId,0).close();
 			  
 			  requiereAsociarDesasociar.push(1);	
 			  requiereAsociarDesasociarNCRs.push(1);
 		   }
		   			 	
		});
 	
 	pagosPorRelacionarGrid.attachEvent("onDragIn",function(sid,tid){
		  
 		if(pagosRelacionadosGrid.doesRowExist(sid))
 		{	 				
 			return true;
 		}
 		
 		  return false;                                       
 		});*/

 	/*
 	pagosPorRelacionarGrid.attachEvent("onSubGridCreated",function(sub,id,ind,value){
 		 
		     //sub.callEvent("onGridReconstructed",[])
		     sub.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleFactura.action?idFactura=" 
		    		 + id + "&tipoElemento=FAC",function(){
	    	 sub.callEvent("onGridReconstructed",[]);
	    	 sub.setSizes();
	    	 pagosPorRelacionarGrid.setSizes();
	    	 
	    	 asociarDesasociarOrdenesPago(sub, id, OP_DESASOCIAR, pagosPorRelacionarGrid, pagosRelacionadosGrid);  
	    	 
	    	 
         });
		 
	     return false;  
	 });*/
	 	 
 	
	 pagosPorRelacionarGrid.attachHeader("&nbsp,#text_filter,#text_filter,&nbsp,&nbsp");
	 
	 pagosPorRelacionarGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarListadoOrdenPago.action?liquidacionSiniestro.proveedor.id=" + idProveedor);		
 	
 	//pagosPorRelacionarGrid.objBox.style.overflowX = "hidden";
 	
 } 
 
//-------- Inicia funcionalidad Autcomplete---------

 function findItem (term, devices) {
 	
 	var items = [];
     for (var i=0;i<devices.length;i++) {
         var item = devices[i];
         for (var prop in item) {
             var detail = item[prop].toString().toLowerCase();           
             if (detail.indexOf(term)>-1) {
                 items.push(item);
                 break;               
             }
         }
     }
     return items;
 }

 jquery143(function(){
	 if (typeof listaPrestadoresServicio != 'undefined')
	 {
		 jquery143("#nombrePrestadorServicio").autocomplete({
         source: listaPrestadoresServicio.size() > 0 ? listaPrestadoresServicio : function(request, response){
         	         	
         },
         minLength: 3,
         delay: 500,
         select: function(event, ui) {
	        	 jquery143("#idPrestadorServicio").val(ui.item.id);         	
	        	obtenerTipoLiquidacionProveedor(ui.item.id);
	        	guardarLiquidacionAutomatico('A');
	         	buscarOrdenesPagoDisponibles(); 
	         	buscarOrdenesPagoRelacionadas();
	         	initGridRecuperaciones();
	         	//buscarNotasCreditoDisponibles();
	         	desplegarBotones();
         },
         search: function(event, ui) {
         	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
             var busqueda = jQuery("#nombrePrestadorServicio").val();
              
             if(findItem(busqueda.toString().toLowerCase(), listaPrestadoresServicio).length == 0){
	             	mostrarMensajeInformativo(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe o no cuenta con ordenes de pago pendientes de relacionar"), '20');
             	jQuery("#nombrePrestadorServicio").val("");
             	return false;
             }
         }
     });
	 } 	
 });

 jquery143(function(){
	 jquery143("#nombrePrestadorServicio").bind("keypress", function(e) {
 	    if (e.keyCode == 13) {
 	        e.preventDefault();         
 	        return false;
 	    }
 	});
 });

 //-------- termina funcionalidad Autocomplete

/* 
function asociarDesasociarOrdenesPago(subGrid, rowId, operacion, targetGrid, sourceGrid)
{
	if(requiereAsociarDesasociar.length > 0)//if(requiereAsociarDesasociar==1)
	{
		var url;
		//var listaOrdenesPago = "";
		var contador = 0; 

		
		
		
		if(operacion == OP_ASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/asociarOrdenPago.action?liquidacionSiniestro.id='+ 
					jQuery('#idLiquidacion').val() + '&factura.id=' +  rowId;
			
		}else if(operacion == OP_DESASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/desasociarOrdenPago.action?liquidacionSiniestro.id='+ 
					jQuery('#idLiquidacion').val() + '&factura.id=' +  rowId;
					
		}
		
		var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
 					manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, JSON.parse(data).mensaje); 
 					
 					if(operacion == OP_ASOCIAR) //Regresa las notas de credito a su estado original
 					{
 						asociarDesasociarNotasCredito(pagosPorRelacionarGrid, rowId, OP_DESASOCIAR);
 						
 						cifrasPagoNotaCreditoGrid.forEachRow(function(id) { //Remover Notas de Credito en caso de que la factura tenga.	 
 							
 							if(cifrasPagoNotaCreditoGrid.cellById(id,0).getValue() == rowId)
 							{
 								cifrasPagoNotaCreditoGrid.deleteRow(id);					
 							}												
 						});	 						
 					}
 					
 				}else
 				{
 					procesarNotasCredito(targetGrid, rowId, operacion); 					
 					actualizarImportesPorPago(subGrid,operacion);
 					actualizarImportesLiquidacion(JSON.parse(data));
 					
 					requiereRecalculo--;
 					if(requiereRecalculo == 0)
 					{
 						var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/recalcularLiquidacion.action?liquidacionSiniestro.id='+ 
 	 					jQuery('#idLiquidacion').val();
 	 					
 	 					var ajaxresults = jQuery.ajax({
 	 						"url" : url,
 	 						"async": false,
 	 						"dataType" : "text json",
 	 						"success": function(data) {
 	 							
 	 							if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 	 			 				{
 	 								mostrarMensajeInformativo(JSON.parse(data).mensaje, '20');	
 	 			 					
 	 			 				}else
 	 			 				{ 					
 	 			 					actualizarImportesLiquidacion(JSON.parse(data)); 					 										
 	 			 				}				
 	 				        }
 	 					});
 						
 	 					contElmentos = -1;
 					}
 						
 				}				
	        }
		});	
		
		requiereAsociarDesasociar.pop();
	}
	unblockPage();
}
*/

/*
function procesarNotasCredito(targetGrid, rowId, operacion)
{
	blockPage();
	if(operacion == OP_ASOCIAR)
	{
		asociarDesasociarNotasCredito(targetGrid, rowId, operacion);					
			
	}else
	{
		
		asociarDesasociarNotasCredito(targetGrid, rowId, operacion); 						
	}
	
	unblockPage();
}*/

/*function asociarDesasociarNotasCredito(grid, rowId, operacion)
{	blockPage();
	if(requiereAsociarDesasociarNCRs.length > 0)
	{		
		if(operacion == OP_ASOCIAR)
		{	
			if(grid.cellById(rowId,1).getValue() != "" && grid.cellById(rowId,1).getValue() != "0")//tiene notas de credito
			{	
				notasCreditoAsociadasGrid.addRow(rowId,"&nbsp,"+ grid.cellById(rowId,1).getValue() + "," 
						+ grid.cellById(rowId,2).getValue() + "," + "FAC" + "," 
						+ "Factura" + "," + grid.cellById(rowId,3).getValue());				//detalle,tieneNotasCredito,numero,tipo,descripcion,total
				
				notasCreditoAsociadasGrid.cells(rowId,0).open();
				notasCreditoAsociadasGrid.cells(rowId,0).close();
				
			}
			
		}else if(operacion == OP_DESASOCIAR)
		{	
			notasCreditoAsociadasGrid.deleteRow(rowId);			
		}
		
		requiereAsociarDesasociarNCRs.pop();		
	}	
	unblockPage();
}*/

function actualizarImportesLiquidacion(liquidacionSiniestroJson)
{	 blockPage();

	

	 jQuery('#subtotal').val(liquidacionSiniestroJson.liquidacionSiniestro.subtotal);
	 jQuery('#deducible').val(liquidacionSiniestroJson.liquidacionSiniestro.deducible);
	 jQuery('#descuentos').val(liquidacionSiniestroJson.liquidacionSiniestro.descuento);
	 jQuery('#iva').val(liquidacionSiniestroJson.liquidacionSiniestro.iva);
	 jQuery('#ivaRetenido').val(liquidacionSiniestroJson.liquidacionSiniestro.ivaRet);
	 jQuery('#isr').val(liquidacionSiniestroJson.liquidacionSiniestro.isr);
	 jQuery('#total').val(liquidacionSiniestroJson.liquidacionSiniestro.netoPorPagar);
	 jQuery('#salvamento').val(liquidacionSiniestroJson.liquidacionSiniestro.importeSalvamento);
	 
	 initCurrencyFormatOnTxtInput();
	 unblockPage();
}

/*
function actualizarImportesPorPago(grid, operacion)
{	
	if(operacion == OP_ASOCIAR){
		
		var tamColumnas = grid.getColumnsNum(); // 11 NO INCLUYE GRID 'SINIESTRO_TERCERO' - 12 SI INCLUYE GRID 'SINIESTRO_TERCERO'
		
		grid.forEachRow(function(id) {  
			
			var colIndex = 3;
			var montosOrdenCompra= '';
			var concepto = '';
			//concatena el numero de orden de pago
			concepto = grid.cellById(id,0).getValue() == ''?'0' : grid.cellById(id,0).getValue();	
			montosOrdenCompra = montosOrdenCompra.concat(concepto + ',');
			//concatena los montos de los diferentes conceptos:subtotal,deducible,descuentos,iva,ivaRet,isr,total,salvamento
			
			while(colIndex < grid.getColumnsNum())
			{		
				// SI EL OBJETO VIENE CON 'SINIESTRO TERCERO' SE SALTA UNA POSICION PARA BRINCARSE EL NUMERO DE SINIESTRO
				if( tamColumnas == 12 && colIndex == 3 ){
					colIndex ++;
				}
				concepto = grid.cellById(id,colIndex).getValue() == ''?'0' : grid.cellById(id,colIndex).getValue();
				montosOrdenCompra = montosOrdenCompra.concat(concepto + ',');
				colIndex++;
			}
			
			cifrasPagoGridGrid.addRow(id,montosOrdenCompra.substring(0, montosOrdenCompra.length - 1)); //Reemplazar con los valores de la factura				
		});			
		
	}else if(operacion == OP_DESASOCIAR)
	{
		grid.forEachRow(function(id) {  			
				cifrasPagoGridGrid.deleteRow(id);			
		});		
	}
	
	this.habilitarDeshabilitarProveedor();
	this.desplegarBotones();
	ocultarIndicadorCarga("indicador");
	unblockPage();
	
}*/

function actualizarImportesPorNotaCredito(grid, operacion, tipo)
{	
	var tipoVal = ',' + tipo + ',';
	
	if(operacion == OP_ASOCIAR)
	{
		grid.forEachRow(function(id) {  
			
			var colIndex = 1;
			var montosOrdenCompra= '';
			
			while(colIndex < grid.getColumnsNum())
			{				
				var concepto = grid.cellById(id,colIndex).getValue() == ''?'0' : grid.cellById(id,colIndex).getValue();
				montosOrdenCompra = montosOrdenCompra.concat(concepto + ',');
				colIndex++;
			}
			montosOrdenCompra = montosOrdenCompra.replace(',', tipoVal);
			cifrasPagoNotaCreditoGrid.addRow(id,montosOrdenCompra.substring(0, montosOrdenCompra.length - 1)); //Reemplazar con los valores de la factura				
		});			
		
	}else if(operacion == OP_DESASOCIAR)
	{	
		grid.forEachRow(function(id) { 
			
			cifrasPagoNotaCreditoGrid.deleteRow(id);			
		});		
	}
	this.habilitarDeshabilitarProveedor();
	this.desplegarBotones();
	ocultarIndicadorCarga("indicador");
	unblockPage();
}

function guardarLiquidacion(estatus)
{
	var form = jQuery('#definirLiquidacionForm').serialize(); //TODO Revisar si se debe enviar la forma completa

	sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/guardarLiquidacion.action?' + form,targetWorkArea,null);	
}

function guardarLiquidacionAutomatico(origen)
{
	//activar algo que indique que esta guardando
	
	var form = jQuery('#definirLiquidacionForm').serialize();
	var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/guardarLiquidacionAutomatico.action?' + form;	
	
	var ajaxresults = jQuery.ajax({
		"url" : url,
		"async": false,
		"dataType" : "text json",
		"success": function(data) {
			
			//TODO informar que se guardo
        }
	});	
}

function eliminarLiquidacion(idLiquidacion)
{
	var form = jQuery('#definirLiquidacionForm').serialize();

	sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/eliminarLiquidacion.action?liquidacionSiniestro.id='+ 
			idLiquidacion,targetWorkArea,null);	
}

function limpiarProveedorLiq()
{
	jQuery('#nombrePrestadorServicio').val(''); 
	jQuery('#idPrestadorServicio').val('');
}

function desplegarBotones(){

	//Por default seleccionar la primera opcion disponible si no ha sido seleecionada ninguna.
	if(jQuery("#centroOperacional").val() == "")
	{		
		jQuery("#centroOperacional option:nth-child(2)").attr("selected", true);		
	}		
	
	//--Boton Datos Cuenta Bancaria	
	if(jQuery("#proveedorTieneInfoBancaria").val() == "true" && jQuery("#listaTipoLiquidaciones").val() == "TRNBANC"){

		jQuery("#botonDatosCtaBancaria").show();
	}
	else
	{
		jQuery("#botonDatosCtaBancaria").hide();		
	}	
	
	//-- Boton solicitar Autorizacion
	if((jQuery('#estatusLiquidacion').val() == 'ENTRAM' || 
			jQuery('#estatusLiquidacion').val() == 'TRMXRECH' || 
			jQuery('#estatusLiquidacion').val() == 'TRMXMIZAR') && 
			(pagosRelacionadosGrid.getRowsNum() > 0 || notasCreditoAsociadasGrid.getRowsNum() > 0) )
	{
		jQuery('#botonSolAutorizacion').show();
		
	}else
	{
		jQuery('#botonSolAutorizacion').hide();		
	}
	
	//-- Boton imprimir
	if(pagosRelacionadosGrid.getRowsNum() > 0 || notasCreditoAsociadasGrid.getRowsNum() > 0)
	{
		jQuery('#botonImprimir').show();
		
	}else
	{
		jQuery('#botonImprimir').hide();		
	}
	
	//-- Boton Reexpedicion
	if(jQuery('#estatusLiquidacion').val() == 'LIBSNSOL')
	{
		jQuery('#botonReexpedir').show();
		
	}else
	{
		jQuery('#botonReexpedir').hide();		
	}
	
	if(jQuery('#estatusLiquidacion').val() == 'TRMXRECH' ||
			jQuery('#estatusLiquidacion').val() == 'TRMXMIZAR' ||
			jQuery('#estatusLiquidacion').val() == 'LIBCNSOL' ||
			jQuery('#estatusLiquidacion').val() == 'LIBSNSOL')
	{
		jQuery('#botonCancelar').show();
		
	}else
	{
		jQuery('#botonCancelar').hide();		
	}	
	
	//-- Boton Eliminar
	if((pagosRelacionadosGrid.getRowsNum() == 0 && notasCreditoAsociadasGrid.getRowsNum() == 0) 
			&& (jQuery('#estatusLiquidacion').val() == 'ENTRAM' || jQuery('#estatusLiquidacion').val() == 'TRMXRECH' ||
			jQuery('#estatusLiquidacion').val() == 'XAUT'))
	{
		jQuery('#botonEliminar').show();
		
	}else
	{
		jQuery('#botonEliminar').hide();		
	}
	
	if(jQuery("#soloLectura").val() == "true"){
		jQuery(".esconder").hide();
	}	
}

function solicitarAutorizacionLiq(idLiquidacion){
	
	sendRequestJQ(null, '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/solicitarAutorizacion.action?liquidacionSiniestro.id='+ 
			idLiquidacion+ '&soloLectura=' + jQuery('#soloLectura').val() + "&pantallaOrigen=" + jQuery('#pantallaOrigen').val(),targetWorkArea,null);	
}

function habilitarDeshabilitarProveedor()
{
	//Se deshabilita el proveedor en caso de existir una orden de pago asociada.
	if(jQuery('#soloLectura').val() == 'true' || 
			pagosRelacionadosGrid.getRowsNum() > 0 || notasCreditoAsociadasGrid.getRowsNum() > 0)
	{
		jQuery("#iconoBorrar").hide(); 
		jQuery("#nombrePrestadorServicio").attr("disabled",true); 	
	}
	else
	{
		jQuery("#iconoBorrar").show(); 
		jQuery("#nombrePrestadorServicio").attr("disabled",false);		
	}	
}

function mostrarCancelarLiquidacion(idLiquidacion) {
	var url="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarCancelarLiquidacion.action?idLiqSiniestro="+idLiquidacion;
	mostrarVentanaModal("mostrarCancelarLiquidacion", 'Cancelación de Liquidación', 100, 200, 700, 300, url);
}

function cerrarVentanaCancelacionLiq(redirect){
	if (redirect) {
		parent.cerrarVentanaModal('mostrarCancelarLiquidacion', "redirectCancelacion()");
	} else {
		parent.cerrarVentanaModal('mostrarCancelarLiquidacion', null);
	}
	
}

function confirmarCancelacion() {
	var nextFunction = 'enviarCancelacion()';
	parent.mostrarMensajeConfirm('¿Está seguro que desea cancelar la liquidación?', '20', 
			nextFunction, null, null)
}

function enviarCancelacion(){
	var parentFrame = parent.getWindowContainerFrame('mostrarCancelarLiquidacion');
	var idLiquidacion = parentFrame.contentWindow.document.getElementById('idLiquidacion').value;
	
	var radios = parentFrame.contentWindow.document.getElementsByName('tipoCancelacion');
	var tipoCancelacion;

	for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[i].checked) {
	    	tipoCancelacion = radios[i].value;
	        break;
	    }
	}
		
	var url="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/cancelacionLiquidacion.action?idLiqSiniestro="
				+idLiquidacion+"&tipoCancelacion="+tipoCancelacion;
	
	parent.redirectVentanaModal('mostrarCancelarLiquidacion', url, null);
}

function redirectCancelacion() {	
	
	sendRequestJQ(null,'/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/mostrarBusquedaLiquidacionProveedor.action?mensaje=Se ha enviado la Cancelacion&tipoMensaje=30',targetWorkArea,null);
}

function obtenerTipoLiquidacionProveedor(idProveedor)
{
	var url;
	var listaTiposLiq;
	var tipoLiquidacionRegistrado="";
	var opcionTransferenciaDefault = false;
	
	url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/obtenerTipoLiquidacionProveedor.action?liquidacionSiniestro.proveedor.id=" 
			+ idProveedor + '&liquidacionSiniestro.tipoLiquidacion=' + dwr.util.getValue("listaTipoLiquidaciones");
	
	var ajaxresults = jQuery.ajax({
		"url" : url,
		"async":false,
		"dataType" : "text json",
		"success": function(data) {
			listaTiposLiq = (JSON.parse(data)).listaTiposLiquidacion;			
			tipoLiquidacionRegistrado = (JSON.parse(data)).liquidacionSiniestro.tipoLiquidacion; 
			
			jQuery('#proveedorTieneInfoBancaria').val((JSON.parse(data)).proveedorTieneInfoBancaria);	
			
			jQuery('#listaTipoLiquidaciones')
		    .find('option')
		    .remove()
		    .end()
		    .append('<option value="">Seleccione ...</option>')
		    .val('');			

			for (var prop in listaTiposLiq) 
			{		       
				if(prop == 'TRNBANC')
				{
					opcionTransferenciaDefault = true;					
				}
				
				jQuery("#listaTipoLiquidaciones").append('<option value="'+prop+'">'+listaTiposLiq[prop]+'</option>');				
		    }
			
			if(opcionTransferenciaDefault && tipoLiquidacionRegistrado == "")//Seleccionar por default si aun no ha sido seleccionado
			{
				jQuery("#listaTipoLiquidaciones option[value='TRNBANC']").attr("selected",true);
			}else
			{
				jQuery("#listaTipoLiquidaciones option[value='"+tipoLiquidacionRegistrado+"']").attr("selected",true);									
			}
        }
	});		
}
  
/*
function asociarDesasociarTodas(gridOrigen, gridDestino)
{
	if(jQuery('#soloLectura').val() == 'false')
	{
		banderaAsociandoTodas = true;
		requiereRecalculo = 0;
		blockPage();
		
		gridOrigen.forEachRow(function(id) {  
			
			requiereRecalculo ++;	
			gridOrigen.moveRowTo(id,"","move","sibling",gridOrigen,gridDestino);
					
		});
		
		unblockPage();
		banderaAsociandoTodas = false;		
	}	
}*/

function asociarDesasociarSeleccionadas(gridOrigen, gridDestino,operacion)
{
	
	var checked=gridOrigen.getCheckedRows(0);
	var tieneNotaDeCreadito = false;
	if(checked == ''){
		alert('Debe seleccionar al menos un elemento');
	}else{
		blockPage();
		var idsArray = checked.split(',');
		var arrayLength = idsArray.length;
		for (var i = 0; i < arrayLength; i++) {
		    gridOrigen.moveRowTo(idsArray[i],"","move","sibling",gridOrigen,gridDestino);	
		    /*
		    if(gridDestino.cellById(idsArray[i],1).getValue() != "" && gridDestino.cellById(idsArray[i],1).getValue() != "0"){
		    	tieneNotaDeCreadito = true;
		    	alert("Tiene Nota");
		    }*/
		}

		var url;
		if(operacion == OP_ASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/asociarOrdenPago.action?liquidacionSiniestro.id='+jQuery('#idLiquidacion').val()+'&liquidacionSiniestro.facturasConcat='+checked; 
			//url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/asociarOrdenPago.action?liquidacionSiniestro.id='+ 

		}else if(operacion == OP_DESASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/desasociarOrdenPago.action?liquidacionSiniestro.id='+jQuery('#idLiquidacion').val()+'&liquidacionSiniestro.facturasConcat='+checked; 
		}
		
		var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
 					mostrarMensajeInformativo(JSON.parse(data).mensaje, '20');	
 					
 					/*if(operacion == OP_ASOCIAR) //Regresa las notas de credito a su estado original
 					{
 						asociarDesasociarNotasCredito(pagosPorRelacionarGrid, rowId, OP_DESASOCIAR);
 						
 						cifrasPagoNotaCreditoGrid.forEachRow(function(id) { //Remover Notas de Credito en caso de que la factura tenga.	 
 							
 							if(cifrasPagoNotaCreditoGrid.cellById(id,0).getValue() == rowId)
 							{
 								cifrasPagoNotaCreditoGrid.deleteRow(id);					
 							}												
 						});	 						
 					}*/
 					
 				}else
 				{
 					//procesarNotasCredito(gridDestino, rowId, operacion); 					
 					//actualizarImportesPorPago(subGrid,operacion);
 					//actualizarImportesLiquidacion(JSON.parse(data));
 					
					var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/recalcularLiquidacion.action?liquidacionSiniestro.id='+ jQuery('#idLiquidacion').val();
 					var ajaxresults = jQuery.ajax({
 						"url" : url,
 						"async": false,
 						"dataType" : "text json",
 						"success": function(data) {
 							
 							if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR){
 								mostrarMensajeInformativo(JSON.parse(data).mensaje, '20');	
 			 					
 			 				}else{
 			 									
 			 					actualizarImportesLiquidacion(JSON.parse(data)); 	
 			 					initGridOrdenesDeCompra();
 			 					initGridRecuperaciones();
 			 					initGridNotasCreditoAsociadas();
 			 					desplegarBotones();
 			 					/*
 			 					pagosPorRelacionarGrid.forEachRow(function(id){
									});*/
 			 					//alert("tieneNotaDeCreadito : "+tieneNotaDeCreadito);				 										
 			 				}				
 				        }
 					});
 				}				
	        }
		});	

		gridDestino.uncheckAll();
		unblockPage();
	}
}


function calculaLiquidacion(){
	var url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/recalcularLiquidacion.action?liquidacionSiniestro.id='+ jQuery('#idLiquidacion').val();
	
 					var ajaxresults = jQuery.ajax({
 						"url" : url,
 						"async": false,
 						"dataType" : "text json",
 						"success": function(data) {
 							
 							if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR){
								mostrarMensajeInformativo(JSON.parse(data).mensaje, '20');	
 			 				}else{ 					
 			 					actualizarImportesLiquidacion(JSON.parse(data)); 	
 			 					
 			 				}				
 				        }
 					});
}

function initGridNotasCreditoAsociadas() {

	var idLiquidacion = jQuery('#idLiquidacion').val();
 	document.getElementById("notasCreditoRelacionadasGrid").innerHTML = '';	
 	notasCreditoAsociadasGrid = new dhtmlXGridObject('notasCreditoRelacionadasGrid');
 	notasCreditoAsociadasGrid.attachEvent("onXLE", function(grid){
		desactivaNotas();
		desplegarBotones();
	});


 	notasCreditoAsociadasGrid.attachHeader("&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,#text_filter,&nbsp");
	notasCreditoAsociadasGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verContenedorNotaCredito.action?liquidacionSiniestro.id=" + idLiquidacion);	

}

 function desactivaNotas(){
 	var tipo;

 	notasCreditoAsociadasGrid.forEachRow(function(id) {
		//alert(id);
		if(notasCreditoAsociadasGrid.cells(id,4).getValue() == 'Factura'){
			notasCreditoAsociadasGrid.cells(id,0).setDisabled(true);	
		}
	});
 }


 




 function initGridRecuperaciones() {

 	var idProveedor = jQuery('#idPrestadorServicio').val();	
	var idLiquidacion = jQuery('#idLiquidacion').val();
 	document.getElementById("notasCreditoDisponiblesGrid").innerHTML = '';	
 	recuperacionesPendientesGrid = new dhtmlXGridObject('notasCreditoDisponiblesGrid');
 	recuperacionesPendientesGrid.attachHeader("&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,#text_filter,&nbsp");
 	recuperacionesPendientesGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verRecuperacionesPendientes.action?liquidacionSiniestro.proveedor.id=" + idProveedor);		
 		
 }


function manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, mensajeError)
{
	 targetGrid.moveRowTo(rowId,"","move","sibling",targetGrid,sourceGrid);
	 mostrarMensajeInformativo(mensajeError, '20');	
}


/*
function buscarNotasCreditoDisponibles()
{
	var idProveedor = jQuery('#idPrestadorServicio').val();	
	
	//mostrarIndicadorCarga('indicadorPagosDisponibles');

	document.getElementById("notasCreditoDisponiblesGrid").innerHTML = '';	
	notasCreditoPorRelacionarGrid = new dhtmlXGridObject('notasCreditoDisponiblesGrid');
	
	if(jQuery('#soloLectura').val() == 'true')
	 {
		notasCreditoPorRelacionarGrid.enableDragAndDrop(false);		 
	 }else
	 {
		 notasCreditoPorRelacionarGrid.enableDragAndDrop(true);			 
	 }	
	
	notasCreditoPorRelacionarGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorNotasDisponibles'); 	
		
    });
	
	notasCreditoPorRelacionarGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
		 blockPage();
		 notasCreditoPorRelacionarGrid.cells(sId,0).open();
		 notasCreditoPorRelacionarGrid.cells(sId,0).close();
		 
		 if(sObj != tObj)
		 {
			 requiereAsociarDesasociarNCRsRec.push(1);
		 }
		 	
		});
	
	notasCreditoPorRelacionarGrid.attachEvent("onRowAdded", function(rId){ //Para funcionalidad asociar/desasociar todas las facturas
		if(banderaAsociandoTodas)
		   {  
			   notasCreditoPorRelacionarGrid.cells(rId,0).open();
			   notasCreditoPorRelacionarGrid.cells(rId,0).close();
			  
			   requiereAsociarDesasociarNCRsRec.push(1);	
		   }
		   			 	
		});
	
	notasCreditoPorRelacionarGrid.attachEvent("onDragIn",function(sid,tid){
		  
 		if(notasCreditoAsociadasGrid.doesRowExist(sid))
 		{	 				
 			return true;
 		}
 		
 		  return false;                                       
 		});

	notasCreditoPorRelacionarGrid.attachEvent("onSubGridCreated",function(sub,id,ind,value){
				
		 sub.callEvent("onGridReconstructed",[])
		     sub.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleNotaCredito.action?idFactura=" + id+ "&tipoElemento=REC",function(){
	    	 sub.callEvent("onGridReconstructed",[]);
	    	 sub.setSizes();
	    	 notasCreditoPorRelacionarGrid.setSizes();
	    	 
	    	 asociarDesasociarNotasCreditoRecuperacion(sub, id, OP_DESASOCIAR, notasCreditoPorRelacionarGrid, notasCreditoAsociadasGrid);
	    	 
	    	 
        });
	  
	     return false;  
	 })
	
	 notasCreditoPorRelacionarGrid.attachHeader("&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,#text_filter");
	 
	notasCreditoPorRelacionarGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/obtenerRecuperacionesPendientesAsignar.action?liquidacionSiniestro.proveedor.id=" 
	 			+ idProveedor);		
	
	//notasCreditoPorRelacionarGrid.objBox.style.overflowX = "hidden";
	
} */


/*
function buscarNotasCreditoRelacionadas()
{
	var tipoNCR;
	 //mostrarIndicadorCarga('indicadorPagosRelacionados');
	 var idLiquidacion = jQuery('#idLiquidacion').val();	 
	 
	 document.getElementById("notasCreditoRelacionadasGrid").innerHTML = '';	
	 notasCreditoAsociadasGrid = new dhtmlXGridObject('notasCreditoRelacionadasGrid');
	 
	 if(jQuery('#soloLectura').val() == 'true')
	 {
		 notasCreditoAsociadasGrid.enableDragAndDrop(false);			 
	 }else
	 {
		 notasCreditoAsociadasGrid.enableDragAndDrop(true);			 
	 } 
	 
	 
	 notasCreditoAsociadasGrid.attachEvent("onXLE", function(grid){
	 		//ocultarIndicadorCarga('indicadorPagosRelacionados');

		 		//Cargar los registros existentes al grid cifras por pago
		 notasCreditoAsociadasGrid.forEachRow(function(id){
			 notasCreditoAsociadasGrid.cells(id,0).open();
			 notasCreditoAsociadasGrid.cells(id,0).close();
		 	 		var subgrid=new dhtmlXGridObject();
		 	 		
		 	 		notasCreditoAsociadasGrid.cells(id,0).getSubGrid().attachEvent("onXLE", function(grid){
		 	 			
		 	 		});
		 	 		
		 		}); 
		 	
	     });
	 													  
	 notasCreditoAsociadasGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){			 
		 blockPage();
		 notasCreditoAsociadasGrid.cells(sId,0).open();
		 notasCreditoAsociadasGrid.cells(sId,0).close();		 
		
		 if(sObj != tObj)
		 {
			 requiereAsociarDesasociarNCRsRec.push(1);		
		 }		 
		 	
		});	
	 
	 notasCreditoAsociadasGrid.attachEvent("onRowAdded", function(rId){
		 
		 if(banderaAsociandoTodas)
		 {	 
			 notasCreditoAsociadasGrid.cells(rId,0).open();
			 notasCreditoAsociadasGrid.cells(rId,0).close();
			 requiereAsociarDesasociarNCRsRec.push(1);			
		 }		   		 
			 	
		});
	 
	 notasCreditoAsociadasGrid.attachEvent("onSubGridCreated",function(sub,id,ind,value){
		 blockPage();
			 sub.callEvent("onGridReconstructed",[])
			     sub.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleNotaCredito.action?idFactura=" + id 
			    		 + "&tipoElemento=" + notasCreditoAsociadasGrid.cellById(id,3).getValue(),function(){
		    	 sub.callEvent("onGridReconstructed",[]);
		    	 sub.setSizes();
		    	 notasCreditoAsociadasGrid.setSizes();	 
		    	 
		    	 asociarDesasociarNotasCreditoRecuperacion(sub, id, OP_ASOCIAR, notasCreditoAsociadasGrid, notasCreditoPorRelacionarGrid);
		    	 if(notasCreditoAsociadasGrid.doesRowExist(id) && requiereAsociarDesasociarNCRsRec.length == 0 
		    			 && notasCreditoAsociadasGrid.cellById(id,3).getValue() == 'REC')//NOTA: Las NCRs asociadas a la factura se indico que no se desplegaran en el Grid de Cifras por NCR. 21/07/2015-Analisis JCCS
	 	 		 {	 	 				
	 	 				tipoNCR = (notasCreditoAsociadasGrid.cellById(id,3).getValue() == 'REC' ? '(-)':'(+)')
	 	 					.concat(notasCreditoAsociadasGrid.cellById(id,4).getValue());
	 	 				actualizarImportesPorNotaCredito(sub,OP_ASOCIAR,tipoNCR);
	 	 		 }
		    	 
		    	 // MOSTRAR SUBGRID ABIERTO AL CARGAR EL JSP
		    	 
		    	 
	         });
			 unblockPage();
		     return false;  
		 });
	 
	 if(jQuery('#soloLectura').val() == 'true')
		{
	 		notasCreditoAsociadasGrid.enableDragAndDrop(false);		 
		}else
		{
			notasCreditoAsociadasGrid.enableDragAndDrop(true);			 
		}	
	 	
	 	notasCreditoAsociadasGrid.attachEvent("onBeforeDrag",function(id){
		  if (notasCreditoAsociadasGrid.cellById(id,3).getValue() == "FAC") //Si se trata de una factura entonces no será posible mover el elemento.  
		  {
			 return false; 
			  
		  }else
		  {
			 return true;  
		  }
		                                         
		});
	 		 	
	 	notasCreditoAsociadasGrid.attachEvent("onDragIn",function(sid,tid){
	 		  
	 		if(notasCreditoPorRelacionarGrid.doesRowExist(sid))
	 		{	 				
	 			return true;
	 		}
	 		
	 		  return false;                                       
	 		});

	 notasCreditoAsociadasGrid.attachHeader("&nbsp,&nbsp,#text_filter,&nbsp,&nbsp,#text_filter");
	 
	 notasCreditoAsociadasGrid.load("/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/obtenerRecuperacionesAsociadas.action?liquidacionSiniestro.id=" 
				+ idLiquidacion);
}

*/


/*function asociarDesasociarNotasCreditoRecuperacion(subGrid, rowId, operacion, targetGrid, sourceGrid)//TODO implementar JS
{	
	if(requiereAsociarDesasociarNCRsRec.length > 0)
	{
		var url;
		var contador = 0; 
		
		if(operacion == OP_ASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/asociarRecuperacion.action?liquidacionSiniestro.id='+ 
					jQuery('#idLiquidacion').val() + '&factura.id=' +  rowId;
			
		}else if(operacion == OP_DESASOCIAR)
		{
			url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/desasociarRecuperacion.action?liquidacionSiniestro.id='+ 
					jQuery('#idLiquidacion').val() + '&factura.id=' +  rowId;
				
		}
		
		var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
 					manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, JSON.parse(data).mensaje);  					
 					
 				}else
 				{
 					actualizarImportesLiquidacion(JSON.parse(data));
 					if(operacion == OP_DESASOCIAR)
 					{
 						actualizarImportesPorNotaCredito(subGrid,operacion,'(-)Recuperaci&oacute;n');  
 					}
 										 										
 				}				
	        }
		});	
		
		requiereAsociarDesasociarNCRsRec.pop();
	}
	unblockPage();
}*/


function asociarDesasociarRecuperacion(gridDestino, gridOrigen, operacion)
{	
	var checked=gridOrigen.getCheckedRows(0);
	var tieneNotaDeCreadito = false;
	if(checked == ''){
		alert('Debe seleccionar al menos un elemento');
	}else{
		blockPage();
		var idsArray = checked.split(',');
		var arrayLength = idsArray.length;
		for (var i = 0; i < arrayLength; i++) {
		    gridOrigen.moveRowTo(idsArray[i],"","move","sibling",gridOrigen,gridDestino);	
	}

	var url;
	
	if(operacion == OP_ASOCIAR)
	{
		url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/asociarRecuperaciones.action?liquidacionSiniestro.id='+jQuery('#idLiquidacion').val()+'&liquidacionSiniestro.facturasConcat='+checked; 
		
	}else if(operacion == OP_DESASOCIAR)
	{
		url = '/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/desasociarRecuperaciones.action?liquidacionSiniestro.id='+jQuery('#idLiquidacion').val()+'&liquidacionSiniestro.facturasConcat='+checked; 
			
	}
		

		var ajaxresults = jQuery.ajax({
			"url" : url,
			"async": false,
			"dataType" : "text json",
			"success": function(data) {
				
				if(JSON.parse(data).tipoMensaje == TIPO_MENSAJE_ERROR)
 				{
 					//manejarErrorAsociarDesasociar(targetGrid, sourceGrid, rowId, JSON.parse(data).mensaje);  					
 					
 				}else
 				{
 					actualizarImportesLiquidacion(JSON.parse(data));
 					initGridCifrasPorNotaCredito();
 					desplegarBotones();
 					/*if(operacion == OP_DESASOCIAR)
 					{
 						alert('Actualiza importes de NC');
 						//actualizarImportesPorNotaCredito(subGrid,operacion,'(-)Recuperaci&oacute;n');  
 					}*/
 										 										
 				}				
	        }
		});	
	}
	unblockPage();
}



function limpiarClassBusquedaLiquidaciones(){
	// SE APOYA EN LA CLASE txtfield PARA REMOVER TODOS LOS DATOS
	jQuery(".txtfield").removeClass("requeridoRango validaCombo validaComboValor");
	
	jQuery("#montoDe").attr('disabled','disabled');
	jQuery("#montoHasta").attr('disabled','disabled');
	jQuery("#fechaSolDe").attr('disabled','disabled');
	jQuery("#fechaSolHasta").attr('disabled','disabled');
	jQuery("#fechaAutDe").attr('disabled','disabled');
	jQuery("#fechaAutHasta").attr('disabled','disabled');
	jQuery("#fechaDeTransfer").attr('disabled','disabled');
	jQuery("#fechaDeTransHasta").attr('disabled','disabled');
	
	jQuery("#hTipoServicio").attr("value","3");
	jQuery("#servicioPublico").attr("value",0);
	jQuery("#servicioParticular").attr("value",0);
}


function consultarOrdenesDeCompra(idFactura){
	var url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleFacturaOrdenCompra.action?idFactura="+idFactura;
	mostrarVentanaModal("vm_detalleFactura", "Detalle de Factura", null, null, 438, 308, url , ""); 	
}

function consultarDetalleNotasCredito(id,tipo){
	var url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verCOntenedorDetalleNotaDeCredito.action?idFactura="+id+"&tipoElemento="+tipo;
	mostrarVentanaModal("vm_detalleNotaCredito", "Detalle de Factura", null, null, 438, 308, url , ""); 	
}


function cerrarDetalleFactura(){
	parent.cerrarVentanaModal("vm_detalleFactura",true);
}

function cerrarDetalleNota(){
	parent.cerrarVentanaModal("vm_detalleNotaCredito",true);
}



function initGridDetalleOrdenesDeCompra(idFactura){

	var url ="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleFactura.action?idFactura="+idFactura;
	detalleOrdenCompraGrid = new dhtmlXGridObject('detalleOrdenCompraGrid');
  	detalleOrdenCompraGrid.attachEvent("onXLS", function(grid){blockPage();});
  	detalleOrdenCompraGrid.attachEvent("onXLE", function(grid){unblockPage();});
  	detalleOrdenCompraGrid.load(url) ;
}


function initGridDetalleNotaCredito(idFactura,tipo){

	var url ="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleNotaCredito.action?idFactura="+idFactura+"&tipoElemento="+tipo;
	detalleNotaCreditoGrid = new dhtmlXGridObject('detalleNotaCreditoGrid');
  	detalleNotaCreditoGrid.attachEvent("onXLS", function(grid){blockPage();});
  	detalleNotaCreditoGrid.attachEvent("onXLE", function(grid){unblockPage();});
  	detalleNotaCreditoGrid.load(url) ;
}



/*function consultarDetalleNota(idNota){
	var url = "/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verContenedorDetalleNotaCredito.action?idFactura="+idFactura;
	mostrarVentanaModal("vm_detalleNota", "Detalle de Nota", null, null, 438, 308, url , ""); 	
}

function cerrarDetalleNota(){
	parent.cerrarVentanaModal("vm_detalleNota",true);
}*/

function initGridDetalleNotas(idNota){

	var url ="/MidasWeb/siniestros/liquidacion/liquidacionSiniestro/verDetalleNota.action?idFactura="+idNota;
	detalleNotaGrid = new dhtmlXGridObject('detalleNotaGrid');
  	detalleNotaGrid.attachEvent("onXLS", function(grid){blockPage();});
  	detalleNotaGrid.attachEvent("onXLE", function(grid){unblockPage();});
  	detalleNotaGrid.load(url) ;
}

function initGridDetalleNotas(notaId){
	alert(notaId);
}






