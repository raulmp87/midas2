var mostrarLayoutSipac = "/MidasWeb/siniestros/sipac/mostrarLayoutSipac.action";
var generarLayoutSipac = "/MidasWeb/siniestros/sipac/generarLayoutSipac.action";
var aceptarLoteSipac = "/MidasWeb/siniestros/sipac/aceptarLoteSipac.action";
var cancelarLoteSipac = "/MidasWeb/siniestros/sipac/cancelarLoteSipac.action";
var descargarLoteSipac = "/MidasWeb/siniestros/sipac/descargarLoteSipac.action";
var subirLoteSipac = "/MidasWeb/siniestros/sipac/subirLoteSipac.action";
var filtrarLotesPath = "/MidasWeb/siniestros/sipac/filtrarLotes.action";

function imprimirReporteLayoutSipac(){
	var fechaInicial = dwr.util.getValue('fechaInicial');
	var fechaFinal = dwr.util.getValue('fechaFinal');
	
	if(fechaInicial == undefined || fechaInicial == null || fechaInicial == ''){
		mostrarMensajeInformativo('Se tiene que especificar la Fecha Inicial.', '20');
	}else if(fechaFinal == undefined || fechaFinal == null || fechaFinal == ''){
		mostrarMensajeInformativo('Se tiene que especificar la Fecha Final.', '20');
	}else{
		var location = generarLayoutSipac;
		if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '')
			location = location+"?fechaInicial="+fechaInicial;
		if(fechaFinal != undefined && fechaFinal != null && fechaFinal != '' )
			location = location+"&fechaFinal="+fechaFinal ;
		window.open(location,"ReporteLayoutSipac");
		setTimeout(function(){ listarFiltradoGenerico(filtrarLotesPath, "loteSipacGrid", jQuery("#reporteLayoutSipacForm")); }, 10000);
	}
}

function marcarTodosAceptarLote(){
	aceptarLote(jQuery("#idLote").val(), true);
}

function aceptarLote(idLote,loadConsulta){
	
	if(idLote != undefined || idLote != null){
		var params = "?idLote=" + idLote;
		jQuery.ajax({
	        url: aceptarLoteSipac + params,
	        dataType: 'json',
	        async:false,
	        type:"GET",
	        success: function(){
	        },
	        beforeSend: function(){
					blockPage();
	        },
	        complete: function(){
	      	  unblockPage();
	      	  if(loadConsulta){
	      		obtenerInformacionLote();
	      	  } else {
	      		jQuery(function(){
		    		listarFiltradoGenerico(filtrarLotesPath, "loteSipacGrid", jQuery("#reporteLayoutSipacForm"));
		    	});
	      	  }
	        }
		});
	}else{
		mostrarMensajeInformativo('No pudo ser obtenido el ID del Lote.', '30');
	}
}

function cancelarLote(idLote){
	
	if(idLote != undefined || idLote != null){
		var params = "?idLote=" + idLote;
		jQuery.ajax({
	        url: cancelarLoteSipac + params,
	        dataType: 'json',
	        async:false,
	        type:"GET",
	        success: function(){
	        },
	        beforeSend: function(){
					blockPage();
	        },
	        complete: function(){
	      	  unblockPage();
		      	jQuery(function(){
		    		listarFiltradoGenerico(filtrarLotesPath, "loteSipacGrid", jQuery("#reporteLayoutSipacForm"));
		    	});
	        }
		});
	}else{
		mostrarMensajeInformativo('No pudo ser obtenido el ID del Lote.', '30');	
	}
}

function descargarLote(idLote){
	
	if(idLote == undefined || idLote == null || idLote == ''){
		mostrarMensajeInformativo('No se pudo obtener el Id del Lote', '20');
	}else{
		var location = descargarLoteSipac;
		if(idLote != undefined && idLote != null && idLote != '')
			location = location+"?idLote="+idLote;
		window.open(location,"ReporteLayoutSipac");
	}
}

function subirLote(idLote){
	if(idLote != undefined || idLote != null){
		var params = "?idLote=" + idLote;
		sendRequestJQ(null, subirLoteSipac + params, targetWorkArea,null);
	} else {
		mostrarMensajeInformativo('No pudo ser obtenido el ID del Lote.', '30');
	}
}

var loteGrid;
var loteGridProcessor;
function obtenerInformacionLote(){
	loteGrid = new dhtmlXGridObject('loteGrid');
	loteGrid.load('/MidasWeb/siniestros/sipac/listarLote.action?idLote=' + jQuery("#idLote").val());
	loteGridProcessor = new dataProcessor('/MidasWeb/siniestros/sipac/guardar.action?idLote=' + jQuery("#idLote").val());	
	loteGridProcessor.enableDataNames(true);
	loteGridProcessor.setTransactionMode("POST");
	loteGridProcessor.setUpdateMode("cell");
	loteGridProcessor.attachEvent("onAfterUpdate",refrescarGridLote);	
	loteGridProcessor.init(loteGrid);	
}

function refrescarGridLote(sid,action,tid,node){
	obtenerInformacionLote();
	return true;
}

function cerrarConsultaLote(){
	sendRequestJQ(null, mostrarLayoutSipac, targetWorkArea,null);
}