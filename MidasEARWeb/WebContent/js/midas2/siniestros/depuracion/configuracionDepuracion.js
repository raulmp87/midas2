/**
 * 
 */
var coberturasAsociadasTree;
var coberturasDisponiblesTree;
var terminosAsociadosGrid;
var terminosDisponiblesGrid;
var depuracionesGrid;

function initGridConfiguraciones(cargaGrid){
	document.getElementById("depuracionesGrid").innerHTML = '';
	depuracionesGrid = new dhtmlXGridObject('depuracionesGrid');
	depuracionesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	depuracionesGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
		depuracionesGrid.sortRows(3,"date","desc");
	});
	depuracionesGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	depuracionesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
    var url = buscarDepuracionesPath + "?";
    if(cargaGrid){
    	url += jQuery(document.busqConfigForm).serialize();
    }
	

	depuracionesGrid.load(url);	
}




function initGrids() {
	var idConfiguracion = jQuery("#idConfiguracion").val();
	if(idConfiguracion != null && idConfiguracion != ""){
		initGridsCobertura();
		initGridsTerminosAjuste();
	}
}

function initGridsCobertura(){
	obtenerCoberturasAsociadas();
	obtenerCoberturasDisponibles();
}

function initGridsTerminosAjuste(){
	obtenerTerminosDisponibles();
	obtenerTerminosAsociados();
}

function obtenerCoberturasAsociadas() {
	document.getElementById("coberturasAsociadasTree").innerHTML = '';
	coberturasAsociadasTree = new dhtmlXTreeObject('coberturasAsociadasTree', "100%","100%", 0);
	coberturasAsociadasTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	coberturasAsociadasTree.enableTreeImages(false);	
	coberturasAsociadasTree.enableSmartXMLParsing(true);
	coberturasAsociadasTree.attachEvent("onXLS", function(grid_obj){blockPage()});
	coberturasAsociadasTree.attachEvent("onXLE", function(grid_obj){unblockPage()});
	coberturasAsociadasTree.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorCoberturasAsociadas");
    });
	coberturasAsociadasTree.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorCoberturasAsociadas');
    });
	var soloConsulta = jQuery("#soloConsulta").val();
	var noEditable = jQuery("#noEditable").val();
	var seleccionado = jQuery("#ch_cobertura").is(':checked');
	if(soloConsulta != "1" && noEditable != "true"){
		
		coberturasAsociadasTree.enableDragAndDrop(true);
		coberturasAsociadasTree.attachEvent("onBeforeDrag",function(id){
			var seleccionado = jQuery("#ch_cobertura").is(':checked');
		  if (seleccionado) {
			  return true;
		  } else {
			  return false;
		  }
		});	
		
		coberturasAsociadasTree.attachEvent("onDrop", 
				function(sId,tId,id,sObject,tObject){
				var path = null;
				var idConfiguracion = jQuery("#idConfiguracion").val();
				if (sId.substring(0, 3) == 'SEC') {
					var idSeccion = sId.substring(4, sId.length);
					
					if (idSeccion.indexOf('_') > -1) {
						idSeccion = idSeccion.substring(0, idSeccion.indexOf('_') );
					}
					
					var path = asociarCoberturaPath  + '?';
					path = path + "accionCoberturas=AS" + "&configuracion.id=" + idConfiguracion +
					"&idToSeccion=" + idSeccion;
				} else if (sId.substring(0, 3) == 'COB') {
					
					var idx = sId.indexOf("-SEC");
					var idxSubCve = sId.indexOf("-CVECAL");
					var idCobertura = sId.substring(4, idx);
					var idSeccion = sId.substring(idx + 5, idxSubCve);
					var claveSubCalculo = sId.substring(idxSubCve + 8, sId.length);
					var path = asociarCoberturaPath + '?';
					path = path + "accionCoberturas=AC" + "&configuracion.id=" + idConfiguracion
					+ "&idToCobertura=" + idCobertura + "&idToSeccion=" + idSeccion + "&claveSubCalculo=" + claveSubCalculo;
				}				
				try {
					coberturasAsociadasTree.loadXML(path, initGridsCobertura);
				} catch(err) {
					initGridsCobertura();
				}
				return true;})
	}
	coberturasAsociadasTree.loadXML(coberturasAsociadasPath + '?' + jQuery(document.confDepuracionForm).serialize());
}

function obtenerCoberturasDisponibles() {
	document.getElementById("coberturasDisponiblesTree").innerHTML = '';
	coberturasDisponiblesTree = new dhtmlXTreeObject('coberturasDisponiblesTree', "100%","100%", 0);
	coberturasDisponiblesTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	coberturasDisponiblesTree.enableTreeImages(false);	
	coberturasDisponiblesTree.enableSmartXMLParsing(true);
	coberturasDisponiblesTree.attachEvent("onXLS", function(grid_obj){blockPage()});
	coberturasDisponiblesTree.attachEvent("onXLE", function(grid_obj){unblockPage()});
	coberturasDisponiblesTree.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorCoberturasDisponibles");
    });
	coberturasDisponiblesTree.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorCoberturasDisponibles');
    });
	var soloConsulta = jQuery("#soloConsulta").val();
	var noEditable = jQuery("#noEditable").val();	
	
	if(soloConsulta != "1" && noEditable != "true"){
		coberturasDisponiblesTree.enableDragAndDrop(true);
		coberturasDisponiblesTree.attachEvent("onBeforeDrag",function(id){
				var seleccionado = jQuery("#ch_cobertura").is(':checked');
			  if (seleccionado) {
				  return true;
			  } else {
				  return false;
			  }
		});
		
		coberturasDisponiblesTree.attachEvent("onDrop", 
				function(sId,tId,id,sObject,tObject){
					var path = null;
					var idConfiguracion = jQuery("#idConfiguracion").val();
					if (sId.substring(0, 3) == 'SEC') {
						var idSeccion = sId.substring(4, sId.length);
						
						if (idSeccion.indexOf('_') > -1) {
							idSeccion = idSeccion.substring(0, idSeccion.indexOf('_') );
						}
						var path = eliminarCoberturaPath + '?';
						path = path + "accionCoberturas=ES" + "&configuracion.id=" + idConfiguracion +
						"&idToSeccion=" + idSeccion;
					} else if (sId.substring(0, 3) == 'COB') {					
						var idx = sId.indexOf("-SEC");
						var idxSubCve = sId.indexOf("-CVECAL");
						var idCobertura = sId.substring(4, idx);
						var idSeccion = sId.substring(idx + 5, idxSubCve);
						var claveSubCalculo = sId.substring(idxSubCve + 8, sId.length);
						var path = eliminarCoberturaPath + '?';
						path = path + "accionCoberturas=EC" + "&configuracion.id=" + idConfiguracion
						+ "&idToCobertura=" + idCobertura + "&idToSeccion=" + idSeccion + "&claveSubCalculo=" + claveSubCalculo;
					}
					coberturasDisponiblesTree.loadXML(path, initGridsCobertura );
					
					
				return true;})
		}
	coberturasDisponiblesTree.loadXML(coberturasDisponiblesPath + '?' + jQuery(document.confDepuracionForm).serialize());
}

function obtenerTerminosAsociados() {
	document.getElementById("terminosAsociadosGrid").innerHTML = '';
	terminosAsociadosGrid = new dhtmlXGridObject('terminosAsociadosGrid');
	terminosAsociadosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	terminosAsociadosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	terminosAsociadosGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorTerminosAsociados");
    });
	terminosAsociadosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorTerminosAsociados');
    });
	
	var soloConsulta = jQuery("#soloConsulta").val();
	var noEditable = jQuery("#noEditable").val();
	
	if (soloConsulta != "1" && noEditable != "true"){
		terminosAsociadosGrid.enableDragAndDrop(true);
		terminosAsociadosGrid.attachEvent("onBeforeDrag",function(id){
			var seleccionado = jQuery("#ch_terminosAjuste").is(':checked');
			if (seleccionado) {
				return true;
			} else {
				return false;
			}
		});
	}		
	
	terminosAsociadosGrid.load(terminosAjusteAsociadosPath + "?" + jQuery(document.confDepuracionForm).serialize());

	var terminosProcessor = new dataProcessor(relacionarTerminosPath +"?" +jQuery(document.confDepuracionForm).serialize());

	terminosProcessor.enableDataNames(true);
	terminosProcessor.setTransactionMode("POST");
	terminosProcessor.setUpdateMode("cell");
	terminosProcessor.attachEvent("onAfterUpdate", initGridsTerminosAjuste);
	terminosProcessor.init(terminosAsociadosGrid);
}

function obtenerTerminosDisponibles() {
	document.getElementById("terminosDisponiblesGrid").innerHTML = '';
	terminosDisponiblesGrid = new dhtmlXGridObject('terminosDisponiblesGrid');
	terminosDisponiblesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	terminosDisponiblesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	terminosDisponiblesGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorTerminosDisponibles");
    });
	terminosDisponiblesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorTerminosDisponibles');
    });
	
	var soloConsulta = jQuery("#soloConsulta").val();
	var noEditable = jQuery("#noEditable").val();
	
	if (soloConsulta != "1" && noEditable != "true"){
		terminosDisponiblesGrid.enableDragAndDrop(true);
		terminosDisponiblesGrid.attachEvent("onBeforeDrag",function(id){
			var seleccionado = jQuery("#ch_terminosAjuste").is(':checked');
		    if (seleccionado) {
			  return true;
		    } else {
			  return false;
		    }
		});
	}		
	
	terminosDisponiblesGrid.load(terminosAjusteDisponiblesPath + "?" + jQuery(document.confDepuracionForm).serialize());
}

function guardarConfigDepuracion(){
	if(validaConfiguracion()){
		removeCurrencyFormatOnTxtInput();
		//alert("guardarConfigDepuracion");
		sendRequestJQ(document.confDepuracionForm, guardarConfiguracionPath,'contenido', null);
	}
}



function configuraTipoProgramacion(soloConsulta, noEditable){
	console.log('soloConsulta: '+soloConsulta+' - noEditable: '+noEditable);
	if(soloConsulta == 1 || noEditable){
		jQuery("#ch_pd").attr("disabled","disabled");
		jQuery("#ch_pf").attr("disabled","disabled");
		jQuery("#ch_dd").attr("disabled","disabled");
		jQuery("#dp_fechaProgramacion").datepicker('disable');
		jQuery('#txt_progPorDia').attr('readonly', true);
	}
	var tipoProgramacion = jQuery("#h_tipo_programacion").val();
	if(!isEmpty(tipoProgramacion)){
		if(jQuery('#ch_pd').val() == tipoProgramacion){
			jQuery("#ch_pd").attr("checked","checked");
			actualizaCheckPorFecha(false);
		} 
		if(jQuery('#ch_pf').val() == tipoProgramacion){
			jQuery("#ch_pf").attr("checked","checked");
			actualizaCheckPorDia(false);
		}
		if(jQuery('#ch_dd').val() == tipoProgramacion){
			jQuery("#ch_dd").attr("checked","checked");
		} 
	}
}

function asociarTodosTerminos(){
	removeCurrencyFormatOnTxtInput();
	sendRequestJQ(document.confDepuracionForm, asociarTodosTerminosPath,'contenido', null);
}

function eliminarTodosTerminos(){
	removeCurrencyFormatOnTxtInput();
	sendRequestJQ(document.confDepuracionForm, eliminarTodosTerminosPath,'contenido', null);
}

function asociarTodasCoberturas(){
	removeCurrencyFormatOnTxtInput();
	sendRequestJQ(document.confDepuracionForm, asociarTodasCoberturasPath,'contenido', null);
}

function onChangeCobertura(){
	var keyCobertura = jQuery("#s_cobertura").val();
	if(keyCobertura != ""){
		var elem = keyCobertura.split('-');
		var idToSeccionToken = elem[0];
		var idToCoberturaToken = elem[1];
		jQuery("#h_seccion").val(idToSeccionToken);
		jQuery("#h_cobertura").val(idToCoberturaToken);
		var cveSubCalculoToken;
		if(elem.length > 2){
			cveSubCalculoToken = elem[2];
			jQuery("#h_claveSubCalculo").val(cveSubCalculoToken);
		}
	}else{
		jQuery("#h_seccion").val("");
		jQuery("#h_cobertura").val("");
		jQuery("#h_claveSubCalculo").val("");
	}
}

function eliminarTodasCoberturas(){
	removeCurrencyFormatOnTxtInput();
	sendRequestJQ(document.confDepuracionForm, eliminarTodasCoberturasPath,'contenido', "removeCurrencyFormatOnTxtInput();initCurrencyFormatOnTxtInput();");
}

function inhabilitaByClass(id, myClass){
	valor = jQuery( "#"  + id ).is(':checked');
	if(valor){
		jQuery( "." + myClass ).attr( "disabled", false );
		jQuery( "#ch_antiguedadCompleta" ).attr("disabled", true) ;
	}else{
		jQuery( "." + myClass ).attr( "disabled", true );
		jQuery( "." + myClass ).attr( "checked", false );
		jQuery( "." + myClass ).attr( "value", "" );
		
	}
}

function habilitarAntigCompleta (){
	var opcion = jQuery("#s_antiguedad").val();
	if (opcion =="ME" || opcion=="AN"){
		jQuery( "#ch_antiguedadCompleta" ).attr("disabled", false) ;
		
	}else{
		jQuery( "#ch_antiguedadCompleta" ).attr("disabled", true) ;
		jQuery( "#ch_antiguedadCompleta" ).attr("checked", false) ;
		jQuery( "#s_condicionTiempo" ).attr("disabled", false);
		jQuery( "#s_condicionTiempo" ).val("");
	}
}
function habilitaCondicionIgual (){
	var valor = jQuery( "#ch_antiguedadCompleta" ).is(':checked');
	if(valor){
		jQuery("#s_condicionTiempo").val("IG");
		jQuery("#s_condicionTiempo").attr("disabled", true);
	}else{
		jQuery("#s_condicionTiempo").val("");
		jQuery("#s_condicionTiempo").attr("disabled", false);
	}
	
}

function inhabilitaByClassWithMonto(id, myClass, idRango, idMontoInicial, idMontoFinal){
	valor = jQuery( "#"  + id ).is(':checked');
	if(valor){
		jQuery( "." + myClass ).attr( "disabled", false );
	}else{
		jQuery( "." + myClass ).attr( "disabled", true );
		jQuery( "." + myClass ).attr( "checked", false );
		jQuery( "." + myClass ).attr( "value", "" );
	}
	valorRango = jQuery( "#"  + idRango ).is(':checked');
	if(valorRango){
		jQuery( "#" + idMontoInicial ).attr( "disabled", false );
		jQuery( "#" + idMontoFinal ).attr( "disabled", false );
	}else{
		jQuery( "#" + idMontoFinal ).attr( "disabled", true );
	}
}

function onChangeRango(idRango, idTipoCondicion, idMontoInicial, idMontoFinal){
	valorRango = jQuery( "#"  + idRango ).is(':checked');
	if(valorRango){
		jQuery( "#" + idMontoInicial ).attr( "disabled", false );
		jQuery( "#" + idMontoFinal ).attr( "disabled", false );
		jQuery( "#" + idTipoCondicion ).val("");
	}else{
		jQuery( "#" + idMontoFinal ).val("");
		jQuery( "#" + idMontoFinal ).attr( "disabled", true );
	}
}

function onChangeTipoCondicion(idRango, idTipoCondicion, idMontoInicial, idMontoFinal){
	valorRango = jQuery( "#"  + idTipoCondicion ).val();
	if(valorRango != ""){
		jQuery( "#" + idMontoFinal ).attr( "disabled", true );
		jQuery( "#" + idRango ).attr( "checked", false );
		jQuery( "#" + idMontoFinal ).val("");
	}else{
		jQuery( "#" + idMontoInicial ).attr( "disabled", false );
		jQuery( "#" + idMontoFinal ).attr( "disabled", false );
	}
}

function initDepuracion(){
	var soloConsulta = jQuery("#soloConsulta").val();
	var noEditable = jQuery("#noEditable").val();
	if(soloConsulta != "1" && noEditable != "true"){
		var idConfiguracion = jQuery("#idConfiguracion").val();
		//inhabilitaByClass('ch_condicion', 'monto');
		inhabilitaByClassWithMonto('ch_condicion', 'monto', 'ch_rango', 'txt_montoInicial', 'txt_montoFinal');
		inhabilitaByClass('ch_periodoTiempo', 'antigSin');
		inhabilitaByClass('ch_cuentaPagos','pagos');
		inhabilitaByClass('ch_periodoTiempoSinP','antigSinPag');
		//inhabilitaByClass('ch_condicionPorcPagos','porcPag');
		inhabilitaByClassWithMonto('ch_condicionPorcPagos', 'porcPag', 'ch_rangoPorcPagos', 'txt_condicionPorcPagoIni', 'txt_condicionPorcPagoFin');
		inhabilitaByClass('ch_cuentenPres','presup');
		inhabilitaByClass('ch_periodoTiempoPres','antigPresup');
		inhabilitaByClass('ch_tipoPerdida','tipPerdida');
		inhabilitaByClass('ch_tipoAtencion','tipAtencion');
		desahilitarBotones('ch_terminosAjuste', 'btnElimTodosTerm', 'btnAsocTodosTerm');
		desahilitarBotones('ch_cobertura', 'btnElimTodasCob', 'btnAsocTodasCob');
	}else{
		jQuery("#fechaProgramacion").datepicker('disable');
	}
}

function validateDia(tDia){
	valor = jQuery( "#"  + tDia );
	if(parseInt(valor.val()) > 31){
		mostrarMensajeInformativo('El dia no puede ser mayor a 31', '20');
		valor.val("");
	}
}

function validaPorcentaje(element){
	var porcentaje = jQuery(element).val();
	if(porcentaje>100){
		mostrarMensajeInformativo('El porcentaje no puede ser mayor a 100', '20');
		jQuery(element).val('');
	}
}

function mostrarConfiguracionReserva(configuracionId, soloConsulta){
	url = mostrarConfiguracionPath + '?vista=1' + '&soloConsulta=' + soloConsulta;
	if(configuracionId != 0){
		url = url + '&configuracion.id=' + configuracionId;
	}
	sendRequestJQ(null, url,'contenido', null);
}

function generaDepuracion(){
	removeCurrencyFormatOnTxtInput();
	sendRequestJQ(document.busqConfigForm, generarDepuracionPath,'contenido', null);
}

function desahilitarBotones(id, btn1, btn2){
	valor = jQuery( "#"  + id ).is(':checked');
	if(valor){
		jQuery( "#" + btn1).show();
		jQuery( "#" + btn2).show();
	}else{
		jQuery( "#" + btn1).hide();
		jQuery( "#" + btn2).hide();
	}
}

function validaAntiguedadSiniestro(){
	var salida = true;
	var chPeriodoTiempo = jQuery("#ch_periodoTiempo").is(':checked');
	var chAntiguedadComp = jQuery("#ch_antiguedadCompleta").is(':checked');
	var chAntigCompEnabled = jQuery("#ch_antiguedadCompleta").is(':enabled');
	var condicionTiempo = jQuery("#s_condicionTiempo").val();
	var lblAntiguedad = jQuery("#txt_antiguedad").val();
	var listCondicion = jQuery("#s_condicionTiempo").val();
	
	if(chPeriodoTiempo == true && chAntiguedadComp != true && chAntigCompEnabled == true){
		if((condicionTiempo == "AN" || condicionTiempo == "ME") && (lblAntiguedad != null || lblAntiguedad != "") && (listCondicion != null || listCondicion != "")  ){
			return salida;
		}else{
			
			mostrarMensajeInformativo('Selecione alguna condición, antiguedad y tiempo. O marque la casilla Antiguedad Completa','20');
			return false;
		}
		
	}
	//return salida;
}

function validaConfiguracion(){
	
	var nombreConfig    = jQuery("#txt_nombre").val();
	var porcDepurar     = jQuery("#txt_porcADepurar").val();
	var estatus         = jQuery("#s_estatus").val();
	var observaciones   = jQuery("#ta_observaciones").val();
	var tipoServicio    = jQuery("input:radio:checked[name=configuracion.tipoServicio]").val();
	var oficina 	    = jQuery("#oficina").val();
	var negocio 	    = jQuery("#negocio").val();
	var numeroPoliza    = jQuery("#txt_numeroPoliza").val();
	var inciso          = jQuery("#txt_inciso").val();
	var numeroSiniestro = jQuery("#txt_numeroSiniestro").val();
	var idConfiguracion = jQuery("#idConfiguracion").val();
	var noEditable = jQuery("#noEditable").val();	
	var valido = true;
	var validoOpcion;

	
	if(!validaDatosDeProgramacion()){
		return false;
	}
	
	if(noEditable != "true" ){
		if(nombreConfig == ""){
			mostrarMensajeInformativo('El campo Nombre es requerido','20');
			return false;
		}		
		
		if(estatus == ""){
			mostrarMensajeInformativo('El campo Estatus es requerido','20');
			return false;
		}
		
		if (estatus == "1"){
			
			if (porcDepurar == "") {
				mostrarMensajeInformativo('El campo Porcentaje a depurar es requerido','20');
				return false;
			}else if (porcDepurar > 100) {
				mostrarMensajeInformativo('El Porcentaje no puede ser mayor a 100','20');
				return false;

			}
			
			if (observaciones == "") {
				mostrarMensajeInformativo('El campo Observaciones es requerido','20');
				return false;
			}
			
		}
		
		if(idConfiguracion != "" ){
			
			if(valido){			
				var checks = jQuery( ".condReq" );
				for(i=0; i<checks.length; i++){
					valor = jQuery( "#"  + checks[i].id ).is(':checked');
					if(valor){
						valido = validaOpcion(checks[i].id);
						if(!valido){
							return valido;
						}
					}
				}
			}
			if(!valido){
				mostrarMensajeInformativo('Seleccione al menos un criterio','20');
			}
		}
	
		
	}
	
	return valido;

}

function validaOpcion(idOpcion){
	var valida = true;
	
	switch(idOpcion){
		case "ch_cobertura":
			if(coberturasAsociadasTree.hasChildren(0) <= 0){
				mostrarMensajeInformativo('Se requiere asociar al menos una cobertura','20');
				valida = false;
			}
			break;
		case "ch_condicion":
			var montoInicial = jQuery("#txt_montoInicial").val();
			var montoFinal   = jQuery("#txt_montoFinal").val();
			var rango        = jQuery( "#ch_rango" ).is(':checked');
			
			if(rango){
				if(montoInicial <= 9 || montoFinal <= 9){
					mostrarMensajeInformativo('Monto inicial y monto final son requeridos','20');
					valida = false;
				}
			}else{
				
				var condicion = jQuery("#s_condicion").val();
				if(condicion == "" || montoInicial == ""){
					mostrarMensajeInformativo('Selecione alguna condición del monto de reserva actual y/o el monto no debe ser nulo','20');
					valida = false;
				}
				
			}
			break;
		case "ch_periodoTiempo":
			
			//valida = validaAntiguedadSiniestro();
			
			var condicion = jQuery("#ch_periodoTiempo").val();
			if(condicion == ""){
				mostrarMensajeInformativo('Selecione alguna condición de antiguedad del siniestro','20');
				valida = false;
			}
			var antiguedad = jQuery("#txt_antiguedad").val();
			if(antiguedad == ""){
				mostrarMensajeInformativo('Capture el valor de la antiguedad del siniestro','20');
				valida = false;
			}
			var criterio = jQuery("#s_antiguedad").val();
			if(criterio == ""){
				mostrarMensajeInformativo('Seleccione el criterio de antiguedad del siniestro','20');
				valida = false;
			}
			break;
		case "ch_cuentaPagos":
			var aplicaPagos = jQuery("#s_cuentaPagos").val();
			if(aplicaPagos == ""){
				mostrarMensajeInformativo('Seleccione si cuenta con pagos','20');
				valida = false;
			}
			break;
		case "ch_periodoTiempoSinP":
			var condicion = jQuery("#s_condicionTiempoSinP").val();
			if(condicion == ""){
				mostrarMensajeInformativo('Selecione alguna condición de antiguedad sin pago','20');
				valida = false;
			}
			var antiguedad = jQuery("#txt_antiguedadSinP").val();
			if(antiguedad == ""){
				mostrarMensajeInformativo('Capture el valor de la antiguedad sin pago','20');
				valida = false;
			}
			var criterio = jQuery("#s_antiguedadSinP").val();
			if(criterio == ""){
				mostrarMensajeInformativo('Seleccione el criterio de antiguedad sin pago','20');
				valida = false;
			}
			break;	
		case "ch_condicionPorcPagos":
			var condicion = jQuery("#s_condicionPorcPagos").val();			
			var rango = jQuery( "#ch_rangoPorcPagos" ).is(':checked');
			
			if(rango){
				var montoInicial = jQuery("#txt_condicionPorcPagoIni").val();
				var montoFinal = jQuery("#txt_condicionPorcPagoFin").val();
				if(montoInicial == "" || montoFinal == ""){
					mostrarMensajeInformativo('Porcentaje de pago inicial y final son requeridos','20');
					valida = false;
				}
			} else {
				if(condicion == ""){
					mostrarMensajeInformativo('Selecione alguna condición del porcentaje de pagos','20');
					valida = false;
				}
			}
			break;
		case "ch_cuentenPres":
			var aplicaPagos = jQuery("#s_Presupuesto").val();
			if(aplicaPagos == ""){
				mostrarMensajeInformativo('Seleccione si cuenta con presupuestos','20');
				valida = false;
			}
			break;
		case "ch_periodoTiempoPres":
			var condicion = jQuery("#s_condicionTiempoPres").val();
			if(condicion == ""){
				mostrarMensajeInformativo('Selecione alguna condición del presupuesto','20');
				valida = false;
			}
			var antiguedad = jQuery("#txt_antiguedadPres").val();
			if(antiguedad == ""){
				mostrarMensajeInformativo('Capture el valor de la antiguedad del presupuesto','20');
				valida = false;
			}
			var criterio = jQuery("#s_antiguedadPres").val();
			if(criterio == ""){
				mostrarMensajeInformativo('Seleccione el criterio de antiguedad del presupuesto','20');
				valida = false;
			}
			break;
		case "ch_tipoPerdida":
			var tipoPerdida = jQuery("#s_tipoPerdida").val();
			if(tipoPerdida == ""){
				mostrarMensajeInformativo('Seleccione un tipo de perdida','20');
				valida = false;
			}
			break;
		case "ch_tipoAtencion":
			var tipoPerdida = jQuery("#s_tipoAtencion").val();
			if(tipoPerdida == ""){
				mostrarMensajeInformativo('Seleccione un tipo de atención','20');
				valida = false;
			}
			break;
		case "ch_terminosAjuste":
			if(terminosAsociadosGrid.rowsCol.length <= 0){
				mostrarMensajeInformativo('Se requiere asociar al menos un termino de ajuste','20');
				valida = false;
			}
			break;
	}
	//alert(valida);
	return valida;
}

function limpiarCampos(){
	jQuery( ".setNew" ).attr( "value", "" );
	jQuery('input:radio').each(function(){
	    var element = jQuery(this);
	    element.attr('checked', false);
	});
	limpiaTiposDeServicio();
}

function onChangeAplicaCriterio(idCheck, idTipoCondicion){
	valor = jQuery( "#"  + idCheck ).is(':checked');
	if(valor){
		jQuery( "#" + idTipoCondicion ).attr( "disabled", false );
	}else{
		jQuery( "#" + idTipoCondicion ).attr( "disabled", true );
	}
}

/**
 * Redirecciona a la pantalla de mostrar Histórico de Depuracion Reserva
 * @param idConfigDepuracionReserva id de la Configuración de la Depuracion Reserva
 */
function verHistoricoDepuracionReserva( idConfigDepuracionReserva ){
	var url = mostrarHistoricoPath + "?idConfigDepuracionReserva=" + idConfigDepuracionReserva;
	sendRequestJQ( null, url, targetWorkArea, null );	
}


function verVista(){
	var idConfig = jQuery("#idConfiguracion").val();
	var url = mostrarGenerarReservasADepurarPath+"?idConfigDepuracionReserva="+idConfig;
	sendRequestJQ( null, url, targetWorkArea, null );
}

function validaTipoProgramacion(element){
	console.log('validaTipoProgramacion - El valor de Element es: '+jQuery(element).val());
	if(jQuery(element).is(':checked')){
		jQuery('#h_tipo_programacion').val(jQuery(element).val());
		onclickTipoProgramacion(jQuery(element).val(),true);
	} else{
		onclickTipoProgramacion(jQuery(element).val(),false);
		jQuery('#h_tipo_programacion').val('');
	}
}


function onclickTipoProgramacion(val,isSelected){
	console.log('Val : '+val+' - isSelected : '+ isSelected);
	if(val == "PD" && isSelected ){
		actualizaCheckPorDia(true);
		actualizaCheckPorFecha(false);
		jQuery("#ch_dd").attr("checked","");
	}else if(val == "PF"  && isSelected ){
		actualizaCheckPorFecha(true);
		actualizaCheckPorDia(false);
		jQuery("#ch_dd").attr("checked","");
	}else if(val == "DD"  && isSelected ){
		actualizaCheckPorFecha(false);
		actualizaCheckPorDia(false);
	}else{
		actualizaCheckPorFecha(false);
		actualizaCheckPorDia(false);
	}
}



function actualizaCheckPorDia(seleccionable){
	console.log('actualizaCheckPorDia -  seleccionable : '+seleccionable);
	if(seleccionable){
		jQuery('#txt_progPorDia').attr('readonly', false);
	}else{
		jQuery("#ch_pd").attr("checked","");
		jQuery('#txt_progPorDia').val('');
		jQuery('#txt_progPorDia').attr('readonly', true);
	}
}

function actualizaCheckPorFecha(seleccionable){
	console.log('actualizaCheckPorFecha -  seleccionable : '+seleccionable);
	if(seleccionable){
		jQuery("#dp_fechaProgramacion").datepicker('enable');
	}else{
		jQuery("#ch_pf").attr("checked","");
		jQuery('#dp_fechaProgramacion').val('');
		jQuery("#dp_fechaProgramacion").datepicker('disable');
	}
}



function validaDatosDeProgramacion(){
	var esValido = true;
	if(jQuery('#ch_pd').is(':checked')) {
		var value = jQuery('#txt_progPorDia').val();
		if(isEmpty(value)) {
			mostrarMensajeInformativo('Se debe seleccionar el dia de la programacion','20');
			esValido = false;
		}
	} 
	if(jQuery('#ch_pf').is(':checked')) {
		var value = jQuery('#dp_fechaProgramacion').val();
		if(isEmpty(value)) {
			mostrarMensajeInformativo('Se debe seleccionar la fecha de la programacion','20');
			esValido = false;
		}	
	}
	return esValido;
	
}


function mascaraDecimal(valor,tag){
	
	var exp = new RegExp(/((\.{1}\d*))?$/);
	var sDecimales;
	var estimacion = jQuery(tag).val();
	
	
	if( valor != null || valor != "" ){
		
		// OBTIENE EN UN ARREGLO TODAS LAS COINCIDENCIAS, EN LA PRIMERA POSICION CONTIENE LA MAS ALTA
		var coincidencias = exp.exec(estimacion);
		
		if( coincidencias != null && coincidencias[0].length > 3 ){
			sDecimales = coincidencias[0].substring(0,3);
			jQuery(tag).val( estimacion.replace(coincidencias[0],sDecimales) ) ;
		}
		
	}
	
	
}

function limpiaTiposDeServicio(){
	jQuery('#ch_spublico').attr('checked',false);
	jQuery('#ch_sparticular').attr('checked',false);
	seleccionaCheckButton();
}

 function seleccionaCheckButton(){

	var publico = jQuery('#ch_spublico').is(':checked');
 	var privado = jQuery('#ch_sparticular').is(':checked');
 	console.log('Publico : '+publico);
 	console.log('Privado : '+privado);
	

	if(publico){
		console.log('Publico checked : '+jQuery('#ch_spublico').val());
		jQuery('#h_tipo_servicio').val(jQuery('#ch_spublico').val());
 	}else if(!privado){
 		console.log('Borra en publico');
 		jQuery('#h_tipo_servicio').val('');
 	}
 	if( privado){
 		jQuery('#h_tipo_servicio').val(jQuery('#ch_sparticular').val());
		console.log('Particular checked: '+jQuery('#ch_sparticular').val());
 	}else if( !publico ){
 		console.log('Borra en privado');
 		jQuery('#h_tipo_servicio').val('');
 	}

 	if(publico  && privado){
 		console.log('Ambos');
 		jQuery('#h_tipo_servicio').val(3);
 	} 
	console.log('Valor actual : '+jQuery('#h_tipo_servicio').val());
}

function validaTipoDeServicio(soloConsulta,noEditable){
	var tipoServicio = jQuery('#h_tipo_servicio').val();
	if(typeof tipoServicio != 'undefined' && !isEmpty(tipoServicio)){
		if( tipoServicio == 1){
			jQuery('#ch_sparticular').attr('checked',true);
		}
		if( tipoServicio == 2){
			jQuery('#ch_spublico').attr('checked',true);
		}
		if( tipoServicio == 3){
			jQuery('#ch_sparticular').attr('checked',true);
			jQuery('#ch_spublico').attr('checked',true);
		}
	}
	if(soloConsulta == 1 || noEditable){
		jQuery('#ch_sparticular').attr('disabled',true);
		jQuery('#ch_spublico').attr('disabled',true);
	}
}