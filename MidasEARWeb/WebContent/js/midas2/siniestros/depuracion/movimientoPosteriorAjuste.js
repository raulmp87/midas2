 var listaDeMovimientosGrid;

 function realizarBusqueda(sendParams){
 	 console.log('--> realizarBusqueda');
 	 var url = "/MidasWeb/siniestros/depuracion/buscarMovimientos.action?";
 	 if(sendParams){
 	 	formParams = jQuery(document.infoGeneralForm).serialize();	
 	 	url += formParams;
 	 }
 	 console.log('url'+url);
	 loadGrid(url);
 }

function limpiarFiltros(){
	jQuery('#infoGeneralForm').each (function(){
		  this.reset();
	});
}

function loadGrid(url){
 		 jQuery("#listaDeMovimientosGrid").empty(); 
		 listaDeMovimientosGrid = new dhtmlXGridObject('listaDeMovimientosGrid');
		 listaDeMovimientosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 listaDeMovimientosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 listaDeMovimientosGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicadorMovPosterior");
		    });
		 listaDeMovimientosGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicadorMovPosterior');
		    });
		 listaDeMovimientosGrid.load( url ) ;	
 }


 function mostrarDetalle(idMovimiento){
 	var url = "/MidasWeb/siniestros/depuracion/verDetalleMovimiento.action?idMovimiento=" + idMovimiento;
 	mostrarVentanaModal("vm_detalleMovimiento", "Detalle_Movimiento", null, null, 869, 330, url, "");
 }

 function cerrarDetalle(){
 	parent.cerrarVentanaModal("vm_detalleMovimiento",true);
 }

 function seleccionaCheckButton(){
	if(jQuery('#ch_sprivado').is(':checked') ){
		console.log('1');
 		jQuery('#h_tipo_servicio').val(1);
 	}else{
 		jQuery('#h_tipo_servicio').val('');
 	}
 	if(jQuery('#ch_sparticular').is(':checked') ){
 		console.log('2');
 		jQuery('#h_tipo_servicio').val(2);
 	}else{
 		jQuery('#h_tipo_servicio').val('');
 	}
 	if(jQuery('#ch_sprivado').is(':checked') && jQuery('#ch_sparticular').is(':checked') ){
 		console.log('3');
 		jQuery('#h_tipo_servicio').val(3);
 	}


 }


 function exportarExcel(){
	var url="/MidasWeb/siniestros/depuracion/exportarResultados.action?" + jQuery(document.infoGeneralForm).serialize();
	window.open(url, "Movimientos_Posteriores_Depuracion");
}