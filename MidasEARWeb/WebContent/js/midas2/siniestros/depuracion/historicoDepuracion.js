var listadoHistoricoDepuracionReservaGrid;
var listadoReservasDepuradas;
var reservasDepurarGrid;

/**
 * Inicializa lo necesario al cargarse la pantalla de Histórico de Depuracion Reserva
 */
function inicializarHistorico(){
	mostrarListadoHistoricoDepuracionReserva();
}

/**
 * Muestra el listado del Histórico de Depuración Reserva
 * en base al id de la Configuración de Depuración Reserva
 */
function mostrarListadoHistoricoDepuracionReserva(){
	var idConfigDepuracionReserva = jQuery("#h_idConfigDepuracionReserva").val();
	var url = mostrarListadoHistoricoPath + "?idConfigDepuracionReserva=" + idConfigDepuracionReserva;
	
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	
	listadoHistoricoDepuracionReservaGrid = new dhtmlXGridObject('historicoDepuracionGrid');
	listadoHistoricoDepuracionReservaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoHistoricoDepuracionReservaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listadoHistoricoDepuracionReservaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorHistoricoDepuracion");
    });
	listadoHistoricoDepuracionReservaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorHistoricoDepuracion');
    });

	listadoHistoricoDepuracionReservaGrid.load( url );	
}

function mostrarReservasParaDepurar( depuracionReservaId ){
	var idConfigDepuracionReserva = jQuery("#h_idConfigDepuracionReserva").val();
	var url = mostrarReservasParaDepurarPath + "?depuracionReservaId=" + depuracionReservaId + "&idConfigDepuracionReserva=" + idConfigDepuracionReserva;
	sendRequestJQ( null, url, targetWorkArea, null );	
}

/**
 * Regresa a la pantalla anterior que es el listado de Configuraciones de Depuración Reserva
 */
function regresarConfiguracionesDepuracionReserva(){
	var url = mostrarListConfigDepuracionReservaPath;
	sendRequestJQ( null, url, targetWorkArea, null );	
}

function mostrarReservasDepuradas(depuracionReservaId){
	var url = mostrarReservasDepuradasPath + "?depuracionReservaId=" + depuracionReservaId ;
	sendRequestJQ( null, url, targetWorkArea, null );	
}

function inicializarReservasDepuradas(){
	mostrarListadoReservasDepuradas();
}

/**
 * Muestra el listado del Histórico de Depuración Reserva
 * en base al id de la Configuración de Depuración Reserva
 */
function mostrarListadoReservasDepuradas(){
	var depuracionReservaId = jQuery("#h_depuracionReservaId").val();
	var idsDepuracionReservaDets = jQuery("#h_depuracionReservaDetString").val();
	
	var url = mostrarListadoReservasDepuradasPath + "?depuracionReservaId=" + depuracionReservaId+"&idsDepuracionDetalle="+idsDepuracionReservaDets;
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoReservasDepuradas = new dhtmlXGridObject('reservasDepuradasGrid');

	listadoReservasDepuradas.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoReservasDepuradas.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listadoReservasDepuradas.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorReservasDepuradas");
    });
	listadoReservasDepuradas.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorReservasDepuradas');
    });
	listadoReservasDepuradas.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
	listadoReservasDepuradas.load( url );	
}

function inicializarReservasDepurar(){	
	mostrarListadoReservasDepurar();
}

function exportarExcelReservaDepurada(){
	var depuracionReservaId = jQuery("#h_depuracionReservaId").val();
	var idsDepuradosList = jQuery("#h_depuracionReservaDetString").val();
	
	idsDepuradosList = deleteLastCommaForChain(idsDepuradosList);
	
	var url="/MidasWeb/siniestros/depuracion/reserva/historico/exportarResultadosReservaDepurada.action?depuracionReservaId=" + depuracionReservaId+"&idsDepuracionDetalle="+idsDepuradosList;
		window.open(url, "Reservas_Depuradas");
}

function exportarExcelReservaDepurar(){
	var depuracionReservaId = jQuery("#h_depuracionReservaId").val();
	var url="/MidasWeb/siniestros/depuracion/reserva/historico/exportarResultadosReservaDepurar.action?depuracionReservaId=" + depuracionReservaId;
		window.open(url, "Reservas_Depurar");
}

function regresarHistoricoDepuracionReserva(){
	var url = mostrarHistoricoPath + "?idConfigDepuracionReserva=" + jQuery("#h_idConfigDepuracionReserva").val();
	sendRequestJQ( null, url, targetWorkArea, null );		
}

function mostrarListadoReservasDepurar(){
	
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	reservasDepurarGrid = new dhtmlXGridObject('reservasDepurarGrid');

	reservasDepurarGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	reservasDepurarGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	reservasDepurarGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorReservasDepurar");
    });
	reservasDepurarGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorReservasDepurar');
    });

	reservasDepurarGrid.load(mostrarListadoReservasDepurarPath + "?" + jQuery(document.reservasDepurarForm).serialize() );
}

/**
 * Valida las condiciones especiales que se encuentran seleccionadas y 
 * crea una cadena con todas las idCondicionEspecial que se quieren 
 * asociar al Inciso
 */
function deleteLastCommaForChain(cadena){
	var texto = cadena;
	var textSalida = "";
	var lastChar=texto.substring(texto.length-1, texto.length);
	if (lastChar==","){
		textSalida = texto.substring(0, texto.length-1);
	}else{
		textSalida = texto;
	}
	
	return textSalida;
}

function obtenerReservasSeleccionadas( ind ) {
    var reservasChecked;
    var reservas = "";
    
    for (var i = 0; i < reservasDepurarGrid.getRowsNum(); i++) {
    	reservasChecked = reservasDepurarGrid.cells2(i, ind).getValue();
    	if( reservasChecked == 1 ){
    		reservas += reservasDepurarGrid.cells2(i, 0).getValue()+",";
    	}
    }
    reservas = deleteLastCommaForChain(reservas);
    return reservas;
}

function ejecutarDepuracion(){
	var reservas = obtenerReservasSeleccionadas(1);
	
	if (reservas != null && reservas != "") {
		var url = ejecutarDepuracionPath + "?idsDepuracionDetalle=" + reservas;
		sendRequestJQ( null, url, targetWorkArea, null );	
	} else {
		mostrarMensajeInformativo('Debe seleccionar al menos una opción', '20');
	}
	
}
