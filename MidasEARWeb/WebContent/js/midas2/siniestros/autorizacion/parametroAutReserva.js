var listadoGrid;

function inicializarListadoParametros(){	
	mostrarListadoParametros();
}


function mostrarListadoParametros(){	
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('listadoParametrosGrid');

	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });	
	var formParams = null;
	
	listadoGrid.load( listarParametrosPath + '?' + formParams );
	
	if( jQuery("#h_estatus").val() == true || jQuery("#h_estatus").val() == "true" ){
		jQuery("#s_estatus").val(1);
	}else if( jQuery("#h_estatus").val() == false || jQuery("#h_estatus").val() == "false" ){
		jQuery("#s_estatus").val(0);
	}else{
		jQuery("#s_estatus").val("");
	}

	if( jQuery("#h_idParametro").val() == null || jQuery("#h_idParametro").val() == "" ){
		var today = new Date(); 
		var dd    = today.getDate(); 
		var mm    = today.getMonth()+1; //January is 0! 
		var yyyy  = today.getFullYear(); 
		if(dd<10){dd='0'+dd} 
		if(mm<10){mm='0'+mm} 
		var today = dd+'/'+mm+'/'+yyyy; 
		 jQuery("#dp_fechaConfiguracion").val( today );
	}
	onChangeLineaNegocio();
}

function mostrarEditarParametro( id ){
	var url = buscarParametroPath + '?idParametro=' + id;

	sendRequest(null,url,"contenido","mostrarListadoParametros();inicializarPantalla();");

}
function eliminarParametro( idParametro ){
	var url = eliminarParametroPath + '?idParametro=' + idParametro;

	if( confirm("\u00BF Realmente desea eliminar el registro ?") ){
		sendRequest(null,url,null,"mostrarListadoParametros();limpiar();");
	}
}

function guardar(){
	removeCurrencyFormatOnTxtInput();
	formParams = jQuery(document.parametroForm).serialize();
	var url = guardarParametroPath + '?' + formParams;

	if( this.validaDatosRequeridos() ){
		sendRequestJQ(null, url, 'contenido', "mostrarListadoParametros();limpiar();");		
	}else{
		isError();
	}
}

function validaDatosRequeridos(){
	validaCondicionesRequeridas();
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				console.log('El elemento vacio es :'+these.attr('id'));
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}

function isError(){
	mostrarVentanaMensaje("20","Favor de llenar los campos requeridos",null);
}

function validacionInicial(){
	onChangeCobertura();
 	onChangeCondicionReserva();
 	onChangeRangoReserva();
 	onChangeCriterioReserva();
 	onChangeCondicionDeducible();
 	onChangeRangoDeducible();
 	onChangeCriterioDeducible();
 	onChangeCondicionPorcentajeDeducible();
 	onChangeRangoPorcentajeDeducible();
 	onChangeCriterioPorcentajeDeducible();
 	
}

function onChangeLineaNegocio(){
    var idToSeccion = jQuery("#s_lineaNegocio").val();
   
    if(idToSeccion != null  && idToSeccion != headerValue){
          listadoService.obtenerCoberturaPorSeccion( idToSeccion, 
        		  								     function(data){
													                addOptionsCoberturas("s_cobertura",data);
													                seleccionarCobertura();
													                //validacionInicial();
													                });
    }else{
    	addOptionsCoberturas("s_cobertura","");
    	//validacionInicial();
    }
}

function seleccionarCobertura() {
	 var idToCobertura = jQuery("#h_cobertura").val();
	 var claveSubCalculo = jQuery("#h_claveSubCalculo").val();
	 var cveTipoDeducible = jQuery("#h_claveTipoDeducible").val();
	  
	 if( claveSubCalculo.indexOf(",") > -1 ){
    	 claveSubCalculo = "";
     }
     if( cveTipoDeducible.indexOf(",") > -1 ){
    	 cveTipoDeducible = "";
     }
     if( idToCobertura != "" ){
    	 jQuery("#s_cobertura").val( idToCobertura+"-"+claveSubCalculo+"-"+cveTipoDeducible );
     }
}

function addOptionsCoberturas( target, map ){
    dwr.util.removeAllOptions(jQuery(target).selector);
    addSelectHeader(target);
    if(map != null){
          dwr.util.addOptions(jQuery(target).selector, map);       
    }
}

function onChangeCobertura(){
	var keyCobertura = jQuery("#s_cobertura").val();
	var elem = keyCobertura.split('-');
	var idToCoberturaToken = elem[0];
	var cveSubCalculoToken = elem[1];
	var cveTipoDeducibleToken = elem[2];
	
	// # LOS 2 ÚLTIMOS CHECKBOX SE ACTVAN SI cveTipoDeducibleToken == 1 
	if( cveTipoDeducibleToken == "1" ){
		jQuery("#ch_condicionDeducible").attr("disabled",false);
		jQuery("#ch_condicionPorcentajeDeducible").attr("disabled",false);
	}else{
		jQuery("#ch_condicionDeducible").attr('checked', false);
		jQuery("#ch_condicionPorcentajeDeducible").attr('checked', false);
		jQuery("#ch_rangoDeducible").attr('checked', false);
		jQuery("#ch_rangoPorcentajeDeducible").attr('checked', false);
		jQuery("#ch_condicionDeducible").attr("disabled","true");
		jQuery("#ch_condicionPorcentajeDeducible").attr("disabled","true");
		jQuery("#ch_rangoDeducible").attr("disabled","true");
		jQuery("#ch_rangoPorcentajeDeducible").attr("disabled","true");
		jQuery("#txt_montoDeducibleInicial").val("");
		jQuery("#txt_montoDeducibleFinal").val("");
		jQuery("#txt_montoPorcentajeDeducibleInicial").val("");
		jQuery("#txt_montoPorcentajeDeducibleFinal").val("");
		
	}
}

function onChangeEstatus(){
	jQuery("#h_estatus").val(jQuery("#s_estatus").val());
}

function onChangeCondicionReserva(){

	 var condicionReserva = jQuery("#ch_condicionReserva").attr("checked");
	 if( condicionReserva == true ){
		 jQuery("#s_criterioReserva").attr("disabled","");
		 jQuery("#ch_rangoReserva").attr("disabled","");
		 jQuery("#txt_montoReservaInicial").attr("disabled","");
	 }else if( condicionReserva == false ){
		 jQuery("#s_criterioReserva").val("");
		 jQuery("#s_criterioReserva").attr("disabled","true");
		 jQuery("#ch_rangoReserva").attr('checked', false);
		 jQuery("#ch_rangoReserva").attr("disabled","true");
		 jQuery("#txt_montoReservaInicial").val("");
		 jQuery("#txt_montoReservaInicial").attr("disabled","true");
		 jQuery("#txt_montoReservaFinal").val("");
		 jQuery("#td_montoReservaFinal").hide("slow");
	 }
	 validaCondicionesRequeridas();
}

function onChangeRangoReserva(){
	 var rangoReserva = jQuery("#h_rangoReserva").val();
	 var rangoReservaCh =  jQuery("#ch_rangoReserva").attr("checked");
	 if( rangoReserva == "true" || rangoReservaCh == true ){
		 jQuery("#ch_rangoReserva").attr('checked', true);
		 jQuery("#td_montoReservaFinal").show("slow");

		 jQuery("#d_montoReserva").removeClass("mostrarTxt");
		 jQuery("#d_montoReserva").addClass("ocultarTxt");	 
		 jQuery("#d_montoDeReserva").removeClass("ocultarTxt");
		 jQuery("#d_montoDeReserva").addClass("mostrarTxt");

	}else if( rangoReserva == "false" || rangoReservaCh == false ){
		jQuery("#ch_rangoReserva").attr('checked', false);
		 jQuery("#txt_montoReservaFinal").val("");
		 jQuery("#td_montoReservaFinal").hide("slow");

		 jQuery("#d_montoReserva").removeClass("ocultarTxt");
		 jQuery("#d_montoReserva").addClass("mostrarTxt");	 
		 jQuery("#d_montoDeReserva").removeClass("mostrarTxt");
		 jQuery("#d_montoDeReserva").addClass("ocultarTxt"); 
	}
	validaCondicionesRequeridas();
}

function onChangeCriterioReserva(){
	if( jQuery("#s_criterioReserva").val() != "" ){
		 jQuery("#ch_rangoReserva").attr('checked', false);
		 jQuery("#ch_rangoReserva").attr("disabled","true");
		 jQuery("#h_rangoReserva").val("false");
		 jQuery("#txt_montoReservaFinal").val("");
		 jQuery("#td_montoReservaFinal").hide("slow");
		 
		 jQuery("#d_montoReserva").removeClass("ocultarTxt");
		 jQuery("#d_montoReserva").addClass("mostrarTxt");	 
		 jQuery("#d_montoDeReserva").removeClass("mostrarTxt");
		 jQuery("#d_montoDeReserva").addClass("ocultarTxt");
	}else{
		 jQuery("#ch_rangoReserva").attr("disabled","");
	}
	validaCondicionesRequeridas();
}

function onChangeCondicionDeducible(){
	 var condicionDeducible = jQuery("#ch_condicionDeducible").attr("checked");

	 if( condicionDeducible == true ){
		 jQuery("#s_criterioRangoDeducible").removeAttr("disabled");
		 jQuery("#ch_rangoDeducible").removeAttr("disabled");
		 jQuery("#txt_montoDeducibleInicial").removeAttr("disabled");
	 }else{
		 jQuery("#s_criterioRangoDeducible").val("");
		 jQuery("#s_criterioRangoDeducible").attr("disabled","true");
		 jQuery("#ch_rangoDeducible").attr('checked', false);
		 jQuery("#ch_rangoDeducible").attr("disabled","disabled");
		 jQuery("#txt_montoDeducibleInicial").val("");
		 jQuery("#txt_montoDeducibleInicial").attr("disabled","true");
		 jQuery("#txt_montoDeducibleFinal").val("");
		 jQuery("#td_montoDeducibleFinal").hide("slow");
	 }
	 validaCondicionesRequeridas();
}

function onChangeRangoDeducible(){
	var rangoDeducible = jQuery("#h_rangoDeducible").val();
	var rangoDeducibleCh =  jQuery("#ch_rangoDeducible").attr("checked");
	if( rangoDeducible == "true" || rangoDeducibleCh == true ){
		 jQuery("#ch_rangoDeducible").attr('checked', true);
		 jQuery("#td_montoDeducibleFinal").show("slow");
		 jQuery("#txt_montoDeducibleFinal").attr("disabled","");
		 jQuery("#d_montoDeducible").removeClass("mostrarTxt");
		 jQuery("#d_montoDeducible").addClass("ocultarTxt");	 
		 jQuery("#d_montoDeDeducible").removeClass("ocultarTxt");
		 jQuery("#d_montoDeDeducible").addClass("mostrarTxt");
	}else if( rangoDeducible == "false" || rangoDeducibleCh == false ){
		 jQuery("#txt_montoDeducibleFinal").val("");
		 jQuery("#td_montoDeducibleFinal").hide("slow");
		
		 jQuery("#d_montoDeducible").removeClass("ocultarTxt");
		 jQuery("#d_montoDeducible").addClass("mostrarTxt");	 
		 jQuery("#d_montoDeDeducible").removeClass("mostrarTxt");
		 jQuery("#d_montoDeDeducible").addClass("ocultarTxt");
	}
	validaCondicionesRequeridas();
}

function onChangeCriterioDeducible(){
	if( jQuery("#s_criterioRangoDeducible").val() != "" ){
		 jQuery("#ch_rangoDeducible").attr('checked', false);
		 jQuery("#ch_rangoDeducible").attr("disabled","true");
		 jQuery("#h_rangoDeducible").val("false");
		 jQuery("#txt_montoDeducibleFinal").val("");
		 jQuery("#td_montoDeducibleFinal").hide("slow");
		 
		 jQuery("#d_montoDeducible").removeClass("ocultarTxt");
		 jQuery("#d_montoDeducible").addClass("mostrarTxt");	 
		 jQuery("#d_montoDeDeducible").removeClass("mostrarTxt");
		 jQuery("#d_montoDeDeducible").addClass("ocultarTxt");
	}else{
		jQuery("#ch_rangoDeducible").attr("disabled","");
	}
	validaCondicionesRequeridas();
}

function onChangeCondicionPorcentajeDeducible(){
	var condicionPorcentajeDeducible = jQuery("#ch_condicionPorcentajeDeducible").attr("checked");

	 if( condicionPorcentajeDeducible == true ){
		 jQuery("#s_criterioRangoPorcentajeDeducible").attr("disabled","");
		 jQuery("#ch_rangoPorcentajeDeducible").attr("disabled","");
		 jQuery("#txt_montoPorcentajeDeducibleInicial").attr("disabled","");
	 }else{
		 jQuery("#s_criterioRangoPorcentajeDeducible").val("");
		 jQuery("#s_criterioRangoPorcentajeDeducible").attr("disabled","true");
		 jQuery("#ch_rangoPorcentajeDeducible").attr('checked', false);
		 jQuery("#ch_rangoPorcentajeDeducible").attr("disabled","true");
		 jQuery("#txt_montoPorcentajeDeducibleInicial").val("");
		 jQuery("#txt_montoPorcentajeDeducibleInicial").attr("disabled","true");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").val("");
		 jQuery("#td_montoPorcentajeFinal").hide("slow");
	 }
	 validaCondicionesRequeridas();
}

function onChangeRangoPorcentajeDeducible(){
	var rangoPorcentajeDeducible = jQuery("#h_rangoPorcentajeDeducible").val();
	var rangoPorcentajeDeducibleCh =  jQuery("#ch_rangoPorcentajeDeducible").attr("checked");
	if( rangoPorcentajeDeducible == "true" || rangoPorcentajeDeducibleCh == true ){
		 jQuery("#ch_rangoPorcentajeDeducible").attr('checked', true);
		 jQuery("#td_montoPorcentajeFinal").show("slow");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").attr("disabled","");
		 jQuery("#d_montoPorcentaje").removeClass("mostrarTxt");
		 jQuery("#d_montoPorcentaje").addClass("ocultarTxt");	 
		 jQuery("#d_montoDePorcentaje").removeClass("ocultarTxt");
		 jQuery("#d_montoDePorcentaje").addClass("mostrarTxt");
	}else if( rangoPorcentajeDeducible == "false" || rangoPorcentajeDeducibleCh == false ){
		 jQuery("#txt_montoPorcentajeDeducibleFinal").val("");
		 jQuery("#td_montoPorcentajeFinal").hide("slow");

		 jQuery("#d_montoPorcentaje").removeClass("ocultarTxt");
		 jQuery("#d_montoPorcentaje").addClass("mostrarTxt");	 
		 jQuery("#d_montoDePorcentaje").removeClass("mostrarTxt");
		 jQuery("#d_montoDePorcentaje").addClass("ocultarTxt");
	}
	validaCondicionesRequeridas();
}

function onChangeCriterioPorcentajeDeducible(){
	if( jQuery("#s_criterioRangoPorcentajeDeducible").val() != "" ){
		 jQuery("#ch_rangoPorcentajeDeducible").attr('checked', false);
		 jQuery("#ch_rangoPorcentajeDeducible").attr("disabled","true");
		 jQuery("#h_rangoPorcentajeDeducible").val("false");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").val("");
		 jQuery("#td_montoPorcentajeFinal").hide("slow");
		 
		 jQuery("#d_montoPorcentaje").removeClass("ocultarTxt");
		 jQuery("#d_montoPorcentaje").addClass("mostrarTxt");	 
		 jQuery("#d_montoDePorcentaje").removeClass("mostrarTxt");
		 jQuery("#d_montoDePorcentaje").addClass("ocultarTxt");
	}else{
		 jQuery("#ch_rangoPorcentajeDeducible").attr("disabled",""); 
	}
	validaCondicionesRequeridas();
}

function iniciarParametro(){
	jQuery(".setNew").attr("disabled","");
}

function setConsultaParametro(){
	jQuery(".setNew").attr("disabled","true");
}

function onBlurRequeridos(){
	
	jQuery(".requerido").blur(
			function(){
				var these = jQuery(this);
				if( isEmpty( these.val() ) ){
					these.addClass("errorField");
				} else {
					these.removeClass("errorField");
				}
			}
		);
}


function obtenerCoberturaPorSeccion(){
	var url = obtenerCoberturaPorSeccionPath + '?idToSeccion=' + jQuery("#s_lineaNegocio").val();
	sendRequest(null,url,null,null);
}

function validaMonto( montoInicial, montoFinal ){
	 if( montoInicial == "" ){montoInicial = 0 }
	 if( montoFinal == "" ){montoFinal = 0 }
	 
    montoInicial = parseFloat( montoInicial );
    montoFinal   = parseFloat( montoFinal ); 
   
    if ( montoInicial > montoFinal ){
    	mostrarVentanaMensaje("20","El monto Inicial no puede ser mayor que el Monto Final",null);
    }     
} 

function validaCondicionesRequeridas(){
	 var condicionReserva = jQuery("#ch_condicionReserva").attr("checked");
	 var rangoReserva = jQuery("#ch_rangoReserva").attr("checked");
	 var condicionDeducible = jQuery("#ch_condicionDeducible").attr("checked");
	 var rangoDeducible = jQuery("#ch_rangoDeducible").attr("checked");
	 var condicionPorcentajeDeducible = jQuery("#ch_condicionPorcentajeDeducible").attr("checked");
	 var rangoPorcentajeDeducible = jQuery("#ch_rangoPorcentajeDeducible").attr("checked");
	 
	 if( condicionReserva == true && rangoReserva == true ){
		 jQuery("#s_criterioReserva").removeClass("errorField");
		 jQuery("#s_criterioReserva").removeClass("requerido");	 
		 jQuery("#txt_montoReservaInicial").addClass("requerido");
		 jQuery("#txt_montoReservaFinal").addClass("requerido");
	 }else if( condicionReserva == true && rangoReserva == false ){
		 jQuery("#s_criterioReserva").addClass("requerido");	
		 jQuery("#txt_montoReservaInicial").addClass("requerido");
		 jQuery("#txt_montoReservaFinal").removeClass("errorField");
		 jQuery("#txt_montoReservaFinal").removeClass("requerido");
	 }else{
		 jQuery("#s_criterioReserva").removeClass("errorField");
		 jQuery("#s_criterioReserva").removeClass("requerido");	 
		 jQuery("#txt_montoReservaInicial").removeClass("errorField");
		 jQuery("#txt_montoReservaInicial").removeClass("requerido");	 
		 jQuery("#txt_montoReservaFinal").removeClass("errorField");
		 jQuery("#txt_montoReservaFinal").removeClass("requerido");	 
	 }
	 
	 if( condicionDeducible == true && rangoDeducible == true ){
		 jQuery("#s_criterioRangoDeducible").removeClass("errorField");
		 jQuery("#s_criterioRangoDeducible").removeClass("requerido");	
		 jQuery("#txt_montoDeducibleInicial").addClass("requerido");
		 jQuery("#txt_montoDeducibleFinal").addClass("requerido");
	 }else if( condicionDeducible == true && rangoDeducible == false ){
		 jQuery("#s_criterioRangoDeducible").addClass("requerido");	 
		 jQuery("#txt_montoDeducibleInicial").addClass("requerido");
		 jQuery("#txt_montoDeducibleFinal").removeClass("errorField");
		 jQuery("#txt_montoDeducibleFinal").removeClass("requerido");
	 }else{
		 jQuery("#s_criterioRangoDeducible").removeClass("errorField");
		 jQuery("#s_criterioRangoDeducible").removeClass("requerido");	 
		 jQuery("#txt_montoDeducibleInicial").removeClass("errorField");
		 jQuery("#txt_montoDeducibleInicial").removeClass("requerido");	 
		 jQuery("#txt_montoDeducibleFinal").removeClass("errorField");
		 jQuery("#txt_montoDeducibleFinal").removeClass("requerido");	 
	 }
	 
	 if( condicionPorcentajeDeducible == true && rangoPorcentajeDeducible == true ){
		 jQuery("#s_criterioRangoPorcentajeDeducible").removeClass("errorField");
		 jQuery("#s_criterioRangoPorcentajeDeducible").removeClass("requerido");	 
		 jQuery("#txt_montoPorcentajeDeducibleInicial").addClass("requerido");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").addClass("requerido");
	 }else if( condicionPorcentajeDeducible == true && rangoPorcentajeDeducible == false ){
		 jQuery("#s_criterioRangoPorcentajeDeducible").addClass("requerido");	
		 jQuery("#txt_montoPorcentajeDeducibleInicial").addClass("requerido");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").removeClass("errorField");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").removeClass("requerido");
	 }else{
		 jQuery("#s_criterioRangoPorcentajeDeducible").removeClass("errorField");
		 jQuery("#s_criterioRangoPorcentajeDeducible").removeClass("requerido");	 
		 jQuery("#txt_montoPorcentajeDeducibleInicial").removeClass("errorField");
		 jQuery("#txt_montoPorcentajeDeducibleInicial").removeClass("requerido");	 
		 jQuery("#txt_montoPorcentajeDeducibleFinal").removeClass("errorField");
		 jQuery("#txt_montoPorcentajeDeducibleFinal").removeClass("requerido");	 
	 }
	 onBlurRequeridos();
}

function limpiar(){
	jQuery("#s_oficina").val("");
	jQuery("#t_nombreConfiguracion").val("");
	jQuery("#s_lineaNegocio").val("");
	jQuery("#s_cobertura").val("");
	jQuery("#s_estatus").val("1");
	jQuery("#s_criterioReserva").val("");
	jQuery("#ch_rangoReserva").attr("disabled","true");
	jQuery("#txt_montoReservaInicial").val("");
	jQuery("#txt_montoReservaFinal").val("");
	jQuery("#ch_condicionDeducible").attr("disabled","true");
	jQuery("#s_criterioRangoDeducible").val("");
	jQuery("#ch_rangoDeducible").attr("disabled","true");
	jQuery("#txt_montoDeducibleInicial").val("");
	jQuery("#txt_montoDeducibleFinal").val("");
	jQuery("#ch_condicionPorcentajeDeducible").attr("disabled","true");
	jQuery("#s_criterioRangoPorcentajeDeducible").val("");
	jQuery("#ch_rangoPorcentajeDeducible").attr("disabled","true");
	jQuery("#txt_montoPorcentajeDeducibleInicial").val("");
	jQuery("#txt_montoPorcentajeDeducibleFinal").val("");

}

function inicializarPantalla() {
	inicializarListadoParametros();
	onBlurRequeridos();
	validacionInicial();
	setConsultaParametro();
	initCurrencyFormatOnTxtInput();
}