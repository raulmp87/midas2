
function limpiarFiltros(){
	
	jQuery('#infoGeneralForm').each (function(){
		  this.reset();
	});
	jQuery("#messageError").hide();
	jQuery("#messageErrorRolesConsecutivos").hide();
}


function guardar(){
	if(validaDatosRequeridos()){
		if(validaRoles()){
			removeCurrencyFormatOnTxtInput();
			formParams = jQuery(document.infoGeneralForm).serialize();
			var url = "/MidasWeb/siniestros/autorizacion/historico/guardarConfiguracion.action?"+formParams;
			sendRequestJQ(null, url, targetWorkArea, null);	
			limpiarFiltros();
		}
	}else{
		mostrarMensajeInformativo('Favor de capturar los datos requeridos', '20');
	}
}

function inactivar(paramId){
	var url = "/MidasWeb/siniestros/autorizacion/historico/inactivarConfiguracion.action?idParametro="+paramId;
	if (confirm("Se desactivar\u00e1 el par\u00e1metro seleccionado. ¿Desea continuar?")) {
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function mostrarConfiguracionesEnGrid(){
	  jQuery("#parametrosGrid").empty(); 
	  var url = "/MidasWeb/siniestros/autorizacion/historico/listarParametros.action";
	  listadoDeParametrosGrid = new dhtmlXGridObject('parametrosGrid');
	  listadoDeParametrosGrid.attachEvent("onXLS", function(grid){blockPage();});
	  listadoDeParametrosGrid.attachEvent("onXLE", function(grid){unblockPage();});
	  listadoDeParametrosGrid.load(url) ;
}


function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}

var array = [];

function validaRoles(){
	var rol1 = jQuery("#s_autorizacion1").val();
	var rol2 = jQuery("#s_autorizacion2").val();
	var rol3 = jQuery("#s_autorizacion3").val();
	var rol4 = jQuery("#s_autorizacion4").val();
	array.length=0;
	console.log(rol1);
	console.log(rol2);
	console.log(rol3);
	console.log(rol4);
	if(validaValorEnArray(rol1) && validaValorEnArray(rol2) && validaValorEnArray(rol3) && validaValorEnArray(rol4)){
		jQuery("#messageError").hide();
		var arrayOrdenRoles = [];
		arrayOrdenRoles.push(rol1);
		arrayOrdenRoles.push(rol2);
		arrayOrdenRoles.push(rol3);
		arrayOrdenRoles.push(rol4);
		if(validaRolesConsecutivos(arrayOrdenRoles)){
			jQuery("#messageErrorRolesConsecutivos").hide();
			return true;	
		}else{
			jQuery("#messageErrorRolesConsecutivos").show("slow");
			return false;	
		}
	}else{
		jQuery("#messageError").show("slow");
		return false;
	}

}

function validaValorEnArray(value){
    if(value!=null && value!=""){
    	var match = array.indexOf(value);
        console.log('match:'+match);
		if(match != -1){
			return false;
		}else{
			console.log('Hace el push : '+value);
            array.push(value);
            return true;
		}
    }else{
    	return true;
    }
}

function validaRolesConsecutivos(arrayOrdenRoles){
	var arrayLength = arrayOrdenRoles.length;
	var hayPrevio = true;
	for (var i = 0; i < arrayLength; i++) {
		
	    valor = arrayOrdenRoles[i];
	    
	    if(valor!=null && valor!=""){
	    	for (var j = 0; j < i; j++) {
	    		 valorAnterior = arrayOrdenRoles[j];
	    		 
	    		 if(valorAnterior==null || valorAnterior==""){
	    			 return false;
	    		 }
	    	}
	    }
	}
	
	return true;
}



function printArray(array){
	for(i=0;i<array.length;i++){
		console.log('Imprime : '+array[i]);
	 }
}

