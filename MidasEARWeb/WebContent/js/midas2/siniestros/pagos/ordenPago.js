
 function limpiarContenedorListadoPagos(){
		jQuery(document.pagosForm).each (function(){
			  this.reset();
		});
	} 
 
 function changeTipoPrestador() {	
	 var tipo = dwr.util.getValue("tipoProveedorLis");	
	 if(null ==tipo   || tipo=="" ){
			dwr.util.removeAllOptions("proveedorLis");		
		}else{
		listadoService.getMapPrestadorPorTipo( tipo ,function(data){
			dwr.util.removeAllOptions("proveedorLis");
			dwr.util.addOptions("proveedorLis", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("proveedorLis", data);
		});
		}
 }

 
 function iniciarLista(){
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		//jQuery("#observacion").val( '');
		listadoGrid = new dhtmlXGridObject('pagosGrid');	
		listadoGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		listadoGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
	    });	
		
		var formParams = jQuery(document.pagosForm).serialize();
		listadoGrid.load( '/MidasWeb/siniestros/pagos/pagosSinestro/mostrarListaVacia.action?'+ formParams);
	}
 
 
 function buscarListaEnter(e) {
	    if (e.keyCode == 13) {
	    	buscarLista();
	    }
	}
 
 
 
 function validarBusqueda(){

		if(jQuery(nsOficinaS).val()  || jQuery(nsConsecutivoRS).val()  || jQuery(nsAnioS).val()  || jQuery(ofinasActivas).val()  || jQuery(idOrdenPago).val()  || jQuery(beneficiario).val()  
				|| jQuery(tipoProveedorLis).val()   || jQuery(proveedorLis).val()   || jQuery(tipoOrdenPago).val()   || jQuery(conceptoPago).val()   || jQuery(terminoAjusteLis).val()   || jQuery(numOrdenCompra).val()   
				|| jQuery(estautsId).val() || jQuery("#servPublico").attr('checked') || jQuery("#servParticular").attr('checked') ){
				return true;
		}
		mostrarVentanaMensaje("20","Debe de sseleccionar al menos un campo para generar la lista",null);
		return false;
	}
 
 
function validateEmptyFields(){
	var a = new Array();
	var contador = 0;
	
	a[0] = jQuery("#nsOficinaS").val();
	a[1] = jQuery("#nsConsecutivoRS").val();
	a[2] = jQuery("#nsAnioS").val();
	a[3] = jQuery("#ofinasActivas").val();
	a[4] = jQuery("#idOrdenPago").val();
	a[5] = jQuery("#beneficiario").val();
	a[6] = jQuery("#tipoProveedorLis").val();
	a[7] = jQuery("#proveedorLis").val();
	a[8] = jQuery("#tipoOrdenPago").val();  
	a[9] = jQuery("#conceptoPago").val(); 
	a[10] = jQuery("#terminoAjusteLis").val();
	a[11] = jQuery("#numOrdenCompra").val();
	a[12] = jQuery("#estautsId").val();
	a[13] = jQuery("#servPublico").attr('checked');
	a[14] = jQuery("#servParticular").attr('checked');
	
	for (x=0; x< a.length; x++){		 	
		if(a[x] != "" || a[x]== true){				
			contador++;
		}	
	}
	 if(contador >2 ){
		 return true;
	 }else{
		 return false;
	 }
}
 
 
 function buscarListaOrdenesPago(){
	 if(validateEmptyFields()){	
		 document.getElementById("pagingArea").innerHTML = '';
		 document.getElementById("infoArea").innerHTML = '';
		 listadoGrid = new dhtmlXGridObject('pagosGrid');	
		 listadoGrid.attachEvent("onXLS", function(grid){	
			blockPage();
		 });
		 listadoGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
		 });	
		 var formParams = jQuery(document.pagosForm).serialize();
		listadoGrid.load( '/MidasWeb/siniestros/pagos/pagosSinestro/mostrarListado.action?'+ formParams);
	}else{
		mostrarVentanaMensaje("20","Para realizar una búsqueda se requiere capturar tres datos.",null);
	}
 }
 
 function buscarLista(){
	 if(validarBusqueda()){	
		 document.getElementById("pagingArea").innerHTML = '';
		 document.getElementById("infoArea").innerHTML = '';
		 listadoGrid = new dhtmlXGridObject('pagosGrid');	
		 listadoGrid.attachEvent("onXLS", function(grid){	
			blockPage();
		 });
		 listadoGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
		 });	
		 var formParams = jQuery(document.pagosForm).serialize();
		listadoGrid.load( '/MidasWeb/siniestros/pagos/pagosSinestro/mostrarListado.action?'+ formParams);
	}
}
 
 
	function registrarOrdenPago(idOrdenCompra, idOrdenPago, accion){
		var pago;
		if(null== idOrdenPago || idOrdenPago==''){
			pago= null;
		}
		sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrarOrdenPago.action?idOrdenCompra='+idOrdenCompra+ '&modoPantalla=' +accion+ '&idOrdenPago='+idOrdenPago,targetWorkArea,null);

	} 
	
	function registrarNuevaOrdenPago(idOrdenCompra, accion){
		

		sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrarOrdenPago.action?idOrdenCompra='+idOrdenCompra+ '&modoPantalla=' +accion,targetWorkArea,null);

	} 
	
	function consultarOrdenPago(idOrdenPago,idOrdenCompra, accion){
		
		sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrarOrdenPago.action?idOrdenCompra='+idOrdenCompra+ '&modoPantalla=' +accion+ '&idOrdenPago='+idOrdenPago,targetWorkArea,null);

	} 
	

	
	function editarOrdenPago(idOrdenPago,idOrdenCompra, accion){
		
		sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrarOrdenPago.action?idOrdenCompra='+idOrdenCompra+ '&modoPantalla=' +accion+ '&idOrdenPago='+idOrdenPago,targetWorkArea,null);

	} 
	
	
	
	function cancelarOrdenPago(idOrdenPago,idOrdenCompra, accion){
		sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrarCancelarOrdenPago.action?idOrdenCompra='+idOrdenCompra+ '&modoPantalla=' +accion+ '&idOrdenPago='+idOrdenPago,targetWorkArea,null);
	} 
	function cancelarOrden(idOrdenPago,idOrdenCompra, accion){
		 var formParams = jQuery(document.definirOrdenPagoForm).serialize();
		 var fecha  = dwr.util.getValue("txtFechaCancela");	
		 var comentarios  = dwr.util.getValue("txtComentarios");	
		 var motivo  = dwr.util.getValue("motivoCancelacion");
		 var estatus=  dwr.util.getValue("estatus_h");
		 if (estatus=='Cancelada'){
			 mostrarMensajeInformativo('La orden de Pago ya se encuentra Cancelada' , '20');
			 
		 }else{
			 if (estatus=='Pagada'){
				 mostrarMensajeInformativo('La orden de Pago no puede cancelarse debido a que se encuentra Pagada.' , '20');
				  
			 }else{
				 if (null==fecha || fecha==""){
					 mostrarMensajeInformativo('Debe Capturar Fecha de Cancelacion' , '20');
				 }else{
					 if (null==comentarios || comentarios==""){
						 mostrarMensajeInformativo('Debe Capturar Comentarios' , '20');
					 }else{
						 if (null==motivo || motivo==""){
							 mostrarMensajeInformativo('Debe Seleccionar Motivo Cancelacion' , '20');
						 }else{
							 
								sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/cancelarOrdenPago.action?'+ formParams,targetWorkArea,null);

						 }
						 
					 }
					 
				 }
				 
			 }
		 }

		 

		
	} 
	
	
	
	
	function cerrar(){
			var id = dwr.util.getValue("id");
		 if(confirm("¿Desea cerrar la pantalla? La información no almacenada se perderá." )){
			 		
				sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrar.idOrdenPago?'+id,targetWorkArea,null);
		 }
	}
 
 function consultarFacturasDevueltas(){		
		sendRequestJQ(null, mostrarBusquedaDevoluciones ,targetWorkArea, null);
	}
 
 
	function consultarBloqueoPagos(){
		var url = "/MidasWeb/siniestros/pagos/bloqueo/mostrarContenedor.action";
		sendRequestJQ(null,url,targetWorkArea,'realizarBusqueda(false);');
	}
	

	 
	 function cerrarDetalle(){
		 var id = dwr.util.getValue("id");
		 
		 var lectura = jQuery("#soloLectura").val();
		 if (lectura=='true'){
				sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrar.action?idOrdenPago='+id,targetWorkArea,null);
		 	   	}else{
		 			 if(confirm("¿Desea cerrar la pantalla? La información no almacenada se perderá." )){
		 				sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrar.action?idOrdenPago='+id,targetWorkArea,null);

		 		   	}
		 	   	}
	 }
	 

	 
	 
	
	 function rechazar(){
		 var idordencompra 	= jQuery("#numOrdenCompra").val();
		 var idOrdenPago 	= jQuery("#id").val();
		 
		  cancelarOrdenPago(idOrdenPago,idordencompra, 'cancelar')
		}
	
	 function autorizar(){
		 var txtCostoUnitarioV = dwr.util.getValue("txtCostoUnitario");	
		 var txtPorcIvaV = dwr.util.getValue("txtPorcIva");	
		 var txtIvaV = dwr.util.getValue("txtIva");	
		 var txtIvaRetenidoV = dwr.util.getValue("txtIvaRetenido");	
		 var txtPorIvaRetenidoV = dwr.util.getValue("txtPorIvaRetenido");	
		 var txtIsrV = dwr.util.getValue("txtIsr");	
		 var txtPorIsrV = dwr.util.getValue("txtPorIsr");	
		 var txtTotalV = dwr.util.getValue("txtTotal");	
		// var txtDescuentosV = dwr.util.getValue("txtDescuentos");	
		 var txtDeducibleV = dwr.util.getValue("txtDeducible");	
		 var txtTotalPagarV = dwr.util.getValue("txtTotalPagar");	
		 var txtSalvamentoV = dwr.util.getValue("txtSalvamento");	
		 var txtSalvamentoPorIvaV = dwr.util.getValue("txtSalvamentoPorIva");
		 var txtTotalSalvamentoV = dwr.util.getValue("txtTotalSalvamento");	
		 var idDetalleOrdenCompraV = dwr.util.getValue("idDetalleOrdenCompra");	
		 var idDetalleOrdenPagoV = dwr.util.getValue("idDetalleOrdenPago");	
		 var txtConceptoV= dwr.util.getValue("txtConcepto");
		 removeCurrencyFormatOnTxtInput();
		 var formParams = jQuery(document.definirOrdenPagoForm).serialize();
		 var origenPago 	= jQuery("#origenPagoLis").val();
		 var cveTipoPago = dwr.util.getValue("cveTipoOrdenPago");
		var tipoPago = dwr.util.getValue("tipoOrdenPago");
		var autoriza =false;

		 if(tipoPago=='PP' && (null!=idDetalleOrdenCompraV && idDetalleOrdenCompraV!='')){
				if(null==txtPorcIvaV || txtPorcIvaV==''){
			    	mostrarMensajeInformativo('Registro no almacenado, debe registrar % I.V.A. ', '20');

				}else if (null==txtPorIvaRetenidoV || txtPorIvaRetenidoV==''){
			    	mostrarMensajeInformativo('Registro no almacenado, debe registrar % I.V.A. Retenido', '20');
					
				}else if (null==txtPorIsrV || txtPorIsrV==''){
			    	mostrarMensajeInformativo('Registro no almacenado, debe registrar % I.S.R. ', '20');
					
				}else{
					autoriza=true;
				}
			}else if(tipoPago=='PB' && (null!=idDetalleOrdenCompraV && idDetalleOrdenCompraV!='')){

				if(null==txtSalvamentoV || txtSalvamentoV==''){
			    	mostrarMensajeInformativo('Registro no almacenado, debe registrar Salvamento   ', '20');

				}else if (null==txtSalvamentoPorIvaV || txtSalvamentoPorIvaV==''){
			    	mostrarMensajeInformativo('Registro no almacenado, debe registrar % I.V.A. Salvamento ', '20');
			    }else{
					autoriza=true;			

			    }
			}else {
				autoriza=true;			
			}
		 
		
		 if(cveTipoPago=="GA" && autoriza==true){
			 if(null==origenPago || origenPago==''){
				 mostrarMensajeInformativo('Debe Seleccionar el Origen del Pago' , '20');

			 }else {
					sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/guardarOrdenPago.action?'+ formParams,targetWorkArea,null);

			 }
		 }else{
			 if(autoriza==true){
					sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/guardarOrdenPago.action?'+ formParams,targetWorkArea,null);

			 }
		 }
	}

	 

	 function mostrarHistoricoMovimientos(){
		 
		 var lectura = jQuery("#soloLectura").val();
	 	var id = jQuery("#idReporteCabina").val();
	 	var idcompra = jQuery("#numOrdenCompra").val();
	 	var idPago = jQuery("#id").val();
	 	var accion = jQuery("#modoPantalla").val();
	   	var url = "/MidasWeb/siniestros/cabina/reportecabina/historicomovimientos/mostrarContenedor.action?idToReporte="+id+"&pantallaOrigen=ORDENPAGOS"+"&idOrdenCompra="+idcompra+"&idOrdenPago="+idPago+"&accion="+  accion; 
	   	if (lectura=='true'){
		   	sendRequestJQ(null, url, targetWorkArea, null);
	   	}else{
	   		if(confirm("¿Desea salir de la pantalla? La información no almacenada se perderá." )){
			   	sendRequestJQ(null, url, targetWorkArea, null);

		   	}
	   	}
	   	
	   	
	 }
	 
	 function iniCancelacion(){
		 
		 var tipoPago = dwr.util.getValue("tipoOrdenPago");
		 if(tipoPago=="PP"){
			 jQuery("#divProvedor").show();
			 jQuery("#divBeneficiario").hide();
			
		 }else{
			 if(tipoPago=="PB"){
				 jQuery("#divProvedor").hide();
				 jQuery("#divBeneficiario").show();
				
			 }

			 
		 }
	 }
	 
	 function iniDetarGrid(){
		 
		 var estatusId = dwr.util.getValue("estatus");

		 if(null==estatusId || estatusId==''){

			 jQuery("#btnRechazar").hide();
		 }
		 
		 var accion = dwr.util.getValue("modoPantalla");
		 if(accion=="consultar"){
			 jQuery("#btnRechazar").hide();
			 jQuery("#btnAutorizar").hide();
		 }
		 var tipoPago = dwr.util.getValue("tipoOrdenPago");
		 if(tipoPago=="PP"){
			 jQuery("#divProvedor").show();
			 jQuery("#divBeneficiario").hide();
			 jQuery("#divFactura").show();
			 jQuery("#divSalvamentos").hide();
			 jQuery("#tituloPago").html("Datos de Pago del Proveedor");
		 }else{
			 if(tipoPago=="PB"){
				 jQuery("#divProvedor").hide();
				 jQuery("#divBeneficiario").show();
				 jQuery("#divFactura").hide();
				// jQuery("#aplicaDeducibles").hide();
				 jQuery("#divIVAS").show(); //Se muestra Porcentajes
				 //jQuery("#contenedorConcepto").height( 265 );
				 //jQuery("#contenedorTotales").height( 100 );
				 jQuery("#divSalvamentos").show();
				 jQuery("#tituloPago").html( "Datos de Pago del Beneficiario" );
				 
			 }
		}
		 var cveTipoPago = dwr.util.getValue("cveTipoOrdenPago");
			 if(cveTipoPago=="GA"){
				 jQuery("#divDeducible").hide();
				 jQuery("#aplicaDeducibles").hide();
				 jQuery("#divReserva").hide();
				 jQuery("#divReservaDisp").hide();
				 jQuery("#divOrigenPago").show();
				 jQuery("#pagoDiv").hide();
				 jQuery("#contenedorConcepto").height( 310  );
				 jQuery("#contenedorTotales").height( 60 );

				 
				 
				  
			 }else{
				 if(cveTipoPago=="OC"){
					 jQuery("#divOrigenPago").hide();
					 jQuery("#pagoDiv").show();
				 }
			 }
	 }
	 
	 
	 var indexId;
	 var selecciono;
	 function iniContenedoDetarGrid(){
	 	iniDetarGrid();
	 	selecciono=false;
	 	var formParams = jQuery(document.definirOrdenPagoForm).serialize();
		document.getElementById("conceptosGrid").innerHTML = '';
		listaConceptosGrid = new dhtmlXGridObject("conceptosGrid");
		listaConceptosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		listaConceptosGrid.attachEvent("onXLE", function(grid_obj){
			unblockPage();		
			if(selecciono==true){
				actualizaDetalle();
			}			
		});				
		listaConceptosGrid.load( '/MidasWeb/siniestros/pagos/pagosSinestro/buscarDetalleCompra.action?'+formParams);
	 	
	 }
	 
	 function actualizaDetalle(){
		 if(indexId && indexId!=''){
			 jQuery("#txtCostoUnitario").val( listaConceptosGrid.cells(indexId,5).getValue() );
			 
			 
			 jQuery("#txtPorcIva").val( listaConceptosGrid.cells(indexId,3).getValue() );
			 jQuery("#txtIva").val( listaConceptosGrid.cells(indexId,4).getValue() );
			 jQuery("#txtPorIvaRetenido").val( listaConceptosGrid.cells(indexId,6).getValue() );
			 jQuery("#txtIvaRetenido").val( listaConceptosGrid.cells(indexId,7).getValue() );
			 jQuery("#txtPorIsr").val( listaConceptosGrid.cells(indexId,8).getValue() );
			 jQuery("#txtIsr").val( listaConceptosGrid.cells(indexId,9).getValue() );
			 jQuery("#txtTotal").val( listaConceptosGrid.cells(indexId,11).getValue() );
			 jQuery("#txtDescuentos").val( listaConceptosGrid.cells(indexId,12).getValue() );
			 jQuery("#txtDeducible").val( listaConceptosGrid.cells(indexId,13).getValue() );
			 jQuery("#txtTotalPagar").val( listaConceptosGrid.cells(indexId,14).getValue() );
			 jQuery("#txtSalvamento").val( listaConceptosGrid.cells(indexId,15).getValue() );
			 jQuery("#txtSalvamentoPorIva").val( listaConceptosGrid.cells(indexId,16).getValue() );
			 jQuery("#txtTotalSalvamento").val( listaConceptosGrid.cells(indexId,17).getValue() );
			 jQuery("#idDetalleOrdenCompra").val( listaConceptosGrid.cells(indexId,18).getValue() );
			 jQuery("#idDetalleOrdenPago").val( listaConceptosGrid.cells(indexId,19).getValue() );
			 jQuery("#txtConcepto").val( listaConceptosGrid.cells(indexId,1).getValue() );
			 jQuery("#txtTotalesPagados").val( listaConceptosGrid.cells(indexId,20).getValue() );
			 jQuery("#txtTotalesPorPagar").val( listaConceptosGrid.cells(indexId,21).getValue() );
			 jQuery("#txtDeduciblesTotal").val( listaConceptosGrid.cells(indexId,22).getValue() );
			 jQuery("#index").val( listaConceptosGrid.cells(indexId,23).getValue() );
			 jQuery("#txtImportePagado").val( listaConceptosGrid.cells(indexId,24).getValue() );
			 jQuery("#txtReserva").val( listaConceptosGrid.cells(indexId,25).getValue() );
			 jQuery("#TxtreservaDisponible").val( listaConceptosGrid.cells(indexId,26).getValue() );
		 }
	 
	 }
	 
	 
	 
	 function calculaTotal(){
		 var subtotal = dwr.util.getValue("txtCostoUnitario");	
		 var iva = dwr.util.getValue("txtIva");	
		 var ivaRetenido = dwr.util.getValue("txtIvaRetenido");	
		 var isr = dwr.util.getValue("txtIsr");
		var total= parseFloat(subtotal) + parseFloat(iva) - parseFloat(ivaRetenido) - parseFloat(isr);
		jQuery("#txtTotal").val(total);
		calculaTotalPagar();
		
	 }
	 function calculaTotalPagar(){
		 var total = dwr.util.getValue("txtTotal");	
		 var pagado = dwr.util.getValue("txtImportePagado");
		 var totalPagar= parseFloat(total) - parseFloat(pagado)
		 jQuery("#txtTotalPagar").val(totalPagar);
	
		
	 }
	 
	 
	 function calculaIvaImporte(){
			var costo = dwr.util.getValue("txtCostoUnitario");	
			var ivaPor = dwr.util.getValue("txtPorcIva");	
			isValidado= true;
			if (null==costo || costo==""){
				isValidado= false;
			}
			if (null==ivaPor || ivaPor==""){
				isValidado= false;
			}else{
				if(ivaPor >100 || ivaPor<0 ){
					isValidado= false;
			    	mostrarMensajeInformativo('El %IVA debe ser en un rango de 0% a 100%', '20');
			    	jQuery("#txtPorcIva").val(0);
				}
			if(isValidado==true){
				var iva = (ivaPor/100)*costo;
				 jQuery("#txtIva").val(iva);
				 calculaTotal();
			}
				
			}
		}
	 
	 function calculaIvaRetenido(){
			var costo = dwr.util.getValue("txtCostoUnitario");	
			var ivaPor = dwr.util.getValue("txtPorIvaRetenido");	
			isValidado= true;
			if (null==costo || costo==""){
				isValidado= false;
			}
			if (null==ivaPor || ivaPor==""){
				isValidado= false;
			}else{
				if(ivaPor >100 || ivaPor<0 ){
					isValidado= false;
			    	mostrarMensajeInformativo('El %IVA debe ser en un rango de 0% a 100%', '20');
			    	jQuery("#txtPorIvaRetenido").val(0);
				}
			if(isValidado==true){
				var iva = (ivaPor/100)*costo;
				 jQuery("#txtIvaRetenido").val(iva);
				 calculaTotal();
			}
				
			}
		}
	 
	 function calculaIvaISR(){
			var costo = dwr.util.getValue("txtCostoUnitario");	
			var ivaPor = dwr.util.getValue("txtPorIsr");	
			isValidado= true;
			if (null==costo || costo==""){
				isValidado= false;
			}
			if (null==ivaPor || ivaPor==""){
				isValidado= false;
			}else{
				if(ivaPor >100 || ivaPor<0 ){
					isValidado= false;
			    	mostrarMensajeInformativo('El Porcentaje debe ser en un rango de 0% a 100%', '20');
					 jQuery("#txtPorIsr").val(0);

				}
			if(isValidado==true){
				var iva = (ivaPor/100)*costo;
				 jQuery("#txtIsr").val(iva);
				 calculaTotal();
			}
				
			}
		}
	 
	 
	 
	 
	 function calculaIvaSalvamento(){
			var costo = dwr.util.getValue("txtSalvamento");	
			var ivaPor = dwr.util.getValue("txtSalvamentoPorIva");	
			isValidado= true;
			if (null==costo || costo==""){
				isValidado= false;
			}
			if (null==ivaPor || ivaPor==""){
				isValidado= false;
			}else{
				if(ivaPor >100 || ivaPor<0 ){
					isValidado= false;
			    	mostrarMensajeInformativo('El Porcentaje debe ser en un rango de 0% a 100%', '20');
					 jQuery("#txtSalvamentoPorIva").val(0);

				}
			if(isValidado==true){
				var iva = (ivaPor/100)*costo;
				 jQuery("#txtTotalSalvamento").val(iva);
			}
				
			}
	}

	function bloquearPago(idOrdenPago){
		console.log('consultarPagoBloqueado :'+idOrdenPago);
	 	var url = '/MidasWeb/siniestros/pagos/bloqueo/bloquearDesdePagos.action?idPago='+idOrdenPago+'&esDeOrdenesDePago=true';
	 	sendRequestJQ(null, url, targetWorkArea, null);	
	}
	 
	 

	 function registarFactura(idOrdenCompra){
			var url = 	mostrarRegistroFactura + '?idOrdenCompra='+ idOrdenCompra;
			sendRequestJQ(null, url ,targetWorkArea, null);
		 }
		 
	 function devolverFactura(idOrdenCompra){
		var url = 	mostrarDevolucionFactura + 
			'?idOrdenCompra='+ idOrdenCompra +
			'&bandejaPrincipal=' + false;
		
		sendRequestJQ(null, url ,targetWorkArea, null);
		 }
	
	 
	 
	 function  generarOrdenPago(idOrdenCompra){
			sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/generacionAutomaticaOrdenPago.action?idOrdenCompra='+idOrdenCompra,targetWorkArea,null);

	 }
	 
		
