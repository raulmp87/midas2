
var ordenesCompraListadoGrid;
var listadoGridAgrupadoresGrid;
var listadoCompanias = new Array();	


var COLUMN_CHEKS = 0;
var COLUMN_SUBTOTAL = 6;
var COLUMN_IVA = 7;
var COLUMN_IVA_RET = 8;
var COLUMN_ISR = 9;
var COLUMN_TOTAL = 10;



function inicializa(){

	var idFactura = jQuery("#h_idAgrupador").val();
	jQuery('#t_nombreAgrupador').attr('disabled','disabled');
	
	if( idFactura != ''){
		cargarOrdenesDeFactura();
		ordenesCompraListadoGrid.checkAll(true);
		ordenesCompraListadoGrid.setColumnHidden(COLUMN_CHEKS,true);
		jQuery('.escondible').attr('disabled','disabled');
		// CONCATENA ID DE PROVEEDOR Y NOMBRE PARA MOSTRAR EN MODO CONSULTA
		jQuery("#companiaAutoComplete").attr("value",jQuery("#companiaId").val()+" "+jQuery("#nombreCompletoCia").val());
	}else{
		buscarOrdenesCompra();	
	}
}



function cargarOrdenesDeFactura(){
	console.log('cargarOrdenesDeFactura');
	var url = listarOrdenesCompraPath + "?factura.id="+jQuery("#h_idAgrupador").val(); ;
	cargaGridOrdenesDeCompra(url);
}



function buscarOrdenesCompra(){
	console.log('buscarOrdenesDeCompra');
	mostrarIndicadorCarga('indicador');
	var form = jQuery('#recepcionFacturaForm').serialize();
	console.log('forma agrupador: '+form);
	var url = listarOrdenesCompraPath + "? " + form;
	cargaGridOrdenesDeCompra(url);
	
}

function cargaGridOrdenesDeCompra(url){
	document.getElementById("ordenesCompraListadoGrid").innerHTML = '';
	ordenesCompraListadoGrid = new dhtmlXGridObject('ordenesCompraListadoGrid');

	ordenesCompraListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		
		var idFactura = jQuery("#h_idAgrupador").val();
		if( idFactura != ''){
			actualizaMontos();
		}else{
			inicializaEnCeros();
		}

    });

	//Para que se desplieguen las opciones del combo con 1 solo click
	ordenesCompraListadoGrid.enableEditEvents(true);
	ordenesCompraListadoGrid.attachHeader("&nbsp ,#text_filter,#text_filter,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,#select_filter,#text_filter,#text_filter");
	ordenesCompraListadoGrid.attachEvent("onCheck", function(rId,cInd,state){
    	console.log(" rId ="+rId+", cInd ="+cInd+",state ="+state);
    	calculaMontos(rId,state);

	});

	ordenesCompraListadoGrid.load(url);
}






function calculaMontos(rowId,state){
	//var subtotal = ordenesCompraListadoGrid.cellById(rowId,COLUMN_SUBTOTAL).getValue();
	//var iva = ordenesCompraListadoGrid.cellById(rowId,COLUMN_IVA).getValue();
	//var ivaRet = ordenesCompraListadoGrid.cellById(rowId,COLUMN_IVA_RET).getValue();
	//var isr = ordenesCompraListadoGrid.cellById(rowId,COLUMN_ISR).getValue();
	var total = ordenesCompraListadoGrid.cellById(rowId,COLUMN_TOTAL).getValue();

	//var subtotalActual =  parseFloat(jQuery("#t_subtotal").val());
	//var ivaActual =  parseFloat(jQuery("#t_iva").val());
	//var ivaRetActual = parseFloat(jQuery("#t_iva_retenido").val());
	//var isrActual =  parseFloat(jQuery("#t_isr").val());
	var totalActual = parseFloat(jQuery("#t_total").val());
	
	var operador = 1;
	
	if(!state){
		operador = -1			
	}
	
	//jQuery("#t_subtotal").val(subtotalActual +  operador*subtotal);
	//jQuery("#t_iva").val(ivaActual +  operador* iva);
	//jQuery("#t_iva_retenido").val(ivaRetActual +  operador* ivaRet);
	//jQuery("#t_isr").val(isrActual +  operador* isr);
	jQuery("#t_total").val(totalActual +  operador* total);

}

function inicializaEnCeros(){
	//jQuery("#t_subtotal").val(0);
	//jQuery("#t_iva").val(0);
	//jQuery("#t_iva_retenido").val(0);
	//jQuery("#t_isr").val(0);
	jQuery("#t_total").val(0);
}

function guardar(){
	if(validaInfoAGuardar()){
		var idsOrdenesCompra = ordenesCompraListadoGrid.getCheckedRows(COLUMN_CHEKS);
		var url = guardarPath + '?oficinaSeleccionada='+jQuery("#s_oficinasList").val()+'&idsOrdenesCompra='+idsOrdenesCompra+'&idPrestadorServicio='+jQuery("#companiaId").val()+'&factura.nombreAgrupador='+jQuery("#t_nombreAgrupador").val()+'&companiaSeleccionada='+jQuery("#companiaId").val()+'&nombreCompletoCia='+jQuery("#nombreCompletoCia").val()+'&filtroAgrupador.tipoAgrupador='+jQuery("#s_tipoAgrupadorList").val();
		sendRequestJQ(null, url, targetWorkArea, null);	
	}
}


function eliminar(){
	if(confirm("¿Esta seguro que se quiere eliminar el agrupador?")){
		var url = eliminarPath + '?factura.id='+jQuery("#h_idAgrupador").val();
		sendRequestJQ(null, url, targetWorkArea, null);		
	}
	
}

function mostrarRegistro(){
	var url = mostrarRegistroPath;
	sendRequestJQ(null, url, targetWorkArea, null);	
	
}

function validaInfoAGuardar(){
	var idsOrdenesCompra = ordenesCompraListadoGrid.getCheckedRows(COLUMN_CHEKS);
	if(idsOrdenesCompra == ''){
		mostrarMensajeInformativo('Se deben de seleccionar ordenes de compra', '10');  
		return false;
	}
	return true;

}


function borrar(){
	console.log('Borra Agrupador');
}



function actualizaMontos(){
	
	//console.log('ACTUALIZA MONTOS');
	//jQuery("#t_subtotal").val(sumaColumnaGrid (COLUMN_SUBTOTAL));
	//console.log('iva');
	//jQuery("#t_iva").val(sumaColumnaGrid (COLUMN_IVA));
	//console.log('ivaRet');
	//jQuery("#t_iva_retenido").val(sumaColumnaGrid (COLUMN_IVA_RET));
	//console.log('isr');
	//jQuery("#t_isr").val(sumaColumnaGrid (COLUMN_ISR));
	//console.log('total');
	jQuery("#t_total").val(sumaColumnaGrid (COLUMN_TOTAL));
	console.log('fin');
}

function sumaColumnaGrid(numColumn){
	var sum=0;                                     
	ordenesCompraListadoGrid.forEachRow(function(id){                   
  		sum+=ordenesCompraListadoGrid.cellById(id,numColumn).getValue();     
	});
	return sum;
}

function nuevoAgrupador(){
	console.log('Entra nuevoAgrupador');
}

function limpiarFormulario(){
	console.log('Entra limpiarFormulario');
	jQuery('#buscarAgrupadoresForm').each (function(){
		  this.reset();
	});
}

function buscarAgrupador(){
	console.log('Entra buscarAgrupador');
	if(validarForma()){
		var form = jQuery('#buscarAgrupadoresForm').serialize();
		console.log('Form: '+form);
		var url = buscarAgrupadoresPath+'?'+form;
		cargaGridRecuperadores(url);
		

	}else{
		mostrarMensajeInformativo('Se necesita seleccionar al menos un elemento', '10');	
	}
}

function cargaGridVacio(){
	cargaGridRecuperadores(buscarAgrupadoresPath);
}

function validarForma(){
	var contadorElementos = 0;
	jQuery(".obligatorio").each(function(index, value) { 
	    if( jQuery(this).val() != "" ){
	    	contadorElementos++;
	    }
	});
	if(contadorElementos > 0){
		return  true;
	}
	return false;
}

function cargaGridRecuperadores(url){
	console.log('cargaGridRecuperadores');
	document.getElementById("listadoGridAgrupadoresGrid").innerHTML = '';
	listadoGridAgrupadoresGrid = new dhtmlXGridObject('listadoGridAgrupadoresGrid');

	listadoGridAgrupadoresGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
    listadoGridAgrupadoresGrid.load(url);
}

function mostrarBusqueda(){
	var url = mostrarPantallaBusquedaPath;
	sendRequestJQ(null, url, targetWorkArea, null);	
	
}

function consultar(idAgrupador){
	var url = mostrarRegistroPath+'?factura.id='+idAgrupador;
	sendRequestJQ(null, url, targetWorkArea, null);	
	
}

function habilitaAutoComplete(){
	 
	listadoService.getMapPrestadorPorTipo( 'CIA' ,function(data){
				
		var contador=0;
		jQuery.each( data, function( key, value ) { 
			console.log("CIA: kye: "+key+" - val: "+value);
			listadoCompanias[contador] = { "label":'   '+value  , "id": key} ;
			contador++;
		});

		jquery143("#companiaAutoComplete").autocomplete({
			source: listadoCompanias.size() > 0 ? listadoCompanias : function(request, response){},
			minLength: 3,
			delay: 500,
			select: function(event, ui) {
				
				jQuery("#companiaId").val(ui.item.id);
				jQuery("#nombreCompletoCia").val(ui.item.label);
			},
			search: function(event, ui) {
				//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
				var busqueda = jQuery("#companiaAutoComplete").val();
				if(findItem(busqueda.toString().toLowerCase(), listadoCompanias).length == 0){
					//alert(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"));
				    mostrarMensajeInformativo(("Compañia ("+busqueda.toString().toUpperCase()+") no existe"), '20');
				    jQuery("#companiaAutoComplete").val("");
				    jQuery("#companiaId").val("");
				    return false;
				}
			}
		});
	});
	 
}


//-------- Inicia funcionalidad Autcomplete---------
//var jquery143;
//var listaPrestadoresServicioOC ;
function findItem (term, devices) {
	
	var items = [];
for (var i=0;i<devices.length;i++) {
    var item = devices[i];
    for (var prop in item) {
        var detail = item[prop].toString().toLowerCase();           
        if (detail.indexOf(term)>-1) {
            items.push(item);
            break;               
        }
    }
}
return items; 
}

jquery143(function(){
	jquery143("#companiaAutoComplete").bind("keypress", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();         
	        return false;
	    }
	});
});


