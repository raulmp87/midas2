/**
 * 
 */
var ordenesCompraListadoGrid;
var facturasListadoGrid;
var idRowFacturaDeselected = "";
var COLUMNINDEX_OCL_RELACIONAR = 9;
var COLUMNINDEX_OCL_TOTAL = 7;
var COLUMNINDEX_OCL_IDORDENCOMPRA = 1;
var COLUMNINDEX_FL_TOTAL = 6;
var COLUMNINDEX_FL_MONTORESTANTE = 8;

//----- Inicia funcionalidad Cargar archivo ----

function cargarZipFacturas(){
  
	try {

		var ext
	
	
	if(jQuery("#idPrestadorServicio").val() != '')
	{		
		if(dhxWins != null) 
			dhxWins.unload();
	
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
		adjuntarDocumento.setText("Carga de Facturas");
		adjuntarDocumento.button("minmax1").hide();
		adjuntarDocumento.button("park").hide();
		adjuntarDocumento.setModal(true);
		adjuntarDocumento.center();
		adjuntarDocumento.denyResize();
		adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
			
	
		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setFilesLimit(1);
	    
	    vault.onAddFile = function(fileName) { 
	    	ext = this.getFileExtension(fileName); 
	    	if (ext != "zip" && ext != "xml") { 
	    		mostrarMensajeInformativo("Solo se pueden cargar Archivos XML o ZIP",'20'); 
	    		return false; 
	    	} else 
	    		return true; 
	        
	     }; 
	     
	    vault.create("vault");
	    vault.setFormField("claveTipo", "16");
	        
	    vault.onBeforeUpload = function(files){
	    	jQuery("#file1").attr("name", "zipFacturas");
	    }
	    
	    jQuery("#buttonUploadId").unbind("click");
	    jQuery("#buttonUploadId").click(function(){
	        
	    	blockPage();
			jQuery.ajaxFileUpload({
				url: "/MidasWeb/siniestros/pagos/facturas/recepcionFacturas/cargarArchivoFacturas.action?idPrestadorServicio=" 
					+ jQuery("#idPrestadorServicio").val()
					+ "&extensionArchivo=" + ext,
				secureuri:false,
				fileElementId: "file1",
				dataType: 'text',					
				success: function(data){
					
					unblockPage();
					parent.dhxWins.window("divContentForm").close();
					
					var respuesta = JSON.parse(jQuery(data).text()); 
					
					if( respuesta.tipoMensaje == 10 ){
						mostrarMensajeInformativo(respuesta.mensaje, '10');
					}else if(respuesta.numFacturasCargadas == 0)
					{
						mostrarMensajeInformativo("No se cargo ninguna factura en el sistema, " +
								"revise que el RFC de las facturas es correcto o que las facturas no han sido previamente registradas", '20');					
					}
						
					var url = "/MidasWeb/siniestros/pagos/facturas/recepcionFacturas/mostrarRecepcionFacturas.action?idBatch=" + respuesta.idBatch+"&idPrestadorServicio=" 
					+ jQuery("#idPrestadorServicio").val() + "&numFacturasCargadas=" + respuesta.numFacturasCargadas;
					
					sendRequestJQ(null,url,targetWorkArea,null);				
				},
				error: function(data){
					unblockPage()
					parent.dhxWins.window("divContentForm").close();
					mostrarMensajeInformativo('Ha ocurrido un error al intentar subir el archivo ZIP', '20');			
				}
			});
		});
    
	}
	else
	{
		mostrarMensajeInformativo('Debe seleccionar un proveedor antes de realizar la carga del archivo ZIP con las facturas', '20');
	}
	
	}catch(err) {
	    //document.getElementById("demo").innerHTML = err.message;
	}
}


jQuery.extend({
    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout		
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})
//----- Termina funcionalidad Cargar archivo ----

//-------- Inicia funcionalidad Autcomplete---------

function findItem (term, devices) {
	
	var items = [];
    for (var i=0;i<devices.length;i++) {
        var item = devices[i];
        for (var prop in item) {
            var detail = item[prop].toString().toLowerCase();           
            if (detail.indexOf(term)>-1) {
                items.push(item);
                break;               
            }
        }
    }
    return items;
}

jquery143(function(){
	jquery143("#nombrePrestadorServicio").autocomplete({
        source: listaPrestadoresServicio.size() > 0 ? listaPrestadoresServicio : function(request, response){
        	        	
        },
        minLength: 3,
        delay: 500,
        select: function(event, ui) {
        	// se agrega validacion para cargar la pantalla completa cuando se cambie de proveedor
        	if (facturasListadoGrid.getRowsNum() > 0) {
        		mostrarMensajeConfirm('Está seguro que desea cambiar el proveedor, se perderán aquellas facturas que aún no han sido registradas?', '20', 'limpiarProveedor()', 'recargarPantalla()', null);
			}
        	else{
        	jQuery("#idPrestadorServicio").val(ui.item.id);         	
        	buscarOrdenesCompra(); //se buscan las ordenes de compra del proveedor //Revisar invocacion segunda vez
        	actualizarOficina();
        	}
        	
        },
        search: function(event, ui) {
        	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
            var busqueda = jQuery("#nombrePrestadorServicio").val();
            if(findItem(busqueda.toString().toLowerCase(), listaPrestadoresServicio).length == 0){
            	//alert(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"));
            	mostrarMensajeInformativo(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"), '20');
            	jQuery("#nombrePrestadorServicio").val("");
            	return false;
            }
        }
    });
});

jquery143(function(){
	jquery143("#nombrePrestadorServicio").bind("keypress", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();         
	        return false;
	    }
	});
});

//-------- termina funcionalidad Autocomplete

function buscarOrdenesCompra()
{
	mostrarIndicadorCarga('indicador');
	var form = jQuery('#recepcionFacturaForm').serialize();
	
	var url = listarOrdenesCompraPath + "? " + form;		
	
	document.getElementById("ordenesCompraListadoGrid").innerHTML = '';	
	ordenesCompraListadoGrid = new dhtmlXGridObject('ordenesCompraListadoGrid');
		
	ordenesCompraListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		ordenesCompraListadoGrid.getColumnCombo(COLUMNINDEX_OCL_RELACIONAR).attachEvent("onBlur", function(){
			
			actualizarMontoRestanteFactura(ordenesCompraListadoGrid.getColumnCombo(COLUMNINDEX_OCL_RELACIONAR).getSelectedValue(), 
					idRowFacturaDeselected, 
					ordenesCompraListadoGrid.getSelectedRowId());   
			return false;
		});
		
		ordenesCompraListadoGrid.getColumnCombo(COLUMNINDEX_OCL_RELACIONAR).attachEvent("onOpen", function(){
		     idRowFacturaDeselected = ordenesCompraListadoGrid.getColumnCombo(COLUMNINDEX_OCL_RELACIONAR).getActualValue();
		     return false;
		});
		
    });		
	
	//Para que se desplieguen las opciones del combo con 1 solo click
	ordenesCompraListadoGrid.enableEditEvents(true); 
	ordenesCompraListadoGrid.attachHeader("#text_filter,#text_filter,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,#select_filter,#text_filter");
	ordenesCompraListadoGrid.attachEvent("onMouseOver", function(id,ind){
		this.cells(id,COLUMNINDEX_OCL_RELACIONAR).cell.title = 'Seleccione el número de factura con el que desea asociar la orden de compra';
		return false;		
    }); 	
	
	ordenesCompraListadoGrid.load(url);	
}

function buscarFacturas()
{
	mostrarIndicadorCarga('indicador');
	var form = jQuery('#recepcionFacturaForm').serialize();	
	
	var url = listarFacturasPath + "? " + form ;	

	document.getElementById("facturasListadoGrid").innerHTML = '';	
	facturasListadoGrid = new dhtmlXGridObject('facturasListadoGrid');
	
	facturasListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		
		
		
    });	
	facturasListadoGrid.enableEditEvents(true); 
	facturasListadoGrid.attachHeader("#text_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,#select_filter,&nbsp,&nbsp,&nbsp");

	
	facturasListadoGrid.load(url);	
	
}


function verDetalleValidacion(idValidacionFactura, numeroFactura)
{	
	var url = verDetalleValidacionesPath + '?' +  "idValidacionFactura=" + idValidacionFactura;
	
	mostrarVentanaModal("DetValidacionesFactura", 'Detalle Validaciones Factura No. ' + numeroFactura, 50, 200, 700, 300, url);
}

function validarRegistrar()
{
	var relaciones = "";
	var contador = 0;	
		
	ordenesCompraListadoGrid.forEachRow(function(id){
		
		if(ordenesCompraListadoGrid.cells(id,COLUMNINDEX_OCL_RELACIONAR).getValue() != "")
		{
			relaciones = relaciones.concat("&listaFacturaOrdenCompra[" + contador + "].idOrdenCompra=" + 
					(ordenesCompraListadoGrid.cells(id,COLUMNINDEX_OCL_IDORDENCOMPRA).getValue()
					+ "&listaFacturaOrdenCompra[" + contador + "].idFactura=" 
					+ ordenesCompraListadoGrid.cells(id,COLUMNINDEX_OCL_RELACIONAR).getValue()));
			contador ++;
		}		
		
	});	
	
	if(relaciones != "")
	{
		sendRequestJQ(null,validarRegistrarPath + "?idBatch=" + jQuery("#idBatch").val() + "&idPrestadorServicio=" + jQuery("#idPrestadorServicio").val() 
				+ "&" + relaciones,targetWorkArea,null);		
	}else
	{
		mostrarMensajeInformativo("Debe relacionar por lo menos 1 factura con una orden de compra para continuar con la validación", '20');		
	}
		
		
}

function actualizarOficina()
{
	var idPrestadorServicio = jQuery("#idPrestadorServicio").val();
	
	if(idPrestadorServicio != ""){
		var url= obtenerOficinaPath;
		var data={"idPrestadorServicio":idPrestadorServicio};	
		
		jQuery.ajax({
    		"url" : obtenerOficinaPath,
    		"data" : data,
    		"dataType" : "text json",
    		"success": function(data) { 
    			cargarOficina(JSON.parse(data));
            }
    	});
		
		
	}	
}

function cargarOficina(json){
	if(json.oficinaPrestadorServicio.id !=null && json.oficinaPrestadorServicio.nombreOficina){
		
		jQuery("#idOficina").val(json.oficinaPrestadorServicio.id);
		jQuery("#nombreOficina").val(json.oficinaPrestadorServicio.nombreOficina);
	}
}


function limpiarProveedor()
{	
	jQuery('#nombrePrestadorServicio').val(''); 
	jQuery('#idPrestadorServicio').val(''); 
	jQuery('#nombreOficina').val(''); 
	jQuery('#idOficina').val('');
	jQuery('#idBatch').val('');
	jQuery('#numFacturasCargadas').val('0');
	
	buscarOrdenesCompra();
	buscarFacturas();
}

function actualizarMontoRestanteFactura(facturaRowId, facturaOldRowId, ordenCompraRowId)
{
	var montoOrdenCompra = ordenesCompraListadoGrid.cells(ordenCompraRowId,COLUMNINDEX_OCL_TOTAL).getValue();
	var montoFacturaRestante;
	
	if(facturaOldRowId != facturaRowId)
	{
		if(facturaRowId != "")
		{
			montoFacturaRestante = facturasListadoGrid.cells(facturaRowId,COLUMNINDEX_FL_MONTORESTANTE).getValue();	
			facturasListadoGrid.cells(facturaRowId,COLUMNINDEX_FL_MONTORESTANTE).setValue(montoFacturaRestante - montoOrdenCompra);
			if(facturasListadoGrid.cells(facturaRowId,COLUMNINDEX_FL_TOTAL).getValue() == facturasListadoGrid.cells(facturaRowId,COLUMNINDEX_FL_MONTORESTANTE).getValue())
			{
				facturasListadoGrid.setCellTextStyle(facturaRowId,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:normal;"); 		
			}else
			{
				facturasListadoGrid.setCellTextStyle(facturaRowId,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:bold;");
			}			
			
		}
		
		if (facturaOldRowId != "")
		{ 
			montoFacturaRestante = facturasListadoGrid.cells(facturaOldRowId,COLUMNINDEX_FL_MONTORESTANTE).getValue();
			facturasListadoGrid.cells(facturaOldRowId,COLUMNINDEX_FL_MONTORESTANTE).setValue(montoFacturaRestante + montoOrdenCompra);
			if(facturasListadoGrid.cells(facturaOldRowId,COLUMNINDEX_FL_TOTAL).getValue() == facturasListadoGrid.cells(facturaOldRowId,COLUMNINDEX_FL_MONTORESTANTE).getValue())
			{
				facturasListadoGrid.setCellTextStyle(facturaOldRowId,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:normal;"); 		
			}else
			{
				facturasListadoGrid.setCellTextStyle(facturaOldRowId,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:bold;");
			}			
		}
		
	}		
}

function asignarTodas(rowIdFactura)
{
	var montoFactura = facturasListadoGrid.cells(rowIdFactura,COLUMNINDEX_FL_TOTAL).getValue();
	var montoTotalOrdenesPago = 0;
	
	for (var i=0; i<ordenesCompraListadoGrid.getRowsNum(); i++){
		   if(ordenesCompraListadoGrid.cellByIndex(i,COLUMNINDEX_OCL_RELACIONAR).getValue() == '')
		   {
			   ordenesCompraListadoGrid.cellByIndex(i,COLUMNINDEX_OCL_RELACIONAR).setValue(rowIdFactura);
		   }		   
		};	
	
	ordenesCompraListadoGrid.forEachRow(function(id){		
		if(ordenesCompraListadoGrid.cells(id,COLUMNINDEX_OCL_RELACIONAR).getValue() == rowIdFactura)
		{
			montoTotalOrdenesPago = montoTotalOrdenesPago + ordenesCompraListadoGrid.cells(id,COLUMNINDEX_OCL_TOTAL).getValue(); 			
		}				
	});	
	
	facturasListadoGrid.cells(rowIdFactura,COLUMNINDEX_FL_MONTORESTANTE).setValue(montoFactura - montoTotalOrdenesPago);
	if(facturasListadoGrid.cells(rowIdFactura,COLUMNINDEX_FL_TOTAL).getValue() == facturasListadoGrid.cells(rowIdFactura,COLUMNINDEX_FL_MONTORESTANTE).getValue())
	{
		facturasListadoGrid.setCellTextStyle(rowIdFactura,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:normal;"); 		
	}else
	{
		facturasListadoGrid.setCellTextStyle(rowIdFactura,COLUMNINDEX_FL_MONTORESTANTE,"font-weight:bold;");
	}
}

// se crea funcion para recargar pantalla cuando la respuesta del mensaje de confirmacion de recargar sea negativa.
function recargarPantalla()
{
	var url = "/MidasWeb/siniestros/pagos/facturas/recepcionFacturas/mostrarRecepcionFacturas.action?idBatch=" + jQuery("#idBatch").val()+"&idPrestadorServicio=" 
	+ jQuery("#idPrestadorServicio").val() + "&numFacturasCargadas=" + facturasListadoGrid.getRowsNum();
	
	sendRequestJQ(null,url,targetWorkArea,null);
}

