	function cerrar(){
		sendRequestJQ(null, cerrarRegistroFactura ,targetWorkArea, null);
	}
	 

	function guardarFactura(){
			if( validaDatosRequeridos() ){
				if( validate_fechaMayorQueRegistro()){
				var url = 	guardarRegistroFactura + '?' + jQuery(document.registroFactura).serialize();
				sendRequestJQ(null, url ,targetWorkArea, null);
				}
			}
		 }
	
	
	function devolucionFactura(){
		if( validaDatosRequeridos() ){
			var url = 	guardarDevolucionFactura + '?' + jQuery(document.devolucionFactura).serialize();
			sendRequestJQ(null, url ,targetWorkArea, null);
		}
	 }

	function devolverFactura(){
		if( validaDatosRequeridos() ){
			if(validate_fechaMayorQueDevolucion()){
				var url = 	guardarDevolucionFactura + '?' + jQuery(document.devolucionFactura).serialize();
				sendRequestJQ(null, url ,targetWorkArea, null);
		 }
		}
	 }
	
	function edicionDevolucionFactura(){
		if( validaDatosRequeridos() ){
			var url = 	editarDevolucionFactura + '?' + jQuery(document.devolucionFactura).serialize();
			sendRequestJQ(null, url ,targetWorkArea, null);
		}
	 }
	
	
	
	function validarFacturaToDevolucion(){
		var validacionDevolucionFactura = jQuery("#validacionDevolucionFactura").val();
		var idOrdenCompra = jQuery("#idOrdenCompra").val();
		
		if(validacionDevolucionFactura =='true'){
			var resultado = confirm("El monto total de la Orden de Compra, No coincide con el monto de la factura. ¿Desea registrar la factura para devolucion?"); 
			if(resultado){
				var url = 	mostrarDevolucionFactura + 
					'?idOrdenCompra='+ idOrdenCompra +
					'&bandejaPrincipal=' + false;
				sendRequestJQ(null, url ,targetWorkArea, null);
			}
		}
		
		
	}
	
	
 /* ***************************************************************************************************************************************************************************** */ 
 /* ******************************************************* LISTADO HISTORIAL DEVOLUCIONES ORDENES COMPRE ********************************************************************** */
 /* ***************************************************************************************************************************************************************************** */
 
	var historialDevolucionesOrdenCompra;
	
	
	 function initGridsHistorialDevoluciones(){
		 getHistorialDevoluciones();
	 }
	
	  function getHistorialDevoluciones(){
		var idOrdenCompra = jQuery("#idOrdenCompra").val();
		var url = cargarHistorialDevoluciones + '?idOrdenCompra=' + idOrdenCompra;
		
	 	jQuery("#historialDevolucionesOrdenCompra").empty(); 
	 	
	 	
	 	historialDevolucionesOrdenCompra = new dhtmlXGridObject('historialDevolucionesOrdenCompra');
	 	historialDevolucionesOrdenCompra.attachEvent("onXLS", function(grid_obj){blockPage()});
	 	historialDevolucionesOrdenCompra.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	 	historialDevolucionesOrdenCompra.load( url ) ;
	 	
	 	historialDevolucionesOrdenCompra.attachEvent("onRowSelect", 
				function(id){
					var idFactura			= historialDevolucionesOrdenCompra.getUserData(id,"idFacturaUD") ;
					var funcScript 			= null;
					var edicion 			= jQuery('#edicion').val();
					var bandejaPrincipal 	=jQuery('#bandejaPrincipal').val();  
					
					if( jQuery("#consulta").length ){
						funcScript = 'setConsultaDevolucion();';
					}
					
					
					if( edicion =='True'){
						jQuery('#facturaId').val( idFactura ) ;
						funcScript = 'setEdicionDevolucion();';
					}
					
					var url = loadInformacionDevolucion + "?idFacturaSiniestro=" + idFactura + "&bandejaPrincipal="+bandejaPrincipal; 
					limpiarDivDevolucion();
					sendRequestJQ(null, url, 'informacionDevolucion', funcScript);	
			 		return true;
			
	 		});	
	
	 }
	  
	  
	/**
	 * Limpia el DIV de la informacion de la Devolucion
	 */
	function limpiarDivDevolucion() {
		document.getElementById("informacionDevolucion").innerHTML = '';
	}
	  
		
	  function imprimir(idFacturaSiniestro){
		  var url = imprimirFacturaDevolucionPath + "?idFacturaSiniestro=" + idFacturaSiniestro ;
		  window.open(url,"devolucion");
	  }
	  
	  
  /* ***************************************************************************************************************************************************************************** */ 
  /* ******************************************************* LISTADO BUSQUEDA DEVOLUCIONES ORDENES COMPRA ********************************************************************** */
  /* ***************************************************************************************************************************************************************************** */
  
	 	var busquedaDevolucionesOrdenCompra;
	 	
	 	
	 	 function initGridsBusquedaDevoluciones(){
	 		loadBusquedaDevoluciones();
	 	 }
	 	
	 	  function loadBusquedaDevoluciones(){
	 
	 	 	jQuery("#busquedaDevolucionesOrdenCompra").empty(); 
	 	 	
	 	 	
	 	 	busquedaDevolucionesOrdenCompra = new dhtmlXGridObject('busquedaDevolucionesOrdenCompra');
	 	 	busquedaDevolucionesOrdenCompra.attachEvent("onXLS", function(grid_obj){blockPage()});
	 	 	busquedaDevolucionesOrdenCompra.attachEvent("onXLE", function(grid_obj){unblockPage()});
	 	
	 	 	busquedaDevolucionesOrdenCompra.load( cargarBusquedaDevoluciones ) ;
	 	
	 	 }
	 	  
	 	 function buscarDevoluciones(){
	 		 
		 		var url = 	cargarBusquedaDevoluciones + '?' + jQuery(document.busquedaDevoluciones).serialize();
		 
		 	 	jQuery("#busquedaDevolucionesOrdenCompra").empty(); 
		 	 	
		 	 	busquedaDevolucionesOrdenCompra = new dhtmlXGridObject('busquedaDevolucionesOrdenCompra');
		 	 	busquedaDevolucionesOrdenCompra.attachEvent("onXLS", function(grid_obj){blockPage()});
		 	 	busquedaDevolucionesOrdenCompra.attachEvent("onXLE", function(grid_obj){unblockPage()});	 	
		 	 	
		 	 	busquedaDevolucionesOrdenCompra.load( url ) ;
		 	
		 	 }
	 	  
	 	  
	 	 function limpiarFormulario(){
	 		jQuery('#busquedaDevoluciones').each (function(){
	 			  this.reset();
	 		});
	 	}
	 	 
	 	 
	 	 
	 	function popUpEdicionMasiva(){
	 		var devolucionesSeleccionadas 	= busquedaDevolucionesOrdenCompra.getCheckedRows(0);
	 		
	 		if(devolucionesSeleccionadas.length > 0 ){
	 			mostrarVentanaModal('vm_edicionMasiva', "Edicion Masiva Devoluciones", 200, 120, 420, 230,mostrarEdicionMasiva,null);
	 		}else{
	 			mostrarMensajeInformativo('Primero seleccione al menos una factura devuelta'  , '20');
	 		}
	 		
	 	 }
	 	
	 	function guardarEdicionMasivaDevoluciones(){
	 		var url = edicionMasivaDevoluciones +'?'+ jQuery(document.edicionMasivaDevoluciones).serialize();
	 		parent.document.getElementById('urlRedirect').value = url;
	 		parent.cerrarVentanaModal("vm_edicionMasiva",'redireccionaEdicionMasiva();');
	 	}
	 	
	 	function cerrarEdicionMaivaDevoluciones(){
	 		parent.cerrarVentanaModal("vm_edicionMasiva",true);
	 	}
	 	
	 	function redireccionaEdicionMasiva(){
	 		var devolucionesSeleccionadas 	= busquedaDevolucionesOrdenCompra.getCheckedRows(0);
	 		var url 						= jQuery("#urlRedirect").val() + "&idsEdicionMasivaDevolucion="+devolucionesSeleccionadas;
	 		
	 		
	 		sendRequestJQ(null, url, targetWorkArea, 'buscarDevoluciones();');
	 	}
	 	
	 	
	 	function mostrarEdicionFactura(idFacturaSiniestro){
	 		var url = mostrarEdicionDevolucion + 
	 		'?idFacturaSiniestro='+idFacturaSiniestro +
	 		'&bandejaPrincipal=' + true;
	 		
	 		sendRequestJQ(null, url ,targetWorkArea, 'setEdicionDevolucion();');
	 		
	 	}
	 	
	 	function mostrarConsultaFactura(idFacturaSiniestro){
	 		var url = mostrarEdicionDevolucion + 
	 		'?idFacturaSiniestro='+idFacturaSiniestro +
	 		'&bandejaPrincipal=' + true;
	 		
	 		sendRequestJQ(null, url ,targetWorkArea, 'setConsultaDevolucion();');
	 		
	 	}
	 	
	 	function setConsultaDevolucion(){
	 		jQuery("#bandejaPrincipal").val('True');
	 		jQuery(".consulta").attr("disabled","disabled");
	 		jQuery("#guardar").remove();
	 		jQuery("#contenido").append("<div id='consulta' style='display:none;' >1</div>");
	 	}
	 	
	 	function setEdicionDevolucion(){
	 		jQuery(".consulta").attr("disabled","disabled");
	 		jQuery("#bandejaPrincipal").val('True');
	 		jQuery("#edicion").val('True');
	 		document.getElementById("opcionCancelarOrdenCompra").innerHTML = '';
	 		
	 		jQuery(".edicion").attr("disabled","");
	 		
	 	}
	 	
	 	function regresarABandeja(){
	 		sendRequestJQ(null, mostrarBusquedaDevoluciones ,targetWorkArea, null);
	 	}
	 	
	 	
	 	
	 	function validaDatosRequeridos(){
	 		var requeridos = jQuery(".requerido");
	 		var success = true;
	 		requeridos.each(
	 			function(){
	 				var these = jQuery(this);
	 				if( isEmpty(these.val()) ){
	 					these.addClass("errorField"); 
	 					success = false;
	 				} else {
	 					these.removeClass("errorField");
	 				}
	 			}
	 		);
	 		return success;
	 	}

	 	function truncarTexto(componente, maximoCaracteres){
	 	      var textoComponente = jQuery(componente).val(); 
	 	      var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	 	      jQuery(componente).val(textoNuevo); 
	 	}
	 	
	 	
	 	
	 	
	 	 function validate_fechaMayorQueRegistro() { 
	 		 var fechaInicial 	= jQuery("#fechaFactura").val();
	 		 var fechaFinal 	= jQuery("#fechaRecepcionMatriz").val();
	 		 
	 		 valuesStart=fechaInicial.split("/"); 
	 		 valuesEnd=fechaFinal.split("/"); // Verificamos que la fecha no sea posterior a la actual 
	 		 var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]); 
	 		 var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]); 
	 		 
	 		 if(dateStart > dateEnd) { 
	 			mostrarMensajeInformativo('La fecha de recepcion matriz debe de ser igual o mayor a la fecha de la factura'  , '20');
	 			 return false; 
	 			 } 
	 	
	 		 return true; 
	 		 } 
	 	 
	 	
	 	
	 	function validate_fechaMayorQueDevolucion() { 
	 		 var fechaInicial 	= jQuery("#fechaDevolucion").val();
	 		 var fechaFinal 	= jQuery("#fechaEntrega").val();
	 		 
	 		 valuesStart=fechaInicial.split("/"); 
	 		 valuesEnd=fechaFinal.split("/"); // Verificamos que la fecha no sea posterior a la actual 
	 		 var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]); 
	 		 var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]); 
	 		 
	 		 if(dateStart > dateEnd) { 
	 			mostrarMensajeInformativo('La fecha de devolucion debe de ser igual o mayor a la fecha de la factura'  , '20');
	 			 return false; 
	 			 } 
	 		
	 		 return true; 
	 		 } 
	 	
	 	
	 	
	 	 function changeTipoPrestador() {	
	 		 var tipo = dwr.util.getValue("tipoProveedorLis");	
	 		 if(null ==tipo   || tipo=="" ){
	 				dwr.util.removeAllOptions("proveedorLis");		
	 			}else{
	 			listadoService.getMapPrestadorPorTipo( tipo ,function(data){
	 				dwr.util.removeAllOptions("proveedorLis");
	 				dwr.util.addOptions("proveedorLis", [ {
	 						id : "",
	 						value : "Seleccione..."
	 					} ], "id", "value");
	 				dwr.util.addOptions("proveedorLis", data);
	 			});
	 			}
	 	 }
