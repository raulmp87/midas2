/**
 * 
 */
var popUp;
var COLINDEX_CHECKSELECT = 0;
var COLINDEX_ESTATUSNCR = 4;
var COLINDEX_CHECKCANCELAROC = 7;
var COLINDEX_TOTALOC = 12;
var COLINDEX_TOTALNC = 9;
var ETIQUETA_CONCEPTO_NCR = "Nota(s) de Crédito";
var ETIQUETA_CONCEPTO_OC = "Orden(es) de Compra";

var numOCSeleccionadas;
var montoOCSeleccionadas;
var numNCRSeleccionadas;
var montoNCRSeleccionadas;
var initGridOC;
var initGridNC;
var OP_AGREGAR = 1;
var OP_QUITAR = -1;

//----- Inicia funcionalidad Cargar Archivo ----

function cargarZipNotasCredito(){
  
	try {	
		
	var ext;	
	
	if(jQuery("#idProveedor").val() != '')
	{		
		if(dhxWins != null) 
			dhxWins.unload();
	
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
		adjuntarDocumento.setText("Carga de Facturas");
		adjuntarDocumento.button("minmax1").hide();
		adjuntarDocumento.button("park").hide();
		adjuntarDocumento.setModal(true);
		adjuntarDocumento.center();
		adjuntarDocumento.denyResize();
		adjuntarDocumento.attachHTMLString("<div id='vault'></div>");
			
	
		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");  
	    vault.setFilesLimit(1);
	    
	    vault.onAddFile = function(fileName) { 
	    	ext = this.getFileExtension(fileName); 
	    	if (ext != "zip" && ext != "xml") { 
	    		mostrarMensajeInformativo("Solo se pueden cargar Archivos XML o ZIP",'20'); 
	    		return false; 
	    	} else 
	    		return true; 
	        
	     }; 
	     
	    vault.create("vault");
	    vault.setFormField("claveTipo", "16");
	        
	    vault.onBeforeUpload = function(files){
	    	jQuery("#file1").attr("name", "archivoNotasCredito");
	    }
	    
	    jQuery("#buttonUploadId").unbind("click");
	    jQuery("#buttonUploadId").click(function(){
	        
	    	blockPage();
			jQuery.ajaxFileUpload({
				url: "/MidasWeb/siniestros/pagos/notasCredito/cargarArchivoNotasCredito.action?" +"&proveedor.id=" 
				+ jQuery("#idProveedor").val() + "&extensionArchivo=" + ext,
				secureuri:false,
				fileElementId: "file1",
				dataType: 'text',					
				success: function(data){
					
					unblockPage();
					parent.dhxWins.window("divContentForm").close();
					
					var respuesta = JSON.parse(jQuery(data).text()); 
					
					if(respuesta.numNotasCargadas == 0)
					{
						mostrarMensajeInformativo("No se cargo ninguna nota de crédito en el sistema, " +
								"revise que el RFC de las notas de crédito es correcto o que las notas de crédito no han sido previamente registradas", '20');					
					}
						
					var url = "/MidasWeb/siniestros/pagos/notasCredito/mostrarRecepcionNotasCredito.action?idBatch=" + respuesta.idBatch+"&proveedor.id=" 
					+ jQuery("#idProveedor").val() + "&numNotasCargadas=" + respuesta.numNotasCargadas + "&factura.id=" + jQuery("#idFactura").val() +
					"&proveedor.personaMidas.nombre=" + jQuery("#nombrePrestadorServicio").val() + "&factura.numeroFactura=" + jQuery("#numeroFactura").val();					
					
					if(respuesta.tipoMensaje == '10')//Si ocurrio un error
					{
						url = url + "&mensaje= " + respuesta.mensaje + "&tipoMensaje= " + respuesta.tipoMensaje; 
					}
					
					sendRequestJQ(null,url,targetWorkArea,null);				
				},
				error: function(data){
					unblockPage()
					parent.dhxWins.window("divContentForm").close();
					mostrarMensajeInformativo('Ha ocurrido un error al intentar subir el archivo ZIP', '20');			
				}
			});
		});
    
	}
	else
	{
		mostrarMensajeInformativo('Debe seleccionar una factura antes de realizar la carga de las Notas de Crédito', '20');
	}
	
	}catch(err) {
	    //document.getElementById("demo").innerHTML = err.message;
	}
}


jQuery.extend({
    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout		
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})
//----- Termina funcionalidad Cargar archivo ----

function nuevaRecepcionNotasCredito()
{
	this.verRecepcionNotasCredito(null,null,null,null,null,null,false,false);
}

function verRecepcionNotasCredito(idAsociacionNCR, idNotaCredito, idFactura, numFactura, idProveedor, nombreProveedor, soloLectura, esCancelacion)
{
	var params = "";
	
	esCancelacion = (typeof esCancelacion != 'undefined')?esCancelacion:'';
	
	if(idAsociacionNCR != null)
	{
		params = "conjuntoOrdenCompraNota.id=" + idAsociacionNCR + "&seleccionNotasCreditoConcat=" + idNotaCredito + "&factura.id=" + idFactura 
				+ "&factura.numeroFactura=" + numFactura 
				+ "&proveedor.id=" + idProveedor + "&proveedor.personaMidas.nombre=" 
				+ nombreProveedor + "&soloLectura=" + soloLectura 
				+ "&esCancelacion=" + esCancelacion;		
	}else
	{
		params = "&soloLectura=" + soloLectura;		
	}
	
	var url = "/MidasWeb/siniestros/pagos/notasCredito/mostrarRecepcionNotasCredito.action?" + params;	
	
	sendRequestJQ(null,url,targetWorkArea,null);		
}

function mostrarBusquedaFacturas()
{
	var url = '/MidasWeb/siniestros/pagos/notasCredito/mostrarBusquedaFacturas.action?';
	
	mostrarVentanaModal("busquedaFacturas", 'Búsqueda de Facturas Registradas', 400, 320, 730, 350, url);
	popUp = mainDhxWindow.window('busquedaFacturas');
}

function obtenerDatosFactura(idFactura, idProveedor, nombreProveedor, noFactura)
{ 	
	if(jQuery("#idFactura").val() != '' && jQuery("#idFactura").val() != idFactura)
	{		
		if(confirm("Está seguro que desea cambiar la factura, se perderán aquellas notas de credito que aún no han sido registradas?"))
		{
			actualizarDatosFactura(idFactura,idProveedor,nombreProveedor,noFactura,true);				
		}		
		
	}else
	{
		actualizarDatosFactura(idFactura,idProveedor,nombreProveedor,noFactura,false);		
	}	
}

function actualizarDatosFactura(idFactura, idProveedor, nombreProveedor, noFactura, limpiarGridNotas)
{
	jQuery("#idFactura").val(idFactura);
	jQuery("#idProveedor").val(idProveedor);
	jQuery("#nombrePrestadorServicio").val(nombreProveedor);
	jQuery("#numeroFactura").val(noFactura);
	popUp.close();
	
	buscarOrdenesCompraPorFactura();	
	
	if(limpiarGridNotas)
	{
		jQuery("#idBatch").val(null);
		listarNotasCredito();		
	}
}

function buscarOrdenesCompraPorFactura()
{
	mostrarIndicadorCarga('indicador');
	
	var form = jQuery('#recepcionNotaCreditoForm').serialize();	
	
	document.getElementById("ordenesCompraListadoGrid").innerHTML = '';	
	ordenesCompraListadoGrid = new dhtmlXGridObject('ordenesCompraListadoGrid');
		
	ordenesCompraListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		deshabilitarGridCheckboxes(grid);
    });	
	
	ordenesCompraListadoGrid.attachEvent("onCheck", function(rId,cInd,state){
	    
	    if(cInd == COLINDEX_CHECKCANCELAROC)
	    {
	    	if(state == true)//check
	    	{
	    		if(ordenesCompraListadoGrid.cells(rId,COLINDEX_CHECKSELECT).getValue() == 1)//Previamente seleccionado
	    		{ //Remover de los totales
	    			actualizarTotalesSeleccion(this, rId, OP_QUITAR); 	    		
	   			}
	    		
	    		ordenesCompraListadoGrid.cells(rId,COLINDEX_CHECKSELECT).setValue(true);	
		    	ordenesCompraListadoGrid.cells(rId,COLINDEX_CHECKSELECT).setDisabled(true);	    		
	    	}
	    	else //uncheck
	    	{
	    		ordenesCompraListadoGrid.cells(rId,COLINDEX_CHECKSELECT).setValue(false);	
		    	ordenesCompraListadoGrid.cells(rId,COLINDEX_CHECKSELECT).setDisabled(false);	  		    		
	    	}
	    }
	    else
	    {
	    	if(state == true)//check
	    	{
	    		actualizarTotalesSeleccion(this, rId, OP_AGREGAR); 
	    		
	    	}else
	    	{
	    		actualizarTotalesSeleccion(this, rId, OP_QUITAR); 
	    	}	    	
	    }
		
	});
	
	var url = '/MidasWeb/siniestros/pagos/notasCredito/listarOrdenesCompraPorFactura.action?' + form; 
	
	ordenesCompraListadoGrid.attachHeader("&nbsp,#text_filter,#text_filter,#text_filter,#select_filter,&nbsp,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter");

	ordenesCompraListadoGrid.load(url);
	ordenesCompraListadoGrid.objBox.style.overflowX = "hidden";
	
	actualizarPieGrid(ordenesCompraListadoGrid, 0, ETIQUETA_CONCEPTO_OC, 0, false);	
	numOCSeleccionadas=0;
	montoOCSeleccionadas=0;
}

function listarNotasCredito()
{
	mostrarIndicadorCarga('indicador');
	var form = jQuery('#recepcionNotaCreditoForm').serialize();	
	
	var url = '/MidasWeb/siniestros/pagos/notasCredito/listarNotasCredito.action?' + form ;	

	document.getElementById("notasCreditoListadoGrid").innerHTML = '';	
	notasCreditoListadoGrid = new dhtmlXGridObject('notasCreditoListadoGrid');
	
	notasCreditoListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		deshabilitarGridCheckboxes(grid);
    });	
	
	notasCreditoListadoGrid.attachEvent("onCheck", function(rId,cInd,state){
	   	    
    	if(state == true)//check
    	{    		
    		actualizarTotalesSeleccion(this, rId, OP_AGREGAR); 
    		
    	}else
    	{
    		actualizarTotalesSeleccion(this, rId, OP_QUITAR); 
    	}    
		
	});

	notasCreditoListadoGrid.attachHeader("&nbsp,#text_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp,#text_filter,&nbsp");
	notasCreditoListadoGrid.load(url);
	notasCreditoListadoGrid.objBox.style.overflowX = "hidden";
	
	//inicializar	
	actualizarPieGrid(notasCreditoListadoGrid, 0, ETIQUETA_CONCEPTO_NCR, 0, false);	
	numNCRSeleccionadas = 0;
	montoNCRSeleccionadas = 0;
	
}

function buscarNotasCredito()
{
	mostrarIndicadorCarga('indicador');
	var form = jQuery('#busquedaNotasCreditoForm').serialize();	
	
	var url = '/MidasWeb/siniestros/pagos/notasCredito/buscarNotasCredito.action?' + form ;	

	document.getElementById("busquedaNotasCreditoGrid").innerHTML = '';	
	busquedaNotasCreditoGrid = new dhtmlXGridObject('busquedaNotasCreditoGrid');
	
	busquedaNotasCreditoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');		
    });	
	
	busquedaNotasCreditoGrid.enablePaging(true,10,10,"pagingArea",true,"infoArea");
	busquedaNotasCreditoGrid.setPagingSkin("bricks");
	busquedaNotasCreditoGrid.load(url);		
}

function limpiarFiltrosBusquedaNC()
{
	jQuery('#busquedaNotasCreditoForm').each (function(){
		  this.reset();
	});
}

function validarRegistrarNC()
{
	var idsOrdenesCompraAsociar = ordenesCompraListadoGrid.getCheckedRows(COLINDEX_CHECKSELECT);
	var idsOrdenesCompraCancelar = ordenesCompraListadoGrid.getCheckedRows(COLINDEX_CHECKCANCELAROC);
	var idsNotasCreditoAsociar = notasCreditoListadoGrid.getCheckedRows(COLINDEX_CHECKSELECT);
	
	if(idsOrdenesCompraAsociar != "" && idsNotasCreditoAsociar != "")
	{
		var url = "/MidasWeb/siniestros/pagos/notasCredito/validarRegistrarNotaCredito.action?idBatch=" + jQuery("#idBatch").val() +"&proveedor.id=" 
		+ jQuery("#idProveedor").val() + "&numNotasCargadas=" + jQuery("#numNotasCargadas").val() + "&factura.id=" + jQuery("#idFactura").val() +
		"&proveedor.personaMidas.nombre=" + jQuery("#nombrePrestadorServicio").val() + "&factura.numeroFactura=" + jQuery("#numeroFactura").val() +
		"&seleccionNotasCreditoConcat=" + idsNotasCreditoAsociar + "&seleccionOrdenesAsociarConcat=" + idsOrdenesCompraAsociar +
		"&seleccionOrdenesCancelarConcat=" + idsOrdenesCompraCancelar;	
		
		sendRequestJQ(null,url,targetWorkArea,null);
	}
	else
	{
		mostrarMensajeInformativo("Debe asociar por lo menos una nota de crédito a una orden de compra",'20'); 
	}
}

function actualizarTotalesSeleccion(grid, idRow, operacion)
{
	var concepto;
	var montoTotal;
	var numElementos;
	var footerLabel;
	var idGridDiv; 
	
	idGridDiv = grid.entBox.id; // notasCreditoListadoGrid    ordenesCompraListadoGrid
	
	if(idGridDiv == 'notasCreditoListadoGrid')
	{
		concepto = ETIQUETA_CONCEPTO_NCR;
		montoNCRSeleccionadas = montoNCRSeleccionadas + (operacion * grid.cells(idRow,COLINDEX_TOTALNC).getValue());
		montoTotal = montoNCRSeleccionadas;
		
		numNCRSeleccionadas = numNCRSeleccionadas + (operacion * 1);
		numElementos = numNCRSeleccionadas;			
		
	}else
	{
		concepto = ETIQUETA_CONCEPTO_OC;	
		montoOCSeleccionadas = montoOCSeleccionadas + (operacion * grid.cells(idRow,COLINDEX_TOTALOC).getValue());
		montoTotal = montoOCSeleccionadas;
		
		numOCSeleccionadas = numOCSeleccionadas + (operacion * 1);
		numElementos = numOCSeleccionadas;
	}		
	
	actualizarPieGrid(grid, numElementos, concepto, montoTotal, true);	
	
	initGrid = initGrid +1 ;
}

function verDetalleValidacionNC(idValidacionNC, numeroNC)
{	
	var url = '/MidasWeb/siniestros/pagos/notasCredito/verDetalleValidacionNotaCredito.action?' + "idValidacionNotaCredito=" + idValidacionNC;
	
	mostrarVentanaModal("DetValidacionesNCR", 'Detalle Validaciones Factura No. ' + numeroNC, 50, 200, 700, 300, url);
}

function mostrarBusquedaNC()
{
	var url = '/MidasWeb/siniestros/pagos/notasCredito/mostrarBusquedaNotasCredito.action?';
	sendRequestJQ(null,url,targetWorkArea,null);		
}

function deshabilitarGridCheckboxes(grid)
{
	var numElementos = 0;
	var montoTotal = 0;
	
	idGridDiv = grid.entBox.id; // notasCreditoListadoGrid    ordenesCompraListadoGrid
	
	if(idGridDiv == 'notasCreditoListadoGrid')
	{
		if(jQuery('#soloLectura').val() == 'true')
		{
			grid.forEachRow(function(id) {  
				
				grid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
				grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);	
				numElementos ++;
				montoTotal = montoTotal + grid.cells(id,COLINDEX_TOTALNC).getValue();
				
			});	
			
			actualizarPieGrid(grid, numElementos, ETIQUETA_CONCEPTO_NCR, montoTotal, true);
		}else if(jQuery('#esCancelacion').val() == 'true')
		{
			grid.forEachRow(function(id) { 				
			
				if(grid.cells(id,COLINDEX_ESTATUSNCR).getValue() != 'ASOCIADA' 
					&& grid.cells(id,COLINDEX_ESTATUSNCR).getValue() != 'REGISTRADA' )				
				{
					grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);						
				}				
			});	
			
			if(jQuery('#idCancelacionPreseleccion').val() != null 
					&& jQuery('#idCancelacionPreseleccion').val() != '')
			{
				grid.cells(jQuery('#idCancelacionPreseleccion').val(),COLINDEX_CHECKSELECT).setValue(true);				
			}
			
		}else  // Al realizar una nueva recepcion de Notas de Crédito.
		{
			grid.forEachRow(function(id) { 				
				
				if(grid.cells(id,COLINDEX_ESTATUSNCR).getValue() != 'PROCESADA')
				{
					grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);						
				}				
			});	
		}
		
	}else
	{
		if(jQuery('#soloLectura').val() == 'true' || jQuery('#esCancelacion').val() == 'true')
		{
			grid.forEachRow(function(id) {  
				
				grid.cells(id,COLINDEX_CHECKSELECT).setValue(true);
				grid.cells(id,COLINDEX_CHECKSELECT).setDisabled(true);	
				grid.cells(id,COLINDEX_CHECKCANCELAROC).setDisabled(true);					
				numElementos ++;
				montoTotal = montoTotal + grid.cells(id,COLINDEX_TOTALOC).getValue();
				
			});	
			
			actualizarPieGrid(grid, numElementos, ETIQUETA_CONCEPTO_OC, montoTotal, true);
		}		
	}	
}

function cancelarNotaCredito(idAsociacionNCR, idNotaCredito, idFactura, numFactura, idProveedor, nombreProveedor, soloLectura, esCancelacion)
{	
	if(idAsociacionNCR != null && idAsociacionNCR != "")
	{
		//Ingresa a la pantalla de recepcion de notas de credito para seleccionar las Notas de Credito a cancelar
		verRecepcionNotasCredito(idAsociacionNCR,idNotaCredito,idFactura,numFactura,idProveedor,
				nombreProveedor, false, true);		
	}
	else
	{
		//No cuenta con un conjunto, se cancela la nota de credito de manera directa.
		var url = "/MidasWeb/siniestros/pagos/notasCredito/cancelarNotaCredito.action?&seleccionNotasCreditoConcat=" + idNotaCredito;	
		
		sendRequestJQ(null,url,targetWorkArea,null);		
	}
}

function cancelarNotasCreditoSeleccionadas()
{
	var idsNotasCreditoCancelar = notasCreditoListadoGrid.getCheckedRows(COLINDEX_CHECKSELECT);
	
	if(idsNotasCreditoCancelar != "")
	{
		var url = "/MidasWeb/siniestros/pagos/notasCredito/cancelarNotaCredito.action?&seleccionNotasCreditoConcat=" + idsNotasCreditoCancelar;	
		
		sendRequestJQ(null,url,targetWorkArea,null);
	}
	else
	{
		mostrarMensajeInformativo("Debe seleccionar al menos una Nota de Crédito para cancelar",'20'); 
	}
}

function actualizarPieGrid(grid, numElementos, concepto, montoTotal, requiereDetach)
{
	if(!isNaN(montoTotal))
	{
		montoTotal = formatCurrency(montoTotal).replace(/,/g , "&#x002C;"); //Formatear el texto y reemplazar la comma por codigo hexa
	}	  
	
	footerLabel = "<div style='float: left; color: #00a000; font-size: 10;' id='divExcelBtn'>Monto total de " +
	"<span style='font-weight:bold;color:black;'>" + numElementos + "</span>. " + concepto +
	" seleccionada(s): <span style='font-weight:bold;color:black;'>" + montoTotal + "</span></div>,#cspan,#cspan,#cspan,#cspan";
	
	if(requiereDetach)
	{
		grid.detachFooter(0);		
	}
	
	grid.attachFooter(footerLabel);	
}


function initNotaCredito(){
	jQuery("#estatus option[value='PR']").remove();
	jQuery("#estatus option[value='E']").remove();
}


