 var listaDePagosBloquedosGrid;

 function realizarBusqueda(sendParams){
 	 console.log('--> realizarBusqueda');
 	 var url = "/MidasWeb/siniestros/pagos/bloqueo/buscarBloqueos.action?";
 	 if(sendParams){
 	 	formParams = jQuery(document.infoGeneralForm).serialize();	
 	 	url += formParams;
 	 }
 	 console.log('url'+url);
	 loadGrid(url);
 }

function limpiarFiltros(){
	jQuery('#infoGeneralForm').each (function(){
		  this.reset();
	});
	dwr.util.removeAllOptions("s_proveedorList");
	dwr.util.addOptions("s_proveedorList", [ {
		id : "",
		value : "Seleccione..."
	} ], "id", "value");
}

function loadGrid(url){
		 jQuery("#listaDePagosBloquedosGrid").empty(); 
		 listaDePagosBloquedosGrid = new dhtmlXGridObject('listaDePagosBloquedosGrid');
		 listaDePagosBloquedosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 listaDePagosBloquedosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 listaDePagosBloquedosGrid.load( url ) ;	
}


function crearBloqueo(){
 	var url = "/MidasWeb/siniestros/pagos/bloqueo/mostrarContenedorDetalleBloqueo.action";
 	sendRequestJQ(null, url, targetWorkArea, null);
 }


 function cerrarDetalle(){
 	var url = "/MidasWeb/siniestros/pagos/bloqueo/mostrarContenedor.action";
 	sendRequestJQ(null, url, targetWorkArea, "realizarBusqueda(false);");
 }

 function guardarPagoBloqueado(){
 	console.log('guardarPagoBloqueado');
 	if(validaRequeridoTipoDeBloqueo() && validaDatosRequeridos()){
	 	var paramsForm = jQuery(document.infoGeneralForm).serialize(); 
	 	var url = "/MidasWeb/siniestros/pagos/bloqueo/guardarPagoBloqueado.action?";
	 	var idPagoBloqueado = jQuery('#h_pagoId').val();
	 	console.log('idPagoBloqueado :'+idPagoBloqueado );
	 	if(!isEmpty(idPagoBloqueado) || !isEmpty(idPago)) { 	
	 		var proveedorId = jQuery('#s_proveedorList').val();
	 		var tipoProveedor = jQuery('#s_tipoProveedorList').val();
	 		var tipoBloqueo = jQuery('#h_tipoBloqueo').val();
	 		var idOrdenPago = jQuery('#t_ordenDePago').val();
	 		url+="proveedorId="+proveedorId+'&tipoPrestadorId='+tipoProveedor+'&bloqueoPago.tipo='+tipoBloqueo+'&bloqueoPago.ordenPago.id='+idOrdenPago+'&'; 	
	 		console.log('url:'+url);
	 	}
	 	console.log('Url completo : '+url+paramsForm); 	
	 	sendRequestJQ(null, url+paramsForm, targetWorkArea, null); 	
	 }
 }



 function consultarPagoBloqueado(idPagoBloqueado){
 	console.log('consultarPagoBloqueado :'+idPagoBloqueado);
 	var url = "/MidasWeb/siniestros/pagos/bloqueo/mostrarContenedorDetalleBloqueo.action?idPagoBloqueado="+idPagoBloqueado+'&esModoConsulta=true';
 	sendRequestJQ(null, url, targetWorkArea, null);	

 }

  function editarPagoBloqueado(idPagoBloqueado){
 	console.log('consultarPagoBloqueado :'+idPagoBloqueado);
 	var url = "/MidasWeb/siniestros/pagos/bloqueo/mostrarContenedorDetalleBloqueo.action?idPagoBloqueado="+idPagoBloqueado+'&esModoConsulta=false';
 	sendRequestJQ(null, url, targetWorkArea, null);	

 }

 function validaVisualizacion(){
 	console.log('esModoConsulta : '+esModoConsulta);
 	var esNueva = false;
 	var vieneDePagos = false;
 	if(isEmpty(idPagoBloqueado)){
 		esNueva = true;
 		validaTipoDeBloqueo();
 		if(!isEmpty(idPago)){
 			vieneDePagos = true;
 		}
 	}else{
 		esNueva = false;
 	}
 	configuraPaginaDetalle(esNueva,esModoConsulta,vieneDePagos);
 }

 

 function validaTipoDeBloqueo(){
 	if(jQuery("#r_tipo_bloqueoPAGO").is(':checked')){
 		
 		jQuery("#t_ordenDePago").attr('disabled','');
 		jQuery("#t_ordenDePago").addClass("requerido");
 		jQuery("#s_tipoProveedorList").attr('disabled','true');
 		jQuery("#s_tipoProveedorList").removeClass("requerido");
 		jQuery("#s_proveedorList").removeClass("requerido");
		jQuery("#s_proveedorList").attr('disabled','true');
 	
 	}else if(jQuery("#r_tipo_bloqueoPROVEEDOR").is(':checked')){
 		
 		jQuery("#t_ordenDePago").attr('disabled','true');
 		jQuery("#t_ordenDePago").removeClass("requerido");
 		jQuery("#s_tipoProveedorList").attr('disabled','');
		jQuery("#s_proveedorList").attr('disabled','');
		jQuery("#s_proveedorList").addClass("requerido");
 	}
 }

function validaBloqueo(){
 	if(jQuery("#ch_bloqueo").is(':checked')){
 		habilitaBloqueo();
 	}else {
 		habilitaDesbloqueo();
 	}
 }

 function habilitaBloqueo(){
 	jQuery('#h_estatusPago').val('true');
 	jQuery("#ta_motivoBloqueo").attr('disabled','');
 	jQuery("#ta_motivoDesbloqueo").attr('disabled','true');
 }

function habilitaDesbloqueo(){
	jQuery('#h_estatusPago').val('false');
 	jQuery("#ta_motivoDesbloqueo").attr('disabled','');
 	jQuery("#ta_motivoBloqueo").attr('disabled','true');
 }

 function changeTipoPrestador() {	
	 var tipo = dwr.util.getValue("s_tipoProveedorList");	
	 if(null ==tipo   || tipo=="" ){
			dwr.util.removeAllOptions("s_proveedorList");		
		}else{
		listadoService.getMapPrestadorPorTipo( tipo ,function(data){
			dwr.util.removeAllOptions("s_proveedorList");
			dwr.util.addOptions("s_proveedorList", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("s_proveedorList", data);
		});
		}
 }

 function configuraPaginaDetalle(esNueva,esModoConsulta,vieneDePagos){
 	console.log('esNueva :'+typeof esNueva);
 	console.log('esModoConsulta :'+typeof esModoConsulta);
 	console.log('vieneDePagos :'+typeof vieneDePagos);
 	console.log('esNueva:'+esNueva+'- esModoConsulta:'+esModoConsulta+' - vieneDePagos:'+vieneDePagos);
	if(esNueva == true || esNueva == 'true'){
		console.log('Es nueva');
		jQuery("#ch_bloqueo").attr(':checked',':checked');
		jQuery("#ch_bloqueo").attr('disabled','true');
		jQuery("#ta_motivoDesbloqueo").attr('disabled','true');
		jQuery("#t_fechaDesbloqueo").attr('disabled','true');
		jQuery("#t_usuarioDesbloqueo").attr('disabled','true');
		if(vieneDePagos == true || vieneDePagos == 'true'){
			console.log('Viene de Pagos.........');
			jQuery("#r_tipo_bloqueoPAGO").attr('disabled','true');
			jQuery("#r_tipo_bloqueoPROVEEDOR").attr('disabled','true');
			jQuery("#s_tipoProveedorList").attr('disabled','true');
			jQuery("#s_proveedorList").attr('disabled','true');
			jQuery("#t_ordenDePago").attr('disabled','true');
			validaBloqueo();
		}
	}else{
		console.log('No es nueva');
		jQuery("#r_tipo_bloqueoPAGO").attr('disabled','true');
		jQuery("#r_tipo_bloqueoPROVEEDOR").attr('disabled','true');
		jQuery("#s_tipoProveedorList").attr('disabled','true');
		jQuery("#s_proveedorList").attr('disabled','true');
		jQuery("#t_ordenDePago").attr('disabled','true');
		if(esModoConsulta == true || esModoConsulta == 'true'){
			console.log('Es modo consulta');
			jQuery("#ch_bloqueo").attr('disabled','true');
			jQuery("#ta_motivoBloqueo").attr('disabled','true');
			jQuery("#ta_motivoDesbloqueo").attr('disabled','true');
			console.log('Acaba es modo consulta');
		}else{
			console.log('Es editable');	
			jQuery("#ch_bloqueo").attr('disabled','true');
			validaBloqueo();
			console.log('Acaba Es editable');	
		}	
	}
	
 }

 function regresaListadoDePagos(){
 	sendRequestJQ(null,'/MidasWeb/siniestros/pagos/pagosSinestro/mostrar.action',targetWorkArea,null);
}

function validaRequeridoTipoDeBloqueo(){
	var success = true;
	var radio1Checked = jQuery("#r_tipo_bloqueoPAGO").is(':checked');
	var radio2Checked = jQuery("#r_tipo_bloqueoPROVEEDOR").is(':checked')
	if( !radio1Checked && !radio2Checked){
		success = false;
		jQuery("#messageError").show("slow");
	}else{
		jQuery("#messageError").hide();
	}
	return success;
}


function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}
 