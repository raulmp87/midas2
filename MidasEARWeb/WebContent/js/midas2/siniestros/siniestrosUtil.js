function validaFormatoSiniestro(elemento){
	console.log('Entra a validaFormatoSiniestro');
	var val = jQuery(elemento).val();
	var result = true;
	if(!isEmpty(val)){
		console.log('Val = '+val);
		var expreg = new RegExp("^([0-9]|\\w)+[-][0-9]+[-][0-9]+$");
		result = expreg.test(val);
		if(!result){
			mostrarMensajeInformativo('Formato incorrecto.', '20');
			jQuery(elemento).val('');
		}
		result = false;
	}
	return result;
}