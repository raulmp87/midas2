var listadoPercepcionesAjustadoresGrid;
var listadoDetallePercepcionGrid;
var solicitudAutorizacionGrid;
var historicoDeEnviosGrid;
var incentivosDiaInhabilGrid;
var incentivosRecEfectivoGrid;
var incentivosRecCompaniaGrid;
var incentivosRecSipacGrid;
var incentivosRechazosGrid;
var totalesIncentivosGrid;
var popUpEnvioNotificacion;

function initGridsSolicitudAutorizacion(){
	getSolicitudesAutorizadas();
}

function getSolicitudesAutorizadas(){
	jQuery("#solicitudAutorizacionGrid").empty(); 
	
	solicitudAutorizacionGrid = new dhtmlXGridObject('solicitudAutorizacionGrid');
	solicitudAutorizacionGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicador");});
	solicitudAutorizacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicador");});

	solicitudAutorizacionGrid.setHeader   ("Folio,Fecha de Inicio,Fecha Fin,Usuario Creador,Fecha Creacion,Usuario Autorizacion,Fecha Autorizacion,Oficina,Estatus");
	solicitudAutorizacionGrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,&nbsp,&nbsp");
	
	solicitudAutorizacionGrid.load( "/MidasWeb/siniestros/incentivoAjustador/mostrarContenedorIncentivosAutorizacionGrid.action" ) ;
}

function limpiarConfiguracionIncentivo(){
	jQuery('#configuracionIncentivosForm').each (function(){
		  this.reset();
	});
	onClickActivarTextfield(jQuery('#esDiasInhabiles'),jQuery('#montoDiaInhabil'),'$100.00');
	onClickActivarTextfield(jQuery("#esRecuperacionCia"),jQuery('#montoRecuperacionCia'),'$150.00');
	onClickActivarTextfield(jQuery("#esRecuperacionSipac"),jQuery('#montoRecuperacionSipac'),'$150.00');
	onClickActivarTextfield(jQuery("#esSiniestroRechazado"),jQuery('#porcentajeRechazo'),'10');
	onClickActivarExcedenteEfectivo();
	onClickActivarRangoEfectivo();
	onClickActivarEfectivo();
}

function generarConfiguracionIncentivo(){
	if(validateAll(true)){
		if(validarMayorQueCero()){
			if(validarParametroRecuperacionEfectivo()){
				if(validarCondicionExcedente()){
					if(validarPeriodo()){
						removeCurrencyFormatOnTxtInput();
						formParams = jQuery(document.configuracionIncentivosForm).serialize();
						var url = guardarConfiguracionIncentivosPath + '?' +  formParams;
						console.log(url);
						sendRequestJQ(null, url, targetWorkArea, null);
					}else{
						mostrarVentanaMensaje("20","\'Periodo hasta\' debe ser una fecha mayor o igual que \'Periodo desde\'",null);
					}
				}else{
					mostrarVentanaMensaje("20","Debe seleccionar la condici\u00F3n de Excedente por Siniestro",null);
				}
			}else{
				mostrarVentanaMensaje("20","Seleccione un Par\u00E1metro de Recuperaci\u00F3n de Efectivo",null);
			}
		}else{
			mostrarVentanaMensaje("20","Los montos a capturar deben ser mayores que cero",null);
		}
	}
}

function validarPeriodo(){
	var stringFechaDesde = jQuery("#periodoDe").val();
	var stringFechaHasta = jQuery("#periodoHasta").val();
	var periodoDesde = new Date(stringFechaDesde);
	var periodoHasta = new Date(stringFechaHasta);
	if(periodoHasta >= periodoDesde){
		return true;
	}else{
		return false;
	}
}
 
function onClickActivarTextfield(chckbox, txtfield, valorAlActivar){
	var esActivo = jQuery(chckbox).attr('checked');
	if(esActivo){
		jQuery(txtfield).removeAttr('disabled');
		jQuery(txtfield).val(valorAlActivar);
	}else{
		jQuery(txtfield).attr('disabled',true);
		if(jQuery(txtfield).hasClass('monto')){
			jQuery(txtfield).val('$0.00');
		}else if(jQuery(txtfield).hasClass('porcentaje')){
			jQuery(txtfield).val('0.00');
		}
	}
}

function onClickActivarEfectivo(){
	var esActivo = jQuery("#esRecuperacionEfectivo").attr('checked');
	if(esActivo){
		jQuery("#esRangoEfectivo").removeAttr('disabled');
		jQuery("#esExcedenteSiniestro").removeAttr('disabled');
	}else{
		jQuery("#esRangoEfectivo").attr('checked',false);
		jQuery("#esExcedenteSiniestro").attr('checked',false);
		onClickActivarRangoEfectivo();
		onClickActivarExcedenteEfectivo();
		jQuery("#esRangoEfectivo").attr('disabled',true);
		jQuery("#esExcedenteSiniestro").attr('disabled',true);		
	}
}

function onClickActivarRangoEfectivo(){
	var esActivo = jQuery("#esRangoEfectivo").attr('checked');
	if(esActivo){
		jQuery("#montoRangoEfectivo").removeAttr('disabled');
		jQuery("#montoHastaRangoEfectivo").removeAttr('disabled');
		jQuery("#porcentajeRangoEfectivo").removeAttr('disabled');
		jQuery("#montoRangoEfectivo").val('$0.00');
		jQuery("#montoHastaRangoEfectivo").val('$15,000.00');
		jQuery("#porcentajeRangoEfectivo").val('20');
	}else{
		jQuery("#montoRangoEfectivo").attr('disabled',true);
		jQuery("#montoHastaRangoEfectivo").attr('disabled',true);
		jQuery("#porcentajeRangoEfectivo").attr('disabled',true);
		jQuery("#montoRangoEfectivo").val('$0.00');
		jQuery("#montoHastaRangoEfectivo").val('$0.00');
		jQuery("#porcentajeRangoEfectivo").val('0.00');
	}
}

function onClickActivarExcedenteEfectivo(){
	var esActivo = jQuery("#esExcedenteSiniestro").attr('checked');
	if(esActivo){
		jQuery("#condicionExcedente").removeAttr('disabled');
		jQuery("#montoExcedente").removeAttr('disabled');
		jQuery("#porcentajeExcedente").removeAttr('disabled');
		jQuery("#condicionExcedente").val('MAY');
		jQuery("#montoExcedente").val('$15,001.00');
		jQuery("#porcentajeExcedente").val('10');
	}else{
		jQuery("#condicionExcedente").attr('disabled',true);
		jQuery("#montoExcedente").attr('disabled',true);
		jQuery("#porcentajeExcedente").attr('disabled',true);
		jQuery("#condicionExcedente").val('');
		jQuery("#montoExcedente").val('$0.00');
		jQuery("#porcentajeExcedente").val('0.00');
	}
}
 
function initConfiguracionIncentivo(){
	inicializarMontosYPorcentajesNulos();
	configurarPorcentajesDeDecimalAEntero();
	buscarPercepcionesAjustadores();
}
 
function inicializarMontosYPorcentajesNulos(){
	jQuery(".monto").each(function(i){
		if(!jQuery(this).val()){
			jQuery(this).val("$0.00");
		}
	});
	jQuery(".porcentaje").each(function(i){
		if(!jQuery(this).val()){
			jQuery(this).val("0.00");
		}
	});
}

function configurarPorcentajesDeDecimalAEntero(){
	jQuery(".porcentaje").each(function(i){
		if(jQuery(this).val() && jQuery(this).val() != "0.00"){
			jQuery(this).val(Number(jQuery(this).toNumber().val() * 100).toFixed(2));
		}
	});
}

function validarMayorQueCero(){
	var validacion = true;
	removeCurrencyFormatOnTxtInput();
	jQuery(".mayorQueCero").each(function(i){
		var esDisabled = jQuery(this).attr('disabled');
		if(!esDisabled && jQuery(this).val() <= 0){
			initCurrencyFormatOnTxtInput();
			validacion = false;
		}
	});
	initCurrencyFormatOnTxtInput();
	return validacion;
}

function validarCondicionExcedente(){
	var esActivo = jQuery("#esExcedenteSiniestro").attr('checked');
	var esDisabled = jQuery("#condicionExcedente").attr('disabled');
	if(!esDisabled){
		if(esActivo){
			var valorSeleccionado = jQuery("#condicionExcedente").val();
			if(valorSeleccionado){
				return true;
			}
		}
	}else{
		return true;
	}
	return false;
}

function validarParametroRecuperacionEfectivo(){
	var esActivoParametroEfectivo = jQuery("#esRecuperacionEfectivo").attr('checked');
	var esActivoRangoEfectivo = jQuery("#esRangoEfectivo").attr('checked');
	var esActivoExcedenteEfectivo = jQuery("#esExcedenteSiniestro").attr('checked');
	
	if(esActivoParametroEfectivo){
		if(esActivoRangoEfectivo || esActivoExcedenteEfectivo){
			return true;
		}
	}else{
		return true;
	}
	return false;
}

function buscarPercepcionesAjustadores(){
	jQuery("#listadoPercepcionesAjustadoresGrid").empty();

	listadoPercepcionesAjustadoresGrid = new dhtmlXGridObject('listadoPercepcionesAjustadoresGrid');
	listadoPercepcionesAjustadoresGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorPercepcionesAjustadores");});
	listadoPercepcionesAjustadoresGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorPercepcionesAjustadores");});
	listadoPercepcionesAjustadoresGrid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp");
	
	var url = buscarConfigIncentivosPath;
	listadoPercepcionesAjustadoresGrid.load( url );
}

function autoriza(idConfiguracion){
	if(confirm("¿ Desea Autorizar la Configuración ?")){
		var url="/MidasWeb/siniestros/incentivoAjustador/autoriza.action?configuracionId="+idConfiguracion;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
	
}

function rechaza(idConfiguracion){
	if(confirm("¿ Desea Realizar el Rechazo de la Configuración ?")){
		var url="/MidasWeb/siniestros/incentivoAjustador/rechaza.action?configuracionId="+idConfiguracion;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}


function mostrarContenedorNotificaciones(idConfiguracion){
	console.log('idConfiguracion : '+idConfiguracion);
	var url="/MidasWeb/siniestros/incentivoAjustador/mostrarContenedorNotificaciones.action?configuracionId="+idConfiguracion;
	mostrarVentanaModal("vm_notificacionIncentivo", 'Notificacion_Incentivo_Ajustador', 950, 610, 800, 500, url,'mostrarConfiguracionIncentivos();');
	popUpEnvioNotificacion = mainDhxWindow.window('vm_notificacionIncentivo');
}


function cargaHistoricoDeEnvios(){
	jQuery("#historicoDeEnviosGrid").empty(); 
	
	historicoDeEnviosGrid = new dhtmlXGridObject('historicoDeEnviosGrid');
	historicoDeEnviosGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicador");});
	historicoDeEnviosGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicador");});
	configuracionId = jQuery("#configuracionId").val();
	historicoDeEnviosGrid.load( "/MidasWeb/siniestros/incentivoAjustador/mostrarEnvioHistorico.action?configuracionId="+configuracionId ) ;
}

function enviarNotificacion(){
//	console.log('Antes de configuracionId');
//	configuracionId = jQuery("#configuracionId").val();
//	console.log('configuracionId : '+configuracionId);
//	console.log('Antes de correos remitentes');
//	destinatariosStr = jQuery("#correosRemitentes").val();
//	var url="/MidasWeb/siniestros/incentivoAjustador/enviaNotificacion.action?configuracionId="+configuracionId+'&destinatariosStr='+destinatariosStr;
//	console.log('URL : '+url);
	parent.submitVentanaModal("vm_notificacionIncentivo", document.envioNotificacionForm);
}


function imprimirPercepcionIncentivos(idConfigIncentivo){
	var url = imprimirConfigIncentivosPath + '?configuracion.id=' + idConfigIncentivo;
	window.open(url, "PercepcionIncentivos");
}

function consultarDetalle(idConfigIncentivo){
	var url = consultarDetalleIncentivosPath + '?configuracion.id=' + idConfigIncentivo + '&esConsulta=true';
	sendRequestJQ(null, url, targetWorkArea, null);
}

function editarDetalle(idConfigIncentivo){
	var url = generarConfigIncentivosPath + '?configuracion.id=' + idConfigIncentivo;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function consultarPercepcion(idConfigIncentivo){
	var url = mostrarContenedorDesglosePercepcionesPath + '?configuracionId=' + idConfigIncentivo + '&esConsulta=true';
	sendRequestJQ(null, url, targetWorkArea, null);
}

function cancelarPercepcionLista(idConfigIncentivo){
	if(confirm('\u00BFEst\u00E1 seguro de cancelar la Percepci\u00F3n\u003F')){
		var url = cancelarConfigIncentivosPatch + "?configuracion.id=" + idConfigIncentivo;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function enviarCorreo(idConfigIncentivo){
	mostrarContenedorNotificaciones(idConfigIncentivo);
}

function cerrarDetallePercepcion(){
	if(confirm("Se perder\u00E1n los cambios que no hayan sido guardados. \u00BFDesea continuar\u003F")){
		
		var estatus = jQuery("#h_estatus").val();
		
		if (estatus == 'TRAMITE') {
			sendRequestJQ(null, eliminarConfigIncentivosPath + '?configuracionId=' + jQuery("#configuracionId").val() , targetWorkArea, null);
		} else {
			mostrarConfiguracionIncentivos();
		}
		
	}
}

function mostrarConfiguracionIncentivos(){
	var url = mostrarConfiguracionIncentivosPath;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function guardarConfiguracionIncentivos(){
	var formParams = jQuery(document.configuracionIncentivosForm).serialize();
	var url = guardarConfiguracionIncentivosPath + '?' + formParams;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function buscarDetallePercepcion(){
	var esConsulta = jQuery("#esConsulta").val();
	jQuery("#listadoDetallePercepcionGrid").empty();

	listadoDetallePercepcionGrid = new dhtmlXGridObject('listadoDetallePercepcionGrid');
	listadoDetallePercepcionGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorDetallePercepcion");});
	listadoDetallePercepcionGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorDetallePercepcion");
																			if(esConsulta){
																				listadoDetallePercepcionGrid.forEachRow(function(id){
																					listadoDetallePercepcionGrid.cells(id,0).setDisabled( true);});}});
	listadoDetallePercepcionGrid.attachHeader("&nbsp,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	
	if(esConsulta){
		listadoDetallePercepcionGrid._in_header_master_checkbox=function(t,i,c){
			t.innerHTML=c[0]+"<input type='checkbox' disabled='true' />"+c[1];
            var self=this;
		}
//		listadoDetallePercepcionGrid.attachEvent("onXLE", function(grid_obj){
//			listadoDetallePercepcionGrid.checkAll();
//			jQuery("#ingresosConcat").val(listadoDetallePercepcionGrid.getCheckedRows(0));
//			listadoDetallePercepcionGrid.forEachRow(function(id){
//				listadoDetallePercepcionGrid.cells(id,0).setDisabled( true);
//			});
//		});
		listadoDetallePercepcionGrid.attachEvent("onPageChanged", function(grid_obj){
//			listadoDetallePercepcionGrid.checkAll();
//			jQuery("#ingresosConcat").val(listadoDetallePercepcionGrid.getCheckedRows(0));
			listadoDetallePercepcionGrid.forEachRow(function(id){
				listadoDetallePercepcionGrid.cells(id,0).setDisabled( true);
			});
		});
	}
	
	var url = buscarDetallesPercepcionPath + "?configuracion.id=" + jQuery("#configuracionId").val();
	listadoDetallePercepcionGrid.load( url );
}

function generarPercepcion(){
	var concat = listadoDetallePercepcionGrid.getCheckedRows(0);
	//alert('concat: ' + concat);
	
	if (concat == null || concat == '') {
		mostrarMensajeInformativo("Debe seleccionar al menos un registro", '20');
	} else {
		var url = preRegistroIncentivosPath + '?configuracionId=' + jQuery("#configuracionId").val() + '&incentivosConcat=' + concat;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
	
	
}

function obtenerDesgloseIncentivos() {
	obtenerIncentivosDiaInhabil();
	obtenerIncentivosRecEfectivo();
	obtenerIncentivosRecCompania();
	obtenerIncentivosRecSipac();
	obtenerIncentivosRechazos();
	obtenerTotalesIncentivos();
}

function obtenerIncentivosDiaInhabil() {
	incentivosDiaInhabilGrid = new dhtmlXGridObject('listadoPercepcionesHorarioInhabilGrid');
	incentivosDiaInhabilGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorHorarioInhabil");});
	incentivosDiaInhabilGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorHorarioInhabil");});

	incentivosDiaInhabilGrid.load(obtenerIncentivosDiaInhabilPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}

function obtenerIncentivosRecEfectivo() {
	incentivosRecEfectivoGrid = new dhtmlXGridObject('listadoPercepcionesRecEfectivoGrid');
	incentivosRecEfectivoGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorRecEfectivo");});
	incentivosRecEfectivoGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorRecEfectivo");});

	incentivosRecEfectivoGrid.load(obtenerIncentivosRecEfectivoPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}

function obtenerIncentivosRecCompania() {
	incentivosRecCompaniaGrid = new dhtmlXGridObject('listadoPercepcionesRecCompaniaGrid');
	incentivosRecCompaniaGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorRecCompania");});
	incentivosRecCompaniaGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorRecCompania");});

	incentivosRecCompaniaGrid.load(obtenerIncentivosRecCompaniaPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}

function obtenerIncentivosRecSipac() {
	incentivosRecSipacGrid = new dhtmlXGridObject('listadoPercepcionesRecSipacGrid');
	incentivosRecSipacGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorRecSipac");});
	incentivosRecSipacGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorRecSipac");});

	incentivosRecSipacGrid.load(obtenerIncentivosRecSipacPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}

function obtenerIncentivosRechazos() {
	incentivosRechazosGrid = new dhtmlXGridObject('listadoPercepcionesRechazosGrid');
	incentivosRechazosGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorRechazos");});
	incentivosRechazosGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorRechazos");});

	incentivosRechazosGrid.load(obtenerIncentivosRechazosPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}


function obtenerTotalesIncentivos() {
	totalesIncentivosGrid = new dhtmlXGridObject('listadoPercepcionesTotalesGrid');
	totalesIncentivosGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicadorTotales");});
	totalesIncentivosGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicadorTotales");});

	totalesIncentivosGrid.load(obtenerTotalesIncentivosPath + "?configuracionId=" + jQuery("#configuracionId").val() ) ;
}

function registrarPercepcion(){
	var url = registrarPercepcionPath + '?configuracionId=' + jQuery("#configuracionId").val();
	sendRequestJQ(null, url, targetWorkArea, null);
	
}

function solicitarAutorizacion() {
	//TODO:
}

function cancelarPercepcion() {
	if(confirm('\u00BFEst\u00E1 seguro de cancelar la Percepci\u00F3n\u003F')){
		var url = cancelarConfigIncentivosPatch + "?configuracion.id=" + jQuery("#configuracionId").val();
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function cerrarRegistro() {
	
	if(confirm("Se perder\u00E1n los cambios que no hayan sido guardados. \u00BFDesea continuar\u003F")){
		
		var estatus = jQuery("#h_estatus").val();
		
		if (estatus == 'TRAMITE') {
			sendRequestJQ(null, eliminarConfigIncentivosPath  + '?configuracionId=' + jQuery("#configuracionId").val(), targetWorkArea, null);
		} else {
			sendRequestJQ(null, cerrarRegistroPath, targetWorkArea, null);
		}
		
	}
	
}

function regresarDetalle() {
	var url = mostrarDetallePath + '?configuracion.id=' + jQuery("#configuracionId").val();
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarOcultarBotones() {
	var estatus = jQuery("#h_estatus").val();
	
	if (estatus == 'TRAMITE') {
		jQuery("#btn_regresar").show();
		jQuery("#btn_registrar").show();
	} else if (estatus == 'REGISTRADO') {
		jQuery("#btn_solicitud").show();
		jQuery("#btn_cancelar").show();
	}
	
}

function enviaAutorizacion(){
	var url = enviarAutorizacionPath + '?configuracionId=' + jQuery("#configuracionId").val();
	sendRequestJQ(null, url, targetWorkArea, null);
}

