var conceptosGridGastos;
var conceptosGridReserva;
var coberturasAsociadasGrid;
var conceptosGridReembolso;
var coberturasGrid;
var tipoPrestadorGrid;
var negociosId = new Array();
function aplicaIVA(){
	var aplicaIva = dwr.util.getValue("aplicaIva");	
	if(aplicaIva == false ){
		jQuery("#porcIva").attr("value",'0');
	}
}

function aplicaISR(){
	var aplicaIsr = dwr.util.getValue("aplicaIsr");	
	if(aplicaIsr == false ){
		jQuery("#porcIsr").attr("value",'0');
	}
}


function aplicaIVARetenido(){
	var aplicaIvaRetenido = dwr.util.getValue("aplicaIvaRetenido");	
	if(aplicaIvaRetenido == false ){
		jQuery("#porcIvaRetenido").attr("value",'0');
	}
}



function checkAplicaIva(){
	var aplicaIva = dwr.util.getValue("aplicaIva");	
	if(aplicaIva == false){
		jQuery("#porcIva").attr("value",'0');
		jQuery("#aplicaIva").attr("value",false);
		jQuery("#porcIva").attr('disabled','disabled');
	}else{
		jQuery("#aplicaIva").attr("value",true);
		jQuery("#porcIva").removeAttr("disabled");

	}
	
}


function checkAplicaIsr(){
	var aplicaIsr = dwr.util.getValue("aplicaIsr");	
	if(aplicaIsr ==false){
		jQuery("#porcIsr").attr("value",'0');
		jQuery("#aplicaIsr").attr("value",false);
		jQuery("#porcIsr").attr('disabled','disabled');


	}else{
		jQuery("#aplicaIsr").attr("value",true);
		jQuery("#porcIsr").removeAttr("disabled");

	}
	
}

function checkAplicaIvaRetenido(){
	var aplicaIvaRetenido = dwr.util.getValue("aplicaIvaRetenido");	
	if(aplicaIvaRetenido ==false){
		jQuery("#porcIvaRetenido").attr("value",'0');
		jQuery("#aplicaIvaRetenido").attr("value",false);
		jQuery("#porcIvaRetenido").attr('disabled','disabled');

	}else{
		jQuery("#aplicaIvaRetenido").attr("value",true);
		jQuery("#porcIvaRetenido").removeAttr("disabled");


	}
	
}



function validar(){
	var vguardar = true;
	
	
	var aplicaIva = dwr.util.getValue("aplicaIva");	
	var porcIva = dwr.util.getValue("porcIva");
	
	var aplicaIvaRetenido = dwr.util.getValue("aplicaIvaRetenido");	
	var porcIvaRetenido = dwr.util.getValue("porcIvaRetenido");
	
	
	var aplicaIsr = dwr.util.getValue("aplicaIsr");	
	var porcIsr = dwr.util.getValue("porcIsr");
	var categoria = dwr.util.getValue("ctgTipoCategoria");
	var tipoConcepto = dwr.util.getValue("ctgTipoConcepto");
	

	
	if(aplicaIva == true){
		if(porcIva >=100 || porcIva<=0 ){
			vguardar= false;
	    	mostrarMensajeInformativo('El %IVA Debe ser mayor a cero (0) y menor a cien (100)', '20');
	    	return false;
		}
	}
	
	if(aplicaIvaRetenido == true){
		if(porcIvaRetenido >=100 || porcIvaRetenido<=0 ){
			vguardar= false;
	    	mostrarMensajeInformativo('El %IVA Retenido Debe ser mayor a cero (0) y menor a cien (100)', '20');
	    	return false;
		}
	}
	
	if(aplicaIsr == true){
		if(porcIsr >=100 || porcIsr<=0 ){
			vguardar= false;
	    	mostrarMensajeInformativo('El %ISR Debe ser mayor a cero (0) y menor a cien (100)', '20');
	    	return false;
		}
	}
	
	return vguardar;
	
}



function cargaGridTipoPrestador(keyLineaNegocio){
	
	document.getElementById("tipoPrestadorGrid").innerHTML = '';
	tipoPrestadorGrid = new dhtmlXGridObject("tipoPrestadorGrid");
	tipoPrestadorGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	tipoPrestadorGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	tipoPrestadorGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	tipoPrestadorGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	tipoPrestadorGrid.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/listarTipoPrestador.action?&keyToCoberturasNegocio="+jQuery("#keyToCoberturasNegocio").val());
	
}


function mostrar(){
	var url = "/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/mostrar.action";
	sendRequestJQ(null, url ,"contenido", null);
}

function vincularCoberturas(){
	
	if( jQuery("#nombreConceptoSeleccion").text() != "" ){
		
		if ( jQuery("#keyToCoberturasNegocio").val() != "" ){
		
			jQuery(".negocioId").each(function (i){
				if ( jQuery(this).is(":checked") ){
					negociosId.push(jQuery(this).val());
				}
			});
			
			if ( negociosId.length > 0 & jQuery("#lineaDeNegocio").val() != "" ){
				var url = "/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/asociarCoberturas.action?keyCoberturasLineasNegocio="+negociosId+"&keyToCoberturasNegocio="+jQuery("#keyToCoberturasNegocio").val()+"&keyLineaNegocio="+jQuery("#lineaDeNegocio").val();
				sendRequestJQ(null, url ,"contenido", null);
			}else{
				mostrarMensajeInformativo('Seleccione las coberturas de la afectación de la reserva y/o línea de negocio', '10');
			}
		
		}else{
			mostrarMensajeInformativo('Seleccione la afectación de la reserva', '10');
		}
		
	}else{
		mostrarMensajeInformativo('Seleccione la afectación de la reserva', '10');
	}
	
}


function vincularPrestador(){
	
		
		if ( jQuery("#keyToCoberturasNegocio").val() != "" ){
		
			jQuery(".negocioId").each(function (i){
				if ( jQuery(this).is(":checked") ){
					negociosId.push(jQuery(this).val());
				}
			});
			
			if ( negociosId.length > 0  ){
				var url = "/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/asociarTipoPrestador.action?keyCoberturasLineasNegocio="+negociosId+"&keyToCoberturasNegocio="+jQuery("#keyToCoberturasNegocio").val()+"&keyLineaNegocio="+jQuery("#lineaDeNegocio").val();
				sendRequestJQ(null, url ,"contenido", null);
			}else{
				mostrarMensajeInformativo('Seleccione los Tipo Prestador Relacionados', '10');
			}
		
		}else{
			mostrarMensajeInformativo('Seleccione el Gasto Ajuste', '10');
		}
		

	
}


function crearConceptoPrincipal(){

	if(validateAll(true)){
		if(validar()){
			var data = jQuery("#altaConcepto").serialize();
			var url = "/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/guardar.action?"+data;
			sendRequestJQ(null, url ,"contenido", "mostrar();");
		}
		

		
	}
	
}

function verConceptoPrincipal(key,nombre,esValidoHGS,estatus,categoria, aplicaIva,aplicaIvaRetenido,aplicaIsr,porcIva,porcIsr,porcIvaRetenido,origen){
	
	// # CAMBIAR BOTON
	jQuery("#accionEditar").show();
	jQuery("#accionAlta").hide();
	
	jQuery("#keyNegocio").attr("value",key);
	jQuery("#nombre").attr("value",nombre);
	
	jQuery("#ctgTipoCategoria").attr("value",categoria);
	jQuery("#estatus").attr("value",estatus);
	
	jQuery("#aplicaIva").attr("value",aplicaIva);
	jQuery("#aplicaIvaRetenido").attr("value",aplicaIvaRetenido);
	jQuery("#aplicaIsr").attr("value",aplicaIsr);
	
	jQuery("#porcIva").attr("value",porcIva);
	jQuery("#porcIsr").attr("value",porcIsr);
	jQuery("#porcIvaRetenido").attr("value",porcIvaRetenido);

	if(esValidoHGS=="true"){
		jQuery("#enviaHGS").attr('checked', true);
	}else{
		jQuery("#enviaHGS").attr('checked', false);
	}
	
	
	if(aplicaIva=="true"){
		jQuery("#aplicaIva").attr('checked', true);
		jQuery("#porcIva").removeAttr("disabled");

	}else{
		jQuery("#aplicaIva").attr('checked', false);
		jQuery("#porcIva").attr('disabled','disabled');

		
	}
	
	if(aplicaIvaRetenido=="true"){
		jQuery("#aplicaIvaRetenido").attr('checked', true);
		jQuery("#porcIvaRetenido").removeAttr("disabled");

	}else{
		jQuery("#aplicaIvaRetenido").attr('checked', false);
		jQuery("#porcIvaRetenido").attr('disabled','disabled');


	}
	
	if(aplicaIsr=="true"){
		jQuery("#aplicaIsr").attr('checked', true);
		jQuery("#porcIsr").removeAttr("disabled");

	}else{
		jQuery("#aplicaIsr").attr('checked', false);
		jQuery("#porcIsr").attr('disabled','disabled');
	}
	
	if( origen != '3' ){
		// DESACTIVAR DE CTGTIPOCONCEPTO REEMBOLSO
		jQuery("#ctgTipoConcepto option[value='RGA']").remove();
	}else{
		// AGREGA RGA 
		validaOptionRga();
		// SELECCION POR DEFAULT
		jQuery("#ctgTipoConcepto option[value='RGA']").attr("selected",true);
	}


}

function inicializarConceptoCobertura(key,nombre){
	
	// # LIMPIAR GRID Y SELECT AL SELECCIONAR
	jQuery("#lineaDeNegocio option[value=0] ").attr("selected",true);
	jQuery("div#coberturasGrid").html("");

	// # SETEAR ID SELECCIONADO DE LA COBERTURA
	jQuery("#keyToCoberturasNegocio").attr("value",key);
	
	// # MOSTRAR NOMBRE DEL CONCEPTO SELECCIONADO
	jQuery(".nombreConcepto").html("<span id='nombreConceptoSeleccion'>"+nombre+"</span>");
	
	// # MOSTRAR LADO DERECHO
	jQuery(".accionesAfectacion").show();
}

function mostarCoberturasAsociadas(keyConcepto,nombreConcepto){
	
	var url = "/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/mostrarCoberturasConceptoAsociadas.action";
	mostrarVentanaModal("vm_tipoServicio", "CoberturasAsociadas", null, null, 700, 500, url+'?keyToCoberturasNegocio='+keyConcepto+"&nombreConcepto="+nombreConcepto, "");
	
}

function cargaGridCoberturaAsociadas(keyLineaNegocio){
	
	document.getElementById("coberturasGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	coberturasGrid = new dhtmlXGridObject("coberturasGrid");
	coberturasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	coberturasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	coberturasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	coberturasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	coberturasGrid.attachHeader("#select_filter,#select_filter,");
	
	coberturasGrid.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/mostrarCoberturasConceptoAsociadasGrid.action?keyToCoberturasNegocio="+jQuery("#keyToCoberturasNegocio").val());
	
}

function inicializarConceptoTipoPrestador(key, nombre){
	
	// # LIMPIAR GRID Y SELECT AL SELECCIONAR
	jQuery("div#tipoPrestadorGrid").html("");
	
	// # SETEAR ID SELECCIONADO DE LA COBERTURA
	jQuery("#keyToCoberturasNegocio").attr("value",key);
	
	// # MOSTRAR NOMBRE DEL CONCEPTO SELECCIONADO
	jQuery(".nombreConcepto").html("<span id='nombreConceptoSeleccion'>"+nombre+"</span>");
	
	// # MOSTRAR LADO DERECHO
	jQuery(".accionesGastoAjuste").show();
	
	cargaGridTipoPrestador(key);
}




function cargaGrids(){
	cargaGridGastos();
	cargaGridAfectacionReserva();
	cargaGridReembolso();
	
	var aplicaIva = dwr.util.getValue("aplicaIva");	
	var aplicaIsr = dwr.util.getValue("aplicaIsr");	
	var aplicaIvaRetenido = dwr.util.getValue("aplicaIvaRetenido");	
	
	if(aplicaIva == false){

		jQuery("#porcIva").attr('disabled','disabled');
	}else{
		jQuery("#porcIva").removeAttr("disabled");

	}
	
	
	if(aplicaIsr ==false){
		jQuery("#porcIsr").attr('disabled','disabled');


	}else{
		jQuery("#porcIsr").removeAttr("disabled");

	}
	
	if(aplicaIvaRetenido ==false){
		jQuery("#porcIvaRetenido").attr('disabled','disabled');

	}else{
		jQuery("#aplicaIvaRetenido").attr("value",true);
		jQuery("#porcIvaRetenido").removeAttr("disabled");


	}
	
	
}

function cargaGridGastos(){
	
	document.getElementById("conceptosGridGastos").innerHTML = '';
	conceptosGridGastos = new dhtmlXGridObject("conceptosGridGastos");
	conceptosGridGastos.attachEvent("onXLS", function(grid_obj){blockPage()});
	conceptosGridGastos.attachEvent("onXLE", function(grid_obj){unblockPage()});
	conceptosGridGastos.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	conceptosGridGastos.setHeader   ("Clave,         Estatus       ,Fecha Alta    ,Fecha Baja,");
	conceptosGridGastos.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,,");
	
	conceptosGridGastos.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	conceptosGridGastos.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/listadoGastosAjuste.action");
	
}

function cargaGridAfectacionReserva(){
	
	document.getElementById("conceptosGridReserva").innerHTML = '';
	conceptosGridReserva = new dhtmlXGridObject("conceptosGridReserva");
	conceptosGridReserva.attachEvent("onXLS", function(grid_obj){blockPage()});
	conceptosGridReserva.attachEvent("onXLE", function(grid_obj){unblockPage()});
	conceptosGridReserva.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	conceptosGridReserva.setHeader   ("Clave,		Estatus       ,Fecha Alta    ,Fecha Baja,,"); // #SE AGREGAN COMAS SIN DATOS PARA RELLENAR ESPACIO CON VERDE
	conceptosGridReserva.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,,,");
	
	conceptosGridReserva.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	conceptosGridReserva.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/listarAfectacionReserva.action");
	
}


function cargaGridReembolso(){
	
	document.getElementById("conceptosGridReembolso").innerHTML = '';
	conceptosGridReembolso = new dhtmlXGridObject("conceptosGridReembolso");
	conceptosGridReembolso.attachEvent("onXLS", function(grid_obj){blockPage()});
	conceptosGridReembolso.attachEvent("onXLE", function(grid_obj){unblockPage()});
	conceptosGridReembolso.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	conceptosGridReembolso.setHeader   ("Clave,         Estatus       ,Fecha Alta    ,Fecha Baja,");
	conceptosGridReembolso.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,,");
	
	conceptosGridReembolso.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	conceptosGridReembolso.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/listarReembolso.action");
	
}


function cargaGridCobertura(keyLineaNegocio){
	
	// # MOSTRAR ETIQUETA CABEZA
	jQuery("#lista_afectacion_reserva").show();
	
	document.getElementById("coberturasGrid").innerHTML = '';
	coberturasGrid = new dhtmlXGridObject("coberturasGrid");
	coberturasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	coberturasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	coberturasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	coberturasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	coberturasGrid.load("/MidasWeb/siniestros/catalogo/conceptos/conceptoAjusteAction/listarCoberturas.action?keyLineaNegocio="+keyLineaNegocio+"&keyToCoberturasNegocio="+jQuery("#keyToCoberturasNegocio").val());
	
}


function validarCategoria(elemento){
	
	if( elemento.value == 1 || elemento.value == 2 ){
		// DESACTIVAR DE CTGTIPOCONCEPTO REEMBOLSO
		jQuery("#ctgTipoConcepto option[value='RGA']").remove();
	}else{
		// AGREGA RGA 
		validaOptionRga();
		// SELECCION POR DEFAULT
		jQuery("#ctgTipoConcepto option[value='RGA']").attr("selected",true);
	}
}

function validaOptionRga(){
	var bandera = "n";
	jQuery("#ctgTipoConcepto > option").each(function(index, value){
		if( this.value == "RGA" ){
			bandera = "s";
		}
	});
	
	if (bandera == "n"){
		jQuery("#ctgTipoConcepto").append("<option value='RGA'>REEMBOLSO DE GASTO DE AJUSTE</option>");
	}
}