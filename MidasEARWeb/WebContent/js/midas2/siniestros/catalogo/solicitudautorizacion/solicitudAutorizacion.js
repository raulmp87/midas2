/**
 * 
 */

var solicitudAutorizacionGrid;


function initGridsSolicitudAutorizacion(){
	getSolicitudesAutorizadas();
}



 function getSolicitudesAutorizadas(){
	jQuery("#solicitudAutorizacionGrid").empty(); 
	solicitudAutorizacionGrid = new dhtmlXGridObject('solicitudAutorizacionGrid');
	solicitudAutorizacionGrid.attachEvent("onXLS", function(grid_obj){blockPage(); mostrarIndicadorCarga("indicador");});
	solicitudAutorizacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage(); ocultarIndicadorCarga("indicador");});
	solicitudAutorizacionGrid.load( "/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/obtenerSolicitudesAutorizadas.action" ) ;
	//solicitudAutorizacionGrid.loadXML("/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/obtenerSolicitudesAutorizadas.action" );
}
 
 
 
 
 function realizarBusqueda(){
	 var url = "/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/obtenerSolicitudesAutorizadas.action?" + jQuery(document.catSolicitudAutorizacionForm).serialize();
	 solicitudAutorizacionGrid.load( url) ;
 }
 

 function cerrarComentarios(){
 	parent.cerrarVentanaModal("vm_comentariosSolicitudAutorizacion",true);
 }


 
 function guardarComentarios(){
	 if(validateAll(true)){
		 var url="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/guardarComentarios.action?"
		 		+ "idSolicitudAutorizacion="+jQuery("#idSolicitudAutorizacion").val()
		 		+ "&tipoSolicitud="+jQuery("#tipoSolicitud").val()
		 		+ "&estatus="+jQuery("#estatus").val()
		 		+ "&comentarios="+jQuery("#comentarios").val();
		 
		parent.document.getElementById('urlRedirect').value = url;
		parent.cerrarVentanaModal("vm_comentariosSolicitudAutorizacion","redirectUrl()");
	 }
 }
 function redirectUrl(){
	 var url  = jQuery("#urlRedirect").val();	
	if(url!= null && url!= ""){
		parent.document.getElementById('urlRedirect').value = "";
		sendRequestJQ(null,url,targetWorkArea,null);

	}
 }
 
 
 function autorizarSolicitud(id , tipoSolicitud, idSolicitudPorTipoAutorizacion){
	 
	 var ruta="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/mostrarContenedorComentarios.action?"
	 		+ "idSolicitudAutorizacion="+id
	 		+ "&tipoSolicitud="+tipoSolicitud
	 		+ "&estatus="+1
	 		+ "&idSolicitudPorTipoAutorizacion="+idSolicitudPorTipoAutorizacion;
		mostrarVentanaModal("vm_comentariosSolicitudAutorizacion", 'Guardar Comentarios', 550, 310, 550, 170,ruta,null);
		
	 /*if(tipoSolicitud=="SAV"){
			var ruta="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/mostrarContenedorComentarios.action?"
		 		+ "idSolicitudAutorizacion="+id
		 		+ "&tipoSolicitud="+tipoSolicitud
		 		+ "&estatus="+1;
			mostrarVentanaModal("vm_comentariosSolicitudAutorizacion", 'Guardar Comentarios', 550, 310, 550, 170,ruta,null);
		}else{
			var url="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/cambiarEstatus.action?"
		 		+ "idSolicitudAutorizacion="+id
		 		+ "&tipoSolicitud="+tipoSolicitud
		 		+ "&estatus="+1;
				sendRequestJQ(null, url, targetWorkArea, null); 
		}*/
 }
 
 function cancelarSolicitud(id , tipoSolicitud, idSolicitudPorTipoAutorizacion){
	 
	 
		var ruta="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/mostrarContenedorComentarios.action?"
	 		+ "idSolicitudAutorizacion="+id
	 		+ "&tipoSolicitud="+tipoSolicitud
	 		+ "&estatus="+2
	 		+ "&idSolicitudPorTipoAutorizacion="+idSolicitudPorTipoAutorizacion;
		mostrarVentanaModal("vm_comentariosSolicitudAutorizacion", 'Guardar Comentarios', 550, 310, 550, 170,ruta,null);	
	 
	 /*if(tipoSolicitud=="SAV"){
			var ruta="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/mostrarContenedorComentarios.action?"
		 		+ "idSolicitudAutorizacion="+id
		 		+ "&tipoSolicitud="+tipoSolicitud
		 		+ "&estatus="+2;
			mostrarVentanaModal("vm_comentariosSolicitudAutorizacion", 'Guardar Comentarios', 550, 310, 550, 170,ruta,null);
	}else{
		 var url="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/cambiarEstatus.action?"
		 		+ "idSolicitudAutorizacion="+id
		 		+ "&tipoSolicitud="+tipoSolicitud
		 		+ "&estatus="+2;
		 sendRequestJQ(null, url, targetWorkArea, null);
		
	}*/
	
 }
 
 
 
function limpiarFormularioSolicitudAut(){
	jQuery('#catSolicitudAutorizacionForm').each (function(){
		  this.reset();
	});
}


function exportarExcel(){
	var url="/MidasWeb/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion/exportarResultados.action?" + jQuery(document.catSolicitudAutorizacionForm).serialize();
		window.open(url, "Solicitud_Autorizaccion");
}

