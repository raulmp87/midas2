

 var COLUMNA_AUTORIZAR = 8
 var COLUMNA_RECHAZAR = 9;

 var solicitudAutorizacionGrid;
 
 
 function realizarBusqueda(cargaFiltro){
 	console.log('realizarBusqueda:'+cargaFiltro);
 	var url = "/MidasWeb/siniestros/autorizacion/solicitudAutorizacionAntiguedad/buscarSolicitudes.action?";
 	if(cargaFiltro){
 		removeCurrencyFormatOnTxtInput(); // ELIMINAR FORMATO CURRENCY
 		if(validaCamposDeBusqueda()){
	 		 formParams = jQuery(document.busquedaAutorizacionesForm).serialize();
	 		 url+=formParams;
	 	}	
 	}
 	console.log('realizarBusqueda - url :'+url);
 	loadGrid(url);
 }

 function loadGrid(url){
 		 jQuery("#solicitudAutorizacionGrid").empty(); 
		 solicitudAutorizacionGrid = new dhtmlXGridObject('solicitudAutorizacionGrid');
		 solicitudAutorizacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 solicitudAutorizacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 solicitudAutorizacionGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicadorSolAutAntiguedad");
		    });
		 solicitudAutorizacionGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicadorSolAutAntiguedad');
		    });
		 solicitudAutorizacionGrid.attachEvent("onCheckbox",doOnCheck);
		 solicitudAutorizacionGrid.load( url, muestraOpcionAutorizaRechazar ) ;	
 }

 function muestraOpcionAutorizaRechazar(){
 	console.log('muestraOpcionAutorizaRechazar');
	for (var i = 0; i < solicitudAutorizacionGrid.getRowsNum(); i++) {
		var id = solicitudAutorizacionGrid.getRowId(i);
		console.log('El id es : '+id+' y el valor es : '+solicitudAutorizacionGrid.getUserData(id,"autoriza") );
		var autoriza = solicitudAutorizacionGrid.getUserData(id,"autoriza") ;
	 	console.log('Autoriza : '+autoriza);
	 	if(autoriza == 0){
	 		solicitudAutorizacionGrid.cellByIndex(i,COLUMNA_AUTORIZAR).setDisabled(true);
	 		solicitudAutorizacionGrid.cellByIndex(i,COLUMNA_RECHAZAR).setDisabled(true);
	 	}
	}
}


 function validaCamposDeBusqueda(){
	var condicionMonto = jQuery("#s_condicionesMonto").val();
	console.log('condicionMonto : '+condicionMonto);
	var monto = jQuery("#txt_monto").val();
	console.log('Monto : '+monto);

	if(monto != ""){
		if(condicionMonto==""){
			jQuery("#s_condicionesMonto").addClass("requerido"); 
			return false;
		}else{
			jQuery("#s_condicionesMonto").removeClass("requerido"); 
			jQuery("#s_condicionesMeses").removeClass("errorField"); 
		}

	}

	var condicionMeses = jQuery("#s_condicionesMeses").val();
	console.log('condicionMeses : '+condicionMeses);
	var meses = jQuery("#txt_meses").val();
	console.log('Monto : '+monto);
	if(meses != ""){
		if(condicionMeses==""){
			jQuery("#s_condicionesMeses").addClass("requerido"); 
			return false;
		}else{
			jQuery("#s_condicionesMeses").removeClass("requerido"); 
			jQuery("#s_condicionesMeses").removeClass("errorField"); 
		}

	}
	validaDatosRequeridos();
	console.log('TERMINA');
	return true;
}

function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}

 
function doOnCheck(rowId,cellInd,state){
 		console.log("User clicked on checkbox or checkbox on row "+rowId+" and cell with index "+cellInd+".State changed to "+state);
 		validaCheckBox(rowId,cellInd,state);
}

function validaCheckBox(fila, columna, state){
	if(state){
		if(columna == COLUMNA_AUTORIZAR){
			solicitudAutorizacionGrid.cells(fila,COLUMNA_RECHAZAR).setValue("0");
		}else if (columna == COLUMNA_RECHAZAR){
			solicitudAutorizacionGrid.cells(fila,COLUMNA_AUTORIZAR).setValue("0");
		}
	}	
}
 
 
function limpiarFormularioSolicitudAut(){
	jQuery('#busquedaAutorizacionesForm').each (function(){
		  this.reset();
	});
}

function getElementosPorColumna(columna) {
	console.log('columna: '+columna);
    var elmentoChecked = "";
    console.log('solicitudAutorizacionGrid.getRowsNum() :'+solicitudAutorizacionGrid.getRowsNum());
    for (var i = 0; i < solicitudAutorizacionGrid.getRowsNum(); i++) {
    	console.log('El valor de I : '+i);
    	var value = solicitudAutorizacionGrid.cells2(i, columna).getValue();
    	console.log('value : '+value);
    	if( value == 1 ){
    		var solicitudId = solicitudAutorizacionGrid.getRowId(i);
    		console.log('solicitudId : '+solicitudId);
    		if(elmentoChecked != ""){
    			elmentoChecked += ",";
    		}
    		elmentoChecked += solicitudId;
    		console.log('elmentoChecked : '+elmentoChecked);
    	}
    }
    return elmentoChecked;
}

function autorizarRechazar(){
	console.log('autorizarRechazar method');
	removeCurrencyFormatOnTxtInput(); // ELIMINAR FORMATO CURRENCY
	var elementosAAutorizar = getElementosPorColumna(COLUMNA_AUTORIZAR);
	var elementosARechazar = getElementosPorColumna(COLUMNA_RECHAZAR);
	var formParams = jQuery(document.busquedaAutorizacionesForm).serialize();
	console.log('elementosAAutorizar : '+elementosAAutorizar);
	console.log('elementosARechazar : '+elementosARechazar);
	var url = "/MidasWeb/siniestros/autorizacion/solicitudAutorizacionAntiguedad/cambiarEstatus.action?"+formParams+"&lstAutorizar="+elementosAAutorizar+"&lstRechazar="+elementosARechazar;
	console.log('url--->'+url);
	loadGrid(url);
}


function exportarExcel(){
	var url="/MidasWeb/siniestros/autorizacion/solicitudAutorizacionAntiguedad/exportarResultados.action?" + jQuery(document.busquedaAutorizacionesForm).serialize();;
	window.open(url, "Solicitudes_de_autorizacion");
}

function consultarReporte( idToReporte ){
	var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarBuscarReporte.action?bandejaSolicitudesAutorizacion=true&idToReporte="+idToReporte+"&vieneBandejaSolicitudAntiguedad=true";
	sendRequestJQ(null, url,"contenido", "setConsultaReporte();");
}




