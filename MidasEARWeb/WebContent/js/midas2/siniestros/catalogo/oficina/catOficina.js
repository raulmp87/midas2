/**
 * 
 */
var cantEstadosAsignados;
// Inicializa el Greid de la lista de Oficinas
function iniContenedorOficinaGrid() {
	
	var oficinaGrid;
	var urlConsulta = "/MidasWeb/siniestros/catalogo/oficina/gridCatalogoOficina.action" ;
	oficinaGrid = new dhtmlXGridObject("oficinaGrid");
	oficinaGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	oficinaGrid.setSkin("light");	
	oficinaGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	oficinaGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });		
	oficinaGrid.init();
	oficinaGrid.load(urlConsulta);
	
}

function agregarOficina() {
	var path = "/MidasWeb/siniestros/catalogo/oficina/mostrarListado.action";
	sendRequestJQ(null, path, 'contenido', null);
}

function guardarOficina() {
	if(validateAll(true) && validaHttpEndPoint() ){		
		var formParams = jQuery(document.contendorListadoForm).serialize();
		console.log(formParams);
		var path = "/MidasWeb/siniestros/catalogo/oficina/salvarCatalogo.action?" + formParams;
		sendRequestJQ(null, path, 'contenido', null);
	}
}

function limpiarPantalla(){
	var path = "/MidasWeb/siniestros/catalogo/oficina/mostrarContenedor.action?&consulta="+false;
	sendRequestJQ(null, path, 'contenido', null);
}

function cerrarAltaOficina(){
	var cerrar = true;
	var consulta = jQuery('#banderaConsulta').val();
	if(consulta !== 'true'){
		if(!confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?')){
			cerrar = false;
		}
	}
	if(cerrar){
		var path = "/MidasWeb/siniestros/catalogo/oficina/mostrarContenedor.action?&consulta="+false;
		sendRequestJQ(null, path, 'contenido', null);
	}
	
}

function buscarListaOficinas(){
	if(validateAll(true)){	
		formParams = encodeForm(jQuery(document.contenedorForm));
		var path = "/MidasWeb/siniestros/catalogo/oficina/buscar.action?" + formParams;
		var oficinaGrid = new dhtmlXGridObject("oficinaGrid");
		oficinaGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
		oficinaGrid.setSkin("light");
		oficinaGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		oficinaGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
	    });			
		oficinaGrid.init();
		oficinaGrid.load(path);
	}
}

/*Funciones base pra el pintado del Grid*/

var estadosDisponiblesGrid;
var estadosAsociadosGrid;
var relanioEtadosDataProcessor;
var primeraCargaDataProcesor = 1;


 function getEstadosDisponiblesAsociados(entidadId, consulta){
	jQuery("#estadosAsociados").empty();
	var estadosAsociadosProcessor;
	var url = "/MidasWeb/siniestros/catalogo/oficina/relacionarEstadoOficina.action?entidad.id="+entidadId;
	estadosAsociadosGrid = new dhtmlXGridObject('estadosAsociados');
	estadosAsociadosGrid.load("/MidasWeb/siniestros/catalogo/oficina/loadGridEstadosAsociados.action?entidad.id="+entidadId + "&consulta="+consulta, function(){cantEstadosAsignados = estadosAsociadosGrid.getRowsNum();});

	// Creacion del DataProcessor
	estadosAsociadosProcessor = new dataProcessor(url);
	estadosAsociadosProcessor.enableDataNames(true);
	estadosAsociadosProcessor.setTransactionMode("POST");
	estadosAsociadosProcessor.setUpdateMode("cell");
	estadosAsociadosProcessor.attachEvent("onAfterUpdate", loadEstadosPais);
	estadosAsociadosProcessor.attachEvent("onBeforeDataSending", function(id, state, data){	
																	return validaAsignacionEstado(estadosAsociadosGrid);
																	});
	estadosAsociadosProcessor.init(estadosAsociadosGrid);
	
}

 function loadEstadosPais() {
	cargarGridsEstadosPorPais( jQuery('#comboPais').val(), jQuery('#id').val(), jQuery('#banderaConsulta').val());
 }
 
 function cargarGridsEstadosPorPais( paisId, entidadId, consulta ){
	 if (paisId != null && entidadId != null){
		 getEstadosDisponiblesPais(paisId, entidadId, consulta);
		 getEstadosDisponiblesAsociados(entidadId, consulta);
	 }
 }
 
 function getEstadosDisponiblesPais(paisId, entidadId, consulta){	
	estadosDisponiblesGrid = new dhtmlXGridObject('estadosDisponibles');
	estadosDisponiblesGrid.load("/MidasWeb/siniestros/catalogo/oficina/loadGridEstadosDisponiblesPais.action?paisId="+paisId+"&entidad.id="+entidadId + "&consulta="+consulta);
 }
 
 function edicionDeForma(banderaEdit){
	 if(banderaEdit == 'true'){ 
		 jQuery(".campoForma").attr('disabled', true);
	 }
 }
 
 function editarOficina(oficinaId){
	 
	var path = "/MidasWeb/siniestros/catalogo/oficina/mostrarListado.action?entidad.id="+oficinaId+"&consulta="+false;
	sendRequestJQ(null, path, 'contenido', null);
	 
 }
 
 function consultarOficina(oficinaId){
	 
	var path = "/MidasWeb/siniestros/catalogo/oficina/mostrarListado.action?entidad.id="+oficinaId+"&consulta="+true;
	sendRequestJQ(null, path, 'contenido', null);
	 
 }
 
 function asociarTodosEstados(oficinaId, paisId){
	 
	var path = "/MidasWeb/siniestros/catalogo/oficina/relacionTodosEstados.action?paisId="+paisId +"&entidad.id="+oficinaId+"&consulta="+false;
	sendRequestJQ(null, path, 'contenido', null);
	
 }
 
 function eliminarTodosEstadosRelacionados(oficinaId){
	 var formParams = jQuery(document.contendorListadoForm).serialize();
	var path = "/MidasWeb/siniestros/catalogo/oficina/eliminarTodosEstadosRelacionados.action?entidad.id="+oficinaId+"&consulta="+false + formParams;
	sendRequestJQ(null, path, 'contenido', null);
 }
 
 function exportarExcel(){
	 formParams = encodeForm(jQuery(document.contenedorForm));
	 var url="/MidasWeb/siniestros/catalogo/oficina/exportarListaOficinas.action?" + formParams;
	 	window.open(url, "Listado_Condiciones");
 }

 function validaAsignacionEstado(estadosAsociadosGrid){
	 cantEstadosAsignados = estadosAsociadosGrid.getRowsNum();
	 var estatus = jQuery('#estatus').val()
	 if(cantEstadosAsignados == 1 && estatus == 1){
		 mostrarMensajeInformativo("No es posible quitar todos los estados relacionados si la oficina se encuentra activa.", '10');
	 }
	 return true;
 }
 
 function habilitarWS(){
	 
	 if( jQuery("#aplicaWS").is(":checked") ){
		 jQuery("#endpoint").attr("disabled",false);
		 console.log("chequeado");
	 }else{
		 jQuery("#endpoint").attr("disabled",true);
		 jQuery("#endpoint").value("");
	 }
 }
 
 function habilitaEdicionWS(){
	 
	 if( jQuery("#aplicaWS").is(":checked") ){
		 jQuery("#endpoint").attr("disabled",false);
	 }
 }
 
 /***
  * Valida que la URL comience con HTTP o HTTPS
  * @returns {Boolean}
  */
 function validaHttpEndPoint(){
	 var patt;
	 if( jQuery("#aplicaWS").is(":checked") ){
		 patt = new RegExp("(HTTP|HTTPS):\/\/{1}");
		 if ( patt.test(jQuery("#endpoint").val()) ){
			 return true;
		 }else{
			 mostrarMensajeInformativo("Ingrese una URL de enpoint válida.", '10');
			 return false;
		 }
	 } else{
		 return true;
	 }
 }
