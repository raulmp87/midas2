var piezaGrid;

function validarAlta(){
	
}

function iniciaListadoGrid(){
	
	document.getElementById("piezaValuacionGrid").innerHTML = '';
	piezaGrid = new dhtmlXGridObject("piezaValuacionGrid");
	piezaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	piezaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	piezaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	piezaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		

	piezaGrid.load("/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/listarCarga.action");
}

function busquedaServicio(){
	
	if( validarPieza() ){
	
		var data=encodeForm(jQuery("#busquedaServicio"));
		
		document.getElementById("piezaValuacionGrid").innerHTML = '';
		var piezaGrid = new dhtmlXGridObject("piezaValuacionGrid");
		piezaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		piezaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		piezaGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
	
		piezaGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
		
		piezaGrid.load("/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/busqueda.action?"+data);
		
	}else{
		mostrarMensajeInformativo('Seleccione algún campo para realizar la búsqueda', '10');
	}
	
}

function validarPieza(){
	
	var numPieza    = jQuery("#numPieza").val();
	var seccion     = jQuery("#seccion").val();
	var descripcion = jQuery("#descripcion").val();
	var estatus     = jQuery("#estatus").val();
	
	if( numPieza == "" & seccion == "" & descripcion == "" & estatus == "" ){
		return false;
	}else{
		return true;
	}
}

function validarPiezaAlta(){
	
	var seccion     = jQuery("#seccion").val();
	var descripcion = jQuery("#descripcion").val();
	var estatus     = jQuery("#estatus").val();
	
	if( seccion == "" || descripcion == "" || estatus == "" ){
		return false;
	}else{
		return true;
	}
}

function mostrarDetalleServicio(id){
	var url = "/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/verDetalle.action?accion=2&piezaValuacionId="+id;
	sendPeticion(url,"contenido");
}

function editarDetalleServicio(id){
	if(validateAll(true)){
		var url = "/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/verDetalle.action?accion=0&piezaValuacionId="+id;
		sendPeticion(url,"contenido");
	}
}

function agregarDetalleServicio(){
	
	if(validateAll(true)){
		var data=jQuery("#altaModificarCtg").serialize();
		var valuacionBusquedaParams = jQuery("#informacionBusquedaValuacionesForm").serialize();
		var url = "/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/salvarActualizar.action?"+data;
		if(valuacionEsPantallaPrevia){
			url += "&" + valuacionBusquedaParams;
		}
		sendPeticion(url,"contenido");
	}else{
		mostrarMensajeInformativo('Todos los campos son obligatorios', '10');
	}
}


function regresar(){
	var valuacionEsPantallaPrevia = jQuery("#valuacionEsPantallaPrevia").val();
	var idValuacionReporte = jQuery("#idValuacionReporte").val();
	var esConsulta = jQuery("#esConsulta").val(); 
	var	formParams = jQuery(document.informacionBusquedaValuacionesForm).serialize();
	if(valuacionEsPantallaPrevia){
		var url = obtenerPath + '?idValuacionReporte=' +  idValuacionReporte + "&esConsulta=" + esConsulta + "&" + formParams;
		sendRequestJQ(null, url, targetWorkArea, null);
	}else{
		var url = "/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/mostrar.action";
		sendPeticion(url,"contenido");
	}
}

function muestraAlta(){
	var url = "/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/mostrarAlta.action?accion=1";
	sendPeticion(url,"contenido");
}


function sendPeticion(url,contenedor){
	sendRequestJQ(null, url ,contenedor, null);
}

function deshabilitarComponentes(){
	jQuery("input,select").attr("disabled",true);
}

function exportarExcel(){
	var data=encodeForm(jQuery("#busquedaServicio"));
	var url="/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/exportarExcel.action?" + data;
	window.open(url, mensaje);
}