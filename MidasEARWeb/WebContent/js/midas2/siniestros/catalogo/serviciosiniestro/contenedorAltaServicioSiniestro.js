jQuery(function(){
	validaComboTipoPersonaSiniestro(true);
	initServicioSiniestro();
	if (puedeCargarFoto()) {
		jQuery("#fotoPersonaMin").click(function() {
			mostrarVentanaModalMode("cargarFotoModal", "Cargar Foto", 100, 100, 1200, 600, "ATTACH_HTML_STRING",
					jQuery("#cargarFotoModalDiv").html());
		}).css('cursor', 'pointer');
	}
	jQuery("#btnCargarFoto").click(function(){
		jQuery("#formCargarFoto").submit();
	})
	jQuery("#transFrame").load(recargarFoto);
});

function recargarFoto(){
	var newSrc = config.contextPath + '/persona/obtenerFoto.action?idPersona='
	  + jQuery('#idPersonaMidas').val() + '&random=' + Math.random();
	jQuery('.fotoPersona').attr('src', newSrc);
	jQuery('#fotoPersonaMin').attr('src', newSrc);
}

function puedeCargarFoto() {
	return jQuery('#idPersonaMidas').val();
}