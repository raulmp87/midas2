/**
 * 
 */

function iniciaListadoGrid(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	
	document.getElementById("servicioSiniestroGrid").innerHTML = '';
	var servicioSiniestroGrid = new dhtmlXGridObject("servicioSiniestroGrid");
	servicioSiniestroGrid.attachEvent("onXLS", function(grid){blockPage()});
	servicioSiniestroGrid.attachEvent("onXLE", function(grid){unblockPage()});
	servicioSiniestroGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	servicioSiniestroGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	servicioSiniestroGrid.load("/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/listarCarga.action?tipoServicioOperacion="+tipoServicioOperacion);
	servicioSiniestroGrid.getRowsNum();
}

function validaListadoVacio(){
	var table_ = servicioSiniestroGrid.firstChild.nextSibling.firstChild;
	var trs = table_.getElementsByTagName('tr');
	 var numFilas = trs.length;
	 
	 if( numFilas < 2 ){
		 jQuery("#d_boxLight").addClass("dhx_pbox_light");
		 jQuery("#d_pagerInfo").addClass("dhx_pager_info_light");
		 jQuery("#d_pagerInfo").html("<div>No se encontraron registros</div>");
	 }else{
		 jQuery("#d_boxLight").removeClass("dhx_pbox_light");
		 jQuery("#d_pagerInfo").removeClass("dhx_pager_info_light");
		 jQuery("#d_pagerInfo").html("");
	 }
}

function iniciaListadoGridDeAsignar(){
	
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	
	document.getElementById("servicioSiniestroGrid").innerHTML = '';
	var servicioSiniestroGrid = new dhtmlXGridObject("servicioSiniestroGrid");
	servicioSiniestroGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	servicioSiniestroGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	servicioSiniestroGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	servicioSiniestroGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	servicioSiniestroGrid.load("/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/listarCargaAsignacion.action?tipoServicioOperacion=1");
}


function muestraAlta(){
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/altaServicio.action?tipoServicioOperacion="+tipoServicioOperacion+"&accion=1";
	sendPeticion(url,"contenido");
	
}


function busquedaServicioSiniestro(){

		if(validateAll(true)){		
			var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
			var data=encodeForm(jQuery("#busquedaServicio"));
			document.getElementById("servicioSiniestroGrid").innerHTML = '';
			var servicioSiniestroGrid = new dhtmlXGridObject("servicioSiniestroGrid");
			servicioSiniestroGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
			servicioSiniestroGrid.attachEvent("onXLE", function(grid_obj){
				unblockPage();
				validaListadoVacio();
			});
			servicioSiniestroGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicador");
		    });
			servicioSiniestroGrid.attachEvent("onRowDblClicked",function(rId,cInd){
	
				parent.setAjustador(rId,servicioSiniestroGrid.cells(rId,1).getValue());
				
			});
			servicioSiniestroGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
		    });	
			servicioSiniestroGrid.load("/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/busquedaGeneral.action?"+data);
		}
}



function regresar(){
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/mostrar.action?tipoServicioOperacion="+tipoServicioOperacion;
	sendPeticion(url,"contenido");
}

function agregarDetalleServicio(catalogo){
	if(validateAll(true)){		
		var data=jQuery("#altaModificacionServcio").serialize();
		var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/salvarServicio.action?"+data;
	
		var mensaje = validarAlta( catalogo ); 
		if ( mensaje == ""){
			sendPeticion(url,"contenido");
		}else{
			mostrarMensajeInformativo(mensaje, '10');
		}
	}
}

function modificarDetalleServicio(catalogo){
	if(validateAll(true)){		
		var data=jQuery("#altaModificacionServcio").serialize();
	
		var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/actualizarServicio.action?"+data;
		
		var mensaje = validarAlta( catalogo ); 
		if ( mensaje == ""){
			sendPeticion(url,"contenido");
		}else{
			mostrarMensajeInformativo(mensaje, '10');
		}
	}
	
}


function validarAlta( catalogo ){
	
	var mensaje = "";
	
	var estatus       		= jQuery("#estatus").val();
	var nombrePersona 		= jQuery("#nombrePersona").val();
	var paterno       		= jQuery("#apellidoPaterno").val();
	var materno       		= jQuery("#apellidoMaterno").val();
	var tipoServicio  		= jQuery("#ambitoPrestadorServicio").val();
	var oficinaId     		= jQuery("#oficinaId").val();
	var email         		= jQuery("#correoPrincipal").val();
	var ladaTelUno          = jQuery("#telCasaLada").val();
	var telUno              = jQuery("#telCasa").val();
	var prestadorServicioId = jQuery("#prestadorServicioId").val();
	var correo				= jQuery("#correoPrincipal").val();
	var tipoPersona			= jQuery("#r_tipoPersona").val();
	var nombreEmp       	= jQuery("#txt_nombre_de_la_empresa").val();
	var nombreCom      		= jQuery("#txt_nombre_comercial").val();
	var nombreAdm      		= jQuery("#txt_administrador").val();
	
	console.log("TIPO PERSONA: "+tipoPersona);
	if( estatus == "" ){
		mensaje = "Seleccione un estatus";
	}else if (nombrePersona == "" && tipoPersona == "PF"){
		mensaje = "Ingrese el nombre de la persona";
	}else if ( paterno == "" && tipoPersona == "PF"){
		mensaje = "Ingrese el apellido patero";	
	}else if (tipoPersona == "PM" && nombreEmp == ""){
		mensaje = "Indique el nombre de la empresa";
	}else if (tipoPersona == "PM" && nombreCom == ""){
		mensaje = "Indique el nombre comercial de la empresa";
	}else if (tipoPersona == "PM" && nombreAdm == ""){
		mensaje = "Indique el nombre del administrador de la empresa";
	}else if ( tipoServicio == ""){
		mensaje = "Seleccione Interno o Externo";
	}else if ( tipoServicio == 2 & prestadorServicioId =="" ){
		mensaje = "Debe asignar un prestador de servicio";
	}else if ( oficinaId == "" ){
		mensaje = "Seleccione la oficina ";	
	}else if (catalogo == 1 && correo == ""){
		mensaje = "Ingrese el correo electrónico";
	}else if ( ladaTelUno == ""){
		mensaje = "Ingrese la lada para el telefono 1";
	}else if ( telUno == "" ){
		mensaje = "Ingrese el numero de telefono 1";
	}else if ( email != ""){
		if ( !validaEmail(email) ){
			mensaje = "Email no valido";
		}
	}
	if (mensaje == ""){
		mensaje = validaDireccion();
	}

	return mensaje;
}

function validaDireccion(){
	var mensaje = "";
	var pais    = jQuery("select#servicioSiniestroDto\\.idPaisName").val();
	var edo     = jQuery("#servicioSiniestroDto\\.idEstadoName").val();
	var mpo     = jQuery("#servicioSiniestroDto\\.idCiudadName").val();
	var calle   = jQuery("#servicioSiniestroDto\\.calleName").val();
	var num     = jQuery("#servicioSiniestroDto\\.numeroName").val();
	var cp      = jQuery("#servicioSiniestroDto\\.cpName").val();
	var col     = jQuery("#servicioSiniestroDto\\.idColoniaName").val();

	if( pais == "" ){
		mensaje = "Seleccione un pais";
	}else if (edo == ""){
		mensaje = "Seleccione un estado";
	}else if (mpo == ""){
		mensaje = "Seleccione un municipio";
	}else if (col == ""){
		mensaje = "Seleccione una colonia";
	}else if (calle == ""){
		mensaje = "Ingrese una calle";
	}else if (num == ""){
		mensaje = "Ingrese número exterior";
	}else if (cp == ""){
		mensaje = "Ingrese código postal";
	}
	
	return mensaje;
}

function mostrarDetalleServicio(idServicio,idPersonaMidas){
	
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	
	var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/verDetalle.action?tipoServicioOperacion="+tipoServicioOperacion+"&idServicioSiniestro="+idServicio+"&idPersonaMidas="+idPersonaMidas+"&readOnly=true";
	sendPeticion(url,"contenido");
}

function editarDetalleServicio(idServicio,idPersonaMidas){
	
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	
	var url = "/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/verDetalle.action?tipoServicioOperacion="+tipoServicioOperacion+"&idServicioSiniestro="+idServicio+"&idPersonaMidas="+idPersonaMidas+"&readOnly=false&accion=0";
	sendPeticion(url,"contenido");
	
}


function exportarExcel(){
	
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	var mensaje = "Listado";
	
	if( tipoServicioOperacion == 1){
		mensaje +="_Ajustadores";
	}else{
		mensaje +="_Abogados";
	}
	
	var data=encodeForm(jQuery("#busquedaServicio"));
	
	var url="/MidasWeb/siniestros/catalogo/servicio/servicioSiniestro/exportarExcel.action?" + data;
	window.open(url, mensaje);

}

function mostrarPrestadorServicio(){
	
	var tipo = jQuery("#keyOperacionPrestadorServicio").val();
	var oficinaId = jQuery("#oficinaId").val();
	var ambitoPrestadorServicio = jQuery("#ambitoPrestadorServicio").val();
	if(ambitoPrestadorServicio == 2){
		var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarContenedor.action?modoAsignar=true&filtroCatalogo.estatus=1&filtroCatalogo.tipoPrestadorStr="+tipo+"&filtroCatalogo.oficinaId="+oficinaId; 
		mostrarVentanaModal("vm_asignarPrestador", "Asignar Prestador de Servicio", null, null, 926, 559, url , "");
	}
}

function setPrestadorId(prestadorId, nombrePrestador){

	jQuery("#prestadorServicioId").val(prestadorId);
	parent.cerrarVentanaModal("vm_asignarPrestador",true);
	
	// # FORZAR CATALOGO A TIPO A EXTERNO SI PRESTADORID -NO- ES NULO
	if( prestadorId!=null || prestadorId!="" ){
		jQuery("#ambitoPrestadorServicio option[value=2]").attr("selected",true);
	}
}

function deshabilitarComponentes(){
	jQuery("input,select").attr("disabled",true);
}



function sendPeticion(url,contenedor){
	sendRequestJQ(null, url ,contenedor, null);
}


function validaEmail(email){
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validarTipoPrestador(valor){
	if ( valor == 1){
		jQuery("#prestadorServicioId").attr("value","");
		jQuery("#buscarPrestador").hide();
	}else if ( valor == 2){
		jQuery("#buscarPrestador").show();
	}
	
}

function onChangeEstadoFiltro(targetId, estadoSelectId){	
	var estadoValue = dwr.util.getValue(estadoSelectId);	
	if(estadoValue != null && estadoValue != ''){
		listadoService.getMapMunicipiosPorEstadoMidas(estadoValue,
				function(data){
					addOptionOderByElementSin(targetId,data);
				});
	}else{
		addOptionsDireccionSin(targetId,"");
	}
}

function limpiar(){
	document.getElementById("busquedaServicio").reset();
}

function filtroValido(){
	var tipoServicioOperacion = jQuery("#tipoServicioOperacion").val();
	var nombreAbogadoAjustador       	= jQuery("#nombreAbogadoAjustador").val();
	var noPrestador						= jQuery("#noPrestador").val();
	var tipoAbogadoAjustador			= jQuery("#tipoAbogadoAjustador").val();
	var nombrePrestador					= jQuery("#nombrePrestador").val();
	var oficinaId						= jQuery("#oficinaId").val();
	var estatus							= jQuery("#estatus").val();
	var filtroValido = nombreAbogadoAjustador+noPrestador+tipoAbogadoAjustador+nombrePrestador+oficinaId+estatus;
	if (tipoServicioOperacion == 1){//Ajustador
		var noAbogadoAjustadorId			= jQuery("#noAbogadoAjustadorId").val();
		filtroValido += noAbogadoAjustadorId;
	}
	
	return filtroValido == "" || filtroValido == 'undefined' ? false : true;
}

function onClickTipoPersonaSiniestro(){
	var tipoPersona = jQuery('input[name=servicioSiniestroDto.tipoPersona]').filter(':checked').val();
	
	if(tipoPersona == "PF"){
		jQuery("#tr_personaFisica").show();
		jQuery("#tr_personaMoral").hide();
		jQuery("#tr_datosComplementariosPersona").show();
	}else if(tipoPersona == "PM"){
		console.log('tipoPersona '+tipoPersona);
		jQuery("#tr_personaFisica").hide();
		jQuery("#tr_personaMoral").show();
		jQuery("#tr_datosComplementariosPersona").hide();
	}
	jQuery("#txt_nombre_de_la_empresa").toggleClass("jQrequired");
	jQuery("#txt_nombre_comercial").toggleClass("jQrequired");
	jQuery("#txt_administrador").toggleClass("jQrequired");
	jQuery("#txt_nombres").toggleClass("jQrequired");
	jQuery("#txt_apellido_paterno").toggleClass("jQrequired");
	jQuery("#txt_apellido_materno").toggleClass("jQrequired");
	jQuery("#dp_fechaNacimiento").toggleClass("jQrequired");
	jQuery("#txt_curp").toggleClass("jQrequired");
}

function initTipoPersonaSiniestro( ){
	var tipoPersona = jQuery('input[name=servicioSiniestroDto.tipoPersona]').filter(':checked').val();
	if(tipoPersona == "PF"){
		jQuery("#tr_personaFisica").show();
		jQuery("#tr_personaMoral").hide();
		jQuery("#tr_datosComplementariosPersona").show();
	}else if(tipoPersona == "PM"){
		jQuery("#tr_personaFisica").hide();
		jQuery("#tr_personaMoral").show();
		jQuery("#tr_datosComplementariosPersona").hide();
	}
}


function validaComboTipoPersonaSiniestro(isEdicion){
	var numServicioSieniestro = dwr.util.getValue("servicioSiniestroDto.id");
	if(Number(numServicioSieniestro)!=0){
		jQuery("#r_tipoPersonaPF").attr("disabled","disabled");
		jQuery("#r_tipoPersonaPM").attr("disabled","disabled");
		jQuery("#tipo_persona_hidden").attr("name","servicioSiniestroDto.tipoPersona");
		jQuery("#tipo_persona_hidden").attr("value",tipoPersona);
		
	}else{
		jQuery("#r_tipoPersonaPF").attr("checked","checked");
	}
	initTipoPersonaSiniestro();
}

function initServicioSiniestro(){
	var tipoPersona = jQuery('input[name=servicioSiniestroDto.tipoPersona]').filter(':checked').val();
	console.log("tipo persona: "+tipoPersona);
	if(tipoPersona == "PF"){
		jQuery("#txt_nombres").toggleClass("jQrequired");
		jQuery("#txt_apellido_paterno").toggleClass("jQrequired");
		jQuery("#txt_apellido_materno").toggleClass("jQrequired");
	}if(tipoPersona == "PM"){
		jQuery("#txt_nombre_de_la_empresa").toggleClass("jQrequired");
		jQuery("#txt_nombre_comercial").toggleClass("jQrequired");
		jQuery("#txt_administrador").toggleClass("jQrequired");
	}
}