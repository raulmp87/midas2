var prestadorDeServicioGrid; 
var listadoCompradoresGrid;
var asignarGbl;
var tiposDePrestadorParams;
var docBancariaParams;
var prestadorServicioCompleto;
var guardado = false;


function buscar(){
	formParams = encodeForm(jQuery(document.busquedaPrestadorServicio));
	var url = buscarPrestadorServicioPath + '?' + formParams ;
	sendRequestJQ(null, url, targetWorkArea, "initGridPrestadorDeServicio();");	
}

function initGridPrestadorDeServicio(asignar){
	getPrestadorDeServicios();
	if(asignar!=null && asignar){
		asignarGbl = asignar;
		assignMode(asignar);
	}
}

function cerrar(){
	var url = buscarPrestadorServicioPath;
	sendRequestJQ(null, buscarPrestadorServicioPath, targetWorkArea, null);
}

function inicializarListadoPrestadorServicio(){	
	mostrarListadoPrestadorServicio();
}

function mostrarPrestadorServicio(){	
	sendRequestJQ(null, mostrarPrestadorServicioPath+"?modoEditar=true&consulta=false", targetWorkArea, null);
}

function buscarPrestadorServicio(){	
	var url = buscarPrestadorServicioPath;
	sendRequestJQ(null, buscarPrestadorServicioPath, targetWorkArea, "initGridPrestadorDeServicio();");
}



function getPrestadorDeServicios(){
	tiposDePrestadorParams = null;
	docBancariaParams = null;
	jQuery('#msjInicio').hide();		
	var paramsForm = encodeForm(jQuery(document.busquedaPrestadorServicio)); 
	jQuery("#prestadorDeServicioGrid").empty(); 
	var url = "";
	url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/buscar.action?modoAsignar="+asignarGbl+"&"+paramsForm;
	prestadorDeServicioGrid = new dhtmlXGridObject('prestadorDeServicioGrid');
  	  
  	prestadorDeServicioGrid.attachEvent("onXLS", function(grid){blockPage();});
  	prestadorDeServicioGrid.attachEvent("onXLE", function(grid){unblockPage();});
  	prestadorDeServicioGrid.attachEvent("onRowDblClicked",function(rId,cInd){
  		 parent.setPrestadorId(rId,prestadorDeServicioGrid.cells(rId,1).getValue());});
    prestadorDeServicioGrid.load(url) ;
    assignMode(asignarGbl);

}


function limpiarFiltros(){
	jQuery('#busquedaPrestadorServicio').each (function(){
		  this.reset();
	});
}

function onClickTipoPersona( ){
	var tipoPersona = jQuery('input[name=dto.tipoPersona]').filter(':checked').val();
	if(tipoPersona == "PF"){
		jQuery("#tr_personaFisica").show();
		jQuery("#tr_personaMoral").hide();
		jQuery("#tr_datosComplementariosPersona").show();
	}else if(tipoPersona == "PM"){
		jQuery("#tr_personaFisica").hide();
		jQuery("#tr_personaMoral").show();
		jQuery("#tr_datosComplementariosPersona").hide();
	}
	jQuery("#txt_nombre_de_la_empresa").toggleClass("jQrequired");
	jQuery("#txt_nombre_comercial").toggleClass("jQrequired");
	jQuery("#txt_administrador").toggleClass("jQrequired");
	jQuery("#txt_nombres").toggleClass("jQrequired");
	jQuery("#txt_apellido_paterno").toggleClass("jQrequired");
	jQuery("#txt_apellido_materno").toggleClass("jQrequired");
	jQuery("#dp_fechaNacimiento").toggleClass("jQrequired");
	jQuery("#txt_curp").toggleClass("jQrequired");
}

function initTipoPersona( ){
	var tipoPersona = jQuery('input[name=dto.tipoPersona]').filter(':checked').val();
	if(tipoPersona == "PF"){
		jQuery("#tr_personaFisica").show();
		jQuery("#tr_personaMoral").hide();
		jQuery("#tr_datosComplementariosPersona").show();
	}else if(tipoPersona == "PM"){
		jQuery("#tr_personaFisica").hide();
		jQuery("#tr_personaMoral").show();
		jQuery("#tr_datosComplementariosPersona").hide();
	}
}


function validaComboTipoPersona(isEdicion){
	var numPrestador = dwr.util.getValue("dto.numPrestador");
	if(Number(numPrestador)!=0){
		jQuery("#r_tipoPersonaPF").attr("disabled","disabled");
		jQuery("#r_tipoPersonaPM").attr("disabled","disabled");
		jQuery("#tipo_persona_hidden").attr("name","dto.tipoPersona");
		jQuery("#tipo_persona_hidden").attr("value",tipoPersona);
		
	}else{
		jQuery("#r_tipoPersonaPF").attr("checked","checked");
	}
	initTipoPersona();
}

function validaOtraColonia(){
	if(jQuery("#dto\\.nombreColoniaDiferente").value != ""){
		jQuery("#idColoniaCheck").attr("value","true");
	}
}

function disableAddressInfo(consulta){
	if(consulta==true){
		jQuery("#dto\\.idPaisString").attr("disabled","disabled");
		jQuery("#dto\\.idEstadoString").attr("disabled","disabled");
		jQuery("#dto\\.idMunicipioString").attr("disabled","disabled");
		jQuery("#dto\\.nombreColonia").attr("disabled","disabled");
		jQuery("#dto\\.codigoPostal").attr("disabled","disabled");
		jQuery("#dto\\.nombreCalle").attr("disabled","disabled");
		jQuery("#dto\\.nombreColoniaDiferente").attr("disabled","disabled");
		jQuery("#idColoniaCheck").attr("disabled","disabled");
		jQuery("#txt_referencia").attr("disabled","disabled");
	}
}

function ocultarDivsTipoPersona(){
	jQuery("#tr_personaFisica").hide();
	jQuery("#tr_fechas").hide();
	jQuery("#tr_personaMoral").hide();
}

function guardarTipoDePrestador(){
	var infoSerializada = jQuery(document.formTiposPrestador).serialize();
	if(infoSerializada != ""){
		parent.tiposDePrestadorParams = infoSerializada;
		jQuery(parent.tipoPrestadorSeleccionado).html(jQuery("#tipoDePrestador"));		
		parent.cerrarVentanaModal("vm_tipoServicio",true);
	}else{
		alert("Es necesario capturar un tipo de prestador");
	}
}

function guardarDocBancaria(){
	var prestadorId = jQuery("#prestadorId").val();
	if(prestadorId != null && prestadorId != 'undefined' && prestadorId != ""){
		if(validateAll(true)){
			var url =  "/MidasWeb/siniestros/catalogos/prestadorDeServicio/salvarDocBancaria.action?";
			var listadoInfoBancGrid = null;
			document.getElementById("pagingArea").innerHTML = '';
			document.getElementById("infoArea").innerHTML = '';
			listadoInfoBancGrid = new dhtmlXGridObject('listadoInfoBancGrid');
			listadoInfoBancGrid.attachEvent("onXLS", function(listadoGrid){blockPage();});
		    listadoInfoBancGrid.attachEvent("onXLE", function(listadoGrid){unblockPage();});
		    listadoInfoBancGrid.load(url + jQuery(document.infoBancariaForm).serialize() + "&prestadorId=" + prestadorId);
		    $('#infoBancariaForm')[0].reset();
		    guardado = true;
		    
		}
	}else{
		if(validateAll(true)){
			var infoSerializada = jQuery(document.infoBancariaForm).serialize();
			parent.docBancariaParams = infoSerializada;
			jQuery(parent.informacionBancariaSeleccionada).html(jQuery("#informacionBancaria"));
			parent.cerrarVentanaModal("vm_infoBancaria",true);
		}

	}
}


function addParamsForm(newParamForms){
	prestadorServicioCompleto = newParamForms + "&"+ tiposDePrestadorParams + "&" + docBancariaParams;
}

function salvarPrestadorServicio() {
	var mensajeError = validarFormulario();
	if(validateAll(true) && mensajeError == ""){
		var url =  "/MidasWeb/siniestros/catalogos/prestadorDeServicio/salvarCatalogo.action?";
		addParamsForm(jQuery(document.altaPrestadorServicio).serialize());
		sendRequestJQ(null, url + prestadorServicioCompleto, targetWorkArea, null);		
	}else{
		mostrarMensajeInformativo(mensajeError, '10');	
	}

		
}

function mostrarTipoPrestador(id, consulta){
	if(isFinite(id)){
		mostrarVentanaModal("vm_tipoServicio", "Tipos de Prestador ", null, null, 350, 384, mostrarTipoPrestadorPath+ '?prestadorId=' + id+'&consulta='+consulta , "");
//		jQuery('input[name=strTipoPrestadorSelected]').attr("disabled","disabled");
	}else{
		mostrarVentanaModal("vm_tipoServicio", "Tipos de Prestador ", null, null, 250, 300, mostrarTipoPrestadorPath+'?consulta='+consulta, "");
	}
	ocultarBotonCerrarVentanaModal("vm_tipoServicio");
		
}

function mostrarInfoBancaria(id, consulta){
	if(isFinite(id)){
		var rfc = dwr.util.getValue("dto.rfc");		
		mostrarVentanaModal("vm_infoBancaria", "Información Bancaria", null, null, 1020, 600, mostrarInfoBancariaPath+ '?prestadorId=' + id+'&consulta='+consulta+ '&rfc=' +rfc, "");
		//jQuery('input[name=strTipoPrestadorSelected]').attr("disabled","disabled");
	}else{
		mostrarVentanaModal("vm_infoBancaria", "Información Bancaria", null, null, 1020, 600, mostrarInfoBancariaPath+ '?consulta='+consulta+'&prestadorId='+jQuery("#prestadorId").val(), "");
	}
	ocultarBotonCerrarVentanaModal("vm_infoBancaria");
}

function mostrarListadoInfoBancaria(id, consulta){
	var formParams = null;
	var listadoInfoBancGrid = null;

	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoInfoBancGrid = new dhtmlXGridObject('listadoInfoBancGrid');

	listadoInfoBancGrid.attachEvent("onXLS", function(listadoGrid){blockPage();});
    listadoInfoBancGrid.attachEvent("onXLE", function(listadoGrid){unblockPage();});
   
	if(isFinite(id)){

		listadoInfoBancGrid.load( buscarInfoBancariaPath + '?prestadorId=' + id+'&consulta='+consulta);
//	jQuery('input[name=strTipoPrestadorSelected]').attr("disabled","disabled");
	}else{
		listadoInfoBancGrid.load( mostrarInfoBancariaPath+ '?consulta='+consulta );

	}
}

function exportarExcel(){
	var url="/MidasWeb/siniestros/catalogos/prestadorDeServicio/exportarResultados.action?" + encodeForm(jQuery(document.busquedaPrestadorServicio));
		window.open(url, "Prestadores_Listado");
}

function activar(id){
	var url="/MidasWeb/siniestros/catalogos/prestadorDeServicio/actualizaInfoBancaria.action?prestadorId="
		+jQuery("#prestadorId").val() +"&infoBancId="+id+"&consulta="+jQuery("#consulta").val();
	
	var listadoInfoBancGrid = null;
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoInfoBancGrid = new dhtmlXGridObject('listadoInfoBancGrid');
	listadoInfoBancGrid.attachEvent("onXLS", function(listadoGrid){blockPage();});
    listadoInfoBancGrid.attachEvent("onXLE", function(listadoGrid){unblockPage();});
    listadoInfoBancGrid.load(url);
}


function assignMode(asignar){
	if(asignar!=null && asignar){
		asignarGbl = asignar;
		jQuery("#wwgrp_s_oficinas").hide(); 
		jQuery("#wwgrp_s_estatus").hide();
		jQuery("#btn_exportar").hide();
		jQuery("#b_agregar").hide();
		jQuery("#s_tipo_prestador").attr("disabled","disabled");
		
		prestadorDeServicioGrid.setColumnHidden(2,true);
		prestadorDeServicioGrid.setColumnHidden(4,true);
		prestadorDeServicioGrid.setColumnHidden(5,true);
		prestadorDeServicioGrid.setColumnHidden(6,true);
	}
}

function validaRequeridos(){
    var requeridos = jQuery(".requerido");
    requeridos.each(
          function(){
                var these = jQuery(this);
                if( isEmpty(these.val()) ){
                      these.addClass("errorField");
                } else {
                      these.removeClass("errorField");
                }
          }
    );
}


function inicializarListadoInfoBancaria(){
	mostrarListadoInfoBancaria();
}

function salirInfoBancaria(consulta){
	if(!guardado){
		salirVentana("vm_infoBancaria", consulta);
	}else{
		parent.cerrarVentanaModal("vm_infoBancaria",true);
	}
}

function salirTipoPrestador(consulta){
	salirVentana("vm_tipoServicio", consulta);
}

function editar(id){
	var url = editarPrestadorServicioPath + '?prestadorId=' + id+"&modoEditar="+true;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function consultar(id){
	var url = consultarPrestadorServicioPath + '?prestadorId=' + id ;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function validaCompenenteDireccion(consulta){
	if(consulta){
		jQuery("#dto\\.idPaisString").attr("disabled","disabled");
		jQuery("#dto\\.idEstadoString").attr("disabled","disabled");
		jQuery("#dto\\.nombreColonia").attr("disabled","disabled");
		jQuery("#dto\\.idMunicipioString").attr("disabled","disabled");
		jQuery("#dto\\.codigoPostal").attr("disabled","disabled");
		jQuery("#dto\\.nombreCalle").attr("disabled","disabled");
		jQuery("#dto\\.nombreColoniaDiferente").attr("disabled","disabled");
		jQuery("#idColoniaCheck").attr("disabled","disabled");
		jQuery("#dto\\.nombreCalleNumero").attr("disabled","disabled");
		jQuery("#dto\\.referencia").attr("disabled","disabled");
		jQuery("#dto\\.numeroInterno").attr("disabled","disabled");
	}
}

function validarFormulario(){
	
	var mensaje = "";

	var prestadorId		= jQuery("#prestadorId").val();
	var oficina			= jQuery("#s_oficina").val();
	var tipoPersona		= jQuery("#r_tipoPersona").val();
	var estatus       	= jQuery("#s_tipo_estatus").val();
	var nombreEmp       = jQuery("#txt_nombre_de_la_empresa").val();
	var nombreCom      	= jQuery("#txt_nombre_comercial").val();
	var nombreAdm      	= jQuery("#txt_administrador").val();
	var nombrePer      	= jQuery("#txt_nombres").val();
	var apPaterno       = jQuery("#txt_apellido_paterno").val();
	var apMaterno       = jQuery("#txt_apellido_materno").val();
	var pais       		= jQuery("#dto\\.idPaisString").val();
	var estado       	= jQuery("#dto\\.idEstadoString").val();
	var municipio       = jQuery("#dto\\.idMunicipioString").val();
	var calle       	= jQuery("#dto\\.nombreCalle").val();
	var numero       	= jQuery("#dto\\.nombreCalleNumero").val();
	var colonia       	= jQuery("#dto\\.nombreColonia").val();
	var coloniaDif      = jQuery("#dto\\.nombreColoniaDiferente").val();
	var coloniaCheck   	= jQuery("#idColoniaCheck").val();
	var cp       		= jQuery("#dto.codigoPostal").val();
	var telLada    		= jQuery("#txt_telefono_1_lada").val();
	var tel       		= jQuery("#txt_telefono_1").val();
	var rfc       		= jQuery("#txt_rfc").val();
	var curp       		= jQuery("#txt_curp").val();
	var fnac       		= jQuery("#dp_fechaNacimiento").val();
	
	if( oficina == "" ){
		mensaje = "Seleccione una oficina";
	}else if (tipoPersona == ""){
		mensaje = "Seleccione tipo de persona";
	}else if (estatus == ""){
		mensaje = "Seleccione estatus";
	}else if (tipoPersona == "PM" && nombreEmp == ""){
		mensaje = "Indique el nombre de la empresa";
	}else if (tipoPersona == "PM" && nombreCom == ""){
		mensaje = "Indique el nombre comercial de la empresa";
	}else if (tipoPersona == "PM" && nombreAdm == ""){
		mensaje = "Indique el nombre del administrador de la empresa";
	}else if (tipoPersona == "PF" && nombrePer == ""){
		mensaje = "Indique el nombre de la persona";
	}else if (tipoPersona == "PF" && apPaterno == ""){
		mensaje = "Indique el apellido paterno de la persona";
	}else if (tipoPersona == "PF" && apMaterno == ""){
		mensaje = "Indique el apellido materno de la persona";
	}else if (tipoPersona == "PF" && curp == ""){
		mensaje = "Indique el CURP";
	}else if (tipoPersona == "PF" && fnac == ""){
		mensaje = "Indique la fecha de nacimiento";
	}else if (pais == ""){
		mensaje = "Indique el pais del domicilio";
	}else if (estado == ""){
		mensaje = "Indique el estado del domicilio";
	}else if (municipio == ""){
		mensaje = "Indique el municipio del domicilio";
	}else if (calle == ""){
		mensaje = "Indique la calle del domicilio";
	}else if (numero == ""){
		mensaje = "Indique el número del domicilio";
	}else if (colonia == "" && coloniaCheck == false){
		mensaje = "Indique la colonia  del domicilio";
	}else if (coloniaDif == "" && coloniaCheck == true){
		mensaje = "Indique la colonia  del domicilio";
	}else if (cp == ""){
		mensaje = "Indique el código postal";
	}else if (tel == "" || telLada == ""){
		mensaje = "Indique el teléfono principal";
	}else if (rfc == ""){
		mensaje = "Indique el RFC.";
	}else if (prestadorId == "" && (tiposDePrestadorParams == "" || tiposDePrestadorParams == null)){
		mensaje = "Debe seleccionar tipo de prestador";
	}
	return mensaje;
}

function init(){
	var tipoPersona = jQuery('input[name=dto.tipoPersona]').filter(':checked').val();
	if(tipoPersona == "PF"){
		jQuery("#txt_nombres").toggleClass("jQrequired");
		jQuery("#txt_apellido_paterno").toggleClass("jQrequired");
		jQuery("#txt_apellido_materno").toggleClass("jQrequired");
		jQuery("#dp_fechaNacimiento").toggleClass("jQrequired");
		jQuery("#txt_curp").toggleClass("jQrequired");
	}if(tipoPersona == "PM"){
		jQuery("#txt_nombre_de_la_empresa").toggleClass("jQrequired");
		jQuery("#txt_nombre_comercial").toggleClass("jQrequired");
		jQuery("#txt_administrador").toggleClass("jQrequired");
	}
	
	if(jQuery("#profesionales").val() == 1 ){
		jQuery("#chkProfesionales").attr('checked',true);
	}else{
		jQuery("#chkProfesionales").attr('checked',false);
	}
}


function salirVentana(ventana, consulta){
	if(consulta){
		parent.cerrarVentanaModal(ventana,true);
	}else if(confirm("\u00BFDesea salir sin guardar?") ){
		parent.cerrarVentanaModal(ventana,true);
	}
}

function ocultarBotonCerrarVentanaModal(id){
	var win = mainDhxWindow.window(id);
	win.button("close").hide();
}

function requiereGuardar(){
	guardado = false;
}

function validaServiciosProfesionales(){
	if(jQuery("#chkProfesionales").is(":checked")){
		jQuery("#profesionales").attr("value",1);
	}else{
		jQuery("#profesionales").attr("value",0);
	}
}

function validaAutosCa(){
	if(jQuery("#chkAutosCa").is(":checked")){
		jQuery("#autosCa").attr("value",1);
	}else{
		jQuery("#autosCa").attr("value",0);
	}
}
function validaDaniosCa(){
	if(jQuery("#chkDaniosCa").is(":checked")){
		jQuery("#daniosCa").attr("value",1);
	}else{
		jQuery("#daniosCa").attr("value",0);
	}
}
function validaVidaCa(){
	if(jQuery("#chkVidaCa").is(":checked")){
		jQuery("#vidaCa").attr("value",1);
	}else{
		jQuery("#vidaCa").attr("value",0);
	}
}

function initCompradoresGrid(){
	
	 document.getElementById("pagingArea").innerHTML = '';
	 document.getElementById("infoArea").innerHTML = '';

	 listadoCompradoresGrid = new dhtmlXGridObject('listadoCompradoresGrid');	
	 listadoCompradoresGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	 listadoCompradoresGrid.attachEvent("onXLE", function(grid_obj){
			unblockPage();
	 });
	 
	 listadoCompradoresGrid.attachEvent("onXLS", function(grid){
		 mostrarIndicadorCarga("indicador");
	 });
	 
	 listadoCompradoresGrid.attachEvent("onRowSelect", function(id,ind){
		 jQuery("#nombre"  ,window.parent.document).attr("value",listadoCompradoresGrid.cells(id,1).getValue());
		 jQuery("#telefono",window.parent.document).attr("value",listadoCompradoresGrid.cells(id,2).getValue());
		 parent.cerrarVentanaModal("vm_busquedaComprador",true);
	 });
	 
	 listadoCompradoresGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	 });
	 
	 listadoCompradoresGrid.setHeader   ("ID,Nombre,");
	 listadoCompradoresGrid.attachHeader("#select_filter,#select_filter,");
	 
	 
	 var tipoPrestador = jQuery("#tipoPrestador").val();
	 
	 if( tipoPrestador == "" ){
		 mostrarMensajeInformativo('Tipo de prestador no enviado', '10');
	 }else{
		 listadoCompradoresGrid.load("/MidasWeb/siniestros/catalogos/prestadorDeServicio/mostrarBusquedaRapidaGrid.action?tipoPrestador="+tipoPrestador ); 
	 }
	
}


function generarUsuarioPrestador(){
	blockPage();
	var id = jQuery('#txt_numPrestador').val();
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/generarUsuarioPrestador.action";
	jQuery.getJSON(url,
			{prestadorId: id}
			,function(json){
				unblockPage();
				if(json != null){					
					jQuery('#claveUsuarioPrestador').val(json.claveUsuarioPrestador);
					mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);
				}else{
					mostrarMensajeInformativo('No hubo respuesta del servicio de creación de usuarios', '10');
				}
			});
	unblockPage();
}


function actualizarUsuarioPrestador(){
	var id = jQuery('#txt_numPrestador').val();
	var clave = jQuery('#claveUsuarioPrestador').val();
	var url = "/MidasWeb/siniestros/catalogos/prestadorDeServicio/actualizarUsuarioPrestador.action";
	jQuery.getJSON(url,
			{prestadorId: id,
			 claveUsuarioPrestador: clave}
			,function(json){
				alert(json);
				mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);
			});
}

