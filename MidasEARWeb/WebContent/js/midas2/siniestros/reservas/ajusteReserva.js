
function mostrarPasesEnGrid(){
	  jQuery("#listaDeCoberturasGrid").empty(); 
	  var url = "";
	  url = "/MidasWeb/siniestros/reservas/mostrarDatosEstimacion.action?idReporteCabina="+idReporteCabina;
	  listadoDePasesGrid = new dhtmlXGridObject('listaDeCoberturasGrid');
	  listadoDePasesGrid.attachEvent("onXLS", function(grid){blockPage();});
	  listadoDePasesGrid.attachEvent("onXLE", function(grid){unblockPage();});
	  listadoDePasesGrid.attachEvent("onRowSelect",function(rId,cId){
		  actualizaSeccionDeReservas(rId);
	  });
//	  listadoDePasesGrid.attachHeader("#select_filter, #select_filter, #select_filter, #select_filter, #select_filter, #select_filter, #select_filter, #select_filter");
	  listadoDePasesGrid.load(url) ;
}


function actualizaSeccionDeReservas(coberturaReporteCabinaId){
		jQuery("#div_reservas").empty(); 
	    var params = "?idEstimacionCobertura="+coberturaReporteCabinaId;
    	var url = "/MidasWeb/siniestros/reservas/mostrarInfoReservas.action"+params;
		sendRequestJQ(null, url, "div_reservas","initCurrencyFormatOnTxtInput();" );
}




function guardarMovimiento(){
	if(validaDatosRequeridos() & validarRequeridosGrid() ){
		removeCurrencyFormatOnTxtInput();
		formParams = jQuery(document.estimacionFormEstimacion).serialize();
		formParams += "&"+jQuery(document.infoGeneralForm).serialize();
		var url = "/MidasWeb/siniestros/reservas/guardarMovimiento.action?"+formParams;
		console.log('guardarMovimiento URL : '+url);
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function validarRequeridosGrid(){
	var existe = true;
	jQuery(".requeridoGrid").each( function(){
				var element = jQuery(this);
				if( isEmpty(element.val()) ){
					mostrarMensajeInformativo('Seleccione una cobertura del grid', '10');
					existe = false;
				}
			}
		);
	return existe;
}

function verImagenes(folio, tituloVentana,idFortimax, folioExpediente){
	var cveCobertura = dwr.util.getValue("cveCobertura");	
	if(cveCobertura!='RCV' && cveCobertura!='RCB' && cveCobertura!='DMA'   ){
    	mostrarMensajeInformativo('No aplica para la cobertura seleccionada', '20');
	}else{
		if(cveCobertura=='RCV'){
			ventanaFortimax('PRCV', folio, tituloVentana,idFortimax, folioExpediente);

		}
		
		if(cveCobertura=='RCB'){
			ventanaFortimax('PRCB', folio, tituloVentana,idFortimax, folioExpediente);

		}
		if(cveCobertura=='DMA'){
			ventanaFortimax('PDM', folio, tituloVentana,idFortimax, folioExpediente);

		}
		
		
	}

	
}


function autorizar(){
	if(validaDatosRequeridos()){
		removeCurrencyFormatOnTxtInput();
		formParams = jQuery(document.estimacionFormEstimacion).serialize();
		formParams += "&"+jQuery(document.infoGeneralForm).serialize();
		var url = "/MidasWeb/siniestros/reservas/autorizarReserva.action?"+formParams;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("errorField"); 
				success = false;
			} else {
				these.removeClass("errorField");
			}
		}
	);
	return success;
}

function cerrar(){
	var url = "/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

