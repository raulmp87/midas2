var historicoRobosGrid;
function buscarHistoricoRobos(){
	jQuery("#tituloListado").show();
	jQuery("#historicoRobosGrid").empty(); 
	
	historicoRobosGrid = new dhtmlXGridObject('historicoRobosGrid');
	historicoRobosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	historicoRobosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	formParams = jQuery(document.busquedaHistoricoRobos).serialize();
	var url = buscarHistoricoRobosPath + '?' +  formParams ;
	historicoRobosGrid.load( url );
}

function limpiarFiltros(){
	jQuery('#busquedaHistoricoRobos').each (function(){
		  this.reset();
	});
}

function regresarDefinirRobo(){
	var idReporte 	= jQuery("#reporteCabinaId").val();
	var idCobertura = jQuery("#idCobertura").val();
  	var url = regresarDefinirRoboPath + "?reporteCabinaId="+idReporte+"&idCobertura="+idCobertura;
  	sendRequestJQ(null, url, targetWorkArea, null);
}