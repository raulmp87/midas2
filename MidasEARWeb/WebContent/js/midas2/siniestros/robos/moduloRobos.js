/**
 * 
 */
// Inicializa el Greid de la lista de eventos
function iniContenedorGrid() {
	buscarListaRobos();
} 

function onChangeMunicipio(){	

	var estados = dwr.util.getValue("estadoslis");	
	if(null ==estados   || estados==""){
		dwr.util.removeAllOptions("municipioslis");		
	}else{
	listadoService.getMapMunicipiosPorEstado( estados ,function(data){
		dwr.util.removeAllOptions("municipioslis");
		dwr.util.addOptions("municipioslis", [ {
				id : "",
				value : "Seleccione..."
			} ], "id", "value");
		dwr.util.addOptions("municipioslis", data);
	});
	}
}


function consultarRobo(){
	 jQuery("#reporteCabinaId").val( listadoGrid.cells(listadoGrid.getSelectedId(),8).getValue() );
	 jQuery("#idCobertura").val( listadoGrid.cells(listadoGrid.getSelectedId(),9).getValue() );
	 jQuery("#reporteRoboId").val( listadoGrid.cells(listadoGrid.getSelectedId(),15).getValue() );
	var params = jQuery(document.robosForm).serialize();
	sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/seguimientoRoboTotal.action?'+ params,targetWorkArea,null);
}



//joksrc
function buscarListaRobos(esPrimeraVes){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('robosGrid');	
	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });
	
	//first call for ROBOS. No get data, however the grid must be loaded even without data.
	if(esPrimeraVes == true){
		var formParams = null;
		listadoGrid.load( '/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/busquedasSiniestrosRobo.action?'+ formParams+'&esPrimeraVes='+esPrimeraVes);
	}else{
		//running when press BUSCAR button inside ROBOS's module...
		esPrimeraVes = false;
		
		var respuesta = validarTresCamposObligatorios();
		if (respuesta == true){
			listadoGrid.attachEvent("onXLE", function(){
			    if (!listadoGrid.getRowsNum())
			    	mostrarMensajeInformativo('No se encontraron resultados.', '20');
			})
			var formParams = jQuery(document.robosForm).serialize();
			listadoGrid.load( '/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/busquedasSiniestrosRobo.action?'+ formParams+'&esPrimeraVes='+esPrimeraVes);
		}else{
			mostrarMensajeInformativo("Seleccione o capture al menos tres datos para realizar la búsqueda", '20');
			
		}
		
		
	}
	
}

function validarTresCamposObligatorios(){
	
	var contador = 0;
	var a = new Array();
	
	a[0] = 	 jQuery("#servPublico").attr('checked');
	a[1] = 	 jQuery("#servParticular").attr('checked');
	a[2] =   jQuery("#nsOficinaS").val();
	a[3] = 	 jQuery("#nsConsecutivoRS").val();
	a[4] =   jQuery("#nsAnioS").val();
	a[5] =   jQuery("#numPoliza").val();
	a[6] =   jQuery("#nsOficina").val();
	a[7] =   jQuery("#nsConsecutivoR").val();
	a[8] =   jQuery("#nsAnio").val();
	a[9] = 	 jQuery("#numSerie").val();
	a[10] =  jQuery("#numActa").val();
	a[11] =  jQuery("#roboLis").val();
	a[12] =  jQuery("#estatusLis").val();
	a[13] =  jQuery("#ofinasActivas").val();
	a[14] =  jQuery("#estadoslis").val();
	a[15] =  jQuery("#municipioslis").val();
	a[16] =  jQuery("#fechaIniReporte").val();
	a[17] =  jQuery("#fechaFinReporte").val();
	a[18] =  jQuery("#fechaIniOcurrido").val();
	a[19] =  jQuery("#fechaFinOcurrido").val();
	
	for (x=0; x< a.length; x++){		 	
		if(a[x] != "" || a[x]== true){				
			contador++;
		}	
	}
	
	 if(contador >2 ){
		 //if two fields are filled, verify that date fields (if captured) are filled start date and end date. (Fecha de Reporte and Fecha de Ocurrido)
		 if( (a[16] != "" && a[17] == "") || (a[16] == "" && a[17] != "")  ){
			 mostrarMensajeInformativo('No se permite buscar con una sola fecha de Reporte. Debe capturar ambas.', '10');
			 return false;
		 }else if( (a[18] != "" && a[19] == "") || (a[18] == "" && a[19] != "") ) {
			 mostrarMensajeInformativo('No se permite buscar con una sola fecha de Ocurrido. Debe capturar ambas.', '10');
			 return false;
		 }else{
			 return true;
		 }
	 }else{
		 return false;
	 }
	
}

function buscarCoberturas(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	//jQuery("#observacion").val( '');
	listadoGrid = new dhtmlXGridObject('CoberturaGrid');	
	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });		
	var formParams = jQuery(document.robosForm).serialize();
	listadoGrid.load( '/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/busquedasSiniestrosRobo.action?'+ formParams);
}

 function exportarExcelRobos(){
		 	window.open("/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/exportarListado.action?" + jQuery(document.robosForm).serialize(), "ListadoRobos");
	 }
 

 
 function abrirRepuve(){
		var formParams = jQuery(document.robosForm).serialize();
		window.open('http://www.repuve.gob.mx/quieres_conocer.html', "REPUVE");
	 }
 
 function condicionEspeciales(){
		var formParams = jQuery(document.robosForm).serialize();
		mostrarMensajeInformativo('exportarExcel '+formParams, '20');
	 }
 
 function limpiarContenedorListadoRobos(){
		jQuery(document.robosForm).each (function(){
			  this.reset();
		});
		//buscarListaRobos();
	} 

function salvar(){
	if(checkForErrors(jQuery("#numActa")) && checkForErrors(jQuery("#averiguacionDen"))){
		var estatusVehiculo = jQuery("#estatusLista").val();
		var montoProvision = jQuery("#montoProvicion").val();
		var ubicacionVehiculo = jQuery("#ubicacionVehiculo").val();
		if(estatusVehiculo != "EST_VEHI_4" 
			|| (estatusVehiculo == "EST_VEHI_4" && montoProvision && ubicacionVehiculo) ){
		if(confirm("¿Está seguro que desea guardar el seguimiento?")){
			if(confirm("¿Desea enviar la información a OCRA? \n Presione Aceptar para enviar o Cancelar para guardar sin enviar a OCRA")){
				 dwr.util.setValue("definicionSiniestroRoboDTO.envioOcra",'true' );
				
			}else{
				 dwr.util.setValue("definicionSiniestroRoboDTO.envioOcra",'false' );
			}
			removeCurrencyFormatOnTxtInput();
			var formParams = jQuery(document.definirRoboForm).serialize();
			sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/guardarSeguimientoRobo.action?'+ formParams,targetWorkArea,null);
		}
		}else{
			mostrarVentanaMensaje('20','Debe capturar el Monto Provisi\u00F3n y Ubicaci\u00F3n Veh\u00EDculo para guardar con el Estatus del Veh\u00EDculo seleccionado', null);
		}
	}else{
		mostrarVentanaMensaje('20','Valide que no haya campos con errores para poder guardar', null);
	}
}

function envioOcra(url){
	window.open(url, "OCRA");
}


function consultarHistorico(){
 //TODO Implementar la llamada a la pantalla de historico. 
	}

 function cerrarDetalleRobo(){
	 sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/roboTotal.action',targetWorkArea,null);
	}
 function getNumeroDeDiasLozalizacion(){
	    var d1 = dwr.util.getValue("definicionSiniestroRoboDTO.fechaRobo");	
	    var d2 = dwr.util.getValue("definicionSiniestroRoboDTO.fechaLocalizacion");	
	    utileriasService.diferenciaDiasFechas(d1,d2 ,function(data){
	    if(data> 365){
	    	jQuery("#diasLocalizacion").val(365 );
	    	jQuery("#diasLocalizacion").addClass( "estilodias" );
	    	
	    }else if(data< 0){
	    	jQuery("#diasLocalizacion").val( data );
	    	jQuery("#diasLocalizacion").addClass( "estilodias" );
	    	
	    } else {
	    	jQuery("#diasLocalizacion").val( data );
	    	jQuery("#diasLocalizacion").removeClass( "estilodias" );
	    }
	    
		});
	 }
 function getNumeroDeDiasRecuperacion(){
	    var d1 = dwr.util.getValue("definicionSiniestroRoboDTO.fechaLocalizacion");	 
	    var d2 = dwr.util.getValue("definicionSiniestroRoboDTO.fechaRecuperacion");
	    utileriasService.diferenciaDiasFechas(d1,d2 ,function(data){
	    	if(data> 365){
		    	jQuery("#diasRecuperacion").val(365 );
		    	jQuery("#diasRecuperacion").addClass( "estilodias" );
		    	
		    } else if(data< 0){
		    	jQuery("#diasRecuperacion").val(data );
		    	jQuery("#diasRecuperacion").addClass( "estilodias" );
		    	
		    } else {
		    	jQuery("#diasRecuperacion").val( data );
		    	jQuery("#diasRecuperacion").removeClass( "estilodias" );
		    }
		});
}
 
 /**
  * 
  */
 // Inicializa el Greid de la lista de eventos
 function iniContenedorDefinicion() {
	 getNumeroDeDiasLozalizacion();
	 getNumeroDeDiasRecuperacion();
	 cargarInvestigadores();
	 activarInvestigacion();
 } 
 
 function activarInvestigacion(){
	if (jQuery( "#idproveedor" ).val() && jQuery( "#idproveedor" ).val()!=''){
		jQuery("#isTurnarInvetigaciontrue").attr('checked', true);
		jQuery("#isTurnarInvetigacionFalse").attr('checked', false);
		jQuery( "#proveedorLista" ).removeAttr("disabled");
	}else{
		jQuery("#isTurnarInvetigaciontrue").attr('checked', false);
		jQuery("#isTurnarInvetigacionFalse").attr('checked', true);
		jQuery( "#proveedorLista" ).attr('disabled','disabled');
		
	}
 }
 function activarProvedor(seleccion) {
		if(seleccion=='false'){
			 	jQuery( "#proveedorLista" ).attr('disabled','disabled');
			 	jQuery("#proveedorLista").val('');
		}
		else{
			 jQuery( "#proveedorLista" ).removeAttr("disabled");
		}

 } 
 
function cargarInvestigadores(){
	 var tipo = "INVES";
			dwr.util.removeAllOptions("proveedorLista");		
		listadoService.getMapPrestadorPorTipo( tipo ,function(data){
			dwr.util.removeAllOptions("proveedorLista");
			dwr.util.addOptions("proveedorLista", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("proveedorLista", data);
			if (jQuery( "#idproveedor" ).val() && jQuery( "#idproveedor" ).val()!=''){
				jQuery("#proveedorLista").val(jQuery( "#idproveedor" ).val());
			}
		});
		
	
}

function mostrarHistoricoRobos(){
	var idReporte 	= jQuery("#reporteCabinaId").val();
	var idCobertura = jQuery("#idCobertura").val();
  	var url = mostrarHistoricoRobosPath + "?reporteCabinaId="+idReporte+"&idCobertura="+idCobertura;
  	sendRequestJQ(null, url, targetWorkArea, null);
} 

 //