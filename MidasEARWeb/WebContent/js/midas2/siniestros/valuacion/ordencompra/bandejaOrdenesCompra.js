var listadoGrid;

 function limpiarContenedor(){
		jQuery(document.bandejaOrdenCompraForm).each (function(){
			  this.reset();
		});
	}  
 
 function changeTipoPrestador() {	
	 var tipo =jQuery("#tipoProveedorMap").val();
	 if(null ==tipo   || tipo=="" ){
			dwr.util.removeAllOptions("proveedorLis");		
		}else{
		listadoService.getMapPrestadorPorTipo( tipo ,function(data){
			dwr.util.removeAllOptions("proveedorLis");
			dwr.util.addOptions("proveedorLis", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("proveedorLis", data);
		});
		}
 }
 
 function buscarLista(){
	 if(validarBusqueda()){	
		 document.getElementById("pagingArea").innerHTML = '';
		 document.getElementById("infoArea").innerHTML = '';
		 if(listadoGrid){
			 listadoGrid.destructor();
		 }
		 listadoGrid = new dhtmlXGridObject('ordenesCGrid');
		 
		 listadoGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		 listadoGrid.setSkin('light');
		 listadoGrid.setHeader("Orden Compra, Siniestro, Reporte, Termino de Ajuste, Cobertura, Total Orden de Compra, Tipo, Tipo Pago, Concepto, Dias Transcurridos,No Orden de Pago,Proveedor, Beneficiario,Estatus, , , ,Consultar , Editar");
		 listadoGrid.setInitWidths("80,100,100,200,200,80,150,150,300,80,80,180,180,100,*,*,*,80,80");
		 listadoGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center");
		 listadoGrid.setColSorting("server,server,server,server,server,server,server,server,na,na,server,server,server,server,na,na,na,na,na");
		 listadoGrid.setColTypes("ro,ro,ro,ro,ro,price,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img");			
		 listadoGrid.enableMultiline(true);
		 listadoGrid.init();
		 listadoGrid.splitAt("5");	
		 listadoGrid.attachEvent("onBeforePageChanged",function(){
	    		if (!this.getRowsNum()) return false;
	    		return true;
	    	});		
		 listadoGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		 listadoGrid.setPagingSkin("bricks");	
		 listadoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		 listadoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		 listadoGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicador");
		    });
		 listadoGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
		    });		
		 
		 var formParams = jQuery(document.bandejaOrdenCompraForm).serialize();
		 var url = '/MidasWeb/siniestros/valuacion/ordencompra/listadoBandejaOrdenCompra.action?'+ formParams;
		 listadoGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
				server = url;			
				listadoGrid.clearAll();
				listadoGrid.load(server+(server.indexOf("?")>=0?"&":"?")
			     +"orderBy="+ind+"&direct="+direct);			
				listadoGrid.setSortImgState(true,ind,direct);
			    return false;
			});
		var formParams = jQuery(document.bandejaOrdenCompraForm).serialize();
		listadoGrid.load( url);
	}
}
 
 function validarBusqueda(){	 
	 var a = new Array();
	 a[0] = jQuery('#nsOficinaS').val();  
	 a[1] = jQuery("#nsConsecutivoRS").val();
	 a[2] = jQuery("#nsAnioS").val();
	 a[3] = jQuery('#ofinasActivas').val();
	 a[4] = jQuery('#beneficiario').val();
	 a[5] = jQuery('#tipoProveedorMap').val();
	 a[6] = jQuery('#proveedorLis').val();
	 a[7] = jQuery('#numOrdenCompra').val();
	 a[8] = jQuery('#terminoAjusteLis').val();
	 a[9] = jQuery('#tipoPago').val();
	 a[10] = jQuery('#estautsId').val();
	 a[11] = jQuery("#servPublico").attr('checked');
	 a[12] = jQuery("#servParticular").attr('checked');
	 a[13] = jQuery('#nsOficinaR').val();
	 a[14] = jQuery('#nsConsecutivoR').val();
	 a[15] = jQuery('#nsAnioR').val(); 	
	 a[16] = jQuery('#tipoOrden').val();
	 
	 var contador = 0;
	 
	 for (x=0; x< a.length; x++){		 	
			if(a[x] != "")
			{				
				contador++;
			}	
		}

	 if(contador >1){
		 return true;
	 }
	 mostrarVentanaMensaje("20","Debe de seleccionar al menos dos campos para generar la lista",null);
	 return false; 	
	}

 function consultarOrden(id){
		var origen 	= "BANDEJAORDENESCOMPRA";   
		sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/consultarDetalleOrdenCompra.action?idOrdenCompra='+id+"&origen=" + origen+"&modoConsulta=true" ,targetWorkArea,null);
	}
 
 function editarOrdenCompra(id){
		var origen 	= "BANDEJAORDENESCOMPRA";   
		sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/detalleOrdenCompra.action?idOrdenCompra='+id+"&origen=" + origen,targetWorkArea,null);
	}
 
 function busquedaReportes(){
		sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action',targetWorkArea,null);
	}

 