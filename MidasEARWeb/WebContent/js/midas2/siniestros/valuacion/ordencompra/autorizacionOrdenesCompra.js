/* ******************************************************************************************************************************************************* */
/* ****************************************************************** CATALOGO PERMISOS ****************************************************************** */
/* ******************************************************************************************************************************************************* */

var permisoUsuarioGrid;


function initGridPermisosUsuario(){
	getPermisosUsuarios();
}

function getPermisosUsuarios(){
	jQuery("#permisoUsuarioGrid").empty(); 
	
	permisoUsuarioGrid = new dhtmlXGridObject('permisoUsuarioGrid');
	permisoUsuarioGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	permisoUsuarioGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	permisoUsuarioGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorPermisoUsuario");
    });
	permisoUsuarioGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorPermisoUsuario');
    });
	
	
	permisoUsuarioGrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,,,");
	

	permisoUsuarioGrid.load( listadoPermisoUsuario ) ;
	
}


function obtenerConceptoDePago( valor ){
	var tipoPago = jQuery('input:radio[name=ordenCompraPermiso\\.tipo]:checked').val();
	
	if( tipoPago == 'GA' || tipoPago == 'RGA'){
		var url = cargarConceptoPago + "?" 
				+ "tipoPago=" + tipoPago ;
	}else if( tipoPago == 'OC'){
		var lineaNegocioId = jQuery('#lineaNegocioId').val();
		var url = cargarConceptoPago + "?" 
					+ "tipoPago=" + tipoPago 
					+ "&lineaNegocioId=" + lineaNegocioId 
					+ "&coberturaCompuesta=" + valor.value;
	}
	
	
	if( valor.value != ""){
	
		document.getElementById("comboConceptos").innerHTML = '';
		sendRequestJQ(null, url, 'comboConceptos', null);
	}
	
}


function obtenerCoberturas( valor ){
	
	var url = cargarCoberturas + "?" 
			+ "lineaNegocioId=" + valor.value ;
	
	if( valor.value != ""){
		document.getElementById("comboCoberturas").innerHTML = '';
		sendRequestJQ(null, url, 'comboCoberturas', null);
		
	}
}

function guardar(){
	var soloConsulta 				= jQuery('#soloConsulta').val();

	if(soloConsulta){
		url = guardarPermiso + "?"+ jQuery("#ordenCompraPersmisos").serialize();
		sendRequestJQ(null, url ,targetWorkArea, null);
	}else{
		var tipoPago 					= jQuery('input:radio[name=ordenCompraPermiso\\.tipo]:checked').val();
		var idCoberturaCompuesto 		= jQuery('#idCobertura').val();
		var idCobertura 				= idCoberturaCompuesto.split("|");
		var url 						= '';
		
		if( tipoPago == 'GA'  || tipoPago == 'RGA'){
			url = guardarPermiso + "?"+ jQuery("#ordenCompraPersmisos").serialize();
		}else if( tipoPago == 'OC'){
			url = guardarPermiso + "?"
			+ "ordenCompraPermiso.coberturaSeccion.id.idtocobertura=" + idCobertura[0]
			+ "&" + jQuery("#ordenCompraPersmisos").serialize();
		}	
		
		if( validaDatosRequeridos()){
			sendRequestJQ(null, url ,targetWorkArea, null);
		}
	}
	
}


function eliminarPermisos( id ){
	
	mostrarMensajeConfirm('Se eliminará el Permiso, ¿Desea continuar?', '20', 
			'confirmadoEliminarPermiso(' + id + ')', null, null);
}

function confirmadoEliminarPermiso( id ){
	
	var url = 	eliminarPermiso + '?ordenCompraPermisoId='+ id;
	
	sendRequestJQ(null, url ,targetWorkArea, null);
}


function validateTipoOrdenDeCompra(){
	var tipoPago = jQuery('input:radio[name=ordenCompraPermiso\\.tipo]:checked').val();
	
	if( tipoPago == 'GA' || tipoPago == 'RGA' ){
		jQuery("#lineaNegocioId").attr("disabled","disabled");
		jQuery("#idCobertura").attr("disabled","disabled");
		jQuery("#lineaNegocioId").val('');
		jQuery("#idCobertura").val('');
		jQuery("#lineaNegocioId").removeClass("requerido");
		jQuery("#idCobertura").removeClass("requerido");
		obtenerConceptoDePago('');
	}else if( tipoPago == 'OC'){
		jQuery("#lineaNegocioId").attr("disabled","");
		jQuery("#idCobertura").attr("disabled","");
		jQuery("#lineaNegocioId").addClass("requerido");
		jQuery("#idCobertura").addClass("requerido");
	}
	
}

function validateHabilitarMontos(){
	
	if( jQuery("#validarMontos").attr('checked') == true){
		jQuery("#montoMax").attr("disabled","");
		jQuery("#montoMin").attr("disabled","");
		jQuery("#montoMax").addClass("requerido");
		jQuery("#montoMin").addClass("requerido");
		
	}else{
		jQuery("#montoMax").attr("disabled","disabled");
		jQuery("#montoMin").attr("disabled","disabled");
		jQuery("#montoMax").removeClass("requerido");
		jQuery("#montoMin").removeClass("requerido");
	}
	
}

function relacionarUsuarios( ordenCompraPermisoId ){
	var url = mostrarRelacionarUsuarios + '?ordenCompraPermisoId=' + ordenCompraPermisoId;
	
	mostrarVentanaModal("vm_relacionarUsuarios", "Relacionar Usuarios", null, null, 900, 500, url , "");
}

function cerrarPopUp( ){
	parent.cerrarVentanaModal("vm_relacionarUsuarios", null);
}


/* ************************************************************************************************************************************************* */
/* ****************************************************************** DRAG & DROP ****************************************************************** */
/* ************************************************************************************************************************************************* */

var relacionUsuariosDisponiblesGrid;
var relacionUsuariosAsociadosGrid;
var relacionUsuariosDataProcessor;


function initGridsRelacionUsuarios(){
	getUsuariosDisponibles();
	getUsuariosAsociados();
}


function getUsuariosDisponibles(){
	jQuery("#relacionUsuariosDisponiblesGrid").empty();
	var ordenCompraPermisoId = jQuery('#ordenCompraPermisoId').val();
	var url = obtenerUsuariosDisponibles + '?ordenCompraPermisoId=' + ordenCompraPermisoId;
	

	relacionUsuariosDisponiblesGrid = new dhtmlXGridObject('relacionUsuariosDisponiblesGrid');
	relacionUsuariosDisponiblesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	relacionUsuariosDisponiblesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	relacionUsuariosDisponiblesGrid.load( url );
}


 function getUsuariosAsociados(){
	jQuery("#relacionUsuariosAsociadosGrid").empty(); 
	var ordenCompraPermisoId = jQuery('#ordenCompraPermisoId').val();
	var url = obtenerUsuariosRelacionados + '?ordenCompraPermisoId=' + ordenCompraPermisoId;
	
	
	relacionUsuariosAsociadosGrid = new dhtmlXGridObject('relacionUsuariosAsociadosGrid');
	relacionUsuariosAsociadosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	relacionUsuariosAsociadosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		
	relacionUsuariosAsociadosGrid.load( url );
	
//	var urlProcessor = accionRelacionarUsuarios + '?ordenCompraPermisoId=' + ordenCompraPermisoId;
	
	relacionUsuariosDataProcessor = new dataProcessor(accionRelacionarUsuarios);
	relacionUsuariosDataProcessor.enableDataNames(true);
//	relacionUsuariosDataProcessor.setTransactionMode("POST");
//	relacionUsuariosDataProcessor.setUpdateMode("cell");
//	relacionUsuariosDataProcessor.attachEvent("onAfterUpdate", initGridsRelacionUsuarios);
	
	relacionUsuariosDataProcessor.setTransactionMode("POST");
	relacionUsuariosDataProcessor.setUpdateMode("row");
	relacionUsuariosDataProcessor.attachEvent("onRowMark", function(id, action, tid, response){
		relacionUsuariosDataProcessor.sendAllData();				
	});
	relacionUsuariosDataProcessor.attachEvent("onFullSync", function(){
		getUsuariosAsociados();
	});	
	relacionUsuariosDataProcessor.init(relacionUsuariosAsociadosGrid);
}
 
 function editarPermiso(id){ 
	 var url = mostrarEditarPermiso + '?ordenCompraPermisoId=' +id +'&soloConsulta='+true ;
	 
	 document.getElementById("altaPermiso").innerHTML = '';
	 sendRequestJQ(null, url ,'altaPermiso', 'loadInfoEdicion();');
 }
 
 function loadInfoEdicion (){
	 jQuery(".consulta").attr("disabled","disabled");
	 validateHabilitarMontos();	 
 }
	
 function nuevo(){
	 document.getElementById("altaPermiso").innerHTML = '';
	 sendRequestJQ(null, nuevoPermiso ,'altaPermiso', null);
	 validateHabilitarMontos();
 }
 
 
 /* ***************************************************************************************************************************************************************************** */ 
 /* ****************************************************************** LISTADO ORDENER DE COMPRA POR AUTORIZAR ****************************************************************** */
 /* ***************************************************************************************************************************************************************************** */
 
 var ordenesCompraPorAutorizar;


 function initGridsOrdenesCompraAutorizar(){
	 getOrdenerCompraAutorizar();
 }



  function getOrdenerCompraAutorizar(){
 	jQuery("#ordenesCompraPorAutorizar").empty(); 
 	
 	ordenesCompraPorAutorizar = new dhtmlXGridObject('ordenesCompraPorAutorizar');
 	ordenesCompraPorAutorizar.attachEvent("onXLS", function(grid_obj){blockPage()});
 	ordenesCompraPorAutorizar.attachEvent("onXLE", function(grid_obj){unblockPage()});
 	
 	ordenesCompraPorAutorizar.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,");
 	
 	ordenesCompraPorAutorizar.load( listadoOrdenesPorAutorizar ) ;

 }
  
  
  
  function consultaOrden(id){
	  
	  var url = consultarDetalleOrdenCompra + '?idOrdenCompra=' + id +'&bandejaPorAutorizar=true';
		
		sendRequestJQ(null,url,targetWorkArea,null);
	}
 
  
  /* ***************************************************************************************************************************************************************************** */ 
  /* ****************************************************************************** VALIDACIONES ********************************************************************************* */
  /* ***************************************************************************************************************************************************************************** */
   
  
  function validaDatosRequeridos(){
		var requeridos = jQuery(".requerido");
		var success = true;
		requeridos.each(
			function(){
				var these = jQuery(this);
				if( isEmpty(these.val()) ){
					these.addClass("errorField"); 
					success = false;
				} else {
					these.removeClass("errorField");
				}
			}
		);
		return success;
	}
