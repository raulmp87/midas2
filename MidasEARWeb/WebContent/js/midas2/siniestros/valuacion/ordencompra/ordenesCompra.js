var aplicaIsr;
var aplicaIvaRetenido;
var aplicaIva;
var aplicaPorcentajes;
var ivaOrigen;
var ivaRetOrigen;
var isrOrigen;
var importeSeleccionado=0;
var conceptosGrid;
function imprimirValeTaller(){
	removeCurrencyFormatOnTxtInput();
	var reporteCabinaId = jQuery('#idReporteCabina').val();
	var prestadorId = jQuery('#proveedorLis').val();
	var url = "/MidasWeb/siniestros/cabina/reporteCabina/valeTaller/mostrarVale.action?idReporteCabina=" 
		+ reporteCabinaId + "&idGrua=" + prestadorId;
	parent.mostrarVentanaModal("vm_valeTaller", "Vale de Servicio de Gr\u00DAa", null, null, 1000, 550, url, "");   
	initCurrencyFormatOnTxtInput();
} 

function salvarEncabezado(){
		removeCurrencyFormatOnTxtInput();
		blockPage(); //parche aigg
		if (validaOrdenCompra()){
			desbloqueoSalvar();
			var formParams = jQuery(document.definirOrdenForm).serialize();
			sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/guardar.action?'+ formParams,targetWorkArea,null);
		}else{
			unblockPage();//parche aigg
		}
		
}
function checkDeducible(){
	var aplDeducible =jQuery("#s_aplDeducible").val(); 
	var pagoA =jQuery("#tipoPagoLis").val(); 
	
	if(aplDeducible=="true"  &&  pagoA!="PB" ){
		mostrarMensajeInformativo('Solo Aplica para Pago a Beneficiario".' , '20')
		jQuery("#s_aplDeducible").val('false');
	}else{
		jQuery("#esRefaccion").val(true);
		mostrarMensajeInformativo('Debe guardar la orden de compra para actualizar el deducible".' , '20')
	}
}

function validaOrdenCompra (){
	var valida =true ;
	var tipo = dwr.util.getValue("tipoOrdenCompraLis");	
	var size = jQuery("#terceroAfectadoLis option").length;
	var coberturaLisSize = jQuery("#coberturaLis option").length;
	var coberturaLis = jQuery("#coberturaLis option").length;
	var tercero = dwr.util.getValue("terceroAfectadoLis");	
	var tipoOrdenCompraLis=jQuery("#tipoOrdenCompraLis").val() ;
	var tipoPagoLis=jQuery("#tipoPagoLis").val() ;
	var coberturaLis=jQuery("#coberturaLis").val() ;
	var nomBeneficiario=jQuery("#nomBeneficiario").val() ;
	var tipoProveedorLis=jQuery("#tipoProveedorLis").val() ;
	var proveedorLis=jQuery("#proveedorLis").val() ;
	var tipoPersonaLis=jQuery("#tipoPersonaLis").val() ;
	var terceroAfectadoLis=jQuery("#terceroAfectadoLis").val() ;
	var reembolsoGastoAList=jQuery("#reembolsoGastoAList").val() ;
	var origen =jQuery("#origen").val() ;
	var numeroSiniestroTercero = jQuery("#siniestroTerceroId").val() ;
	var terminoAjuste = jQuery("#h_terminoDeAjuste").val() ;
	if (coberturaLisSize>=1 && (null==coberturaLis || coberturaLis=="")  && (tipo=='OC')){
		mostrarMensajeInformativo('Seleccione la Cobertura ' , '20')
		return false;
	} 
	
	if (size>1 && (null==tercero || tercero=="")  && (tipo=='OC')){
		mostrarMensajeInformativo('Seleccione el Tercero Afectado  ' , '20');
		return false;
	}
	
	if (null==tipoPagoLis || tipoPagoLis==""){
		mostrarMensajeInformativo('Debe Seleccionar el tipo de pago.', '20');
		return false;
	}
	
	if (tipoPagoLis=='PB'){
		if (null==nomBeneficiario || nomBeneficiario==""){
			mostrarMensajeInformativo('Debe Capturar el nombre de Beneficiario.', '20');
			return false;
		}
		if (null==tipoPersonaLis || tipoPersonaLis==""){
			mostrarMensajeInformativo('Debe Seleccionar el tipo de persona.', '20');
			return false;
		}
	}

	if (tipoPagoLis=='PP'){
			if (tipoProveedorLis==='CIA'){
				if(terminoAjuste!="EOC" && terminoAjuste!='EOAC' && terminoAjuste!='SRSIPAC' && terminoAjuste!='SRSIPACYRC' && terminoAjuste!='SESIPAC' && terminoAjuste!='SESIPACYRC' && terminoAjuste!='SEONASIPAC' && terminoAjuste!='SEONASIPTR' 
					&& terminoAjuste!='SEONASIOCT' && terminoAjuste!='SEOSIOCT' && terminoAjuste!='SESIPACTER' && terminoAjuste!='SESIPYRECM' ){
					mostrarMensajeInformativo('No se puede crear ordenes de compra para conpañia con el término de siniestro seleccionado ', '20');
					return false;	
				}else if(null==numeroSiniestroTercero || numeroSiniestroTercero==""){
					mostrarMensajeInformativo('Debe ingresar el número del siniestro del tercero.', '20');
					return false;
				}
			}	
			if (null==tipoProveedorLis || tipoProveedorLis==""){
				mostrarMensajeInformativo('Debe Seleccionar el tipo de proveedor.', '20');
				return false;
			}
			if (null==proveedorLis || proveedorLis==""){
				mostrarMensajeInformativo('Debe Seleccionar el proveedor.', '20');
				return false;
			}
		}
	
	/*if (tipo=='RGA'  || tipo=='GA' ){
		var afectadoLis=jQuery("#afectadoLis").val() ;
		if(null==afectadoLis || afectadoLis==""){
			mostrarMensajeInformativo('Debe Seleccionar el tipo de Afectado.', '20');
			return false;
		}
	}*/
	return true;
}

function cargaGrid(){
		document.getElementById("comprasGrid").innerHTML = '';
		var listadogrid = new dhtmlXGridObject("comprasGrid");
		listadogrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		listadogrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		listadogrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		//comprasGrid.setHeader   ("No Orden de Compra,         Concepto      ,Estatus       ,Fecha Alta    ,Fecha Baja,");
		listadogrid.attachHeader(",#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,,");
		
		listadogrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
		listadogrid.attachEvent("onXLE", function(){
	 	    if (!listadogrid.getRowsNum())
	 	    	listadogrid.addRow(comprasGrid.uid(), "No Encontró Resultados.");
	 	})
		
		var formParams = jQuery(document.ordenCompraForm).serialize();
		listadogrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/listadoOrdenesCompra.action?'+ formParams);
		
	}


function cancelacionrOrden(bolean){
	removeCurrencyFormatOnTxtInput();
	var formParams = jQuery(document.definirOrdenForm).serialize();		
	if(confirm("¿Desea Cancelar la Orden de Compra? ")){
		sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/cancelarOrdenCompra.action?'+ formParams,targetWorkArea,null);
	}
}
function calculaIvaImporte(){
	removeCurrencyFormatOnTxtInput();
	var costo = dwr.util.getValue("costoUnitario");	
	var tipoPago = dwr.util.getValue("tipoPagoLis");	
	if (tipoPago=='PP' || tipoPago=='PB'  ){ //Se agrega PB aa validacion
		
		if(costo< 0){
			mostrarMensajeInformativo('El Costo Unitario debe ser mayor a cero', '20');
			return 0;
		}
		jQuery("#iva").val(calcularIva());
		jQuery("#ivaRetenido").val(calcularIvaRet());
		jQuery("#isr").val(calcularISR());
		//var total = parseFloat(costo)+parseFloat(calcularIva())-parseFloat(calcularIvaRet())-parseFloat(calcularISR());
		//sumaTotales();
		//jQuery("#importe").val(total);
		
	}else{
		jQuery("#importe").val(costo);
	}
	initCurrencyFormatOnTxtInput();
}
function validarIvaRetenido(){
	removeCurrencyFormatOnTxtInput();
	var costo = dwr.util.getValue("costoUnitario");
	var porcIvaRetenido = dwr.util.getValue("porcIvaRetenido");
	 
	
	if(porcIvaRetenido >100 || porcIvaRetenido<0){
    	mostrarMensajeInformativo('El %IVA Retenido debe ser en un rango de 0% a 100%', '20');
    	jQuery("#porcIvaRetenido").val(0);

	}else{
		var monto = calcularIvaRet();
		if (monto> costo){
			mostrarMensajeInformativo('El IVA Retenido no puede ser mayor al Costo Unitario', '20');
			jQuery("#ivaRetenido").val(0);
			jQuery("#porcIvaRetenido").val(0);
		}else{
			jQuery("#ivaRetenido").val(monto);
		}

	}
	sumaTotales();
	initCurrencyFormatOnTxtInput();

}


function validarIva(){
	removeCurrencyFormatOnTxtInput();
	var costo = dwr.util.getValue("costoUnitario");
	var porIva = dwr.util.getValue("porcIva");	
	if(porIva >100 || porIva<0){
    	mostrarMensajeInformativo('El %IVA debe ser en un rango de 0% a 100%', '20');
    	jQuery("#porcIva").val(0);
	}else{
		var monto = calcularIva();
		if (monto> costo){
			mostrarMensajeInformativo('El IVA no puede ser mayor al Costo Unitario', '20');
			jQuery("#porcIva").val(0);
			jQuery("#iva").val(0);
		}else{
			jQuery("#iva").val(monto);
		}
		
	}
	sumaTotales();
	initCurrencyFormatOnTxtInput();
	
}


function validarIsr(){
	removeCurrencyFormatOnTxtInput();
	var costo = dwr.util.getValue("costoUnitario");
	var porcIsr = dwr.util.getValue("porcIsr");	
	if(porcIsr >100 || porcIsr<0){
    	mostrarMensajeInformativo('El %ISR debe ser en un rango de 0% a 100%', '20');
    	jQuery("#porcIsr").val(0);

	}else{
		var monto = calcularISR();
		if (monto> costo){
			mostrarMensajeInformativo('El ISR no puede ser mayor al Costo Unitario', '20');
			jQuery("#porcIsr").val(0);
			jQuery("#isr").val(0);
		}else{
			jQuery("#isr").val(monto);
		}
	}
	sumaTotales();
	initCurrencyFormatOnTxtInput();
}


function calcularIva(){
	var impuesto = 0;
	var porIva = dwr.util.getValue("porcIva");	
	var costo = dwr.util.getValue("costoUnitario");
	if(costo>=0 &&  porIva>=0){
		impuesto = (porIva/100)*costo;
	}
	return impuesto;
}
function calcularIvaRet(){
	var impuesto =0;
	var porcIvaRetenido = dwr.util.getValue("porcIvaRetenido");
	var costo = dwr.util.getValue("costoUnitario");
	if(costo>=0 && porcIvaRetenido>=0){
		impuesto = (porcIvaRetenido/100)*costo;
	}
	return  impuesto ;
}

function calculaTotal(){
	removeCurrencyFormatOnTxtInput();
	sumaTotales();
	initCurrencyFormatOnTxtInput();
}
function sumaTotales(){
	

	var costoV =parseFloat(jQuery("#costoUnitario").val()) ;    
	var ivaV =parseFloat(jQuery("#iva").val()) ;
	var ivaRetV=parseFloat(jQuery("#ivaRetenido").val()) ;
	var isrV=parseFloat(jQuery("#isr").val()) ;
	var totalV =costoV+ivaV-ivaRetV-isrV;
	jQuery("#importe").val(totalV);
	

}
function calcularISR(){
	var impuesto =0;
	var porcIsr = dwr.util.getValue("porcIsr");	
	var costo = dwr.util.getValue("costoUnitario");
	if(costo>=0 &&  porcIsr>=0){
		impuesto=(porcIsr/100)*costo;		
	}
	return impuesto;
}

function calculaImportes(id){
	removeCurrencyFormatOnTxtInput();
      var data=jQuery("#definirOrdenForm").serialize();
      jQuery.ajax({
            url: '/MidasWeb/siniestros/valuacion/ordencompra/calcularImportes.action?idOrdenCompr='+id,
            dataType: 'json',
            async:false,
            type:"POST",
            data: data,
            success: function(json){
            	 var importe = json.importesDTO;
                 jQuery("#Text_subtotal").val(importe.subtotal);
                 jQuery("#Text_Iva").val(importe.iva);
                 jQuery("#Text_IvaRetenido").val(importe.ivaRetenido);
                 jQuery("#Text_Isr").val(importe.isr);
                // jQuery("#Text_Deducible").val(importe.deducible);
                 jQuery("#Text_total").val(importe.total);
                 jQuery("#text_Tpagado").val(importe.totalPagado);
                 jQuery("#text_Tpendiente").val(importe.totalPendiente);
            }
      });
      
      initCurrencyFormatOnTxtInput();
}

function changeConceptos(){
	removeCurrencyFormatOnTxtInput();
	var conceptoId = jQuery("#conceptoLis").val();
	var tipoPago = dwr.util.getValue("tipoPagoLis");
	importeSeleccionado = parseFloat (0.00);
	 jQuery("#costoUnitario").val(0);
	 jQuery("#porcIva").val(0);
	 jQuery("#iva").val(0);
	 jQuery("#importe").val(0);
	 jQuery("#observaciones").val(0);
	 jQuery("#nuevo").val(0);
	 jQuery("#alta").val(0);
	 jQuery("#observaciones").val(0);
	 jQuery("#ivaRetenido").val(0);
	 jQuery("#isr").val(0);
	 jQuery("#porcIsr").val(0);
	 jQuery("#porcIvaRetenido").val(0);	 
	 jQuery("#observaciones").val( '');
	 jQuery("#idDetalleOrdenSeleccionado").val( 0);	 
	var data= 'idConcepto='+conceptoId;

	 if (null==conceptoId || conceptoId==""){
		 initCurrencyFormatOnTxtInput();
		 return 0;
	 }
	 
	 jQuery.ajax({
         url: '/MidasWeb/siniestros/valuacion/ordencompra/cargaConcepto.action?',
         dataType: 'json',
         async:false,
         type:"POST",     
         data: data,
         success: function(json){
        	 	var conceptoAjuste = json.conceptoAjusteDTO;
        	 	 aplicaIsr =conceptoAjuste.aplicaIsr;
            	 aplicaIvaRetenido=conceptoAjuste.aplicaIvaRetenido;
            	 aplicaIva=conceptoAjuste.aplicaIva;
            	 ivaOrigen=conceptoAjuste.porcIva;
            	 ivaRetOrigen=conceptoAjuste.porcIvaRetenido;
            	 isrOrigen=conceptoAjuste.porcIsr;
            	 aplicaPorcentajes=conceptoAjuste.aplicaPorcentajes;
            	 if (tipoPago=='PP'  || tipoPago=='PB'){//Se agrega PB ala validacion
            		 if (aplicaIsr==true){
                		 jQuery("#porcIsr").removeAttr("disabled");
                		 jQuery("#isr").removeAttr("disabled");
                		 jQuery("#porcIsr").val(conceptoAjuste.porcIsr);
                	 }else{
                		 jQuery("#porcIsr").val(0);

                		 jQuery("#porcIsr").attr('disabled','disabled');
                		 jQuery("#isr").attr('disabled','disabled');
                	 }
                	 
                	 if (aplicaIva==true){
                		 jQuery("#porcIva").val(conceptoAjuste.porcIva);
                		 jQuery("#porcIva").removeAttr("disabled");
                		 jQuery("#iva").removeAttr("disabled");
                	 }else{
                		 jQuery("#porcIva").val(0);
                		 jQuery("#porcIva").attr('disabled','disabled');
                		 jQuery("#iva").attr('disabled','disabled');
                	 }
                	
                	 if (aplicaIvaRetenido==true){
                		 jQuery("#porcIvaRetenido").val(conceptoAjuste.porcIvaRetenido);
                		 jQuery("#porcIvaRetenido").removeAttr("disabled");
                		 jQuery("#ivaRetenido").removeAttr("disabled");
                	 }else{
                		 jQuery("#porcIvaRetenido").val(0);
                		 jQuery("#porcIvaRetenido").attr('disabled','disabled');
                		 jQuery("#ivaRetenido").attr('disabled','disabled');
                	 }
            		 
            		 //Se comenta Validacion para PB
            	 }/*else {
            		 if(tipoPago=="PB" && aplicaPorcentajes==true){
            			 nuevoConcepto();
            			 mostrarMensajeInformativo('El Concepto Seleccionado maneja porcenajes, no aplica para tipo Beneficiario.', '20');
            		 }
            		 
            		 
            	 }*/
         }
	 });
	 initCurrencyFormatOnTxtInput();
}

function iniContenedoDetarGrid(){
	iniDetarGrid();
	 jQuery("#idDetalleOrdenSeleccionado").val( 0);
	 var idOrden = jQuery("#idOrdencompra").val();
	 var tipoProveedor = jQuery("#tipoProveedorLis").val();
	 var origen = jQuery("#origen").val();

	 
	if(null==tipoProveedor || tipoProveedor==""|| tipoProveedor!='GRUA'){
		jQuery("#tallerBtn").hide();	
	}else{
			jQuery("#tallerBtn").show();	

	}
	
	var lectura = dwr.util.getValue("soloLectura");	
	if(lectura==true){
		bloquearOrdenCompra(true);
	}else{
		var tipo = dwr.util.getValue("tipoOrdenCompraLis");		
		if  (tipo=='OC'){
			jQuery("#divcoberura1").show();
			jQuery("#divterceroafec").show();
//			jQuery("#ch_aplDeducible").removeAttr("disabled");
		}else {
//			jQuery("#ch_aplDeducible").attr('disabled','disabled');
			jQuery("#divcoberura1").show();
			jQuery("#divterceroafec").hide();
			
		}
		
		var tipoP = dwr.util.getValue("tipoPagoLis");
		if  (tipoP=='PP'){
			jQuery("#divNomBenef").hide();
			jQuery("#divdatosBeneficiario").hide();
			jQuery("#tituloPago").html("Datos de Pago a Proveedor");  
			jQuery("#divProvedor1").show();
			jQuery("#divProvedorNom1").show();
			jQuery("#curp").attr('disabled','disabled');
			jQuery("#factura").attr('disabled','disabled');
			jQuery("#rfc").attr('disabled','disabled');
			var tipoProveedor=  jQuery('#tipoProveedorLis').val();
			if(tipoProveedor!= null && tipoProveedor!=""){
				cargaCatalogoPrestador(tipoProveedor);
			}
			
			
		}else {
			jQuery("#divNomBenef").show();
			jQuery("#divdatosBeneficiario").show();
			jQuery("#divProvedor1").hide();
			jQuery("#divProvedorNom1").hide();
			jQuery("#tituloPago").html("Datos de Pago a Beneficiario");
			jQuery("#curp").removeAttr("disabled");
			jQuery("#factura").removeAttr("disabled");
			jQuery("#rfc").removeAttr("disabled");
		}		
		if  (tipo=='RGA'){
			jQuery("#divReembolsoGA").show();
			jQuery("#divAfectadoRembolsoGA").show();
			jQuery("#contenedorFiltrosCompras2").height( 140 );
		}else {
			if(tipo=='GA'){
				jQuery("#divAfectadoRembolsoGA").show();
			}else{
				jQuery("#divAfectadoRembolsoGA").hide();
			}
			jQuery("#divReembolsoGA").hide();
		}
		if  (origen=='AUT_IND'  ||  origen== 'HGS_IN_PT'){
			 jQuery("#divDatosIndemnizacion").show();
			 jQuery("#contenedorFiltrosCompras2").height( 140 );
		}else {
			jQuery("#divDatosIndemnizacion").hide();
		}
		var id = dwr.util.getValue("idOrdencompra");
		if(null==id || id==''){
			blockDetaConcepto();
		}	
		ocultarPorcentajes();
	}
	
}

function iniDetarGrid(){
	var formParams = jQuery(document.definirOrdenForm).serialize();
	var id = dwr.util.getValue("idOrdenCompra");
	document.getElementById("conceptosGrid").innerHTML = '';
	conceptosGrid = new dhtmlXGridObject("conceptosGrid");
	conceptosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	conceptosGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
		calculaImportes(id);
	});
	conceptosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	conceptosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	
	conceptosGrid.attachEvent("onXLE", function(){
	    if (conceptosGrid.getRowsNum()){
	    	bloquearHeader(true);
	    	
	    	}else{
	    		bloquearHeader(false);
	    	}
	});

	conceptosGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/buscarDetalleCompra.action?'+formParams);
}
//aplica solo para indemnizacion 
function iniDetaIndemnizacionrGrid(){
	var formParams = jQuery(document.definirOrdenForm).serialize();
	var id = dwr.util.getValue("idOrdenCompra");
	document.getElementById("conceptosGrid").innerHTML = '';
	conceptosGrid = new dhtmlXGridObject("conceptosGrid");
	conceptosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	conceptosGrid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
		calculaImportes(id);
	});
	conceptosGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	conceptosGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });	
	
	conceptosGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/buscarDetalleCompra.action?'+formParams);
}


function actualizaDetalle(){
	 jQuery("#conceptoLis").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),13).getValue() );
	 jQuery("#costoUnitario").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),2).getValue() );
	 jQuery("#porcIva").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),3).getValue() );
	 jQuery("#iva").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),4).getValue() );
	 jQuery("#importe").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),9).getValue() );	 
	 jQuery("#porcIvaRetenido").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),5).getValue() );
	 jQuery("#ivaRetenido").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),6).getValue() );
	 jQuery("#porcIsr").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),7).getValue() );
	 jQuery("#isr").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),8).getValue() );
	 jQuery("#observaciones").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),12).getValue() );
	 jQuery("#idDetalleOrdenSeleccionado").val( conceptosGrid.cells(conceptosGrid.getSelectedId(),14).getValue() );
	 importeSeleccionado = parseFloat (conceptosGrid.cells(conceptosGrid.getSelectedId(),2).getValue() );
	 initCurrencyFormatOnTxtInput();
}




function nuevoConcepto(){
	jQuery("#conceptoLis").val("");
	 jQuery("#costoUnitario").val("");
	 jQuery("#porcIva").val(0);
	 jQuery("#iva").val("");
	 jQuery("#importe").val("");
	 jQuery("#observaciones").val("");
	 jQuery("#nuevo").val("");
	 jQuery("#alta").val("");
	 jQuery("#observaciones").val("");
	 jQuery("#ivaRetenido").val("");
	 jQuery("#isr").val("");
	 jQuery("#porcIsr").val(0);
	 jQuery("#porcIvaRetenido").val(0);
	 importeSeleccionado = parseFloat (0.00);
	}

function altaConcepto(){	
	removeCurrencyFormatOnTxtInput();
	blockPage(); //parche aigg
	 var seleccionadoIdDeta = dwr.util.getValue("idDetalleOrdenSeleccionado");
	 var idcompuesta = dwr.util.getValue("coberturaLis");
	 var split = idcompuesta.split('|');
	 var idCobertura  = split[0];
	 var cve = split[1];
	 var idTercero = dwr.util.getValue("terceroAfectadoLis");
	var concepto = dwr.util.getValue("conceptoLis");	
	var costo = dwr.util.getValue("costoUnitario");	
	var ivaPor = dwr.util.getValue("porcIva");	
	var iva = dwr.util.getValue("iva");	
	var importe = dwr.util.getValue("importe");	
	var tipoPago = dwr.util.getValue("tipoPagoLis");	
	var porcIsr = dwr.util.getValue("porcIsr");	
	var isr = dwr.util.getValue("isr");	
	var porcIvaRetenido = dwr.util.getValue("porcIvaRetenido");	
	var ivaRetenido = dwr.util.getValue("ivaRetenido");	
	var isValidado= true;
	var validarReserva= false;
	 var id = jQuery("#idOrdenCompra_h").val();
	//var id = dwr.util.getValue("idOrdencompra");
	var obs = dwr.util.getValue("observaciones");	
	var tipo = dwr.util.getValue("tipoOrdenCompraLis");	
	if (null==id || id==""){
		isValidado= false;
    	mostrarMensajeInformativo('Debe existir una Orden de Compra ', '20');
	}
	
	if (null==concepto || concepto==""){
		isValidado= false;
    	mostrarMensajeInformativo('Debe Capturar Detalle Concepto', '20');
	}
	
	if (null==costo || costo==""){
		isValidado= false;
    	mostrarMensajeInformativo('Debe Capturar Costo Unitario', '20');
	}
	
	if (  costo!=0   	){//Se agrega PB ala validacion
		
		if (null==ivaPor || ivaPor==""){
			isValidado= false;
	    	mostrarMensajeInformativo('El %IVA no puede ser nulo', '20');
		}else{
			if(ivaPor >100 || ivaPor<0 ){
				isValidado= false;
		    	mostrarMensajeInformativo('El %IVA debe ir en un rango de 0 a 100', '20');
			}
		}
		
		if (null==porcIsr || porcIsr==""){
			isValidado= false;
	    	mostrarMensajeInformativo('El %ISR no puede ser nulo', '20');
		}else{
			if(porcIsr >100 || porcIsr<0 ){
				isValidado= false;
		    	mostrarMensajeInformativo('El %ISR debe ir en un rango de 0 a 100', '20');
			}
		}
		if (null==porcIvaRetenido || porcIvaRetenido==""){
			isValidado= false;
	    	mostrarMensajeInformativo('El %IVA Retenido no puede ser nulo', '20');
		}else{
			if(porcIvaRetenido >100 || porcIvaRetenido<0 ){
				isValidado= false;
		    	mostrarMensajeInformativo('El %IVA Retenido debe ir en un rango de 0 a 100', '20');
			}
		}
		
		if ( parseFloat (iva)> parseFloat(costo)){
			isValidado= false;
	    	mostrarMensajeInformativo('el I.V.A no puede ser Mayor al Costo Unitario ', '20');
		}
		
		if ( parseFloat (isr) >parseFloat(costo)){
			isValidado= false;
	    	mostrarMensajeInformativo('el I.S.R no puede ser Mayor al Costo Unitario', '20');
		}
		
		if (parseFloat(ivaRetenido) >parseFloat(costo)){
			isValidado= false;
	    	mostrarMensajeInformativo('el  I.V.A Retenido no puede ser Mayor al Costo Unitario', '20');
		}
		 
		 if (ivaOrigen>0 && iva==0){ 
			 isValidado= false;
		    	mostrarMensajeInformativo('La configuracion del concepto no permite IVA igual a cero', '20');
		 }
		 
		 if (isrOrigen>0 && isr==0){
			 isValidado= false;
		    	mostrarMensajeInformativo('La configuracion del concepto no permite ISR igual a cero', '20');
		 }
		 
		 if (ivaRetOrigen>0 && ivaRetenido==0){
			 isValidado= false;
		    	mostrarMensajeInformativo('La configuracion del concepto no permite IVA Retenido igual a cero', '20');
		 }
		 
		 if (null==importe || importe==""){
				isValidado= false;
		    	mostrarMensajeInformativo('El importe no puede ser nulo', '20');
			}
			
	}
	
	if (tipo =='GA'){
		if (costo==0 && importe==0 && isr==0 && ivaRetenido==0 ){
			 var aplicaAltaCeros = dwr.util.getValue("altaCeros");
			if (aplicaAltaCeros=='false'){
				isValidado= false;
				mostrarMensajeInformativo('El usuario no tiene permisos para capturar conceptos en ceros ', '20');
			}
		}
		
	}else {
		if (costo==0 ){
			 var aplicaAltaCeros = dwr.util.getValue("altaCeros");

			if (aplicaAltaCeros=='false'  || tipo !='GA' ){
				isValidado= false;
				mostrarMensajeInformativo('El usuario no tiene permisos para capturar conceptos en ceros ', '20');
			}
			
		}
	}
	
	var reembolsoGastoAList = dwr.util.getValue("reembolsoGastoAList");	
	if(isValidado==true && tipo=='OC'  ){
				var reservaDisp = 0;
				utileriasService.obtenerReservaDisponibleOrdenCompra(idTercero,null,  idCobertura ,function(data){
					reservaDisp= parseFloat(data)+  parseFloat(importeSeleccionado)  ;
					if(costo> reservaDisp  /*&& seleccionadoIdDeta>0*/ ){
						mostrarMensajeInformativo('El Costo Unitario $ '+costo+' es mayor a la reserva disponible $'+reservaDisp , '20');
						isValidado= false;
					}
					
					if (isValidado==true){
						var formParams = jQuery(document.definirOrdenForm).serialize();
						nuevoConcepto();
						document.getElementById("conceptosGrid").innerHTML = '';
						conceptosGrid = new dhtmlXGridObject("conceptosGrid");
						conceptosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
						conceptosGrid.attachEvent("onXLE", function(grid_obj){
							unblockPage();
							calculaImportes(id);});
						conceptosGrid.attachEvent("onXLS", function(grid){
								mostrarIndicadorCarga("indicador");
						   });
						conceptosGrid.attachEvent("onXLE", function(grid){
								ocultarIndicadorCarga('indicador');
						   });	
						conceptosGrid.attachEvent("onXLE", function(){
						    if (conceptosGrid.getRowsNum()){
						    	bloquearHeader(true);
						    	}else{
						    		bloquearHeader(false);
						    	}
						    changeConceptos();
						});
						conceptosGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/agregarConcepto.action?'+formParams);
							
						}
				});
		}else {
			
			if (isValidado==true){
				var formParams = jQuery(document.definirOrdenForm).serialize();
				nuevoConcepto();
				document.getElementById("conceptosGrid").innerHTML = '';
				conceptosGrid = new dhtmlXGridObject("conceptosGrid");
				conceptosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
				conceptosGrid.attachEvent("onXLE", function(grid_obj){
					unblockPage();
					calculaImportes(id);});
				conceptosGrid.attachEvent("onXLS", function(grid){
						mostrarIndicadorCarga("indicador");
				   });
				conceptosGrid.attachEvent("onXLE", function(grid){
						ocultarIndicadorCarga('indicador');
				   });	
				conceptosGrid.attachEvent("onXLE", function(){
				    if (conceptosGrid.getRowsNum()){
				    	bloquearHeader(true);
				    	}else{
				    		bloquearHeader(false);
				    	}
				    changeConceptos();
				});
				conceptosGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/agregarConcepto.action?'+formParams);
				}
		}
	unblockPage(); //parche aigg
	}

function bloquearOrdenCompra(bolean){
	if (bolean==true){
		jQuery("#coberturaLis").attr('disabled','disabled');
		jQuery("#tipoOrdenCompraLis").attr('disabled','disabled');
		jQuery("#tipoPagoLis").attr('disabled','disabled');
		jQuery("#nomBeneficiario").attr('disabled','disabled');
		jQuery("#tipoPersonaLis").attr('disabled','disabled');		
		jQuery("#btnBorrarProveedor").hide();	
		jQuery("#tipoProveedorLis").attr('disabled','disabled');
		jQuery("#nombrePrestadorServicio").attr('disabled','disabled');//jQuery("#proveedorLis").attr('disabled','disabled'); CAMBIO_PROVEEDOR_AUTOCOMPLETE
		jQuery("#curp").attr('disabled','disabled');
		jQuery("#rfc").attr('disabled','disabled');
		jQuery("#factura").attr('disabled','disabled');
		jQuery("#creadoPor").attr('disabled','disabled');
		jQuery("#modificadoPor").attr('disabled','disabled');
		jQuery("#terceroAfectadoLis").attr('disabled','disabled');
		jQuery("#guardarBtn").hide();
		jQuery("#terminoAjuste").attr('disabled','disabled');
		jQuery("#terminoSiniestro").attr('disabled','disabled');
		jQuery("#rfcOrigen").attr('disabled','disabled');
		jQuery("#proveedorOrigen").attr('disabled','disabled');
		jQuery("#facturaOrigen").attr('disabled','disabled');
		jQuery("#afectadoLis").attr('disabled','disabled');
		jQuery("#bancosLis").attr('disabled','disabled');
		jQuery("#clabe").attr('disabled','disabled');
		jQuery("#correo").attr('disabled','disabled');
		jQuery("#lada").attr('disabled','disabled');
		jQuery("#telefono").attr('disabled','disabled');
//		jQuery("#ch_aplDeducible").attr('disabled','disabled');
		jQuery("#altaConcepto").hide();	
	}else{
		jQuery("#coberturaLis").removeAttr("disabled");
		jQuery("#tipoOrdenCompraLis").removeAttr("disabled");
		jQuery("#tipoPagoLis").removeAttr("disabled");
		jQuery("#nomBeneficiario").removeAttr("disabled");
		jQuery("#tipoPersonaLis").removeAttr("disabled");
		jQuery("#tipoProveedorLis").removeAttr("disabled");
		jQuery("#btnBorrarProveedor").show();	
		jQuery("#nombrePrestadorServicio").removeAttr("disabled");//jQuery("#proveedorLis").removeAttr("disabled"); CAMBIO_PROVEEDOR_AUTOCOMPLETE
		jQuery("#curp").removeAttr("disabled");
		jQuery("#rfc").removeAttr("disabled");
		jQuery("#factura").removeAttr("disabled");
		jQuery("#creadoPor").removeAttr("disabled");
		jQuery("#modificadoPor").removeAttr("disabled");
		jQuery("#terceroAfectadoLis").removeAttr("disabled");
		jQuery("#terminoAjuste").removeAttr("disabled");
		jQuery("#terminoSiniestro").removeAttr("disabled");
		jQuery("#rfcOrigen").removeAttr("disabled");
		jQuery("#proveedorOrigen").removeAttr("disabled");
		jQuery("#facturaOrigen").removeAttr("disabled");
		jQuery("#afectadoLis").removeAttr("disabled");
		jQuery("#bancosLis").removeAttr("disabled");
		jQuery("#clabe").removeAttr("disabled");
		jQuery("#correo").removeAttr("disabled");
		jQuery("#lada").removeAttr("disabled");
		jQuery("#telefono").removeAttr("disabled");
//		jQuery("#ch_aplDeducible").removeAttr("disabled");
		jQuery("#guardarBtn").show();
		jQuery("#altaConcepto").show();	
	}
}
function eliminarConcepto(id, idCompra){
	if(confirm("¿Desea eliminar el concepto? ")){
		document.getElementById("conceptosGrid").innerHTML = '';
		 conceptosGrid = new dhtmlXGridObject("conceptosGrid");
		 conceptosGrid.attachEvent("onXLS", function(grid_obj){
			 blockPage()});
		 conceptosGrid.attachEvent("onXLE", function(grid_obj){
			 calculaImportes(idCompra);
			 unblockPage();
			 
		 });
		 conceptosGrid.attachEvent("onXLS", function(grid){
				mostrarIndicadorCarga("indicador");
		    });
		 conceptosGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
		    });	
		 conceptosGrid.attachEvent("onXLE", function(){
			    if (conceptosGrid.getRowsNum()){
			    	bloquearHeader(true);
			    	}else{
			    		bloquearHeader(false);
			    		
			    	}
			    changeConceptos();
			});
			conceptosGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/eliminarConcepto.action?idOrdenCompraDetalle='+id);
	}
	 
}
function limpiarContenedorOrdenCompra(){
		jQuery(document.ordenCompraForm).each(function(){
			  this.reset();
		});
	}


function cerrar(){
	 var tipoOrdenCompra 	= jQuery("#tipoOrdenCompra").val();
	 var origen 	= jQuery("#origen").val();
	 var idReporteCabina = jQuery("#idToReporte").val();
		var urlReporte = regresarReporteCabina + "?idToReporte="+idReporteCabina;
	if (origen=='LISTADOREPORTE'){
		sendRequestJQ(null, '/MidasWeb/siniestros/cabina/reportecabina/mostrarListadoReportes.action',targetWorkArea, null);
	}else if (origen=='BANDEJAORDENESCOMPRA'){
		sendRequestJQ(null, '/MidasWeb/siniestros/valuacion/ordencompra/mostrarBandejaOrdenCompra.action',targetWorkArea, null);
	}else{
		 if(tipoOrdenCompra=='GA'  ||  tipoOrdenCompra=='RGA'){
			 volverReporteCabina();
		 }else {
			 var modoConsulta 	= jQuery("#modoConsulta").val();

			 var soloConsulta =0;
			 if(modoConsulta=='true'){
				 soloConsulta=1;
			 }
			 var urlPath = '/MidasWeb/siniestros/cabina/siniestrocabina/puedeVerAfectacionInciso.action?siniestroDTO.reporteCabinaId=' + idReporteCabina+ "&soloConsulta=" + soloConsulta;
				jQuery.ajax({
					url: urlPath,
					dataType: 'json',
					async:false,
					type:"GET",
					data: null,
					success: function(json){
						regresarAfectacionIncisoCallBack(json,modoConsulta);
					}
				});
		 }	
	}
}

function regresarAfectacionIncisoCallBack(json, esConsulta){
	var idReporteCabina = jQuery("#idToReporte").val();
	var validOnMillis = jQuery("#h_validOnMillis").val();
	var recordFromMillis = jQuery("#h_recordFromMillis").val();
	var soloConsulta = null;
	if( esConsulta=='true' ){
		soloConsulta = 1;
	} else {
		soloConsulta = 0;
	}
	var puedeVerAfetacion = json.verAfectacionInciso;
	if(puedeVerAfetacion){
		var url = "/MidasWeb/siniestros/cabina/siniestrocabina/mostrarContenedor.action?siniestroDTO.reporteCabinaId="+ idReporteCabina + '&soloConsulta=' + soloConsulta + '&validOnMillis=' + validOnMillis + '&recordFromMillis=' + recordFromMillis;
		sendRequestJQ(null, url, 'contenido', null);
	}else{
		mostrarMensajeInformativo('Es necesario guardar previamente la información del inciso', '20');
	}
}

function volverReporteCabina(){
	var idToReporte 		= jQuery("#idToReporte").val();
	var soloConsulta 		= jQuery("#modoConsulta").val();
	var url = regresarReporteCabina + "?idToReporte="+idToReporte;
	var cerrar = true;
		if(soloConsulta == 'true' ){
			sendRequestJQ(null, url,targetWorkArea, "setConsultaReporte();");
		}else{
			sendRequestJQ(null, url,targetWorkArea, null);
		}
	
}

 function cerrarDetalle(){
 	
	 var id 					= jQuery("#idReporteCabina").val();
	 var origenPantalla 					= jQuery("#origenPantalla").val();
	 var bandejaPorAutorizar 	= jQuery("#bandejaPorAutorizar").val();
	 var tipoOrdenCompra 	= jQuery("#tipoOrdenCompra").val();
	 var modoConsulta 	= jQuery("#modoConsulta").val();
	 
	 var url 					= '';
	 if(confirm("¿Desea cerrar la pantalla? La información no almacenada se perderá." )){

	 	if (origenPantalla=='BANDEJAORDENESCOMPRA'){
			sendRequestJQ(null, '/MidasWeb/siniestros/valuacion/ordencompra/mostrarBandejaOrdenCompra.action',targetWorkArea, null);
		}else{
			if( bandejaPorAutorizar == 'true'){
				url = '/MidasWeb/siniestros/valuacion/ordencompraautorizacion/mostrarListadoOrdenesPorAutorizar.action';
			}else{
				url = '/MidasWeb/siniestros/valuacion/ordencompra/mostrar.action'+ '?idReporteCabina=' + id+ "&tipo=" + tipoOrdenCompra+ "&origen=" + origenPantalla+ "&modoConsulta=" + modoConsulta;
			}
		}
	 	sendRequestJQ(null,url,targetWorkArea,null);
	 }
	
}
 
 
 function cerrarOCIndemnizacion(){
	    var idIndemnizacion = jQuery("#idIndemnizacion").val();
		var url = regresarListaIndemnizacionPath + "?filtroPT.idIndemnizacion=" + idIndemnizacion;
		sendRequestJQ(null,url,targetWorkArea,'buscarPerdidasTotales(false);');
	 }
 
 
 function consultarOrden(id){
		var origen 	= jQuery("#origen").val();   
		var modoConsulta 	= jQuery("#modoConsulta").val();  
		sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/consultarDetalleOrdenCompra.action?idOrdenCompra='+id+"&origen=" + origen+"&modoConsulta=" + modoConsulta,targetWorkArea,null);
	}
 
 function editarOrden(id){
	 	var origen 	= jQuery("#origen").val();
		sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/detalleOrdenCompra.action?idOrdenCompra='+id+"&origen=" + origen  ,targetWorkArea,null);
	}
 
 function changeTipoOrden() {	
		var tipo = dwr.util.getValue("tipoOrdenCompraLis");	
		var id = dwr.util.getValue("idReporteCabina");
		
		if(null ==tipo   || tipo=="" || tipo!='OC'){

			dwr.util.removeAllOptions("coberturaLis");	
			jQuery("#imprimirBtn").hide();
			jQuery("#verBtn").hide();
			
		}else{
			jQuery("#imprimirBtn").show();
			jQuery("#verBtn").show();
		listadoService.getCoberturasOrdenCompra( id,tipo ,function(data){
			dwr.util.removeAllOptions("coberturaLis");
			dwr.util.addOptions("coberturaLis", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("coberturaLis", data);
		});
		
		}
		if  (tipo=='OC'){
			jQuery("#divcoberura1").show();
			jQuery("#divterceroafec").show();
			
		}else {
			jQuery("#divcoberura1").hide();
			jQuery("#divterceroafec").hide();
			dwr.util.removeAllOptions("coberturaLis");	
		}
 }
 
 
 function cargaConceptos() {	
		var tipo = dwr.util.getValue("tipoOrdenCompraLis");	
		var idcompuesta = dwr.util.getValue("coberturaLis");
		var tipoConcepto=null;
		var tipoProveedorLis = dwr.util.getValue("tipoProveedorLis");	
		 var split = idcompuesta.split('|');
		 var idCob = split[0];
		 var cve = split[1];
		 jQuery("#idCoberturaReporteCabina").val(idCob);
		 jQuery("#cveSubTipoCalculoCobertura").val(cve);
		var tipoCategoria=0;
		 
		if(null== tipo || tipo==""){
			dwr.util.removeAllOptions("conceptoLis");
		}else {
			if (tipo =='OC'){
				tipoCategoria= 1;
			}else {
				if (tipo =='GA'){
					tipoCategoria= 2;
				}else if (tipo =='RGA'){
					tipoCategoria= 3;
					tipoProveedorLis=null;
					tipoConcepto='RGA'
				}
			}
			listadoService.listarConceptosPorCoberturaOrdenCompra( idCob ,cve,tipoCategoria,tipoConcepto,true,tipoProveedorLis,function(data){
				dwr.util.removeAllOptions("conceptoLis");
				dwr.util.addOptions("conceptoLis", [ {
						id : "",
						value : "Seleccione..."
					} ], "id", "value");
				dwr.util.addOptions("conceptoLis", data);
			});
		}
}

 function changeTipoPrestador() {	
	 var tipo = dwr.util.getValue("tipoProveedorLis");	
	 limpiarProveedor();	 
	listaPrestadoresServicioOC = new Array();	
	 var tipoOC = dwr.util.getValue("tipoOrdenCompraLis");	
	 if (tipoOC=='GA' ){
		 dwr.util.removeAllOptions("conceptoLis");
	 }
	 if(null ==tipo   || tipo=="" ){
		 jQuery('#proveedorLis').val(''); //dwr.util.removeAllOptions("proveedorLis");		CAMBIO_PROVEEDOR_AUTOCOMPLETE
		}else{
			jQuery('#proveedorLis').val(''); //dwr.util.removeAllOptions("proveedorLis");		CAMBIO_PROVEEDOR_AUTOCOMPLETE
			cargaCatalogoPrestador(tipo);
			

			
		}
		
	 

 }
 
 function cargaCatalogoPrestador(tipo){
	 if(tipo!=null && tipo!=""){
			listadoService.getMapPrestadorPorTipo( tipo ,function(data){
				
				/*dwr.util.addOptions("proveedorLisPrueba", [ {
						id : "",
						value : "Seleccione..."
					} ], "id", "value");
				
				dwr.util.addOptions("proveedorLisPrueba", data);		
				*/
				
				
				var contador=0;
				jQuery.each( data, function( key, value ) { 
		    		var keyV=key;
		        	var valueV=value;
		        	listaPrestadoresServicioOC[contador] = { "label":'   '+valueV  , "id": keyV} ;
					contador=contador+1;
				});

				jquery143("#nombrePrestadorServicio").autocomplete({
				      source: listaPrestadoresServicioOC.size() > 0 ? listaPrestadoresServicioOC : function(request, response){
				      	        	
				      },
				      minLength: 3,
				      delay: 500,
				      select: function(event, ui) {
				      	jQuery("#proveedorLis").val(ui.item.id);      
				      	changeProveedor();      	
				 
				      },
				      search: function(event, ui) {
				      	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
				          var busqueda = jQuery("#nombrePrestadorServicio").val();
				          if(findItem(busqueda.toString().toLowerCase(), listaPrestadoresServicioOC).length == 0){
				          	//alert(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"));
				          	mostrarMensajeInformativo(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"), '20');
				          	jQuery("#nombrePrestadorServicio").val("");
				          	jQuery("#proveedorLis").val("");
				          	return false;
				          }
				      }
				  });


			});
	 }
	 
 }
 
 
 function changeProveedor() {	
	 var id = dwr.util.getValue("proveedorLis");
	 var tipoOC = dwr.util.getValue("tipoOrdenCompraLis");	
	 if (tipoOC=='GA' ){
		 dwr.util.removeAllOptions("conceptoLis");
	 }
	 
	 jQuery("#rfc").val("" );
	 jQuery("#curp").val("" );
	 if(null==id || id==""){
		 jQuery("#rfc").val("" );
		 jQuery("#curp").val("" );
	 }else{
		 if(null !=id   || id!='' ){
			 utileriasService.obtenerRFCbeneficiario( id ,function(data){
				jQuery("#rfc").val(data );
			});
			}
		 if(null !=id   || id!='' ){
			 utileriasService.obtenerCURPbeneficiario( id ,function(data){
				jQuery("#curp").val(data );
			});
			}
	 }
	 
	 
 }
 
 function blockDetaConcepto() {	
	 jQuery("#conceptoLis").attr('disabled','disabled');
	 jQuery("#costoUnitario").attr('disabled','disabled');
	 jQuery("#porcIva").attr('disabled','disabled');
	 jQuery("#iva").attr('disabled','disabled');
	 jQuery("#importe").attr('disabled','disabled');
	 jQuery("#observaciones").attr('disabled','disabled');
	 jQuery("#nuevo").attr('disabled','disabled');
	 jQuery("#alta").attr('disabled','disabled');
	 jQuery("#observaciones").attr('disabled','disabled');
	 jQuery("#ivaRetenido").attr('disabled','disabled');
	 jQuery("#porcIvaRetenido").attr('disabled','disabled');
	 jQuery("#porcIsr").attr('disabled','disabled');
	 jQuery("#isr").attr('disabled','disabled');
 } 
 
 function ocultarPorcentajes() {	
	 var tipoPago = dwr.util.getValue("tipoPagoLis");	
		if (tipoPago=='PP'){
			jQuery("#divPorcentajes").show();	
			
		}else{
			jQuery("#divPorcentajes").show();	
		}
 }
 
 function changeCobertura() {	
	 var idcompuesta = dwr.util.getValue("coberturaLis");
	var tipo = dwr.util.getValue("tipoOrdenCompraLis");
	 var split = idcompuesta.split('|');
	 var id = split[0];
	 var cve = split[1];
	 var tipoOC = dwr.util.getValue("tipoOrdenCompraLis");	
	 if (tipoOC=='OC' ){
		 dwr.util.removeAllOptions("conceptoLis");
	 }
	 jQuery("#idCoberturaReporteCabina").val(id);
	 jQuery("#cveSubTipoCalculoCobertura").val(cve);
	 if(null ==id   || id=="" || id=='0' ||tipo!='OC' ){
			dwr.util.removeAllOptions("terceroAfectadoLis");		
		}else{

		listadoService.getListasTercerosAfectadorPorCobertura( id ,cve,function(data){
			addOptions("terceroAfectadoLis", data, null) ;
			/*dwr.util.removeAllOptions("terceroAfectadoLis");
			dwr.util.addOptions("terceroAfectadoLis", [ {
					id : "",
					value : "Seleccione..."
				} ], "id", "value");
			dwr.util.addOptions("terceroAfectadoLis", data);*/
		});
		cambiarTipodePago();
		/* utileriasService.ordenCompraAplicaBeneficiario(id,cve ,function(data){
				var aplicaBeneficiario=data;
				if(aplicaBeneficiario==true){
					jQuery("#tipoPagoLis").val("PB");
					cambiarTipodePago();
				}});*/ //AIGG SE QUITA PARCHE VALIDACION APLICA BENEFICIARIO
		}
 }
 
 
 function changeTipoPago() {
	 	var idcompuesta = dwr.util.getValue("coberturaLis");
		var tipoOrden = dwr.util.getValue("tipoOrdenCompraLis");
//		jQuery("#ch_aplDeducible").attr('checked', false);
		if(tipoOrden=='OC'){
			//si selecciono cobertura , valida si solo acepta Beneficiario.
			if(null !=idcompuesta   && idcompuesta!=""){
				var split = idcompuesta.split('|');
				var id = split[0];
				var cve = split[1];
				cambiarTipodePago();
				/*utileriasService.ordenCompraAplicaBeneficiario(id,cve ,function(data){
					var aplicaBeneficiario=data;
					if(aplicaBeneficiario==true){
				    	mostrarMensajeInformativo('La cobertura, solo aplica para pago a Beneficiario ', '20');

						jQuery("#tipoPagoLis").val("PB");
						cambiarTipodePago();
					}else{
						cambiarTipodePago();
					}
					
				});*/  //AIGG SE QUITA PARCHE VALIDACION APLICA BENEFICIARIO
				
			}else{
				cambiarTipodePago();
			}
		}else{
			cambiarTipodePago();
		}
		
}
function cambiarTipodePago() {
	 var tipo = dwr.util.getValue("tipoPagoLis");
		if  (tipo=='PP'){
			jQuery("#divNomBenef").hide();
			jQuery("#divProvedor1").show();
			jQuery("#divProvedorNom1").show();
			jQuery("#divdatosBeneficiario").hide();
			jQuery("#nomBeneficiario").val("");
			jQuery("#tipoPersonaLis").val("");
			jQuery("#reembolsoGastoAList").val("");
			jQuery("#tituloPago").html("Datos de Pago a Proveedor");
			jQuery("#curp").attr('disabled','disabled');
			jQuery("#rfc").attr('disabled','disabled'); 
			jQuery("#factura").attr('disabled','disabled');
			jQuery("#factura").val("");
		}else {
			jQuery("#divNomBenef").show();
			jQuery("#divdatosBeneficiario").show();
			jQuery("#divProvedor1").hide();
			jQuery("#divProvedorNom1").hide();
			jQuery("#tipoProveedorLis").val("");
			jQuery("#proveedorLis").val("");
			jQuery("#tipoPersonaLis").val("");
			jQuery("#reembolsoGastoAList").val("");
			jQuery("#curp").val("");
			jQuery("#rfc").val("");
			jQuery("#factura").val("");
			jQuery("#tituloPago").html("Datos de Pago a Beneficiario");
			jQuery("#curp").removeAttr("disabled");
			jQuery("#rfc").removeAttr("disabled");
			jQuery("#factura").removeAttr("disabled");
		}
 }
 
 
 
 function cargaGridConceptos(){
		document.getElementById("conceptosGrid").innerHTML = '';
		comprasGrid = new dhtmlXGridObject("conceptosGrid");
		comprasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		comprasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		comprasGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		comprasGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
		var formParams = jQuery(document.ordenCompraForm).serialize();
	 	comprasGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/listadoOrdenesCompra.action?'+ formParams);
	}
 
 function nuevaOrden(){
	 var formParams  = jQuery(document.ordenCompraForm).serialize();
	 var ruta= '/MidasWeb/siniestros/valuacion/ordencompra/nuevaOrdenCompra.action?'+ formParams;
	 sendRequestJQ(null,ruta,targetWorkArea,null);
 }
 
 function nuevaReembolsoGA(){
	 jQuery("#tipoOrdenCompra").val("RGA");
	 var formParams  = jQuery(document.ordenCompraForm).serialize();
	 var ruta= '/MidasWeb/siniestros/valuacion/ordencompra/nuevaOrdenCompra.action?'+ formParams;
	 sendRequestJQ(null,ruta,targetWorkArea,null);
 }
 
 function nuevaGA(){
	 jQuery("#tipoOrdenCompra").val("GA");
	 var formParams  = jQuery(document.ordenCompraForm).serialize();
	 var ruta= '/MidasWeb/siniestros/valuacion/ordencompra/nuevaOrdenCompra.action?'+ formParams;
	 sendRequestJQ(null,ruta,targetWorkArea,null);
 }
 
 function buscarOrdenCompra(){
 	document.getElementById("pagingArea").innerHTML = '';
 	document.getElementById("infoArea").innerHTML = '';
 	var comprasGrid = new dhtmlXGridObject('robosGrid');	 
 	comprasGrid.attachEvent("onXLS", function(grid){	
 		blockPage();
     });
 	comprasGrid.attachEvent("onXLE", function(grid){		
 		unblockPage();
     });	
 	comprasGrid.attachEvent("onRowSelect", function(id,ind){		 
 		 jQuery("#reporteCabinaId").val( comprasGrid.cells(comprasGrid.getSelectedId(),8).getValue() );
 		 jQuery("#idCobertura").val( comprasGrid.cells(comprasGrid.getSelectedId(),9).getValue() );
 		 jQuery("#reporteRoboId").val( comprasGrid.cells(comprasGrid.getSelectedId(),15).getValue() );
 		var params = jQuery(document.ordenCompraForm).serialize();
 		sendRequestJQ(null,'/MidasWeb/siniestros/cabina/reporteCabina/moduloRobos/seguimientoRoboTotal.action?'+ params,targetWorkArea,null);
 	  });
 	conceptosGrid.attachEvent("onXLE", function(){
 	    if (!comprasGrid.getRowsNum())
 	    	comprasGrid.addRow(comprasGrid.uid(), "No Encontró Resultados.");
 	})
 	var formParams = jQuery(document.ordenCompraForm).serialize();
 	comprasGrid.load( '/MidasWeb/siniestros/valuacion/ordencompra/listadoOrdenesCompra.action?'+ formParams);
 }
function iniContenedorGrid() {
	buscarOrdenCompra();
} 

function imprimir(){
	removeCurrencyFormatOnTxtInput();
	var id = jQuery("#idOrdenCompra_h").val();
	if (null==id || id==""){
		isValidado= false;
    	mostrarMensajeInformativo('No a registado una Orden de Compra ', '20');
	}
	else{
		window.open( "/MidasWeb/siniestros/valuacion/ordencompra/imprimirOrdenCompra.action?"+jQuery(document.definirOrdenForm).serialize(), '');
	}
	initCurrencyFormatOnTxtInput();
}

function generarOrdenCompra(tipo,estimacion){
	var generar=true;
	if(generar==true){
		if(validateAll(true)){
			if(confirm("¿Desea Iniciar una indemnizacion?" )){
				jQuery("#ordenCompraIndForm").attr("action","generarOrdenCompraPorIndemnizacion.action");
				jQuery("#nomBeneficiario").val(encodeURI(jQuery("#nomBeneficiario").val()));
				jQuery("#nomBeneficiario").val(jQuery("#nomBeneficiario").val().replace(/%20/g, ' '));
				parent.submitVentanaModal("vm_ventanaModalCompra", document.ordenCompraIndForm);
			}
		}
	}
	
}


function ventanaModalCompra(idestimacion){
	if (null==idestimacion || idestimacion==''){
		mostrarMensajeInformativo('Debe proporcionar el numero de estimacion.', '20');
	}else{
		mostrarVentanaModal("vm_ventanaModalCompra", "", 100, 100, 420, 200, "/MidasWeb/siniestros/valuacion/ordencompra/mostrarOrdenCompraIndemnizacion.action?idEstimacionReporteCabina="+idestimacion , null);
	}
}

function iniContenedoOrdenCompraIndemnizacion(){
	iniDetaIndemnizacionrGrid();
	 var idOrden = jQuery("#idOrdencompra").val();
	 var isindemnizacion = dwr.util.getValue("soloIndemnizacion");	
	 var estatus = dwr.util.getValue("estatusTxt");	
	 var tipoP = dwr.util.getValue("tipoPagoLis");
	if(isindemnizacion=='true' ){
		jQuery("#coberturaLis").attr('disabled','disabled');
		jQuery("#tipoOrdenCompraLis").attr('disabled','disabled');
		jQuery("#tipoPagoLis").attr('disabled','disabled');
		jQuery("#tipoProveedorLis").attr('disabled','disabled');
		jQuery("#btnBorrarProveedor").hide();	
		jQuery("#nombrePrestadorServicio").attr('disabled','disabled'); //jQuery("#proveedorLis").attr('disabled','disabled'); CAMBIO_PROVEEDOR_AUTOCOMPLETE
		jQuery("#curp").removeAttr("disabled");
		jQuery("#rfc").removeAttr("disabled");
		jQuery("#nomBeneficiario").removeAttr("disabled");
		jQuery("#tipoPersonaLis").removeAttr("disabled");
		jQuery("#factura").removeAttr("disabled");
		jQuery("#conceptoLis").removeAttr("disabled");
		jQuery("#observaciones").removeAttr("disabled");
		jQuery("#creadoPor").attr('disabled','disabled');
		jQuery("#modificadoPor").attr('disabled','disabled');		
		jQuery("#terminoAjuste").attr('disabled','disabled');
		jQuery("#terminoSiniestro").attr('disabled','disabled');
		jQuery("#terceroAfectadoLis").attr('disabled','disabled');
		jQuery("#bancosLis").removeAttr("disabled");
		jQuery("#clabe").removeAttr("disabled");
		jQuery("#correo").removeAttr("disabled");
		jQuery("#lada").removeAttr("disabled");
		jQuery("#telefono").removeAttr("disabled");
//		jQuery("#ch_aplDeducible").attr('disabled','disabled');
		jQuery("#guardarBtn").show();
	}else{
		jQuery("#coberturaLis").attr('disabled','disabled');
		jQuery("#tipoOrdenCompraLis").attr('disabled','disabled');
		jQuery("#tipoPagoLis").attr('disabled','disabled');
		jQuery("#nomBeneficiario").attr('disabled','disabled');
		jQuery("#tipoPersonaLis").attr('disabled','disabled');
		jQuery("#tipoProveedorLis").attr('disabled','disabled');
		jQuery("#nombrePrestadorServicio").attr('disabled','disabled');//jQuery("#proveedorLis").attr('disabled','disabled'); CAMBIO_PROVEEDOR_AUTOCOMPLETE
		jQuery("#btnBorrarProveedor").hide();	
		jQuery("#curp").attr('disabled','disabled');
		jQuery("#rfc").attr('disabled','disabled');
		jQuery("#factura").attr('disabled','disabled');
		jQuery("#creadoPor").attr('disabled','disabled');
		jQuery("#modificadoPor").attr('disabled','disabled');
		jQuery("#terminoAjuste").attr('disabled','disabled');
		jQuery("#terminoSiniestro").attr('disabled','disabled');
		jQuery("#terceroAfectadoLis").attr('disabled','disabled');
		jQuery("#conceptoLis").attr('disabled','disabled');
		jQuery("#observaciones").attr('disabled','disabled');
		jQuery("#bancosLis").attr('disabled','disabled');
		jQuery("#clabe").attr('disabled','disabled');
		jQuery("#correo").attr('disabled','disabled');
		jQuery("#lada").attr('disabled','disabled');
		jQuery("#telefono").attr('disabled','disabled');
//		jQuery("#ch_aplDeducible").attr('disabled','disabled');
		jQuery("#guardarBtn").hide();
	}
	if  (tipoP=='PP'){
		jQuery("#divNomBenef").hide();
		jQuery("#divdatosBeneficiario").hide();
		jQuery("#tituloPago").html("Datos de Pago a Proveedor");  
		jQuery("#divProvedor1").show();
		jQuery("#divProvedorNom1").show();
	}else {
		jQuery("#divNomBenef").show();
		jQuery("#divdatosBeneficiario").show();
		jQuery("#divProvedor1").hide();
		jQuery("#divProvedorNom1").hide();
		jQuery("#tituloPago").html("Datos de Pago a Beneficiario");
	}
	if (estatus=='Indemnizacion'){
		jQuery("#divOC").hide();
	}else{
		jQuery("#divOC").show();
	}
}

function guardarOrdenCompraIndemnizacion(){
	blockPage();
	var validacion = validaGuardarOCIndemnizacion();
	unblockPage();
	if(validacion){
		guardarOCIndemnizacion();
	}
}


function validaGuardarOCIndemnizacion(){
	var valida= true;
	var bancosLis = dwr.util.getValue("bancosLis");
	var formaPago = dwr.util.getValue("formaPago");
	var concepto = dwr.util.getValue("conceptoLis");
	var estatus = dwr.util.getValue("estatusTxt");
	var total = dwr.util.getValue("Text_total");	
	var tipoPersonaLis = dwr.util.getValue("tipoPersonaLis");
	var nomBeneficiario = dwr.util.getValue("nomBeneficiario");
	if  (  (null==concepto || concepto=="") &&  (estatus=='Indemnizacion') ){
		mostrarMensajeInformativo('Debe confirmar el concepto de la orden de compra, seleccionando el concepto de pago  ' , '20');
		return false;
	}
	if (null==tipoPersonaLis || tipoPersonaLis =="" ){
		mostrarMensajeInformativo('Debe Seleccionar el tipo de persona  ' , '20');
		return false;
	}
	
	if ( formaPago =="TB" ){
		var clabe = dwr.util.getValue("clabe");	
		var correo = dwr.util.getValue("correo");	
		var lada = dwr.util.getValue("lada");	
		var telefono = dwr.util.getValue("telefono");	
		
		if (null==clabe || clabe =="" ){
			mostrarMensajeInformativo('El tipo de Forma de pago es Transferencia Bancaria, Debe capturar CLABE ' , '20');
			return false;
		}
		
		if (null==correo || correo =="" ){
			mostrarMensajeInformativo('El tipo de Forma de pago es Transferencia Bancaria, Debe capturar Correo ' , '20');
			return false;
		}
		
		if (null==lada || lada =="" ){
			mostrarMensajeInformativo('El tipo de Forma de pago es Transferencia Bancaria, Debe capturar Lada ' , '20');
			return false;
		}
		if (null==telefono || telefono =="" ){
			mostrarMensajeInformativo('El tipo de Forma de pago es Transferencia Bancaria, Debe capturar Telefono ' , '20');
			return false;
		}
		if (null==bancosLis || bancosLis =="" ){
			mostrarMensajeInformativo('Debe Seleccionar el banco receptor ' , '20');
			return false;
		}
	}
	
	if (null==nomBeneficiario || nomBeneficiario =="" ){
		mostrarMensajeInformativo('El nombre del Beneficiario no puuede ser nulo  ' , '20');
		return false;
	}
	
	return true;
}

function guardarOCIndemnizacion(){
		removeCurrencyFormatOnTxtInput();
		var idcompuesta = jQuery('#coberturaLis').val();    
		var split = idcompuesta.split('|');
		var idCobertura  = split[0];
		var idTercero = jQuery('#terceroAfectadoLis').val(); 
		var subtotal = jQuery('#Text_subtotal').val(); 
		var reservaDisp = 0;
		blockPage();
		utileriasService.obtenerReservaDisponibleOrdenCompra(jQuery('#terceroAfectadoLis').val(),null,  jQuery('#coberturaLis').val().split('|')[0],function(data){
			reservaDisp= parseFloat(data);
			if(subtotal > reservaDisp  ){
				mostrarMensajeInformativo('El Sub Total $ '+subtotal+' es mayor a la reserva disponible $'+reservaDisp , '20');
				valida=false;
				unblockPage();	
			}else{
				var formParams = jQuery(document.definirOrdenForm).serialize();
				unblockPage();	
				if(confirm("¿Desea Generar la Orden de Compra ? ")){
					sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/guardarOrdenCompraPorIndemnizacion.action?'+ formParams,targetWorkArea,null);
				}
			}
			
			
		});
}

function consultarOrdenCompraIndemnizacion(id,formaPago){
	sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/consultarDetalleOrdenCompraIndemnizacion.action?idOrdenCompra='+id+ "&formaPago=" +formaPago,targetWorkArea,null);
}


function consultarOrdenCompraListadoIndemnizacion(idOrdenCompra, formaPago, idIndemnizacion){
	var url = '/MidasWeb/siniestros/valuacion/ordencompra/consultarDetalleOrdenCompraIndemnizacion.action?idOrdenCompra=' + idOrdenCompra + "&formaPago=" + formaPago + "&idIndemnizacion=" + idIndemnizacion;
	sendRequestJQ(null,url,targetWorkArea,null);
}


function rechazar(){
	var idOrdenCompra = jQuery("#idOrdenCompra_h").val();
	var bandejaPorAutorizar = jQuery("#bandejaPorAutorizar").val();
	var url = rechazarOrdenCompra + "?" + "idOrdenCompra=" + idOrdenCompra + "&bandejaPorAutorizar=" + bandejaPorAutorizar/*+ "&soloLectura=" + isSoloLectura*/;
	if(confirm("¿Desea Rechazar la Orden de Compra? ")){
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function autorizar(){
	removeCurrencyFormatOnTxtInput();
	var idOrdenCompra = jQuery("#idOrdenCompra_h").val();
	 var origenPantalla = jQuery("#origenPantalla").val();
	 var id = dwr.util.getValue("idOrdenCompra");
	var bandejaPorAutorizar = jQuery("#bandejaPorAutorizar").val();
	var isSoloLectura = jQuery("#soloLectura").val();
	var url = autorizarOrdenCompra + "?" + "idOrdenCompra=" + idOrdenCompra + "&bandejaPorAutorizar=" + bandejaPorAutorizar+ "&soloLectura=" + isSoloLectura+ "&origen=" + origenPantalla;
	var data=jQuery("#definirOrdenForm").serialize();
      jQuery.ajax({
            url: '/MidasWeb/siniestros/valuacion/ordencompra/validarCalculoImportes.action?idOrdenCompr='+id,
            dataType: 'json',
            async:false,
            type:"POST",
            data: data,
            success: function(json){

            	 var importesConceptoDTO = json.importesPorConceptoDTO;
            	 var mensajeMostrar = json.mensajeMostrar;
            	 if(null!=mensajeMostrar && mensajeMostrar != ""){
             		mostrarMensajeInformativo('' +mensajeMostrar, '20');

            	 }else{
            		if(confirm("¿Desea Autorizar la Orden de Compra? ")){
            			sendRequestJQ(null, url, targetWorkArea, null);
            		} 
            	 }
            }
      });
      initCurrencyFormatOnTxtInput();
}

function validateIsFromBandeja(){
	var bandejaPorAutorizar = jQuery("#bandejaPorAutorizar").val();
	if( bandejaPorAutorizar == 'true'){
		document.getElementById("altaConcepto").innerHTML = '';
	}
}

function bloquearHeader(bolean){
	var tipoOrdenCompra = jQuery("#tipoOrdenCompra").val();
	var estatusTxt = jQuery("#estatusTxt").val();
	var lectura = dwr.util.getValue("soloLectura");	
	var tipoPagoLis=jQuery("#tipoPagoLis").val() ;
	if (bolean==true){
		if(tipoOrdenCompra=='OC'){
			jQuery("#coberturaLis").attr('disabled','disabled');
			jQuery("#terceroAfectadoLis").attr('disabled','disabled');
		}
		
		if(tipoOrdenCompra=='GA' ){
			jQuery("#tipoProveedorLis").attr('disabled','disabled');
			jQuery("#nombrePrestadorServicio").attr('disabled','disabled'); //jQuery("#proveedorLis").attr('disabled','disabled'); CAMBIO_PROVEEDOR_AUTOCOMPLETE
			jQuery("#btnBorrarProveedor").hide();	
		}
		jQuery("#tipoPagoLis").attr('disabled','disabled');
		
	}else{
		jQuery("#coberturaLis").removeAttr("disabled");
		jQuery("#tipoPagoLis").removeAttr("disabled");
		jQuery("#tipoProveedorLis").removeAttr("disabled");
		jQuery("#nombrePrestadorServicio").removeAttr("disabled");//jQuery("#proveedorLis").removeAttr("disabled"); CAMBIO_PROVEEDOR_AUTOCOMPLETE
		jQuery("#btnBorrarProveedor").show();	
		jQuery("#terceroAfectadoLis").removeAttr("disabled");
		
		
	}
	//Si el estatus es diferente de tramite se bloquea todo el header
	if (  (null!=estatusTxt && estatusTxt!="" && estatusTxt!='Tramite' )  ||   lectura=='true'  ){
		bloquearOrdenCompra(true);
	}
}


function desbloqueoSalvar(){
	jQuery("#rfc").removeAttr("disabled");
	jQuery("#curp").removeAttr("disabled");
	jQuery("#coberturaLis").removeAttr("disabled");
	jQuery("#tipoPagoLis").removeAttr("disabled");
	jQuery("#tipoProveedorLis").removeAttr("disabled");
	jQuery("#nombrePrestadorServicio").removeAttr("disabled"); //jQuery("#proveedorLis").removeAttr("disabled"); CAMBIO_PROVEEDOR_AUTOCOMPLETE
	jQuery("#btnBorrarProveedor").show();	
	jQuery("#terceroAfectadoLis").removeAttr("disabled");
}




function mostrarBuscarOrdenCompra(reportecabinaId,  cadenaCobertura,cadenaPaseAtencion,tipoOrdenC,  fieldName ){
	var tipoOrdenCompra;
	if(null==reportecabinaId && reportecabinaId !=""){
		mostrarVentanaMensaje("20","Debe Seleccionar Siniestro",null);

	}else if(null==tipoOrdenC && tipoOrdenC !=""){
		mostrarVentanaMensaje("20","Debe Seleccionar tipo de orden de compra",null);

	}else{
		if(tipoOrdenC==1){
			tipoOrdenCompra='OC';
		}else if (tipoOrdenC==2){
			tipoOrdenCompra='GA';
		}else{
			tipoOrdenCompra='';
		}
		var url = "/MidasWeb/siniestros/valuacion/ordencompra/mostrarBusquedaRecuperacion.action?idReporteCabina="+reportecabinaId+ "&tipo=" +tipoOrdenCompra+ "&cadenaCoberturas=" +cadenaCobertura+ "&cadenaPaseAtencion="+cadenaPaseAtencion  + "&fieldName="+fieldName ;
		mostrarVentanaModal("vm_ventanaBuscarOrdenCompra", "Buscar Orden Compra", null, null, 900, 410, url, null);
		//sendRequestJQ(null, url, targetWorkArea, null);

	}
}

function cerrarVentanaBuscarOC( ){
 	parent.cerrarVentanaModal("vm_ventanaBuscarOrdenCompra","redireccionaBusquedaOrdenCompra();");
}


var busquedaGrid;
function cargaGridBusquedaOrdenesCompra( ){
	
	var idToReporte =jQuery("#idReporteCabinaH").val();
		var tipo = jQuery("#tipoH").val();
		var cadenaCoberturas = jQuery("#cadenaCoberturasH").val();
		var fieldName = jQuery("#fieldNameH").val();
		var cadenaPaseAtencion = jQuery("#cadenaPaseAtencionH").val();
		
		
		document.getElementById("comprasBusquedaGrid").innerHTML = '';
		busquedaGrid = new dhtmlXGridObject("comprasBusquedaGrid");
		busquedaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		busquedaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		busquedaGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		//comprasGrid.setHeader   ("No Orden de Compra,         Concepto      ,Estatus       ,Fecha Alta    ,Fecha Baja,");
		busquedaGrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
		
		busquedaGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
		busquedaGrid.attachEvent("onXLE", function(){
	 	    if (!busquedaGrid.getRowsNum())
	 	    	busquedaGrid.addRow(busquedaGrid.uid(), "No Encontró Resultados.");
	 	})
		var formParams = jQuery(document.ordenCompraBusquedaForm).serialize();
		busquedaGrid.load("/MidasWeb/siniestros/valuacion/ordencompra/busquedaOrdenCompraRecuperacion.action?idReporteCabina="+idToReporte+ "&tipo=" +tipo+ "&cadenaCoberturas=" +cadenaCoberturas+ "&cadenaPaseAtencion="+cadenaPaseAtencion+ "&fieldName="+fieldName);
				
		
	}



function asignarOrdenesCompra(){
	
	var filtro = jQuery("#fieldNameH").val();
    var id=busquedaGrid.getSelectedId();
    var idValue=busquedaGrid.cellById(id, 0).getValue();
    var selectedId=busquedaGrid.getSelectedRowId();
    
    var	noProveedor	=busquedaGrid.cellById(id,0).getValue();
    var	nombreProveedor	=busquedaGrid.cellById(id,1).getValue();
    var	numOrdenCompra	=busquedaGrid.cellById(id,2).getValue();
    var	fechaOrdenCompra	=busquedaGrid.cellById(id,3).getValue();
    var	montoOrdenCompra	=busquedaGrid.cellById(id,4).getValue();
    var	adminRefacciones	=busquedaGrid.cellById(id,5).getValue();
    var	adminRefaccionesId	=busquedaGrid.cellById(id,6).getValue();
    var	correoProveedor	=busquedaGrid.cellById(id,7).getValue();
    var	esRefaccion	=busquedaGrid.cellById(id,8).getValue();
    var	factura	=busquedaGrid.cellById(id,9).getValue();
    var	fechaOrdenPago	=busquedaGrid.cellById(id,10).getValue();
    var	marcaVehiculo	=busquedaGrid.cellById(id,11).getValue();
    var	modeloVehiculo	=busquedaGrid.cellById(id,12).getValue();
    var	montoOrdenPago	=busquedaGrid.cellById(id,13).getValue();
    var	nombreTaller	=busquedaGrid.cellById(id,14).getValue();
    var	nombreValuador	=busquedaGrid.cellById(id,15).getValue();
    var	numValuacion	=busquedaGrid.cellById(id,16).getValue();
    var	telefonoProveedor	=busquedaGrid.cellById(id,17).getValue();
    var	tipoVehiculo	=busquedaGrid.cellById(id,18).getValue();
    var esResCivil = busquedaGrid.cellById(id,19).getValue();
    var esDanosMat = busquedaGrid.cellById(id,20).getValue();
    
   var txt= "	noProveedor	"+	noProveedor	+
    "	nombreProveedor	"+	nombreProveedor	+
    "	numOrdenCompra	"+	numOrdenCompra	+
    "	fechaOrdenCompra	"+	fechaOrdenCompra	+
    "	montoOrdenCompra	"+	montoOrdenCompra	+
    "	adminRefacciones	"+	adminRefacciones	+
    "	adminRefaccionesId	"+	adminRefaccionesId	+
    "	correoProveedor	"+	correoProveedor	+
    "	esRefaccion	"+	esRefaccion	+
    "	factura	"+	factura	+
    "	fechaOrdenPago	"+	fechaOrdenPago	+
    "	marcaVehiculo	"+	marcaVehiculo	+
    "	modeloVehiculo	"+	modeloVehiculo	+
    "	montoOrdenPago	"+	montoOrdenPago	+
    "	nombreTaller	"+	nombreTaller	+
    "	nombreValuador	"+	nombreValuador	+
    "	numValuacion	"+	numValuacion	+
    "	telefonoProveedor	"+	telefonoProveedor	+
    "	tipoVehiculo	"+	tipoVehiculo	;   
   	console.log("asignarOrdenesCompra "+ txt);   	
   	
   	if (parent.document.getElementById(filtro+'noProveedor')!=null){
   	   	parent.document.getElementById(filtro+'noProveedor').value=noProveedor;
   	}
   	
	if (parent.document.getElementById(filtro+'nombreProveedor' )!=null){
	   	parent.document.getElementById(filtro+'nombreProveedor').value=nombreProveedor;
	 }
	
	if (parent.document.getElementById(filtro+'numOrdenCompra' )!=null){
	   	parent.document.getElementById(filtro+'numOrdenCompra').value=numOrdenCompra;
	 }
	if (parent.document.getElementById(filtro+'fechaOrdenCompra' )!=null){
	   	parent.document.getElementById(filtro+'fechaOrdenCompra').value=fechaOrdenCompra;

	 }
	if (parent.document.getElementById(filtro+'montoOrdenCompra' )!=null){
	   	parent.document.getElementById(filtro+'montoOrdenCompra').value=montoOrdenCompra;

	 }
	if (parent.document.getElementById(filtro+'adminRefacciones' )!=null){
	   	parent.document.getElementById(filtro+'adminRefacciones').value=adminRefacciones;

	 }
	if (parent.document.getElementById(filtro+'adminRefaccionesId' )!=null){
	   	parent.document.getElementById(filtro+'adminRefaccionesId').value=adminRefaccionesId;

	 }
	if (parent.document.getElementById(filtro+'correoProveedor' )!=null){
	   	parent.document.getElementById(filtro+'correoProveedor').value=correoProveedor;
	 }
	if (parent.document.getElementById(filtro+'esRefaccion' )!=null){
	   	parent.document.getElementById(filtro+'esRefaccion').value=esRefaccion;
	 }
	if (parent.document.getElementById(filtro+'factura' )!=null){
	   	parent.document.getElementById(filtro+'factura').value=factura;
	 }
	if (parent.document.getElementById(filtro+'fechaOrdenPago' )!=null){
	   	parent.document.getElementById(filtro+'fechaOrdenPago').value=fechaOrdenPago;
	 }
	if (parent.document.getElementById(filtro+'marcaVehiculo' )!=null){
	   	parent.document.getElementById(filtro+'marcaVehiculo').value=marcaVehiculo;
	 }
	if (parent.document.getElementById(filtro+'modeloVehiculo' )!=null){
	   	parent.document.getElementById(filtro+'modeloVehiculo').value=modeloVehiculo;
	 }
	if (parent.document.getElementById(filtro+'montoOrdenPago' )!=null){
	   	parent.document.getElementById(filtro+'montoOrdenPago').value=montoOrdenPago;
	 }
	if (parent.document.getElementById(filtro+'nombreTaller' )!=null){
	   	parent.document.getElementById(filtro+'nombreTaller').value=nombreTaller;
	 }
	if (parent.document.getElementById(filtro+'nombreValuador' )!=null){
	   	parent.document.getElementById(filtro+'nombreValuador').value=nombreValuador;

	 }
	if (parent.document.getElementById(filtro+'numValuacion' )!=null){
	   	parent.document.getElementById(filtro+'numValuacion').value=numValuacion;

	 }
	if (parent.document.getElementById(filtro+'telefonoProveedor' )!=null){
	   	parent.document.getElementById(filtro+'telefonoProveedor').value=telefonoProveedor;

	 }
	if (parent.document.getElementById(filtro+'tipoVehiculo' )!=null){
	   	parent.document.getElementById(filtro+'tipoVehiculo').value=tipoVehiculo;

	 }
	
	if (parent.document.getElementById(filtro+'esDanosMateriales' )!=null){
	   	parent.document.getElementById(filtro+'esDanosMateriales').value=esDanosMat ;

	 }
	
	if (parent.document.getElementById(filtro+'esResponsabilidadCivil' )!=null){
	   	parent.document.getElementById(filtro+'esResponsabilidadCivil').value=esResCivil;

	 }
   	cerrarVentanaBuscarOC();

}

function validaInfoPagoCompania(){
	var tipoProveedor = dwr.util.getValue("tipoProveedorLis");
	if(tipoProveedor == 'CIA'){
		jQuery("#companiaDiv").show();
	}else{
		jQuery("#companiaDiv").hide();
	}


}

//-------- Inicia funcionalidad Autcomplete---------
//var jquery143;
// var listaPrestadoresServicioOC ;
function findItem (term, devices) {
	
	var items = [];
  for (var i=0;i<devices.length;i++) {
      var item = devices[i];
      for (var prop in item) {
          var detail = item[prop].toString().toLowerCase();           
          if (detail.indexOf(term)>-1) {
              items.push(item);
              break;               
          }
      }
  }
  return items; 
}

jquery143(function(){
	jquery143("#nombrePrestadorServicio").bind("keypress", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();         
	        return false;
	    }
	});
});

function limpiarProveedor()
{	
	jQuery('#nombrePrestadorServicio').val(''); 
	jQuery('#proveedorLis').val(''); 
	
	jQuery("#rfc").val("" );
	 jQuery("#curp").val("" );
	
}



function consultarBandejaOrdenesCompra(){
	sendRequestJQ(null,'/MidasWeb/siniestros/valuacion/ordencompra/mostrarBandejaOrdenCompra.action',targetWorkArea,null);
}
//-------- termina funcionalidad Autocomplete
