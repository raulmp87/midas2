/**
 * Control de TarifaVersion
 */

var targetHeaderTarifa = "headerTarifa";
var targetDetalleTarifa = "detalleTarifa";

function nuevaVersionTarifa() {
	
	sendRequestJQ(null, generarNuevaVersionPath + "?"+jQuery(document.tarifaVersionForm).serialize(), targetHeaderTarifa, 'cargaTarifas();');
	
}


function cambiaEstatusTarifa(nuevoEstatus) {
	
	
	var nextFunction = null;
	var estatusBorrado = dwr.util.getValue("estatusBorrado");
	if (nuevoEstatus == estatusBorrado) {
		nextFunction = 'cargaTarifas();';
	}
	if (nuevoEstatus == estatusBorrado) {
		var respuesta = confirm("\u00BFSeguro desea borrar la Versi\u00F3n?");
		if(!respuesta){
			return;
		}
	}
	dwr.util.setValue("estatus", nuevoEstatus);
	sendRequestJQ(null, cambiarEstatusPath + "?"+jQuery(document.tarifaVersionForm).serialize(), targetHeaderTarifa, nextFunction);
}

function iniciaTarifas(valorMenu) {
	if (valorMenu == null) {
		valorMenu = dwr.util.getValue("valorMenu");
	}
	var claveNegocio = GLOBAL_NEGOCIO;
	var version = dwr.util.getValue("tarifaVersion.id.version");
	var idMoneda = dwr.util.getValue("tarifaVersion.id.idMoneda");
	
	sendRequestJQTarifa(null, cargarPorValorMenuPath + "?valorMenu=" + valorMenu +
			"&version=" + version+"&idMoneda="+idMoneda+"&claveNegocio="+claveNegocio, targetHeaderTarifa, 'cargaTarifas();');
}


function cargaTarifas(){
	if(document.tarifaVersionForm != null){
		var negocio = dwr.util.getValue("tarifaVersion.tarifaConcepto.claveNegocio");
		var valorMenu = dwr.util.getValue("valorMenu");
		
		//Se establece la vista en caso de que este incluida en el valorMenu  LineaNegocio_idRiesgoConcepto(idPadreArbol)vistaTarifa
		var indiceFinConcepto = valorMenu.lastIndexOf(")");
		
		var vistaTarifa = (valorMenu.length - indiceFinConcepto > 1 ? 
				valorMenu.substr(indiceFinConcepto + 1, valorMenu.length - (indiceFinConcepto + 1))
				:"");
		if (negocio == 'D') {
			
			//Se regresa a la manera original que lo espera la configuracion de tarifas de Da�os
			var valorDanios = valorMenu.substr(valorMenu.lastIndexOf("_") + 1, (indiceFinConcepto + 1) - (valorMenu.lastIndexOf("_") + 1));
			
			var version = dwr.util.getValue("tarifaVersion.id.version");
			sendRequest(null,  "/MidasWeb/tarifa/configuracion/listar.do?idToRiesgo=" + valorDanios + "&version=" + version ,  targetDetalleTarifa , null);
		
		} else if (negocio == 'A'){
			cargarContenedorRegiDinOld(targetDetalleTarifa, vistaTarifa);	
		
		}else{
			unblockPage();
		}
	}
}


