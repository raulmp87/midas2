
/*
 * Variables globales
 */
var simpleDHXWindow = null;
var CONSULTA_DE_CLIENTE = 1;
var CONSULTA_DE_GRUPO_CLIENTE = 2;
var URL_BUSQUEDA_CLIENTE = null;
var IMAGE_PATH_DHX_WINDOW = null;
var ventanaClientesGlobal = null;

function obtenerVentanaClientes(){
	if(ventanaClientesGlobal == null)
		ventanaClientesGlobal = new VentanaClientes();
	return ventanaClientesGlobal;
}

/*
 * Objetos utilizados
 */
var ClienteJsBean = function(
			idCliente,
			idToPersona,
			claveTipoPersona,
			nombre,
			apellidoPaterno,
			apellidoMaterno,
			fechaNacimiento,
			codigoRFC,
			telefono,
			email,
			codigoCURP,
			sexo,
			idToDireccion,
			idDomicilio,
			
			nombreCalle,
			numeroExterior,
			numeroInterior,
			entreCalles,
			idColonia,
			idColoniaString,
			nombreColonia,
			nombreDelegacion,
			idMunicipio,
			idMunicipioString,
			idEstado,
			idEstadoString,
			codigoPostal,
			descripcionEstado,
			direccionCompleta
			){
	this.idCliente = idCliente;
	this.idToPersona = idToPersona;
	this.claveTipoPersona = claveTipoPersona;
	this.nombre = nombre;
	this.apellidoPaterno=apellidoPaterno;
	this.apellidoMaterno=apellidoMaterno;
	this.fechaNacimiento=fechaNacimiento;
	this.codigoRFC = codigoRFC;
	this.telefono = telefono;
	this.email = email;
	this.codigoCURP = codigoCURP;
	this.sexo = sexo;
	this.idToDireccion = idToDireccion;
	this.idDomicilio = idDomicilio;
	
	this.nombreCalle=nombreCalle;
	this.numeroExterior=numeroExterior;
	this.numeroInterior=numeroInterior;
	this.entreCalles=entreCalles;
	this.idColonia=idColonia;
	this.idColoniaString=idColoniaString;
	this.nombreColonia=nombreColonia;
	this.nombreDelegacion=nombreDelegacion;
	this.idMunicipio=idMunicipio;
	this.idMunicipioString=idMunicipioString;
	this.idEstado=idEstado;
	this.idEstadoString=idEstadoString;
	this.codigoPostal=codigoPostal;
	this.descripcionEstado=descripcionEstado;
	this.direccionCompleta=direccionCompleta;

	if (claveTipoPersona == 1) {
		this.nombreCompleto = this.nombre + " " + this.apellidoPaterno + " " + this.apellidoMaterno;
	} else {
		this.nombreCompleto = this.nombre;
	}
}

var GrupoClienteJsBean=function(idGrupo,
		nombreGrupo){
	this.idGrupo = idGrupo;
	this.nombreGrupo = nombreGrupo;
}

var VentanaClientes = function(){
	this.contenedorVentanas = obtenerContenedorVentanas();
	this.ventana = null;
	
	this.personaResultado = null;
	this.grupoClienteResultado = null;
	//Indica el tipo de comnsulta a realizar cliente o grupo
	this.tipoBusqueda =null;
	this.funcionPosteriorConsulta = null;
	
	this.mostrarVentanaClientes = function(tipoConsulta){
		this.tipoBusqueda = tipoConsulta;
		this.ventana = this.contenedorVentanas.createWindow("consultaClientes", 400, 320, 700, 500);
		if(this.tipoBusqueda == CONSULTA_DE_CLIENTE){
			this.ventana.setText("Consulta de clientes.");
		}
		else if (this.tipoBusqueda == 2){
			this.ventana.setText("Buscar grupos de Clientes.");
		}
		else{
			alert("Parametro no valido: tipoConsulta="+tipoConsulta);
			return;
		}
		this.ventana.center();
		this.ventana.setModal(true);
		this.ventana.attachURL("/MidasWeb/busquedaCliente/mostrarPantallaConsulta.action?tipoBusqueda="+tipoConsulta);
		this.ventana.button("minmax1").hide();
	}

	this.setGrupoClienteResultado = function(idGrupo,nombreGrupo){
		this.grupoClienteResultado = new GrupoClienteJsBean(idGrupo,nombreGrupo);
	}
	
	this.setPersonaResultado = function(
			idCliente,
			idToPersona,
			claveTipoPersona,
			nombre,
			apellidoPaterno,
			apellidoMaterno,
			fechaNacimiento,
			codigoRFC,
			telefono,
			email,
			codigoCURP,
			sexo,
			idToDireccion,
			idDomicilio,
			
			nombreCalle,
			numeroExterior,
			numeroInterior,
			entreCalles,
			idColonia,
			idColoniaString,
			nombreColonia,
			nombreDelegacion,
			idMunicipio,
			idMunicipioString,
			idEstado,
			idEstadoString,
			codigoPostal,
			descripcionEstado,
			direccionCompleta){
		
		this.personaResultado = new ClienteJsBean(
				idCliente,
				idToPersona,
				claveTipoPersona,
				nombre,
				apellidoPaterno,
				apellidoMaterno,
				fechaNacimiento,
				codigoRFC,
				telefono,
				email,
				codigoCURP,
				sexo,
				idToDireccion,
				idDomicilio,
				
				nombreCalle,
				numeroExterior,
				numeroInterior,
				entreCalles,
				idColonia,
				idColoniaString,
				nombreColonia,
				nombreDelegacion,
				idMunicipio,
				idMunicipioString,
				idEstado,
				idEstadoString,
				codigoPostal,
				descripcionEstado,
				direccionCompleta);
	}
	
	this.setFuncionPosteriorConsulta = function(funcion){
		this.funcionPosteriorConsulta = funcion;
	}
	this.finalizaConsulta = function(funcion){
//		var CONSULTA_DE_CLIENTE = 1;
//		var CONSULTA_DE_GRUPO_CLIENTE = 2;
		if(this.tipoBusqueda == CONSULTA_DE_CLIENTE && this.funcionPosteriorConsulta != null){
			this.funcionPosteriorConsulta(this.personaResultado);
		}
		else if(this.tipoBusqueda == CONSULTA_DE_GRUPO_CLIENTE && this.funcionPosteriorConsulta != null){
			this.funcionPosteriorConsulta(this.grupoClienteResultado);
		}
		this.ventana.setModal(false);
		this.ventana.close();
	}
}
var ventanClienteModal=null;
/*
 * Funciones para acceder a la funcionalidad
 */
function buscarCliente(funcionPosteriorConsulta) {
	var tipoRegreso = jQuery("#tipoRegreso").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var idToNegocio =  jQuery("#idToNegocio").val();
	var divCarga = 'contenido';
	var pathRegreso = "";
	if(tipoRegreso != null && tipoRegreso != undefined){
		if(idToCotizacion != null && idToCotizacion != undefined){
			pathRegreso = "&tipoRegreso=" + tipoRegreso + "&idToCotizacion="+idToCotizacion;
		}
		if(idToNegocio != null && idToNegocio != undefined){
			//FIXME Eliminar esta variable y sustituir por la de negocio
			pathRegreso = pathRegreso + "&idNegocio="+idToNegocio;
		}
	}
	//var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?idField=idClienteResponsable&tipoBusqueda=1&divCarga=windowContent&tipoAccion=consulta"+pathRegreso;
	var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?idField=idClienteResponsable&tipoBusqueda=1&divCarga="+divCarga+"&tipoAccion=consulta"+pathRegreso;
	//parent.mostrarVentanaModal("clienteModal", "Consulta de clientes", 400, 320, 950, 450, url);	
	sendRequestWindow(null, url, obtenerVentanaClienteXXX);
}

function buscarClienteNoAgregar(funcionPosteriorConsulta) {
	var tipoRegreso = jQuery("#tipoRegreso").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var idToNegocio =  jQuery("#idToNegocio").val();
	var pathRegreso = "";
	if(tipoRegreso != null && tipoRegreso != undefined){
		if(idToCotizacion != null && idToCotizacion != undefined){
			pathRegreso = "&tipoRegreso=" + tipoRegreso + "&idToCotizacion="+idToCotizacion;
		}
		if(idToNegocio != null && idToNegocio != undefined){
			//FIXME Eliminar esta variable y sustituir por la de negocio
			pathRegreso = pathRegreso + "&idNegocio="+idToNegocio;
		}
	}
	var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?idField=idClienteResponsable&tipoBusqueda=1&divCarga=contenido&tipoAccion=consulta&ocultarAgregar=1"+pathRegreso;	
	sendRequestWindow(null, url, obtenerVentanaClienteXXX);
}

function obtenerVentanaClienteXXX(){
	var wins = obtenerContenedorVentanas();
	ventanClienteModal= wins.createWindow("clienteModal", 0, 0, 950, 500);
	ventanClienteModal.center();
	ventanClienteModal.setModal(true);
	ventanClienteModal.setText("Consulta de Clientes");
	return ventanClienteModal;
}