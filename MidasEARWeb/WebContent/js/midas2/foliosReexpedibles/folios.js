jQuery(document).ready(function(){
	
	jQuery('#idToNegocio').change(function(){
		var idNegocio = jQuery('#idToNegocio').val();
		if(idNegocio !== null && idNegocio >= 0){			
			listadoService.getMapNegProductoPorNegocio(idNegocio,function(data){
				addOptionSelect('idToNegProducto', data, 'ASC', null);
			});
		}
	});
	
	jQuery('#idToNegProducto').change(function(){
		var idNegProd = jQuery('#idToNegProducto').val();
		if(idNegProd !== null && idNegProd > 0){
			listadoService.getMapNegTipoPolizaPorNegProducto(idNegProd,function(data){
				addOptionSelect('idToTipoPoliza', data, 'ASC', null);
			});
		}
	});
	
	jQuery('#idToTipoPoliza').change(function(){
		var idTpoliza = jQuery('#idToTipoPoliza').val();
		if(idTpoliza !== null && idTpoliza > 0){
			listadoService.getMapMonedaPorNegTipoPoliza(idTpoliza,function(data){
				addOptionSelect('idTcMoneda', data, 'ASC', null);
			});
			
			listadoService.getMapVigencias(function(data){
				addOptionSelect('idTcVigencia', data, 'ASC', null);
			});
		}
	});
	
	
	/*
	 * JQUERY PARA EL FILTRO DE BUSQUEDA  
	 */
	
	
	jQuery('#idToNegocioB').change(function(){
		var idNeg = jQuery('#idToNegocioB').val();		
		if(idNeg !== null && idNeg > 0){
			listadoService.getMapNegProductoPorNegocio(idNeg,function(data){
				addOptionSelect('ToNegProductoB', data, 'ASC', null);
			});
		}
	});
	
	jQuery('#ToNegProductoB').change(function(){
		var idNegProd = jQuery('#ToNegProductoB').val();
		if(idNegProd !== null && idNegProd > 0){
			listadoService.getMapNegTipoPolizaPorNegProducto(idNegProd,function(data){
				addOptionSelect('idToTipoPolizaB', data, 'ASC', null);
			});
		}
	});
	
	jQuery('#idToTipoPolizaB').change(function(){
		var idTpoliza = jQuery('#idToTipoPolizaB').val();
		if(idTpoliza !== null && idTpoliza > 0){
			listadoService.getMapMonedaPorNegTipoPoliza(idTpoliza,function(data){
				addOptionSelect('idTcMonedaB', data, 'ASC', null);
			});
			
			listadoService.getMapVigencias(function(data){
				addOptionSelect('idTcVigenciaB', data, 'ASC', null);
			});
		}
	});
	
	realizarBusqueda(true, false);
});

var listaTarjetaSeguroObligatorioGrid;
function realizarBusqueda(mostrarBusquedaVacia, isFiltro){
	var url = "/MidasWeb/folio/reexpedible/buscar.action?filtroFolio=" + isFiltro + "&";
	var serializedForm = encodeForm(jQuery("#buscarFolioForm"));
	
	if(mostrarBusquedaVacia){
		if(listaTarjetaSeguroObligatorioGrid){listaTarjetaSeguroObligatorioGrid.destructor();}
		listaTarjetaSeguroObligatorioGrid = new dhtmlXGridObject('folioReexpedibleList');
		listaTarjetaSeguroObligatorioGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		listaTarjetaSeguroObligatorioGrid.setSkin('light');
		
		listaTarjetaSeguroObligatorioGrid.setHeader("Tipo Folio, Negocio, Producto, Tipo Póliza, Vigencia, Número Folio, Fecha Vigencia, Agente Venta Libre, Comentarios");
		
		listaTarjetaSeguroObligatorioGrid.setInitWidths("76,228,177,120,66,96,106,134,134");
		
		listaTarjetaSeguroObligatorioGrid.setColAlign("center,center,center,center,center,center,center,center,center");
		
		listaTarjetaSeguroObligatorioGrid.setColSorting("server,server,server,server,server,server,server,server,server");
		listaTarjetaSeguroObligatorioGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
		listaTarjetaSeguroObligatorioGrid.init();
		listaTarjetaSeguroObligatorioGrid.attachEvent("onBeforePageChanged",function(){
    		if (!this.getRowsNum()) return false;
    		return true;
    	});
		
		listaTarjetaSeguroObligatorioGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		listaTarjetaSeguroObligatorioGrid.setPagingSkin("bricks");	
		listaTarjetaSeguroObligatorioGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		listaTarjetaSeguroObligatorioGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		listaTarjetaSeguroObligatorioGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		
		listaTarjetaSeguroObligatorioGrid .attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });
		
		url += serializedForm;
		
		listaTarjetaSeguroObligatorioGrid.load(url);
	} 
}

function guardarFolio(){
	
	var idNegocio = document.getElementById('idToNegocio').value;
	var idNegProd = document.getElementById('idToNegProducto').value;
	var idNegTipoPol = document.getElementById('idToTipoPoliza').value;
	var idMoneda = document.getElementById('idTcMoneda').value;
	var idVigencia = document.getElementById('idTcVigencia').value;
	var fechaVigencia = document.getElementById('fechaValidesHasta').value;
	var numeroFolios = document.getElementById('numeroFolios').value;
	var agenteFacul = document.getElementById('idAgenteFacultativo').value;
	var comentarios = document.getElementById('comentarios').value;
	var tipoFolio = document.getElementById('idtiposFolio').value;
	
	var toFolio = {'toFolio.negocio' : idNegocio, 'toFolio.negprod': idNegProd, 'toFolio.negTpoliza' : idNegTipoPol,
		'toFolio.idTipoMoneda' : idMoneda, 'toFolio.vigencia' : idVigencia, 'toFolio.fechaValidesHasta' : fechaVigencia, 'numeroFolios' :numeroFolios,
		'toFolio.agenteFacultativo' : agenteFacul, 'toFolio.comentarios' : comentarios, 'toFolio.tipoFolio.idTipoFolio': tipoFolio};
	
	
	jQuery.ajax({
		url: '/MidasWeb/folio/reexpedible/guardar.action',
		type : "POST",
		data : toFolio,
		dataType: 'json',
		success : function(json){
			if(manejoErroresFolios(json)){
				limpiarBusqueda();
				limpiarFolioNuevo();
				realizarBusqueda(true ,false);
				mostrarFormularios(false);
			}
		}
	});
}

function vincularFolios(){
	var folioInicio = document.getElementById('folioInicioB').value;
	var folioFin = document.getElementById('folioFinB').value;
	
	filtro = {'filtroFolioRe.folioInicio': folioInicio, 'filtroFolioRe.folioFin' : folioFin};
	
	jQuery.ajax({
		url : '/MidasWeb/folio/reexpedible/vincularFolios.action',
		type : 'POST',
		data : filtro,
		dataType : 'json',
		success : function(json){
			manejoMensajesFoliosMapa(json);
			limpiarVinculacion();
		}
	});
}

function manejoMensajesFoliosMapa(json){
	var stringData = json.stringMapJson;
	var mensajes = stringData.split("#");
	var mensajesExito = mensajes[0].split("|");
	var rowsExito = '<thead> <tr class="successblock"> <th> FOLIOS VINCULADOS </th> </tr> </thead><tbody>';
	var rowsFallido = '<thead><tr class="errorblock"> <th> FOLIOS FALLIDOS </th> </tr> </thead> <tbody>';
	for(var x = 0; x < mensajesExito.length; x++){
		rowsExito += '<tr> <td> ' + mensajesExito[x] + ' </td> </tr>';
	}
	
	var mensajesFallido = mensajes[1].split("|");
	
	for(var x = 0; x < mensajesFallido.length; x++){
		rowsFallido += '<tr> <td > ' + mensajesFallido[x] + ' </td> </tr>';
	}
	rowsFallido+='</tbody>';
	rowsExito += '</tbody>';
	jQuery("#folioExito").html(rowsExito);
	jQuery("#folioError").html(rowsFallido);
	document.getElementById('tablaInfo').style.display = 'block';
	paginarElementosInfo('folioExito');
	paginarElementosInfo('folioError');
}

function manejoErroresFolios(json){
	var mensaje = json.mensajeFolio;
	var mensajeFinal = mensaje.split('|');
	var valorBandera = mensajeFinal[1];
	var bandera = false;
	if(valorBandera == "true"){
		document.getElementById('mensajes').style.display = 'block';
		document.getElementById('textMessage').className = 'successblock';
		jQuery("#textMessage").html(mensajeFinal[0]);
		bandera = true;
	}
	else {
		document.getElementById('mensajesAlta').style.display = 'block';
		document.getElementById('textMessageAlta').className = 'errorblock';
		jQuery("#textMessageAlta").html(mensajeFinal[0]);
		bandera = false;
	}
	setTimeout(closeMessageDiv, 3500);
	return bandera;
}

function closeMessageDiv(){
	document.getElementById('mensajes').style.display = 'none';
	document.getElementById('textMessage').className = '';
	jQuery("#textMessage").html('');
	document.getElementById('mensajesAlta').style.display = 'none';
	document.getElementById('textMessageAlta').className = '';
	jQuery("#textMessageAlta").html('');
}

function limpiarFolioNuevo(){
	document.getElementById('idToNegocio').value = -1;
	document.getElementById('idToNegProducto').value = -1;
	document.getElementById('idToTipoPoliza').value = -1;
	document.getElementById('idTcMoneda').value = -1;
	document.getElementById('idTcVigencia').value = -1;
	document.getElementById('fechaValidesHasta').value = '';
	document.getElementById('numeroFolios').value = '';
	document.getElementById('idAgenteFacultativo').value = -1;
	document.getElementById('comentarios').value = '';
	document.getElementById('idtiposFolio').value = -1;
}

function limpiarBusqueda(){
	document.getElementById('idtiposFolioB').value = -1;
	document.getElementById('folioInicioB').value = '';
	document.getElementById('folioFinB').value = '';
	document.getElementById('idToNegocioB').value = -1;
	document.getElementById('ToNegProductoB').value = -1;
	document.getElementById('idToTipoPolizaB').value = -1;
	document.getElementById('idTcVigenciaB').value = -1;
	document.getElementById('idTcMonedaB').value = -1;
	document.getElementById('fechaValidezHastaB').value = '';
}

function limpiarVinculacion(){
	document.getElementById('folioInicioB').value = '';
	document.getElementById('folioFinB').value = '';
}

function addOptionSelect(obj, data, order, base){
	jQuery("#"+obj).empty();
	var opt = "";
	opt+="<option value='-1'>SELECCIONE ...</option>";
	var datos;
	if(order=="ASC"){
		datos = Object.keys(data).sort();
	}else{
		datos = Object.keys(data).sort().reverse();
	}

    var lista = [];
	jQuery.each(data, function(index, val) {
		lista.push(val);
	});
	order=="ASC"?lista.sort():lista.sort().reverse();
	var selected = "";
	var selectedInput = false;	 
	if(sizeMap(datos) >= 1){
		jQuery.each(lista, function(index, valor) {
			for (var i in datos) {
				for (var e in data) {
					if(datos[i]>0){
						if(valor == data[datos[i]]){
							if(selected==""){
								selected = datos[i];
							}
							opt+="<option value='"+datos[i]+"'>"+data[datos[i]]+"</option>";
							break;
						}
					}
				}
			}
		});
		jQuery("#"+obj).html(opt);
	}else {
		restartCombo(obj, "", "No se encontraron registros ...", "id", "value");						
	}
}

function paginarElementosInfo(tabla){
	var currentPage = 0;
    var numPerPage = 10;
    var $table = jQuery('#' + tabla);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
	if(numRows > 1){
		numRows = numRows - 1;
	}
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = jQuery('<div style="width: 100%; clear: both;"></div>');
	var $lineShort = jQuery('<div class="dhx_pbox_light"><span>&nbsp;</span> </div>');
	var $spacePage = jQuery('<div style="width: auto;" class="dhx_pline_light"> </div>');
	var $divContentPager = jQuery('<div style="clear: both;"> </div>');
    for (var page = 0; page < numPages; page++) {
        jQuery('<div class="dhx_page_light"> <div style="width: 15px; background-color: #ffffff; " ></div></div>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            jQuery(this).addClass('dhx_page_active_light').siblings().removeClass('dhx_page_active_light');
        }).appendTo($divContentPager);
    }
	$spacePage.html($divContentPager).find('div.dhx_page_light:first').addClass('dhx_page_active_light');
	$lineShort.html($spacePage);
	$pager.html($lineShort);
    jQuery('#pagingArea'+tabla).html($pager);
}

function mostrarFormularios(isAlta){
	if(isAlta){
		jQuery("#contenedorFiltros").css('display', 'none');
		jQuery('#folioReexpedibleList').css('display', 'none');
		jQuery('#pagingArea').css('display', 'none');
		jQuery('#infoArea').css('display', 'none');
		
		jQuery('#contenedorAlta').css('display', 'block');
	} else {
		jQuery("#contenedorFiltros").css('display', 'block');
		jQuery('#folioReexpedibleList').css('display', 'block');
		jQuery('#pagingArea').css('display', 'block');
		jQuery('#infoArea').css('display', 'block');
		
		jQuery('#contenedorAlta').css('display', 'none');
	}
}