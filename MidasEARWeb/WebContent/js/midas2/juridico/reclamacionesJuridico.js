function inicializarReclamacionOficio(){
	configurarSiExisteNumeroPolizaOnumeroSiniestro();
	configurarModoConsulta();
	configurarBotonNotificacionReserva();
	configurarValorPredeterminadoEstatusNuevaReclamacion();
}


function configurarModoConsulta(){
	var esConsulta = jQuery("#esConsulta").val();
	var estatus = jQuery("#estatus_s").val();
	if(esConsulta == 'true' || estatus == 'C'){
		jQuery(".esConsulta").attr("disabled", true);
		jQuery("#deshabilitarBusquedaSiniestro").val(true);
	}else{
		jQuery(".noEsConsulta").show();
	}
}


function configurarSiExisteNumeroPolizaOnumeroSiniestro(){
	var numeroPoliza = jQuery("#numeroPoliza").val();
	var numeroSiniestro = jQuery("#numeroSiniestro").val();
	var numeroReclamacion = jQuery("#numeroReclamacion").val();
	if(numeroPoliza
			|| numeroSiniestro){
		if(numeroReclamacion){
			jQuery("#deshabilitarBusquedaSiniestro").val(true);
			jQuery("#ramo_s").attr("disabled", true);
			jQuery("#numeroPoliza_t").attr("disabled", true);
			jQuery("#numeroSiniestro_t").attr("disabled", true);
			jQuery("#oficinaSiniestro_s").attr("disabled", true);
		}
	}
}


function configurarBotonNotificacionReserva(){
	var estatusNotificacionReserva = jQuery("#estatusNotificacionReserva").val();
	var ESTATUS_RESERVA_GUARDADA = 2;
	if(estatusNotificacionReserva == 2){
		jQuery(".mostrarEnEdicionDeReserva").show();
	}
}


function configurarValorPredeterminadoEstatusNuevaReclamacion(){
	var esPantallaAgregarOficio = jQuery("#esPantallaAgregarOficio").val();
	var oficioEstatus = jQuery("#oficioEstatus").val();
	var EN_PROCESO = 'P';
	var CONCLUIDO = 'C';
	if(esPantallaAgregarOficio 
			&& oficioEstatus != CONCLUIDO){
		jQuery("#estatus_s").val(EN_PROCESO);
		jQuery("#oficioEstatus").val(EN_PROCESO);
	}
}


function mostrarListadoReclamaciones(){
	var esConsulta = jQuery("#esConsulta").val();
	if(esConsulta == 'true'){
		var url= mostrarListadoReclamacionesPath;
		sendRequestJQ(null, url, targetWorkArea, 'buscarReclamaciones(true);');
	}else{
		if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){
			var url= mostrarListadoReclamacionesPath;
			sendRequestJQ(null, url, targetWorkArea, 'buscarReclamaciones(true);');
		}
	}
}


function mostrarEditarOficio(numeroReclamacion, numeroOficio, esConsulta){
	var ESTATUS_RESERVA_INICIAL = 0;
	removeCurrencyFormatOnTxtInput();
	jQuery("#esConsulta").val(esConsulta);
	jQuery("#numeroReclamacion").val(numeroReclamacion); 
	jQuery("#numeroOficio").val(numeroOficio);
	jQuery("#estatusNotificacionReserva").val(ESTATUS_RESERVA_INICIAL);
	var formParams = jQuery(document.reclamacionOficioForm).serialize();
	var url = mostrarEditarOficioPath + "?" + formParams;
	sendRequestJQ(null, url, targetWorkArea, "buscarOficios(false);buscarReservas(false);initCurrencyFormatOnTxtInput();validarNumeroPoliza();");
}

function consultarReclamacion(numeroReclamacion){
	mostrarEditarOficio(numeroReclamacion, null, true);
}

function editarReclamacion(numeroReclamacion){
	mostrarEditarOficio(numeroReclamacion, null, false);
}

function consultarOficio(numeroReclamacion, numeroOficio){
	mostrarEditarOficio(numeroReclamacion, numeroOficio, true);
}

function editarOficio(numeroReclamacion, numeroOficio){
	mostrarEditarOficio(numeroReclamacion, numeroOficio, false);
}


function guardarOficio(){
	var existePoliza = jQuery("#existePoliza").val();
	var ramo = jQuery("#ramo").val();
	var RAMO_AUTO = 'A';
	if(validaMaximoMontoReclamacion()){
		if(validateAll(true)){
			if(validaEstatusEstaSeleccionado()){
				if((ramo != RAMO_AUTO) 
						|| (ramo == RAMO_AUTO && existePoliza == 'true')){
					removeCurrencyFormatOnTxtInput();
					var formParams = jQuery(document.reclamacionOficioForm).serialize();
					var reservasParams = obtenerReservasParams();
					var url = guardarReclamacionOficioPath + "?" + formParams + reservasParams;
					sendRequestJQ(null, url, targetWorkArea, 'buscarOficios(false);buscarReservas(false);initCurrencyFormatOnTxtInput();');
				}else{
					mostrarVentanaMensaje("20","El N\u00FAmero de P\u00F3liza ingresado no existe. Para el Ramo 'Autos' este campo es Obligatorio",null);
				}
			}else{
				mostrarVentanaMensaje("20","El campo Estatus es obligatorio para poder guardar el oficio",null);
			}
		}
	}
}


function validaMaximoMontoReclamacion(){
	montoReclamacion = jQuery("#montoReclamado_t").val();
	console.log(montoReclamacion);
	console.log(jQuery("#montoReclamado_t").toNumber().val());
	if(!montoReclamacion || jQuery("#montoReclamado_t").toNumber().val() <= 99999999.99){
		jQuery("#montoReclamado_t").removeClass("warningField"); 
		return true;
	}
	jQuery("#montoReclamado_t").addClass("warningField"); 
	mostrarVentanaMensaje("20","El monto reclamado no puede ser mayor a $99,999,999.99",null);
	return false;
}


function validaCantidadEnterosMontoReclamado(campoTexto, cantidadEnteros){
	var enteroYdecimales = campoTexto.value.split(".");
	var enteros = enteroYdecimales[0];
	var cantidad = enteros.length;
	if(cantidad > cantidadEnteros){
		if(enteroYdecimales.length == 1){
			var nuevosEnteros = enteros.substring(0,cantidadEnteros);
		}else{
		}
	}
	return true;
}


function establecerNuevoValorMontoReclamado(enteros, decimales){
	if(enteros){
		jQuery("#montoReclamado_t").val(enteros.substring(0,cantidadEnteros));
	}else if(enteros && decimales){
		jQuery("#montoReclamado_t").val(enteros.substring(0,cantidadEnteros) + "." + enteroYdecimales[1]);
	}else{
		jQuery("#montoReclamado_t").val(0);
	}
}


function borrarMontoReclamado(){
	jQuery("#montoReclamado_t").val(0);
}


function validaEstatusEstaSeleccionado(){
	var estatus = jQuery("#estatus_s").val();
	if(estatus){
		return true;
	}
	return false;
}


function obtenerReservasParams(){
    var siniestroId = jQuery("#siniestroId").val();
	var reservas = "";
	var contador = 0; 
      
	if(siniestroId){
		listadoReclamacionReservasJuridicoGrid.forEachRow(function(id){
			var reservaMontoAutoridad = listadoReclamacionReservasJuridicoGrid.cells(id,2).getValue();
			if(!reservaMontoAutoridad){reservaMontoAutoridad = 0;}
			reservas = reservas.concat("&reservas[" + contador + "].claveSubCobertura=" + listadoReclamacionReservasJuridicoGrid.getUserData(id,"claveSubCoberturaCell")
					+ "&reservas[" + contador + "].coberturaId=" + listadoReclamacionReservasJuridicoGrid.getUserData(id,"coberturaIdCell")
					+ "&reservas[" + contador + "].oficio=" + listadoReclamacionReservasJuridicoGrid.getUserData(id,"oficioCell")
					+ "&reservas[" + contador + "].reclamacionId=" + listadoReclamacionReservasJuridicoGrid.getUserData(id,"reclamacionIdCell")
					+ "&reservas[" + contador + "].reservaMontoAutoridad=" + reservaMontoAutoridad);
			contador ++;
		});
	}
	return reservas;
}


var listadoReclamacionJuridicoGrid;
function buscarReclamaciones(mostrarListadoVacio){
	if(validarBusquedaReclamaciones()){
		jQuery("#tituloListado").show();
		jQuery("#reclamacionesGridContainer").show();	
		jQuery("#listadoReclamacionJuridicoGrid").empty();
		jQuery("#mostrarListadoVacio").val(mostrarListadoVacio);
	
		listadoReclamacionJuridicoGrid = new dhtmlXGridObject('listadoReclamacionJuridicoGrid');
		listadoReclamacionJuridicoGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorReclamaciones");});
		listadoReclamacionJuridicoGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorReclamaciones");});
		
		formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url = listarReclamacionesPath + '?' +  formParams;
		listadoReclamacionJuridicoGrid.load( url );
	}
}

function validarBusquedaReclamaciones(){
	return true;
}


var listadoReclamacionReservasJuridicoGrid;
function buscarReservas(mostrarListadoVacio){
	if(validarCargarReservasGridPorTipoReclamacion()){
		removeCurrencyFormatOnTxtInput();
		jQuery(".listadoReservas").show();
		jQuery("#listadoReclamacionReservasJuridicoGrid").empty(); 
		jQuery("#mostrarListadoVacio").val(mostrarListadoVacio);
		
		listadoReclamacionReservasJuridicoGrid = new dhtmlXGridObject('listadoReclamacionReservasJuridicoGrid');
		listadoReclamacionReservasJuridicoGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorReservas");});
		listadoReclamacionReservasJuridicoGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorReservas");});
		
		listadoReclamacionReservasJuridicoGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
			var MONTO_AUTORIDAD = 2;
			var ESTATUS_RESERVA_EDITADO = 1
			if(cInd == MONTO_AUTORIDAD ){
				if( stage == 2 && !(nValue === oValue) ) {
					jQuery("#estatusNotificacionReserva").val(ESTATUS_RESERVA_EDITADO);
				}
			}
			return true;
		});	
		
		formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url = listarReclamacionReservasPath + '?' +  formParams;
		console.log("URL buscar RESERVAS: " + url);
		listadoReclamacionReservasJuridicoGrid.load( url );
	}
}

function validarCargarReservasGridPorTipoReclamacion(){
	var LITIGIO = 'LI';
	var CONDUSEF = 'CO';
	var tipoReclamacion = jQuery("#tipoReclamacion").val();
	if(tipoReclamacion == LITIGIO 
			|| tipoReclamacion == CONDUSEF){
		return true
	}
	return false;
}


var listadoReclamacionOficiosJuridicoGrid;
function buscarOficios(mostrarListadoVacio){
	if(validarBusquedaOficios()){
		removeCurrencyFormatOnTxtInput();
		jQuery(".listadoOficios").show();	
		jQuery("#listadoReclamacionOficiosJuridicoGrid").empty();
		jQuery("#mostrarListadoVacio").val(mostrarListadoVacio);
	
		listadoReclamacionOficiosJuridicoGrid = new dhtmlXGridObject('listadoReclamacionOficiosJuridicoGrid');
		listadoReclamacionOficiosJuridicoGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorOficios");});
		listadoReclamacionOficiosJuridicoGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorOficios");});
		
		formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url = listarOficiosPath + '?' +  formParams;
		console.log("URL buscar OFICIOS: " + url);
		listadoReclamacionOficiosJuridicoGrid.load( url );
	}
}

function validarBusquedaOficios(){
	return true;
}


function onChangeEstado(target){
	var idEstado = jQuery("#estado_s").val();
	if(idEstado != null  && idEstado != headerValue){		
		dwr.engine.beginBatch();
		listadoService.getMapMunicipiosPorEstadoMidas(idEstado,
				function(data){
					addOptions(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(target,"");
	}
}


function exportarExcel(){
	var formParams = jQuery(document.reclamacionOficioForm).serialize();
	var url= exportarExcelPath + "?" + formParams;
		window.open(url, "Reclamaciones");
}

function exportarListadoOficios(){
	var numeroReclamacion = jQuery("#numeroReclamacion").val()
	if(numeroReclamacion){
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url= exportarListadoOficiosPath + "?" + formParams;
		console.log(url);
			window.open(url, "Oficios");
		initCurrencyFormatOnTxtInput();
	}else{
		mostrarVentanaMensaje("20","Debe guardar la reclamacion para poder imprimir",null);
	}
}


function onChangeParteEnJuicio(){
	var parteEnJuicio = jQuery("#parteEnJuicio_s").val();
	if(parteEnJuicio == 'C'){
		//FUNCIONALIDAD DE LA FECHA DE NOTIFICACION
		jQuery("#fechaNotificacion").hide();
		jQuery("#fechaNotificacion_contra").show();
		jQuery("#fechaNotificacion_promovida").hide();
		//FUNCIONALIDAD DEL CAMPO ACTOR
		jQuery("#actor_t").attr('readonly',false);
	}else if(parteEnJuicio == 'P'){
		//FUNCIONALIDAD DE LA FECHA DE NOTIFICACION
		jQuery("#fechaNotificacion").hide();
		jQuery("#fechaNotificacion_contra").hide();
		jQuery("#fechaNotificacion_promovida").show();
		//FUNCIONALIDAD DEL CAMPO ACTOR
		jQuery("#actor_t").attr('readonly',true);
		jQuery("#actor_t").val(jQuery("#actorPromovida").val());
	}else{
		//FUNCIONALIDAD DE LA FECHA DE NOTIFICACION
		jQuery("#fechaNotificacion").show();
		jQuery("#fechaNotificacion_contra").hide();
		jQuery("#fechaNotificacion_promovida").hide();
		//FUNCIONALIDAD DEL CAMPO ACTOR
		jQuery("#actor_t").attr('readonly',false);
	}
}


function onChangeRamo(){
	var ramo = jQuery("#ramo_s").val();
	jQuery("#ramo").val(jQuery("#ramo_s").val());
	if(ramo){
		jQuery('.ramoSelecionado').attr('readonly',false);
	}else{
		jQuery('.ramoSelecionado').attr('readonly',true);
		jQuery('.ramoSelecionado').val('');
	}
	jQuery("#oficinaId").val("");
	jQuery("#oficinaSiniestro_s").val("");
	jQuery("#numeroPoliza_t").val("");
	jQuery("#numeroPoliza").val("");
	jQuery("#numeroSiniestro_t").val("");
	jQuery("#numeroSiniestro").val("");
	jQuery("#siniestroId").val("");
}


function onChangeOficinaSiniestro(){
	jQuery("#oficinaId").val(jQuery("#oficinaSiniestro_s").val());
}

function onChangeEstatus(){
	jQuery("#oficioEstatus").val(jQuery("#estatus_s").val());
}


function onBlurNumeroPoliza(){
	jQuery("#numeroPoliza").val(jQuery("#numeroPoliza_t").val());
}


function onBlurNumeroSiniestro(){
	jQuery("#numeroSiniestro").val(jQuery("#numeroSiniestro_t").val());
}


function mostrarBuscarSiniestro(){
	var ramo = jQuery("#ramo_s").val();
	var deshabilitarBusquedaSiniestro = jQuery("#deshabilitarBusquedaSiniestro").val();
	var esConsulta = jQuery("#esConsulta").val();
	if(deshabilitarBusquedaSiniestro != 'true'){
		if(ramo == 'A'){
			buscarReporte();
		}else{
			mostrarVentanaMensaje("20","La b\u00FAsqueda de siniestro solo se puede realizar para el ramo Autos",null);
		}
	}else{
		if(esConsulta == 'true'){
			mostrarVentanaMensaje("20","La b\u00FAsqueda de siniestro no se puede realizar en modo consulta",null);
		}else{
			mostrarVentanaMensaje("20","Ya no se puede modificar el n\u00FAmero de siniestro",null);
		}
	}
}


function validarNumeroPoliza(){
	var ramo = jQuery("#ramo_s").val();
	var numeroPoliza = jQuery("#numeroPoliza_t").val();
	if(ramo == 'A' && numeroPoliza){
		jQuery.ajax({
            url: '/MidasWeb/siniestros/juridico/validarPoliza.action?reclamacionOficio.numeroPoliza='+numeroPoliza,
            dataType: 'json',
            async:true,
            type:"POST",
            data: null,
            success: function(json){
            	 var result = json.existePoliza;
            	 jQuery("#existePoliza").val(result);
            	 if(!result){
            		 mostrarVentanaMensaje("20","El n\u00FAmero de p\u00F3liza ingresado no existe",null);
            	 }
            }
      });
	}else{
		jQuery("#existePoliza").val(true);
	}
}


function bloquearAsegurado(){
	var numeroPoliza = jQuery("#numeroPoliza_t").val();
	if(numeroPoliza){
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url = notificarBloqueoAseguradoPath + "?" + formParams;
		sendRequestJQ(null, url, targetWorkArea, 'buscarOficios(false);buscarReservas(false);validarNumeroPoliza();');
		initCurrencyFormatOnTxtInput();
	}else{
		mostrarVentanaMensaje("20","El n\u00FAmero de  p\u00F3liza es requerido para bloquear al asegurado",null);
	}
}


function notificarModificacionReservas(){
	var siniestroId = jQuery("#siniestroId").val();
	if(siniestroId){
		removeCurrencyFormatOnTxtInput();
		var formParams = jQuery(document.reclamacionOficioForm).serialize();
		var url = notificarModificacionReservaPath + "?" + formParams;
		sendRequestJQ(null, url, targetWorkArea, 'buscarOficios(false);buscarReservas(false);validarNumeroPoliza();');
		initCurrencyFormatOnTxtInput();
	}else{
		mostrarVentanaMensaje("20","La reclamaci\u00F3n no tiene un siniestro valido con reservas",null);
	}
}


function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}

//-------------------------------------------
//VENTANA DE SELECCION DE TIPO DE RECLAMACION
//-------------------------------------------


function mostrarVentanaTipoReclamacion(){
	var url = mostrarVentanaTipoReclamacionPath;
	mostrarVentanaModal("vm_tipoReclamacion", 'Seleccione el Tipo de Reclamaci\u00F3n',  1, 1, 400, 210, url, null);
}


function crearNuevaReclamacion(){
	if(validarSeleccionTipoReclamacion()){
		var formParams = jQuery(document.tipoReclamacionForm).serialize();
		var url = mostrarAgregarOficioPath + '?' + formParams + '&esPantallaAgregarOficio=true';
		parent.document.getElementById('urlCrearNuevoOficio').value = url;
		parent.cerrarVentanaModal("vm_tipoReclamacion", "mostrarAgregarOficioCallback();");
		
	}else{
		jQuery("#validacionSeleccionTipo").show();
	}
}


function mostrarAgregarOficioCallback(){
	var url = jQuery("#urlCrearNuevoOficio").val();
	sendRequestJQ(null, url, targetWorkArea, "buscarOficios(true);buscarReservas(true);initCurrencyFormatOnTxtInput();");
}


function validarSeleccionTipoReclamacion(){
	var tipoReclamacion = jQuery("#tipoReclamacion_s").val();
	if(tipoReclamacion){
		return true;
	}
	return false;
}


function limpiarFiltros(){
	jQuery('#reclamacionOficioForm').each (function(){
		  this.reset();
	});
}

//---------------------
//BUSQUEDA DE SINIESTRO
//---------------------

var buscarPolizaJuridicoGrid;
var action = "/MidasWeb/siniestros/juridico/buscarPolizaJuridicoGrid.action";
var serializedForm;

function buscarReporte(){
	
	var url = "/MidasWeb/siniestros/juridico/mostrarContenedorBusquedasPolizaJuridico.action";
	mostrarVentanaModal("vm_tipoServicio", "Buscar Reporte", null, null, 900, 670, url, "cargarReservasSiHaySiniestro();");
	
}


function realizarBusqueda(){
	jQuery("#validacionFormatoNumeroSiniestro").hide();
	jQuery("#validacionBusqueda").hide();
	 if ( validarBusqueda() == "s" ){
		 console.log("dentro de if: validarBusqueda");
		 if(validaFormatoSiniestroReclamaciones(jQuery("#numSiniestro"))){
			 console.log("dentro de if: validaFormatoSiniestroReclamaciones");
			 cargaGridBusquedaPoliza("s");
		 }
	 }else{
		 jQuery("#validacionBusqueda").show();
	 }
}


function validarBusqueda(){
	
	var bandera = "n";
	jQuery(".cleaneable").each( function(){
		if( jQuery(this).val() != "" ){
			bandera = "s";
		}		
	});
	return bandera;
	
}


function cargaGridBusquedaPoliza(aplicaBusqueda){
	console.log("ejecutando cargaGridBusquedaPoliza...");
	var siniestroId;
	var numPoliza;
	var numSiniestro;
	var oficinaId;
	document.getElementById("busquedaJuridicoSiniestrosGrid").innerHTML = '';
	buscarPolizaJuridicoGrid = new dhtmlXGridObject("busquedaJuridicoSiniestrosGrid");
	buscarPolizaJuridicoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	buscarPolizaJuridicoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	buscarPolizaJuridicoGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });

	buscarPolizaJuridicoGrid.setHeader   ("No. Siniestro ,Oficina       ,Fecha Ocurrido,N° de Póliza  ,Inciso ");
	buscarPolizaJuridicoGrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
	
	buscarPolizaJuridicoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	buscarPolizaJuridicoGrid.attachEvent("onRowSelect", function(id,ind){

		siniestroId   = buscarPolizaJuridicoGrid.cells(id,6).getValue();
		numPoliza     = buscarPolizaJuridicoGrid.cells(id,3).getValue();
	    numSiniestro  = buscarPolizaJuridicoGrid.cells(id,0).getValue();
		oficinaId     = buscarPolizaJuridicoGrid.cells(id,5).getValue();
		
		parent.document.getElementById('siniestroId').value  		= siniestroId;
		parent.document.getElementById('oficinaId').value	= oficinaId;
		parent.document.getElementById('numeroPoliza').value		= numPoliza;
		parent.document.getElementById('numeroSiniestro').value	= numSiniestro;
		
		parent.document.getElementById('oficinaSiniestro_s').value	= oficinaId;
		parent.document.getElementById('numeroPoliza_t').value		= numPoliza;
		parent.document.getElementById('numeroSiniestro_t').value	= numSiniestro; 
		
		parent.cerrarVentanaModal("vm_tipoServicio",'validarNumeroPoliza();cargarReservasSiHaySiniestro();');
		
	});
	
	
	if( aplicaBusqueda == "s"){
		serializedForm = encodeForm(jQuery("#formReporteSiniestroDTO"));
		buscarPolizaJuridicoGrid.load(action+"?"+serializedForm);
	}else{
		buscarPolizaJuridicoGrid.load(action);
	}	
}

function limpiarFiltrosSiniestro(){
	jQuery('#formReporteSiniestroDTO').each (function(){
		  this.reset();
	});
}

function cargarReservasSiHaySiniestro(){
	var siniestroId = jQuery("#siniestroId").val();
	if(siniestroId){
		buscarReservas(false);
	}
}

function validaFormatoSiniestroReclamaciones(elemento){
	console.log('Entra a validaFormatoSiniestro');
	var val = jQuery(elemento).val();
	var result = true;
	if(!isEmpty(val)){
		console.log('Val = '+val);
		var expreg = new RegExp("^([0-9]|\\w)+[-][0-9]+[-][0-9]+$");
		result = expreg.test(val);
		console.log("Result = " + result);
		if(!result){
			jQuery("#validacionFormatoNumeroSiniestro").show();
			result = false;
		}
	}
	return result;
}