

function guardar(){
	var nombre = jQuery('#nombre').val();
	var estautsId = jQuery('#estautsId').val();
	var oficinaId = jQuery('#oficinaId').val();	
	if(  (null==nombre || nombre=="")   ){
    	mostrarMensajeInformativo('El campo Nombre es obligatorio', '20');
	}else{
		if(  (null==estautsId || estautsId=="")   ){
	    	mostrarMensajeInformativo('El campo Estatus es obligatorio', '20');
		}else{
			if(  (null==oficinaId || oficinaId=="")   ){
		    	mostrarMensajeInformativo('El campo Oficina es obligatorio', '20');
			}else{
				var formParams = jQuery(document.catJuridicoForm).serialize();
				sendRequestJQ(null,'/MidasWeb/juridico/catalogosJuridico/guardarCatalogo.action?'+ formParams,targetWorkArea,null);
			}
		}
	}
}


function validarFecha(){
	var estatus = jQuery('#estautsId').val();
	if(estatus=="0"){
		jQuery("#fechaInactivo").removeAttr("disabled");
	}else{
		jQuery("#fechaInactivo").attr('disabled','disabled');
		jQuery("#fechaInactivo").val("");
	}

}



function nuevo(){
	var tipoCatalogo 		= jQuery("#tipoCatalogo").val();
	sendRequestJQ(null,'/MidasWeb/juridico/catalogosJuridico/mostrar.action?tipoCatalogo='+ tipoCatalogo,targetWorkArea,null);
}

function buscarLista(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('catalogoGrid');	
	listadoGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	listadoGrid.attachEvent("onXLE", function(grid){		
		changeFilter(2);
		unblockPage();
    });	
	/*listadoGrid.attachEvent("onXLE", function(){
	    if (!listadoGrid.getRowsNum())
	    	mostrarMensajeInformativo('No se Encontraron Resultados.', '20');
	})*/
	
	listadoGrid.attachHeader("#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,");

	var formParams = jQuery(document.catJuridicoForm).serialize();
	listadoGrid.load( '/MidasWeb/juridico/catalogosJuridico/listarCatalogos.action?'+ formParams);
}


function editar(tipoCatalogo, idCatalogo){
	var url='/MidasWeb/juridico/catalogosJuridico/mostrar.action?'+ 'idCatalogo=' + idCatalogo+ "&tipoCatalogo=" + tipoCatalogo;
	sendRequestJQ(null,url,targetWorkArea,null);
}

function changeFilter(id){
	listadoGrid.getFilterElement(id)._filter = function(){
    	var input = this.value;
    	return function(value, id){
	        for(var i=0;i<listadoGrid.getColumnsNum();i++){
	        	if(input == '') {
	        		return true;
	        	}else{
		            var val=listadoGrid.cells(id,i).getValue();
		            if (val==input){ 
		                return true;
		            }
	        	}
	        }
	        return false;
    	}
	}
}


