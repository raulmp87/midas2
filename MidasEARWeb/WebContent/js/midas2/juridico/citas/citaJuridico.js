/**
 * 
 */
var citasJuridicoListadoGrid;
var mensajeConfirmCerrarAltaEdicionCita = "Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?";
var mensajeConfirmEliminarCita = "\u00BFEst\u00E1 seguro que desea eliminar el elemento seleccionado? Cita No. : ";

function crearCitaJuridico()
{
	sendRequestJQ(null,pathMostrarCitaJuridico,targetWorkArea,"configurarValorPredeterminadoEstatus();");	
}

function configurarValorPredeterminadoEstatus(){
	var PENDIENTE_POR_ATENDER = 'P';
	jQuery("#estatus").val(PENDIENTE_POR_ATENDER);
}

function guardarCitaJuridico()
{
	if(validateAll(true,'save')){
		var form = jQuery('#citaJuridicoForm').serialize();
		sendRequestJQ(null,pathGuardarCitaJuridico + '?' + form,targetWorkArea,null);
	}		
}

function buscarCitasPaginado(page, nuevoFiltro)
{	
	var posPath = 'posActual=' + page + '&funcionPaginar=' + 'buscarCitasPaginado' 
		+ '&divGridPaginar=' + 'citasJuridicoListadoGrid';
	
	var form = jQuery('#listadoCitasJuridicoForm').serialize();
	
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&' + jQuery(document.paginadoGridForm).serialize();
	}
	mostrarIndicadorCarga('indicador');
	sendRequestJQTarifa (null, pathBuscarCitasJuridicoPaginado + "?"+ posPath + '&' + form, 
			'gridCitasJuridicoPaginado','buscarCitas();');
}

function buscarCitas()
{
	var form = jQuery('#listadoCitasJuridicoForm').serialize();
	
	document.getElementById("citasJuridicoListadoGrid").innerHTML = '';	
	citasJuridicoListadoGrid = new dhtmlXGridObject('citasJuridicoListadoGrid');
		
	citasJuridicoListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	var posPath = 'posActual=' + jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	var url = pathBuscarCitasJuridico + "? " + posPath + '&' + form;
	citasJuridicoListadoGrid.load(url);
}

function limpiarFiltrosBusqueda(divId)
{
	jQuery('#listadoCitasJuridicoForm').each (function(){
		  this.reset();
	});
}

function eliminarCita(idCita)
{
	if(confirm(mensajeConfirmEliminarCita + idCita)) {
		
		var form = jQuery('#listadoCitasJuridicoForm').serialize();
		sendRequestJQ(null,pathEliminarCitaJuridico + "?cita.id=" + idCita + "&" + form,targetWorkArea,null);		
	}	
}

function verCitaJuridico(idCita, soloConsulta)
{	
	sendRequestJQ(null,pathMostrarCitaJuridico + "?cita.id=" + idCita + "&soloLectura=" + soloConsulta,targetWorkArea,null);
}

function regresarListadoCitasJuridico()
{
	var soloLectura = jQuery('#soloLectura').val();
	
	if(soloLectura != "true")
	{
		if(confirm(mensajeConfirmCerrarAltaEdicionCita)) {
			
			sendRequestJQ(null,pathMostrarListadoCitasJuridico,targetWorkArea,null);		
		}
		
	}else
	{
		sendRequestJQ(null,pathMostrarListadoCitasJuridico,targetWorkArea,null);
	}		
}

function exportarExcel(){
		var form = jQuery('#listadoCitasJuridicoForm').serialize();
		var url= pathExportarCitaExcel + "?" + form;
		window.open(url, "Citas Juridico");
}