var gridRecibos;
var listRecibos;

var numPoliza;
var claveAgente;
var numCliente;
var numRecibo;
var myUrl;

function buscarRecibos(){
	numPoliza = jQuery("#txtNoPoliza").val();
	claveAgente = jQuery("#txtClaveAgente").val();
	numCliente = jQuery("#txtNoCliente").val();
	numRecibo = jQuery("#txtNoRecibo").val();
	var numExhibicion = jQuery("#txtNoExhibicion").val();
	var numEndoso = jQuery("#txtNoEndoso").val();
	var numInciso = jQuery("#txtNoInciso").val();
	
	//validar que al menos venga el numPoliza, claveAgente o el numCliente
	if (numPoliza == '' && claveAgente == '' && numCliente == '' && numRecibo == '') {
		//alert("Debe especificar al menos el n\u00FAmero de p\u00F3liza, la clave del agente, el n\u00FAmero de cliente  \u00F3 el n\u00FAmero del recibo");
		mostrarMensajeInformativo("Debe especificar al menos el n\u00FAmero de p\u00F3liza, la clave del agente, el n\u00FAmero de cliente  \u00F3 el n\u00FAmero del recibo","10");
		return;
	}
	
	if (numPoliza != null && numPoliza != '') {
		var expreg = /^\d{3}-?(\d{10}|\d{12})-?\d{2}$/;
	
		if (!expreg.test(numPoliza)) {
			var codeHtml = "<div style='width: 240px;height: 260px;float: right;font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 12px;color: #999999;font-weight: bold;text-align: left;vertical-align: bottom;overflow: auto;margin-top: 10px;'>El formato de la póliza no es correcto<br><br>Formatos correctos:<br>xxx-xxxxxxxxxx-xx<br>Eje: 016-0008126152-01<br><br>xxxxxxxxxxxxxxx<br>Eje: 016000812615201<br><br>xxx-xxxxxxxxxxxx-xx<br>Eje: 016-000812615211-01<br><br>xxxxxxxxxxxxxxxxx<br>Eje: 01600081261521101</div>";
			
			mostrarVentanaModalMode('formatos', 'Error en formatos', 200, 350, 250, 270, 'ATTACH_HTML_STRING', codeHtml, null);
			return;
		}
	}
	blockPage();
	
	myUrl = "/MidasWeb/cobranza/recibos/reexpedicion/listarRecibosFiltrado.action";
	myUrl+= "?filtroRecibo.numeroPoliza="+		numPoliza,
    myUrl+= "&filtroRecibo.idAgente="+ 			claveAgente,
	myUrl+= "&filtroRecibo.numeroCliente="+		numCliente,
    myUrl+= "&filtroRecibo.numeroRecibo="+		numRecibo,
    myUrl+= "&filtroRecibo.numeroEndoso="+		numEndoso,
    myUrl+= "&filtroRecibo.numeroInciso="+		numInciso

	gridRecibos = new dhtmlXGridObject('listadoGridRecibos');
	mostrarIndicadorCarga('indicador');	
	gridRecibos.attachEvent("onXLS", function(grid_obj){blockPage()});
	gridRecibos.attachEvent("onXLE", function(grid_obj){unblockPage()});
	gridRecibos.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });			
	gridRecibos.setImagePath('/MidasWeb/img/dhtmlxgrid/');
	gridRecibos.setSkin('light');
	gridRecibos.setHeader("#master_checkbox,Poliza,Endoso,Exhibición,No Recibo,Clave,Prima Total,Situación,Id Agente,Agente,Id Cliente,Cliente,PDF,XML",
		null, ["text-align:center;","text-align:center;","text-align:center","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;"]);
	gridRecibos.setInitWidths("30,140,60,65,90,45,70,78,63,150,60,150,45,45");
	gridRecibos.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center");
	gridRecibos.setColSorting("server,server,server,server,server,server,server,server,server,server,server,server,server,server");
	gridRecibos.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img");
	gridRecibos.init();
	
	gridRecibos.enablePaging(true,20,5,"pagingArea",true,"infoArea");      
	gridRecibos.setPagingSkin("bricks");
	
	gridRecibos.load(myUrl);
	unblockPage();
}

function showModalReexpedirDatosComplementarios(){
	listRecibos =  cargaIdSeleccionadas();
	
	if (listRecibos == "") {
		mostrarMensajeInformativo('Favor de seleccionar los recibos a reexpedir', "10");
	} else {
		var url = "/MidasWeb/cobranza/recibos/reexpedicion/modalReexpedirDatosComplementarios.action";
			url+= "?"+listRecibos;
		
		blockPage();
		
		//mostrar pantalla para pedir Cuenta y Método de Pago.
		mostrarVentanaModal("modalInfoReexpedirRecibos","Reexpedir Recibos",200,350, 500, 200, url);	
		unblockPage();		
	}


}

var pIdMetodoPago;
var pCuenta;
var pNumPolizaSend = null;

function reexpedirComprobantes(idMetodoPago, cuenta){
	if (idMetodoPago == '') {
		parent.mostrarMensajeInformativo("Favor de seleccionar el método de pago", "10");
		return;
	}
	
	parent.pIdMetodoPago = idMetodoPago;
	parent.pCuenta = cuenta;
	
	parent.mostrarMensajeConfirm("Esta accion regenerara el Recibo con una nueva Llave Fiscal. \u00BFEst\u00E1 seguro que desea regenerar el Recibo?","20","parent.reexpedir()",null,null,null);	
}

function reexpedir(){
	idMetodoPago = parent.pIdMetodoPago;
	cuenta = parent.pCuenta;
	
	if (idMetodoPago == '') {
		parent.mostrarMensajeInformativo("Favor de seleccionar el método de pago", "10");
		return;
	}
	
	var numPolizaSend = null;
	
	if (typeof parent.numPoliza != "undefined") {
		numPolizaSend = parent.numPoliza.replace('-', '');
		numPolizaSend = numPolizaSend.replace('-', '');
		numPolizaSend = numPolizaSend.substring(3,numPolizaSend.length);
	}
		
	//Ajax que ejecuta el servicio
	var urlReexpedir = "/MidasWeb/cobranza/recibos/reexpedicion/reexpedirComprobantes.action?listaRecibos="+parent.listRecibos;
	
	jQuery.ajax({
        url: urlReexpedir,
        type : "GET",
        data : {
        	"idMetodoPago":		idMetodoPago,
        	"numeroCuenta": 	cuenta,
			"filtroRecibo.numeroPoliza":		numPolizaSend, 
			"filtroRecibo.idAgente": 			parent.claveAgente,
			"filtroRecibo.numeroCliente":		parent.numCliente,
			"filtroRecibo.numeroRecibo":		parent.numRecibo
        },
        dataType: 'text/html',
        success : function(json){
				parent.mostrarMensajeInformativo("La solicitud se encuentra en proceso", "30");
				parent.gridRecibos.clearAll(); 
				parent.gridRecibos.load(parent.myUrl);
				
				//cerrar la ventana modal
				parent.cerrarVentanaModal('modalInfoReexpedirRecibos');
				
        },
		error: function(e){
			alert('**Error: '+JSON.stringify(e));
		}
	});
	
}

function cambiarMetodoDePago(idMetodoPago){
	if(idMetodoPago == '01' || idMetodoPago == '98' || idMetodoPago == '99'){
		$("#trNumeroTarjeta").hide();
	} else{
		$("#trNumeroTarjeta").show();
	}
}

function limit18(element)
{
    var max_chars = 4;
    if(element.value.length > max_chars) {
        element.value = element.value.substr(0, max_chars);
    }
}

function cargaIdSeleccionadas(){
	var idRecibos = "";
	var num = 0;
	var idCheck = gridRecibos.getCheckedRows(0);
	
	/*if(idCheck != ""){
		var ids = idCheck.split(",");
		var num = 0;
		for(i = 0; i < ids.length; i++){
	   	 	if(idRecibos != ""){
	   	 		idRecibos+="&";
	   	 	}
			idRecibos += "recibos["+ num+ "].numeroRecibo=" + ids[i];
	   	 	num = num + 1;		
		}
	}
	
	return idRecibos;*/
	
	return idCheck;
}

function descargarComprobante(llaveFiscal, tipoDoc){
	//Ajax que ejecuta el servicio
	myUrl = "/MidasWeb/cobranza/recibos/reexpedicion/descargarComprobante.action?llaveFiscal="+llaveFiscal+"&tipoDocumento="+tipoDoc;
	
	window.open(myUrl);
}