/**   
 * operacionesConsolidadas.js
 **/
var gridListadoLog ;
var gridRecibosConsolidadosLog ;
var sumaRecibosAConsolidarTotal =0;
var recibosAConsolidarCount =0;
var recibosYaConsolidadosCount =0;
var newHtml = '<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Cargando la informaci&oacute;n, espere un momento por favor...</font>';
function mostrarIndicadorCargaNegTarifa(){

	if (document.getElementById('loadingIndicatorComps') !== null){
		if (document.getElementById('loadingIndicatorComps').innerHTML === ''){
			document.getElementById('loadingIndicatorComps').innerHTML = newHtml;
			document.getElementById('loadingIndicatorComps').style.display='block';
		}else
			document.getElementById('loadingIndicatorComps').style.display='block';
	}
}

function ocultarIndicadorCargaNegTarifa(){
if(document.getElementById('loadingIndicatorComps') !== null)
	document.getElementById('loadingIndicatorComps').style.display='none';
}


function resetSumTotal (){
			jQuery('#numeroRecibos').val(0);
			jQuery('#sumaTotal').val(0);
			sumaRecibosAConsolidarTotal =0;
            recibosAConsolidarCount =0;
			recibosYaConsolidadosCount=0
			jQuery("#checkAll").attr('checked', false);
			}


function exportarExcelConsolidado(){
		
		var countConsolidado=  gridRecibosConsolidadosLog ;
			if(countConsolidado==0){
				alert('No hay registros para exportar');
			}else{
				var data=jQuery("#polizaForm").serialize();
				var url='/MidasWeb/cobranza/recibos/consolidado/exportarExcelConsolidado.action?fechaFinEjecucionManual='+jQuery('#fechaCorte').val() + '&numPolizaConsolidar='+jQuery('#numPolizaConsolidar').val()  + '&fechaConsolidado='+jQuery('#fechaConsolidado').val();
				window.open(url, "Excel");
			}
			
}
function exportarExcel(){

	var countRC= gridListadoLog.getRowsNum() ;
		
			if(countRC==0){
				alert('No hay registros para exportar');
			}else{
			
						if (!jQuery('#numPolizaConsolidar').val()){
					   alert("Favor de capturar numero de poliza");
					   return; 
						} 
					  if (!jQuery('#fechaConsolidado').val()){
						alert("Debe seleccionar fecha de consolidado");
							return;	
					}
					  if (!jQuery('#fechaCorte').val()){
							alert("Debe seleccionar fecha de corte");
							return;	
						}
					  if (!jQuery('#centroEmision').val()){
							alert("Debe capturar Centro de Emision");
								return;	
						}
					  if (!jQuery('#numRenovPol').val()){
								alert("Debe capturar Numero de Renovacion de Poliza");
								return;	
						}
					
						
				var data=jQuery("#polizaForm").serialize();
				var url='/MidasWeb/cobranza/recibos/consolidado/exportarExcel.action?fechaFinEjecucionManual='+jQuery('#fechaCorte').val() + '&numPolizaConsolidar='+jQuery('#numPolizaConsolidar').val()  + '&fechaConsolidado='
				+jQuery('#fechaConsolidado').val()	+ '&centroEmision='+jQuery('#centroEmision').val()
				+ '&numRenovPol='+jQuery('#numRenovPol').val()
				;  
				window.open(url, "Excel");
	
	}
}



function consolidarRecibosSeleccionados(fechaConsolidado,fechaFinEjecucionManual, numPolizaConsolidar){

		var idchecked = gridListadoLog.getCheckedRows(1);
		if(idchecked!=""){
		var idRecibosConsolidar="";
		idRecibosConsolidar=sumarRecibosSeleccionados();
			if(idRecibosConsolidar!=""){
				if(jQuery('#idMedioPago_0').val()){
					var idMetodoPago=jQuery('#idMedioPago_0').val();
					if(idMetodoPago==15||idMetodoPago==2){
						jQuery('#numTarjeta').val("0")
					}else{
					if(!(jQuery('#numTarjeta').val().length==16 || jQuery('#numTarjeta').val().length==18)){				
						alert('El Numero de Tarjeta debe de ser de 16 digitos y la CLABE InterBancaria de 18 digitos');
							return false;
						}
					}	
			if(confirm("Desea consolidar los "+jQuery('#numeroRecibos').val()+" recibos seleccionados por la cantidad de $ "+jQuery('#sumaTotal').val()+"?")){
			
				blockPage();
				
				var posPath = '/MidasWeb/cobranza/recibos/consolidado/consolidarRecibosSeleccionados.action?idRecibosConsolidar='+idRecibosConsolidar+'&fechaFinEjecucionManual='+fechaFinEjecucionManual + '&numPolizaConsolidar='+numPolizaConsolidar+'&fechaConsolidado='+fechaConsolidado+'&idMedioPago=' +jQuery('#idMedioPago_0').val()+'&numTarjeta=' +jQuery('#numTarjeta').val();
				jQuery.ajax({
		             url: posPath,
		             type : "POST",
		             data : null,
		             dataType: 'json',
		             success : function(json){
		            	 	unblockPage();
							alert(json.mensaje);
							resetSumTotal ();
							buscarRecibosDePoliza(jQuery('#fechaConsolidado').val()
							,jQuery('#fechaCorte').val(),jQuery('#numPolizaConsolidar').val()
							,jQuery('#centroEmision').val(),jQuery('#numRenovPol').val());
							
							getGridConsolidados();
							
		             }
		       });
				}
				
				}else{//buscar por metodo dde pago
				alert("Seleccione un metodo de pago");
				
				}
				
			}
		}
		else{
			alert("Seleccione recibo para consolidar");
		}
}


	 function doOnCheckBoxSelectedConsolidado(rID, cInd, state)
        {
            if (state=='1'){
			recibosYaConsolidadosCount=recibosYaConsolidadosCount+1;
			countAllConsolidadosRecords(recibosYaConsolidadosCount);
			}else{
			recibosYaConsolidadosCount=recibosYaConsolidadosCount-1;
			countAllConsolidadosRecords(recibosYaConsolidadosCount);
			}
        
		}
	 function doOnCheckBoxSelected(rID, cInd, state)
        {
            if (state=='1'){
			sumaRecibosAConsolidarTotal=sumaRecibosAConsolidarTotal+parseFloat(gridListadoLog.cellById(rID, 9).getValue());
			recibosAConsolidarCount=recibosAConsolidarCount+1;
			jQuery('#numeroRecibos').val(recibosAConsolidarCount);
			jQuery('#sumaTotal').val(sumaRecibosAConsolidarTotal.toFixed(2)  );
			countAllRecords(recibosAConsolidarCount);
			}else{
			sumaRecibosAConsolidarTotal=sumaRecibosAConsolidarTotal-parseFloat(gridListadoLog.cellById(rID, 9).getValue());
			recibosAConsolidarCount=recibosAConsolidarCount-1;
			jQuery('#numeroRecibos').val(recibosAConsolidarCount);
			jQuery('#sumaTotal').val(sumaRecibosAConsolidarTotal.toFixed(2));
			countAllRecords(recibosAConsolidarCount);
			
			}
		}

function countAllConsolidadosRecords(countRowsInt){

var countRC= gridRecibosConsolidadosLog.getRowsNum() ;
if(countRowsInt<countRC){
jQuery("#checkAllCancelar").attr('checked', false);
}
if(countRowsInt==countRC){
	jQuery("#checkAllCancelar").attr('checked', true);
}
}
	
 function countAllRecords(countRowsInt){
var countRC= gridListadoLog.getRowsNum() ;
if(countRowsInt<countRC){
jQuery("#checkAll").attr('checked', false);
}
if(countRowsInt==countRC){
	jQuery("#checkAll").attr('checked', true);
}
}
function getGridLogEjecucion(fechaConsolidado,fechaFinEjecucionManual, numPolizaConsolidar,centroEmision,numRenovPol)
{
		document.getElementById("logListadoGridRecibosCosolidar").innerHTML = '';
		document.getElementById("logListadoGridRecibosCosolidar").innerHTML = newHtml;
		document.getElementById('logListadoGridRecibosCosolidar').style.display='block';

		var posPath = '/MidasWeb/cobranza/recibos/consolidado/llenarListadoReciboConsolidado.action?fechaFinEjecucionManual=01/01/1900&numPolizaConsolidar=0000&fechaConsolidado=01/01/1900&centroEmision=00&numRenovPol=00'
			;
		
		
		
		
	
		gridListadoLog = new dhtmlXGridObject('logListadoGridRecibosCosolidar');//  logListadoGrid
		gridListadoLog
			.attachHeader("&nbsp,&nbsp,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter");
		var posPath = '/MidasWeb/cobranza/recibos/consolidado/llenarListadoReciboConsolidado.action' 
			+'?fechaFinEjecucionManual='+fechaFinEjecucionManual + '&numPolizaConsolidar='+numPolizaConsolidar  
			+ '&fechaConsolidado='+fechaConsolidado
			+ '&centroEmision='+centroEmision
			+ '&numRenovPol='+numRenovPol
			;
		gridListadoLog.attachEvent("onCheckbox", doOnCheckBoxSelected);
		gridListadoLog.loadXML(posPath,function(){ });
		gridListadoLog.init();
		document.getElementById('divSelectAll').style.display='block';
}
          
function buscarRecibosDePoliza(fechaConsolidado,fechaFinEjecucionManual, numPolizaConsolidar,centroEmision,numRenovPol) {
	  if (!numPolizaConsolidar){
           alert("Favor de capturar numero de poliza");
           return; 
       } 
	  if (!fechaConsolidado){
      		alert("Debe seleccionar fecha de consolidado");
        	  	return;	
      	}
	  if (!fechaFinEjecucionManual){
        		alert("Debe seleccionar fecha de corte");
          	  	return;	
        }
	  if (!centroEmision){
  		alert("Debe seleccionar un centro de emision");
    	  	return;	
  }
	  if (!numRenovPol){
  		alert("Debe seleccionar un numero de renovacion de poliza");
    	  	return;	
  }
        resetSumTotal ();
        getGridLogEjecucion(fechaConsolidado,fechaFinEjecucionManual, numPolizaConsolidar,centroEmision,numRenovPol);
    }
	
	
function cancelarPolizas(fechaConsolidado,fechaFinEjecucionManual, numPolizaConsolidar){
	var sumaTotal =0;
	var countRecibos =0;
		var idchecked = gridRecibosConsolidadosLog.getCheckedRows(1);
		if(idchecked!=""){
			var idRecibosConsolidar="";
			var idArray = idchecked.split(",");
			for(i=1;i<=idArray.length;i++){
			countRecibos=countRecibos+1;
				if(i!=idArray.length){
					idRecibosConsolidar += gridRecibosConsolidadosLog.cellById(idArray[i - 1],0).getValue() + ",";				
				}else{
					idRecibosConsolidar += gridRecibosConsolidadosLog.cellById(idArray[i - 1],0).getValue();
				}
			}
			if(idRecibosConsolidar!=""){
				if(confirm("Desea cancelar los "+countRecibos+" recibos seleccionados ?")){
				var posPath = '/MidasWeb/cobranza/recibos/consolidado/cancelarRecibosSeleccionados.action?idRecibosConsolidar='+idRecibosConsolidar+'&fechaFinEjecucionManual='+fechaFinEjecucionManual + '&numPolizaConsolidar='+numPolizaConsolidar+ '&fechaConsolidado='+fechaConsolidado ;
				jQuery.ajax({
		             url: posPath,
		             type : "POST",
		             data : null,
		             dataType: 'json',
		             success : function(json){
							alert(json.mensaje);
							getGridConsolidados();
		             }
		       });
				}
			}
		}
		else{
			alert("Seleccione un recibo para cancelarlo");
		}
}

function checkUncheckAllCancelar(){
	var sel = jQuery("#checkAllCancelar").attr("checked");
	if(sel){	
		
						var countRC= gridRecibosConsolidadosLog.getRowsNum() ;
						if(countRC==0){
							alert('No hay registros para seleccionar');
							sumaRecibosAConsolidarTotal=0;
							recibosAConsolidarCount= 0;
							recibosYaConsolidadosCount=0;
						}else{
							gridRecibosConsolidadosLog.checkAll(true);	
							recibosYaConsolidadosCount=countRC;
							}
		}else{
			gridRecibosConsolidadosLog.getCheckedRows(1);
			gridRecibosConsolidadosLog.checkAll(false);	
			recibosYaConsolidadosCount=0;
		}
	
	function reloadGrid(){	 
        gridRecibosConsolidadosLog.filterBy(1,0);		
}

	function allChecksSelected(){	
	var idchecked = gridRecibosConsolidadosLog.getCheckedRows(1);
	if(idchecked!=""){
		gridRecibosConsolidadosLog.filterBy(1,1);
	}else{
		alert("No existe ningun registro marcado");
	}
}
	
}
	function sumarRecibosSeleccionados(){
	var sumaTotal =0;
	var countRecibos =0;
	var idchecked = gridListadoLog.getCheckedRows(1);
	var idRecibosConsolidar="";
			var idArray = idchecked.split(",");
			for(i=1;i<=idArray.length;i++){
			countRecibos=countRecibos+1;
			
			sumaTotal = sumaTotal+parseFloat(gridListadoLog.cellById(idArray[i - 1], 9).getValue());
				if(i!=idArray.length){
					idRecibosConsolidar += gridListadoLog.cellById(idArray[i - 1],0).getValue() + ",";				
				}else{
					idRecibosConsolidar += gridListadoLog.cellById(idArray[i - 1],0).getValue();
				}
			}
			jQuery('#numeroRecibos').val(countRecibos);
			jQuery('#sumaTotal').val(sumaTotal.toFixed(2));
			sumaRecibosAConsolidarTotal=sumaTotal.toFixed(2);
			recibosAConsolidarCount= countRecibos;
	return idRecibosConsolidar;	
	}
	
function checkUncheckAll(){
		var sel = jQuery("#checkAll").attr("checked");
				if(sel){
				
						var countRC= gridListadoLog.getRowsNum() ;
						if(countRC==0){
							alert('No hay registros para seleccionar');
							resetSumTotal ();
						}else{
							gridListadoLog.checkAll(true);	
							sumarRecibosSeleccionados();
							}
					
				}else{
				jQuery('#numeroRecibos').val(0);
				jQuery('#sumaTotal').val(0);
				sumaRecibosAConsolidarTotal=0;
				recibosAConsolidarCount= 0;
				gridListadoLog.getCheckedRows(1);
					gridListadoLog.checkAll(false);	
				}
			
			
			function reloadGrid(){	 
					gridListadoLog.filterBy(1,0);		
			}

				function allChecksSelected(){	
					var idchecked = gridListadoLog.getCheckedRows(1);

					if(idchecked!=""){
						gridListadoLog.filterBy(1,1);
					}else{
						alert("No existe ningun registro marcado");
					}
				}
	
}
 
function exportarAviso(folioFiscal){
alert('El recibo '+folioFiscal+' tiene estatus cancelado.');
}

function exportarPDF(folioFiscal){
	var url='/MidasWeb/cobranza/recibos/consolidado/exportarPDF.action?folioFiscal='+folioFiscal;
	window.open(url, "PDF");
}

function exportarXML(folioFiscal){
	var data=jQuery("#polizaForm").serialize();
	var url='/MidasWeb/cobranza/recibos/consolidado/exportarXML.action?folioFiscal='+folioFiscal;
	window.open(url, "XML");
}

function mandarCorreoReciboConsolidado(txtUserEmail,numFolioCorreoEnviar){
	blockPage();
	var data=jQuery("#polizaForm").serialize();                                                         
	var posPath='/MidasWeb/cobranza/recibos/consolidado/mandarCorreoReciboConsolidado.action?numFolio='+numFolioCorreoEnviar.value+"&correo="+txtUserEmail.value; 
	 $.ajax({
			 url:posPath,
			 async: true,
			 dataType: 'json',
			 success: function(result){
			        unblockPage();
					alert("Se ha enviado el correo a la direccion capturada");/** result.mensaje **/
					},
			 error: function(xhr,status,error) {
			        unblockPage();
					alert('Se ha generado un error al procesar el envio de los archivos ');
					}
			});
}

function mandarCorreo(folioFiscal){
		var mandarCorreoPath = '/MidasWeb/cobranza/recibos/consolidado/capturarInformacionCorreo.action?numFolio='+folioFiscal;
		mostrarVentanaModal("modalEnviarMail","Enviar Archivos",200,350, 710, 300, mandarCorreoPath);
}
function limit(element,max_chars){
     if(element.value.length > max_chars) {
        element.value = element.value.substr(0, max_chars);
    }
}

function cambiarMetodoDePago(idMetodoPago){
		if(idMetodoPago==15||idMetodoPago==2){
		$("trNumeroTarjeta").hide();
		}else{
		$("trNumeroTarjeta").show();
		}
}

function getGridConsolidados(){
	realizarBusqueda(true, true);
}

function applyFilter(){
    gridRecibosConsolidadosLog.clearAll();
   		var url = "/MidasWeb/cobranza/recibos/consolidado/llenarRecibosYaConsolidados.action?";
			var serializedForm = encodeForm(jQuery("#polizaForm"));
		    url += serializedForm;
			server = url;			
			var ind= '4';
			var direct='asc'
		
			var gridQString=server+(server.indexOf("?")>=0?"&":"?")
		     +"orderBy="+ind+"&direct="+direct+"&filterForm="+txtFiltroConsolidado.value;
			gridRecibosConsolidadosLog.load(gridQString); // load new dataset from sever with additional param		
			gridRecibosConsolidadosLog.setSortImgState(true,ind,direct);
		    return false;
		
}


/*BUSQUEDA DE CONSOLIDADOS*/
function realizarBusqueda(mostrarBusquedaVacia, isFiltro){
	var url = "/MidasWeb/cobranza/recibos/consolidado/llenarRecibosYaConsolidados.action?";
	var serializedForm = encodeForm(jQuery("#polizaForm"));
	
	if(mostrarBusquedaVacia){
		if(gridRecibosConsolidadosLog){gridRecibosConsolidadosLog.destructor();}
		gridRecibosConsolidadosLog = new dhtmlXGridObject('logListadoGridRecibosConsolidados');
		gridRecibosConsolidadosLog.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		gridRecibosConsolidadosLog.setSkin('light');
		gridRecibosConsolidadosLog.setHeader("Id, Seleccionar,Poliza,Folio Fiscal,Recibo Consolidado,Fecha de Consolidado,Fecha Corte,Fecha Limite,Fecha Creacion,Total,Estatus,Creado por:,PDF,XML,Correo");
		gridRecibosConsolidadosLog.setInitWidths("0,114,83,60,94,93,85,77,80,63,70,120,46,46,46");
		gridRecibosConsolidadosLog.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,center");
		gridRecibosConsolidadosLog.setColSorting("server,server,server,server,server,server,server,server,server,server,server,server,server,server,server");
		gridRecibosConsolidadosLog.setColTypes("ro,ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img,img");
		gridRecibosConsolidadosLog.init();
		gridRecibosConsolidadosLog.attachEvent("onBeforePageChanged",function(){
    		if (!this.getRowsNum()) return false;
    		return true;
    	});
		gridRecibosConsolidadosLog.enablePaging(true,20,5,"pagingArea",true,"infoArea");      
		gridRecibosConsolidadosLog.setPagingSkin("bricks");	
		gridRecibosConsolidadosLog.attachEvent("onXLS", function(grid_obj){blockPage()});
		gridRecibosConsolidadosLog.attachEvent("onXLE", function(grid_obj){unblockPage()});
		gridRecibosConsolidadosLog.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		gridRecibosConsolidadosLog .attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });
			url += serializedForm;
			gridRecibosConsolidadosLog.attachEvent("onBeforeSorting",function(ind, server, direct){	
			if(ind==0||ind==1||ind==12||ind==13||ind==14){
			/*estas columnas no son de busqueda*/
					}else{
			server = url;			
			gridRecibosConsolidadosLog.clearAll();
			gridRecibosConsolidadosLog.load(server+(server.indexOf("?")>=0?"&":"?")
		     +"orderBy="+ind+"&direct="+direct+"&filterForm="+txtFiltroConsolidado.value);
			gridRecibosConsolidadosLog.setSortImgState(true,ind,direct);
		    return false;
				}
			});
     		gridRecibosConsolidadosLog.load(url);
	} 
}


