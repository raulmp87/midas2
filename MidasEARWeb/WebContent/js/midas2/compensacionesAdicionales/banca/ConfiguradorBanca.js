/**
 * Funcionalidad para el Configurador de Contraprestacion de Banca
 */
var gridEstadosActivos;

var getGridEstadosActivos = function(){	
	
	var url = '/MidasWeb/compensacionesAdicionales/banca/cargarGridEstadosActivos.action';
	var idConfiguracionBanca = Number(jQuery('#bancaConfiguracionId').val());
	if(!isNaN(idConfiguracionBanca) && idConfiguracionBanca > 0){
		url = url + '?configuracionContraprestacionBanca.caConfiguracionBanca.id=' + idConfiguracionBanca;	
	}	
	var grid = listarFiltradoGenerico(url,"divGridEstadosId", null,"id",null,null,null);
	
	return grid;
}

/**
 * Funcion Generica para obtener los datos de una tabla
 * @param Nombre del Div en el cual esta el grid
 */
var getDataTable = function(divTable){
	 var objectArray, subArray;
	 objectArray = new Array();
	 var filas, columnas, celda, valor;		 
	 filas = jQuery('#'+divTable).find('table.obj').find('tr');	
	 
	 for (var f = 0; f < filas.length;f++) {
		 columnas = filas[f].children;
		 subArray = new Array();
		 for(var c = 0; c< columnas.length; c++){			
			 celda = columnas[c];				 
			 if(celda.tagName === 'TD'){
				 if(celda.childElementCount > 0){
					 if(jQuery(celda).find('input')){
						 valor = jQuery(celda).find('input').val();
					 }						
				 }else{
					 valor = celda.innerHTML;
				 }
				 valor = (valor === '' || typeof valor === 'undefined') ? 0 : valor;
				 subArray.push(valor);
			 }				 
		 }
		 if(subArray.length>0){
			 objectArray.push(subArray);
		 }
	 }
	 
	 return objectArray;
}


var BancaEstadosView = function (arrayValues){
	this.id = Number(arrayValues[0]);
	this.configuracionBancaId = Number(arrayValues[1]);
	this.estadoId = arrayValues[2];
	this.estadoNombre = arrayValues[3];	
	this.serialize = function (name){
		var a = '';		
		try{
		a = name+'.id='+this.id
			+'&'+name+'.configuracionBancaId='+this.configuracionBancaId
			+'&'+name+'.estadoId='+this.estadoId
			+'&'+name+'.estadoNombre='+this.estadoNombre;	
		}catch(e){			
			
		}
		return a;
	}
}

function getListBancaEstadosView(){	
	var estadosActivos = getDataTable('divGridEstadosId');		
	var list = new Array();
	for(i = 0; i < estadosActivos.length; i++){
		list.push(new BancaEstadosView(estadosActivos[i]));
	}
	return list;
}


function existeEstadoListaActivos(estadoId){
	
	var listBancaEstadosView = getListBancaEstadosView();
	var existe = false;
	for(i = 0; i < listBancaEstadosView.length; i++){		
		if(estadoId.toString() === listBancaEstadosView[i].estadoId){
			existe = true;
			break;
		}		
	}
	return existe;
}


function agregarEstado(){	
	
	var estadoSelect = jQuery('#listEstadosId>li.selectEstado');
	
	if(estadoSelect.length > 0){		
		var estadoId = estadoSelect.attr('value');
		var estadoNombre = estadoSelect.attr('title');
		
		if(existeEstadoListaActivos(estadoId)){			
			alert('El estado: '+ estadoNombre +' ya ha sido agregado al listado');
		}else{
			var numRows = gridEstadosActivos.getRowsNum();
			var iconDelete = '<a href="javascript:void(0)" onclick="eliminarEstado('+estadoId+')"> <img src="/MidasWeb/img/icons/ico_eliminar.gif" /> </a>'
			var dataRow = [0,0,estadoId, estadoNombre, iconDelete];
			gridEstadosActivos.addRow(estadoId, dataRow);
		}
	}else{
		alert('Seleccione un Estado por favor');
	}
	
	jQuery('#listEstadosId').children().removeClass('selectEstado');
}

function eliminarEstado(estadoId){
	console.info(estadoId);
	gridEstadosActivos.deleteRow(estadoId);
}


function selectEstado(element){
	jQuery('#listEstadosId').children().removeClass('selectEstado');
	jQuery(element).addClass('selectEstado');
}


function guardarConfiguracionContraprestacionBanca(){
	var $configuracionId = jQuery('#bancaConfiguracionId');
	var $nombreConfiguracionId = jQuery('#nombreConfiguracionId');
	var $listLineasVenta = jQuery('#listLineasVenta');

	var urlSave = '/MidasWeb/compensacionesAdicionales/banca/guardarConfiguracion.action?';
	/**
	 * Validacion nuevo registro
	 */
	if(Number($configuracionId.val()) > 0){
		
		if(validateFormConfiguracion()){
			urlSave = urlSave + '&configuracionContraprestacionBanca.caConfiguracionBanca.id='+$configuracionId.val()+'&'+ $nombreConfiguracionId.serialize() +'&'+ $listLineasVenta.serialize() 
			+ getListRamos() + getGerencia() + getListEstadosActivos();
			saveConfiguracionBanca(urlSave);
		}else{
			alert('Todos los campos son requeridos por favor revise el formulario');
		}
		
	}else{
		if(validateFormConfiguracion()){
			urlSave = urlSave + $nombreConfiguracionId.serialize() +'&'+ $listLineasVenta.serialize() 
			+ getListRamos() + getGerencia() + getListEstadosActivos();
			saveConfiguracionBanca(urlSave);
		}else{
			alert('Todos los campos son requeridos por favor revise el formulario');
		}
	}
}

function saveConfiguracionBanca(urlSave){
	blockPage();
	jQuery.ajax({
		url: urlSave,
		type: 'POST',
		async: true,
		success: function (res) {
			unblockPage();
			try{
				var objectRes = JSON.parse(res);
				var id = Number(objectRes.id);
				if(isNaN(id)){
			    	alert("Ocurrio un error al guardar la configuracion");
			    }else{
			    	console.info(res);
					jQuery('#bancaConfiguracionId').val(id);
					jQuery('#usuarioId').val(objectRes.usuario);
					jQuery('#ultimaModificacionId').val(objectRes.fecha);
				    console.log("Se ha guardado la configuracion");		
			    }
			}catch(e){
				alert("Ocurrio un error al guardar la configuracion");
			}
		},
		error: function (res) {
			unblockPage();
		    console.log("Ocurrio un error al guardar la configuracion");			   
		},
		complete: function(jqXHR) {				
			unblockPage();
		}
	});
}

function getListRamos(){
	var list = '';
	var name
	var index = 0;
	jQuery('#divListRamosId').find('input').each(function(i){
		if(jQuery(this).is(':checked')){
			list = list+'&'+this.name+'['+index+']'+'='+this.value;
			index++;
		}
	});		
	return list;
}

function getListLineasVentas(){
	var list = '';
	var name
	var index = 0;
	jQuery('#divListLineasVentaId').find('input').each(function(i){
		if(jQuery(this).is(':checked')){
			list = list+'&'+this.name+'['+index+']'+'='+this.value;
			index++;
		}
	});		
	return list;
}


function getGerencia(){
	var gerencia = '';
	var $element = jQuery('#divListGerenciasId').find('input:checked');

	if($element.length > 0){
		var name = $element.attr('name');
		var idGerencia = $element.val();
		var idempresa = $element.attr('idempresa');
		var fsit = $element.attr('fsit');
		gerencia = '&'+ name +'.idGerencia='+ idGerencia;
		gerencia = gerencia +'&'+ name +'.idEmpresa='+ idempresa;
		gerencia = gerencia +'&'+ name +'.fsit='+ fsit;
	}
	
	return gerencia;
}

function getListGerencias(){
	var list = '';
	var index = 0;
	jQuery('#divListGerenciasId').find('input').each(function(i){
		$element = jQuery(this);
		if($element.is(':checked')){
			list = list+'&'+this.name+'['+index+']'+'.idGerencia='+this.value
				  +'&'+this.name+'['+index+']'+'.idEmpresa='+$element.attr('idempresa')
				  +'&'+this.name+'['+index+']'+'.fsit='+$element.attr('fsit');
			index++;
		}
	});		
	return list;
}


function getListEstadosActivos(){
	var listEstados = getListBancaEstadosView();
	var name = 'configuracionContraprestacionBanca.listBancaEstadosActivos';
	var list = '';
	var estado = null;
	var index = 0;
	for(i = 0; i < listEstados.length; i++){
		estado = listEstados[i];
		list = list+'&'+estado.serialize(name+'['+index+']');
		index++;
	}
	return list;
}


function validateFormConfiguracion(){
	var validate = false;
	
	try{
		var nombreConfiguracion = jQuery('#nombreConfiguracionId').val().trim().length > 0;
		var lineasVenta = Number(jQuery('#listLineasVenta').val()) > 0;
		var listRamos = jQuery('#divListGerenciasId').find('input').is(':checked');
		var listGerencias = jQuery('#divListGerenciasId').find('input').is(':checked');
		var listEstados = getDataTable('divGridEstadosId').length > 0;		
		validate = nombreConfiguracion && lineasVenta && listRamos && listGerencias && listEstados;	
	}catch(e){
		console.error('Ocurrio un error al validar');
	}	
	return validate;
}

var ventanaCompensacion;

function openCompensacionAdicional(){
	
	var configuracionBancaId = Number(jQuery('#bancaConfiguracionId').val());
	
	if(!isNaN(configuracionBancaId) && configuracionBancaId>0){
		try{
			blockPage();
			var tabActiva = '&tabActiva=contraprestacion';
			ventanaCompensacion = window.open("/MidasWeb/compensacionesAdicionales/init.action?ramo=B&idConfiguracionBanca="+configuracionBancaId+tabActiva, 'Compensaciones', 'height=600,width=1200,modal=yes,resizable=yes,scrollbars=auto');
			ventanaCompensacion.focus();
			compensacionRecargado = false;
			temporizadoWinComp = setInterval("checkVentanaCompensacion();", 3000);
		}catch(e){
			unblockPage();
			alert('Ocurrio un error al abrir el configurador de compensaciones');
		}
	}else{
		alert('Es necesario primero crear el registro de la configuracion de contraprestacion de Banca');
	}
}


function checkVentanaCompensacion(){
	if (ventanaCompensacion.closed == true ){
		clearInterval(temporizadoWinComp);
		if (compensacionRecargado == false){
			wCloseCompensacion();
		}
	}
}


function wCloseCompensacion(){
	unblockPage();
	compensacionRecargado = true;
}


function regresarListadoConfiguraciones(){
	var url = '/MidasWeb/compensacionesAdicionales/banca/listConfiguracionesBanca.action';
	sendRequest(null, url, 'contenido', null);
}


function historicoConfiguracionBanca(){	
	
	var idConfiguracionBanca = Number(jQuery('#bancaConfiguracionId').val());
	var url = '/MidasWeb/compensacionesAdicionales/banca/historicoConfiguracionBanca.action?';
	url = url + 'configuracionContraprestacionBanca.caConfiguracionBanca.id=' + idConfiguracionBanca;	
	

	var ventanaModificaciones;
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	
	ventanaModificaciones = dhxWins.createWindow("MODIFICACIONES", 400, 320, 722, 400);
	ventanaModificaciones.btns["minmax1"].hide(); 
	ventanaModificaciones.btns["minmax2"].hide();
	ventanaModificaciones.btns["park"].hide(); 
	ventanaModificaciones.setText("Bitácora de Acciones");
	ventanaModificaciones.setModal(true);
	ventanaModificaciones.centerOnScreen();
	
	ventanaModificaciones.attachURL(url);
}


function returnListado(){
	var url = '/MidasWeb/compensacionesAdicionales/banca/listConfiguracionesBanca.action';
	jQuery('#contenido').children().detach();
	sendRequestJQ(null, url, 'contenido',   null);
}