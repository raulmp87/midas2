function setearMesAnio (){
    var anioactual = jQuery("#testAnio").val();
	if(anioactual != null){
		var year = anioactual.split(".",1);
		jQuery("#anios").val(year);
	}
	if (anioactual ==''){
		var date = new Date();
		var y = date.getFullYear();
		jQuery("#anios").val(y);
	}
	
	var actual = jQuery("#testValor").val();
	if (actual != null){
		var month = actual.split(".",1);
		jQuery("#meses").val(month);
	}
	if (actual == ''){
		var date = new Date();
		var d = date.getMonth();
		if (d == 0){
		var diciembre = 12;
		var anio = date.getFullYear()-1;
		jQuery("#meses").val(diciembre);
		jQuery("#anios").val(anio);
		} 
		else{
		jQuery("#meses").val(d);
		}
	}
		

} 

function testValidacion(){
	 		var varprimaPaga = jQuery("#primasPagadas").val();
	 		var varsiniMes = jQuery("#siniMes").val();
	 		var varcostonetosinaut = jQuery("#costonetosinaut").val();
		 	var varcostonetosindan= jQuery("#costonetosindan").val();
		 	var varprimasretendevenaut = jQuery("#primasretendevenaut").val();
		 	var varprimasretendevendan =  jQuery("#primasretendevendan").val();
		 	var varmes = jQuery("#meses").val();
		 	var varanios = jQuery("#anios").val();
		 	var date = new Date();
		 	var mespermitido = date.getMonth();
		 	if(varprimaPaga == '' || varsiniMes == '' || varcostonetosinaut == '' || varcostonetosindan == '' || 
		 	varprimasretendevenaut == '' || varprimasretendevendan == '' || varmes == '' || varanios == ''){
		 		alert("Introduce todos los campos por favor");
		 		return false;
		 	}
		 	
		 	if(varmes == 12 && mespermitido ==0){
	 			if (confirm('Guardar la configuracion de Diciembre?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 1 && mespermitido ==1){
	 			if (confirm('Guardar la configuracion de Enero?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 2 && mespermitido ==2){
	 			if (confirm('Guardar la configuracion de Febrero?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 3 && mespermitido ==3){
	 			if (confirm('Guardar la configuracion de Marzo?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 4 && mespermitido ==4){
	 			if (confirm('Guardar la configuracion de Abril?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 5 && mespermitido ==5){
	 			if (confirm('Guardar la configuracion de Mayo?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 6 && mespermitido ==6){
	 			if (confirm('Guardar la configuracion de Junio?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 7 && mespermitido ==7){
	 			if (confirm('Guardar la configuracion de Julio?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 8 && mespermitido ==8){
	 			if (confirm('Guardar la configuracion de Agosto?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 9 && mespermitido ==9){
	 			if (confirm('Guardar la configuracion de Septiembre?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 10 && mespermitido ==10){
	 			if (confirm('Guardar la configuracion de Octubre?')) {
					    return true;
					} else {
					    return false;
					}
	 		}
	 		if(varmes == 11 && mespermitido ==11){
	 			if (confirm('Guardar la configuracion de Noviembre?')) {
					    return true;
					} else {
					   return false;
				}
	 		}
	 		
	 			return false;
	 }

	


function desbloquearInputCostosPrimas(){
	jQuery("#costonetosinaut").removeAttr('disabled');
	jQuery("#costonetosindan").removeAttr('disabled');
	jQuery("#primasretendevenaut").removeAttr('disabled');
	jQuery("#primasretendevendan").removeAttr('disabled');
}

function verHistoriaConfiguracion(){
	var configuracionId = jQuery("#testId").val();
	var ventanaModificaciones;
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	ventanaModificaciones = dhxWins.createWindow("MODIFICACIONES", 400, 320, 722, 400);
	ventanaModificaciones.btns["minmax1"].hide(); 
	ventanaModificaciones.btns["minmax2"].hide();
	ventanaModificaciones.btns["park"].hide(); 
	ventanaModificaciones.setText("Bitácora de Acciones");
	ventanaModificaciones.setModal(true);
	ventanaModificaciones.centerOnScreen();	
	ventanaModificaciones.attachURL('/MidasWeb/compensacionesAdicionales/banca/verHistorialConfiguracion.action?idCp='+configuracionId);
}