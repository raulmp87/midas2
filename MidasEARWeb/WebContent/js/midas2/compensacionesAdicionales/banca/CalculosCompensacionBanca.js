/**
 * 
 */
var getGridListConfiguracionesBanca = function(){	
	
	var $divGrid = jQuery('#divListConfiguracionesBancaId');	
	$divGrid.html('');	
	var url = '/MidasWeb/compensacionesAdicionales/banca/buscarConfiguracionesContraprestacionBanca.action?';	
	var listGerencias = getListGerencias();
	var listLineasNegocio = getListLineasVentas();
	var listRamos = getListRamos();
	url = url + listGerencias + listLineasNegocio + listRamos;
	var grid = listarFiltradoGenerico(url,$divGrid.attr('id'), null,'',null,null,null);
	grid.setColumnHidden(3, true);
	
	return grid;
}


getGridListConfiguracionesBanca();


var getGridResultCalculosBanca = function(){	
	
	var $divGrid = jQuery('#divResultsCalculosBanca');	
	$divGrid.html('');	
	var url = '/MidasWeb/compensacionesAdicionales/banca/ejecutarCalculosBanca.action?';	
	var listGerencias = getListGerencias();
	var listLineasNegocio = getListLineasVentas();
	var listRamos = getListRamos();
	var listCaConfiguracionBanca = getListCaConfiguracionBancaActivos();
	var fechaId = "&fechaId=" +jQuery("#fechaId").val();
	url = url + listGerencias + listLineasNegocio + listRamos + listCaConfiguracionBanca + fechaId;
	var grid = listarFiltradoGenerico(url,$divGrid.attr('id'), null,null,null,null,null);
	grid.setColumnHidden(1, true);
	grid.setColumnHidden(13, true);
	//grid.setColumnHidden(2, true);
	
	return grid;
}


getGridResultCalculosBanca();


function provisionarCalculosBanca(){
	var url = '/MidasWeb/compensacionesAdicionales/banca/provisionarCalculosBanca.action?';	
	/**/
	var listCaConfiguracionBanca = getDataTable('divResultsCalculosBanca');		
	var list = new Array();
	var lista = new Array();
	var fecha = jQuery("#fechaId").val();
	var fechaCalculoCompensacion = "&fechaCalculoCompensacion="+fecha;
	/**/
	var listGerencias = getListGerencias();
	var listLineasNegocio = getListLineasVentas();
	var listRamos = getListRamos();
	var listCaConfiguracionBanca = getListCaConfiguracionBancaActivos();
	var listResultsCalculosBanca = getListCaConfiguracionBancaView();
	url = url + listResultsCalculosBanca +fechaCalculoCompensacion;
	sendRequest(null,url, 'divMensajesConfiguracionContraprestacionBanca',null);
}

function validateChecksList(element){
	
	if(jQuery(element).is(':checked')){		
		if(!validateList()){
			alert('Para seleccionar necesita primero aplicar todos los filtros: Gerencias, Lineas de Venta y Ramos');
			jQuery(element).attr('checked', false);
		}
	}
}


function validateList(){
	var validate = false;	
	try{
		var lineasVenta = jQuery('#divListLineasVentaId').find('input').is(':checked');
		var listRamos = jQuery('#divListRamosId').find('input').is(':checked');
		var listGerencias = jQuery('#divListGerenciasId').find('input').is(':checked');	
		validate = lineasVenta && listRamos && listGerencias;	
	}catch(e){
		console.error('Ocurrio un error al validar');
	}	
	return validate;
}



function sendResultCalculos(){
	var $divForm = jQuery('#divformExportExcelCalculosBancaId');
	var arrayObject = getDataTable('divResultsCalculosBanca');
	
	 $divForm.append('<form action="/MidasWeb/compensacionesAdicionales/banca/exportarExcelResultadosCalculos.action" target="iframeMensajesCompensacionesAdicionales" id="formExportExcelCalculosBancaId" name="formExportExcelCalculosBanca"> ')
	
	 var $form = $divForm.find('form');
	 
	var calculosBancaView = null;
	for(i =0; i<arrayObject.length; i++){
		 calculosBancaView = new CalculosBancaView(arrayObject[i]);
		 
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].id" value="'+calculosBancaView.id+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].ramo" value="'+calculosBancaView.ramo+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].primaNetaEmitida" value="'+calculosBancaView.primaNetaEmitida+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].primaNetaPagada" value="'+calculosBancaView.primaNetaPagada+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].porcentajeContraprestacionProducto" value="'+calculosBancaView.porcentajeContraprestacionProducto+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].montoContraprestacionProducto" value="'+calculosBancaView.montoContraprestacionProducto+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].porcentajeContraprestacionCumplimientoMeta" value="'+calculosBancaView.porcentajeContraprestacionCumplimientoMeta+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].montoContraprestacionCumplimientoMeta" value="'+calculosBancaView.montoContraprestacionCumplimientoMeta+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].porcentajeCalidadCartera" value="'+calculosBancaView.porcentajeCalidadCartera+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].montoCalidadCartera" value="'+calculosBancaView.montoCalidadCartera+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].bonoFijo" value="'+calculosBancaView.bonoFijo+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].descuento" value="'+calculosBancaView.descuento+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].total" value="'+calculosBancaView.total+'">');
		 $form.append('<input type="hidden"  name="listResultsCalculosBanca['+i+'].comentarios" value="'+calculosBancaView.comentarios+'">');
		 
	}
	
	 $form.submit();
	 $form.detach();
	
}


var CalculosBancaView = function (arrayValues){
	this.ramo = arrayValues [0];
	this.codigoRamo = Number(arrayValues[1]);
	this.primaNetaEmitida = Number(arrayValues[2]);
	this.primaNetaPagada = Number(arrayValues[3]);
	this.porcentajeContraprestacionProducto = Number(arrayValues[4]);
	this.montoContraprestacionProducto = Number(arrayValues[5]); 
	this.porcentajeContraprestacionCumplimientoMeta = Number(arrayValues[6]);
	this.montoContraprestacionCumplimientoMeta = Number(arrayValues[7]);
	this.porcentajeCalidadCartera = Number(arrayValues[8]);
	this.montoCalidadCartera = Number(arrayValues[9]);
	this.bonoFijo = Number(arrayValues[10]);
	this.descuento = Number(arrayValues[11]);
	this.total = Number(arrayValues[12]);
	this.totalBase = Number(arrayValues[13]);
	this.comentarios = arrayValues[14];
	
	this.serialize = function (name){
		var a = '';
		try {
			a = name+'.ramo='+this.ramo
			+'&'+name+'.codigoRamo='+this.codigoRamo
			+'&'+name+'.primaNetaEmitida='+this.primaNetaEmitida
			+'&'+name+'.primaNetaPagada='+this.primaNetaPagada
			+'&'+name+'.porcentajeContraprestacionProducto='+this.porcentajeContraprestacionProducto
			+'&'+name+'.montoContraprestacionProducto='+this.montoContraprestacionProducto
			+'&'+name+'.porcentajeContraprestacionCumplimientoMeta='+this.porcentajeContraprestacionCumplimientoMeta
			+'&'+name+'.montoContraprestacionCumplimientoMeta='+this.montoContraprestacionCumplimientoMeta
			+'&'+name+'.porcentajeCalidadCartera='+this.porcentajeCalidadCartera
			+'&'+name+'.montoCalidadCartera='+this.montoCalidadCartera
			+'&'+name+'.bonoFijo='+this.bonoFijo
			+'&'+name+'.descuento='+this.descuento
			+'&'+name+'.total='+this.total
			+'&'+name+'.totalBase='+this.totalBase
			+'&'+name+'.comentarios='+this.comentarios
		}catch(e){
			
		}
		return a;
	}
}




var CaConfiguracionBanca = function (arrayValues){
	this.id = Number(arrayValues[0]);
	this.nombre = arrayValues[1];
	this.usuario = arrayValues[2];
	this.fechaModificacion = 0;	
	this.serialize = function (name){
		var a = '';		
		try{
		a = name+'.id='+this.id
			+'&'+name+'.nombre='+this.nombre
			+'&'+name+'.usuario='+this.usuario;
			//+'&'+name+'.fechaModificacion='+this.fechaModificacion;	
		}catch(e){			
			
		}
		return a;
	}
}



function getListCaConfiguracionBanca(){
	var listCaConfiguracionBanca = getDataTable('divListConfiguracionesBancaId');		
	var list = new Array();
	for(i = 0; i < listCaConfiguracionBanca.length; i++){
		list.push(new CaConfiguracionBanca(listCaConfiguracionBanca[i]));
	}
	return list;
}
function getListaConfiguracionBancaView(){
	var listResultsCalculosBanca = getDataTable('divResultsCalculosBanca');
	var list = new Array();
	for (i = 0; i <listResultsCalculosBanca.length; i ++){
		list.push(new CalculosBancaView(listResultsCalculosBanca[i]));
	}
	return list;
}


function getListCaConfiguracionBancaActivos(){
	var listCaConfiguracionBanca = getListCaConfiguracionBanca();
	var name = 'listCaConfiguracionBanca';
	var list = '';
	var CaConfiguracionBanca = null;
	var index = 0;
	for(i = 0; i < listCaConfiguracionBanca.length; i++){
		CaConfiguracionBanca = listCaConfiguracionBanca[i];
		list = list+'&'+CaConfiguracionBanca.serialize(name+'['+index+']');
		index++;
	}
	return list;
}

function getListCaConfiguracionBancaView(){
	var listResultsCalculosBanca = getListaConfiguracionBancaView();
	var name = 'listResultsCalculosBanca';
	var list = '';
	var CalculosBancaView = null;
	var index = 0;
	for(i = 0; i<listResultsCalculosBanca.length; i ++){
		CalculosBancaView = listResultsCalculosBanca[i];
		list = list+'&'+CalculosBancaView.serialize(name+'['+index+']');
		index++;
	}
	return list;
	
}


function aplicarDescuento(element){
	var descuento = Number(jQuery(element).val());	
	if(!isNaN(descuento)){
		var fila = element.id.split('_')[1];
		var totalBase = Number(jQuery("#totalBase_"+fila).val());
		if(!isNaN(totalBase)){
			if(descuento>totalBase){
				alert("El Descuento no puede exceder el Total");
			}
			else{
				var total = totalBase-descuento;		
				jQuery("#total_"+fila).val(total);
			}
		}
	}
}

