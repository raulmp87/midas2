jQuery(document).ready(function() { 	
	listadoService.getMapMonths(function(data) {
		addOptionsHeaderAndSelect("meses2", data, null, "", "Seleccione...");
	})
	listadoService.getMapYears(16, function(data) {
		 addOptionOrderdesc("anios2", data);
	});
	
});

function addOptionOrderdesc(target, map){
	jQuery("#"+target).append("<option value=''>Seleccione...</option>");
	var a = 0;
	var arr1 = new Array();
	for (var key in map) {
		arr1[a]= key;
		a++;
	}
	for(var b = arr1.length; b>0 ;b--){
		jQuery("#"+target).append("<option value="+arr1[b-1]+">"+arr1[b-1]+"</option>");
	}
}

var generarReporteCompensaciones = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteCompensaciones.action";
var generarReporteDerechoPoliza = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteDerechoPoliza.action";
var generarReportePagadasDevengar = "/MidasWeb/compensacionesAdicionales/reportes/generarReportePagadasDevengar.action";
var generarReporteEstadoCuenta = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteEstadoCuenta.action";
var generarReportePagoCompContra= "/MidasWeb/compensacionesAdicionales/reportes/generarReportePagoCompContra.action";
var generarReportePagoSaldosCom = "/MidasWeb/compensacionesAdicionales/reportes/generarReportePagoSaldosCom.action";
var generarReporteBancaSeguros = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteBancaSeguros.action";
var generarReporteHonorarios = "/MidasWeb/compensacionesAdicionales/reportes/generarReciboHonorarios.action";
var generarReporteDetallePrima = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteDetallePrima.action";
var generarReportePagosSaldos = "/MidasWeb/compensacionesAdicionales/reportes/generarReportePagosSaldos.action";

function tipoReporte(){
	var x = document.getElementById("select_tipoReporte").value;
	if (x==1){
		document.getElementById('EstadoCuentaTBody').style.display = 'block';
		document.getElementById('PagosTBody').style.display = 'none';
		
		
	}
	else if (x==2){
		document.getElementById('EstadoCuentaTBody').style.display = 'none';
		document.getElementById('PagosTBody').style.display = 'block';
	}
	else if (x==3){
		document.getElementById('EstadoCuentaTBody').style.display = 'block';
		document.getElementById('PagosTBody').style.display = 'none';
	}
	else if (x==4){
		document.getElementById('EstadoCuentaTBody').style.display = 'block';
		document.getElementById('PagosTBody').style.display = 'none';
	}
	else{
		
	}
	//alert(x);
}

function tipoGerencia(){
	var x = document.getElementById("select_tipoGerencia").value;
	if(x==1){
		document.getElementById("PagosTBody").style.display = 'block';
	}
}

function imprimirReporteEstadoCuenta(){
	var tipoReporte = document.getElementById("select_tipoReporte").value;
	var name_reporte = 0;
	var REP_PAGADASXDEVENGAR = 1;
	var REP_COMCONTRA_PAGDEVENGAR = 2;
	var REP_TIPO_REPORTE_PAGDEV = 5;
	if(tipoReporte == REP_TIPO_REPORTE_PAGDEV){
		tipoReporte = REP_COMCONTRA_PAGDEVENGAR
		name_reporte = REP_PAGADASXDEVENGAR;
	}
	
	var cveAgente=jQuery("#cveAgente").val();
	var cvePromotor=jQuery("#cvePromotor").val();
	var cveProveedor=jQuery("#cveProveedor").val();
	
	if(tipoReporte==1){
		var cero= 0;
		var anio = jQuery('#anios').find(":selected").text();
		var mes = jQuery('#meses').val();
		if(mes <=9){
			mes =cero.toString()+mes.toString();
		}
		else{
	        mes=mes.toString();
	       }
		var anioMes = anio+mes;
		var location = generarReporteEstadoCuenta;
		
		if(anioMes != undefined && anioMes != null && anioMes != '' )
			location = location+"?anioMes="+anioMes;
		if(cveAgente != undefined && cveAgente != null && cveAgente != '' )
			location = location+"&cveAgente="+cveAgente;
		if(cvePromotor != undefined && cvePromotor != null && cvePromotor != '' )
			location = location+"&cvePromotor="+cvePromotor;
		if(cveProveedor != undefined && cveProveedor != null && cveProveedor != '' )
			location = location+"&cveProveedor="+cveProveedor;
		window.open(location,"generarReporteEstadoCuenta");
	}
	else if (tipoReporte==2){
		var ramoo = jQuery('#idRamo').val();		
		var poliza = jQuery('#poliza').val();
		var claveNombreAgente = dwr.util.getValue('agente');
		var claveNombreProveedor = dwr.util.getValue('proveedor');
		var claveNombreGerencia =  dwr.util.getValue('gerencia');
		var claveNombrePromotor =  dwr.util.getValue('promotor');
		var fechaInicio = jQuery('#txtFechaInicio').val();
		var fechaFinal = jQuery('#txtFechaFinal').val();
		alert(fechaInicio+ " " + fechaFinal );
		var location = generarReportePagoCompContra;
		if(ramoo ==null || ramoo =='' || ramoo =='0'){
			parent.mostrarVentanaMensaje("10","Seleccione un Ramo", null);
		}else{
			if( poliza == '' && claveNombreAgente == '' && claveNombreProveedor == '' && claveNombreGerencia == '' && claveNombrePromotor =='' && fechaFinal =='' && fechaFinal =='' ){
				parent.mostrarVentanaMensaje("10","Selecciones Algun otro filtro  para generar el reporte", null);
			}else{
				if(ramoo != undefined && ramoo != null && ramoo != '')
					location = location+"?ramo="+ramoo;
				if(poliza != undefined && poliza != null && poliza != '')
					location = location+"&poliza="+poliza;
				if(claveNombreAgente != undefined && claveNombreAgente != null && claveNombreAgente != '')
					location = location+"&claveNombreAgente="+claveNombreAgente;
				if(claveNombreProveedor != undefined && claveNombreProveedor != null && claveNombreProveedor != '')
					location = location+"&claveNombreProveedor="+claveNombreProveedor;
				if(claveNombrePromotor != undefined && claveNombrePromotor !=null && claveNombrePromotor !='')
					location = location+"&claveNombrePromotor="+claveNombrePromotor;
				if(claveNombreGerencia != undefined && claveNombreGerencia != null && claveNombreGerencia !='')
					location = location+"&claveNombreGerencia="+claveNombreGerencia;
				if(fechaInicio != undefined && fechaInicio != null && fechaInicio !='')
					location = location+"&fechaInicio="+fechaInicio;
				if(fechaFinal != undefined && fechaFinal != null && fechaFinal !='')
					location = location+"&fechaFinal="+fechaFinal;
					if(name_reporte != undefined && name_reporte != null && name_reporte !='')
						location = location+"&name_reporte="+name_reporte;
					window.open(location,"generarReportePagoCompContra");
			}
	  }
}
	if(tipoReporte==3){
		var cero= 0;
		var anio = jQuery('#anios').find(":selected").text();
		var mes = jQuery('#meses').val();
		if(mes <=9){
			mes =cero.toString()+mes.toString();
		}
		else{
	        mes=mes.toString();
	       }
		var anioMes = anio+mes;
		var cveAgente=jQuery("#cveAgente").val();
		var cvePromotor=jQuery("#cvePromotor").val();
		var cveProveedor=jQuery("#cveProveedor").val();
		var location = generarReporteHonorarios;
		
		if(anioMes != undefined && anioMes != null && anioMes != '' )
			location = location+"?anioMes="+anioMes;
		if(cveAgente != undefined && cveAgente != null && cveAgente != '' )
			location = location+"&cveAgente="+cveAgente;
		if(cvePromotor != undefined && cvePromotor != null && cvePromotor != '' )
			location = location+"&cvePromotor="+cvePromotor;
		if(cveProveedor != undefined && cveProveedor != null && cveProveedor != '' )
			location = location+"&cveProveedor="+cveProveedor;
		window.open(location,"generarReporteHonorarios");
		
	}	
	if(tipoReporte==4){
		var cero= 0;
		var anio = jQuery('#anios').find(":selected").text();
		var mes = jQuery('#meses').val();
		if(mes <=9){
			mes =cero.toString()+mes.toString();
		}
		else{
	        mes=mes.toString();
	       }
		var anioMes = anio+mes;
		var location = generarReporteDetallePrima;
		
		if(anioMes != undefined && anioMes != null && anioMes != '' )
			location = location+"?anioMes="+anioMes;
		if(cveAgente != undefined && cveAgente != null && cveAgente != '' )
			location = location+"&cveAgente="+cveAgente;
		if(cvePromotor != undefined && cvePromotor != null && cvePromotor != '' )
			location = location+"&cvePromotor="+cvePromotor;
		if(cveProveedor != undefined && cveProveedor != null && cveProveedor != '' )
			location = location+"&cveProveedor="+cveProveedor;
		window.open(location,"generarReporteDetallePrima");
	}
	
}

function imprimirReporteCompensaciones(){
	var tipoReporte = 1;
	var ramo = dwr.util.getValue('ramo');
	var agente = dwr.util.getValue('agente');
	var promotor = dwr.util.getValue('promotor');
	var proveedor = dwr.util.getValue('proveedor');
	var gerencia = dwr.util.getValue('gerencia');
	var idNegocio = dwr.util.getValue('idNegocio');
	var nombreNegocio = dwr.util.getValue('nombreNegocio');
	var fechaInicial = dwr.util.getValue('fechaInicio');
	var fechaFin = dwr.util.getValue('fechaFinal');
	
		
		if(tipoReporte != undefined && tipoReporte != null && tipoReporte != '' )
			location = location+"?tipoReporte="+tipoReporte;
		if(ramo != undefined && ramo != null && ramo != '' )
			location = location+"&ramo="+ramo;
		if(agente != undefined && agente != null && agente != '' )
			location = location+"&agente="+agente;
		if(promotor != undefined && promotor != null && promotor != '' )
			location = location+"&promotor="+promotor;
		if(proveedor != undefined && proveedor != null && proveedor != '' )
			location = location+"&proveedor="+proveedor;
		if(gerencia != undefined && gerencia != null && gerencia != '' )
			location = location+"&gerencia="+gerencia;
		if(idNegocio != undefined && idNegocio != null && idNegocio != '' )
			location = location+"&idNegocio="+idNegocio;
		if(nombreNegocio != undefined && nombreNegocio != null && nombreNegocio != '' )
			location = location+"&nombreNegocio="+nombreNegocio;		
		if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '' )
			location = location+"&fechaInicial="+fechaInicial;
		if(fechaFin != undefined && fechaFin != null && fechaFin != '' )
			location = location+"&fechaFin="+fechaFin ;
		window.open(location,"generarReporteCompensaciones");
  }
	
function imprimirReporteDerechoPoliza(){
	var ramo = dwr.util.getValue('ramo');
	var agente = dwr.util.getValue('agente');
	var promotor = dwr.util.getValue('promotor');
	var idNegocio = dwr.util.getValue('idNegocio');
	var nombreNegocio = dwr.util.getValue('nombreNegocio');
	var fechaInicial = dwr.util.getValue('fechaInicial');
	var fechaFin = dwr.util.getValue('fechaFinal');
	    
		var location  = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteDerechoPoliza.action?tipoReporte=2";
		
		if(ramo ==null || ramo =='' || ramo =='0'){
			parent.mostrarVentanaMensaje("10","Seleccione un Ramo", null);
		}else{
			if(agente=='' && promotor=='' && idNegocio=='' && nombreNegocio=='' && fechaInicial=='' && fechaFin==''){
				parent.mostrarVentanaMensaje("10","Selecciones Algun otro filtro  para generar el reporte", null);
			}else{
				if(ramo != undefined && ramo != null && ramo != '' )	
					location = location+"&ramo="+ramo;
				if(agente != undefined && agente != null && agente != '' )
					location = location+"&agente="+agente;
				if(promotor != undefined && promotor != null && promotor != '' )
					location = location+"&promotor="+promotor;
				if(idNegocio != undefined && idNegocio != null && idNegocio != '' )
					location = location+"&idNegocio="+idNegocio;
				if(nombreNegocio != undefined && nombreNegocio != null && nombreNegocio != '' )
					location = location+"&nombreNegocio="+nombreNegocio;		
				if(fechaInicial != undefined && fechaInicial != null && fechaInicial != '' )
					location = location+"&fechaInicial="+fechaInicial;
				if(fechaFin != undefined && fechaFin != null && fechaFin != '' )
					location = location+"&fechaFin="+fechaFin ;
				window.open(location,"generarReporteDerechoPoliza");
               }
        }        	
        }        	
	
function imprimirReportesCompensacionesAdicionales(){
	
	var tipoReporte = dwr.util.getValue('tipoReporte');
	var ramo = dwr.util.getValue('idRamo');
	var agente = dwr.util.getValue('agente');
	var promotor = dwr.util.getValue('promotor');
	var proveedor = dwr.util.getValue('proveedor');
	var fechaInicial = dwr.util.getValue('fechaInicio');
	var fechaFin = dwr.util.getValue('fechaFinal');
	
	if (ramo ==1){
		ramo='A';
	}
	else if (ramo ==2){
		ramo='D';
	}
	else if (ramo ==3){
		ramo='V';
	}
	else if(ramo==4){
		ramo='B';
	}

	var cero= 0;
	var location="";
	if (tipoReporte == 1){
		 location = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteCompensaciones.action";
	}
	else if (tipoReporte == 2){
		 location  = "/MidasWeb/compensacionesAdicionales/reportes/generarReporteDerechoPoliza.action";
	}
	else if (tipoReporte == 3){
		 location = "/MidasWeb/compensacionesAdicionales/reportes/generarReportePagadasDevengar.action";
	}
	if(ramo ==null || ramo =='' || ramo =='0'){
		parent.mostrarVentanaMensaje("10","Seleccione un Ramo", null);
	}else{
		if(agente=='' && promotor=='' && proveedor=='' && fechaInicial=='' && fechaFin==''){
			parent.mostrarVentanaMensaje("10","Selecciones Algun otro filtro  para generar el reporte", null);
		}else{
			llenarDatosReporte(ramo,tipoReporte,agente,promotor,proveedor, fechaInicial, fechaFin,location);
		}
	}
}
function llenarDatosReporte(ramo,tipoReporte,agente,promotor,proveedor, fechaInicial, fechaFin, location){
	var tArchivo = dwr.util.getValue('tipoArchivo');
	var gerencia = dwr.util.getValue('gerencia');
	var idNegocio = dwr.util.getValue('idNegocio');
	var nombreNegocio = dwr.util.getValue('nombreNegocio'); 
	var numeroPoliza = dwr.util.getValue('IdToPoliza'); 
	
	if (tipoReporte != undefined && tipoReporte != null && tipoReporte != '')
		location = location +"?tipoReporte="+tipoReporte;
	if(tArchivo != undefined && tArchivo != null && tArchivo != '' )
		location = location+"&tipoArchivo="+tArchivo;
	if(ramo != undefined && ramo != null && ramo != '' )
		location = location+"&ramo="+ramo;
	if(agente != undefined && agente != null && agente != '' )
		location = location+"&agente="+agente;
	if(promotor != undefined && promotor != null && promotor != '' )
		location = location+"&promotor="+promotor;
	if(proveedor != undefined && proveedor != null && proveedor != '' )
		location = location+"&proveedor="+proveedor;
	if(gerencia != undefined && gerencia != null && gerencia != '' )
		location = location+"&gerencia="+gerencia;
	if(idNegocio != undefined && idNegocio != null && idNegocio != '' )
		location = location+"&idNegocio="+idNegocio;
	if(nombreNegocio != undefined && nombreNegocio != null && nombreNegocio != '' )
		location = location+"&nombreNegocio="+nombreNegocio;
	if (numeroPoliza != undefined && numeroPoliza != null && numeroPoliza != '')
		location = location+"&noPoliza="+numeroPoliza;	
	if(fechaInicial != undefined && fechaInicial != null && fechaInicial !='')
		location = location+"&fechaInicio="+fechaInicial;
	if(fechaFin != undefined && fechaFin != null && fechaFin !='')
		location = location+"&fechaFinal="+fechaFin;

	if (tipoReporte == 1){
		window.open(location,"generarReporteCompensaciones");
	}else if (tipoReporte == 2){
		window.open(location,"generarReporteDerechoPoliza");
	}else if (tipoReporte == 3){
		window.open(location,"generarReportePagadasDevengar");
	}
}   
function ajustarFechaFinal(){
		var dParts = dwr.util.getValue('fechaInicio').split('/');
		var endDate = (new Date(dParts[2] + '/' + dParts[1] + '/' +(+dParts[0]))).addMonths(1);
		jQuery('#txtFechaFinal').datepicker( "option", "maxDate", endDate );
		jQuery('#txtFechaFinal').datepicker( "option", "minDate", endDate );
		jQuery('#txtFechaFinal').val(endDate.toString('dd/MM/yyyy'));
}