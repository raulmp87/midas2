/**
 * 
 */
var CompensacionUtils = function(){
		
	/**
	 * Configuracion Inicial Compensacion
	 */
	 var configCompensacion = function () {
		
		/**
		* Valores Generales de la Configuracion
		*/
		var config = new ValoresGenerales();
					
		var urlRangosBajaSini, urlSubRamosDanios;		
		
		switch (config.ramo) {		
				
			case 'A':								
				bloquearControlesAUTOS(config);				
				var urlLineasNegocio = '/MidasWeb/compensacionesAdicionales/verLineasNegocio.action?ramo=A&idNegocio=' + config.idNegocio + '&contraprestacionB=' + false + "&idCompensacionActual=" + config.compensacionId;
				listarFiltradoGenerico(urlLineasNegocio,"subramosAutosGrid", null,null,null,null,null);				
				
				urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidad.action?ramo=A&idNegocio=' + config.idNegocio + '&contraprestacionB=' + false + "&entidadPersonaId=" + config.entidadPersonaId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGrid", null,null,null,null,null);
				
				break;
			
			case 'D':
				bloquearControlesDANOS(config);				
				urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidad.action?ramo=D&idCotizacion=' + config.idCotizacion + '&contraprestacionB=' + false + "&entidadPersonaId=" + config.entidadPersonaId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGrid", null,null,null,null,null);
			
				urlSubRamosDanios = '/MidasWeb/compensacionesAdicionales/verSubRamosDanios.action?ramo=D&idCotizacion=' + config.idCotizacion 
					+ '&idCotizacionEndoso=' + config.idCotizacionEndoso 
					+ "&entidadPersonaId=" + config.entidadPersonaId 
					+ "&idCompensacionActual=" + config.compensacionId
					+ '&idPoliza='+config.idPoliza
				    + '&idRecibo='+config.idRecibo;

				CompensacionUtils.parametros.loadGridSubRamosDanios(urlSubRamosDanios, 'divSubRamosDanios', config);
	  
				break;
			
			case 'V':		
				bloquearControlesVIDA(config);
				urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidad.action?ramo=V&idNegocioVida=' + config.idNegocioVida + '&contraprestacionB=' + false + "&entidadPersonaId=" + config.entidadPersonaId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGrid", null,null,null,null,null);
				break;
				
			case 'B':		
				bloquearControlesBANCA(config);
				urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidad.action?ramo=B&idConfiguracionBanca=' + config.idConfiguracionBanca + '&contraprestacionB=' + false + "&entidadPersonaId=" + config.entidadPersonaId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGrid", null,null,null,null,null);
				
				break;
		}
		
		
	 }
	 
	 /**
	  * Configuracion Inicial Contraprestacion
	  */
	 var configContraprestacion = function(){

		/**
		* Valores Generales de la Configuracion
		*/
		var configContra = CompensacionUtils.config();
			
		switch (configContra.ramo) {		
			
			case 'A':				
				bloquearControlesAUTOS(configContra);
				
				var urlLineasNegocio = '/MidasWeb/compensacionesAdicionales/verLineasNegocioContra.action?ramo=A&idNegocio=' + configContra.idNegocio + '&contraprestacionB=' + true;	
				listarFiltradoGenerico(urlLineasNegocio,"subramosAutosGridContra", null,null,null,null,null);				
				
				var urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidadContra.action?ramo=A&idNegocio=' + configContra.idNegocio+ '&contraprestacionB=' + true + "&entidadPersonaId=" + configContra.entidadPersonaContraId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGridContra", null,null,null,null,null);
			break;
			
			case 'D':
				bloquearControlesDANOS(configContra);
				
				var urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidadContra.action?ramo=D&idCotizacion=' + configContra.idCotizacion+ '&contraprestacionB=' + true + "&entidadPersonaId=" + configContra.entidadPersonaContraId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGridContra", null,null,null,null,null);	
			
				var urlSubRamosDanios = '/MidasWeb/compensacionesAdicionales/verSubRamosDanios.action?ramo=D&idCotizacion=' + configContra.idCotizacion 
					+ '&idCotizacionEndoso=' + configContra.idCotizacionEndoso 
					+ "&entidadPersonaId=" + configContra.entidadPersonaContraId
					+ "&idCompensacionActual=" + configContra.contraprestacionId
					+ '&idPoliza='+configContra.idPoliza
				    + '&idRecibo='+configContra.idRecibo;
					
				var Parametros = CompensacionUtils.parametros;
				Parametros.loadGridSubRamosDanios(urlSubRamosDanios, 'divSubRamosDaniosContra', configContra);			
				Parametros.porUtilidad.consultarPago();
				Parametros.porUtilidad.consultarProvision();
			break;
			
			case 'V':
				bloquearControlesVIDA(configContra);
				var urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidadContra.action?ramo=V&idNegocioVida=' + configContra.idNegocioVida+ '&contraprestacionB=' + true + "&entidadPersonaId=" + configContra.entidadPersonaContraId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGridContra", null,null,null,null,null);
				var urlProductosVida = '/MidasWeb/compensacionesAdicionales/verProductosVida.action';
				listarFiltradoGenerico(urlProductosVida,"productosVidaGrid", null,null,null,null,null);
				activarDesactivarPor();
				break;
			
			case 'B':
				bloquearControlesBANCA(configContra);
				
				var urlRangosBajaSini = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidadContra.action?ramo=B&idConfiguracionBanca=' + configContra.idConfiguracionBanca+ '&contraprestacionB=' + true + "&entidadPersonaId=" + configContra.entidadPersonaContraId;
				listarFiltradoGenerico(urlRangosBajaSini,"rangosSiniestralidadGridContra", null,null,null,null,null);
				
				var urlRangosCumplimientoMeta = '/MidasWeb/compensacionesAdicionales/verRangosSiniestralidadContra.action?ramo=B&idConfiguracionBanca=' + configContra.idConfiguracionBanca+ '&contraprestacionB=' + true + "&entidadPersonaId=" + configContra.entidadPersonaContraId+"&idTipoCompensacion=7";
				listarFiltradoGenerico(urlRangosCumplimientoMeta,"rangosSiniestralidadCumplimientoMeta", null,null,null,null,null);
				break;
		}
			
		
	 }
	 

	 /**
	  * AUTOS
	  * Bloquea los Controles de 
	  */
	 function bloquearControlesAUTOS(config){

		 
		 	if(config.isContraprestacion){
				var bloqueo;
				
		 		if(config.contraprestacionId > 0 || (config.compensacionId > 0  && config.estatusCompensacion !== config.COMPENSACION_AUTORIZADA)){	
		 			checkAutorizacionesContraprestacion(config, true);
					if(config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA){
						bloqueo = 0;
						jQuery("#btnAceptarContraGral").hide();
					}else{
						bloqueo = 1;
		 			//Boton de Agregar Agente
		 			jQuery("#btnAgregarAgenteContra").hide();
		 			//Boton de Guardado
		 			jQuery('#btnGuardarConfigGralContra').hide();
				}
		 		}else{		
		 			//Boton de Agregar Agente
		 			jQuery("#btnAgregarAgenteContra").show();		
		 			jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');	
		 			checkAutorizacionesContraprestacion(config, false);
					bloqueo = 0;
					initDataChecks(config);
		 		}
		 		/**
		 		 * @ REVISAR ESTA FUNCION
		 		 */
		 		bloquearConfiguracion(bloqueo,'bloquearConfContra');
		 		
		 	}else{
		 		var bloqueo;
		 		if(config.compensacionId > 0 || (config.contraprestacionId > 0 && config.estatusContraprestacion !== config.COMPENSACION_AUTORIZADA)){					
			 		
					checkAutorizacionesCompensacion(config, true);	
					
					if(config.estatusCompensacion === config.COMPENSACION_AUTORIZADA){
						bloqueo = 0;
						jQuery("#btnAceptarConfigGral").hide();
					}else{
						bloqueo = 1;
		 			//Boton de Agregar Agente
					jQuery("#btnAgregarAgente").hide();
					//Boton de Guardado	
					jQuery('#btnGuardarConfigGral').hide();
					}
				}else{		
					//Boton de Agregar Agente
					jQuery("#btnAgregarAgente").show();	
					jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');
					checkAutorizacionesCompensacion(config, false);
					bloqueo = 0;
					initDataChecks(config);
				}
		 		/**
		 		 * @ REVISAR ESTA FUNCION
		 		 */
		 		bloquearConfiguracion(bloqueo,'bloquearConf');	
		 	}
			jQuery('#openConfigurador').val("0");
	 }
	 
	 /**
	  * VIDA
	  * Bloquea los Controles de 
	  */
	 function bloquearControlesVIDA(config){
		 var COMISION_AGENTE = 1;
		 var BASE_CALCULO    = 5;
		 
		 if(config.isNewRecord){
			jQuery('#textFechaModificacionVida').hide();
			jQuery('#fechaModificacionVida').hide();
			jQuery('.ui-datepicker-trigger').hide();
		 }else if(config.estatusCompensacion === config.COMPENSACION_AUTORIZADA || config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA){
//			jQuery('#fechaModificacionVida').attr('disabled',false);
//			jQuery('#fechaModificacionVida').val('');
//			jQuery('.ui-datepicker-trigger').attr('disabled',false);			
		 }else{			
			jQuery('#fechaModificacionVida').attr('disabled',true);
			jQuery('.ui-datepicker-trigger').attr('disabled',true);
		 }
		 if(config.isContraprestacion){
			 var baseCalculo = jQuery("#selectBaseCalcBajaSiniContra").val();
			 if(config.comisionAgente == COMISION_AGENTE){
				 jQuery('#textPorcePrimaContra').hide();
			 }
			 if (baseCalculo != BASE_CALCULO){
				 jQuery('#l_FormulaPrimaRiesgo_contra').hide();
			 }
			var bloqueo;	
			
			if(config.contraprestacionId > 0 || (config.compensacionId > 0  && config.estatusCompensacion !== config.COMPENSACION_AUTORIZADA)){		 			
					 			
				checkAutorizacionesContraprestacion(config, true);		 
				
				if(config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA){
					bloqueo = 0;
					jQuery("#btnAceptarContraGral").hide();
				}else{
					bloqueo = 1;
					//Boton de Agregar Proveedor
					jQuery("#btnAgregarAgenteContra").hide();
					//Boton de Guardado	
					jQuery('#btnGuardarConfigGralContra').hide();
					jQuery("input[name='compensacionesDTO.comisionAgente']").attr('disabled',true);
					jQuery("input[name='compensacionesDTO.aplicaComision']").attr('disabled',true);
				}				
			}else{		
				//Boton de Agregar Proveedor
				jQuery("#btnAgregarAgenteContra").show();		
				jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');	
				checkAutorizacionesContraprestacion(config, false);
				bloqueo = 0;
				initDataChecks(config);
			}
			
			bloquearConfiguracion(bloqueo,'bloquearConfContra');
			
		 		
	 	}else{
			 var baseCalculo = jQuery('#selectBaseCalcBajaSiniGral').val();
			 if (baseCalculo != BASE_CALCULO){
				 jQuery('#l_FormulaPrimaRiesgo').hide();
			 }
	 		var bloqueo;
	 		if(config.compensacionId > 0 || (config.contraprestacionId > 0 && config.estatusContraprestacion !== config.COMPENSACION_AUTORIZADA) ){					
	 		
				checkAutorizacionesCompensacion(config, true);	
				
				if(config.estatusCompensacion === config.COMPENSACION_AUTORIZADA){
					bloqueo = 0;
					jQuery("#btnAceptarConfigGral").hide();
				}else{
					bloqueo = 1;
					//Boton de Agregar Agente
					jQuery("#btnAgregarAgente").hide();
					//Boton de Guardado	
					jQuery('#btnGuardarConfigGral').hide();
					jQuery("input[name='compensacionesDTO.comisionAgente']").attr('disabled',true);
					jQuery("input[name='compensacionesDTO.aplicaComision']").attr('disabled',true);
				}
				
			}else{		
				//Boton de Agregar Agente
				jQuery("#btnAgregarAgente").show();	
				jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');
				checkAutorizacionesCompensacion(config, false);
				bloqueo = 0;
				initDataChecks(config);
			}
	 		
	 		bloquearConfiguracion(bloqueo,'bloquearConf');	
	 	}
		jQuery('#openConfigurador').val("0");
	 }

	 /**
	  * DANOS
	  * Bloquea los Controles de 
	  */
	 function bloquearControlesDANOS(config){	 
		 	
		 	if(config.isContraprestacion){	

		 		if(config.contraprestacionId >0) {
	                  jQuery('#btnBuscaProveedor').hide();	
			 		}
		 	    
		 		switch (config.tipoAccion) {
				
			 		case '1':
			 			//Boton de Agregar Agente
			 			jQuery("#btnAgregarAgenteContra").hide();
			 			//Boton de Guardado
			 			jQuery('#btnGuardarConfigGralContra').hide();
			 			//Boton Recalcular
			 			jQuery('#btnRecalcularContraprestacionId').hide();	
			 			//Revisa las autorizaciones
			 			checkAutorizacionesContraprestacion(config, true);	
			 			bloquearConfiguracion(config.bloquearConfigContra,'bloquearConfContra');
						break;
					
		 			case '2':
			 			//Boton de Agregar Agente
			 			jQuery("#btnAgregarAgenteContra").show();		
			 			jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');		
			 			if(!config.contraprestacionId > 0){
				 			//Boton Recalcular
				 			jQuery('#btnRecalcularContraprestacionId').hide();	
			 			}			 			
			 		
			 			checkAutorizacionesContraprestacion(config, config.invocacionConfigurador === 'listadoConfiguraciones');
			 			
						break;
						
		 			case '3':
			 			//Boton de Agregar Agente
			 			jQuery("#btnAgregarAgenteContra").hide();
			 			//Boton de Guardado
			 			jQuery('#btnGuardarConfigGralContra').hide();	
			 			//Mostrar boton de recalcular
			 			checkAutorizacionesContraprestacion(config, false);
						break;				 		
				}

		 	
		 	}else{
		 				 		
		 		if(config.compensacionId >0) {
	                  jQuery('#btnBuscaAgenteCompensacionLink').hide();	
			 		}
		 		
      	 		switch (config.tipoAccion) {
				
			 		case '1':
			 			//Boton de Agregar Agente
						jQuery("#btnAgregarAgente").hide();
						//Boton de Guardado	
						jQuery('#btnGuardarConfigGral').hide();
						//Boton Recalcular
						jQuery('#btnRecalcularCompensacionId').hide();	
			 			//Revisa las autorizaciones
						checkAutorizacionesCompensacion(config, true);
						
						bloquearConfiguracion(config.bloquearConfigComp,'bloquearConf');
						break;
					
		 			case '2':
		 				//Boton de Agregar Agente
						jQuery("#btnAgregarAgente").show();	
						jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');	
						if(!config.compensacionId > 0){
				 			//Boton Recalcular
				 			jQuery('#btnRecalcularCompensacionId').hide();	
			 			}
						
						checkAutorizacionesCompensacion(config, config.invocacionConfigurador === 'listadoConfiguraciones');
						
						break;
						
		 			case '3':
		 				//Boton de Agregar Agente
						jQuery("#btnAgregarAgente").show();	
						jQuery('#btnGuardarConfigGral').hide();	
						jQuery('#btnDocumentosCompensacionLink').css('disabled','disabled');	
						checkAutorizacionesCompensacion(config, false);
						break;
		 		}	
		 		
		 	}
	 }
	 
	 
	 /**
	  * BANCA
	  * Bloquea los Controles de 
	  */
	 function bloquearControlesBANCA(config){
		 /**
		  * @ Analizar
		  */
		 bloquearControlesAUTOS(config);
	 }
	 
	 /**
	  * Activa o Desactiva los Check de Autorizaciones de Compensaciones
	  */
	 function checkAutorizacionesCompensacion(config, valida){
	 		
		 if(valida){
			 
			 	/**
		 		* Boton Aceptar (Valida la Autorizacion Checkeada)
		 		*/
		 		jQuery("#btnAceptarConfigGral").hide();
		 			
		 		if( (config.fnTecnico || config.fnAgente) && config.compensacionId > 0 ){
		 			jQuery("#btnAceptarConfigGral").show();
		 		}	
		 		
		 		/**
		 		*Desabilita los Check de Autorizaciones deacuerdo a permisos y si ya estan cheked
		 		*/	
		 		jQuery('input[name="compensacionesDTO.listTipoAutorizacionca"]').each(function(index) {        
		 	
		 			if(config.AUTORIZACION_TECNICA === jQuery(this).val() && config.fnTecnico){
		 				if(jQuery(this).is(":checked")) {						
		 		        	jQuery(this).attr("disabled", "disabled");
		 		        	jQuery(jQuery('input[name="compensacionesDTO.listTipoAutorizacionca"]')[1]).attr("disabled", "disabled");
		 		        }
		 			}else if(config.AUTORIZACION_TECNICA === jQuery(this).val()){
		 				jQuery(this).attr("disabled", "disabled");
		 			}
		 			
		 			if(config.AUTORIZACION_AGENTE === jQuery(this).val() && config.fnAgente){
		 				if(jQuery(this).is(":checked")) {						
		 		        	jQuery(this).attr("disabled", "disabled");
		 		        	jQuery(jQuery('input[name="compensacionesDTO.listTipoAutorizacionca"]')[0]).attr("disabled", "disabled");
		 		        }
		 			}else if(config.AUTORIZACION_AGENTE === jQuery(this).val()){
		 				jQuery(this).attr("disabled", "disabled");
		 			}        
		 	        
		 		});
		 		
		 }else{
			 
			jQuery("#btnAceptarConfigGral").hide();	
			 	
		 	/**
		 	*Desabilita los Check de Autorizaciones
		 	*/	
		 	jQuery('input[name="compensacionesDTO.listTipoAutorizacionca"]').each(function(index) {        
		     	jQuery(this).attr("disabled", "disabled"); 	        
		 	});
		 }

	 }

	 
	 /**
	  * Activa o Desactiva los Check de Autorizaciones de Contraprestaciones
	  */
 	 function checkAutorizacionesContraprestacion(configContra, valida){
 		
 		if(valida){	
 			/**
 			*Oculta o desabilita controles si la compensacion ya existe
 			*/
 			jQuery("#btnAceptarContraGral").hide();
 			if( (configContra.fnTecnico || configContra.fnAgente || configContra.fnJuridico) && configContra.contraprestacionId > 0){
 				jQuery("#btnAceptarContraGral").show();
 			}
 			/**
 			*Desabilita los Check de Autorizaciones deacuerdo a permisos y si ya estan cheked
 			*/	
 			jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]').each(function(index) {        
 				
 				if(configContra.AUTORIZACION_TECNICA === jQuery(this).val() && configContra.fnTecnico){
 					if(jQuery(this).is(":checked")) {
 			        	jQuery(this).attr("disabled", "disabled");
 			        	jQuery(jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]')[1]).attr("disabled", "disabled");
 			        }
 				}else if(configContra.AUTORIZACION_TECNICA === jQuery(this).val()){
 					jQuery(this).attr("disabled", "disabled");
 				}
 				
 				if(configContra.AUTORIZACION_AGENTE === jQuery(this).val() && configContra.fnAgente){
 					if(jQuery(this).is(":checked")) {
 			        	jQuery(this).attr("disabled", "disabled");
 			        	jQuery(jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]')[0]).attr("disabled", "disabled");
 			        }
 				}else if(configContra.AUTORIZACION_AGENTE === jQuery(this).val()){
 					jQuery(this).attr("disabled", "disabled");
 				}
 				
 				if(configContra.AUTORIZACION_JURIDICO === jQuery(this).val() && configContra.fnJuridico){
 					if(jQuery(this).is(":checked")) {
 			        	jQuery(this).attr("disabled", "disabled");
 			        }
 				}else if(configContra.AUTORIZACION_JURIDICO === jQuery(this).val()){
 					jQuery(this).attr("disabled", "disabled");
 				}        
 				
 			});
 		
 		}else{
 			jQuery("#btnAceptarContraGral").hide();					
 			/**
 			*Desabilita los Check de Autorizaciones
 			*/	
 			jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]').each(function(index) {        
 		    	jQuery(this).attr("disabled", "disabled"); 	        
 			});
 		}
 	 }
 	 
	 
	 /**
	  * Valores Generales de la Configuracion
	  */
	 var ValoresGenerales = function(){
	 	
		this.idNegocio = Number(jQuery('#varIdNegocio').val());
	 	this.idCotizacion = Number(jQuery('#varIdCotizacion').val());
	 	this.idNegocioVida = Number(jQuery('#varIdNegocioVida').val());
	 	this.idNegocioVidaIsNew = Number(jQuery('#varIdNegocioVidaIsNew').val()) === 0;
	 	this.idNegocioAutosIsNew = Number(jQuery('#varIdNegocioAutosIsNew').val()) === 0;
	 	this.idConfiguracionBanca = Number(jQuery('#varIdConfiguracionBanca').val());
	 	this.idPoliza = Number(jQuery('#varIdPoliza').val());
	 	this.ramo = jQuery('#varRamo').val();
	 	this.tabActiva = jQuery('#varTabActiva').val();
	 	this.isNewRecord = Boolean(Number(jQuery("#varCompensacionNueva").val()));
	 	this.fnTecnico = jQuery('#varFnTecnico').val() === "true" ? true : false;
	 	this.fnAgente = jQuery('#varFnAgente').val() === "true" ? true : false;
	 	this.fnJuridico = jQuery('#varFnJuridico').val() === "true" ? true : false;
	 	this.jsonCompensacion = jQuery('#jsonCompensacionId').val();
	 	this.jsonContraprestacion = jQuery('#jsonContraprestacionId').val();
	 	this.compensacionId = Number(jQuery('#varCompensacionId').val());
	 	this.contraprestacionId = Number(jQuery('#varContraprestacionId').val());
	 	this.isContraprestacion = Boolean(Number(jQuery('#varIsContraprestacion').val())); 
	 	this.entidadPersonaId = isNaN(Number(jQuery('#selectAgregarAgteGral').val())) ? 0 : Number(jQuery('#selectAgregarAgteGral').val());
	 	this.entidadPersonaContraId = isNaN(Number(jQuery('#selectAgregarAgteContra').val())) ? 0 : Number(jQuery('#selectAgregarAgteContra').val());
	 	this.claveAgente = Number(jQuery('#textClaAgeGral').val());
	 	this.clavePromotor = Number(jQuery('#textClavePromotorGral').val());
	 	this.claveProveedor = Number(jQuery('#textClaProveContra').val());
	 	this.bloquearConfigContra = Number(jQuery('#bloquearConfContraId').val());
	 	this.bloquearConfigComp = Number(jQuery('#bloquearConfCompId').val());
	 	this.tipoAccion = jQuery('#varTipoAccion').val();
	 	this.numeroEndoso = Number(jQuery('#varNumeroEndoso').val());
	 	this.idCotizacionEndoso = Number(jQuery('#varIdCotizacionEndoso').val());
	 	this.invocacionConfigurador = jQuery('#varInvocacionConfigurador').val();
	 	this.varVidaInd = jQuery('#varVidaIndividual').val();
	 	this.porcentajeGralComp = jQuery('#textPorcePrimaGral').val();
	 	this.porcentajeGralContra = jQuery('#textPorcePrimaContra').val();
	 	this.estatusCompensacion = isNaN(Number(jQuery('#varEstatusCompensacion').val())) ? 0 : Number(jQuery('#varEstatusCompensacion').val());
	 	this.estatusContraprestacion = isNaN(Number(jQuery('#varEstatusContraprestacion').val())) ? 0 : Number(jQuery('#varEstatusContraprestacion').val());
	 	this.isComisionOriginal = Boolean(Number(jQuery("input[name='compensacionesDTO.comisionAgente']:checked").val()));
		this.openConfigurador = Boolean(Number(jQuery("#openConfigurador").val()));
		this.idRecibo = isNaN(Number(jQuery('#idRecibo').val())) ? 0 : Number(jQuery('#idRecibo').val());
		this.topeMaximo = Number(jQuery('#topeMaximo').val());
		this.formaPagoBanca = jQuery('#formaPagoBanca').val();
		this.comisionAgente = jQuery('#varComisionAgente').val();
		
		
	 	this.AUTORIZACION_TECNICA = "1";
	 	this.AUTORIZACION_AGENTE = "2";
	 	this.AUTORIZACION_JURIDICO = "3";
	 	
	 	this.COMPENSACION_AUTORIZADA = 5;
	 }
	 
	 /**
	  * Funcionalidades de la Configuracion
	  */
	 var ParametrosConfiguracion = function(){
		 
		 this.porUtilidad = new function(){
			 
			 var $montoUtilidadTotal = jQuery('#textMontoUtilidadTotal');
			 var $porcentajeUtilidad = jQuery('#textPorceParticiUtilidadContra');
			 var $montoUtilidad = jQuery('#textMontoParticiUtilidadContra');
			 var $parcialidad = jQuery('#textMontoParcialidad');
			 var $montoPagado = jQuery('#textMontoPagado');
			 var $montoDevengar = jQuery('#textMontoPorDevengar');
			 var $botonSolicitarPago = jQuery('#botonSolicitarPagoUtiliad');
			 var $montoUtilidadProvision = jQuery('#montoUtilidadProvision');
			 
			 this.calcularMontoParticipacion = function (){
				 var montoTotal = Number($montoUtilidadTotal.val());
				 var porcentaje = Number($porcentajeUtilidad.val());
				 var result = (porcentaje * montoTotal) / 100;	
				 if(result > 0){
					 $montoUtilidad.val(result);
				 }		
			 }

			 this.provisionar = function (){
				 	var monto = Number($montoUtilidad.val());
				 	var montoTotal=Number($montoUtilidadTotal.val());
			 		var varGenerales = new ValoresGenerales();
			 		var url = '/MidasWeb/compensacionesAdicionales/provisionar.action?idCotizacion='+ varGenerales.idCotizacion+'&entidadPersonaId=' + varGenerales.entidadPersonaContraId+'&compensacionesDTO.montoUtilidadesUtilidad='+monto+'&compensacionesDTO.montoDeLasUtilidades='+montoTotal;
			 		sendRequestJQ(null, url , 'mensajesCompensacionesAdicionalesId', null);
			
			}	
			 
			 this.solicitarPago = function (){
				 	var monto = Number($parcialidad.val());
			 		var varGenerales = new ValoresGenerales();
			 		var url = '/MidasWeb/compensacionesAdicionales/solicitarPagoUtilidad.action?idPoliza='+ varGenerales.idPoliza+'&entidadPersonaId=' + varGenerales.entidadPersonaContraId+'&compensacionesDTO.montoDeLasUtilidades='+monto;
			 		sendRequestJQ(null, url , 'mensajesCompensacionesAdicionalesId', 'CompensacionUtils.parametros.porUtilidad.consultarPago();');

			}
			 
			this.consultarPago = function (){
				var varGenerales = new ValoresGenerales();
				var urlConsultarPagoUtilidad = '/MidasWeb/compensacionesAdicionales/consultarPagoUtilidad.action?'+'idCompensacionActual='+varGenerales.contraprestacionId+'&entidadPersonaId='+varGenerales.entidadPersonaContraId;
				jQuery.ajax ({  
				    url: urlConsultarPagoUtilidad,
				    type: 'POST',
				    dataType: 'text',
				    success: function (data) {
				    	jQuery('#textMontoPagado').val(data);
				    	jQuery('#textMontoParcialidad').val('');
				    	CompensacionUtils.parametros.porUtilidad.calcularMontoDevengar();
				    }
				});				
			}
			
			this.consultarProvision = function (){
				var varGenerales = new ValoresGenerales();
				var urlConsultarPagoUtilidad = '/MidasWeb/compensacionesAdicionales/consultarProvision.action?'+'idCompensacionActual='+varGenerales.contraprestacionId+'&entidadPersonaId='+varGenerales.entidadPersonaContraId;
				jQuery.ajax ({  
				    url: urlConsultarPagoUtilidad,
				    type: 'POST',
				    dataType: 'text',
				    success: function (data) {				    	
				    	jQuery('#montoUtilidadProvision').val(data);
				    	
				    	var montoActual = Number(jQuery('#textMontoParticiUtilidadContra').val());
				    	var montoProvision = Number(data);
				    	CompensacionUtils.parametros.porUtilidad.calcularMontoDevengar();
				    	jQuery('#botonProvisionarUtiliad').unbind();
				    	if(!isNaN(montoActual) && !isNaN(montoProvision) && montoActual !== montoProvision ){
				    		jQuery('#botonProvisionarUtiliad').click(function (){
						    	montoActual = Number(jQuery('#textMontoParticiUtilidadContra').val());
						    	var montoPagado= Number(jQuery('#textMontoPagado').val());
				    			if(	montoActual>=montoPagado ){
				    				CompensacionUtils.parametros.porUtilidad.provisionar();
				    			}else{
				    				alert('El monto de utilidad debe ser mayor o igual al monto pagado');
				    			}
				    		});
				    	}
				    }
				});				
			}
			
			this.calcularMontoDevengar = function(){
				var res = Number(jQuery('#textMontoParticiUtilidadContra').val()) - Number(jQuery('#textMontoPagado').val());
				jQuery('#textMontoPorDevengar').val(res);				
			}

			this.cambioProvision = function(){
				var montoDevengar = Number($montoDevengar.val());
		        var value = Number($montoUtilidadTotal.val());
		        if(isNaN(value)){
		          $montoUtilidadTotal.val('0');      
		        }        
		        this.calcularMontoParticipacion();
		        this.calcularMontoDevengar();
		        this.consultarProvision();
				if($montoDevengar.val()<0){
					alert('El monto de utilidad debe ser mayor o igual al monto pagado');
				}
		      }

			this.cambioPorcentaje = function(id){
				var value = Number(jQuery('#'+id).val());
				if(isNaN(value) || value > 100){
					jQuery('#'+id).val('');
				}else{
					this.calcularMontoParticipacion();
					this.calcularMontoDevengar();
				}
			}
			
			this.cambioMontoParcialidad = function(obj){
				var value = Number(jQuery(obj).val());
				var montoDevengar = Number($montoDevengar.val());
				if(isNaN(value)){
					jQuery(obj).val('');
					jQuery('#botonSolicitarPagoUtiliad').unbind();
				}else if(value > montoDevengar){
					alert('La parcialidad es mayor al monto por devengar');
					jQuery(obj).val('');
				}else if(value > 0){
					jQuery('#botonSolicitarPagoUtiliad').unbind();
					jQuery('#botonSolicitarPagoUtiliad').click(function(){
						CompensacionUtils.parametros.porUtilidad.solicitarPago();
					});
				}else{
					jQuery('#botonSolicitarPagoUtiliad').unbind();
				}
			}
			
			
		 }
		 
		 this.calcularSubRamosDanios = function()	{
			var config = CompensacionUtils.config();
			var urlSubRamosDanios;
		
			if(config.isContraprestacion){
				
		 		urlSubRamosDanios = '/MidasWeb/compensacionesAdicionales/calcularProvisionDanios.action?ramo=D&idCotizacion=' 
		 			+ config.idCotizacion 
					+ '&idCotizacionEndoso=' + config.idCotizacionEndoso 
					+ "&entidadPersonaId=" + config.entidadPersonaContraId
					+ "&idCompensacionActual=" + config.contraprestacionId
					+ '&idPoliza='+config.idPoliza
					+ '&idRecibo='+config.idRecibo
					+ getListSubRamosDaniosViewWithParam();
					
		 		this.loadGridSubRamosDanios(urlSubRamosDanios, 'divSubRamosDaniosContra', config);
				
			}else{

			    urlSubRamosDanios = '/MidasWeb/compensacionesAdicionales/calcularProvisionDanios.action?ramo=D&idCotizacion=' 
			 		+ config.idCotizacion 
					+ '&idCotizacionEndoso=' + config.idCotizacionEndoso 
					+ "&entidadPersonaId=" + config.entidadPersonaId 
					+ "&idCompensacionActual=" + config.compensacionId
					+ '&idPoliza='+config.idPoliza
					+ '&idRecibo='+config.idRecibo
					+ getListSubRamosDaniosViewWithParam();
				
			    this.loadGridSubRamosDanios(urlSubRamosDanios, 'divSubRamosDanios', config);

			}	
		 }
		 
		 this.loadGridSubRamosDanios = function (url, div, config){
			 var grid = new dhtmlXGridObject(div);
			 grid.attachEvent("onXLE", function(grid,count){
				 CompensacionUtils.parametros.sumCalculosSubRamos(config);
			 }); 
			 
			if(config.isContraprestacion){
				jQuery('#totalContraprestacionId').val(0);
				jQuery('#textPorcePrimaContra').val(0);
			}else{
				jQuery('#totalCompensacionId').val(0);
				jQuery('#textPorcePrimaGral').val(0);
			}
			grid.load(url);
			
			return grid;
		 }
		 
		 this.sumCalculosSubRamos = function(config){
			var listSubRamos = getListSubRamosDaniosView();
			var sum = 0;
			var sumBaseCal = 0;
			var porcentajeGral = 0;
			for (var i = 0; i < listSubRamos.length; i++) {
			 sum = sum + Number(listSubRamos[i].porcentajeCompensacionImporte);
			 sumBaseCal = sumBaseCal + Number(listSubRamos[i].primaBase);
			}   
		
			porcentajeGral = (sum/sumBaseCal)*100;
			if(config.isContraprestacion){        
				jQuery('#totalContraprestacionId').val(sum);        
				jQuery('#textPorcePrimaContra').val(isNaN(porcentajeGral) ? 0 : porcentajeGral);
			    
			}else{
				jQuery('#totalCompensacionId').val(sum);
				jQuery('#textPorcePrimaGral').val(isNaN(porcentajeGral) ? 0 : porcentajeGral);
			}
		 }		 		 
		 		
		 this.validarPorcentaje = function(id){
			 var porcentaje = Number(jQuery('#'+id).val());			 
			 if(isNaN(porcentaje) || porcentaje > 100){
				 jQuery('#'+id).val('');
			 }else if(Number(porcentaje.toFixed()) == 0 && porcentaje > 0){
				 jQuery('#'+id).val(porcentaje);
			 }			 
		 }
		 
		 this.validarPorcentajeSubRamos = function(element){  
	       var value = Number(jQuery(element).val());
	       
	       if(isNaN(value) || Number(value)>100){
	         jQuery(element).val('0');       
	       }
	     }
	 
	 }//End Parametros
	 
	 

	 
	 /**
	  * Funcion Generica para obtener los datos de una tabla
	  * @param Nombre del Div en el cual esta el grid
	  */
	 var getDataTable = function(divTable){
		 var objectArray, subArray;
		 objectArray = new Array();
		 var filas, columnas, celda, valor;		 
		 filas = jQuery('#'+divTable).find('table.obj').find('tr');	
		 
		 for (var f = 0; f < filas.length;f++) {
			 columnas = filas[f].children;
			 subArray = new Array();
			 for(var c = 0; c< columnas.length; c++){			
				 celda = columnas[c];				 
				 if(celda.tagName === 'TD'){
					 if(celda.childElementCount > 0){
						 if(jQuery(celda).find('input')){
							 valor = jQuery(celda).find('input').val();
						 }						
					 }else{
						 valor = celda.innerHTML;
					 }
					 valor = (valor === '' || typeof valor === 'undefined') ? 0 : valor;
					 subArray.push(valor);
				 }				 
			 }
			 if(subArray.length>0){
				 objectArray.push(subArray);
			 }
		 }
		 
		 return objectArray;
	 }
	 
	 /**
	  * Autoriza Compensaciones
	  */	 
	 var autorizarCompensacion = function (){
		
		var configActual = new ValoresGenerales();		
		var $autorizaciones;	
		var contraprestacion = '&contraprestacionB='+ configActual.isContraprestacion;
		var ramoId = 'ramo='+configActual.ramo;
		switch (configActual.ramo) {
		
			case 'A':
				identificador = '&idNegocio=' +configActual.idNegocio;
				break;
			
			case 'D':
				identificador = '&idCotizacion=' +configActual.idCotizacion;
				break;
				
			case 'V':
				identificador = '&idNegocioVida=' +configActual.idNegocioVida;
				break;
				
			case 'B':
				identificador = '&idConfiguracionBanca=' +configActual.idConfiguracionBanca;
				break;
		}
		
	 	
		try{
			if(configActual.isContraprestacion){			
				compensacionId = '&compensacionesDTO.compensacionId='+ configActual.contraprestacionId;		
				$autorizaciones = jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]:checked').not(':disabled');
				var autorizar = false;	
				if($autorizaciones.val() === configActual.AUTORIZACION_JURIDICO){
					
					jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]').each(function(index) {        
						if(configActual.AUTORIZACION_TECNICA === jQuery(this).val() || configActual.AUTORIZACION_AGENTE === jQuery(this).val()){
							if(jQuery(this).is(":checked")) {
								autorizar = true;
								data = '&compensacionesDTO.tipoAutorizacionca.id='+$autorizaciones.val();
					        }
						} 			        
					});		
					if(autorizar){
						var urlAutorizarCompensacion = "/MidasWeb/compensacionesAdicionales/autorizarCompensacion.action?" + ramoId + identificador + compensacionId + contraprestacion + data;
						sendRequestJQ(null, urlAutorizarCompensacion, null,   null);	
						jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]:checked').not(':disabled').attr('disabled', 'disabled');
					}else{
						alert('No es posible autorizar se necesita la aprobacion por el Director Tecnico o Administrador de Agentes');
					}
				}else{
					data = '&compensacionesDTO.tipoAutorizacionca.id='+$autorizaciones.val();
					if($autorizaciones.length > 0){
						var urlAutorizarCompensacion = "/MidasWeb/compensacionesAdicionales/autorizarCompensacion.action?" + ramoId + identificador + compensacionId + contraprestacion + data;
						sendRequestJQ(null, urlAutorizarCompensacion, null,   null);	
						jQuery('input[name="compensacionesDTO.listTipoAutorizacionco"]:checked').not(':disabled').attr('disabled', 'disabled');
					}else{
						alert('Seleccione un tipo de autorizacion');
					}
				}
				
				
				
			}else{
				$autorizaciones = jQuery('input[name=compensacionesDTO.listTipoAutorizacionca]:checked').not(':disabled');
				
				if($autorizaciones.length > 0){				
					compensacionId = '&compensacionesDTO.compensacionId='+ configActual.compensacionId;
					data = '&compensacionesDTO.tipoAutorizacionca.id='+$autorizaciones.val();					
					var urlAutorizarCompensacion = "/MidasWeb/compensacionesAdicionales/autorizarCompensacion.action?" + ramoId + identificador + compensacionId + contraprestacion + data;
					sendRequestJQ(null, urlAutorizarCompensacion, null,   null);
					jQuery('input[name=compensacionesDTO.listTipoAutorizacionca]').attr('disabled', 'disabled');
				}else{
					alert('Seleccione un tipo de autorizacion');
				}
			}
		}catch(e){
			alert('Ocurrio un error seleccione un tipo de autorizacion y de click en aceptar');
		}
		
	 }
	 
	 /**
	  * Retorna el Filtro para hacer la busqueda de Agente
	  */
	 var filtroAgente = function(){
		 var ramo = jQuery('#varRamo').val();
		 var filtro = 'AGENTE';		 
		 if(ramo === 'V'){
			 filtro = 'AGENTE_SEYCOS'; 
		 }		 
		 return filtro;
	 }
	 
	 return {		 
		 initCompensacion: function(){
			configCompensacion();
		 },	
		 initContraprestacion: function(){
			 configContraprestacion();
		 },	
		 parametros: new ParametrosConfiguracion(),
		 config: function(){
			 return new ValoresGenerales();
		 },
		 getDataTable: function(divTable){
			return getDataTable(divTable);
		 },
		 autorizarCompensacion: function(){
			 autorizarCompensacion();
		 },
		 getAgenteABuscar:function(){
			return filtroAgente();
		 }
		 
	 }
}();


/**
*Variable Global que encapsula los valores y funciones
*/
var CompensacionAdicional = CompensacionUtils;
var selectComision=false;
var tabsCompensacion;


var initTabsCompensacion = function (){
	
	var configuracion = CompensacionUtils.config();
	tabsCompensacion = new dhtmlXTabBar("configuracionCompensacionTabBar","top");
	tabsCompensacion.setImagePath("/MidasWeb/img/imgs_tabbar/");
	tabsCompensacion.setStyle('default');
	tabsCompensacion.setSkinColors('#FCFBFC','#F4F3EE');
	tabsCompensacion.setSize();
	tabsCompensacion.addTab("compensacion","Compensaciones");
	tabsCompensacion.addTab("contraprestacion","Contraprestaciones");
	
	/**
	 * Muestra el tab seleccionado por primera vez
	 */
	if(configuracion.ramo === 'B'){
		tabsCompensacion.setTabActive('contraprestacion');
		tabsCompensacion.disableTab('compensacion');
		verContraprestacionesConfig();
	}else if(configuracion.tabActiva === 'compensacion'){
		tabsCompensacion.setTabActive('compensacion');				
		verCompensacionConfig();	
	}else if(configuracion.tabActiva === 'contraprestacion'){
		tabsCompensacion.setTabActive('contraprestacion');
		if(configuracion.ramo === 'V' && (configuracion.isNewRecord || configuracion.formaPagoBanca!='')){
			tabsCompensacion.disableTab('compensacion');
			mostrarFormaPago();
		}
		if(configuracion.ramo == 'V' && configuracion.comisionAgente == 1){
			tabsCompensacion.disableTab('compensacion');
		}
		verContraprestacionesConfig();
	}else{
		tabsCompensacion.setTabActive('compensacion');
		verCompensacionConfig();	
	}
	
	tabsCompensacion.attachEvent("onSelect", function(id, lastId){
		
		var config = CompensacionUtils.config();
		
		var isNewRecord = config.isNewRecord;
	 	var compensacionId = config.compensacionId;
	 	var contraprestacionId = config.contraprestacionId;
	 	
	 	var select = true;
	 		
	 	var validacionVida = config.ramo === 'V' && (config.estatusCompensacion === config.COMPENSACION_AUTORIZADA || config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA);
	 	var validacionAutos = config.ramo === 'A' && (config.estatusCompensacion === config.COMPENSACION_AUTORIZADA || config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA);
		switch (id) {
			case 'compensacion':
				if(validacionVida||validacionAutos){
					select = enviarAGuardar(select, false);
				}else if(contraprestacionId > 0){
					verCompensacionConfig();
				}else if(!isNewRecord){
					verCompensacionConfig();					
				}else{
					select = enviarAGuardar(select, false);
				}				
				break;
				
			case 'contraprestacion':
				if(validacionVida || validacionAutos){
					select = enviarAGuardar(select, true);
				}else if(compensacionId > 0){
					verContraprestacionesConfig();		
				}else if(!isNewRecord){
					verContraprestacionesConfig();
				}else{
					select = enviarAGuardar(select, true);
				}				
				break;
		}
		
	    return select;
	});
	
	return tabsCompensacion;
}

function enviarAGuardar(select, isContraprestacion){
	if(!selectComision){
		var resultConfirm = confirm("Está a punto de abandonar la sección puede perder los datos previamente configurados."
				 +"\n¿Desea continuar para posteriormente guardar?");					
		if(resultConfirm){
			resultConfirm = confirm("Continuar y guardar - 'Aceptar'\n"+"Continuar sin guardar - 'Cancelar'");									 
			if(resultConfirm){				
				select = guardarCompensacionesAdicionales(!isContraprestacion, true, select);
			}else{					
				if(isContraprestacion){
					jQuery('#jsonContraprestacionId').val('');					
					verContraprestacionesConfig();
				}else{
					jQuery('#jsonCompensacionId').val('');												
					verCompensacionConfig();
				}
			}
		}else{
			select = false;
		}
	}else{
		if(isContraprestacion){
			jQuery('#jsonContraprestacionId').val('');					
			verContraprestacionesConfig();
		}else{
			jQuery('#jsonCompensacionId').val('');												
			verCompensacionConfig();
		}
	}
	selectComision=false;
	return select;
}

function validacionGuardadoCompensaciones(configCA, flagTab, select){
	
	/**
	 * Objecto validaciones 
	 * resultCompensacion: indica si continua con el guardado
	 * select: indica si cambia de pestaña, esto cuando se guarda la informacion desde el cambio de pestaña
	 */
	var validaciones = {resultCompensacion: true, select: select};
	
	try{
		switch (configCA.ramo) {
			
			case 'A':	
				/**
				*Cuando es el Ramo Autos una vez guardada ya no se permitiran modificaciones
				*/	
				if(configCA.idNegocio > 0 && (config.estatusCompensacion === config.COMPENSACION_AUTORIZADA || config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA)){	
					/**
					 * Si el negocio ya existe y se trata de una modificacion ala configuracion
					 */
					if(!configCA.idNegocioAutosIsNew){
						unblockPage();
						var res = confirm('Al guardar, se cancelarán las órdenes de pago existentes. \n'
								+'Se generarán nuevas órdenes de pago considerando la nueva configuración \n '
								+'¿Desea continuar?');
						if(res){
							blockPage();
						}else{
							validaciones.select = false;
							validaciones.resultCompensacion = false;
						}
					}
				}else if(!flagTab){
					unblockPage();
					validaciones.resultCompensacion = confirm("Una vez guardada la configuración de la Compensacion no podrán hacerse modificaciones. ¿Desea continuar?");
					blockPage();
				}
				break;
			
			case 'D':				
				break;
			
			case 'V':	
				/**
				*Cuando es el Ramo Vida se tiene que guardar antes el negocio creado y despues la configuracion
				*/				
				if(configCA.idNegocioVida > 0 && (config.estatusCompensacion === config.COMPENSACION_AUTORIZADA || config.estatusContraprestacion === config.COMPENSACION_AUTORIZADA)){	
					/**
					 * Si el negocio ya existe y se trata de una modificacion ala configuracion
					 */
					if(!configCA.idNegocioVidaIsNew){
						jQuery('#varIdNegocioVida').val(configCA.idNegocioVida);
						if(!fechaValida){
							validaciones.select = false;
							validaciones.resultCompensacion = false;
							unblockPage();
							alert('Fecha de Inicio Vigencia No Valida, Favor de Capturar');
						}else{
							unblockPage();
							var res = confirm('Al guardar, se cancelarán las órdenes de pago existentes. \n'
								 +'Se generarán nuevas órdenes de pago considerando la nueva configuración \n '
								 +'y la fecha de inicio de vigencia de la modificación. \n'
								 +'¿Desea continuar?');
							if(res){
								blockPage();
							}else{
								validaciones.select = false;
								validaciones.resultCompensacion = false;
							}
						}
					}
				}else{
					/**
					*El guardado del negocio vida debe regresar el idNegocioVida
					*/	
					var idNegocioVida = window.opener.guardarConfiguracionNegocioVida();
					
					if(idNegocioVida != null){
						jQuery('#varIdNegocioVida').val(idNegocioVida);
					}else{
						validaciones.resultCompensacion = false;
						validaciones.select = false;
						unblockPage();
						alert('Ocurrio un error al guardar la configuracion de Vida por favor revise los campos');
						window.close();
					}
				}		
				break;
				
			case 'B':				
				break;
		}
	}catch(e){
		
	}
	
	return validaciones;
}


function guardarCompensacionesAdicionales(isContraprestacion, flagTab, select){
	
	blockPage();
	
	var configCA = CompensacionAdicional.config();
		
	/**
	 * Validaciones Por Ramo
	 */
	var validaciones = validacionGuardadoCompensaciones(configCA, flagTab, select);
	
	select = validaciones.select;
	
	if(validaciones.resultCompensacion){
				
		if(isContraprestacion){	
			if(validateContraprestacion(isContraprestacion, configCA)){
				guardarCA(flagTab, isContraprestacion);
				select = true;
			}else if(flagTab){
				select = false;
				unblockPage();
			}else{
				unblockPage();
			}
		} else{	
			
			if(validateCompensacion(isContraprestacion, configCA)){
				guardarCA(flagTab, isContraprestacion);
			}else if(flagTab){
				select = false;
				unblockPage();
			}else{
				unblockPage();
			}			
		}
	}else{
		unblockPage();
	}
	
	return select;
}
	
function guardarCA(flagTab, isContraprestacion){
	
	var configCA = CompensacionAdicional.config();
	var data = getArrayCompensacionesView();
	var jsonObject = JSON.parse(data);
	var arrayObjects = jsonObject['dataGson'];
	
	if(configCA.ramo === 'V'){
		for(i = 0; i<arrayObjects.length;i++){
			arrayObjects[i].idNegocioVida = configCA.idNegocioVida;
		}
	}
	
	var stringGson = JSON.stringify(arrayObjects);
	
	jQuery.ajax({
		url: "/MidasWeb/compensacionesAdicionales/guardarCompensacion.action",
		data: JSON.stringify({'dataGson':stringGson}),
		dataType: 'json',
		contentType: 'application/json',
		type: 'POST',
		async: true,
		success: function (res) {
		    console.log("Se ha guardado la compensacion");
		    unblockPage();
		},
		error: function (res) {
		    console.log("Ocurrio un error al guardar la compensacion");		
		    unblockPage();
		},
		complete: function(jqXHR) {				
			if(flagTab){
				unblockPage();
				if(isContraprestacion){
					verCompensacionConfig();
				}else{
					verContraprestacionesConfig();
				}
			}else{
				unblockPage();
				window.close();
				/**
				 * Se envia flag para mostrar 
				 * mensaje informativo para vida
				 */
				if(configCA.ramo === 'V'){
					window.opener.wCloseCompensacion(configCA.isNewRecord);
				}else{
					window.opener.wCloseCompensacion();
				}
			}
		}
	});
	
}
	
	
function validateCompensacion(isContraprestacion, configCA){	
	var validate = checkCamposPorPrima(configCA) 
					&& checkCamposBajaSini(isContraprestacion, 'divRangosSiniestralidadGrid') 
					&& checkCamposDerechoPoliza() 
					&& emptyCompensacion();
	return validate;
}

function validateContraprestacion(isContraprestacion, configCA){
	var validate;
	if(configCA.ramo === 'B'){
		 validate = checkCamposPorPrima(configCA) 
					 && checkCamposBajaSini(isContraprestacion, 'divRangosSiniestralidadGridContra') 
					 && checkCamposBajaSini(isContraprestacion, 'divRangosSiniestralidadCumplimientoMeta') 
					 && checkTipoContrato() 
					 && checkProveedor() 
					 && emptyContraprestacion();
	}else{
		 validate = checkCamposPorPrima(configCA) 
					 && checkCamposBajaSini(isContraprestacion, 'divRangosSiniestralidadGridContra') 
					 && checkTipoContrato() 
					 && checkProveedor() 
					 && emptyContraprestacion();
	}
	return validate;
}

function verContraprestacionesConfig(){
	
	var config = CompensacionAdicional.config();
	var verContraprestacionesConf;
	
	switch (config.ramo) {
	
		case 'A':
			verContraprestacionesConf = '/MidasWeb/compensacionesAdicionales/verContraprestacionesConf.action?ramo=A&idNegocio=' + config.idNegocio + "&contraprestacionB=" + true;
			break;
			
		case 'D':
			verContraprestacionesConf = '/MidasWeb/compensacionesAdicionales/verContraprestacionesConf.action?ramo=D&idCotizacion=' 
			+ config.idCotizacion 
			+ '&contraprestacionB=' + true 
			+ "&compensacionesDTO.polizaId="+config.idPoliza
			+ "&idPoliza="+config.idPoliza;	
			break;
			
		case 'V':
			verContraprestacionesConf = '/MidasWeb/compensacionesAdicionales/verContraprestacionesConf.action?ramo=V&idNegocioVida=' + config.idNegocioVida 
			+ "&contraprestacionB=" + true 
			+ "&vidaIndiv="+config.varVidaInd
			+ "&compensacionesDTO.estatusCompensacionId="+config.estatusCompensacion
			+ "&compensacionesDTO.estatusContraprestacionId="+config.estatusContraprestacion;
			break;
		
		case 'B':
			verContraprestacionesConf = '/MidasWeb/compensacionesAdicionales/verContraprestacionesConf.action?ramo=B&idConfiguracionBanca=' + config.idConfiguracionBanca + "&contraprestacionB=" + true;
			break;

	}
	limpiarDivsCompe();	
	sendRequestJQ(null,verContraprestacionesConf,'contenido_contraprestacion', 'activarDesactivarPor();');	
}

function verCompensacionConfig(){	
	
	var config = CompensacionAdicional.config();
	var verCompensacionConfi;
	
	switch (config.ramo) {
	
		case 'A':
			verCompensacionConfi = '/MidasWeb/compensacionesAdicionales/verCompensacionConf.action?ramo=A&idNegocio=' + config.idNegocio + "&contraprestacionB=" + false;
			break;
			
		case 'D':
			verCompensacionConfi = '/MidasWeb/compensacionesAdicionales/verCompensacionConf.action?ramo=D&idCotizacion=' + config.idCotizacion 
			+ "&contraprestacionB=" + false 
			+ "&compensacionesDTO.polizaId="+config.idPoliza
			+ "&idPoliza="+config.idPoliza;	
			break;
			
		case 'V':
			verCompensacionConfi = '/MidasWeb/compensacionesAdicionales/verCompensacionConf.action?ramo=V&idNegocioVida=' + config.idNegocioVida 
			+ "&contraprestacionB=" + false 
			+ "&vidaIndiv="+config.varVidaInd
			+ "&compensacionesDTO.estatusCompensacionId="+config.estatusCompensacion
			+ "&compensacionesDTO.estatusContraprestacionId="+config.estatusContraprestacion;
			desactivarPorcPromotor(config.clavePromotor);
			break;
		
		case 'B':
			verCompensacionConfi = '/MidasWeb/compensacionesAdicionales/verCompensacionConf.action?ramo=B&idConfiguracionBanca=' + config.idConfiguracionBanca + "&contraprestacionB=" + false;
			break;

	}
	limpiarDivsContra();
	sendRequestJQ(null,verCompensacionConfi,'contenido_compensacion', 'activarDesactivarPor();');
}

function limpiarDivsContra() {	
	limpiarDiv('contenido_contraprestacion');
}
function limpiarDivsCompe() {
	limpiarDiv('contenido_compensacion');
}

function validatePersonaResponsable(proveedorElementId, agentElementId){

	var promotorClave = jQuery('#textClavePromotorGral').val();
	var agenteClave = jQuery('#textClaAgeGral').val();
	var compensacionId = jQuery('#textIdCompensacionesCa').val();	
	
	if((compensacionId === null || compensacionId.length === 0 
		|| Number(compensacionId) === 0) && (promotorClave.length === 0 
		|| Number(promotorClave) === 0)
		&& agenteClave.length > 0){
		jQuery('#'+proveedorElementId).val(0);
		jQuery('#'+proveedorElementId).attr('disabled', true);
		jQuery('#'+agentElementId).val(100);
		jQuery('#'+agentElementId).attr('disabled', true);
	}
}

/**
 * Funcion ejecutada al seleccionar un agente
 * 
 */
jQuery("#selectAgregarAgteGral").change(function() {		

	var config = CompensacionAdicional.config();
	var idCompensacion = jQuery("#varCompensacionId").val();

	var idEntidadPersona = this.value;
	var ramo, ramoId, datoIdentificador;
	ramo = jQuery('#varRamo').val();
	ramoId = 'ramo='+ramo+'&';		
	
	if(ramo === 'A'){
		datoIdentificador ='&idNegocio='+ jQuery('#varIdNegocio').val();
	}else if(ramo === 'D'){
		datoIdentificador = '&idCotizacion='+jQuery('#varIdCotizacion').val()+'&compensacionesDTO.polizaId='+ jQuery('#varIdPoliza').val();
	}else if(ramo === 'V'){
		datoIdentificador = '&idNegocioVida='+jQuery('#varIdNegocioVida').val() 
			+'&vidaIndiv='+config.varVidaInd
			+ "&compensacionesDTO.estatusCompensacionId="+config.estatusCompensacion
			+ "&compensacionesDTO.estatusContraprestacionId="+config.estatusContraprestacion;
	}
	
	if(Number(idCompensacion) > 0){
		limpiarDivsCompe();	
		sendRequestJQ(null,'/MidasWeb/compensacionesAdicionales/cargarAgente.action?'+ramoId+'entidadPersonaId=' + idEntidadPersona 
				+ "&idCompensacionActual=" + idCompensacion+datoIdentificador+'&contraprestacionB=false'
				,'contenido_compensacion','postCargarAgente("selectAgregarAgteGral", '+this.value+')');
	}else{
		alert('Por el momento no es posible ver la configuracion de la Persona Configurada');
	}

});

/**
 * Funcion ejecutada al seleccionar un proveedor
 * 
 */	
jQuery("#selectAgregarAgteContra").change(function() {		
	
	var config = CompensacionAdicional.config();
	var idContraprestacion = jQuery("#varContraprestacionId").val();

	var idEntidadPersona = this.value;
	var ramo, ramoId, datoIdentificador;
	ramo = jQuery('#varRamo').val();
	ramoId = 'ramo='+ramo+'&';		
	
	if(ramo === 'A'){
		datoIdentificador ='&idNegocio='+ jQuery('#varIdNegocio').val();
	}else if(ramo === 'D'){
		datoIdentificador = '&idCotizacion='+jQuery('#varIdCotizacion').val()+'&compensacionesDTO.polizaId='+ jQuery('#varIdPoliza').val();
	}else if(ramo === 'V'){
		datoIdentificador = '&idNegocioVida='+jQuery('#varIdNegocioVida').val() 
			+'&vidaIndiv='+config.varVidaInd
			+ "&compensacionesDTO.estatusCompensacionId="+config.estatusCompensacion
			+ "&compensacionesDTO.estatusContraprestacionId="+config.estatusContraprestacion;
	}
	
	if(Number(idContraprestacion) > 0){
		limpiarDivsContra();	
		sendRequestJQ(null,'/MidasWeb/compensacionesAdicionales/cargarProveedor.action?'+ramoId+'entidadPersonaId=' 
				+ idEntidadPersona + "&idCompensacionActual=" + idContraprestacion+datoIdentificador+'&contraprestacionB=true'
				,'contenido_contraprestacion','postCargarAgente("selectAgregarAgteContra", '+this.value+')');
	}else{
		alert('Por el momento no es posible ver la configuracion de la Persona Configurada');
	}
});



function postCargarAgente(selector, value){
	var element = document.getElementById(selector);
	for (var i=0; i<element.length; i++){			
		if(Number(element.options[i].value) === Number(value)){
			element.options[i].selected = true;			
		}
	}
}


function cargarPresupuestoAnual(){
	var archivotxt = jQuery("#fileUpload").val();
	if(archivotxt.indexOf(".xls")!=-1) {
		if(archivotxt.length > 255){
			alert("El nombre del Archivo es demasiado largo, la longuitud de caracteres deve ser menor a 255");
		}else{
			var idConfiguracionBanca = Number(jQuery('#varIdConfiguracionBanca').val());
			if(isNaN(idConfiguracionBanca)){
				alert('No se pudo encontrar el id de la Configuracion de Banca por favor verifique que ya este creada ');
			}else{
				jQuery('#fileIdConfiguracionBanca').val(idConfiguracionBanca);
				document.formularioCargaPresupuestoAnual.submit();
			}
		}
	}
	else{
		alert("Seleccione un archivo .xls ");
	}
}

function selectComAgente(){
	selectComision=true;
	var result = "";
	result = jQuery('input:radio[name="compensacionesDTO.aplicaComision"]:checked').val();
	if(result == 1){
		tabsCompensacion.disableTab('compensacion');
		tabsCompensacion.setTabActive('contraprestacion');
	}else if(result == 0){
		tabsCompensacion.setTabActive('compensacion');
		tabsCompensacion.enableTab('compensacion');
	}
}

function selectComOriginal(){
var config = CompensacionAdicional.config();
 var result = "";
	result = jQuery('input:radio[name="compensacionesDTO.comisionAgente"]:checked').val();
	if(result == 1){
		if(config.isContraprestacion){
			jQuery('#textPorcePrimaContra').val('');
			jQuery('#textPorcePrimaContra').attr("disabled", true);
		}else{			
			jQuery('#textPorcePrimaGral').val('');
			jQuery('#textPorcePrimaAgenGral').val(100);
			jQuery('#textPorcePrimaPromoGral').val(0);
			jQuery('#textPorcePrimaGral').attr("disabled", true);
			jQuery('#textPorcePrimaAgenGral').attr("disabled", true);
			jQuery('#textPorcePrimaPromoGral').attr("disabled", true);
		}
	}else if(result == 0){
		if(config.isContraprestacion){
			jQuery('#textPorcePrimaContra').val('');
			jQuery('#textPorcePrimaContra').attr("disabled", false);
		}else{			
			jQuery('#textPorcePrimaGral').val('');
			jQuery('#textPorcePrimaAgenGral').val('');
			jQuery('#textPorcePrimaPromoGral').val('');
			jQuery('#textPorcePrimaGral').attr("disabled", false);
			jQuery('#textPorcePrimaAgenGral').attr("disabled", false);
			jQuery('#textPorcePrimaPromoGral').attr("disabled", false);
		}
	}
}

function insertTitle(x) {		
	if(x.value == 1){
		jQuery("#radioTipoContratoContra1").attr({
			   'title':'Contrato de Instituciones Financieras.'
			});
	}else{
		jQuery("#radioTipoContratoContra2").attr({
			   'title':'Contrato de Prestador de Servicios.'
			});
	}		
}

function fechaValida(){
	var config = CompensacionAdicional.config();
	var valida = true;
	try{
		valida = jQuery('#fechaModificacionVida').val() !== '' 
					&& jQuery('#fechaModificacionVida').val() !== typeof undefined;
		
		new Date(jQuery('#fechaModificacionVida').val());			
	}catch(e){
		valida = false;		
	}
	return valida;
}
function desactivarPorcPromotor(idPromotor) {
	if(idPromotor>0){
		jQuery('#textPorcePrimaPromoGral').attr("disabled", true);
	}
}
function mostrarFormaPago(){
	jQuery("#formaPagoBanca").val("MENSUAL");
	jQuery("#formaPago").show();
}
function activarDesactivarPor(){
	var config = CompensacionAdicional.config();
	var result = "";
	result = jQuery('input:radio[name="compensacionesDTO.aplicaComision"]:checked').val();
	if(result == 1){
		if(config.isContraprestacion){
			jQuery('#incluirContraprestacionesAgente').css('display', 'block');
			jQuery('#incluirContraprestacionesAgente').css('display', 'table-cell');
			jQuery('#productosVidaGrid').css('display', 'block');
			jQuery('#productosVidaGrid').css('display', 'table-cell');
			jQuery('#textPorcePrimaContra').hide();
			jQuery('#textPorcePrimaContra').val(0);
			jQuery('#labelPorcentaje').hide();
			jQuery('#labelSigPorcentaje').hide();
		}else{
			jQuery('#incluirCompensacionesAgente').css('display', 'block');
			jQuery('#incluirCompensacionesAgente').css('display', 'table-cell');
		}
	}else{
		if(config.isContraprestacion){
			jQuery('#incluirContraprestacionesAgente').hide();
			jQuery('#textPorcePrimaContra').show();
			jQuery('#labelPorcentaje').show();
			jQuery('#labelSigPorcentaje').show();
		}else{
			jQuery('#incluirCompensacionesAgente').hide();
		}
	}
	
}
jQuery("#selectBaseCalcBajaSiniGral").change(function() {
	var BASE_CALCULO    = 5;
	var baseCalculo = this.value;
	 if (baseCalculo == BASE_CALCULO){
		 jQuery('#l_FormulaPrimaRiesgo').show();
	 }else{
		 jQuery('#l_FormulaPrimaRiesgo').hide();
	 }
});
jQuery("#selectBaseCalcBajaSiniContra").change(function() {
	var BASE_CALCULO    = 5;
	var baseCalculo = this.value;
	 if (baseCalculo == BASE_CALCULO){
		 jQuery('#l_FormulaPrimaRiesgo_contra').show();
	 }else{
		 jQuery('#l_FormulaPrimaRiesgo_contra').hide();
	 }
});