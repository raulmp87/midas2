
/**
 *	CompensacionView es igual a la configuracion
 *	de la compensacion por entidadPersona
 * @param elementForm
 * @returns
 */
var CompenensacionView = function(elementForm) {
	
	try{
		var data = null;	
		var $elementClone = jQuery(elementForm).clone();
		$elementClone.find(':input').removeAttr('disabled');		

		data = getSerializeJSON($elementClone);
		
		if(typeof data === 'undefined' || data === null){
			throw 'Error al obtener los datos';
		}

		var configuracion = CompensacionAdicional.config();
		
		/**
		 * Datos Generales
		 */
		this.compensacionId = data['compensacionesDTO.compensacionId'];		
		this.contraprestacion = configuracion.isContraprestacion;
		this.idRecibo = jQuery('#idRecibo').val();
		this.ramo = {
				identificador : configuracion.ramo
		}
		this.caEntidadPersona = {
				//Este Campo se esta utilizando como transporte del valor de la clave
				id : this.contraprestacion ? configuracion.claveProveedor : configuracion.claveAgente,
				nombres: getNombre(this.contraprestacion)
		};
		
		this.idNegocio = configuracion.idNegocio;
		this.cotizacionId = configuracion.idCotizacion;
		this.idNegocioVida = configuracion.idNegocioVida;
		this.idConfiguracionBanca = configuracion.idConfiguracionBanca;
		this.polizaId = configuracion.idPoliza;
		if(jQuery('#fechaModificacionVida').val() !== ''){
			this.fechaModificacionVida = jQuery('#fechaModificacionVida').val();
		}
		this.aplicaComision = jQuery('input[name="compensacionesDTO.aplicaComision"]:checked').val();
		this.topeMaximo = configuracion.topeMaximo;
		this.formaPagoBanca = configuracion.formaPagoBanca;
		if(this.contraprestacion){
			
			this.caTipoContrato = {
				id: data['compensacionesDTO.caTipoContrato.id']
			};
			/**
			 * Prima
			 */
			this.porPrimaContra = jQuery('#checkPrimaContra').is(":checked");			
			if(this.porPrimaContra){
				this.porcePrimaContra = data['compensacionesDTO.porcePrimaContra'];
				this.montoPrimaContra = data['compensacionesDTO.montoPrimaContra'];
				this.baseCalculocaPrimaContra = {
						id: jQuery('#selectBaseCalPrimaContra').val()
				};
				this.tipoMonedacaPrimaContra ={
					id:jQuery('#selectMonedaPrimaContra').val()
				};
								
				switch (this.ramo.identificador) {
					case 'A':
						this.condicionesCalcPrimaContra = {
							id : data['compensacionesDTO.condicionesCalcPrimaContra.id']
						};
						this.lineasNegocio = getLineasNegocio(this.porPrimaContra, this.contraprestacion);
						break;
					
					case 'D':
						this.tipoProvisioncaPrima = {
							id : data['compensacionesDTO.tipoProvisionca.id']
						};
						this.listSubRamosDaniosView = getListSubRamosDaniosView();			
						break;
						
					case 'V':
						this.comisionAgente = data['compensacionesDTO.comisionAgente'];	
						this.ivaPrima = data['compensacionesDTO.ivaPrima'];
					break;
				}
			}			
			/**
			 * Siniestralidad
			 */
			
			this.porSiniestralidadContra = jQuery('#checkBajaSiniestralContra').is(":checked");			
			if(this.porSiniestralidadContra){
				this.porceSiniestralidadContra = data['compensacionesDTO.porceSiniestralidadContra'];
				this.montoSiniestralidadContra = data['compensacionesDTO.montoSiniestralidadContra'];
				this.baseCalculocaSiniestralidadContra = {
						id:jQuery('#selectBaseCalcBajaSiniContra').val()
				};
				this.tipoMonedacaSiniestralidadContra = {				
					id:jQuery('#selectMonedaBajaSiniContra').val()
				};			
				this.nivelesRangos = getRangosSiniestralidad(this.porSiniestralidadContra, 'divRangosSiniestralidadGridContra');
				
				this.ivaSiniestralidad = data['compensacionesDTO.ivaSiniestralidad'];
			}
			
			/**
			 * Por Utilidad
			 */
			if(this.ramo.identificador === 'D'){
				this.porUtilidad = jQuery('#checkUtilidadContra').is(":checked");
				if(this.porUtilidad){
					this.porcenUtilidadesUtilidad = data['compensacionesDTO.porcenUtilidadesUtilidad'];
					this.montoUtilidadesUtilidad = data['compensacionesDTO.montoUtilidadesUtilidad'];
					this.montoDeLasUtilidades = data['compensacionesDTO.montoDeLasUtilidades'];					
				}
			}
			
			/**
			 * Por Cumplimiento de Meta
			 */
			if(this.ramo.identificador === 'B'){
				this.porCumplimientoDeMeta = jQuery('#checkCumplimientoMeta').is(":checked");
				if(this.porCumplimientoDeMeta){
					this.baseCalculoCumplimientoMeta = {
							id:jQuery('#selectBaseCalcCumplimientoMeta').val()
					};
					this.caTipoMonedaCumplimientoMeta = {				
						id:jQuery('#selectMonedaCumplimientoMeta').val()
					};			
					this.nivelesRangosCumplimientoMeta = getRangosSiniestralidad(this.porSiniestralidadContra, 'divRangosSiniestralidadCumplimientoMeta');			
					this.presupuestoAnual = jQuery('#fileUpload').val();
				}
			}
			
		}else{		

			this.inclusionesGral = data['compensacionesDTO.inclusionesGral'];
			
			/**
			 * Agente Seycos solo aplica para vida
			 */
			
			if(this.ramo.identificador === 'V'){
				this.datosSeycos = {
						idAgente: configuracion.claveAgente,
						idEmpresa: jQuery('#varIdEmpresa').val(),
						fsit:jQuery('#varFSit').val()
				}
			}
			/**
			 * Prima
			 */			
			this.porPrima = jQuery('#checkPrimaGral').is(":checked");
			if(this.porPrima){
				this.porcePrima = data['compensacionesDTO.porcePrima'];
				this.montoPrima = data['compensacionesDTO.montoPrima'];
				this.porceAgentePrima = data['compensacionesDTO.porceAgentePrima'];
				this.porcePromotorPrima = data['compensacionesDTO.porcePromotorPrima'];
				this.baseCalculocaPrima = {
					id : jQuery('#selectBaseCalGral').val()
				};
				
				switch (this.ramo.identificador) {
					case 'A':
						this.condicionesCalcPrima = {
							id : data['compensacionesDTO.condicionesCalcPrima.id']
						};
						this.lineasNegocio = getLineasNegocio(this.porPrima, this.contraprestacion);
						break;
					
					case 'D':
						this.tipoProvisioncaPrima = {
							id : data['compensacionesDTO.tipoProvisionca.id']
						};
						this.listSubRamosDaniosView = getListSubRamosDaniosView();	
						break;		
						
					case 'V':
						this.comisionAgente = data['compensacionesDTO.comisionAgente'];
						this.iva = data['compensacionesDTO.iva'];
						this.topeMaximoPrima = data['compensacionesDTO.topeMaximoPrima'];
						break;
					}
				}
			/**
			 * Siniestralidad
			 */
			this.porSiniestralidad = jQuery('#checkBajaSiniestralGral').is(":checked");
			if(this.porSiniestralidad){
				this.porceSiniestralidad = data['compensacionesDTO.porceSiniestralidad'];
				this.porceAgenteSiniestralidad = data['compensacionesDTO.porceAgenteSiniestralidad'];
				this.porcePromotorSiniestralidad = data['compensacionesDTO.porcePromotorSiniestralidad'];
				this.baseCalculocaSiniestralidad = {
					id : jQuery('#selectBaseCalcBajaSiniGral').val()
				};
				this.nivelesRangos = getRangosSiniestralidad(this.porSiniestralidad, 'divRangosSiniestralidadGrid');
			}
			/**
			 * Poliza
			 */
			this.porPoliza = jQuery('#checkDerechoPoliza').is(":checked");
			if(this.porPoliza){
				this.montoPoliza = data['compensacionesDTO.montoPoliza'];
				this.porcePoliza = data['compensacionesDTO.porcePoliza'];
				this.porceAgentePoliza = data['compensacionesDTO.porceAgentePoliza'];
				this.porcePromotorPoliza = data['compensacionesDTO.porcePromotorPoliza'];
				this.emisionInterna = jQuery('#checkEmisInterPolizaCompensacion').is(":checked");
				this.emisionExterna = jQuery('#checkEmisExterPolizaCompensacion').is(":checked");	
			}
		}
		
		console.info('>>CompensacionView'); //+ JSON.stringify(this));
		
	}catch(e){
		var msj = "Error al Crear el Objeto CompensacionView: " + e.message;
		throw msj;
	}
	
	function getLineasNegocio(isPrima, isContraprestacion){
		if(isPrima){
			return obtenerLineasNegocio(isContraprestacion);
		}
		return null;
	}	

	function getRangosSiniestralidad(isSiniestralidad, divId){		
		if(isSiniestralidad){
			return validarRangosTodos(divId);
		}
		return null;
	}
	
	function getNombre(isContraprestacion){
		var nombre = null;
		if(jQuery('#textNomAgenGral').length && !isContraprestacion){
			nombre = jQuery('#textNomAgenGral').val().length !== 0 
					? jQuery('#textNomAgenGral').val()
					: null;
		}else if(jQuery('#textNomProvContra').length && isContraprestacion) {
			nombre = jQuery('#textNomProvContra').val().length !== 0 
			? jQuery('#textNomProvContra').val()
			: null;	
		}
		return nombre;
	}
	
	function getSerializeJSON(element){
		var o = {};
		var a = element.serializeArray();
		jQuery.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || null);
			} else {
				o[this.name] = this.value || null;
			}
		});
		return o;
	}

};


var getCompensacionViewActual = function (){
	
	var isContraprestacion = Number(jQuery("#varIsContraprestacion").val()) === 0 ? false : true;
	var compensacionVw = null;
	if(isContraprestacion){
		compensacionVw = new CompenensacionView(document.getElementById('formConfigContraprestacion'));
	}else{
		compensacionVw = new CompenensacionView(document.getElementById('formCompensacionesConf'));	
	}
	return compensacionVw;
}


var getArrayCompensacionesView = function (){
	
	var compensacionVw, jsonString, jsonObject = null, data, $jsonHidden, isContraprestacion;
	
	try{
		
		isContraprestacion = Number(jQuery("#varIsContraprestacion").val()) === 0 ? false : true;
		
		compensacionVw = getCompensacionViewActual();
		
		if(isContraprestacion){
			$jsonHidden = jQuery('#jsonContraprestacionId');
			jsonString = $jsonHidden.val();
		}else{
			$jsonHidden = jQuery('#jsonCompensacionId');
			jsonString = $jsonHidden.val();
		}

		if(typeof jsonString !== 'undefined' && jsonString.length > 2){
			var obj = JSON.parse(jsonString);
			jsonObject = obj['dataGson'];
		}else{
			jsonObject = [];
		}
		
		if(jsonObject !== null && jsonObject.length == 0){	
			jsonObject.push(compensacionVw);
		}else{		
			var lon = jsonObject.length;
			var existeCompensacion = false;
			
			for (i = 0; i < lon; i++) {					
				if(jsonObject[i].caEntidadPersona.id === compensacionVw.caEntidadPersona.id){
					jsonObject[i] = compensacionVw;
					existeCompensacion = true;
					break;
				}
			}
			
			if(!existeCompensacion){
				jsonObject.push(compensacionVw);
			}
		}
		/**
		 * Variable en Action
		 * dataGson
		 */	
		data = JSON.stringify({'dataGson':jsonObject});
		
		$jsonHidden.val(data);
	}catch(e){
		console.error(e.message);
		return alert("Ocurrio al guardar");
	}
//	console.info('dataGson -->');
//	console.info(JSON.stringify(jsonObject));
	
	return data;
	
}


var SubRamosDaniosView = function(values){
	
	this.id = values[0];
	this.descripcionRamo = values[1];
	this.idTcSubRamo = values[2];
	this.subRamo = values[3];
	this.primaBase = values[4];
	this.porcentajeCompensacion = values[5];
	this.porcentajeCompensacionImporte = values[6];
	this.existe = values[7];
	
	this.serialize = function (name){
		var a = '';		
		try{
			a = name+'.id='+this.id
				+'&'+name+'.descripcionRamo='+this.descripcionRamo
				+'&'+name+'.idTcSubRamo='+this.idTcSubRamo
				+'&'+name+'.subRamo='+this.subRamo
				+'&'+name+'.primaBase='+this.primaBase
				+'&'+name+'.porcentajeCompensacion='+this.porcentajeCompensacion
				+'&'+name+'.porcentajeCompensacionImporte='+this.porcentajeCompensacionImporte
				+'&'+name+'.existe='+this.existe;			
		}catch(e){			
			console.error('Ocurrio un error al serializar SubRamosDaniosView: '+e)
		}
		return a;
	}
	
}


var getListSubRamosDaniosView = function(){
	var listSubRamosDaniosView, data, config, divTable;
	
	try{
		config = CompensacionUtils.config();
		
		divTable = config.isContraprestacion ? 'divSubRamosDaniosContra' : 'divSubRamosDanios';
		
		data = CompensacionUtils.getDataTable(divTable);
	}catch(e){
		return new Array();
	}
	
	listSubRamosDaniosView = new Array();
	
	var item;
	
	for(var i = 0; i < data.length; i++){
		item = new SubRamosDaniosView(data[i]);
		if(item.existe !== 'REMOVE'){
			listSubRamosDaniosView.push(item);
		}
	}
	
	return listSubRamosDaniosView;
}


var CaRangosView = function(values){

}

var getListCaRangosView= function(){
	var listSubRamosDaniosView = new Array();
	return listSubRamosDaniosView;
}


function getListSubRamosDaniosViewWithParam(){
	var list = '';
	var name = 'compensacionesDTO.listSubRamosDaniosView'
	var index = 0;
	var arraySubRamosView = getListSubRamosDaniosView();
	
	for(var i = 0; i < arraySubRamosView.length; i++){
		list = list+'&'+arraySubRamosView[i].serialize(name+'['+i+']');
	}
	
	return list;
}