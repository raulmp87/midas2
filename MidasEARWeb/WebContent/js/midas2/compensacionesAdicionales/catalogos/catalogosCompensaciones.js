/**
 * 
 */
var identJQ='';
var identJQKey = '';
var identJQValue = '';
var buscarBool = false;
var idCompensacion = 0;
var idNegocioAgente = 0;
var selectAgente = null;
var contraprestacionBool = false;
var catalogoBuscar = "";
	
var functionExecutePostSelectAgente = '';

function seleccionarAgenteTipo(catalogo, idJQ, idJQKey, idJQValue,buscar,idComp,idNegocio,select,contraBool,tipoEntidad,nombreFuncion,titulo){	
	
	identJQ=idJQ;
	identJQKey = idJQKey;
	identJQValue = idJQValue;
	buscarBool = buscar;
	idCompensacion = idComp;
	idNegocioAgente = idNegocio;
	selectAgente = jQuery("#"+select);
	contraprestacionBool = contraBool;
	catalogoBuscar = catalogo;
	functionExecutePostSelectAgente= nombreFuncion;
	if(titulo==null || titulo=="") titulo = catalogo;
	var path = '/MidasWeb/compensacionesAdicionales/catalogos/ventanaCatalogo.action?catalogo=' + catalogo+'&tipoEntidad='+tipoEntidad;
	parent.mostrarVentanaModal("vmBuscarCatalogo","Selecci\u00F3n "+titulo,200,400, 510, 230, path);	
}

function seleccionarAgente(catalogo, idJQ, idJQKey, idJQValue,buscar,idComp,idNegocio,select,contraBool,tipoEntidad,nombreFuncion){	
	return seleccionarAgenteTipo(catalogo, idJQ, idJQKey, idJQValue,buscar,idComp,idNegocio,select,contraBool,0,nombreFuncion,catalogo);
	
}

function cargaAgenteEncontrado(item){
	
	/**
	* Valores Generales de la Configuracion
	*/
	var configCompensaciones = CompensacionUtils.config();
	
	var ramoId = configCompensaciones.ramo;
	
	var varVidaInd = jQuery('#varVidaIndividual').val();
	var urlContrapresacion = '/MidasWeb/compensacionesAdicionales/agregarAgenteContra.action?ramo='+ramoId+'&idAgenteAgregar='+item.idAgente
								+"&idCompensacionActual="+idCompensacion+"&contraprestacionB="+true+'&vidaIndiv='+varVidaInd;
	
	var urlCompensacion = '/MidasWeb/compensacionesAdicionales/agregarAgente.action?ramo='+ramoId+'&idAgenteAgregar='+item.idAgente
							+"&idCompensacionActual="+idCompensacion+"&contraprestacionB="+false+'&vidaIndiv='+varVidaInd;

	
	if(buscarBool){
		eliminarAgenteActual(contraprestacionBool);
		if(contraprestacionBool){
			limpiarDivsContra();
			if(ramoId === 'D'){
				urlContrapresacion = urlContrapresacion + '&compensacionesDTO.polizaId='+  configCompensaciones.idPoliza;
			}else if(ramoId === 'V'){
				urlContrapresacion = urlContrapresacion
				+'&vidaIndiv='+varVidaInd
				+ "&compensacionesDTO.estatusCompensacionId="+configCompensaciones.estatusCompensacion
				+ "&compensacionesDTO.estatusContraprestacionId="+configCompensaciones.estatusContraprestacion;
			}
			sendRequestJQ(null, urlContrapresacion,'contenido_contraprestacion', 'postAgregarAgente(true);');
		}else{
			if(ramoId === 'V'){				
				urlCompensacion = urlCompensacion+'&compensacionesDTO.datosSeycos.idAgente='+item.idAgente
								+'&compensacionesDTO.datosSeycos.idEmpresa='+item.idEmpresa 
								+'&compensacionesDTO.datosSeycos.fsit='+item.fsit
								+'&compensacionesDTO.datosSeycos.nombreCompleto='+item.nombreCompleto
								+ "&compensacionesDTO.estatusCompensacionId="+configCompensaciones.estatusCompensacion
								+ "&compensacionesDTO.estatusContraprestacionId="+configCompensaciones.estatusContraprestacion;
			}
			limpiarDivsCompe();
			sendRequestJQ(null, urlCompensacion,'contenido_compensacion', 'postAgregarAgente(false);');	
		}
	
	}else{		
		if(contraprestacionBool){			
			/**
			 * Validacion
			 */
			if(validateContraprestacion(contraprestacionBool,configCompensaciones)){
				/**
				 * Guarda los agentes que pueden ir seleccinando
				 */	
				getArrayCompensacionesView();
				limpiarDivsContra();
				if(ramoId === 'D'){
					urlContrapresacion = urlContrapresacion + '&compensacionesDTO.polizaId='+ configCompensaciones.idPoliza;
				}else if(ramoId === 'V'){
					urlCompensacion = urlCompensacion
					+'&vidaIndiv='+varVidaInd
					+ "&compensacionesDTO.estatusCompensacionId="+configCompensaciones.estatusCompensacion
					+ "&compensacionesDTO.estatusContraprestacionId="+configCompensaciones.estatusContraprestacion;
				}
				sendRequestJQ(null, urlContrapresacion,'contenido_contraprestacion', 'postAgregarAgente(true);');
				
			}
		
		}else{
			/**
			 * Validacion
			 */
			if(validateCompensacion(contraprestacionBool,configCompensaciones)){
				/**
				 * Guarda los agentes que pueden ir seleccinando
				 */	
				if(ramoId === 'V'){
					urlCompensacion = urlCompensacion+'&compensacionesDTO.datosSeycos.idAgente='+item.idAgente
									+'&compensacionesDTO.datosSeycos.idEmpresa='+item.idEmpresa 
									+'&compensacionesDTO.datosSeycos.fsit='+item.fsit
									+'&compensacionesDTO.datosSeycos.nombreCompleto='+item.nombreCompleto
									+'&vidaIndiv='+varVidaInd
									+ "&compensacionesDTO.estatusCompensacionId="+configCompensaciones.estatusCompensacion
									+ "&compensacionesDTO.estatusContraprestacionId="+configCompensaciones.estatusContraprestacion;
				}				
				getArrayCompensacionesView();
				limpiarDivsCompe();				
				sendRequestJQ(null, urlCompensacion,'contenido_compensacion', 'postAgregarAgente(false);');
			}
		}		
	}
	
	cerrarVentanaModal("vmBuscarCatalogo");
}

function postAgregarAgente(isContraprestacion){
	ocultarIndicadorCarga('indicador');
	var selectOption, jsonObject, gsonData = JSON.parse(getArrayCompensacionesView());
	
	jsonObject = gsonData['dataGson'];
	
	if(isContraprestacion){		
		selectOption = 'selectAgregarAgteContra';
	}else{		
		selectOption = 'selectAgregarAgteGral';
	}
	
	var isSelect, lon = jsonObject.length;
	var claveActual = Number(getClaveActual(isContraprestacion));
	for (i = 0; i < lon; i++){
		
		isSelect = jsonObject[i].caEntidadPersona.id === claveActual ? 'selected' : null;
		
		jQuery('#'+selectOption+'').append(new Option(jsonObject[i].caEntidadPersona.nombres, 
				jsonObject[i].caEntidadPersona.id,
				null,
				isSelect));

	}
	jQuery('#'+selectOption+'').children().attr('disabled', true);
}

function eliminarAgenteActual(isContraprestacion){
	var $jsonHidden, jsonString, jsonObeject, compensacionActual = getCompensacionViewActual();
	
	if(isContraprestacion){
		$jsonHidden = jQuery('#jsonContraprestacionId');
		jsonString = $jsonHidden.val();
	}else{
		$jsonHidden = jQuery('#jsonCompensacionId');
		jsonString = $jsonHidden.val();
	}
	
	if(typeof jsonString !== 'undefined' && jsonString.length > 2){
		var obj = JSON.parse(jsonString);
		jsonObject = obj['dataGson'];
	}else{
		jsonObject = [];
	}
	
	
	for(index = 0; index<jsonObject.length; index++){		
		if(compensacionActual.caEntidadPersona.id = jsonObject[index].caEntidadPersona.id){
			jsonObject.splice(index, 1);
			if(jsonObject.length === 0){			
				$jsonHidden.val('');
			}else{
				var data = JSON.stringify({'dataGson':jsonObject});			
				$jsonHidden.val(data);
			}
			break;
		}
	}
	
}

function getClaveActual(isContraprestacion){
	var clave = null;
	if(jQuery('#textClaAgeGral').length && !isContraprestacion){
		clave = jQuery('#textClaAgeGral').val().length !== 0 
				? jQuery('#textClaAgeGral').val()
				: null;
	}else if(jQuery('#textClaProveContra').length && isContraprestacion) {
		clave = jQuery('#textClaProveContra').val().length !== 0 
		? jQuery('#textClaProveContra').val()
		: null;	
	}
	return clave;
}


function filtrosBusquedaCompensaciones(item){
	var valor = item.value;
	jQuery('#'+identJQValue+'').val(valor);
	jQuery('#'+identJQKey).val(item.id);
	cerrarVentanaModal("vmBuscarCatalogo");
}


function agregaValorSeleccionado(item){
	jQuery('#'+identJQValue).val(item.value);
	jQuery('#'+identJQKey).val( item.id );
	cerrarVentanaModal("vmBuscarCatalogo");

	
}

function agregaValorSeleccionadoOrdenesPago(item){
	var valor = item.idAgente;
	jQuery('#'+identJQValue+'').val(valor);
	cerrarVentanaModal("vmBuscarCatalogo");
}
function cargarAgenteSeycos(item){	
	var idAgente = item.idAgente;
	var agente = item.value;
	var idEmpresa = item.idEmpresa;
	var valfSit = item.fsit;
	var indice = jQuery("#listaBeneficiariosAgentes li").length;
	if(idAgente!=null){
		var indicename = indice;
		var option='<li id="'+idAgente+'" idEmpresa="'+idEmpresa+'" valfSit="'+valfSit+'"> <img src="../img/icons/ico_eliminar.gif" onclick="javascript:delete_agente('+idAgente+');" style="cursor:pointer;"/>';
		option+='&nbsp;&nbsp;<label for="'+idAgente+'" />'+agente+'</label> <s:hidden name="agenteLong['+indicename+']"/> </li>';
		
		jQuery("#listaBeneficiariosAgentes").append(option);
		cerrarVentanaModal("vmBuscarCatalogo");
	}	
}

function delete_agente(id){
	jQuery("#listaBeneficiariosAgentes #"+id).remove();
}
