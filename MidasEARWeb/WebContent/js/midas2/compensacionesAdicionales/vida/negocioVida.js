var gridAgente;
var negocioGrid;
var gridPoliza;
var rangoFactorGrid;
var gridCveAmis;
var ventanaCompensacion;
var temporizadoWinComp;
var compensacionRecargado = false;

function cascadeoProductosXLineaVentas(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaLineaVenta input:checked").each(function(index){
		var clave={"clave":jQuery(this).val()};		
		arrSeleccionados.push(clave);		
		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["clave"],"listaLineaVentaSeleccionadas");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarProductosPorLineaDeVenta.action";
	
	jQuery.asyncPostJSON(url,data,responseProductos);
	
}

function responseProductos(json){
	comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","id");	
}


function cascadeoRamosXProducto(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var id={"idProducto":jQuery(this).val()};		
		arrSeleccionados.push(id);		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["idProducto"],"listaProductosSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarRamosPorProducto.action";
	
	jQuery.asyncPostJSON(url,data,responseRamos);
	
}
function responseRamos(json){
	comboChecksCreate(json.listaRamos,"ajax_listaRamos","listaRamosSeleccionados","cascadeoSubramoXRamo","id");	
}

function cascadeoSubramoXRamo(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaRamos input:checked").each(function(index){
		var id={"id":jQuery(this).val()};		
		arrSeleccionados.push(id);				
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["idRamo"],"listaRamosSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarSubramosPorRamoYLineaNegocioPorRamo.action";
	
	jQuery.asyncPostJSON(url,data,responseSubRamos);
}

function responseSubRamos(json){
	comboChecksCreate(json.listaSubRamos,"ajax_listaSubRamosSeycos","subRamosLong",null,"id");
	comboChecksCreate(json.listaLineaNegocio,"ajax_lineaNegocioSeycos","lineaNegocioLong","cascadeoCoberturasXLineaNegocio","id");
}

function cascadeoCoberturasXLineaNegocio(){
	
	var arrSeleccionados = new Array();
	jQuery("#ajax_lineaNegocioSeycos input:checked").each(function(index){
		var id={"id":jQuery(this).val()};		
		arrSeleccionados.push(id);		
		
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["id"],"listaLineaNegocioSeleccionados");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarCoberturasPorLineaNegocio.action";
	
	jQuery.asyncPostJSON(url,data,responseCoberturas);
}
function responseCoberturas(json){
	comboChecksCreate(json.listaCoberturas,"ajax_listaCoberturas","listaCoberturasSeleccionadas",null,"id");
}

//jsonList = lista devuelta en formato json
//idSelect = id del elemento select donde se mostrara la informacion
//nameObj = nombre que recibira el atributo name del select 
//onclickFunction = funcion que se ejecutara en el onclick de cada checkbox
function comboChecksCreate(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"('"+property.toString()+"');";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li><label for="'+key+'">';
					option+='<input type="checkbox" name="'+nameObj+'['+i+'].'+property+'" id="'+nameObj+'['+i+'].'+property+'" value="'+key+'" ';
					if(check==1){
						option+= "checked='checked'";
						}
					option+='onchange="'+fn+'" />';
					option+= value + '</label>';
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}


function guardarConfiguracionNegocioVida(){
		
	if(validateForm()){
		var nombreNegocio = 'configuracionNegocioVida.descripcionnegociovida=' +jQuery('#descripcionnegociovida').val();
		var listasNegocio = getListsNegocioVida();
		var listaAgente = getAgenteList();
		var path = "/MidasWeb/compensacionesAdicionales/vida/guardarConfiguracionNegocioSeycos.action?"+nombreNegocio+listasNegocio+listaAgente;
		var negocioId = null;
		jQuery.ajax({
			url: path,
			type: 'POST',
			async: false,
			success: function (res) {
			    console.log("Se ha guardado la configuracion de negocio vida");
			    	jQuery('#txtIdNegocio').val(res);
			    	if(jQuery('#txtIdNegocio').length){
						negocioId = jQuery('#txtIdNegocio').val();
					}		
			},
			error: function (res) {
			    console.log("Ocurrio un error al guardar la configuracion de negocio vida");			   
			},
			complete: function(jqXHR) {				
			}
		});
			
		return negocioId;
	}else{
		parent.mostrarMensajeInformativo("Existen algunos errores en la captura","10");
	}
}


function findIdsCompensaciones(flagMensaje){
	var negocioId = jQuery('#txtIdNegocio').val();	
	if(!isNaN(Number(negocioId)) && Number(negocioId) > 0){
		
		jQuery('#configuracionNegocioVidaCaForm').find('input').attr('disabled', true);
		jQuery('#listaBeneficiariosAgentes').find('img').removeAttr('onclick');
		jQuery('#btnBuscarAgenteSeycos').find('a').removeAttr('onclick');		
		
		var path = "/MidasWeb/compensacionesAdicionales/vida/findIdsCompensaciones.action?configuracionNegocioVida.id="+negocioId;
		jQuery.ajax({
			url: path,
			type: 'POST',
			async: false,
			success: function (res) {		    
				var result = {}, ids = new Array();
				try{
					result = JSON.parse(res);				
					for (var i in result) {
						ids.push(result[i]);
					}					
					jQuery('#txtIdCompensacion').val(ids);			
					if(flagMensaje){
						var mensaje = 'Se ha creado el Negocio Id ' + negocioId + '</br>';
						if(result['compensacion']){
							mensaje = mensaje + 'con el numero de Compensacion ' + result['compensacion'] + '</br>';
						}
						if(result['contraprestacion']){
							mensaje = mensaje + 'con el numero de Contraprestacion ' + result['contraprestacion'];
						}
						mostrarMensajeInformativo(mensaje);
					}
				}catch (e) {
					
				}
				
			}
		});
	}
	
	
}

function verConfiguracionBono() {	
	sendRequestJQ(null, verDetalleConfiguracionBonosPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+'&configuracionNegocioVida.id='+dwr.util.getValue('configuracionNegocioVida.id'),'contenido_configuracionBonos','limpiarDivsConfiguracionBonos();');
	
}

function verExcepciones() {
	sendRequestJQ(null, verExcepcionesPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva")+'&configuracionNegocioVida.id='+dwr.util.getValue('configuracionNegocioVida.id'),'contenido_excepciones','limpiarDivsExcepciones();');
}

/*function incializarTabs(){
	//var idconfigBono=jQuery("#configuracionBono\\.id").val();
	contenedorTab=window["configuracionBonoTabBar"];
	//Nombre de todos los tabs
	var tabs=["configuracionBonos","excepciones"];
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
	if(!jQuery.isValid(idconfigBono)){
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
	}
	for(var i=0;i<tabs.length;i++){
		var tabName=tabs[i];
		var tab=contenedorTab[tabName];
		jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
			if(jQuery.isValid(idconfigBono)){
				if(jQuery("#tipoAccion").val()!=2){
					event.stopPropagation();
					parent.mostrarMensajeConfirm("Está a punto de abandonar la sección, ¿Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}else{
					
				}
			}else{
				parent.mostrarMensajeInformativo("Debe guardar datos de la configuración","10");
			}
		});
	}
}*/
function irDetalleBonos(tipoAccion) {
	tipoAccion = 1;
	var path= "/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action";
	sendRequestJQ(null, path + "?tipoAccion=" + tipoAccion, targetWorkArea, 'dhx_init_tabbars();');	
}

function limpiarDivsConfiguracionBonos() {
	limpiarDiv('contenido_excepciones');
}
function limpiarDivsExcepciones() {
	limpiarDiv('contenido_configuracionBonos');
}

function limpiarChec(grupoChecks){
	jQuery("."+grupoChecks).each(function(index){
		jQuery(this).attr("checked",false);
	});
}

function llenarGridAgengte(idAgente){
	
	var data = {"agente.id":idAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
	
	jQuery.asyncPostJSON(url,data,responseAgente);
	
}
function responseAgente(json){
	var Id_Row =gridAgente.getRowsNum()+1;
	var idAgente = json.agente.id; 
	var grid="gridAgente";
	gridAgente.addRow(Id_Row,[,idAgente,json.agente.persona.nombreCompleto,"","../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepAgente("+Id_Row+")^_self", ]);
//	alert("se Agrego el agente: "+json.agente.persona.nombreCompleto);
}
function delete_rowExcepAgente(Id_Row) {
	gridAgente.deleteRow(Id_Row);
	activaInactivaSegunChecks();
}

function llenarGridNegocio(){}
function responseNegocio(json){}

jQuery("#chkActivo").click(function(){
	activaFechasSiEsActivo();
});


function delete_rowExcepNegocio(Id_Row) {
	negocioGrid.deleteRow(Id_Row);
}
	function presionTecla(code,cFlag,sFlag){
//		alert(code);
		    if (code==188){
				window.event.keyCode = 188;
				window.event.returnValue=false;
//		      alert('No se puede ingresar espacios en blanco para un nombre de usuario '+code);
		    }
		    else    if (e) { alert(e.which);
		      var tecla=e.which;
//			  alert("2 - "+tecla)
		      if(tecla==44)  {
		        e.preventDefault();
//		        alert('No se puede ingresar espacios en blanco para un nombre de usuario');
		      }
		    }
	}
	 function validateNum (Id_Row, ind, value){
		 var esPorcentaje=jQuery("input:radio[name=configuracionNegocioVida.modoAplicacion.id]:checked").next("label").text();
	        if (ind <= 11 && !rangoFactorGrid._ignore_next){
	        	if (ind == 3 || ind == 4 && !rangoFactorGrid._ignore_next){
	        		if(!validateNumberNeg(value)){
		        		rangoFactorGrid._ignore_next = true;
		        		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
		        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
		        	}
	        	}else
        		if(ind == 11 && esPorcentaje=='Porcentaje' && !rangoFactorGrid._ignore_next){
//	        		alert(ind+" "+esPorcentaje+" "+validateNum4Enteros(value));
	        		if(validateNum4Enteros(value)==false){
		        		rangoFactorGrid._ignore_next = true;
		        		parent.mostrarMensajeInformativo("El valor ingresado debe de ser máximo 4 enteros y 2 decimales, campo bonificación","10");
		        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
	        		}
	        	}else
	        	if(!validateNumber(value)){
	        		rangoFactorGrid._ignore_next = true;
	        		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
	        		rangoFactorGrid.cells(Id_Row, ind).setValue("");
	        	}
	        	rangoFactorGrid._ignore_next = false;
	        }
		}
 function validateNumberNeg(numStr){
		var expreg = /^-?(\d{1,14})(\.\d{1,2})?$/;//^-?(\d{0,14})(\.\d{1,2})?$/;//^(\d{0,14})(\.\d{1,2})?$
		if(numStr!='' && numStr!='&nbsp;'){	
			numStr = numStr.toString().replace(/,/g,'');
			if(numStr!=''){
					return expreg.test(numStr);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
function validateNumber(numStr){
	var expreg = /^(\d{0,14})(\.\d{1,2})?$/;//^(\d{0,14})(\.\d{1,2})?$
	if(numStr!='' && numStr!='&nbsp;'){
	    numStr=numStr.toString().replace(/,/g,'');
		if(numStr!=''){
			return expreg.test(numStr);			
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function validarEstandarEnCampos(obj){
	if(obj.id=='pctSiniestralidadMaxima'){
		if(validateNumberNeg(obj.value)==true){
			jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
			jQuery("#"+obj.id).val("");
		}		
	}else{
		if(validateNumber(obj.value)==true){
			 jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
		}else{
			parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
			jQuery("#"+obj.id).val("");
		}				
	}
} 
function validarEstandarEnModApl(obj){
	if(validateNum4Enteros(obj.value)==true){
		 jQuery("#"+obj.id).val(formato_numero(obj.value, "2", ".", ","));
	}else{
		parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 4 enteros y 2 decimales","10");
		jQuery("#"+obj.id).val("");
	}				
} 

function delete_rowRangoFactor(Id_Row) {
	rangoFactorGrid.deleteRow(Id_Row);
	numerarRowsRangoFactor();
}

function numerarRowsRangoFactor() {
	var Id_Row = "";
	var listaIds = rangoFactorGrid.getAllRowIds(",");
	if(listaIds!=""){
		var idArray = listaIds.split(",");
		for(i=0;i<idArray.length;i++){
			var valor = idArray[i];
			Id_Row = i + 1;
			rangoFactorGrid.cells(valor,0).setValue(Id_Row);
			rangoFactorGrid.cells(valor,12).setValue("../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowRangoFactor("+ Id_Row +")^_self");
			rangoFactorGrid.changeRowId(valor,Id_Row);
		}
	}
}



var ventanaBusquedaAgente;
function pantallaModalBusquedaAgente(){
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgenteBusqueda&closeModal=No";
	sendRequestWindow(null, url,obtenerVentanaBusquedaAgente);
}
function pantallaModalBusquedaAgenteBeneficiario(){
	
	var url="/MidasWeb/fuerzaventa/configuracionBono/abrirModalGirdAgente.action?&idField=txtIdAgenteBeneficiario";
	sendRequestWindow(null, url,obtenerVentanaBusquedaAgente);
	
}
function obtenerVentanaBusquedaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaBusquedaAgente= wins.createWindow("agenteModal", 10, 320, 900, 450);
	ventanaBusquedaAgente.centerOnScreen();
	ventanaBusquedaAgente.setModal(true);
	ventanaBusquedaAgente.setText("Buscar Agente Compensaciones");
	return ventanaBusquedaAgente;
}
function addAgenteProduccion(){
	var busIdAgente = jQuery("#txtIdAgenteBusqueda").val();
	var data = {"agente.id":busIdAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";

	jQuery.asyncPostJSON(url,data,responseAgenteProduccion);
}
function responseAgenteProduccion(json){
	
	var idAgente = json.agente.id;
	var claveAgente = json.agente.idAgente;
	var agente = json.agente.persona.nombreCompleto;
	var indice = jQuery("#listaProduccionAgentes li").length;
	if(idAgente!=null){
		var indicename = indice;
		var option='<li id="'+idAgente+'"><img src="../img/icons/ico_eliminar.gif" onclick="delete_agente('+idAgente+');" style="cursor:pointer;"/>';
		option+='&nbsp;&nbsp;<label for="'+idAgente+'" />'+claveAgente+" - "+agente+'</label> <s:hidden name="lista_Agentes['+indicename+'].agente.id"/> </li>';
		
		jQuery("#listaProduccionAgentes").append(option);
	}			
}
function delete_agente(id){
	jQuery("#listaBeneficiariosAgentes #"+id).remove();
}
function deleteAgenteBenefi(id){
	jQuery("#listaBeneficiariosAgentes .benef"+id).remove();
}

function addAgenteBeneficiario(){
	var busIdAgente = jQuery("#txtIdAgenteBeneficiario").val();
	var data = {"agente.id":busIdAgente};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";

	jQuery.asyncPostJSON(url,data,responseAgenteBeneficiario);
}
function responseAgenteBeneficiario(json){
	
	var idAgente = json.agente.id;
	var agente = json.agente.persona.nombreCompleto;
	if(idAgente!=null){
		var option='<li id="'+idAgente+'"><img src="../img/icons/ico_eliminar.gif" onclick="deleteAgenteBenef('+idAgente+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+agente+'" name="listaAgentes['+idAgente+'].id">'+agente+'</label></li>';
		
		jQuery("#listaBeneficiariosAgentes").append(option);
	}			
}

var ventanaPromotoria;
function pantallaModalPromotoria(){
	var url="/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdBeneficiarioPromotoria";
	sendRequestWindow(null, url,obtenerVentanaBusquedaPromotoria);
	
}

function obtenerVentanaBusquedaPromotoria(){
	var wins = obtenerContenedorVentanas();
	ventanaPromotoria= wins.createWindow("promotoriaModal", 10, 320, 900, 450);
	ventanaPromotoria.centerOnScreen();
	ventanaPromotoria.setModal(true);
	ventanaPromotoria.setText("Buscar Promotoria");
	return ventanaPromotoria;
}
function addPromotoriaBeneficiaria(){
	var idPromotoria = jQuery("#txtIdBeneficiarioPromotoria").val();
	var data = {"promotoria.id":idPromotoria};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPromotoria.action";

	jQuery.asyncPostJSON(url,data,responsePromotoriaBenef);
}
function responsePromotoriaBenef(json){
	
	var idPromotoria = json.promotoria.id;
	var promotoria = json.promotoria.descripcion;
	if(idPromotoria!=null){
		var option='<li id="'+idPromotoria+'"><img src="../img/icons/ico_eliminar.gif" onclick="deletePromotoriaBenef('+idPromotoria+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+promotoria+'" name="listaAgentes['+idPromotoria+'].id">'+promotoria+'</label></li>';
		
		jQuery("#listaPromotoriasBeneficiarias").append(option);
	}			
}
function deletePromotoriaBenef(id){
	jQuery("#listaPromotoriasBeneficiarias #"+id).remove();
}

var ventanaPolizaConfig;
function mostrarModalPolizaConfig(){
	var url="/MidasWeb/fuerzaventa/configuracionBono/agregarPoliza.action?tipoAccion=consulta&idField=txtIdPoliza";
	sendRequestWindow(null, url,obtenerVentanaPolizaConfig);
}
function obtenerVentanaPolizaConfig(){
	var wins = obtenerContenedorVentanas();
	ventanaPolizaConfig= wins.createWindow("modalPoliza", 10, 320, 700, 400);
	ventanaPolizaConfig.centerOnScreen();
	ventanaPolizaConfig.setModal(true);
	ventanaPolizaConfig.setText("Búsqueda de Póliza");
	return ventanaPolizaConfig;
}
function addPolizaConfig(){
	var busIdPoliza = jQuery("#txtIdPoliza").val();
	var data = {"filtroPoliza.idPoliza":busIdPoliza};
	var url ="/MidasWeb/fuerzaventa/configuracionBono/seleccionarPoliza.action";
//	alert(busIdPoliza + url);
	jQuery.asyncPostJSON(url,data,responsePolizaConfig);
}
function responsePolizaConfig(json){
	var idPoliza = json.filtroPoliza.idPoliza;
	var numeroPoliza = json.filtroPoliza.numeroPolizaMidasString;
	if(idPoliza!=null){
		var option='<li id="'+idPoliza+'"><img src="../img/icons/ico_eliminar.gif" onclick="delete_poliza('+idPoliza+');" style="cursor:pointer;"/>';
		option+='&nbsp;<label for="'+numeroPoliza+'" name="listaConfigPolizas['+idPoliza+'].id">&nbsp;&nbsp;'+numeroPoliza+'</label></li>';
		
		jQuery("#listaPolizasProduccion").append(option);
	}			
}
function delete_poliza(id){
	jQuery("#listaPolizasProduccion #"+id).remove();
}

function checarChec(elementoAchecar){
	jQuery("#"+elementoAchecar).attr("checked",true);
}


function salirConfigBono(){	
	var tipoAccion = jQuery("#tipoAccion").val();
	var url = mostrarContenedorConfiguracionBonosPath;
	if(tipoAccion != 2){
		parent.mostrarMensajeConfirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?","20","sendRequestJQ(null,'"+url+"',targetWorkArea,null)",null,null,null);
	}else{
		sendRequestJQ(null, url , targetWorkArea, 'dhx_init_tabbars();');
	}
}

function mostrarHistoricoConfigBono(){
	var tipoOperacion = 70;
	var url = '/MidasWeb/fuerzaventa/agente/mostrarHistorial.action?idTipoOperacion='+tipoOperacion+ "&idRegistro="+parent.dwr.util.getValue("configuracionNegocioVida.id");
	mostrarModal("historicoGrid", 'Historial', 100, 200, 700, 400, url);
}

function obtenerFuncionTab(tab){
	var tipoOperac=70;
	if(tab==0){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionNegocioVida.id='+dwr.util.getValue('configuracionNegocioVida.id')+'&tabActiva=configuracionBonos&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('configuracionNegocioVida.id'),targetWorkArea,'dhx_init_tabbars();');
  }else if(tab==1){
        sendRequestJQ(null,'/MidasWeb/fuerzaventa/configuracionBono/verDetalleBono.action?tipoAccion='+dwr.util.getValue('tipoAccion')+'&configuracionNegocioVida.id='+dwr.util.getValue('configuracionNegocioVida.id')+'&tabActiva=excepciones&idTipoOperacion='+tipoOperac+'&idRegistro='+dwr.util.getValue('configuracionNegocioVida.id'),targetWorkArea,'dhx_init_tabbars();');
  }
}

function enableCheckBox(){
	jQuery(".js_checkEnable").each(function(index){		
		jQuery(this).attr("disabled",true);
	});	
}

function checarChecksCascadeados(elementoAchecar){
	dwr.engine.beginBatch();
	jQuery("#"+elementoAchecar).attr("checked",true);
	dwr.engine.endBatch({async:false});
		checkCombosCascadeados();
	
}

function checkCombosCascadeados(){
	var arrSeleccionados = new Array();
	jQuery("#ajax_listaLineaVenta input:checked").each(function(index){
		var clave={"clave":jQuery(this).val()};		
		arrSeleccionados.push(clave);
	});
	
	var data=jQuery.convertJsonArrayToParams(arrSeleccionados,["clave"],"listaLineaVentaSeleccionadas");
	var url ="/MidasWeb/fuerzaventa/configuracionBono/listarProductosPorLineaDeVenta.action";
	
	jQuery.asyncPostJSON(url,data,loadChecksProductos);
}
function loadChecksProductos(json){
	//comboChecksCreate(json.listaProductos,"ajax_listaProductos","listaProductosSeleccionados","cascadeoRamosXProducto","idProducto");	
}

//function grabarYcopiar(){
//	
//	var banderaGardadoPol = "";
//	var banderaGardadoNeg = "";
//	var banderaGardadoAgt = "";
//	var banderaGuardadoCveAmis = "";
//	
//	guardarExcepcion();
//	//saveConfigBonoExcluTipoCedula();
//	saveConfigBonoExcluTipoBono();
//	banderaGardadoPol = guardarExcepcionPoliza();
//	banderaGardadoNeg = guardarNegocioExcepcion();
//	banderaGardadoAgt = guardarAgenteExcepcion();
//	banderaGuardadoCveAmis = guardarExcepcionClaveAmis();
//	
//	
//	
//	if(banderaGardadoPol == "correcto" && banderaGardadoNeg== "correcto" && banderaGardadoAgt == "correcto" && banderaGuardadoCveAmis=="correcto"){
//		var path = "/MidasWeb/fuerzaventa/configuracionBono/grabarYcopiarConfigBonos.action?"
//			+"tipoAccion="+parent.dwr.util.getValue("tipoAccion")
//		    +"&configuracionNegocioVida.id="+parent.dwr.util.getValue("configuracionNegocioVida.id")
//		    +"&tabActiva=excepciones";
//		sendRequestJQ(null, path, targetWorkArea, null);
//		
//	}else{
//		parent.mostrarMensajeInformativo("Las excepciones seleccionadas deben de aplicar en el porcentaje de descuento o contener montos en los porcentajes de bono. Favor de no dejar valores nulos.","10");
//	}
//	
//}

//ventana y grid Polizas

var listarPolizaG;
var gridPoliza;
var listarPolizaGrid;





function llenarGridPoliza(){}

function cargarGridListadoPolizas(){}

//function listarPolizas(){}
//function cargarGridPolizalistado(){
//}

//function cargarGridPoliza(){}

//function delete_rowExcepPoliza(Id_Row) {
//	gridPoliza.deleteRow(Id_Row);
//}

//function guardarExcepcionPoliza(){}
//function responseGuardadoExcepPoliza(json){
////	alert(json);
//}
//polizas excepciones
//function addPolizaExcepciones(){}
//function responsePolizaExcepcion(json){}



//jQuery("#descuentoPoliza").change(function(){});	


//function activaInactivaSegunChecks(){
//	var marcadosPoliza = gridPoliza.getCheckedRows(0);
//	var marcadosAgente = gridAgente.getCheckedRows(0);
//	var descuentoPoliza=jQuery("#descuentoPoliza").val();
//	
//	if(marcadosPoliza || marcadosAgente){		
//		jQuery("#todasPolizas").removeAttr("checked");		
//	}else{
//		jQuery("#todasPolizas").attr("checked","checked");
//		if(descuentoPoliza==""){
//			jQuery("#todasPolizas").removeAttr("checked");
//		}
//	}
//}

//function existeCrecimiento(){
//	var texto =	jQuery("#comparacionProduccionLbl").text();
//	var idConf=dwr.util.getValue("configuracionNegocioVida.id");		
//	if(idConf!=''&&idConf!=null){
//		if(dwr.util.getValue("configuracionNegocioVida.porcentajeImporteDirecto")==null || dwr.util.getValue("configuracionNegocioVida.porcentajeImporteDirecto")==''){
//			validacionService.validarExisteCrecimiento(idConf,function(data){
//				if(data){
//					jQuery("#periodoComparacion").removeClass("ErrorField");
//					jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""));
//					jQuery("#periodoComparacion").attr('disabled',true);
//					jQuery("#periodoComparacion option[value='']").attr("selected",true);
//				}
//				else{
//					jQuery("#periodoComparacion").attr('disabled',false);
//				}
//			});
//		}
//	}
//	else{
//		jQuery("#periodoComparacion").removeClass("jQrequired");
//		jQuery("#comparacionProduccionLbl").text(texto.replace("*", ""))
//		jQuery("#periodoComparacion").attr('disabled',true);
//		jQuery("#periodoComparacion option[value='']").attr("selected",true);
//	}	
//}


function activaBtnSeleccionarAgtBenef(){	
	if(jQuery("#todosAgentesBoolean").is(':checked')==true){
		jQuery("#btnBenefAgt").css("display","none");
		jQuery("#listaBeneficiariosAgentes").html("");
	}else{
		jQuery("#btnBenefAgt").css("display","block");
	}
}

function activaBtnSeleccionarPromoBenef(){	
	if(jQuery("#todasPromotoriasBoolean").is(':checked')==true){
		jQuery("#btnBenefProm").css("display","none");
		jQuery("#listaPromotoriasBeneficiarias").html("");
	}else{
		jQuery("#btnBenefProm").css("display","block");
	}
}



function guardarConfigBono_AndValidConf(){
	var nombreNegocio=jQuery("#descripcionnegociovida").val();	
	if(nombreNegocio == '' && nombreNegocio == null){
		parent.mostrarMensajeInformativo("Favor de llenar el campo de Nombre Negocio","10");
	}else{
		if(validateForm()){
			openCompensacionAdicional();
		}else{
			alert('Por favor revise los campos del formulario');
		}
	}
}

/** Da formato a un número para su visualización 
 * numero (Number o String) - Número que se mostrará 
 * decimales (Number, opcional) - Nº de decimales (por defecto, auto) 
 * separador_decimal (String, opcional) - Separador decimal (por defecto, coma) 
 * separador_miles (String, opcional) - Separador de miles (por defecto, ninguno) 
 */ 
function formato_numero(numero, decimales, separador_decimal, separador_miles){
	var num = numero.indexOf(',');
	if (num !=-1){
		var i =0;
		var splitStr = numero.split('.');
		var splitLeft = splitStr[0];
		var entero = splitLeft.split(',');
		for (i =0; entero.size()-1>=i; i++) {
			if (i==0){
				if(entero[i].length>3){
					parent.mostrarMensajeInformativo("El valor Ingresado no es Correcto","10");
					return "";
				}
			}else{
				if(entero[i].length!=3){
					parent.mostrarMensajeInformativo("El valor Ingresado no es Correcto","10");
					return "";
				}
			}
		}
		if(separador_decimal!='' && numero.indexOf('.')==-1){
			numero = numero+'.00';
		}else if(numero.indexOf('.')!=-1){
			numero = numero.replace('.00','');
		}
			
		return numero;
	}
	// Convertimos el punto en separador_decimal  
	numero=parseFloat(numero);     
	if(isNaN(numero)){         
		return "";     
	}
	numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");     
	if(separador_miles){         // Añadimos los separadores de miles         
		var miles=new RegExp("(-?[0-9]+)([0-9]{3})");        
		while(miles.test(numero)) {             
			numero=numero.replace(miles, "$1" + separador_miles + "$2");         
			}     
		}
		if(separador_decimal!='' && numero.indexOf('.')==-1){
			numero = numero+'.00';
		}else if(numero.indexOf('.')!=-1){
			numero = numero.replace('.00','');
		}
		return numero; 
	}
	function presionTecla(e){
	  if (window.event)  {
	    var tecla=window.event.keyCode;
	    if (tecla==44)    {
	      window.event.returnValue=false;
	    }
	  }  else    if (e) { alert(e.which);
	      var tecla=e.which;
	      if(tecla==44)  {
	        e.preventDefault();
	      }
	    }
	}
	
	function validateNum4Enteros(numStr){
		var expreg = /^(\d{0,4})(\.\d{1,2})?$/;
		if(numStr!='' && numStr!='&nbsp;'){
		    numStr=numStr.toString().replace(/,/g,'');
			if(numStr!=''){
				return expreg.test(numStr);			
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	function validarCheckModoAplicacion(){		
		var texto = jQuery('input:radio[name=configuracionNegocioVida.modoAplicacion.id]:checked').next().text();
		if (texto == 'Porcentaje'){
			jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","9");
			if(validateNum4Enteros(jQuery("#valorPorcentajeImporteDirecto").val())==true){
				jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
			}else{
				parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 4 enteros y 2 decimales","10");
				jQuery("#valorPorcentajeImporteDirecto").val("");
			}
		}else if(texto == 'Importe'){
			jQuery("#valorPorcentajeImporteDirecto").attr("maxlength","21");
			if(validateNumber(jQuery("#valorPorcentajeImporteDirecto").val())==true){
				jQuery("#valorPorcentajeImporteDirecto").val(formato_numero(jQuery("#valorPorcentajeImporteDirecto").val(), "2", ".", ","));
			}else{
				parent.mostrarMensajeInformativo("El valor Ingresado debe de ser máximo 14 enteros y 2 decimales","10");
				jQuery("#valorPorcentajeImporteDirecto").val("");
			}
		}
	}
	
	function desavilitarEdisionSegunBenef(){
		
		alert("Ingreso a la funcion");
		
		var tipoBenefConfig = document.getElementById("configuracionBono_benef").value;
		var polPorBonAgt = document.getElementById("porBonoAgt");
		var polPorBonoProm = document.getElementById("porBonoProm");
		if(tipoBenefConfig == 838 ){
			alert('Promotoria');
			if(polPorBonAgt == null){
				
				alert('elemeto vacio');
			}else{
				
				alert('elemeto NO vacio');
			}
		}else if(tipoBenefConfig == 839){
			alert('Agente');
		}else if(tipoBenefConfig == 840){
			alert('Ambos');
		}
		
		
	}	
function pad(n, width, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}


function validateNum5Enteros(numStr){
	var expreg = /^(\d{0,5})?$/;
	if(numStr!='' && numStr!='&nbsp;'){
	    numStr=numStr.toString().replace(/,/g,'');
		if(numStr!=''){
			return expreg.test(numStr);			
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function validaBanca() {
	var str = jQuery("#lineaNegocio").val();
    alert(str);
    var n = str.search("BANCA");
    if (n >= 0){
    	jQuery("#btnBuscarAgenteSeycos").attr('disabled',true);
    }
}

function openCompensacionAdicional(){
	blockPage();
	var negocioId = Number(jQuery('#txtIdNegocio').val());
	var tabActiva = contieneBancaGerencias() ? '&tabActiva=contraprestacion' : '';
	var vidaIndiv = '&vidaIndiv='+contieneVidaInd();
	ventanaCompensacion = window.open("/MidasWeb/compensacionesAdicionales/init.action?ramo=V&idNegocioVida="+negocioId+tabActiva+vidaIndiv, 'Compensaciones', 'height=600,width=1200,modal=yes,resizable=yes,scrollbars=auto');
	ventanaCompensacion.focus();
	compensacionRecargado = false;
	temporizadoWinComp = setInterval("checkVentanaCompensacion();", 2000);
}


function checkVentanaCompensacion(){
	if (ventanaCompensacion.closed == true ){
		clearInterval(temporizadoWinComp);
		if (compensacionRecargado == false){
			wCloseCompensacion();
		}
	}
}

function pruebaMensajeVentana(){
	console.error('Prueba Mensaje Ventana: EXITOSO!');
}

function wCloseCompensacion(flagMensaje){
	findIdsCompensaciones(flagMensaje);
	unblockPage();
	compensacionRecargado = true;
}

function validateForm(){
	
	var valideNombreNegocio = jQuery('#descripcionnegociovida').val().trim().length > 0;
	
	if(valideNombreNegocio){	
		var arrayListas = ['ajax_listaProductos','ajax_listaRamosSeycos','ajax_listaSubRamosSeycos','ajax_lineaNegocioSeycos', 'listaBeneficiariosAgentes'];
		var valide;
		for(count = 0; count<arrayListas.length;count++){
			valide = validateListas(arrayListas[count]);
			if(!valide){
				return false;
			}
		}
	}else{
		return false;
	}
	
	return true;
}

function validateListas(id){
	
	var elements, validate = false;
	
	if(id === 'listaBeneficiariosAgentes'){		
		if(!contieneBancaGerencias()){
			elements = jQuery('#'+id).find('li');
			if(elements.length > 0){
				validate = Number( elements[0].id ) > 0;
			}
		}else{
			validate=true;
		}	
	}else{		
		elements = jQuery('#'+id).find('input');		
		if(elements.length > 0){
			validate = elements.is(':checked');
		}
	}
	return validate;
	
}


function getListsNegocioVida(){
	var arrayListas = [
	                   ['ajax_listaProductos', 'productoLong'],
	                   ['ajax_listaRamosSeycos', 'ramoLong'],
	                   ['ajax_listaSubRamosSeycos', 'subRamoLong'],
	                   ['ajax_lineaNegocioSeycos', 'lineaNegocioLong'],
	                   ['ajax_listaGerenciaSeycos', 'gerenciaLong']
	                  ];
	var listsNegocioVida = '';
	var item;
	for(count = 0; count<arrayListas.length;count++){
		item = arrayListas[count];
		listsNegocioVida = listsNegocioVida + getValuesList(item[0], item[1]);
	}
	console.info(listsNegocioVida);
	return listsNegocioVida;
}

function getValuesList(id, name){
	var list = '';
	var index = 0;
	jQuery('#'+id+ ' li').find('input').each(function(i){
		if(jQuery(this).is(':checked')){
			list = list+'&'+name+'['+index+']'+'='+this.value;
			index++;
		}
	});	
	return list;
}

function getAgenteList(){
	  var list = '';
	  var index = 0;
	  jQuery('#listaBeneficiariosAgentes').find('li').each(function(i){
	    list = list+'&'+'agenteLong'+'['+index+']'+'='+this.id;
	    index++;
	  });    
	  return list;
}

function contieneBancaGerencias(){
	var contiene = false;
	jQuery('#ajax_listaGerenciaSeycos li').each(function(i){
		if(jQuery(this).find('input').is(':checked') && jQuery(this).text().trim().indexOf('BANCA') > -1){
			contiene = true;
		}
	});
	return contiene;
}

function contieneVidaInd(){
	var result = "";
	jQuery("#ajax_listaRamosSeycos input:checked").each(function(index){
		var claveRam=jQuery(this).val();
		//if(jQuery(this).find('input').is(':checked')){
			result = claveRam;
		//}
	});
	
	return result;
}

function checkAllProducto(){
   if (jQuery('#marcarTodo').is(':checked')) {
		 jQuery("#ajax_listaProductos input[type=checkbox]").attr('checked', true);
   } else {
	     jQuery("#ajax_listaProductos input[type=checkbox]").attr('checked', false);
   }
}
function checkAllLineasNegocio(){
   if (jQuery('#marcarTodasLineas').is(':checked')) {
		 jQuery("#ajax_lineaNegocioSeycos input[type=checkbox]").attr('checked', true);
   } else {
		 jQuery("#ajax_lineaNegocioSeycos input[type=checkbox]").attr('checked', false);
   }
}


