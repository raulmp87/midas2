function onChangeRamo(){
	var idSeleccionados="";
	jQuery('input[name="ramoLong[1]"').attr("disabled", false);
	jQuery('input[name="ramoLong[0]"').attr("disabled", false);	
	jQuery("#ajax_listaRamosSeycos input:checked").each(function(index){
		var claveRam=jQuery(this).val();
		idSeleccionados=claveRam+","+idSeleccionados;
		if(jQuery(this).is(":checked")) {
			var nameCheck = jQuery(this).attr('name');
			if(nameCheck === 'ramoLong[0]' ){
				jQuery('input[name="ramoLong[1]"').attr("disabled", true);
			}else if(nameCheck === 'ramoLong[1]'){
				jQuery('input[name="ramoLong[0]"').attr("disabled", true);
			}
        }
	});

	var idNexList="";
	jQuery("#ajax_listaSubRamosSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();
		listadoService.listaSubramoSeycos(idSeleccionados,idNexList,
				function(data){
					comboChecksCreateCascade(data,'ajax_listaSubRamosSeycos','subRamoLong','onChangeSubRamo','');
				});
		dwr.engine.endBatch({async:false});
		onChangeSubRamo();
}

function onChangeSubRamo(){
	var idSeleccionados="";
	jQuery("#ajax_listaSubRamosSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
		
		dwr.engine.beginBatch();
		listadoService.listaProductoSeycos(idSeleccionados,idNexList,
				function(data){
			comboChecksCreateCascade(data,'ajax_listaProductos','listaProductosSeleccionados','onChangeProducto',"idProducto");
				});
		dwr.engine.endBatch({async:false});
		onChangeProducto();
		
}
function onChangeProducto(){
	var idSeleccionados="";
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_lineaNegocioSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	
//	if(jQuery.isValid(idSeleccionados)){
		dwr.engine.beginBatch();		
		listadoService.listaLineaNegocioSeycos(idSeleccionados,idNexList,
				function(data){
				comboChecksCreateCascade(data,'ajax_lineaNegocioSeycos','listaLineaNegocioSeycos','onChangeLineaNegocio','');
				});
		dwr.engine.endBatch({async:false});
		onChangeLineaNegocio();
}

function onChangeLineaNegocio(){
	var idSeleccionados="";
	jQuery("#ajax_lineaNegocioSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionados=clave+","+idSeleccionados;		
	});

	var idNexList="";
	jQuery("#ajax_lineaNegocioSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
		dwr.engine.beginBatch();
		/*listadoService.listaProductoSeycos(idSeleccionados,idNexList,
				function(data){
			//comboChecksCreateCascade(data,'ajax_listaProductos','listaProductosSeleccionados','onChangeProducto','');
				});*/
		dwr.engine.endBatch({async:false});
		//onChangeProducto();
}

function onChangeRamo_LineaNegocio(){
	var idSeleccionadosRamo="";
	jQuery("#ajax_listaRamosSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionadosRamo=clave+","+idSeleccionadosRamo;		
	});
	
	var idSeleccionadosProd="";	
	jQuery("#ajax_listaProductos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idSeleccionadosProd=clave+","+idSeleccionadosProd;		
	});	
	
	if(idSeleccionadosRamo==""){
		idSeleccionadosProd="";
	}
	
	var idNexList="";	
	jQuery("#ajax_lineaNegocioSeycos input:checked").each(function(index){
		var clave=jQuery(this).val();		
		idNexList=clave+","+idNexList;		
	});
	dwr.engine.beginBatch();
	listadoService.listaLineaNegocioSeycos(idSeleccionadosProd,idSeleccionadosRamo,idNexList,
			function(data){
				comboChecksCreateCascade(data,'ajax_lineaNegocioSeycos','lineaNegocioLong','','');
			});
	dwr.engine.endBatch({async:false});
}

function onChangeGerenciaNegocio(){
	jQuery("#listaBeneficiariosAgentes").html("");
}

function comboChecksCreateCascade(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"();";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li>';
					option+='<input type="checkbox" name="'+nameObj+'['+i+']" id="'+nameObj+'['+i+']" value="'+key+'" ';
				  	option+= "checked='checked'";
					option+='onclick="'+fn+'" />';
					option+= value
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}
function comboChecksCreateCascadeCobertura(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"();";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li>'; //<label for="'+key+'">';
					option+='<input type="checkbox" checked="checked" class="js_checkEnable checkCobertura" name="'+nameObj+'['+i+'].'+property+'" id="'+nameObj+'['+i+'].'+property+'" value="'+key+'" ';
					if(check==1){
						option+= "checked='checked'";
						}
					option+='onchange="'+fn+'" />';
					option+= value //+ '</label>';
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}

function comboChecksCreateCascadeLineaNegocio(jsonList,idSelect,nameObj,onclickFunction,property){
	if(onclickFunction){
		fn = onclickFunction +"();";
	}else{
		fn = "";
	}	
	var select= jQuery("#"+idSelect);
	select.html("");
	if(jsonList!=null && jsonList.length>0){
		for(var i=0;i<jsonList.length;i++){
			var json=jsonList[i];		
			var key=eval('json.id');
			var value=eval('json.valor');
			var check=eval('json.checado');
			if(key!=null){
				var option='<li>'; //<label for="'+key+'">';
					option+='<input type="checkbox" class="js_checkEnable lineaNegocio" name="'+nameObj+'['+i+'].'+property+'" id="'+nameObj+'['+i+'].'+property+'" value="'+key+'" ';
					if(check==1){
						option+= "checked='checked'";
						}
					option+='onchange="'+fn+'" />';
					option+= value //+ '</label>';
					option+='</li>';
				
				select.append(option);
			}			
		}
	}
}

