/**
 * 
 * Powered by AddMotions.
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso
 * 
 */
	/**
	 * Declaracion de variables
	 */	
	var esContraprestacion = null;
	/**
	 *  CONTRAPRESTACION
	 */

	/**
	 * Por Prima Contraprestaciones
	 */
	var $textPorcePrimaContra = null;
	var $textMontoPrimaContra = null;
	var $selectBaseCalContra = null;
	var $selectMonedContra = null;
	var $checkPrimaContra = null;
	var $radioCondicionesCalcContra = null;
	var $tableContenedorCondicionesPrimaContra = null;
	var $arregloElemsPrimaContra = null;
	var sLineasNegocioContra = null;
	var banderaLineasNegocioContra = true;
	
	/**
	 * Por Baja Siniestralidad Contraprestaciones
	 */
	var $textPorceBajaSiniestralContra = null;
	var $textMontoBajaSiniestralContra = null;
	var $selectBaseCalcBajaSiniContra = null;
	var $selectMonedaBajaSiniContra = null;
	var $checkBajaSiniestralContra = null;
	var $btnAgregaRangosContra = null;
	var sRangosBSContra = null;
	var $arregloElemsBajaSiniContra = null;

	/**
	 * Compensaciones Por Prima
	 */
	var $textPorcePrimaGral = null;
	var $textMontoPrimaGral = null;
	var $textPorcePrimaAgenGral = null;
	var $textPorcePrimaPromoGral = null;
	var $selectBaseCalGral = null;
	var $checkPrimaGral = null;
	var $divContenedorCondicionesPrima = null;
	var $radioCondicionesCalc = null;
	var $tableContenedorCondicionesPrimaGral = null;
	var $arregloElemsPrima = null;
	var sLineasNegocio = null;
	var banderaLineasNegocio = true;
	
	/**
	 * Por Baja Siniestralidad
	 */
	var $textPorceBajaSiniestralGral = null;
	var $textMontoBajaSiniestralGral = null;
	var $textPorceBajaSiniAgenGral = null;
	var $textPorceBajaSiniPromoGral = null;
	var $selectBaseCalcBajaSiniGral = null;
	var $checkBajaSiniestralGral = null;
	var $btnAgregaRangos = null;
	var sRangosBS = null;
	var $arregloElemsBajaSini = null;
	
	/**
	 * Por Derecho de Poliza
	 */
	var $textPorcePoliza = null;
	var $textMontoPoliza = null;
	var $textPorceAgePoliza = null;
	var $textPorcePromoPoliza = null;
	var $checkEmisInterPolizaCompensacion = null;
	var $checkEmisExterPolizaCompensacion = null;
	var $checkDerechoPoliza = null;
	var $tableEmisionPoliza = null;
	var $arregloElemsDerechoPoliza = null;
	
	/**
	 * Por Utilidad
	 */
	var $checkPorUtilidad = null;
	var $checkPorUtilidad = null;
	var $textMontoPorUtilidadTotal = null;
	var $textParticipacionUtilidades = null;
	var $textMontoUtilidad = null;
	var $textParcialidad = null;
	var $arregloElemsPorUtilidad = null;
	
	/**
	 * Por Cumplimiento de Meta
	 */
	var $checkCumplimientoMeta = null;
	var $baseCalculoCumplimientoMeta = null;
	var $monedaCumplimientoMeta = null;
	var $agregarRangosCumplimientoMeta = null;
	var $arregloElemsPorCumplimientoMeta= null;
	/**
	 * Iniciar/Asignar las variables
	 */
	function initComponents(){
		/**
		 * Por Prima Contraprestacion
		 */
		$textPorcePrimaContra = jQuery('#textPorcePrimaContra');
//		$textMontoPrimaContra = jQuery('#textMontoPrimaContra');
		$selectBaseCalContra = jQuery('#selectBaseCalPrimaContra');
		$selectMonedContra = jQuery('#selectMonedaPrimaContra');
		$checkPrimaContra = jQuery('#checkPrimaContra');
		$radioCondicionesCalcContra = jQuery('#radioCondicionesCalcContra');
		$tableContenedorCondicionesPrimaContra = jQuery('#tableContenedorCondicionesPrimaContra');
		$arregloElemsPrimaContra = [$textPorcePrimaContra,$selectBaseCalContra,$selectMonedContra,$radioCondicionesCalcContra,$tableContenedorCondicionesPrimaContra];
		
		/**
		 * Por Baja Siniestralidad Contraprestaciones
		 */
		$textPorceBajaSiniestralContra = jQuery("#textPorceBajaSiniestralContra");
//		$textMontoBajaSiniestralContra = jQuery("#textMontoBajaSiniestralContra");
		$selectBaseCalcBajaSiniContra = jQuery("#selectBaseCalcBajaSiniContra");
		$selectMonedaBajaSiniContra = jQuery("#selectMonedaBajaSiniContra");
		$checkBajaSiniestralContra = jQuery("#checkBajaSiniestralContra");
		$btnAgregaRangosContra = jQuery("#divBtnAgregaRangosContra");
		$arregloElemsBajaSiniContra = [$textPorceBajaSiniestralContra,$selectBaseCalcBajaSiniContra,$selectMonedaBajaSiniContra];
		
		/**
		 * Por Prima
		 */
		$textPorcePrimaGral = jQuery('#textPorcePrimaGral');
//		$textMontoPrimaGral = jQuery('#textMontoPrimaGral');
		$textPorcePrimaAgenGral = jQuery('#textPorcePrimaAgenGral');
		$textPorcePrimaPromoGral = jQuery('#textPorcePrimaPromoGral');
		$selectBaseCalGral = jQuery('#selectBaseCalGral');
		$checkPrimaGral = jQuery('#checkPrimaGral');
		$divContenedorCondicionesPrima = jQuery('#tableContenedorCondicionesPrimaGral');
		$radioCondicionesCalc = jQuery('#radioCondicionesCalc');
		$tableContenedorCondicionesPrimaGral = jQuery('#tableContenedorCondicionesPrimaGral');
		$arregloElemsPrima = [$textPorcePrimaGral,$textPorcePrimaAgenGral,$textPorcePrimaPromoGral,$selectBaseCalGral,$radioCondicionesCalc,$tableContenedorCondicionesPrimaGral];
		
		/**
		 * Por Baja Siniestralidad
		 */
		$textPorceBajaSiniestralGral = jQuery('#textPorceBajaSiniestralGral');
		$textMontoBajaSiniestralGral = jQuery('#textMontoBajaSiniestralGral');
		$textPorceBajaSiniAgenGral = jQuery('#textPorceBajaSiniAgenGral');
		$textPorceBajaSiniPromoGral = jQuery('#textPorceBajaSiniPromoGral');
		$selectBaseCalcBajaSiniGral = jQuery('#selectBaseCalcBajaSiniGral');
		$checkBajaSiniestralGral = jQuery('#checkBajaSiniestralGral');
		$btnAgregaRangos = jQuery('#divBtnAgregaRangos');
		$arregloElemsBajaSini = [$textPorceBajaSiniestralGral,$textPorceBajaSiniAgenGral,$textPorceBajaSiniPromoGral,$selectBaseCalcBajaSiniGral,jQuery("#divRangosSiniestralidadGrid")];
		
		/**
		 * Derecho de Poliza
		 */
		$textPorcePoliza = jQuery('#textPorcePoliza');
		$textMontoPoliza = jQuery('#textMontoPoliza');
		$textPorceAgePoliza = jQuery('#textPorceAgePoliza');
		$textPorcePromoPoliza = jQuery('#textPorcePromoPoliza');
		$checkEmisInterPolizaCompensacion = jQuery('#checkEmisInterPolizaCompensacion');
		$checkEmisExterPolizaCompensacion = jQuery('#checkEmisExterPolizaCompensacion');
		$checkDerechoPoliza = jQuery('#checkDerechoPoliza');
		$tableEmisionPoliza = jQuery('#tableEmisionPoliza');
		$arregloElemsDerechoPoliza = [$textPorcePoliza,$textMontoPoliza,$textPorceAgePoliza,$textPorcePromoPoliza,$checkEmisInterPolizaCompensacion,$checkEmisExterPolizaCompensacion];
		/**
		 * Por Utilidad
		 */
		$checkPorUtilidad = jQuery('#checkUtilidadContra');
		$textMontoPorUtilidadTotal = jQuery('#textMontoUtilidadTotal');
		$textParticipacionUtilidades = jQuery('#textPorceParticiUtilidadContra');
		$textMontoUtilidad = jQuery('#textMontoParticiUtilidadContra');
		$textParcialidad = jQuery('#textMontoParcialidad');		
		$arregloElemsPorUtilidad = [$textMontoPorUtilidadTotal, $textParticipacionUtilidades, $textMontoUtilidad, $textParcialidad];
		
		/**
		 * Por Cumplimiento de Meta
		 */
		$checkCumplimientoMeta = jQuery('#checkCumplimientoMeta');
		$baseCalculoCumplimientoMeta = jQuery('#selectBaseCalcCumplimientoMeta');
		$monedaCumplimientoMeta = jQuery('#selectMonedaCumplimientoMeta');
		$agregarRangosCumplimientoMeta = jQuery('#divBtnAgregaCumplimientoMeta');
		$arregloElemsPorCumplimientoMeta = [$checkCumplimientoMeta, $baseCalculoCumplimientoMeta, $monedaCumplimientoMeta, $agregarRangosCumplimientoMeta,jQuery("#divRangosSiniestralidadCumplimientoMeta")];
	
		desactivarPorcPromotor();
	}
	
	/**
	 * Asignar/ejecutar eventos iniciales
	 */
	function initEvents() {
		/// Por Prima Contraprestacion
		$checkPrimaContra.click(function(){
			checkPorConfiguracion($arregloElemsPrimaContra,this.id,'classDisabledPrimaContra');	
		});
		/// Por Baja Siniestralidad Contraprestacion
		$checkBajaSiniestralContra.click(function(){
			checkPorConfiguracion($arregloElemsBajaSiniContra,this.id,'classDisabledBajaSiniContra');
		});		
		/// Por Prima
		$checkPrimaGral.click(function(){
			var check = checkPorConfiguracion($arregloElemsPrima,this.id,'classDisabledPrima');
			if(check){
				validatePersonaResponsable('textPorcePrimaPromoGral', 'textPorcePrimaAgenGral');
			}			
		});
		/// Por Baja Siniestralidad
		$checkBajaSiniestralGral.click(function(){
			var check = checkPorConfiguracion($arregloElemsBajaSini,this.id,'classDisabledBajaSini');
			if(check){
				validatePersonaResponsable('textPorceBajaSiniPromoGral', 'textPorceBajaSiniAgenGral');
			}
		});
		/// Derecho de Poliza
		$checkDerechoPoliza.click(function(){
			var check = checkPorConfiguracion($arregloElemsDerechoPoliza,this.id,'classDisabledDerechoPoliza');
			if(check){
				validatePersonaResponsable('textPorcePromoPoliza', 'textPorceAgePoliza');
			}
		});
		
		/**
		 * Por Utilidad
		 */
		$checkPorUtilidad.click(function(){
			checkPorConfiguracion($arregloElemsPorUtilidad,this.id,'classDisabledPorUtilidad');	
		});
		
		/**
		 * Por Cumplimiento Meta
		 */
		$checkCumplimientoMeta.click(function(){
			checkPorConfiguracion($arregloElemsPorCumplimientoMeta,this.id,'classDisabledCumplimientoMeta');	
		});

		
		jQuery(".radioCondicionesCalcGrupoContra").click(function(){
			verLineasNegocio('subramosAutosGridContra','radioCondicionesCalcGrupoContra');
		});
		
		
		jQuery(".radioCondicionesCalcGrupo").click(function(){
			verLineasNegocio('subramosAutosGrid','radioCondicionesCalcGrupo');
		});
		
		verLineasNegocio('subramosAutosGridContra','radioCondicionesCalcGrupoContra');
		verLineasNegocio('subramosAutosGrid','radioCondicionesCalcGrupo');	
		
		jQuery(".validarPorcentaje").keyup(validarPorcentaje);
		jQuery(".validarPorcentaje").blur(function(){ponerDecimales(this.id, 20);});
		jQuery(".validarMonto").keyup(validarMonto);
        
	}
	
	/**
	 * init,Funcion para Inicializar Componentes y Eventos
	  @eventos funcion que manda llamar a initComponents() y initEvents() para inicializar componentes y eventos
	 */
	function init() {
		initComponents();
		initEvents();
	}
	init();
	 	
	function equis(id){
		var elemento = jQuery("#" + id);		
		alert(elemento.attr('type'));
	}


	function initDataChecks(configCA) {

		if(configCA.isContraprestacion){
			/// Contraprestacion
			
			checkPorConfiguracion($arregloElemsPrimaContra,$checkPrimaContra.attr('id'),'classDisabledPrimaContra');
			checkPorConfiguracion($arregloElemsBajaSiniContra,$checkBajaSiniestralContra.attr('id'),'classDisabledBajaSiniContra');
			checkPorConfiguracion($arregloElemsPorUtilidad,$checkPorUtilidad.attr('id'),'classDisabledPorUtilidad');
			checkPorConfiguracion($arregloElemsPorCumplimientoMeta,$checkCumplimientoMeta.attr('id'),'classDisabledCumplimientoMeta');
			verLineasNegocio('subramosAutosGridContra','radioCondicionesCalcGrupoContra');
		}else{
			/// Compensacion
			checkPorConfiguracion($arregloElemsPrima,$checkPrimaGral.attr('id'),'classDisabledPrima');
			checkPorConfiguracion($arregloElemsBajaSini,$checkBajaSiniestralGral.attr('id'),'classDisabledBajaSini');
			checkPorConfiguracion($arregloElemsDerechoPoliza,$checkDerechoPoliza.attr('id'),'classDisabledDerechoPoliza');
			verLineasNegocio('subramosAutosGrid','radioCondicionesCalcGrupo');		
		}
	        
	}
	
	
	/** 
	 * removerClaseError, remueve la clase indicada	 
	  @parametro claseActual, es la clase que contienen los elementos a manipular 
	  @parametro claseRemover, es la clase a remover	  
	  @eventos remueve la clase claseRemover de los elementos que contangan la clase claseActual
	 */
	function removerClaseError(claseActual,claseRemover){
		var elementos = jQuery(document).find("." + claseActual);
		
		for ( var i = 0; i < elementos.length; i++)
			jQuery("#" + elementos[i].attr('id') ).removeClass(claseRemover);
	}
	/**
	 * validarMonto, Funcion para validar el monto capturado
	  @eventos valida el valor del elemento para las reglas aplicadas(solo numeros y decimales), 
	  @eventos asigna el valor procesado al elemento que lo mando llamar
	 */
	function validarMonto(){
		var id = jQuery("#" + this.id);
		var valor = jQuery.trim(id.val());
		if(valor > 0)
			id.val(checkNDecimales(valor,30));
		else
			id.val('');
	}
	
	function imprimirReporteEstadoCuenta(){
		var location="/MidasWeb/compensacionesAdicionales/reportes/generarReporteEstadoCuenta.action";
		window.open(location,"generarReporteEstadoCuenta");
	}
	/**
	 * validarPorcentaje, Funcion para validar el porcentaje capturado
	  @eventos valida el valor del elemento para las reglas aplicadas(porcentaje),
	  @eventos asigna el valor procesado al elemento que lo mando llamar
	 */
	function imprimirReporteBancaSeguros(){
		var anio = jQuery("#anios2").val();
		var mes = jQuery("#meses2").val();
		var gerencia = jQuery("#gerencia").val();
		var location="/MidasWeb/compensacionesAdicionales/reportes/generarReporteBancaSeguros.action?anio="+anio+"&mes="+mes+"&gerencia="+gerencia;
		window.open(location,"generarReporteBancaSeguros");
	}
	
	function imprimirReportePagosSaldos(){
	var idCompensacion = jQuery("#folio").val();
	var cotizacion = jQuery().val("#cotización");
	var poliza = jQuery("#poliza").val;
	var agente = jQuery("#agente").val;
	var promotor = jQuery("#promotor").val;
	var proveedor = jQuery("#proveedor").val;
	var fechaInicial = jQuery("#fechaInicio").val;
	var fechaFinal = jQuery("#fechaFinal").val;
	var location="/MidasWeb/compensacionesAdicionales/reportes/generarReportePagosSaldos.action";
	window.open(location,"generarReportePagosSaldos");
	}
	
	function validarPorcentaje() {
		var id = jQuery("#" + this.id);
		var valor = jQuery.trim(id.val());
		if(valor[0] == "."){
			var numeroCompleto = checkNDecimales(valor,30);
			var indicePunto = valor.indexOf('.'); 
			var enteros = valor.substring(0,indicePunto);
			if(enteros.length == 0)
				numeroCompleto = "0" + numeroCompleto;			
			id.val(numeroCompleto);
		}else{
			if(valor <= 100 && valor >= 0)
				id.val(checkNDecimales(valor,30));
			else
				id.val('');
		}
	}
	
	/** Plantilla de comentarios
	 * Que es lo que hace la funcion	 
	  @parametro parametro(s) necesario(s) 
	  @regreso resultado a regresar	  
	  @eventos descripcion breve de lo que hace/afecta la funcion
	 */
	function ponerDecimales(id,limite){
		if(id != undefined){
			var valor = jQuery("#" + id).val();
			var indicePunto = valor.indexOf('.');
			if(indicePunto == -1){
				nResultado = parseFloat(valor);
				try{
					var ramo = jQuery('#varRamo').val();
					if(!isNaN(nResultado) && ramo === 'D'){
						nResultado = nResultado.toFixed(limite);
						jQuery("#" + id).val(nResultado);
					}
				}catch(e){
					
				}
			}else{
				var decimales = valor.substring(indicePunto, valor.length);
				switch(decimales.length){
				case 1:
					decimales += "000";
					break;
				case 2:
					decimales += "00";
					break;
				case 3:
					decimales += "0";
					break;
				}
				var enterosPunto = valor.substring(0,indicePunto);
				jQuery("#" + id).val(enterosPunto + decimales);
			}			
		}
	}
	
	/**
	 * checkNDecimales, Funcion que valida cuantos decimales debe de contener la cantidad a evaluar
	  @parametro valor,variable con el texto a procesar  
	  @parametro limite,variable con el numero de decimales a limitar
	  @regreso nResultado, numero con los decimales segun el limite
	 */
	function checkNDecimales(valor,limite){
		var indicePunto = valor.indexOf('.');
		var nOriginal = 0.0;
		var nResultado = valor;
		if(indicePunto >= 0){
			var decimales = valor.substring(indicePunto, valor.length);
			if(decimales.length > limite){
				nOriginal = parseFloat(valor);
				nResultado = nOriginal.toFixed(limite);
			}
		}		
		return nResultado;
	}
	
	/**
	 * checkPorcen2Elementos, Funcion para Sumar/Restar porcentajes entre dos elementos(de tipo texto)
	  @parametro a,elemento pivote para sumar/restar de 100
	  @parametro b,elemento secundario afectado directamente del resultado anterior
	  @eventos obtiene el valor de a para restar: 100 - a y el resultado asignarlo en b
	  @eventos el valor asignado en b esta limitado a 3 decimales
	 */
	function checkPorcen2Elementos(a,b){
		elementoA = jQuery("#" + a);
		elementoB = jQuery("#" + b);
		var valorA = elementoA.val();
		var valorB = elementoB.val();

		var resultB = 100 - valorA;
		if(isNaN(resultB) || resultB < 0)
			elementoB.val('');
		else{
		//	var numeroB = resultB.toFixed();
			elementoB.val(resultB);
		}
	}
	/**
	 * limpiaElemento, Funcion que limpia el valor del elemento que se le envia de parametro
	  @params elemento, objeto el cual se va a manipular
	  @eventos limpia el valor del elemento que se le envia de parametro, dependiendo del 'type'
	 */
	function limpiaElemento(elemento){
		if(elemento != undefined){
			switch(elemento.attr('type')){
				case 'select-one':
				case 'select-multiple':
					elemento.val(-1);
					break;
				case 'text':
						elemento.val('');
					break;
				case 'checkbox':
					elemento.attr( "checked", false );
					break;
			}			
		}
		elemento.removeClass('error-validate');
	}
	/**
	 * checkPorConfiguracion, Funcion para Activar/Desactivar los elementos en base a los parametros que se le inyectan
	  @eventos activa/desactiva todos los elementos en base a una clase especifica, un checkbox especifico y un arreglo de objetos
	  @parametro elementos, arreglo de objetos que contiene los elementos a limpiar
	  @parametro id, id del checkbox de la configuracion a manipular
	  @parametro clase, clase que contienen los elementos a activar/desactivar
	 */
	function checkPorConfiguracion(elementos,id,clase){
		var desactivar = jQuery('#'+id).attr( "checked" );
		desActivarPorClase(desactivar,clase);
		if(!desactivar){
			for(var i = 0; i < elementos.length; i++){
				limpiaElemento(elementos[i]);					
			}
		}	
		return desactivar;
	}
	/**
	 * desActivarPorClase, Funcion para Activar/Desactivar cualquier elemento
	  @parametro desactivar, bandera que indica activar/desactivar
	  @parametro clase, nombre de la clase que tienen los elementos a activar/desactivar
	  @eventos activa/desactiva todos los elementos en base a una clase especifica asignada los elementos
	  @eventos mediante una bandera
	 */ 
	function desActivarPorClase(desactivar, clase){
		var elementosClase = jQuery(document).find("." + clase);
		var configCA = CompensacionAdicional.config();
		jQuery.each(elementosClase, function(ind, val){			
			if(jQuery(this).attr('atributo') == "agre"){
				if(desactivar)
					jQuery(this).show();
				else
					jQuery(this).hide();					
			}			
			jQuery(this).attr("disabled", !desactivar);
		});
	}

	/** 
	 * checkCamposGenerales, Funcion que valida los campos Generales
	  @eventos valida todos los campos necesarios para la parte de configuracion General
	 */
	function checkCamposGenerales(){
		return false;
	}
	
	/** 
	 * porcentajeOmonto, Funcion que valida que solo se guarde un campo o el otro(monto o porcentaje)
	  @parametro a, parametro principal
	  @parametro b, elemento afecatdo dependiendo de a
	  @eventos si el elemento a sufre cambio el elemento b no debe de conetener nigun valor
	 */
	function porcentajeOmonto(a,b){
		elementoB = jQuery("#" + b);
		if(elementoB.length>0 && elementoB.val().length > 0){
			elementoB.val('');
		}
	}

	/** 
	 * validaPorcentajeGuardar, Funcion para validar elemento para el procentaje
	  @parametro elemento a validar
	  @parametro validaOtro en caso de ser necesario validar otro elemento
	  @regreso un arreglo que contiene el nombre del elemento y un bool para indicar si es valido
	  @eventos inicialmente se limpia la clase 'error-validate',en caso de que la tenga
	  @eventos luego se valida si el texto del elemento es un numero/porcentaje valido
	  @eventos en caso de que el parametro validaOtro sea diferente de null
	  @eventos se valida tambien, segun reglas especificas
	  @eventos si el texto NO es valido se agrega la clase 'error-validate' al elemento
	  @eventos agregamos el nombre del elemento(id) a un arreglo y un bool para indicar si es valido o no
	 */
	function validaPorcentajeGuardar(elemento,validaOtro){
		elemento.removeClass('error-validate');
		var arrayResult = new Array();
		var valorElemento = elemento.val();
		var esValido = false;
		if(valorElemento.length > 0){
			if(isNaN(valorElemento))
				esValido = false;
			else
				if(valorElemento >= 0 && valorElemento <= 100)
					esValido = true;
				else
					esValido = false;
		}else
			if(validaOtro != null)
				if(validaOtro.val().length > 0)
					esValido = true;
				else
					esValido = false;
		
		if(!(esValido))
			jQuery(elemento).addClass('error-validate');

		arrayResult.push(elemento.attr('id'),esValido);
		return arrayResult;
	}
	/** 
	 * validaMontoGuardar, Funcion para validar elemento para el monto
	  @parametro elemento a validar
	  @parametro validaOtro en caso de ser necesario validar otro elemento
	  @regreso un arreglo que contiene el nombre del elemento y un bool para indicar si es valido
	  @eventos inicialmente se limpia la clase 'error-validate',en caso de que la tenga
	  @eventos luego se valida si el texto del elemento es un numero/monto valido
	  @eventos en caso de que el parametro validaOtro sea diferente de null
	  @eventos se valida tambien, segun reglas especificas
	  @eventos si el texto NO es valido se agrega la clase 'error-validate' al elemento
	  @eventos agregamos el nombre del elemento(id) a un arreglo y un bool para indicar si es valido o no
	 */
	function validaMontoGuardar(elemento,validaOtro){
		elemento.removeClass('error-validate');
		var arrayResult = new Array();
		var valorElemento = elemento.val();
		var esValido = false;
		if(valorElemento.length > 0){
			if(isNaN(valorElemento))
				esValido = false;
			else
				if(valorElemento > 0)
					esValido = true;
				else
					esValido = false;
			}
		else
			if(validaOtro != null)
				if(validaOtro.val().length > 0)
					esValido = true;
				else
					esValido = false;
		
		if(!(esValido))
			elemento.addClass('error-validate');
		arrayResult.push(elemento.attr('id'),esValido);
		return arrayResult;
	}
	
	/** 
	 *validaComboSelectGuardar, Funcion para validar elemento para un comboselect en especifico	 
	  @parametro elemento a validar 
	  @regreso un arreglo que contiene el nombre del elemento y un bool para indicar si es valido  
	  @eventos inicialmente se limpia la clase 'error-validate',en caso de que la tenga
	  @eventos luego se valida si la seleccion elemento es una opcion valida
	  @eventos si NO es valida se agrega la clase 'error-validate' al elemento
	  @eventos agregamos el nombre del elemento(id) a un arreglo y un bool para indicar si es valido   
	 */
	function validaComboSelectGuardar(elemento){
		elemento.removeClass('error-validate');
		var valor = elemento.val();
		var arrayResult = new Array();
		var esValido = false;
		if(valor == -1)
			esValido = false;
		else
			esValido = true;
		if(!esValido)
			elemento.addClass('error-validate');
		arrayResult.push(elemento.attr('id'),esValido);
		return arrayResult;
	}
	/** 
	 * validaCheckboxRadiobuttonGuardar, Funcion para validar elementos Checkbox/Radiobutton	 
	  @parametro clase para identificar los elementos a validar 
	  @parametro elemento(tabla,div,etcetera) que contiene los elementos a validar
	  @regreso un arreglo que contiene el nombre de los elementos y un bool para indicar si uno es valido  
	  @eventos inicialmente se limpia la clase 'error-validate',en caso de que la tenga
	  @eventos luego se valida si cualquiera de los elementos es valido
	  @eventos si NINGUNO es valido se agrega la clase 'error-validate' al elemento
	  @eventos agregamos el nombre de los elementos a un arreglo y un bool para indicar si es valido
	 */
	function validaCheckboxRadiobuttonGuardar(clase,contenedor){
		contenedor.removeClass('error-validate');
		var arrayResult = new Array();
		var esValido = new Array();
		
		var radios = jQuery(document).find( "." + clase );
		
		jQuery.each(radios, function(indice, valor){
			if(valor.checked)
				esValido.push(true);
			else 
				esValido.push(false);
		});
		var bandera = false;
		for ( var i = 0; i < esValido.length; i++)
			if(esValido[i]){
				bandera = true;
				break;
			}
		if(!bandera)
			contenedor.addClass('error-validate');
//		arrayResult.push(radios,esValido);
		arrayResult.push(radios,bandera);
		return arrayResult;
	}
	function checkProveedor(){
		if(jQuery("#textNomProvContra").val() != '')
			return true;
		alert('Revisar al Proveedor.');
		return false;
	}
	function checkTipoContrato(){
		var tipoContratoContra = validaCheckboxRadiobuttonGuardar('radioTipoContratoGrupoContra',jQuery("#tableContenedorTipoContratoContra"));
		
		var resultTipoContrato = tipoContratoContra[1];
		if(!resultTipoContrato){
			alert('revisar el Tipo de Contrato');
		}
		return resultTipoContrato;
	}
	
	function checkCamposPorPrima(configCA){
	var contraprestacionEs = configCA.isContraprestacion;
	var chechPrimaActul = null;
	var ramo = configCA.ramo;
	if(contraprestacionEs){
		chechPrimaActul = $checkPrimaContra;
	}else{
		chechPrimaActul = $checkPrimaGral;
	}
	
	if(chechPrimaActul.attr( "checked" )){
		var primaOk = false;
		var matrizPrima = new Array();
		
		if(contraprestacionEs){
			var porcePrimaContra = validaPorcentajeGuardar($textPorcePrimaContra,$textMontoPrimaContra);
			var baseCalContra = validaComboSelectGuardar($selectBaseCalContra);
			var monedaSelContra = validaComboSelectGuardar($selectMonedContra);
			var tipoCondicionesCalculoContra, tipoProvisiona;
			
			if(ramo === 'A'){
				tipoCondicionesCalculoContra = validaCheckboxRadiobuttonGuardar('radioCondicionesCalcGrupoContra', $tableContenedorCondicionesPrimaContra);
			}else if(ramo === 'D'){
				var valueTP = jQuery('input[name="compensacionesDTO.tipoProvisionca.id"]:checked').val();
				tipoProvisiona = Number(valueTP)>0 && !isNaN(valueTP); 
			}else if(ramo === 'V' && configCA.isComisionOriginal){
				porcePrimaContra[1] = true;
			}
						
			matrizPrima.push(porcePrimaContra[1]);
			matrizPrima.push(baseCalContra[1]);
			matrizPrima.push(monedaSelContra[1]);
			
			if(ramo === 'A'){
				matrizPrima.push(tipoCondicionesCalculoContra[1]);
			}else if(ramo === 'D'){
				matrizPrima.push(tipoProvisiona);
			}
			
		}else{
			var porcePrimaGral = validaPorcentajeGuardar($textPorcePrimaGral,$textMontoPrimaGral);
			var porcePrimaAgenGral = validaPorcentajeGuardar($textPorcePrimaAgenGral,null);
			var porcePrimaPromoGral = validaPorcentajeGuardar($textPorcePrimaPromoGral,null);
			var baseCalGral = validaComboSelectGuardar($selectBaseCalGral);
			var tipoCondicionesCalculo, tipoProvisiona;
			if(ramo === 'A'){
				tipoCondicionesCalculo = validaCheckboxRadiobuttonGuardar('radioCondicionesCalcGrupo', $tableContenedorCondicionesPrimaGral);
			}else if(ramo === 'D'){
				var valueTP = jQuery('input[name="compensacionesDTO.tipoProvisionca.id"]:checked').val();
				tipoProvisiona = Number(valueTP)>0 && !isNaN(valueTP); 
			}else if(ramo === 'V' && configCA.isComisionOriginal){
				porcePrimaGral[1] = true;
			}
			
			matrizPrima.push(porcePrimaGral[1]);
			matrizPrima.push(porcePrimaAgenGral[1]);
			matrizPrima.push(porcePrimaPromoGral[1]);
			matrizPrima.push(baseCalGral[1]);
			if(ramo === 'A'){
				matrizPrima.push(tipoCondicionesCalculo[1]);
			}else if(ramo === 'D'){
				matrizPrima.push(tipoProvisiona);
			}
		}
		
		var banderaAux = true;
		jQuery.each( matrizPrima , function( indice , valor ){
			bandera = matrizPrima[indice];
			if(bandera)
				primaOk = true;
			else{
				banderaAux = false;
				}
			});
		primaOk = banderaAux;
		
		/// Validar lineas de negocio, obtener lineas de negocio, cargar lineas de negocio
		var auxLineasN =  obtenerLineasNegocio(contraprestacionEs);				
		if(contraprestacionEs){
			sLineasNegocioContra = auxLineasN; 
			if(!banderaLineasNegocioContra){
			primaOk = banderaLineasNegocioContra;
			alert('Por lo menos una linea de negocio debe de tener % de comision fija.');
			}
		}else{
			sLineasNegocio = auxLineasN;
			if(!banderaLineasNegocio){
			primaOk = banderaLineasNegocio;
			alert('Por lo menos una linea de negocio debe de tener % de comision fija');
			}
		}
		
		//				validarMatriz(matrizPrima);
		if(!primaOk ){
			console.info('Por Prima: FAIL');
			alert('revisar campos POR PRIMA');
		}
		return primaOk;
		}
	console.info('Por Prima: OK');
	return true;
	}	
	
	/**
	 * Valida que la compensacion 
	 * halla sido configurada
	 * SOLO PARA AUTOS
	 * @returns {Boolean}
	 */
	function emptyCompensacion(){
		var validate = jQuery('#checkPrimaGral').is(":checked") 
					|| jQuery('#checkBajaSiniestralGral').is(":checked") 
					|| jQuery('#checkDerechoPoliza').is(":checked");
		
		if(!validate){
			alert('No se ha configurado compensacion por favor revise');
		}
		return validate;
	}
	
	/**
	 * Valida que la contraprestacion 
	 * halla sido configurada
	 * SOLO PARA AUTOS
	 * @returns {Boolean}
	 */
	function emptyContraprestacion(){
		
		var validate = jQuery('#checkPrimaContra').is(":checked") 
					|| jQuery('#checkBajaSiniestralContra').is(":checked") 
					|| jQuery('#checkUtilidadContra').is(":checked")
					|| jQuery('#checkCumplimientoMeta').is(":checked");
		if(!validate){
			alert('No se ha configurado compensacion por favor revise');
		}
		return validate;
	}
	
	/** 
	 * llevaLineasNegocio, Funcion que valida, si lleva o no las lineas de negocio
	  @regreso llevaLineas,booleano que contiene el resultado si/no
	  @eventos se evaluan los elementos(raddio button) que contengan una clase especifica
	  @eventos y en base al valor actula seleccionado se evalua para determinar
	  @eventos si lleva o no lineas de negocio
	 */
	function llevaLineasNegocio(esContra){
		var llevaLineas = false;
		var claseLineaNegocio = esContra ? '.radioCondicionesCalcGrupoContra' : '.radioCondicionesCalcGrupo';
		var radiosCondiciones = jQuery(document).find(claseLineaNegocio);
		jQuery.each(radiosCondiciones, function(ind, val){
			var valorActual = jQuery("#"+this.id).val();
			/// 3 = Con Ajuste y Comision Pactada, 4 = Sin Ajuste y Comision Pactada,
			/// con cualquier valor(3 o 4); si lleva Lineas de Negocio
			if(valorActual == 3 || valorActual == 4)
				if(jQuery("#" + this.id).attr( "checked" ))
					llevaLineas = true;
		});		
		return llevaLineas;
	}
	function validarLineasNegocio(clase){
		var resultado = false;
		var lineasNegocio = jQuery(document).find(clase);
		jQuery.each(lineasNegocio, function(indice, valor){
			var valorActual = jQuery("#"+this.id).val();
			if(valorActual.length > 0)
				resultado = true;
		});		
		return resultado;
	}
	
	/** 
	 * obtenerLineasNegocio, Funcion que obtiene las lineas de negocio, si es que aplica y las regresa en una variable String
	  @regreso lineasString, variable String con las lineas de negocio 
	  @eventos Principalmente se valida si lleva Lineas de Negocio y si son validas, posteriormente
	  @eventos se genera una variable String para agregar las lineas de negocio 
	 */
	function obtenerLineasNegocio(esContra){
		var lineasString = null;
		var claseLineaNegocio = esContra ? '.lineaNegocioContra' : '.lineaNegocio';
		if(llevaLineasNegocio(esContra)){
			if(validarLineasNegocio(claseLineaNegocio)){
				lineasString = "";
				var lineasNegocio = jQuery(document).find(claseLineaNegocio);
				jQuery.each(lineasNegocio, function(indice, valor){
					var elementoActual = jQuery('#' + this.id);
//					if(elementoActual.val() > 0)
					var desripcionHidden = jQuery('#' + this.id +"_");
					lineasString += '$id'+ this.id + 'valor' + elementoActual.val() + 'descripcion' + desripcionHidden.val();
				});
				if(esContra)
					banderaLineasNegocioContra = true;
				else
					banderaLineasNegocio = true;
			}else{
				if(esContra)
					banderaLineasNegocioContra = false;
				else
					banderaLineasNegocio = false;
			}
		}		
		return lineasString;
	}
	
	/** 
	 * validarMatriz, Funcion que valida la matriz recibida	 
	  @parametro matriz, arreglo de objetos con los elementos a validar
	  @eventos valida todos los elementos de la matriz
	 */
	function validarMatriz(matriz){
		jQuery.each( matriz , function( indice , valor ){
				alert(matriz[indice]);
		});
	}
	function checkCamposBajaSini(contraprestacionEs, divId){
		return checkCamposBajaSini(contraprestacionEs, divId, true);
	}
	function checkCamposBajaSini(contraprestacionEs, divId, validarCompos){
		var checkBajaSiniActual = null;
		if(contraprestacionEs)
			checkBajaSiniActual = $checkBajaSiniestralContra;
		else
			checkBajaSiniActual = $checkBajaSiniestralGral;
		
		if(checkBajaSiniActual.attr( "checked" )){
			
			var bajaSiniOk = false;
			var matrizBajaSini = new Array();
			
			if(validarCompos){
				if(contraprestacionEs){
					var porceBajaSiniContra = validaPorcentajeGuardar($textPorceBajaSiniestralContra,null);
					var baseCalGralContra = validaComboSelectGuardar($selectBaseCalcBajaSiniContra);
					var monedaGralContra = validaComboSelectGuardar($selectMonedaBajaSiniContra);
										
					matrizBajaSini.push(porceBajaSiniContra[1]);
					matrizBajaSini.push(baseCalGralContra[1]);
					matrizBajaSini.push(monedaGralContra[1]);
				}else{
					var porceBajaSiniAgenGral = validaPorcentajeGuardar($textPorceBajaSiniAgenGral,null);
					var porceBajaSiniPromoGral = validaPorcentajeGuardar($textPorceBajaSiniPromoGral,null);
					var baseCalGral = validaComboSelectGuardar($selectBaseCalcBajaSiniGral);
										
					matrizBajaSini.push(porceBajaSiniAgenGral[1]);
					matrizBajaSini.push(porceBajaSiniPromoGral[1]);
					matrizBajaSini.push(baseCalGral[1]);
				}
				
				var banderaAux = true;
				jQuery.each( matrizBajaSini , function( indice , valor ){
					
					bandera = matrizBajaSini[indice];
					if(bandera)
						bajaSiniOk = true;
					else{
						banderaAux = false;
					}
				});
				
				bajaSiniOk = banderaAux;
				console.info('Sinisestralidad');
				console.info(bajaSiniOk);
			}
			
			if(contraprestacionEs){
				sRangosBSContra = validarRangosTodos(divId); 
			}else{
				sRangosBS = validarRangosTodos(divId); 
			}
			
			if(!bajaSiniOk && validarCompos){
				alert('Revisar campos POR BAJA SINIESTRALIDAD');
			}else{
				bajaSiniOk = true;
			}
			if(jQuery('#formaPagoBanca').val()=='' || jQuery('#formaPagoBanca').val()==null){
				var sRangosBSAux = null; 
				if(contraprestacionEs){
					sRangosBSAux = sRangosBSContra; 
					if(sRangosBSAux == null){
						alert('Revisar los Rangos de Sinistralidad');
						bajaSiniOk = false;
					}else if(validarPorcentajeRangos('rangosSiniestralidadGridContra')){
						var divParent = document.getElementById('rangosSiniestralidadGridContra');
						var divObjbox = divParent.getElementsByClassName("objbox");
						var tableObjs = divObjbox[0].getElementsByClassName("obj");
						var tableObj = tableObjs[0];					
						total = tableObj.rows.length;						 
						if(total == 1){
							alert('Debe de haber por lo menos 1 Nivel para Rangos de Sinistralidad');
							bajaSiniOk = false;
						}
					}else{
						alert('Los Porcentajes para la compensacion deben ser Descendentes');
						bajaSiniOk = false;
					}
				}else{
					sRangosBSAux = sRangosBS; 
					if(sRangosBSAux == null){
						alert('Revisar los Rangos de Sinistralidad');
						bajaSiniOk = false;
					}else if(validarPorcentajeRangos('rangosSiniestralidadGrid')){
							var divParent = document.getElementById('rangosSiniestralidadGrid');
							var divObjbox = divParent.getElementsByClassName("objbox");
							var tableObjs = divObjbox[0].getElementsByClassName("obj");
							var tableObj = tableObjs[0];
						
							total = tableObj.rows.length;						 
							if(total == 1){
								alert('Debe de haber por lo menos 1 Nivel para Rangos de Sinistralidad');
								bajaSiniOk = false;
							}
					}else{
						alert('Los Porcentajes para la compensacion deben ser Descendentes');
						bajaSiniOk = false;
					}
				}
			}
			if(validarCompos === false){
				if(bajaSiniOk){
					alert('Rangos de Sinistralidad Correctos');
				}
			}
			return bajaSiniOk;
		}
		return true;
	}
	
	/** 
	 * checkCamposDerechoPoliza, Funcion que valida los campos Por Derecho de Poliza
	  @eventos valida todos los campos necesarios para la parte de configuracion por Derecho de Poliza
	 */
	function checkCamposDerechoPoliza(){
		if($checkDerechoPoliza.attr( "checked" )){
			var derechoPolizaOk = false;
			var matrizderechoPoliza = new Array();

			var porceDerechoPolizaGral = validaPorcentajeGuardar($textPorcePoliza,$textMontoPoliza);
			var montoDerechoPolizaGral = validaMontoGuardar($textMontoPoliza,$textPorcePoliza);
			var porceDerechoPolizaAgenGral = validaPorcentajeGuardar($textPorceAgePoliza,null);
			var porceDerechoPolizaPromoGral = validaPorcentajeGuardar($textPorcePromoPoliza,null);
			var tipoCondicionesCalculo = validaCheckboxRadiobuttonGuardar('checkEmisionGrupo', $tableEmisionPoliza);
			
			matrizderechoPoliza.push(porceDerechoPolizaGral[1]);
			matrizderechoPoliza.push(montoDerechoPolizaGral[1]);
			matrizderechoPoliza.push(porceDerechoPolizaAgenGral[1]);
			matrizderechoPoliza.push(porceDerechoPolizaPromoGral[1]);
			matrizderechoPoliza.push(tipoCondicionesCalculo[1]);
			
			var banderaAux = true;
			jQuery.each( matrizderechoPoliza , function( indice , valor ){
				bandera = matrizderechoPoliza[indice];
				if(bandera)
					derechoPolizaOk = true;
				else{
					banderaAux = false;
				}
			});
			derechoPolizaOk = banderaAux;
			if(!derechoPolizaOk)
				alert('Revisar campos POR DERECHO DE POLIZA');
			return derechoPolizaOk;
		}
		return true;
	}
	
	/** 
	 *verLineasNegocio, Funcion para mostrar/ocultar las lineas de negocio
	  @eventos En base a una clase especifica, se buscan todos los elementos que la contengan(raddio button)
	  @eventos por cada elemento se le asigna esta funcion en el evento 'click' para validar 
	  @eventos si se muestran/ocultan las lineas de negocio, dependiendo del valor especifico de cada elemento
	 */
	function verLineasNegocio(grid,clase){
		var banderaTresOcuatro = false;
		var radiosCondiciones = jQuery(document).find("." + clase);	
		jQuery.each(radiosCondiciones, function(ind, val){
			var valorActual = jQuery("#"+this.id).val(); 
			/// 3 = Con Ajuste y Comision Pactada, 4 = Sin Ajuste y Comision Pactada
			if(valorActual == 3 || valorActual == 4){
				if(jQuery("#"+this.id).attr( "checked" )){
					banderaTresOcuatro = true;
					jQuery("#" + grid).show();
				}
				else
					if(!banderaTresOcuatro){
						sLineasNegocio = null;
						jQuery("#" + grid).hide();
					}
			}
		});
	}	
	
	/** 
	 * validarRangosTodos, Funcion que valida los Rangos para Baja Siniestralidad
	  @parametro parametro(s) necesario(s) 
	  @regreso resultado a regresar	  
	  @eventos descripcion breve de lo que hace/afecta la funcion
	 */
	function validarRangosTodos(divId){
		var arrayNombre = jQuery('#'+divId).find(".nombreNivel");
		var arrayInicial = jQuery('#'+divId).find(".inicialNivel");
		var arrayFinal = jQuery('#'+divId).find(".finalNivel");
		var arrayCompensacion = jQuery('#'+divId).find(".compensacionNivel");
		
		var validarRangos = new Array();
		var validarSiguientes = new Array();
		var rangosString = "";
		var validaCampo = new Array();
		jQuery.each(arrayNombre, function(indice, valor){
			validarRangos.push(Number(arrayInicial[indice].value) < Number(arrayFinal[indice].value));
			var renglones = arrayNombre.length;
			if(renglones > 1 && indice < renglones - 1)
				validarSiguientes.push(Number(arrayFinal[indice].value) < Number(arrayInicial[indice + 1].value));
			validaCampo.push(!isNaN(arrayNombre[indice].value));
			validaCampo.push(!isNaN(arrayInicial[indice].value));
			validaCampo.push(!isNaN(arrayFinal[indice].value));
			validaCampo.push(!isNaN(arrayCompensacion[indice].value));
			rangosString += '$nombreR' + arrayNombre[indice].value;
			rangosString += 'inicialR' + arrayInicial[indice].value; 
			rangosString += 'finalR' + arrayFinal[indice].value;
			rangosString += 'compensacionR' + arrayCompensacion[indice].value;

		});
		
		var vRangos = true;
		for ( i = 0; i < validarRangos.length; i++)
			if(!(validarRangos[i])){
				vRangos = validarRangos[i];
				break;
			}		
		
		var vRangosSig = true;
		for(var i = 0;i < validarSiguientes.length;i++)
			if(!(validarSiguientes[i])){
				vRangosSig = false;
				break;
			}		
		
		var validaCampos = true;
		for(var i = 0; i< validaCampo.length;i++)
			if(!(validaCampo[i])){
				validaCampos = false;
				break;
			}
		
		var resultFinal = false;
		if(vRangos && vRangosSig && validaCampos)
			resultFinal = true;
				
		if(resultFinal){}
		else
			rangosString = null;
		return rangosString;
	}
	function validarPorcentajeRangos(idGrid){
		var validado = true;
		var arrayValidados = new Array();
		var arrayNiveles = jQuery('#'+idGrid).find(".compensacionNivel");
		jQuery.each(arrayNiveles, function(indice, valor){
			if(arrayNiveles[indice + 1] != undefined)
				arrayValidados.push(Number(arrayNiveles[indice].value) > Number(arrayNiveles[indice + 1].value) ? true:false);
		});
		for ( var index = 0; index < arrayValidados.length; index++)
			if(!arrayValidados[index]){
				validado = false;
				break;
			}
		
		return validado;
	}
	/** 
	 * eliminarRango, Funcion que elimina un rango mas a la tabla de Rangos por Baja Siniestralidad
	  @eventos elimina un renglon a la tabla a la tabla de Rangos por Baja Siniestralidad para posteriormente guardarlo
	 */
	function eliminarRango(indiceRango,idGrid){
		if($checkBajaSiniestralGral.attr( "checked" ) || $checkBajaSiniestralContra.attr( "checked" )){
			var indiceCelda = 4;
			var divParent = document.getElementById(idGrid);
			var divObjbox = divParent.getElementsByClassName("objbox");
			var tableObjs = divObjbox[0].getElementsByClassName("obj");
			var tableObj = tableObjs[0];
			
	        var rowCount = tableObj.rows.length;
	        for(var i = 0; i < rowCount; i++){
	        	var row = tableObj.rows[i];
	        	if(row != undefined){
	        	var idDelete = row.cells[indiceCelda].childNodes[0];	        	
	        	if(idDelete != undefined)
	        		if(indiceRango == idDelete.id)
	        			tableObj.deleteRow(i);
	        	}
	        }
        }
		 actualizarRangos(idGrid);
	}
	function actualizarRangos(idGrid){
		var indexCell = 0;
		var divParent1 = document.getElementById(idGrid);
		var divObjbox1 = divParent1.getElementsByClassName("objbox");
		var tableObjs1 = divObjbox1[0].getElementsByClassName("obj");
		var tableObj1 = tableObjs1[0];
		
		var total = tableObj1.rows.length;		
        for(var i = 0; i < total; i++){
        	var ren = tableObj1.rows[i];
        	if(ren != undefined){
        	var actual = ren.cells[indexCell].childNodes[0];
        	if(actual != undefined){        		
        		if(actual.className == 'wwgrp'){
        			var wwgrpDiv = actual.getElementsByClassName('wwgrp');      			
        		}else
        			actual.value = i ;
        		}
        	}
        }	        
	}
	
	/** 
	 * agregarRango, Funcion que agrega un rango mas a la tabla de Rangos por Baja Siniestralidad
	  @eventos agrega un renglon a la tabla a la tabla de Rangos por Baja Siniestralidad para posteriormente guardarlo
	 */
	function agregarRango(idGrid){
	    var divParent = document.getElementById(idGrid);
	    var divObjbox = divParent.getElementsByClassName("objbox");
	    var tableObjs = divObjbox[0].getElementsByClassName("obj");
	    var tableObj = tableObjs[0];
        var rowCount = tableObj.rows.length;
        var row = tableObj.insertRow(rowCount);
                
        var cell = 0;
        var cellNombre = row.insertCell(cell);
        var elementNombre = document.createElement("input");
        elementNombre.id = "nombre_" + row.rowIndex;
        elementNombre.type = "text";
        elementNombre.className = "cajaTextoM2 w100 nombreNivel ";
        elementNombre.disabled = "disabled";
        elementNombre.value = row.rowIndex;
        cellNombre.appendChild(elementNombre);
        
        var cellInicial = row.insertCell(++cell);
        var elementInicial = document.createElement("input");
        elementInicial.id = "inicial_" + row.rowIndex;
        elementInicial.type = "text";
        elementInicial.className ="cajaTextoM2 w100 inicialNivel classDisabledBajaSini";
        elementInicial.setAttribute("onkeyup","CompensacionUtils.parametros.validarPorcentaje(this.id)");
        cellInicial.appendChild(elementInicial);
        
        var cellFinal = row.insertCell(++cell);
        var elementFinal = document.createElement("input");
        elementFinal.id = "final_" + row.rowIndex;
        elementFinal.type = "text";
        elementFinal.className ="cajaTextoM2 w100 finalNivel classDisabledBajaSini";
        elementFinal.setAttribute("onkeyup","CompensacionUtils.parametros.validarPorcentaje(this.id)");
        cellFinal.appendChild(elementFinal);
        
        var cellCompensacion = row.insertCell(++cell);
        var elementCompensacion = document.createElement("input");
        elementCompensacion.id = "compensacion_" + row.rowIndex;
        elementCompensacion.type = "text";
        elementCompensacion.className ="cajaTextoM2 w100 compensacionNivel classDisabledBajaSini";
        elementCompensacion.setAttribute("onkeyup","CompensacionUtils.parametros.validarPorcentaje(this.id)");
        cellCompensacion.appendChild(elementCompensacion);
        
        var cellEliminar = row.insertCell(++cell);
        var elementEliminar = document.createElement("A");
        var img = document.createElement('img');
        img.src = '../img/icons/ico_eliminar.gif';
        elementEliminar.setAttribute("id",row.rowIndex);
        elementEliminar.className ="classDisabledBajaSini";
        elementEliminar.setAttribute("href","javascript:void(0)");
        elementEliminar.setAttribute("onclick","eliminarRango(this.id,'" + idGrid + "')");
        elementEliminar.appendChild(img);
        cellEliminar.appendChild(elementEliminar);
	}

	/** 
	 * cancelarContraprestacion, Funcion para cancelar la configuracion de contraprestaciones
	  @eventos Cancelar la configuracion, si se confirma se cierra la pagina,caso contrario se mantiene la pagina actual
	 */
	function cancelarContraprestacion(){
		var result = confirm("Al abandonar esta sección se perderán los datos que no hayan sido guardados. ¿Desea continuar?");
		if (result == true) {
			window.close();
			window.opener.wCloseCompensacion();
		}
	}
	

	

	/** 
	 * cancelar, Funcion para cancelar la configuracion
	  @eventos Cancelar la configuracion, si se confirma se cierra la pagina,caso contrario se mantiene la pagina actual
	 */
	function cancelar(){
		var isNewRecord = jQuery('#varCompensacionNueva').val() === '1' ? true : false;
		var result = true;
		
		if(isNewRecord){
			result = confirm("Al abandonar esta sección se perderán los datos que no hayan sido guardados. ¿Desea continuar?");
		}
		
		if (result == true) {
			
			var $invocador = jQuery('#varInvocacionConfigurador');
			
			if($invocador.length > 0 && $invocador.val() === 'listadoConfiguraciones'){
				var url = '/MidasWeb/compensacionesAdicionales/compensacionesInicio.action';
				jQuery('#contenido').children().detach();
				sendRequestJQ(null, url, 'contenido',   null);
			}else{
			window.close();
			window.opener.wCloseCompensacion();
		}
	}
	}

/** 
 * desactiva elementos indicados	 
  @parametro bloquear
  @parametro clase
  @eventos desactiva los elementos encontrados por medio de una clase indicada
 */
	function bloquearConfiguracion(bloquear,clase){
		
		if(bloquear == 1){
			
			var elemensBloqu = jQuery(document).find("." + clase).attr("disabled", true);
			
			jQuery.each(elemensBloqu, function(ind, val){
				var elementoActual = jQuery("#"+this.id);
				switch(elementoActual.attr('type')){
					case 'select-one':
					case 'select-multiple':
					case 'text':
					case 'checkbox':
					case 'radio':
						elementoActual.attr("disabled", true);
						break;
					case 'anchor':				
						elementoActual.hide();					
					break;
				}
			});
		}
	}


function buscarCompensaciones(){
	var paramsForm = encodeForm(jQuery(document.filtroCompensacionForm));

	jQuery("#compensacionGrid").empty(); 
	var url = "";
	url = "/MidasWeb/compensacionesAdicionales/listaCompensaciones.action?" + paramsForm;
	
	compensacionGrid = new dhtmlXGridObject('compensacionGrid');
  	  
	compensacionGrid.attachEvent("onXLS", function(grid){
		//blockPage();
	});
	compensacionGrid.attachEvent("onXLE", function(grid){
		//unblockPage();
	});
	
	compensacionGrid.load(url) ;
}

function listarCompensacionPaginado(page, nuevoFiltro, filtrar){
	var posPath = '&posActual='+page+'&funcionPaginar='+'listarCompensacionPaginado'+'&divGridPaginar='+'compensacionGrid';
	if(nuevoFiltro){
		if(filtrar === null || filtrar === undefined || filtrar === true){
			paginadoParamsNegPath = jQuery(document.filtroCompensacionForm).serialize() +  "&filtrar=true";
		}else{
			paginadoParamsNegPath = "filtrar=false";
		}
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	

	mostrarIndicadorCarga('indicador');	
	sendRequestJQTarifa(null, "/MidasWeb/compensacionesAdicionales/listaPaginadoCompensaciones.action" + "?" + paginadoParamsNegPath + posPath, 'compensacionPaginadoGrid', 'listarCompensaciones();');
	sizeGridCompensacion();
}

function sizeGridCompensacion(){
	jQuery(document).ready(function(){
		jQuery('#compensacionGrid').css('height','400px');
		jQuery('div[class=objbox]').css('height','348px');
	})
}


function listarCompensaciones(){
	
	document.getElementById("compensacionGrid").innerHTML = '';

	compensacionGrid = new dhtmlXGridObject("compensacionGrid");

	compensacionGrid.enableEditEvents(false, false);
	//
	mostrarIndicadorCarga('indicador');	
	compensacionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	var posPath = 'posActual=' + jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();

	compensacionGrid.load("/MidasWeb/compensacionesAdicionales/listaCompensaciones.action?" + paginadoParamsNegPath + "&"+ posPath);	
}

function limpiarFiltrosEntidadPersona(){
	
	jQuery('#idAgente').val('');
	jQuery('#nombreAgente').val('');
	jQuery('#nombrePromotor').val('');
	jQuery('#idPromotor').val('');
	jQuery('#idProveedor').val('');
	jQuery('#nombreProveedor').val('');
	jQuery('#idNegocio').val('');
	
	jQuery('#nombreNegocio').val('');
	jQuery('#filtroCompensacionIdCompensacion').val('');
	jQuery('#filtroCompensacionIdCotizacion').val('');
	jQuery('#numeroPoliza').val('');
	jQuery('#filtroCompensacionIdNegocio').val('');
	
	jQuery('#filtroCompensacionIdRamo').val('');
	jQuery('#filtroCompensacionForm_filtroCompensacion_idGerencia').val('');
	jQuery('#filtroCompensacionForm_filtroCompensacion_idEstatus').val('');

}

function buscarEntidadesPersona(){
	
	document.getElementById("compensacionGrid").innerHTML = '';

	compensacionGrid = new dhtmlXGridObject("compensacionGrid");

	compensacionGrid.enableEditEvents(false, false);
	
	mostrarIndicadorCarga('indicador');	
	compensacionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	var filtros = jQuery(document.filtroCompensacionForm).serialize();
	
	var posPath = 'posActual=' + jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	var url = "/MidasWeb/compensacionesAdicionales/listarEntidadesPersona.action?";
	
	var idGerencia = Number(jQuery('#filtroCompensacionForm_filtroCompensacion_idGerencia').val());
	if(idGerencia > 0){
		url = url+"filtroCompensacion.idGerencia=" + idGerencia;
	}	
	
	var idCompensacion = Number(jQuery('#filtroCompensacionIdCompensacion').val());	
	if(idCompensacion > 0){
		url = url+"&filtroCompensacion.idCompensacion=" + idCompensacion;
	}
	
	var idEstatus = Number(jQuery('#filtroCompensacionForm_filtroCompensacion_idEstatus').val());
	if(idEstatus > 0){
		url = url+"&filtroCompensacion.idEstatus=" + idEstatus;
	}
	
	var idPoliza = Number(jQuery('#numeroPoliza').val());
	if(idPoliza > 0){
		url = url+"&filtroCompensacion.idPoliza=" + idPoliza;
	}
	
	var idNegocio = Number(jQuery('#filtroCompensacionIdNegocio').val());
	if(idNegocio > 0){
		url = url+"&filtroCompensacion.idNegocio=" + idNegocio;
	}else{
		idNegocio = Number(jQuery('#idNegocio').val());
		if(idNegocio > 0){
			url = url+"&filtroCompensacion.idNegocio=" + idNegocio;
		}		
	}
	
	var idRamo = Number(jQuery('#filtroCompensacionIdRamo').val());
	if(idRamo > 0){
		url = url+"&filtroCompensacion.idRamo=" + idRamo;
	}
	
	var idCotizacion = Number(jQuery('#filtroCompensacionIdCotizacion').val());
	if(idCotizacion > 0){
		url = url+"&filtroCompensacion.idCotizacion=" + idCotizacion;
	}
	
	var idAgente = Number(jQuery('#idAgente').val());
	if(idAgente > 0){
		url = url+"&filtroCompensacion.idAgente=" + idAgente;
	}
	
	var idProveedor = Number(jQuery('#idProveedor').val());
	if(idProveedor > 0){
		url = url+"&filtroCompensacion.idProveedor=" + idProveedor;
	}

	var idPromotor = Number(jQuery('#idPromotor').val());
	if(idPromotor > 0){
		url = url+"&filtroCompensacion.idPromotor=" + idPromotor;
	}
	
	console.info(url);
	compensacionGrid.load(url);	
}

function buscarParametros(){
	
	document.getElementById("compensacionGrid").innerHTML = '';

	compensacionGrid = new dhtmlXGridObject("compensacionGrid");

	compensacionGrid.enableEditEvents(false, false);
	
	mostrarIndicadorCarga('indicador');	
	compensacionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	var filtros = jQuery(document.filtroCompensacionForm).serialize();
	var posPath = 'posActual=' + jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	compensacionGrid.load("/MidasWeb/compensacionesAdicionales/listarParametros.action?" + filtros);	
}

function eliminarBlockPage(){
	jQuery('.blockUI').detach();
}

function irConfigurador(idCompensacionIr,sinCompensacion){ 
	if(sinCompensacion == 0){			
		var urlConfigurador = "/MidasWeb/compensacionesAdicionales/init.action?invocacionConfigurador=listadoConfiguraciones&idCompensacionActual="+idCompensacionIr;
		sendRequestJQ(null, urlConfigurador, 'contenido','eliminarBlockPage()');
	}else{
		mostrarMensajeInformativo('Esta Póliza a sido marcada para no recibir Bonos o Compensaciones. <BR> No se puede mostrar pantalla de compensaciones', '20');
	}
}

function irConfiguradorCompensacionId(irCompensacionId){
    var idRamo = "ramo=A"; // Ramo A es igual a autos
	var url = "/MidasWeb/compensacionesAdicionales/init.action?"+idRamo+"&"+"idCompensacionActual=" + irCompensacionId;
	sendRequestJQ(null, url, 'contenido',   null);
}

function irConfiguradorNegocioId(irIdNegocio){
	var idRamo = "ramo=A"; // Ramo A es igual a autos
	var url = "/MidasWeb/compensacionesAdicionales/init.action?"+idRamo+"&"+"idNegocio=" + irIdNegocio;
	sendRequestJQ(null, url, 'contenido',   null);
}

function compensacionNueva(){
	var idRamo = "ramo=A"; // Ramo A es igual a autos
	var url = "/MidasWeb/compensacionesAdicionales/init.action?"+idRamo+"&"+"compensacionNueva=" + true;
	sendRequestJQ(null, url, 'contenido',   null);
}

function eliminarCompensacion(idCompensacionEliminar){
	var urlEliminar = "/MidasWeb/compensacionesAdicionales/eliminarCompensacion.action?idCompensacionEliminar=" + idCompensacionEliminar;
	sendRequestJQ(null, urlEliminar, 'contenido',   null);
}


function verHistoriaModificacion(compensacionId){
	var ventanaModificaciones;
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	
	ventanaModificaciones = dhxWins.createWindow("MODIFICACIONES", 400, 320, 722, 400);
	ventanaModificaciones.btns["minmax1"].hide(); 
	ventanaModificaciones.btns["minmax2"].hide();
	ventanaModificaciones.btns["park"].hide(); 
	ventanaModificaciones.setText("Bitácora de Acciones");
	ventanaModificaciones.setModal(true);
	ventanaModificaciones.centerOnScreen();
	
	ventanaModificaciones.attachURL('/MidasWeb/compensacionesAdicionales/verHistorialCompensacion.action?idCompensacionActual='+compensacionId);

}
function desactivarPorcPromotor() {
	var idPromotor=Number(jQuery('#textClavePromotorGral').val());
	if(idPromotor>0){
		jQuery('#textPorcePrimaPromoGral').attr("disabled", false);
	}else{
		jQuery('#textPorcePrimaPromoGral').attr("disabled", true);
	}
}