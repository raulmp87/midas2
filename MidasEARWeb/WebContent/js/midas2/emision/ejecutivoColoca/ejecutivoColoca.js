﻿
var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";

//limpiar formulario
function limpiarCampos(){
	$('npolmidas').value='';
	$('npolseycos').value='';
	$('nendoso').value='';
	$('fechaInicial').value='';
	$('fechaFinal').value='';
	$('agentes').value='';
	$('ramo').value='';
}

//invoca action para buscar polizas, pero antes valida la forma
function buscarPoliza(){
	var numeroPolizaMidas=$('npolmidas').value;
	var numeroPolizaSeycos=$('npolseycos').value;
	var numeroEndoso= $('nendoso').value;
	var fechaInicial= $('fechaInicial').value;
	var fechaFinal= $('fechaFinal').value;
	var sucursal= $('agentes').value;
	var ramo= $('ramo').value;
	var bandera=validarForma();
	if(bandera)
		iniciarGrid(numeroPolizaMidas, numeroPolizaSeycos, numeroEndoso, fechaInicial, fechaFinal, sucursal, ramo);

}

///////////////////////////metodos para las validaciones de los campos/////////////////////////////////
function validarExisteValor(campo){
	var valorCampo=$(''+campo+'').value;
	if(valorCampo  != 'undefined' && valorCampo != '')
		return true;
	return false;
}

function validarForma(){
	//campos parte 1
	var existePolMidas=validarExisteValor('npolmidas');
	var existePolSeycos=validarExisteValor('npolseycos');
	var existeEndoso=validarExisteValor('nendoso');
	var validaParte1=false;
	
	//campos parte 2
	var existeFInicio=validarExisteValor('fechaInicial');
	var existeFFin=validarExisteValor('fechaFinal');
	var existeSucursal=validarExisteValor('agentes');
	var existeRamo=validarExisteValor('ramo');
	var validaParte2=false;
	
	//validar parte 1
	if(!existeRamo)
	{	
		alert("Se debe seleccionar un ramo.");
		validaParte2= false;
	}
	else{
	if(existeEndoso && (existePolMidas && !existePolSeycos))
		if(validarEndoso() && validarNumeroPolizaMidas())
				validaParte1= true;
	if(existeEndoso && (existePolSeycos && !existePolMidas))
		if(validarEndoso() && validarNumeroPolizaSeycos())
				validaParte1= true;
	if (!existeEndoso && (existePolMidas && !existePolSeycos))
		if(validarNumeroPolizaMidas())
				validaParte1= true;
	if (!existeEndoso && (!existePolMidas && existePolSeycos))
		if(validarNumeroPolizaSeycos())
					validaParte1= true;
	if(!existeEndoso && !existePolMidas && !existePolSeycos)
		validaParte1= true;
	if(existeEndoso && !existePolMidas && !existePolSeycos){
		alert("Si se introduce n\u00c1mero de endoso, se debe introducir n\u00c1mero de P\u00f3liza de Midas o de Seycos en formato (9999-999999-99)");
		validaParte1= false;
	}
	if(existePolSeycos && existePolMidas){
		alert("Solo debe introducir n\u00c1mero de P\u00f3liza de Midas o de Seycos en formato (9999-999999-99) pero NO ambos");
		validaParte1= false;
	}
	
	//validar parte 2
	if(existeSucursal && (existeFInicio && existeFFin))
	
		if(validarFechas())
			validaParte2= true;
	if( existeRamo && (existeFInicio && existeFFin))
	
		if(validarFechas())
			validaParte2= true;
	if(!existeSucursal && (existeFInicio && existeFFin))
		
		if(validarFechas())
			validaParte2= true;
	if(!existeRamo && (existeFInicio && existeFFin))
		
		if(validarFechas())
			validaParte2= true;
	if(!existeRamo && !existeFInicio && !existeFFin)
		validaParte2= true;
	if(!existeSucursal && !existeFInicio && !existeFFin)
		validaParte2= true;
	if(existeSucursal && !existeFInicio && !existeFFin)
	{	
		alert("Si se selecciona una sucursal, se debe seleccionar una fecha.");
		validaParte2= false;
	}
	
	}
	return (validaParte1 && validaParte2);
}

function validarNumeroPolizaMidas(){
	var numPoliza=$('npolmidas').value;
	if(validarNumeroPoliza(numPoliza))
		return true;
	var polizaPattern= /^\d{1,4}\s?-\s?\d{5,12}\s?-\s?\d{2}$/;
	var bandera = true;
	if (numPoliza != 'undefined' && numPoliza != ''){
		if(numPoliza.length<10 || !numPoliza.match(polizaPattern)){
			
			alert("La P\u00f3liza debe cumplir con el formato adecuado (9999-999999-99)");
			bandera = false;
		}
	}
	return bandera;
}

function validarNumeroPolizaSeycos(){
	var numPoliza=$('npolseycos').value;
	if(validarNumeroPoliza(numPoliza))
		return true;
	var polizaPattern= /^\d{1,3}\s?-\s?\d{5,12}\s?-\s?\d{2}$/;
	var bandera = true;
	if (numPoliza != 'undefined' && numPoliza != ''){
		
		if(numPoliza.length<10 || !numPoliza.match(polizaPattern)){
			alert("La P\u00f3liza debe cumplir con el formato adecuado (99999-999999-99)");
			bandera = false;
		}
	}
	return bandera;
}

function validarNumeroPoliza(numPoliza){
	var polizaPattern= /^\d{5,12}$/;
	var bandera = true;
	if (numPoliza != 'undefined' && numPoliza != ''){
		if(numPoliza.length<5 || !numPoliza.match(polizaPattern)){
			bandera = false;
		}
	}
	return bandera;
}

function validarEndoso(){
	
	var nEndoso=$('nendoso').value;
	var endosoPattern= /^\d{1,2}$/;
	var bandera = true;
	if (nEndoso != 'undefined' && nEndoso != ''){
		if(nEndoso.length<2 || !nEndoso.match(endosoPattern)){
			alert("El endoso debe cumplir con el formato adecuado (99)");

			bandera = false;
		}
	}
	return bandera;
	
}

 function validarFechas() { 
		var fechaInicial=$('fechaInicial').value;
		var fechaFinal=$('fechaFinal').value;
		
		var dateStart = '';
		var dateEnd = '';
		var fechaValida = '';
		
	 valuesStart=fechaInicial.split("/"); 
	 valuesEnd=fechaFinal.split("/"); // Verificamos que la fecha no sea posterior a la actual 
	 dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]); 
	 dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]); 
	  
	   fechaValida = new Date( valuesStart[2], (valuesStart[1]-1)+1, valuesStart[0] );
	    
		if (dateEnd > fechaValida)
		 {
			 mostrarMensajeInformativo('Solo se permite un rango de un mes para la busqueda'  , '20');
			 return false;  
			 
		 }	
	 
	 if(dateStart > dateEnd) { 
		mostrarMensajeInformativo('La fecha final debe de ser igual o mayor a la fecha inicial'  , '20');
		 return false; 
		 } 

	 return true; 
	 }
	 
/////////////////////////// FIN   metodos para las validaciones de los campos/////////////////////////////////
 
 //metodos para cargar el combo de los agentes
 function getAllAgentes(){
		var actionURL= "/MidasWeb/suscripcion/cotizacion/auto/agentes/buscarAgente.action?descripcionBusquedaAgente=";

		blockPage();
		jQuery.ajax({
		    type: "GET",
		    url: actionURL,
		    dataType: "XML",
		    async: false,
		    success: function(xml) {
				jQuery("#agentes").append('<option value="0">SELECCIONE ...</option>');
				jQuery(xml).find('item').each(function(){
				  var idAgente = jQuery(this).find('idAgente').text();
				  var id = jQuery(this).find('id').text();
				  var descripcion = jQuery(this).find('descripcion').text();
				  
				   if (descripcion.includes("SUCURSAL")){
					   
					   jQuery("#agentes").append('<option value='+id+'>'+descripcion+'</option>');
				   }
				  
				});
		    },
		    complete: function(jqXHR) {
		    	unblockPage();
		    },
		    error:function (xhr, ajaxOptions, thrownError){
	            alert("Ocurrió un error al obtener los agentes");
				unblockPage();
	        }   	    
		});
 }
	
	
	function iniciarGrid(numeroPolizaMidas, numeroPolizaSeycos, numeroEndoso, fechaInicial, fechaFinal, sucursal, ramo){
		document.getElementById("ejecutivoColocaGrid").innerHTML = '';
		var reporteSesasGrid = new dhtmlXGridObject('ejecutivoColocaGrid');
		mostrarIndicadorCarga('indicador');	
		reporteSesasGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });	
		
		
		var url = "/MidasWeb/emision/ejecutivoColoca/obtenerResultadosEjecutivoColoca.action?polizaMidas="+numeroPolizaMidas+"&polizaSeycos="+numeroPolizaSeycos+"&endoso="+numeroEndoso+"&fechaInicial="+fechaInicial+"&fechaFinal="+fechaFinal+"&agentes="+sucursal+"&claveNeg="+ramo;
		reporteSesasGrid.load(url);
	}
	
	
	function asignarEjecutivo(username, numeroPolizaMidas, numeroPolizaSeycos,numeroEndoso, nombreSucursal, producto, ejecutivo, ramo, esBancaSeguros){
		//Si no es Banca Seguros
		if (esBancaSeguros == 'N'){
			alert("No es posible asignar esta póliza, ya que pertenece a un Agente que no es de Banca Seguros");
			return;
		}
		
		var url = "/MidasWeb/emision/ejecutivoColoca/getPolizaEjecutivoColoca.action?userName="+username+"&polizaMidas="+numeroPolizaMidas
		+"&polizaSeycos"+numeroPolizaSeycos
		+"&endoso="+numeroEndoso+"&claveNeg="+ramo;
		
		
		mostrarVentanaModal( "vm_asignarEjecutivoColoca", "Edicion ejecutivo coloca", null, null, 400, 400, url , null);
		
	}
	
	function guardarPolizaEjecutivo(claveNeg, idCotizacion, idPoliza, ejecutivo, numPoliza, endoso, identificador){

		var ejecutivoAsigVar = document.getElementById('ejecutivoAsig').value

		if (ejecutivoAsigVar != 'undefined' && ejecutivoAsigVar != ''){
  			
			ejecutivo = ejecutivoAsigVar
		}
		
		
		var url = "/MidasWeb/emision/ejecutivoColoca/savePolizaEjecutivoColoca.action?idCotizacionEditar="+idCotizacion+"&idPolizaEditar="+idPoliza+"&claveNeg="+claveNeg+"&userName="+ejecutivo+"&numeroPolizaEditar="+numPoliza+"&endoso="+endoso+"&identificador="+identificador;
		sendRequestModalJQ(document.ejecutivoColocaForm, url,'contenido', 'cerrarVentanaPolizaEjecutivo();','html', defaultContentType);		
	}
	
	function sendRequestModalJQ(fobj, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {

		jQuery.ajax({
		    type: "POST",
		    url: actionURL,
		    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
		    dataType: dataTypeParam,
		    async: true,
		    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
		    success: function(data) {
		    	if(targetId !== undefined && targetId !== null){
			    	jQuery("#" + targetId).html("");
			    	jQuery("#" + targetId).html(data);	    		
		    	}
		    },
		    complete: function(jqXHR) {
		    	unblockPage();
		    	var mensaje = jQuery("#mensaje").text();
		    	var tipoMensaje = jQuery("#tipoMensaje").text();
		    	
		    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				}
				
				if (tipoMensaje != MENSAJE_TIPO_ERROR) {
					if (typeof pNextFunction == "string") {
						eval(pNextFunction);
					} else if (typeof pNextFunction == "function") {
						pNextFunction();
					}
				}
				
		    },
		    error:function (xhr, ajaxOptions, thrownError){
	        }   	    
		});

	}
	
	function cerrarVentanaPolizaEjecutivo(){
		
		parent.cerrarVentanaModal("vm_asignarEjecutivoColoca", 'buscarPoliza()');
	}