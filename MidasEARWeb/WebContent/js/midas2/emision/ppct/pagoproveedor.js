var grid;
var idGrid
var nameGrid;
var ultimaSeccionVisitada;
var gColumnIndex = null;
var gSortDirection = null;
var consultaOrdenPagoProcessor;
var isCancel;
var isHistorico;


jQuery(document).ready(function(){
	
	jQuery("#limpiarOP").click(function(){
		limpiarLista('#divFiltrosOP');
	});
	
	jQuery("#filtrosOP").click(function(){
		displayFilters(this,'#divFiltrosOP');
	});
	
	/***Agregar OrdenPago***/
	jQuery("#cancelAddOP").click(function(){
		regresarIniOP(true);
	});
	
	jQuery("#agregarOP").click(function(){
		agregarOP();
	});
	
	/**agregar detalle**/	
	
	jQuery("#filtrosRecibo").click(function(){
		displayFilters(this,'#divFiltrosRecibo');
	});
	
	jQuery("#limpiarRecibo").click(function(){
		limpiarLista('#divFiltrosRecibo');
	});
});

/**
 * Oculta el contenedor de los filtros y cambia la etiqueta del boton
 * @param btn
 * @param div
 */
function displayFilters(btn, div){
	var isShow = (jQuery(div).css('display')=='none')?false:true;
	
	jQuery(div).toggle('fast', function() {		
		jQuery(btn).html(isShow ? "Mostrar Filtros" : "Ocultar Filtros");
	});
}

/**
 * Limpia los filtros del contenedor
 * @param divOP
 */
function limpiarLista(divOP){
	var claveNegocio = jQuery('#claveNegocio').val();
	jQuery(divOP).find("input").val("");
	jQuery(divOP).find("select").val("");
	jQuery('#claveNegocio').val(claveNegocio);
}

/**
 * Muestra la pantalla para agregar la orden de pago
 */
function agregarOP(){
	var claveNegocio = jQuery('#claveNegocio').val();	
	blockPage();
	mostrarVentanaModal(ventanaAgregar, 'Agregar Orden de Pago', null, null, 980, 240, urlAddOP+'?ordenPago.claveNegocio='+claveNegocio, "regresarIniOP(true);unblockPage();");
}

/**
 * Redirecciona a la pagina donde se agrega el detalle de la Orden de Pago
 */
function agregarDetalle(){
	var param = jQuery('#formAddOP').serialize();
	//redirectVentanaModal(ventanaAgregar, urlContinueOP+'?'+param, null);
	redirectVentanaModalWithBlocking(ventanaAgregar, urlContinueOP+'?'+param, null);
	
}

function redimensionarVentana(id, w, h, isBack){
	if(isBack){
		var fecha = jQuery('#fechaCorteH').val();
		if(fecha!=undefined && fecha!=null && fecha!=""){
			fecha = fecha.split("/");
			jQuery('#fechaCorte').val(fecha[1]+"/"+fecha[2]);	
		}	
	}
	parent.mainDhxWindow.window(id).setDimension(w, h);
	var positionX = (screen.width/2)-(w/2);
	var positionY = (screen.height/2)-(h/2);
	parent.mainDhxWindow.window(id).setPosition(positionX,positionY);
}

/**
 * Regresa a la pantalla de alta de Orden de Pago
 */
function regresarIniOP(isCancel){
	if(isCancel){
		sendRequestJQInWindow('#formDetalleOP', urlBackOP,null, 'parent.cerrarVentanaModal("'+ventanaAgregar+'");', 'html', null);
		//sendRequestJQ(null, urlBackOP,null, 'parent.cerrarVentanaModal("'+ventanaAgregar+'");');
	}else{
		var param = jQuery('#formDetalleOP').serialize()+"&cancel="+isCancel;
		redirectVentanaModalWithBlocking(ventanaAgregar, urlBackOP+'?'+param, null);	
	}
}

/**
 * Confirma el guardado de la Orden de Pago
 */
function confirmGuardaOP(){
	if(confirm('\u00BFEst\u00e1 seguro que desea guardar la Orden de Pago?')){
		guardarOP();
	}
	
}

/**
 * Guarda la Orden de Pago
 */
function guardarOP(){
	//redirectVentanaModal(ventanaAgregar, urlSaveOP, '#formDetalleOP');
	
	sendRequestJQInWindow('#formDetalleOP', urlSaveOP,"results", null, "html", null);
	
}

/**
 * Consulta la OP seleccionado
 * @param id
 */
function verDetalleOP(id){
	blockPage();
	mostrarVentanaModal(ventanaEditar+id, 'Detalle Orden de Pago', null, null, 980, 550, urlViewDetail+'?ordenPago.id='+id, 'unblockPage();');
}

/**
 * Cancela la Orden de Pago
 * @param id
 */
function cancelaOP(id){
	if(confirm('\u00BFEst\u00e1 seguro que desea Cancelar la Orden de Pago?')){
		
		sendRequestJQ(null, urlCancelOP+'?ordenPago.id='+id, 'contenido', null);
	}
}

/**
 * Selecciona un recibo para incluirlo/excluirlo de la Orden de Pago
 */
function seleccionarRecibo_chk(rowId, colIndex, chkState) {
	
	var id = scrollableGrid.cells(rowId, 0).getValue();
	
	url = urlDesAsignarRecibo+'?'+'reciboProveedor.id='+id+'&importePreeliminar='+jQuery('#importePreeliminar').val();
	
	jQuery.asyncPostJSON(url, null, actualizaImporte);
	
}

function actualizaImporte(json){
	var total = json.listImporte[0];
	jQuery('#importePreeliminar').val(total);
	jQuery('#totalOP').html("Total: $"+total);
}

function cerrarVentanaAgregar(){
	parent.cerrarVentanaModal(ventanaAgregar);
	loadGrid();
		
}

function validaFechas(inputIni, inputFin, modal){
	if(compara_fecha(jQuery('#'+inputIni).val(), jQuery('#'+inputFin).val())){
		jQuery('#'+inputIni).val("");
		jQuery('#'+inputFin).val("");
		var mensaje = "Para el rango, la fecha inicial no puede ser mayor a la fecha final";
		if(!modal){
			jQuery('#fechainicial').val("");
			jQuery('#fechaFinal').val("");
		}
		alert(mensaje);
	}
}

function compara_fecha(fechaIncio,fechaFin){
	if(fechaIncio!=null && fechaIncio !="" && fechaFin!=null && fechaFin!=""){

		var fechaIncio1=new Date(fechaIncio.substring(6,10),fechaIncio.substring(3,5)-1,fechaIncio.substring(0,2));
		var anioAlta=fechaIncio1.getFullYear();
		var mesAlta =fechaIncio1.getMonth()+1;
		var diaAlta =fechaIncio1.getDate();
		
		var fechaFin1=new Date(fechaFin.substring(6,10),fechaFin.substring(3,5)-1,fechaFin.substring(0,2));
		var anioFin=fechaFin1.getFullYear();
		var mesFin =fechaFin1.getMonth()+1;
		var diaFin =fechaFin1.getDate();
		
		if(anioAlta>anioFin){
			return true;
		}
		if(anioAlta==anioFin && mesAlta>mesFin){
			return true;
		} 
		if(anioAlta==anioFin && mesAlta==mesFin){
			if(diaAlta>diaFin){
				return true;
			}
		}
		return false;	
	}else{
		return false;
	}
}

