var consultaEmisionGrid;
var consultaEmisionProcessor;
var ultimaSeccionVisitada;
var gColumnIndex = null;
var gSortDirection = null;

function mostrarDetalleConsultaEmision(seccion) {
	dwr.util.setValue("seccion", seccion);
	sendRequestJQ(null, mostrarDetallePath+"?"+jQuery(document.consultaEmisionForm).serialize(), 
			'contenido_' + seccion, 'limpiarSeccionVisitada(\'' + seccion + '\');refreshAccordionResumen();buscarConsultaEmision(null);');
	
}

function limpiarSeccionVisitada(seccion) {
	if (ultimaSeccionVisitada) {
		idContenido = "contenido_" + ultimaSeccionVisitada;
		jQuery(document.getElementById(idContenido)).empty();
	}
	
	ultimaSeccionVisitada = seccion;
}

function seleccionarConsultaEmision(parameters) {
	
	sendRequestJQ(null, seleccionarPath+"?seccion="+ dwr.util.getValue("seccion") + "&" + parameters, 
			'contenido_' + dwr.util.getValue("seccion"), 'refreshAccordionResumen();buscarConsultaEmision(null);');
	
}

function limpiarConsultaEmision() {
	jQuery('#divDetalleEmisionConsulta').find("input").val("");
	jQuery('#divDetalleEmisionConsulta').find("textarea").val("");
}

function limpiarConsultaEmisionGlobal() {
	sendRequestJQ(null, mostrarDetallePath+"?seccion="+ dwr.util.getValue("seccion"), 
			'contenido_' + dwr.util.getValue("seccion"), 'refreshAccordionResumen();buscarConsultaEmision(null);');
}


function buscarConsultaEmision(sOrderBy) {
	
	var seccion = dwr.util.getValue("seccion");
	
	var sOrder = "";
	
	//Creacion del DataGrid
	consultaEmisionGrid = new dhtmlXGridObject("consultaEmisionGrid");
	
	//Evento al seleccionar un registro del grid
	if (jQuery.trim(seccion) != "COBERTURA" && jQuery.trim(seccion) != "COBRANZA") {
		consultaEmisionGrid.attachEvent("onRowSelect", function(id,ind){
			seleccionarConsultaEmision(consultaEmisionProcessor._getRowData(id));
		});
	}
	
	//Evento al ordenar una columna del grid
	consultaEmisionGrid.attachEvent("onBeforeSorting", sortGridOnServer);
	
	//Evento al iniciar la carga del grid
	consultaEmisionGrid.attachEvent("onXLS", function(grid){
		jQuery('#indicador').show();
	});
	
	//Evento al terminar de cargar el grid
	consultaEmisionGrid.attachEvent("onXLE", function(grid){
		jQuery('#indicador').hide();
		
		if (gColumnIndex != null && gSortDirection != null) {
			//Sets Sort Image visible for the column we sort and with the direction we need.
			consultaEmisionGrid.setSortImgState(true, gColumnIndex, gSortDirection);

			gColumnIndex = null;
			gSortDirection = null;
		}
		
    });	
	
	consultaEmisionGrid.enableSmartRendering(true);
	
	var form = document.consultaEmisionForm;
	if (form != null) {
		
		if (sOrderBy != null) {
			sOrder = "&" + sOrderBy;
		}
		
		consultaEmisionGrid.load(navegarPath+"?"+jQuery(document.consultaEmisionForm).serialize() + sOrder);
	} else {
		
		if (sOrderBy != null) {
			sOrder = "?" + sOrderBy;
		}
		
		consultaEmisionGrid.load(navegarPath + sOrder);
	}
	
	
	//Creacion del DataProcessor
	consultaEmisionProcessor = new dataProcessor(seleccionarPath);

	consultaEmisionProcessor.enableDataNames(true);
	consultaEmisionProcessor.setTransactionMode("POST");
	consultaEmisionProcessor.setUpdateMode("off");
	
	consultaEmisionProcessor.init(consultaEmisionGrid);
	
}

function refreshAccordionResumen() {
	
	sendRequestJQ(null, mostrarResumenPath+"?"+jQuery(document.consultaEmisionForm).serialize(), 
			'resumenConsulta', null);
	
}

function alternarResumenConsulta() {
	jQuery('#resumenConsulta').toggle('fast', function() {		
		if (jQuery('#resumenConsulta')[0].style.display == 'none') {
			jQuery('#alternarResumen').html('Mostrar Resumen')
		} else {
			jQuery('#alternarResumen').html('Ocultar Resumen');
		}
	})
}

function sortGridOnServer(columnIndex, sortType, sortDirection) {
	
	var sortPredicate = "orderBy=" + consultaEmisionGrid.getColumnId(columnIndex) + "&direct=" + sortDirection;

	gColumnIndex = columnIndex;
	gSortDirection = sortDirection;
	
	buscarConsultaEmision(sortPredicate);
	
	//Cancels client side sorting by returning “false” from event handler function for onBeforeSorting event.
	return false;
    
}
