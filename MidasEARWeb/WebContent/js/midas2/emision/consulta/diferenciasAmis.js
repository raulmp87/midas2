/**
 * 
 */

function cleanInput(id) {
	jQuery('#' + id).val('');
}
function cleanInputDiv(id) {
	jQuery('#' + id).text('');
}

function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}

/**
 * Traemos la lista de diferencias Amis
 */
function iniciaDiferenciasAmis(){
	
	document.getElementById("diferenciasAmisGrid").innerHTML = '';
	var diferenciasAmisGrid = new dhtmlXGridObject('diferenciasAmisGrid');
	mostrarIndicadorCarga('indicador');	
	diferenciasAmisGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	var url = "/MidasWeb/informacionVehicular/diferenciasAmis/buscarDiferenciasAmis.action";
	diferenciasAmisGrid.load(url);
}

/**
 * Enviamos la peticion del cambio en base a la opcion elegida
 */
function solicitarCambio( id, numPoliza ){
	
	document.getElementById("diferenciasAmisGrid").innerHTML = '';
	var diferenciasAmisGrid = new dhtmlXGridObject('diferenciasAmisGrid');
	mostrarIndicadorCarga('indicador');	
	diferenciasAmisGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	var url = "/MidasWeb/informacionVehicular/diferenciasAmis/solicitarCambio.action?idParam1="+ id + "&idParam2="+ numPoliza;
	diferenciasAmisGrid.load(url);
	
	fijarEstatusDefault();
}

/**Validamos los filtros y realizamos la busqueda***/
function validarFiltros(){
	
	fechaIni=jQuery("#fechaInicial").val();
	fechaFin=jQuery("#fechaFinal").val();
	polizaNum=jQuery("#polizaNum").val();
	agenteId=jQuery("#idAgente").val();
	estatus=jQuery("#estatus").val();
	
/**	if ( fechaIni=='' && fechaFin=='' && polizaNum=='' &&  agenteId=='' && estatus=='0' ){
//		
//		mostrarMensajeInformativo("Al menos un filtro debe elegirse para realizar la búsqueda. Verificar.", 10, null);
//	}
//	else{**/
		
		/**alguna de las fechas o ambas tienen valor*/
		if ( fechaIni != '' || fechaFin != '' ){
			
			if ( fechaIni != '' && fechaFin != '' ){
				if (compare_dates(fechaIni, fechaFin)){  
			    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
			    }
				else{
					this.traerPorFiltrosDiferenciasAmis( fechaIni, fechaFin, polizaNum, agenteId, estatus );
				}
			}
			else{				
				mostrarMensajeInformativo("Para la busqueda por fechas se requieren ambas. Verificar.", 10, null);
			}
		}
		
		else{			
			this.traerPorFiltrosDiferenciasAmis( fechaIni, fechaFin, polizaNum, agenteId, estatus );			
			
		}
	/*}*/
}

/**
 * Traemos la lista por filtros de diferencias Amis
 */
function traerPorFiltrosDiferenciasAmis( fechaIni, fechaFin, polizaNum, agenteId, estatus ){
	
	document.getElementById("diferenciasAmisGrid").innerHTML = '';
	var diferenciasAmisGrid = new dhtmlXGridObject('diferenciasAmisGrid');
	mostrarIndicadorCarga('indicador');	
	diferenciasAmisGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	var url = "/MidasWeb/informacionVehicular/diferenciasAmis/buscarDiferenciasAmisFiltros.action?fechaIni="+fechaIni+"&fechaFin="+fechaFin+"&polizaNum="+polizaNum+"&agenteId="+agenteId+"&estatus="+estatus;
	diferenciasAmisGrid.load(url);
}

/**
 * funcionalidades de la busqueda de agente
 * @param bandera
 */
function seleccionarAgente(bandera){
	mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,320, 710, 155, "/MidasWeb/informacionVehicular/diferenciasAmis/ventanaAgentes.action?bandera="+bandera);
}

function cargaAgente(id,nombre){
	if(nombre != "" && nombre != undefined){
		var campos = nombre.split("-");
		jQuery('#idAgente').val(id);
		jQuery('#agenteNombre').text(campos[0]);
		cerrarVentanaModal("agente");
	}
}

function displayFiltersDifAmis(){
	jQuery('#contenedorFiltros').toggle('fast', function() {
			if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
				jQuery('#mostrarFiltros').html('Mostrar Filtros')
				sizeGridRenovacion();
			} else {
				jQuery('#mostrarFiltros').html('Ocultar Filtros')
				reSizeGridRenovacion();
			}
		});
}

function sizeGridDifAmis(){
	jQuery(document).ready(function(){
		jQuery('#diferenciasAmisGrid').css('height','320px');
		jQuery('div[class=objbox]').css('height','275px');
	})
}

function reSizeGridDifAmis(){
	jQuery(document).ready(function(){
		jQuery('#diferenciasAmisGrid').css('height','140px');
		jQuery('div[class=objbox]').css('height','100px');
	})
}

function fijarEstatusDefault(){
	
	$("#estatus").val("1");
	
}

$(document).ready(function(){

  fijarEstatusDefault();

});


