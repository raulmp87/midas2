/**
 * @author Adriana Flores
 */
function buscarPoliza(){
	var numeroPolizaMidas = dwr.util.getValue("numeroPoliza");	
	var re = /^[0-9]{4}-[0-9]{6,8}-[0-9]{2}$/;
    
	var varText = "";
	if(numeroPolizaMidas.trim() == ''){
		varText = "&nbsp;&nbsp;&nbsp;- Número de Póliza <br>";
	}
	
	if(varText==""){
		if(re.test(numeroPolizaMidas)){
		sendRequestJQ(null, URL_BUSCAR_POLIZA + '?' + jQuery(document.filtrosForm).serialize(), 'contenido', null);
		} else {
			var input = jQuery('#numeroPoliza');
			input.addClass('errorField');
			input.attr('title', "Valor inválido, el formato debe ser: 0000-00000000-00");
		}
	} else{
		mostrarMensajeInformativo("Se debe ingresar: <br>" + varText, 10);
	}
}

function guardarConductoresAdicionales(){
	var conductorAdicional1 = dwr.util.getValue("polizaAutoSeycos.nombreConductorAdicional1");
	var conductorAdicional2 = dwr.util.getValue("polizaAutoSeycos.nombreConductorAdicional2");
	var conductorAdicional3 = dwr.util.getValue("polizaAutoSeycos.nombreConductorAdicional3");
	var conductorAdicional4 = dwr.util.getValue("polizaAutoSeycos.nombreConductorAdicional4");
	var conductorAdicional5 = dwr.util.getValue("polizaAutoSeycos.nombreConductorAdicional5");
	
	if(conductorAdicional1 != "" || conductorAdicional2 != "" || conductorAdicional3 != ""
		|| conductorAdicional4 != "" || conductorAdicional5 != ""){
		var numeroInciso = dwr.util.getValue("polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso");
		if(numeroInciso == ""){		
			dwr.util.setValue("polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso", 1);
		}
		sendRequestJQ(null, URL_GUARDAR_CONDUCTORES + '?' + jQuery(document.configConductoresAdicionalesForm).serialize(), 'contenido', 'buscarPoliza()');
	} else {
		mostrarMensajeInformativo("Debe ingresar al menos un Conductor Adicional");
	}
}

function validateFormat(){	
	var numeroPoliza = dwr.util.getValue("numeroPoliza");
	numeroPoliza = numeroPoliza.replace(/([0-9]{4})([0-9]{6,8})([0-9]{2})/,"$1-$2-$3");
	dwr.util.setValue("numeroPoliza", numeroPoliza);
}