var targetWorkArea='contenido';
var headerValue = "";
var MENSAJE_TIPO_ERROR = "10";
var mainDhxWindow;
var dhxWinsMessage;
var zIndexParentWindow;
var parentWindow;
var headerDescripcionDefault = "Seleccione ...";
var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";
var downloadFileChannelPreffix = "/downloadFile/";

function sendRequestJQasyncFalse(fobj, actionURL, targetId, pNextFunction) {
	return sendRequestJQasyncFalse(fobj, actionURL, targetId, pNextFunction, "html", defaultContentType);
}

function sendRequestJQasyncFalse(fobj, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {

	//blockPage();
	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: false,
	    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
	    success: function(data) {
	    	if(targetId !== undefined && targetId !== null){
		    	jQuery("#" + targetId).html("");
		    	jQuery("#" + targetId).html(data);	    		
	    	}
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				//mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (typeof pNextFunction == "string") {
					eval(pNextFunction);
				} else if (typeof pNextFunction == "function") {
					pNextFunction();
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   	    
	});

}





function sendRequestJQ(fobj, actionURL, targetId, pNextFunction) {
	return sendRequestJQ(fobj, actionURL, targetId, pNextFunction, "html", defaultContentType);
}

function sendRequestXmlJQ(fobj, actionURL, targetId, pNextFunction) {
	return sendRequestJQ(fobj, actionURL, targetId, pNextFunction, "xml", defaultContentType);
}

function sendRequestJQ(fobj, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {

	blockPage();
	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: true,
	    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
	    success: function(data) {
	    	if(targetId !== undefined && targetId !== null){
		    	jQuery("#" + targetId).html("");
		    	jQuery("#" + targetId).html(data);	    		
	    	}
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				//mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (typeof pNextFunction == "string") {
					eval(pNextFunction);
				} else if (typeof pNextFunction == "function") {
					pNextFunction();
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   	    
	});

}
//Esta funcion hace lo mismo que sendRequestJQ, se creo anteriormente mientras que se
//cambiaba a async la otra funcion.
function sendRequestJQAsync(fobj, actionURL, targetId, pNextFunction, dataTypeParam) {
	sendRequestJQ(fobj, actionURL, targetId, pNextFunction, dataTypeParam, defaultContentType);
}

function sendRequestWindow(fobj, actionURL, windowInit, pNextFunction, dataTypeParam) {
	var windowContentId = "windowContent";
	
	//jQuery("#"+windowContentId).remove();
	
	jQuery('<div/>', {
	    id: windowContentId
	}).appendTo('#contenido');
	
	var compositeNextFunction = function () {
		windowInit().attachObject(windowContentId);
		if (pNextFunction != null) {
			pNextFunction();
		}
	}
	setTimeout(function(){dhtmlxWindowOverflow()},4000);
	sendRequestJQAsync(fobj, actionURL, windowContentId, compositeNextFunction, dataTypeParam);	
}

function dhtmlxWindowOverflow(){
	jQuery(".dhtmlxWindowMainContent").each(function(i,obj){
		jQuery(obj).css("overflow-x","hidden");
		jQuery(obj).css("overflow-y","auto");
	});
}

function getMatches(data){
	
}


function sendRequestJQTarifa(fobj, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam){	
	blockPage();
	//FIXME: Poner el async a true cuando se corrija las llamadas a esta funcion que hacen
	//las ventanas de dhtmlx porque por alguna razon estas no funcionan con async true.
	
	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: false,
	    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
	    success: function(data) {
	    	jQuery("#" + targetId).html("");
	    	jQuery("#" + targetId).html(data);
	    },
	    complete: function(jqXHR) {	 
	    	unblockPage();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				//mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (pNextFunction !== null && pNextFunction !== '') {
					eval(pNextFunction);
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   
	});
	
}

/*
function sendRequestJQTarifa(fobj, actionURL, targetId, pNextFunction, dataTypeParam) {
	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: false,
	    success: function(data) {
	    	jQuery("#" + targetId).html("");
	    	jQuery("#" + targetId).html(data);	
	    },
	    complete: function(jqXHR) {
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (pNextFunction !== null && pNextFunction !== '') {
					eval(pNextFunction);
				}
			}
		}
	});

}
*/

/* Obtiene el id del objeto seleccionado de una lista
 * objetoGrid : objeto de tipo dhtmlxGrid
 * indiceIdObjeto : indice de la columna donde se obtiene el id del objeto seleccionado de una lista
*/
function getIdFromGrid(objetoGrid, indiceIdObjeto) {
	var selectedId = objetoGrid.getSelectedId();
	return objetoGrid.cells(selectedId,indiceIdObjeto).getValue();
	
}


function obtieneTipoAccion(tipoAccion){
	return tipoAccion;
}



function blockPage() {
	if (typeof jQuery != 'undefined' && typeof jQuery.unblockUI != 'undefined'){
		jQuery.blockUI({message: jQuery('<div class="block_loading"></div>'),
			css: {width:'0%', top:'35%', left:'40%', border:'0px'} });
	}
}

function unblockPage() {
	if (typeof jQuery != 'undefined' && typeof jQuery.unblockUI != 'undefined'){
		jQuery.unblockUI();
	}
}

function mostrarIndicadorCarga(divName){
	var newHtml = '<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Procesando la informaci&oacute;n, espere un momento por favor...</font>';
	try{
		if (document.getElementById(divName).innerHTML != null){
			document.getElementById(divName).innerHTML = newHtml;
			document.getElementById(divName).style.display='block';
		}
	}catch(e){
		
	}
	
}
function ocultarIndicadorCarga(divName){
	if(document.getElementById(divName) != null){
		document.getElementById(divName).style.display='none';
		try{
			//can cause runtime exception in iexplorer
			document.getElementById(divName).innerHTML = '';
		}catch(e){			
		}
	}
}

function appendQueryString(baseUrl, queryString) {
    var url = baseUrl + ((baseUrl.indexOf('?') > -1) ? '' : '?') + queryString;
    return url;
}

/**
 * Genera un query string a partir de un grid. El query string incluye el nombre proporcionado
 * y como valor el id de la fila. Esté metodo es utilizado cuando se quiere usar el grid como
 * un select multi select de una forma.
 * @param grid el dhtmlx grid
 * @param name nombre utilizado como nombre del parametro para los valores.
 */
function generarQueryStringDeGrid(grid, name) {
	var nameValues = [];
	grid.forEachRow(function(id) {
		nameValues.push({name:name, value:id});
	});
	return jQuery.param(nameValues);
}

/**
 * Borra la fila de un grid utilizando el id.
 * @param grid
 * @param id el id de la fila
 */
function borrarFilaGrid(grid, id) {
	grid.deleteRow(id);
}

/**
 * Metodo para mostrar una ventana usando un contenedor general de ventanas de la aplicacion. 
 * Para acceder a la ventana creada de esta forma se utiliza la siguiente notacion: mainDhxWindow.window(id)
 * donde id, es el id con el que se haya nombrado la ventana. Este metodo es centralizado y sirve para abrir 
 * ventanas que cargan url, usar attach obj, o appendObj. 
 * 
 * @param id - Id con el que se conocera a la ventana dentro del  contenedor
 * @param text - Titulo de la ventana
 * @param positionX
 * @param positionY
 * @param width 
 * @param height
 * @param mode - Modo de trabajo ('URL', 'ATTACH', 'APPEND', 'ATTACH_HTML_STRING')
 * @param target - Depende del modo, si es URL entonces sera la liga. Si es ATTACH , APPEND o ATTACH_HTML_STRING, será el objeto DOM con el que se debe trabajar
 * @param onCloseFunction Funcion para ser ligada a la accion close
 * @returns {Boolean}
 */
function mostrarVentanaModalMode(id, text, positionX, positionY, width, height, mode, target, onCloseFunction){
	/**
	 * Pone la ventana centrada
	 */
	if (width != null && height != null) {
		var positionX = (screen.width/2)-(width/2);
		var positionY = (screen.height/2)-(height/2);
	}
	/**
	 * Si las posiciones son null
	 * lo centra automaticamente
	 */
	if(positionX == null && positionY == null){
		positionX = (screen.width/2);
		positionY = (screen.height/2);
	}
	if(mainDhxWindow == null){
		mainDhxWindow = new dhtmlXWindows();	
		mainDhxWindow.enableAutoViewport(true);
		mainDhxWindow.setImagePath("/MidasWeb/img/dhxwindow/");
		mainDhxWindow.attachEvent("onContentLoaded", function(){
			unblockPage();
		});
	}
	
	var win = mainDhxWindow.window(id);
	if(win != null){
		alert('Ya existe una ventana con ese ID. Defina un nuevo id para evitar conflictos. Error de desarrollo');
		return false;
	}
	
	if(mainDhxWindow != null){
		for (var a in mainDhxWindow.wins) {
			if (mainDhxWindow.wins[a].isModal()) {
				parentWindow = mainDhxWindow.wins[a];
				parentWindow.setModal(false);
				zIndexParentWindow = parentWindow.style.zIndex;
				parentWindow.style.zIndex = 1;
			}
		}
	}	
	win = mainDhxWindow.createWindow(id, positionX, positionY, width, height);
	win.setText(text);
	win.center();
	win.setModal(true);
	win.button("close").attachEvent("onClick", function(){
		cerrarVentanaModal(id);
		if (typeof onCloseFunction == 'function') {
			onCloseFunction();
		} else {
			if(onCloseFunction){
				eval(onCloseFunction);
			}
		}
	});	
	win.button("minmax1").hide();	
	win.button("park").hide();
	if(mode == 'URL'){
		win.attachURL(target);
	}else if(mode == 'APPEND'){
		win.appendObject(target);
	}else if(mode == 'ATTACH'){
		win.attachObject(target);
	}else if(mode == 'ATTACH_HTML_STRING'){
		win.attachHTMLString(target);
	}else{
		alert('El MODE de ventana seleccionado no está soportado');
	}
	return win;
}

/**
 * Metodo para mostrar una ventana usando un contenedor general de ventanas de la aplicacion. 
 * Para acceder a la ventana creada de esta forma se utiliza la siguiente notacion: mainDhxWindow.window(id)
 * donde id, es el id con el que se haya nombrado la ventana.
 * 
 * @param id - Id con el que se conocera a la ventana dentro del  contenedor
 * @param text - Titulo de la ventana
 * @param positionX
 * @param positionY
 * @param width 
 * @param height
 * @param url
 * @param onCloseFunction Funcion para ser ligada a la accion close
 * @returns {Boolean}
 */
function mostrarVentanaModal(id, text, positionX, positionY, width, height, url, onCloseFunction){	
	mostrarVentanaModalMode(id, text, positionX, positionY, width, height, 'URL', url, onCloseFunction);
}

function redirectVentanaModal(id, url, form){
	blockPage();
	if(form != null){		
		url = url + '?' + jQuery(form).serialize();
	}	
	if(url.length < 2000){
		if(parent.mainDhxWindow != null){
			parent.mainDhxWindow.window(id).attachURL(url);
		}else{
			mainDhxWindow.window(id).attachURL(url);
		}
	}else{
		form.submit();
	}
		
}


function submitVentanaModal(id, form){
	blockPage();
	if(form != null){			
		form.submit();
	}		
}


/**
 * Cierra una ventana modal abierta con la funcion mostrarVentanaModal 
 * @param id - Id con el que se definio la ventana
 */
function cerrarVentanaModal(id, onCloseFunction){
	if(mainDhxWindow != null){
		var win = mainDhxWindow.window(id);
		if(win != null){		
			win.close();
			
			//Fix Focus IE
			var existeVentanaAbierta = false;
			mainDhxWindow.forEachWindow(function(win){
				existeVentanaAbierta = true;
		    });
			if(existeVentanaAbierta == false){
				if(jQuery("#contenido").find("input[type$='text']").not("input[class$='hasDatepicker']")[0] != undefined){
					try{
						jQuery("#contenido").find("input[type$='text']").not("input[class$='hasDatepicker']")[0].focus();
					}catch(err){}
				}else{
					try{
						jQuery("#contenido").find('a')[0].focus();
					}catch(err){}
				}
			}
			reestablecerVentanaPadre();
			if(onCloseFunction != null){
				eval(onCloseFunction);
			}
		}	
	}
}

/**
 * Funcion de apoyo para el cierre de ventanas en cascada
 */
function reestablecerVentanaPadre(){
	if(parentWindow != null){
		parentWindow.setModal(true);
		parentWindow.style.zIndex = zIndexParentWindow;
		parentWindow = null;
	}
}

/**
 * Muestra un mensaje informativo tipo alert. Esta funcion es utilizada dentro de messageHeader 
 * y se ejecuta siempre que venga seteado un mensaje de manera automatica
 * @param mensaje
 * @param id -- 30 OK, 20 INFO, 10 NO
 * @param afterClose
 * @param executeAfterCloseWithError
 */
function mostrarMensajeInformativo(mensaje, id, afterClose, executeAfterCloseWithError){	
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;	
	var parent = null;
	var arregloVentanas = null;	
	var html = "<div class=\"mensaje_encabezado\"></div>"
		html = html  + "<div class=\"mensaje_contenido\">"
		html = html  + "<div id=\"mensajeImg\">"
		if (id === "30"){
			html = html  + "<img src='/MidasWeb/img/b_ok.png'>";
		}else if (id === "20"){
			html = html  + "<img src='/MidasWeb/img/b_info.jpg'>";
			//afterClose = null; // OFAL: Se comenta para soportar ejecución de funciones despues de cerrar un mensaje tipo INFO
		}else if (id === "10"){
			html = html  + "<img src='/MidasWeb/img/b_no.jpg'>";
			afterClose = null;
		}
		html = html  + "</div>"	
		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\">"		
		html = html  + remplazarCaracteresFix(mensaje);
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">"
		html = html  + "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=\" cerrarMensajeInformativo();\">Aceptar</a></div>";
		html = html  + "</div>"
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_pie\"></div>"				
		/*
		var browserName=navigator.appName;
		if (browserName=="Microsoft Internet Explorer"){
			mostrarVentanaModal("mensajeAUsuario", '', 500, 340, 433, 190);
		}else{
			mostrarVentanaModal("mensajeAUsuario", '', 500, 340, 420, 157);
		}
		mensajeAUsuario = mainDhxWindow.window("mensajeAUsuario");
		mainDhxWindow.setSkin("midas_message");
		mensajeAUsuario.attachHTMLString(html);		
		mensajeAUsuario.attachEvent("onClose", function(win){
			mensajeAUsuario = null;				
			reestablecerVentanaPadre();
			if(id == null || id != '10' || (id == '10' && executeAfterCloseWithError)){ //que no sea error
				if (afterClose) {
					eval(afterClose);
				}
			}				
		});
		*/
		
		if (dhxWinsMessage==null){
			dhxWinsMessage = new dhtmlXWindows();
			dhxWinsMessage.setSkin("midas_message");
		}		
		mensajeAUsuario = dhxWinsMessage.window("mensajeAUsuario");
		if (mensajeAUsuario!=null && dhxWinsMessage.window("mensajeAUsuario").isHidden()) {
			mensajeAUsuario.close();
			mensajeAUsuario = null;
		}		
		if (mensajeAUsuario==null){
			var browserName=navigator.appName; 			
			if (browserName=="Microsoft Internet Explorer"){
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 433, 190);
			}else{
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 420, 157);
			}
			mensajeAUsuario.center();			
			if(mainDhxWindow != null){	
				var topWindow = mainDhxWindow.getTopmostWindow();
				if(topWindow != null){
					var ventanaPadreId = topWindow.getId();
					if(ventanaPadreId != null){
						ventanaInvocadora = mainDhxWindow.window(ventanaPadreId);			
						ventanaInvocadora.setModal(false);				
						//ventanaInvocadora.style.zIndex = 40;
					}else{
						ventanaInvocadora = null;
					}
				}
				var i = 0;
				for (var a in mainDhxWindow.wins) {	
					if(!mainDhxWindow.wins[a].isHidden()){
						arregloVentanas = new Array(++i);
						arregloVentanas[i - 1] = mainDhxWindow.wins[a].style.zIndex;
						mainDhxWindow.wins[a].style.zIndex = 40;
					}
				}
			}
			mensajeAUsuario.setModal(true);
			mensajeAUsuario.bringToTop();
			mensajeAUsuario.attachHTMLString(html);
			//mensajeAUsuario.style.zIndex = 100; //top of everything
			mensajeAUsuario.attachEvent("onClose", function(win){
				mensajeAUsuario =null;		
				try{
					if(ventanaInvocadora != null){					
					ventanaInvocadora.setModal(true);						
					if(arregloVentanas != null){
						i = 0;
						for (var a in mainDhxWindow.wins) {
							mainDhxWindow.wins[a].style.zIndex = arregloVentanas[i++];						
						}
					}
				}	
					
				}catch(error) {
					// FIX venta modal IE 8
					cerrarVentanaModal(ventanaPadreId);
				}
				
				if(id == null || id != '10' || (id == '10' && executeAfterCloseWithError)){ //que no sea error
					//alert(afterClose);
					if (afterClose) {
						try{
							eval(afterClose);
						}catch(e){}
					}
				}				
			});			
		}
}
/**
 * Muestra un mensaje de confirmacion tipo alert. Esta funcion es utilizada dentro de messageHeader 
 * y se ejecuta automaticamente siempre que venga seteado un mensaje de la tiguiente manera "confirm:message:"
 * mostrando como como mensaje el texto que este despues de los ultimos : (dos puntos)
 * ejemplo confirm:message:Yo soy, Yo soy, Yo soy el rey del tribal...
 * @param mensaje
 * @param id
 * @param method metodo a ejecutar cuando recibe una respuesta afirmativa
 * @param methodClose metodo que se ejecuta si recibe una respuesta negativa
 * @param executeAfterCloseWithError
 */
function mostrarMensajeConfirm(mensaje, id, method,methodClose,executeAfterCloseWithError){	
	var mensajeAUsuario = null;
	var ventanaInvocadora = null;
	var zIndexInvocadora = 10;	
	var parent = null;
	var arregloVentanas = null;	
	var html = "<div class=\"mensaje_encabezado\"></div>"
		html = html  + "<div class=\"mensaje_contenido\">"
		html = html  + "<div id=\"mensajeImg\">"
		if (id === "30"){
			html = html  + "<img src='/MidasWeb/img/b_ok.png'>";
		}else if (id === "20"){
			html = html  + "<img src='/MidasWeb/img/b_info.jpg'>";
		}else if (id === "10"){
			html = html  + "<img src='/MidasWeb/img/b_no.jpg'>";
		}
		html = html  + "</div>"	;
		html = html  + "<div id=\"mensajeGlobal\" class=\"mensaje_texto\">";		
		html = html  + remplazarCaracteresFix(mensaje);
		html = html  + "</div>";
		html = html  + "<div class=\"mensaje_botones\" id=\"mensajeBoton\">";
		html = html  + "<div class=\"b_cancelar\"> <a href=\"javascript:void(0);\"onclick=";
		if(methodClose != null && methodClose != 'undefined'){
			html = html  +  methodClose + ";cerrarMensajeInformativo();>Cancelar</a></div>";
		}else{
			html = html  + "cerrarMensajeInformativo();>Cancelar</a></div>";
		}
	
		html = html  + "<div class=\"b_aceptar\"> <a href=\"javascript:void(0);\" onclick=";
		html = html  + method +";cerrarMensajeInformativo();>Aceptar</a></div>";
		html = html  + "</div>"
		html = html  + "</div>"
		html = html  + "<div class=\"mensaje_pie\"></div>"				
		
		if (dhxWinsMessage==null){
			dhxWinsMessage = new dhtmlXWindows();
			dhxWinsMessage.setSkin("midas_message");
		}		
		mensajeAUsuario = dhxWinsMessage.window("mensajeAUsuario");
		if (mensajeAUsuario!=null && dhxWinsMessage.window("mensajeAUsuario").isHidden()) {
			mensajeAUsuario.close();
			mensajeAUsuario = null;
		}		
		if (mensajeAUsuario==null){
			var browserName=navigator.appName; 			
			if (browserName=="Microsoft Internet Explorer"){
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 433, 190);
			}else{
				mensajeAUsuario = dhxWinsMessage.createWindow("mensajeAUsuario", 500, 340, 420, 157);
			}
			mensajeAUsuario.center();			
			if(mainDhxWindow != null){	
				var topWindow = mainDhxWindow.getTopmostWindow();
				if(topWindow != null){
					var ventanaPadreId = topWindow.getId();
					if(ventanaPadreId != null){
						ventanaInvocadora = mainDhxWindow.window(ventanaPadreId);			
						ventanaInvocadora.setModal(false);				
						//ventanaInvocadora.style.zIndex = 40;
					}else{
						ventanaInvocadora = null;
					}
				}
				var i = 0;
				for (var a in mainDhxWindow.wins) {	
					if(!mainDhxWindow.wins[a].isHidden()){
						arregloVentanas = new Array(++i);
						arregloVentanas[i - 1] = mainDhxWindow.wins[a].style.zIndex;
						mainDhxWindow.wins[a].style.zIndex = 40;
					}
				}
			}
			mensajeAUsuario.setModal(true);
			mensajeAUsuario.bringToTop();
			mensajeAUsuario.attachHTMLString(html);
			//mensajeAUsuario.style.zIndex = 100; //top of everything
			mensajeAUsuario.attachEvent("onClose", function(win){
				mensajeAUsuario =null;				
				if(ventanaInvocadora != null){					
					ventanaInvocadora.setModal(true);						
					if(arregloVentanas != null){
						i = 0;
						for (var a in mainDhxWindow.wins) {
							mainDhxWindow.wins[a].style.zIndex = arregloVentanas[i++];						
						}
					}
				}	
//				if(id == null || id != '10' || (id == '10' && executeAfterCloseWithError)){ //que no sea error
//					if (afterClose) {
//						try{
//							eval(afterClose);
//						}catch(e){}
//					}
//				}				
			});			
		}
}
/**
 * Function anexada al boton cerrar de la ventana de mensaje informativo y sirve para cerrar la ventana
 */
function cerrarMensajeInformativo(){
	if(dhxWinsMessage != null){
		dhxWinsMessage.window("mensajeAUsuario").setModal(false);
		dhxWinsMessage.window("mensajeAUsuario").hide();
		dhxWinsMessage.window("mensajeAUsuario").close();
		dhxWinsMessage.unload();
		dhxWinsMessage = null;
	}
}

/**
 * Método que genera y configura la ventana de tipo Vault para subir archivos. 
 * Para utilizarla es necesario seleccionar un codigoTipoDocumento para el archivo que se está subiendo. 
 * En caso de no contar con el código revisarlo y/o incluirlo en el Action VaultAction.uploadHandler. 
 * Este método se encarga de guardar la referencia del archivo en base datos (ControlArchivoDTO). Para un ejemplo simple ver el tipo 30.
 * @param urlProcesaArchivo. Url que se encargará de procesar el objeto archivo que se está subiendo. 
 * 							 Este objeto es el contenedor de ControlArchivoDTO más atributos adicionales que se ocupen. 
 * 							 Para ver un ejemplo abrir ArchivoAdjuntoCondicionEspecial
 * @param parameters Lista de parámetros adicionales que ocupa el método del action en formato atributo=valor&atributo2=valor2
 * @param target Div contenedor que se actualizará con el action. 
 * 		  	     Tratar de utilizar PRG en los actions. Ver ejemplo en AdjuntarArchivoCondicionEspecial en CondicionEspecialAction
 * @param codigoTipoDocumento Código del documento de ControlArchivoDTO. Ver explicación del método para más información.
 * @param functionOnAddFile. Función anónima (function(){}) para sobreescribir el método onAddFile del Vault. 
 * 		 					 Aquí se puede validar por ejemplo la extensión del archivo.
 * @param functionOnBeforeUpload Función anónima (function(){}) para sobreescribir el método onAddFile del Vault. 
 * 								 Por default en este método se incluye una confirmación para el usuario. 
 * 		 					 	
 */
function mostrarVentanaVault(urlProcesaArchivo, parameters, target, codigoTipoDocumento, functionOnAddFile, functionOnBeforeUpload ){
	var url;
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjutarDocumentoWin", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar Archivo");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			urlProcesaArchivo = urlProcesaArchivo + '?';
        			if (parameters != null && parameters !== ''){
        				url = urlProcesaArchivo + parameters + '&';
        			}
        			url = url + 'idToControlArchivo=' + idToControlArchivo;
        			sendRequestJQ(null, url, target, null);
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("adjutarDocumentoWin").close();
    };
    
    if(functionOnBeforeUpload == null || functionOnBeforeUpload == ''){
	    vault.onBeforeUpload = function(fileName) {	    	
	    	var respuesta = confirm("\u00BFEstá seguro que desea agregar el archivo?");
	    	if(respuesta){
	    		return true;
	    	}else{
	    		return false;
	    	}
	    }
    }else{
    	vault.onBeforeUpload = functionOnBeforeUpload;
    }
            
    vault.onAddFile = functionOnAddFile;
    vault.create("vault");
    vault.setFormField("claveTipo", codigoTipoDocumento);
}

function remplazarCaracteresFix(str){
	str = str.replace(/&lt;/g, '<');
	str = str.replace(/&gt;/g, '>');
	str = str.replace(/Ã³/gi, "\u00F3").replace(/Ã©/gi, "\u00E9").replace(/Ãº/gi, "\u00FA").replace(/Ã¡/gi, "\u00E1").replace(/Â¿/gi, "\u00bf")
	return str;
}
/**
 * Funcion para hacer el cambio de Oculto a Visible de los campos de Filtros Extra Escencialmente,
 * sin embargo se puede usar para ocultar y mostrar cualquier cosa que tenga la clase CSS de JS_hide, o la clase que tu asignes como parametro.
 * @param id
 */
function toggle_Hidden(CSSclass){
	var idHidden = "JS_hide";
	if(CSSclass!=undefined){
		idHidden = CSSclass;
	}
	jQuery("."+idHidden).toggle();
}
/**
 * 
 * @param url
 * @param forma
 * @param callback
 */
function genericPostJsonReturn(url,forma,callback){
	try{
		if(forma !=undefined)
			params = jQuery(forma).serialize();
		if (callback==undefined)
			callback =genericCallBack;
		jQuery.asyncPostJSON(url,params,callback);
	}catch(e){alert('Error en la funcion util.js->genericPostJsonReturn()');}
}
function genericCallBack(JSON){
	alert('Se te olvido Agregar un Callback al genericPostJsonReturn');
}

function genericPostWithParamsJsonReturn(url,jsonParam,callback){
	try{
		var params = (jsonParam!=undefined)?jQuery.param(jsonParam):{};
		if (callback==undefined)
			callback =genericCallBack;
		jQuery.asyncPostJSON(url,params,callback);
	}catch(e){alert('Error en la funcion util.js->genericPostJsonReturn()');}
}

/**
 * Funcion callback para mostrar los datos del agente y su relacion con el Agente. 
 * @param JSON
 */
function callBackRelacionClienteAgente(JSON){
	var Datos = JSON;
}

/**
 * Accede a la ventana modal  de Asegurado en inciso abierta con la funcion mostrarVentanaModal y ejecuta la funcion asignar cliente
 * @param id - Id con el que se definio la ventana
 */
function accederVentanaModalComplementarAsegurado(id, idCliente, idDomCliente){
	if(mainDhxWindow != null){
		var win = mainDhxWindow.window(id);
		if(win != null){		
			// calling function from inner.html
			win._frame.contentWindow.asignarIdCliente(idCliente, idDomCliente);
		}	
	}
}


function getWindowContainerFrame(windowId){
	var win = null;
	try{
		if(mainDhxWindow != null && mainDhxWindow != 'undefined'){
			win = mainDhxWindow.window(windowId);
		}
	}catch(e){		
	}	
	if(win != null){		
		return win._frame;
	}else{
		return null;
	}	
}


function readResponseDataProcessor(node,pNextFunction){
	mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipoMensaje"),pNextFunction); // Details
}
/**
 * Metodo que identifica si el mensaje contiene la palabra reservada
 * confirm:message:
 * @param arg
 * @returns true si es una ventana de tipo confirm
 */
function typeWindow(arg){
    var re = new RegExp('^confirm:message:');
    return  re.test(arg);
}

/**
 * Obtiene el mensaje a mostrar de una confirmaci�n
 * @param arg
 * @returns mensaje
 */
function getMenssageConfirm(arg){
	return arg.substring(16,arg.length);
}
/**
 * Limpia un formulario
 */
function $_cleanForm(idForm){
	try{
		jQuery('#'+idForm).each (function(){
			  this.reset();
		});
	} catch(error) {
		mostrarMensajeInformativo("Limpiar Form Error: \n" + error.message, "30", null, null);
	}
	
}

function getDateFormat(fullDate) {
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
	  
	var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
	
	return currentDate;
}

function loopSound(sound, times) {
	var current = 0;
	_loopSound(sound, times, current);
	function _loopSound(sound, times, current) {
		if (++current <= times) {
			sound.play({
				onfinish : function() {
					_loopSound(sound, times, current);
				}
			});	
		}
	}	
}

function getParentObject( id ){
	return parent.document.getElementById(id);
}

/**
 * Variación de metodo "mostrarVentanaModal" permite recuperar el objeto
 * dhtmlxWindow, para manipularlo 
 * 
 * @param id - Id con el que se conocera a la ventana dentro del  contenedor
 * @param text - Titulo de la ventana
 * @param positionX
 * @param positionY
 * @param width 
 * @param height
 * @param url
 * @param onCloseFunction Funcion para ser ligada a la accion close
 * @returns {dhtmlXWindows}
 */
function viewAndGetModalWindow(id, text, positionX, positionY, width, height, url, onCloseFunction){	
	return mostrarVentanaModalMode(id, text, positionX, positionY, width, height, 'URL', url, onCloseFunction);
}

function blockPageInWindow() {
	if (typeof parent.jQuery != 'undefined' && typeof parent.jQuery.unblockUI != 'undefined'){
		parent.jQuery.blockUI({message: parent.jQuery("#img_indicator"),
			css: {width:'0%', top:'35%', left:'40%', border:'0px'} });
	}
}

function unblockPageInWindow() {
	if (typeof parent.jQuery != 'undefined' && typeof parent.jQuery.unblockUI != 'undefined'){
		parent.jQuery.unblockUI();
	}
}

function sendRequestJQInWindow(fobj, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {

	blockPageInWindow();
	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: true,
	    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
	    success: function(data) {
	    	if(targetId !== undefined && targetId !== null){
	    		
	    		jQuery("#" + targetId).html("");
		    	jQuery("#" + targetId).html(data);
	    			    		
	    	}
	    },
	    complete: function(jqXHR) {
	    	unblockPageInWindow();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				//mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (typeof pNextFunction == "string") {
					eval(pNextFunction);
				} else if (typeof pNextFunction == "function") {
					pNextFunction();
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
	    	unblockPageInWindow();
        }   	    
	});

}

function redirectVentanaModalWithBlocking(id, url, form){
	blockPageInWindow();
	if(form != null){		
		url = url + '?' + jQuery(form).serialize();
	}	
	if(url.length < 2000){
		if(parent.mainDhxWindow != null){
			parent.mainDhxWindow.window(id).attachURL(url);
		}else{
			mainDhxWindow.window(id).attachURL(url);
		}
	}else{
		form.submit();
	}
	unblockPageInWindow();	
}

/***
 * Uso: se deberá agregar al onkeyup la funcion onkeyup="mascaraDecimales('#valuacionTxt',this.value);" 
 * @param elemento el tag sobre el cual afectar con . ó # , ejem #valuacionTxt .valuacionTxt
 * @param valor
 */
function mascaraDecimales(elemento,valor){
	
	var exp = new RegExp(/((\.{1}\d*))?$/);
	var sDecimales;
	var estimacion = jQuery(elemento).val();
	
	if( valor != null || valor != "" ){
		
		// OBTIENE EN UN ARREGLO TODAS LAS COINCIDENCIAS, EN LA PRIMERA POSICION CONTIENE LA MAS ALTA
		var coincidencias = exp.exec(estimacion);
		
		if( coincidencias != null && coincidencias[0].length > 3 ){
			sDecimales = coincidencias[0].substring(0,3);
			jQuery(elemento).val( estimacion.replace(coincidencias[0],sDecimales) ) ;
		}
		
	}	
	
}

/***
* Función que serializa una forma y resuelve el problema del envío de caracteres
* especiales cuando se hace un load de dhtmlxgrid o una exportación a Excel.
*/
function encodeForm(form){
	var serializedForm = encodeURI(form.serialize());
	 //Reemplaza las diagonales para los atributos tipo Date
	serializedForm = replaceAll(serializedForm, '%252F', '/');
	return serializedForm;
}

function encodeStr(cadena){
	var str = encodeURI(cadena);
	str = replaceAll(str, '%252F', '/');
	return str;
}

function replaceAll(oldStr, removeStr, replaceStr, caseSenitivity){
	if(caseSenitivity == 1){
		cs = "g";
		}else{
		cs = "gi";	
	}
	var myPattern=new RegExp(removeStr,cs);
	newStr =oldStr.replace(myPattern,replaceStr);
	return newStr;
}

/**
 * Inicializa los input text para agregar format de moneda al cargarse ademas de setear la funcion blur
 * 
 * Se requiere agregar el css class de formatCurrency a los inputs que deben usar el formato.
 * Se requiere agregar la lib de js jquery.formatCurrency-1.4.0.min.js en el jsp donde se utilizarán
 * 
 * Recordar utilizar removeCurrencyFormatOnTxtInput antes de dar submit o sendrequest.  
 */
function initCurrencyFormatOnTxtInput(){
	try{
		jQuery('.formatCurrency').each(function(){
			jQuery(this).formatCurrency({ negativeFormat :'- %s%n'  });
			jQuery(this).blur(function()
		    {
	            jQuery('.formatCurrency').formatCurrency({ negativeFormat :'- %s%n'  });
	        });
		});
	}catch(e){}
}

/**
 * Remueve el formato de moneda de los inputs. Sirve para hacer el submit/send request y se espera un numero del lado de action
 */
function removeCurrencyFormatOnTxtInput(){
	try{
		jQuery('.formatCurrency').each(function(){
			jQuery(this).toNumber();		
		});
	}catch(e){}
}

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

var ie = (function(){

    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );

    return v > 4 ? v : undef;

}());


function downloadFileCallback(message) {
	
	/* JSON returned
	 * {"action":"start/end","uuid":"<uuid>", "name":"<context name>","ext":"<file extension>"}
	 */
	
	var process = jQuery.parseJSON(message.data);
		
	if (process.action == "end") {
		
		cometdUtil.unsubscribe(downloadFileChannelPreffix + process.uuid);
		
		jQuery('iframe#downloadFrame_' + process.uuid).attr('name', 'finished');
		
	}
	
	return process;
	
}

function downloadFileWithoutBlockingCallback(message) {
	
	var process = downloadFileCallback(message);
	
	var container = null;
	
	if (process.action == "end") {
		
		jQuery('div#loading_' + process.uuid).remove();
		
		return true; //just indicates is the ending part
		
	} else {
		
		if (jQuery('div#loading_container').length > 0) {
			
			container = jQuery('div#loading_container');
			
		} else {
			
			container = jQuery('<div id="loading_container"></div>');
			
		}
		
		container.appendTo(document.body);
		
		jQuery('<div id="loading_' + process.uuid + '" class="downloadingFile">Procesando ' + process.name +  '...<div class="' + process.ext + '"></div></div>').appendTo('div#loading_container');
		
		return false;
		
	}
	
}

function downloadFileWithBlockingCallback(message) {
	
	var process = downloadFileCallback(message);
	
	if (process.action == "end") {
		
		unblockPage();
		
		return true; //just indicates is the ending part
		
	} else {
		
		blockPage();
		
		return false;
		
	}
	
}

function downloadFile(url, callback) {
	
	
	var appendParamChar = "?";
	
	var downloadFileChannel = downloadFileChannelPreffix;
	
	var uuid = generateUUID();
	
	downloadFileChannel += uuid;
	
	jQuery( "iframe[id^='downloadFrame_'][name*='finished']" ).remove();
	
	if (jQuery( "iframe[id^='downloadFrame_'][src*='" + url + "'][name*!='finished']" ).length > 0) {
		
		if (!confirm('La solicitud ya está en proceso \u00BF Desea generar el proceso nuevamente?')) {
			
			return;
			
		}
		
	}
	
	
	jQuery('<iframe id="downloadFrame_' + uuid + '" frameborder="0" width="0px" height="0px"></iframe>').appendTo(document.body);
		
	cometdUtil.subscribe(downloadFileChannel, callback);
	
	if (url.indexOf(appendParamChar) >= 0) {
		
		appendParamChar = "&";
		
	}
	
	jQuery('iframe#downloadFrame_' + uuid).attr('src', url + appendParamChar + "uuid=" + uuid);
	
}

function downloadFileCustom(url, callback) {
	
	if (ie < 9) {
		
		window.open(url);
		
	} else {
		
		downloadFile(url, callback);
		
	}
	
}

function downloadFileWithoutBlocking(url) {
	
	downloadFileCustom(url, downloadFileWithoutBlockingCallback);
	
}

function downloadFileWithBlocking(url) {
	
	downloadFileCustom(url, downloadFileWithBlockingCallback);
	
}


Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return "$" + sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

