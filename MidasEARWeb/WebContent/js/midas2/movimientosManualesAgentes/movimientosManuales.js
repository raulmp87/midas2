var movimientosManualesGrid;

//Agentes
function mostrarListadoAgentes_MovManual(){
	var idAgente = jQuery("#idAgente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function obtenerVentanaAgentes(){	
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 400, 320, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agentes");
	return ventanaAgentes;
}

function onChangeAgente_MovManual(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgt_MovManual(){
	var calveAgente = jQuery("#idAgente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function loadInfoAgente(json){
	if(json.agente==null){
		alert("La clave del Agente no Existe ");//ó no esta Autorizado
		jQuery("#id").val("");
		jQuery("#idAgente").val("");
		jQuery("#fechaEstatus").val("");
		jQuery("#motivoEstatusAgente").val("");
		jQuery("#tipoSituacion").val("");
		
		jQuery("#cargo").val("");
		jQuery("#abono").val("");
		jQuery("#saldoFinal").val("");
	}else{
		var agente=json.agente;
		var id=agente.id;
		var idAgen=agente.idAgente;
		var nombreCompleto = agente.persona.nombreCompleto;
		var tipoSituacion = agente.tipoSituacion.valor;
		var motivoEstatus = agente.descripMotivoEstatusAgente;
		var fechaAlta= agente.fechaAltaString;
		var cargo = json.cargo;
		var abono = json.abono;
		var saldoFinal = json.saldoFinal;
		
		jQuery("#nombreCompleto").val(nombreCompleto);
		jQuery("#id").val(id);
		jQuery("#idAgente").val(idAgen);
		jQuery("#fechaEstatus").val(fechaAlta);
		jQuery("#motivoEstatusAgente").val(motivoEstatus);
		jQuery("#tipoSituacion").val(tipoSituacion);
		
		jQuery("#cargo").val(formatCurrency(cargo));		
		jQuery("#abono").val(formatCurrency(abono));
		jQuery("#saldoFinal").val(formatCurrency(saldoFinal));
	}
}

//Conceptos
function mostrarListadoConceptos(){
	var idConcepto = jQuery("#conceptoId").val();
	var field="txtIdConcepto";
	if(idConcepto == ""){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/mostrarContenedorConcepto.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaConceptos);
	}else{
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerConcepto.action";
		var data={"conceptoMovimiento.id.idConcepto":idConcepto,"conceptoMovimiento.id.claveTipoConcepto":""};
		jQuery.asyncPostJSON(url,data,loadInfoConcepto);
	}
}

function obtenerVentanaConceptos(){	
	var wins = obtenerContenedorVentanas();
	ventanaConceptos= wins.createWindow("conceptoModal", 400, 320, 900, 450);
	ventanaConceptos.center();
	ventanaConceptos.setModal(true);
	ventanaConceptos.setText("Consulta de Conceptos");
	return ventanaConceptos;
}

function onChangeConcepto(){
	var id = jQuery("#txtIdConcepto").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerConcepto.action";
		var data={"conceptoMovimiento.idConcepto":id,"conceptoMovimiento.claveTipoConcepto":""};
		jQuery.asyncPostJSON(url,data,loadInfoConcepto);
	}
}

function onChangeIdConcepto(){
	var claveConcepto = jQuery("#conceptoId").val();
	if(jQuery.isValid(claveConcepto)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerConcepto.action";
		var data={"conceptoMovimiento.idConcepto":claveConcepto,"conceptoMovimiento.claveTipoConcepto":""};
		jQuery.asyncPostJSON(url,data,loadInfoConcepto);
	}else{
		jQuery("#conceptoDescripcion").val("");
		jQuery("#idC").val("");		
	}
}

function loadInfoConcepto(json){
	
	if(json.conceptoMovimiento==null || json.conceptoMovimiento.descripcionConcepto==null){ 
		alert("La clave del Concepto no Existe ");//ó no esta Autorizado
		jQuery("#idC").val("");		
		jQuery("#conceptoId").val("");
		jQuery("#conceptoDescripcion").val("");
		jQuery("#descripcion").val("");
		jQuery("#tipoMovimientoId").val("");
		
	}else{
		var conceptoMovimiento=json.conceptoMovimiento;
		
		//var idC=conceptoMovimiento.id;
		var conceptoId=conceptoMovimiento.idConcepto;				
		var conceptoDescripcion = conceptoMovimiento.descripcionConcepto;
		var claveTipoMovimiento = conceptoMovimiento.claveCargoAbono;
		
		//jQuery("#idC").val(idC);
		jQuery("#conceptoId").val(conceptoId);		
		jQuery("#conceptoDescripcion").val(conceptoDescripcion);
		jQuery("#descripcion").val(conceptoDescripcion);
		jQuery("#tipoMovimientoId").val(claveTipoMovimiento);
	}
}

//Ramo
function mostrarListadoRamos(){
	var claveRamo = jQuery("#ramoId").val();
	var field="txtIdRamo";
	if(claveRamo == ""){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/mostrarContenedorRamo.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaRamos);
	}else{
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerRamo.action";
		var data={"ramoMovimiento.codigo":claveRamo,"ramoMovimiento.idTcRamo":""};
		jQuery.asyncPostJSON(url,data,loadInfoRamo);
	}
}

function obtenerVentanaRamos(){	
	var wins = obtenerContenedorVentanas();
	ventanaRamos= wins.createWindow("ramoModal", 400, 320, 900, 450);
	ventanaRamos.center();
	ventanaRamos.setModal(true);
	ventanaRamos.setText("Consulta de Ramos");
	return ventanaRamos;
}

function onChangeRamo(){
	var id = jQuery("#txtIdRamo").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerRamo.action";
		var data={"ramoMovimiento.idTcRamo":id,"ramoMovimiento.codigo":""};
		jQuery.asyncPostJSON(url,data,loadInfoRamo);
	}
	jQuery("#idS").val("");
	jQuery("#subramoId").val("");
	jQuery("#subramoDescripcion").val("");
}

function onChangeIdRamo(){	
	var claveRamo = jQuery("#ramoId").val();
	if(jQuery.isValid(claveRamo)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerRamo.action";
		var data={"ramoMovimiento.codigo":claveRamo,"ramoMovimiento.idTcRamo":""};
		jQuery.asyncPostJSON(url,data,loadInfoRamo);
	}else{
		jQuery("#idR").val("");
		jQuery("#ramoId").val("");		
		jQuery("#ramoDescripcion").val("");
	}	
	jQuery("#idS").val("");
	jQuery("#subramoId").val("");
	jQuery("#subramoDescripcion").val("");
}

function loadInfoRamo(json){
	if(json.ramoMovimiento==null || json.ramoMovimiento.idTcRamo == null){
		alert("La clave del Ramo no Existe ");//ó no esta Autorizado
		jQuery("#idR").val("");
		jQuery("#ramoId").val("");		
		jQuery("#ramoDescripcion").val("");
		
	}else{
		var ramoMovimiento=json.ramoMovimiento;
		var idR=ramoMovimiento.idTcRamo;
		var ramoId=ramoMovimiento.codigo;
		var ramoDescripcion = ramoMovimiento.descripcion;
		
		jQuery("#idR").val(idR);
		jQuery("#ramoId").val(ramoId);
		jQuery("#ramoDescripcion").val(ramoDescripcion);		
	}	
}

//Subramo
function mostrarListadoSubRamos(){
	var idRamo = jQuery("#ramoId").val();
	var idSubRamo = jQuery("#subramoId").val();
	var field="txtIdSubramo";
	if(idRamo == ""){
		alert("Debe seleccionar un Ramo.");
	}else{
		if(idSubRamo == ""){
			var url="/MidasWeb/fuerzaventa/MovimientosManuales/mostrarContenedorSubRamo.action?tipoAccion=consulta&idField="+field+"&ramoMovimiento.codigo="+idRamo;
			sendRequestWindow(null, url, obtenerVentanaSubRamos);
		}else{
			var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerSubRamo.action";
			var data={"idTcSubRamo.codigoSubRamo":idSubRamo,"subRamoMovimiento.idTcSubRamo":""};
			jQuery.asyncPostJSON(url,data,loadInfoSubRamo);
		}
	}
}

function obtenerVentanaSubRamos(){	
	var wins = obtenerContenedorVentanas();
	ventanaSubRamos= wins.createWindow("subramoModal", 400, 320, 900, 450);
	ventanaSubRamos.center();
	ventanaSubRamos.setModal(true);
	ventanaSubRamos.setText("Consulta de SubRamos");
	return ventanaSubRamos;
}

function onChangeSubRamo(){
	var idRamo = jQuery("#ramoId").val();
	if(idRamo == ""){
		alert("Debe seleccionar un Ramo.");
	}else{
		var id = jQuery("#txtIdSubramo").val();
		if(jQuery.isValid(id)){
			var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerSubRamo.action";
			var data={"subRamoMovimiento.idTcSubRamo":id,"subRamoMovimiento.codigoSubRamo":""};
			jQuery.asyncPostJSON(url,data,loadInfoSubRamo);
		}
	}
}

function onChangeIdSubRamo(){
	var claveSubRamo = jQuery("#subramoId").val();
	var claveRamo = jQuery("#ramoId").val();
	if(jQuery.isValid(claveRamo) && jQuery.isValid(claveSubRamo)){
		var url="/MidasWeb/fuerzaventa/MovimientosManuales/obtenerSubRamo.action";
		var data={"ramoMovimiento.codigo":claveRamo,"subRamoMovimiento.codigoSubRamo":claveSubRamo,"subRamoMovimiento.idTcSubRamo":""};
		jQuery.asyncPostJSON(url,data,loadInfoSubRamo);
	}else{			
		jQuery("#idS").val("");
		jQuery("#subramoId").val("");
		jQuery("#subramoDescripcion").val("");	
	}
}

function loadInfoSubRamo(json){
	if(json.subRamoMovimiento==null || json.subRamoMovimiento.idTcSubRamo==null){
		alert("La clave del SubRamo no Existe");
		jQuery("#idS").val("");
		jQuery("#subramoId").val("");
		jQuery("#subramoDescripcion").val("");		
	}else{		
		var subRamoMovimiento=json.subRamoMovimiento;		
		var idS=subRamoMovimiento.idTcSubRamo;
		var subramoId=subRamoMovimiento.codigoSubRamoMovsManuales;
		var subramoDescripcion = subRamoMovimiento.descripcionSubRamo;
				
		jQuery("#idS").val(idS);
		jQuery("#subramoId").val(subramoId);
		jQuery("#subramoDescripcion").val(subramoDescripcion);		
	}
}

function guardarMovimientoManual()
{
	var msg = null;
	
	var idAgente = jQuery("#idAgente").val();
	var idConcepto = jQuery("#conceptoId").val();		
	var idDescripcion = jQuery("#descripcion").val();
	var idRamo = jQuery("#ramoId").val();
	var idSubramo = jQuery("#subramoId").val();
	var tipoMovimiento = jQuery("#tipoMovimientoId").val();
	var importeMovimiento = jQuery("#importe").val();
	var fechaMovimiento = jQuery("#fechaMovimiento").val();
	var usuarioMovimiento = jQuery("#usuario").val();
	
	if(idAgente==""){
		msg = 'Debe seleccionar un Agente.\n';
	}
	
	if(idConcepto==""){
		msg += 'Debe ingresar un Concepto.\n';
	}
	
	if(idRamo==""){
		msg += "Debe seleccionar un Ramo.\n";
	}
	
	if(idSubramo==""){
		msg += "Debe seleccionar un Subramo.\n";
	}
	
	if(tipoMovimiento==""){
		msg += "Debe seleccionar un Tipo de Movimiento.\n";
	}
	
	if(importeMovimiento==""){
		msg += "Debe ingresar un Importe.\n";
	}		
	
	if(msg!=null){
		alert(msg);
	}else{
		
	sendRequestJQ(null,'/MidasWeb/fuerzaventa/MovimientosManuales/guardar.action?movimientoManual.agente.idAgente=' + 
			idAgente + '&movimientoManual.idConcepto=' + idConcepto + '&movimientoManual.descripcionMovto=' + idDescripcion + 
			'&movimientoManual.idRamoContable=' + idRamo + '&movimientoManual.idSubramoContable=' + idSubramo + '&movimientoManual.naturalezaConcepto=' + 
			tipoMovimiento + '&movimientoManual.importeMovto=' + importeMovimiento + '&movimientoManual.idUsuarioCreacion=' + usuarioMovimiento + 
			'&movimientoManual.fechaCreacion =' + fechaMovimiento ,targetWorkArea,null);	
	}
}

function seleccionarUsuario(bandera){	
	mostrarVentanaModal("BuscarUsuario","Selecci\u00F3n de usuario",200,400, 510, 230, seleccionarUsuarioMovManualesPath+'?bandera='+bandera+
			'&busquedaPath='+buscarUsuarioMovManualesPath);	
}

function cargaUsuario(id,nombre){
	jQuery('#usuarioId').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#usuarioNombre').val(nombre);
	cerrarVentanaModal("BuscarUsuario");
}

function buscarMovimientosManuales()
{	
	var idUsuario = jQuery('#usuarioId').val();
	var fechaInicioPeriodo = jQuery('#fechaInicio').val();
	var fechaFinPeriodo = jQuery('#fechaFin').val();
	jQuery("#saldo").val("");
	movimientosManualesGrid = listarFiltradoGenerico(buscarMovimientosManualesPath+'?movimientoManual.idUsuarioCreacion='+idUsuario+'&movimientoManual.fechaInicioPeriodo='
			+fechaInicioPeriodo+'&movimientoManual.fechaFinPeriodo='+fechaFinPeriodo, 'movimientosManualesGrid', null,null,null);
	
	
	movimientosManualesGrid.attachEvent("onCheck", function(rId,cInd,state)
    {
		actualizarSaldo(rId,state,false);
		   	
    });
	
	movimientosManualesGrid.attachEvent("customMasterChecked",function(val){
		actualizarSaldo(null,null,true);
	})

	
	/*
	movimientosManualesGrid.attachEvent("onXLE", function(grid){
		var saldo = 0;
		grid.forEachRow(function(row_id){ // function that gets id of the row as an incoming argument
            // here id - id of the row 
			var importe = Number(grid.cellById(row_id, 8).getValue());
			if(grid.cellById(row_id, 7).getValue()== 'C')
			{
				importe = importe * -1;				
			}
         saldo = saldo + importe;
    })
    
    jQuery('#saldo').val(formatCurrency(saldo));		
    });		*/
}

function aplicarMovimientos()
{
	var elementosSeleccionados = movimientosManualesGrid.getCheckedRows(0);	
//	var saldo = Number(removeCurrency(jQuery('#saldo').val()));
	var saldo = Number(removeCurrencyMovMan(jQuery('#saldo').val()));
	var cifraControl = Number(jQuery('#cifraControl').val());
	
	if(elementosSeleccionados == null || elementosSeleccionados.length == 0)
	{
		alert("Debe seleccionar al menos un movimiento.")
	}else if(cifraControl == null || saldo != cifraControl)
	{
		alert("La cifra de control no cuadra con la suma del importe de los movimientos seleccionados.");
		
	}else
	{				
		var idUsuario = jQuery('#usuarioId').val();
		var fechaInicioPeriodo = jQuery('#fechaInicio').val();
		var fechaFinPeriodo = jQuery('#fechaFin').val();
		
		if(confirm("\u00BFEst\u00e1 seguro que desea Aplicar los movimientos seleccionados \u003F "))
		{
			sendRequestJQ(null,aplicarMovimientosManualesPath + '?idsSeleccionados=' + elementosSeleccionados + '&movimientoManual.idUsuarioCreacion='+idUsuario+
					'&movimientoManual.fechaInicioPeriodo='+fechaInicioPeriodo+'&movimientoManual.fechaFinPeriodo='+fechaFinPeriodo,targetWorkArea,null);		
		}				
	}	
}

function eliminarMovimientos()
{
	var elementosSeleccionados = movimientosManualesGrid.getCheckedRows(0);

	if(elementosSeleccionados == null || elementosSeleccionados.length == 0)
	{
		alert("Debe seleccionar al menos un movimiento.")
	}else
	{		
		var idUsuario = jQuery('#usuarioId').val();
		var fechaInicioPeriodo = jQuery('#fechaInicio').val();
		var fechaFinPeriodo = jQuery('#fechaFin').val();
		
		if(confirm("\u00BFEst\u00e1 seguro que desea Eliminar los movimientos seleccionados \u003F "))
		{
			sendRequestJQ(null,eliminarMovimientosManualesPath + '?idsSeleccionados=' + elementosSeleccionados + '&movimientoManual.idUsuarioCreacion='+idUsuario+
					'&movimientoManual.fechaInicioPeriodo='+fechaInicioPeriodo+'&movimientoManual.fechaFinPeriodo='+fechaFinPeriodo,targetWorkArea,null);			
		}		
	}		
}

function actualizarSaldo(rId,state,isMasterCheckbox)
{
	var saldo = 0;	
	
	if(isMasterCheckbox) // todos los registros
	{
		grid.forEachRow(function(row_id){
           
			var importe = Number(grid.cellById(row_id, 8).getValue());
			if(grid.cellById(row_id, 7).getValue()== 'C')
			{
				importe = importe * -1;				
			}
			
			if(grid.cellById(row_id, 0).getValue() == true)
			{
				saldo = saldo + importe;				
			}	        
    })    	
		
	}else // registro individual
	{	
//		saldo = Number(removeCurrency(jQuery('#saldo').val()));
		saldo = Number(removeCurrencyMovMan(jQuery('#saldo').val()));		
		
		var importe = Number(movimientosManualesGrid.cellById(rId, 8).getValue());
		if(movimientosManualesGrid.cellById(rId, 7).getValue()== 'C')
		{
			importe = importe * -1;				
		}		
		
		if(state) //seleccionado 
		{
			saldo = saldo + importe;			
		}else //deseleccionado
		{
			saldo = saldo - importe;
		}		
	}
	
	jQuery('#saldo').val(formatCurrency(saldo));
}

function masterCheckboxEvent()
{
	alert("test");	
}

function removeCurrencyMovMan(text){
	var strMinus = "";
	if(text!=null){
		objRegExp = /\)|\(|[,]/g;
		text = text.replace(objRegExp, "");
		var esNegativo = text.indexOf("-")!=-1;
		if(esNegativo==true){
			text= text.replace('-', '');
			var strMinus = "-";
		}
		var esFormatoNumerico = text.indexOf("$")!=-1
		if (esFormatoNumerico == true) {
			text= text.replace('$', '');
		}
	}
	return strMinus + text;
}



