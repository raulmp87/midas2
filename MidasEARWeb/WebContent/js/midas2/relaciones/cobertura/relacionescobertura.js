/**
 * Relaciones de cobertura
 */
var gruposVariablesAsociadosGrid;
var gruposVariablesDisponiblesGrid;	
var gruposVariablesProcessor;

/**
 * Grupos de Variables de Modificacion de Prima 
 */

function obtenerGruposVariablesAsociados(){
	
	gruposVariablesAsociadosGrid = new dhtmlXGridObject('gruposVariablesAsociadosGrid');
	
	gruposVariablesAsociadosGrid.load(obtenerGruposVariablesAsociadosPath + "?idCobertura=" + dwr.util.getValue("idCobertura") + "&idSeccion=" + dwr.util.getValue("idSeccion"));
	
	//Creacion del DataProcessor
	gruposVariablesProcessor = new dataProcessor(accionSobreGruposVariablesAsociadosPath + "?idCobertura=" + dwr.util.getValue("idCobertura") + "&idSeccion=" + dwr.util.getValue("idSeccion"));

	gruposVariablesProcessor.enableDataNames(true);
	gruposVariablesProcessor.setTransactionMode("POST");
	gruposVariablesProcessor.setUpdateMode("cell");
	
	gruposVariablesProcessor.attachEvent("onAfterUpdate",refrescarGridsGruposVariables);
	
	gruposVariablesProcessor.init(gruposVariablesAsociadosGrid);
}


function obtenerGruposVariablesDisponibles(){
	
	gruposVariablesDisponiblesGrid = new dhtmlXGridObject('gruposVariablesDisponiblesGrid');
	
	gruposVariablesDisponiblesGrid.load(obtenerGruposVariablesDisponiblesPath + "?idCobertura=" + dwr.util.getValue("idCobertura") + "&idSeccion=" + dwr.util.getValue("idSeccion"));
}

function iniciaGridsGruposVariables() {
	refrescarGridsGruposVariables(null,null,null,null);
}


function refrescarGridsGruposVariables(sid,action,tid,node){
//	var tipoRespuesta = node.getAttribute("type");
//	var mensajeError = node.firstChild.data;
//	var tipoMensaje = node.getAttribute("tipoMensaje");
//	
//	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
//		mostrarVentanaMensaje(tipoMensaje, mensajeError);
//	}
	obtenerGruposVariablesAsociados();
	obtenerGruposVariablesDisponibles();
	return true; 
}



