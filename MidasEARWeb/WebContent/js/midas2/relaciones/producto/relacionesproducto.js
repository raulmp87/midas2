/**
 * Relaciones de producto
 */
var formasPagoAsociadasGrid;
var formasPagoDisponiblesGrid;
var formasPagoProcessor;
var mediosPagoAsociadasGrid;
var mediosPagoDisponiblesGrid;
var mediosPagoProcessor;

/**
 * Formas de Pago 
 */

function obtenerFormasPagoAsociadas(){
	
	formasPagoAsociadasGrid = new dhtmlXGridObject('formasPagoAsociadasGrid');
	
	formasPagoAsociadasGrid.load(obtenerFormasPagoAsociadasPath + "?idProducto=" + dwr.util.getValue("idProducto"));
	
	//Creacion del DataProcessor
	formasPagoProcessor = new dataProcessor(accionSobreFormasPagoAsociadasPath + "?idProducto=" + dwr.util.getValue("idProducto"));

	formasPagoProcessor.enableDataNames(true);
	formasPagoProcessor.setTransactionMode("POST");
	formasPagoProcessor.setUpdateMode("cell");
	
	formasPagoProcessor.attachEvent("onAfterUpdate",refrescarGridsFormaPago);
	
	formasPagoProcessor.init(formasPagoAsociadasGrid);
}


function obtenerFormasPagoDisponibles(){
	
	formasPagoDisponiblesGrid = new dhtmlXGridObject('formasPagoDisponiblesGrid');
	
	formasPagoDisponiblesGrid.load(obtenerFormasPagoDisponiblesPath + "?idProducto=" + dwr.util.getValue("idProducto"));
}

function iniciaGridsFormaPago() {
	refrescarGridsFormaPago(null,null,null,null);
}


function refrescarGridsFormaPago(sid,action,tid,node){
//	var tipoRespuesta = node.getAttribute("type");
//	var mensajeError = node.firstChild.data;
//	var tipoMensaje = node.getAttribute("tipoMensaje");
//	
//	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
//		mostrarVentanaMensaje(tipoMensaje, mensajeError);
//	}
	obtenerFormasPagoAsociadas();
	obtenerFormasPagoDisponibles();
	return true; 
}



/**
 * Medios de Pago 
 */

	

function obtenerMediosPagoAsociadas(){
	
	mediosPagoAsociadasGrid = new dhtmlXGridObject('mediosPagoAsociadasGrid');
	
	mediosPagoAsociadasGrid.load(obtenerMediosPagoAsociadasPath + "?idProducto=" + dwr.util.getValue("idProducto"));
	
	//Creacion del DataProcessor
	mediosPagoProcessor = new dataProcessor(accionSobreMediosPagoAsociadasPath + "?idProducto=" + dwr.util.getValue("idProducto"));

	mediosPagoProcessor.enableDataNames(true);
	mediosPagoProcessor.setTransactionMode("POST");
	mediosPagoProcessor.setUpdateMode("cell");
	
	mediosPagoProcessor.attachEvent("onAfterUpdate",refrescarGridsMedioPago);
	
	mediosPagoProcessor.init(mediosPagoAsociadasGrid);
}


function obtenerMediosPagoDisponibles(){
	
	mediosPagoDisponiblesGrid = new dhtmlXGridObject('mediosPagoDisponiblesGrid');
	
	mediosPagoDisponiblesGrid.load(obtenerMediosPagoDisponiblesPath + "?idProducto=" + dwr.util.getValue("idProducto"));
}

function iniciaGridsMedioPago() {
	refrescarGridsMedioPago(null,null,null,null);
}


function refrescarGridsMedioPago(sid,action,tid,node){
//	var tipoRespuesta = node.getAttribute("type");
//	var mensajeError = node.firstChild.data;
//	var tipoMensaje = node.getAttribute("tipoMensaje");
//	
//	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
//		mostrarVentanaMensaje(tipoMensaje, mensajeError);
//	}
	obtenerMediosPagoAsociadas();
	obtenerMediosPagoDisponibles();
	return true; 
}
