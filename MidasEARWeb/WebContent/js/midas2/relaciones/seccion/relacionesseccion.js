/**
 * Relaciones de seccion
 */
var paquetesAsociadosGrid;
var paquetesDisponiblesGrid;	
var paquetesProcessor;

/**
 * Paquetes 
 */

function obtenerPaquetesAsociados(){
	
	paquetesAsociadosGrid = new dhtmlXGridObject('paquetesAsociadosGrid');
	
	paquetesAsociadosGrid.load(obtenerPaquetesAsociadosPath + "?idSeccion=" + dwr.util.getValue("idSeccion"));
	
	//Creacion del DataProcessor
	paquetesProcessor = new dataProcessor(accionSobrePaquetesAsociadosPath + "?idSeccion=" + dwr.util.getValue("idSeccion"));

	paquetesProcessor.enableDataNames(true);
	paquetesProcessor.setTransactionMode("POST");
	paquetesProcessor.setUpdateMode("cell");
	
	paquetesProcessor.attachEvent("onAfterUpdate",refrescarGridsPaquetes);
	
	paquetesProcessor.init(paquetesAsociadosGrid);
}


function obtenerPaquetesDisponibles(){
	
	paquetesDisponiblesGrid = new dhtmlXGridObject('paquetesDisponiblesGrid');
	
	paquetesDisponiblesGrid.load(obtenerPaquetesDisponiblesPath + "?idSeccion=" + dwr.util.getValue("idSeccion"));
}

function iniciaGridsPaquetes() {
	refrescarGridsPaquetes(null,null,null,null);
}


function refrescarGridsPaquetes(sid,action,tid,node){
//	var tipoRespuesta = node.getAttribute("type");
//	var mensajeError = node.firstChild.data;
//	var tipoMensaje = node.getAttribute("tipoMensaje");
//	
//	if(mensajeError != null && mensajeError != undefined && trim(mensajeError) != ""){
//		mostrarVentanaMensaje(tipoMensaje, mensajeError);
//	}
	obtenerPaquetesAsociados();
	obtenerPaquetesDisponibles();
	return true; 
}



