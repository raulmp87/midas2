function datosValidos() {
	
	if (!jQuery( "input[id^='datepicker']" ).val()) {
		
		alert("Favor de ingresar Periodo");
		
		return false;
	}
	
	return true;
	
}


function exportarReporteExcel(url) {
	
	downloadFileWithoutBlocking(url+'?' + jQuery('#reporte').serialize());

	//downloadFileWithBlocking(url+'?' + jQuery('#reporte').serialize());
	
}


function exportarDetalleExcel() {
	
	if (datosValidos()) {
		exportarReporteExcel(urlExportarDetalleExcel);
	}
	
}

function exportarResumenExcel() {
	
	if (datosValidos()) {
		exportarReporteExcel(urlExportarResumenExcel);
	}
	
}
