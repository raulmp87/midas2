function cleanInput(id) {
	jQuery('#' + id).val('');
}
function cleanInputDiv(id) {
	jQuery('#' + id).text('');
}

function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}

/**Revisamos que las fechas sean correctas y se haya elegido un modo**/
function compareDates(){
	
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	modo=jQuery("#modo").val();
	
	if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
		mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
	}
	else{
		
		if (compare_dates(fecha1, fecha2)){  
	    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
	    }
		else{
	    	  this.generarReporte();
	    }
	}
}

function generarInformacion(){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	var location ="/MidasWeb/reportes/reporteHistPolizas/generarInformacion.action?fechaInicial=" + fecha1 + "&fechaFinal=" + fecha2;
	window.open(location, "ReporteSesas");
	
}

/**
 * Generamos los archivos .txt 
 */
function generarReporte(){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	sendRequestJQ(document.reporteSiniestrosForm, "/MidasWeb/reportes/reporteHistPolizas/generarReporte.action?fechaInicial=" + fecha1 + "&fechaFinal=" + fecha2, null, 'generarInformacion()');

}
