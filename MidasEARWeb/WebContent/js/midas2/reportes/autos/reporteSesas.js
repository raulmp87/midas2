/**
 * 
 */

function cleanInput(id) {
	jQuery('#' + id).val('');
}
function cleanInputDiv(id) {
	jQuery('#' + id).text('');
}

function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}

/**
 * Traemos las ultimas tareas en la DB
 */
function iniciaReporteSesas(claveNeg){
	document.getElementById("reporteSesasGrid").innerHTML = '';
	var reporteSesasGrid = new dhtmlXGridObject('reporteSesasGrid');
	mostrarIndicadorCarga('indicador');	
	reporteSesasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
	if(claveNeg==1 || claveNeg=='V')
    {
		claveNeg="V"
    }
	else
	{
		claveNeg="A"
	}
	
	var url = "/MidasWeb/reportes/reporteSesas/buscarTareasSesas.action?claveNeg="+claveNeg;
	reporteSesasGrid.load(url);
}

/**Revisamos que las fechas sean correctas y se haya elegido un modo**/
function compareDates(claveNeg){
	
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
		mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
	}
	else{
		
		if (compare_dates(fecha1, fecha2)){  
	    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
	    }
		else{
			  //alert('claveNeg: ' + claveNeg)
	    	  this.generarInformacion(claveNeg);
	    }
	}
}

/**Revisamos que las fechas sean correctas y se haya elegido un modo**/
function compareDatesVida(claveNeg){
	if(confirm("Seguro que desear programar la ejecucion de Sesas?")){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
		mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
	}
	else{
		
		if (compare_dates(fecha1, fecha2)){  
	    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
	    }
		else{
			  //alert('claveNeg: ' + claveNeg)
	    	  this.generarInformacion(claveNeg);
	    }
	}
	}
}

/**
 * Enviamos a habilitar el job para que genere la informacion durante la noche
 */	
function generarInformacion(claveNeg){   
   if (claveNeg=='V')
   {	 
	   sendRequestJQ(document.reporteSesasForm, '/MidasWeb/reportes/reporteSesas/generarInformacion.action?claveNeg='+claveNeg, 'contenido', 'iniciaReporteSesas(1)' );
   }
   else
   {
	   sendRequestJQ(document.reporteSesasForm, '/MidasWeb/reportes/reporteSesas/generarInformacion.action?claveNeg='+claveNeg, 'contenido', 'iniciaReporteSesas(2)' );  
   }

}

         
function generaArchivosSesas(){ 
	sendRequestJQ(document.reporteSesasForm, '/MidasWeb/reportes/reporteSesas/generaArchivosSesas.action?claveNeg='+'V', 'contenido', 'iniciaReporteSesas(1)' ); 
}

function generarReporte(idReporteSesas,claveNeg){
	var listaSubRamos = document.getElementsByName('subramos');
	var varsubRamosSel = null; 
	var totSubRamos = listaSubRamos.length;
	
	var listaTipoArchivo = document.getElementsByName('tipoarchi');
	var listaTipoArchivoSel = null; 
	var totTipArch = listaTipoArchivo.length;
	
	var i=0;
	var j=0;
	var archivosSesas = '';
	var valorId = '';
	var valorTipo = '';
	var modo="ORI";

	
	while (i<totTipArch){
		if (listaTipoArchivo[i].checked){
			listaTipoArchivoSel = true;
			valorTipo=listaTipoArchivo[i].value;
			while (j<totSubRamos){
				if (listaSubRamos[j].checked){
					varsubRamosSel = true; 
					valorId= ( (valorTipo * totSubRamos)-totSubRamos + (parseInt(listaSubRamos[j].value)));
					if(claveNeg=='V')
						valorId=valorId+100;
					archivosSesas = archivosSesas + valorId +',';
				}
				j++;
			}
			j=0;
		}
		i++;
	}
	i=0;
	var n = archivosSesas.length-1;
	archivosSesas = archivosSesas.substring(0, n);
	if (varsubRamosSel&&listaTipoArchivoSel){
			if(confirm("Seguro que desear generar reporte en el modo elegido?")){
				var location ="/MidasWeb/reportes/reporteSesas/generarReporte.action?modo="+modo+"&idGeneracion=" + idReporteSesas +"&listArchivos=" + archivosSesas + "&claveNeg=" + claveNeg  ;
				window.open(location, "ReporteSesas" );
			}
	}
	else{
		if (listaTipoArchivoSel){
			mostrarMensajeInformativo("Seleccione un SubRamo", 10, null);
		}
		else{
			mostrarMensajeInformativo("Seleccione un Tipo de Archivo", 10, null);
			
		}

	}
	
}

function generarReporteVida(){
	var listaSubRamos = document.getElementsByName('subramos');
	var varsubRamosSel = null; 
	var totSubRamos = listaSubRamos.length;
	
	var listaTipoArchivo = document.getElementsByName('tipoarchi');
	var listaTipoArchivoSel = null; 
	var totTipArch = listaTipoArchivo.length;
	
	var i=0;
	var j=0;
	var archivosSesas = '';
	var valorId = '';
	var valorTipo = '';
	var modo="ORI";


	
	while (i<totTipArch){
		if (listaTipoArchivo[i].checked){
			listaTipoArchivoSel = true;
			valorTipo=listaTipoArchivo[i].value;
			while (j<totSubRamos){
				if (listaSubRamos[j].checked){
					varsubRamosSel = true; 
					valorId= ( (valorTipo * totSubRamos)-totSubRamos + (parseInt(listaSubRamos[j].value)));
					valorId=valorId+100;
					archivosSesas = valorId;
				}
				j++;
			}
			j=0;
		}
		i++;
	}
	i=0;
	if (varsubRamosSel&&listaTipoArchivoSel){
		if(confirm("Seguro que desear generar reporte en el modo elegido?")){
			var location ="/MidasWeb/reportes/reporteSesas/generarReporteVida.action?&listArchivos=" + archivosSesas;
			window.open(location, "ReporteSesas" );
			
		}
	}
	else{
		if (listaTipoArchivoSel){
			mostrarMensajeInformativo("Seleccione un SubRamo", 10, null);
		}
		else{
			mostrarMensajeInformativo("Seleccione un Tipo de Archivo", 10, null);
			
		}

	}
	
}

function generarMatriz(idReporteSesas){
			if(confirm("Desea descargar la matriz de validacion?")){
				var location ="/MidasWeb/reportes/reporteSesas/generarMatriz.action?idGeneracion=" + idReporteSesas ;
				window.open(location, "ReporteSesas" );
			}
		
}


/**
 * Generamos los archivos .txt 
 */
function generarReporte3(){
	
	modo=jQuery("#modo").val();
	numArchivo=jQuery("#numArchivo").val();
	alert(modo);
	if ( this.validaModo(modo) && this.validaNumArchivo(numArchivo) ){
		if(confirm("Seguro que desear generar reporte en el modo elegido?")){
			var location ="/MidasWeb/reportes/reporteSesas/generarReporte.action?modo=" + modo + "&numArchivo=" + numArchivo;
			window.open(location, "ReporteSesas" + modo);
		}

	}

}

/**
 * Valida el modo de generar el reporte
 * @param modo
 * @returns {Boolean}
 */
function validaModo(modo){
	
	if ( modo != null && modo != ''){
		return true;
	}
	else{
		mostrarMensajeInformativo("Seleccione Modo.", 10, null);
		return false;
	}
	
}

/**
 * Valida el numero de Archivo
 * @param modo
 * @returns {Boolean}
 */
function validaNumArchivo(numArchivo){
	
	if ( numArchivo != null && numArchivo != ''){
		return true;
	}
	else{
		mostrarMensajeInformativo("Seleccione Número de Archivo.", 10, null);
		return false;
	}
	
}