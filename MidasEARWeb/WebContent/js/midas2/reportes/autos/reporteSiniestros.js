/**
 * 
 */

function cleanInput(id) {
	jQuery('#' + id).val('');
}
function cleanInputDiv(id) {
	jQuery('#' + id).text('');
}

function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}

/**
 * Traemos las ultimas tareas en la DB
 */

function mostrarReporteSiniestrosAutos(){
	sendRequest(null,'/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do?nombrePlantilla=ReporteSiniestroConReservaPendienteAnexo','contenido','crearCalendario()');
}


/**Revisamos que las fechas sean correctas y se haya elegido un modo**/
function compareDates(){
	
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	modo=jQuery("#modo").val();
	
	if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
		mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
	}
	else{
		
		if (compare_dates(fecha1, fecha2)){  
	    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
	    }
		else{
	    	  this.generarReporte();
			
	    }
	}
}


function compareDatesV(option){
	
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	modo=jQuery("#modo").val();
	
	if(option == "1"){
		
		if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
			mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
		}
		else{
			
			if (compare_dates(fecha1, fecha2)){  
		    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
		    }
			else{
				this.programarTarea(1);
		    }
		}
	}else{
		this.generarInformacionVigor();
	}
	
	
}


function compareDatesAutos(option){
	
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	modo=jQuery("#modo").val();
	
	if(option == "1"){
		
		if ( fecha1 == null || fecha1 == '' || fecha2== null || fecha2 == ''){
			mostrarMensajeInformativo("Una o las dos fecha son vacías. Verificar.", 10, null);
		}
		else{
			
			if (compare_dates(fecha1, fecha2)){  
		    	mostrarMensajeInformativo("La fecha inicial no debe ser mayor a la fecha final. Verificar.", 10, null); 
		    }
			else{
					this.programarTarea(2);
		    }
		}
	}else				
		this.generarInformacionAutos();
	
}

/**
 * 
 */
function generarInformacion(){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	var location ="/MidasWeb/reportes/reporteSiniestros/generarInformacion.action?fechaInicial=" + fecha1 + "&fechaFinal=" + fecha2;
	window.open(location, "ReporteSesas");
	
}

/**
 * Traemos las ultimas tareas en la DB
 */
function iniciaReporteReservas(option){
	document.getElementById("reporteSesasGrid").innerHTML = '';
	var reporteSesasGrid = new dhtmlXGridObject('reporteSesasGrid');
	mostrarIndicadorCarga('indicador');	
	reporteSesasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var url = "/MidasWeb/reportes/reporteVigor/buscarTareasVigor.action?option="+option;
	reporteSesasGrid.load(url);
}

/**
 * Generamos los archivos .txt 
 */
function generarReporte(){
		generarInformacion();
}

/**
 * 
 */
function generarInformacionVigor(){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	var location ="/MidasWeb/reportes/reporteVigor/generarInformacion.action?fechaInicial=" + fecha1 + "&fechaFinal=" + fecha2;
	window.open(location, "ReporteSesas");
	
}

/**
 * Generamos los archivos .txt 
 */
function generarReporteVigor(){
	sendRequestJQ(document.reporteVigorForm, '/MidasWeb/reportes/reporteVigor/generarReporte.action', null, 'generarInformacionVigor()');

}


/**
 * Generamos los archivos .txt 
 */
function generarReporteAutos(){
	sendRequestJQ(document.reporteAutosNZForm, '/MidasWeb/reportes/reporteAutosNZ/generarReporte.action', null, 'generarInformacionAutos()');
}

function generarInformacionAutos(){
	fecha1=jQuery("#fechaInicial").val();
	fecha2=jQuery("#fechaFinal").val();
	
	var location ="/MidasWeb/reportes/reporteAutosNZ/generarInformacion.action?fechaInicial=" + fecha1 + "&fechaFinal=" + fecha2;
	window.open(location, "ReporteSesas");
	
}

function programarTarea(option){
	if(option == 1)
		sendRequestJQ(document.reporteVigorForm, '/MidasWeb/reportes/reporteVigor/programarTarea.action?option='+option, null, 'programarTareaOK()');
	else if(option == 3)
		sendRequestJQ(document.reporteSiniestrosForm, '/MidasWeb/reportes/reporteVigor/programarTarea.action?option='+option, null, 'programarTareaOK()');
	else
		sendRequestJQ(document.reporteAutosNZForm, '/MidasWeb/reportes/reporteVigor/programarTarea.action?option='+option, null, 'programarTareaOK()');
	
	iniciaReporteReservas(option);
}

function programarTareaOK(){
	alert("La tarea se ha programado");
}

