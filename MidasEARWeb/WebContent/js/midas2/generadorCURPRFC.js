var PersonaFisicaRFC	    = 0;
var PersonaMoralRFC		    = 1;
var HomoclaveRFC			= 2;
var nombre, s,apePat, apeMat, curp, cadena, y, caracter1,caracter2,indice, palabra, edos, caracter3,letra, edo, sex,i, consonante1, consonante2, consonante3, strLetra1, strLetra2, strLetra3, strLetra4, strClvAlf, strComplet, strRegistro;

 var aDelPerMoral=new Array(69);
 var aDelApe=new Array(40);
 var estados=new Array(32); 
 var gender=new Array(2); 

 var ANT=new Array(45); 
 var conso;

 estados["01000"]="AS"; estados["02000"]="BC"; estados["03000"]="BS"; estados["04000"]="CC"; estados["05000"]="CL"; estados["06000"]="CM"; estados["07000"]="CS"; 
 estados["08000"]="CH"; estados["09000"]="DF";  estados["10000"]="DG"; estados["11000"]="GT"; estados["12000"]="GR"; estados["13000"]="HG"; estados["14000"]="JC"; 
 estados["15000"]="MC"; estados["16000"]="MN"; estados["17000"]="MS"; estados["18000"]="NT"; estados["19000"]="NL";  estados["20000"]="OC"; estados["21000"]="PL";
 estados["22000"]="QR";  estados["23000"]="QT"; estados["24000"]="SP"; estados["25000"]="SL"; estados["26000"]="SR"; estados["27000"]="TC"; estados["28000"]="TS"; 
 estados["29000"]="TL"; estados["30000"]="VZ"; estados["31000"]="YN"; estados["32000"]="ZS";

gender["M"]="H";
gender["F"]="M";
 
ANT[0]="BUEI"; ANT[1]="BUEY"; ANT[2]="CACA"; ANT[3]="CACO"; ANT[4]="CAGA"; ANT[5]="CAGO"; ANT[6]="CAKA";
ANT[7]="CAKO"; ANT[8]="COGE"; ANT[9]="COGA"; ANT[10]="COJE"; ANT[11]="COJI"; ANT[12]="COJO"; ANT[13]="CULO";
ANT[14]="FETO"; ANT[15]="GUEY"; ANT[16]="JOTO"; ANT[17]="KACA"; ANT[18]="KAKO"; ANT[19]="KAGA"; ANT[20]="KAGO";
ANT[21]="KOGE"; ANT[22]="KOJO"; ANT[23]="KAKA"; ANT[24]="KULO"; ANT[25]="LOCA"; ANT[26]="LOCO"; ANT[27]="LOKA";
ANT[28]="LOKO"; ANT[29]="MAME"; ANT[30]="MAMO"; ANT[31]="MEAR"; ANT[32]="MEAS"; ANT[33]="MEON"; ANT[34]="MION";
ANT[35]="MOCO"; ANT[36]="MULA"; ANT[37]="PEDA"; ANT[38]="PEDO"; ANT[39]="PENE"; ANT[40]="PUTA"; ANT[41]="PUTO";
ANT[42]="QULO"; ANT[43]="RATA"; ANT[44]="RUIN";

var codigo=new Array(34);
codigo[0]="1"; codigo[1]="2"; codigo[2]="3"; codigo[3]="4"; codigo[4]="5"; codigo[5]="6"; codigo[6]="7";
codigo[7]="8"; codigo[8]="9"; codigo[9]="A"; codigo[10]="B"; codigo[11]="C"; codigo[12]="D"; codigo[13]="E";
codigo[14]="F"; codigo[15]="G"; codigo[16]="H"; codigo[17]="I"; codigo[18]="J"; codigo[19]="K"; codigo[20]="L";
codigo[21]="M"; codigo[22]="N"; codigo[23]="P"; codigo[24]="Q";  codigo[25]="R"; codigo[26]="S"; codigo[27]="T";
codigo[28]="U"; codigo[29]="V"; codigo[30]="W"; codigo[31]="X"; codigo[32]="Y"; codigo[33]="Z";   

//Arreglo para la persona moral
aDelPerMoral[1] = " SA DE CV";
aDelPerMoral[2] = " S.A. DE C.V.";
aDelPerMoral[3] = " S.A DE C.V";
aDelPerMoral[4] = " S EN C";
aDelPerMoral[5] = " S EN C POR A";
aDelPerMoral[6] = " S DE RL";
aDelPerMoral[7] = " S DE RL DE IP";
aDelPerMoral[8] = " SNC";
aDelPerMoral[9] = " SNC ";
aDelPerMoral[10] = " SOFOL";
aDelPerMoral[11] = " SOFOL ";
aDelPerMoral[12] = " AFORE";
aDelPerMoral[13] = " AFORE ";
aDelPerMoral[14] = " SIEAFORE";
aDelPerMoral[15] = " SIEAFORE ";
aDelPerMoral[16] = " S DE RL MI";
aDelPerMoral[17] = " SA";
aDelPerMoral[18] = " SA ";
aDelPerMoral[19] = " CV";
aDelPerMoral[20] = " CV ";
aDelPerMoral[21] = " AC";
aDelPerMoral[22] = " AC ";
aDelPerMoral[23] = " SC";
aDelPerMoral[24] = " SC ";
aDelPerMoral[25] = " AF";
aDelPerMoral[26] = " AF ";
aDelPerMoral[27] = " AP";
aDelPerMoral[28] = " AP ";
aDelPerMoral[29] = " AR";
aDelPerMoral[30] = " AR ";
//se agregan las excepciones de articulos y preposiciones para la generacion correcta del rfc para persona moral
aDelPerMoral[31] = " Y ";
aDelPerMoral[32] = " DE ";
aDelPerMoral[33] = " DEL ";
aDelPerMoral[34] = " . ";
aDelPerMoral[35] = " LA ";
aDelPerMoral[36] = " LOS ";
aDelPerMoral[37] = " LAS ";
aDelPerMoral[38] = " MC ";
aDelPerMoral[39] = " MAC ";
aDelPerMoral[40] = " VON ";
aDelPerMoral[41] = " VAN ";
aDelPerMoral[42] = " DA ";
aDelPerMoral[43] = " DI ";
aDelPerMoral[44] = " DELGI ";
aDelPerMoral[45] = " DELLA ";
aDelPerMoral[46] = " D ";
aDelPerMoral[47] = " DES ";
aDelPerMoral[48] = " DU ";
aDelPerMoral[49] = " VANDEN ";
aDelPerMoral[50] = " VANDER ";
aDelPerMoral[51] = " CON ";
aDelPerMoral[52] = " A ";
aDelPerMoral[53] = " ANTE ";
aDelPerMoral[54] = " BAJO ";
aDelPerMoral[55] = " CABE ";
aDelPerMoral[56] = " CONTRA ";
aDelPerMoral[57] = " DESDE ";
aDelPerMoral[58] = " EN ";
aDelPerMoral[59] = " ENTRE ";
aDelPerMoral[60] = " HACIA ";
aDelPerMoral[61] = " HASTA ";
aDelPerMoral[62] = " PARA ";
aDelPerMoral[63] = " POR ";
aDelPerMoral[64] = " PARA ";
aDelPerMoral[65] = " SEGUN ";
aDelPerMoral[66] = " SIN ";
aDelPerMoral[67] = " SI ";
aDelPerMoral[68] = " SOBRE ";
aDelPerMoral[69] = " TRAS ";



aDelApe[1] = " DE ";
aDelApe[2] = " DEL ";
aDelApe[3] = " LA ";
aDelApe[4] = " LOS ";
aDelApe[5] = " Y ";
aDelApe[6] = " . ";
aDelApe[7] = " LAS ";
aDelApe[8] = " MC ";
aDelApe[9] = " MAC ";
aDelApe[10] = " VON ";
aDelApe[11] = " VAN ";
aDelApe[12] = " DA ";
aDelApe[13] = " DI ";
aDelApe[14] = " DEGLI ";
aDelApe[15] = " DELLA ";
aDelApe[16] = " D ";
aDelApe[17] = " DES ";
aDelApe[18] = " DU ";
aDelApe[19] = " VANDEN ";
aDelApe[20] = " VANDER ";
aDelApe[21] = ". ";
aDelApe[22] = " CON ";
aDelApe[23] = " A ";
aDelApe[24] = " ANTE ";
aDelApe[25] = " BAJO ";
aDelApe[26] = " CABE ";
aDelApe[27] = " CONTRA ";
aDelApe[28] = " DESDE ";
aDelApe[29] = " EN ";
aDelApe[30] = " ENTRE ";
aDelApe[31] = " HACIA ";
aDelApe[32] = " HASTA ";
aDelApe[34] = " PARA ";
aDelApe[35] = " POR ";
aDelApe[36] = " SEGUN ";
aDelApe[37] = " SIN ";
aDelApe[38] = " SI ";
aDelApe[39] = " SOBRE ";
aDelApe[40] = " TRAS ";

 strLetra1 = "";
 strLetra2 = "";
 strLetra3 = "";
 strLetra4 = "";
 strClvAlf = "";
 strComplet = "";
 strRegistro = "";

  function midasFormatRFC(vName,vFatherLastName,vMotherLastName,vDateBirth,receptSiglas,receptFecha,receptHom) {
	  
	  if(vName!==null && vName!=="" &&
	     vFatherLastName!==null && vFatherLastName!=="" &&
	     vMotherLastName!==null && vMotherLastName!=="" &&
	     vDateBirth!==null && vDateBirth!==""){
		  var rfcGenerated = midasGenerateRFC(vName, vFatherLastName, vMotherLastName, vDateBirth);		  
		  dwr.util.setValue(receptSiglas,rfcGenerated.substring(0, 4));
		  dwr.util.setValue(receptFecha,rfcGenerated.substring(4, 10));
		  dwr.util.setValue(receptHom,rfcGenerated.substring(10, rfcGenerated.length));
	  }	 
	}
  /**
   * Funcion para obtener el RFC por medio de los datos personales
   * @param vName Nombre
   * @param vFatherLastName Apellido Paterno
   * @param vMotherLastName Apellido Mterno
   * @param vDateBirth Fecha de nacimiento
   * @returns RFC
   */
  function getMidasRFCByFields(vName,vFatherLastName,vMotherLastName,vDateBirth){
	  var rfcGenerated=null;
	  if(vName!==null && vName!=="" && vFatherLastName!==null && vFatherLastName!=="" && vMotherLastName!==null && vMotherLastName!=="" && vDateBirth!==null && vDateBirth!==""){
		  rfcGenerated= midasGenerateRFC(vName, vFatherLastName, vMotherLastName, vDateBirth);		  
	  }
	  return rfcGenerated;
  }
 /**
  * Calcula el RFC por medio de los campos y lo setea en un campo[field].
  * @param vName
  * @param vFatherLastName
  * @param vMotherLastName
  * @param vDateBirth
  * @param field
  */
  function midasFormatRFCInOneField(vName,vFatherLastName,vMotherLastName,vDateBirth,field) {
	  var RFC=getMidasRFCByFields(vName, vFatherLastName, vMotherLastName, vDateBirth);
	  dwr.util.setValue(field,RFC);
  }

  function midasGenerateRFC(nameId, lastNameId, motherLastNameId, dateOfBirthId) {	  
		var personData = new Array(6);		
		personData[0] = nameId;
		personData[1] = lastNameId;
		personData[2] = motherLastNameId;
		personData[3] = dateOfBirthId;	
		var rfc;		
		if (evaluateNotNull(personData, "rfc")) {			
			rfc = calcMidasRFC(personData[1], personData[2], personData[0], personData[3]);			
		}
		return rfc;
	}
  
function calcMidasRFC(paterno,materno,nombreC,nac)
{  
    cadena=PrepararCadena(paterno);
    apePat=cadena;
  	checaApellidos(cadena);
  	caracter1=cadena.substring(i, i+1);
    
    var bandera=true; 
    var existeVocal = false;
    
    //Validar si la cadena contiene alg?n caracter vocal.
    if ( cadena.length == 1 )
		return 'Error en RFC, Apellido Paterno No v?lido';
    
    for(i=1; i<=cadena.length-1; i++)
		if ((cadena.substring(i,i+1) == "A") ||(cadena.substring(i,i+1) == "E")||(cadena.substring(i,i+1) == "I")||(cadena.substring(i,i+1) == "O")||(cadena.substring(i,i+1) == "U"))
		{
			existeVocal = true;
			break;
		}       
	
	if ( ! existeVocal )
		return 'Error en RFC, No se encontr? ninguna vocal';
	
	i = 0;
    while (bandera && i <= cadena.length-1)
    {   
		i++; 
        letra=cadena.substring(i,i+1);
        if ((letra=="A") ||(letra=="E")||(letra=="I")||(letra=="O")||(letra=="U")){bandera=false;}
    }
	
    caracter1+=letra; 
    
    if(materno=="")
    {
        caracter2="X";      
    }
    else
    {  
        cadena=PrepararCadena(materno);
        apeMat=cadena;
  		checaApellidos(cadena);
        caracter2=cadena.substring(i, i+1);  
     }    
    
    cadena=PrepararCadena(nombreC);	    
    nombre=cadena;
    checaNombres(cadena);
    caracter3=cadena.substring(i, i+1);        
    RFC=caracter1+caracter2+caracter3; 
    palabra=RFC;
    altisonante();
    RFC=caracter1+caracter2+caracter3;
    fechas(nac); 
    RFC+=caracter1+caracter2+caracter3;
    completo=apePat+" "+apeMat+" "+nombre;
    homoclaveRFC();    
	return RFC;   
}

function PrepararCadena(evaluada)
{
	var cad="";
    var s=evaluada;
    var letra;
    espacio=true;
    evaluada=Trim(s);
    evaluada=evaluada.toUpperCase();   
    
    for (i=0; i<evaluada.length; i++)
    {
        letra=evaluada.charAt(i);
        if (letra!=" " ) 
        {
	         if(letra.charCodeAt(0)==193){letra="A";}
             if(letra.charCodeAt(0)==201){letra="E";}
             if(letra.charCodeAt(0)==205){letra="I";}
             if(letra.charCodeAt(0)==211){letra="O";}
             if(letra.charCodeAt(0)==218){letra="U";}
             if(letra.charCodeAt(0)==220){letra="U";}

             cad+=letra;
             espacio=true;           
        }
       if((letra==" ")&&(espacio))
        {
           cad+=letra;
           espacio=false;  
        }
    }	
    return cad;
}

function checaApellidos(cadena)
{
i=0;
  if ((cadena.substring(0,4)=="DEL ")||(cadena.substring(0,4)=="VAN ") ||(cadena.substring(0,4)=="VON " )||(cadena.substring(0,4)=="MAC " )){i=4; }

  if (cadena.substring(0,6)=="DE LA " ){i=6;}
  else{
        if ((cadena.substring(0,7)=="DE LAS ")||(cadena.substring(0,7)=="DE LOS ")){i=7;}
        else{
            if ((cadena.substring(0,3)=="DE ")||(cadena.substring(0,3)=="MC ")){i=3;}  
        }
   }    
   if (cadena.substring(0,2)=="Y "){ i=2; }  
}
  
function checaNombres()
{
   i=0;
   
   if (cadena.substring(0,6)=="MARIA ")
   {
       i=6;
        if (cadena.substring (6,10)=="DEL "){i=10;} 
        else{
             if ((cadena.substring(6,13)=="DE LOS ")||(cadena.substring(6,13)=="DE LAS ")){i=13;}
             else{
                 if (cadena.substring(6,9)=="DE "){i=9;}  
                 }
           }
   }

   if (cadena.substring(0,5)=="JOSE ")
   {
       i=5;
       if (cadena.substring(0,8)=="JOSE DE "){i=8;}
   }

   if (cadena.substring(0,4)=="MA. "){i=4;}
   
   if (cadena.substring(0,3)=="MA "){i=3;}    
}

function altisonante()
{	
   for(j=0; j<ANT.length; j++)
   {
       if (palabra==ANT[j]){ caracter3="X";}
   }
}

function fechas(nac)
{
    caracter3=nac.substring(0,2);
    caracter2=nac.substring(3,5);
    caracter1=nac.substring(8,11);
}

function homoclaveRFC()
{
     temp="0";
     indice=0;
     while (indice<completo.length)
     {
          car=completo.charAt(indice);
          if ((car=="0")||(car=="1")||(car=="2")||(car=="3")||(car=="4")||(car=="5")||(car=="6")||(car=="7")||(car=="8")||(car=="9"))
         {
              car=car.charCodeAt(0)-48;}
          else{
                 if (car==" "){ car=0;}
                 else{
                     if(car.charCodeAt(0)==209){car=10;}
                     else{
                         if(car<"J"){ car=car.charCodeAt(0)-54;}
                         else{
                             if (car<"S"){car=car.charCodeAt(0)-53;}
                             else {  
                                  car=car.charCodeAt(0)-51;
                             }
                         }
                     }
                 }
           }
           car+=100;
           car=""+car;
          car=car.substring(1,3);
          temp+=car;
          indice++;         
     }     
      
     res=0;
     indice=0;
     while (indice<temp.length-1)
     {
	pri=parseInt(temp.substring(indice, indice+2));
        seg=parseInt(temp.substring(indice+1, indice+2));
        res+=(pri*seg);
         indice++;
     }

     pri=Math.floor((res % 1000)/34);
     seg=Math.floor((res % 1000)%34);
       car=codigo[pri];
       RFC+=car;
       car=codigo[seg];
       RFC+=car;   
      
      temp=" ";
      seg=0;
      indice=0;//probablemente sea 0
      while (indice<RFC.length)
      {
          car=RFC.charAt(indice);
           // car=RFC.substring(indice, indice+1);
           if ((car=="0")||(car=="1")||(car=="2")||(car=="3")||(car=="4")||(car=="5")||(car=="6")||(car=="7")||(car=="8")||(car=="9"))
            {   pri=car.charCodeAt(0)-48;}
            else
            {
             if (car==" "){pri=37;}
              else {
                  if (car.charCodeAt(0)==209){pri=24;}
                      else{
	                  if ((car=="A")||(car=="B")||(car=="C")||(car=="D")||(car=="E")||(car=="F")||(car=="G")||(car=="H")||(car=="I")||(car=="J")||(car=="K")||(car=="L")||(car=="M")||(car=="N"))
                             {pri=car.charCodeAt(0)-55;}
                             else{ pri=car.charCodeAt(0)-54;}
                          }
                  }
           }
          seg+=(pri*(13-indice));
          indice++;
       }       

       pri=seg % 11;
       
       if (pri==0){RFC+="0"}
       else{
           pri=11-pri;
           
           if(pri==10){ RFC+="A";}
           else{
               if(pri==0){RFC+="0"}
               else{RFC+=""+pri;  }
           }
       } 
}

function setRFCAtForm(rfc, targetSingles, targetDate, targetHomoclave){
	if (rfc && rfc != "") {
		var flag = true;
		var dateRFC = rfc.match(/\B\d{6}/);
		var firstIndex = rfc.indexOf(dateRFC);
		var lastIndex = firstIndex + 6;
		var acronymRFC = rfc.substr(0, firstIndex);
		var homoclaveRFC = rfc.substr(lastIndex, rfc.length - lastIndex);
		if (isNaN(parseInt(dateRFC))) {
			dateRFC = "";
			flag = false;
		}
		if (homoclaveRFC.length > 3) {
			homoclaveRFC = "";
			flag = false;
		}
		if (acronymRFC.length > 4) {
			acronymRFC = "";
			flag = false;
		}	
		$(targetSingles).value = acronymRFC;
		$(targetDate).value = dateRFC;
		$(targetHomoclave).value = homoclaveRFC;		
	}
}

function evaluateNotNull(personData, type){

	var data = new Array(6); 
	var dataLength;
	var i=0;
	var valid = true;
	var tmpField = '';
	
	data = personData;
	dataLength = data.length;
	
	if (type != 'curp'){
		dataLength = 4;
	}
	
	for(i=0;i<dataLength;i++){
		//eval('tmpField = ' + data[i]);
		tmpField = data[i];
		if(tmpField == null || tmpField.length == 0){
			valid = false;
		}
	}	
	return valid;	
}

//////////persona moral


function calcularRFCMoral(razonSocial, fechaConstitucion)
{		
	var nombre=null;
	var nac=null;
	if(jQuery.isValid(razonSocial) && jQuery.isValid(fechaConstitucion)){
		nombre=razonSocial;
		nac=fechaConstitucion;
	}
	else{
		 nombre = document.contractingDatsForm.socialReason.value;
		 nac = document.contractingDatsForm.constitutionDate.value;
	}
	if(!jQuery.isValid(nombre)||!jQuery.isValid(nac)){
		alert('No se puede calcular el RFC de una persona moral con los datos capturados');
		return false;
	}
var intJ = 0;
//Se quitan las palabras no deseadas de la cadena
for(intI = 1; intI <= 69; intI++)  //  Len(aDelPerMoral)
  {	
	
//	intJ = " "+nombre.indexOf(aDelPerMoral[intI])+" ";
	nombre = nombre.replace(aDelPerMoral[intI]," ");	
//	if(intJ!=0)
//	{
//		nombre = nombre.replace(aDelPerMoral[intI],"");		
//	}
//	if(intI<=22)
//	{
//		intJ = nombre.indexOf(aDelPerMoral[intI]);
//		if(intJ!=0)
//		{
//			nombre = nombre.replace(aDelPerMoral[intI],"");			
//		}
//	}
  }
  nombre = Trim(nombre) + " ";  
  strRegistro = nombre;

  if (nombre.indexOf(" ") == 0) {
	alert("No se puede calcular el RFC de una persona moral con los datos capturados");

	  RFC = "";
	  return RFC;
	}
	//Se obtiene el primer segmento de la cadena
	strLetra1 = Trim(strRegistro.substr(0, strRegistro.indexOf(" ") - 1));
	strRegistro = Trim(strRegistro.substr((strRegistro.indexOf(" ") + 1), strRegistro.length));
	
	//Se obtiene el segundo segmento de la cadena
	if (strRegistro.indexOf(" ") != -1) {
		strLetra2 = strRegistro.substr(0, strRegistro.indexOf(" ") - 1);
		strRegistro = strRegistro.substr((strRegistro.indexOf(" ") + 1), strRegistro.length);
	}
	else
	{
		strLetra2 = strRegistro.substr(0,2);
		strRegistro = "";
		strLetra3 = "";	
	}
	
	//Se obtiene el tercer segmento de la cadena
	if(strRegistro.length > 0){ 
		strLetra3 = strRegistro.substr(0, 1);
	}
	
	//Si hay tres palabras
	if(strLetra3 != "" && strLetra2 != "" && strLetra1 != ""){
		  strRegistro = strLetra1.substr(0, 1) + strLetra2.substr(0, 1) + strLetra3.substr(0, 1);	  
	}
	
	//Si hay dos palabras
	if(strLetra3 == "" && strLetra2 != "" && strLetra1 != ""){
		 strRegistro = strLetra1.substr(0, 1) + strLetra2.substr(0, 2);
	}
	
	//Si hay solo una palabra
	if(strLetra3 == "" && strLetra2 == "" && strLetra1 != ""){
		strRegistro = strLetra1.substr(0, 3);	
	}
	 

//Se concatena la fecha de RFC

    RFC = strRegistro
    fechas(nac); 
    RFC+=caracter1+caracter2+caracter3;
    if(!jQuery.isValid(razonSocial) && !jQuery.isValid(fechaConstitucion)){
    	$("RFCMoral").value = RFC;
    }
    return RFC;
}

function calcularCURPMidas()
{	
	  var nombre = $("firstName").value;	  
	  var apePat = $("fatherLastName").value;
	  var apeMat = $("motherLastName").value;	  
	  var nac = $("dateOfBirth").value;	  	  
	  var edos = $("stateOfBirthId").value ;	  
	  var sex = $("genderId").value;
	  if(nombre!==null && nombre !=="" && 
		 apeMat!==null && apeMat !=="" &&
		 apePat!==null && apePat !=="" &&
		 nac !== null && nac !=="" &&
		 edos !==null && edos !=="" &&
		 sex !==null && sex !==""){
		  cadena=PrepararCadena(apePat);
		  	checaApellidos(cadena);
		  	caracter1=cadena.substring(i, i+1);
		        y=i;
		        consonantes(cadena);
		        consonante1=cadena.charAt(y); 
			//consonante1=conso;
		        var bandera=true;    
		        while (bandera)
		         {   
		            i++; 
		            letra=cadena.substring(i,i+1);
		              
		             if ((letra=="A") ||(letra=="E")||(letra=="I")||(letra=="O")||(letra=="U")){bandera=false;}
		             if(cadena.length < i)
		             	break;
		        }
		        caracter1+=letra; 
		   
		    	if(apeMat=="")		
		        {
		         caracter2="X";
		         consonante2="X";
		         //consonante2=conso;
		        }
		       else
		       {  
		       cadena=PrepararCadena(apeMat);
		       checaApellidos(cadena);
		       caracter2=cadena.substring(i, i+1)
		       y=i;
		       consonantes(cadena);
		       consonante2=cadena.charAt(y); 
			//consonante2=conso;
		    }
		    cadena=PrepararCadena(nombre);
		    checaNombres(cadena);
		    caracter3=cadena.substring(i, i+1);
		    y=i;
		    consonantes(cadena);
		    consonante3=cadena.charAt(y);      
		    //consonante3=conso;
		    curp=caracter1+caracter2+caracter3;
		    palabra=curp;
		    altisonante();
		    curp=caracter1+caracter2+caracter3;
		    fechas(nac);
		    //SEXO();
		    curp+=caracter1+caracter2+caracter3+gender[sex];
		    edo=estados[edos];
			curp+=edo+consonante1+consonante2+consonante3;
			$("CURP").value = curp;
		    return curp;   	
	  }      
}

function consonantes(cadena){	
   bandera=true;
   while (bandera)
         {   
             y++; 
            letra=cadena.charAt(y); 
             if ((letra!="A") && (letra!="E")&&(letra!="I")&&(letra!="O")&&(letra!="U"))
             {  bandera=false;}
             if(cadena.length < y)
             	break;
         }
}

function Trim(s){	
return LTrim(RTrim(s));}

function LTrim(s){	
	// Devuelve una cadena sin los espacios del principio
	var i=0;
	var j=0;
	
	// Busca el primer caracter <> de un espacio
	for(i=0; i<=s.length-1; i++)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(j, s.length);
}

function RTrim(s){
        var i=0;
	var j=0;
	for(var i=s.length-1; i>-1; i--)
		if(s.substring(i,i+1) != ' '){
			j=i;
			break;
		}
	return s.substring(0, j+1);
}

//los parametros siglas y fecha son usados cuando queremos separar el rfc, si se requiere el rfc en un solo campo enviarlos nulos
function generarYDividirRFCMoral(razonSocial, fechaConstitucion, siglas, fecha, unCampo){

	if(jQuery.isValid(razonSocial)&&jQuery.isValid(fechaConstitucion)){	
		var rfcMoral=calcularRFCMoral(razonSocial, fechaConstitucion);
		if(fechaConstitucion!=null && siglas !=null && rfcMoral!=null && rfcMoral!=''){
			dwr.util.setValue(siglas,rfcMoral.substring(0,3));
			dwr.util.setValue(fecha,rfcMoral.substring(3,rfcMoral.length));
			parent.mostrarMensajeInformativo("La homoclave debe capturarse manualmente","20");
		}else if(unCampo!=null && unCampo!='' && rfcMoral!=null && rfcMoral!=''){
			dwr.util.setValue(unCampo,rfcMoral);
			parent.mostrarMensajeInformativo("La homoclave debe capturarse manualmente","20");
		}
	}
}