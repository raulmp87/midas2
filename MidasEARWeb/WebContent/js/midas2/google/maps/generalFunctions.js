/**
 * Funciones generales para un uso mas facil del api de google mas V3
 * @author eftregue
 */

var map,
	containerMap,
	marker, 
	geocoder,
	markers = [],
	directionsDisplay,
	directionsService, 
	marcador,
	infowindow,
	paths = [],
	showWayPoint = false;

var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/',
	iconURLPrefixAlter = '/MidasWeb/img/maps/' //'http://maps.gstatic.com/mapfiles/ridefinder-images/';


var ICON_REPORTE = iconURLPrefixAlter + 'letter_r_red.png',
	ICON_REPORTE_ASIGNADO = iconURLPrefixAlter + 'letter_r_yellow.png',
	ICON_REPORTE_CONTACTO = iconURLPrefixAlter + 'letter_r_green.png',
	ICON_REPORTE_ALTERNO = iconURLPrefixAlter + 'letter_r_bluenavy.png',
	ICON_AJUSTADOR_DISPONIBLE = iconURLPrefixAlter +'car_white.png',
	ICON_AJUSTADOR_DISPONIBLE_HISTORIA = iconURLPrefixAlter + 'mm_20_white.png',
	ICON_AJUSTADOR_ASIGNADO = iconURLPrefixAlter + 'car_yellow.png',
	ICON_AJUSTADOR_ASIGNADO_HISTORIA = iconURLPrefixAlter + 'mm_20_yellow.png',
	ICON_REPORTE_ASIGNADO_REPORTE_ACTUAL = iconURLPrefixAlter + 'letter_r_blue.png',
	ICON_AJUSTADOR_ASIGNADO_REPORTE_ACTUAL = iconURLPrefixAlter + 'car_blue.png';

var BTN_ON = '/MidasWeb/img/maps/on.png',
	BTN_OFF = '/MidasWeb/img/maps/off.png';

var TIPO_AJUSTADOR = "ajustador",
	TIPO_REPORTE   = "reporte",
	TIPO_REPORTE_ALTERNO = "reporteAlterno";
	TIPO_REPORTE_NOCOORDS = "reporteNoCoords";

var DEFAULT_ZOOM = 11;


var PATH_COLOR = ['#015c66', '#664101', '#24211c', '#6e341f', '#481f6e', '#6e1f35'];

/**
 * Funcion que inicializa el mapa en el contenedor indicado, con las coordenadas indicadas.
 * 
 * @param   container   String con el id del contenedor del mapa	     (No hay valor por default)
 * @param   zoom        Acercamiento o zoom que se verá en el mapa       (default 11)
 * @param   Lat         Latitud donde se desea centrar el mapa           (default 25.7026522)
 * @param   Lng         Longitud donde se desea centrar el mapa          (default -100.2975857)
 * 
 * @returns map         Objeto del tipo Map()
 */
function initMap( container , zoom , oficinaId, Lat, Lng ) {
	directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer();
	containerMap = document.getElementById(container);
	markers = [];
	marker = null;
	marcador = null;
	
	/*
	 * Valores default posicionados en Monterrey
	*/
		defaultLatitud = 25.7026522;
		defaultLongitud = -100.2975857;
		zoomDefault=1;
	
    if( !Lat ){
    	Lat = defaultLatitud
    }
    
    if( !Lng ){
    	Lng = defaultLongitud;
    }

    if( !zoom ){
    	zoom = zoomDefault;
    }

    var latlng = new google.maps.LatLng(Lat,Lng);
    var settings = {
	    zoom: zoom,
	    center: latlng,
	    mapTypeControl: true,
	    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
	    navigationControl: true,
	    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
	    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    map = new google.maps.Map(containerMap, settings);
    directionsDisplay.setMap(map);
	var width = jQuery("#"+container).parent().width()-5;
	var height = jQuery("#"+container).parent().height()-5;
	jQuery("#"+container).width(width);
	jQuery("#"+container).height(height);
	
	var trafficLayer = new google.maps.TrafficLayer();
	trafficLayer.setMap(map);
	
	var contentString = '';

	infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 250
	});


	return map;
}

/**
 * Funcion para crear marcadores, se envia un objeto JSON con: Valor boleano si se quiere que sea dragabble
 * 
 * @param marcador   objeto con atributos de configuracion
 * 				.latitud
 * 				.longitud
 * 				.icon
 * 				.position
 * 				.titulo
 * 				.draggable
 * 
 * @param map        objeto del tipo map donde se pondra el marcador
 * 
 * @returns {google.maps.Marker}
 */
function createMarker( marcador , map ){

	if( !map ){ 
		map = getMap();
	}
	
	if( !marcador ){
		marcador = new Object();
	}
	
    if( !marcador.position ){
    	if( !marcador.latitud ){marcador.latitud = 25.7026522; }
        if( !marcador.longitud ){ marcador.longitud = -100.2975857;}
        
        marcador.position = new google.maps.LatLng( marcador.latitud , marcador.longitud );
    }

	var marker = new google.maps.Marker(
		{
			position: marcador.position,
			map: map,
			title: marcador.titulo,
			draggable:marcador.draggable,
		    animation: google.maps.Animation.DROP
		}
	);
	markers.push(marker);
	
	return marker;
}

/**
 * Retorna Latitud y longitud a partir de un marcador
 * @param marker      Objeto del tipo Marker()
 * @returns position  Objeto con latitud(v) y longitud(B) 
 */
function getLatLng( marker ){
	return marker.getPosition();
}

/**
 * Función que permite poner eventos a un marcador, ver tipos de 
 * eventos en la siguiente URL
 * @see   https://developers.google.com/maps/documentation/javascript/events?hl=es
 * 
 * @param marker     Objeto del tipo Marker()
 * @param event      String con el nombre del evento
 * @param functions  Funciones que se desean ejecutar cuando el evento se ejecute
 */
function setEvent( marker, event, functions ){
	google.maps.event.addListener( marker, event, functions	);
}

/**
 * Función que permite recuperar el objeto "map" recien creado
 * @returns map Objeto del tipo map
 */
function getMap(){
	return map;
}

function getDirectionsService(){
	return directionsService;
}

function getDirectionsDisplay(){
	return directionsDisplay;
}


/**
 * Función que permite recuperar un arreglo con todos los marcadores 
 * creados al momento de la ejecución
 *  
 * @returns {Array} markers
 */
function getMarkerArray(){
	return markers;
}

function getMarker(){
	return marker;
}

/**
 * Función que retorna un objeto del tipo "Geocoder()"
 * @returns geocoder
 */
function getGeocoder(){
	geocoder = new google.maps.Geocoder();
	return geocoder;
}

/**
 * Función que ejecuta la busqueda de las coordenadas en base a una dirección 
 * 
 * @param address      String con la dirección a buscar
 * @param callbackFn   Función a ejecutar una vez que se haya realizado la busqueda
 * @returns
 */
function createMarkerFromAddress( address , callbackFn  ){
	var geocoder = getGeocoder();
	
	var response = 
		geocoder.geocode(
			{
				'address': address,
				'region': 'MX'
			},
			function( results , status ){
				callbackFn( results, status );
			}
		);
	
	return response;
}

function responseToGeoCoder( results , status , callbackFn ){
	marcador = new Object();
	marcador.titulo = "Ubicaci\u00F3n Siniestro";
	marcador.draggable = true;
	marcador.icon = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
	
	if(status == google.maps.GeocoderStatus.OK){
		var map = getMap();
		map.setCenter(results[0].geometry.location);
		map.setZoom(16);
		marcador.position = results[0].geometry.location;
		var marcadorCreado = createMarker(marcador);
		callbackFn( marcadorCreado );
	}else{
		getMap().setZoom(4);
		var marcadorCreado = createMarker(marcador);
		callbackFn( marcadorCreado );
	}
}

/**
 * Obtener ruta entre origen y destino
 * @param origin
 * @param destination
 * @param resetAjustadorData
 * @param marcador
 * @param map
 */
function setRoutes( origin , destination , resetAjustadorData , marcador, map ){
	var dS = getDirectionsService();
	var dD = getDirectionsDisplay();
	
	var request = {
		origin:new google.maps.LatLng( origin.lat() , origin.lng() ),
		destination:new google.maps.LatLng( destination.lat() , destination.lng() ),
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.DirectionsUnitSystem.METRIC,
		durationInTraffic: true
	};
	
	dS.route(
		request,
		function(result, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				dD.setMap(map);
				dD.setDirections(result);
				if( resetAjustadorData ){
					var distancia = result.routes[0].legs[0].distance.value/1000;
					marcador.distanciaSiniestroKM=distancia;
					var tiempo = result.routes[0].legs[0].duration;
					marcador.duracion=tiempo.text;
					setListadoAjustadoresMapa( marcador , resetAjustadorData );
				}
			}else{
				alert('No se pudo calcular la ruta entre ambos puntos: ' + status);
			}
		}
	);
}


/**
 * Geneerar un marcador para un ajustador
 * @param ajustador
 * @returns
 */
function crearMarcadorAjustador(ajustador, reporteId){
	var ajustadorMarker = null;
	if(reporteId != null && reporteId != '' && contieneReporte(ajustador, reporteId)){
		icon = ICON_AJUSTADOR_ASIGNADO_REPORTE_ACTUAL;
	}else if(!ajustador.estatus && !ajustador.estaAsignado){
		icon = ICON_AJUSTADOR_DISPONIBLE_HISTORIA;
	}else if (!ajustador.estatus && ajustador.estaAsignado){
		icon = ICON_AJUSTADOR_ASIGNADO_HISTORIA;
	}else if(ajustador.estaAsignado ){
		icon = ICON_AJUSTADOR_ASIGNADO;
	}else {
		icon = ICON_AJUSTADOR_DISPONIBLE;
	}	
	
	ajustadorMarker = new google.maps.Marker(
				{
					id:ajustador.idAjustador,
					position: new google.maps.LatLng( ajustador.latitud , ajustador.longitud ),
					map: map,
					icon:icon,
					draggable: (ajustador.draggable != null && ajustador.draggable != '' ? ajustador.draggable : false),
					title: ajustador.nombreAjustador,					
				    animation: google.maps.Animation.DROP,
				    calcularRuta:ajustador.calcularRuta,
				    distancia:ajustador.distanciaSiniestroKM,
					disponibilidad:ajustador.disponibilidad,
					fechaCoordenadas:ajustador.fechaCoordenadas,
					estatus:ajustador.estatus,
					estaAsignado:ajustador.estaAsignado,
					numeroReportesAsignados:ajustador.numeroReportesAsignados,
					tiempo:ajustador.duracion,
					tipo: TIPO_AJUSTADOR,
					reportes: ajustador.reportesAsignados
				}
			);
	markers.push(ajustadorMarker);
		
	google.maps.event.addListener(ajustadorMarker, 'click', (function(ajustadorMarker) {
        return function() {
          infowindow.setContent(generarInfoWindowAjustador(ajustadorMarker));
          infowindow.open(map, ajustadorMarker);
        }
      })(ajustadorMarker));
	
	return ajustadorMarker;
}


/**
 * Generar un marcador para un reporte
 * @param objetoReporte
 * @returns
 */
function crearMarcadorReporte(objetoReporte){
	var reporteMark = null;
	if(objetoReporte.coordenadas != null){
		var reporteLatitud = objetoReporte.coordenadas.latitud;
		var reporteLongitud = objetoReporte.coordenadas.longitud;
		if(reporteLatitud != null && reporteLatitud != ''){
			reporteMark = new google.maps.Marker(
					{
						id: objetoReporte.reporteId,
						position: new google.maps.LatLng( reporteLatitud,reporteLongitud),
						map: map,
						icon: objetoReporte.icono,
						title: objetoReporte.numeroReporte + ' : ' + objetoReporte.estatus,
					    animation: google.maps.Animation.DROP,					   
					    tipo: objetoReporte.tipo,
					    draggable: (objetoReporte.draggable != null && objetoReporte.draggable != '' ? objetoReporte.draggable : false),
					    numReporte : objetoReporte.numeroReporte,
					    fechaAlta: objetoReporte.fechaReporteStr,
					    fechaAsignacion: objetoReporte.fechaAsignacionStr,
					    fechaContacto: objetoReporte.fechaContactoStr,
					    fechaOcurrido: objetoReporte.fechaOcurridoStr,
					    cliente: objetoReporte.cliente,
					    numPoliza: objetoReporte.numPoliza,
					    asignadoA: objetoReporte.asignadoA
					}
			);					
			
			markers.push(reporteMark);

			google.maps.event.addListener(reporteMark, 'click', (function(reporteMark) {
		        return function() {
		          infowindow.setContent(generarInfoWindowReporte(reporteMark));
		          infowindow.open(map, reporteMark);
		        }
		      })(reporteMark));
			
		}
	}	
	return reporteMark;
}

/**
 * Generar una venta informativa para un listado de reportes que no tiene coordenadas
 * @param reportes
 * @returns {String}
 */
function generarInfoWindowNoCoordenadas(reportes){
	
	var content = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h2 id="firstHeading" class="firstHeading">Reportes sin confirmación de coordenadas</h2>'+
    '<div id="bodyContent" align="left">';
	for(var i=0; i < reportes.length; i++ ){
		content = content +  '<p><b>Reporte:</b> ' + reportes[i].numReporte + '</p>';
	}    
    '</div></div>';
	
	return content;		
}

/**
 * Generar una ventana informativa para un marcador de reporte
 * @param reporteMark
 * @returns {String}
 */
function generarInfoWindowReporte(reporteMark){
	var content = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h2 id="firstHeading" class="firstHeading">Reporte: '+ reporteMark.numReporte + '</h2>'+
    '<div id="bodyContent" align="left">'+
    '<p><b>Fecha Ocurrido:</b> ' + reporteMark.fechaOcurrido + '</p>' +
    '<p><b>Fecha Alta Reporte:</b> ' + reporteMark.fechaAlta + '</p>' +
    '<p><b>Fecha Asignaci&oacute;n:</b> ' + reporteMark.fechaAsignacion + '</p>' +
    '<p><b>Fecha Contacto:</b> ' + reporteMark.fechaContacto + '</p>' +
    '<p><b>P&oacute;liza:</b> ' + reporteMark.numPoliza + '</p>' +
    '<p><b>Report&oacute;:</b> ' + reporteMark.cliente + '</p>' +
    '<p><b>Asignado A:</b> ' + reporteMark.asignadoA + '</p>' +
    '</div></div>';
	
	return content;		
}


/**
 * Generar una ventana informativa para un marcador de tipo ajustador
 * @param ajustadorMark
 */
function generarInfoWindowAjustador(ajustadorMark){	
	var content = '<div id="content" >'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h2 id="firstHeading" class="firstHeading">Ajustador: '+ ajustadorMark.title + '</h2>'+
    '<div id="bodyContent" align="left">'+
    '<p><b>&Uacute;ltimo registro m&oacute;vil:</b> ' + (ajustadorMark.fechaCoordenadas) + '</p>';	
	//complementar con informacion reportes	
	if(ajustadorMark.numeroReportesAsignados != null ){	
		content = content + 
			'<p><b>Estatus:</b> ' + (ajustadorMark.estaAsignado ? 'Asignado' : 'Disponible') + '</p>' +
			'<p><b>Num reportes asignados:</b> ' + ajustadorMark.numeroReportesAsignados + '</p>';		
		//listado de reportes
		if(ajustadorMark.reportes != null && ajustadorMark.reportes.length > 0){
			content = content + '<p><b>Reportes:</b></p>';
			var contentReportes = ''
			for(var i=0; i < ajustadorMark.reportes.length; i++ ){
				contentReportes = contentReportes +  '<p>- ' + ajustadorMark.reportes[i].numeroReporte + '</p>';
			}
			content = content + contentReportes;
		}    
	}    
    content = content + '</div></div>';	
	return content;		
}

/**
 * Obtener un marcador en especifico. Se debe definir el <code>tipo</code> y el <code>id</code> con el que esta registrado
 * @param tipo
 * @param id
 * @returns
 */
function obtenerMarcador(tipo, id){
	for(var i = 0; i < markers.length; i++){
		if(markers[i].tipo == tipo && markers[i].id == id){
			return markers[i];
		}
	}
	return null;
}

/**
 * Limpiar marcadores del <code>tipo</code>. Se puede limpiar un <code>id</code> en especifico.
 * @param tipo
 * @param id <optioal/>
 */
function limpiarMarcador(tipo, id){
	for(var i = 0; i < markers.length; i++){
		if(markers[i].tipo == tipo && 
				((id == null || id == '') || (id != null && id != '' && markers[i].id == id))){
			markers[i].setMap(null);						
		}
	}
}


/**
 * Limpiar waypoints existentes.
 * 
 */
function limpiarWayPoints(){	
	for(var i = 0; i < paths.length; i++){
		paths[i].path.setPath([]);
	}
}


/**
 * Cambiar el foco del mapa para visualizar las coordenadas de una oficina
 * @param oficinaId
 */
function setMapaCoordenadasOficina(oficinaId){
	var coordenadas = obtenerCoordenadasOficina(oficinaId);
	if(coordenadas != null && coordenadas.latitud != null && coordenadas.longitud != null){
		var map = getMap();
		if(map != null){
			 var latlng = new google.maps.LatLng(coordenadas.latitud,coordenadas.longitud);
			 map.setCenter(latlng);
			 map.setZoom(DEFAULT_ZOOM);
		}	
	}				
}

/**
 * Obtener los valores de longitud y latitud para una oficina
 * @param oficinaId
 * @returns coordenadas. Objeto que contiene coordenadas.latitud y coordenadas.longitud
 */
function obtenerCoordenadasOficina(oficinaId){
	var coordenadas = new Object();
	var url = "/MidasWeb/siniestros/catalogo/servicio/obtenerCoordenadasOficina.action";	
	jQuery.ajax({
		  dataType: "json",
		  url: url,
		  async: false,
		  data: {oficinaId : oficinaId},
		  beforeSend: function(){
			  parent.blockPage();
		  },
		  success: function(data) {			  
			  if(data != null && data.latitud != null && data.longitud != null){
				  coordenadas.latitud = data.latitud;
				  coordenadas.longitud = data.longitud;
			  }			  
			  parent.unblockPage();
		  }
		});
	return coordenadas;		
}


/**
 * Generar marcadores en mapa con un timeout para una mejor visualizacion. Se utiliza cuando hay un gran numero de marcadores a generar.
 * @param ajustador
 * @param timeout
 */
function generarMarcadorAjustadorTimeout(ajustador, timeout) {	 
	window.setTimeout(function() {
		crearMarcadorAjustador(ajustador);
	 }, timeout);
}

/**
 * Revisar si un objeto ajustador contiene un numero de reporte en especial
 * @param ajustador
 * @param numReporte
 * @returns {Boolean}
 */
function contieneReporte(ajustador, reporteId){
	var contiene = false;
	
	if(ajustador != null && reporteId != null && ajustador.reportesAsignados != null && ajustador.reportesAsignados.length > 0){
		for(var i = 0; i < ajustador.reportesAsignados.length; i++){						
			if(reporteId == ajustador.reportesAsignados[i].reporteId){
				return true;
			}
		}
	}
	return contiene;
}

/**
 * Determinar el icono a utilizar dependiendo del estatus del reporte.
 * @param reporte
 * @returns
 */
function determinarIcono(reporte){		
	if(reporte.fechaAsignacionStr == "NA"){
		reporte.icono = ICON_REPORTE;
	}else if(reporte.fechaContactoStr == "NA"){
		reporte.icono = ICON_REPORTE_ASIGNADO;							
	}else{
		reporte.icono = ICON_REPORTE_CONTACTO;
	}			
	return reporte;
}


/**
 * Trazar linea entre puntos (waypoints). 
 * @param coordenadas matriz que contiene las coordenadas. Se debe definir asi: [{lat: val1, lng: val2}, {lat: val3, lng: val4}]
 * @param color. El codigo hexadecimal para el color con el cual se desea pintar la linea. Default es rojo
 * @param calibre. El calibre/grosor de la linea, default es 1
 */
function trazarLineaEntreCoordenadas(coordenadas, color, calibre){
	if(color == null || color == ''){
		color = '#FF0000';
	}
	if(calibre == null || calibre == ''){
		calibre = 1;
	}	
	var path = new google.maps.Polyline({
        path: coordenadas,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: 1.0,
        strokeWeight: calibre
      });
      path.setMap(getMap());
      return path;
}

/**
 * Ocultar/Mostrar waypoints previamente definidos
 */
function activarDesactivarWayPoint(){
	if(paths != null && paths.length > 0){
		for(var i = 0; i < paths.length; i++){
			var pathObj = paths[i];
			if(pathObj.path != null){
				if(pathObj.path.getMap() == null){
					pathObj.path.setMap(getMap());
				}else{
					pathObj.path.setMap(null);					
				}
			}
		}		
	}	
}


/**
 * Mostrar cuadro de ayuda
 */
function showMonitorHelp(){
	var url = '/MidasWeb/jsp/siniestros/cabina/ayudaMonitorMapa.jsp';
	parent.mostrarVentanaModal('monitorHelp', 'Ayuda', null, null, 600, 400, url, null);
}
