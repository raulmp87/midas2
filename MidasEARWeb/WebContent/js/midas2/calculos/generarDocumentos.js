var agentesDocumentosGrid;

/**
 * Lista las gerencias por centro operacion
 */
function loadGerenciasByCentroOperacionCascada() {
	var idCentroOperacion = checkInputs("operacionesList");	
	listadoService
			.listarGerenciasPorCentroOperacion(
					idCentroOperacion,
					function(data) {
						makeCheck(data, "gerenciaList",
								"gerenciasSeleccionadas", "descripcion",
								"gerenciaList",
								"loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');");
					});
}

/**
 * Obtiene los Ejecutivos por las gerencias seleccionadas
 */
function loadEjecutivoByGerenciaCascada() {	
	var idGerencias = checkInputs("gerenciaList");	
	listadoService.listarEjecutivoPorGerencia(idGerencias, function(data) {
		makeCheck(data, "ejecutivoList", "ejecutivosSeleccionados",
				"nombreCompleto", "ejecutivoList",
				"loadPromotoriaByEjecutivoCascada()");
	});
}

/**
 * Obtiene las promotorias por los ejecutivos seleccionados
 */
function loadPromotoriaByEjecutivoCascada() {
	var idEjecutivos = checkInputs("ejecutivoList");
	listadoService.listarPromotoriaPorEjecutivo(idEjecutivos, function(data) {
		makeCheck(data, "promotoriaList", "promotoriasSeleccionadas",
				"descripcion", "promotoriaList", null);
	});
}

jQuery(document).ready(function() {	
	listadoService.getMapMonths(function(data) {
		addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
	})
	listadoService.getMapYears(16, function(data) {
		 addOptionOrderdesc("anios", data)
//		addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
	});
	

	habilitarFiltrosCondicionales();
});

function addOptionOrderdesc(target, map){
	jQuery("#"+target).append("<option value=''>Seleccione...</option>");
	var a = 0;
	var arr1 = new Array();
	for (var key in map) {
		arr1[a]= key;
		a++;
	}
	for(var b = arr1.length; b>0 ;b--){
		jQuery("#"+target).append("<option value="+arr1[b-1]+">"+arr1[b-1]+"</option>");
	}
}

function habilitarFiltrosCondicionales()
{
	var valorSeleccionado = jQuery.trim(dwr.util.getValue("tiposDocs")); 	
	
	if (valorSeleccionado == 'ESTADO DE CUENTA') 
	{	
		jQuery('#chkDetalle').attr("disabled", false);		
		jQuery('#chkSegAfirmeCom').attr("disabled", false);
		jQuery('#chkGuiaLectura').attr("disabled", false);
		jQuery('#chkMostrarColAdicionales').attr("disabled", false);
	}else 
	{
		jQuery('#chkDetalle').attr("disabled", true);		
		jQuery('#chkSegAfirmeCom').attr("disabled", true);
		jQuery('#chkGuiaLectura').attr("disabled", true);
		jQuery('#textoSegAfirmeCom').attr("disabled", true);
		jQuery('#chkMostrarColAdicionales').attr("disabled", true);
	}		
}

function activarAreaAfirmeCom()
{
	if(jQuery('#chkSegAfirmeCom').is(':checked'))
	{
		jQuery('#textoSegAfirmeCom').attr("disabled", false);		
	}
	else
	{
		jQuery('#textoSegAfirmeCom').attr("disabled", true);		
	}	
}

function buscarAgentesParaGeneracionDocs(validarCampos)
{	
	if(validarCampos == 1)
	{
		if(!validarCamposMandatorios())
		{
		    return false;	
		}
	}
	
	mostrarIndicadorCarga('indicador');
	var fechaValidacion;	
	
	document.getElementById('agentesDocumentosGrid').innerHTML = '';	
	agentesDocumentosGrid = new dhtmlXGridObject('agentesDocumentosGrid');
		
	agentesDocumentosGrid.attachEvent("onXLE", function(agentesDocumentosGrid){
		ocultarIndicadorCarga('indicador');
    });		
		
	var serializedForm = jQuery(document.generarDocumentosForm).serialize();
	serializedForm = serializedForm.replace(/listaAgentes/g,'agenteList');
		
	var posPath = '&'+ serializedForm
	
	
	var url = buscarAgentesParaGenDocsPath + "?" + posPath;	
	
	agentesDocumentosGrid.load(url, function(){
		for (var i=0; i<agentesDocumentosGrid.getRowsNum(); i++) agentesDocumentosGrid.render_row(i);
		
		if(agentesDocumentosGrid.getRowsNum() > 0)
		{
			jQuery("#agentesDocumentosGrid input:checkbox").attr('checked',true);			
		}
		
		agentesDocumentosGrid.checkAll(true);			
	});	
}

function enviarPorCorreo()
{
	if(validarCamposMandatorios())
	{	
        var selectedRowsIds = agentesDocumentosGrid.getCheckedRows(0);
    
        var serializedForm = jQuery(document.generarDocumentosForm).serialize();//gridAgentesDocumentosPaginado
	
	    sendRequestJQ(null, enviarDocsPath + "?"+ serializedForm +'&idsSeleccionados=' + selectedRowsIds,null,notificacionEnvioCorreo());
	}
}

function verDocumento(idAgente)
{
	if(validarCamposMandatorios())
	{
		var serializedForm = jQuery(document.generarDocumentosForm).serialize();	
		
		window.open(verDocumentoPath + "?"+ serializedForm +'&idAgenteSeleccionado=' + idAgente,"Documento");
	   	
	}
}

function validarCamposMandatorios()
{
	var idSTipoDocSel = dwr.util.getValue("tiposDocs");
	var idMesSel = dwr.util.getValue("meses");
	var idAnioSel = dwr.util.getValue("anios");

	if(idSTipoDocSel == '' || idMesSel == '' || idAnioSel == '' )
	{
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios ", "20", null);
		return false;
	}
	
	return true;	
}

function ocultarFormatoSalida(){
	var tipoDocumento = jQuery("#tiposDocs option:selected").text();
	if(tipoDocumento == "Detalle de Primas"){
		jQuery("#lbFormatoSalida").css("display","block");
		jQuery("#txfFormatoSalida").css("display","block");
		jQuery("#formatosal").attr('disabled', false);
	}else{
		jQuery("#lbFormatoSalida").css("display","none");
		jQuery("#txfFormatoSalida").css("display","none");
		jQuery("#formatosal").attr('disabled', true);
	}
}
function notificacionEnvioCorreo(){
	mostrarMensajeInformativo("El envío de correos se está procesando ", "20", buscarAgentesParaGeneracionDocs(1));
}

function verGuiaReciboHonorarios(solicitudChequeId) {
	var idAgente = agentesDocumentosGrid.getCheckedRows(0);
	var serializedForm = jQuery(document.generarDocumentosForm).serialize();	
	
	window.open(verDocumentoPath + "?"+ serializedForm +'&idAgenteSeleccionado=' + idAgente +"&solicitudChequeId="+ solicitudChequeId);
}