
/*
 * Variables globales
 */
var CLAVE_CATALOGO_BUSQUEDA = null;

var ventanaBusquedaGlobal = null;

function getVentanaBusquedaGenerica(){
	if(ventanaBusquedaGlobal == null)
		ventanaBusquedaGlobal = new VentanaBusquedaGenerica();
	return ventanaBusquedaGlobal;
}

var VentanaBusquedaGenerica = function(){
	
	this.contenedorVentanas = obtenerContenedorVentanas();
	this.ventana = null;
	
	this.claveCatalogoConsulta = null;
	
	this.objetoResultado = null;
	this.funcionMapeoObjeto = null;
	
	//Indica el tipo de consulta a realizar cliente o grupo
	this.funcionPosteriorConsulta = null;
	
	this.mostrarVentanaBusqueda = function(tipoConsulta){
		PREFIX = tipoConsulta;
		this.claveCatalogoConsulta = tipoConsulta;
		this.ventana = this.contenedorVentanas.createWindow("consultaClientes", 400, 320, 700, 400);
		
		this.ventana.setText("Consulta.");
		
		this.ventana.center();
		this.ventana.setModal(true);
		this.ventana.attachURL("/MidasWeb/controlDinamico/verDetalle.action?clave="+PREFIX+"&accion=5");//5=filtrar
		this.ventana.button("minmax1").hide();
	}
	
	//El parametro recibido es una funcion que recibe un row del Grid, lo convierte en un objeto Javascript y lo regresa 
	this.setFuncionMapeoObjeto = function(funcionMapeo){
		this.funcionMapeoObjeto = funcionMapeo;
	}
	
	//Se invoca la funcion de mapeo y se obtiene el objeto resultado, en forma de arreglo.
	this.setObjetoResultado = function(rowSeleccionada){
		this.objetoResultado = this.funcionMapeoObjeto(rowSeleccionada);
	}
	
	//esta funcion debe recibir un objeto javascript para procesarlo
	this.setFuncionPosteriorConsulta = function(funcion){
		this.funcionPosteriorConsulta = funcion;
	}
	
	this.finalizaConsulta = function(){
		this.funcionPosteriorConsulta(this.objetoResultado);
		this.ventana.setModal(false);
		this.ventana.close();
	}
}

function regresarRegistro(selectedId){
	
	var resultArray = new Array();
	
	var i = 0;
	gridRegistrosDinamicos.forEachCell(selectedId, function(celda){
		resultArray[i] = celda.getValue();
		i++;
	});
	
	var ventanaBusquedaGenerica = parent.getVentanaBusquedaGenerica();
	
	ventanaBusquedaGenerica.setObjetoResultado(resultArray);
	
	ventanaBusquedaGenerica.finalizaConsulta();
}