var valActivo = 0;
function infoConfigNotifica() {

	var fechaActualEjecucion = jQuery("#fechaEjecucion").val();
	var diaNuevoEje = jQuery("#fechaNueva").val();

	valActivo = jQuery("#active").val();

	if (diaNuevoEje == "") {
		diaNuevoEje = fechaActualEjecucion;

	}
	alertaChecked();
	saveData(diaNuevoEje,valActivo);
}

function saveData (fechaNueva, valActivo) {
	var path = "/MidasWeb/fuerzaventa/notificacionCierreMes/guardarDatos.action?"
			+ jQuery("#notificacionM").serialize();
	sendRequestJQ(null, path, targetWorkArea, null);

};

function alertaChecked() {
	/**
	 * regresa el valor true o false si esta o no seleccionado
	 * document.notificacionM.active.value Valida si tiene valor o no
	 */
	
	var iActive = document.notificacionM.active.checked

	if (iActive == true) {
		// Checked
		valActivo = 1;
	} else {
		// Not checked
		valActivo = 0;
	}
}