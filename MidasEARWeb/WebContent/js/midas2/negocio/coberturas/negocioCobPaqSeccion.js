var negocioCobPaqSeccionAsociadasGrid;
var negocioCobPaqSeccionDisponiblesGrid;
var negocioCobPaqSeccionProcessor;
var idToNegPaqSeccionAct = -1;
var idToNegTipoPolizaAct = -1;
var idToNegSeccionAct = -1;
var idMonedaAct = -1;
var idMunicipioAct = -1;
var TipoUsoVehiculo = -1;
var esRenovacion = 'null';

var jquery143;

function obtenerLineasDeNegocio(idToTipoPoliza){

	idToNegTipoPolizaAct = idToTipoPoliza;
	listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza,function(data){
		var combo = document.getElementById('idToNegSeccion');
		addOptions(combo,data);
	});
	idToNegSeccionAct = -1;
}

function obtenerMonedas(idToTipoPoliza){
	listadoService.getMapMonedaPorNegTipoPoliza(idToTipoPoliza,function(data){
		var combo = document.getElementById('idTcMoneda');
		addOptions(combo,data);
	});
	idMonedaAct = -1;
	validaCombos();
}

function obtenerPaquetes(idToNegSeccion){
	idToNegSeccionAct = idToNegSeccion;
	listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(
			idToNegSeccion,
			function(data){
				var combo = document.getElementById('idToNegPaqueteSeccion');
				addOptions(combo,data);
			});
	idToNegPaqSeccionAct = -1;
	validaCombos();
}

function obtenerTipoUsoVehiculo(idToNegSeccion){
	idToNegSeccionAct = idToNegSeccion;
	listadoService.getMapTipoUsoVehiculoByNegocio(
			idToNegSeccion,
			function(data){
				var combo = document.getElementById('idTcTipoUsoVehiculo');
				addOptionsConSeleccioneSiempre(combo,data);
			});
	TipoUsoVehiculo = -1
	jquery143('#agenteId').val(-1);
	validaCombos();
}

function obtieneMunicipio(idEstado){
	listadoService.getMapMunicipiosPorEstado(
			idEstado,
			function(data){
				var combo = document.getElementById('cityId');
				addOptionsConSeleccioneSiempre(combo,data);
			});
	idMunicipioAct = -1;
	validaCombos();
}

function onChangeComboMoneda(idMoneda){
	if(idMoneda == ""){
		idMoneda = -1;
	}
	idMonedaAct = idMoneda;
	validaCombos();
}

function onChangeComboPaquete(idToNegPaqSeccion){
	if(idToNegPaqSeccion == ""){
		idToNegPaqSeccion = -1;
	}
	idToNegPaqSeccionAct = idToNegPaqSeccion;
	validaCombos();
}

function onChangeComboMunicipio(idMunicipio){
	idMunicipioAct = idMunicipio;
	validaCombos();
}

function onChangeComboTipoUsoVehiculo(idTipoUsoVehiculo){
	if (document.getElementById('idTcTipoUsoVehiculo').getValue() == ""){
		idTipoUsoVehiculo = -1;
	}

	TipoUsoVehiculo = idTipoUsoVehiculo;
	validaCombos();
}

function onChangeRenovacion(renovacion){
	esRenovacion = jQuery("#idRenovacion").val();
	validaCombos();
}

function validaCombos(){
	if(idToNegTipoPolizaAct > -1 && idToNegSeccionAct > -1 && idToNegPaqSeccionAct > -1  && idMonedaAct > -1){
		jQuery("#stateId").removeAttr("disabled");
		jQuery("#cityId").removeAttr("disabled");
		jQuery("#idTcTipoUsoVehiculo").removeAttr("disabled");
		jQuery("#descripcionBusquedaAgente").removeAttr("disabled");
		jQuery("#idRenovacion").removeAttr("disabled");
		refrescarGridsNegocioCobPaq(null,null,null,null);
	}else{
		jQuery("#stateId").attr("disabled","true");
		jQuery("#cityId").attr("disabled","true");
		jQuery("#idTcTipoUsoVehiculo").attr("disabled", "true");
		jQuery("#descripcionBusquedaAgente").attr("disabled", "true");
		jQuery("#idRenovacion").attr("disabled", "true");
		document.getElementById('negocioSeccionCobPaqAsociadasGrid').innerHTML='';
		negocioCobPaqSeccionAsociadasGrid = new dhtmlXGridObject('negocioSeccionCobPaqAsociadasGrid');
		document.getElementById('negocioSeccionCobPaqDisponiblesGrid').innerHTML='';
		negocioCobPaqSeccionDisponiblesGrid = new dhtmlXGridObject('negocioSeccionCobPaqDisponiblesGrid');		
	}
}



function obtenerNegocioTipoPolizaAsoiadas(){
	var loadUrl = obtenerNegocioSeccionCobPaqAsociadasPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct + "&idTcTipoUsoVehiculo=" + TipoUsoVehiculo + "&agenteId=" + jquery143('#agenteId').val();
	if(esRenovacion != 'null'){ //Si esta seleccionado null en la pantalla NO se pasa el parametro para que se quede null en el action
		loadUrl += "&esRenovacionString=" + esRenovacion;
	}
	negocioCobPaqSeccionAsociadasGrid = new dhtmlXGridObject('negocioSeccionCobPaqAsociadasGrid');
	
	if(idMunicipioAct > 0){
		loadUrl = loadUrl + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId");
//		negocioCobPaqSeccionAsociadasGrid.load(obtenerNegocioSeccionCobPaqAsociadasPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId"));
	}else{
		loadUrl = loadUrl + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=";
//		negocioCobPaqSeccionAsociadasGrid.load(obtenerNegocioSeccionCobPaqAsociadasPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=''");
	}

	negocioCobPaqSeccionAsociadasGrid.attachEvent("onXLE", function(grid_obj){deshabilitarCamposCoberturasNoRC();});
	negocioCobPaqSeccionAsociadasGrid.load(loadUrl);
	
	
	negocioCobPaqSeccionProcessor = new dataProcessor(relacionarNegocioSeccionCobPaqPath);	
	negocioCobPaqSeccionProcessor.enableDataNames(true)
	negocioCobPaqSeccionProcessor.setTransactionMode("POST");
	negocioCobPaqSeccionProcessor.setUpdateMode("cell");
	negocioCobPaqSeccionProcessor.attachEvent("onAfterUpdate",refrescarGridsNegocioCobPaq);
	negocioCobPaqSeccionProcessor.defineAction("mensajeGenerico", response);
	negocioCobPaqSeccionProcessor.setVerificator(8, isInteger);
	negocioCobPaqSeccionProcessor.setVerificator(9, isInteger);
	negocioCobPaqSeccionProcessor.setVerificator(10, isInteger);
	negocioCobPaqSeccionProcessor.setVerificator(12, isFloat0);
	negocioCobPaqSeccionProcessor.setVerificator(13, isFloat0);
	negocioCobPaqSeccionProcessor.setVerificator(14, isFloat0);
	
	negocioCobPaqSeccionProcessor.init(negocioCobPaqSeccionAsociadasGrid);
}

function deshabilitarCamposCoberturasNoRC(){
    var claveTipoCalculo;
    var CELLINDEX = 19;
    var rowsNum = negocioCobPaqSeccionAsociadasGrid.getRowsNum();
    var count = 0;

    negocioCobPaqSeccionAsociadasGrid.forEachRow(function(id){
    	claveTipoCalculo = negocioCobPaqSeccionAsociadasGrid.getUserData(id,"claveTipoCalculo");
            if(claveTipoCalculo == 'RC'){
            	negocioCobPaqSeccionAsociadasGrid.editStop(); 
            	negocioCobPaqSeccionAsociadasGrid.setCellExcellType(id,CELLINDEX,'edn');
            }
    	});
}

function response(node){
	parent.readResponseDataProcessor(node,initGridsNegocioCobPaqSeccion);
	
}

function obtenerNegocioTipoPolizaDisponibles(){
	negocioCobPaqSeccionDisponiblesGrid = new dhtmlXGridObject('negocioSeccionCobPaqDisponiblesGrid');
	var url = obtenerNegocioSeccionCobPaqDisponiblesPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + "&idTcTipoUsoVehiculo=" + TipoUsoVehiculo + "&agenteId=" + jquery143('#agenteId').val();
	if(esRenovacion != 'null'){ //Si esta seleccionado null en la pantalla NO se pasa el parametro para que se quede null en el action
		url += "&esRenovacionString=" + esRenovacion;
	}
	negocioCobPaqSeccionDisponiblesGrid.load(url);
}

function refrescarGridsNegocioCobPaq(sid,action,tid,node){
	obtenerNegocioTipoPolizaAsoiadas();
	obtenerNegocioTipoPolizaDisponibles();
	return true;
}

function initGridsNegocioCobPaqSeccion(){
	if(idToNegPaqSeccionAct != -1){
		refrescarGridsNegocioCobPaq(null,null,null,null);
	}	
}

function mostrarVentanaDeducibles(idToNegCobPaqSeccion){
	var url = "/MidasWeb/negocio/deducibles/mostrarDeduciblesCobertura.action?idToNegCobPaqSeccion="+ idToNegCobPaqSeccion;
	parent.mostrarVentanaModal("deduciblesCobPaqSeccion", "Deducibles Por Cobertura", 50, 50, 600, 450, url );
}

function mostrarVentanaSumasAseguradas(idToNegCobPaqSeccion){
	var url = "/MidasWeb/negocio/cobertura/mostrarSumasAseguradasCobertura.action?idToNegCobPaqSeccion="+ idToNegCobPaqSeccion;
	parent.mostrarVentanaModal("sumasAseguradasCobPaqSeccion", "Sumas Aseguradas Por Cobertura", 50, 50, 600, 450, url );
}

function habilitarAutocompletarBusquedaAgente(){
	jQuery("#agenteId").val(-1);
	jQuery(function(){
		jquery143( '#descripcionBusquedaAgente' ).autocomplete({
	               source: function(request, response){
	            	   jquery143.ajax({
				            type: "POST",
				            url: autocompletarBusquedaAgentesPath,
				            data: {descripcionAgente:request.term},              
				            dataType: "xml",	                
				            success: function( xmlResponse ) {
				           		response( jquery143( "item", xmlResponse ).map( function() {
									return {
										idAgente: jquery143("idAgente",this).text(),
										value: jquery143( "descripcion", this ).text(),
					                    id: jquery143( "id", this ).text()
									}
								}));
	               		}
	               	})},
	               minLength: 3,
	               delay: 1000,
	               select: function( event, ui ) {
	            	   jquery143('#agenteId').val(ui.item.id);
	            	   jQuery("#descripcionBusquedaAgente").attr("readonly","true");
	            	   validaCombos();
	               }		          
	         });
	 });
}

function limpiarAgente(){
	jQuery("#descripcionBusquedaAgente").val("");
	jQuery("#agenteId").val(-1);
	jQuery("#descripcionBusquedaAgente").removeAttr("readonly");
}

function limpiarAgenteSiEsVacioAlInicio(){
	descripcionAgente = jQuery("#descripcionBusquedaAgente").val();
	if(descripcionAgente == " - "){
		jQuery("#descripcionBusquedaAgente").val("");
	}
}
