/**
 * Negocio Deducibles
 */

var sumasAseguradasGrid;
var sumasAseguradasProcessor;

function iniciaGridSumasAseguradas(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid) {	
	sumasAseguradasGrid = new dhtmlXGridObject(nombreGrid);	
	sumasAseguradasGrid.init();
	sumasAseguradasGrid.load(cargaElementosPath + "?idToNegCobPaqSeccion=" + dwr.util.getValue("idToNegCobPaqSeccion"));
	
	//Creacion del DataProcessor
	sumasAseguradasProcessor = new dataProcessor(accionSobreElementosPath + "?idToNegCobPaqSeccion=" + dwr.util.getValue("idToNegCobPaqSeccion"));
	sumasAseguradasProcessor.enableDataNames(true);
	sumasAseguradasProcessor.setTransactionMode("POST");
	sumasAseguradasProcessor.setUpdateMode("cell");
	sumasAseguradasProcessor.attachEvent("onAfterUpdate",refrescarGrid);
	sumasAseguradasProcessor.setVerificator(1, function(value,colName){
     return (value != "" && parseInt(value) >= 0? true : false);
	});	
	sumasAseguradasProcessor.defineAction("mensajeGenerico", response)
	sumasAseguradasProcessor.init(sumasAseguradasGrid);	
}
function response(node){
	parent.readResponseDataProcessor(node,iniciaGridSumasAseguradas);
	
}

function agregarDeducible() {
	sumasAseguradasGrid.addRow(null,",");
}

function eliminarDeducible() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sumasAseguradasGrid.deleteSelectedItem();
	}
}

/**
 * Smas aseguradas de Cobertura
 */
function iniciaGridSumasAseguradasCobertura() {
	iniciaGridSumasAseguradas('sumasAseguradasGrid', obtenerSumasAseguradasCobertura, accionSobreSumasAseguradasCobertura,'iniciaGridSumasAseguradasCobertura');	
}

function agregarSumaAsegurada() {
	sumasAseguradasGrid.addRow(null,",");
}

function eliminarSumaAsegurada() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		sumasAseguradasGrid.deleteSelectedItem();
	}
}
