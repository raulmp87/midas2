/**
 * 
 */
var targetDetalleNegocioTarifa = "detalleNegocioTarifa";
var idToNegTipoPolizaAct = -1;

function onChangeComboTipoPoliza(idToNegTipoPoliza){
	mostrarIndicadorCargaNegTarifa();
	idToNegTipoPolizaAct = idToNegTipoPoliza;
	//alert(idToNegTipoPolizaAct);
	if(idToNegTipoPolizaAct != -1){
		//alert("1");
		obtenerNegocioSecciones();
	}else{
		//alert("2");
		document.getElementById("detalleNegocioTarifa").innerHTML = '';
	}
	ocultarIndicadorCargaNegTarifa();
}

function obtenerNegocioSecciones(){
	sendRequestJQTarifa(null, obtenerNegocioSeccionesPath
			+ "?idToNegTipoPoliza=" + idToNegTipoPolizaAct,
			targetDetalleNegocioTarifa, 'cargaNegocioTarifas();');
}

function cargaNegocioTarifas(){
	if(document.negocioTarifaDetalleForm != null){
		//alert("cargado");
	}
}

function onChangeAgrupadorTarifa(idSeccion, idMoneda, idAgrupador,id){
	//id = id.substring(id.indexOf("_")+1,id.length);
	if(idAgrupador > 0){
		listadoService.getAgrupadorTarifaById(idAgrupador,function(data){
			var combo = document.getElementById('idToNegAgrupTarifaSeccionVersion_'+id);
			addOptions(combo,data);
		});
	}
}
/**
 * Obtiene las versiones de un agrupador de tarifa
 * @param idSeccion
 * @param idMoneda
 * @param colum
 * @returns {Boolean}
 */
function buscarOtrasVersiones(idSeccion, idMoneda, colum) {
	if (colum == null) {
		mostrarMensajeInformativo("Por favor selecciona un Agrupador valido",
				"10", null, null);
		return false;
	} else {
		var idAgrupador = dwr.util.getValue('idToNegAgrupTarifaSeccion_'
				+ colum);
		if (idAgrupador == '' || idAgrupador == -1) {
			mostrarMensajeInformativo(
					"Por favor selecciona un Agrupador valido", "10", null,
					null);
			return false;
		}
		onChangeAgrupadorTarifa(idSeccion, idMoneda, idAgrupador, colum);
	}
}
/**
 * Actualiza el agurpadorTarifaSeccion
 * @param idSeccion
 * @param idMoneda
 * @param colum
 * @returns {Boolean}
 */
function guardarVersion(idSeccion, idMoneda,colum){
	if (colum == null || colum == '') {
		mostrarMensajeInformativo("Por favor selecciona un Agrupador valido", "10",
				null, null);
		return false;
	} else {
		var idAgrupador = dwr.util
				.getValue('idToNegAgrupTarifaSeccion_' + colum);
		var idVersion = dwr.util.getValue('idToNegAgrupTarifaSeccionVersion_'
				+ colum);
		if (idAgrupador == '' || idVersion == '' || idVersion == -1
				|| idAgrupador == -1) {
			mostrarMensajeInformativo(
					"Por favor selecciona un Agrupador valido", "10", null,
					null);
			return false;
		}
		sendRequestJQ(null, relacionarNegocioTarifaPath + "?idToNegSeccion="
				+ idSeccion + "&idTcMoneda=" + idMoneda
				+ "&idToAgrupadorTarifa=" + idAgrupador
				+ "&idVerAgrupadorTarifa=" + idVersion, null,
				'validaGuardarModificarM1()');
	}
}

function guardarVersiones() {
	var error = false;
	if (validateAll(true, 'save')) {
		sendRequestJQ(null, relacionarNegocioTarifaPath + "?"+jQuery(document.negocioTarifaDetalleForm).serialize(), null,
		'validaGuardarModificarM1()');
	} else {
		mostrarMensajeInformativo("Por favor selecciona una Version valida",
				"10", null, null);
	}
}
/**
 * Se encarga de generar un JSON dinamico
 * @autor martin
 */
function generateJSON(arrayData) {
	var string = '{';
	for (var x = 0;x<arrayData.length;x++) {
		string += '"' + x + '"' + ':' + arrayData[x] + ',';
	}
	string = string.substring(0, string.length - 1);
	string += '}';
	try{
		var jsonObj = jQuery.parseJSON(string);
	}catch (error){
		alert(error.message);
	}
	
	return jsonObj;
}

function mostrarIndicadorCargaNegTarifa(){
	var newHtml = '<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Cargando la informaci&oacute;n, espere un momento por favor...</font>';
	if (document.getElementById('loadingIndicatorComps') !== null){
		if (document.getElementById('loadingIndicatorComps').innerHTML === ''){
			document.getElementById('loadingIndicatorComps').innerHTML = newHtml;
			document.getElementById('loadingIndicatorComps').style.display='block';
		}else
			document.getElementById('loadingIndicatorComps').style.display='block';
	}
}
function ocultarIndicadorCargaNegTarifa(){
if(document.getElementById('loadingIndicatorComps') !== null)
	document.getElementById('loadingIndicatorComps').style.display='none';
}