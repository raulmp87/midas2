/**
 * Negocio Derechos
 */

var derechosGrid;
var derechosEndosos;
var derechosPoliza;

var derechosProcessor;
var endosoProcessor;
var polizaProcessor;

function afterUpdateGridDerechos() {	
	mostrarMensajeExito();
}
function iniciaGridEndosos(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid){
	derechosEndosos = iniciaGridDerechos(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid);
	iniciaProcessor(derechosEndosos,accionSobreElementosPath,refrescarGrid,endosoProcessor);
	
}

function iniciaGridPoloiza(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid){
	derechosPoliza = iniciaGridDerechos(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid);
	iniciaProcessor(derechosPoliza,accionSobreElementosPath,refrescarGrid,polizaProcessor);
}

function iniciaGridDerechos(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid) {	
	derechosGrid = new dhtmlXGridObject(nombreGrid);			
	derechosGrid.init();
	derechosGrid.attachEvent("onEditCell",doOnEditCell);
	derechosGrid.load(cargaElementosPath + "?idToNegocio=" + dwr.util.getValue("idToNegocio") + "&esAltaInciso=" + dwr.util.getValue("esAltaInciso"));
	return derechosGrid;
}

function doOnEditCell(stage,rowId,cellIndex,newValue,oldValue){
	if(stage==1&&cellIndex==1){ /*here INDEX is column index*/
		derechosGrid.editor.obj.onkeypress = function(){
			var valueStr = derechosGrid.editor.obj.value + "";
			if(valueStr === "") return true;
			return (valueStr.length < 13);
		}
	}
 }

function iniciaProcessor(grid,accionSobreElementosPath,refrescarGrid,derechosProcessor){
	//Creacion del DataProcessor
	derechosProcessor = new dataProcessor(accionSobreElementosPath + "?idToNegocio=" + dwr.util.getValue("idToNegocio")+ "&esAltaInciso=" + dwr.util.getValue("esAltaInciso"));
	derechosProcessor.enableDataNames(true);
	derechosProcessor.setTransactionMode("POST");
	derechosProcessor.setUpdateMode("cell");
	derechosProcessor.setVerificator(1, function(value,colName){
		if(value === '') return false;
		
		
		var valueStr = value + "";
		var enterosDecimales = valueStr.split(".");
		var enteros = enterosDecimales[0];
		if(enteros != null && enteros != undefined){
			if(enteros.length > 10) return false;
		}
		var decimales = enterosDecimales[1];
		if(decimales != null && decimales != undefined){
			if(decimales.length > 2) return false;
		}
		
		if (parent.activeCell == "endosoNeg2") {
			return value >= 0;
		}
		return value > 0;
	});	
	derechosProcessor.attachEvent("onAfterUpdate",refrescarGrid);
    document.getElementById('b_agregar').style.display = 'inline';
	// Muestra el mensaje de exito / error
    derechosProcessor.defineAction("result", response);
	derechosProcessor.init(grid);
}

function response(node){
	parent.mostrarMensajeInformativo(node.firstChild.data, node.getAttribute("tipoMensaje"), null, null);
return false;
}
function agregarDerecho() { 
		var grid = null;
	if (parent.activeCell == "endosoNeg2" || parent.activeCell == "endosoNeg4") {
		grid = derechosEndosos;
	} else if (parent.activeCell == "polizaNeg3") {
		grid = derechosPoliza;
	}
	if (grid != null) {
		var j = 0;
		j++;
		if (j <= 1) {
			grid.addRow(null, "0,");
			document.getElementById('b_agregar').style.display = 'none';
		}
	}else{
		parent.mostrarMensajeInformativo("Ocurrio un error inesperado", "10");
	}
}	 

function eliminarDerecho() {
	var grid = null;
	if (parent.activeCell == "endosoNeg2" || parent.activeCell == "endosoNeg4") {
		grid = derechosEndosos;
	} else if (parent.activeCell == "polizaNeg3") {
		grid = derechosPoliza;
	}
	if (grid != null){
		if (grid.selectedRows == '' || grid.selectedRows == null) {
			parent.mostrarMensajeInformativo(
					"Para eliminar un registro debe seleccionarlo primero. ",
					"10", null, null);
		} else {
			document.getElementById('b_agregar').style.display = 'inline';
			grid.deleteSelectedItem();
		}
	}else{
		parent.mostrarMensajeInformativo("Ocurrio un error inesperado", "10");
	}
		
}

/**
 * Derechos de Endoso
 */
function iniciaGridDerechosEndoso() {	
	iniciaGridEndosos('derechosEndosoGrid', obtenerDerechosEndosoPath, accionSobreDerechosEndosoPath, 'iniciaGridDerechosEndoso');	
}

/**
 * Derechos de Poliza
 */
function iniciaGridDerechosPoliza() {
	iniciaGridPoloiza('derechosPolizaGrid', obtenerDerechosPolizaPath, accionSobreDerechosPolizaPath,'iniciaGridDerechosPoliza');	
}

/*function negocioDerechosEndoso() {
	sendRequestJQ(null,'/MidasWeb/negocio/derechos/mostrarDerechosEndoso.action?idToNegocio=1', targetWorkArea , 'iniciaGridDerechosEndoso()');
}

function negocioDerechosPoliza() {
	sendRequestJQ(null,'/MidasWeb/negocio/derechos/mostrarDerechosPoliza.action?idToNegocio=1', targetWorkArea , 'iniciaGridDerechosPoliza()');
}*/

