/*********************************************************
 * @author 	Eduardo Chavez
 * @since 	2015-10-15
 *********************************************************/
var gridEmailSAPAMIS;
var gridAlertasSAPAMIS;
var gridEmailConfig;
var comboTipoUsuario;
var subGrids = [];
var recienAgregado = false;
function inicializarSapAmisEmail(){
	jQuery("#agregarEmailALertas").fadeOut();
	jQuery("#listadoEmailConfig").fadeOut();
	jQuery("#listadoCorreosEnviados").fadeOut();
	opcionAgregarCorreosAlertas();
}

function opcionAgregarCorreosAlertas(){
	jQuery("#labelSapAmisEmailTitulo").html("Email Alértas SAP-AMIS");
	jQuery("#labelSapAmisEmailSubtitulo").html("Es necesario registrar las direcciones de correo para las Alertas del SAP-AMIS");
	jQuery("#agregarEmailALertas").fadeIn();
	jQuery("#listadoEmailConfig").fadeOut();
	jQuery("#listadoCorreosEnviados").fadeOut();
	obtenerListaEmail();
	obtenerListaAlertas();
}

function opcionConfigurarCorreosAlertas(){
	//alert('opcionConfigurarCorreosAlertas');
	jQuery("#agregarEmailALertas").fadeOut();
	jQuery("#listadoCorreosEnviados").fadeOut();
	jQuery("#listadoEmailConfig").fadeIn();
	obtenerListaEmailNegocioConfig();
}

function obtenerListaEmail(){
	document.getElementById("listadoEmail").innerHTML = '';
	gridEmailSAPAMIS = new dhtmlXGridObject('listadoEmail');
	gridEmailSAPAMIS.enableColSpan(true);
	gridEmailSAPAMIS.load(listarEmailsPath);
	gridEmailSAPAMIS.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		if(stage === 2 && cInd === 1){
			//alert('stage: ' + stage + ', rId: ' + rId + ', cInd: ' + cInd + ', nValue: ' + nValue + ', oValue: ' + oValue);
			if(valEmail(nValue)){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	});		
}

function obtenerListaEmailNegocioConfig(){
	document.getElementById("listadoEmailConfig").innerHTML = '';
	gridEmailConfig = new dhtmlXGridObject('listadoEmailConfig');
	
	gridEmailConfig.attachEvent("onXLE", function(grid){
		var xml = [];
		var i = 0;
		gridEmailConfig.forEachRow(function(id){
			xml[i] = gridEmailConfig.cells(id,3).getValue().replace("encabezado","head").replace("encabezado","head");
			//console.log(xml[i]);
			gridEmailConfig.cells(id,3).setValue("<div id='row" + id + "' style='margin-top:15px'></div>");
			i++;
		});
		gridEmailConfig.setColTypes("ro,ro,ro");
		for(var x=0; x<xml.length; x++){
			subGrids[x] = new dhtmlXGridObject("row" + (x+1));
			subGrids[x].attachEvent("onXLE", function(grid){
				var opciones = subGrids[x].cells("nueva",0).getValue();
				//console.log(opciones);
				//console.log("Opciones.length="+opciones.length);
				opciones = opciones.substring(0,opciones.length - 3);
				//console.log("OpcionesProcesada:::"+opciones);
				//console.log("TotalOpciones:::"+opciones.split("|_|").length)
				var cadenaHtml = "<select id='select_" + (x+1) + "' style='width:100%'>";
				for(var i = 0; i<opciones.split("|_|").length; i++) {
					cadenaHtml += "<option value='" + opciones.split("|_|")[i].split("|@|")[0] + "'>" + opciones.split("|_|")[i].split("|@|")[1] + "</option>"
				}
				cadenaHtml += "</select>";
				//console.log("CadenaHtmls:::" + cadenaHtml);
				subGrids[x].cells("nueva",0).setValue(cadenaHtml);
				document.getElementById("row" + (x+1)).style.height = "auto";
				jQuery("#row" + (x+1) +" > .xhdr").css("height","0px");
				jQuery("#row" + (x+1) +" > .objbox").css("height","auto");
				jQuery("#row" + (x+1) +" > .objbox").css("background","transparent");				
			});
			subGrids[x].init();
			subGrids[x].parse(xml[x]);
		}
	});
	
	//console.log(listarEmailsConfigPath);
	gridEmailConfig.load(listarEmailsConfigPath);
	
	
	
}

function obtenerListaAlertas(){
	document.getElementById("listadoAlertas").innerHTML = '';
	gridAlertasSAPAMIS = new dhtmlXGridObject('listadoAlertas');
	gridAlertasSAPAMIS.load(listarAlertasPath);
	//gridAlertasSAPAMIS.groupBy(1);
	//gridAlertasSAPAMIS.collapseGroup(1);
}

function guardarEmailNegocio(idEmail, email, emailTipoUsuario, estatus) {
		var idEmail = gridEmailSAPAMIS.cellById(gridEmailSAPAMIS.getSelectedRowId(), 0).getValue().trim();
		var email = gridEmailSAPAMIS.cellById(gridEmailSAPAMIS.getSelectedRowId(), 1).getValue().trim();
		var emailTipoUsuario = gridEmailSAPAMIS.cellById(gridEmailSAPAMIS.getSelectedRowId(), 2).getText().trim();
		
	var path='/MidasWeb/negocio/sapamisemail/guardarEmailNegocio.action?idEmailNegocioParam='+idEmail+'&emailParam='+email+'&tipoUsuarioParam='+emailTipoUsuario;
		
	if(estatus==3){
		path+='&estatusEmailNegocioParam=0';
	} else {
		path+='&estatusEmailNegocioParam='+estatus;
	}
		
	sendRequestJQTarifa(null, path, 'guardarEmailNegocioDiv', null);
	var respuesta = jQuery("#guardarEmailNegocioDiv").html().replace(/(?:\r\n|\r|\n)/g, '');
	jQuery("#guardarEmailNegocioDiv").html("");
	
	if(respuesta.split("|")[0]=="OK"){
		if(estatus==1){
			gridEmailSAPAMIS.deleteSelectedRows();
		} else if(estatus==3){
			var index = gridEmailSAPAMIS.getRowIndex("nueva");
			
			var data = [
			            respuesta.split("|")[1],
			            email,
			            emailTipoUsuario,
			            "/MidasWeb/img/icons/ico_guardar.gif^Guardar^javascript:guardarEmailNegocio(0,0,0,0);^_self",
			            "/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript:guardarEmailNegocio(0,0,0,1);^_self"
			];
			
			//console.log(data);
			
			gridEmailSAPAMIS.addRow(respuesta.split("|")[1],data,index);
			
			gridEmailSAPAMIS.cells("nueva",1).setValue("");
			gridEmailSAPAMIS.cells("nueva",2).setValue("");
		}
	}
	

}

function guardarEmailNegocioConf(idNegocioParam,idAlertaEmailNegocioParam,
		idEmailNegocioConfigParam,sapAmisAlertasParam,
		estatusEmailNegocioConfigParam,idGrid) {
	
	//console.log(idGrid);
	var idRow = subGrids[idGrid - 1].getSelectedRowId();
	var valueSelected = jQuery("#select_" + idGrid).val();

	// idEmail = gridEmailSAPAMIS.cellById(gridEmailSAPAMIS.getSelectedRowId(), 0).getValue().trim();
	var path='/MidasWeb/negocio/sapamisemail/guardarEmailNegocioConfig.action?'+
		'idNegocioParam='+idNegocioParam+
		'&idAlertaEmailNegocioParam='+idAlertaEmailNegocioParam+
		'&idEmailNegocioConfigParam='+idEmailNegocioConfigParam+
		'&catAlertasSapAmisParam='+sapAmisAlertasParam+
		'&idEmailNegocioParam='+valueSelected+
		'&estatusEmailNegocioConfigParam='+estatusEmailNegocioConfigParam;
	
	//console.log('guardarEmailNegocioConfig path: ' + path);
	sendRequestJQTarifa(null, path, 'guardarEmailNegocioDiv', null);
	
	var respuesta = jQuery("#guardarEmailNegocioDiv").html().replace(/(?:\r\n|\r|\n)/g, '');
	jQuery("#guardarEmailNegocioDiv").html("");
	
	//console.log("Respuesta+"+respuesta);
	
	if(respuesta.split("|")[0]=="OK") {
		
		//console.log("idRow="+idRow);
		if(idRow == "nueva"){
			var index = subGrids[idGrid - 1].getRowIndex("nueva");
			
			//console.log("Index:::"+index);
			
			var data = [
			            jQuery("#select_" + idGrid + " option[value='" + valueSelected + "'").text(),
			            "/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^" +
			            "javascript:guardarEmailNegocioConf("+idNegocioParam+"," +
			            ""+idAlertaEmailNegocioParam+","+respuesta.split("|")[1]+", \"" +  sapAmisAlertasParam + "\"g,1,"+idGrid+");^_self"
			];
			subGrids[idGrid - 1].addRow(respuesta.split("|")[1],data,index);
		} else {
			subGrids[idGrid - 1].deleteSelectedRows();
		}	
	}
}

function guardarEmailNegocioAlertas(idAlertaEmailNegocioParam, estatus){
	
	var idBase						= gridAlertasSAPAMIS.cellById(gridAlertasSAPAMIS.getSelectedRowId(), 0).getValue().trim();
	var sapAmisSistemaParam 		= gridAlertasSAPAMIS.cellById(gridAlertasSAPAMIS.getSelectedRowId(), 1).getValue().trim();
	var sapAmisAlertasParam 		= gridAlertasSAPAMIS.cellById(gridAlertasSAPAMIS.getSelectedRowId(), 2).getValue().trim();
	var selectedId=gridAlertasSAPAMIS.getSelectedRowId();
	gridAlertasSAPAMIS.deleteSelectedRows();
	var usar = "";
	if(estatus==0) {
		usar = '/MidasWeb/img/icons/check_in.png^Desactivar^javascript:guardarEmailNegocioAlertas(' + idAlertaEmailNegocioParam + ', 1);^_self';
	} else {
		usar = '/MidasWeb/img/icons/check_out.png^Activar^javascript:guardarEmailNegocioAlertas(0, 0);^_self';
	}
	var data = [
			idBase,sapAmisSistemaParam,sapAmisAlertasParam,usar
	];
	gridAlertasSAPAMIS.addRow(selectedId,data,selectedId);
	var path=	'/MidasWeb/negocio/sapamisemail/guardarEmailNegocioAlertas.action?'
		+'idAlertaEmailNegocioParam='+idAlertaEmailNegocioParam
		+'&catAlertasSapAmisParam='+sapAmisAlertasParam
		+'&estatusEmailNegocioConfigParam='+estatus;
	sendRequestJQ(null, path, null, null);
}

/**
 * 24/Nov/2015
 * Se agrega para la nueva pantalla de log de correos
 * @author Luis Ibarra
 */
function opcionConsultarCorreosEnviados(){
	var enviadosGrid = new dhtmlXGridObject("listadoCorreosEnviadosGrid");
	var path='/MidasWeb/negocio/sapamisemail/cargarCorreosEnviados.action?idNegocioParam='+jQuery("#idNegocioSapAmis").val();
	enviadosGrid.init();
	enviadosGrid.load(path);
	jQuery("#agregarEmailALertas").fadeOut();
	jQuery("#listadoEmailConfig").fadeOut();
	jQuery("#listadoCorreosEnviados").fadeIn();
}
