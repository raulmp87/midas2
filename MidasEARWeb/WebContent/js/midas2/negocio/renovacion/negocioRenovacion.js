/**
 * 
 */
var condRenCoberturasGrid;
var condRenCoberturasProcessor;

function desplegarRiesgosNormales(){
	limpiarDivsGeneral();	
	sendRequestJQ(null, condicionesRenovacionPath + "?condicionTipo=1&idNegocio="+jQuery("#idNegocio").val(),"contenido_riesgosNormales", null);
}

function desplegarAltoRiesgo(){
	limpiarDivsGeneral();
	sendRequestJQ(null, condicionesRenovacionPath + "?condicionTipo=2&idNegocio="+jQuery("#idNegocio").val(),"contenido_altoRiesgo", null);
}

function desplegarAltaFrecuencia(){
	limpiarDivsGeneral();
	sendRequestJQ(null, condicionesRenovacionPath + "?condicionTipo=3&idNegocio="+jQuery("#idNegocio").val(),"contenido_altaFrecuencia", null);
}

function validaCondicionesRenovacion(){
	var valido = true;
	
	if(jQuery("#pctTarifa").val() > 100 || jQuery("#valorDescuentoComercial").val() > 100 ||
			jQuery("#pctLimiteIncremento").val() > 100){
		valido = false;
	}
	if(valido){
		if(jQuery("#aplicaDeducibleDanos").is(":checked")){
			jQuery(".deduciblesDanos").find("input[type=text]").each(function(){
				if(jQuery(this).val() > 100){
					valido = false;
				}
			});
		}
	}
	if(valido){
		if(jQuery("#aplicaDescuentoNoSinietro").is(":checked")){
			jQuery(".descuentoNoSiniestro").find("input[type=text]").each(function(){
				if(jQuery(this).val() > 100) valido = false;
			});
		}
	}
	
	return valido;
}

function guardaCondicionRenovacion(){
	if(!validaCondicionesRenovacion()){
		mostrarVentanaMensaje('10', "No puede haber porcentajes mayores a 100.");
		return;
	}
	
	var targetContenido = "";
	if(jQuery("#condicionTipo").val() == 1){
		targetContenido = "contenido_riesgosNormales";
	}
	if(jQuery("#condicionTipo").val() == 2){
		targetContenido = "contenido_altoRiesgo";
	}
	if(jQuery("#condicionTipo").val() == 3){
		targetContenido = "contenido_altaFrecuencia";
	}
	
	var descuentoMax = jQuery("#descuentoMax").val();
	if(parseFloat(jQuery("#valorDescuentoComercial").val()) > parseFloat(descuentoMax)){
		mostrarVentanaMensaje('10', "Descuento Comercial no debe ser mayor que Descuento Maximo del Negocio (" + descuentoMax + ").");
		jQuery("#valorDescuentoComercial").val(descuentoMax);
	}else {
		sendRequestJQ(jQuery("#condicionesRenovacionForm"), guardarCondicionesRenovacionPath,targetContenido, null);
	}
}

function initCondicionesRenovacion(){
	cambiaDeducibleDanos();
	cambiaDescuentoNoSiniestro();
	jQuery("input[type$='checkbox']").each(function(){
		onChangeCondicionRenovacion(this, true);
	});
}

function cambiaDeducibleDanos(){
	if(jQuery("#aplicaDeducibleDanos").is(":checked")){
		jQuery(".deduciblesDanos").find("input").removeAttr("disabled");
	}else{
		jQuery(".deduciblesDanos").find("input").attr("disabled","disabled");
	}	
}

function cambiaDescuentoNoSiniestro(){
	if(jQuery("#aplicaDescuentoNoSinietro").is(":checked")){
		jQuery(".descuentoNoSiniestro").find("input").removeAttr("disabled");
	}else{
		jQuery(".descuentoNoSiniestro").find("input").attr("disabled","disabled");
	}	
}

function onChangeCondicionRenovacion(object, esInit){
	if(jQuery(object).attr("id") == "mantieneDescuentoComercial"){
		if(esInit != true){
			jQuery("#otorgaDescuentoComercial").removeAttr("checked");
			jQuery("#mantieneDescuentoComercial").attr("checked","checked");
		}
		if(jQuery("#mantieneDescuentoComercial").is(":checked")){
			jQuery("#valorDescuentoComercial").attr("disabled","disabled");
		}
	}
	if(jQuery(object).attr("id") == "otorgaDescuentoComercial"){
		if(esInit != true){
			jQuery("#otorgaDescuentoComercial").attr("checked","checked");
			jQuery("#mantieneDescuentoComercial").removeAttr("checked");
		}
		if(jQuery("#otorgaDescuentoComercial").is(":checked")){
			jQuery("#valorDescuentoComercial").removeAttr("disabled");
		}
	}
	if(jQuery(object).attr("id") == "detenerNumeroIncisos"){		
		if(jQuery("#detenerNumeroIncisos").is(":checked")){
			jQuery("#numeroIncisos").removeAttr("disabled");
		}else{
			jQuery("#numeroIncisos").attr("disabled","disabled");
		}		
	}
	if(jQuery(object).attr("id") == "aumentarPctTarifa"){
		if(jQuery("#aumentarPctTarifa").is(":checked")){
			jQuery("#pctTarifa").removeAttr("disabled");
		}else{
			jQuery("#pctTarifa").attr("disabled","disabled");
		}		
	}
	if(jQuery(object).attr("id") == "limiteIncrementoTarifa"){
		if(jQuery("#limiteIncrementoTarifa").is(":checked")){
			jQuery("#pctLimiteIncremento").removeAttr("disabled");
		}else{
			jQuery("#pctLimiteIncremento").attr("disabled","disabled");
		}		
	}
	if(jQuery(object).attr("id") == "sinLimiteSinietros"){
		if(jQuery("#sinLimiteSinietros").is(":checked")){
			jQuery("#limiteSiniestros").removeAttr("disabled");
		}else{
			jQuery("#limiteSiniestros").attr("disabled","disabled");
		}		
	}
}

function validaDescuentoComercial(){
	if(jQuery("#valorDescuentoComercial").val() == ""){
		jQuery("#valorDescuentoComercial").val(0);
	}
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_riesgosNormales');
	limpiarDiv('contenido_altoRiesgo');
	limpiarDiv('contenido_altaFrecuencia');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}



function obtenerCoberturasCondRenovacion(){		
	
	var idToNegSeccion = jQuery("#idToNegSeccion").val();
	var condicionTipo = jQuery("#condicionTipo").val();
	var idNegocio = jQuery("#idNegocio").val();
	
	condRenCoberturasGrid = new dhtmlXGridObject('condRenCoberturasGrid');	
	condRenCoberturasGrid.load(mostrarCoberturasCondRenPath+"?idToNegSeccion="+idToNegSeccion+"&condicionTipo="+condicionTipo+"&idNegocio="+idNegocio);
	
	
	condRenCoberturasProcessor = new dataProcessor(guardarCoberturasCondRenPath+"?idToNegSeccion="+idToNegSeccion+"&condicionTipo="+condicionTipo+"&idNegocio="+idNegocio);	
	condRenCoberturasProcessor.enableDataNames(true)
	condRenCoberturasProcessor.setTransactionMode("POST");
	condRenCoberturasProcessor.setUpdateMode("cell");
	condRenCoberturasProcessor.attachEvent("onAfterUpdate",refrescarCoberturasCondRenovacion);
	condRenCoberturasProcessor.defineAction("mensajeGenerico", responseNR);
	
	condRenCoberturasProcessor.init(condRenCoberturasGrid);
}

function refrescarCoberturasCondRenovacion(sid,action,tid,node){
	obtenerCoberturasCondRenovacion();
	return true;
}

function responseNR(node){
	mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipo")); 
	obtenerCoberturasCondRenovacion();
	return false;
}
