/**
 * 
 */
var negocioUsuarioEdicionImpresionAsociadoGrid;
var negocioUsuarioEdicionImpresionDisponibleGrid;

function initGridsNegocioUsuarioEdicionImpresion(){
	jQuery("#negocioUsuarioEdicionImpresionDisponibleGrid").empty();
	getUsuariosEdicionImpresionDisponibles();
	getUsuariosEdicionImpresionAsociados();
}

function getUsuariosEdicionImpresionDisponibles( idNegocio ){
	negocioUsuarioEdicionImpresionDisponibleGrid = new dhtmlXGridObject('negocioUsuarioEdicionImpresionDisponibleGrid');
	negocioUsuarioEdicionImpresionDisponibleGrid.load( "/MidasWeb/negocio/impresion/obtenerUsuariosDisponibles.action");
}


function getUsuariosEdicionImpresionAsociados(){
	negocioUsuarioEdicionImpresionAsociadoGrid = new dhtmlXGridObject('negocioUsuarioEdicionImpresionAsociadoGrid');
	negocioUsuarioEdicionImpresionAsociadoGrid.load("/MidasWeb/negocio/impresion/obtenerUsuariosAsociados.action");
	// Creacion del DataProcessor
	var url = "/MidasWeb/negocio/impresion/relacionarUsuario.action";;
	negocioUsuarioEdicionImpresionProcessor = new dataProcessor(url);
	negocioUsuarioEdicionImpresionProcessor.enableDataNames(true);
	negocioUsuarioEdicionImpresionProcessor.enableUTFencoding(false);
	negocioUsuarioEdicionImpresionProcessor.setTransactionMode("POST");
	negocioUsuarioEdicionImpresionProcessor.setUpdateMode("cell");
	negocioUsuarioEdicionImpresionProcessor.attachEvent("onRowMark", function(){
		restart();
	});
	negocioUsuarioEdicionImpresionProcessor.init(negocioUsuarioEdicionImpresionAsociadoGrid);
}


function desasociarTodas(){
	var url = "/MidasWeb/negocio/impresion/desasociarTodas.action";
	sendRequestJQ(null,url,	null, "restart();");
}

function asociarTodas(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var url="";
	
	url+="/MidasWeb/negocio/impresion/asociarTodas.action";
	
	sendRequestJQ(null,url,	null, "restart();");
	
}

function restart(){
	jQuery("#contenido_negocioUsuarioEdicionImpresion").empty();
	verNegocioUsuarioEdicionImpresion();
}