/**
 * Negocio Deducibles
 */

var deduciblesGrid;
var deduciblesProcessor;

function afterUpdateGridDerechos() {	
	mostrarMensajeExito();
}

function iniciaGridDeducibles(nombreGrid, cargaElementosPath, accionSobreElementosPath, refrescarGrid) {	
	deduciblesGrid = new dhtmlXGridObject(nombreGrid);	
	deduciblesGrid.init();
	deduciblesGrid.load(cargaElementosPath + "?idToNegCobPaqSeccion=" + dwr.util.getValue("idToNegCobPaqSeccion"));
	
	//Creacion del DataProcessor
	deduciblesProcessor = new dataProcessor(accionSobreElementosPath + "?idToNegCobPaqSeccion=" + dwr.util.getValue("idToNegCobPaqSeccion"));
	deduciblesProcessor.enableDataNames(true);
	deduciblesProcessor.setTransactionMode("POST");
	deduciblesProcessor.setUpdateMode("cell");
	deduciblesProcessor.attachEvent("onAfterUpdate",refrescarGrid);
	deduciblesProcessor.setVerificator(1, function(value,colName){
     return (value != "" && parseInt(value) >= 0? true : false);
	});	
	deduciblesProcessor.defineAction("mensajeGenerico", response)
	deduciblesProcessor.init(deduciblesGrid);	
}
function response(node){
	parent.readResponseDataProcessor(node,iniciaGridDeducibles);
	
}

function agregarDeducible() {
	deduciblesGrid.addRow(null,",");
}

function eliminarDeducible() {
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		deduciblesGrid.deleteSelectedItem();
	}
}

/**
 * Deducibles de Cobertura
 */
function iniciaGridDeduciblesCobertura() {
	iniciaGridDeducibles('deduciblesGrid', obtenerDeduciblesCobertura, accionSobreDeduciblesCobertura,'iniciaGridDeduciblesCobertura');	
}

