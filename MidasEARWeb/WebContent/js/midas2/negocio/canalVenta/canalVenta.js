/**
 * 
 */

var canalVentaCamposGrid;

/**
 * Inicializa grids
 */
function initGridCanalVenta() {

	jQuery("#canalVentaCamposGrid").empty();

	getCanalVentaNegocioCampos();


}

/**
 * Crea grid y obtiene disponibles
 */
function getCanalVentaNegocioCampos() {
	canalVentaCamposGrid = new dhtmlXGridObject('canalVentaCamposGrid');
    canalVentaCamposGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
    canalVentaCamposGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	canalVentaCamposGrid.init();
    canalVentaCamposGrid.load(URL_MOSTRAR_CAMPOS);
    canalVentaCamposGrid.attachEvent("onCheck", function(rId, cInd, state,
			oValue) {
    	sendRequestJQ(null, URL_GUARDAR_CAMPOS+'?idCampoEditado='+rId,	null, 'getCanalVentaNegocioCampos()');

});
    
}

/**
 */
function hacerVisiblesTodos() {
	sendRequestJQ(null, URL_TODOS_VISIBLES+'?activo=true', null, 'getCanalVentaNegocioCampos()');
}

/**
 */
function hacerInvisiblesTodos() {
	sendRequestJQ(null, URL_TODOS_INVISIBLES+'?activo=false', null, 'getCanalVentaNegocioCampos()');
}



