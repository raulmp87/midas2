/**
 * 
 */

var tiposVigenciaDisponiblesGrid,
	tiposVigenciaAsociadosGrid;

function initGridsTiposVigencia(){
	jQuery("#tiposVigenciaDisponiblesGrid").empty();
	getTiposVigenciaDisponibles();
	getTiposVigeciaAsociadas();
	declaraEventosGrids();
}

function declaraEventosGrids(){	
	tiposVigenciaDisponiblesGrid.attachEvent("onDrag", function(dId,tId,sObj,tObj){		
		relacionarVigencia(dId, "deleted", tiposVigenciaAsociadosGrid);
	});
	
	tiposVigenciaAsociadosGrid.attachEvent("onCheck", function(rId,cInd,state){
		relacionarVigencia(rId, "updated", tiposVigenciaAsociadosGrid);
	});
	
	tiposVigenciaAsociadosGrid.attachEvent("onDrag", function(dId,tId,sObj,tObj){		
		relacionarVigencia(dId, "inserted", tiposVigenciaDisponiblesGrid);
	});
}

function getTiposVigenciaDisponibles(){	
	tiposVigenciaDisponiblesGrid = new dhtmlXGridObject('tiposVigenciaDisponiblesGrid');
	tiposVigenciaDisponiblesGrid.load(URL_LISTAR_VIGENCIAS_DISPONIBLES);
}

function getTiposVigeciaAsociadas(){	
	tiposVigenciaAsociadosGrid = new dhtmlXGridObject('tiposVigenciaAsociadasGrid');	
	tiposVigenciaAsociadosGrid.load(URL_LISTAR_VIGENCIAS_ASOCIADAS);
}

function relacionarVigencia(rId, accion, grid){	
	var negocioTipoVigencia = {
			"negocioTipoVigencia.id": grid.cellById(rId, 0).getValue(),
			"negocioTipoVigencia.esDefault" : grid.cellById(rId, 5).getValue(),
			"accion" : accion,
			"negocioTipoVigencia.negocio.idToNegocio":grid.cellById(rId, 1).getValue(),
			"negocioTipoVigencia.tipoVigencia.id" : grid.cellById(rId, 2).getValue(),
			"negocioTipoVigencia.tipoVigencia.descripcion": grid.cellById(rId, 3).getValue(),
			"negocioTipoVigencia.tipoVigencia.dias": grid.cellById(rId, 4).getValue(),	
	};	
	
	var ajaxVigencia = jQuery.ajax({
        url: URL_RELACIONAR_VIGENCIA,	          
        dataType: 'json',
        async:false,
        type:"POST",
        data: negocioTipoVigencia,
        success: function(json){        	
        	 var msj = json.mensaje;	          	 
        	 if(msj != null && msj !=''){	          		
        		 mostrarMensajeInformativo(msj, json.tipoMensaje);	          		
        	 }
        	 initGridsTiposVigencia();
        },
        error : function(data) {        	
        	initGridsTiposVigencia();
		},
        beforeSend: function(){
			blockPage();
		},
		complete: function(){
			unblockPage();	
		}
  });
}

function mostrarVentanaAgregar(){	
	mostrarVentanaModal("tipovigencia", "Agregar Tipo de vigencia", 200,320, 500, 200, URL_MOSTRAR_VENTANA, "");
}

function cerrarVentanaAgregar(){
	cerrarVentanaModal('tipovigencia');	
	initGridsTiposVigencia();
}

function asociarTodas(){
	sendRequestJQ(null, URL_ASOCIAR_TODAS, 'contenido_tipoVigencia', null);
}

function desasociarTodas(){
	sendRequestJQ(null, URL_DESASOCIAR_TODAS, null, 'mostrarContenedorTiposVigencia()');
}

function eliminarTipoVigencia(idVigencia){
	if(confirm("\u00BFEst\u00e1 seguro de Eliminar el Tipo de vigencia?")){	
		sendRequestJQ(null, URL_ELIMINAR_VIGENCIA + '?idVigencia='+idVigencia, 'contenido_tipoVigencia', null);
	}
}

function desasociarVigencia(idVigencia){
	sendRequestJQ(null, URL_DESASOCIAR_UNA + '?idNegocioTipoVigencia='+idVigencia, null, 'mostrarContenedorTiposVigencia()');
}
