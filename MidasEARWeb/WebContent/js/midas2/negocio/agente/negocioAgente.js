var gridAgentesAsociados;
var negocioAgenteProcessor;
var insertedAction = 'inserted';
var deletedAction = 'deleted';

function inicializarAgentesAsociados(){
	document.getElementById("listadoAgentesAsociados").innerHTML = '';
	gridAgentesAsociados = new dhtmlXGridObject('listadoAgentesAsociados');
	gridAgentesAsociados.attachEvent("onXLE", function(grid_obj,count){
		var total = grid_obj.getRowsNum();
		if(total > 0){
			jQuery("#labelValidaAgentes").html("Este negocio opera para los siguientes agentes \u00Fanicamente ");
		}else{
			jQuery("#labelValidaAgentes").html("Este negocio opera para todos los agentes ");
		}
	}); 
	gridAgentesAsociados.load(listarAgentesAsociadosPath );
	
	negocioAgenteProcessor = new dataProcessor(relacionarNegocioAgentePath);
	negocioAgenteProcessor.enableDataNames(true);
	negocioAgenteProcessor.setTransactionMode("POST");
	negocioAgenteProcessor.setUpdateMode("cell");
	negocioAgenteProcessor.setVerificator(1, function(value,colName){
		return value > 0;
	});	
	negocioAgenteProcessor.attachEvent("onAfterUpdate", refrescarGrids);
	negocioAgenteProcessor.init(gridAgentesAsociados);
		
}

function refrescarGrid(){
	inicializarAgentesAsociados();
}


function agregarNegocioAgente() { 
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?idField=idNuevoAgente&tipoAccion=consultaNegAge";
	sendRequestWindow(null,url,obtenerAgenteModal);
	
}	 

function obtenerAgenteModal(){
    var wins = obtenerContenedorVentanas();
    ventanaResponsable= wins.createWindow("agenteModal", 400, 320, 900, 450);
    ventanaResponsable.center();
    ventanaResponsable.setModal(true);
    ventanaResponsable.setText("Consulta de agentes");
    return ventanaResponsable;
}

function relacionarNegocioAgentes(idsAgentes, accion){
	var targetId = 'contenido_agentes';
	var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";
	jQuery.ajax({
		type : 'POST',
		url : relacionarNegocioAgentePath,
		data : {'idsRelacionar' : idsAgentes, 'accionAgente' : accion},
		dataType: "html",
		async: true,
		contentType: defaultContentType,
		success: function(data) {
			if(targetId !== undefined && targetId !== null){
				jQuery("#" + targetId).html("");
				jQuery("#" + targetId).html(data);	    		
			}
		} 	    
	});
}

function registrarNuevoAgente(idsAgentes){
	relacionarNegocioAgentes(idsAgentes,insertedAction);
}

function eliminarNegocioAgente() {
	var ids = gridAgentesAsociados.getCheckedRows(7);
	var idNegAgente ="";
	if (ids!="")
	{
		var countSelected = ids.split(",");
		if (countSelected.length > 0) {
			if(confirm('\u00BFEsta seguro de eliminar la relacion ?')){
				for (i = 1; i <= countSelected.length; i++) {
					if (i != countSelected.length) {
						idNegAgente  += gridAgentesAsociados.cellById(countSelected[i - 1], 0)
						.getValue()
						+ ",";
					} else {
						idNegAgente  += gridAgentesAsociados.cellById(countSelected[i - 1], 0)
						.getValue();
					}
				}
			}
		}
//		var idNegAgente = getIdFromGrid(gridAgentesAsociados, 0);
		relacionarNegocioAgentes(idNegAgente,deletedAction);
	}else{
		parent.mostrarMensajeInformativo("Para eliminar un registro debe seleccionarlo primero. ", "10", null, null);
	}
}


function cancelarNegocioAgente(){
	var mensaje = "\u00BFEst\u00E1 seguro de hacer disponible para todo agente?, se perder\u00E1 la configuraci\u00F3n de agentes si ya ha definido alguna.";
	if(confirm(mensaje)){
		sendRequestJQ(null, '/MidasWeb/negocio/agente/eliminarRelacion.action', 'contenido_agentes', null);		
	}
}



