/**
 *
 */

var negocioEstadoDescuentoAsociadosGrid;
var  negocioEstadoDescuentoProcessor;
/**
 *
 */

function obtenerNegocioEstadoDescuentoAsociados(){

	negocioEstadoDescuentoAsociadosGrid = new dhtmlXGridObject('negocioEstadoDescuentoAsociadosGrid');
	negocioEstadoDescuentoAsociadosGrid.attachEvent("onXLE", function(grid_obj,count){
		negocioEstadoDescuentoAsociadosGrid.sortRows(2,'str','asc');
		var total = grid_obj.getRowsNum();
		if(total > 0){
			parent.jQuery("#labelValidaEstadoDescuento").html("Este negocio opera para los siguientes estados \u00FAnicamente");
		}else{
			parent.jQuery("#labelValidaEstadoDescuento").html("Este negocio opera para todos los estados");
		}
	}); 
	
	negocioEstadoDescuentoAsociadosGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){		
		var descuentoMaximo = negocioEstadoDescuentoAsociadosGrid.cellById(rId, 4).getValue();
		var descuentoDefault = negocioEstadoDescuentoAsociadosGrid.cellById(rId, 5).getValue();
		
		if(descuentoMaximo > 100) {
			mostrarVentanaMensaje('20', 'El descuento Máximo no es válido');
			return false;
		} else {
			if(descuentoDefault > descuentoMaximo) {
				mostrarVentanaMensaje('20', 'El descuento Default no puede ser mayor que el Descuento Máximo');
				return false;
			} else {
				return true;
			}
		}
	});
	
	negocioEstadoDescuentoAsociadosGrid.load("/MidasWeb/negocio/estadodescuento/obtenerRelacionesAsociadas.action?id="+ dwr.util.getValue("id"));

	//Processor
	negocioEstadoDescuentoProcessor = new dataProcessor("/MidasWeb/negocio/estadodescuento/guardar.action?negocioEstadoDescuento.negocio.idToNegocio="+dwr.util.getValue("id"));
	negocioEstadoDescuentoProcessor.setUpdateMode("off");
	negocioEstadoDescuentoProcessor.enableDataNames(true);
	negocioEstadoDescuentoProcessor.setTransactionMode("POST");

	// Mensaje error/exito
	negocioEstadoDescuentoProcessor.defineAction("mensaje", response);
	
	negocioEstadoDescuentoProcessor.setVerificator(3, checkSoloLetras);
	negocioEstadoDescuentoProcessor.setVerificator(4, checkIfFloat);
	negocioEstadoDescuentoProcessor.setVerificator(4, checkIfFloat);

	negocioEstadoDescuentoProcessor.init(negocioEstadoDescuentoAsociadosGrid);
}

function response(node){
	parent.guardarEstadoDescuento(node);
	obtenerNegocioEstadoDescuentoAsociados();
return false;
}

function iniciaGridsEstadoDescuento() {
	refrescarGridsEstadoDescuento(null,null,null,null);
}

function refrescarGridsEstadoDescuento(sid,action,tid,node){
	obtenerNegocioEstadoDescuentoAsociados();
	return true; 
}

function checkSoloLetras(value,colName){
	var val = value.toString()._dhx_trim();
	if (/^([a-z]+)?$/gi.test(val)) {
		return true;
	} else {
        return false;
    }
}

function checkIfFloat(value,colName){
	var val = parseFloat(value.toString()._dhx_trim());
	if(isNaN(val)) {
		return false;
	} else {
		return true;
	}
}