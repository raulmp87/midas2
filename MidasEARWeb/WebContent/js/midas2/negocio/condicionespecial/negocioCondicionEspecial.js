/**
 * 
 */
var negocioCondicionEspecialAsociadasGrid;
var negocioCondicionEspecialDisponiblesGrid;

function initGridsNegocioCondicionEspecial(){
	jQuery("#condicionesEspecialesDisponiblesGrid").empty();
	getCondicionesEspecialesDisponibles();
	getCondicionesEspecialesAsociadas();
}

function getCondicionesEspecialesDisponibles( idNegocio ){
	negocioCondicionEspecialDisponiblesGrid = new dhtmlXGridObject('condicionesEspecialesDisponiblesGrid');
	if( idNegocio != null && idNegocio != 0 ){
		negocioCondicionEspecialDisponiblesGrid.load( "/MidasWeb/negocio/condicionespecial/obtenerCondicionesDisponibles.action?idToNegocio=" + idNegocio );
	} else {
		negocioCondicionEspecialDisponiblesGrid.load( "/MidasWeb/negocio/condicionespecial/obtenerCondicionesDisponibles.action" );
	}	
}


function getCondicionesEspecialesAsociadas(){
	negocioCondicionEspecialAsociadasGrid = new dhtmlXGridObject('condicionesEspecialesAsociadasGrid');
	negocioCondicionEspecialAsociadasGrid.load("/MidasWeb/negocio/condicionespecial/obtenerCondicionesAsociadas.action");
	// Creacion del DataProcessor
	url = "/MidasWeb/negocio/condicionespecial/relacionarNegocioCondicionEspecial.action?";
	url.concat(jQuery(document.negocioCondicionEspecialForm).serialize());
	url.concat("&idSeccion=" + jQuery("#idSeccion").val());
	url.concat("&aplicaExterno=" + jQuery("#aplicaExterno").val());
	negocioCondicionEspecialProcessor = new dataProcessor(url);
	negocioCondicionEspecialProcessor.enableDataNames(true);
	negocioCondicionEspecialProcessor.setTransactionMode("POST");
	negocioCondicionEspecialProcessor.setUpdateMode("cell");
	negocioCondicionEspecialProcessor.attachEvent("onRowMark", function(){
		restart();
	});
	negocioCondicionEspecialProcessor.init(negocioCondicionEspecialAsociadasGrid);
}

function searchCondicionesEspeciales(){
	var codigoCondicionEspecial = jQuery("#codigoCondicionEspecial").val();
	var url = "/MidasWeb/negocio/condicionespecial/obtenerCondicionesDisponibles.action?codigoNombre=" 
			+ codigoCondicionEspecial;
	blockPage();
	negocioCondicionEspecialDisponiblesGrid.load(url,unblockPage());
	
}

function changeComboCondicionesEspeciales(){
	var idSeccion = jQuery("#idSeccion").val();
	var url = "/MidasWeb/negocio/condicionespecial/obtenerCondicionesDisponibles.action?idSeccion=" 
			+ idSeccion;
	blockPage();
	negocioCondicionEspecialDisponiblesGrid.load(url,unblockPage());
	
}

function setAplicaExterno(){

	var aplicaExterno = 0;
	
		if( jQuery("#aplicaExterno").is(":checked") ){
			aplicaExterno = 1;
		}

	jQuery("#aplicaExterno").val(aplicaExterno);
	
	sendRequestJQ(null, "/MidasWeb/negocio/condicionespecial/aplicaExterno.action?aplicaExterno="+aplicaExterno,
			"contenido_negocioCondicionEspecial", "verNegocioCondicionEspecial();");
}

function borrarAsociadas(){
	var url = "/MidasWeb/negocio/condicionespecial/desligarTodas.action";
	sendRequestJQ(null,url,	null, "restart();");
}

function asociarTodas(){
	var idSeccion = jQuery("#idSeccion").val();
	var codNombre = jQuery("#codigoCondicionEspecial").val();
	var aplicaExterno = 0;
	var url="";
	
	if( jQuery("#aplicaExterno").is(":checked") ){
		aplicaExterno = 1;
	}
	
	url+="/MidasWeb/negocio/condicionespecial/ligarTodas.action?idSeccion=";
	url+=idSeccion+"&codigoNombre="+codNombre+"&aplicaExterno="+aplicaExterno;
	
	sendRequestJQ(null,url,	null, "restart();");
	
}

function restart(){
	jQuery("#contenido_negocioCondicionEspecial").empty();
	verNegocioCondicionEspecial();
}