/**
 * Relaciones de paquete
 */
var negocioTipoPolizaAsociadasGrid;
var negocioTipoPolizaDisponiblesGrid;	
var  negocioTipoPolizaProcessor;
/**
 * Tipo Poliza
 */

function obtenerNegocioTipoPolizaAsociadas(){
	document.getElementById("negocioTipoPolizaAsociadasGrid").innerHTML = '';
	negocioTipoPolizaAsociadasGrid = new dhtmlXGridObject('negocioTipoPolizaAsociadasGrid');
	negocioTipoPolizaAsociadasGrid.load("/MidasWeb/negocio/tipopoliza/obtenerNegocioTipoPolizaAsociadas.action?idToNegProducto="+dwr.util.getValue("idToNegProducto"));
	
	//Creacion del DataProcessor
	negocioTipoPolizaProcessor = new dataProcessor("/MidasWeb/negocio/tipopoliza/accionrelacionarNegocioTipoPoliza.action?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));

	negocioTipoPolizaProcessor.enableDataNames(true);
	negocioTipoPolizaProcessor.setTransactionMode("POST");
	negocioTipoPolizaProcessor.setUpdateMode("cell");
	negocioTipoPolizaProcessor.attachEvent("onAfterUpdate",refrescarGridsNegocioTipoPoliza);
	negocioTipoPolizaProcessor.init(negocioTipoPolizaAsociadasGrid);
}


function obtenerNegocioTipoPolizaDisponibles(){
	document.getElementById("negocioTipoPolizaDisponiblesGrid").innerHTML = '';
	negocioTipoPolizaDisponiblesGrid = new dhtmlXGridObject('negocioTipoPolizaDisponiblesGrid');
	negocioTipoPolizaDisponiblesGrid.load("/MidasWeb/negocio/tipopoliza/obtenerNegocioTipoPolizaDisponibles.action?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));
}

function iniciaGridsNegocioTipoPoliza() {
	refrescarGridsNegocioTipoPoliza(null,null,null,null);
}


function refrescarGridsNegocioTipoPoliza(sid,action,tid,node){

	obtenerNegocioTipoPolizaAsociadas();
	obtenerNegocioTipoPolizaDisponibles();
	return true; 
}



