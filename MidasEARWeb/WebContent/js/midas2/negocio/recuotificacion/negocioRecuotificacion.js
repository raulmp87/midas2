var BTN_ON = '/MidasWeb/img/maps/on.png',
	BTN_OFF = '/MidasWeb/img/maps/off.png';

var negocioRecUsuariosPermisoManualGrid, 
	negocioRecUsuariosDisponiblesGrid,
	negocioRecUsuariosProcessor,
	gridProgPagos,
	gridRecibos;

var PCTE_PROGPAGO = 'PCTE_PROGPAGO', 
	PCTE_RECIBO_DERECHOS = 'PCTE_RECIBO_DERECHOS',	
	PCTE_VERSION_DERECHOS = 'PCTE_VERSION_DERECHOS', 
	PCTE_RECIBOS_PRIMA = 'PCTE_RECIBOS_PRIMA',
	DIAS_DURACION = 'DIAS_DURACION';

var ANUAL = 'ANUAL',
	SEMESTRAL = 'SEMESTRAL';

function ProgPagoObj(){
	this.versionId = null;
	this.progPagoId = null;
	this.numProgPago = null;
	this.pcteProgPago = null;
}

ProgPagoObj.prototype.init = function(versionId, id, num, pcte){
	this.progPagoId = id;
	this.numProgPago = num;
	this.pcteProgPago = pcte;
	this.versionId = versionId;
}


function ReciboObj(){
	this.progPagoId = null;
	this.reciboId = null;
	this.tipoModificacion = null;
	this.numRecibo = null;
	this.pcteRecibo = null;	
}

ReciboObj.prototype.init = function(progPagoId, id, num, pcte, tipo){
	this.progPagoId = progPagoId;
	this.reciboId = id;
	this.tipoModificacion = tipo;
	this.numRecibo = num;
	this.pcteRecibo = pcte;		
}

/**
 * Inicializa grids
 */
function initGridsNegocioRecuotificacionUsuarios(){
	jQuery("#negocioRecUsuariosDisponiblesGrid").empty();
	getNegRecUsuariosDisponibles();
	getNegRecUsuariosAsociados();
}

/**
 * Crea grid y obtiene disponibles
 */
function getNegRecUsuariosDisponibles(){
	negocioRecUsuariosDisponiblesGrid = new dhtmlXGridObject('negocioRecUsuariosDisponiblesGrid');
	negocioRecUsuariosDisponiblesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	negocioRecUsuariosDisponiblesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	negocioRecUsuariosDisponiblesGrid.load( URL_LISTAR_DISPONIBLES);
}

/**
 * Crea grid, dataprocessor y obtiene asociados
 */
function getNegRecUsuariosAsociados(){
	negocioRecUsuariosPermisoManualGrid = new dhtmlXGridObject('negocioRecUsuariosPermisoManualGrid');
	negocioRecUsuariosPermisoManualGrid.load(URL_LISTAR_ASOCIADIOS);
	// Creacion del DataProcessor
	negocioRecUsuariosProcessor = new dataProcessor(URL_RELACIONAR_USUARIO);
	negocioRecUsuariosProcessor.enableDataNames(true);
	negocioRecUsuariosProcessor.enableUTFencoding(false);
	negocioRecUsuariosProcessor.setTransactionMode("POST");
	negocioRecUsuariosProcessor.setUpdateMode("cell");	
	negocioRecUsuariosProcessor.attachEvent("onRowMark", function(id, state, tid, response){		
		negocioRecUsuariosProcessor.sendAllData();					
		setTimeout(function(){getNegRecUsuariosAsociados();}, 800);		
	});	
	negocioRecUsuariosProcessor.attachEvent("onFullSync", function(){negocioRecUsuariosPermisoManualGrid.load(URL_LISTAR_ASOCIADIOS);});
	negocioRecUsuariosProcessor.init(negocioRecUsuariosPermisoManualGrid);
}

/**
 * Asociar todos los disponibles
 */
function asociarTodosNegRecUsuarios(){	
	sendRequestJQ(null, URL_ASOCIAR_TODOS,	null, 'mostrarRecuotificacionContenedor()');
}

/**
 * Desasociar todos los asociados
 */
function desAsociarTodosNegRecUsuarios(){
	sendRequestJQ(null,URL_DESASOCIAR_TODOS,	null, 'mostrarRecuotificacionContenedor()');
}

/**
 * Cambiar el incono de swith
 * @param imgId - id de la imagen de switch
 * @param objId - id del hidden que guarda el valor
 */
function switchOperacion(imgId, objId){
	var source = jQuery('#'+imgId)[0].src;	
	if(source.indexOf(BTN_OFF)>=0){
		jQuery('#'+imgId)[0].src = BTN_ON;
	}else{
		jQuery('#'+imgId)[0].src = BTN_OFF;
	}	
	var flag = jQuery('#'+objId).val() === 'true';	
	jQuery('#'+objId).val(!flag);
	
}

function activarAutomatica(id){
	var objId = 'inputActivarAutomatica';	
	activarOperacion(URL_ACTIVAR_AUTOMATICA, id, objId);		
}

function activarManual(id){
	var objId = 'inputActivarManual';	
	activarOperacion(URL_ACTIVAR_MANUAL, id, objId);
}

function activarPrimaTotal(id){	
	var objId = 'inputActivarModificacionPrimaTotal';
	activarOperacion(URL_ACTIVAR_PRIMA_TOTAL, id, objId);
}


function activarOperacion(url, imgId, objId){		
		jQuery.ajax({
	          url: url,
	          dataType: 'json',
	          async:false,
	          type:"POST",
	          data: {'valor' : jQuery('#'+objId).val(), 'recuotificacion\.id' : jQuery('#recuotificacionId').val()},
	          success: function(json){		        	 
	        	 switchOperacion(imgId, objId);
	          	 var msj = json.mensaje;	          	 
	          	 if(msj != null && msj !=''){	          		
	          		mostrarMensajeInformativo(msj, '20');	          		
	          	 }
	          },
	          beforeSend: function(){
					blockPage();
				},
			  complete: function(){
				  unblockPage();
			  }
	    });			    
}

/**
 * Modificar valor permitido para modificacion de la prima total
 * @param valor
 */
function modificarPrimaTotal(valor){	
	removeCurrencyFormatOnTxtInput();
	jQuery.ajax({
          url: URL_MODIFICAR_PRIMA_TOTAL,
          dataType: 'json',
          async:false,
          type:"POST",
          data: {'valor' : valor, 'recuotificacion\.id' : jQuery('#recuotificacionId').val()},
          success: function(json){        	
          	 var msj = json.mensaje;	          	 
          	 if(msj != null && msj !=''){
          		mostrarMensajeInformativo(msj, '20');
          	 }
          },
          beforeSend: function(){
				blockPage();
			},
		  complete: function(){
			  initCurrencyFormatOnTxtInput();
			  unblockPage();			  
		  }
    });			    
}

/**
 * Abrir contenedor de versiones
 */
function abrirContenedorVersion(){		
	mostrarVentanaModal("negVersion", "Versiones Programas de Pago", null, null, 600, 450, URL_CARGAR_VERSION, ""); 
}


/**
 * Cargar Grid de programas de pago para la version activa
 * @param idToCotizacion
 */
function getGridProgramaPagos(versionId)
{
   document.getElementById("programaPagoListadoGrid").innerHTML = '';
   gridProgPagos = new dhtmlXGridObject('programaPagoListadoGrid');
   var posPath = URL_LLENAR_PROGRAMAS_VERSION_ACTUAL + '?versionId='+versionId;
   gridProgPagos.loadXML(posPath,function(){
	   gridProgPagos.attachFooter("TOTAL PROGRAMA PAGO,#cspan,#cspan,<div id='primaPP'>0</div>,<div id='derechosPP'>0</div>,#cspan", ["text-align:left;"]);
	   gridProgPagos.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,<div id='primaDifPP'>0</div>,<div id='derechosDiffPP'>0</div>,#cspan", ["text-align:left;"]);
	   actualizarSaldos(versionId, gridProgPagos, PCTE_PROGPAGO);   
	   actualizarSaldos(versionId, gridProgPagos, PCTE_VERSION_DERECHOS);   
   });
   gridProgPagos.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
	   try{
		   var val = parseFloat(oValue);
		   if(stage == 2 && cInd == 2){
			   if(nValue != "" && parseFloat(nValue) >= 0 && parseFloat(nValue) <= 100){
				   var progPago = new ProgPagoObj();			   
				   progPago.init(versionId, obtenerValorColumna(gridProgPagos, rId, 0), obtenerValorColumna(gridProgPagos, rId, 1), nValue);
				   var exito = enviarJson(URL_MODIFICAR_PROGRAMA_PAGO, progPago);
				   if(exito){					 
					   actualizarSaldos(versionId, gridProgPagos, PCTE_PROGPAGO);
					   val = parseFloat(nValue);
				   }
			   }else{
				   parent.mostrarMensajeInformativo('El porcentaje debe ser entre 0 y 100', '10');				  				  
			   }		 		   
		   }
	   }catch(e){
		   parent.mostrarMensajeInformativo('Ocurrió un error al editar la celda ' + e.message, '10');		 
	   }
	   return val;
   });   
   gridProgPagos.attachEvent("onXLS", function(grid_obj){parent.blockPage()});
   gridProgPagos.attachEvent("onXLE", function(grid_obj){parent.unblockPage()});
   gridProgPagos.init();
}

function actualizarSaldos(versionId, grid, tipoSaldo){
	var saldo = obtenerSaldo(versionId, tipoSaldo);
	if(tipoSaldo == PCTE_VERSION_DERECHOS){
		jQuery('#derechosDiffPP').html(redondeo(saldo,2));	
	}else{
		jQuery('#primaDifPP').html(redondeo(saldo,2));
	}
	var status = obtenerEstatusVersion(versionId);
	jQuery('#status').html(status.toUpperCase());
}

function actualizarSaldosRecibos(versionId, progPagoId, grid, tipoSaldo){
	var saldo = obtenerSaldo(progPagoId, tipoSaldo);
	if(tipoSaldo == PCTE_RECIBOS_PRIMA){
		jQuery('#primaTotalReciboDiff').html(redondeo(saldo,2));
	}else if(tipoSaldo == PCTE_RECIBO_DERECHOS || tipoSaldo == PCTE_VERSION_DERECHOS){
		jQuery('#derechosTotalDiff').html(redondeo(saldo,2));
	}else if(tipoSaldo == DIAS_DURACION){
		jQuery('#diasDuracionDiff').html(redondeo(saldo,2));
	}	
	var status = obtenerEstatusVersion(versionId);
	jQuery('#status').html(status.toUpperCase());
}

/**
 * Obtener el saldo para el tipo de valor y version data
 * @param version id de la version
 * @param tipo correspondiente a TIPOSALDO
 */
function obtenerSaldo(version, tipo){
	var saldo = 0;
	jQuery.ajax({
      url: URL_OBTENER_SALDO,   
      dataType: 'json',
      async: false,
      data: {'versionId' : version, 'tipoSaldo' : tipo},
      success: function(data) {    	
    	 saldo =  data.saldo;
      },
      beforeSend: function(){		
      	//parent.blockPage();
			},
	  complete: function(){
		//parent.unblockPage();
	  }
    });
	return saldo;
}

/**
 * Obtener el estatus para la version 
 * @param version id de la version
 */
function obtenerEstatusVersion(version){
	var status;
	jQuery.ajax({
      url: URL_OBTENER_STATUS,   
      dataType: 'json',
      async: false,
      data: {'versionId' : version},
      success: function(data) {    	
    	status =  data.status;
      },
      beforeSend: function(){		      	
	  },
	  complete: function(){		
	  }
    });
	return status;
}
	
function setValorColumna(grid, rId, cId, val){
	grid.cellById(rId,cId).setValue(val);
}
	
function obtenerValorColumna(grid, rId, cId ){
	return grid.cellById(rId, cId).getValue();
}



function enviarJson(urlToSend, obj){
	var exito = true;
	jQuery.ajax({
        url: urlToSend,
        dataType: 'json',
        async:false,
        type:"POST",
        data: {json : JSON.stringify(obj)},
        success: function(json){        	
        	 var msj = json.mensaje;	          	 
        	 if(msj != null && msj !=''){
        		exito = false;
        		parent.mostrarMensajeInformativo(msj, '20');
        	 }
        },
        beforeSend: function(){		
        	parent.blockPage();
			},
		complete: function(){
			parent.unblockPage();
		}
	});	
	return exito;
}


function generarNuevoProgramaPago(versionId){	
	var saldo = jQuery('#primaDifPP').html();
	if(parseFloat(saldo) == 0){
		parent.mostrarMensajeInformativo('No existe saldo suficiente para generar un nuevo programa de pago', '20');
	}else{
		var posPath = URL_NUEVO_PROG + '?versionId='+versionId;
		gridProgPagos.loadXML(posPath,function(){
			gridProgPagos.attachFooter("TOTAL PROGRAMA PAGO,#cspan,#cspan,<div id='primaPP'>0</div>,<div id='derechosPP'>0</div>,#cspan", ["text-align:left;"]);
		    gridProgPagos.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,<div id='primaDifPP'>0</div>,<div id='derechosDiffPP'>0</div>,#cspan", ["text-align:left;"]);
		    actualizarSaldos(versionId, gridProgPagos, PCTE_PROGPAGO);   
		    actualizarSaldos(versionId, gridProgPagos, PCTE_VERSION_DERECHOS);  
		});
	}
}

function confirmarEliminarPrograma(progPagoId){
	parent.mostrarMensajeConfirm('El programa de pago y sus recibos serán eliminados, continuar?', '20', 'eliminarProgramaPago('+progPagoId+')',null,null);
}

function eliminarProgramaPago(progPagoId){
	if(confirm('El programa de pago y sus recibos ser\u00E1n eliminados, continuar?')){
		var versionId = jQuery('#versionId').val()
		var posPath = URL_ELIMINA_PROG + '?versionId='+versionId+'&progPagoId='+progPagoId;
		gridProgPagos.loadXML(posPath,function(){		
			gridProgPagos.attachFooter("TOTAL PROGRAMA PAGO,#cspan,#cspan,<div id='primaPP'>0</div>,<div id='derechosPP'>0</div>,#cspan", ["text-align:left;"]);
		    gridProgPagos.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,<div id='primaDifPP'>0</div>,<div id='derechosDiffPP'>0</div>,#cspan", ["text-align:left;"]);
		    actualizarSaldos(versionId, gridProgPagos, PCTE_PROGPAGO);   
		    actualizarSaldos(versionId, gridProgPagos, PCTE_VERSION_DERECHOS);  
		   });
	}
}

function nuevaVersion(negocioId){
	parent.redirectVentanaModalWithBlocking('negVersion', URL_NUEVA_VERSION, null);
}

function activarVersion(versionId){
	var url = URL_ACTIVAR_VERSION + '?versionId='  + parseInt(versionId);	
	parent.redirectVentanaModalWithBlocking('negVersion', url, null);
}

function cargarVersion(versionId){	
	var url = URL_SELECCIONAR_VERSION + '?versionId='  + parseInt(versionId);	
	parent.redirectVentanaModalWithBlocking('negVersion', url, null);	
}


function modificarTipoVigencia(valor){
	var versionId = jQuery('#versionId').val();
	jQuery.ajax({
      url: URL_MODIFICAR_TIPO_VIGENCIA,   
      dataType: 'json',
      async: false,
      data: {'versionId' : versionId, 'tipoVigencia' : valor},
      success: function(json) {    	
    	 var msj = json.mensaje;	          	 
     	 if(msj != null && msj !=''){     		
     		parent.mostrarMensajeInformativo(msj, '20');
     	 }
      },
      beforeSend: function(){		      	
	  },
	  complete: function(){		
	  }
    });
	return status;
}

function redondeo(numero, decimales){
	var flotante = parseFloat(numero);
	var resultado = Math.round(flotante*Math.pow(10,decimales))/Math.pow(10,decimales);
	return resultado;
}

function validanNegativos(numero){
   out = true;
   if (numero < 0)
   {	alert("El monto debe ser igual o mayor que cero");
   		out = false;
   }   
   return out;
}

function sumColumn(grid, ind) {
	var out = 0;
	for (var i = 0; i < grid.getRowsNum(); i++) {
		out += redondeo(parseFloat(grid.cells2(i, ind).getValue()),2);
	}
	return out;
}

function verDetalleRecibos(progPagoId){
	var url = URL_MOSTRAR_RECIBOS + '?progPagoId='+progPagoId;
	parent.redirectVentanaModalWithBlocking("negVersion", url, null);
}
 

/**
 * Cargar Grid de programas de pago para la version activa
 * @param idToCotizacion
 */
function getGridRecibos(progPagoId)
{
   document.getElementById("recibosListadoGrid").innerHTML = '';
   gridRecibos = new dhtmlXGridObject('recibosListadoGrid');
   var posPath = URL_OBTENER_RECIBOS + '?progPagoId='+progPagoId;
   gridRecibos.loadXML(posPath,function(){
	   gridRecibos.attachFooter("TOTAL RECIBOS,#cspan,<div id='diasDuracion'>0</div>,<div id='primaTotalRecibo'>0</div>,<div id='derechosTotal'>0</div>,#cspan", ["text-align:left;"]);
	   gridRecibos.attachFooter("SALDO O DIFERENCIA,#cspan,<div id='diasDuracionDiff'>0</div>,<div id='primaTotalReciboDiff'>0</div>,<div id='derechosTotalDiff'>0</div>,#cspan", ["text-align:left;"]);
	   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBOS_PRIMA);
	   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBO_DERECHOS);  
	   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, DIAS_DURACION);  
   });
   gridRecibos.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
	   try{
		   var val = parseFloat(oValue);
		   var recibo;
		   if(stage == 2){
			   if(cInd == 2){
				   var tipoVigencia = jQuery('#tipoVigencia').val()
				   if(nValue != "" && parseInt(nValue) > 0 && 
						   ((tipoVigencia == ANUAL && parseInt(nValue) <= 365) || (tipoVigencia == SEMESTRAL && parseInt(nValue) <= 183))){
					   recibo = new ReciboObj(); 
					   recibo.init(progPagoId, obtenerValorColumna(gridRecibos, rId, 0), obtenerValorColumna(gridRecibos, rId, 1), nValue, DIAS_DURACION);
					   var exito = enviarJson(URL_MODIFICAR_RECIBO, recibo);
					   if(exito){	
						   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, DIAS_DURACION);
						   val = parseFloat(nValue);
					   }					
				   }else{
					   parent.mostrarMensajeInformativo('Los dias no son acordes al tipo de vigencia', '10');			   
				   }
			   }else if(cInd == 3 || cInd == 4){//prima neta o derechos
				   if(nValue != "" && parseFloat(nValue) >= 0 && parseFloat(nValue) <= 100){
					   recibo = new ReciboObj();
					   if(cInd == 3){
						   recibo.init(progPagoId, obtenerValorColumna(gridRecibos, rId, 0), obtenerValorColumna(gridRecibos, rId, 1), nValue, PCTE_RECIBOS_PRIMA);
					   }else{
						   recibo.init(progPagoId, obtenerValorColumna(gridRecibos, rId, 0), obtenerValorColumna(gridRecibos, rId, 1), nValue, PCTE_RECIBO_DERECHOS);
					   }
					   var exito = enviarJson(URL_MODIFICAR_RECIBO, recibo);
					   if(exito){	
						   if(cInd == 3){
							   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBOS_PRIMA);
						   }else{
							   actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBO_DERECHOS);
						   }
						   val = parseFloat(nValue);
					   }
				   }else{
					   parent.mostrarMensajeInformativo('El porcentaje debe ser entre 0 y 100', '10');				  				  
				   }		 		   
			   }
		   }
	   }catch(e){
		   parent.mostrarMensajeInformativo('Ocurrió un error al editar la celda: ' + e.message, '10');		 
	   }
	   return val;
   });   
   gridRecibos.attachEvent("onXLS", function(grid_obj){parent.blockPage()});
   gridRecibos.attachEvent("onXLE", function(grid_obj){parent.unblockPage()});
   gridRecibos.init();
}


function generarNuevoRecibo(progPagoId){	
	var saldoPrima = jQuery('#primaTotalReciboDiff').html();
	var saldoDias = jQuery('#diasDuracionDiff').html();
	if(parseFloat(saldoPrima) == 0 || parseInt(saldoDias) == 0){
		parent.mostrarMensajeInformativo('No existe saldo suficiente para generar un nuevo recibo', '20');
	}else{
		var posPath = URL_NUEVO_RECIBO + '?progPagoId='+progPagoId;
		gridRecibos.loadXML(posPath,function(){
			 gridRecibos.attachFooter("TOTAL RECIBOS,#cspan,<div id='diasDuracion'>0</div>,<div id='primaTotalRecibo'>0</div>,<div id='derechosTotal'>0</div>,#cspan", ["text-align:left;"]);
			 gridRecibos.attachFooter("SALDO O DIFERENCIA,#cspan,<div id='diasDuracionDiff'>0</div>,<div id='primaTotalReciboDiff'>0</div>,<div id='derechosTotalDiff'>0</div>,#cspan", ["text-align:left;"]);
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBOS_PRIMA);
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBO_DERECHOS);  
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, DIAS_DURACION);  
		});
	}
}



function eliminarRecibo(reciboId){
	if(confirm('El recibo ser\u00E1 eliminado, continuar?')){
		var progPagoId = jQuery('#progPagoId').val()
		var posPath = URL_ELIMINA_RECIBO + '?reciboId='+reciboId+'&progPagoId='+progPagoId;
		gridRecibos.loadXML(posPath,function(){		
			gridRecibos.attachFooter("TOTAL RECIBOS,#cspan,<div id='diasDuracion'>0</div>,<div id='primaTotalRecibo'>0</div>,<div id='derechosTotal'>0</div>,#cspan", ["text-align:left;"]);
			 gridRecibos.attachFooter("SALDO O DIFERENCIA,#cspan,<div id='diasDuracionDiff'>0</div>,<div id='primaTotalReciboDiff'>0</div>,<div id='derechosTotalDiff'>0</div>,#cspan", ["text-align:left;"]);
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBOS_PRIMA);
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, PCTE_RECIBO_DERECHOS);  
			 actualizarSaldosRecibos(jQuery('#versionId').val(), progPagoId, gridRecibos, DIAS_DURACION);  
		   });
	}
}