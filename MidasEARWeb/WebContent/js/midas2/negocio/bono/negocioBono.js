/**
 *  Id's asociados a los Grids
 */
var negocioBonoAsociadasGrid;
var negocioBonoDisponiblesGrid;
/**
 * Id asociado al dataProcesor
 */
var negocioBonoProcessor;



function obtenerNegocioBonoAsociadas(){
	document.getElementById("negocioBonoAsociadasGrid").innerHTML = '';
	negocioBonoAsociadasGrid = new dhtmlXGridObject('negocioBonoAsociadasGrid');
	negocioBonoAsociadasGrid.load(obtenerNegocioBonoAsociadasPath + "?idToNegocio=" + dwr.util.getValue("idToNegocio"));
		
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioSeccionHeader.jsp que corresponda al
	 * metodo relacionarNegocioSeccion de la clase NegocioSeccionAction y se encuentre en el mapeo del archivo negocioseccion-struts.xml
	 */
	negocioBonoProcessor = new dataProcessor(relacionarNegocioBonoPath);
	negocioBonoProcessor.enableDataNames(true);
	negocioBonoProcessor.setTransactionMode("POST");
	negocioBonoProcessor.setUpdateMode("cell");
	negocioBonoProcessor.attachEvent("onAfterUpdate", refrescarGridsNegocioBono);
	negocioBonoProcessor.setVerificator(5, isFloat0);
	negocioBonoProcessor.setVerificator(6, isFloat0);
	negocioBonoProcessor.setVerificator(7, isFloat0);	
	negocioBonoProcessor.init(negocioBonoAsociadasGrid);
}

function obtenerNegocioBonoDisponibles(){
	document.getElementById("negocioBonoDisponiblesGrid").innerHTML = '';
	negocioBonoDisponiblesGrid = new dhtmlXGridObject('negocioBonoDisponiblesGrid');
	negocioBonoDisponiblesGrid.load(obtenerNegocioBonoDisponiblesPath  + "?idToNegocio=" + dwr.util.getValue("idToNegocio"));
	
}

function validaRows(){
	if(idTipoPolizaAct > 0){
		
		if(negocioSeccionAsociadasGrid.getRowsNum() == 0){
			alert("La Linea de Negocio no tiene paquetes asociados " + negocioSeccionAsociadasGrid.getRowsNum());
		}
	}	
}


function refrescarGridsNegocioBono(sid, action, tid, node){
	obtenerNegocioBonoAsociadas();
	obtenerNegocioBonoDisponibles();
	return true;
}

function iniciaGridsNegocioBono(){	
	refrescarGridsNegocioBono(null, null, null, null);
}
