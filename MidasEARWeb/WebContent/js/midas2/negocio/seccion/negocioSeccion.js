/**
 *  Id's asociados a los Grids
 */
var negocioSeccionAsociadasGrid;
var negocioSeccionDisponiblesGrid;
/**
 * Id asociado al dataProcesor
 */
var negocioSeccionProcessor;
var idTipoPolizaAct = -1;

var ventanaEstilos=null;

function onChangeComboTipoPoliza(id){	
	idTipoPolizaAct = id;
	if(idTipoPolizaAct != -1){
		iniciaGridsNegocioSeccion();
	}else{
		document.getElementById("negocioSeccionAsociadasGrid").innerHTML = '';
		negocioSeccionAsociadasGrid = new dhtmlXGridObject('negocioSeccionAsociadasGrid');
		document.getElementById("negocioSeccionDisponiblesGrid").innerHTML = '';
		negocioSeccionDisponiblesGrid = new dhtmlXGridObject('negocioSeccionDisponiblesGrid');
	}
}

function obtenerNegocioSeccionAsociadas(){
	document.getElementById("negocioSeccionAsociadasGrid").innerHTML = '';
	negocioSeccionAsociadasGrid = new dhtmlXGridObject('negocioSeccionAsociadasGrid');
	negocioSeccionAsociadasGrid.load(obtenerNegocioSeccionAsociadasPath + "?negocioTipoPoliza.idToNegTipoPoliza=" + idTipoPolizaAct);
	
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioSeccionHeader.jsp que corresponda al
	 * metodo relacionarNegocioSeccion de la clase NegocioSeccionAction y se encuentre en el mapeo del archivo negocioseccion-struts.xml
	 */
	negocioSeccionProcessor = new dataProcessor(relacionarNegocioSeccionPath);
	negocioSeccionProcessor.enableDataNames(true);
	negocioSeccionProcessor.setTransactionMode("POST");
	negocioSeccionProcessor.setUpdateMode("cell");
	negocioSeccionProcessor.attachEvent("onAfterUpdate",refrescarGridsNegocioSeccion);
	negocioSeccionProcessor.init(negocioSeccionAsociadasGrid);
}

function obtenerNegocioSeccionDisponibles(){
	document.getElementById("negocioSeccionDisponiblesGrid").innerHTML = '';
	negocioSeccionDisponiblesGrid = new dhtmlXGridObject('negocioSeccionDisponiblesGrid');
	negocioSeccionDisponiblesGrid.load(obtenerNegocioSeccionDisponiblesPath + "?negocioTipoPoliza.idToNegTipoPoliza=" + idTipoPolizaAct);
	
}

function refrescarGridsNegocioSeccion(sid, action, tid, node){
	obtenerNegocioSeccionAsociadas();
	obtenerNegocioSeccionDisponibles();
	return true;
}

function iniciaGridsNegocioSeccion(){	
	refrescarGridsNegocioSeccion(null, null, null, null);
}
function asociarNegocioSeccionTipoUso(){
	if(negocioSeccionAsociadasGrid.getSelectedId() != null){
		var idNegocioSeccion = getIdFromGrid(negocioSeccionAsociadasGrid, 0);	
		mostrarVentanaNegSeccionTipoUso( idNegocioSeccion,null);
	}

}


function mostrarVentanaNegSeccionTipoUso(idNegocioSeccion,onCloseFunction){ 
	
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.negocioTipoUso = parent.dhxWins.createWindow("tipoUso", 200, 120, 710, 450);
	parent.negocioTipoUso.setText("Asociar Tipo Uso Vehiculo");
	parent.negocioTipoUso.center();
	parent.negocioTipoUso.setModal(true);	
	parent.negocioTipoUso.attachURL("/MidasWeb/negocio/producto/tipopoliza/seccion/tipouso/mostrarNegocioTipoUso.action?idToNegSeccion="+idNegocioSeccion);	
	parent.negocioTipoUso.button("minmax1").hide();

}

function mostrarVentanaEstilos(idNegocioSeccion,onCloseFunction){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaEstilos = parent.dhxWins.createWindow("estilos", 200, 320, 700, 450);
	parent.ventanaEstilos.setText("Asignar Estilos A Linea de Negocio");
	parent.ventanaEstilos.center();
	parent.ventanaEstilos.setModal(true);
	parent.ventanaEstilos.attachEvent("onClose", onCloseFunction);
	parent.ventanaEstilos.attachURL("/MidasWeb/negocio/estilovehiculo/mostrarVentana.action?id="+idNegocioSeccion);
	
	parent.ventanaEstilos.button("minmax1").hide();	
}

function mostrarNegocioSeccionTipoServicioAsociadas(){
	var idToNegSeccionAsociadas = getIdFromGrid(negocioSeccionAsociadasGrid, 0);
	    asociarNegocioSeccionTipoServicio( idToNegSeccionAsociadas,close);
}
function asociarNegocioSeccionTipoServicio(idToNegSeccionAsociadas,onCloseFunction){
	
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaNegocioSeccionTipoServicio = parent.dhxWins.createWindow("tiposdeServicios", 200, 320, 700, 450);
	parent.ventanaNegocioSeccionTipoServicio.setText("Tipo Servicio Asociados Por Negocios");
	parent.ventanaNegocioSeccionTipoServicio.center();
	parent.ventanaNegocioSeccionTipoServicio.setModal(true);
	parent.ventanaNegocioSeccionTipoServicio.attachEvent("onClose", onCloseFunction);
	parent.ventanaNegocioSeccionTipoServicio.attachURL("/MidasWeb/negocio/seccion/tiposervicio/mostrarNegocioSeccionTipoServicio.action?idToNegSeccion="+idToNegSeccionAsociadas);
	parent.ventanaNegocioSeccionTipoServicio.button("minmax1").hide();	

}

function close(){
parent.dhxWins.window('tiposdeServicios').setModal(false);
parent.dhxWins.window('tiposdeServicios').hide();
parent.ventanaNegocioSeccionTipoServicio=null;
}
