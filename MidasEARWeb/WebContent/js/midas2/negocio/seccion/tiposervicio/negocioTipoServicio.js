/**
 * Relaciones de paquete
 */
var negocioSeccionTipoServicioAsociadasGrid;
var negocioSeccionTipoServicioDisponiblesGrid;	
var  negocioTipoServicioProcessor;
/**
 * Tipo Servicio
 */

function obtenerNegocioTipoServicioAsociadas(){
	negocioSeccionTipoServicioAsociadasGrid = new dhtmlXGridObject('negocioSeccionTipoServicioAsociadasGrid');
	negocioSeccionTipoServicioAsociadasGrid.load("/MidasWeb/negocio/seccion/tiposervicio/obtenerNegocioTipoServicioAsociadas.action?idToNegSeccion="+dwr.util.getValue("idToNegSeccion"));
	//Creacion del DataProcessor
	
	negocioTipoServicioProcessor = new dataProcessor("/MidasWeb/negocio/seccion/tiposervicio/accionrelacionarNegocioTipoServicio.action");
	
	negocioTipoServicioProcessor.enableDataNames(true);
	negocioTipoServicioProcessor.setTransactionMode("POST");
	negocioTipoServicioProcessor.setUpdateMode("cell");
	negocioTipoServicioProcessor.attachEvent("onAfterUpdate",refrescarGridsNegocioTipoServicio);
	negocioTipoServicioProcessor.init(negocioSeccionTipoServicioAsociadasGrid);
}


function obtenerNegocioTipoServicioDisponibles(){
	negocioSeccionTipoServicioDisponiblesGrid = new dhtmlXGridObject('negocioSeccionTipoServicioDisponiblesGrid');
	negocioSeccionTipoServicioDisponiblesGrid.load("/MidasWeb/negocio/seccion/tiposervicio/obtenerNegocioTipoServicioDisponibles.action?idToNegSeccion="+dwr.util.getValue("idToNegSeccion"));
}

function iniciaGridsNegocioTipoServicio() {
	refrescarGridsNegocioTipoServicio(null,null,null,null);
}


function refrescarGridsNegocioTipoServicio(sid,action,tid,node){

	obtenerNegocioTipoServicioAsociadas();
	obtenerNegocioTipoServicioDisponibles();
	return true; 
}
