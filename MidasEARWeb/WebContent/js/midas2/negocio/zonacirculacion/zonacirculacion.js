/**
 *
 */

var negocioZonaAsociadasGrid;
var negocioZonaDisponiblesGrid;	
var  negocioZonaProcessor;
/**
 *
 */


function obtenerNegocioZonaAsociadas(){

	negocioZonaAsociadasGrid = new dhtmlXGridObject('negocioZonaAsociadasGrid');
	negocioZonaAsociadasGrid.attachEvent("onXLE", function(grid_obj,count){
		var total = grid_obj.getRowsNum();
		if(total > 0){
			parent.jQuery("#labelValidaEstados").html("Este negocio opera para las siguientes zonas \u00FAnicamente");
		}else{
			parent.jQuery("#labelValidaEstados").html("Este negocio opera para todas las zonas ");
		}
	}); 
	negocioZonaAsociadasGrid.load("/MidasWeb/negocio/zonacirculacion/obtenerRelacionesAsociadas.action?id="+ dwr.util.getValue("id"));

	//Creacion del DataProcessor
	negocioZonaProcessor = new dataProcessor("/MidasWeb/negocio/zonacirculacion/relacionarNegocioZonaCirculacion.action?negocioEstado.negocio.idToNegocio="+dwr.util.getValue("id")); //+dwr.util.getValue("id"));
	negocioZonaProcessor.setUpdateMode("off");
	negocioZonaProcessor.enableDataNames(true);
	negocioZonaProcessor.setTransactionMode("POST");
	// Muestra mensaje error / exito
	negocioZonaProcessor.defineAction("mensaje", response);
	negocioZonaProcessor.init(negocioZonaAsociadasGrid);
}

function response(node){
	parent.guardarZonasC(node);
	obtenerNegocioZonaAsociadas();
return false;
}

function obtenerNegocioZonaDisponibles(){
	
	negocioZonaDisponiblesGrid = new dhtmlXGridObject('negocioZonaDisponiblesGrid');
	negocioZonaDisponiblesGrid.load("/MidasWeb/negocio/zonacirculacion/obtenerRelacionesDisponibles.action?id=" + dwr.util.getValue("id"));
}

function iniciaGridsZonas() {
	refrescarGridsNegocioZona(null,null,null,null);
}


function refrescarGridsNegocioZona(sid,action,tid,node){
	obtenerNegocioZonaAsociadas();
	obtenerNegocioZonaDisponibles();
	return true; 
}



