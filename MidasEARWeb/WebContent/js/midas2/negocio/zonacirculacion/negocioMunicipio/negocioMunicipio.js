/**
 *
 */

var negocioZonaMunicipioAsociadasGrid;
var negocioZonaMunicipioDisponiblesGrid;	
var  negocioZonaMunicipioProcessor;
var idEstado = -1;
/**
 *
 */

function getEstadosPorNegocio(idEstado){	 
	if(idEstado != -1){
		iniciaGridsZonasMunicipio();
	}else{
		document.getElementById("negocioZonaMunicipioAsociadasGrid").innerHTML = '';
		negocioZonaMunicipioAsociadasGrid = new dhtmlXGridObject('negocioZonaMunicipioAsociadasGrid');
		document.getElementById("negocioZonaMunicipioDisponiblesGrid").innerHTML = '';
		negocioZonaMunicipioDisponiblesGrid = new dhtmlXGridObject('negocioZonaMunicipioDisponiblesGrid');
	}
}
function obtenerNegocioZonaAsociadas(){	
	negocioZonaMunicipioAsociadasGrid = new dhtmlXGridObject('negocioZonaMunicipioAsociadasGrid');
	negocioZonaMunicipioAsociadasGrid.load("/MidasWeb/negocio/zonacirculacion/negociomunicipio/obtenerRelacionesAsociadas.action?id="+ dwr.util.getValue("id") +"&stateId=" + dwr.util.getValue("idEstado"));
	//Creacion del DataProcessor	
	negocioZonaMunicipioProcessor = new dataProcessor("/MidasWeb/negocio/zonacirculacion/negociomunicipio/relacionarNegocioZonaCirculacion.action?negocioMunicipio.negocioEstado.id="+ dwr.util.getValue("idEstado") +"&stateId=" + dwr.util.getValue("idEstado"));
	negocioZonaMunicipioProcessor.enableDataNames(true);
	negocioZonaMunicipioProcessor.setUpdateMode("off");
	negocioZonaMunicipioProcessor.setTransactionMode("POST");
//	negocioZonaMunicipioProcesor.attachEvent("onAfterUpdate",refrescarGridsNegocioZonaMunicipio);
	negocioZonaMunicipioProcessor.init(negocioZonaMunicipioAsociadasGrid);
	negocioZonaMunicipioProcessor.defineAction("mensaje", response);
}
function response(node){
	guardarMunicipios(node);
	obtenerNegocioZonaAsociadas();
return false;
}

function obtenerNegocioZonaDisponibles(){	
	negocioZonaMunicipioDisponiblesGrid = new dhtmlXGridObject('negocioZonaMunicipioDisponiblesGrid');
	negocioZonaMunicipioDisponiblesGrid.load("/MidasWeb/negocio/zonacirculacion/negociomunicipio/obtenerRelacionesDisponibles.action?id=" + dwr.util.getValue("id")+"&stateId=" + dwr.util.getValue("idEstado") );
}

function iniciaGridsZonasMunicipio() {
	refrescarGridsNegocioZonaMunicipio();
}


function refrescarGridsNegocioZonaMunicipio(){
	obtenerNegocioZonaAsociadas();
	obtenerNegocioZonaDisponibles();
	return true; 
}



