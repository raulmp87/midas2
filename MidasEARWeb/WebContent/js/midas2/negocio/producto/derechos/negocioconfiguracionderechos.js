var listadoConfigDerechoGrid;

var idToNegTipoPolizaAct = "";
var idToNegSeccionAct = "";
var idToNegPaqSeccionAct = "";
var idMonedaAct = "";
var idTipoDerechoAct = "";
var idEstadoAct = "";
var idMunicipioAct = "";
var idNegTipoUsoAct = "";
var idImporteDerechoAct = "";

jQuery(document).ready(
	function(){
		cargarInformacionInicial();
		obtenerImportesDerecho();
		validaCombos();
		realizarBusqueda(true);
	}
);

function cargarInformacionInicial(){
	idToNegTipoPolizaAct = jQuery("#idToTipoPoliza").val();
	idToNegSeccionAct = jQuery("#idToNegSeccion").val();
	idToNegPaqSeccionAct = jQuery("#idToNegPaqueteSeccion").val();
	idMonedaAct = jQuery("#idTcMoneda").val();
	idTipoDerechoAct = jQuery("#idTipoDerecho").val();
}

function realizarBusqueda(mostrarBusquedaVacia){
	var url = "/MidasWeb/negocio/configuracionderecho/buscarConfiguracionesDerecho.action?";
	var serializedForm = jQuery(document.configuracionDerechoForm).serialize();
	var busquedaValidada =  validaCombos();

	if(mostrarBusquedaVacia || busquedaValidada == "s"){
		if(listadoConfigDerechoGrid){
			listadoConfigDerechoGrid.destructor();
		}
		
		listadoConfigDerechoGrid = new dhtmlXGridObject('listadoConfigDerechoGrid');
		listadoConfigDerechoGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		listadoConfigDerechoGrid.setSkin('light');
		listadoConfigDerechoGrid.setHeader("Default, Tipo Póliza, Línea Negocio, Paquete, Moneda, Tipo Derecho, Importe, Estado, Municipio, Tipo Uso, Acciones, ");
		listadoConfigDerechoGrid.setInitWidths("40,100,100,100,100,100,100,100,100,70,100,30");
		listadoConfigDerechoGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
		listadoConfigDerechoGrid.setColSorting("na,server,server,server,server,server,server,server,server,server,na,na");
		listadoConfigDerechoGrid.setColTypes("img,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img");			
		listadoConfigDerechoGrid.enableMultiline(true);
		listadoConfigDerechoGrid.init();
		listadoConfigDerechoGrid.attachEvent("onBeforePageChanged",function(){
    		if (!this.getRowsNum()) return false;
    		return true;
    	});		
		listadoConfigDerechoGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		listadoConfigDerechoGrid.setPagingSkin("bricks");	
		listadoConfigDerechoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		listadoConfigDerechoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		listadoConfigDerechoGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		listadoConfigDerechoGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });		

		if(!mostrarBusquedaVacia && busquedaValidada == "s"){
//			url += "idToNegTipoPolizaAct=" + idToNegTipoPolizaAct + "&idToNegSeccionAct=" + idToNegSeccionAct + "&idToNegPaqSeccionAct=" + idToNegPaqSeccionAct + "&idMonedaAct=" + idMonedaAct + "&idTipoDerechoAct=" + idTipoDerechoAct;
			url += "?"+serializedForm;
			console.log(url);
		}else{
			url += "?"+serializedForm;
//				"?idToNegProducto=" + jQuery("#idToNegProducto").val() + "&negocioConfigDerecho.negocio.idToNegocio=" + jQuery("#idToNegocio").val();
			console.log(url);
		}
		listadoConfigDerechoGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
			server = url;			
			listadoConfigDerechoGrid.clearAll();
			listadoConfigDerechoGrid.load(server+(server.indexOf("?")>=0?"&":"?")
		     +"orderBy="+ind+"&direct="+direct);			
			listadoConfigDerechoGrid.setSortImgState(true,ind,direct);
		    return false;
		});
		listadoConfigDerechoGrid.load( url );
	} else{
//		 mostrarMensajeInformativo('Se debe ingresar al menos un campo para realizar la búsqueda', '20');
	 }
}

function obtenerLineasDeNegocio(idToTipoPoliza){
	idToNegTipoPolizaAct = idToTipoPoliza;
	if(idToTipoPoliza != ""){
		listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza,function(data){
			var combo = document.getElementById('idToNegSeccion');
			addOptions(combo,data);
		});
	}
	idToNegSeccionAct = "";
}

function obtenerMonedas(idToTipoPoliza){
	if(idToTipoPoliza != ""){
		listadoService.getMapMonedaPorNegTipoPoliza(idToTipoPoliza,function(data){
			var combo = document.getElementById('idTcMoneda');
			addOptions(combo,data);
		});
	}
	idMonedaAct = "";
	validaCombos();
}

function obtenerPaquetes(idToNegSeccion){
	idToNegSeccionAct = idToNegSeccion;
	if(idToNegSeccion != ""){
			listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(
			idToNegSeccion,
			function(data){
				var combo = document.getElementById('idToNegPaqueteSeccion');
				addOptions(combo,data);
		});
	}
	idToNegPaqSeccionAct = "";
	validaCombos();
}

function obtieneMunicipio(idEstado){
	listadoService.getMapMunicipiosPorEstado(
			idEstado,
			function(data){
				var combo = document.getElementById('cityId');
				addOptionsConSeleccioneSiempre(combo,data);
			});
	idMunicipioAct = "";
	validaCombos();
}

function obtenerTipoUso(idToNegSeccion){
	if(idToNegSeccion != ""){
		listadoService.getMapTipoUsoVehiculoByNegocio(idToNegSeccion,function(data){
			var combo = document.getElementById('idTcTipoUsoVehiculo');
			addOptionsConSeleccioneSiempre(combo,data);
		});
	}
	idToNegTipoUsoAct = "";
}

function obtenerImportesDerecho(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var tipoDerecho = jQuery("#idTipoDerecho").val();
	if(idToNegocio 
			&& tipoDerecho){
		listadoService.listaImportesDeDerechosPorNegocio(
				idToNegocio,
				tipoDerecho,
				function(data){
					var combo = document.getElementById('importeDerecho');
					addOptions(combo,data);
				});
	}
}

function onChangeComboPaquete(idToNegPaqSeccion){
	idToNegPaqSeccionAct = idToNegPaqSeccion;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboMoneda(idMoneda){
	idMonedaAct = idMoneda;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboTipoDerecho(idTipoDerecho){
	idTipoDerechoAct = idTipoDerecho;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboEstado(idEstado){
	idEstadoAct = idEstado;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboMunicipio(idMunicipio){
	idMunicipioAct = idMunicipio;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboTipoUsoVehiculo(idTipoUso){
	idNegTipoUsoAct = idTipoUso;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function onChangeComboImporteDerecho(idImporteDerecho){
	idImporteDerechoAct = idImporteDerecho;
	if(validaCombos() == 's'){
		realizarBusqueda(false);
	}
}

function validaCombos(){
	if(idToNegTipoPolizaAct != "" && idToNegSeccionAct != "" && idToNegPaqSeccionAct != ""  && idMonedaAct != "" && idTipoDerechoAct != ""){
		jQuery("#stateId").removeAttr("disabled");
		jQuery("#cityId").removeAttr("disabled");
		jQuery("#idTcTipoUsoVehiculo").removeAttr("disabled");
		jQuery("#importeDerecho").removeAttr("disabled");
		return "s";
	}else{
		jQuery("#stateId").attr("disabled","true");
		jQuery("#cityId").attr("disabled","true");
		jQuery("#idTcTipoUsoVehiculo").attr("disabled","true");
		jQuery("#importeDerecho").attr("disabled","true");
		return "n";
	}
}

function guardarConfiguracionDerecho(){
	if(validateAll(true)){
		var formParams = jQuery(document.configuracionDerechoForm).serialize();
		var url = guardarNegocioDerechosPath + '?' +  formParams;
		sendRequestJQ(null, url, 'contenido_derechos', null);
	}
}

function eliminarConfiguracionDerecho(idToNegConfigDerecho){
	var formParams = jQuery(document.configuracionDerechoForm).serialize();
	if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
		var url = eliminarNegocioDerechosPath + '?negocioConfigDerecho.idToNegConfigDerecho=' + idToNegConfigDerecho + "&" + formParams;
		sendRequestJQ(null, url, 'contenido_derechos', null);
	}
}