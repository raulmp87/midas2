var idToNegocioAct = -1;
var negocioFormaPagoProcessor;
var negocioFormaPagoAsociadasGrid;
var negocioFormaPagoDisponiblesGrid;

function refrescarGridsFormaPago(sid, action, tid, node){
	obtenerFormaPagoDisponibles();
	obtenerFormaPagoAsociadas();	
	return true;
}

function iniciaGridsFormaPago(){	
	refrescarGridsFormaPago(null, null, null, null);
}

function obtenerFormaPagoAsociadas(){
	document.getElementById("negocioFormaPagoAsociadasGrid").innerHTML = '';
	negocioFormaPagoAsociadasGrid = new dhtmlXGridObject('negocioFormaPagoAsociadasGrid');
	negocioFormaPagoAsociadasGrid.load(obtenerFormaPagoAsociadasPath + "?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));
		
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioSeccionHeader.jsp que corresponda al
	 * metodo relacionarNegocioSeccion de la clase NegocioSeccionAction y se encuentre en el mapeo del archivo negocioseccion-struts.xml
	 */
	negocioFormaPagoProcessor = new dataProcessor(relacionarFormaPagoPath);
	negocioFormaPagoProcessor.enableDataNames(true);
	negocioFormaPagoProcessor.setTransactionMode("POST");
	negocioFormaPagoProcessor.setUpdateMode("cell");
	negocioFormaPagoProcessor.attachEvent("onAfterUpdate", refrescarGridsFormaPago);	
	negocioFormaPagoProcessor.init(negocioFormaPagoAsociadasGrid);
}

function obtenerFormaPagoDisponibles(){
	document.getElementById('negocioFormaPagoDisponiblesGrid').innerHTML = '';
	negocioFormaPagoDisponiblesGrid = new dhtmlXGridObject('negocioFormaPagoDisponiblesGrid');
	negocioFormaPagoDisponiblesGrid.load(obtenerFormaPagoDisponiblesPath + "?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));	
}

