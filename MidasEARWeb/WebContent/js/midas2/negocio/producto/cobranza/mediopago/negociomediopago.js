var idToNegocioAct = -1;
var negocioMedioPagoProcessor;
var negocioMedioPagoAsociadasGrid;
var negocioMedioPagoDisponiblesGrid;

//function desplegarCobranzaMedioPago(){
//	sendRequestJQ(null,'/MidasWeb/negocio/mediopago/mostrarMedioPago.action?idToNegProducto='+ dwr.util.getValue("idToNegProducto"),'contenido_condicionesCobranza','dhx_init_tabbars();iniciaGridsMedioPago()');
//}

function refrescarGridsMedioPago(sid, action, tid, node){
	obtenerMedioPagoDisponibles();
	obtenerMedioPagoAsociadas();	
	return true;
}

function iniciaGridsMedioPago(){	
	refrescarGridsMedioPago(null, null, null, null);
}

function obtenerMedioPagoAsociadas(){
	document.getElementById("negocioMedioPagoAsociadasGrid").innerHTML = '';
	negocioMedioPagoAsociadasGrid = new dhtmlXGridObject('negocioMedioPagoAsociadasGrid');
	negocioMedioPagoAsociadasGrid.load(obtenerMedioPagoAsociadasPath + "?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));
		
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioSeccionHeader.jsp que corresponda al
	 * metodo relacionarNegocioSeccion de la clase NegocioSeccionAction y se encuentre en el mapeo del archivo negocioseccion-struts.xml
	 */
	negocioMedioPagoProcessor = new dataProcessor(relacionarMedioPagoPath);
	negocioMedioPagoProcessor.enableDataNames(true);
	negocioMedioPagoProcessor.setTransactionMode("POST");
	negocioMedioPagoProcessor.setUpdateMode("cell");
	negocioMedioPagoProcessor.attachEvent("onAfterUpdate", refrescarGridsMedioPago);	
	negocioMedioPagoProcessor.init(negocioMedioPagoAsociadasGrid);
}

function obtenerMedioPagoDisponibles(){
	document.getElementById('negocioMedioPagoDisponiblesGrid').innerHTML = '';
	negocioMedioPagoDisponiblesGrid = new dhtmlXGridObject('negocioMedioPagoDisponiblesGrid');
	negocioMedioPagoDisponiblesGrid.load(obtenerMedioPagoDisponiblesPath + "?idToNegProducto="+ dwr.util.getValue("idToNegProducto"));	
}

