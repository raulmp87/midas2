/**
 *  Id's asociados a los Grids
 */
var negocioPaqueteSeccionAsociadosGrid;
var negocioPaqueteSeccionDisponiblesGrid;
/**
 * Id asociado al dataProcesor
 */
var negocioPaqueteSeccionProcessor;


function obtenerNegocioPaqueteSeccionAsociados(){
	
	document.getElementById("negocioPaqueteSeccionAsociadosGrid").innerHTML = '';
	negocioPaqueteSeccionAsociadosGrid = new dhtmlXGridObject('negocioPaqueteSeccionAsociadosGrid');
	negocioPaqueteSeccionAsociadosGrid.load(obtenerNegocioPaqueteSeccionAsociadosPath + "?idToNegSeccion=" + dwr.util.getValue("idLineas"));
		
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioPaqueteSeccionHeader.jsp que corresponda al
	 * metodo relacionarNegocioPaqueteSeccion de la clase NegocioPaqueteSeccionAction y se encuentre en el mapeo del archivo negociopaqueteseccion-struts.xml
	 */
	negocioPaqueteSeccionProcessor = new dataProcessor(relacionarNegocioPaquetePath);
	negocioPaqueteSeccionProcessor.enableDataNames(true);
	negocioPaqueteSeccionProcessor.setTransactionMode("POST");
	negocioPaqueteSeccionProcessor.setUpdateMode("cell");
	negocioPaqueteSeccionProcessor.attachEvent("onAfterUpdate", refrescarGridsNegocioPaqueteSeccion);	
	negocioPaqueteSeccionProcessor.setVerificator(4, isInteger);
	negocioPaqueteSeccionProcessor.init(negocioPaqueteSeccionAsociadosGrid);

}

function obtenerNegocioPaqueteSeccionDisponibles(){
	document.getElementById("negocioPaqueteSeccionDisponiblesGrid").innerHTML = '';
	negocioPaqueteSeccionDisponiblesGrid = new dhtmlXGridObject('negocioPaqueteSeccionDisponiblesGrid');
	negocioPaqueteSeccionDisponiblesGrid.load(obtenerNegocioPaqueteSeccionDisponiblesPath + "?idToNegSeccion=" + dwr.util.getValue("idLineas"));
	
}


function refrescarGridsNegocioPaqueteSeccion(sid, action, tid, node){
	obtenerNegocioPaqueteSeccionAsociados();
	obtenerNegocioPaqueteSeccionDisponibles();
	return true;
}

function iniciaGridsNegocioPaqueteSeccion(){	
	refrescarGridsNegocioPaqueteSeccion(null, null, null, null);
}



function getNegocioSeccionPorTipoPoliza(tipoPolizaSelect, lineasSelect) {
	var idNegocioPoliza =  dwr.util.getValue(tipoPolizaSelect);
	if (idNegocioPoliza != headerValue) {
		listadoService.getMapNegocioSeccionPorTipoPoliza(idNegocioPoliza, function(negocioSeccionMap) {
			addOptions(lineasSelect, negocioSeccionMap);
		});
	} else {
		addOptions(lineasSelect, null);
	}
}