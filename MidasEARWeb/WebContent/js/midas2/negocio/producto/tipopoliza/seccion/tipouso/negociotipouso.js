/**
 *  Id's asociados a los Grids
 */
var negocioTipoUsoAsociadasGrid;
var negocioTipoUsoDisponiblesGrid;
/**
 * Id asociado al dataProcesor
 */
var negocioTipoUsoProcessor;
var idToNegSeccion = -1;

function obtenerNegocioTipoUsoAsociadas(){
	document.getElementById("negocioTipoUsoAsociadasGrid").innerHTML = '';
	negocioTipoUsoAsociadasGrid = new dhtmlXGridObject('negocioTipoUsoAsociadasGrid');
	negocioTipoUsoAsociadasGrid.load(obtenerNegocioTipoUsoAsociadasPath + "?idToNegSeccion=" + idToNegSeccion);
		
	/**
	 * Creacion del DataProcessor debe existir la definicion de un Path en el negocioTipoUsoHeader.jsp que corresponda al
	 * metodo relacionarNegocioTipoUso de la clase NegocioTipoUsoAction y se encuentre en el mapeo del archivo negociotipouso-struts.xml
	 */
	negocioTipoUsoProcessor = new dataProcessor(relacionarNegocioTipoUsoPath);
	negocioTipoUsoProcessor.enableDataNames(true);
	negocioTipoUsoProcessor.setTransactionMode("POST");
	negocioTipoUsoProcessor.setUpdateMode("cell");
	negocioTipoUsoProcessor.attachEvent("onAfterUpdate", refrescarGridsNegocioTipoUso);	
	negocioTipoUsoProcessor.init(negocioTipoUsoAsociadasGrid);
}

function obtenerNegocioTipoUsoDisponibles(){
	document.getElementById("negocioTipoUsoDisponiblesGrid").innerHTML = '';
	negocioTipoUsoDisponiblesGrid = new dhtmlXGridObject('negocioTipoUsoDisponiblesGrid');
	negocioTipoUsoDisponiblesGrid.load(obtenerNegocioTipoUsoDisponiblesPath + "?idToNegSeccion=" + idToNegSeccion);
	
}


function refrescarGridsNegocioTipoUso(sid, action, tid, node){
	idToNegSeccion = dwr.util.getValue("idToNegSeccion");	
	obtenerNegocioTipoUsoAsociadas();
	obtenerNegocioTipoUsoDisponibles();
	return true;
}

function iniciaGridsNegocioTipoUso(){	
	refrescarGridsNegocioTipoUso(null, null, null, null);
}

