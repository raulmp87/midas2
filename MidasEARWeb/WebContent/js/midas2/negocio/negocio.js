var negocioGrid;
var ventanaProductos = null;
var productosGrid;
var borradoLogico = "3";

var verDetalleParamGralesURL = '<s:url action="verdetalleParamGrales" namespace="/negocio"/>';
var negocioDerechosEndosoURL = '<s:url action="mostrarDerechosEndoso" namespace="/negocio/derechos"/>';
var negocioDerechosPolizaURL = '<s:url action="mostrarDerechosPoliza" namespace="/negocio/derechos"/>';

var accordionParametrosNeg;
var gridDatosNeg001;
var gridEndosoNeg001;
var gridPolizaNeg001;
var gridNegZonas001;
var gridNegMunicipio001;
var gridAltaIncisoNeg001;

var accordionCompAdicional;
var gridCompAdicionalResumen;
var gridCompAdicionalConfig;

var activeCell = null;

var PARAMETRO_ID_CLIENTE = "idCliente";
var PARAMETRO_ID_GPO_CLIENTE = "idGrupoCliente";
var PARAMETRO_ID_NEGOCIO = "idNegocio";

var ID_CELDA_DATOS_GENERALES = "datosNeg1";
var ID_CELDA_DERECHOS_ENDOSO = "endosoNeg2";
var ID_CELDA_DERECHOS_POLIZA = "polizaNeg3";
var ID_CELDA_DERECHOS_ALTA_INCISO = "endosoNeg4";



/*
 * Contenedor Para Zonas de Negocio
 */
var accordionZonasNeg = null;
var ID_CELDA_ZONAS_GENERALES="datosNegZonas";
var ID_CELDA_ZONAS_MUNICIPIO="datosNegMunicipio";
/*  fin de asignacion de variables*/


function verDetalleNegocio(tipoAccion, idNegocio) {
	if(idNegocio == null || idNegocio == undefined){
		if (negocioGrid != null && negocioGrid.getSelectedId() != null) {
			idNegocio = getIdFromGrid(negocioGrid, 0);
		}
	}
	if (idNegocio != null && idNegocio != undefined) {
		sendRequestJQ(null, '/MidasWeb/negocio/verDetalle.action'
				+ "?tipoAccion=" + tipoAccion + "&id=" + idNegocio+ "&claveNegocio="+ dwr.util.getValue("claveNegocio"),
				targetWorkArea, null);
	} else {
		sendRequestJQ(null, '/MidasWeb/negocio/verDetalle.action'
				+ "?tipoAccion=" + tipoAccion + "&claveNegocio="+ dwr.util.getValue("claveNegocio"), targetWorkArea,
				null);
	}

}

function agregarNegocio(tipoAccion) {
		sendRequestJQ(null, '/MidasWeb/negocio/verDetalle.action'
				+ "?tipoAccion=" + tipoAccion + "&claveNegocio="+ dwr.util.getValue("claveNegocio"), targetWorkArea,
				null);
}


function mostrarCopiarNegocio() {
	var contenedorVentanas = obtenerContenedorVentanas();
}

function mostrarAsignarProducto() {
	var idNeg = dwr.util.getValue("id");
	mostrarVentanaProductos(idNeg, poblarProductos);

	/*
	parent.ventanaProductos.setText("Asignar Producto A Negocio");
	parent.ventanaProductos.center();
	parent.ventanaProductos.setModal(true);
	parent.ventanaProductos.attachEvent("onClose", poblarProductos);
	parent.ventanaProductos
			.attachURL("/MidasWeb/negocio/producto/mostrarAsignarProducto.action?idNegocio="
					+ idNeg);

	parent.ventanaProductos.button("minmax1").hide();
	*/
}

function mostrarVentanaProductos(idNegocio, onCloseFunction) {

	/*
	if (parent.dhxWins == null) {
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaProductos = parent.dhxWins.createWindow("productos", 200,
			320, 610, 260);
	parent.ventanaProductos.setText("Asignar Producto A Negocio");
	parent.ventanaProductos.center();
	parent.ventanaProductos.setModal(true);
	parent.ventanaProductos.attachEvent("onClose", onCloseFunction);
	parent.ventanaProductos
			.attachURL("/MidasWeb/negocio/producto/mostrarAsignarProducto.action?idNegocio="
					+ idNegocio);

	parent.ventanaProductos.button("minmax1").hide();*/	
	var ventanaProductosPaht = "/MidasWeb/negocio/producto/mostrarAsignarProducto.action?idNegocio="+ idNegocio+"&tipoAccion="+jQuery("#tipoAccion").val();
	mostrarVentanaModal("productos","Asignar Producto A Negocio",200,320, 610, 260, ventanaProductosPaht, onCloseFunction);
}

function cerrarVentanaProductos(){
	if(dhxWins != null){
		dhxWins.window('productos').setModal(false);
		dhxWins.window('productos').hide();
	}else{
		cerrarVentanaModal('productos');
	}
	poblarProductos();
}



function listar() {
	sendRequestJQ(null, "/MidasWeb/negocio/listar.action?claveNegocio=A", targetWorkArea, 
				'listarNegocio();');
}

function listarNegocio() {
	document.getElementById("negocioGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	negocioGrid = new dhtmlXGridObject("negocioGrid");
	// deshabilita el doble click y el click en las celdas del grid
	negocioGrid.enableEditEvents(false, false);
	negocioGrid
			.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,&nbsp,&nbsp,&nbsp,&nbsp");
	mostrarIndicadorCarga('indicador');	
	negocioGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	negocioGrid.load("/MidasWeb/negocio/listarFiltrado.action?claveNegocio="
			+ dwr.util.getValue("claveNegocio"));

}

function poblarProductos() {

	var idNegocio = getIdNegocio();

	document.getElementById("productosGrid").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	productosGrid = new dhtmlXGridObject("productosGrid");
	mostrarIndicadorCarga('indicador');	
	productosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	productosGrid
			.load("/MidasWeb/negocio/producto/listarProductosAsociados.action?idNegocio="
					+ idNegocio+'&claveNegocio='+claveNegocio);

	if(parent.dhxWins.window('productos') != null){
		parent.dhxWins.window('productos').setModal(false);
		parent.dhxWins.window('productos').hide();
		parent.ventanaProductos = null;
	}
}

function desplegarConfiguracion(tipoAccion) {
	if (productosGrid.getSelectedId() != null) {
		var id = getIdFromGrid(productosGrid, 0);
		var tipoAccion = jQuery("#tipoAccion").val();
		var claveNegocio = jQuery("#claveNegocio").val();
		sendRequestJQ(null,
				'/MidasWeb/negocio/producto/desplegarConfiguracion.action'
						+ "?id=" + id + "&tipoAccion="+tipoAccion+"&claveNegocio="+claveNegocio, 'contenido_detalle',
				null);
	}
}

function verDetalleProducto(tipoAccion, claveNegocio) {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null, '/MidasWeb/negocio/producto/verDetalle.action?id='+idNegocioProducto+"&tipoAccion="+tipoAccion+"&claveNegocio="+claveNegocio,
			'contenido_productoInfo', null);
}

function desplegarDerechos(){
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/configuracionderecho/mostrarContenedor.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_derechos', null);
}

function desplegarNegocioTipoPoliza() {
	
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/tipopoliza/mostrar.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_tipoPoliza', null);
}


function desplegarNegocioLineas() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();;
	sendRequestJQ(
			null,
			'/MidasWeb/negocio/producto/tipopoliza/seccion/mostrarNegocioSeccion.action?idToNegProducto='
					+ idNegocioProducto, 'contenido_lineas',
			null);
}

function desplegarNegocioTarifa() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/tarifa/mostrarNegocioTarifa.action?idToNegProducto='
					+ idNegocioProducto, 'contenido_tarifa',
			'');
}


function desplegarNegocioCobPaqSeccion() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/cobertura/mostrar.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_coberturas', 'initGridsNegocioCobPaqSeccion()');
}

function desplegarNegocioTarifa() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/tarifa/mostrarNegocioTarifa.action?idToNegProducto='
					+ idNegocioProducto, 'contenido_tarifa',
			'');
}

function desplegarNegocioCobPaqSeccion() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/cobertura/mostrar.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_coberturas', 'initGridsNegocioCobPaqSeccion()');
}

function deplegarNegocioAgentes() {
	limpiarDivsGeneral();
	sendRequestJQ(null,
			'/MidasWeb/negocio/agente/mostrarContenedor.action', 'contenido_agentes', '');
}

function guardar(tipoAccion) {
	var target = 'contenido';
	if(document.getElementById('txtDescripcion').value == '' || document.getElementById('txtDescripcion').value == null || document.getElementById('fecha2').value == '' || document.getElementById('fecha2').value == null){
		target = 'contenido_detalle'
	}
	sendRequestJQ(null, "/MidasWeb/negocio/guardar.action?"
			+ jQuery(document.negocioForm).serialize(),target,
			null);
}

function desplegarNegocioPaquetes() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/paquete/mostrar.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_paquetes', null);
}

function activarNegocio() {
	if (confirm('\u00BFEst\u00e1 seguro que desea Autorizar el negocio?')) {
		sendRequestJQ(null, "/MidasWeb/negocio/activar.action",targetWorkArea,
		null);
	}
}


function eliminarNegocio(idToNegocio, claveEstatus){
	var claveNegocio = jQuery("#claveNegocio").val();
	if (confirm("\u00BFEst\u00e1 seguro de Eliminar el negocio "+ idToNegocio + "?")) {
		sendRequestJQ(null,
				"/MidasWeb/negocio/deleteRowFromGrid.action?idToNegocio="+idToNegocio, targetWorkArea, "listarNeg('"
						+ claveNegocio + "','" + claveEstatus + "');");
	}
}

function CambiarEstatus(idToNegocio, claveEstatus) {
	var claveNegocio = dwr.util.getValue("claveNegocio");

	switch (claveEstatus) {
	case 0:
		if (confirm("\u00BFEst\u00e1 seguro de Eliminar el negocio?")) {
			sendRequestJQ(null,
					"/MidasWeb/negocio/deleteRowFromGrid.action", targetWorkArea, "listarNeg('"
							+ claveNegocio + "','" + claveEstatus + "');");
		}

		break;

	case 1:
	case 2:
		claveEstatus = borradoLogico;

		sendRequestJQ(null, "/MidasWeb/negocio/guardarFromGrid.action?negocio.claveEstatus=" + claveEstatus, targetWorkArea,
				"listarNeg('" + claveNegocio + "','" + claveEstatus + "');");
		break;

	}
}

function listarNeg(claveNegocio, claveEstatus) {
	document.getElementById("negocioGrid").innerHTML = '';
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	negocioGrid = new dhtmlXGridObject("negocioGrid");
	negocioGrid
			.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
	mostrarIndicadorCarga('indicador');	
	negocioGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	sendRequestJQ(null, "/MidasWeb/negocio/listar.action?claveNegocio="
			+ claveNegocio + "&claveEstatus=" + claveEstatus, targetWorkArea,
			"listarNegocio();");
}

function desplegarCobranzaFormaPago() {
	var idNegocioProducto = getIdNegocioProducto();
	limpiarDivsProducto();
	sendRequestJQ(null,
			'/MidasWeb/negocio/formapago/mostrarContenedor.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_condicionesCobranza',
			null);
}

function desplegarCobranzaMedioPago() {
	var idNegocioProducto = getIdNegocioProducto();
	//limpiarDivsCobranza();
	sendRequestJQ(null,
			'/MidasWeb/negocio/mediopago/mostrarMedioPago.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_negocioMedioPago',
			null);
}

function desplegarFormaPago() {
	var idNegocioProducto = getIdNegocioProducto();
	//limpiarDivsCobranza();
	sendRequestJQ(null,
			'/MidasWeb/negocio/formapago/mostrarFormaPago.action?idToNegProducto='
					+ idNegocioProducto,
			'contenido_negocioFormaPago',
			null);
}

function desplegarParamGrales(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/contenedorParamGrales.action?idNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+mensaje+'&tipoMensaje='+tipoMensaje+'&nivelActivo='+nivelActivo, 'contenido_parametrosGrales',
			null);
}

function inicializarParametrosGeneralesAccordion() {	
		accordionParametrosNeg = new dhtmlXAccordion(
			"accordionParametrosGeneralesNegocio");
	
	var id = getIdNegocio();
	gridDatosNeg001 = accordionParametrosNeg.addItem(ID_CELDA_DATOS_GENERALES, "Datos Generales");
	gridEndosoNeg001 = accordionParametrosNeg.addItem(ID_CELDA_DERECHOS_ENDOSO, "Derechos Endoso");
	gridPolizaNeg001 = accordionParametrosNeg.addItem(ID_CELDA_DERECHOS_POLIZA, "Derechos P\u00f3liza");
	gridAltaIncisoNeg001 = accordionParametrosNeg.addItem(ID_CELDA_DERECHOS_ALTA_INCISO, "Derechos Alta Inciso");
	accordionParametrosNeg.setEffect(true);
	accordionParametrosNeg.attachEvent("onActive", function(itemId) {
		activeCell = itemId;
		return true;
	});
	
	accordionParametrosNeg.cells(ID_CELDA_DATOS_GENERALES).attachURL('/MidasWeb/negocio/verdetalleParamGrales.action');
	accordionParametrosNeg.cells(ID_CELDA_DERECHOS_ENDOSO).attachURL('/MidasWeb/negocio/derechos/mostrarDerechosEndoso.action'+ "?idToNegocio=" + id + "&esAltaInciso=0");
	accordionParametrosNeg.cells(ID_CELDA_DERECHOS_POLIZA).attachURL('/MidasWeb/negocio/derechos/mostrarDerechosPoliza.action'+ "?idToNegocio=" + id);
	accordionParametrosNeg.cells(ID_CELDA_DERECHOS_ALTA_INCISO).attachURL('/MidasWeb/negocio/derechos/mostrarDerechosEndoso.action'+ "?idToNegocio=" + id + "&esAltaInciso=1");
	accordionParametrosNeg.openItem(ID_CELDA_DATOS_GENERALES);
 
}

function verInfoGeneral(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
//	var mensaje = jQuery("#mensaje").val();
//	var tipoMensaje = jQuery("#tipoMensaje").val();
//	var pathMensaje = "";
//	if(mensaje != ''){
//		pathMensaje = "?mensaje="+mensaje+"&tipoMensaje="+tipoMensaje;
//	}
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/mostrarInfoGeneral.action?idNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
		'&claveNegocio='+claveNegocio+'&mensaje='+mensaje+'&tipoMensaje='+tipoMensaje+'&nivelActivo='+nivelActivo,
			'contenido_detalle',
			null);
}


function limpiarDivsCobranza() {
	limpiarDiv('contenido_negocioFormaPago');
	limpiarDiv('contenido_negocioMedioPago');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function limpiarDivsProducto() {
	limpiarDiv('contenido_tipoPoliza');
	limpiarDiv('contenido_lineas');
	limpiarDiv('contenido_tarifa');
	limpiarDiv('contenido_coberturas');
	limpiarDiv('contenido_paquetes');
	limpiarDiv('contenido_condicionesCobranza');
}

function limpiarDivsGeneral() {
	limpiarDivsCobranza();
	limpiarDivsProducto();
	limpiarDiv('contenido_detalle');
	limpiarDiv('contenido_clientes');
	limpiarDiv('contenido_agentes');
	limpiarDiv('contenido_parametrosGrales');
	limpiarDiv('contenido_bonos');
	limpiarDiv('contenido_renovacion');
	limpiarDiv('contenido_emailAlertasSAPAMIS');
	limpiarDiv('contenido_tipoVigencia');
}

function desplegarCopiarNegocio() {
	var url = "/MidasWeb/negocio/mostrarCopiarNegocio.action?idToNegocio="+ dwr.util.getValue("id");
	mostrarVentanaModal("CopiarNegocio", "Copiar Negocio",200, 320,350, 200, url);	
}

function verDetalleBonos(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/bonocomision/mostrar.action?idNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+'&tipoMensaje='+'&nivelActivo='+nivelActivo, 'contenido_bonos',
			null);
}

function desplegarNegocioClientes(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo){
	limpiarDivsGeneral();
	sendRequestJQ(null,'/MidasWeb/negocio/cliente/asociar/mostrarContenedor.action?idNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+mensaje+'&tipoMensaje='+tipoMensaje+'&nivelActivo='+nivelActivo+"&tipoRegreso=1", 'contenido_clientes', null);
}

function verCondicionesRenovacion(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/renovacion/mostrarContenedor.action?idNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+'&tipoMensaje='+'&nivelActivo='+nivelActivo, 'contenido_renovacion',
			null);
}

function getIdNegocio(){
	return dwr.util.getValue("id");
	
}

function getIdNegocioProducto(){
	return dwr.util.getValue("idToNegProducto");
}

function validaNombreNegocio(){
    if(document.getElementById("nombreNegocio").value==null || document.getElementById("nombreNegocio").value==''){
      alert('Es requerido el nombre para copiar el negocio');
          return false;
    }else{
    	return true;
    };
}

function cerrarVentanaCopiarNegocio() {
	parent.ventanaCopiarNegocio.hide();
	parent.ventanaCopiarNegocio = null;
}



function desplegaZonas(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/zonacirculacion/mostrar.action?id='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+''+'&tipoMensaje='+''+'&nivelActivo='+nivelActivo , 'contenido_zona',
			null);
}

function inicializarZonasAccordion() {
	accordionZonasNeg = new dhtmlXAccordion(
			"accordionZonasNegocio");
	var id = getIdNegocio();
	gridNegZonas001 = accordionZonasNeg.addItem(ID_CELDA_ZONAS_GENERALES, "Configurar Estado");
	gridNegMunicipio001 = accordionZonasNeg.addItem(ID_CELDA_ZONAS_MUNICIPIO,       "Configurar Municipio");
	accordionZonasNeg.setEffect(true);
	accordionZonasNeg.attachEvent("onActive", function(itemId) {
		activeCell = itemId;
		return true;
	});
	accordionZonasNeg.cells(ID_CELDA_ZONAS_GENERALES).attachURL(
					'/MidasWeb/negocio/zonacirculacion/listar.action?id=' + id)				
	accordionZonasNeg.cells(ID_CELDA_ZONAS_MUNICIPIO).attachURL(
					'/MidasWeb/negocio/zonacirculacion/negociomunicipio/listar.action' +"?idToNegocio=" + id);
		accordionZonasNeg.openItem(ID_CELDA_ZONAS_GENERALES);
	
}

function fechaRequerida(fecha){
    if (fecha == undefined || fecha.value == "" ){
    	alert('Se requiere de una fecha inicio de operaci�n para almacenar los datos generales del Negocio');
        return false;
    }else{
        return true;
    }
}


function eliminarNegocioProducto(idToNegProducto,id,idproducto,claveNegocio,idNegocio,tipoAccion){
	if (confirm("\u00BFEst\u00e1 seguro de Eliminar el Producto "+ idproducto + "?")) {
		sendRequestJQ(null,
				"/MidasWeb/negocio/producto/deleteProducto.action?idToNegProducto="+idToNegProducto + "&id=" + id+'&claveNegocio='+claveNegocio+'&idNegocio='+idNegocio+'&tipoAccion='+tipoAccion, targetWorkArea, null);
	}
}



function desplegarConfiguracionProducto(id) {
	    var tipoAccion="3";
		sendRequestJQ(null,
				'/MidasWeb/negocio/producto/listarProductosAsociados.action'
						+ "?id=" + id, 'contenido_detalle',	
				null);
}

function validaciones(){
	var today= new Date();
	var day=0; var month=0; var year=0;
	var todayStr;
	day= today.getDate();
	month= today.getMonth();
	year= today.getFullYear();
	todayStr=  day + "/" + "0"+month + "/" + year;
   
    if(document.getElementById("fecha").value!=""){
    	var fecha = document.getElementById("fecha").value;
    	var arrayFecha = fecha.split("/");
    	var resultado = comparaFechaVencimiento(day,month, year, arrayFecha[0], (arrayFecha[1] - 1), arrayFecha[2]);
     if(resultado >= 0){
	      alert('La fecha de Vencimiento debe de ser a futuro');
          return false;	 
     }
    }    
    return true;
}

function comparaFechaVencimiento(dd1, mm1, yy1, dd2, mm2, yy2) {
	var f1 =  new Date(yy1, mm1, dd1);
	var f2 =  new Date(yy2, mm2, dd2);
	return f1.getTime() - f2.getTime();
}
function imprimirNegocio(idToNegocio) {
	mostrarMensajeConfirm(
			"\u00BFEstá seguro de que desea imprimir el negocio #"
					+ idToNegocio
					+ "\u003F Este proceso puede demorar algunos minutos.",
			"20", "$_imprimirNegocio(" + idToNegocio + ")", null, null);
}
function $_imprimirNegocio(idToNegocio){
	blockPage();
	var win = window.open(imprimirNegocioPath+"?idToNegocio="+idToNegocio,null,
    "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
	 var timer = setInterval(function() {   
	        if(win.closed) {  
	            clearInterval(timer);  
	            unblockPage();
	        }  
	    }, 1000); 
}

/**
 * Inicio de
 * Sección de negocio/condicion especial
 * by: eTrejo
 */


function verNegocioCondicionEspecial(){
	limpiarDivsGeneral();
	sendRequestJQ(
			null, 
			'/MidasWeb/negocio/condicionespecial/mostrarContenedor.action'
//			+'?idNegocio='+
//				idNegocio+'&tipoAccion='+tipoAccion+
//				'&claveNegocio='+claveNegocio+'&mensaje='+
//				'&tipoMensaje='+'&nivelActivo='+nivelActivo
				, 
				'contenido_negocioCondicionEspecial',
			null
	);
}

function verNegocioUsuarioEdicionImpresion(){
	limpiarDivsGeneral();
	sendRequestJQ(
			null, 
			'/MidasWeb/negocio/impresion/mostrarContenedor.action'
//			+'?idNegocio='+
//				idNegocio+'&tipoAccion='+tipoAccion+
//				'&claveNegocio='+claveNegocio+'&mensaje='+
//				'&tipoMensaje='+'&nivelActivo='+nivelActivo
				, 
				'contenido_negocioUsuarioEdicionImpresion',
			null
	);
}


/**
 * Fin de
 * Sección de negocio/condicion especial 
 */

function desplegaDescuentosPorEstado(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/estadodescuento/mostrar.action?id='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+''+'&tipoMensaje='+''+'&nivelActivo='+nivelActivo , 'contenido_descuentoPorEstado',
			null);
}

/**
 * Pestania Antecedentes
 * */
function desplegarAntecedentes(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo) {
	limpiarDivsGeneral();
	sendRequestJQ(null, '/MidasWeb/negocio/antecedentes/mostrarContenedor.action?idToNegocio='+idNegocio+'&tipoAccion='+tipoAccion+
			'&claveNegocio='+claveNegocio+'&mensaje='+'&tipoMensaje='+'&nivelActivo='+nivelActivo, 'contenido_antecedentes',
			null);
}

/**
 * Pestania Compensaciones Adicionales
 * */
function verNegocioCompensacionAdicional(idNegocio){
	cargarCompensacion(idNegocio);
}

function cargarCompensacion(idNegocio){
	limpiarDivsGeneral();
	sendRequestJQ(null, 
			     '/MidasWeb/negocio/compensacionadicional/mostrarContenedor.action?filtroCompensacion.idNegocio=' + idNegocio, 
				 'contenido_compensacionAdicional',
			    null);
}

var winCompensacion ;
var temporizadoWinComp;
var compensacionRecargado = false;

function wCompensacion(idNegocioCompensacion){
	blockPage();
	var ramoId = 'ramo=A&';
	winCompensacion = window.open("/MidasWeb/compensacionesAdicionales/init.action?"+ramoId+"idNegocio=" + idNegocioCompensacion,
		    'WinCompensaciones', 'height=600,width=1200,modal=yes,resizable=yes,scrollbars=auto');
	winCompensacion.focus();
	compensacionRecargado = false;
	temporizadoWinComp = setInterval("checkWindowComp();", 3000);
}

function wCloseCompensacion(){
	var idNegocio = jQuery('#txtIdNegocio').val();
	unblockPage();
	compensacionRecargado = true;
	cargarCompensacion(idNegocio);
}

function checkWindowComp(){
	if (winCompensacion.closed == true ){
		clearInterval(temporizadoWinComp);
		if (compensacionRecargado == false){
		     wCloseCompensacion();
		}
	}
}

function vmCompensacionesAdicionales(){
	var url = "/MidasWeb/negocio/compensacionadicional/mostrarConfiguracion.action?idNegocio=" + idNegocioCompensacion;
	mostrarVentanaModal("vmCompensacionesAdicionales", "Configurar Compensaciones Adicionales", 50, 50,1024, 600, url);
}

function gridCompensaciones() {
	var idNegocio = jQuery('#txtIdNegocio').val();
    
    document.getElementById("compensacionesGrid").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	var compensacionesGrid = new dhtmlXGridObject("compensacionesGrid");
	
	if (idNegocio > 0 ) {
		mostrarIndicadorCarga('indicador');	
		compensacionesGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
		});	
		
	
		compensacionesGrid.load("/MidasWeb/negocio/compensacionadicional/mostrarResumen.action?filtroCompensacion.idNegocio="
					+ idNegocio);
	}
}

function inicializarCompensacionAdicionalAccordion(idNegocioCompensacion) {
	accordionCompAdicional = new dhtmlXAccordion("accordionCompensacionAdicional");
    var id = getIdNegocio();
    
    gridCompAdicionalResumen = accordionCompAdicional.addItem('idCompResumen', "Resumen");
    gridCompAdicionalConfig = accordionCompAdicional.addItem('idCompConfig', "Configuración");

    accordionCompAdicional.setEffect(true);
    accordionCompAdicional.attachEvent("onActive", function(itemId) {
    	activeCell = itemId;
    	return true;
    });
    var urlResumen = '/MidasWeb/negocio/compensacionadicional/mostrarContenedorResumen.action?filtroCompensacion.idNegocio=' + idNegocioCompensacion;
    var ramoId = 'ramo=A&';
    var urlConfig = "/MidasWeb/compensacionesAdicionales/init.action?"+ramoId+"idNegocio=" + idNegocioCompensacion;
    //var urlConfig = '/MidasWeb/negocio/compensacionadicional/mostrarConfiguracion.action'+ "?idToNegocio=" + id ;
    accordionCompAdicional.cells('idCompResumen').attachURL(urlResumen);
    accordionCompAdicional.cells('idCompConfig').attachURL(urlConfig);
    accordionCompAdicional.openItem('idCompResumen');
}

function compAdicAccordionChangeTo(acordeon){
	parent.accordionCompAdicional.openItem(acordeon);
}

/**
 * Fin de:
 * Pestania Compensaciones Adicionales
 * */
 
 /**
  * Pestaña Email Sap Amis
  **/
function desplegarContenedorEmail() {
	limpiarDivsGeneral();
	var path='/MidasWeb/negocio/sapamisemail/contenedorEmail.action';
	sendRequestJQ(null, path, 'contenido_emailAlertasSAPAMIS', null);
}



function mostrarRecuotificacionContenedor(){
	limpiarDivsGeneral();
	sendRequestJQ(
		null, 
		'/MidasWeb/negocio/recuotificacion/mostrarContenedor.action'			, 
			'contenido_recuotificacion',
		null
	);
}
function mostrarContCanalVenta(){
	limpiarDivsGeneral();
	sendRequestJQ(
		null, 
		'/MidasWeb/negocio/canalventa/mostrarContenedorCanalVenta.action'			, 
			'contenido_canalventa',
		null
	);
}

function mostrarContenedorTiposVigencia(){
	limpiarDivsGeneral();
	sendRequestJQ(
			null, 
			'/MidasWeb/negocio/tipovigencia/mostrarContenedor.action', 
				'contenido_tipoVigencia',
			null
		);
}