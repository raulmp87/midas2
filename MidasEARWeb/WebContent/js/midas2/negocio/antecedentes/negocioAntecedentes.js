/**
 * Componentes CENA
 * ################
 */



/**
 * ####################################
 * Antecedentes Anexos
 */

/**
 * Grid Archivos Anexos
 */
var negocioAntecedentesAnexosGrid;
var negocioAntecedentesAnexosProcessor;
function obtenerNegocioAntecedentesAnexos(){
	negocioAntecedentesAnexosGrid = new dhtmlXGridObject('negocioAntecedentesAnexosGrid');
	negocioAntecedentesAnexosGrid.load("/MidasWeb/negocio/antecedentes/obtenerAntecedentesAnexos.action?idNegocio="+ dwr.util.getValue("idNegocio")); //idNegocio
	negocioAntecedentesAnexosProcessor = new dataProcessor("/MidasWeb/negocio/antecedentes/actualizarAntecedentesAnexos.action");
	negocioAntecedentesAnexosProcessor.setUpdateMode("off");
	negocioAntecedentesAnexosProcessor.enableDataNames(true);
	negocioAntecedentesAnexosProcessor.setTransactionMode("POST");
	negocioAntecedentesAnexosProcessor.init(negocioAntecedentesAnexosGrid);
}
function iniciaGridAntecedentesAnexos() {
	refrescarGridAntecedentesAnexos(null,null,null,null);
}
function refrescarGridAntecedentesAnexos(sid,action,tid,node){
	obtenerNegocioAntecedentesAnexos();
	return true; 
}

/**
 * Agregar archivos anexos a CENA
 */
function mostrarAnexarArchivoCenaWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	anexarArchivoWindow = dhxWins.createWindow("adjuntarArchivoSolicitud", 34, 100, 440, 265);
	anexarArchivoWindow.setText("Adjuntar archivos");
	anexarArchivoWindow.button("minmax1").hide();
	anexarArchivoWindow.button("park").hide();
	anexarArchivoWindow.setModal(true);
	anexarArchivoWindow.center();
	anexarArchivoWindow.denyResize();
	anexarArchivoWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    
    vault.onUploadComplete = function(files) {
    	parent.dhxWins.window("adjuntarArchivoSolicitud").close();
    	var funcionRedirecciona = "iniciaGridAntecedentesAnexos();";
    	mostrarVentanaMensaje('30','Documento Registrado con \u00e9xito',funcionRedirecciona);    	    	
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "36");
}

/**
 * Descarga archivos CENA de Fortimax
 */
function descargarDocumentoCENA(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

/**
 * Actualiza Grid Archivos Anexos
 */
function dataGridSendDataCena(processor) {
	processor.sendData();
}
function ejecutarFuncionResp(funcionAEjecutar, bandera, funcionExito, funcionNoExito) {
	eval(funcionAEjecutar);
	if (bandera != null) {
		if (!bandera) {
			eval(funcionExito);
		}
		else {
			eval(funcionNoExito);
		}
	}
}
function mostrarMensajeExitoCena() {
	mostrarMensajeExito();
	iniciaGridAntecedentesAnexos();
}
function actualizarGridCENAAnexos(processor) {
	bandera = false;
	ejecutarFuncionResp('dataGridSendDataCena(' + processor + ')', bandera,'mostrarMensajeExitoCena()', 'mostrarMensajeFallido()');
}



/**
 * #########################################
 * Antecedentes Comentarios
 */

/**
 * Grid Comentarios
 */
var negocioAntecedentesComentariosGrid;
function obtenerNegocioAntecedentesComentarios(){
	negocioAntecedentesComentariosGrid = new dhtmlXGridObject('negocioAntecedentesComentariosGrid');
	negocioAntecedentesComentariosGrid.load("/MidasWeb/negocio/antecedentes/obtenerAntecedentesComentarios.action?idNegocio="+ dwr.util.getValue("idNegocio"));
}
function iniciaGridAntecedentesComentarios() {
	refrescarGridAntecedentesComentarios(null,null,null,null);
}
function refrescarGridAntecedentesComentarios(sid,action,tid,node){
	obtenerNegocioAntecedentesComentarios();
	return true; 
}

/**
 * Guardar Comentario
 */
function guardarComentario(){
	if ( this.validaComentarioForm() ){
		var comentario = document.getElementById("negocioCEAComentarios.comentario").value;
		var idToNegocio = document.getElementById("idNegocio").value;
		jQuery.ajax({
			  type     : 'POST',
			  url      : '/MidasWeb/negocio/antecedentes/guardarAntecedentesComentarios.action',
			  data     : {comentario:comentario, idToNegocio:idToNegocio},
			  dataType : 'json',
			  async    : true,
			  success  : function(data){
				  iniciaGridAntecedentesComentarios();
				  limpiaCajasTexto();
				  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
			  }
		});
	}
}
function validaComentarioForm(){
	var comentario = document.getElementById("negocioCEAComentarios.comentario").value;	
	if ( comentario == ''){
		mostrarMensajeInformativo("¡Necesario ingresar su Comentario!", 10, null);
		return false;
	}
	return true;
}
function limpiaCajasTexto(){
	document.getElementById("negocioCEAComentarios.comentario").value = '';
}
