function procesarInfo(){
	//var fechaCorte2 = document.getElementById("fechaCorte").value;
	var tipoArchivo = document.getElementById("tipoArchivo").value;
	if(tipoArchivo == '0'){
		mostrarMensajeInformativo("Favor seleccionar Tipo Archivo",20, null,null);
		return;
	}/*else if(fechaCorte2 == undefined || fechaCorte2 == null || fechaCorte2 == '' )
	{
		mostrarMensajeInformativo("Favor seleccionar Fecha Corte",20, null,null);
		return;
	}*/
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    				var e = document.getElementById("tipoArchivo");
    				var tipoArchivo = e.options[e.selectedIndex].value;
    				dwr.util.setValue("tipoArchivo", tipoArchivo);
        			dwr.util.setValue("idToControlArchivo", idToControlArchivo);
        			//dwr.util.setValue("fechaCorte", fechaCorte.value);
        			
        			sendRequest(null, procesarInfoPath +"?"+jQuery(document.generacionForm).serialize(), 'contenido', 'prueba()');
        			document.getElementById("resultadosDocumentos").innerHTML = document.getElementById("mensaje").value;
        		}
    		} 
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

function prueba(){
	mostrarMensajeInformativo(document.getElementById("mensaje").value,20, null,null);
	document.getElementById("resultadosDocumentos").innerHTML = document.getElementById("mensaje").value;
}
