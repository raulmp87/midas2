var edoCuentaCFDIGrid;

function descargarPlantillaECAbreviada(){
	descargarPlantilla(1);
}

function descargarPlantillaECEstandar(){
	descargarPlantilla(2);
}

function descargarPlantillaCancelaciones(){
	descargarPlantilla(3);
}

function descargarPlantilla(tipoPlantilla){
	
	var contratoId = dwr.util.getValue("contratoId");
	
	if (contratoId == 0 && tipoPlantilla != 3) {
		alert("Seleccione un contrato")
	} else {
		dwr.util.setValue("tipoPlantilla", tipoPlantilla);
		window.open(obtenerPlantillaPath+"?"+jQuery(document.generacionForm).serialize(), "download");
	}
	
}

function procesarInfo(){
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			dwr.util.setValue("idToControlArchivo", idToControlArchivo);
        			window.open(procesarInfoPath +"?"+jQuery(document.generacionForm).serialize(), "download");
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "40");
}

function imprimirCFDI(folio, esPDF) {
	
	//dwr.util.setValue("folio", folio);
	window.open(imprimirPath+"?esPDF="+ esPDF + "&folio=" + folio + "&" + jQuery(document.impresionForm).serialize(), "download");
	
}

function buscarCFDI() {
	
	edoCuentaCFDIGrid = new dhtmlXGridObject("edoCuentaCFDIGrid");
	var form = document.impresionForm;
		
	var contratoId = dwr.util.getValue("contratoId");
	
	var ramoId = dwr.util.getValue("ramoId");
	
	var nombreReasCorr = dwr.util.getValue("nombreReasCorr");
	
	var fIniPer = dwr.util.getValue("fIniPer");
	
	var fFinPer = dwr.util.getValue("fFinPer");
	
	var folio = dwr.util.getValue("folio");
	
	if (contratoId == '0' && ramoId == '0' && nombreReasCorr == '0' && (fIniPer == null || fIniPer=="") && (fFinPer == null || fFinPer=="") && (folio == null || folio=="")) {
		alert("Introduzca al menos un filtro");
	} else {
		if(form!=null){
			edoCuentaCFDIGrid.load(buscarImpPath+"?"+jQuery(form).serialize());
		}else{
			edoCuentaCFDIGrid.load(buscarImpPath);
		}
	}
	
}

