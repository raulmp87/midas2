var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";
var targetWorkarea = 'contenido';

function descargarPlantilla(tipoPlantilla){
	
	var tipoArchivo = dwr.util.getValue("tipoArchivo");
	
	if (tipoArchivo == 0) {
		mostrarMensajeInformativo('Por favor elija una plantilla v\xE1lida.', '10', null,null);
	} else {
		window.open(obtenerPlantillaPath+"?"+jQuery(document.cargaRR4Form).serialize(), "download");
	}
	
}

function deshabilitaBotones(){
	
	var tipoArchivo = dwr.util.getValue("tipoArchivo");
	
	if (tipoArchivo == 2) {
		document.getElementById("fechaCorte").disabled = true;
		jQuery(".wwctrl").attr("disabled","disabled");
	}else{
		document.getElementById("fechaCorte").disabled = false;
		
		jQuery(".wwctrl").attr("disabled","");
		
	}
	
}

function actualizarCalificacionesRR4(){
	var anioCorte = document.getElementById("fechaCorte").value;
	var cve_negocio = 2;
	var location = "/MidasWeb/danios/reportes/reporteRCS/actualizaCalificaciones.do";
	
	if(anioCorte != undefined && anioCorte != null && anioCorte != '' ) {
		location = location+"?anioCorte="+anioCorte+"&cve_negocio="+cve_negocio;
		jQuery.ajax({
		    type: "POST",
		    url: location,
		    data: null,
		    dataType: null,
		    async: true,
		    contentType: defaultContentType,
		    success : function(data) {
		    	jQuery("#mensaje_popup").html(data);
		    	mostrarMensajeInformativo(document.getElementById("msgDocumentos").textContent, '20');
		    }
		});
		jQuery("#nsOficina").focus();
		return;
	} else {
		mostrarMensajeInformativo('No se ha ingresado una Fecha.', '20');
		jQuery("#nsOficina").focus();
		return;
	}
}
  
function procesarInfo(accion){
	var tipoArchivo = dwr.util.getValue("tipoArchivo");
	var fechaCorte = dwr.util.getValue("fechaCorte");
	var mensaje = dwr.util.getValue("mensaje");
	var action = accion;

	if (tipoArchivo == 0 || fechaCorte == undefined || fechaCorte == "" || fechaCorte == null) {
		mostrarMensajeInformativo('Por favor elija una opci\u00f3n v\xE1lida para los siguientes campos: Reporte y Fecha Corte', '10', null,null);	
	}else{
		
		if(dhxWins != null) 
			dhxWins.unload();

		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
		adjuntarDocumento.setText("Cargar plantilla");
		adjuntarDocumento.button("minmax1").hide();
		adjuntarDocumento.button("park").hide();
		adjuntarDocumento.setModal(true);
		adjuntarDocumento.center();
		adjuntarDocumento.denyResize();
		adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

		var vault = new dhtmlXVaultObject();
	    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
	    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
	            "/MidasWeb/sistema/vault/getInfoHandler.do",
	            "/MidasWeb/sistema/vault/getIdHandler.do");
	    vault.setFilesLimit(1);
	    vault.onUploadComplete = function(files) {
	    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
	    		onSuccess : function(transport) {
	    			var xmlDoc = transport.responseXML;
	    			var items = xmlDoc.getElementsByTagName("item");
	    			var item = items[0];
	    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	    			if (idRespuesta == 1){
	    				
	    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
	    				var e = document.getElementById("tipoArchivo");
	    				var tipoArchivo = e.options[e.selectedIndex].value;
	    				dwr.util.setValue("tipoArchivo", tipoArchivo);
	        			dwr.util.setValue("idToControlArchivo", idToControlArchivo);
	        			dwr.util.setValue("fechaCorte", fechaCorte);
	        			dwr.util.setValue("accion", accion);
	        			
	    				new Ajax.Request(procesarInfoPath, {
	    					method : "post",
	    					asynchronous : false,
	    					parameters : jQuery(document.cargaRR4Form).serialize(),
	    					onSuccess : function(transport) {
	    							
	    						var resultado = transport.responseText.evalJSON();
	    						var res = resultado.mensaje;
	    						
	    						if(res == "Carga Finalizada."){
	    							mostrarMensajeInformativo(res, '30', null,null); 
	    						}else if(res == "Carga Bloqueada."){
	    							mostrarMensajeInformativo(res, '20', null,null);
	    						}else if(res == "Carga Existente."){
	    							mostrarMensajeConfirm("Ya existe informaci&oacute;n para ese corte.\r\n"+"\u00BFDesea recargar?","20","cerrarMensajeInformativo();recargarReporteRR6();",null,null);
	    						}else{
	    							mostrarMensajeInformativo(res, '10', null,null);
	    						}
	    					  },
	    					  onFailure: function() { alert('No se pudo cargar el archivo.'); }
	    				});    				
	    				
	        		}
	    		} 
	    	});
	        parent.dhxWins.window("cargaInfo").close();
	    };
	    vault.onAddFile = function(fileName) {    	
	        var ext = this.getFileExtension(fileName); 
	        if (ext != "xls" && ext != "xlsx") { 
	           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
	           return false; 
	        } 
	        else return true; 
	     }; 
	     
	    vault.onBeforeUpload = function(fileName) {
	    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
	    	if(respuesta){
	    		return true;
	    	}else{
	    		return false;
	    	}
	    }
	    vault.create("vault");
	    vault.setFormField("claveTipo", "16");
	 }
	}
