// Nota: ciertas funciones funcionan con variables declaradas en: grupoAgenteHeader.jsp
var grid;
var asociarGrupoAgenteGrid;
var asociarAgenteGrid;

function agregarAgente(tipoAccion) {
	if(tipoAccion==null || tipoAccion==''){
		tipoAccion=parent.dwr.util.getValue("tipoAccion");
	}
	var path= "/MidasWeb/fuerzaventa/grupoAgente/verDetalle.action";
	sendRequestJQ(null, path + "?tipoAccion=" + tipoAccion, targetWorkArea, 'dhx_init_tabbars();');	
}

function regresarPaginaPrincipal(){
	var path= "/MidasWeb/fuerzaventa/grupoAgente/mostrarContenedor.action";
	sendRequestJQAsync(null, path, targetWorkArea, null);
}

function asociarAgentes(){
	var idSelected = grid.getSelectedRowId();
	if(idSelected != null){
		sendRequestJQ(null, asociarGrupoAgentePath+"?grupoAgenteDTO.id=" + idSelected,targetWorkArea);
	}else{
		mostrarMensajeInformativo("Seleccione un Registro", '20');
	}
}

function iniciaGrids(){	
	refrescarGrids();
}

function refrescarGrids(){
	obtenerGrupoAgentes();
	obtenerAgentes();
	return true;
}

function obtenerGrupoAgentes(){
	var idGrupo = jQuery("#txtIdGrupo").val();
	if(idGrupo == ""){
		idGrupo = 0;
	}
	document.getElementById("asociarGrupoAgenteGrid").innerHTML = '';
	asociarGrupoAgenteGrid = new dhtmlXGridObject('asociarGrupoAgenteGrid');	
	asociarGrupoAgenteGrid.load(obtenerAsociarGrupoAgentePath + "?tipoGrupoAgenteDTO.id=" + idGrupo);		
}

function obtenerAgentes(idAgente){
	if(idAgente == null || idAgente == ""){
		idAgente = 0;
	}
	document.getElementById("asociarAgenteGrid").innerHTML = '';
	asociarAgenteGrid = new dhtmlXGridObject('asociarAgenteGrid');
	asociarAgenteGrid.load(obtenerAgentesPath + "?filtroAgente.id=" + idAgente);
}


function filtrarAgentesAsociados() {
	var urlFil = listarFiltradoAgenteAsociadoPath + "?tipoAccion=" + tipoAccionAgente;
	listarFiltradoGenerico(urlFil, "asociarGrupoAgenteGrid",jQuery("#asociarAgenteGrupoForm"), idFieldAgente, varModal);
}

function filtrarAgentes() {
	var input1 = document.getElementById('campoBusqueda_id').value;
	var input2 = document.getElementById('campoBusqueda_nombreCompleto').value;
	var input3 = document.getElementById('campoBusqueda_gerencia').value;

	if (input1 == "" && input2  == "" && input3 =="") {
		mostrarMensajeInformativo("Almenos un campo es requerido para iniciar la busqueda.", '20');
        return false;
    }
	var urlFil = listarFiltradoAgentePath + "?tipoAccion=" + tipoAccionAgente;
	listarFiltradoGenerico(urlFil, "asociarAgenteGrid",jQuery("#asociarAgenteGrupoForm"), idFieldAgente, varModal);
}

function guardarAgentesAsociadosenGrupo() {
	
	var params = "grupoAgenteDTO.id=" + jQuery('#txtIdGrupo').val();
	jQuery.ajax({
        url: "/MidasWeb/fuerzaventa/grupoAgente/guardarAgentesAsociados.action?" + params,
        dataType: 'json',
        async:false,
        type:"POST",
        data: {
        	idsAgentes:asociarGrupoAgenteGrid.getAllRowIds(",")
        },
        success: function(data){
      	 unblockPage();
      	 mostrarMensajeInformativo(data.mensaje, '30');
        },
        input: function(data){
      	  mostrarMensajeInformativo(data.mensaje, '20');
      	  unblockPage();
        },
        beforeSend: function(){
				blockPage();
        },
        complete: function(){
      	  unblockPage();
        }
	});
	console.log(asociarGrupoAgenteGrid.getAllRowIds(","));
}

function guardarGrupo() {
	if(validateAll(true)){
		sendRequestJQ(null, guardarGrupoPath+"?"+ jQuery(document.grupoAgentesForm).serialize(),targetWorkArea);
	}	
}

function eliminarGrupo(){
	if(confirm('Esta seguro de eliminar este grupo')){
		sendRequestJQ(null,eliminarGrupoPath+"?"+ jQuery(document.grupoAgentesForm).serialize(),targetWorkArea);
	}
}