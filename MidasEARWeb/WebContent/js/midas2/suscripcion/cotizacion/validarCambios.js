/**
 * Valida que los inputs tengan un cambio
 * @author martin
 */	var displayMessage = 1;
		(function($){
			$.fn.cambio = function(){
				// Valor anterior al cambio
				var beforeValue = $(this).val();	
				this.change(function(){					
					//fix pago fraccionado									
					var pagoFracc = null;
					if($(this).attr("id") != 'porcentajePagoFraccionado'){
						pagoFracc = jQuery('#porcentajePagoFraccionado').val();
					}else{
						pagoFracc = beforeValue;
					}				
					if(beforeValue != '' && beforeValue != -1 && displayMessage == 1){
						if(beforeValue != $(this).val()){
							parent.mostrarMensajeInformativo(
									'Se requiere guardar los cambios efectuados antes de agregar un inciso o terminar la cotizaci\u00f3n',
									'30','$_confirmacionMensaje();');
						}
						displayMessage = 0;
					}
				});				
			}
		})(jQuery);
	
function $_confirmacionMensaje(){
	// Pone el resumen de riesgos en ceros
	jQuery('table[id=t_riesgo] td div').text('$0.00');
	// Oculta el boton de terminar cotizacion
	jQuery('div[id=terminarCotizacion]').css('display','none');
	// Oculta el boton de agregar inciso
	jQuery('div[id=agregarInciso]').css('display','none');
	// Oculta el boton de carga masiva inciso
	jQuery('div[id=cargaMasiva]').css('display','none');
}