var resumenCotizacionExpressWindow;

function mostrarResumenCotizacionExpress(idPaquete, idFormaPago) {
	var id = jQuery('input[name="id"]').val();
	var queryString = "id=" + id + "&idPaquete=" + idPaquete + "&idFormaPago=" + idFormaPago + "&cotizacionExpressDTO.paso="+2;
	var url = appendQueryString(guardarCotizacionExpressUrl, queryString);
	sendRequestWindow(null, url, resumenCotizacionVentanaInit);
}


function resumenCotizacionVentanaInit() {
	var wins = obtenerContenedorVentanas();
	resumenCotizacionExpressWindow = wins.createWindow("resumenCotizacionExpress", 400, 290, 800, 547);
	resumenCotizacionExpressWindow.center();
	resumenCotizacionExpressWindow.setModal(true);
	resumenCotizacionExpressWindow.setText("Resumen de la cotizaci\u00f3n express");
	return resumenCotizacionExpressWindow;
}

function cotizarExpress() {
	if(validarCotizar()){
		var idNegocioSeccion = dwr.util.getValue(jQuery("cotizacionExpressDTO.negocioSeccion.idToNegSeccion").selector);
		var queryString = "cotizacionExpressDTO.paso=1&idNegocioSeccion="+idNegocioSeccion;
		var url = appendQueryString(guardarCotizacionExpressUrl, queryString);
		sendRequestJQAsync(jQuery("#cotizacionExpressForm"), url, "contenido",null);
	}else{
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios.", "20", null);
	}
}

function validarCotizar(){
	var valido = true;
	
	var idEstado = dwr.util.getValue(jQuery("cotizacionExpressDTO.estado.stateId").selector);
	var idMunicipio = dwr.util.getValue(jQuery("cotizacionExpressDTO.municipio.municipalityId").selector);
	var idMarca = dwr.util.getValue(jQuery("cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.marcaVehiculoDTO.idTcMarcaVehiculo").selector);
	var idEstilo = dwr.util.getValue(jQuery("cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.id.strId").selector);
	var idModelo = dwr.util.getValue(jQuery("cotizacionExpressDTO.modeloVehiculo.id.modeloVehiculo").selector);
	
	if(idEstado == '' || idMunicipio == '' || idMarca == '' || idEstilo == '' || idModelo == ''){
		valido = false;
	}
	
	return valido;
}

function mostrarMatrizDatosCotizacionExpress() {
	sendRequestJQAsync(null, mostrarMatrizDatosCotizacionExpressUrl, "matrizConfiguracionExpress");
}

function mostrarBandejaCotizacion(id,idAgente){
//	var url = "/MidasWeb/suscripcion/solicitud/busquedaPaginada.action?claveNegocio=A&solicitud.idToPolizaAnterior=0&OpcionEmision=0&solicitud.claveTipoPersona=1&solicitud.idToSolicitud="+id;
	//var url = "/MidasWeb/suscripcion/solicitud/mostrar.action?claveNegocio=A";
	var url = "/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=A";
	resumenCotizacionExpressWindow.close();
	//script ="obtenerDatosAgente(jQuery('#idTcAgente').val());";
	sendRequestJQ(null, url ,'contenido', null);
}
function cotizarFormalmente() {
	var url = cotizarFormalmenteUrl;
	if(validarCotFormal()){
		sendRequestJQAsync(jQuery("#resumenForm"), url, "contenido", null);
	}
}

function validarCotFormal(){
	if (jQuery('#idAgenteCot').val() == '') {
		parent.mostrarMensajeInformativo("Favor de seleccionar un agente.", "20",null, null);
		return false;
	}
	return true;
}
var coberturaCotizacionExpressGrid;
function inicializarCotizacionExpressResumen() {
	inicializarCoberturaCotizacionExpressGrid();
}

function inicializarCoberturaCotizacionExpressGrid() {
	coberturaCotizacionExpressGrid = new dhtmlXGridObject("coberturaCotizacionExpressGrid");
	coberturaCotizacionExpressGrid.load(mostrarCoberturaCotizacionDhtmlxUrl);
}

function imprimirCotizacionExpress(){
	var form = jQuery('#cotizacionExpressForm').serialize();
	var path = "/MidasWeb/suscripcion/cotizacion-express/auto/imprimirCotizacionExpress.action?"+form;
	window.open(path, "Cotizacion_COT");
}

function enviarCorreoCotizacionExpress(){
	var url = '/MidasWeb/suscripcion/cotizacion-express/auto/capturarDatosCorreo.action?' + jQuery('#cotizacionExpressForm').serialize(); 
	mostrarVentanaModal("correo", 'Capturar Datos Correo', 100, 300, 500, 220, url);
}

function ajustarCaracteristicas() {
	//llamar a la ventana
	var variableModificadoraDescripcion = "dummy"; //aqui hacer el llamado a la ventana.
	
	//del valor regresado de la ventana asignar el nuevo valor al hidden de la forma.
	jQuery("#variableModificadoraDescripcion").val(variableModificadoraDescripcion);
}
//function capturarDatosCorreo(){
//	if (parent.dhxWins==null){
//		parent.dhxWins = new dhtmlXWindows();
//		parent.dhxWins.enableAutoViewport(true);
//		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
//	}alert(urlCorreo);
//	parent.ventanaRechazo = parent.dhxWins.createWindow("correo", 200, 320, 550, 200);
//	parent.ventanaRechazo.setText("Enviar Correo");
//	parent.ventanaRechazo.center();
//	parent.ventanaRechazo.setModal(true);
//	parent.ventanaRechazo.attachEvent("onClose", onCloseFunction);
//	parent.ventanaRechazo.attachURL(urlCorreo);
//	
//	parent.ventanaRechazo.button("minmax1").hide();
//}	

function capturarDatosCorreo(){
	var win = dhxWins.createWindow("correo", 200, 320, 550, 200);
	win.setText("Enviar Correo");
	win.center();
	win.setModal(true);
	win.attachURL(urlCorreo);
}

function ajustarCaracteristicas() {
	//llamar a la ventana
	var variableModificadoraDescripcion = "dummy"; //aqui hacer el llamado a la ventana.
	
	//del valor regresado de la ventana asignar el nuevo valor al hidden de la forma.
	jQuery("#variableModificadoraDescripcion").val(variableModificadoraDescripcion);
}