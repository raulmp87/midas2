var cotizacionGrid;
var ventanaInciso;
var ventanaAgente;
var ventanaAsegurado;
var idAgente=null;
var ventanaEsquemaPago;
var coberturaCotizacionGrid;
var PARAMETRO_ID_CLIENTE = "idCliente";
var PARAMETRO_ID_toCotizacion = "idToCotizacion";
var  texAdicionalGridProcessor;
var ventanacambiosglobales;
var ventanaPaquete;
var resumenTotalesCotizacionGrid;
var resumenTotalesIncisoGrid;
var ventanaAsignacion;
var paginadoCotParamPath;
var incisoWindow;
/**
 * Modificar estas variables para ajustar el iva que se despliega en el combo
 * porcentajeIva
 */
var ivaFrontera = 11;
var IvaInterior = 16;

$(document).ready(function(){
	$("input:radio").attr("checked", false);
	$('#pepDiV').hide();
	$("#cuentaPropiaDiv").hide();
 });

function limpiarFormCotizacion(){
	jQuery('#cotizacionForm').each (function(){
		  this.reset();
	});
	cleanInputDiv('descripcionBusquedaPromotoria');
	cleanInput('idPromotoria');
	cleanInputDiv('agenteNombre');
	cleanInput('idAgenteCot');
}

function validaComisionCedida(valor,max,min,valorActual){
	if(valor > max){
		alert("El valor m\u00e1ximo permitido es: " + max);
		jQuery('#comisionCedida').val(valorActual);
	}else if(valor < min){
		alert("El valor m\u00ednimo permitido es: " + min);
		jQuery('#comisionCedida').val(valorActual);
	}
}

// Deshabilita el iva
function validaIva(){
	ocultarIndicadorCarga("cargaResumenTotales");
	if( idContratante > 0){
				jQuery('select#porcentajeIva.txtfield').attr('disabled','disabled');
		}
}

function listarCotizacionPaginado(page, nuevoFiltro){
	var posPath = 'posActual='+page+'&funcionPaginar='+'listarCotizacionPaginado'+'&divGridPaginar='+'cotizacionGrid';
	if(nuevoFiltro){
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, "/MidasWeb/suscripcion/cotizacion/auto/busquedaRapidaPaginada.action" + "?"+ posPath, 'gridCotizacionPaginado', 'listarCotizacion();');
}

function listarCotizacion() {
	document.getElementById("cotizacionGrid").innerHTML = '';
	cotizacionGrid = new dhtmlXGridObject("cotizacionGrid");
	cotizacionGrid.enableEditEvents(false, false, false);
	mostrarIndicadorCarga('indicador');	
	cotizacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cotizacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cotizacionGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
    
	cotizacionGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	cotizacionGrid.load("/MidasWeb/suscripcion/cotizacion/auto/listarFiltrado.action?claveNegocio="
			+ dwr.util.getValue("claveNegocio")+ "&" + posPath);
}

function limpiar(){
	jQuery('#cotizacionForm').each (function(){
		  this.reset();
	});
}

function cotizacionesGridPaginado(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'cotizacionesGridPaginado'+'&divGridPaginar='+'cotizacionGrid';
	if(nuevoFiltro){
		paginadoCotParamPath = jQuery(document.cotizacionForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, "/MidasWeb/suscripcion/cotizacion/auto/busquedaPaginada.action" + "?"+ paginadoCotParamPath + posPath, 'gridCotizacionPaginado', 'obtenerCotizaciones(0);');

}

function obtenerCotizaciones(esBusqueda){
	document.getElementById("cotizacionGrid").innerHTML = '';
	cotizacionGrid = new dhtmlXGridObject('cotizacionGrid');
	cotizacionGrid.enableEditEvents(false, false, false);
	cotizacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cotizacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	var posPath = '&posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	cotizacionGrid.load("/MidasWeb/suscripcion/cotizacion/auto/buscarCotizacion.action" + "?"+ paginadoCotParamPath + '&' +posPath);
	
	if(esBusqueda == 1)// 1 = metodo invocado desde boton busqueda
	{		
		displayFilters();		
	}
	else // 0 = metodo invocado desde paginacion
	{
		selectGridCotizacionSize();		
	}		
}

/* Acciones Determinadas */
function definirCotizacion(){
	alert("definir cotizacion");
}

function copiarCotizacionAuto(idToCotizacion){
	if(confirm('\u00BFEsta seguro que desea copiar la Cotizaci\u00F3n (' + idToCotizacion + ')?')){
		parent.sendRequestJQAsync(null, '/MidasWeb/suscripcion/cotizacion/auto/copiarCotizacion.action?idToCotizacion='+idToCotizacion,'contenido', null);
	}
}

function imprimirCotizacion(idToCotizacion){
	var location ="/MidasWeb/impresiones/poliza/imprimirCotizacion.action?idToCotizacion=" + idToCotizacion;
	window.open(location, "Cotizacion_COT" + idToCotizacion);
}


function iniciarCotizacionFormal(idTcAgente){
	var url;
	if(idTcAgente != null && idTcAgente != undefined && idTcAgente > 0){
		url = "/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente="+idTcAgente;	
	}else{
		url = "/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente=";
	}
	parent.mostrarVentanaModal("crearCotizacion", "Crear Cotizaci\u00F3n", 200, 320, 610, 460, url);		
}

function cargarNuevoCotizador(idTocotizacion, claveEstatus){
	var forma = 1;
	if(claveEstatus==12){
		forma = 4;
	}
	jQuery('#nuevoCotizadorAgentes').attr('href','/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?idToCotizacion='+idTocotizacion+'&numeroInciso=1&forma='+forma);
	document.getElementById('nuevoCotizadorAgentes').click();
}

function cerrarVentanaCrearCotizacion(){
	parent.cerrarVentanaModal("crearCotizacion");
}

function mostrarVentanaCrearCotizacion(id, onCloseFunction) {
	if (parent.dhxWins == null) {
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaCrearCotizacion = parent.dhxWins.createWindow("crearCotizacion", 200, 320, 610, 460);
	parent.ventanaCrearCotizacion.setText("Crear Cotizacion");
	parent.ventanaCrearCotizacion.center();
	parent.ventanaCrearCotizacion.setModal(true);
	parent.ventanaCrearCotizacion.attachEvent("onClose", onCloseFunction);
	if(idAgente > 0){
		parent.ventanaCrearCotizacion.attachURL("/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente="+idAgente);	
	}else{
		parent.ventanaCrearCotizacion.attachURL("/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente=");
	}
	parent.ventanaCrearCotizacion.button("minmax1").hide();
}

function iniciarCotizacionExpress(){
	sendRequestJQAsync(null, "/MidasWeb/suscripcion/cotizacion-express/auto/mostrar.action", "contenido", null);
}

function clickColumnaExpress(col, totalFil){
	var index = 0;
	jQuery("#t_riesgo tr").each(function(){		
		if(index > 0){
			var numTd = col - 1;
			if(jQuery("#encabezado_"+col).is(":checked")){
				if(jQuery("#fila_"+index).is(":checked")){
					jQuery(this).find("td").eq(numTd).find("div").show();
				}
			}else{
				jQuery(this).find("td").eq(numTd).find("div").hide();
			}
		}
		index= index+1;
	});
}

function clickFilaExpress(fil, totalCol){
	var index = 1;
	jQuery("#t_riesgo tr").eq(fil).find("td").each(function(){
		if(index > 0){
			if(jQuery("#fila_"+fil).is(":checked")){
				if(jQuery("#encabezado_"+index).is(":checked")){
					jQuery(this).find("div").show();
				}				
			}else{
				jQuery(this).find("div").hide();
			}			
		}
		index= index+1;
	});
}

function crearCotizacion(){
	var target = 'contenido';
	var idToNegocio = dwr.util.getValue('cotizacion.solicitudDTO.negocio.idToNegocio');
	var idToNegProducto = dwr.util.getValue('cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto');
	var idToNegTipoPoliza = dwr.util.getValue('cotizacion.negocioTipoPoliza.idToNegTipoPoliza');
	if (idToNegocio == "" || idToNegProducto == "" || idToNegTipoPoliza == ""){
		target = 'detalle';
		alert('Se necesitan datos para poder crear la cotizaci\u00F3n');
	}else{
		parent.sendRequestJQAsync(null, '/MidasWeb/suscripcion/cotizacion/auto/crearCotizacion.action?'+jQuery(document.cotizacionAutoForm).serialize(),target, null);
		parent.cerrarVentanaModal("crearCotizacion");
	}
}


function guardarCotizacion(){
	      var formaDePago = dwr.util.getValue('formaPago');
	      var derechos    = dwr.util.getValue('derechos');
	     if(formaDePago =="" || derechos ==""){
            mostrarMensajeInformativo("Favor de Capturar los campos Forma de pago y/o Derechos antes de Guardar La cotizaci\u00F3n", "20");
            return false;
	      }
	     
		 if (!fechaMenorQue(jQuery("#fecha").val(), jQuery("#fechados").val())) {
			 mostrarMensajeInformativo("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia", "20");
			 return false;
		  }
		 if(jQuery("#descuentoGlobal").val() == ""){
			 mostrarMensajeConfirm("El descuento global es requerido <br/> Antes de guardar el sistema lo ajustara a 0.0 \u00BFdesea continuar\u003F", "20","jQuery('#descuentoGlobal').val('0.0');guardarCotizacion();", null, null);
			 return false;
		 }
  parent.sendRequestJQ(null,
 '/MidasWeb/suscripcion/cotizacion/auto/guardarCotizacion.action?'+jQuery(document.cotizacionForm).serialize()
			,'contenido', null);
	
}



function verDetalleCotizacion(idToCotizacion, claveEstatus){
	var cotizacionAsignada = 11;
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	if(claveEstatus==cotizacionAsignada){
		var url =  '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacion.action?id='+idToCotizacion;
		parent.mostrarVentanaModal("detalleCotizacion", "Cotizaci\u00F3n", 200, 320, 610, 450, url);		
	}else{
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacion.action?id='+idToCotizacion+soloConsulta,'contenido_detalle', null);
	}
	
}


function verDetalleCotizacionContenedor(idToCotizacion, claveEstatus){
	var cotizacionAsignada = 11;
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	if(claveEstatus==cotizacionAsignada){
		var url =  '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion;
		parent.mostrarVentanaModal("detalleCotizacion", "Cotizaci\u00F3n", 200, 320, 610, 450, url);		
	}else{
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion+soloConsulta,'contenido', null);
	}
	
}

function verDetalleCotizacionContenedorSoloLectura(idToCotizacion){
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion+"&soloConsulta=1",'contenido', null);
}


function desplegarMensaje(){
 jQuery(".error").hide();
 var hasError = false;
   if(dwr.util.getValue("mensajeFechaOperacion")=='-1') {
      jQuery("#fechados").after('<span class="error">Requiere Autorizaci�n</span> \n' );
      hasError = true;
   }	
   if(hasError == true) { return false; }
}


function enConstruccion(){
	alert("");
}

function ventanaEmails(){
	mostrarVentanaEmails(null, closeEmails);	
}

function mostrarVentanaEmails(id, onCloseFunction) {
	if (parent.dhxWins == null) {
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaEmails = parent.dhxWins.createWindow("emails", 200,320, 580, 280);
	parent.ventanaEmails.setText("Enviar por E-Mail");
	parent.ventanaEmails.center();
	parent.ventanaEmails.setModal(true);
	parent.ventanaEmailsn.attachEvent("onClose", onCloseFunction);
	parent.ventanaEmails.attachURL("/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action" );
	parent.ventanaEmails.button("minmax1").hide();
}

function ventanaCorreo(idCotizacion){
	var url = '/MidasWeb/suscripcion/cotizacion/auto/ventanaEmails.action?idToCotizacion='+idCotizacion;
	// mostrarModal("correo", 'Capturar Datos Correo', 100, 300, 500, 250, url);
	parent.mostrarVentanaModal("correo", 'Capturar Datos Correo', 100, 300, 500, 250, url);
}

function closeEmails(){
	parent.dhxWins.window('emails').setModal(false);
	parent.dhxWins.window('emails').hide();
	parent.ventanaCrearCotizacion=null;
}

function agregarInciso(tipoAccion) {
	mostrarVentanaInciso(null, 'parent.closeInciso();');		
}

function mostrarInciso(idCotizacion, numInciso, soloConsulta,numeroSecuencia){
	mostrarVentanaInciso(numInciso, 'parent.closeInciso();', soloConsulta,numeroSecuencia);	
}

function mostrarVentanaInciso(id, onCloseFunction, soloConsulta,numeroSecuencia) {
	var url;
	var porcentajeIva = "&incisoCotizacion.cotizacionDTO.porcentajeIva="+dwr.util.getValue("porcentajeIva");
	var soloConsultaPath = "";
	if(soloConsulta != "" && soloConsulta != undefined){
		soloConsultaPath = "&soloConsulta="+soloConsulta;
	}
	if(id == null){
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+porcentajeIva+soloConsultaPath;
	}else{
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+"&incisoCotizacion.id.numeroInciso="+id+porcentajeIva+soloConsultaPath;
	}
	if(id != null) {
		mostrarVentanaModal("inciso", "Inciso para la cotizaci\u00f3n: " + dwr.util.getValue("idToCotizacion") + " n\u00famero de inciso: " + numeroSecuencia , 50, 50, 800, 600, url, null);
	}else{
		mostrarVentanaModal("inciso", "Inciso para la cotizaci\u00f3n: " + dwr.util.getValue("idToCotizacion") , 50, 50, 800, 600, url, null);
	}
		
}

/*
 * function onChangeLineaNegocio(){
 * listadoService.getMapMarcaVehiculoPorNegocioSeccion(dwr.util.getValue("idToNegSeccion"),
 * function(data){ var combo = document.getElementById('idTcMarcaVehiculo');
 * addOptions(combo,data); onChangeMarcaVehiculo(); });
 * 
 * 
 * listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(dwr.util.getValue("idToNegSeccion"),
 * function(data){ var combo = document.getElementById('idToNegPaqueteSeccion');
 * addOptions(combo,data); }); }
 * 
 * function onChangeMarcaVehiculo(){
 * listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(dwr.util.getValue("idTcMarcaVehiculo"),dwr.util.getValue("idToNegSeccion"),dwr.util.getValue("idMoneda"),
 * function(data){ var combo = document.getElementById('estiloId');
 * addOptions(combo,data); onChangeEstiloVehiculo(); }); }
 * 
 * function onChangeEstiloVehiculo(){
 * listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(dwr.util.getValue("idMoneda"),
 * dwr.util.getValue("estiloId"),dwr.util.getValue("idToNegSeccion"),
 * function(data){ var combo = document.getElementById('modeloVehiculoId');
 * addOptions(combo,data); onChangeModeloVehiculo(); }); }
 * 
 * function onChangeModeloVehiculo(){
 * listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(dwr.util.getValue("estiloId"),
 * function(data){ var combo = document.getElementById('idTcTipoUsoVehiculo');
 * addOptions(combo,data); }); } function onChangeSeleccion(seleccion){
 * if(seleccion == 1){ jQuery("#trNormal1").hide(); jQuery("#trNormal2").hide();
 * jQuery("#trNormal3").hide(); jQuery("#trExpress1").show(); }else{
 * jQuery("#trNormal1").show(); jQuery("#trNormal2").show();
 * jQuery("#trNormal3").show(); jQuery("#trExpress1").hide(); } }
 */


function verDetalleNegocio(tipoAccion) {
	if (negocioGrid.getSelectedId() != null) {
		var idNegocio = getIdFromGrid(negocioGrid, 0);
		sendRequestJQ(null, '/MidasWeb/negocio/verDetalle.action'
				+ "?tipoAccion=" + tipoAccion + "&id=" + idNegocio+ "&claveNegocio="+ dwr.util.getValue("claveNegocio"),
				targetWorkArea, null);
	} else {
		sendRequestJQ(null, '/MidasWeb/negocio/verDetalle.action'
				+ "?tipoAccion=" + tipoAccion + "&claveNegocio="+ dwr.util.getValue("claveNegocio"), targetWorkArea,
				null);
	}
}

function validaDescuento(){
	var descuento = dwr.util.getValue("descuentoGlobal");
	if ( descuento > 100){
		dwr.util.setValue("descuentoGlobal",'');
		mostrarMensajeInformativo ("El descuento global no es v\u00e1lido.","10");
	}
}


function seleccionarAgente(){	
	parent.mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,400, 510, 230, seleccionarAgentePath);	
}
function seleccionarAgenteCotizacion(bandera){	
	parent.mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,400, 510, 230, seleccionarAgentePath+"?bandera="+bandera);	
}
function seleccionarAgenteCotizacionBuscar(bandera){	
	parent.mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,400, 510, 230, seleccionarAgenteBuscarPath+"?bandera="+bandera);	
}

/**
 * Llena los datos del agente en la cotizacion formal
 * 
 * @param id
 */
function cargaAgente(agenteId,id){
	cerrarVentanaModal("agente");
	cerrarVentanaModal("crearCotizacion");	
	iniciarCotizacionFormal(id);	
}
/**
 * Llena los datos del agente en la cot express
 * 
 * @param id
 * @param nombre
 */
function cargarAgenteCotizacionExpress(id,nombre){
	var campos = nombre.split(" - ");
	jQuery('#idAgenteCot').val(id);
	jQuery('td[id=idAgenteCot]').text(campos[1]);
	jQuery('td[id=agenteNombre]').text(nombre);
	cerrarVentanaModal("agente");
}
/**
 * Llena el nombre en la bandeja de cot
 * 
 * @param id
 * @param nombre
 */
function cargaAgenteCotizacion(id,nombre){
	jQuery('#idAgenteCot').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#agenteNombre').text(nombre);
	cerrarVentanaModal("agente");
}

function seleccionarPromotoria(){
	parent.mostrarVentanaModal("promotoria","Selecci\u00F3n Promotoria",200,320, 510, 150, seleccionarPromotoriaPath);	
}

function cargarPromotoria(id,nombre){
	jQuery('#idPromotoria').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#descripcionBusquedaPromotoria').text(nombre);
	cerrarVentanaModal("promotoria");
}
/**
 * Fix IE description: ie don't support the trim function this FIX add the
 * function
 */
if(typeof String.prototype.trim !== 'function') {
	  String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g, ''); 
	  }
}

function verEsquemaPago(primaNetaCoberturas){
	mostrarVentanaEsquemaPago(null,primaNetaCoberturas, closeEsquemaPago);
}

function verIgualarPrimas(idToCotizacion){
	var url = verIgualarPrimasPath;
	url = url + '?idToCotizacion='+idToCotizacion;
	mostrarVentanaModal("igualarPrimas", "Igualaci&oacute;n de primas", 200,320, 600, 180, url);
}

function mostrarVentanaEsquemaPago(id, onCloseFunction) {
	var primaTotal = '';
	var totalPrima = '';
	var derechos = '';	
	
try{
	var iva     	=  dwr.util.getValue("porcentajeIva");
	var derechosId  =  dwr.util.getValue("derechos");

	if(derechosId != null && derechosId != ''){
		derechos =  dwr.util.getText("derechos");
	}
	var primaTotalText = jQuery('table[id=t_riesgo] td div[id=primaTotal]').text();
	if(primaTotalText != null && primaTotalText != ''){
		primaTotal = primaTotalText.trim().replace('$','').replace(',','');
	}
	var totalPrimaText = jQuery('table[id=t_riesgo] td div[id=totalPrima]').text();
	if(totalPrimaText != null && totalPrimaText != ''){
		totalPrima = totalPrimaText.trim().replace('$','').replace(',','');
	}
	if(totalPrima == '' || derechos == '' || iva == '' || totalPrima == ''){
		alert('Es necesario tener los derechos definidos, agregar al menos un inciso y tener una prima total');
		return;
	}
	var url = verEsquemaPagoPath+"?cotizacion.fechaInicioVigencia="+dwr.util.getValue("fecha")
    +"&cotizacion.fechaFinVigencia="+dwr.util.getValue("fechados")
    +"&cotizacion.idFormaPago="+dwr.util.getValue("formaPago") 
    +"&totalPrima="+totalPrima
    +"&derechos="+derechos
    +"&iva="+iva
    +"&primaTotal="+primaTotal
    +"&cotizacion.idToCotizacion="+dwr.util.getValue("idToCotizacion");
	mostrarVentanaModal("esquemaPago", "Esquemas de Pago", 200,320, 600, 200, url);
}catch(e){
	alert(e);
}
}

function abrirEsquemaPagos(id) {

	var url = "/MidasWeb/suscripcion/cotizacion/auto/verEsquemaPago.action?cotizacion.idToCotizacion="+id;
	
	parent.mostrarVentanaModal("esquemaPago", "Esquemas de Pago", 200,320, 600, 200, url);
}

function closeEsquemaPago(){
	
	parent.dhxWins.window('esquemaPago').setModal(false);
	parent.dhxWins.window('esquemaPago').hide();
}

function obtenerCoberturaCotizaciones(){	
	
	if (dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId') == -2) {
		return false;
	}
	document.getElementById("coberturaCotizacionGrid").innerHTML = '';
	jQuery('#cargando').css('display','block');
	jQuery('#btnGuardar').css('display', 'none');
	var numeroInciso = dwr.util.getValue('incisoCotizacion.id.numeroInciso') ;
	var idToCotizacion = dwr.util.getValue('incisoCotizacion.id.idToCotizacion'); 
	var idNegocioPaqueteId = dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId'); 
	var idEstado = dwr.util.getValue('incisoCotizacion.incisoAutoCot.estadoId');
	var idMunicipio = dwr.util.getValue('incisoCotizacion.incisoAutoCot.municipioId');
	
	if( numeroInciso != '' && idToCotizacion != ''){
		sendRequestJQTarifa(document.incisoForm,
				"/MidasWeb/vehiculo/inciso/recuperarCorberturaCotizacion.action",
				"coberturaCotizacionGrid", jQuery('#btnGuardar').css('display','block'));
		// muestra el boton actualizar
	}else{
		if(idNegocioPaqueteId != ''&& idEstado != '' && idMunicipio != ''){
			jQuery('#error').hide();
			sendRequestJQTarifa(document.incisoForm,
					"/MidasWeb/vehiculo/inciso/mostrarCorberturaCotizacion.action",
					"coberturaCotizacionGrid", null);
		}else if(idEstado == '' && idMunicipio == ''){
			parent.mostrarMensajeInformativo('Favor de seleccionar un Estado y/o Municipio',"20");
		}
	}
	// muestra el boton actualizar
	jQuery('#btnRecalcular').css('display', 'block');
	jQuery('#cargando').css('display','none');
}

function cargaCotizacionCoberturas(){
	// alert("");
}

function obtieneProductos(id){
	listadoService.getMapProductos(id,1,function(data){
		var combo = document.getElementById('producto');
		addOptions(combo,data);
	});	
}

function listarNegocios(id){
	var activos = 1;
	if(id != null && id != ''){
		listadoService.listarNegociosPorAgente(id, jQuery('#claveNegocio').val(), activos, function(data){
			addOptions(jQuery('#negocios')[0], data);
		});
	}else{
		jQuery('#negocios')[0].options.length = 0;
	}
}


function mostrarAgregar(){
	var ventanaCliente;
	var tipoVentana = 1;

	if (parent.dhxWins == null) {
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	
	parent.ventanaCliente = parent.dhxWins.createWindow("consultaClientes",  400, 400, 700, 400);
	parent.ventanaCliente.setText("PConsulta de Clientes");
	parent.ventanaCliente.center();
	parent.ventanaCliente.setModal(true);
	parent.ventanaCliente.attachEvent("onClose", onCloseFunction);
	parent.ventanaCliente.attachURL("/MidasWeb/busquedaCliente/mostrarPantallaConsulta.action?tipoBusqueda="+2);
	parent.ventanaCliente.button("minmax1").hide();	
	
}


function mostrarBotonCrearCotizacion(){	
	var idToNegocio 		= dwr.util.getValue('cotizacion.solicitudDTO.negocio.idToNegocio');
	var idToNegProducto 	= dwr.util.getValue('cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto');
	var idToNegTipoPoliza 	= dwr.util.getValue('cotizacion.negocioTipoPoliza.idToNegTipoPoliza');
	var idMoneda 			= dwr.util.getValue("cotizacion.idMoneda");
	
	if (idToNegocio != '' && idToNegProducto != '' && idToNegTipoPoliza != '' && idMoneda != '')
		jQuery('#divCrearBtn').show();
	else
		jQuery('#divCrearBtn').hide();

}

function cleanCmbo(target1,target2,target3){
	if(target1 != '')
		removeAll(target1);
	if(target2 != '')
		removeAll(target2);
	if(target3 != '')
		removeAll(target3);
}

function removeAll(target){
	dwr.util.removeAllOptions(target);
	addSelectHeader(target);
}

function cleanCmbo(target1,target2,target3){
	if(target1 != '')
		removeAll(target1);
	if(target2 != '')
		removeAll(target2);
	if(target3 != '')
		removeAll(target3);
}

function removeAll(target){
	dwr.util.removeAllOptions(target);
	addSelectHeader(target);
}

function iniciGrids(){
	obtenertexAdicionalGrid();
}
var x = 1;
	function obtenertexAdicionalGrid(){
	texAdicionalGrid = new dhtmlXGridObject('contenido_textosAdicionalesGrid');
	texAdicionalGrid.attachEvent("onXLE", function(grid,count){	 		 	
		validaTextosPendientes(grid);	 	
	 });
	texAdicionalGrid.load("/MidasWeb/suscripcion/cotizacion/auto/textoadicional/obtenertexAdicionalGrid.action?cotizacion.idToCotizacion=" + dwr.util.getValue("idToCotizacion"));
	initDataProcesor(texAdicionalGrid);
}
	
	function validaTextosPendientes(texAdicionalGrid){
		var valido = true;
		texAdicionalGrid.forEachRow(function(id){
			 var cellObj = texAdicionalGrid.cellById(id, 2).getValue();
			 if(cellObj == ''){
				jQuery("#b_agregar").hide();
				valido = false;
			 }
		});
		if(valido){
			jQuery("#b_agregar").show();
		}
	}
	
	function initDataProcesor(texAdicionalGrid){
		texAdicionalGridProcessor = new dataProcessor("/MidasWeb/suscripcion/cotizacion/auto/textoadicional/accionTextoAdicional.action?cotizacion.idToCotizacion=" + dwr.util.getValue("idToCotizacion"));
		texAdicionalGridProcessor.enableDataNames(true);
		// texAdicionalGridProcessor.sendAllData(false);
		texAdicionalGridProcessor.setTransactionMode("POST");
		texAdicionalGridProcessor.setUpdateMode("cell");
		texAdicionalGridProcessor.attachEvent("onAfterUpdate", function() {
			iniciGrids();
		});				
		texAdicionalGridProcessor.setVerificator(2, function(value,row) {
			var valido = true;
			if (value === "") {
				valido =  false;
			}
			return valido;
		})
		texAdicionalGridProcessor.init(texAdicionalGrid);
	}
	function agregarFilaGrid(){
		jQuery("#b_agregar").hide();
		if (texAdicionalGrid.getRowsNum() ==0 ) {
			texAdicionalGrid.addRow(1,[]);
		} else {
			var secuencia = parseInt(texAdicionalGrid.getRowId(texAdicionalGrid.getRowsNum() - 1)) + 1;
			texAdicionalGrid.addRow(secuencia, []);
		}
	}
	
	function eliminarRow() {
		if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
			texAdicionalGrid.deleteSelectedItem();
		}
	}	

	function mostrarVentanaCambiosGlobales(id, onCloseFunction) {
		var url = "/MidasWeb/suscripcion/cotizacion/auto/cambiosglobales/verDetallelineaNegocio.action?id="+ id;
		mostrarVentanaModal("CambiosGlobales", "Cambios Globales",50,50, 800, 600, url,null);	
	}

	function verDetalleCotizacionFormal(idToCotizacion){
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion,'contenido', null);
	}
	
	function onChangeLineaNegocioporPaquete(){
		jQuery('#mensaje').html("");
		jQuery('#tipoMensaje').html("");
	      parent.removeAllOptionsAndSetHeader(
                  document.getElementById("idToNegPaqueteSeccion"), parent.headerValue,
                  "CARGANDO...");
		listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(dwr.util.getValue("idToNegSeccion"),
				function(data){
					var combo = document.getElementById('idToNegPaqueteSeccion');
					addOptions(combo,data);
				});	
		
		
	}



	
	function obtenerCoberturaCotizacionesCambiosGlobales(){
		document.getElementById("coberturaCotizacionGrid").innerHTML = '';
	    mostrarIndicadorCarga('indicador');	
		sendRequestJQTarifa(null,"/MidasWeb/suscripcion/cotizacion/auto/cambiosglobales/mostrarCoberturaCotizacion.action?"+jQuery(document.cambiosGlobalesForm).serialize(), "coberturaCotizacionGrid", null);
		ocultarIndicadorCarga('indicador');
	}

	function hidBoton (){
		var inciso = dwr.util.getValue("inciso");
		 if(inciso>0)
			jQuery('#divinciso').show();
		else
			jQuery('#divinciso').hide();	
	}

	function cargaCotizacionCoberturasCambiosGlobales(){
		// alert("");
	}
	
	
	
	function guardarCoberturas(){
		var idToCobertura = dwr.util.getValue('coberturaSeccionDTO.coberturaDTO.idToCobertura');
		var claveContrato = dwr.util.getValue('claveContrato');
		var valorSumaAsegurada = dwr.util.getValue('valorSumaAsegurada');
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cambiosglobales/saveCoberturtas.action?'+jQuery(document.cambiosGlobalesForm).serialize(),'contenido', null);
		
	}
 
	function closeAplicarPaquete(){
		if(parent.mainDhxWindow.window("Aplicar") != null){
			parent.mainDhxWindow.window("Aplicar").setModal(false);
			parent.mainDhxWindow.window("Aplicar").hide();
		}else{
			mainDhxWindow.window("Aplicar").setModal(false);
			mainDhxWindow.window("Aplicar").hide();
		}
	}

function validaComentarios(){
	if(document.cambiosGlobalesForm.comentarios.value=="" || document.cambiosGlobalesForm.comentarios.value.length==0){
		alert("Debe de capturar el campo Comentarios.");return;
	}else{
		jQuery("#guardarCambiosGlobalesBtn").click();
	}
}

function sendData(check){
	alert(check);
}
	
	function obtenerResumenTotalesCotizacionGrid(){
		document.getElementById("resumenTotalesCotizacionGrid").innerHTML = '';
		mostrarIndicadorCarga("cargaResumenTotales"); 
		sendRequestJQ(null,"/MidasWeb/suscripcion/cotizacion/auto/mostrarResumenTotalesCotizacion.action?esComplementar=false&"+jQuery(document.cotizacionForm).serialize(), "resumenTotalesCotizacionGrid",'validaIva();');
	}
	
	
	function calcularDetalleInciso(){
		var path = "/MidasWeb/vehiculo/inciso/mostrarResumenTotalesInciso.action";
		sendRequestModalInciso(document.incisoForm,path,closeResumenInciso,"html");
	}
	
	
	function mostrarVentanaReasignarCotizacion(idToCotizacion,onCloseFunction){
		// var url =
		// "/MidasWeb/suscripcion/cotizacion/reasignar/mostrarVentana.action?id="+idToCotizacion
		// parent.mostrarVentanaModal("asignar", "Reasignar Cotizaci\u00F3n",
		// 200, 320, 550, 200, url);
		if (parent.dhxWins==null){
			parent.dhxWins = new dhtmlXWindows();
			parent.dhxWins.enableAutoViewport(true);
			parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		}
		parent.ventanaAsignacion = parent.dhxWins.createWindow("asignar", 200, 320, 550, 200);
		parent.ventanaAsignacion.setText("Reasignar Cotizaci\u00F3n");
		parent.ventanaAsignacion.center();
		parent.ventanaAsignacion.setModal(true);
		parent.ventanaAsignacion.attachEvent("onClose", onCloseFunction);
		parent.ventanaAsignacion.attachURL("/MidasWeb/suscripcion/cotizacion/reasignar/mostrarVentana.action?id="+idToCotizacion);
		
		parent.ventanaAsignacion.button("minmax1").hide();	
	}
	
	function verComplementarCotizacion(idToCotizacion){
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+idToCotizacion,'contenido', null);
	
		
	}
	function changePEPS(){
		
		var pep = dwr.util.getValue("entrevista.pep");
		
		if(pep=='true'){
			
			jQuery("#pepDiV").show();
		}else {
			jQuery("#pepDiV").hide();
		}
	}
	function changeCuentaPropia(){
		
		var cuentaPropia = dwr.util.getValue("entrevista.cuentaPropia");
		
		if(cuentaPropia=='false'){
			
			jQuery("#cuentaPropiaDiv").show();
		}else {
			jQuery("#cuentaPropiaDiv").hide();
		}
	}
	
	function verComplementarCotizacionDeEntrevista(){
		var moneda = dwr.util.getValue("entrevista.moneda");
		var primaEstimada = dwr.util.getValue("entrevista.primaEstimada");
		var numeroSerieFiel = dwr.util.getValue("entrevista.numeroSerieFiel");
		var pep = dwr.util.getValue("entrevista.pep");
		var parentesco = dwr.util.getValue("entrevista.parentesco");
		var nombrePuesto = dwr.util.getValue("entrevista.nombrePuesto");
		var cuentaPropia = dwr.util.getValue("entrevista.cuentaPropia");
		var documentoRepresentacion = dwr.util.getValue("entrevista.documentoRepresentacion");
		var folioMercantil = dwr.util.getValue("entrevista.folioMercantil");
		var nombreRepresentado=dwr.util.getValue("entrevista.nombreRepresentado");
		var pdfDownload ='pdfDownload';
		var	periodoFunciones=dwr.util.getValue("entrevista.periodoFunciones");
		var idToPersonaContratante = dwr.util.getValue("cotizacion.idToPersonaContratante");
		var idToCotizacion = dwr.util.getValue("cotizacion.idToCotizacion");
		var parms=null;
		parms = jQuery('#entrevistaForm').serialize();
		if(numeroSerieFiel===''){
			alert('Es necesaria la FIEL');
			return;
		}

		if(document.getElementById("personaFisica")){
			
			if(jQuery('input[name="entrevista\\.pep"]:checked').val() == undefined){
				alert('La pregunta "PEP" es obligatoria.');
				return;
			}
			
			if(pep=='true'){
			if(parentesco==='' ||nombrePuesto===''||periodoFunciones===''){
				alert("Los campos:Parentesco,Puesto y Periodo de funciones son obligatorios ");
				return;
				}
				
			}
			
			if(jQuery('input[name="entrevista\\.cuentaPropia"]:checked').val() == undefined){
				alert('La pregunta "Actúa por cuenta propia" es obligatoria.');
				return;
			}
			
			if(cuentaPropia=='false'){
				
					if(nombreRepresentado==''||jQuery('input[name="entrevista\\.documentoRepresentacion"]:checked').val() == undefined)
					{
						alert("Los campos: Documento Juridico y nombre por quién actua son obligatorios ");
						return;
					}
					
				
			}
		}
		
		if(document.getElementById("personaMoral")){
			if(folioMercantil=='')
			{
				alert("El Campo Folio Mercantil es Obligatorio.");
				return;
			}
		}


		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/emision/guardarEntrevista.action?cotizacion.idToPersonaContratante='+idToPersonaContratante
				+'&cotizacion.idToCotizacion='+idToCotizacion
				+'&'+parms
				,'contenido', null);
		
	}
	
	
	function generarReporteEntrevistaPDF(){
		
		var idToCotizacion = dwr.util.getValue("idToCotizacion");
		var urlPDF='/MidasWeb/suscripcion/emision/getFormatoEntrevista.action?idToCotizacion='+idToCotizacion;
		
		window.open(urlPDF, 'download');
		
	}
	function loadFortimax(){
		var url = dwr.util.getValue("linkFortimax");
		window.open(url,'Fortimax');
		
	}
	
	function checkBoxSeleccioneDocumento(indiceDocumento){
		var c=dwr.util.getValue("checkBox["+indiceDocumento+"].seleccionado");
		
		if(c==true) {
			document.getElementById("documentacion["+indiceDocumento+"].descripcion").disabled=false;
			document.getElementById("documentacion["+indiceDocumento+"].fechaVigencia").disabled=false;
			
	
		 }else {
			 document.getElementById("documentacion["+indiceDocumento+"].descripcion").disabled=true;
			 document.getElementById("documentacion["+indiceDocumento+"].fechaVigencia").disabled=true;
				
		}
	}
	
	function verComplementarCotizacionDeDocumentacion(){
		var idToPersonaContratante = dwr.util.getValue("cotizacion.idToPersonaContratante");
		var idToCotizacion = dwr.util.getValue("cotizacion.idToCotizacion");
		var fechaVigencia5 = dwr.util.getValue("documentacion[5].fechaVigencia");
		var temp= fechaVigencia5.split('/');
		var dt = new Date(temp[1]+'/'+temp[0]+'/'+temp[2]);
		var myDate = new Date();	
		var startMsec = myDate.getTime();
		var mils =86400000 *91;
		startMsec=startMsec-mils;
		var milsDate = dt.getTime();
		if(milsDate<startMsec){
			alert("La la fecha de vigencia no debe ser menor  a tres");
			return;
		}
		var parms=null;
		parms = jQuery('#DocumentacionForm').serialize();
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/emision/guardarDocumentacion.action?cotizacion.idToPersonaContratante='+idToPersonaContratante
									+'&'+parms
									+'&cotizacion.idToCotizacion='+idToCotizacion
									,'contenido', null);	
	}
	
	function getFormatoEntrevista() {
		window.open('/MidasWeb/suscripcion/emision/getFormatoEntrevista.action', 'download');
	}
	function verComplementarCotizacionConsulta(idToCotizacion){
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+idToCotizacion+"&soloConsulta=1",'contenido', null);
	}
	
	function terminarCotizacionAgente(idToCotizacion){
		var error = '';
		var numInciso = jQuery("#numInciso").val();
		if(!validaCamposObligatorio()){
			// Termina la cotizacion
			sendRequestJQ(null, 
					'/MidasWeb/suscripcion/cotizacion/auto/terminarcotizacion/terminar.action?id='+idToCotizacion+'&isAgente=S&numeroInciso=' + numInciso , 
					'contenido', null);
		}else{
		mostrarMensajeInformativo(
				"No se puede terminar la cotizaci\u00f3n revisa que contenga los siguientes datos. \n"
						+ "Inicio de Vigencia,\nFin de Vigencia,\nForma de Pago,\nDerechos,\nDescuento Global,\nComisi\u00f3n Cedida.",
				"10", null, null);
		}
	}

	function validaCamposObligatorio(){
		var arrayCampos = new Array("comisionCedida","fechados","porcentajeIva","derechos","descuentoGlobal","fecha");
		var indicador = false;
		for(var x in arrayCampos){
			if(jQuery('#comisionCedida').val() == '' || jQuery('#fechados').val() == '' || jQuery('#porcentajeIva').val() == ''
				|| jQuery('#derechos').val() == '' || jQuery('#descuentoGlobal').val() == '' || jQuery('#fecha').val() == ''){
				indicador = true;
			}
		}
		return indicador;
	}
	function terminarCotizacion(idToCotizacion){
		var error = '';
		
		if(!validaCamposObligatorio()){
			// Termina la cotizacion
			sendRequestJQ(null, 
					'/MidasWeb/suscripcion/cotizacion/auto/terminarcotizacion/terminar.action?id='+idToCotizacion+'&isAgente=N', 
					'contenido', null);
		}else{
		mostrarMensajeInformativo(
				"No se puede terminar la cotizaci\u00f3n revisa que contenga los siguientes datos. \n"
						+ "Inicio de Vigencia,\nFin de Vigencia,\nForma de Pago,\nDerechos,\nDescuento Global,\nComisi\u00f3n Cedida.",
				"10", null, null);
		}
	}
	
	function sendRequestModalInciso(fobj, actionURL, pNextFunction, dataTypeParam) {
		// parent.blockPage();
		var ifr; 
		if (_isIE) {
			ifr = parent.incisoWindow.window('inciso')._frame.contentWindow.documentgetElementById("resumenTotalesIncisoGrid");
		 } else {
			ifr = parent.incisoWindow.window('inciso')._frame.contentDocument.getElementById("resumenTotalesIncisoGrid");
		 }
		jQuery.ajax({
		    type: "POST",
		    url: actionURL,
		    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
		    dataType: dataTypeParam,
		    async: true,
		    success: function(data) {
		    	ifr.innerHTML= '';
		    	ifr.innerHTML = data;
		    },
		    complete: function(jqXHR) {
		    	// parent.unblockPage();
		    	var mensaje = jQuery("#mensaje").text();
		    	var tipoMensaje = jQuery("#tipoMensaje").text();
		    	
		    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
					mostrarVentanaMensaje(tipoMensaje, mensaje, null);
				}
				
				if (tipoMensaje != MENSAJE_TIPO_ERROR) {
					if (pNextFunction !== null) {
						pNextFunction();
					}
				}
				
		    },
		    error:function (xhr, ajaxOptions, thrownError){
		    	// parent.unblockPage();
	        }   	    
		});
	}

	function closeResumenInciso() {
// if(parent.incisoWindow != null){
		if(incisoWindow != null){
			
		}else{
// parent.incisoWindow.window('inciso').setModal(false);
// parent.incisoWindow.window('inciso').hide();
			incisoWindow.window('inciso').setModal(false);
			incisoWindow.window('inciso').hide();
		}
	}
	
	
	function mostrarMsjValidacionDatosComplementarios(idToCotizacion) {
		var path= '/MidasWeb/suscripcion/cotizacion/auto/validarDatosComplementarios.action?idToCotizacion='+idToCotizacion;
		var targetWorkArea ="detalle";
		sendRequestJQ(null,path,targetWorkArea,null);	
	}
	
	function actualizarEstatusCotizacion() {
		var idToCotizacion = dwr.util.getValue("idToCotizacion");
		var mensaje = dwr.util.getValue("mensajeDTO.mensaje");
		var visible = dwr.util.getValue("mensajeDTO.visible");
		var path= '/MidasWeb/suscripcion/cotizacion/auto/actualizarEstatusCotizacion.action?idToCotizacion='+idToCotizacion+'&mensajeDTO.mensaje='+mensaje+'&mensajeDTO.visible='+visible;
		var targetWorkArea ="detalle";
		sendRequestJQ(null,path,targetWorkArea,null);
	}
	
	function mostrarVentanaCargaControles(idToCotizacion,numeroInciso){
		if (parent.dhxWins==null){
			parent.dhxWins = new dhtmlXWindows();
			parent.dhxWins.enableAutoViewport(true);
			parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		}
		
		parent.ventanaControles= parent.dhxWins.createWindow("winCargaControles", 200, 220, 800, 500);
		parent.ventanaControles.setText("Complementar Datos Riesgos");
		parent.ventanaControles.center();
		parent.ventanaControles.setModal(true);
// parent.ventanaControles.attachEvent("onClose", onCloseFunction);
		parent.ventanaControles.attachURL("/MidasWeb/vehiculo/inciso/mostrarControlDinamicoRiesgo.action?incisoCotizacion.id.idToCotizacion="+idToCotizacion+"&incisoCotizacion.id.numeroInciso="+numeroInciso);
		parent.ventanaControles.button("minmax1").hide();	
	}
	
	
	
	function closeVentanaControles(){
		if(parent.dhxWins.window('winCargaControles') != null){
			parent.dhxWins.window('winCargaControles').setModal(false);
			parent.dhxWins.window('winCargaControles').hide();
		}else{
			parent.ventanaControles.window('winCargaControles').setModal(false);
			parent.ventanaControles.window('winCargaControles').hide();	
		}
	}
	
	function cambiarEstatusEnProceso(idToCotizacion){
		sendRequestJQ(null, 
				'/MidasWeb/suscripcion/cotizacion/auto/cambiarEstatusEnProceso.action?id='+idToCotizacion , 
				'contenido', null);		
	}
	
	function cambiarEstatusEnProcesoAgente(idToCotizacion){
		sendRequestJQ(null, 
				'/MidasWeb/suscripcion/cotizacion/auto/cambiarEstatusEnProceso.action?id='+idToCotizacion+'&isAgente=S' , 
				'contenido', null);		
	}
	
	function cancelarCotizacion(idToCotizacion){
		if(comfirm("\u00BFEstas seguro que deseas Cancelar la cotizaci\u00f3n n\u00famero " + idToCotizacion)){
			sendRequestJQ(null, 
				'/MidasWeb/suscripcion/cotizacion/auto/cancelarCotizacion.action?id='+idToCotizacion , 
				'contenido', null);		
		}
	}

	
	function habilitaLineaCobertura(linea){
		if(jQuery("#claveCobertura_"+linea).is(":checked")){
			jQuery("#claveContrato_"+linea).removeAttr("disabled");
			jQuery("#valor_"+linea).removeAttr("disabled");
			jQuery("#deducible_"+linea).removeAttr("disabled");
		}else{
			jQuery("#claveContrato_"+linea).attr("disabled","disabled");
			jQuery("#valor_"+linea).attr("disabled","disabled");
			jQuery("#deducible_"+linea).attr("disabled","disabled");
		}
	}
	
	function modificaIVA(){
		var idToCotizacion = dwr.util.getValue("idToCotizacion");
		mostrarMensajeConfirm("'Se afectar\u00e1n los montos de la cotizaci\u00f3n, \u00BFDesea modificar el IVA\u003F'", "20",
				"$_cambiarIvaCotizaicon("+idToCotizacion+")", "$_modificarIva()", null);
	}
	

	function $_cambiarIvaCotizaicon(idToCotizacion) {
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/guardarCotizacion.action?'+jQuery(document.cotizacionForm).serialize()
				,'contenido', null);
	}

	function $_modificarIva() {
		var iva = dwr.util.getValue('porcentajeIva');
		if(iva == IvaInterior) {
			dwr.util.setValue('porcentajeIva',ivaFrontera);
		}else{
			dwr.util.setValue('porcentajeIva',IvaInterior);
		}
	}
	
	function sizeGridCotizacion(){
		jQuery(document).ready(function(){
			jQuery('#cotizacionGrid').css('height','320px');
			jQuery('div[class=objbox]').css('height','275px');
		})
	}

	function reSizeGridCotizacion(){
		jQuery(document).ready(function(){
			jQuery('#cotizacionGrid').css('height','140px');
			jQuery('div[class=objbox]').css('height','100px');
		})
	}
	function displayFilters(){
		jQuery('#contenedorFiltros').toggle('fast', function() {
				if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
					jQuery('#mostrarFiltros').html('Mostrar Filtros')
					sizeGridCotizacion();
				} else {
					jQuery('#mostrarFiltros').html('Ocultar Filtros')
					reSizeGridCotizacion();
				}
			});
		

	}
	function abrirCargaMasiva(idToCotizacion, tipoRegreso){
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/mostrarCarga.action?id='+idToCotizacion+"&tipoRegreso="+tipoRegreso,'contenido', null);
	}
	
	function iniciarCargaMasivaIndividual(){
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarContenedor.action','contenido', null);
	}
	
	function iniciarAutoExpedibles(){
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarContenedor.action','contenido', null);
	}
	
	
	function cerrarVentanaComponente(id){
	 parent.cerrarVentanaModal("mostrarContenedorImpresion");
	}
	
	function mostrarContenedorImpresion(tipoImpresion,idCotizacion, fechaValidez){
		var url = '/MidasWeb/impresiones/componente/mostrarContenedorImpresion.action?id='+idCotizacion+ "&tipoImpresion="+tipoImpresion;
		if(fechaValidez){
			url = url + "&fechaValidez=" + fechaValidez;
		}
		
		parent.mostrarVentanaModal("mostrarContenedorImpresion", 'Impresiones', 200, 320, 540, 315, url);
	}
	
	function cargarRecargoPagoFraccionado(value, idMoneda){		
		if(value != null && value != '' && value != '-1'){
			blockPage();
			listadoService.getPctePagoFraccionado(value, idMoneda, function(data){
				jQuery('#porcentajePagoFraccionado').val(data);
				unblockPage();
			});
		}else{
			jQuery('#porcentajePagoFraccionado').val('');
		}		
	}
	
	function selectGridCotizacionSize()
	{		
		if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
			jQuery('#mostrarFiltros').html('Mostrar Filtros')
			sizeGridCotizacion();
		} else {
			jQuery('#mostrarFiltros').html('Ocultar Filtros')
			reSizeGridCotizacion();
		}
	}
	
	function cotizacionesGridPaginadoBtnBuscar(page, nuevoFiltro){
		var posPath = '&posActual='+page+'&funcionPaginar='+'cotizacionesGridPaginado'+'&divGridPaginar='+'cotizacionGrid';
		if(nuevoFiltro){
			paginadoCotParamPath = jQuery(document.cotizacionForm).serialize();
		}else{
			posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
		}	
		sendRequestJQTarifa(null, "/MidasWeb/suscripcion/cotizacion/auto/busquedaPaginada.action" + "?"+ paginadoCotParamPath + posPath, 'gridCotizacionPaginado', 'obtenerCotizaciones(1);');

	}
	
	function ajustaFechaFinal(){
	    var date = Date.parseExact(jQuery("#fecha").val(),'dd/MM/yyyy');
	    var new_date = date.add(1).year();
	    jQuery('#fechados').val(new_date.toString('dd/MM/yyyy'));
	}
	
	
	function iniciarCotizadorAgentes(idToCotizacion){
		var id = "";
		if(idToCotizacion != null && idToCotizacion != undefined && idToCotizacion != ''){
			id = "?idToCotizacion="+idToCotizacion;
		}
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrarContenedor.action'+id,'contenido', null);
	}
	
	function iniciarCotizadorServicioPublico(){
		sendRequestJQAsync(null, "/MidasWeb/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico/mostrar.action", "contenido", null);
	}
	
	function iniciarCargaMasivaServicioPublico(){
		sendRequestJQAsync(null, "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/mostrarContCargaMasiva.action", targetWorkArea, null);
	}
	