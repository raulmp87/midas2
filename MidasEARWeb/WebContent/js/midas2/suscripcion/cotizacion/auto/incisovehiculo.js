var listadoSolicitudGrid;
var ventanaVehiculo;
var cargaDatosCliente = 1;
var paginadoParamsPath = null;

function closeInciso(){	
	var idToCotizacion = dwr.util.getValue("incisoCotizacion.id.idToCotizacion")
	parent.cerrarMensajeInformativo();
	parent.cerrarVentanaModal('inciso');		
}

/*function mostrarCaracteristicasVehiculo(){
	var path= "/MidasWeb/vehiculo/inciso/mostrarOtrasCaract.action";	
	sendRequestJQAsync(null,path,targetWorkArea,null);	
}*/


/**
 * Multiplica un inciso n veces
 * recibe dos parametros para formar el incisoCotizaconId
 * y el numero de copias que se van hacer
 * @param numeroInciso 
 * @param idToCotizacion
 * @param numeroCopias
 */
function multiplicarInciso(numeroInciso,idToCotizacion,numeroCopias){
	var path="/MidasWeb/vehiculo/inciso/multiplicarInciso.action?incisoCotizacion.id.numeroInciso=" + numeroInciso + 
				"&incisoCotizacion.id.idToCotizacion=" + idToCotizacion + '&numeroCopias=' + numeroCopias;
	parent.redirectVentanaModal('multiplicarInciso', path, null)		
}

function funcionCierreMultiplicarInciso(idToCotizacion){
	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion,'contenido', null);
	parent.cerrarVentanaModal('multiplicarInciso');
}

function guardarInciso(idToCotizacion){
	var path= '/MidasWeb/vehiculo/inciso/guardarInciso.action';
	parent.sendRequestJQAsync(document.incisoForm,path,'contenido',evaluaTipoPolizaInciso);
	/*
	console.log("guardarInciso: numero de serie="+idToCotizacion);
	var vin = jQuery("[id~='incisoCotizacion.incisoAutoCot.numeroSerie']").val();
	console.log("guardarInciso: numero de serie="+vin);
	if(vin != "" && vin != undefined && vin != null){		
		parent.document.getElementById("noSerieVin").value = vin;
		var respuesta = parent.consultarCantidadAlertas(vin);
		if(respuesta=="si"){
			console.log("pintar de rojo");
			parent.pintarDeRojo();
		}
	}
	*/
}

function mostrarBotonGuardar(){
	jQuery('#btnGuardar').css('display', 'block');
}

function recalcular(){
	var path= '/MidasWeb/vehiculo/inciso/calcularInciso.action';
	parent.submitVentanaModal("inciso", document.incisoForm);	
}
function procesoCmbDiasSalario(){
	jQuery("#idValDiasSalario option" ).each(function(){
	        if(jQuery("#valDiasSalario").val()==jQuery(this).val()+".0"){
	        		jQuery(this).attr("selected","selected");
	        }
	 });
}

function muestraDetallDatosComplementarios(){
	var idToCotizacion = parent.jQuery("#recargaVehiculo").find("#idToCotizacion").val();
	var numeroInciso = parent.jQuery("#recargaVehiculo").find("#numeroInciso").val();
	if(numeroInciso != undefined){
		closeVentanaControles();
		//closeIncisoSimple();
		parent.jQuery("#recargaVehiculo").html("");
		parent.muestraDetalleInciso(idToCotizacion, numeroInciso);
	}
}

function afterRecalcular(){
	var idToCotizacion = parent.jQuery("#recargaVehiculo").find("#idToCotizacion").val();
	var numeroInciso = parent.jQuery("#recargaVehiculo").find("#numeroInciso").val();
	if(numeroInciso != undefined){
		//closeIncisoSimple();
	    parent.jQuery("#recargaVehiculo").html("");
	    validaRiesgos(idToCotizacion,numeroInciso);
		parent.muestraDetalleInciso(idToCotizacion, numeroInciso);
		mostrarBotonGuardar();
	}
}

function mostrarDatosRiesgo(idToCotizacion, numeroInciso){
	 var url = "/MidasWeb/vehiculo/inciso/mostrarControlDinamicoRiesgo.action?incisoCotizacion.id.idToCotizacion="+
		idToCotizacion+"&incisoCotizacion.id.numeroInciso="+numeroInciso;	
	parent.mostrarVentanaModal("winCargaControles", "Complementar Datos de Riesgo", 200, 220, 800, 500, url);	
}

function validaRiesgos(idToCotizacion,numeroInciso){
	listadoService.obtieneNumeroDeRiesgos(idToCotizacion,numeroInciso,
	function(data){
		if(data == 0){
			mostrarVentanaCargaControles(idToCotizacion,numeroInciso);
		}
	});
}

function muestraDatosAdicionalesVehiculo(){
	listadoService.requiereDatosRiesgoIncisoCotizacion(dwr.util.getValue("incisoCotizacion.id.idToCotizacion"),dwr.util.getValue("incisoCotizacion.id.numeroInciso"),
			function(data){
				if(data){
					jQuery('#btnDatosAdicionalesPaquete').css('display', 'block');
				}
	});
}
function pageGridPaginado(page, nuevoFiltro) {
	var idToCotizacion = jQuery('#idToCotizacion').val();
	var posPath = '&posActual=' + page + '&funcionPaginar='
			+ 'pageGridPaginadoIncisos' + '&divGridPaginar=' + 'listadoIncisos';
	if (nuevoFiltro) {
		paginadoParamsPath = jQuery(document.IncisoForm).serialize();
	} else {
		posPath = '&posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosPath + "?" + paginadoParamsPath
			+ posPath, 'gridListadoDeIncisos', 'mostrarListadoIncisos('
			+ idToCotizacion + ')');
}
function pageGridPaginadoIncisos(page, nuevoFiltro) {
	var idToCotizacion = jQuery('#idToCotizacion').val();
	var posPath = 'posActual=' + page + '&funcionPaginar='
			+ 'pageGridPaginadoIncisos' + '&divGridPaginar=' + 'listadoIncisos'
			+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion;
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosRapidaPath + "?" + posPath,
			'gridListadoDeIncisos', 'mostrarListadoIncisos('+idToCotizacion+')');
}

function pageGridPaginadoIncisos_complementar(page, nuevoFiltro) {
	var idToCotizacion = jQuery('#idToCotizacion').val();
	var posPath = 'posActual=' + page + '&funcionPaginar='
			+ 'pageGridPaginadoIncisos' + '&divGridPaginar=' + 'listadoIncisos'
			+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion
			+ '&incisoCotizacion.numeroSecuencia=' + dwr.util.getValue('numeroSecuencia')
			+ '&incisoCotizacion.incisoAutoCot.descripcionFinal=' + dwr.util.getValue('descripcionFinal') 
			+ '&incisoCotizacion.incisoAutoCot.numeroSerie=' + dwr.util.getValue('numeroSerie')
			+ '&incisoCotizacion.incisoAutoCot.placa=' + dwr.util.getValue('placa')
			+ '&incisoCotizacion.incisoAutoCot.nombreAsegurado=' + dwr.util.getValue('nombreAsegurado');
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&'
				+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosRapidaPath + "?" + posPath,
			'gridListadoDeIncisos', 'mostrarListadoIncisos('+idToCotizacion+')');
}

function mostrarListadoIncisos(idToCotizacion){
	document.getElementById("listadoIncisos").innerHTML = '';
	listadoSolicitudGrid = new dhtmlXGridObject('listadoIncisos');
	mostrarIndicadorCarga('indicador');
	listadoSolicitudGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
	listadoSolicitudGrid.attachEvent("onXLE", function(grid){
		CantidadRegistrosGrid();
		var cadena = "";
		listadoSolicitudGrid.forEachRow(function(id){
			
			console.log("for each row");
			console.log(listadoSolicitudGrid.cellById(id, 0).getValue());
			
			if(listadoSolicitudGrid.cellById(id, 0).getValue() != null && listadoSolicitudGrid.cellById(id, 0).getValue() != ""){
				cadena += listadoSolicitudGrid.cellById(id, 0).getValue() + "|";
			} else {
				console.log("nulo");
			}
		});
		
		jQuery('#noSerieVin').val(cadena.substring(0, cadena.length - 1));
		console.log(jQuery('#noSerieVin').val());
		
		
		if(jQuery('#noSerieVin').val() != "" && jQuery('#noSerieVin').val() != null){
			iniciarlizarPestania();
		}
	
	});
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	
	if(paginadoParamsPath != null){
		listadoSolicitudGrid.load(busquedaRapidaPath +"?incisoCotizacion.id.idToCotizacion=" + idToCotizacion + '&'+paginadoParamsPath +"&" + posPath  + soloConsulta);
	}else{
		listadoSolicitudGrid.load(busquedaRapidaPath
				+ "?incisoCotizacion.id.idToCotizacion=" + idToCotizacion + "&"
				+ posPath + soloConsulta);
	}
	
}

function obtenerIncisos(){
	var path = '/MidasWeb/vehiculo/inciso/listarIncisos.action';
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoSolicitudGrid = new dhtmlXGridObject('listadoIncisos');
		mostrarIndicadorCarga('indicador');	
		listadoSolicitudGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
		listadoSolicitudGrid.load(path + "?" + jQuery(document.IncisoForm).serialize());
}

function eliminarInciso(idCotizacion,numeroSecuencia,seccion,filtros){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el inciso '+ numeroSecuencia +'?')){
		var path = 	'/MidasWeb/vehiculo/inciso/eliminarInciso.action?incisoCotizacion.id.idToCotizacion=' + idCotizacion + '&incisoCotizacion.numeroSecuencia=' + numeroSecuencia + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccion 
			+ '&descripcionInciso=' + filtros.descripcion
			+ '&incisoCotizacion.incisoAutoCot.negocioPaqueteId='+ filtros.paqueteId;
		parent.sendRequestJQAsync(null,path,'contenido',null);
	}
}

function eliminarIncisoIndividual(idCotizacion,numeroSecuencia,seccion){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el inciso?')){
		var path = 	'/MidasWeb/vehiculo/inciso/eliminarInciso.action?incisoCotizacion.id.idToCotizacion=' + idCotizacion + '&incisoCotizacion.numeroSecuencia=' + numeroSecuencia + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccion;
		parent.sendRequestJQAsync(null,path,'contenido',null);
	}
}

// Abre la ventana Multiplicar inciso en modal
function mostrarMultiplicarInciso(numeroInciso, idToCotizacion,seccionId){
	var url = '/MidasWeb/vehiculo/inciso/mostrarMultiplicarInciso.action?incisoCotizacion.id.numeroInciso=' + numeroInciso + '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccionId;
	mostrarVentanaModal('multiplicarInciso', 'Multiplicar Inciso', 200, 220, 500, 200, url);	
}

/**
 * Abre la ventana para igualar la prima total de un inciso
 */
function mostrarIgualarInciso(idToCotizacion, numeroInciso, claveEstatus) {
	var url = '/MidasWeb/vehiculo/inciso/mostrarIgualarInciso.action?incisoCotizacion.id.numeroInciso=' + numeroInciso
				+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + "&claveEstatus="+claveEstatus;
	mostrarVentanaModal('igualarInciso', 'Igualar Inciso', 200, 220, 500, 200, url, "verDetalleCotizacion("+idToCotizacion + "," + claveEstatus +")");	
}

function evaluaTipoPolizaInciso(){
	var isFlotilla =  document.getElementById("incisoCotizacion.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas").value;
	var idToCotizacion = parent.dwr.util.getValue("incisoCotizacion.id.idToCotizacion")
	if(isFlotilla == 1){
		if(confirm("\u00BFDesea agregar otro inciso\u003F")){
			document.getElementById("incisoCotizacion.id.numeroInciso").value = '';		
			jQuery("#btnGuardar").css('display','none');
		}else{
			closeInciso();
		}		
	}else{
		closeInciso();
	}
}


function muestraDetalleInciso(idToCotizacion, numeroInciso){
	var url;
	var porcentajeIva = "&incisoCotizacion.cotizacionDTO.porcentajeIva="+dwr.util.getValue("porcentajeIva");
	if(numeroInciso == null){
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+porcentajeIva;
	}else{
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+"&incisoCotizacion.id.numeroInciso="+numeroInciso+porcentajeIva;
	}
	parent.redirectVentanaModal("inciso", url, null);	
}

function cargaValoresInciso(){
	obtenerCoberturaCotizaciones();
}

function cargaValoresIncisoAgente(){
	obtenerCoberturaCotizacionesAgente();
}

function CantidadRegistrosGrid(){
	if (listadoSolicitudGrid.getRowsNum()==0){
		 jQuery("#esquemadePago").hide();
		 jQuery("#imprimeCotizacion").hide();
		 jQuery("#enviaEmail").hide();
		 jQuery("#solicitaEmision").hide();
		 jQuery("#guardaInciso").show();
		 jQuery("#cargaMrasiva").hide();
		 jQuery("#agregarInciso").show();
	 }else{
		 jQuery("#esquemadePago").show();
		 jQuery("#imprimeCotizacion").show();
		 jQuery("#enviaEmail").show();
		 jQuery("#solicitaEmision").show();
		 jQuery("#guardarInciso").show();
		 jQuery("#cargaMasiva").show();
		 jQuery("#agregarInciso").show();
	 }		
}

function recargarVehiculoAgente(idToCotizacion, numeroInciso, cargaCliente, numeroSecuencia, soloConsulta){		
	var url = datosVehiculoPath+"?cotizacionId="+idToCotizacion+"&incisoId="+numeroInciso+"&copiarDatos="+cargaCliente+"&soloConsulta="+soloConsulta;		
	document.getElementById("divComplementarInciso").innerHTML = '';
	mostrarIndicadorCarga("divCargaComplementarInciso"); 
	sendRequestJQ(null,url, "divComplementarInciso","ocultarIndicadorCarga('divCargaComplementarInciso'); ");
}
function mostrarDatosVehiculo(idToCotizacion, numeroInciso, soloConsulta,numeroSecuencia){
	mostrarVentanaVehiculo(numeroInciso, null, soloConsulta,numeroSecuencia);
}

function mostrarVentanaVehiculo(id, onCloseFunction, soloConsulta,numeroSecuencia) {
	var soloConsultaPath = "";
	if(soloConsulta != "" && soloConsulta != undefined){
		soloConsultaPath = "&soloConsulta="+soloConsulta;
	}
	var url = datosVehiculoPath + "?cotizacionId="+dwr.util.getValue("idToCotizacion")+"&incisoId="+id+"&copiarDatos="+cargaDatosCliente+soloConsultaPath;
	mostrarVentanaModal('ventanaVehiculo', 'Datos Complementarios Veh\u00edculo n\u00famero de inciso: ' + numeroSecuencia, 50, 50, 800, 450, url, null );
}
function closeVehiculo(cotizacionId){
	cerrarVentanaModal('ventanaVehiculo');
	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+dwr.util.getValue("idToCotizacion"),'contenido', null);
}

function recargarVentanaVehiculo(idToCotizacion, numeroInciso, cargaCliente, numeroSecuencia){
	//closeVehiculo();
	cerrarVentanaModal('ventanaVehiculo');
	cargaDatosCliente = cargaCliente;
	mostrarDatosVehiculo(idToCotizacion, numeroInciso,"",numeroSecuencia);
}

function mostrarVentanaAsegurado(numeroInciso,idToCotizacion,nuermoSecuencia){
	var url = "/MidasWeb/vehiculo/inciso/asegurado.mostrarVentanaAsegurado.action?incisoCotizacion.id.idToCotizacion="
			+ idToCotizacion
			+ "&incisoCotizacion.id.numeroInciso="
			+ numeroInciso;
	mostrarVentanaModal('ventanaAsegurado', 'Complementar Datos Asegurado n\u00famero de inciso: ' + nuermoSecuencia, 200, 400, 450, 240, url, null );	
}

function mostrarVentanaAseguradoAgente(numeroInciso,idToCotizacion,nuermoSecuencia){
	var url = "/MidasWeb/vehiculo/inciso/asegurado.mostrarVentanaAseguradoAgente.action?incisoCotizacion.id.idToCotizacion="
			+ idToCotizacion
			+ "&incisoCotizacion.id.numeroInciso="
			+ numeroInciso;
	mostrarVentanaModal('ventanaAsegurado', 'Complementar Datos Asegurado n\u00famero de inciso: ' + nuermoSecuencia, 200, 400, 450, 240, url, null );	
}

/*function guardarComplementarInciso(){
	parent.sendRequestJQAsync(jQuery('#complementarIncisoForm')[0], 
			'/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardar.action' , 
			'mensaje', null);
}
*/

function closeVentanaAseguradoAgente(cotizacionId){
	cerrarVentanaModal('ventanaAsegurado');
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrarContenedor.action?tabActiva=complementar&idToCotizacion='+cotizacionId,'contenido', null);
}

function closeVentanaAsegurado(cotizacionId){
	cerrarVentanaModal('ventanaAsegurado');
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+cotizacionId,'contenido', null);
}

function guardarRiesgosCotizacion(){
	parent.submitVentanaModal("winCargaControles", jQuery('#complementarIncisoForm')[0]);
	/*
	parent.sendRequestJQAsync(jQuery('#complementarIncisoForm')[0], 
			'/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardarRiesgosCotizacion.action', 
			"recargaVehiculo",muestraDetallDatosComplementarios);*/
}	

function cerrarRiesgoRefrescarInciso(idToCotizacion, numeroInciso){	
	parent.cerrarVentanaModal("winCargaControles");
	var ifr = parent.getWindowContainerFrame('inciso');
	ifr.contentWindow.validaRecalcular();
}

function mostrarVentanaDatosAdicionalesPaquete(){
	mostrarVentanaCargaControles(dwr.util.getValue("incisoCotizacion.id.idToCotizacion"),dwr.util.getValue("incisoCotizacion.id.numeroInciso"));
}	

function validaRangoSumaAsegurada(obj,min,max,valorDefault,index){
	aplicaRedondeo(index);
	var valor = removeFormatNumber(dwr.util.getValue(obj));
	if(parseFloat(valor) > parseFloat(max)){
		alert("El valor m\u00e1ximo permitido es: " + dwr.util.getValue("valorSumaAseguradaMax_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}else if(parseFloat(valor) < parseFloat(min)){
		alert("El valor m\u00ednimo permitido es: " + dwr.util.getValue("valorSumaAseguradaMin_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}
}

function aplicaFormatoKeyUp(index){
	jQuery("#valorSumaAseguradaStr_"+index).keyup(function(e) {
		var e = window.event || e;
		var keyUnicode = e.charCode || e.keyCode;
		if (e !== undefined) {
			switch (keyUnicode) {
				case 16: break; // Shift
				case 17: break; // Ctrl
				case 18: break; // Alt
				case 27: this.value = ''; break; // Esc: clear entry
				case 35: break; // End
				case 36: break; // Home
				case 37: break; // cursor left
				case 38: break; // cursor up
				case 39: break; // cursor right
				case 40: break; // cursor down
				case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
				case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
				case 190: break; // .
				default: jQuery("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1 });
			}
		}
	});
}		

function aplicaRedondeo(index){
	jQuery("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
}


function habilitaRegistroCobertura(index){
	if(index!=null){
		if(jQuery("#claveContratoBoolean_"+index).is(":checked")){
			jQuery("#valorSumaAseguradaStr_"+index).removeAttr("readonly");
			jQuery("#valorDeducible_"+index).removeAttr("readonly");
		}else{
			jQuery("#valorSumaAseguradaStr_"+index).attr("readonly","readonly");
			jQuery("#valorDeducible_"+index).attr("readonly","readonly");
		}
	}
}

function validaDisable(combo, index){
	var DIAS_SALARIO=52.59;
	var sumaAsegurada=jQuery('#numeroAcientos').val()*jQuery('#idValDiasSalario').val()*DIAS_SALARIO;
	jQuery('#idSumaAsegurada').val(sumaAsegurada);
	if(jQuery("#claveContratoBoolean_"+index).is(":checked") || jQuery('#coberturaCotizacionList_'+index+'__claveObligatoriedad').val() == '0'){
		ocultaGuardar();
	}else{
		parent.mostrarMensajeInformativo("Se necesita contratar la cobertura antes de modificar el valor", "20", null, null);
		combo.selectedIndex = 0;		
	}
	
}

function validaSumaSeleccionada(combo, index){
	var valorSuma = jQuery('#valorSumaAseguradaStr_'+index).val();
	if(!valorSuma){
		parent.mostrarMensajeInformativo("Se necesita seleccionar un valor de suma asegurada", "20", null, null);
		combo.selectedIndex = 1;
	}
}

function mostrarModal(id, text, x, y, width, height, url){
	mostrarVentanaModal(id, text, x, y, width, height, url);
}

function eliminarIncisosSeleccionados(){
	var pathIncisos = cargaIncisosAEliminar();
	if(pathIncisos != null && pathIncisos != undefined && pathIncisos != ""){
		if(confirm('\u00BFEst\u00e1 seguro que desea eliminar los incisos seleccionados?')){
			var filtros = obtieneFiltros();
			var path = 	'/MidasWeb/vehiculo/inciso/eliminarIncisoMasivo.action?incisoCotizacion.id.idToCotizacion=' + jQuery("#idToCotizacion").val()
				+ '&descripcionInciso=' + filtros.descripcion + "&" + pathIncisos;
			parent.sendRequestJQAsync(null,path,'contenido',null);
		}
	}else{
		mostrarMensajeInformativo("Debe seleccionar al menos un Inciso a Eliminar.", "10", null, null);
	}
}

function cargaIncisosAEliminar(){
	var incisos = "";
	var num = 0;
	var idCheck = listadoSolicitudGrid.getCheckedRows(0);
	if(idCheck != ""){
	var ids = idCheck.split(",");
	for(i = 0; i < ids.length; i++){
   	 	if(incisos != ""){
   	 	incisos+="&"
   	 	}
   	 	incisos+="incisoCotizacionList["+ num+ "].id.numeroInciso="+ids[i];
   	 	num = num + 1;		
	}
	}
	
	return incisos;
}

function eliminarTodosLosIncisos(){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar todos los incisos?')){
		var filtros = obtieneFiltros();
		var pathIncisos = cargaIncisosAEliminar();
		var path = 	'/MidasWeb/vehiculo/inciso/eliminarTodosIncisos.action?incisoCotizacion.id.idToCotizacion=' + jQuery("#idToCotizacion").val()
			+ '&descripcionInciso=' + filtros.descripcion;
		parent.sendRequestJQAsync(null,path,'contenido',null);
	}
}

function mostrarVentanaCondicionesEspeciales(numeroInciso, idToCotizacion, numeroSecuencia, tipoLlamada){
	var url = "/MidasWeb/componente/condicionespecial/mostrarContenedor.action?idToCotizacion="	+ idToCotizacion + "&numeroInciso=" + numeroInciso + "&tipoLlamada=" + tipoLlamada;
	mostrarVentanaModal('ventanaCondicionesEspeciales', 'Condiciones especiales del inciso n\u00famero: ' + numeroSecuencia, 200, 400, 900, 500, url, null );	
}

var numeroInciso   = '<s:property value="incisoCotizacion.id.numeroInciso"/>';
var idToCotizacion = '<s:property value="incisoCotizacion.id.idToCotizacion"/>';

// TWRY
function mostrarDaniosOcasionadosPorLaCarga(combo, index){
	validaRecalcular();
}

function validaRecalcular() {	
	if (document.getElementById("coberturaCotizacionGrid").innerHTML != '') {
		if(numeroInciso == '' || numeroInciso == null) {
			numeroInciso = $_numeroIncisoTemp;
		}
		if (document.getElementById("idCodigo").value !=''){
			recalcularCotizadorAgente();
		}else{
			parent.mostrarMensajeInformativo("El C\u00F3digo postal es obligatorio", "20", null, null);
		} 
	} else {
		parent.mostrarMensajeInformativo("No tienes coberturas para Recalcular", "20", null, null);
	}
}