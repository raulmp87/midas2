var listadoArchivosCargadosGrid;

function iniciaArchivosCargaMasivaGrid(){
	
	jQuery("#archivosAdjuntosGrid").empty();
	
	listadoArchivosCargadosGrid = new dhtmlXGridObject('archivosAdjuntosGrid');
	listadoArchivosCargadosGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
	listadoArchivosCargadosGrid.setSkin('light');
	listadoArchivosCargadosGrid.setHeader("ID, Archivo, Fecha de carga, Usuario, Estatus, Acciones");
	listadoArchivosCargadosGrid.setInitWidths("50,*,200,200,200,200");
	listadoArchivosCargadosGrid.setColAlign("center,center,center,center,center,center");
	listadoArchivosCargadosGrid.setColSorting("na,server,server,server,server,na");
	listadoArchivosCargadosGrid.setColTypes("ro,ro,ro,ro,ro,img");			
	listadoArchivosCargadosGrid.enableMultiline(true);
	listadoArchivosCargadosGrid.init();
	listadoArchivosCargadosGrid.attachEvent("onBeforePageChanged",function(){
		if (!this.getRowsNum()) return false;
		return true;
	});		
	listadoArchivosCargadosGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
	listadoArchivosCargadosGrid.setPagingSkin("bricks");	
	listadoArchivosCargadosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoArchivosCargadosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listadoArchivosCargadosGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
    });
	listadoArchivosCargadosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
    });
	
	listadoArchivosCargadosGrid.attachEvent("onBeforeSorting",function(ind, server, direct){
		var server = "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/obtenerArchivosAdjuntos.action";
		listadoArchivosCargadosGrid.clearAll();
		listadoArchivosCargadosGrid.load(server+(server.indexOf("?")>=0?"&":"?")
	     +"orderBy="+ind+"&direct="+direct);			
		listadoArchivosCargadosGrid.setSortImgState(true,ind,direct);
	    return false;
	});

	listadoArchivosCargadosGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/obtenerArchivosAdjuntos.action");
}

function mostrarResumenCargaMasivaServicioPublico(id){
	var location ="/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/mostrarContCargaMasivaDetalle.action?idToControlArchivo=" + id;
	sendRequestJQ(null, location, targetWorkArea, null);
}

function importarArchivo(){
	var path = "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/validaCarga.action?";
	var tipoCarga = 1;

	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaServicioPublicoArchivos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva de Archivos Servicio P\u00FAblico");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			sendRequestJQ(null, path + jQuery(document.cargaMasivaServicioPublicoForm).serialize() + "&idToControlArchivo=" + idToControlArchivo + "&tipoCarga=" + tipoCarga, targetWorkArea, null);
    			}else{
    				mostrarMensajeInformativo('Fallo carga de archivo, favor de volver a intentar en unos momentos.',"20");
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaServicioPublicoArchivos").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           mostrarMensajeInformativo('Solo puede importar archivos Excel (.xls - .xlsx).',"20");
           return false; 
        } 
        else return true; 
     }; 
    vault.create("vault");
    vault.setFormField("claveTipo", "43");
}

function regresaCotizador(){
	var location ="/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=A";
	sendRequestJQ(null, location, targetWorkArea, null);
}

function iniciarbitacorasCargaMasivaSPublicoGrid(id){
	var location ="/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/bitacora/mostrarContBitacorasCargaMasivaSPublico.action?idToControlArchivo=" + id;
	sendRequestJQ(null, location, targetWorkArea, null);
}