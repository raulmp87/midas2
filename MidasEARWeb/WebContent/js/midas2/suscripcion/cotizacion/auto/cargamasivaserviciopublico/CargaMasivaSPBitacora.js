var bitacorasCargadaGrid;

function regresaCargaMasiva(){
	sendRequestJQAsync(null, "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/mostrarContCargaMasiva.action", "contenido", null);
}

//function iniciarbitacorasCargaMasivaSPublicoGrid(){
//	document.getElementById("bitacorasArchivosAdjuntosGrid").innerHTML = '';
//	bitacorasCargadaGrid = new dhtmlXGridObject("bitacorasArchivosAdjuntosGrid");
//	bitacorasCargadaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
//	bitacorasCargadaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
//	bitacorasCargadaGrid.attachEvent("onXLS", function(grid){
//		mostrarIndicadorCarga("indicadorGrid");
//    });
//	bitacorasCargadaGrid.attachEvent("onXLE", function(grid){
//		ocultarIndicadorCarga('indicadorGrid');
//    });		
//	bitacorasCargadaGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/bitacora/buscarContBitacorasCargaMasivaSPublicoGrid.action");
//}

function limpiar(){
	jQuery(".cleaneable").each(
		function(){
			jQuery(this).val("");
		}
	);
}

function realizarBusqueda(sendParams){
	 var url = "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/bitacora/buscarContBitacorasCargaMasivaSPublicoGrid.action?";
	 var busquedaValidada =  validarBusqueda();
	 
	 if(sendParams){
	 	formParams = jQuery('#formBitacoraCargaMasivaSPFiltradoDTO').serialize();	
	 	url += formParams;
	 }
//	 if(busquedaValidada == "s"){
	 if(busquedaValidada == "s"){ //ESTA LINEA SE HARCODEO PARA PROBAR. 
		jQuery("#bitacorasArchivosAdjuntosGrid").empty();
		
		bitacorasCargadaGrid = new dhtmlXGridObject('bitacorasArchivosAdjuntosGrid');
		bitacorasCargadaGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		bitacorasCargadaGrid.setSkin('light');
		bitacorasCargadaGrid.setHeader("id, Póliza, Num. Serie, Estatus, Usuario Carga, Fecha Carga, Descripción");
		bitacorasCargadaGrid.setInitWidths("*,*,*,*,*,*,*");
		bitacorasCargadaGrid.setColAlign("center,center,center,center,center,center,center");
		bitacorasCargadaGrid.setColSorting("na,server,server,server,server,server,server,server");
		bitacorasCargadaGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");			
		bitacorasCargadaGrid.init();
		bitacorasCargadaGrid.setColumnHidden(0,true);

		
		bitacorasCargadaGrid.attachEvent("onBeforePageChanged",function(){
			if (!this.getRowsNum()) return false;
			return true;
		});		
		
		bitacorasCargadaGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		bitacorasCargadaGrid.setPagingSkin("bricks");	
		bitacorasCargadaGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		bitacorasCargadaGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		bitacorasCargadaGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
		});
		
		bitacorasCargadaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
		});		
		
			bitacorasCargadaGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
			server = url;			
			bitacorasCargadaGrid.clearAll();
			bitacorasCargadaGrid.load(server+(server.indexOf("?")>=0?"&":"?")+"orderBy="+ind+"&direct="+direct);			
			bitacorasCargadaGrid.setSortImgState(true,ind,direct);
			return false;
		});
		
		bitacorasCargadaGrid.load( url );
	} else{
	mostrarMensajeInformativo('Se debe ingresar al menos un campo para realizar la búsqueda', '20');
	
	}
}
	
function validarBusqueda(){
	var bandera = "n";
	jQuery(".cleaneable").each( function(){
		if( jQuery(this).val() != "" ){
			bandera = "s";
		}		
	});
	return bandera;
}
