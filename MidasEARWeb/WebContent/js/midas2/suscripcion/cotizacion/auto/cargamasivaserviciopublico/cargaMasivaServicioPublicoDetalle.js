var cargaMasivasGrid;

function iniciarCargaMasivaDetalleGrid(sendParams){
	 var url = "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/buscarContCargaMasivaDetalleGrid.action?idToControlArchivo="+jQuery('#idToControlArchivo').val();
		jQuery("#detalleArchivosAdjuntosGrid").empty();
		
		cargaMasivasGrid = new dhtmlXGridObject('detalleArchivosAdjuntosGrid');
		cargaMasivasGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
		cargaMasivasGrid.setSkin('light');
		cargaMasivasGrid.setHeader("id, Póliza, Cliente, Num Serie, Estilo, Modelo, Prima Total, Estatus, Accion ");
		cargaMasivasGrid.setInitWidths("*,*,*,*,*,*,*,*,*");
		cargaMasivasGrid.setColAlign("center,center,center,center,center,center,center,center,center");
		cargaMasivasGrid.setColSorting("na,server,server,server,server,server,server,server,na");
		cargaMasivasGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,img");			
		cargaMasivasGrid.init();
		cargaMasivasGrid.setColumnHidden(0,true);

		
		cargaMasivasGrid.attachEvent("onBeforePageChanged",function(){
			if (!this.getRowsNum()) return false;
			return true;
		});		
		
		cargaMasivasGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
		cargaMasivasGrid.setPagingSkin("bricks");	
		cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
		});
		
		cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
		});		
		
		cargaMasivasGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
			server = url;			
			cargaMasivasGrid.clearAll();
			cargaMasivasGrid.load(server+(server.indexOf("?")>=0?"&":"?")+"orderBy="+ind+"&direct="+direct);			
			cargaMasivasGrid.setSortImgState(true,ind,direct);
			return false;
		});
		
		cargaMasivasGrid.load( url );
	
}
	
function consultarErrorDetalle(idDetalle){
	 var url = "/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/consultarErroresDetalle.action?idDetalle="+ idDetalle;
	 mostrarVentanaModal("vm_errorDetalle", 'Esta es la descripci\u00F3n de error',  1, 1, 750, 350, url, null);
	
	
}




//function iniciarCargaMasivaDetalleGrid(){
//	document.getElementById("detalleArchivosAdjuntosGrid").innerHTML = '';
//	cargaMasivasGrid = new dhtmlXGridObject("detalleArchivosAdjuntosGrid");
//	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
//	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
//	cargaMasivasGrid.attachEvent("onXLS", function(grid){
//		mostrarIndicadorCarga("indicadorGrid");
//    });
//	cargaMasivasGrid.attachEvent("onXLE", function(grid){
//		ocultarIndicadorCarga('indicadorGrid');
//    });		
//	cargaMasivasGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/buscarContCargaMasivaDetalleGrid.action?idToControlArchivo="+jQuery('#idToControlArchivo').val());
//}
