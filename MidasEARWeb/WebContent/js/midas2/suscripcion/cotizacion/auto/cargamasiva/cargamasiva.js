/**
 * 
 */
var cargaMasivasGrid;
var cargaMasivasDetalleGrid;

function iniciaListado(){
	var idCotizacion = jQuery("#id").val();
	document.getElementById("cargasMasivasGrid").innerHTML = '';
	cargaMasivasGrid = new dhtmlXGridObject("cargasMasivasGrid");
	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/listarCarga.action?id="+ idCotizacion);
}
function importarArchivo(tipoCarga){
	var idToCotizacion = jQuery("#id").val();
	var tipoRegreso = jQuery("#tipoRegreso").val();
	if(!jQuery("#idAcurdoAfirmeMasiva").is(':checked')) {
		 
		 alert("Debe marcar el acuerdo");
		 return;
	 }
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaIncisos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva de Incisos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/validaCarga.action?id=" + idToCotizacion + "&idToControlArchivo=" + idToControlArchivo + "&tipoCarga=" + tipoCarga + "&tipoRegreso=" + tipoRegreso, "contenido", null);
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaIncisos").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "40");
}

function descargarCargaMasiva(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function descargarLogCargaMasiva() {
	if(jQuery("#logErrors").val() == 'true'){
		var idToCotizacion = jQuery("#id").val();
		var idToControlArchivo = jQuery("#idToControlArchivo").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarLogErrores.action?id=" + idToCotizacion + "&idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT" + idToCotizacion);
	}
}

function descargarPlantillaCarga(){
	var idToCotizacion = jQuery("#id").val();
	var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarPlantilla.action?id=" + idToCotizacion;
	window.open(location, "Cotizacion_COT" + idToCotizacion);
}

function mostrarResumenCargaMasiva(idToCargaMasiva){
	var idToCotizacion = jQuery("#id").val();
	var tipoRegreso = jQuery("#tipoRegreso").val();
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/verDetalleCarga.action?id=" + idToCotizacion + "&idToCargaMasivaAutoCot=" + idToCargaMasiva + "&tipoRegreso="+tipoRegreso, "contenido", null);
}

function iniciaListadoDetalle(){
	var idToCargaMasivaAutoCot = jQuery("#idToCargaMasivaAutoCot").val();
	document.getElementById("cargasMasivasDetalleGrid").innerHTML = '';
	cargaMasivasDetalleGrid = new dhtmlXGridObject("cargasMasivasDetalleGrid");
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasDetalleGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/listarDetalleCarga.action?idToCargaMasivaAutoCot="+ idToCargaMasivaAutoCot);
}

function regresarACargaMasivaAuto(){
	var idToCotizacion = jQuery("#id").val();
	var tipoRegreso = jQuery("#tipoRegreso").val();
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/mostrarCarga.action?id=" + idToCotizacion + "&tipoRegreso="+tipoRegreso, "contenido", null);	
}

function mostrarErrorCargaMasiva(estatus, idToDetalleCargaMasivaAutoCot){
	if(estatus == 0){
		//mostrarVentanaMensaje('10', mensajeError);
		var idToCotizacion = jQuery("#id").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarLogErroresInciso.action?idToDetalleCargaMasivaAutoCot=" + idToDetalleCargaMasivaAutoCot;
		window.open(location, "Cotizacion_COT" + idToCotizacion);
	}
	if(estatus == 1){
		mostrarMensajeExito();
	}
	if(estatus == 2){
		mostrarVentanaMensaje('30', 'Pendiente de Procesar');
	}
}

function regresarACotizacion(tipoRegreso){
	var idToCotizacion = jQuery("#id").val();
	if(tipoRegreso == "1"){
		sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=A", "contenido", null);
	}
	if(tipoRegreso == "2"){
		sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id=" + idToCotizacion, "contenido", null);
	}
	if(tipoRegreso == "3"){
		sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId=" + idToCotizacion, "contenido", null);
	}
}