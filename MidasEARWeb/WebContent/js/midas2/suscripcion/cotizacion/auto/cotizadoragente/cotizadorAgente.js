/**
 * 
 */
var dhxWins= new dhtmlXWindows();
dhxWins.enableAutoViewport(true);
dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");

function cargarAgenteCotizadorAgente(id, name){
	document.getElementById("divSeleccionarAgente").innerHTML = '';
	mostrarIndicadorCarga("divSeleccionarAgenteIndicador"); 
	var nameComp = 'cotizacion.solicitudDTO.codigoAgente';
	var valor = '&cotizacion.solicitudDTO.codigoAgente='+id;
	sendRequestJQ(null,"/MidasWeb/componente/agente/getInfoAgenteCotizador.action?" +
			"idTcAgenteCompName="+nameComp+"&nombreAgenteCompName="+name+"&permiteBuscar=true"+valor, "divSeleccionarAgente",'cargaNegociosAgente('+id+');');
	cerrarVentanaModal("agente");
}

function cargaNegociosAgente(idAgente){
	ocultarIndicadorCarga("divSeleccionarAgenteIndicador"); 
	if(idAgente != null  && idAgente != headerValue){
		listadoService.listarNegociosPorAgente(idAgente,'A',null,
				function(data){
					addOptions('negocio',data);
				});	
	}else{
		addOptions('negocio',null);
	}
}

/**
 * Carga listado de vigencias asociadas al negocio
 * @author Adriana Flores
 * @param {integer}
 */
function cargaTiposVigenciaNegocio(idNegocio){
	if(idNegocio != null  && idNegocio != headerValue){
		listadoService.obtenerTiposVigenciaNegocio(idNegocio,
				function(data){		
					deshabilitarVigencias(jQuery.isEmptyObject(data));
					addOptions('tiposvigencia',data);
					
					if (sizeMap(data) > 0) {						
						 listadoService.obtenerVigenciaDefault(idNegocio, function(dataDefault){
							 setTimeout(function(){ 
								 if(dataDefault == -1){
									 dataDefault = "";
								 }
								 selectValorDefault('tiposvigencia', dataDefault);
								 actualizarFechas(dataDefault);
							 }, 2000);
						 });
					} else {						
						actualizarFechas(365);
					} 
				});
	}else{
		addOptions('tiposvigencia',null);
	}
}

function selectValorDefault(select, valorDefault){
	document.getElementById(select).value = valorDefault;		
}

function cargaProductosNegocioAgente(idNegocio){
	if(idNegocio != null  && idNegocio != headerValue){
		listadoService.getMapNegProductoPorNegocio(idNegocio,
				function(data){
					addOptions('producto',data);
					if (sizeMap(data) > 1) {				
						selectFirstValue('producto')
					}
				});	
	}else{
		addOptions('producto',null);
	}
}

function cargaTipoPolizaAgente(idNegocioProducto){
	if(idNegocioProducto != null  && idNegocioProducto != headerValue){
		listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(idNegocioProducto,0,
				function(data){
					addOptions('tipoPoliza',data);
					if (sizeMap(data) > 1) {				
						selectFirstValue('tipoPoliza')
					}
				});	
	}else{
		addOptions('tipoPoliza',null);
	}
}

function cargaFormasPagoAgente(idNegocioTipoPoliza){
	if(idNegocioTipoPoliza != null  && idNegocioTipoPoliza != headerValue){
		listadoService.getMapFormasdePagoByNegTipoPoliza(idNegocioTipoPoliza,
				function(data){
					addOptions('formaPago',data);
					if (sizeMap(data) > 1) {				
						selectFirstValue('formaPago')
					}
				});	
	}else{
		addOptions('formaPago',null);
	}
}

function selectFirstValue(select){
	// use first select element
	var el = document.getElementById(select); 
	// assuming el is not null, select 2th option
	el.selectedIndex = 1;
	
	jQuery("#"+select).change();
}

function cargaDerechosNegocio(idNegocio){
	if(!idNegocio){
	var idNegocio = dwr.util.getValue("negocio");
	}
	var idTipoPoliza = dwr.util.getValue("tipoPoliza");
	var idSeccion = dwr.util.getValue("incisoCotizacion.incisoAutoCot.negocioSeccionId");
	var idPaquete = dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId'); 
	var idMoneda = dwr.util.getValue("idMoneda");
	var valorActualDerecho = jQuery("#derechos").val();
	if(validaObligatoriosBusquedaConfiguracionesDerecho(idNegocio,idTipoPoliza,idSeccion,idPaquete,idMoneda)){
		var estado = dwr.util.getValue('incisoCotizacion.incisoAutoCot.estadoId');
		var municipio = dwr.util.getValue('incisoCotizacion.incisoAutoCot.municipioId');
		var idTipoUso = dwr.util.getValue('incisoCotizacion.incisoAutoCot.tipoUsoId');
		listadoService.getMapDerechosPolizaByConfiguracion(idNegocio, idTipoPoliza, idSeccion, idPaquete, idMoneda, estado, municipio, idTipoUso,
				function(data){
					addOptions('derechos',data);
					sortSelect('derechos');					
					listadoService.getDerechoPolizaDefaultByConfiguracion(idNegocio, idTipoPoliza, idSeccion, idPaquete, idMoneda, estado, municipio, idTipoUso, 
							function(value){
							dwr.util.setValue(jQuery('derechos').selector, value);
							if(valorActualDerecho && value != valorActualDerecho){
								parent.mostrarMensajeInformativo('Derechos actualizados. Favor de validarlos',"20");
							}
					});
					
				});
	}
	else if(idNegocio != null  && idNegocio != headerValue){
		listadoService.getMapDerechosPolizaByNegocio(idNegocio,
				function(data){
					addOptions('derechos',data);
					sortSelect('derechos');					
					listadoService.getDerechoPolizaDefault(idNegocio, function(value){
						dwr.util.setValue(jQuery('derechos').selector, value);
						if(valorActualDerecho && value != valorActualDerecho){
							parent.mostrarMensajeInformativo('Derechos actualizados. Favor de validarlos',"20");
						}
					});
					
				});	
	}else{
		addOptions('derechos',null);
	}
}

function validaObligatoriosBusquedaConfiguracionesDerecho(idNegocio, idTipoPoliza, idSeccion, idPaquete, idMoneda){
	if(idNegocio && idTipoPoliza && idSeccion && idPaquete && idMoneda){
		return true;
	}
	return false;
}

function mostrarPagoFraccionado(idNegocio){
	if(idNegocio == null || idNegocio == undefined){
		idNegocio = jQuery('#negocio').val();
	}
	if(idNegocio != null  && idNegocio != headerValue){
		listadoService.mostrarPagoFraccionado(idNegocio,
				function(data){
					if(data){
						jQuery('#divPagoFraccionado').show();
					}else{
						jQuery('#divPagoFraccionado').hide();
					}
				});	
	}else{
		jQuery('#divPagoFraccionado').hide();
		jQuery('#porcentajePagoFraccionado').val(0);
	}	
}

function obtenerResumenTotalesCotizacionGridAgente(){
	document.getElementById("resumenTotalesCotizacionGrid").innerHTML = '';
	mostrarIndicadorCarga("cargaResumenTotales"); 
	sendRequestJQ(document.cotizacionForm,"/MidasWeb/suscripcion/cotizacion/auto/mostrarResumenTotalesCotizacionAgente.action", "resumenTotalesCotizacionGrid",'validaIva();');
}


function mostrarDivEsquemaPago() {
	var primaTotal = '';
	var totalPrima = '';
	var derechos = '';	
	
try{
	var iva     	=  dwr.util.getValue("porcentajeIva");
	var derechosId  =  dwr.util.getValue("derechos");

	if(derechosId != null && derechosId != ''){
		derechos =  dwr.util.getText("derechos");
	}
	var primaTotalText = jQuery('table[id=t_riesgo] td div[id=primaTotal]').text();
	if(primaTotalText != null && primaTotalText != ''){
		primaTotal = primaTotalText.trim().replace('$','').replace(',','');
	}
	var totalPrimaText = jQuery('table[id=t_riesgo] td div[id=totalPrima]').text();
	if(totalPrimaText != null && totalPrimaText != ''){
		totalPrima = totalPrimaText.trim().replace('$','').replace(',','');
	}
	
	var url = verEsquemaPagoPath+"?cotizacion.fechaInicioVigencia="+dwr.util.getValue("fecha")
    +"&cotizacion.fechaFinVigencia="+dwr.util.getValue("fechados")
    +"&cotizacion.idFormaPago="+dwr.util.getValue("formaPago") 
    +"&totalPrima="+totalPrima
    +"&derechos="+derechos
    +"&iva="+iva
    +"&primaTotal="+primaTotal
    +"&cotizacion.idToCotizacion="+dwr.util.getValue("idCotizacion");
	
	document.getElementById("resumenEsquemaPago").innerHTML = '';
	mostrarIndicadorCarga("cargaEsquemaPago"); 
	sendRequestJQ(null,url, "resumenEsquemaPago","ocultarIndicadorCarga('cargaEsquemaPago')");
	
}catch(e){
	alert(e);
}
}


function mostrarDivVehiculo() {

try{
	var idNegocio = dwr.util.getValue("negocio");
	var idNegProducto = dwr.util.getValue("producto");
	var idToNegTipoPoliza = dwr.util.getValue("tipoPoliza");
	
	if(idNegocio != null  && idNegocio != headerValue &&
			idNegProducto != null  && idNegProducto != headerValue &&
			idToNegTipoPoliza != null  && idToNegTipoPoliza != headerValue){
		var url = verVehiculoPath+"?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idCotizacion")
		+"&incisoCotizacion.id.numeroInciso="+dwr.util.getValue("numeroInciso")
		+"&incisoCotizacion.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas="+0 
		+"&incisoCotizacion.cotizacionDTO.porcentajeIva="+dwr.util.getValue("porcentajeIva")
		+"&cotizacion.solicitudDTO.negocio.idToNegocio="+dwr.util.getValue("negocio")
		+"&cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto="+dwr.util.getValue("producto")
		+"&cotizacion.negocioTipoPoliza.idToNegTipoPoliza="+dwr.util.getValue("tipoPoliza")
		+"&incisoCotizacion.cotizacionDTO.idMoneda="+dwr.util.getValue("idMoneda");
	
		document.getElementById("divVehiculo").innerHTML = '';
		mostrarIndicadorCarga("divCargaVehiculo"); 
		sendRequestJQ(null,url, "divVehiculo","ocultarIndicadorCarga('divCargaVehiculo'); ");
		unblockPage();
	}else{
		document.getElementById("divVehiculo").innerHTML = '';
	}
}catch(e){
	alert(e);
}
}

function sortSelect(idTarget){
	//Sort
	var target = '#'+idTarget;
	var selectedOption = jQuery(target).val();
	jQuery(target).append(jQuery(target+" option").remove().sort(function(a, b) {
	    var at = parseInt(jQuery(a).text()), bt = parseInt(jQuery(b).text());
	    return (at > bt)?1:((at < bt)?-1:0);
	}));
	jQuery(target).val(selectedOption);
}

/**
 * Calcular las fechas dependiendo el tipo de vigencia seleccionada
 * @author Adriana Flores
 * @param {integer}
 */
function actualizarFechas(dias){
	var fechaInicioVigencia = new Date();
	var diasVigencia = parseInt(dias);
	
	var fechaFinVigencia = new Date();
	fechaFinVigencia.setDate(fechaInicioVigencia.getDate() + diasVigencia);	
	
	dwr.util.setValue('fecha', fechaInicioVigencia.getDate() + '/' + (fechaInicioVigencia.getMonth() + 1) + '/' + fechaInicioVigencia.getFullYear());
	dwr.util.setValue('fechados', fechaFinVigencia.getDate() + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getFullYear());
}

/**
 * Cambiar la visibilidad de campos: Tipo vigencia, fechas inicio y fin de vigencia 
 * @author Adriana Flores
 */
function deshabilitarVigencias(bool){
	
	if(!bool){
		
		jQuery("#tiposvigencia").attr("disabled", false);
		jQuery("#fecha").attr("readonly", true);
		jQuery("#fechados").attr("readonly", true);
	 } else {
		jQuery("#tiposvigencia").attr("disabled", true);
		jQuery("#fecha").attr("readonly", false);
		jQuery("#fechados").attr("readonly", false);		
	 }
}

function obtenerCoberturaCotizacionesAgente(){	
	
	if (dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId') == -2) {
		return false;
	}
	if (dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId') === '') {
		document.getElementById("coberturaCotizacionGrid").innerHTML = '';
		return false;
	}
	document.getElementById("coberturaCotizacionGrid").innerHTML = '';
	jQuery('#cargando').css('display','block');
	var numeroInciso = dwr.util.getValue('incisoCotizacion.id.numeroInciso') ;
	var idToCotizacion = dwr.util.getValue('incisoCotizacion.id.idToCotizacion'); 
	var idNegocioPaqueteId = dwr.util.getValue('incisoCotizacion.incisoAutoCot.negocioPaqueteId'); 
	var idEstado = dwr.util.getValue('incisoCotizacion.incisoAutoCot.estadoId');
	var idMunicipio = dwr.util.getValue('incisoCotizacion.incisoAutoCot.municipioId');
	var modeloId = dwr.util.getValue('incisoCotizacion.incisoAutoCot.modeloVehiculo');
	if( numeroInciso != '' && idToCotizacion != ''){
		sendRequestJQTarifa(document.cotizacionForm,
				"/MidasWeb/vehiculo/inciso/recuperarCorberturaCotizacion.action",
				"coberturaCotizacionGrid", jQuery('#btnGuardar').css('display','block'));
		// muestra el boton actualizar				
	}else{
		if(idNegocioPaqueteId != ''&& idEstado != '' && idMunicipio != '' && modeloId != ''){
			jQuery('#error').hide();
			sendRequestJQTarifa(document.cotizacionForm,
					"/MidasWeb/vehiculo/inciso/mostrarCorberturaCotizacion.action",
					"coberturaCotizacionGrid", null);
		}else if(idEstado == '' || idMunicipio == ''){
			parent.mostrarMensajeInformativo('Favor de seleccionar un Estado y/o Municipio',"20");
		}
	}
	// muestra el boton actualizar
	jQuery('#btnRecalcular').css('display', 'block');
	jQuery('#cargando').css('display','none');
}


function recalcularCotizadorAgente(){	
    var negocio = dwr.util.getValue('negocio');
    var producto    = dwr.util.getValue('producto');
    var tipoPoliza    = dwr.util.getValue('tipoPoliza');
    var numeroInciso    = dwr.util.getValue('incisoCotizacion.id.numeroInciso');
    var tipoVigencias = dwr.util.getValue('tiposvigencia');    
    
    if(negocio ==="" || producto ==="" || tipoPoliza ===""){
        mostrarMensajeInformativo("Favor de Capturar los campos Negocio, Producto y/o TipoPoliza antes de Generar la cotizaci\u00F3n", "20");
        return false;
    }	
	
    var formaDePago = dwr.util.getValue('formaPago');
    var derechos    = dwr.util.getValue('derechos');
    if(formaDePago ==="" || derechos ===""){
      mostrarMensajeInformativo("Favor de Capturar los campos Forma de pago y/o Derechos antes de Generar la cotizaci\u00F3n", "20");
      return false;
    }
   
	if (!fechaMenorQue(jQuery("#fecha").val(), jQuery("#fechados").val())) {
		 mostrarMensajeInformativo("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia", "20");
		 return false;
	}
	
	if(tipoVigencias === "" && document.getElementById("tiposvigencia").options.length > 1){
		mostrarMensajeInformativo("Favor de seleccionar el Tipo de vigencia", "20");
		 return false;
	}
	
	if(jQuery("#descuentoGlobal").val() == ""){
		 jQuery('#descuentoGlobal').val('0.0');
	}
	
	sendRequestJQasyncFalse(document.cotizacionForm,'/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/validarCedulaAgente.action','divResultado', 'unblockPage();');
	var resultado = jQuery('#divResultado').html();
	if(resultado !== ""){
		 mostrarMensajeInformativo(resultado, "20");
		 return;
	}
	sendRequestJQ(document.cotizacionForm,'/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/generaCotizacionAgente.action','contenido', 'unblockPage();');

}

function nuevaCotizacion(){
	 sendRequestJQ(null,'/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrarContenedor.action','contenido', null);	
}

function verIgualarPrimasAgente(idToCotizacion){
	var url = verIgualarPrimasAgentePath;
	url = url + '?idToCotizacion='+idToCotizacion;
	mostrarVentanaModal("igualarPrimasAgente", "Igualaci&oacute;n de primas", 200,320, 600, 250, url);
}

function verDetalleCotizacionAgente(idToCotizacion, claveEstatus){
	var cotizacionAsignada = 11;
	var soloConsulta = "";
	var numeroInciso = dwr.util.getValue("numInciso");
	var inciso = "&incisoCotizacion.id.numeroInciso=" + numeroInciso;
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	if(idToCotizacion === null){
		idToCotizacion = '';
	}
	if(claveEstatus==cotizacionAsignada){
		var url =  '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrar.action?idToCotizacion='+idToCotizacion;
		mostrarVentanaModal("detalleCotizacion", "Cotizaci\u00F3n", 200, 320, 610, 450, url);		
	}else{
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrar.action?idToCotizacion='+idToCotizacion+soloConsulta+inciso,'contenido_detalle', "ocultarIndicadorVentana()");
	}
	
}

function ocultarIndicadorVentana(){
	ocultarIndicadorCarga('indicadorContenedor');
	cerrarVentanaModal('igualarPrimasAgente');
}

function verComplementarEmisionAgente() {
	limpiarDivsGeneral();
	var cotizacionId = dwr.util.getValue('idToCotizacion');
	var soloConsulta = jQuery("#soloConsulta").val();

	var path= '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/validarDatosComplementarios.action?idToCotizacion='+cotizacionId+"&soloConsulta="+soloConsulta;
	sendRequestJQ(null, path,
			'contenido_complementar',
			"ocultarIndicadorCarga('indicadorContenedor')");
}

function desplegarDocumentos140Agente() {
	limpiarDivsGeneral();
	var path='/MidasWeb/complementar/documentos/mostrarDocumentos.action?cotizacion.idToCotizacion='+dwr.util.getValue('idToCotizacion');
	sendRequestJQ(null, path,
			'contenido_documentos140',
			"ocultarIndicadorCarga('indicadorContenedor')");
}

function desplegarCobranzaAgente(){
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cabranza/mostrar.action?idToCotizacion='+
			dwr.util.getValue('idToCotizacion')+"&soloConsulta="+soloConsulta+"&cotizadorAgente=true",'contenido_cobranza',"ocultarIndicadorCarga('indicadorContenedor')");
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_detalle');
	limpiarDiv('contenido_complementar');
	limpiarDiv('contenido_cobranza');
	limpiarDiv('contenido_documentos140');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function generarLigaIfimaxCliente() {	
	var url='/MidasWeb/complementar/documentos/generarLigaIfimax.action?cotizacion.idToCotizacion='+dwr.util.getValue("idToCotizacion");
	window.open(url,'Fortimax');
}

function guardarDocumentosFortimaxCliente(){
	var url="/MidasWeb/complementar/documentos/guardarDocumentos.action?cotizacion.idToCotizacion="+dwr.util.getValue("idToCotizacion");
	var data;
	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantesClient);
}

function populateDocumentosFaltantesClient(json){
	if(json){
		var docsFaltantes=json.documentosFaltantes;
		if(docsFaltantes!=null){
			parent.mostrarMensajeInformativo('Los siguientes archivos no han sido digitalizados: '+docsFaltantes+', favor de digitalizar',"10");
		}
		else{
			parent.mostrarMensajeInformativo('Todos los documentos han sido guardados exitosamente',"30");					
		}
	}
}

function auditarDocumentosCliente(){
	var url="/MidasWeb/complementar/documentos/matchDocumentosCliente.action?cotizacion.idToCotizacion="+dwr.util.getValue("idToCotizacion");
	var data="";
	jQuery.asyncPostJSON(url,data,populateDocumentosFortimaxCliente);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimaxCliente(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#clienteDocumentosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
			});
		}
	}
}

function importarArchivoPrimaObjetivo(){

	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("documentoPrimaObjetivo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Documento Prima Objetivo");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    		    	var url = verIgualarPrimasAgentePath;
    		    	url = url + '?idToCotizacion='+jQuery("#idToCotizacion").val() + "&idControlArchivo="+idToControlArchivo;
    				parent.redirectVentanaModal("igualarPrimasAgente", url, null);
    			}
    		} // End of onSuccess    		
    	});
        parent.dhxWins.window("documentoPrimaObjetivo").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "4");
    vault.setFormField("idToCotizacion", dwr.util.getValue("idToCotizacion"));
}

function setValoresArchivoPrima(id, nombre){
	jQuery("#controlArchivoId").val(id);
	jQuery("#nombreArchivoPrima").val(nombre);
}


function modificaIVAAgente(){
	var idToCotizacion = dwr.util.getValue("idToCotizacion");
	mostrarMensajeConfirm("'Se afectar\u00e1n los montos de la cotizaci\u00f3n, \u00BFDesea modificar el IVA\u003F'", "20",
			"recalcularCotizadorAgente()","restauraIvaAgente()", null);
}

function restauraIvaAgente() {
	var iva = dwr.util.getValue('porcentajeIva');
	var ivaFrontera = 11;
	var IvaInterior = 16;
	
	if(iva == IvaInterior) {
		dwr.util.setValue('porcentajeIva',ivaFrontera);
	}else{
		dwr.util.setValue('porcentajeIva',IvaInterior);
	}
}

function mostrarContenedorImpresionAgente(tipoImpresion,idCotizacion, fechaValidez){
	
	if(jQuery("#validacionesPendientes").val() === 'true'){
		mostrarMensajeInformativo('La Cotizaci\u00f3n tiene Solicitudes de Autorizaci\u00f3n Pendientes. No es posible imprimirla en este momento.',"30");
		return;
	}
	var url = '/MidasWeb/impresiones/componente/mostrarContenedorImpresion.action?id='+idCotizacion+ "&tipoImpresion="+tipoImpresion;
	if(fechaValidez){
		url = url + "&fechaValidez=" + fechaValidez;
	}
	
	parent.mostrarVentanaModal("mostrarContenedorImpresion", 'Impresiones', 200, 320, 540, 315, url);
}

function recalcularCotizadorAgente2(){
	alert('recalcularCotizadorAgente2');
    var negocio      = dwr.util.getValue('negocio');
    var producto     = dwr.util.getValue('producto');
    var tipoPoliza   = dwr.util.getValue('tipoPoliza');
    var numeroInciso = dwr.util.getValue('incisoCotizacion.id.numeroInciso') ;
    if(negocio ==="" || producto ==="" || tipoPoliza ===""){
        mostrarMensajeInformativo("Favor de Capturar los campos Negocio, Producto y/o TipoPoliza antes de Generar la cotizaci\u00F3n", "20");
        return false;
    }	
	
    var formaDePago = dwr.util.getValue('formaPago');
    var derechos    = dwr.util.getValue('derechos');
    if(formaDePago ==="" || derechos ===""){
      mostrarMensajeInformativo("Favor de Capturar los campos Forma de pago y/o Derechos antes de Generar la cotizaci\u00F3n", "20");
      return false;
    }
   
	if (!fechaMenorQue(jQuery("#fecha").val(), jQuery("#fechados").val())) {
		 mostrarMensajeInformativo("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia", "20");
		 return false;
	}
	if(jQuery("#descuentoGlobal").val() == ""){
		 jQuery('#descuentoGlobal').val('0.0');
	}
	 
	sendRequestJQ(document.cotizacionForm,'/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/generaCotizacionAgente2.action','contenido', 'unblockPage();');
}

