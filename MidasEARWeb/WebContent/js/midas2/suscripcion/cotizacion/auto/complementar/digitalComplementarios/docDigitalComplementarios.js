/**
 * 
 */
var docCotizacionGrid;
var comentariosDisponiblesGrid;

function agregarDocCotizacion(){
	if(dhxWins != null) 
		dhxWins.unload();
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("adjuntarDocumentoDigitalComplementario", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar archivos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        //sendRequest(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/digitalComplementarios/verDigitalComplementarios.action?idToCotizacion='+dwr.util.getValue("idToCotizacion"),'contenido_documentosDigitales',null);
        mostrarDocCotizacion();
        parent.dhxWins.window("adjuntarDocumentoDigitalComplementario").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "4");
    vault.setFormField("idToCotizacion", dwr.util.getValue("idToCotizacion"));
}

function mostrarDocCotizacion(){
	document.getElementById("docCotizacionGrid").innerHTML = '';
	docCotizacionGrid = new dhtmlXGridObject('docCotizacionGrid');
	docCotizacionGrid.load(obtenerDocCotizacionPath + "?idToCotizacion=" + dwr.util.getValue("idToCotizacion"));
}

function eliminarDocCotizacion(idDocumentoDigitalCotizacion){
	var answer = confirm('\u00BFDesea borrar este Documento Digital Complementario?');
	if (answer){
		sendRequestJQ(null, eliminarDocDigitalCotizacionPath + "?idDocumentoDigitalCotizacion="+idDocumentoDigitalCotizacion,null,'mostrarDocCotizacion();');
	}		
}

function descargarDocCotizacion(idToControlArchivo){
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function obtenerComentarios(){
	document.getElementById('docSolicitudGrid').innerHTML = '';
	comentariosDisponiblesGrid = new dhtmlXGridObject('docSolicitudGrid');
	comentariosDisponiblesGrid.load(obtenerComentariosPath + "?idSolicitud=" + dwr.util.getValue("idSolicitud"));
}

function descargarDocumentoComentarios(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function adjuntarDocumentos(idComentario,idSolicitud) {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("AdjuntarDocumentoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;        			
        			var path= "/MidasWeb/suscripcion/solicitud/comentarios/guardarComentarios.action?comentario.id="+idComentario 
        			+ "&comentario.documentoDigitalSolicitudDTO.controlArchivo.idToControlArchivo=" + idToControlArchivo
        			+ "&comentario.documentoDigitalSolicitudDTO.controlArchivo.nombreArchivoOriginal=" + nombreArchivo
        			+ "&comentario.solicitudDTO.idToSolicitud=" + idSolicitud;	
        			sendRequestJQ(null,path,targetWorkArea,null);
        			mostrarMensajeInformativo('Documento guardado exitosamente',"30");
    			}
    			else{
    				mostrarMensajeInformativo('Error al adjuntar el archivo intentelo nuevamente',"10");
    			}
    		} // End of onSuccess    		
    	});
        parent.dhxWins.window("AdjuntarDocumentoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "18");
    vault.setFormField("idSolicitud",idSolicitud);
}

function eliminarComentarios() {	
	var path= "/MidasWeb/suscripcion/solicitud/comentarios/eliminarComentarios.action";	
	var idComentario = getIdFromGrid(comentariosDisponiblesGrid, 0);	
	var idSolicitud = getIdFromGrid(comentariosDisponiblesGrid, 1);
		sendRequestJQ(null, path + "?comentario.solicitudDTO.idToSolicitud=" + idSolicitud + "&comentario.id=" + idComentario, null,'obtenerComentarios()');	
}

function guardarAclaraciones(){
	sendRequestJQ(jQuery("#docDigitalComplementariosForm"), guardarAclaracionesPath,targetWorkArea,'mensajeAclaracion();');
}

function mensajeAclaracion(){

}