/**
 * 
 */
function validarMaxNumeroPisos(id, after) {
                var obj = jQuery(document.getElementById(id));
                var numPisos = obj.val();             

                if (numPisos != null && numPisos != "") {
                               if (isInt(numPisos)) {
                                               CotizacionFacade.getMaxNumeroPisos(function(maxNumPisos) {
                                                               if (numPisos > maxNumPisos) {
                                                                              obj.val("");
                                                                              mostrarMensajeYEnfocar("El numero de pisos excede el maximo permitido. El maximo permitido es: " + maxNumPisos, id);
                                                               }
                                                               
                                                               eval(after);
                                               })
                               } else {
                                               obj.val("");
                                               mostrarMensajeYEnfocar("Debe de escribir un numero entero.", id);
                               }
                               
                }
                eval(after);
}

function validarPermitidoCotizarCasa(id, after) {
                var valor = dwr.util.getValue(id);
                
                if (valor != null && valor != "") {
                               CotizacionFacade.isPermitidoCotizarCasa(id, valor, function(isPermitido) {
                                               if (!isPermitido) {
                                                               dwr.util.setValue(id, "");
                                                               mostrarMensajeYEnfocar("Enviar a Seguros Afirme a cotizar, el inmueble no cumple con politicas de aceptacion para este producto.", id);
                                               }
                                               
                                               eval(after);
                               })
                }
                eval(after);
}

function validarPermitidoCotizarYHorizontal(id, dependenciaId) {
                var valor = dwr.util.getValue(id);
                
                if (valor != null && valor != "") {
                               CotizacionFacade.isPermitidoCotizarCasa(id, valor, function(isPermitido) {
                                               if (!isPermitido) {
                                                               dwr.util.setValue(id, "");
                                                               mostrarMensajeYEnfocar("Enviar a Seguros Afirme a cotizar, el inmueble no cumple con politicas de aceptacion para este producto", id);
                                               }
                                               validarHorizontal(id, dependenciaId); //Actualiza dependiendo de la validacion el input dependiente.
                               })
                } else {
                               validarHorizontal(id, dependenciaId);
                }
}

function validarHorizontal(id, dependenciaId) {
                var valor = dwr.util.getValue(id);
                var dependenciaObj = document.getElementById(dependenciaId);
                
                if (valor != "") {
                	ConfiguracionDatoIncisoService.isHorizontal(id, valor, function(isHorizontal) {
                                               if (isHorizontal) {
                                                               dwr.util.setValue(dependenciaId, "");
                                                               hideInput(dependenciaObj);
                                               } else {
                                                               showInput(dependenciaObj);
                                               }
                               })
                } else {
                               dwr.util.setValue(dependenciaId, "");
                               hideInput(dependenciaObj);
                }
}

function validaPisoEncuentra(){
                var idTipoVivienda = 'cotizacion.datosRiesgo_\'1x1x6011x0x100\'_';
                var idPisoEncuentra = 'cotizacion.datosRiesgo_\'1x1x6011x0x120\'_';
                var divPisoEncuentra = 'wwgrp_cotizacion.datosRiesgo_\'1x1x6011x0x120\'_';
                var valor = dwr.util.getValue(idTipoVivienda);
                dwr.util.setValue(idPisoEncuentra, "0");
                if( valor == "30"){
                               document.getElementById(divPisoEncuentra).style.display="block";
                }
                else{
                               document.getElementById(divPisoEncuentra).style.display="none";
                }
}
