/**
 * 
 */
var anexosGrid;
var anexosProcessor;

function obtenerAnexos(){
	anexosGrid = null;
	anexosProcessor = null;
	
	document.getElementById("anexosGrid").innerHTML = '';
	anexosGrid = new dhtmlXGridObject('anexosGrid');
	anexosGrid.attachEvent("onEditCell",function(){
		return true;
	});	
	anexosGrid.attachEvent("onRowCreated",function(rowId, rowObj) {
		var obligatoriedad=anexosGrid.cellById(rowId, 1).getValue();
		var tipo=anexosGrid.cellById(rowId, 3).getValue();
			
		if(obligatoriedad=="3" && (tipo=="TP" || tipo=="P")){
			anexosGrid.cellById(rowId, 2).setDisabled(true);
		}
	});
	anexosGrid.load(obtenerAnexosPath + "?idToCotizacion=" + dwr.util.getValue("idToCotizacion"),function(){$_disabledAnexos()});
	
	//Data processor
	anexosProcessor = new dataProcessor(modificarAnexosPath);
	anexosProcessor.enableDataNames(true);
	anexosProcessor.setTransactionMode("POST");
	anexosProcessor.setUpdateMode("off");
	anexosProcessor.attachEvent("onAfterUpdateFinish", function(){
		if(anexosProcessor.getSyncState()){
			obtenerAnexos();
		}
	});
	anexosProcessor.init(anexosGrid);	
	
}
/**
 * Deshabilita los anexos
 * obligatorios
 */
function $_disabledAnexos(){
	jQuery("td:contains('-3')").each(
			function(index, e) {
				anexosGrid.cellById(jQuery(e).text().substring(0, 1), 3)
						.setDisabled(true);
			});
}

function guardarAnexoCotizacion(){
	if(!anexosProcessor.getSyncState()){	
		anexosProcessor.sendData();		
	}
}

function descargarAnexo(idToControlArchivo){
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}