var listadoCobIncisosGrid;
var paginadoParamsCobIncPath = null;

function verCobIncisoContratante(idToCotizacion) {
	limpiarDivsCobInciso();
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	
	var path= '/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/mostrarContratante.action?idToCotizacion='+idToCotizacion+soloConsulta;
	sendRequestJQ(null, path,'contenido_cobIncisoContratante',"ocultarIndicadorCarga('indicadorContenedor')");
}

function verCobIncisoAsegurador(idToCotizacion) {
	limpiarDivsCobInciso();
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	
	var path= '/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/mostrarAsegurado.action?idToCotizacion='+idToCotizacion+soloConsulta;
	sendRequestJQ(null, path,'contenido_cobIncisoAsegurado',"ocultarIndicadorCarga('indicadorContenedor')");
}

function verCobIncisoAgregarNuevo(idToCotizacion) {
	limpiarDivsCobInciso();
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	
	var path= '/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/mostrarAgregarNuevo.action?idToCotizacion='+idToCotizacion+soloConsulta;
	sendRequestJQ(null, path,'contenido_cobIncisoAgregarNuevo',"ocultarIndicadorCarga('indicadorContenedor')");
}

function limpiarDivsCobInciso() {
	limpiarDivCobInciso('contenido_cobIncisoContratante');
	limpiarDivCobInciso('contenido_cobIncisoAsegurado');
	limpiarDivCobInciso('contenido_cobIncisoAgregarNuevo');
}

function limpiarDivCobInciso(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function onchangeMediosInciso() {
	if (jQuery('#medioPagoDTOs').val() == ""
			|| jQuery('#medioPagoDTOs').val() == 15) {
		jQuery("div[id=conductos_div]").hide('slow');
	} else {
		if(obtieneMediosDeCobroPorPesonaInciso()){
			jQuery("div[id=conductos_div]").show('slow');
		}
	}
}

function obtieneMediosDeCobroPorPesonaInciso() {
	var idToPersonaContratante = jQuery("#idToPersonaContratante").val();
	if(!validarExisteContratanteInciso(idToPersonaContratante))
	{
		return false;		
	}	
	if (jQuery('#medioPagoDTOs').val() != ""
			|| jQuery('#medioPagoDTOs').val() == null) {
			listadoService.getConductosCobro(idToPersonaContratante, jQuery(
			'#medioPagoDTOs').val(), function(data) {
			addOptions(document.getElementById('conductos'), data);
	});
	}
	return true;
}

function actualizaConductosCobro(fila) {
	var idToPersonaContratante = jQuery("#idClienteCob_" + fila).val();
	var idMedioPago = jQuery("#idMedioPago_" + fila).val();	
	if (idMedioPago != "" || idMedioPago == null || idToPersonaContratante == "" || idToPersonaContratante == null) {
			listadoService.getConductosCobro(idToPersonaContratante, idMedioPago, function(data) {
				addOptions(document.getElementById('idConductoCobro_'+fila), data);
			});
	}
	return true;
}

function validarExisteContratanteInciso(idToPersonaContratante)
{
	if(idToPersonaContratante == "" || idToPersonaContratante == null){
		mostrarMensajeInformativo("Se necesita un contratante, por favor dirigase a la pestaña Complem. Emisión y seleccione un contratante antes de continuar. ", "10");
		return false;
	}else
	{
		return true;
	}
}

function agregarDatosCobranza(){
	
}

function aplicarCob(){
	limpiarIdCobIncisos();
	var idIncisos = cargaIdCobIncisos();
	var idMedioPago = jQuery('#medioPagoDTOs').val();
	var idConducto = jQuery('#conductos').val();
	var conductoTxt = validaAgregarConductosCobro();
	if(idMedioPago === "" || idMedioPago === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione el Medio de Pago a aplicar.");
	}else if (idMedioPago != 15 && (conductoTxt === "" || conductoTxt === undefined)){
		parent.mostrarVentanaMensaje('10', "Seleccione el Conducto de Cobro a aplicar.");
	}else if(idIncisos === "" || idIncisos === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos un Inciso a aplicar.");
	}else{
		var form = jQuery('#cobIncisoForm')[0];
		creaIdCobIncisos(idIncisos);
		var validarPolizaPath = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/aplicarCobranza.action";
		form.setAttribute("action", "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/aplicarCobranza.action");		
		sendRequestJQTarifa(form, validarPolizaPath, 'contenido_cobIncisoContratante', null);
	}
}

function cargaIdCobIncisos(){
	var idPolizas = "";
	if(listadoCobIncisosGrid === undefined){
		return idPolizas;
	}
	var num = 0;
	var idCheck = listadoCobIncisosGrid.getCheckedRows(0);
	if(idCheck != ""){
		return idCheck;
	}
	
	return idPolizas;
}

function creaIdCobIncisos(idIncisos){
	jQuery( ".idIncisosCob" ).remove();
	if(idIncisos != null && idIncisos != ""){
		var ids = idIncisos.split(",");
		var num = 0;
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#cobIncisoForm').append('<input type="hidden" name="incisos['+ num+ '].id.numeroInciso" value="' + ids[i] + '" class="idIncisosCob" />');
	   	 	num = num + 1;		
		}
	}
}

function limpiarIdCobIncisos(){
	jQuery('#cobIncisoForm').remove('.idIncisosCob');
}

function aplicarCobTodos(){
	limpiarIdCobIncisos();
	var idMedioPago = jQuery('#medioPagoDTOs').val();
	var idConducto = jQuery('#conductos').val();
	var conductoTxt = validaAgregarConductosCobro();
	if(idMedioPago === "" || idMedioPago === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione el Medio de Pago a aplicar.");
	}else if (idMedioPago != 15 && (conductoTxt === "" || conductoTxt === undefined)){
		parent.mostrarVentanaMensaje('10', "Seleccione el Conducto de Cobro a aplicar.");
	}else{
		var form = jQuery('#cobIncisoForm')[0];
		var validarPolizaPath = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/aplicarCobranzaTodos.action";
		form.setAttribute("action", validarPolizaPath);		
		sendRequestJQTarifa(form, validarPolizaPath, 'contenido_cobIncisoContratante', null);
	}	
}

function guardarMediosPago(){
	var error = false;
	jQuery("errores").html("");	
	var result = validateAllCobranzaInciso();
	if (result == 0) {
		var cobAseguradoPath = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/aplicarCobranzaAsegurado.action";
		sendRequestJQ(jQuery(document.cobIncisoAsegForm), cobAseguradoPath,
				null, 'validaGuardarModificarM1()');
	} else {
		mostrarMensajeInformativo("Por favor selecciona un Medio de Pago valido para el Inciso ("+result+")",
				"10", null, null);
	}
}

function validateAllCobranzaInciso(){
		var result = 0
		var cont = true;
		var conta = 1;
		var total = jQuery(".validaCobInciso").length;
		do{
			if(conta > total){
				cont = false;
			}else{
			var idMedioPago = jQuery('#idMedioPago_'+conta).val();
			
			var conductoTxt = dwr.util.getText('idConductoCobro_'+conta);
			if(conductoTxt != 'Agregar nuevo...'){		
				conductoTxt = "OK"; 
			}else{
				conductoTxt = "";
			}
			if(idMedioPago === "" || idMedioPago === undefined){
				result = conta;
				cont = false;
			}else if (idMedioPago != 15 && (conductoTxt === "" || conductoTxt === undefined)){
				result = conta;
				cont = false;
			}
			conta = conta + 1;
			}
		}while(cont);
		
		return result;
}



function buscarIncisosCobPaginado(page, nuevoFiltro) {
	limpiarIdCobIncisos();
	var idToCotizacion = jQuery('#idToCotizacion').val();
	var posPath = '&posActual=' + page + '&funcionPaginar='+ 'buscarIncisosCobPaginado' + '&divGridPaginar=' + 'listadoIncisos'
	+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&esCobInciso=1';
	if(nuevoFiltro){
		paginadoParamsCobIncPath = jQuery(document.cobBuscarIncisoForm).serialize();
	}else{
		posPath = '&posActual=' + page + '&'+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosRapidaPath + "?" + paginadoParamsCobIncPath + posPath,
			'gridListadoDeIncisos', 'mostrarListadoIncisos('+idToCotizacion+')');
}




function mostrarListadoIncisos(idToCotizacion){
	document.getElementById("listadoIncisos").innerHTML = '';
	listadoCobIncisosGrid = new dhtmlXGridObject('listadoIncisos');
	mostrarIndicadorCarga('indicadorCobIncisos');
	listadoCobIncisosGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicadorCobIncisos');});
	listadoCobIncisosGrid.attachEvent("onXLE", function(grid){CantidadRegistrosGrid();});
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() 
		+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&esCobInciso=1';
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}
	
	if(paginadoParamsCobIncPath != null){
		listadoCobIncisosGrid.load(busquedaRapidaPath +'?'+paginadoParamsCobIncPath +"&" + posPath  + soloConsulta);
	}else{
		listadoCobIncisosGrid.load(busquedaRapidaPath+ '?'+ posPath + soloConsulta);
	}
	
}




function buscarIncisosCobPaginadoAseg(page, nuevoFiltro) {
	limpiarIdCobIncisos();
	var idToCotizacion = jQuery('#idToCotizacion').val();
	var posPath = '&posActual=' + page + '&funcionPaginar='+ 'buscarIncisosCobPaginado' + '&divGridPaginar=' + 'listadoIncisos'
	+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&esCobInciso=1';
	if(nuevoFiltro){
		paginadoParamsCobIncPath = jQuery(document.cobBuscarIncisoForm).serialize();
	}else{
		posPath = '&posActual=' + page + '&'+ jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQTarifa(null, incisosPaginadosRapidaPath + "?" + paginadoParamsCobIncPath + posPath,
			'gridListadoDeIncisos', 'mostrarListadoIncisosAseg('+idToCotizacion+')');
}




function mostrarListadoIncisosAseg(idToCotizacion){
	document.getElementById("listadoIncisos").innerHTML = '';
	 jQuery('#listadoIncisos').css('height', '');
	 jQuery('#listadoIncisos').css('overflow', 'auto'); 
	mostrarIndicadorCarga('indicadorCobIncisos');
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() 
		+ '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&esCobInciso=1';
	
	var soloConsulta = "";
	if(jQuery("#soloConsulta").val() != "" && jQuery("#soloConsulta").val() != undefined){
		soloConsulta = "&soloConsulta="+ jQuery("#soloConsulta").val();
	}	
	if(paginadoParamsCobIncPath != null){
		sendRequestJQTarifa(null, buscarIncisosAseg + "?" + paginadoParamsCobIncPath +"&" + posPath  + soloConsulta,
				'listadoIncisos', "ocultarIndicadorCarga('indicadorCobIncisos')");
	}else{
		sendRequestJQTarifa(null, buscarIncisosAseg + "?" + posPath  + soloConsulta,'listadoIncisos', "ocultarIndicadorCarga('indicadorCobIncisos')");
	}
	
}

function cargarComboPaquetesCob(idToCotizacion,idNegocioSeccion) {
	removeAllOptionsAndSetHeader(document.getElementById('incisoCotizacion.incisoAutoCot.negocioPaqueteId'), headerValue,"Cargando...");
	if (idNegocioSeccion > 0 || idNegocioSeccion != "") {
		listadoService.getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(idToCotizacion,idNegocioSeccion, function(
				data) {
			addOptions(document.getElementById('incisoCotizacion.incisoAutoCot.negocioPaqueteId'), data);
		});
	}else{
		removeAllOptionsAndSetHeader(document.getElementById('incisoCotizacion.incisoAutoCot.negocioPaqueteId'), headerValue,"Seleccione...");
	}
}

function onchangeTitularIncisoIgual() {
	var infoTitularUrl = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/complementarInfoTitularInciso.action";
	if (jQuery('#titularIgual').val() == 'true') {
		var idConducto = validaAgregarConductosCobro();
		var url = infoTitularUrl + "?idToCotizacion="
		+ dwr.util.getValue('idToCotizacion') + "&datosIguales="
		+ jQuery('#titularIgual').val() + idConducto + "&idMedioPago="
		+ jQuery('#medioPagoDTOs').val() + "&idToPersonaContratante=" + jQuery("#idToPersonaContratante").val();		
		sendRequestJQ(null, url, "second", null);
	}else if(jQuery('#titularIgual').val() == 'false' || jQuery('#titularIgual').val() == ''){
		sendRequestJQ(jQuery('#cobranzaForm'), infoTitularUrl, "second", null);
	}
}

function validaAgregarConductosCobro(){
	var option = dwr.util.getText('conductos');
	var idConducto = "";
	if(option != 'Agregar nuevo...'){		
		idConducto = "&idConductoCobro=" + jQuery('#conductos').val(); 
	}
	return idConducto;
}

function onchangeConductosInciso() {
	var infoCuentasUrl = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/complementarInfoCuentaInciso.action";
	if (jQuery('#conductos').val() != "" && dwr.util.getText('conductos') != 'Agregar nuevo...') {
		// Cargar un conducto
		sendRequestJQ(jQuery('#cobranzaForm'), infoCuentasUrl, "infoCuentasCobranza", null);
		jQuery("div[id=checkDatos]").show('slow');
	} else if (jQuery('#conductos').val() != '') {
		// Agregar nuevo conducto
		jQuery("div[id=checkDatos]").show('slow');
		var url = infoCuentasUrl + "?idToCotizacion="
		+ dwr.util.getValue('idToCotizacion') + "&idMedioPago="
		+ jQuery('#medioPagoDTOs').val() + "&idToPersonaContratante=" + jQuery("#idToPersonaContratante").val();		
		sendRequestJQ(null, url,"infoCuentasCobranza","cleanInfoCuentaIncisoForm()");
	}
}

function cleanTitularForm() {
	jQuery('#second').find('input').val('');
	jQuery('#second').find('select').val('');
}
/**
 * Limpia los input dentro del div infoCuenta
 */
function cleanInfoCuentaIncisoForm() {
	jQuery('#infoCuentasCobranza').find('input[type!=hidden]').val('');
	cleanPromocionesInciso();
}

function cleanPromocionesInciso(){
	var promos = document.getElementById('promociones');
	if(promos != null){
		promos.options.length = 1;
	}
}

function guardarCobranzaInciso() {
	var idToPersonaContratante = jQuery("#idToPersonaContratante").val();
	if(jQuery('#medioPagoDTOs').val() != 15 && !validarExisteContratanteInciso(idToPersonaContratante))
	{
		return false;
	}
	var guardarCobranzaUrl = "/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/guardarCobranzaInciso.action";
	var url = guardarCobranzaUrl;
	if (jQuery('#medioPagoDTOs').val() == 15) {
		sendRequestJQ(jQuery('#cobranzaForm'), url, 'contenido_cobIncisoAgregarNuevo', null);
	} else if (jQuery('#medioPagoDTOs').val() != '' && validateAll(false, 'save')) {
		sendRequestJQ(jQuery('#cobranzaForm'), url, 'contenido_cobIncisoAgregarNuevo', null);
	} else {
		mostrarMensajeInformativo(checkForm(), "10",null,null);
	}
}

function muestraMensajeInformativoCobranzaInciso(){
	if (parent.mostrarMensajeInformativo != null) {
		parent.mostrarMensajeInformativo(jQuery(
				'#mensajeCobInc').html(), jQuery('#tipoMensajeCobInc').html(), jQuery('#nextFunctionCobInc').html());
	} else {
		mostrarMensajeInformativo(jQuery('#mensajeCobInc')
				.html(), jQuery('#tipoMensajeCobInc').html(),
				jQuery('#nextFunctionCobInc').html());
	}
	jQuery('#mensajeCobInc').html("");
	jQuery('#tipoMensajeCobInc').html("");
	jQuery('#nextFunctionCobInc').html("");
}
