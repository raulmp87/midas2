/**
 * 
 */
function verComplementarEmision() {
	limpiarDivsGeneral();
	var cotizacionId = dwr.util.getValue('cotizacionId');
	var soloConsulta = jQuery("#soloConsulta").val();
	//var path= '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/verComplementarEmision.action?idToCotizacion='+cotizacionId;
	var path= '/MidasWeb/suscripcion/cotizacion/auto/validarDatosComplementarios.action?idToCotizacion='+cotizacionId+"&soloConsulta="+soloConsulta;
	sendRequestJQ(null, path,
			'contenido_detalle',
			null);
}

function desplegarTextosAdicionales() {
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/textoadicional/listar.action?cotizacion.idToCotizacion='+dwr.util.getValue('cotizacionId')+"&soloConsulta="+soloConsulta,
			'contenido_textosAdicionales',
			null);
}

function desplegarAnexos() {
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	
	var idToCotizacion = dwr.util.getValue('cotizacionId');
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/anexos/verAnexos.action?idToCotizacion='+idToCotizacion+"&soloConsulta="+soloConsulta,
			'contenido_documentosAnexos',
			null);
}

//idCliente solo para pruebas
function desplegarDocumentos140() {
	limpiarDivsGeneral();
	var path='/MidasWeb/complementar/documentos/mostrarDocumentos.action?cotizacion.idToCotizacion='+dwr.util.getValue('cotizacionId');
	sendRequestJQ(null, path,
			'contenido_documentos140',
			null);
}

function desplegarCobranza(){
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cabranza/mostrar.action?idToCotizacion='+dwr.util.getValue('cotizacionId')+"&soloConsulta="+soloConsulta,'contenido_cobranza',null);
}

function desplegarCobranzaInciso(){
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/cobranzaInciso/mostrar.action?idToCotizacion='+
			dwr.util.getValue('cotizacionId')+"&soloConsulta="+soloConsulta,'contenido_cobranzaInciso',null);
}

function desplegarDigitalComplementarios(idToCotizacion) {
	limpiarDivsGeneral();
	var soloConsulta = jQuery("#soloConsulta").val();
	var idToCotizacion = dwr.util.getValue('cotizacionId');
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/digitalComplementarios/verDigitalComplementarios.action?idToCotizacion='+idToCotizacion+"&soloConsulta="+soloConsulta,
			'contenido_documentosDigitales',
			null);
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_detalle');
	limpiarDiv('contenido_complementarEmision');
	limpiarDiv('contenido_documentosDigitales');
	limpiarDiv('contenido_documentosAnexos');
	limpiarDiv('contenido_textosAdicionales');
	limpiarDiv('contenido_cobranza');
	limpiarDiv('contenido_cobranzaInciso');
	limpiarDiv('contenido_documentos140');
	limpiarDiv('contenido_condicionesEspeciales');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function generarLigaIfimaxCliente() {	
	var url='/MidasWeb/complementar/documentos/generarLigaIfimax.action?cotizacion.idToCotizacion='+dwr.util.getValue("cotizacionId");
	window.open(url,'Fortimax');
}

function guardarDocumentosFortimaxCliente(){
	var url="/MidasWeb/complementar/documentos/guardarDocumentos.action?cotizacion.idToCotizacion="+dwr.util.getValue("cotizacionId");
	var data;
	jQuery.asyncPostJSON(url,data,populateDocumentosFaltantesClient);
}

function populateDocumentosFaltantesClient(json){
	if(json){
		var docsFaltantes=json.documentosFaltantes;
		if(docsFaltantes!=null){
			parent.mostrarMensajeInformativo('Los siguientes archivos no han sido digitalizados: '+docsFaltantes+', favor de digitalizar',"10");
		}
		else{
			parent.mostrarMensajeInformativo('Todos los documentos han sido guardados exitosamente',"30");					
		}
	}
}

function auditarDocumentosCliente(){
	var url="/MidasWeb/complementar/documentos/matchDocumentosCliente.action?cotizacion.idToCotizacion="+dwr.util.getValue("cotizacionId");
	var data="";
	jQuery.asyncPostJSON(url,data,populateDocumentosFortimaxCliente);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populateDocumentosFortimaxCliente(json){
	if(json){
		var lista=json.listaDocumentosFortimax;
		if(!jQuery.isEmptyArray(lista)){
			jQuery("#clienteDocumentosForm input[type='checkbox']").each(function(i,elem){
				if(lista[i].existeDocumento == 1){
					jQuery(this).attr('checked',true);
				}
				else{
					jQuery(this).attr('checked',false);
				}
				
			});
		}
	}
}


function complementarEmision(idPoliza,idTipoEndoso,accionEndoso,fechaIniVigenciaEndoso, idCotizacionContinuity)
{
	sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/mostrarDetalle.action?tipoEndoso=' + idTipoEndoso +'&polizaId='+idPoliza+'&fechaIniVigenciaEndoso='+fechaIniVigenciaEndoso+'&accionEndoso='+accionEndoso + '&cotizacion.continuity.id=' + idCotizacionContinuity + '&namespaceOrigen=' + currentNamespace + '&actionNameOrigen=' + currentMainAction ,targetWorkArea,null);
}

function verCondicionesEspeciales(tipoLlamada){
	limpiarDivsGeneral();
	var idToCotizacion = dwr.util.getValue('cotizacionId');
	var numeroInciso = dwr.util.getValue('numeroInciso');
	var url = "/MidasWeb/componente/condicionespecial/mostrarContenedor.action?idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso + "&tipoLlamada=" + tipoLlamada;
	sendRequestJQ(null, url, 'contenido_condicionesEspeciales',	null);
}

//function mostrarMsjValidacionDatosComplementarios() {
//	var path= '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/mostrarValidacionDatosComplementarios.action?idToCotizacion='+dwr.util.getValue("idToCotizacion");
//	var targetWorkArea ="datosComplementariosDiv";
//	sendRequestJQ(null,path,targetWorkArea,null);	
//}