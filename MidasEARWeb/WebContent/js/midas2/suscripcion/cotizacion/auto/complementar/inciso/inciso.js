function guardarInciso(){	
	var form = jQuery('#complementarIncisoForm')[0];
//	parent.submitVentanaModal('ventanaVehiculo', form);
//	parent.redirectVentanaModal('ventanaVehiculo','/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardar.action',form);	
	var nextFunction = "&nextFunction=validaGuardado()";
	var url = '/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardar.action' + '?' + jQuery(form).serialize() + nextFunction;		
	//alert(url);
	parent.redirectVentanaModal('ventanaVehiculo', url, null);	
}

function validaGuardado(){
	if(jQuery("#guardadoExitoso").val() == "true"){
		var idCotizacion = jQuery('#cotizacionId').val();
		cerrarVentanaModal('ventanaVehiculo');	
		verComplementarCotizacion(idCotizacion); 			
	}	
}


function validaCiente(){
		if(jQuery("#idCliente").val() > 0){
			jQuery("input[name$='copiarDatos']").removeAttr("disabled");
		}else{
			jQuery("input[name$='copiarDatos']").attr("disabled",true);		
		}
		if(jQuery("input[name$='copiarDatos']:checked").val() == 2){
			jQuery("#nombre").attr("disabled",true);
			jQuery("#paterno").attr("disabled",true);
			jQuery("#materno").attr("disabled",true);
		}
}

function copiarDatosUsuario(sel){
	var numeroSecuencia = jQuery("#numeroSecuencia").val();
	if (claveTipoPersona == 2 && sel == 2) {
		jQuery("#copiarDatos1").attr('checked', true);
		parent.mostrarMensajeInformativo("No se puede copiar una persona moral", "10",null, null);
		return false;
	}
		if(sel == 1){
		jQuery("#nombre").removeAttr("disabled");
		jQuery("#paterno").removeAttr("disabled");
		jQuery("#materno").removeAttr("disabled");
	}else {
		parent.recargarVentanaVehiculo(jQuery("#cotizacionId").val, jQuery("#incisoId").val(),sel,numeroSecuencia);
	}
}



function remplazarCaracteresNumSerie(obj){
	var pattN = /[�]/gi;
	var valN = 'N';
	var pattOQU = /[oq]/gi;
	var valOQU = '0';
	var pattI = /[i]/gi;
	var valI = '1';	
	var space = /[ ]/gi;
	var empty = '';
	var str = obj.value;
	str = str.replace(pattN, valN);
	str = str.replace(pattOQU, valOQU);
	str = str.replace(pattI, valI);
	str = str.replace(space, empty);
	obj.value = str.toUpperCase();
}