/**
 * Tabla de resumen de totales
 * TODO El iva no se permitira modificar
 */
function obtenerResumenTotalesCotizacionGridAgenteConsulta(){
	document.getElementById("resumenTotalesCotizacionGridConsulta").innerHTML = '';
	mostrarIndicadorCarga("cargaResumenTotalesConsulta"); 
	sendRequestJQ(null,"/MidasWeb/suscripcion/cotizacion/auto/mostrarResumenTotalesCotizacionAgente.action?soloConsulta=1&cotizacion.idToCotizacion="+jQuery("#idToCotizacion").val()+"&esComplementar=true", "resumenTotalesCotizacionGridConsulta",null);
}

/**
 * Muestra los datos complementarios del inciso
 */
function mostrarDivComplementarInciso() {
		try{					
			var url = datosVehiculoPath+"?cotizacionId="+jQuery("#idToCotizacion").val()
			+"&soloConsulta=" + dwr.util.getValue("soloConsulta") + "&incisoId=1";		
			document.getElementById("divComplementarInciso").innerHTML = '';
			mostrarIndicadorCarga("divCargaComplementarInciso"); 
			sendRequestJQ(null,url, "divComplementarInciso","ocultarIndicadorCarga('divCargaComplementarInciso'); ");
			unblockPage();			
		}catch(e){
			alert(e);
		}
}

/**
 * Utilizada, cuando se selecciona el cliente de la busqueda
 * @param idCliente
 */
function seleccionarCliente(idCliente){	
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmisionAgente/seleccionarContratante.action?idToCotizacion='+jQuery("#idToCotizacion").val()+'&idCliente='+idCliente,
			'contenido', null);	
}

/**
 * Envia al catalogo de clientes para complementar la informacion del contratante
 * @param idCliente
 * @param idToCotizacion
 */
function complementarContratante(idCliente, idToCotizacion){
	var path = '/MidasWeb/catalogoCliente/loadById.action?tipoAccion=1&divCarga=contenido&cliente.idCliente=' + idCliente;
	path += '&tipoRegreso=4&idToCotizacion='+idToCotizacion;
	if(idCliente != null && idCliente != ""){
		sendRequestJQ(null,path,targetWorkArea,null);
	}
}

/**
 * Modal de busqueda de clientes asociados. 
 */
function mostrarClientesAsociados(){
	var url = getClientesAsociadosURL + "?idToNegocio=" + dwr.util.getValue("idToNegocio");
	mostrarVentanaModal("winClientesAsociados", "Consulta de Clientes", 0,0,700,400, url);	
}

/**
 * Utilizada al seleccionar un cliente asociado 
 */
function cargaClienteAsociado(idCliente,nombreCliente){
	dwr.util.setValue("cotizacionDTO.idToPersonaContratante",idCliente);
	dwr.util.setValue("cliente.nombreCliente",nombreCliente);
	cerrarVentanaModal("winClientesAsociados");
	seleccionarCliente(idCliente);	
}

/**
 * Emitir Cotizacion
 */
function emitirCotizacion() {
	var idToCotizacion = dwr.util.getValue("idToCotizacion");
	var idToPersonaContratante = dwr.util.getValue("cotizacionDTO_idToPersonaContratante");
	
	var mensaje = dwr.util.getValue("mensajeDTO.mensaje");
	var visible = dwr.util.getValue("mensajeDTO.visible");
	var path= '/MidasWeb/suscripcion/emision/emitirAgente.action?idToCotizacion='+idToCotizacion+'&mensajeDTO.mensaje='+mensaje+'&mensajeDTO.visible='+visible;
	var targetWorkArea ="contenido";
	var urlPDF='/MidasWeb/suscripcion/emision/getFormatoEntrevista.action?cotizacion.idToPersonaContratante='+idToPersonaContratante+'&idToCotizacion='+idToCotizacion;
	sendRequestJQ(null,path,targetWorkArea,null);
}

/**
 * Despues de guardar la informacion del inciso, recarga el contenido del tab
 */
function loadDivComplementar(){	
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmisionAgente/verComplementarEmision.action?idToCotizacion='+jQuery("#idToCotizacion").val(),
			'detalle', null);		
}