/**
 * 
 */
var DEFAULT_MENSAJES_ID = "mensajes";
var DEFAULT_SCROLL_TO_BOTTOM = true;
var MENSAJE_ERROR = 1;
var MENSAJE_ADVERTENCIA = 2;

function addSelectHeader(selectControl) {
                dwr.util.addOptions(jQuery(selectControl).selector, [{id: '-1', value:"Seleccione ..." }], 
                                               "id", "value");
}

/**
* Por mientras que se arreglar el addSelectHeader para que funcione con jquery. Este metodo si utiliza un objeto jquery.
* @param selectControl
* @return
*/
function addDefaultHeader(selectControl) {
                selectControl.append(jQuery("<option></option>").attr("value",DEFAULT_HEADER_KEY).text(DEFAULT_HEADER_VALUE)); 
}

function addOptions(target, map) {
                dwr.util.removeAllOptions(jQuery(target).selector);
                addSelectHeader(target);
                dwr.util.addOptions(jQuery(target).selector, map);
}

function validarDecimal(origenId, enteros, decimales, after) {
                var mensaje = validarDecimalMensaje(origenId, enteros, decimales);
                if (mensaje != "") {
                               mostrarMensajeYEnfocar(mensaje, origenId);
                }
                
                eval(after)
}

function mostrarMensaje(mensaje) {
                alert(mensaje);
}

function mostrarMensajeYEnfocar(mensaje, id) {
                var obj = jQuery(document.getElementById(id));
                mostrarMensaje(mensaje);
                obj.focus();
}

/**
* @param origenId
* @param enteros
* @param decimales
* @return
*/
function validarDecimalMensaje(origenId, enteros, decimales) {
                var strValidacion = "";
                var obj = document.getElementById(origenId);
                var fieldValue = obj.value;
                if (isNaN(fieldValue)) {
                               strValidacion = "Debe introducir un valor numerico.";
                }
                else {
                               if (fieldValue.indexOf('.') == -1)
                                               fieldValue += ".";
                               decText = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
                               if (decText.length > decimales){
                                               strValidacion = "Debe introducir un numero con " + decimales + " decimales maximo.";
                               }
                               else {
                                               intText = fieldValue.substring (0,fieldValue.indexOf('.'));
                                               if (intText.length > enteros){
                                                               strValidacion = "Debe introducir un numero con " + enteros + " enteros maximo.";
                                               }
                               }
                }
                return strValidacion;      
}

function isInt(value){
                  if((parseFloat(value) == parseInt(value)) && !isNaN(parseInt(value))){
                      return true;
                } else {
                      return false;
                }
}

function show(obj) {
                jQuery(obj).show();
}

function hide(obj) {
                jQuery(obj).hide();
}

function showInput(obj) {
                showGroupDiv(obj);
}

function hideInput(obj) {
                hideGroupDiv(obj);
}

function findLabel(inputElement) {
                var id =inputElement.id;
   labels = document.getElementsByTagName('label');
                for (var i = 0; i < labels.length; i++) {
                               if (labels[i].htmlFor == id)
                                               return labels[i];
                }
}

function hideErrors(inputElement) {
                var errors = getErrors(inputElement);
                for (var i = 0; i < errors.length; i++) {
                               hide(errors[i]);
                }
}

function showErrors(inputElement) {
                var errors = getErrors(inputElement);
                for (var i = 0; i < errors.length; i++) {
                               show(errors[i]);
                }
}

function getErrors(inputElement) {
                var id =inputElement.id;
                var trs = document.getElementsByTagName("tr");
                var divs = document.getElementsByTagName("div");
                var errors = new Array();
                var totalFound = 0;
                
                //Find tr errors
                for (var i = 0; i < trs.length; i++) {
                               var attribs = trs[i].attributes;
                               var item = attribs.getNamedItem("errorfor");
                               if (item != null) {
                                               var errorForVal = item.value;
                                               if (errorForVal == id) {
                                                               errors[totalFound] = trs[i];
                                                               totalFound++;
                                               }
                               }
                }
                
                var divs = document.getElementsByTagName("div");
                //Find div errors
                for (var i = 0; i < divs.length; i++) {
                               var attribs = divs[i].attributes;
                               var item = attribs.getNamedItem("errorfor");
                               if (item != null) {
                                               var errorForVal = item.value;
                                               if (errorForVal == id) {                   
                                                               errors[totalFound] = divs[i];
                                                               totalFound++;
                                               }
                               }
                }
                
                return errors;
}


/**
* En caso de que sea un layout de div por ejemplo css_xhtml esta funcion esconde el id relacionado al input.
* @param inputElement
* @return
*/
function hideGroupDiv(inputElement) {
                var groupDiv = getInputGroupDiv(inputElement);
                if (groupDiv != null) {
                               hide(groupDiv);
                }
}

function showGroupDiv(inputElement) {
                var groupDiv = getInputGroupDiv(inputElement);
                if (groupDiv != null) {
                               show(groupDiv);
                }
}

function getInputGroupDiv(inputElement) {
                var prefix = "wwgrp";
                var divId = prefix + "_" + inputElement.id;
                var obj = jQuery(document.getElementById(divId));
                return obj;
}

/*
function blockPage() {
                jQuery.blockUI({message: jQuery("#loading"),
                               css: {width:'0%', top:'30%', left:'40%', border:'0px'} });
}

function unblockPage() {
                jQuery.unblockUI();
}
*/

/**
* set a option from a list from an array or map, this MUST contain just one record 
 * otherwise it will take the first parameter in the array .
* 
 */
function selectOption (ele, data/*, options*/) {
                  ele = dwr.util._getElementById(ele, "addOptions()");
                  if (ele == null) return;
                  var useOptions = dwr.util._isHTMLElement(ele, "select");
                  var useLi = dwr.util._isHTMLElement(ele, ["ul", "ol"]);
                  if (!useOptions && !useLi) {
                    dwr.util._debug("addOptions() can only be used with select/ul/ol elements. Attempt to use: " + dwr.util._detailedTypeOf(ele));
                    return;
                  }
                  if (data == null) return;
                  
                  var argcount = arguments.length;
                  var options = {};
                  var lastarg = arguments[argcount - 1]; 
                  if (argcount > 2 && dwr.util._isObject(lastarg)) {
                    options = lastarg;
                    argcount--;
                  }
                  var arg3 = null; if (argcount >= 3) arg3 = arguments[2];
                  var arg4 = null; if (argcount >= 4) arg4 = arguments[3];
                  if (!options.optionCreator && useOptions) options.optionCreator = dwr.util._defaultOptionCreator;
                  if (!options.optionCreator && useLi) options.optionCreator = dwr.util._defaultListItemCreator;
                
                  var text, value, li;
                  if (dwr.util._isArray(data)) {
                    // Loop through the data that we do have
                    for (var i = 0; i < data.length; i++) {
                      options.data = data[i];
                      options.text = null;
                      options.value = null;
                      if (useOptions) {
                        if (arg3 != null) {
                          if (arg4 != null) {
                            options.text = dwr.util._getValueFrom(data[i], arg4);
                            options.value = dwr.util._getValueFrom(data[i], arg3);
                          }
                          else options.text = options.value = dwr.util._getValueFrom(data[i], arg3);
                        }
                        else options.text = options.value = dwr.util._getValueFrom(data[i]);
                        if (options.text != null || options.value) {
                          var opt = options.optionCreator(options);
                          opt.text = options.text;
                          opt.value = options.value;
                          for(i=0;i<ele.length;i++){
                                if(ele.options[i].value==opt.value){
                                               ele.options[i].selected =true;
                                               break;
                                }
                          }
                          
                        }
                      }
                      
                    }
                  }else{
                                for(i=0;i<ele.length;i++){
                                if(ele.options[i].value==data){
                                ele.options[i].selected =true;
                                break;
                                }
                                }
                  }
                  // All error routes through this function result in a return, so highlight now
                  dwr.util.highlight(ele, options); 
}


function scrollToBottom(id) {
                jQuery("#" + id).scrollTop(jQuery("#" + id)[0].scrollHeight);
}

/**
* Escribe una cadena en un contenedor como un div.
* @param str
* @param id no requerido
* @param saltarLinea no requerido
* @param scrollToBottom indica si se quiere que se haga scroll a abajo.
* @return
*/
function escribe(str, tipoMensaje, id, scrollToBott, saltarLinea) {
                var strMsg;
                var spanStartTag = null;
                id = getMensajesId(id);

                if (saltarLinea == true) {
                               strMsg = "<br/>";
                }
                
                if (tipoMensaje == MENSAJE_ERROR) {
                               spanStartTag = "<span style='color: red'>";                        
                } else if(tipoMensaje == MENSAJE_ADVERTENCIA) {
                               spanStartTag = "<span style='color: black'>";
                }
                
                if (spanStartTag != null) {
                               strMsg += spanStartTag;
                }
                
                strMsg += str;
                
                if (spanStartTag != null) {
                               strMsg += "</span>";
                }
                
                jQuery("#" + id).append(strMsg);
                
                if (getScrollToBottom(scrollToBott)) {
                               scrollToBottom(id);
                }
}

/**
* Salta una linea y escribe la cadena en un contenedor como un div.
* @param str
* @param id no requerido
* @param scrollToBott no requerido
* @return
*/
function escribeSig(str, tipoMensaje, id, scrollToBott) {
                escribe(str, tipoMensaje, id, scrollToBott, true);
}

function mostrarMensajeJQuery(titulo,mensaje,tipomensaje){
                if(tipomensaje == MENSAJE_ERROR){
                               tipomensaje = "error";
                }
                else if (tipomensaje == MENSAJE_ADVERTENCIA){
                               tipomensaje = "info";
                }
                
                jQuery.msgBox({
                               type:tipomensaje,
                               title:titulo,
                               content:mensaje,
                               autoClose:false
                });
}

function borrarMensajes(id) {
                jQuery("#" + getMensajes(id)).html("");
}

function getMensajesId(id) {
                if (id == null) {
                               id = DEFAULT_MENSAJES_ID;
                }
                return id;
}

function getScrollToBottom(scrollToBottom) {
                if (scrollToBottom == null) {
                               scrollToBottom = DEFAULT_SCROLL_TO_BOTTOM;
                }
                return scrollToBottom; 
}

function isValidNumeric(value) {
                if (isNaN(value)) {
                               return false;
                }
                //Convertirlo a string.
                value += "";
                return dhtmlxValidation.isValidNumeric(value);
}

function isMinValido(minimo, valor) {
                if (minimo != "" && !(valor >= minimo)) {
                               return false;
                }
                return true;
}

function isMaxValido(maximo, valor) {
                if(maximo != "" && !(valor <= maximo)) {
                               return false;
                }
                return true;
}

function isMinMaxValido(minimo, maximo, valor) {
                if ((minimo != "" && maximo != "") && !(isMinValido(minimo, valor) && isMaxValido(maximo, valor))) {
                               return false;
                }
                return true;
}

function numbersonly(el, e, dec) {
                               var key;
                               var keychar;

                               
                               
                               if (window.event) {
                                               key = window.event.keyCode;
                               } else if (e) {
                                               key = e.which;
                               } 
                               
                               /**
                               * Dentro de este IF se remueven todos los caracteres no numbers,
                               * se ejecuta solo una vez, cuando el campo pierde el foco, 
                                * (Con esto se evita que peguen cosas raras).
                               *
                               **/
                               if(key==0 || !e){
                                               if(el.hasClassName('money')){
                                                                              el.value = el.value.replace(/([^0-9\$\,\.])+/g,'');
                                               }else{
                                                                              el.value = el.value.replace(/([^0-9])+/g,'');
                                               }
                                               el.value = el.value.trim();
                                               return true;
                               }
                               
                               keychar = String.fromCharCode(key);

                               
                               if ((key==null) || (key==0) || (key==8) ||                         // control keys
                                               (key==9) || (key==13) || (key==27) ) {
                                               return true;
                               } else if ((("0123456789").indexOf(keychar) > -1)) {        // numbers
                                               return true;
                               } else if (dec && (keychar == ".")) {                        // decimal point jump
                                               // myfield.form.elements[dec].focus();
                                               return true;
                               } else {
                                               return false;
                               }
}//end numbersonly()

function jumpTo(objValue, numOfChars, objId){
                if(objValue.length == numOfChars){
                               var _obj = jQuery("input[name='" + objId + "']");
                               if(_obj != null){
                                               _obj.focus();
                               }
                }              
}
