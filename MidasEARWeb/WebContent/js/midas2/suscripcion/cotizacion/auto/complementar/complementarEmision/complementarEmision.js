var comboAgrupacionesValue;

/**
 * 
 */
function volverAListadoCotizacion(){
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=A',
			'contenido', null);
}

function seleccionarCliente(idCliente){
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/seleccionarContratante.action?idToCotizacion='+jQuery("#idToCotizacion").val()+'&idCliente='+idCliente,
			'contenido', null);	
}
function seleccionarTipoImpresionCliente(){
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/seleccionarTipoImpresionCliente.action?idToCotizacion='+jQuery("#idToCotizacion").val()+'&idTipoImpresionCliente='+jQuery("#tipoImpresionCliente").val(),
			'contenido', null);	
}

/* Agrupacion 20131212 */
function seleccionarTipoAgrupacionRecibos(comboVal){
	if(confirm("\u00BFEst\u00E1 seguro que desea modificar el tipo de agrupaci\u00F3n de los recibos? \n      Si selecciona AGRUPADO no podr\u00E1 generar cobranza por inciso.")){
		
		sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/seleccionarTipoAgrupacionRecibos.action?idToCotizacion='+jQuery("#idToCotizacion").val()+'&tipoAgrupacionRecibos='+comboVal,
				'contenido', null);
		/*
		jQuery.ajax({
    		"url" : '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/seleccionarTipoAgrupacionRecibos.action',
    		"data" : 'idToCotizacion='+jQuery('#idToCotizacion').val()+'&tipoAgrupacionRecibos='+comboVal,
    		"dataType" : "json",
    		
    		"success": function(data) {
                response($.map(data, function(item) {
                    return {
                        label: item.label,
                        id: item.id
                    }
                }))
            }
    	});
		*/
		
	}else{				
		jQuery("#cotizacionDTO\\.tipoAgrupacionRecibos").val(comboAgrupacionesValue);		
	}
}

function setVariableValue(variable, value){
	eval(variable + "= '" + value + "'");
}

function procesoCmbTipoImpresionCliente(){
	jQuery("#tipoImpresionCliente option" ).each(function(){
	        if(jQuery("#valTipoImpresion").val()==jQuery(this).val()){
	        		jQuery(this).attr("selected","selected");
	        }
	 });
}
function validaIncisos(){
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/complementarEmision/validarDatosComplementarios.action?idToCotizacion='+jQuery("#idToCotizacion").val(),
			'incisosCompletos', null);
}

function validaEmision(){
	jQuery("#botonEmision").hide();
	if(jQuery("#visible").val() == 'true'){
		if(jQuery("#claveEstatus").val() == 12){
			jQuery("#botonEmision").show();
		}
		if(jQuery("#claveEstatus").val() == 14){
			jQuery("#botonEmision").show();
		}
	}
}

function mostrarVentanModalDatosTarjeta(){
	var html = 	'<table id="tblDatosTarjeta" style="margin:10px 10px 10px 10px; ">' +
					'<tbody>' +
						'<tr>' +
							'<td for="codigoSeguridad"> Código de Seguridad </td>' +
							'<td>' +'<div id="wwgrp_codigoSeguridad" class="wwgrp">' +
									'<div id="wwctrl_codigoSeguridad" class="wwctrl">' +
										'<input type="text" name="codigoSeguridad" maxlength="4" value="" id="codigoSeguridad" class="txtfield input_text jQrequired" onkeypress="return soloNumeros(this, event, false)">' +
									'</div>' +
								'</div>' +
							'</td>' +
						'</tr>' +
						'<tr>' +
							'<td for="fechaVencimiento"> Fecha vencimiento (MMYY)</td>' +
							'<td>' +
								'<div id="wwgrp_fechaVencimiento" class="wwgrp">' +
									'<div id="wwctrl_fechaVencimiento" class="wwctrl">' +
										'<input type="text" name="fechaVencimiento" maxlength="4" value="" id="fechaVencimiento" class="txtfield input_text jQrequired"  onkeypress="return soloNumeros(this, event, false)">' +
									'</div>' +
								'</div>' +
							'</td>' +
						'</tr>' +
					'</tbody>' +
				'</table>'+
				
				'<div class="btn_back w140" style="display: block; margin :10px 10px 10px 10px; float:right"">'+
					'<a href="javascript: validaDatosTarjeta();">'+
						'Emitir</a>'+
				'</div>';	
	mostrarVentanaModalMode("winDatosTarjeta", "Datos de tarjeta", 0,0,400,150, 'ATTACH_HTML_STRING',html);
}

function validaDatosTarjeta(){
	if (jQuery("#codigoSeguridad").val() == '') {
		mostrarMensajeInformativo("C\u00F3digo de seguridad no valido", "20");
	}else if(jQuery("#fechaVencimiento").val() == ''){
		mostrarMensajeInformativo("Fecha de vencimiento no valida","20");
	}else{
		emitirCotizacion();
	}
}

function emitirCotizacion() {
	var idToCotizacion = dwr.util.getValue("idToCotizacion");
	var idToPersonaContratante = dwr.util.getValue("cotizacionDTO_idToPersonaContratante");
	
	var mensaje = dwr.util.getValue("mensajeDTO.mensaje");
	var visible = dwr.util.getValue("mensajeDTO.visible");
	var path= '/MidasWeb/suscripcion/emision/emitir.action?idToCotizacion='+idToCotizacion+'&mensajeDTO.mensaje='+mensaje+'&mensajeDTO.visible='+visible+'&fechaVencimiento=' + jQuery("#fechaVencimiento").val() + '&codigoSeguridad=' + jQuery("#codigoSeguridad").val();
	var targetWorkArea ="contenido";
	var urlPDF='/MidasWeb/suscripcion/emision/getFormatoEntrevista.action?&idToCotizacion='+idToCotizacion;
	sendRequestJQ(null,path,targetWorkArea,cerrarVentanaModal("winDatosTarjeta"));
}


function complementarContratante(idCliente, idToCotizacion){
	var path = '/MidasWeb/catalogoCliente/loadById.action?tipoAccion=1&divCarga=contenido&cliente.idCliente=' + idCliente;
	path += '&tipoRegreso=2&idToCotizacion='+idToCotizacion;
	if(idCliente != null && idCliente != ""){
		sendRequestJQ(null,path,targetWorkArea,null);
	}
}

function mostrarClientesAsociados(){
	var url = getClientesAsociadosURL + "?idToNegocio=" + dwr.util.getValue("idToNegocio");
	mostrarVentanaModal("winClientesAsociados", "Consulta de Clientes", 0,0,700,400, url);	
}

function cargaClienteAsociado(idCliente,nombreCliente){
	dwr.util.setValue("cotizacionDTO.idToPersonaContratante",idCliente);
	dwr.util.setValue("cliente.nombreCliente",nombreCliente);
	seleccionarCliente(idCliente);
	cerrarVentanaModal("winClientesAsociados");
}


