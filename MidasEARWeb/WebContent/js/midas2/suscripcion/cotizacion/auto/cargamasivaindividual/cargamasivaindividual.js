/**
 * 
 */
function verCargaMasivaCotizacion(){
	limpiarDivsGeneral();
	var path= '/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarCargaMasivaCotizacion.action';
	sendRequestJQ(null, path,'contenido_detalle',null);
}

function verCargaMasivaEmision(){
	limpiarDivsGeneral();
	var path= '/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarCargaMasivaEmision.action';
	sendRequestJQ(null, path,'contenido_cargaMasivaEmision',null);
}

var cargaMasivasGrid;
var cargaMasivasDetalleGrid;

function iniciaListadoIndividual(claveTipo){

	document.getElementById("cargasMasivasIndividualesGrid").innerHTML = '';
	cargaMasivasGrid = new dhtmlXGridObject("cargasMasivasIndividualesGrid");
	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
    });
	cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
    });		
	cargaMasivasGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/listarCargaIndividual.action?" + jQuery(document.cargamasivaindividualForm).serialize());
}
function importarArchivo(tipoCarga){
 if(!jQuery("#idAcurdoAfirmeMasiva").is(':checked')) {
		 
		 alert("Debe marcar el acuerdo");
		 return;
	 }
	var claveTipo = jQuery("#claveTipo").val();
	if(claveTipo == 0){
	if(jQuery("#negocios").val() == null || jQuery("#negocios").val() == "" ||
			jQuery("#productos").val() == null || jQuery("#productos").val() == "" ||
			jQuery("#polizas").val() == null || jQuery("#polizas").val() == ""){
		mostrarMensajeInformativo('Favor de seleccionar un Negocio, Producto y/o Tipo Poliza',"20");
		return;
	}
	}
	
	
	var path = "/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/validaCargaIndividual.action?";
	var divcontenido = "";
	if(claveTipo == 0){
		divcontenido = "contenido_detalle";
	}
	if(claveTipo == 1){
		divcontenido = "contenido_cargaMasivaEmision";
	}
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaIncisos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva de Incisos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			sendRequestJQ(null, path + jQuery(document.cargamasivaindividualForm).serialize() + "&idToControlArchivo=" + idToControlArchivo + "&tipoCarga=" + tipoCarga, divcontenido, null);
    			}else{
    				mostrarMensajeInformativo('Fallo carga de archivo, favor de volver a intentar en unos momentos.',"20");
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaIncisos").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           mostrarMensajeInformativo('Solo puede importar archivos Excel (.xls).',"20");
           return false; 
        } 
        else return true; 
     }; 
    vault.create("vault");
    vault.setFormField("claveTipo", "40");
}

function descargarCargaMasiva(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function descargarLogCargaMasivaIndividual() {	
	if(jQuery("#logErrors").val() == 'true'){
		var idToControlArchivo = jQuery("#idToControlArchivo").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/descargarLogErrores.action?idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT");
	}
}

function descargarPlantillaCargaIndividual(){
	var claveTipo = jQuery("#claveTipo").val();
	var idToNegTipoPoliza = jQuery("#polizas").val();
	var idToNegocio = jQuery("#negocios").val();
	
	
	if(jQuery("#negocios").val() == null || jQuery("#negocios").val() == "" ||
			jQuery("#productos").val() == null || jQuery("#productos").val() == "" ||
			jQuery("#polizas").val() == null || jQuery("#polizas").val() == ""){
		mostrarMensajeInformativo('Favor de seleccionar un Negocio, Producto y/o Tipo Poliza',"20");
		return;
	}
	var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/descargarPlantilla.action?claveTipo=" + claveTipo + "&idToNegTipoPoliza=" + idToNegTipoPoliza +"&idToNegocio=" + idToNegocio;
	window.open(location, "Cotizacion_COT");
}

function mostrarResumenCargaMasivaIndividual(idToCargaMasivaIndAutoCot){
	var claveTipo = jQuery("#claveTipo").val();
	var divContenido = "contenido_detalle";
	if(claveTipo == 1){
		divContenido = "contenido_cargaMasivaEmision";
	}
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarDetalleCargaIndividual.action?idToCargaMasivaIndAutoCot=" + idToCargaMasivaIndAutoCot + "&" + jQuery(document.cargamasivaindividualForm).serialize(), divContenido, null);
}

function iniciaListadoDetalleIndividual(){
	var idToCargaMasivaIndAutoCot = jQuery("#idToCargaMasivaIndAutoCot").val();
	document.getElementById("cargasMasivasIndDetalleGrid").innerHTML = '';
	cargaMasivasDetalleGrid = new dhtmlXGridObject("cargasMasivasIndDetalleGrid");
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
    });
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
    });		
	cargaMasivasDetalleGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/listarDetalleCargaIndividual.action?idToCargaMasivaIndAutoCot="+ idToCargaMasivaIndAutoCot);
}

function regresarACargaMasivaIndividualAuto(){
	var claveTipo = jQuery("#claveTipo").val();
	var divContenido = "contenido_detalle";
	var path =  "/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarCargaMasivaCotizacion.action?";
	if(claveTipo == 1){
		divContenido = "contenido_cargaMasivaEmision";
		path =  "/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/mostrarCargaMasivaEmision.action?";
	}
	sendRequestJQ(null, path + jQuery(document.cargamasivaindividualdetalleForm).serialize(), divContenido, null);	
}

function mostrarErrorCargaMasivaIndividual(estatus, mensajeError){
	if(estatus == 0){
		mostrarVentanaMensaje('10', mensajeError);
	}
	if(estatus == 1){
		mostrarMensajeExito();
	}
	if(estatus == 2){
		mostrarVentanaMensaje('30', 'Pendiente de Procesar');
	}
	if(estatus == 3){
		mostrarVentanaMensaje('30', mensajeError);
	}
	if(estatus == 4 || estatus == 5){
		mostrarVentanaMensaje('30', mensajeError);
	}
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_detalle');
	limpiarDiv('contenido_cargaMasivaEmision');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}

function verDetalleImpresionPolizaCargaMasiva(idToCargaMasivaIndAutoCot){
	var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasivaindividual/descargarPolizasCargaMasiva.action?idToCargaMasivaIndAutoCot=" + idToCargaMasivaIndAutoCot;
	window.open(location, "Cotizacion_COT");	
}
