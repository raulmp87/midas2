var coberturasObligatoriasGrid;
var coberturasAdicionalesGrid;
var sumasAseguradasAdicionalesGrid;
var deduciblesAdicionalesGrid;

var deduciblesAdicionalesProcessor;

var idToNegAct = -1;
var idToNegProductoAct = -1;
var idToNegPaqSeccionAct = -1;
var idToNegTipoPolizaAct = -1;
var idToNegSeccionAct = -1;
var idMonedaAct = -1;
var idVigenciaAct = -1;
var idMunicipioAct = -1;
var idToCotizacionAct = -1;

var cotizacionPath = '';

var idCoberturaString = "";
var primaNeta = 0;
var sumaAsegurado = 0;
var idToCobertuta = 0;
var claveTarifa = "";
var deducible = 0;
var param = "";

var numeroFolio ="";
var numCot = "";


jQuery(document).ready(function(){
	idToNegAct = jQuery("#idToNegocio").val()
	if(idToNegAct!=null && idToNegAct != '') {
		obtenerProductos(idToNegAct);
	}
});

function obtenerEstados(idToNegocio){
    idToNegAct = idToNegocio;
    if(idToNegocio!=-1 && idToNegocio!= null && idToNegocio != ''){
	    listadoService.getEstadosPorNegocioId(idToNegocio,false,function(data){
			addOptionSelect('stateId', data, 'ASC', null, null);
	    });
    }else{
    	addOptions('stateId', null);
    }
}

function obtenerProductos(idToNegocio){
	idToNegAct = idToNegocio;
	if(idToNegocio!=-1 && idToNegocio!= null && idToNegocio != ''){
		listadoService.getMapNegProductoPorNegocio(idToNegocio,function(data){
			addOptionSelect('idToNegProducto', data, 'ASC', null, null);
		});
	}else{
    	addOptions('idToNegProducto', null);
    }
	obtenerEstados(idToNegocio);
}

function obtenerTiposPoliza(idToNegProducto){
	idToNegProductoAct = idToNegProducto;
	if(idToNegProducto!=null && idToNegProducto != ''){
		listadoService.getMapNegTipoPolizaPorNegProducto(idToNegProducto,function(data){
			addOptionSelect('idToTipoPoliza', data, 'ASC', null, null);
		});
	}else{
    	addOptions('idToTipoPoliza', null);
    }
}

function obtenerLineasDeNegocio(idToTipoPoliza){
	idToNegTipoPolizaAct = idToTipoPoliza;
	if(idToTipoPoliza!=null && idToTipoPoliza != ''){
		listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza,function(data){
			addOptionSelect('idToNegSeccion', data, 'ASC', null, null)
		});
	}else{
    	addOptions('idToNegSeccion', null);
    }
}

function obtenerMonedas(idToTipoPoliza){
	if(idToTipoPoliza!=-1 && idToTipoPoliza!= null && idToTipoPoliza != ''){
		listadoService.getMapMonedaPorNegTipoPoliza(idToTipoPoliza,function(data){
			addOptionSelect('idTcMoneda', data, 'ASC', null, idMonedaAct)
		});
	}else{
    	addOptions('idTcMoneda', null);
    }
}

function obtenerVigencias(){
	var idToNegocio = jQuery("#idToNegocio").val();
	if(idToNegocio!=-1 && idToNegocio!= null && idToNegocio != ''){
		listadoService.getMapVigencias(function(data){
			addOptionSelect('idTcVigencia', data, 'ASC', null, null)
		});
	}else{
    	addOptions('idTcVigencia', null);
    }
	validaCombos();
}

function obtenerPaquetes(idToNegSeccion){
	idToNegSeccionAct = idToNegSeccion;
	if(idToNegSeccion!=null){
		listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(
				idToNegSeccion,
				function(data){
					addOptionSelect('idToNegPaqueteSeccion', data, 'ASC', null, null)
				});
	}else{
    	addOptions('idToNegPaqueteSeccion', null);
    }
	validaCombos();
}

function obtieneMunicipio(idEstado){
	if(idEstado!=null){
		listadoService.getMapMunicipiosPorEstado(
				idEstado,
				function(data){
					addOptionSelect('cityId', data, 'ASC', null, null)
				});
	}else{
    	addOptions('cityId', null);
    }
	validaCombos();
	
}

function onChangeComboMoneda(idMoneda){
	idMonedaAct = idMoneda;
}

function onChangeComboVigencia(idVigencia){
	idVigenciaAct = idVigencia;
	validaCombos();
}

function onChangeComboPaquete(idToNegPaqSeccion){
	idToNegPaqSeccionAct = idToNegPaqSeccion;
	validaCombos();
}

function onChangeComboMunicipio(idMunicipio){
	idMunicipioAct = idMunicipio;
	validaCombos();
}

function validaCombos(){
	$('body').css('cursor','progress');
	if(jQuery('#idToNegProducto').val() > 0 && jQuery('#idTcMoneda').val() > 0  && jQuery('#idToTipoPoliza').val() > 0 &&
				 jQuery('#idToNegSeccion').val() > 0  &&  jQuery('#idToNegPaqueteSeccion').val() > 0){
		jQuery("#stateId").removeAttr("disabled");
		jQuery("#cityId").removeAttr("disabled"); 
		if(jQuery('#cityId').val() > 0) {
            refrescarGridsTarifaServicioPublico(null,null,null,null);
        }
	}else{
		jQuery("#stateId").attr("disabled","true");
		jQuery("#cityId").attr("disabled","true");
	}
	$('body').removeAttr('style');
}

function obtenerCoberturasAdicionales(){
	coberturasAdicionalesGrid = new dhtmlXGridObject('coberturasAdicionalesGrid');
	idToCotizacionAct = jQuery("#idToCotizacion").val();
	if(idToCotizacionAct != null && idToCotizacionAct != '') {
		cotizacionPath = "&idToCotizacion=" + idToCotizacionAct;
	}
	if(idMunicipioAct > 0){
		coberturasAdicionalesGrid.load(obtenerCoberturasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	} else {
		coberturasAdicionalesGrid.load(obtenerCoberturasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=''" + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	}
}

function obtenerCoberturasObligatorias(){
	coberturasObligatoriasGrid = new dhtmlXGridObject('coberturasObligatoriasGrid');
	
	coberturasObligatoriasGrid.attachEvent("onRowCreated",function(rowId) {
		coberturasObligatoriasGrid.cellById(rowId, 9).setDisabled(true);
	});
	
	idToCotizacionAct = jQuery("#idToCotizacion").val();
	if(idToCotizacionAct != null && idToCotizacionAct != '') {
		cotizacionPath = "&idToCotizacion=" + idToCotizacionAct;
	}
	coberturasObligatoriasGrid.load(obtenerCoberturasObligatoriasGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
}

function refrescarGridsTarifaServicioPublico(sid,action,tid,node){
	obtenerCoberturasObligatorias();
	obtenerCoberturasAdicionales();
	obtenerSumasAseguradasAdicionales();
	obtenerDeduciblesAdicionales();
	return true;
}

function initGridsTarifaServicioPublico(){
	if(idToNegPaqSeccionAct != -1){
		refrescarGridsTarifaServicioPublico(null,null,null,null);
	}	
}

//COBERTURAS ADICIONALES
function refrescarGridCoberturasAdicionales(sid,action,tid,node){
	obtenerCoberturasAdicionales();
	return true;
}

//SA ADICIONALES
function obtenerSumasAseguradasAdicionales(){
	sumasAseguradasAdicionalesGrid = new dhtmlXGridObject('sumasAseguradasAdicionalesGrid');
	idToCotizacionAct = jQuery("#idToCotizacion").val();
	if(idToCotizacionAct != null && idToCotizacionAct != '') {
		cotizacionPath = "&idToCotizacion=" + idToCotizacionAct;
	}

	sumasAseguradasAdicionalesGrid.attachEvent("onCheck", function(rId){
		onChecked(sumasAseguradasAdicionalesGrid, rId);
	});
	
	if(idMunicipioAct > 0){
		sumasAseguradasAdicionalesGrid.load(obtenerSumasAseguradasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	} else {
		sumasAseguradasAdicionalesGrid.load(obtenerSumasAseguradasAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=''" + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	}
}

/**
 * Obtenemos busca un idCobertura ya seleccionado anteriormente.
 * @grid Grid dhtmlx  
 * @rId row
 * @cId column
 * @state
 */
function onChecked(grid, rId){
	var i=0;
	var _idToCobertura = grid.cellById(rId, 1).getValue();
	for(var id=0;id<grid.getRowsNum()-1;id++){
		if(rId!=id){
		for (var cInd=0;cInd<grid.getColumnsNum();cInd++){
			if(grid.cells(id,cInd).isCheckbox()){
				if(grid.cells(id,cInd).getValue()==1){
					if(_idToCobertura == grid.cells(id,1).getValue()){
						grid.cells(rId,cInd).setValue(0);
						mostrarMensajeInformativo("No puede Seleccionar mas de una cobertura de tipo "+ grid.cells(id,7).getValue(),"10");
					}
				}
			}
		}}
	}
}

function refrescarGridSumasAseguradasAdicionales(sid,action,tid,node){
	obtenerSumasAseguradasAdicionales();
	return true;
}

//DEDUCIBLES
function obtenerDeduciblesAdicionales(){
	deduciblesAdicionalesGrid = new dhtmlXGridObject('deduciblesAdicionalesGrid');
	idToCotizacionAct = jQuery("#idToCotizacion").val();
	if(idToCotizacionAct != null && idToCotizacionAct != '') {
		cotizacionPath = "&idToCotizacion=" + idToCotizacionAct;
	}
	if(idMunicipioAct > 0){
		deduciblesAdicionalesGrid.load(obtenerDeduciblesAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=" + dwr.util.getValue("cityId") + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	} else {
		deduciblesAdicionalesGrid.load(obtenerDeduciblesAdicionalesGridPath + "?idToNegPaqueteSeccion=" + idToNegPaqSeccionAct + "&idMonedaDTO=" + idMonedaAct +  "&idTcVigencia=" + idVigenciaAct + "&stateId=" + dwr.util.getValue("stateId") + "&cityId=''" + cotizacionPath+ "&idToNegSeccion=" + idToNegSeccionAct);
	}
	deduciblesAdicionalesGrid.groupBy(10);
}

function refrescarGridDeduciblesAdicionales(sid,action,tid,node){
	obtenerDeduciblesAdicionales();
	return true;
}

function crearCotizacionServicioPublico(prima, idAgrupadorPasajeros, rInd){
		$('body').css('cursor','progress');
		claveTarifa = "";
		param = "";
		primaNeta = prima;
		idToCobertuta = deduciblesAdicionalesGrid.cells(rInd,1).getValue();
		deducible = deduciblesAdicionalesGrid.cells(rInd,7).getValue();
		sumaAsegurado = deduciblesAdicionalesGrid.cells(rInd,9).getValue();
		claveTarifa = idToCobertuta+'-'+sumaAsegurado+'-'+deducible;		
		
		param = {
				"cotizacion.solicitudDTO.negocio.idToNegocio"                   : jQuery('#idToNegocio').val(),
				"cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto"  : jQuery('#idToNegProducto').val(),
				"cotizacion.idMoneda"                                           : jQuery('#idTcMoneda').val(),
				"cotizacion.negocioTipoPoliza.idToNegTipoPoliza"                : jQuery('#idToTipoPoliza').val(),
	  		    "cotizacion.porcentajePagoFraccionado"                          : 0, 
				"incisoCotizacion.incisoAutoCot.negocioSeccionId"               : jQuery('#idToNegSeccion').val(),
				"incisoCotizacion.incisoAutoCot.negocioPaqueteId"               : jQuery('#idToNegPaqueteSeccion').val(),
				"incisoCotizacion.incisoAutoCot.estadoId"                       : jQuery('#stateId').val(),
				"incisoCotizacion.incisoAutoCot.municipioId"                    : jQuery('#cityId').val(),
	  		    "idTcVigencia"          : idVigenciaAct,
	  		    "idToNegocio"           : dwr.util.getValue("idToNegocio"),
	  		    "idCoberturaRC"         : idToCobertuta,
	  		    "sumaAsegurado"         : sumaAsegurado,
	  		    "deducible"             : deducible,
	  		    "codigoAgente"			: jQuery('#codigoAgente').val(),
	  		    "idAgrupadorPasajeros"	: idAgrupadorPasajeros
				};
		
		var i = 0;
		
		getLisCoberturacotizacion(coberturasAdicionalesGrid, true, 8, true);
		
		getLisCoberturacotizacion(sumasAseguradasAdicionalesGrid, true, 9, false);
		jQuery.extend(param, {idCoberturaNA : idCoberturaString});
		jQuery.extend(param, {'cotizacion.primaTarifa' : primaNeta});
		jQuery.extend(param, {'claveTarifa' : claveTarifa});
		jQuery.extend(param, {'cveFolio' : numeroFolio});
		if(primaNeta!=0){
			if(jQuery('#idToNegProducto').val() > 0 && jQuery('#idTcMoneda').val() > 0  && jQuery('#idToTipoPoliza').val() > 0 &&
			   jQuery('#idToNegSeccion').val() > 0  &&  jQuery('#idToNegPaqueteSeccion').val() > 0 && jQuery('#stateId').val() >0  && jQuery('#cityId').val() > 0){
				mostrarMensajeConfirm("\u00BFDesea crear la cotizaci\u00f3n con una prima de " + formatCurrency(primaNeta) + "?","20","crearCotizacion()",null,null,null);
			} else {
				mostrarMensajeInformativo("Dede indicar Negocio, Producto, Tipo de P\u00f3liza, L\u00ednea de Negocio, Paquete, Moneda , Vigencia, Estado y Municipio para crear la cotizaci\u00f3n.","10");
			}
		}else{
			mostrarMensajeInformativo("La prima debe ser mayor a 0.","10");
		}
}

function crearCotizacion(){
	jQuery.ajax({
	  	type     : 'POST',
	  	url      : crearCotizacionServicioPublicoPath,
	  	data     : param,
	  	dataType : 'json',
	  	async    : true,
	  	success  : function(data){
	  		jQuery("#idToCotizacion").val(data.idToCotizacion);
			numCot = data.idToCotizacion;
	  	},
	  	complete : function(data){
	  		jQuery('#nuevoCotizadorAgentes').attr('href','/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?idCotizacion=' + numCot + '&numeroInciso=1&primaNeta=' + primaNeta + '&numeroFolio=' + numeroFolio);
	  		document.getElementById('nuevoCotizadorAgentes').click();
	  		jQuery('#nuevoCotizadorAgentes').attr('href','/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action');
	  		idCoberturaString = "";
			$('body').removeAttr('style');
	  	}
	});
}

function getLisCoberturacotizacion(grid, sumaPrima, colPrim, aplicaCobertura){
	var i=0;
	for(var id=0;id<grid.getRowsNum();id++){
		for (var cInd=0;cInd<grid.getColumnsNum();cInd++){
			if(grid.cells(id,cInd).isCheckbox()){
				if(grid.cells(id,cInd).getValue()==1){
					var idToCobertutaOb = grid.cells(id, 1).getValue();
					if(idToCobertutaOb!=idToCobertuta){
						if(sumaPrima){
							var prima = grid.cells(id, colPrim).getValue();
							primaNeta = parseInt(primaNeta) + parseInt(prima);
						}
					}
					if(!aplicaCobertura){
						sumaAsegurado = grid.cells(id, 8).getValue();
						claveTarifa += ','+idToCobertutaOb+'-'+sumaAsegurado+'-';
					}
				}else{
					if(aplicaCobertura){
						if(idCoberturaString=="")
							idCoberturaString += grid.cells(id,1).getValue();
						else
							idCoberturaString += ","+grid.cells(id,1).getValue();	
					}
				}
			}
		}
	}
}

function addOptionSelect(obj, data, order, base, value){
	jQuery("#"+obj).empty();
	var opt = "<option>seleccione ...</option>";
	var datos;
	if(order=="ASC"){
		datos = Object.keys(data).sort();
	}else{
		datos = Object.keys(data).sort().reverse();
	}

    var lista = [];
	$.each(data, function(index, val) {
		lista.push(val);
	});
	order=="ASC"?lista.sort():lista.sort().reverse();
	var selected = "";
	var selectedInput = false;
	if(sizeMap(datos) >= 1){
		jQuery.each(lista, function(index, valor) {
			for (var i in datos) {
				for (var e in data) {
					if(datos[i]>0){
						if(valor == data[datos[i]]){
							if(selected==""){
								selected = datos[i];
							}
							selectedInput =(selectedInput==false)?((datos[i]==$("#"+base).val())?true:false):false;
							opt+="<option value='"+datos[i]+"'>"+data[datos[i]]+"</option>";
							break;
						}
					}
				}
			}
			if(base!=null){
				selected = selectedInput?$("#"+base).val():selected;
			}
		});
		jQuery("#"+obj).html(opt);
		if(value != null && value > 0){
			jQuery("#"+obj).val(value);
		} else {
			jQuery("#"+obj).val(selected);
		}
		jQuery("#"+obj).change();
	}else {
		restartCombo(obj, "", "No se encontraron registros ...", "id", "value");						
	}
}

function buscarFolioVenta(){
	numeroFolio = document.getElementById('numeroFolio').value;
	if(numeroFolio != null && numeroFolio.trim() != ""){
		var toFiltro = {'toFiltro.folioInicio' : numeroFolio};	
		jQuery.ajax({
			url: '/MidasWeb/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico/buscarFolio.action',
			type : "POST",
			data : toFiltro,
			dataType: 'json',
			success : function(json){
				cargarInformacionFolio(json);
			}
		});
	} else {
		var jsonError = {'mensaje' : "No puede quedar el n&uacute;mero de folio vacio", 'toFolio' : null };
		cargarInformacionFolio(jsonError);
	}
}


function cargarInformacionFolio(json){
	var mensaje = json.mensaje;
	var toFolio = json.toFolio;
	if(mensaje == null || mensaje == ""){
		jQuery('#idToNegocio').val(toFolio.negocio);
		idMonedaAct = toFolio.idTipoMoneda;
		obtenerProductos(toFolio.negocio);
		obtenerTiposPoliza(toFolio.negprod);
		obtenerLineasDeNegocio(toFolio.negTpoliza);
		obtenerEstados(toFolio.negocio);
		
		document.getElementById('idToNegProducto').value = toFolio.negprod;
		document.getElementById('idToTipoPoliza').value = toFolio.negTpoliza;
		
		document.getElementById('idTcVigencia').value = toFolio.vigencia;
		document.getElementById('codigoAgente').setAttribute('value', json.codigoAgente);
		
		
	jQuery("#textMessage").html('');
	} else {
		document.getElementById('mensajes').style.display = 'block';
		document.getElementById('textMessage').className = 'errorblock';
		jQuery("#textMessage").html(mensaje);
		setTimeout(closeMessageDiv, 3500);
	}
}

function closeMessageDiv(){
	document.getElementById('mensajes').style.display = 'none';
	document.getElementById('textMessage').className = '';
}