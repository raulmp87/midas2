var headerValue = '';
var validoForm;
var targetNegocioSeccion;
var jspForm;
var idNegocio;
var idToCotizacion;
var numeroInciso;
var idNegocioSeccion;
var idToPersonaContratante;
var claveEstatusCotizacion;
var claveEstatusEmitido=16;
var validos = ".-0123456789";
var internetExplorer = 'Microsoft Internet Explorer';
var MSIE = "MSIE";
var btnEmitircotizacion;
var baseCliente = "#cliente\\.";
var tipoCotizacion;
var yaRecalculo = false;
var personaFisica = 1;
var doblClic = false;
var COTIZACION_SERVICIO_PUBLICO ='TCSP';
var recalculoInicial;
var negocio;
var paqueteModificado = false;
var COBRANZA_Y_EMISION_FORM = "4";
var FINALIZA_COTIZACION_FORM = "-3";

var emailCliente = "";

$(document).ready(function(){
	$("input:radio").attr("checked", false);
	$('#pepDiV').hide();
	$("#cuentaPropiaDiv").hide();
	$(".fancybox").fancybox({
	    helpers : {
	        overlay : {
	            css : {
	                'background' : 'rgba(1, 1, 1, 0.8)',
	            }
	        }
	    }
	});
	var claveTipoPersona=$('#claveTipoPersona').val();
	
	if(claveTipoPersona===1){
		$('#divPersonaFisica').show();
		$('#divPersonaMoral').hide();
	}else {
		$('#divPersonaFisica').hide();
		$('#divPersonaMoral').show();
	}
	
	var pdfDownload =$('#pdfDownload').val();
	if(pdfDownload!=null&&pdfDownload!=''){
		
		$('#pdfDownload').val(null);
		window.open('/MidasWeb/suscripcion/cotizacion/auto/terminarcotizacion/getFormatoEntrevista.action?cotizacion.idToPersonaContratante='+$("#idToPersonaContratante").val()+'&idToCotizacion='+$('#idToCotizacion').val(), 'download');
		
	}
	
	if(compatibleIE()){

		$('#idToCotizacion').val($('#0idToCotizacion').val());
		idToCotizacion = $('#idToCotizacion').val();
		$('#numeroInciso').val($('#0numeroInciso').val());
		numeroInciso = $('#numeroInciso').val();
		jspForm = $('#jspForm').val();
		claveEstatusCotizacion = $('#claveEstatus').val();
		tipoCotizacion = $("#0tipoCotizacion").val();
		actualizaMensajes();
			
	    $("input,select").change(function(){
	    	if($(this).val()) {
	    		$(this).parent().parent().parent().removeClass('has-error');
	    	}
	    });

	    $("label").change(function(){
	    	if($(this).html()) {
	    		$(this).parent().removeClass('has-error');
	    	}
	    });
	    
	    $('a#divAlertasSapAmisBtn').fancybox();
	    $('#backMSN').click(function(e){
	    	$.closeAllMesaje();
	    });
	    
		$("#newCot").click(function(e){
			if(confirm('\u00BF Est\u00e1 usted seguro(a) de crear una nueva cotizaci\u00f3n? \n Se perder\u00e1n los cambios hechos hasta el momento')){
				
				jspForm = $('#jspForm').val();
				
				var idForm = '';
				switch(jspForm){
					case "1":
						idForm = 'fromDatosGenerales';
					break;
					case "2":
						idForm = 'detalleCoberturasForm';
					break;
					case "3":
						idForm = 'datosComentarioForm';
					break;	
					case COBRANZA_Y_EMISION_FORM:
						idForm = 'cobranzaYEmisionForm';
					break;
					case FINALIZA_COTIZACION_FORM:
						idForm = 'finalizaCotizacionForm';
					break;
					default:
						idForm = 'cobranzaYEmisionForm';
				}
				
				generaNuevaCotizacion(idForm);
			}
		});
		
		if(jspForm=="1"){
	  		$(".datepicker").datepicker('update');
			$('a#modalFlotillas').fancybox();
			$('a#featuresLink').fancybox();
			

			$("#btnToggle").click(function(e){
				e.preventDefault();
				mostrarOcultarDiv('seleccionarAgente', $('#btnToggle'), null);
			});
			
			$('#nextBtnDG').click(function(){
	    		guardarCotizacion();      
			});
			
			//combos
			$("#idNegocio").change(function(){
				cargaProductosNegocioAgente(this.value); 
				cargarEstadoNegocio(this.value);
				cargaNegocioSeccion();
				validaFechasCotizacion();
				cargaTiposVigenciaNegocio(this.value);
			   
			});
			
			$("#idProducto").change(function(){
				cargaTipoPolizaAgente(this.value); 
				cargaNegocioSeccion();
			});
			
			$("#idTipoPoliza").change(function(){
				cargaNegocioSeccion();
			});
			
			if(!$.isValid(idToCotizacion)){
				$("#inivig").val(Date.today().toString("dd/MM/yyyy"));
		  		$("#finvig").val(Date.today().add(1).years().toString("dd/MM/yyyy"));

				idNegocio = $('#idNegocio').val();
				if($.isValid(idNegocio)){
					cargaProductosNegocioAgente(idNegocio);
					cargarEstadoNegocio(idNegocio);
				}

			}else{
				if($.isValid( $('#nombreContratante').val())){
					if($('#claveTipoPersona').val() == personaFisica){
						$.setName('#nombreContratante','#nombreProspecto','#aPaterno','#aMaterno');
					}
				}
				$('#estiloSeleccionado').val($('#0idEstilo').val());
				$('#vehiculoEstiloName').val($('#0estiloSeleccionado').val());
		  		validaEstiloVehiculo();

				if(tipoCotizacion==COTIZACION_SERVICIO_PUBLICO && !$.isValid($('#idTipoUso').val())){
					onChangeLineaNegocio('idTipoUso', 'idNegocioSeccion');	
				}
			}
	  		cargarAutocmpleteExtilo();
			if($.isValid( $('#nombreContratante').val())){
				if($('#claveTipoPersona').val() == personaFisica){
					$.setName('#nombreContratante','#nombreProspecto','#aPaterno','#aMaterno');
				}
			}
			$('#estiloSeleccionado').val($('#0idEstilo').val());
			$('#vehiculoEstiloName').val($('#0estiloSeleccionado').val());
			if(claveEstatusCotizacion==claveEstatusEmitido){
				$("#nextBtnDG").hide();
			}
	  		validaEstiloVehiculo();
	    
		}else if(jspForm=="2"){
			//Variable para recalcular la cotizacion al momento de desplegar la seccion por primera vez
			recalculoInicial = true;
			
			$('a#btn_impresion').fancybox({
				'beforeShow':function(){
					mostrarContenedorImpresionAgente()
				}
			});
			
			
			$('a#btn_Igualar').fancybox({
				'beforeClose' : function(){
					verDetalleCotizacion(true,false);
				},
				'beforeShow':verIgualarPrimasAgente()
			});
			
			configurarBotonesContinuar();
			
			$("#btnToggleCob").click(function(e){
				e.preventDefault();
				mostrarOcultarDiv('divCoberturas', $('#btnToggleCob'), null);
			});

			$("#btn_datosAdicionalesPaq").show();
			
			mostrarPagoFraccionado($("#idNegocio").val());

			$('a#btn_datosAdicionalesPaq').fancybox({
    			'closeBtn'    : false,
    			'beforeClose' : function(){
					closeModalRiesgos();
				}
			});
				
			$("#btn_datosAdicionalesPaquete").click(function(e){
				cargarRiesgosCotizacion(false);
					$('#btn_datosAdicionalesPaq').click();
				}
			);
			
			$('a#btn_EsquemasPagos').fancybox();

			$("#btn_EsquemasPagos").click(function(e){
				$('#btn_EsquemasPagos').click();
			});

			if(claveEstatusCotizacion==claveEstatusEmitido){
				$("#nextBtnDetalle").hide();
				$("#returnBtnCobertura").hide();
			}
			validaRiesgos(true);
			mostrarPorcentajeDescuentoEstado();	
			verDetalleCotizacion(false, true);		
			if($("#pctDescuentoEstado").val()=='0.0'){
				mostrarPorcentajeDescuentoEstadoDefault();
			}

			
			$("#idFormaPago").change(function(){
				cargarRecargoPagoFraccionado(this.value, $('#idMoneda').val());
				
			});
			
			$("#porcentajePagoFraccionado").change(function(){
				recalcularCotizador(true);
			});
		//	obtenerConfYEsconderCampos('divDatosPaquete', negocio);

		}else if(jspForm=="3"){
			$('a#searchClientBtn').fancybox({
				'beforeClose' : function(){
					$('#searchClientDiv').empty();
				},
				'beforeShow': buscarCliente("#serachCliente", "#nombreCliente","#idCliente")
			});

			$('#searchClientBtn').click(function(e){
				buscarCliente("#serachCliente", "#nombreCliente","#idCliente");
			});

			$('a#searchClientBtnAgen').fancybox({
				'beforeClose' : function(){
					$('#searchClientDiv').empty();
				},
				'beforeShow': buscarCliente("#serachCliente", "#nombreAsegurado","#idAsegurado")
			});

			$('#searchClientBtnAgen').click(function(e) {
				
				buscarCliente("#serachCliente", "#nombreAsegurado","#idAsegurado");
			});
			
			$('a#addClientBtn').fancybox({
				helpers: {
					overlay: { closeClick: false }
				},
				'beforeClose' : function(){
					
					$('#searchPersonaDiv').empty();
				}
			});

			$('#addClientBtn').click(function(e){
				addClienteDesdeAgente();
			});
			
			$("#nextBtnCom").click(function(e){
				validarContratanteAsignadoCotizacion();
			});
			
			/* Botones de regreso */
			$("#returnBtnCobertura").click(function(e){
				cargarForma(1);
			});
			
			$("#returnBtnComplementario").click(function(e){
				regresarDatosComp();
			});
			
			$("#returnBtnCotizacion").click(function(e){
				cargarForma(1);
			});
			
			$('#btn_copiarCliente').click(function(e){
				copiarDatosCliente();
			});
			
			$('#btn_copiarInciso').click(function(e){
				copiarIncisoDeAsegurado();
			});
			
			if(claveEstatusCotizacion==claveEstatusEmitido){
				$("#nextBtnCom").hide();
			}
			
			idToPersonaContratante = $('#idCliente').val();
			if($.isValid(idToPersonaContratante)){
				cargarDocumentFortimaxInd(true);
				if($('#claveTipoPersona').val() == personaFisica){
					$('#btn_copiarCliente').show();
				}
				if($.isValid($('#idAsegurado').val())){
					$('#btn_copiarInciso').show();
				}
			}
			
			$("#btnToggleObs").click(function(e){
				mostrarOcultarDiv('divObservaciones', $('#btnToggleObs'), null);
			});			
			
			defineCamposObligatorios(tipoCotizacion);
			
		}else if(jspForm=="4"){
			var idExcepcion = $("#excepcionesList0\\.idExcepcion").val();
			var mensajeRetorno = $('#mensajeExitoText').html();
			$('#mensajeExitoText').html('');
			if(idExcepcion==undefined){

				$('a#btn_impresion').fancybox({
					'beforeShow':mostrarContenedorImpresionAgente()
				});
				//se cargan los datos de la cobranza ya guardada
				onChangeMedios();
				
				var metodoPagoDTOs = $('#medioPagoDTOs').val();

				$("#nextBtnEmt").click(function(e){
					guardarCobranza();
				});

				$("#btn_newCotizacion").click(function(e){
					generaNuevaCotizacion('cobranzaYEmisionForm');
				});

				if($.isValid(metodoPagoDTOs)){
					btnEmitircotizacion = $("#btn_emitirCotizacion");
					btnEmitircotizacion.click(function(e){//REV
						var confirmarAP;
						var confirmadoAP;
						if ($('input[name="aviso"]').is(':checked')) {
							if(confirm('\u00BFSolicitar Emisi\u00F3n?')){
								guardarAntesDeEmitir();
							}
						} else {
							confirmarAP =   confirm("Es necesario Aceptar Aviso de Privacidad y Uso de Medios Electrónicos Seguros para continuar");
							if(!confirmarAP){ 
								confirmadoAP = confirm("Seguro que desea cancelar la emisión");
								if(confirmadoAP){
									generaNuevaCotizacion('cobranzaYEmisionForm');
									alert("Has cancelado la emisión vuelve a cotizar");
					 			}
							}
					 	}
					});

				  	$("#btn_newCotizacion").show();
					if(claveEstatusCotizacion!=claveEstatusEmitido){
						validarDatosComplementariosAgente();
					}else{
						$("#nextBtnEmt").hide();
						$("#printBtn").show();
						var mesajeTexto = $('#mensajeExitoText').html();
						$('#mensajeExito').css({'width':'290px'});
						mesajeTexto = mesajeTexto.replace(/confirm:message:La/gi, "La");
						mesajeTexto += "</br>";
						$('#mensajeExitoText').html(mesajeTexto);
						$("#printBtn").clone().appendTo("#mensajeExitoText");
					}
				}
				
				$('#idToPersonaContratante').val($('#0idToPersonaContratante').val());
				
				cargarResumenCotizacion(false, true);
			}else{
				$('#id').val($('#0idToCotizacion').val());
				$('#enviarAutorizacion').click(function(e){
					enviarAutorizacion();
				});
				
				$('#cancelarAutorizacion').click(function(e){
					cancelarAutorizacion();
				});
			}
			
			//Se agrega condicion para mostrar mensaje de error;
			messagesAfterAjax(mensajeRetorno);  //-- REV
		}
		
		actualizaPantallaTipoCotizacion(tipoCotizacion);

	}
});

//Función para mostrar mensajes despues de todas las llamadas AJAX.
function messagesAfterAjax(mensaje){
	$(document).ajaxStop(function(){
		$.closeMesaje();
		if(mensaje != undefined && mensaje != ''){
			$.loadMensajeExito(mensaje);
		}
	})
}
/*********** COTIZADOR DE AUTOS INDIVIDUAL  ******/
/**************************************** Forma 1 Datos Generales ****************************************/

/**
 * Realiza la budqueda de agente
 */
$(function(){
	$( '#descripcionBusquedaAgente' ).autocomplete({
               source: function(request, response){
               		$.ajax({
			            type: "POST",
			            url: urlBusquedaAgentes,
			            data: {descripcionBusquedaAgente : request.term},      
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( $( "item", xmlResponse ).map( function() {
								return {
									idAgente: $("idAgente",this).text(),
									value: $( "descripcion", this ).text(),
				                    id: $( "id", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 3,
               delay: 1000,
               select: function( event, ui ) {
            	   $('#idAgente').val(ui.item.id);
            	   cargaAgente();
               }		          
         });
    $('#descripcionBusquedaAgente').focus();
 });

/**
 * Carga la el agente seleeciondo de la lista para asignarlo en pantalla
 */
function cargaAgente(){
	
	if($.isValid($('#idAgente').val())){
		$('#backMSN').modal();
		$.loadMensajeInfo('Cargando cotizador...', true);
		$('#fromDatosGenerales').submit();
	}else{
		return false;
	}
}

/**
 * Valida las fechas asignadas a la cotizacion.
 */
function validaFechasCotizacion(){
	var param = {
			'cotizacion.fechaInicioVigencia'              : $('#inivig').val(),
			'cotizacion.fechaFinVigencia'                 : $('#finvig').val(),
			'cotizacion.solicitudDTO.negocio.idToNegocio' : $('#idNegocio').val(),
			'idTipoPoliza'                                : $('#idTipoPoliza').val()
	};

	if (!fechaMenorQue($('#inivig').val(), $("#finvig").val())) {
		$.loadMensajeError('La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia');
		return false;
	}
	
	if($.isValid($('#inivig').val()) && $.isValid($('#finvig').val()) && $.isValid($('#idTipoPoliza').val())){
		$.ajax({
		  	type     : 'POST',
		  	url      : urlValidaFechasCotizacion,
		  	data     : param,
	      	async    : true,
		  	dataType : 'JSON',
	    	success  : function(data) {
	    		var data = data.mapGenerico;
				if($.isValid(data.mensaje)){
					$.loadMensajeError(data.mensaje);
					if($.isValid(data.fechaFijaFinVigencia)){
						$('#finvig').val(data.fechaFijaFinVigencia);				
					}
				}				
	    	}
		});	
	}
}

function validarInicioVigencia(){
	var param = {
			'cotizacion.fechaInicioVigencia'              : $('#inivig').val(),
			'cotizacion.solicitudDTO.negocio.idToNegocio' : $('#idNegocio').val(),
	};
	if($.isValid($('#inivig').val()) && $.isValid($("#idNegocio").val())){
		$.ajax({
		  	type     : 'POST',
		  	url      : urlValidarInicioVigencia,
		  	data     : param,
	      	async    : true,
		  	dataType : 'JSON',
	    	success  : function(resp) {
	    		var fechaInicioValida = resp.inicioVigenciaValido;
	    		if(!fechaInicioValida){
	    			$.loadMensajeError(resp.mensaje);
	    			$("#inivig").val(Date.today().toString("dd/MM/yyyy"));
	    			actualizarFinVigencia();
	    		}
	    	}
		});	
	}
	
}

function actualizarFinVigencia(){
	var iniVig = $("#inivig").val();
	if(!iniVig){
		$("#finvig").val("");
	}else{
		$('#finvig').val(Date.parse(iniVig).add(1).years().toString("dd/MM/yyyy"));
	}
}


/**
 *Valida que no se pueda ingresarel estilo del vehiculo hasta aver ingresado los datos d el Vehiculo
 */
function validaEstiloVehiculo() {

	var selects = $.getObject('#divDatosVehiculo',"select", null);
	var isdisable;
	$.each(selects, function( index, select ) {
  		if(select.value == headerValue || select.value ==null){
  			$('#idEstiloVehiculo').attr('disabled','disabled');
  			isdisable=true;
  			$('#lblEstilo').html(headerValue);
  		}  			
	});

	if(!isdisable){
		$('#idEstiloVehiculo').removeAttr('disabled');	
	}
 }

/**
 * Actualiza el nombre del contratante para la cotizacion
 */
function actualizaNombreContratante(){
	var nombre = $('#nombreProspecto').val()+' '+$('#aPaterno').val()+' '+$('#aMaterno').val();
	$('#nombreContratante').val(nombre.toUpperCase());
}

/**
 * Carga la lista de los productos en pantalla deacuerdo al negocio seleccionado
 * @param idNegocio
 */
function cargaProductosNegocioAgente(idNegocio){
	if($.isValid(idNegocio)){
		listadoService.getMapNegProductoPorNegocio(idNegocio,
				function(data){
					addOptionSelect('idProducto', data, 'ASC', 'idProductoBase');
				});	
	}else{
		addOptions('idProducto',null);
	}
}

/**
 * Carga los tipo de Polizas de acuerdo al tipo de producto y negocio
 * @param idNegocioProducto
 */
function cargaTipoPolizaAgente(idNegocioProducto){
	if($.isValid(idNegocioProducto)){
		listadoService.getMapNegTipoPolizaPorNegProducto(idNegocioProducto,
				function(data){
					addOptionSelect('idTipoPoliza', data, 'ASC', 'idTipoPolizaBase');
				});	
	}else{
		addOptions('idTipoPoliza',null);
	}
}

/**
 * Carga los estados descuerdo al negocio seleccionado
 * @param idNegocio
 */
function cargarEstadoNegocio(idNegocio){
	if($.isValid(idNegocio)){
		restartCombo('idEstado', headerValue, "Cargando...", "id", "value");
		listadoService.getEstadosPorNegocioId(idNegocio, false,
			function(data){
				addOptionSelect('idEstado', data, 'ASC', null);
			});	
	}else{
		addOptions('estado',null);
	}
}

/**
 * Carga los municipios de acuerdo al estado seleccionado
 * @param target
 * @param idEstado
 */
function onChangeEstado(target, idEstado){
	if($('#codigoPostal').val() == ''){
		var idToCotizacion = null;
		if($.isValid(idEstado)){
			restartCombo(target, headerValue, "Cargando...", "id", "value");
			listadoService.getMunicipiosPorEstadoId(idToCotizacion, $('#'+idEstado).val(),
				function(data){
					addOptionSelect(target, data, 'ASC',null);
				});	
		}else{
			addOptions(target,null);
		}
	}
}

/**
 * Muestra la pantalla donde se agregran los datos del vehiculo(s) a cotizar
 */
function cargarDatosVehiculo(){
	
	var contenidoDiv = 'divDatosVehiculo';
	var param = {
			'cotizacion.solicitudDTO.negocio.idToNegocio'                  : $('#idNegocio').val(),
			'cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto' : $('#idProducto').val(),
			'cotizacion.negocioTipoPoliza.idToNegTipoPoliza'               : $('#idTipoPoliza').val(),
			'incisoCotizacion.incisoAutoCot.estiloId'                      : null,
			'&incisoCotizacion.incisoAutoCot.modeloVehiculo'               : null,
			'cotizacion.idMoneda'                                          : $('#idMonedaName').val()
	};
	
	if($.isValid($('#idNegocio').val()) && $.isValid($('#idProducto').val()) && $.isValid($('#idTipoPoliza').val())){
		sendRequestJQAgente(param, urlDatosVehiculo, contenidoDiv, "cargaDatosComplementarios('divFlotillas','divDatosVehiculo');");	
	}
}

function cargaDatosComplementarios(divFlotillas, divDatosVehiculo){

	if($.isValid($('#0idNegocioSeccion').val()))
		$('#idNegocioSeccion').val($('#0idNegocioSeccion').val());
	
	onChangeLineaNegocio('idTipoUso', 'idNegocioSeccion');
	cargarAutocmpleteExtilo();
}

function cargarAutocmpleteExtilo(){
	$(function(){
		$( '#idEstiloVehiculo' ).autocomplete({
	        source: function(request, response){
	       		$.ajax({
		            type: "POST",
		            url: urlBusquedaEstilos,
		            data: {
		            		descripcionBusquedaEstilos : request.term,
		            		idModeloVehiculo           : $('#idModeloVehiculo').val(),
		            		idMoneda          		   : $('#idMonedaName').val(),
		            		idMarcaVehiculo            : $('#idMarcaVehiculo').val(),
			            	idNegocioSeccion           : $('#idNegocioSeccion').val(),
			            	idAgrupadorPasajeros       : $('#0idAgrupadorPasajeros').val()
			              },              
		            dataType: "xml",	                
		            success: function( xmlResponse ) {
		           		response( $( "item", xmlResponse ).map( function() {
							return {
								value: $( "descripcion", this ).text(),
			                    key: $( "id", this ).text()
							}
						}));			           
	       		}
	       	})},
	       minLength: 3,
	       delay: 1000,
	       select: function( event, ui ) {
	    	   $('#estiloSeleccionado').val(ui.item.key);
	    	   $('#vehiculoEstiloName').val(ui.item.value);
	    	   $('#lblEstilo').html(ui.item.value);
	    	   $('#incisoModificadoresDescripcion').val();
	    	   $('#incisoDescripcionFinal').val(ui.item.value);
	       }		          
	 });
	$('#idEstiloVehiculo').focus();
	});
}
/**
 *Carga las caracteristicas opcionales con las que puede contar el vehiculo
 */
function cargarCaracterisitas(esFlotilla){
	var parms = null;
	if(esFlotilla){
		parms = $('#divFlotillas').serialize();
	}else{
		parms = {
			'estiloSeleccionado'        : $('#estiloSeleccionado').val(),
			'modificadoresDescripcion=' : $('#incisoModificadoresDescripcion').val()
		}
	}
	$('#featuresLink').click();
	sendRequestJQAgente(parms, urlCargarCaracteristicas, 'divModalCaracteristicas', null);
}

function definirCaracteristicas(){
	var parms = $('#otrasCaracteristicasForm').serialize();
	sendRequestJQAgente(parms, urlDefinirOtrasCaract, 'divModalCaracteristicas', null);
}

function guardarVariablesModificadoras(){
	var tipoYClase = $('#vehiculoEstiloName').val();
	tipoYClase = tipoYClase.split(" ");
	$('#incisoModificadoresDescripcion').val($("#modificadoresDescripcionName").val());
	$('#incisoDescripcionFinal').val(tipoYClase[2]+" "+tipoYClase[3]+" "+tipoYClase[4]+" "+$("#descripcionFinalName").val());
	$('.fancybox-close').click();
}

function mostrarPagoFraccionado(idNegocio){
	if(idNegocio != null  && idNegocio != headerValue){
		listadoService.mostrarPagoFraccionado(idNegocio,
				function(data){
					if(data){
						$('#divPagoFraccionado').show();
						obtenerConfYEsconderCampos('divDatosPaquete', negocio);

					}else{
						$('#divPagoFraccionado').hide();
					}
				});	
	}else{
		$('#divPagoFraccionado').hide();
	}
}

/**
 * Carga la lista de los productos en pantalla deacuerdo al negocio seleccionado
 * @param idNegocio
 */
function cargaNegocioSeccion(){
		listadoService.listarNegocioSeccionPorProductoNegocioTipoPoliza(
				$('#idProducto').val(), $('#idNegocio').val(), $('#idTipoPoliza').val(),
				function(data){
					addOptionSelect('idNegocioSeccion', data, 'ASC', '0idNegocioSeccion');
					onChangeLineaNegocio('idTipoUso', 'idNegocioSeccion');
					
				});
}
//joksrc
function limpiarTextBoxEstilo(){
	var estiloVehiculo = jQuery('#idEstiloVehiculo').val();
	if (estiloVehiculo!= null || estiloVehiculo !=''){
		jQuery('#idEstiloVehiculo').val('');
		$('#lblEstilo').html(headerValue);
	}
}

/**
 *Carga los tipos de usos deacuerdo a la marca y linea de negocio seleccionada
 * @param target
 * @param negocioSeccionSelect
 */
function onChangeLineaNegocio(target, negocioSeccionSelect){
	limpiarTextBoxEstilo();
	var idNegocioSeccion = null;
	if(negocioSeccionSelect != null){
		idNegocioSeccion = $('#'+negocioSeccionSelect).val();
		$('#0idNegocioSeccion').val(idNegocioSeccion);
	}
	
	targetTipoUso = target;
	if($.isValid(idNegocioSeccion)){
		restartCombo(target, headerValue, "Cargando...", "id", "value");
		listadoService.getMapTipoUsoVehiculoByNegocio(idNegocioSeccion,
		function(data){
			addOptionSelect(targetTipoUso, data , 'DESC', '0idTipoUsoBase');			
			listadoService.getCountNegocioEstiloVehiculo(idNegocioSeccion,$('#idMonedaName').val(),
			function(data){
				if(data > 0){
					 $('#divMessageInfo').show();
	            	 $('span[id=message]').text('Este negocio tiene estilos limitados');
				}else{
					onChangeTipoUsoVehiculo('idMarcaVehiculo', negocioSeccionSelect);
				}
			});
					
		});
	}else{
		addOptions(targetNegocioSeccion, null);
		onChangeTipoUsoVehiculo('idMarcaVehiculo', negocioSeccionSelect);
	}
}

/**
 * Carga la lista de Marcas de acuerdo a la LN seleccionada y la marca 
 * @param target
 * @param marcaVehiculoSelect
 * @param negocioSeccionSelect
 */
function onChangeTipoUsoVehiculo(target, negocioSeccionSelect){
	limpiarTextBoxEstilo();
	var idNegocioSeccion=null; 
	if(negocioSeccionSelect != null){
		idNegocioSeccion = $('#'+negocioSeccionSelect).val();
	}
	var targetMarcaVehiculo = target;			
	if($.isValid(idNegocioSeccion)){
		restartCombo(target, headerValue, "Cargando...", "id", "value");
		listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion,
			function(data){
				addOptionSelect(targetMarcaVehiculo, data, 'ASC', null);
				onChangeMarcaVehiculo('idModeloVehiculo', targetMarcaVehiculo, negocioSeccionSelect);
			}
		);
	}else{
		addOptions(targetTipoUso,null);
		onChangeMarcaVehiculo('idModeloVehiculo', targetMarcaVehiculo, negocioSeccionSelect);
	}	
}

/**
 * Carga la lista de Modelos de acuerdo a la LN seleccionada y la marca 
 * @param target
 * @param marcaVehiculoSelect
 * @param negocioSeccionSelect
 */
function onChangeMarcaVehiculo(target, marcaVehiculoSelect, negocioSeccionSelect){
	limpiarTextBoxEstilo();
	var idMarcaVehiculo = null;	
	var idNegocioSeccion = null;
	if(marcaVehiculoSelect != null){
		idMarcaVehiculo = $('#'+marcaVehiculoSelect).val();
	}	
	
	if(negocioSeccionSelect != null){
		idNegocioSeccion = $('#'+negocioSeccionSelect).val();
	}
	
	var idMoneda = $('#idMonedaName').val();
	var targetModelo = target;
	if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idMarcaVehiculo != headerValue){
		restartCombo(target, headerValue, "Cargando...", "id", "value");
		listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(idMoneda,idMarcaVehiculo, idNegocioSeccion,
			function(data){
				addOptionSelect(targetModelo,data, 'DESC', null);
			}
		);	
	}	
}

/**
 * Realiza el guardado de la cotizacion actual
 */
function guardarCotizacion(){
	if(validaformaActual()
			&& validarCorreo()) {
		$('#backMSN').modal({backdrop: "static"});
		$.loadMensajeInfo('Guardando Cotizaci\u00F3n', true);
		var nombre = $('#nombreProspecto').val();
		var apellidoPaterno = $('#aPaterno').val();
	    var apellidoMaterno = $('#aMaterno').val();
		var urlguardaCotizacion = urlSaveCotizacion;
		
		
		$('#nombreContr').attr("value", nombre.trim());
		$('#apellidoPatCont').attr("value", apellidoPaterno.trim()); 
		$('#apellidoMatCont').attr("value", apellidoMaterno.trim());
		
		if($('#numeroSerie').val() != '' && $('#numeroSerie').val().length == 17){
			$('#noSerieVin').val($('#numeroSerie').val());
		}
		
		if((nombre !== undefined && nombre.trim().length > 0) || (apellidoPaterno !== undefined && apellidoPaterno.trim().length > 0) || (apellidoMaterno !== undefined && apellidoMaterno.trim().length > 0)){
			urlguardaCotizacion += '?nombreContr=' + nombre.trim() +'&apellidoPatCont=' + apellidoPaterno +'&apellidoMatCont=' +apellidoMaterno;
		}
		
		$('#fromDatosGenerales').attr('action', urlguardaCotizacion);
		$('#fromDatosGenerales').submit();
      } else {
    	  $('html,body').scrollTop($('.has-error').first().offset().top);
      }
}

//MLA metodo para validar el correo para las ligas de agente 
function validarCorreo(){
	var configuracionId = $("#configuracionId").val();
	if(configuracionId){
		$("#correoContratante").val($("#correoContratante").val().trim());
		var campoCorreo = $("#correoContratante").val();
		if(campoCorreo && /^([\w\.\xF1\-]+@[\w\.\xF1\-]+\.[\w]{2,3})?$/.test(campoCorreo)){
			$("#correoContratanteDiv").removeClass("has-error");
			return true;
		}
		$("#correoContratanteDiv").addClass("has-error");
		return false;
	}
	return true;
}//MLA

/**************************************** Fin Forma 1 Datos Generales ****************************************/

/**************************************** Forma 2 Paquete ****************************************/

function mostrarPorcentajeDescuentoEstado(){
	var idToNegPaqueteSeccion = $('#idPaquete').val();
	if($.isValid(idToNegPaqueteSeccion)){
		listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion,function(data){
			if(data){
				if(!$('#porcentajeDescuentoEstado').is(":visible")){
					$('#porcentajeDescuentoEstado').show("slow");
				}
			} else {
				if($('#porcentajeDescuentoEstado').is(":visible")){
					$('#porcentajeDescuentoEstado').hide("slow");
				}
			}
		});
	}
}

function resetPrimaResumen(){
	$('#div_resumenCostosDTO_resumen\\.primaTotal').html('Calculando...')
	$('#div_resumenCostosDTO\\.primaTotal').html('Calculando...')
	ocultarBotonSiguiente();	
}

function ocultarBotonSiguiente(){
	jQuery('#nextBtnDetalle').hide();
	jQuery('#returnBtnCobertura').hide();
	//Boton finalizar	
	if(jQuery("#nextBtnDetalleFinalizaCot").length > 0 )
	{
		jQuery('#nextBtnDetalleFinalizaCot').hide();		
	}
}

function mostrarBotonSiguiente(){
	jQuery('#nextBtnDetalle').show();
	jQuery('#returnBtnCobertura').show();
	
		
	//Boton finalizar
	if(jQuery("#nextBtnDetalleFinalizaCot").length > 0 )
	{
		jQuery('#nextBtnDetalleFinalizaCot').show();	
	}
}

function mostrarPorcentajeDescuentoEstadoDefault(){
    var targetPctDescuentoEstado = $("#pctDescuentoEstado");
	var idToNegPaqueteSeccion = $('#idPaquete').val();
	if($.isValid(idToNegPaqueteSeccion)){
		listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion,function(data){
			if(data){
				$('#porcentajeDescuentoEstado').show("slow");
				obtenerPctDescuentoEstadoDefault(targetPctDescuentoEstado);
			} else {
				$(targetPctDescuentoEstado).val("0.00");
				$('#porcentajeDescuentoEstado').hide("slow");
			}
		});
	}
	cambiarPaquete();
}


function agregarDomicilio(){
	jQuery("#cliente\\.domicilioTemp\\.colonia").val(jQuery("#cliente\\.nombreColonia").val());
	jQuery("#cliente\\.domicilioTemp\\.cvePais").val(jQuery("#cliente\\.idPaisString").val());
	jQuery("#cliente\\.domicilioTemp\\.cveEstado").val(jQuery("#cliente\\.idEstadoDirPrin").val());
	jQuery("#cliente\\.domicilioTemp\\.cveCiudad").val(jQuery("#cliente\\.idMunicipioDirPrin").val());
	jQuery("#cliente\\.domicilioTemp\\.calle").val(jQuery("#cliente\\.nombreCalle").val());
	jQuery("#cliente\\.domicilioTemp\\.numero").val(jQuery("#cliente\\.numeroDom").val());
	jQuery("#cliente\\.domicilioTemp\\.codigoPostal").val(jQuery("#cliente\\.codigoPostal").val());
	
	jQuery("#cliente\\.idPaisString").val("PAMEXI");
	jQuery("#cliente\\.idEstadoDirPrin").val("");
	jQuery("#cliente\\.idMunicipioDirPrin").val("");
	jQuery("#cliente\\.nombreColonia").val("");
	jQuery("#cliente\\.codigoPostal").val("");
	jQuery("#cliente\\.numeroDom").val("");
	jQuery("#cliente\\.nombreCalle").val("");
	jQuery("#cliente\\.nombreColoniaDiferente").val("");
	jQuery("#bttnDomAceptar").hide();
	jQuery("#bttnDomCancelar").show();
}

function cancelarDomicilio(){
	
	jQuery("#bttnDomAceptar").show();
	jQuery("#bttnDomCancelar").hide();
	
	

	
	
	jQuery("#cliente\\.idPaisString").val(jQuery("#cliente\\.domicilioTemp\\.cvePais").val());
	onChangePais('cliente.idEstadoDirPrin','cliente.idMunicipioDirPrin','cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idPaisString');
	jQuery("#cliente\\.idEstadoDirPrin").val(jQuery("#cliente\\.domicilioTemp\\.cveEstado").val());
	onChangeEstadoGeneral('cliente.idMunicipioDirPrin','cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idEstadoDirPrin');
	jQuery("#cliente\\.idMunicipioDirPrin").val(jQuery("#cliente\\.domicilioTemp\\.cveCiudad").val());
	onChangeCiudad('cliente.nombreColonia','cliente.codigoPostal','cliente.nombreCalle','cliente.idMunicipioDirPrin');
	jQuery("#cliente\\.nombreColonia").val(jQuery("#cliente\\.domicilioTemp\\.colonia").val());
	onChangeColonia('cliente.codigoPostal','cliente.nombreCalle', 'cliente.nombreColonia','cliente.idMunicipioDirPrin');
	jQuery("#cliente\\.numeroDom").val(jQuery("#cliente\\.domicilioTemp\\.numero").val());
	jQuery("#cliente\\.nombreCalle").val(jQuery("#cliente\\.domicilioTemp\\.calle").val());
	jQuery("#cliente\\.codigoPostal").val(jQuery("#cliente\\.domicilioTemp\\.codigoPostal").val());
	
}

function habilitarActualizarDerecho(){
	paqueteModificado = true;
}

function actualizarListadoDerechos(){
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	var valorActualDerecho = jQuery("#idDerecho").val();
	var valorDefault;
	
	if(idToCotizacion && numeroInciso){
		listadoService.getDerechoPolizaDefault(idToCotizacion, numeroInciso,function(data){
			valorDefault = data;
			listadoService.getMapDerechosPolizaByNegocio(idToCotizacion, numeroInciso,function(data){
				addOptionAndSelect('idDerecho', data, valorDefault);
				recalcularCotizador(false);
				if(valorDefault != valorActualDerecho){
					$.loadMensajeInfo("Derechos actualizados. Favor de validarlos", false);
				}
			});
		});
	}
}

function cargarRecargoPagoFraccionado(value, idMoneda){		
	if($.isValid(value)){
		listadoService.getPctePagoFraccionado(value, idMoneda, function(data){
			$('#porcentajePagoFraccionado').val(data);
			recalcularCotizador(true);
		});
	}else{
		jQuery('#porcentajePagoFraccionado').val('');
		recalcularCotizador(true);
	}
}

function obtenerPctDescuentoEstadoDefault(targetPctDescuentoEstado) {
	var idToNegocio = $('#idNegocio').val();
	var idEstado = $("#idEstado").val();
	if($.isValid(idToNegocio) && $.isValid(idEstado)){
		listadoService.getPcteDescuentoDefaultPorNegocio(idToNegocio, idEstado,function(data){
			if(data){
				$(targetPctDescuentoEstado).val(data);
			}else{
				$(targetPctDescuentoEstado).val("0.00");
			}
		});	
	}
}

function validaPorcentajeDescuentoEstado(target) {
	var idToNegocio = $('#idNegocio').val();
	var idEstado = $("#idEstado").val();
	var descuento = $("#pctDescuentoEstado").val();
	if ( descuento > 100){
		$.loadMensajeError('El Descuento por Estado no es v\u00e1lido.');
		$("#pctDescuentoEstado").val("0.00");
	} else {
		listadoService.getPcteDescuentoMaximoPorEstado(idToCotizacion, idToNegocio, idEstado,function(data){
			if(descuento > data){
				$.loadMensajeError('Se sugiere que el m\u00e1ximo porcentaje de Descuento por Estado sea de ' + data + ' %');
			}
			recalcularCotizador(true);
		});
	}
}

//MLA Se agrega metodo para limitar el descuento por estado máximo capturado dependiendo de la configuracion del negocio
function validaPorcentajeDescuentoEstadoLigaAgente(){
	var idToNegocio = $('#idNegocio').val();
	var idEstado = $("#idEstado").val();
	var descuento = $("#pctDescuentoEstado").val();
	listadoService.getPcteDescuentoMaximoPorEstado(idToCotizacion, idToNegocio, idEstado,function(data){
		if(descuento > data){
			$("#pctDescuentoEstado").val(data);
			$.loadMensajeError('El m\u00e1ximo porcentaje de Descuento por Estado es de ' + data + ' %');
		}
		recalcularCotizador(true);
	});
}//MLA

/**
 * Carga el resumen de la cotizacion en pantalla 
 */
function cargarResumenCotizacion(isAjax, isCobranza){

	var configuracionIdEnCoberturas = $("#configuracionIdEnCoberturas").val();
	var param = {
		"cotizacion.idToCotizacion" : $("#idToCotizacion").val(),
		"configuracionIdResumen" : configuracionIdEnCoberturas,
		};
	
	var divResumenCotizacion=null;
	var funcion = isCobranza?"$('#divButtonResumen').hide();$('#divBienAsegurado').html($('#0bienasegurado').val());":"verEsquemaCotizacion("+isAjax+");$('#divBienAsegurado').html($('#0bienasegurado').val());";
	$('.wwgrp').css('padding','0');
	$('input[type=checkbox]').removeClass('');	
	if(isAjax){
		cargarResumenCostos(urlCargarResumenTotalCotizacionAjax, param);
//solo estos se quedan para divResumenDiv
	}else{
		if(tipoCotizacion!=COTIZACION_SERVICIO_PUBLICO){
			divResumenCotizacion = "divResumen";
		}else{
			divResumenCotizacion = (yaRecalculo || jspForm!="2"?"divResumen":null);
		}
		
		sendRequestJQAgente(param, urlCargarResumenTotalCotizacion, divResumenCotizacion, funcion);	

	}
	
	obtenerConfYEsconderCampos('divResumenDiv', negocio);
	
	
}

/**
 * Actualiza los datos del Resumen de costos mediante ajax
 */
function cargarResumenCostos(url, param){

	$.ajax({
		  	type     : 'POST',
		  	url      : url,
		  	data     : param,
	      	async    : true,
		  	dataType : 'JSON',
	    	success  : function(data) {
				var resumenCostos = data.resumenCostosDTO;
				if($.isValid(resumenCostos)){ 
					if(data.statusExcepcion){
						$("#btn_IgualarDiv").attr('style', 'visibility:hidden;');
						$("#btn_EsquemasPagosDiv").attr('style', 'visibility:hidden;');
						$("#btn_impresionDiv").attr('style', 'visibility:hidden;');
						$("#btn_MostrarResumenDiv").attr('style', 'visibility:hidden;');
						$('#btn_mostrarResumen').attr('disabled', true);
						$('#nextBtnDetalle').attr('disabled', true);
						$('#div_resumenCostosDTO\\.primaNetaCoberturas').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.descuentoComisionCedida').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.totalPrimas').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.recargo').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.derechos').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.iva').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.primaTotal').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO_resumen\\.primaTotal').html("0.00").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$.loadMensajeError(data.mensajeExcepcion);
						//mostrarExcepcion(data.idExcepcion);
					} else {
						$("#btn_IgualarDiv").attr('style', 'visibility:visible;');
						$("#btn_EsquemasPagosDiv").attr('style', 'visibility:visible;');
						$("#btn_impresionDiv").attr('style', 'visibility:visible;');
						$("#btn_MostrarResumenDiv").attr('style', 'visibility:visible;');
						$('#btn_mostrarResumen').attr('disabled', false);
						$('#nextBtnDetalle').attr('disabled', false);
						$('#div_resumenCostosDTO\\.primaNetaCoberturas').html(resumenCostos.primaNetaCoberturas).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.descuentoComisionCedida').html(resumenCostos.descuentoComisionCedida).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.totalPrimas').html(resumenCostos.totalPrimas).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.recargo').html(resumenCostos.recargo).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.derechos').html(resumenCostos.derechos).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.iva').html(resumenCostos.iva).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO\\.primaTotal').html(resumenCostos.primaTotal).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
						$('#div_resumenCostosDTO_resumen\\.primaTotal').html(resumenCostos.primaTotal).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
					}										
				}	    		
	    	},
		  	complete : function(data){
				verEsquemaCotizacion(true);
				mostrarBotonSiguiente();
		  	}
	});

}

/**
 * Carga el esquema de la cotizacion
 */
function verEsquemaCotizacion(isAjax){
	if(tipoCotizacion!=COTIZACION_SERVICIO_PUBLICO){
		var primaTotal = '';
		var totalPrima = '';
		var derechosId = '';	
		
		try{
			var iva     	=  $("#porcentajeIva").val();
			var derechosText  =  $("#div_resumenCostosDTO\\.derechos").text();
			if($.isValid(derechosText)){
				derechosId = derechosText.trim().replace('$','').replace(',','');
			}			
			var primaTotalText = $('#div_resumenCostosDTO\\.primaTotal').text();
			if($.isValid(primaTotalText)){
				primaTotal = primaTotalText.trim().replace('$','').replace(',','');
			}
			var totalPrimaText = $('#div_resumenCostosDTO\\.totalPrimas').text();
			if($.isValid(totalPrimaText)){
				totalPrima = totalPrimaText.trim().replace('$','').replace(',','');
			}
			
			var param = {
					"cotizacion.fechaInicioVigencia"     : $("#fechaIni").val(),
					"cotizacion.fechaFinVigencia"        : $("#fechaFin").val(),
					"cotizacion.idFormaPago"             : $("#idFormaPago").val(),
					"cotizacion.valorTotalPrimas"        : totalPrima,
					"cotizacion.valorDerechosUsuario"    : derechosId,
					"cotizacion.porcentajeIva"           : iva,
					"cotizacion.valorPrimaTotal"         : primaTotal,
					"cotizacion.idToCotizacion"          : idToCotizacion,
					"cotizacion.porcentajebonifcomision" : $('#comision').val()
			};
			
			$('#btn_Igualar').show();
			$('#btn_EsquemasPagos').show();
			
			
			if(isAjax){
				cargarDatosEsquema(urlVerEsquemaPagoAgenteAjax, param);
				
			}else{
				$('#divCoberturas').hide();
				$('body').removeAttr('style');
				sendRequestJQAgente(param, urlVerEsquemaPagoAgente, "divEstimacion",null);
			}
			
		}catch(e){
			alert(e);
		}
	}else{
		if(!yaRecalculo)
			recalculaPrimaConf($('#0primaTarifa').val(), idToCotizacion, null, "verDetalleCotizacion(false,false);$('body').removeAttr('style');");
		else
			$('body').removeAttr('style');
			$('#divCoberturas').find('select, input').attr('disabled', 'disabled');
	}
}

/**
 *Carga los datos del Esquema de pago actualizando solo los datos con ajax
 */
function cargarDatosEsquema(url, param){
	$.ajax({
			type     : 'POST',
			url      : url,
			data     : param,
	    	async    : true,
		  	dataType : 'JSON',
	    	success  : function(data) {
				var esquemaPagoCotizacionList = data.esquemaPagoCotizacionList;
				if($.isValid(esquemaPagoCotizacionList)){
					for(var i in esquemaPagoCotizacionList){
						var esquema = esquemaPagoCotizacionList[i];
						if($.isValid(esquema.pagoSubsecuente)){
							$('#pagoSubsecuente\\.numExhibicion_'+i).html(esquema.pagoSubsecuente.numExhibicion);
							$('#pagoSubsecuente\\.importeFormat_'+i).html(esquema.pagoSubsecuente.importeFormat);
						}
						if($.isValid(esquema.pagoInicial)){
							$('#pagoInicial\\.importeFormat_'+i).html(esquema.pagoInicial.importeFormat);
						}else{
							$('#pagoInicial\\.importeFormat_'+i).html(0);
		  					$('#pagoSubsecuente\\.importeFormat_'+i).html("");
						}
					}
				}
	    	},
			complete : function(data){
				$('body').removeAttr('style');
				if(jspForm = '2' && recalculoInicial){
					recalculoInicial = false;
					recalcularCotizador(false);
					
				}
		  	}
	    	
	}); 
	
}

/**
 * Muestra la tabla con el detalle de la cotizacion
 */
function verDetalleCotizacion(isAjax, cambiaCoberturas){

	var cotizacionAsignada = 11;

	$('body').css('cursor','progress');	
	if(!isAjax) 
		mostrarIndicadorCarga("divResumen"); 
	
	var param = {
		'incisoCotizacion.id.idToCotizacion'              : idToCotizacion,
		'incisoCotizacion.id.numeroInciso'                : numeroInciso,
		'incisoCotizacion.incisoAutoCot.negocioPaqueteId' : $('#idPaquete').val(),
		'configuracionIdEnCoberturas'					  : $('#configuracionId').val()
	};
	
	if(isAjax){
		if(cambiaCoberturas)
			sendRequestJQAgente(param, urlVerDetalleIncisoAgente, 'divCoberturas', "cargarResumenCotizacion(true, false);");
		else	
			cargarDetalleCotizacion(urlVerDetalleIncisoAgenteAjax, param);
	}else{
		sendRequestJQAgente(param, urlVerDetalleIncisoAgente, 'divCoberturas', "cargarResumenCotizacion("+isAjax+", false);");	
	}
	
}

function cargarDetalleCotizacion(url, param){

	$.ajax({
			type     : 'POST',
			url      : url,
			data     : param,
	    	async    : true,
		  	dataType : 'JSON',
	    	success  : function(data) {
				var coberturaCotizacionList = data.coberturaCotizacionList;
				var requiereDatosAdicionales = data.requiereDatosAdicionales;
				var incisoAutoCot = data.incisoCotizacion.incisoAutoCot;
				var configuracionId = data.configuracionId;
				if($.isValid(requiereDatosAdicionales)){
					$('#requiereDatosAdicionales').val(requiereDatosAdicionales);
				}
				if($.isValid(coberturaCotizacionList)){
					for(var i in coberturaCotizacionList){
						var coberturaCotizacion = coberturaCotizacionList[i];
						if($.isValid(coberturaCotizacion))
							$('#coberturaCotizacionList\\.valorPrimaNeta_'+i).html(coberturaCotizacion.valorPrimaNeta).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1 });;
							$('#coberturaCotizacionList'+i+'porcentajeDeducible').val(coberturaCotizacion.porcentajeDeducible);
		  				
					}
				}
				if($.isValid(incisoAutoCot)){
					$('#pctDescuentoEstado').val(incisoAutoCot.pctDescuentoEstado);	
				}		
				if($.isValid(configuracionId)){
					$('#configuracionId').val(configuracionId);
				}
	    	},
			complete : function(data){
					cargarResumenCotizacion(true, false);				
		  	}
	});	
	
}

function cambiarPaquete(){
		
	var param = {
		'incisoCotizacion.id.idToCotizacion'              : idToCotizacion,
		'incisoCotizacion.id.numeroInciso'                : numeroInciso,
		'incisoCotizacion.incisoAutoCot.negocioPaqueteId' : $('#idPaquete').val()
	};
	
	resetPrimaResumen();
	sendRequestJQAgente(param, urlVerDetalleIncisoAgente, 'divCoberturas', "recalcularCotizador(true);");
}

function recalcularCotizador(cambiaCoberturas){
	
	if(validaDeducibles('#divCoberturas', 'select', '.mandatory')){
	    var formaDePago = $('#idFormaPago').val();
	    var derechos    = $('#idDerecho').val();
		$('body').css('cursor','progress');
		$('#isJson').val(true);
	    if(formaDePago == headerValue || derechos == headerValue){
	    	$.loadMensajeError('Favor de Capturar los campos Forma de pago y/o Derechos antes de Generar la cotizaci\u00F3n');
	      	return false;
	    }
		
		$.ajax({
				type     : 'POST',
				url      : urlSaveCotizacion,
				data     : $('#detalleCoberturasForm').serialize(),
		    	async    : true,
		  		dataType : 'JSON',
		  		beforeSend : function(data){
		  			resetPrimaResumen();
		  		},
		    	success  : function(data) {
					var requiereDatosAdicionales = data.requiereDatosAdicionales;
					var tipoMensaje = data.tipoMensaje;
					var mensaje= data.mensaje;
					if($.isValid(requiereDatosAdicionales)){
						$('#requiereDatosAdicionales').val(requiereDatosAdicionales);
					}
				},
				complete : function(data){					
					cargarRiesgosCotizacion(false);
					verDetalleCotizacion(true, cambiaCoberturas);
					if(paqueteModificado){
						paqueteModificado = false;
						actualizarListadoDerechos();
					}	
			  	}
		});	
	}

}		

function aplicaRedondeo(index){
	$("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
}

function habilitaRegistroCobertura(index){
	if(index!=null){
		if($("#claveContratoBoolean_"+index).is(":checked")){
			$("#valorSumaAseguradaStr_"+index).removeAttr("readonly");
			$("#valorDeducible_"+index).removeAttr("readonly");
		}else{
			$("#valorSumaAseguradaStr_"+index).attr("readonly","readonly");
			$("#valorDeducible_"+index).attr("readonly","readonly");
		}
	}
	recalcularCotizador(false);
}

function validaDisable(combo, index){
	var DIAS_SALARIO=52.59;
	var sumaAsegurada=$('#numeroAcientos').val()*$('#idValDiasSalario').val()*DIAS_SALARIO;
	$('#idSumaAsegurada').val(sumaAsegurada);
	if($("#claveContratoBoolean_"+index).is(":checked") || $('#coberturaCotizacionList_'+index+'__claveObligatoriedad').val() == '0'){

	}else{
		$.loadMensajeError("Se necesita contratar la cobertura antes de modificar el valor");
		combo.selectedIndex = 0;		
	}
	
}

function validaDeducible(input){
	if(input.value>0){
		recalcularCotizador(false);
	}
}

 //inicia Modal Riesgos

function validaRiesgos(cargarAuto){	
	var riesgos = $('#requiereDatosAdicionales').val();
	if(riesgos)		
		cargarRiesgosCotizacion(cargarAuto);
}

function cargarRiesgosCotizacion(cargarAuto){

	var param = {
		"idToCotizacion" : idToCotizacion,
		"numeroInciso"   : numeroInciso
	};
	
	sendRequestJQAgente(param, urlViewRiesgos, 'divDatosAdicionalesPaquete', "cargarDatosRiesgo("+cargarAuto+")");
}

function cargarDatosRiesgo(cargarAuto){
	$('input, select, textarea').removeAttr('style').css("float","left");
	$('input, select, textarea').removeClass('txtfield jQrequired').addClass('form-control');	
	$('label').removeClass('titulo').css("color", "#000");
	$('#loading').css('display', 'none');
	$('#complementarIncisoForm_cotizacionId').val(idToCotizacion);
	$('#complementarIncisoForm_incisoId').val(numeroInciso);

	if($('#complementarIncisoForm').find('label').html()=="No se encontraron datos de riesgo&nbsp;\n"){
		$('#b_guardarRiesgos').hide();
		$('#b_CerrarRiesgos').show();
	}else{
		if(cargarAuto){
			$('#msnRiesgo').show('slow');
			$('#msnRiesgoText').html('Es necesario ingresar los Datos Adicionales del Paquete para Cotizar');
		}
	}
		
	if(validaInputsRiesgos())
		$('#btn_datosAdicionalesPaq').click();
}

function closeModalRiesgos(){
	var riesgos =  validaInputsRiesgos();
	if(riesgos){
		$('#mensajeRiesgoError').show('slow');
		$('#mensajeRiesgoErrorText').html('Favor de llenar todos los campos');
		//$('#btn_datosAdicionalesPaq').click();
	}else{
		recalcularCotizador(false);
	}
}

// valida que los inputs del formulario esten llenos
function validaInputsRiesgos(){
	var valor = false;
	if($('#complementarIncisoForm').find('label').html()!="No se encontraron datos de riesgo&nbsp;\n"){
		var valor1 = validaObjetos('#complementarIncisoForm', 'select', null);
		var valor2 = validaObjetos('#complementarIncisoForm', 'input[type!=checkbox]', null);
		var valor3 = validaObjetos('#complementarIncisoForm', 'textarea', null);
		if(valor1 || valor2 || valor3)
			valor = true;
	}else{
		return false;	
	}
	return valor;
}

function validaGuardarRiesgos(){
	var riesgos =  validaInputsRiesgos();
	if(riesgos){
			$('#mensajeRiesgoError').show('slow');
			$('#mensajeRiesgoErrorText').html('Favor de llenar todos los campos');
	}else{
		guardarRiesgosCotizacion();
	}
}

function guardarRiesgosCotizacion(){
	yaRecalculo=false;
	var param = $('#complementarIncisoForm').serialize();
	sendRequestJQAgente(param, urlSaveRiesgos, 'divDatosAdicionalesPaquete', "cargarDatosRiesgo("+false+");$('#b_CerrarRiesgos').click();");
} 

//fin Modal Riesgos

// Modal Impresion cotizacion 

/**D
 * Muestra la ventana para la impresion
 */
function mostrarContenedorImpresionAgente(){
	var tipoImpresion = 1;
	var div = $('#divMsnImpresion');
	div.hide();
	if($("#validacionesPendientes").val() === 'true'){
		div.addClass('alert alert-warning');
		div.show('slow');
		$('#divMsnImpresionText').html("La Cotizaci\u00f3n tiene Solicitudes de Autorizaci\u00f3n Pendientes. No es posible imprimirla en este momento.");
		return;
	}
	var param = '?id='+$('#idToCotizacion').val()+ "&tipoImpresion="+tipoImpresion;	
	$('#iframeImpresion').attr('src', urlImpresionModal+param);
	cargarDatosImpresion();
}

/**
 * Carga los datos iniciales para la descarga de la cotizacion
 */
function cargarDatosImpresion(){
	setIncios();
	if( $('#txtIncios').val() != "" ){
		$('#chkinciso').hide();	
		$('#txtIncios').attr('readonly',true);
	}

}

function setIncios(){
	if($("#chkinciso").is(":checked")){
     $("#txtIncios").val("");		
  }
 }
 
 function setInciosTxt(valor){
  if(valor.length > 0){
	 $("input[name$='chkinciso']").removeAttr("checked");		
  }	
 }
 
 //fin Modal Impresion
 
 // Modal Igualar Primas

 function verIgualarPrimasAgente(){
	 var param = '?idToCotizacion='+$('#idToCotizacion').val();
		
	$('#iframeIgualaPrimas').attr('src', urlVerIgualarPrimasAgente+param);
	$('#ip_idToCotizacion').val($('#idToCotizacion').val());
 }
 
function ejecutarIgualacion(){
	var idControlArchivo = $("#file1").val();
	var div = $('#divMsnIgualarPrima');
	div.hide();
	if(!$.isValid(idControlArchivo)){
		div.addClass('alert alert-danger');
		div.show('slow');
		$('#divMsnIgualarPrimaText').html("Favor de Cargar el Documento para la Igualacion de la cotizaci\u00F3n");
		return false;
	}	
	if(!$.isValid($('#primaAIgualar').val())){
		div.addClass('alert alert-danger');
		div.show('slow');
		$('#divMsnIgualarPrimaText').html('Favor de ingresar el monto de la prima total');
		return false;
	}
	
	if(confirm('\u00BFEst\u00e1 seguro que desea igualar la prima de la cotizaci\u00F3n\u003F')){
		recalculaPrimaConf($('#primaAIgualar').val(), $('#ip_idToCotizacion').val(), 'documentoPrimaObjetivo', "parent.cerrarModal()");
	}
}
function recalculaPrimaConf(primaAIgualar, idToCotizacion, targetId, funtion){
	var param = {
			'primaAIgualar'  : primaAIgualar,
			'idToCotizacion' : idToCotizacion
	};
	if(tipoCotizacion==COTIZACION_SERVICIO_PUBLICO)
		yaRecalculo = true;
	
	sendRequestJQAgente(param, urlIgualarPrimasAgente, targetId, funtion);
	
}

function subirArchivo(){
	var param = '?idToCotizacion='+$("#ip_idToCotizacion").val() + "&idControlArchivo="+idToControlArchivo;
		new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    		    	var pram = '?idToCotizacion='+$("#idToCotizacion").val() + "&idControlArchivo="+idToControlArchivo;
    		    	recalcularcotizacion();
    			}
    		} // End of onSuccess    		
    	});
}

function recalcularcotizacion(){
	var param = $('#igualarPrimasForm').serialize();
	sendRequestJQAgente(param, urlIgualarPrimasAgente, 'documentoPrimaObjetivo', null);
}

function eliminarIgualacion(){
	if(confirm('\u00BFEst\u00e1 seguro que desea restaurar la prima de la cotizaci\u00F3n\u003F')){
		var param = $('#igualarPrimasForm').serialize();
		sendRequestJQAgente(param, urlRestaurarPrima, 'documentoPrimaObjetivo', "parent.cerrarModal()");
	}		
}

function setValoresArchivoPrima(id, nombre){
	$("#controlArchivoId").val(id);
	$("#nombreArchivoPrima").val(nombre);
}

 //fin Modal Impresion
 
/**
 * Finaliza el llenado de la forma Paquete y muestra la forma de Datos Complementario
 */
function guardarDetalleCobertura(finalizaCotizacion){
   var riesgos = $.isValid($('#requiereDatosAdicionales').val());
	var datosAdicionales = riesgos?(!validaInputsRiesgos()):true;
	if(datosAdicionales){
		if(validaformaActual()) {
			$.loadMensajeInfo('Guardando Cotizaci\u00F3n', true);
			$('#detalleCoberturasForm').attr('action', urlMostrarContenedor);
			$('#jspForm').val(3);
			jQuery('#finalizaCotizacion').val(finalizaCotizacion);
			$('#detalleCoberturasForm').submit();
	    } else {
	    	$('html,body').scrollTop($('.has-error').first().offset().top);
	    }	
	}
}

 /**************************************** Fin Forma 2 Paquete ****************************************/
 
 /**************************************** Forma 3 Datos Complementarios ****************************************/

/**
 *Carga Pantalla Para busqueda de cliente
 */
function buscarCliente(forma, nombre, id){
	
	sendRequestJQAgente(null, urlViewSearchCliente+'?tipoBusqueda=3', 'searchClientDiv', "buscarClienteFunction('"+forma+"', '"+nombre+"', '"+id+"');");
}

function buscarClienteFunction(forma, nombre, id){
	if (negocioTipoPersona == "1") {
		$('#divPersonaFisica').show();
		$('#divPersonaMoral').hide();
		$('#divTipoPersonaBuscarCliente').hide();
		$('#divPersonaMoral').find('input').val("");
		$('input[name="filtroCliente.claveTipoPersona"]').val(1);
	}else if (negocioTipoPersona == "2") {
		$('#divPersonaFisica').hide();
		$('#divPersonaMoral').show();
		$('#divTipoPersonaBuscarCliente').hide();
		$("#divPersonaFisica").find('input').val("");
		$('input[name="filtroCliente.claveTipoPersona"]').val(2);
	}else{
	$('#claveTipoPersona1').click(function(){
		$('#divPersonaFisica').show();
		$('#divPersonaMoral').hide();
		$('#divPersonaMoral').find('input').val("");
    });
	
	$('#claveTipoPersona2').click(function(){
		$("#divPersonaFisica").hide();
		$("#divPersonaMoral").show();
		$("#divPersonaFisica").find('input').val("");
	});}
	$('#searchClientDiv').find(".datepicker").datepicker({format: 'dd/mm/yyyy', language: 'es', todayHighlight: 'true', autoclose: 'true'});

	$.setName('#nombreCliente','#busNombre','#busNombreaPaterno','#busNombreaMaterno');
	
	

	$('#btn_filterSearchClient').click(function(){
			listaFiltrada(urlSearchCliente, 'clienteGrid', 'divCarga', forma, nombre, id);
	});
}


/**
 * Selecciona la persona para asignar como el mismo y/o Agente
 */
function doOnRowDblClicked(grid,  nombre, id) {
	var selectedId = grid.getSelectedRowId();
	var id_        = grid.cellById(selectedId, 0).getValue();
	var nombre_    = grid.cellById(selectedId, 1).getValue();
	doblClic       = true;
	$(nombre).val(nombre_);
	$(id).val(id_);
	
	if(nombre=='#nombreAsegurado'){
		$('#saveAsegurado').val(1);
		var param = {
			"incisoCotizacion.incisoAutoCot.personaAseguradoId"  : $('#idAsegurado').val(),
			"incisoCotizacion.incisoAutoCot.nombreAsegurado"     : $('#nombreAsegurado').val()
		};
		if($("#claveTipoPersona1")[0].checked){
			$('#btn_copiarInciso').show();
		}
		guardarAsegurado(param, false);
		cerrarModal();
	}else if(nombre=='#nombreRepresentanteFiscal'){
		$('#searchPersonaDiv').hide();
		$('#searchPersonaDiv').empty();
		$('#altaCliente').show();
	}else{
		var direccion = grid.cellById(selectedId, 6).getValue();
		if(!$.isValid(direccion)){
			complementarCliente(id_,$("#claveTipoPersona1")[0].checked);
			return;
		}

		if($("#claveTipoPersona1")[0].checked){
			$('#btn_copiarCliente').show();	
		}
		guardarClienteCotizacion(true);

	}
}

function complementarCliente(idCliente, isPersonaFisica){
	var param = {
		"cliente.idCliente"   : idCliente,
		"tipoRegreso" 		  : 5
	};	
	
	$('addClientDiv').show();
	sendRequestJQAgente(param, urlSearchClientebyId, "addClientDiv", 'afterModalAddCliente('+true+','+isPersonaFisica+');$("#addClientBtn").click();');
	
}
function complementarClienteDupplicados(idCliente, isPersonaFisica){
	var param = {
		"cliente.idCliente"   : idCliente,
		"tipoRegreso" 		  : 5
	};	
	
	$('addClientDiv').show();
	sendRequestJQAgente(param, urlSearchClientebyId, "addClientDiv", 'afterModalAddCliente('+true+','+isPersonaFisica+');');
	//sendRequestJQAgente(param, urlSearchClientebyId, "addClientDiv", 'afterModalAddCliente('+true+','+isPersonaFisica+');');
	
}
/**
 * Copia los datos del cliente para el asegurado  y los guarda en el inciso
 */
function copiarDatosCliente(){
	$('#saveAsegurado').val(1);
	$('#nombreAsegurado').val($('#nombreCliente').val());
	$('#idAsegurado').val($('#idCliente').val());
	$('#btn_copiarInciso').show();
	var param = {
		"incisoCotizacion.incisoAutoCot.personaAseguradoId"  : $('#idAsegurado').val(),
		"incisoCotizacion.incisoAutoCot.nombreAsegurado"     : $('#nombreAsegurado').val()
	}
	guardarAsegurado(param,false);
}

/**
 * Guarda Solo el nombre del asegurado en el IncisoCotizacion
 */
function cargarNombreAsegurado(){
	$('#idAsegurado').val("");
	$('#saveAsegurado').val(2);
	var param = {
		"incisoCotizacion.incisoAutoCot.nombreAsegurado" : $('#nombreAsegurado').val()
	}
	
	guardarAsegurado(param,false);
}

/**
 * Copia los datos del inciso para le conductor y los guarda  en el incisoCotizacion
 */
function copiarIncisoDeAsegurado(){
	$('#saveAsegurado').val(4);
	var param = {
		"incisoCotizacion.incisoAutoCot.nombreAsegurado"    : $('#nombreAsegurado').val(),
		"incisoCotizacion.incisoAutoCot.personaAseguradoId" : $('#idAsegurado').val()
	};
	guardarAsegurado(param, true);
}

/**
 * Guarda los datos del asegurado de acuerdo al tipo
 */
function guardarAsegurado(param, isConductor){	
	var paramBase =  {
		"incisoCotizacion.id.idToCotizacion" : idToCotizacion,
		"incisoCotizacion.id.numeroInciso"   : numeroInciso,
		"saveAsegurado"                      : $('#saveAsegurado').val()
	};

	$.extend(paramBase, param );
	$.ajax({
		  type     : 'POST',
		  url      : urlSaveAsegurado,
		  data     : paramBase,
		  dataType : 'JSON',
		  async    : true,
		  success: function(data){
		  			actualizarDatosPantalla(data, isConductor);
		  		}
	});
}

/**
 * Actualiza los datos del asegurado en pantalla
 */
function actualizarDatosPantalla(json, isConductor){
	var incisoCotizacion = json.incisoCotizacion;
	if(incisoCotizacion!=undefined){
		if(isConductor){
			var fecha = incisoCotizacion.incisoAutoCot.fechaNacConductor;
			if(fecha!=undefined){
				fecha = fecha.replace('T00:00:00','');
				var fechaConductor = fecha.split('-');
				fecha = fechaConductor[2]+'/'+fechaConductor[1]+'/'+fechaConductor[0];	
				$('#fechaNacimientoCondutor').val(fecha);
			}
			$('#nombreCondutor').val(incisoCotizacion.incisoAutoCot.nombreConductor);
			$('#aPaternoCondutor').val(incisoCotizacion.incisoAutoCot.paternoConductor);
			$('#aMaternoCondutor').val(incisoCotizacion.incisoAutoCot.maternoConductor);
		}else{
			$('#nombreAsegurado').val(incisoCotizacion.incisoAutoCot.nombreAsegurado);
			$('#idAsegurado').val(incisoCotizacion.incisoAutoCot.personaAseguradoId);
		}	
	}
	$('searchClientDiv').empty();
}

/**
 * Carga la lista de documentos en pantalla
 */ 
function cargarDocumentFortimax(auto){
	var divDoc = $('#documentosFortimaxDiv');
	if(divDoc != undefined){
		divDoc.show();	
		if(auto){
			mostrarIndicadorCarga("docFortimaxLoad");
			$('body').css('cursor','progress');
		}else{
			$('#documentosFortimax').show();
		}
		
		var param = {
			'incisoCotizacion.cotizacionDTO.idToPersonaContratante' : +$('#idCliente').val()
			};
		sendRequestJQAgente(param, urlListaDocumentos, "documentosFortimax", 'cargarLigaFortimax('+auto+')');
	}
}

function cargarDocumentFortimaxInd(auto){
	//alert('Individual' || $('#idToCotizacion') );
	var divDoc = $('#documentosFortimaxDiv');
	if(divDoc != undefined){
		divDoc.show();	
		if(auto){
			mostrarIndicadorCarga("docFortimaxLoad");
			$('body').css('cursor','progress');
		}else{
			$('#documentosFortimax').show();
		}
		
		var param = {
			'incisoCotizacion.cotizacionDTO.idToPersonaContratante' : +$('#idCliente').val(),
			'incisoCotizacion.cotizacionDTO.idToCotizacion'         : +$('#idToCotizacion').val()
			};
		sendRequestJQAgente(param, urlListaDocumentos, "documentosFortimax", 'cargarLigaFortimax('+auto+')');
	}
}

function cargarLigaFortimax(auto){
	if(auto){
		var param = {
			'incisoCotizacion.cotizacionDTO.idToPersonaContratante' : +$('#idCliente').val(),
			'incisoCotizacion.id.idToCotizacion'                    : +$('#idToCotizacion').val()
			};
		$.ajax({
			type     : 'POST',
			url      : urlGetLigaFortimax,
			data     : param,
			dataType : 'JSON',
			async    : true,
			success  : function(data){
		  			if(data){
		  				var urls = data.mapGenerico;
		  				if(urls!=undefined){
			  				if(sizeMap(urls) >= 1){
			  					var count = 0;
								$.each(urls, function(index, valor) {
									var name = $('#labelName_'+count).html();
	  								var liga = "<a href="+valor+" > " + name + "</a>"
	  								$('#labelName_'+count).html(liga);
	  								$('#file_'+count).removeClass('mandatory');
	  								count = count+1;
								});
							}	
		  				}
		  			}
		  		},
		  	complete : function(data){
				$('body').removeAttr('style');
				$('#docFortimaxLoad').empty();
				$('#docFortimaxLoad').hide();
				$('#documentosFortimax').show();
		  	}
		});
	}
}

//Modal Agregar Cliente

/**
 *Carga la pantalla para agregar un nuevo cliente 
 */
function addClienteDesdeAgente(){
	if(!doblClic){
		
		var nombreCont = $('#nombreContr').val();
		var apellidoPatCont = $('#apellidoPatCont').val();
		var apellidoMatCont = $('#apellidoMatCont').val();
		var urlLoadFormClientes = urlLoadFormCliente;
		var correoObligatorio = $('#correoObligatorio').val();
		if($.isValid(nombreCont) || $.isValid(apellidoPatCont) ||$.isValid(apellidoMatCont)){
			var nombre = nombreCont.split(',');
			var apellidoP = apellidoPatCont.split(',');
			var apellidoM = apellidoMatCont.split(',');
			urlLoadFormClientes = urlLoadFormCliente + '?nombre=' + nombre[0].replace(' ', '_') + '&apellidoPaterno=' + apellidoP[0].replace(' ', '_') + '&apellidoMaterno=' + apellidoM[0].replace(' ', '_') + '&correoObligatorio=' + correoObligatorio;
		}
		
		sendRequestJQ(null, urlLoadFormClientes, "addClientDiv", 'afterModalAddCliente('+false+','+true+')');
	
	} else {
		doblClic = false;
	}
}
function afterModalAddCliente(modifica, isPersonaFisica){
//TODO hola
	
	obtenerConfYEsconderCampos('idClienteUnico', negocio);
	$('#addClienteAgente').find(".datepicker").datepicker({format: 'dd/mm/yyyy', language: 'es', todayHighlight: 'true', autoclose: 'true'});
	$('#giroDIV').hide();
	$('#acTipoPersonaRB').hide();
	//Inicia Edición
	if (negocioTipoPersona == "1") {
		
		$('#addDivPersonaFisica').show();
		$('#addDivPersonaMoral').hide();
		$('#addDivPersonaFisicaFiscal').show();
		$('#addDivPersonaMoralFiscal').hide();
		$('#curpDIV').show();
		$('#giroDIV').hide();
		$('input[name="cliente.claveTipoPersona"]').val(1);
	}else if (negocioTipoPersona == "2") {
		$('#addDivPersonaFisica').hide();
		$('#addDivPersonaMoral').show();
		$('#addDivPersonaFisicaFiscal').hide();
		$('#addDivPersonaMoralFiscal').show();
		$('#curpDIV').hide();
		$('#giroDIV').show();
		$('input[name="cliente.claveTipoPersona"]').val(2);
	}else{
		$('#acTipoPersonaRB').show();
		
		$('#claveTipoPersona1').click(function(){
			$('#addDivPersonaFisica').show();
			$('#addDivPersonaMoral').hide();
			$('#addDivPersonaFisicaFiscal').show();
			$('#addDivPersonaMoralFiscal').hide();
			$('#curpDIV').show();
			$('#giroDIV').hide();
		});
		
		$('#claveTipoPersona2').click(function(){
			$('#addDivPersonaFisica').hide();
			$('#addDivPersonaMoral').show();
			$('#addDivPersonaFisicaFiscal').hide();
			$('#addDivPersonaMoralFiscal').show();
			$('#curpDIV').hide();
			$('#giroDIV').show();
			
		});
	}
	
	//Finaliza Edición
	
	$("input[name='cliente.claveTipoPersona']").change(function(){
	   if($(this).val()==1){
		   
		   $('#claveTipoPersona1').click();
		   
	   }else {     $('#claveTipoPersona2').click();}
	});

	
	
	

	$("#btnCopiarDatosGenerales").click(function(){
		copiarDatosGeneralesAFiscales();
	});
	
	$("#btnCopiarDatosGeneralesCotInd").click(function(){
		copiarDatosGeneralesAFiscalesCotInd();
	});
	
	if(modifica){
		$('#addClient').html("Guardar");
		$('#msnInfo').show('slow');
		$('#msnInfoText').html("Favor de complementar la informaci\u00F3n del cliente");
	}
	
	if(tipoCotizacion==COTIZACION_SERVICIO_PUBLICO) {
		defineCamposObligatorios();
	}
	
	$('#addClienteAgente').find("input,select").change(function(){
	    	if($(this).val()) {
	    		$(this).parent().parent().parent().removeClass('has-error');
	    	}
	    });
}


// valida que los inputs del formulario esten llenos
function validaInputsAgregarCliente(){
	var valor = true;
	if($('#addClienteAgente').html()!=undefined){
		var valor1 = validaObjetosCliente('#addClienteAgente', 'select', '.mandatory');
		var valor2 = validaObjetosCliente('#addClienteAgente', 'input[type!=checkbox]', '.mandatory');
		var valor3 = validaObjetosCliente('#addClienteAgente', 'textarea', '.mandatory');
		if(!valor1 || !valor2 || !valor3)
			valor = false;
	}else
		return true;
	return valor;
}
/**
 * Prepara el guardado del nuevo cliente
 * @param divCarga
 */
function guardarClienteAjustador(){
	$.loadMensajeInfo("Guardando datos de cliente", true);
	var idDiv = 'altaCliente';
	var data = $("#addClienteAgente").serialize();
	var rfc = $(baseCliente+"codigoRFC").val();
	var tipoPersona = $('input[name="cliente.claveTipoPersona"]').val() == "1";
	var rfcValido = validarRFCAgente();
	var correoObligatorio = $('#correoObligatorio').val();
	var correo = $('#correoElectronicoAviso').val();
	var bandera = false;
	validacionesPrimaMayor();
	if(validaInputsAgregarCliente()){
		if(tipoPersona){
			if(rfc!=null && rfc.toString().length>0){
				if((esMayorEdadServidor($(baseCliente+"fechaNacimiento").val()))){
					var campoCoreo = !(correo === undefined || correo.trim().length == 0);
					if(correoObligatorio == "true"){
						if(!campoCoreo){
							unlockPageCliente();
							bandera = true;
							alert("El correo es obligatorio, no puede quedar vacio");
						}
					}
					
					if(campoCoreo && !/^([\w\.\xF1\-]+@[\w\.\xF1\-]+\.[\w]{2,3})?$/.test(correo)){
						alert("El correo tiene un formato erroneo");
						bandera = true;
					}else if(!bandera){
						emailCliente = correo;
						operacionGenericaConParamRendererIntoDiv(urlSaveCliente+"?"+data,null,idDiv);
					}
				}
			}else{
				bandera = true;
				alert("RFC inv\u00E1lido!");
			}
		}else if((rfc!=null && rfc.toString().length>0) && !tipoPersona){
			if(rfc.toString().length==12){
				operacionGenericaConParamRendererIntoDiv(urlSaveCliente+"?"+data,null,idDiv);
			}
			else{
				bandera = true;
				alert("RFC invalido, favor de capturar la homoclave manualmente");
			}
		}else{
			bandera = true;
			alert("RFC invalido, favor de capturar el RFC y verificar el tipo de persona jur\u00EDdica");
		}	
	}else{
		bandera  = true;
	}
	if(bandera) { $.closeMesaje();}
}


/**
 * Verifica si aplican las validaciones de prima mayor a 2,500 usd
 * @returns {Boolean}
 */

function validacionesPrimaMayor(){
	//Validaciones Cliente Único
	var idCliente = jQuery("#cliente\\.idClienteUnico").val();
	var elTipoPersona=$("#claveTipoPersona1")[0].checked;
	var tipoPersona=null;
	tipoPersona=(elTipoPersona)?1:2;
	
	
	var $giroCNSF;
	var $paisNacimiento;
	if(tipoPersona == 1){
		$paisNacimiento = jQuery("#cliente\\.idPaisNacimiento");
		$giroCNSF = jQuery("#cliente\\.ocupacionCNSF");
	}else{
		$paisNacimiento = jQuery("#cliente\\.idPaisConstitucion");
		$giroCNSF = jQuery("#cliente\\.cveGiroCNSF");
	}
	
	var cveGiroCNSF = $giroCNSF.val();
	var idPaisNacimiento = $paisNacimiento.val();
	
	
	if(idCliente){
		
		if(!idPaisNacimiento || !cveGiroCNSF){
			var valPrimaMayor = consultarPrima();
			
			if(valPrimaMayor){
				
				$giroCNSF.addClass("mandatory");
				$paisNacimiento.addClass("mandatory");
			
			}else{
				
				$giroCNSF.removeClass("mandatory");
				$paisNacimiento.removeClass("mandatory");
				
				if(!idPaisNacimiento){
					$paisNacimiento.parent().parent().parent().css({'visibility':'hidden'});
				}
				
				
				if(!cveGiroCNSF){
					$giroCNSF.parent().parent().parent().css({'visibility':'hidden'});
				}
			}
			
		}
		
	}else{
		$giroCNSF.removeClass("mandatory");
		$paisNacimiento.removeClass("mandatory");
		$paisNacimiento.parent().parent().parent().css({'visibility':'hidden'});
		$giroCNSF.parent().parent().parent().css({'visibility':'hidden'});
		
		$paisNacimiento.val('');
	}
}


/**
 * Consultar ws prima mayor
 * @returns {Boolean}
 */
function consultarPrima(){
	if(resultadoWSPrima == null){
		var url="/MidasWeb/catalogoCliente/validateClientePrima.action";
		var data={"cliente.idClienteUnico":jQuery("#cliente\\.idClienteUnico").val()};
		//url+="?cliente.claveTipoPersona="+idTipoPersona+"&cliente.idNegocio="+idNegocio;
		jQuery.ajax({
			url: url,
			dataType: 'json',
			async:false,
			type:"GET",
			data: data,
			success: function(params){
				try{
					primaJSON = JSON.parse(params.primaJSON);
					resultadoWSPrima = primaJSON.value;
				}catch(e){
					alert("Error al validar la prima acumulada.");
				}
			}
		});
	}
	
	return resultadoWSPrima;
}


function guardarClienteDuplicadosAjustador(){
	var tipoPersona = $("#cliente.claveTipoPersona").val();
	var data = $("#addClienteAgente").serialize();
	var idDiv = 'altaCliente';

	if(confirm("¿Está seguro que desea guardar el registro aunque exista uno similar? Se guardará historíco del registro.")){
		var idClienteVista = $('#altaCliente').find('#cliente.idCliente').val();
		var nombre = $('#altaCliente').find(baseCliente+'nombre').val();
		var apellidoP = $('#altaCliente').find(baseCliente+'apellidoPaterno').val();
		var apellidoM = $('#altaCliente').find(baseCliente+'apellidoMaterno').val();
		var razonSocial = $('#altaCliente').find(baseCliente+'razonSocial').val();
		var nombreCliente = null;

		nombreCliente = razonSocial;
		if(!$.isValid(nombreCliente)){
			nombreCliente = nombre+' '+apellidoP+' '+apellidoM;
		}
		
		$('#nombreCliente').val(nombreCliente);
		$('#idCliente').val(idClienteVista);
		if(idClienteVista!=null){
			$('#btn_copiarCliente').show();	
		}
		
		operacionGenericaConParamRendererIntoDivClientesDuplicados(urlSaveCliente+"?"+data,null,idDiv);
		
		
		
	}
}
function guardarClienteDuplicadosAjustadorCerrar(){
	$('#altaCliente').parent().parent().parent().parent().hide();
	$('#altaCliente').parent().parent().parent().parent().parent().hide();
	$( ".fancybox-overlay-fixed" ).hide();
	
}


function cancelarGuardarClienteDuplicadosAjustado(){
	$('#altaCliente').parent().parent().parent().parent().hide();
	$('#altaCliente').parent().parent().parent().parent().parent().hide();
	$( ".fancybox-overlay-fixed" ).hide();
	$('#altaCliente').remove();
}
/**
 * Indica si es igual o no el RFC esperado
 * @returns {Boolean}
 */
function validarRFCAgente(){
	 var nombre=$(baseCliente+"nombre").val();
	 var apPaterno=$(baseCliente+"apellidoPaterno").val();
	 var apMaterno=$(baseCliente+"apellidoMaterno").val();
	 var fechaNac=$(baseCliente+"fechaNacimiento").val();
	 var rfcActual=$(baseCliente+"codigoRFC").val();
	 var RFC=getMidasRFCByFields(nombre, apPaterno,apMaterno,fechaNac);
	 return RFC==rfcActual;
}

/**
 * Guarda el nuevo Cliente
 * @param path
 * @param jsonParam
 * @param divToRenderer
 */
function operacionGenericaConParamRendererIntoDivClientesDuplicados(path,jsonParam,divToRenderer){
	var params = (jsonParam!=undefined)?"?"+$.param(jsonParam):"";
	sendRequestJQ(null, path + params, divToRenderer, "cargaNuevoClienteDuplicado()");
}
function operacionGenericaConParamRendererIntoDiv(path,jsonParam,divToRenderer){
	var params = (jsonParam!=undefined)?"?"+$.param(jsonParam):"";
	var isPersonaFisica = (negocioTipoPersona == 1) ? true : false;
	sendRequestJQ(null, path + params, divToRenderer, "cargaNuevoCliente();$.closeAllMesaje();afterModalAddCliente("+true+","+isPersonaFisica+");");
}
function cargaNuevoClienteDuplicado(){
	var idClienteVista = $('#altaCliente').find('#idClienteVista').val();
	var nombre = $('#altaCliente').find(baseCliente+'nombre').val();
	var apellidoP = $('#altaCliente').find(baseCliente+'apellidoPaterno').val();
	var apellidoM = $('#altaCliente').find(baseCliente+'apellidoMaterno').val();
	var razonSocial = $('#altaCliente').find(baseCliente+'razonSocial').val();
	var nombreCliente = null;

	nombreCliente = razonSocial;
	if(!$.isValid(nombreCliente)){
		nombreCliente = nombre+' '+apellidoP+' '+apellidoM;
	}

	$('#nombreCliente').val(nombreCliente);
	$('#idCliente').val(idClienteVista);
	if(idClienteVista!=null){
		$('#btn_copiarCliente').show();	
	}
	guardarClienteCotizacionDuplicado(true);
}
function cargaNuevoCliente(){
	var idClienteVista = $('#altaCliente').find('#idClienteVista').val();
	var nombre = $('#altaCliente').find(baseCliente+'nombre').val();
	var apellidoM = $('#altaCliente').find(baseCliente+'apellidoPaterno').val();
	var apellidoP = $('#altaCliente').find(baseCliente+'apellidoMaterno').val();
	var razonSocial = $('#altaCliente').find(baseCliente+'razonSocial').val();
	var nombreCliente = null;

	nombreCliente = razonSocial;
	if(!$.isValid(nombreCliente)){
		nombreCliente = nombre+' '+apellidoM+' '+apellidoP;
	}

	$('#nombreCliente').val(nombreCliente);
	$('#idCliente').val(idClienteVista);
	if(idClienteVista!=null){
		$('#btn_copiarCliente').show();	
	}
	guardarClienteCotizacion(true);
}

function guardarClienteCotizacion(modal){
	$('#saveAsegurado').val(5);	
	var params = {
		"incisoCotizacion.incisoAutoCot.nombreAsegurado"        : $('#nombreAsegurado').val(),
		"incisoCotizacion.cotizacionDTO.idToPersonaContratante" : $('#idCliente').val(),
		"incisoCotizacion.id.idToCotizacion"                    : $('#idToCotizacion').val(),
		"incisoCotizacion.id.numeroInciso"                      : numeroInciso,
		"saveAsegurado"                                         : $('#saveAsegurado').val()
	};

	$.ajax({
		  type     : 'POST',
		  url      : urlSaveAsegurado,
		  data     : params,
		  complete : function(data){
				  	if(modal){
				  		cerrarModal();
				  		$('altaCliente').empty();
				  		cargarDocumentFortimaxInd(false);
				  	}else{
				  		cargarDocumentFortimaxInd(false);
				  		$('searchClientDiv').empty();
				  	}
		  		},
		  dataType: 'JSON',
		  async:false
	});
		
}
function guardarClienteCotizacionDuplicado(modal){
	$('#saveAsegurado').val(5);	
	var params = {
		"incisoCotizacion.incisoAutoCot.nombreAsegurado"        : $('#nombreAsegurado').val(),
		"incisoCotizacion.cotizacionDTO.idToPersonaContratante" : $('#idCliente').val(),
		"incisoCotizacion.id.idToCotizacion"                    : $('#idToCotizacion').val(),
		"incisoCotizacion.id.numeroInciso"                      : numeroInciso,
		"saveAsegurado"                                         : $('#saveAsegurado').val()
	};

	$.ajax({
		  type     : 'POST',
		  url      : urlSaveAsegurado,
		  data     : params,
		  complete : function(data){
				  	if(modal){
				  		cerrarModal();
				  		$('altaCliente').empty();
				  		cargarDocumentFortimax(false);
				  	}else{
				  		cargarDocumentFortimax(false);
				  		$('searchClientDiv').empty();
				  	}
		  		},
		  dataType: 'JSON',
		  async:false
	});
	guardarClienteDuplicadosAjustadorCerrar();
}

/**
 * Genera el RFC de la persona deacuerdo al tipo 
 */
function generarRFCAgente(){

	var input = 'cliente.codigoRFC';
	var fecNac = null;
	var nombre = null;

	if($("#claveTipoPersona1")[0].checked){
		nombre = $(baseCliente+'nombre').val();
		fecNac = $(baseCliente+'fechaNacimiento').val()
		var aPaterno = $(baseCliente+'apellidoPaterno').val();
		var aMaterno = $(baseCliente+'apellidoMaterno').val();

		midasFormatRFCInOneField(nombre, aPaterno, aMaterno, fecNac, input);
	}else{
		nombre = $(baseCliente+'razonSocial').val();
		fecNac = $(baseCliente+'fechaConstitucion').val();
		generarYDividirRFCMoral(nombre, fecNac, null, null, input);
		cerrarMensajeInformativo();
		alert("RFC invalido, favor de capturar la homoclave manualmente"); 
	}
}

/**
 * Carga la lista de los paises al input indicado
 * @param target
 */
function cargarListaPaises(target){

	restartCombo(target, headerValue, "Cargando...", "id", "value");

	if(target != null && target != headerValue ){
		listadoService.getMapPaises(
			function(data){
				if (sizeMap(data) == 0) {
					restartCombo(target, headerValue, "No se encontraron registros ...", "id", "value");						
				}else{
					addOptions(target,data);
					if (sizeMap(data) > 1) {
					}
				}
			}
		);
	}
}

/**
 * Copia los datos Generales como datos fiscales
 */
function copiarDatosGeneralesAFiscales(){
	if($("#claveTipoPersona1")[0].checked){
		$(baseCliente+'nombreFiscal').val($(baseCliente+'nombre').val());
		$(baseCliente+'apellidoPaternoFiscal').val($(baseCliente+'apellidoPaterno').val());
		$(baseCliente+'apellidoMaternoFiscal').val($(baseCliente+'apellidoMaterno').val());
		$(baseCliente+'fechaNacimientoFiscal').val($(baseCliente+'fechaNacimiento').val());		
	}else{
		$(baseCliente+'razonSocialFiscal').val($(baseCliente+'razonSocial').val());
		$(baseCliente+'fechaNacimientoFiscal').val($(baseCliente+'fechaConstitucion').val());
	}
	$(baseCliente+'idPaisFiscal').val($(baseCliente+'idPaisString').val());
	onChangePais('cliente.idEstadoFiscal','cliente.idMunicipioFiscal','cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idPaisFiscal');
	$(baseCliente+'idEstadoFiscal').val($(baseCliente+'idEstadoDirPrin').val());
	onChangeEstadoGeneral('cliente.idMunicipioFiscal','cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idEstadoFiscal');
	$(baseCliente+'idMunicipioFiscal').val($(baseCliente+'idMunicipioDirPrin').val());
	onChangeCiudad('cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idMunicipioFiscal');
	$(baseCliente+'nombreColoniaFiscal').val($(baseCliente+'nombreColonia').val());
	onChangeColonia('cliente.codigoPostalFiscal','cliente.nombreCalleFiscal', 'cliente.nombreColoniaFiscal','cliente.idMunicipioFiscal');
	$(baseCliente+'codigoPostalFiscal').val($(baseCliente+'codigoPostal').val());
	$(baseCliente+'nombreCalleFiscal').val($(baseCliente+'nombreCalle').val());
	$(baseCliente+'numeroDomFiscal').val($(baseCliente+'numeroDom').val());
}
// Fin Modal Agregar Cliente

function copiarDatosGeneralesAFiscalesCotInd(){
	if($("#claveTipoPersona1")[0].checked){
		$(baseCliente+'nombreFiscal').val($(baseCliente+'nombre').val());
		$(baseCliente+'apellidoPaternoFiscal').val($(baseCliente+'apellidoPaterno').val());
		$(baseCliente+'apellidoMaternoFiscal').val($(baseCliente+'apellidoMaterno').val());
		$(baseCliente+'fechaNacimientoFiscal').val($(baseCliente+'fechaNacimiento').val());		
	}else{
		$(baseCliente+'razonSocialFiscal').val($(baseCliente+'razonSocial').val());
		$(baseCliente+'fechaNacimientoFiscal').val($(baseCliente+'fechaConstitucion').val());
	}
	$(baseCliente+'idPaisFiscal').val($(baseCliente+'idPaisString').val());
	onChangePais('cliente.idEstadoFiscal','cliente.idMunicipioFiscal','cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idPaisFiscal');
	$(baseCliente+'idEstadoFiscal').val($(baseCliente+'idEstadoString').val());
	onChangeEstadoGeneral('cliente.idMunicipioFiscal','cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idEstadoFiscal');
	$(baseCliente+'idMunicipioFiscal').val($(baseCliente+'idMunicipioString').val());
	onChangeCiudad('cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal','cliente.nombreCalleFiscal','cliente.idMunicipioFiscal');
	$(baseCliente+'nombreColoniaFiscal').val($(baseCliente+'nombreColonia').val());
	onChangeColonia('cliente.codigoPostalFiscal','cliente.nombreCalleFiscal', 'cliente.nombreColoniaFiscal','cliente.idMunicipioFiscal');
	$(baseCliente+'codigoPostalFiscal').val($(baseCliente+'codigoPostal').val());
	$(baseCliente+'nombreCalleFiscal').val($(baseCliente+'nombreCalle').val());
}

//Modal buscar representante legal
function mostrarModalResponsable(){
	var param =	{
			tipoAccion : "cotizador_agentes",
			idField    : ""
		}
	$('#altaCliente').hide();
	sendRequestJQAgente(param, urlfindResponsable + '?idElemento=_res', 'searchPersonaDiv', "buscarFunctionReprecentante('#serachPersona_res', '#nombreRepresentanteFiscal', '#idRepresentanteFiscal');");
}

function buscarFunctionReprecentante(forma, nombre, id){
	$('#searchPersonaDiv').show('slow');
	$('#divLista_res').show('slow');
	$('#serachPersona').find(".datepicker").datepicker('update');

	$("#btn_filterSearchPerson").click(function(){
			listaFiltrada(listarFiltradoResponsablePath, 'PersonaGrid_res', 'divCarga_res', forma, nombre, id);
	});
	$("#claveTipoPersona2").click();
}


function onChangeCodigoPostalAgentes(value, seccion){
	
	var targetColonia = '';
	var targetMunicipio = '';
	var targetIdEstado = '';
	var targetCalleNumero = '';
	var targetPais = '';
	var targetCP = '';
		
	if(seccion !== typeof 'undefined' && seccion !== '' && seccion == 'GENERALES'){
		targetColonia = 'cliente\\.nombreColonia';
		targetMunicipio = 'cliente\\.idMunicipioDirPrin';
		targetIdEstado = 'cliente\\.idEstadoDirPrin';
		targetCalleNumero = 'cliente\\.nombreCalle';
		targetPais = 'cliente\\.idPaisString';
		targetCP = 'cliente\\.codigoPostal';
	} else if(seccion !== typeof 'undefined' && seccion !== '' && seccion == 'FISCALES') {
		targetColonia = 'cliente\\.nombreColoniaFiscal';
		targetMunicipio = 'cliente\\.idMunicipioFiscal';
		targetIdEstado = 'cliente\\.idEstadoFiscal';
		targetCalleNumero = 'cliente\\.nombreCalleFiscal';
		targetPais = 'cliente\\.idPaisFiscal';
		targetCP = 'cliente\\.codigoPostalFiscal';
	} else if(seccion !== typeof 'undefined' && seccion !== '' && seccion == 'AGENTECP') {
		targetMunicipio = 'cliente\\.idMunicipioFiscal';
		targetIdEstado = 'cliente\\.idEstadoFiscal';
		targetCP = 'cliente\\.codigoPostalFiscal';
	}
    if ($('#'+targetPais).val()=='-1'){
        $('#'+targetPais).val('PAMEXI');
    }
    
    //Verificamos si alguno de los combos antes del codigo postal no esta completo, si es asi, entonces
    //se lanza la funcion de llenar la direccion por cp
    getNameColoniasPorCPAgentes(value,targetColonia ,targetMunicipio,targetIdEstado);
    //si no encuentra direccion por codigo postal se resetea el estado a "seleccione"
    //se agregarn las css class jQ_ciudad y jQ_estado para la validacion
    if(!$.isValid($('#'+targetMunicipio).val())){
    	$('#'+targetIdEstado).val("");
    }
}


function onChangeCodigoPostalAgenteCP(value){
	
	if(value != ''){
		var targetColonia = '';
		var targetMunicipio = '';
		var targetIdEstado = '';
		var targetPais = '';
		var targetCP = '';
		
		targetMunicipio = 'idMunicipio';
		targetIdEstado = 'idEstado';
		targetCP = 'codigoPostal';
		
		if ($('#'+targetPais).val()=='-1'){
	        $('#'+targetPais).val('PAMEXI');
	    }
		
	    //Verificamos si alguno de los combos antes del codigo postal no esta completo, si es asi, entonces
	    //se lanza la funcion de llenar la direccion por cp
		getNameColoniasPorCPAgenteCP(value,targetMunicipio,targetIdEstado);
	    //si no encuentra direccion por codigo postal se resetea el estado a "seleccione"
	    //se agregarn las css class jQ_ciudad y jQ_estado para la validacion
	    if(!$.isValid($('#'+targetMunicipio).val())){
	    	$('#'+targetIdEstado).val("");
	    }
	}else{
		cargarEstadoNegocio($('#idNegocio').val());
	}
}

/*****************************************************************************************
**funciones para obtener las colonias por codigo postal
***se carga la informacion de la colonia done el value y el texto son el nombre de la colonia
**** esta funciones se utilizan en Agentes
******************************************************************************************/
function getNameColoniasPorCPAgentes(codigoPostal, colonia, ciudad, estado) {
    if (codigoPostal != null || codigoPostal != "") {
        $.ajax({
            type     : 'POST',
            url      : "/MidasWeb/codigoPostal.do",
            data     : {id : codigoPostal},
            dataType : 'xml',
            success  : function(xmlResponse) {
                loadCombosDomicilioAgentes(xmlResponse, colonia, ciudad, estado, codigoPostal);
            }
        });
    }
}


function getNameColoniasPorCPAgenteCP(codigoPostal, ciudad, estado) {
	
    if (codigoPostal != null || codigoPostal != "") {
    	$.ajax({
            type     : 'POST',
            url      : "/MidasWeb/codigoPostal.do",
            data     : {id : codigoPostal},
            dataType : 'xml',
            success  : function(xmlResponse) {
            	loadCombosDomicilioAgenteCP(xmlResponse, ciudad, estado, codigoPostal);
            }
        });
    }
}

function loadCombosDomicilioAgentes(doc, colonies, city, state, cp) {
	 if ($.isValid(colonies) && $.isValid(city) && $.isValid(state)) {
        var stateElement = $( "state", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(state, stateElement, 'MAP', null);
        var cityElement = $( "city", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(city, cityElement, 'MAP', null);
        var items = $( "colonies", doc ).map( function() {
			return {
				id:  $( "description", this ).text().replace('-'+cp,''),
				value: $( "description", this ).text().replace('-'+cp,''),
			}
		});

		addOptionSelect(colonies, items, 'MAP', null);
    }
}


function loadCombosDomicilioAgenteCP(doc,  city, state, cp) {
	
	 if ( $.isValid(city) && $.isValid(state)) {
        var stateElement = $( "state", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(state, stateElement, 'MAP', null);
        var cityElement = $( "city", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(city, cityElement, 'MAP', null);
        
    }
}

//End Modal buscar representante legal

/**
 * Guarda los datos de la pantalla Datos Complementarios
 */
function  guardarDatosComplementarios(){
	var nombreAsegurado = $('#nombreCliente').val();
	var correoObligatorio = $('#correoObligatorio').val();
	$('#altaCliente').remove();
	if(validaformaActual()) {
		$.loadMensajeInfo("Guardando datos Complementarios", true);
		$('#datosComentarioForm').attr('action', urlSaveDatosComplementarios+'?correo='+emailCliente + '&nombreContratante='+nombreAsegurado+'&correoObligatorio='+correoObligatorio);
		$('#datosComentarioForm').submit();
      } else {
    	  $('html,body').scrollTop($('.has-error').first().offset().top);
      }
}

 /**************************************** Fin Forma 3 Datos Complementarios ****************************************/

  /**************************************** Forma 4 Cobranza Emision ****************************************/

function onChangeMedios() {
	if ($('#medioPagoDTOs').val() == headerValue || $('#medioPagoDTOs').val() == 15) {
		$("#divcuentaTarjeta").empty();
		$("#divTitular").empty();
		$("#datosTitularYTarjeta").css("display","none");
		$("#divSeccionTitular").css("display","none");
		$("#divSeccionTarjeta").css("display","none");

		$("div#conductos_div").hide();
		$("div#checkDatos").hide();
		$('#titularIgual').val('');
		cleanTitularForm();
		cleanInfoCuentaForm();
	} else {
		//alert("into else");
		idConducto = $('#idConductoCobroCliente').val();
		if(obtieneMediosDeCobroPorPesona(idConducto)){
			//alert("into else if "+$('#conductos').val());
			$('#conductos').val(idConducto);
			cleanInfoCuentaForm();
			$("div#conductos_div").show('slow');
			$("div#checkDatos").show('slow');
			if($('#conductos').val() != '-1' && $('#conductos').val() != headerValue){
				onChangeConductos();
				onChangeTitularIgual();
			}
		}	
	}
}

/**
 * Llena el combo de conductos disponibles para la persona
 */
function obtieneMediosDeCobroPorPesona(idConducto) {
	idToPersonaContratante = $('#0idToPersonaContratante').val();
	if(!validarExisteContratante()){
		return false;		
	}

	if ($.isValid($('#medioPagoDTOs').val())) {
		listadoService.getConductosCobro(idToPersonaContratante, $('#medioPagoDTOs').val(), 
			function(data) {
				addOptions('conductos', data);
				if(idConducto!=undefined){
					$('#conductos').val(idConducto);
				}
			});
	}
	return true;
}

/**
 * Complementa la informacion de la cuenta
 */
function onChangeConductos() {
	$("div#checkDatos").show('slow');
	$("div#datosTitularYTarjeta").show('slow');
	$("div#divSeccionTarjeta").show('slow');
	var url;
	var param = {
		"idToCotizacion"      : idToCotizacion,
		"idMedioPago"         : $('#medioPagoDTOs').val(),
		"idConductoCobro"     : $('#conductos').val(),
		"newCotizadorAgente"  : $('#newCotizadorAgente').val(),
		"conductoCobroId"	  : $('#conductos').val()
	};
	
	
	if ($('#conductos').val() == '-1'){
		ocultarOrMostrarDatosTitularTarjeta(false);
	} else if ($('#conductos').val() != "" && dwr.util.getText('conductos') != 'Agregar nuevo...') {
		// Cargar un conducto
		sendRequestJQAgente(param, 
				urlComplementInfInciso, "divcuentaTarjeta", "onChangeTitularIgual();ocultarOrMostrarTitularIgual(false);onChangeTitularIgualImproved(true);");
	} else if ($('#conductos').val() != '') {
		// Agregar nuevo conducto
		if(dwr.util.getText('conductos') == 'Agregar nuevo...'){
			var parametrosNuevos = {
					"idToCotizacion"      : idToCotizacion,
					"idMedioPago"         : $('#medioPagoDTOs').val(),
					"idConductoCobro"     : $('#conductos').val(),
					"newCotizadorAgente"  : $('#newCotizadorAgente').val(),
					"conductoCobroId"	  : '0'
				};
			sendRequestJQAgente(parametrosNuevos, 
					urlComplementInfInciso,"divcuentaTarjeta","cleanInfoCuentaForm();onChangeTitularIgual();ocultarOrMostrarTitularIgual(true);");
		}else{
			sendRequestJQAgente(param, 
					urlComplementInfInciso,"divcuentaTarjeta","cleanInfoCuentaForm();onChangeTitularIgual();ocultarOrMostrarTitularIgual(true);");
		}
		//$("div#datosTitularYTarjeta").show('slow');
		
		
	} 
}

//joksrc
function ocultarOrMostrarTitularIgual(accion){
	if(accion){
		$('#checkDatos').show('slow');
	}else{
		$('#checkDatos').hide('slow');
	}
	
}
//joksrc
function ocultarOrMostrarDatosTitularTarjeta(accion){
	if(accion){
		//$("div#datosTitularYTarjeta").show('slow');
		$('div#divSeccionTitular').show('slow');
	}else{
		$('div#divSeccionTitular').hide('slow');
	}
	
}

/**
 * complementa la informacion del titular
 */
function onChangeTitularIgual() {
	var param = null;
	
	if ($('#titularIgual').val() == 'true') {
		$.loadMensajeInfo("Cargando Datos del Titular", true);
		param = {
				"idToCotizacion"         : idToCotizacion,
				"datosIguales"           : $('#titularIgual').val(),
				"newCotizadorAgente"     : $('#newCotizadorAgente').val()
				//"conductoCobroId"	     : $('#conductos').val()
			};
		sendRequestJQAgente(param, urlComplementInfIncisoTitular, "divTitular", "$.closeMesaje();$('div#divSeccionTitular').show('slow');");
	}else if($('#titularIgual').val() == 'false' || $('#titularIgual').val() == ''){
		//cleanTitularForm();
		$('#txbMexico').val('MÉXICO');
		param = {
				"idToCotizacion"         : idToCotizacion,
				"datosIguales"           : $('#titularIgual').val(),
				"idMedioPago"            : $('#medioPagoDTOs').val(),
				"newCotizadorAgente"     : $('#newCotizadorAgente').val(),
				"conductoCobroId"	  : $('#conductos').val()
			};
		sendRequestJQAgente(param, urlComplementInfIncisoTitular, "divTitular", "$('div#divSeccionTitular').show('slow');");
	}
	//$("div#divSeccionTitular").show('slow');
}

function onChangeTitularIgualImproved(accion){
var param = null;
	if (accion) {
		$.loadMensajeInfo("Cargando Datos del Titular", false);
		param = {
				"idToCotizacion"         : idToCotizacion,
				"datosIguales"           : $('#titularIgual').val(),
				"newCotizadorAgente"     : $('#newCotizadorAgente').val(),
				"conductoCobroId"	  : $('#conductos').val()
			};
		sendRequestJQAgente(param, urlComplementInfIncisoTitular, "divTitular", "$.closeMesaje();$('div#divSeccionTitular').show('slow');");
	}
	//$("div#divSeccionTitular").show('slow');
}

/**
 * Llena el compo de estados
 */
function onchangePaisCobranza() {
	var idPais = $('#idPais').val();
	if (idPais != "" && idPais != null) {
		listadoService.getMapEstados(idPais, function(data) {
			addOptions('idEstadoCobranza', data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idEstadoCobranza');
		$('#idEstadoCobranza').change();
	}
}
/**
 * Llena el combo de municipios
 */
function onchangeEstadoCobranza() {
	var idEstado = $('#idEstadoCobranza').val();
	if (idEstado != "" && idEstado != null) {
		listadoService.getMapMunicipiosPorEstado(idEstado, function(data) {
			addOptions('idMunicipioCobranza', data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idMunicipioCobranza');
		$('#idMunicipioCobranza').change();
	}
}
/**
 * Llena el combo de colonias
 */
function onchangeMunicipioCobranza() {
	var idMunicipio = $('#idMunicipioCobranza').val();
	if (idMunicipio != "" && idMunicipio != null) {
		listadoService.getMapColonias(idMunicipio, function(data) {
			addOptions('idColoniaCobranza', data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idColoniaCobranza');
		$('#idColoniaCobranza').change();
	}
}

function onchangeCp(){
	listadoService.getEstadoIdPorCp($('#codigoPostal').val(),function(data){
		if (data != null) {
			$('#idEstadoCobranza').val(data);
			$('#idEstadoCobranza').change();
			var cPostal = $('#codigoPostal').val();
			asignarMunicipioPorCP(cPostal);
		}	
	});
	
}


//joksrc
function asignarMunicipioPorCP(cp){
	listadoService.getCiudadPorCPMidas(cp,function(data){
		if(data != null){
			//alert("data: "+data);
			$('#idMunicipioCobranza').val(data);
			asignarListaColoniasByCp(cp);
		}
	});
}
//joksrc
function asignarListaColoniasByCp(cPostal){
	
	//var cPostal = $('#codigoPostal').val();
	if (cPostal != "" && cPostal != null) {
		listadoService.getMapColoniasPorCPMidas(cPostal, function(data) {
			if(data != null){
				addOptions('idColoniaCobranza', data);
			}
		});
		
	} else {
		removeAllOptionsAndSetHeaderDefault('idColoniaCobranza');
		$('#idColoniaCobranza').change();
	}
}

/**
 * Limpia los input dentro del div titularCuenta
 */
function cleanTitularForm() {
	$('#divTitular').find('input').val('');
	$('#divTitular').find('select').val('');
}

/**
 * Limpia los input dentro del div infoCuenta
 */
function cleanInfoCuentaForm() {
	$('#divcuentaTarjeta').find('input[type!=hidden]').val('');	
	cleanPromociones();
}

function cleanPromociones(){
	var promos = document.getElementById('promociones');
	
	if(promos != null && promos!=undefined){
		promos.options.length = 1;
	}
}

/**
 * Buscar las promociones de las tarjetas de credito
 */
function getPromos(){
	var numTarjeta = $('#cuenta').val();//cuentaPagoDTO.cuenta
	cleanPromociones();
	if(numTarjeta != null){
		$.loadMensajeInfo("Buscando promociones de pago con la tarjeta de cr\u00E9dito ingresada...", false);
		
		listadoService.getPromocionesTC(numTarjeta,function(data){
			addOptions('promociones', data);
			// buscar 
			$.closeMesaje();
			if(document.getElementById('promociones').options.length>1 || ($('#promociones').val() != '-1') && $('#promociones').val() != ''){
				$.loadMensajeError("Existen promociones de pago con la tarjeta de cr\u00E9dito ingresada.");
			}else{
				$.loadMensajeError("No se cuenta por el momento con promociones de pago con la tarjeta de cr\u00E9dito ingresada.");
			}
		});
	}
}

function validarExisteContratante(){
	if(!$.isValid(idToPersonaContratante)){
		$.loadMensajeError("Se necesita un contratante, por favor dir\u00EDjase a la pesta\u00F1a Complem. Emisi\u00F3n y seleccione un contratante antes de continuar. ");
		return false;
	}else{
		return true;
	}
}

function guardarCobranza(){
	if(validaformaActual()) {
		$.loadMensajeInfo("Guardando Datos de la Cobranza",true);
		$('#jspForm').val(4);
		$('#cobranzaYEmisionForm').attr('action', urlSaveCobranza);
		$('#cobranzaYEmisionForm').submit();
    } else {
    	$('html,body').scrollTop($('.has-error').first().offset().top);
    }
}

//joksrc
function guardarAntesDeEmitir(){
	if(validaformaActual()){
		$.loadMensajeInfo("Guardando Datos de la Cobranza",true);
		var parametros = jQuery('#cobranzaYEmisionForm').serialize();
		var url = '/MidasWeb/suscripcion/cotizacion/auto/cabranza/guardarCobranza.action?'+parametros;
		sendRequestJQ(null,url,targetWorkArea,"emitirCotizacion();");
	}else{
		$('html,body').scrollTop($('.has-error').first().offset().top);
	}

}

function generaNuevaCotizacion(idForm){

	var clati = jQuery('#clati').val();
	var urlAction = urlMostrarContenedor;

	if(clati)//Ingreso desde liga agentes
	{
		urlAction = urlMostrarCotizadorLigaAgente;
	}
	
	$('#'+idForm).find('input[type="hidden"]').not('#clati').remove();
	$('#jspForm').val(1);
	$('#'+idForm).attr('action', urlAction);
	$('#'+idForm).submit();
}

/**
 * Valida si se puede Emitir la cotizacion para la generacion de la poliza
 */
function validarDatosComplementariosAgente(){
	$.loadMensajeInfo("Validando Datos de la Cotizaci\u00F3n", false);
	$.ajax({
		  	type     : 'POST',
		  	url      : urlValidarDatosAgente,
		  	data     : {
	  					"idToCotizacion" : idToCotizacion
		  				},	
		  	dataType : 'JSON',
		  	async    : true,
		  	success : function(data){
	  			var mensajeDTO = data.mensajeDTO;
	  			if(mensajeDTO!=undefined && mensajeDTO!=null){
		  			if($.isValid(mensajeDTO.mensaje)){
		  				$.loadMensajeInfo(mensajeDTO.mensaje, false);
		  			}
		  			if(mensajeDTO.visible=="true" || mensajeDTO.visible){
	  					if(btnEmitircotizacion!=undefined)
	  						btnEmitircotizacion.show();
		  			}
	  			}
	  		}
	});
}


/**
 * Modifica el estatus de la cotizacion a Terminada
 */
function emitirCotizacion(){
	var claveEstatus = $("#claveEstatus").val();
	if(claveEstatus==12){
		emitirPoliza();
	}else{
		$.loadMensajeInfo("Emitiendo P\u00F3liza. Espere un momento...", true);
		$('#cobranzaYEmisionForm').attr('action', urlActualizaEstatusTerminado);
		$('#cobranzaYEmisionForm').submit();
	}
}




function emitirPoliza(){
	$.loadMensajeInfo("Emitiendo P\u00F3liza", true);
	var nombre = $('#nombreContratante').val();
	var correo = $('#correo').val();
	var correoObligatorio = $('#correoObligatorio').val();
	var idMediopago = document.getElementById('medioPagoDTOs').value;
	// solo validamos conducto de cobro AMERICAN EXPRESS
	if (idMediopago == 6) {
		var fechaVencimiento = document.getElementById('fechaVencimiento').value;
		var codigoSeguridad = document.getElementById('codigoSeguridad').value;
		
		if (codigoSeguridad.trim() == '') {
			mostrarMensaje("C\u00F3digo de seguridad no valido");
			return;
		}else if(fechaVencimiento.trim() == ''){
			mostrarMensaje("Fecha de vencimiento no valida");
			return;
		}
	}
	var urlEmitirAgenteCotizaciones = urlEmitirAgenteCotizacion +'?correoObligatorio='+correoObligatorio+'&nombreContratante='+nombre+'&correo='+correo+'&idNegocio='+negocio+'&idDiv=divMostEntrev';
	parent.sendRequestJQ(null,
			urlEmitirAgenteCotizaciones
			,'contenido', null);
}
//Modal para imprecion de poliza

function $_imprimirDetallePoliza(idToPoliza,claveTipoEndoso,recordFromMillis,validOnMillis){
	
	var validOn = new Date();
	var fechaValidaOn = validOn.getDate() + "/" + validOn.getMonth() + "/" + validOn.getFullYear();
	$('#btn_impresion').click();
	var nombre = $('#nombreContratante').val();
	var correo = $('#correo').val();
	var correoObligatorio = $('#correoObligatorio').val();
	var param = "?idToPoliza="+ idToPoliza+ "&chkanexos=true&chkcaratula=true&chkcertificado=true&chkinciso=true&chkrecibo=true"
				+ "&claveTipoEndoso=" + claveTipoEndoso + "&tipoImpresion=2"+ "&validOn=" + fechaValidaOn +"&recordFromMillis="
				+ validOnMillis+ "&validOnMillis=" + recordFromMillis+'&contratante='+nombre+'&correo='+correo+'&correoObligatorio='+correoObligatorio;
	$('#iframeImpresion').attr('src', urlImpresionModal+param);
	cargarDatosImpresion(); 
	
}

/************************************ Excepciones *****************************************/

function cancelarAutorizacion() {
	if (confirm('\u00BFEst\u00e1 seguro que desea regresar para modificar la cotizaci\u00F3n?')) {
		var mensaje = "Las excepciones detectadas requieren de una autorizaci\u00F3n o bien ser modificadas con datos permitidos en el negocio.";
		$('#tipoMensaje').val("20");
		$('#mensaje').val(mensaje);
		$('#resultadoTerminarCotizacionForm').attr('action', urlMostrarContenedor);
		$('#resultadoTerminarCotizacionForm').submit();
	}
}

function enviarAutorizacion(){
	if(confirm('\u00BFEst\u00e1 seguro que desea solicitar la autorizaci\u00F3n de las excepciones encontradas?')){
		$('#resultadoTerminarCotizacionForm').attr('action', urlGuardarSolicitudes);
		$('#resultadoTerminarCotizacionForm').submit();
	}
}

function regresaACotizacion(){
	cargarForma(4);
}
/************************************ funciones Generales  *****************************************/


//get from AS360
function obtenerUrl(){
	// capturamos la url
    var loc = document.location.href;
    // si existe el interrogante
    if(loc.indexOf('?')>0)
    {
        // cogemos la parte de la url que hay despues del interrogante
        var getString = loc.split('?')[1];
        // obtenemos un array con cada clave=valor
        var GET = getString.split('&');
        var get = {};
        // recorremos todo el array de valores
        for(var i = 0, l = GET.length; i < l; i++){
            var tmp = GET[i].split('=');
            get[tmp[0]] = unescape(decodeURI(tmp[1]));
        }
        return get;
	}
}
function obtenerParams(){
  	// Cogemos los valores pasados por get
    var valores=obtenerUrl();
    if(valores)
    {	var contador=0;
    	var idCotiza = 0;
        // hacemos un bucle para pasar por cada indice del array de valores
        for(var index in valores)
        {
        	if(index=="idToCotizacion" && valores[index] != ""){
        		idCotiza = valores[index];
        		contador++;
        	}
        	if(index=="numeroInciso" && (valores[index] == "1") ){
        		contador++;
        	}
        	if(contador>=2){
        	cotAgentesJsPrueb();
        	}
        }
    }
}


/**
 * Carga la forma seleccionada 
 */ 
function cargarForma(idForma){
	var form ;
	var nextTap = $('#nextTap').val();
	//Si nos encontramos en la misma pantalla en donde damos click no se actualiza nada	
	if(idForma==jspForm){
		return false;
	}
	if(nextTap!=4){
		if(nextTap<=idForma){
			$.loadMensajeInfo("Necesita completar los pasos anteriores para continuar a la siguiente forma", false);
			return false;
		}
	}
	if(jspForm=="1"){
		form = $('#fromDatosGenerales');
	}else if(jspForm =="2"){
		form = $('#detalleCoberturasForm');
	}else if(jspForm =="3"){
		form = $('#datosComentarioForm');
	}else{
		form = $('#cobranzaYEmisionForm');
	}	
	
	$('#jspForm').val(idForma);
	form.submit();
}
function regresarDatosGen(){
	var url = jQuery('#cobranzaYEmisionForm').context.URL.replace("forma=2","forma=1");
	window.location.href = url;
}
function regresarDatosComp(){
	var url = jQuery('#cobranzaYEmisionForm').context.referrer.replace("forma=4","forma=2");
	window.location.href = url;
}
function regresarCotizacionEmisionDocumentacion(){
	
	
	window.location.href ="/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?mensaje=&tipoMensaje=none&idToCotizacion="+$('#DocumentacionForm_cotizacion_idToCotizacion').val()+"&numeroInciso="+$('#DocumentacionForm_numeroInciso').val()+"&forma=4&newCotizadorAgente=true&compatilbeExplorador=true&correoObligatorio=false&correo=&nombreContratante=&claveEstatus=10";
	
}

function regresarCotizacionEmision(){
	
	
	window.location.href ="/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?mensaje=&tipoMensaje=none&idToCotizacion="+$('#entrevistaForm_cotizacion_idToCotizacion').val()+"&numeroInciso="+$('#entrevistaForm_numeroInciso').val()+"&forma=4&newCotizadorAgente=true&compatilbeExplorador=true&correoObligatorio=false&correo=&nombreContratante=&claveEstatus=10";
	
}

function changePEPS(){
	var pep = dwr.util.getValue("entrevista.pep");
	if(pep=='true'){
		
		jQuery("#pepDiV").show();
	}else {
		jQuery("#pepDiV").hide();
	}
}
function changeCuentaPropia(){
	
	var cuentaPropia = dwr.util.getValue("entrevista.cuentaPropia");
	
	if(cuentaPropia=='false'){
		
		jQuery("#cuentaPropiaDiv").show();
	}else {
		jQuery("#cuentaPropiaDiv").hide();

	}
}


function verComplementarCotizacionDeEntrevista(){
	var moneda = dwr.util.getValue("entrevista.moneda");
	var idToCotizacion = dwr.util.getValue("cotizacion.idToCotizacion");
	var numeroSerieFiel = dwr.util.getValue("entrevista.numeroSerieFiel");
	var pep = dwr.util.getValue("entrevista.pep");
	var parentesco = dwr.util.getValue("entrevista.parentesco");
	var nombrePuesto = dwr.util.getValue("entrevista.nombrePuesto");
	var cuentaPropia = dwr.util.getValue("entrevista.cuentaPropia");
	var documentoRepresentacion = dwr.util.getValue("entrevista.documentoRepresentacion");
	var nombreRepresentado=dwr.util.getValue("entrevista.nombreRepresentado")
	var	periodoFunciones=dwr.util.getValue("entrevista.periodoFunciones");
	var parms=null;
	parms = $('#entrevistaForm').serialize();
	if(numeroSerieFiel===''){
		alert('Es necesaria la FIEL');
		return;
	}

	if(document.getElementById("personaFisica")){
		
		if(jQuery('input[name="entrevista\\.pep"]:checked').val() == undefined){
			alert('La pregunta "PEP" es obligatoria.');
			return;
		}
		
		if(pep=='true'){
			if(parentesco==='' ||nombrePuesto===''||periodoFunciones===''){
				alert("Los campos:Parentesco,Puesto y Periodo de funciones son obligatorios ");
				return;
			}
			
		} 
		
		if(jQuery('input[name="entrevista\\.cuentaPropia"]:checked').val() == undefined){
			alert('La pregunta "Actúa por cuenta propia" es obligatoria.');
			return;
		}
		
		if(cuentaPropia=='false'){
			
				if(nombreRepresentado==''||jQuery('input[name="entrevista\\.documentoRepresentacion"]:checked').val() == undefined)
				{
					alert("Los campos: Documento Juridico y nombre por quién actua son obligatorios ");
					return;
				}
				
			
		}
	}
	
	if(document.getElementById("personaMoral")){
		if(folioMercantil=='')
		{
			alert("El Campo Folio Mercantil es Obligatorio.");
			return;
		}
	}
	
	var pdfDownload ='pdfDownload';
	var url = "/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?mensaje=&tipoMensaje=none&idToCotizacion="+$('#entrevistaForm_cotizacion_idToCotizacion').val()+"&forma=4&newCotizadorAgente=true&compatilbeExplorador=true&correoObligatorio=false&correo=&nombreContratante=&claveEstatus=10"
						+'&guardarEntrevista=true'
						+'&'+parms
;
	window.location.href =url;
	
}

function loadFortimax(){
	var url = dwr.util.getValue("linkFortimax");
	window.open(url,'Fortimax');
	
}
function checkBoxSeleccioneDocumento(indiceDocumento){
	 if($("#checkBox["+indiceDocumento+"].seleccionado").val()=="true") {

	jQuery("#documentacion"+indiceDocumento+"].descripcion ").removeAttr("disabled");
	jQuery("#documentacion"+indiceDocumento+"].fechaVigencia ").removeAttr("disabled");
	
	 }else {
		
		 jQuery("#documentacion"+indiceDocumento+"].descripcion ").attr("disabled","disabled");
			jQuery("#documentacion"+indiceDocumento+"].fechaVigencia ").attr("disabled","disabled");
	}
}

function checkBoxSeleccioneDocumento(indiceDocumento){
	var c=dwr.util.getValue("checkBox["+indiceDocumento+"].seleccionado");
	
	if(c==true) {
		document.getElementById("documentacion["+indiceDocumento+"].descripcion").disabled=false;
		document.getElementById("documentacion["+indiceDocumento+"].fechaVigencia").disabled=false;
		

	 }else {
		 document.getElementById("documentacion["+indiceDocumento+"].descripcion").disabled=true;
		 document.getElementById("documentacion["+indiceDocumento+"].fechaVigencia").disabled=true;
			
	}
}

function verComplementarCotizacionDeDocumentacion(){
	
	
	
	var fechaVigencia5 = dwr.util.getValue("documentacion[5].fechaVigencia");
	
	
	var temp= fechaVigencia5.split('/');
	
	var dt = new Date(temp[1]+'/'+temp[0]+'/'+temp[2]);
	var myDate = new Date();	
	var startMsec = myDate.getTime();
	var mils =86400000 *91;
	startMsec=startMsec-mils;
	var milsDate = dt.getTime();
	
	if(milsDate<startMsec){
		alert("La fecha de vigencia no debe ser menor  a tres");
		return;
	}
	var parms=null;
	parms = $('#DocumentacionForm').serialize();
	var url = "/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action?mensaje=&tipoMensaje=none&idToCotizacion="+$('#DocumentacionForm_cotizacion_idToCotizacion').val()+"&numeroInciso="+$('#DocumentacionForm_numeroInciso').val()+"&forma=4&newCotizadorAgente=true&compatilbeExplorador=true&correoObligatorio=false&correo=&nombreContratante=&claveEstatus=10"
						+'&'+parms
						+'&guardarDocumentacion=true';
						
						
	window.location.href =url;
	
}




/**
 * Muesta y/u oculta el objct de la forma indicado de acuerdo a su estado
 */
function mostrarOcultarDiv(div, objt, msn){
	div = $("#"+div);
	if(div.is(":visible")) {
		div.toggle('slow');
		$(objt.selector).html(msn?msn:"Mostrar");
	} else {
		div.toggle('slow');
		$(objt.selector).html(msn?msn:"Ocultar")
	}	
}

/**
 * Valida que los campos Obligatorios en la forma sean llenados antes de hacer submit
 */
function validaformaActual(){
	// Se limpian todos los errores
    $(".form-group").removeClass('has-error');
    // Se validan los campos obligatorios
    validoForm = true;
    var validoMail = true;
    
    $.each($(".mandatoryInfoAdicional"), function(index, input) {
    	if(($("#medioPagoDTOs").val() != '15' && $("#medioPagoDTOs").val() != '') && ($(input).val() == null || $(input).val() == '')){//agente
    		 $(input).parent().parent().parent().parent().addClass('has-error');
    		 validoForm = false;    		 
   	    }	    	
	});	 
    
    $.each($(".mandatory"), function(index, input) {
      if(!input.value) {
        $(input).parent().parent().parent().addClass('has-error');
        validoForm = false;
      }
    });
    $.each($(".mandatoryLbl"), function(index, input) {
      if(!$(input).html()) {
        $(input).parent().addClass('has-error');
        validoForm = false;
      }
    });
    
    if(($("#idTipoVigencia").val() === "" || $("#idTipoVigencia").val() === "-1") && document.getElementById("idTipoVigencia").options.length > 1){
		$("#idTipoVigencia").parent().parent().parent().addClass('has-error');
		 validoForm = false;
	}
    
    $.each($(".email"), function(index, input) {
        if(input.value) {      
        	if(/^([\w\.\xF1\-]+@[\w\.\xF1\-]+\.[\w]{2,3})?$/.test(input.value)){        		
        		validoMail = true;
        	}else {
        		$(input).parent().parent().parent().addClass('has-error');
        		validoMail = false;        		
        	}
        }
      });
    return validoForm && validoMail;
}

/**
 * Agrega las opciones al select indicado
 */
function addOptionSelect(obj, data, order, base){
	$("#"+obj).empty();
	var opt = "";
	var datos;
	if(order=="ASC" || order=="MAP"){
		datos = Object.keys(data).sort();
	}else{
		datos = Object.keys(data).sort().reverse();
	}

    var lista = [];
	$.each(data, function(index, val) {
		lista.push(val);
	});
	(order=="ASC" || order == 'MAP')?lista.sort():lista.sort().reverse();
	var selected = "";
	var selectedInput = false;
	if(sizeMap(datos) >= 1){
		$.each(lista, function(index, valor) {
			if(order=="MAP"){
				if(selected==headerValue){
					selected = valor.id;
				}					
				selectedInput =(selectedInput==false)?((valor.id==base)?true:false):false;
				opt+="<option value='"+valor.id+"'>"+valor.value+"</option>";
			}else{
				for (var i in datos) {
					for (var e in data) {
							
						if(valor == data[datos[i]]){
							if(selected==headerValue){
								selected = datos[i];
							}
								
							selectedInput =(selectedInput==false)?((datos[i]==$("#"+base).val())?true:false):false;
							opt+="<option value='"+datos[i]+"'>"+data[datos[i]]+"</option>";
							break;
						}
					}
				}
				if($.isValid(base)){
					selected = selectedInput?$("#"+base).val():selected;
				}
			}
		});
		$("#"+obj).html(opt);
		$("#"+obj).val(selected);
		$("#"+obj).change();
	}else {
		restartCombo(obj, headerValue, "No se encontraron registros ...", "id", "value");						
	}
}

/**
 *Cierra el fancybox abierto actualmente
 */
function cerrarModal(){
	//$('.fancybox-close').click();
}

function sendRequestJQAgente(param, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {
	$.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (param !== undefined && param !== null) ? param : null,
	    dataType: dataTypeParam,
	    async: true,
	    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
	    success: function(data) {
	    	if(targetId !== undefined && targetId !== null){
		    	jQuery("#" + targetId).html("");
		    	jQuery("#" + targetId).html(data);	    		
	    	}
	    },
	    complete: function(jqXHR) {
	    	if($.isValid(pNextFunction))
	    		eval(pNextFunction);
	    }  	    
	});

}

/**
 * Mensajes
 */

function actualizaMensajes(){
	var mensajeExito = $('#mensajeExitoText').html();
	var mensajeError = $('#mensajeErrorText').html();
	var mensajeInfo = $('#mensajeInfoText').html();
	
	$.loadMensajeError(mensajeError);
	$.loadMensajeInfo(mensajeInfo, false);
	$.loadMensajeExito(mensajeExito);
	
	if($.isValid(mensajeExito) || $.isValid(mensajeError) || $.isValid(mensajeInfo)){
		$('#backMSN').show();
	}
}

function replaceAllCaracter(text){	
	return text.replace(/Ã³/gi, "\u00F3").replace(/Ã©/gi, "\u00E9").replace(/Ãº/gi, "\u00FA").replace(/Ã¡/gi, "\u00E1").replace(/Â¿/gi, "\u00bf"); 
}

/**
 * Muestra la Lista con filtros
 */
function listaFiltrada(url, grid, loadDiv, forma, nombre, id){
	var gridList = new dhtmlXGridObject(grid);
	gridList.attachEvent("onRowDblClicked", function(){
		doOnRowDblClicked(gridList, nombre, id);
	});
	gridList.attachEvent("onXLS", function(gridList,count){
		mostrarIndicadorCarga(loadDiv);
		 $('#'+ grid).hide();
		 $('#divLista').show();
	 });
	
	(forma!=null)?gridList.load(url+"?"+$(forma).serialize()):gridList.load(url);

	gridList.attachEvent("onXLE", function(gridList,count){
		ocultarIndicadorCarga(loadDiv);
		$('#'+ grid).show();
	}); 

	return grid;
}
		

//Algunas Validacion

/**
 * Funciones genericas
 */
(function($){
	$.extend({
		/**
		 * Valida si una cadena esta vacia o nula
		 */
		isValid:function(val){
			return (val!=null && val.toString().length>0 && val!=undefined)?true:false;
		},
		getObject:function(contenedor, tipo, filtro){
			if(filtro!=null && filtro!=''){
				return $(contenedor).find(tipo).filter(filtro);
			}else{
				return $(contenedor).find(tipo);
			}
		},
		setName: function(nombreCompleto, nombres, apellidoPaterno, apellidoMaterno){
			var nombreCliente = $(nombreCompleto).val();
			var nombre = nombreCliente.split(" ");
			var maximo = nombre.length;
			var nombreClient = nombreCliente.replace(nombre[maximo-2],'').replace(nombre[maximo-1],'');
			$(nombres).val(nombreClient.trim());
			$(apellidoPaterno).val(nombre[maximo-2]);
			$(apellidoMaterno).val(nombre[maximo-1]);			
		},
		loadMensajeInfo:function(msn, isStatic){
			if($.isValid(msn)){
				$.loadMesajeGeneric(msn, '#mensajeInfoText', '#mensajeInfo', isStatic);
				//$.showInfoGeneric(msn, '#mensajeInfoText', '#mensajeInfo', isStatic);
			}
		},
		loadMensajeError: function(msn){
			if($.isValid(msn)){
				$('#mensajeExitoText').html("");
				$.loadMesajeGeneric(msn, '#mensajeErrorText', '#mensajeError', false);
			}
		},
		loadMensajeExito: function(msn){
			if($.isValid(msn)){
				$.loadMesajeGeneric(msn, '#mensajeExitoText', '#mensajeExito', false);
			}
		},
		showInfoGeneric:function(msn, contentText, divMSN, isStatic){
			msn = replaceAllCaracter(msn);
			$(contentText).html(msn);
			$(divMSN).show();
			if(!$('#backINFO').is(":visible")){
				$('#backINFO').show();
			}
			if(isStatic){
			    $('#backINFO').unbind( "click" );	 
			}
		},
		loadMesajeGeneric:function(msn, contentText, divMSN, isStatic){
			msn = replaceAllCaracter(msn);
			$(contentText).html(msn);
			$(divMSN).show();
			if(!$('#backMSN').is(":visible")){
				
				$('#backMSN').click(function(e) { //--REV
					$.closeAllMesaje();//--REV
				});//--REV
				
				$('#backMSN').show();
				$('#btnAceptarModal').show();
				$('#titleModal').text('Aviso');
			}
			if(isStatic){
			    $('#backMSN').unbind( "click" );
			    $('#btnAceptarModal').hide();
			    $('#titleModal').text('Espere...');
			}
		},
		closeMesaje: function(){
			$('#backMSN').hide();
			$('#mensajeInfo').hide();
		},
		closeAllMesaje: function(){
			$('#backMSN').hide();
			$('#mensajeInfo').hide();
			$('#mensajeError').hide();
		    $('#mensajeExito').hide();
		}
	});
})(jQuery);

/**
 *funcion para validar que solo se capturen numeros en pantalla de impresion 
 */
 function soloNumerosYGuion(campo,hasError) {
	 $("#error").hide();
	    hasError = false;
    var letra;
    var bandera = true;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 bandera=false;
    	 hasError == true;
     }
    }
 
    if (!bandera) {
        $('#txtIncios').css("background-color", "#FF0000");
        campo.value="";
        hasError = true;
    }
    if(bandera){
       $('#txtIncios').css("background-color", "#ffffff");
    }
    if(hasError == true) {
    	$("#txtIncioshidden").after('<span id="error">Ej: 1-10</span>');
    	campo.value=""; return false; 
    }
 }

/**
 *Valida Fechas 
 */
 function fechaMenorQue(fechaMenor, fechaMayor){
	if(fechaMenor!==null && fechaMenor!==headerValue && fechaMayor!==null && fechaMayor!==headerValue){
	
		fechaMenorArr = fechaMenor.split("/");	
		fechaMayorArr = fechaMayor.split("/");
	
		diaMenor = fechaMenorArr[0];
		mesMenor = fechaMenorArr[1];
		anioMenor = fechaMenorArr[2];
	
		diaMayor = fechaMayorArr[0];
		mesMayor = fechaMayorArr[1];
		anioMayor = fechaMayorArr[2];
		
		if(anioMenor<anioMayor)return true;
					
		if(anioMenor>anioMayor)return false;
		
		if(mesMenor<mesMayor)return true;
		
		if(mesMenor>mesMayor)return false;
		
		if(diaMenor<diaMayor)return true;
	}
}

 /**
  * Valida si la fecha ingresada corresponde a una persona mayor de edad
  * @param fecha
  * @returns {Boolean}
  */
function esMayorEdadServidor(fecha){
	if($.isValid(fecha)){
		return true;
	}
	var f=$("#txtFechaActual").val();
	var array_fecha = fecha.split("/"); 
  	
   	var year = parseInt(array_fecha[2]); 

   	var mes = parseInt(array_fecha[1]); 

   	var dia = parseInt(array_fecha[0]);	
	
   	var array_fechaServer=f.split("/");
   	var yearServer = parseInt(array_fechaServer[2]);
   	var mesServer =parseInt(array_fechaServer[1]);
   	var diaServer =parseInt(array_fechaServer[0]);
	if((yearServer - year) > 18)	
	return true;	
	else if((yearServer - year) < 18){
		mostrarOcultarDiv( "mensajeError", $('#mensajeError'), "Fecha de nacimiento inv\u00E1lida, debe ser mayor de edad");
		return false;	
	}
	else {	
		if (mesServer - mes > 0){ 
			return true; 
		}
		else if ((mesServer - mes) < 0){ 
			mostrarOcultarDiv( "mensajeError", $('#mensajeError'), "Fecha de nacimiento inv\u00E1lida, debe ser mayor de edad");
			return false; 
		}
		else{
			if ((diaServer - dia) >= 0){ 
				return true; 
			}
			else if ((diaServer - dia) < 0) {
				mostrarOcultarDiv( "mensajeError", $('#mensajeError'), "Fecha de nacimiento inv\u00E1lida, debe ser mayor de edad");
				return false;
			}
		}	 
	}	
}

/**
 * Valida la comision cedida del agetne en la cotizacion
 * @param valor
 * @param max
 * @param min
 * @param valorActual
 */
function validaComisionCedidaAgen(valor,max,min,valorActual){
	if(valor > max){
		alert("El valor m\u00e1ximo permitido es: " + max);
		$('#comisionCedida').val(valorActual);
	}else if(valor < min){
		alert("El valor m\u00ednimo permitido es: " + min);
		$('#comisionCedida').val(valorActual);
	}
}

/**
 * Remueve el formato numerico
 * @param objValue
 * @returns {String}
 */
function removeFormatNumber(objValue) {
    var obj1 = "";
    obj1 = objValue;
    while(obj1.lastIndexOf("$")!= -1){
    	obj1 = obj1.replace("$", "");
    }
    while(obj1.lastIndexOf(",")!= -1){
    	obj1 = obj1.replace(",", "");
    }
    return obj1;
}

/**
 * Valida la compatibilidad del Internet Explorer 
 *  y el modo vista compatibilidad
 * @returns {Boolean}
 */
function compatibleIE() {

    var browserName = navigator.appName;

    if (browserName == internetExplorer) {

        var IEVersion = getInternetExplorerVersion();
        var IEDocMode = document.documentMode;
        var IECompatibilityMode = document.compatMode;

        if (IEDocMode != undefined) {

            if (IEVersion >= 9 && IECompatibilityMode == "CSS1Compat") {
                return true;
            }

            if (IEDocMode <= 9) {
				$("#errorCargaExplorerMode").show();
				$("#formContainer").hide();
				return false;
            }
        }else{
			$("#errorCargaExplorer").show();
			$("#formContainer").hide();
			return false;
        }
    }else
    	return true;
}

/**
 * Regresa la version del explorador("solo para explorer")
 * @returns
 */
function getInternetExplorerVersion() {
    var myNav = navigator.userAgent;
    return (myNav.indexOf(MSIE) != -1) ? parseInt(myNav.split(MSIE)[1]) : false;
}



/**
 * Valida el monto de las sumas aseguradas en la tabla de coberturas
 */
function validaRangoSumaAsegurada(obj,min,max,valorDefault,index){
	aplicaRedondeo(index);
	var valor = removeFormatNumber(dwr.util.getValue(obj));
	if(parseFloat(valor) > parseFloat(max)){
		alert("El valor m\u00e1ximo permitido es: " + dwr.util.getValue("valorSumaAseguradaMax_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}else if(parseFloat(valor) < parseFloat(min)){
		alert("El valor m\u00ednimo permitido es: " + dwr.util.getValue("valorSumaAseguradaMin_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}
}

function aplicaFormatoKeyUp(index){
	$("#valorSumaAseguradaStr_"+index).keyup(function(e) {
		var e = window.event || e;
		var keyUnicode = e.charCode || e.keyCode;
		if (e !== undefined) {
			switch (keyUnicode) {
				case 16: break; // Shift
				case 17: break; // Ctrl
				case 18: break; // Alt
				case 27: this.value = ''; break; // Esc: clear entry
				case 35: break; // End
				case 36: break; // Home
				case 37: break; // cursor left
				case 38: break; // cursor up
				case 39: break; // cursor right
				case 40: break; // cursor down
				case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
				case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
				case 190: break; // .
				default: $("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1 });
			}
		}
	});
}

/**
 * Valida que los campos de un contenedor no vengan vacios
 * @param contenedor
 * @param objecto
 * @param filtro
 * @returns {Boolean}
 */
function validaObjetos(contenedor, objecto, filtro){
	var valor = false;
	var objects = $.getObject(contenedor, objecto, filtro);
		if(objects.size() > 0 ){
			$.each(objects, function( index, input ){
				if(input.value == '' || input.value == null){
					$(input).parent().parent().parent().addClass('has-error');
					valor = true;
				}
			});
		}else
			valor = false;
	return valor;
}

/**
 * Valida los campos obligatorios del cliente
 * @param contenedor
 * @param objecto
 * @param filtro
 * @returns {Boolean}
 */
function validaObjetosCliente(contenedor, objecto, filtro){
	var valor = true;
	var fisica = $("#claveTipoPersona1")[0].checked;
	var divPerso = fisica?"addDivPersonaFisica": "addDivPersonaMoral";
	var divFiscal = fisica?"addDivPersonaFisicaFiscal":"addDivPersonaMoralFiscal";
	var giroDIV = fisica?"datosGenericosADDCLIENTE":"giroDIV";
	var objects = $.getObject(contenedor, objecto, filtro);
	if(objects.size() > 0 ){
		$.each(objects, function( index, input ){
			if(input.value == '' || input.value == null){
				var parent = $(input).parent().parent().parent().parent().get(0).id;
				if(parent == divPerso || parent == divFiscal || "divDomicilio" == parent || "divDomicilioFiscal" == parent||"datosGenericosADDCLIENTE"==parent||giroDIV==parent){
					$(input).parent().parent().parent().addClass('has-error');
					valor = false;	
				}
			}
		});
	}else{
		return true;
	}
	return valor;
}

/**
 * Valida el deducible
 * @param contenedor
 * @param objecto
 * @param filtro
 * @returns {Boolean}
 */
function validaDeducibles(contenedor, objecto, filtro){
	var valor = true;
	var objects = $.getObject(contenedor, objecto, filtro);
	if(objects.size() > 0 ){
		$.each(objects, function( index, input ){
			var numero = input.id.split('_');
			numero = numero[1];
			if(input.value == '-1' && $('#claveContratoBoolean_'+numero).is(":checked")){
				$(input).parent().parent().parent().addClass('has-error');
				valor = false;
			}
		});
	}
	return valor;
}

function actualizaPantallaTipoCotizacion(tipoCotizacion){
	if(tipoCotizacion==COTIZACION_SERVICIO_PUBLICO){
		if(jspForm=="1"){
			$('#idEstado').attr('disabled', 'disabled');
		    $('#idMunicipio').attr('disabled', 'disabled');
		    $('#idNegocio').attr('disabled', 'disabled');
		    $('#idProducto').attr('disabled', 'disabled');
		    $('#idTipoPoliza').attr('disabled', 'disabled');
		    $('#inivig').attr('disabled', 'disabled');
		    $('#finvig').attr('disabled', 'disabled');
		    $('#idNegocioSeccion').attr('disabled', 'disabled'); 
		
		}else if(jspForm=="2"){
			$('#divDatosPaquete').find('select, input').attr('disabled', 'disabled');
			mostrarPagoFraccionado($('#idNegocio').val());	

		}
	}
}

function defineCamposObligatorios (tipoCotizacion){

	if(tipoCotizacion==COTIZACION_SERVICIO_PUBLICO) {
		$('#cliente\\.sexo').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.claveEstadoNacimiento').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.nombreFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.apellidoPaternoFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.apellidoMaternoFiscal').removeClass('form-control mandatory').addClass('form-control');	
		$('#cliente\\.nombreCalleFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.codigoPostalFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.nombreColoniaFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.idMunicipioFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.idEstadoFiscal').removeClass('form-control mandatory').addClass('form-control');
		$('#cliente\\.numeroTelefono').removeClass('form-control').addClass('form-control mandatory');
	}
}

/**
 * @author Luis Ibarra
 * @param idToCotizacion
 * @param vin
 */
function mostrarAlertas(idToCotizacion,vin){
	
	if($("#divAlertasSapAmis").html() == ""){
		var urlConsulta = "/MidasWeb/sapAmis/emision/llenarGridAccionesAlertas.action";
		urlConsulta = urlConsulta + '?consultaNuemeroSerie=' + vin;
		urlConsulta = urlConsulta + '&idCotizacion=' + idToCotizacion;	
		urlConsulta = urlConsulta + '&nuevoCotizador=1';
		
		sendRequestJQAgente(null,urlConsulta,"divAlertasSapAmis","mostrarAlertasUI()");
	} else {
		mostrarAlertasUI();
	}
	

}

function mostrarAlertasUI(){
	$('#divAlertasSapAmisBtn').click();
}


/**
 * @param id sap_amis_acciones_relacion
 * @param posicion en tabla
 */
function agregarAccionNuevoCotizador(id, idSubtabla){
	if($('#nuevaAccion'+id).val()!= "") {
		var urlConsulta='/MidasWeb/sapAmis/emision/agregarNuevaAccion.action';
		urlConsulta = urlConsulta + '?idRelacionSapAmis=' + id;
		urlConsulta = urlConsulta + '&descripcionAccionSapAmis=' + $('#nuevaAccion'+id).val();
		sendRequestJQAgente(null, urlConsulta, 'conenedorRespuestaAccioneSapAmis', "actualizarTabla(" + id + " ," + idSubtabla+")");
	} else {
		alert("Capture una accion.")
	}
}

function actualizarTabla(id, idSubtabla){
	var idAccion = jQuery("#conenedorRespuestaAccioneSapAmis").html().replace(/(?:\r\n|\r|\n)/g, '');
	idAccion = 1;
	if(idAccion==0){
		alert("Error al guardar la accion.")
	} else {
		//Try to get tbody first with jquery children. works faster!
		var tbody = $("#tablaAcciones"+idSubtabla).children('tbody');
		var table = tbody.length ? tbody : $("#tablaAcciones"+idSubtabla);
		table.before('<tr><td>' + $("#nuevaAccion" + id).val() + '</td><td></td></tr>');
		$("#nuevaAccion" + id).val("")
	}
}

function mostrarResumenCotizacion(){	
	if(jspForm == '4'){
		$('#resumenPaquete').html($('#nomPaquete').val());	
		$('#resumenPago').html($('#nomFormaPago').val());
	}else{
		$('#resumenPaquete').html($('#idPaquete option:selected').text());	
		$('#resumenPago').html($('#idFormaPago option:selected').text());		
	}
	$('#resumenFecha').html($('#fechaIni').val() + " AL " +($('#fechaFin').val()));
	$('#resumenCotizacion').modal('show');

}

function actualizarCodigoPostal(){
	

	var targetMunicipio = '';
	var targetIdEstado = '';
	var targetCP = '';

		targetMunicipio = 'cliente\\.idMunicipioString';
		targetIdEstado = 'cliente\\.idEstadoString';
		targetCP = 'cliente\\.codigoPostal';
}
//Verificamos si alguno de los combos antes del codigo postal no esta completo, si es asi, entonces
//se lanza la funcion de llenar la direccion por cp
//getNameColoniasPorCPAgentes(value,targetMunicipio,targetIdEstado);
//si no encuentra direccion por codigo postal se resetea el estado a "seleccione"
//se agregarn las css class jQ_ciudad y jQ_estado para la validacion
//if(!$.isValid($('#'+targetMunicipio).val())){
//	$('#'+targetIdEstado).val("");
//}


function getNameEstadoPorCPAgentes(codigoPostal, ciudad, estado) {
    if (codigoPostal != null || codigoPostal != "") {
        $.ajax({
            type     : 'POST',
            url      : "/MidasWeb/codigoPostal.do",
            data     : {id : codigoPostal},
            dataType : 'xml',
            success  : function(xmlResponse) {
                loadCombosDomicilioAgentes(xmlResponse, ciudad, estado, codigoPostal);
            }
        });
    }
}



function loadCombosZonacirculacionAgentes(doc, city, state, cp) {
    if ($.isValid(city) && $.isValid(state)) {
        var stateElement = $( "state", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(state, stateElement, 'MAP', null);
        var cityElement = $( "city", doc ).map( function() {
			return {
				id: $("id",this).text(),
				value: $( "description", this ).text(),
			}
		});
		addOptionSelect(city, cityElement, 'MAP', null);
    }
}

function limpiarcodigoPostal(){
	$("#codigoPostal").val("");
	//$("#codigoPostal").removeAttr("readonly");
	cargarEstadoNegocio($('#idNegocio').val());
}
	

function refrescarConfiguracionVisibilidad(){
	//se trae todos los divs que se habian escondido por configuracion
	var arr=document.querySelectorAll('.confVisibilidad');

	for (var i=0; i<arr.length; i++){
		// Corrección para mostrar los campos (numero de serie y tipo de vehiculo) en el cotizador individual
		removeClass(arr[i],'confVisibilidad'); 
	}
	negocio = document.getElementById('idNegocio').value;

	esconderCamposPaso(1, negocio);
}

function esconderCamposPaso(paso, idNegocio){

if(paso==1){
	negocio = document.getElementById('0idNegocioSeccion').value;
	obtenerConfYEsconderCampos('idDatosPoliza', negocio);
	obtenerConfYEsconderCampos('divZonaCirculacion', negocio);
}
if(paso==2){
	ocultarBotonSiguiente();
	obtenerConfYEsconderCampos('divDatosPaquete', negocio);
	obtenerConfYEsconderCampos('divResumenDiv', negocio);
	obtenerConfYEsconderCampos('idDeAsegurada', negocio);

}
if (paso==3){
	obtenerConfYEsconderCampos('idDatosVehiculo', negocio);
	obtenerConfYEsconderCampos('idClientes', negocio);
	obtenerConfYEsconderCampos('idAsegurados', negocio);
	obtenerConfYEsconderCampos('idObserva', negocio);
	obtenerConfYEsconderCampos('idConductor', negocio);
	
	
}
if(paso==4){
	obtenerConfYEsconderCampos('idMediosP', negocio);
	obtenerConfYEsconderCampos('divResumenDiv', negocio);
	obtenerConfYEsconderCampos('idClienteUnico', negocio);

}
}

function obtenerConfYEsconderCampos(idDiv, negocio){
	 url = "/MidasWeb/negocio/canalventa/obtenerCamposCotizadorJSON.action?idDiv="+idDiv+"&idToNegocio="+negocio;

	   jQuery
		.ajax( {
			url : url,
			dataType : 'json',
			async : false,
			success : function(data) {
			esconderCampos(data);
			},error : function(xhr, status) {
				   //console.log('####obtenerConfYEsconderCampos:'+idDiv +negocio+ ' FALLO!');
		    }
		});
}


function esconderCampos(campos) {
	     for (var i in campos){
		       if(false==campos[i].status){
		    	   var elemento=$('#' + campos[i].codigo);
		    	   if(elemento!=null){
		    		   if(elemento.css("display")!=null)
			    			  elemento.css("display", ""); 
		    		   elemento.addClass('confVisibilidad');
		    	   }
		       }
		     }
}

/**
 * Asocia los metodos al evento click de los botones correspondientes 
 * a finalizar o continuar cotizacion.
 */
function configurarBotonesContinuar()
{
	var configuracionId = jQuery("#configuracionId").val();
	var emitir = jQuery('#configuracion-emitir').val();
	
	//Boton Continuar sig paso - default visible
	jQuery("#nextBtnDetalle").click(function(e){
		guardarDetalleCobertura(false);
	});
	
	jQuery("#returnBtnCobertura").click(function(e){
		regresarDatosGen();
	});
	
	//Boton Finalizar Cotizacion - default no visible
	jQuery("#nextBtnDetalleFinalizaCot").click(function(e){
		guardarDetalleCobertura(true);				
	});

}

function validarContratanteAsignadoCotizacion() {
	
	var idCotizacion = jQuery('#idToCotizacion').val();; 

	validacionService.validarContratanteAsignadoCotizacion(idCotizacion,function(tieneContratante){
				
		if(tieneContratante)
		{		
			guardarDatosComplementarios();
			
		}else
		{	
			
			jQuery.loadMensajeError("Debe de seleccionar un contratante antes de continuar");
		}			 
			 
	});

}

/**
 * Remover la clase que se mande de parametro
 * @param el :  Elemento
 * @param classNameToRemove :  Nombre de clase Clase
 */
function removeClass(el, classNameToRemove){
    var elClass = ' ' + el.className + ' ';
    while(elClass.indexOf(' ' + classNameToRemove + ' ') !== -1){
         elClass = elClass.replace(' ' + classNameToRemove + ' ', '');
    }
    el.className = elClass;
}

function mostrarDaniosCargaInclude(index){
	if(index!=null){
		if($("#claveContratoBoolean_"+index).is(":checked")){
			$("#valorSumaAseguradaStr_"+index).removeAttr("readonly");
			$("#valorDeducible_"+index).removeAttr("readonly");
		}else{
			$("#valorSumaAseguradaStr_"+index).attr("readonly","readonly");
			$("#valorDeducible_"+index).attr("readonly","readonly");
		}
	}
	recalcularCotizador(false);
}

/**
 * Carga listado de vigencias asociadas al negocio
 * @author Adriana Flores
 * @param {integer}
 */
function cargaTiposVigenciaNegocio(idNegocio){
	if(idNegocio != null  && idNegocio != headerValue){
		listadoService.obtenerTiposVigenciaNegocio(idNegocio,
				function(data){
					deshabilitarVigencias(jQuery.isEmptyObject(data));
					addOptions('idTipoVigencia',data);
					if (sizeMap(data) > 1) {
						 listadoService.obtenerVigenciaDefault(idNegocio, function(dataDefault){
							 selectValorDefault('idTipoVigencia', dataDefault);
							 actualizarFechas(dataDefault);
						 });					
					}
				});
	}else{
		addOptions('idTipoVigencia',null);
	}
}
function selectValorDefault(select, valorDefault){
	document.getElementById(select).value = valorDefault;		
}
function selectFirstValue(select){
	// use first select element
	var el = document.getElementById(select); 
	// assuming el is not null, select 2th option
	el.selectedIndex = 1;
	
	jQuery("#"+select).change();
}
/**
 * Calcular las fechas dependiendo el tipo de vigencia seleccionada
 * @author Adriana Flores
 * @param {integer}
 */
function actualizarFechas(dias){
	var fechaInicioVigencia = new Date();
	var diasVigencia = parseInt(dias);
	
	var fechaFinVigencia = new Date();
	fechaFinVigencia.setDate(fechaInicioVigencia.getDate() + diasVigencia);	
	
	dwr.util.setValue('inivig', fechaInicioVigencia.getDate() + '/' + (fechaInicioVigencia.getMonth() + 1) + '/' + fechaInicioVigencia.getFullYear());
	dwr.util.setValue('finvig', fechaFinVigencia.getDate() + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getFullYear());
}

/**
 * Cambiar la visibilidad de campos: Tipo vigencia, fechas inicio y fin de vigencia 
 * @author Adriana Flores
 */
function deshabilitarVigencias(bool){
	
	if(!bool){
		
		jQuery("#idTipoVigencia").attr("disabled", false);
		jQuery("#inivig").attr("readonly", true);
		jQuery("#finvig").attr("readonly", true);
		jQuery("#inivig").unbind("click").unbind("focus");
		jQuery("#finvig").unbind("click").unbind("focus");
	 } else {
		jQuery("#idTipoVigencia").attr("disabled", true);
		jQuery("#inivig").attr("readonly", false);
		jQuery("#finvig").attr("readonly", false);
		actualizarFinVigencia();
	 }	
}
