/**
 * 
 */

function cancelarAutorizacion() {
	aplicaEndoso = dwr.util.getValue("aplicaEndoso");
	if (aplicaEndoso == 'false') {
		if (confirm('\u00BFEst\u00e1 seguro que desea regresar para modificar la cotizaci\u00F3n?')) {
			var mensaje = "Las excepciones detectadas requieren de una autorizaci\u00F3n o bien ser modificadas con datos permitidos en el negocio.";
			mostrarVentanaMensaje("30", mensaje, "regresaACotizacion()");
		}
	} else {
		if (confirm('\u00BFEst\u00e1 seguro que desea regresar para modificar el endoso?')) {
			tipoEndoso = dwr.util.getValue("tipoEndoso");
			var mensaje = "Las excepciones detectadas requieren de una autorizaci\u00F3n o bien ser modificadas con datos permitidos en el negocio.";
			if(tipoEndoso == 5){
				mostrarVentanaMensaje("30", mensaje, "regresaACotizacionEndosoAlta()");
			}else{
				if(tipoEndoso == 7){
					mostrarVentanaMensaje("30", mensaje, "regresaACotizacionEndosoCambioDatos()");
				}
			}
		}
	}
}

function regresaACotizacion(){
	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/terminarcotizacion/redireccionarCotizacion.action?id='+dwr.util.getValue("id"),'contenido', null);
}

function regresaACotizacionEndosoAlta(){
	parent.sendRequestJQ(null, '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/redireccionarEndoso.action?cotizacion.continuity.numero='+dwr.util.getValue("id")+"&namespaceOrigen="+dwr.util.getValue("namespaceOrigen")+"&actionNameOrigen="+dwr.util.getValue("actionNameOrigen")+"&polizaId="+dwr.util.getValue("polizaId"),'contenido', null);
}

function regresaACotizacionEndosoCambioDatos(){
	parent.sendRequestJQ(null, '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos/mostrarCambioDatos.action?accionEndoso='+dwr.util.getValue("accionEndoso")+"&namespaceOrigen="+dwr.util.getValue("namespaceOrigen")+"&actionNameOrigen="+dwr.util.getValue("actionNameOrigen")+"&polizaId="+dwr.util.getValue("polizaId")+"&fechaIniVigenciaEndoso="+dwr.util.getValue("fechaIniVigenciaEndoso"),'contenido', null);
}

function enviarAutorizacion(){
	if(confirm('\u00BFEst\u00e1 seguro que desea solicitar la autorizaci\u00F3n de las excepciones encontradas?')){
		parent.sendRequestJQ(document.resultadoTerminarCotizacionForm, '/MidasWeb/suscripcion/cotizacion/auto/terminarcotizacion/enviarSolicitudes.action','contenido', null);
	}
}


function mostrarExcepcionDetalle(id){
	var url = verExcepcionDetallePath + "?idExcepcion=" + id;
	mostrarVentanaModal("excepcionDetalle", "Excepci\u00F3n Ocurrida", 50, 50, 600, 500, url);	
}

