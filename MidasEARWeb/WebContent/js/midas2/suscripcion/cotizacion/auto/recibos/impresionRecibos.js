/**
 * 
 */
var polizasGrid;
var paginadoParamsRenPath;
var recibosGrid;

function initImpresionRecibos(){	
	if (agenteControlDeshabilitado) {
		jQuery("#idAgenteClavePol").attr("readonly", true);
	}
	
	if (promotoriaControlDeshabilitado) {
		jQuery("#promotoriaId").attr("readonly", true);		
	}
}

function listarRecibosPaginado(page, nuevoFiltro){
	jQuery('#polizasReciboDiv').empty();
	var posPath = '&posActual='+page+'&funcionPaginar='+'listarRecibosPaginado'+'&divGridPaginar='+'polizaGrid';
	if(nuevoFiltro){
		paginadoParamsRenPath = jQuery(document.impresionRecibosForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'listarRecibos(1);';
	
	mostrarIndicadorCarga('indicador');	
	sendRequestJQTarifa(null, "/MidasWeb/impresiones/recibos/buscarPolizasPaginado.action" + "?filtrar=true" + "&" + paginadoParamsRenPath + posPath, 'gridPolizasPaginado', nextFunc);
} 

function listarRecibos(esBusqueda){
	document.getElementById("polizaGrid").innerHTML = '';
	polizasGrid = new dhtmlXGridObject("polizaGrid");
	polizasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	polizasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	polizasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	polizasGrid.load("/MidasWeb/impresiones/recibos/buscarPolizas.action?filtrar=true" + "&" + paginadoParamsRenPath + "&"+ posPath);
	
	if(esBusqueda == 1)//1 = metodo invocado desde boton busqueda
	{		
		displayFiltersRecibos();		
	}
	else //0 = metodo invocado desde paginacion
	{
		selectGridRecibosSize();		
	}	
	
}

function sizeGridRecibos(){
	jQuery(document).ready(function(){
		jQuery('#polizaGrid').css('height','320px');
		jQuery('div[class=objbox]').css('height','275px');
	})
}

function reSizeGridRecibos(){
	jQuery(document).ready(function(){
		jQuery('#polizaGrid').css('height','140px');
		jQuery('div[class=objbox]').css('height','100px');
	})
}

function displayFiltersRecibos(){
	jQuery('#contenedorFiltros').toggle('fast', function() {
			if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
				jQuery('#mostrarFiltros').html('Mostrar Filtros')
				sizeGridRecibos();
			} else {
				jQuery('#mostrarFiltros').html('Ocultar Filtros')
				reSizeGridRecibos();
			}
		});
}

function selectGridRecibosSize()
{		
	if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
		jQuery('#mostrarFiltros').html('Mostrar Filtros')
		sizeGridRecibos();
	} else {
		jQuery('#mostrarFiltros').html('Ocultar Filtros')
		reSizeGridRecibos();
	}
}

function seleccionarPromotoria(){
	parent.mostrarVentanaModal("promotoria","Selecci\u00F3n Promotoria",200,320, 510, 150, seleccionarPromotoriaPath);	
}

function cargarPromotoria(id,nombre){
	jQuery('#idPromotoria').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#descripcionBusquedaPromotoria').text(nombre);
	cerrarVentanaModal("promotoria");
}

function mostrarSeleccionarNombre(){
	var ids = cargaIdRecibos();
	if(ids === "" || ids === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Enviar al Proveedor.");
	}else{
		creaIdRecibosEnVentana(ids);
		var form = jQuery('#impresionRecibosForm')[0];
		var validarPolizaPath = "/MidasWeb/impresiones/recibos/seleccionarNombre.action";
		//form.setAttribute("action", validarPolizaPath);
		mostrarVentanaModal("seleccionarNombre","Seleccione",200,320, 470, 160, validarPolizaPath);
	}
}

function setNombreArchivo(nombreArchivo){
	jQuery("#nombreArchivo").val(nombreArchivo);
	sendRequestJQ(jQuery('#impresionRecibosForm')[0], "/MidasWeb/impresiones/recibos/enviarProveedor.action", 'contenido', 'cerrarVentanaModal("seleccionarNombre")');
	cerrarVentanaModal("seleccionarNombre");
}

function mostrarContenedorImpresion(){
	var ids = cargaIdRecibos();
	if(ids === "" || ids === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Imprimir Recibo.");
	}else{
		creaIdRecibosEnVentana(ids);
		var form = jQuery('#impresionRecibosForm')[0];
		var validarPolizaPath = "/MidasWeb/impresiones/recibos/mostrarContenedorImpresion.action";
		//form.setAttribute("action", validarPolizaPath);
		parent.mostrarVentanaModal("mostrarContenedorImpresion","Imprimiendo",200,320, 470, 320, validarPolizaPath);
	}
}

function mostrarContenedorEmail(){
	var ids = cargaIdRecibos();
	if(ids === "" || ids === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Imprimir Recibo.");
	}else{
		creaIdRecibosEnVentana(ids);
		var form = jQuery('#impresionRecibosForm')[0];
		var validarPolizaPath = "/MidasWeb/impresiones/recibos/mostrarEnviarEmailMasivo.action";
		parent.mostrarVentanaModal("mostrarContenedorEmail","Email",200,320, 470, 320, validarPolizaPath);
	}
}

function mostrarImprimirRecibos(){
	var ids = cargaIdRecibos();
	if(ids === "" || ids === undefined){
		parent.mostrarVentanaMensaje('10', "Seleccione al menos una P\u00F3liza a Imprimir Recibo.");
	}else{
		creaIdRecibosEnVentana(ids);
		var form = jQuery('#impresionRecibosForm')[0];
		var validarPolizaPath = "/MidasWeb/impresiones/recibos/generarImpresion.action";
		//form.setAttribute("action", validarPolizaPath);
		//parent.mostrarVentanaModal("mostrarContenedorImpresion","Imprimiendo",200,320, 470, 320, validarPolizaPath + '?' + jQuery(form).serialize());
		parent.redirectVentanaModal("mostrarContenedorImpresion", validarPolizaPath, form);
	}
}

function cargaIdRecibos(){
	var idPolizas = "";
	var num = 0;
	if(polizasGrid != undefined){
	var idCheck = polizasGrid.getCheckedRows(0);
	if(idCheck != ""){
		return idCheck;
	}
	}
	return idPolizas;
}

function creaIdRecibosEnVentana(idPolizas){
	jQuery('#polizasReciboDiv').empty();
	if(idPolizas != null && idPolizas != ""){
		var ids = idPolizas.split(",");
		var num = 0;		
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#polizasReciboDiv').append('<input type="hidden" name="polizaList['+ num+ '].idToPoliza" value="' + ids[i] + '" />');
	   	 	num = num + 1;		
		}
	}
}



function limpiarFiltrosRecibos(tipo){
	jQuery("input").each(function(){
		if(jQuery(this).attr("type") == "text"){
			if (jQuery(this).attr("id") == "idAgenteClavePol" && agenteControlDeshabilitado) {
				return;
			}
			jQuery(this).val('');
		}
	});
	if(tipo == 1 && !agenteControlDeshabilitado){
		cleanInputDiv("agenteNombre");
	}
	jQuery("select").each(function(){
		jQuery(this).val('');
		jQuery(this).change();
	});
}

function initRecibosGrid(){
	document.getElementById("recibosGrid").innerHTML = '';
	recibosGrid = new dhtmlXGridObject("recibosGrid");
	recibosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	recibosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	recibosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	recibosGrid.load("/MidasWeb/impresiones/recibos/mostrarRecibosPoliza.action?"+jQuery(document.recibosPolizaForm).serialize());		
}

function imprimirReciboLlaveFiscal(idRecibo){
	var url = '/MidasWeb/impresiones/recibos/imprimirReciboLlaveFiscal.action?idRecibo='+idRecibo+'&'+jQuery(document.recibosPolizaForm).serialize();
	window.open(url);
}

function cargaProgramaPagosImpresion(numeroEndoso){
	var idToPoliza     	=  dwr.util.getValue("idToPoliza");
	if(numeroEndoso != null  && numeroEndoso != headerValue){
		listadoService.getMapProgramaPagos(idToPoliza, numeroEndoso,
				function(data){
					addOptions('progPago',data);
					if (sizeMap(data) > 1) {				
						selectFirstValue('progPago');
					}
				});	
	}else{
		addOptions('progPago',null);
		addOptions('incisos', null);
	}
}

function selectFirstValue(select){
	// use first select element
	var el = document.getElementById(select); 
	// assuming el is not null, select 2th option
	el.selectedIndex = 1;
	
	jQuery("#"+select).change();
}

function verMovimientosRecibo(idRecibo){
	var validarPolizaPath = "/MidasWeb/impresiones/recibos/mostrarMovimientosRecibo.action?idRecibo="+idRecibo+'&'+jQuery(document.recibosPolizaForm).serialize();
	parent.mostrarVentanaModal("mostrarMovimientosRecibos","Movimientos Recibo",200,320, 470, 320, validarPolizaPath);
}

function regenerarRecibo(idRecibo){
	if(confirm(' Esta accion regenerara el Recibo con una nueva Llave Fiscal. \u00BFEsta seguro que desea regenerar el Recibo?')){
		var url = '/MidasWeb/impresiones/recibos/regenerarRecibo.action?idRecibo='+idRecibo+'&'+jQuery(document.recibosPolizaForm).serialize();
		window.open(url);
	}
}

function cargarIncisosProgramaPago(idProgPago){		
	jQuery('#incisos option').remove();
	
	if(idProgPago != null && idProgPago != '' && idProgPago != '-1'){
		var idPoliza = jQuery('#idToPoliza').val();
		listadoService.getIncisosProgPago(idPoliza, idProgPago, function(data){
			var miArray = data.split(",");
			
			jQuery.each(miArray, function(index, value){
				jQuery("#incisos").append('<option value="'+value+'">'+value+'</option>');
			});
			
			initRecibosGrid();
		});
	}else{
		addOptions('incisos', null);
	}	
}