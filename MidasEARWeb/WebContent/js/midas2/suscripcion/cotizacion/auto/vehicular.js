/**
 * Funciones para cargar informacion vehicular 
 * cuando se ingrese el numero de serie (VIN)
 * en la cotizacion
 * 
 */

var tamanioVin = 17;
var numeroSerie;
var idNegocioSeccion;
var idMoneda;
var idEstado;
var idToCotizacion;
var nuevoCotizador=false;
var vin;

function mostrarInfVehicularById(serie, negocioSeccion, moneda, estado,
		cotizacion, marca, estilo, modelo, descripcion, tipoUso, nuevoCotizador) {
	fijarNuevo(nuevoCotizador);
	obtenerVinById(serie);
	if (validate()) {
		obtenerValoresById(negocioSeccion, moneda, estado, cotizacion);
		if (validarLinea()){
			limpiarPorVin(marca,negocioSeccion);
			if(this.nuevoCotizador){
				jQuery('#icon_ValidaVin').removeClass('glyphicon-question-sign');
			}
			obtenerInfVehicular(urlMostrarVehicular, marca, estilo, modelo, descripcion, tipoUso);			
		}
	}
}

/**
 * 
 * @param marca
 * @param negocioSeccion
 */
function limpiarPorVin(marca, negocioSeccion){
	limpiarValores();
	if (this.nuevoCotizador){
		onChangeLineaNegocio('idTipoUso', 'idNegocioSeccion');
		validaEstiloVehiculo();
	}
	else{
		onChangeLineaNegocio(marca, negocioSeccion);
	}
}

function limpiarPorLinea(vinInput){
	limpiarValores();
	jQuery('#'+suprCaracteres(vinInput)).val('');
}

function limpiarValores(){
	jQuery("#vinValidoId").val('false');
	jQuery("#observacionesSesa").val('');
	jQuery("#coincideEstiloId").val('false');	
}

/**
 * 
 * @returns {Boolean}
 */
function validarLinea(){
	var valor = this.idNegocioSeccion;
	if (valor != null && valor != undefined && valor != ''){
		return true;
	}
	else{
		avisar('30','Favor de seleccionar Línea de Negocio');
		return false;
	}
}

/**
 * 
 * @param nuevoCotizador
 */
function fijarNuevo(nuevoCotizador){
	if (nuevoCotizador){
		this.nuevoCotizador = true;
	}
}

/**
 * 
 * @param negocioSeccion
 * @param moneda
 * @param estado
 * @param cotizacion
 */
function obtenerValoresById( negocioSeccion, moneda, estado, cotizacion ){
	
	this.idNegocioSeccion = jQuery('#'+suprCaracteres(negocioSeccion)).val();
	this.idMoneda = jQuery('#'+suprCaracteres(moneda)).val();
	this.idEstado = jQuery('#'+suprCaracteres(estado)).val();
	this.idToCotizacion = jQuery('#'+suprCaracteres(cotizacion)).val();
}

/**
 * 
 * @param serie
 */
function obtenerVinById( serie ){
	
	this.vin = jQuery('#'+suprCaracteres(serie)).val();
}

/**
 * vehiculo: Entidad de transporte: VehiculoView.java
 * url: Se define desde el header jsp
 *
 * @param url
 * @param marca
 * @param estilo
 * @param modelo
 * @param descripcion
 * @param tipoUso
 */
function obtenerInfVehicular(url, marca, estilo, modelo, descripcion, tipoUso) {
	
	bloquearVista();
	jQuery.ajax({
		type : 'POST',
		url : url,
		data : crearData(),
		dataType : 'json',
		success : function(data) {
			fijarVinValido(data.tipoMensaje, data.mensaje, data.infoVehicular);
			fijarCoincideEstilo(data.infoVehicular);
			setOptions(data.infoVehicular, marca, estilo, modelo, descripcion);
			fijaTipoUso(data.infoVehicular, tipoUso);
		},
		complete : function(data) {
			desbloquearVista();
		}
	});
}

/**
 * 
 * @param data
 * @param marcaId
 * @param estiloId
 * @param modeloId
 * @param descripcionId
 */
function setOptions(data, marcaId, estiloId, modeloId, descripcionId){
	
	if(sizeMap(data) >= 1){
		modeloSel = traerModeloSeleccionado(data);
		jQuery.each(data, function(index, valor) {
			
			if(index=="marca"){				
				addOptionsSelect( valor, suprCaracteres(marcaId), 'select', null, null );				
			}
			if(index=="estilo"){
				if (nuevoCotizador){
					fijarEstiloNuevoCotizador( valor, suprCaracteres(estiloId) );		
				}
				else{
					addOptionsSelect( valor, suprCaracteres(estiloId), 'select', null, null );
				}			
			}
			if(index=="modelo"){
				addOptionsSelect( valor, suprCaracteres(modeloId), 'select', modeloSel, 'seleccione' );
			}
			if(index=="descripcion"){
					addOptionsSelect( valor, suprCaracteres(descripcionId), 'input', null, null );
					if (nuevoCotizador){
						fijaDescripcionNuevoCotizador( valor );		
					}		
			}
		});
	}
}

/**
 * 
 * @param valores
 * @param idInput
 * @param type
 */
function addOptionsSelect( valores, idInput, type, valSelected, header ){
	
	var opt="";
	jQuery.each(valores, function(index, valor) {		
		var selectedText = index == valSelected ? 'selected' : '';
		if (type == 'select'){
			opt=opt+"<option value='"+index+"' " +selectedText+ ">"+valor+"</option>";
		}
		else{
			opt = valor;
			jQuery('#'+idInput).val(opt);
		}		
	});
	if (type == 'select' && header != null && !nuevoCotizador){
		opt = "<option value=''>Seleccione...</option>" + opt;
	}
	jQuery('#'+idInput).html(opt);
}

function fijarEstiloNuevoCotizador(valores, idInput){
	
	jQuery.each(valores, function(index, valor) {
		jQuery('#estiloSeleccionado').val(index);
		jQuery('#vehiculoEstiloName').val(valor);
	});	
}

function fijaDescripcionNuevoCotizador(valores){
	jQuery.each(valores, function(index, valor) {
		jQuery('#lblEstilo').html(valor);
	});
}

/**
 * vinValidoId, observacionesSesa: ids de parametros hiddens
 * 
 * @param tipo
 * @param mensaje
 */
function fijarVinValido(tipo, mensaje, data) {

	if (tipo == '30') {
		jQuery("#vinValidoId").val('true');
		fijarClaves(data);
		if(this.nuevoCotizador){
			jQuery('#icon_ValidaVin').addClass('glyphicon-ok-sign');
		}
			
	} else {
		if(this.nuevoCotizador){
			jQuery('#icon_ValidaVin').addClass('glyphicon-question-sign');
		}
		jQuery("#vinValidoId").val('false');
		if (mensaje!=undefined && mensaje!='desactivada'){
			jQuery("#observacionesSesa").val(mensaje);
		}
		//jQuery("#coincideEstiloId").val('false');
	}
	if (mensaje!=undefined && mensaje!='desactivada'){
		avisar(tipo, mensaje);	
	}	
}

/**
 * 
 * @param data
 */
function fijarCoincideEstilo(data) {

	if (sizeMap(data) >= 1) {
		var nocoincide = true;

		jQuery.each(data, function(index, valor) {
			if (index == "coincideEstilo") {
				jQuery("#coincideEstiloId").val('true');
				nocoincide = false;
			}
		});
		if (nocoincide) {
			jQuery("#coincideEstiloId").val('false');
		}
	}
	else{
		jQuery("#coincideEstiloId").val('false');
	}
}

/**
 * 
 * @returns {Boolean}
 */
function validate() {
	var param = this.vin;
	var mensaje='Número de serie incorrecto. Ingrese el número de serie de 17 caracteres.';
	if (param != null && param != undefined && param != '') {		
		if (param.length >= tamanioVin){
			return true;
		}
		else{
			avisar('30',mensaje);
			return false;
		}
	}
	else{
		avisar('30',mensaje);
		return false;
	}
}

/**
 * 
 * @param target
 */
function fijaTipoUso(dataInf, target){
	
	if (sizeMap(dataInf) >= 1) {
		var targetTipoUso = target;
		if(esValido(this.idNegocioSeccion)){
			//restartCombo(target, headerValue, "Cargando...", "id", "value");
			listadoService.getMapTipoUsoVehiculoByNegocio(this.idNegocioSeccion,
			function(data){
				addOptionsSelect(data,suprCaracteres(targetTipoUso),'select',null,'seleccione');
			});
		}else{
			addOptions(targetTipoUso,null);
		}		
	}
}

/**
 * 
 * @param data
 */
function traerModeloSeleccionado(data){
var resultado = '';	
	if(sizeMap(data) >= 1){
		jQuery.each(data, function(index, valores) {			
			if(index=="modeloSel"){				
				jQuery.each(valores, function(ind, valor) {
					resultado = valor;
				});	
			}
		});
	}
	return resultado;
}

/**
 * 
 * @returns
 */
function crearData(){
	
	if (this.nuevoCotizador){		
		return { 'vehiculo.numeroSerie' : this.vin,
			'vehiculo.idNegocioSeccion' : this.idNegocioSeccion,
			'vehiculo.idMoneda' : this.idMoneda,
			'vehiculo.idEstado' : this.idEstado,
			'incisoCotizacion.id.idToCotizacion' : this.idToCotizacion };
	}
	else{
		return { 'VIN' : this.vin,
			'idNegocioSeccion' : this.idNegocioSeccion,
			'idMoneda' : this.idMoneda,
			'idEstado' : this.idEstado,
			'incisoCotizacion.id.idToCotizacion' : this.idToCotizacion };
	}
}

/**
 * 
 */
function bloquearVista(){
	if (this.nuevoCotizador){
		jQuery('#numeroSerie').addClass('ui-autocomplete-input ui-autocomplete-loading');
	}
	else{
		parent.blockPage();	
	}
}

/**
 * 
 */
function desbloquearVista(){
	if (this.nuevoCotizador){
		jQuery('#numeroSerie').removeClass('ui-autocomplete-input ui-autocomplete-loading');	
	}
	else{
		parent.unblockPage();
	}
}

/**
 * 
 * @param tipo
 * @param mensaje
 */
function avisar(tipo, mensaje){
	if (this.nuevoCotizador){
		mostrarOcultarDiv('mensajeError', jQuery('#mensajeErrorText'),mensaje);
		$('html,body').scrollTop(jQuery('#mensajeErrorText'));
	}
	else{
		parent.mostrarVentanaMensaje(tipo, mensaje, null);
	}
}

/**
 * 
 * @param param
 * @returns
 */
function suprCaracteres(param){
	return param.replace(/\./gi,'\\.');
}

function esValido(val){
	return (val!=null && val.toString().length>0)?true:false;
}

function fijarClaves(data) {
	if(sizeMap(data) >= 1){		
		jQuery.each(data, function(indices, valores) {
			if (indices == "clavesAmis") {
				jQuery.each(valores, function(indice, valor) {
					if (indice == "claveAmis") {
						jQuery("#claveAmis").val(valor);
					}
				});
			} else if (indices == "clavesSesa") {
				jQuery.each(valores, function(indice, valor) {
					if (indice == "claveSesa") {
						jQuery("#claveSesa").val(valor);
					}
				});
			}
		});
	}
}