/**
 * @author martin
 */
var infoTitularUrl = "/MidasWeb/suscripcion/cotizacion/auto/cabranza/complementarInfoTitular.action";
var infoCuentasUrl = "/MidasWeb/suscripcion/cotizacion/auto/cabranza/complementarInfoCuenta.action";
var infoTarjetaUrl = "/MidasWeb/suscripcion/cotizacion/auto/cabranza/obtenerDatosTarjeta.action";
/**
 * Llena los combos si ya existe una cobertura guardada
 */
	function inicializar(){
		jQuery("div[id=datosTarjeta]").hide();
		if (jQuery('#medioPagoDTOs').val() != "" && jQuery('#medioPagoDTOs').val() != 15) {
			onchangeTitularIgual();
			onchangeConductos();
			jQuery("div[id=datosTarjeta]").show('slow');
		}
		if(jQuery('#medioPagoDTOs').val() == 15) {
			jQuery("div[id=checkDatos]").hide('slow');
			jQuery("div[id=conductos_div]").hide('slow');
			jQuery("div[id=datosTarjeta]").hide();
		}
	}
	
	inicializar();
	
function onchangeMedios() {
		if (jQuery('#medioPagoDTOs').val() == ""
				|| jQuery('#medioPagoDTOs').val() == 15) {
			jQuery("div[id=checkDatos]").hide('slow');
			jQuery("div[id=conductos_div]").hide('slow');
			cleanTitularForm();
			cleanInfoCuentaForm();
			jQuery('#infoCuentasCobranza').html('');
			jQuery('#second').html('');
			jQuery('#titularIgual').val('');
			jQuery("div[id=datosTarjeta]").hide('slow');
		} else {
			if(obtieneMediosDeCobroPorPesona()){
				cleanInfoCuentaForm();
				jQuery("div[id=conductos_div]").show('slow');
				jQuery("div[id=datosTarjeta]").show('slow');
				onchangeConductos();
			}
		}
}
/**
 * Llena el combo de conductos disponibles para la persona
 */
function obtieneMediosDeCobroPorPesona() {
	if(!validarExisteContratante())
	{
		return false;		
	}
	
	listadoService.getConductosCobro(idToPersonaContratante, jQuery(
		'#medioPagoDTOs').val(), function(data) {
			poblarConductos(document.getElementById('conductos'), data)
		});
	
	return true;
}

function poblarConductos(target, data){
	dwr.util.removeAllOptions(target);
	dwr.util.addOptions(target, [{id: '-1', value:"Seleccione ..." }],"id", "value");
	dwr.util.addOptions(target, data);
}

/**
 * complementa la informacion de la cuenta
 */
function onchangeConductos() {
	if (jQuery('#conductos').val() != "" && dwr.util.getText('conductos') != 'Agregar nuevo...') {
		// Cargar un conducto
		var url = infoCuentasUrl + "?idToCotizacion="
				+ dwr.util.getValue('cotizacionId') + "&idMedioPago="
				+ jQuery('#medioPagoDTOs').val() + "&idConductoCobro="
				+ jQuery('#conductos').val();
		sendRequestJQ(null, url, "infoCuentasCobranza", null);
		jQuery("div[id=checkDatos]").show('slow');
	} else if (jQuery('#conductos').val() != '') {
		// Agregar nuevo conducto
		jQuery("div[id=checkDatos]").show('slow');
		sendRequestJQ(null, infoCuentasUrl + "?idToCotizacion="
				+ dwr.util.getValue('cotizacionId')+ "&idMedioPago="
				+ jQuery('#medioPagoDTOs').val(),"infoCuentasCobranza","cleanInfoCuentaForm()");
	}
}

/**
 * complementa la informacion del titular
 */
function onchangeTitularIgual(idConducto) {
	jQuery("div[id=conductos_div]").show('slow');
	if (jQuery('#titularIgual').val() == 'true') {
		var url = infoTitularUrl + "?idToCotizacion="
				+ dwr.util.getValue('cotizacionId') + "&datosIguales="
				+ jQuery('#titularIgual').val() + "&idConductoCobro="
				+ jQuery('#conductos').val() + "&idMedioPago="
				+ jQuery('#medioPagoDTOs').val();
		sendRequestJQ(null, url, "second", null);
	}else if(jQuery('#titularIgual').val() == 'false' || jQuery('#titularIgual').val() == ''){
		var url = infoTitularUrl + "?idToCotizacion="
		+ dwr.util.getValue('cotizacionId') + "&datosIguales="
		+ jQuery('#titularIgual').val() + "&idMedioPago="
		+ jQuery('#medioPagoDTOs').val();
		sendRequestJQ(null, url, "second", null);
	}
}
/**
 * Llena el compo de estados
 */
function onchangePaisCobranza() {
	var idPais = jQuery('#idPais').val();
	if (idPais != "" && idPais != null) {
		listadoService.getMapEstados(idPais, function(data) {
			addOptions(document.getElementById('idEstadoCobranza'), data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idEstadoCobranza');
		jQuery('#idEstadoCobranza').change();
	}
}
/**
 * Llena el combo de municipios
 */
function onchangeEstadoCobranza() {
	var idEstado = jQuery('#idEstadoCobranza').val();
	if (idEstado != "" && idEstado != null) {
		listadoService.getMapMunicipiosPorEstado(idEstado, function(data) {
			addOptions(document.getElementById('idMunicipioCobranza'), data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idMunicipioCobranza');
		jQuery('#idMunicipioCobranza').change();
	}
}
/**
 * Llena el combo de colonias
 */
function onchangeMunicipioCobranza() {
	var idMunicipio = jQuery('#idMunicipioCobranza').val();
	if (idMunicipio != "" && idMunicipio != null) {
		listadoService.getMapColonias(idMunicipio, function(data) {
			addOptions(document.getElementById('idColoniaCobranza'), data);
		});
	} else {
		removeAllOptionsAndSetHeaderDefault('idColoniaCobranza');
		jQuery('#idColoniaCobranza').change();
	}
}

function onchangeCp(){
	listadoService.getEstadoIdPorCp(jQuery('#codigoPostal').val(),function(data){
		if (data != null) {
			jQuery('#idEstadoCobranza').val(data);
			jQuery('#idEstadoCobranza').change();
		}	
	});
}

function obtenerDatosTarjeta(){
	if(idToPersonaContratante == "" || idToPersonaContratante == null){
		mostrarMensajeInformativo("Se necesita un contratante, por favor dirigase a la pesta\u00F1a Complem. Emisi\u00f3n y seleccione un contratante antes de continuar. ", "10");
	}
	mensajeConfirmacion='¿Confirma la busqueda de Tarjetas del Cliente seleccionado?';
	mostrarMensajeConfirm(mensajeConfirmacion,"20","procesaCuentasCliente()",null,null,null);
}

function procesaCuentasCliente(){
	var url =infoTarjetaUrl+ "?idToCotizacion="+ dwr.util.getValue('cotizacionId')+ "&idMedioPago="+ jQuery('#medioPagoDTOs').val();
	sendRequestJQ(null, url, null, "onchangeMedios()");
}


function guardarCobranza() {
	
	if(jQuery('#medioPagoDTOs').val() != 15 && !validarExisteContratante())
	{
		return false;		
	}

	var url = guardarCobranzaUrl + "?idToCotizacion="
			+ dwr.util.getValue('cotizacionId');
	if (jQuery('#medioPagoDTOs').val() == 15) {
		sendRequestJQ(jQuery('#cobranzaForm'), url, targetWorkArea, null);
	} else if (jQuery('#medioPagoDTOs').val() != ''
			&& validateAll(false, 'save')) {
		sendRequestJQ(jQuery('#cobranzaForm'), url, targetWorkArea, null);
	} else {
		mostrarMensajeInformativo(checkForm(), "10",null,null);
	}

}

/**
 * Obtiene los nombres de los campos con error
 * para mostrarlos en guardarCobranza
 * @returns string mensaje con los campos que se tienen complementar
 */
function checkForm(){
	if(jQuery.browser.msie){
		return "Por favor complementa la informaci\u00f3n señalada en rojo";
	}else {
		 var mensaje = " <ul>Por favor complementa la informaci\u00f3n:";
			jQuery(".errorField").each(function(index, value) {
				label = jQuery('td[for=' + value.id + ']').text();
				if(label != 'undefined' && value.id != 'titularIgual'){
					mensaje += "<li>"+label+"</li>";
				}
				}
			);
			mensaje = mensaje + "</ul>";
			return mensaje;
	}
		 
}

/**
 * Limpia los input dentro del div titularCuenta
 */
function cleanTitularForm() {
	jQuery('#second').find('input').val('');
	jQuery('#second').find('select').val('');
}
/**
 * Limpia los input dentro del div infoCuenta
 */
function cleanInfoCuentaForm() {
	jQuery('#infoCuentasCobranza').find('input[type!=hidden]').val('');
	//jQuery('#infoCuentasCobranza').find('select').val('');
	
	cleanPromociones();
}

function cleanPromociones(){
	var promos = document.getElementById('promociones');
	
	if(promos != null){
		promos.options.length = 1;
	}
}

function getPromos(){
	var numTarjeta = jQuery('#promociones').val();
	
	cleanPromociones();
	
	if(numTarjeta != null){
		listadoService.getPromocionesTC(numTarjeta,function(data){
			addOptions(document.getElementById('promociones'), data);
			if(document.getElementById('promociones').options.length>1 || jQuery('#promociones').val() != '')
				mostrarMensajeInformativo('Existen promociones de pago con la tarjeta de cr\u00e9dito ingresada.');
			else
				mostrarMensajeInformativo('No se cuenta por el momento con promociones de pago con la tarjeta de cr\u00e9dito ingresada.');
		});
	}
}

jQuery(document).ready(function(){
	jQuery('#indicador').hide()
});

function validarExisteContratante()
{
	if(idToPersonaContratante == "" || idToPersonaContratante == null){
		mostrarMensajeInformativo("Se necesita un contratante, por favor dirigase a la pesta\u00F1a Complem. Emisi\u00f3n y seleccione un contratante antes de continuar. ", "10");
		return false;
	}else
	{
		return true;
	}
}