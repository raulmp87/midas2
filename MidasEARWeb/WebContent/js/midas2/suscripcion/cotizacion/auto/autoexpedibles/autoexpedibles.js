/**
 * 
 */
function verAutoExpediblesCotizacion(){
	limpiarDivsGeneral();
	var path= '/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarAutoExpediblesCotizacion.action';
	sendRequestJQ(null, path,'contenido_detalle',null);
}

function verAutoExpediblesEmision(){
	limpiarDivsGeneral();
	var path= '/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarAutoExpediblesEmision.action';
	sendRequestJQ(null, path,'contenido_autoExpediblesEmision',null);
}

var autoExpediblesGrid;
var autoExpediblesDetalleGrid;

function iniciaListadoAutoExpedibles(claveTipo){

	document.getElementById("autoExpediblesGrid").innerHTML = '';
	autoExpediblesGrid = new dhtmlXGridObject("autoExpediblesGrid");
	autoExpediblesGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	autoExpediblesGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	autoExpediblesGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
    });
	autoExpediblesGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
    });		
	autoExpediblesGrid.load("/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/listarAutoExpedibles.action?" + jQuery(document.autoexpediblesForm).serialize());
}
function importarArchivo(tipoCarga){
	

	 if(!jQuery("#idAcurdoAfirmeMasiva").is(':checked')) {
		 
		 alert("Debe marcar el acuerdo");
		 return;
	 }
	
	var claveTipo = jQuery("#claveTipo").val();
	if(claveTipo == 0){
	if(jQuery("#negocios").val() == null || jQuery("#negocios").val() == "" ||
			jQuery("#productos").val() == null || jQuery("#productos").val() == "" ||
			jQuery("#polizas").val() == null || jQuery("#polizas").val() == ""){
		mostrarMensajeInformativo('Favor de seleccionar un Negocio, Producto y/o Tipo Poliza',"20");
		return;
	}
	}
	
	
	var path = "/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/validaAutoExpedibles.action?";
	var divcontenido = "";
	if(claveTipo == 0){
		divcontenido = "contenido_detalle";
	}
	if(claveTipo == 1){
		divcontenido = "contenido_autoExpediblesEmision";
	}
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaAutoExpedibles", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga AutoExpedibles");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			sendRequestJQ(null, path + jQuery(document.autoexpediblesForm).serialize() + "&idToControlArchivo=" + idToControlArchivo + "&tipoCarga=" + tipoCarga, divcontenido, null);
    			}else{
    				mostrarMensajeInformativo('Fallo carga de archivo, favor de volver a intentar en unos momentos.',"20");
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaAutoExpedibles").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           mostrarMensajeInformativo('Solo puede importar archivos Excel (.xls).',"20");
           return false; 
        } 
        else return true; 
     }; 
    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}

function descargarAutoExpedibles(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function descargarLogAutoExpedibles() {	
	if(jQuery("#logErrors").val() == 'true'){
		var idToControlArchivo = jQuery("#idToControlArchivo").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/descargarLogErrores.action?idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT");
	}
}

function descargarPlantillaAutoExpedibles(){
	var claveTipo = jQuery("#claveTipo").val();
	var idToNegTipoPoliza = jQuery("#polizas").val();
	if(jQuery("#negocios").val() == null || jQuery("#negocios").val() == "" ||
			jQuery("#productos").val() == null || jQuery("#productos").val() == "" ||
			jQuery("#polizas").val() == null || jQuery("#polizas").val() == ""){
		mostrarMensajeInformativo('Favor de seleccionar un Negocio, Producto y/o Tipo Poliza',"20");
		return;
	}
	var location ="/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/descargarPlantilla.action?claveTipo=" + claveTipo + "&idToNegTipoPoliza=" + idToNegTipoPoliza;
	window.open(location, "Cotizacion_COT");
}

function mostrarResumenAutoExpedibles(idToAutoExpediblesAutoCot){
	var claveTipo = jQuery("#claveTipo").val();
	var divContenido = "contenido_detalle";
	if(claveTipo == 1){
		divContenido = "contenido_autoExpediblesEmision";
	}
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarDetalleAutoExpedibles.action?idToAutoExpediblesAutoCot=" + idToAutoExpediblesAutoCot + "&" + jQuery(document.autoexpediblesForm).serialize(), divContenido, null);
}

function iniciaListadoDetalleAutoExpedibles(){
	var idToAutoExpediblesAutoCot = jQuery("#idToAutoExpediblesAutoCot").val();
	document.getElementById("autoExpediblesDetalleGrid").innerHTML = '';
	cargaMasivasDetalleGrid = new dhtmlXGridObject("autoExpediblesDetalleGrid");
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorGrid");
    });
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorGrid');
    });		
	cargaMasivasDetalleGrid.load("/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/listarDetalleAutoExpedibles.action?idToAutoExpediblesAutoCot="+ idToAutoExpediblesAutoCot);
}

function regresarAAutoExpedibles(){
	var claveTipo = jQuery("#claveTipo").val();
	var divContenido = "contenido_detalle";
	var path =  "/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarAutoExpediblesCotizacion.action?";
	if(claveTipo == 1){
		divContenido = "contenido_autoExpediblesEmision";
		path =  "/MidasWeb/suscripcion/cotizacion/auto/autoexpedibles/mostrarAutoExpediblesEmision.action?";
	}
	sendRequestJQ(null, path + jQuery(document.autoexpediblesdetalleForm).serialize(), divContenido, null);	
}

function mostrarErrorAutoExpedibles(estatus, mensajeError){
	if(estatus == 0){
		mostrarVentanaMensaje('10', mensajeError);
	}
	if(estatus == 1){
		mostrarMensajeExito();
	}
	if(estatus == 2){
		mostrarVentanaMensaje('30', 'Pendiente de Procesar');
	}
	if(estatus == 3){
		mostrarVentanaMensaje('30', mensajeError);
	}
	if(estatus == 4){
		mostrarVentanaMensaje('30', mensajeError);
	}
}

function limpiarDivsGeneral() {
	limpiarDiv('contenido_detalle');
	limpiarDiv('contenido_autoExpediblesEmision');
}

function limpiarDiv(nombreDiv) {
	var div = document.getElementById(nombreDiv);
	if (div != null && div != undefined) {
		div.innerHTML = '';
	}
}
