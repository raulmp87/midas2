
function cargarComboNegProductos(negocioId){
	parent.listadoService.getMapProductosPorNegocio(negocioId, function(data){
		parent.addOptions(document.getElementById('productos'), data);
	}); 
}

function cargarComboTipoPolizas(productoId){
	parent.listadoService.getMapTipoPolizaPorProducto(productoId, function(data){
		parent.addOptions(document.getElementById('tipoPolizas'), data);
	});
}

function cargarComboLineasNegocio(tipoPolizaId){
	parent.listadoService.getMapNegocioSeccionPorTipoPoliza(tipoPolizaId, function(data){
		parent.addOptions(document.getElementById('lineasNegocio'), data);
	});
}

function cargarComboNegPaquete(lineaNegocioId){
	parent.listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(lineaNegocioId, function(data){
		parent.addOptions(document.getElementById('paquetes'), data);
	});
}
function cargarComboNegCobertura(paqueteId){
	parent.listadoService.getMapNegocioCoberturaPaqueteSeccionPorPaquete(paqueteId, function(data){
		parent.addOptions(document.getElementById('coberturas'), data);
	});
}
/*
cargarComboNegProductos();
cargarComboTipoPolizas()
cargarComboLineasNegocio();
cargarComboNegPaquete();
cargarComboNegCobertura();
		productos = listadoService.getMapProductosPorNegocio(negocioId);
		tipoPolizas = listadoService.getMapTipoPolizaPorProducto(productoId);
		lineasNegocio = listadoService.getMapNegocioSeccionPorTipoPoliza(tipoPolizaId);
		paquetes = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(lineaNegocioId);
		coberturas = listadoService.getMapNegocioCoberturaPaqueteSeccionPorPaquete(paqueteId);
*/