var listadoGrid;
var headerValue = '';
var headerDescription = 'Este valor no importa';

jQuery(document).ready(function(){
	listadoGrid = new dhtmlXGridObject('listadoExcepciones');
	mostrarListadoExcepciones();
});	


function mostrarListadoExcepciones(){
	document.getElementById("pagingArea").innerHTML = '';
	document.getElementById("infoArea").innerHTML = '';
	listadoGrid = new dhtmlXGridObject('listadoExcepciones');
	listadoGrid
	.attachHeader("&nbsp;,&nbsp;,&nbsp;,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter," +
			"#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter," +
			"#text_filter,#text_filter,#text_filter");
	parent.mostrarIndicadorCarga('indicador');	
	//listadoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
	listadoGrid.load(listarExcepcionesPath);
}

function limpiaFiltros(){
	if(listadoGrid != undefined){
		jQuery(".filter").each(function(){
			jQuery(this).find("input").val("");
		});		
		listadoGrid.filterByAll();
	}
}

function agregarExcepcion(){
	var url = '/MidasWeb/suscripcion/excepcion/autos/mostrarAgregar.action';
	mostrarVentanaModal("mostrar", 'Agregar Excepci\u00F3n', 200, 120, 850, 550, url);
}

/*
function mostrarModal(id, text, x, y, width, height, url){
	var win = dhxWins.createWindow(id, x, y, width, height);
	win.setText(text);
	win.center();
	win.setModal(true);
	win.attachURL(url);
}*/

function eliminarExcepcion(id, num){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar la excepci\u00F3n '+ num +'?')){
		sendRequestJQ(null, "/MidasWeb/suscripcion/excepcion/autos/eliminarExcepcion.action?excepcionId="
				+ id, targetWorkArea,
				null);		
	}
}

function mostrarExcepcion(id){
	var url = '/MidasWeb/suscripcion/excepcion/autos/mostrarExcepcion.action?excepcionId='+id;
	mostrarVentanaModal("mostrar", 'Modificar Excepci\u00F3n', 200, 120, 850, 550, url,'parent.mostrarListadoExcepciones();');	
}

function recargarExcepcion(id,mensaje){
	var url = '/MidasWeb/suscripcion/excepcion/autos/mostrarExcepcion.action?excepcionId='+id+"&mensaje="+mensaje;
	redirectVentanaModal("mostrar", url, null);	
}

function imprimirListadoExcepciones(){
	if(confirm('\u00BFEst\u00e1 seguro que desea imprimir el listado de excepciones?')){
		var location ="/MidasWeb/impresiones/generales/imprimirListadoExcepciones.action?cveNegocio=A";
		window.open(location, "ListadoExcepciones");
	}
}