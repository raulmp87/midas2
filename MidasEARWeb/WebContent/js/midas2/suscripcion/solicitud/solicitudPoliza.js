var selectValue = '';
var selectDescripcion = "SELECCIONE...";
var solicitudesPolizaGrid;
var ventanaRechazo;
var ventanaAsignacion;
var ventanaReasignacion;
var dhxWins= new dhtmlXWindows();
dhxWins.enableAutoViewport(true);
dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
var paginadoParamsPath;
var disableOnchage = false;

function opcionCaptura(opcion,id){
	var script = '';
	var path = opcionCapturaPath +"?seleccion="+opcion+"&claveNegocio="+dwr.util.getValue("claveNegocio");
	if(id != null && id != ''){
		path += "&id="+id;
		//script = "obtenerDatosAgente(jQuery('#idTcAgente').val())"
	}
	//sendRequestJQ(null, path ,'contenido', script);	
	sendRequestJQ(null, path ,'contenido', null);
}

function limpiarFormSolicitud(){
	jQuery('#solicitudPolizaForm').each (function(){
		  this.reset();
	});
	
	var target = validateTarget(jQuery("#idToProducto")[0]);
	dwr.util.removeAllOptions(target);
	dwr.util.addOptions(target, [{id: headerValue, value:"Seleccione ..." }],"id", "value");
}

function pageGridPaginado(page, nuevoFiltro){
	jQuery("#tipoBusqueda").val(2);
	var posPath = '&posActual='+page+'&funcionPaginar='+'pageGridPaginado'+'&divGridPaginar='+'solicitudesPolizaGrid';
	if(nuevoFiltro){
		paginadoParamsPath = jQuery(document.solicitudPolizaForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}
	
	var nextFunc = 'obtenerSolicitudes();';
	if(page == 1)
	{
		nextFunc = nextFunc + 'displayFilterSolicitud();';		
	}	
	sendRequestJQTarifa(null, solicitudesPaginadasPath + "?"+ paginadoParamsPath + posPath, 'gridSolicitudesPaginado', nextFunc);
}

function obtenerSolicitudes(){
	document.getElementById("solicitudesPolizaGrid").innerHTML = '';
	solicitudesPolizaGrid = new dhtmlXGridObject('solicitudesPolizaGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	solicitudesPolizaGrid.load(buscarSolicitudPath + "?"+ paginadoParamsPath + '&' +posPath,function(){
		sizeGridSolicitud();
	});
}


function pageGridPaginadoEmision(page, nuevoFiltro){
	jQuery("#tipoBusqueda").val(1);
	var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEmision'+'&divGridPaginar='+'solicitudesPolizaGrid';
	if(nuevoFiltro){
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	if(!jQuery('#contenedorFiltros').is(':visible')){
		sendRequestJQTarifa(null, solicitudesPaginadasRapidaPath + "?"+ posPath, 'gridSolicitudesPaginado', 'jQuery("#contenedorFiltros").toggle();obtenerSolicitudesEmision();');
		return true;
	}
	sendRequestJQTarifa(null, solicitudesPaginadasRapidaPath + "?"+ posPath, 'gridSolicitudesPaginado', 'obtenerSolicitudesEmision();');
}

function obtenerSolicitudesEmision(){
	document.getElementById("solicitudesPolizaGrid").innerHTML = '';	
	solicitudesPolizaGrid = new dhtmlXGridObject('solicitudesPolizaGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	solicitudesPolizaGrid.load(busquedaRapidaPath + "?" + posPath);
		
}

function refrescarGridSolicitudPoliza(sid, action, tid, node){
	obtenerSolicitudes();
	return true;
}

function initOficina(){
	removeAllOptionsAndSetHeader(document.getElementById('codigoEjecutivo'), parent.headerValue,"Cargando...");
	listadoService.getMapEjecutivosPorGerencia(null, function(data){
		var combo = document.getElementById('codigoEjecutivo');
		addOptions(combo,data);
	});
}

function obtieneProductos(id){
	listadoService.getMapProductos(id,1,function(data){
		var combo = document.getElementById('idToProducto');
		addOptions(combo,data);
	});	
}


function obtenerDatosAgente(id, seleccion){
	if(id != null && id != ''){
		var url="/MidasWeb/fuerzaventa/agente/findByClaveAgenteSolicitudes.action";
		var params={"agente.idAgente":id};
		if(seleccion == 2){
			params={"agente.idAgente":id,"agente.tipoSituacion.idRegistro":1302};
		}
		jQuery.asyncPostJSON(url,params,function(data){
			if(data.agente != null && data.agente.id != null){
				disableOnchage = true;
				if(data.agente.promotoria != null && data.agente.promotoria.ejecutivo != null){
					jQuery('#codigoEjecutivo').val(data.agente.promotoria.ejecutivo.id);					
				}	
				if(data.agente.persona != null){
					removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], selectValue, selectDescripcion);
					dwr.util.addOptions(document.getElementById('codigoAgente'),[{id: data.agente.id, value:data.agente.persona.nombreCompleto}],"id", "value");
				}else{
					removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], selectValue, selectDescripcion);
				}
				jQuery('#codigoAgente').val(data.agente.id);
				jQuery('#idTcAgenteHdn').val(data.agente.id);
				obtenerNegocio(data.agente.id);
			}else{
				jQuery('#codigoAgente').val("");
				jQuery('#idTcAgenteHdn').val("");
				removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], 
						selectValue, 
						selectDescripcion);
				if(seleccion == 2){
					mostrarVentanaMensaje('10', "Agente no Autorizado.", null);
				}else{
					mostrarVentanaMensaje('10', "Agente no valido.", null);
				}
			}			
			
		});
	}
}

function obtieneAgentes(id, seleccion){
	//alert("A");
	if(disableOnchage){
		disableOnchage = false;
		return;
	}
	var soloAutorizados = 0;
	if(seleccion == 2){
		soloAutorizados = 1;
	}
	if(id!=null&&id!=''){
		listadoService.getMapAgentesPorGerenciaOficinaPromotoria(null, dwr.util.getValue("codigoEjecutivo"), null, soloAutorizados, function(data){
			var combo = jQuery('#codigoAgente')[0];
			addOptions(combo,data);
		});
	}else{
		removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], 
				selectValue, 
				selectDescripcion);
	}
}

function enviar(tipoAccion) {
	var valido = true;
	var msg = "<ul>";
	if(jQuery("#negocios").val() == ''){
		msg+="<li>Seleccione un Negocio.</li>"
		valido = false;
	}
	
	if(jQuery("#idToProducto").val() == ''){
		msg+="<li>Seleccione un producto.</li>"
		valido = false;
	}
	if(jQuery.trim(jQuery("#codigoAgente").val()) == '' && (jQuery("#idTcAgente").val() == '' || jQuery("#idTcAgenteHdn").val() == '')){
		msg+="<li>Ingrese Clave del Agente o seleccione un agente a trav&eacute;s de los combos de selecci&oacute;n</li>"
		valido = false;
	}
	if(jQuery('#razonSocialText').is(':visible')){
		if(jQuery.trim(jQuery("#razonSocialText").val()) == '' && valido){
			msg+="<li>Por favor ingrese La Razon Social.</li>";
			valido = false;
		}
	}
	
	if(!jQuery('#nombrePersona').attr('disabled')){
		if(jQuery.trim(jQuery("#nombrePersona").val()) == '' || jQuery.trim(jQuery("#apellidoPaterno").val()) == '' || jQuery.trim(jQuery("#apellidoMaterno").val()) == ''){
		msg+="<li>Ingrese Nombre completo del Cliente.</li>"
		valido = false;
		}
	}
	
	if(jQuery.trim(jQuery("#tiposSolicitud").val()) == ''){
		msg+="<li>Ingrese Tipo de Movimiento.</li>"
		valido = false;
	}
	if(!valido){
		msg+="</ul>";
		mostrarVentanaMensaje('10', msg, null);
	}
		
	if(valido){
		sendRequestJQAsync(null, "/MidasWeb/suscripcion/solicitud/agregarSolicitud.action?"
			+ jQuery(document.solicitudPolizaForm).serialize(),'contenido',null);
	}
}

function iniciaSolicitudPoliza(){	
	//document.getElementById("solicitudesPolizaGrid").innerHTML = '';
	//solicitudesPolizaGrid = new dhtmlXGridObject('solicitudesPolizaGrid');
	//refrescarGridSolicitudPoliza(null, null, null, null);
	//obtenerSolicitudesEmision();
	pageGridPaginadoEmision(1, true);
}



function mostrarVentanaAsignarSolicitud(idSolicitud,onCloseFunction){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaAsignacion = parent.dhxWins.createWindow("asignar", 200, 320, 550, 240);
	parent.ventanaAsignacion.setText("Asignar Solicitud");
	parent.ventanaAsignacion.center();
	parent.ventanaAsignacion.setModal(true);
	parent.ventanaAsignacion.attachEvent("onClose", onCloseFunction);
	parent.ventanaAsignacion.attachURL("/MidasWeb/suscripcion/solicitud/asignar/mostrarVentana.action?id="+idSolicitud);
	
	parent.ventanaAsignacion.button("minmax1").hide();	
}

function closeAsignarSolicitud(){
	parent.dhxWins.window('asignar').setModal(false);
	parent.dhxWins.window('asignar').hide();
	parent.ventanaAsignacion=null;
	iniciaSolicitudPoliza();
}


function mostrarVentanaReasignar(idSolicitud,onCloseFunction){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaReasignacion = parent.dhxWins.createWindow("reasignar", 200, 320, 550, 240);
	parent.ventanaReasignacion.setText("Reasignar Solicitud");
	parent.ventanaReasignacion.center();
	parent.ventanaReasignacion.setModal(true);
	parent.ventanaReasignacion.attachEvent("onClose", onCloseFunction);
	parent.ventanaReasignacion.attachURL("/MidasWeb/suscripcion/solicitud/reasignar/mostrarVentana.action?id="+idSolicitud);
	
	parent.ventanaReasignacion.button("minmax1").hide();	
}

function cerrarVentanaReasignar(){
	parent.dhxWins.window('reasignar').setModal(false);
	parent.dhxWins.window('reasignar').hide();
	parent.ventanaReasignacion=null;
	iniciaSolicitudPoliza();
}



function mostrarVentanaRechazar(idSolicitud,onCloseFunction){
 var url =  "/MidasWeb/suscripcion/solicitud/rechazar/mostrarVentana.action?id="+idSolicitud
 parent.mostrarVentanaModal("rechazar", "Rechazar Solicitud", 200, 320, 550, 200, url);
}

function cerrarVentanaSolicitud(){
	parent.cerrarVentanaModal("rechazar");
}
function displayFilterSolicitud(){
	jQuery('#contenedorFiltros').toggle('fast', function() {
		if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
			jQuery('#mostrarFiltros').html('Mostrar Filtros')
			sizeGridSolicitud();
		} else {
			jQuery('#mostrarFiltros').html('Ocultar Filtros');
			reSizeGridSolicitud();
		}
	})
}
function sizeGridSolicitud(){
	jQuery(document).ready(function(){
		jQuery('#solicitudesPolizaGrid').css('height','300px');
		jQuery('div[class=objbox]').css('height','275px');
	})
}

function reSizeGridSolicitud(){
	jQuery(document).ready(function(){
		jQuery('#solicitudesPolizaGrid').css('height','130px');
		jQuery('div[class=objbox]').css('height','105px');
	})
}



function cerrarVentanaSolicitud(){
	parent.sendRequestJQAsync(null, "/MidasWeb/suscripcion/solicitud/mostrar.action?claveNegocio=A",'contenido',null);	
	parent.cerrarVentanaModal("rechazar");
	
}
function redirectMostrarCotizacion(idToCotizacion){
	if(idToCotizacion != null && idToCotizacion != ""){
		var url = "/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id="+idToCotizacion;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function obtenerProducto(idToNegocio){
	idToNegocio = idToNegocio;
	listadoService.getMapProductos(idToNegocio,function(data){
		var combo = document.getElementById('idToProducto');
		addOptions(combo,data);
	});
}

function mostrarCancelarSolicitud(idToSolicitud){
	mostrarMensajeConfirm("\u00BFSeguro que desea cancelar la solicitud " + idToSolicitud + "\u003F", "20", "cancelarSolicitud("+idToSolicitud+")");
}

/**
 * Cambia el estatus de una cotizacion
 * @autor martin
 */
function cancelarSolicitud(idToSolicitud) {
	var url = "/MidasWeb/suscripcion/solicitud/cancelarSolicitud.action?id="+idToSolicitud;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarSolicitudImpresion(idToSolicitud) {
	var path = "/MidasWeb/suscripcion/solicitud/generarImpresion.action?idToSolicitud="+idToSolicitud;
	window.open(path, "Solicitud_Imp");
}


function adjuntarDocumentosPruebas(){
	jQuery("#idToControlArchivo").val("131367");
}


function descargarDocumento(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function adjuntarDocumentosbySolicitud(idSolicitud) {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("AdjuntarDocumentoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/spn/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    //vault.strings.remove = "Eliminar"; // Remove 
    //vault.strings.done = "Hecho"; // Done 
    //vault.strings.error = "Error"; // Error 
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    		    	if(idSolicitud != null && idSolicitud != undefined && idSolicitud != ''){
    					var pathId = "?id="+idSolicitud + "&idToControlArchivo="+idToControlArchivo;
    					var url = "/MidasWeb/suscripcion/solicitud/adjuntarControlArchivo.action" + pathId;
    					parent.redirectVentanaModal("adjuntarDocumentos", url, null);
    				}else{
    					var ids = parent.prepareSolicitudDocumentos(idToControlArchivo);
    					var url = "/MidasWeb/suscripcion/solicitud/adjuntarDocumentos.action?"+ids;
    					parent.redirectVentanaModal("adjuntarDocumentos", url, null);
    				}
    			}
    		} // End of onSuccess    		
    	});
        parent.dhxWins.window("AdjuntarDocumentoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "18");
    vault.setFormField("idSolicitud",idSolicitud);
    
}

function prepareSolicitudDocumentos(idToControlArchivo){
	var total = jQuery("#controlArchivoTemp").find("input[type$='hidden']").length;
	var newHidden = "<input name='idToControlArchivoList["+total+"]'  type='hidden' value='"+ idToControlArchivo+"'/>";
	jQuery("#controlArchivoTemp").append(newHidden);
	var arrayId = obtieneIdControlArchivoTemporal();
	return arrayId;
}

function obtieneIdControlArchivoTemporal(){
	var arrayId = "";
	var isFirst = true;
	var indice = 0;
	jQuery("#controlArchivoTemp").find("input[type$='hidden']").each(function(){
		if(isFirst){
			arrayId+="idToControlArchivoList["+indice+"]="+jQuery(this).val()+"";
			isFirst = false;
		}else{
			arrayId+="&idToControlArchivoList["+indice+"]="+jQuery(this).val();
		}
		indice=indice+1;
	});
	return arrayId;
}

function onChangeCodigoAgente(id){
	obtieneClaveAgente(id,'idTcAgente'); 
	jQuery('#idTcAgenteHdn').val(id);
	obtenerNegocio(id);
}

function obtenerNegocio(idAgente){
	if(idAgente != ''){
		listadoService.listarNegociosPorAgente(idAgente,'A',1,function(data){
			var combo = document.getElementById('negocios');
			addOptions(combo,data);
		});
	}else{
		removeAllOptionsAndSetHeader(jQuery('negocios'), 
				selectValue, 
				selectDescripcion);
	}
}

function $_iniciarCotizacionFormal(){
		var url = "/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente="
			+ dwr.util.getValue('idTcAgenteHdn')
			+ "&cotizacion.solicitudDTO.negocio.idToNegocio="
			+ dwr.util.getValue('negocios')
			+ "&cotizacion.solicitudDTO.productoDTO.idToProducto="
			+ dwr.util.getValue('idToProducto')
			+ "&cotizacion.solicitudDTO.idToSolicitud=" 
			+ dwr.util.getValue('solicitud.idToSolicitud')
			+ "&cotizacion.idToCotizacion=" + dwr.util.getValue('solicitud.idToSolicitud');
	parent.mostrarVentanaModal("crearCotizacion", "Crear Cotizaci\u00F3n", 200, 320, 610, 460, url);		
}
function $_iniciarCotizacionFormal_bandeja(idTcAgente,negocio,idToProducto,idToSolicitud){
	
	listadoService.getNegocioProducto(negocio,idToProducto,function(idToNegocioProducto){
		if(idToNegocioProducto == 0){
			mostrarMensajeInformativo("Ocurrio un error inesperado", "10", null, null);
			return;
		}
		var url = "/MidasWeb/suscripcion/cotizacion/auto/iniciarCotizacionFormal.action?cotizacion.solicitudDTO.codigoAgente="
			+ idTcAgente
			+ "&cotizacion.solicitudDTO.negocio.idToNegocio="
			+ negocio
			+ "&cotizacion.solicitudDTO.productoDTO.idToProducto="
			+ idToNegocioProducto
			+ "&cotizacion.solicitudDTO.idToSolicitud=" 
			+ idToSolicitud
			+ "&cotizacion.idToCotizacion=" + idToSolicitud;
		parent.mostrarVentanaModal("crearCotizacion", "Crear Cotizaci\u00F3n", 200, 320, 610, 460, url);	
	});
	
}
function descargaExcelSolicitudPoliza(){
	//jQuery(document.solicitudPolizaForm).serialize();
	//jQuery(document.paginadoGridForm).serialize();
	var tipoBusqueda = jQuery("#tipoBusqueda").val();
	if(tipoBusqueda == 2){
		window.open('/MidasWeb/suscripcion/solicitud/descargaExcel.action?' + paginadoParamsPath, 'download');
	}else{
		window.open('/MidasWeb/suscripcion/solicitud/descargaExcel.action?tipoBusqueda=1', 'download');
	}
}

function mostrarVentanaAdjuntarDocumentos(idSolicitud, tipoConsulta){
	var pathId = "";
	if(idSolicitud != null && idSolicitud != undefined && idSolicitud != ''){
		pathId = "?id="+idSolicitud;
		if(tipoConsulta != null && tipoConsulta != undefined && tipoConsulta != ""){
			pathId += "&tipoConsulta="+tipoConsulta;
		}
	}else{
		if(tipoConsulta != null && tipoConsulta != undefined && tipoConsulta != ""){
			pathId += "?tipoConsulta="+tipoConsulta;
		}		
	}
	var url = "/MidasWeb/suscripcion/solicitud/adjuntarDocumentos.action" + pathId;
	parent.mostrarVentanaModal("adjuntarDocumentos", "Adjuntar Documentos", 200, 320, 610, 260, url, "cierraVaultAdjuntarDocumentos()");		
}

function cierraVaultAdjuntarDocumentos(){
	if(parent.dhxWins.window("AdjuntarDocumentoWindow") != null){
		parent.dhxWins.window("AdjuntarDocumentoWindow").close();
	}
}

function seleccionaCartaCobertura(elem, idToCA){
	jQuery(parent.document).find('#idToCACartaCobertura').val(idToCA);
	iniciaSolicitudDocumentos();
}

function iniciaSolicitudDocumentos(){
	document.getElementById("solicitudDocumentosGrid").innerHTML = '';
	var solicitudDocumentosGrid = new dhtmlXGridObject('solicitudDocumentosGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudDocumentosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var id = jQuery("#idSolicitud").val();
	var idToCA = jQuery(parent.document).find('#idToCACartaCobertura').val();
	var pathId = "?idToCACartaCobertura="+(idToCA?idToCA:0);
	if(id != null && id != undefined && id != ''){
		pathId = pathId + "&id="+id;
	}else{
		pathId = pathId + "&" + parent.obtieneIdControlArchivoTemporal();
	}
	
	
	var url = "/MidasWeb/suscripcion/solicitud/buscarSolicitudDocumentos.action"+pathId;
	solicitudDocumentosGrid.load(url);
}