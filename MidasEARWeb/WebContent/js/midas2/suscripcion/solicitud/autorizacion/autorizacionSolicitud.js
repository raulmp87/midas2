var listadoSolicitudAutGrid;
var ventanaDetalle;
var excepcionDetalle;
var paginadoParamsSolAutPath;

function mostrarListadoSolicitudesPaginado(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'mostrarListadoSolicitudesPaginado'+'&divGridPaginar='+'listadoSolicitudes';
	if(nuevoFiltro){
		paginadoParamsSolAutPath = jQuery(document.solicitudForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'mostrarListadoSolicitudes();';
	
	mostrarIndicadorCarga('indicador');	
	sendRequestJQTarifa(null, obtenerSolicitudesAutPaginadoPath + "?" + paginadoParamsSolAutPath + posPath, 'gridListadoSolicitudesPaginado', nextFunc);
} 

function mostrarListadoSolicitudes(){

	//document.getElementById("pagingArea").innerHTML = '';
	//document.getElementById("infoArea").innerHTML = '';
	document.getElementById("listadoSolicitudes").innerHTML = '';
	listadoSolicitudAutGrid = new dhtmlXGridObject('listadoSolicitudes');
	listadoSolicitudAutGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	listadoSolicitudAutGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	listadoSolicitudAutGrid.attachEvent("onXLE", function(grid){ocultarIndicadorCarga('indicador');});
	//listadoSolicitudAutGrid.load(obtenerSolicitudesAutPath + "?" + jQuery(document.solicitudForm).serialize());
	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	listadoSolicitudAutGrid.load(obtenerSolicitudesAutPath + "?" + paginadoParamsSolAutPath + "&"+ posPath);
	
}

function mostrarSolicitudAutDetalle(id){
	var url = obtenerSolicitudesDetalleAutPath + "?solicitudExcepcionCot.id="+id;
	mostrarVentanaModal("solicitudDetalle", "Detalle de la Solicitud", 50, 50, 940, 600, url, "mostrarListadoSolicitudes()");	
}

function cancelarSolicitud(id){
	if(confirm('\u00BFEst\u00e1 seguro que desea cancelar la solicitud '+ id +'?')){
		sendRequestJQ(null, "/MidasWeb/suscripcion/solicitud/autorizar/cancelarSolicitud.action?idSolicitud="
				+ id, targetWorkArea);		
	}
}

function seleccionarUsuario(bandera){	
//	alert(selecionarUsuarioPath+'?bandera='+bandera);
	mostrarVentanaModal("BuscarUsuario","Selecci\u00F3n de usuario",200,400, 510, 230, selecionarUsuarioPath+"?bandera="+bandera);	
}
/**
 * Llena los datos del usuario solicitante
 * @param id
 * @param nombre
 * @autor martin
 */
function cargaUsuarioSolicitante(id,nombre){
	jQuery('#usuarioSolicitante').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#nombreSolicitante').val(nombre);
	cerrarVentanaModal("BuscarUsuario");
}
/**
 * Llena los datos del usuario autorizador
 * @param id
 * @param nombre
 * @autor martin
 */
function cargaUsuarioAutorizador(id,nombre){
	jQuery('#usuarioAutorizador').val(id);
	if (nombre.length > 26){
		nombre = nombre.substring(0,26);
	}
	jQuery('#nombreAutorizador').val(nombre);
	cerrarVentanaModal("BuscarUsuario");
}

function limpiarFiltrosAutorizacion(){
	$_cleanForm('solicitudForm');
	
	if (!usuarioControlDeshabilitado) {
		jQuery('#usuarioSolicitante').val('');
		jQuery('#nombreSolicitante').val('');
	}
	jQuery('#usuarioAutorizador').val('');
}

function terminarSolicitudesMasiva(tipoAutorizacion){
	var ids = cargaIdAutorizacionSeleccionadas();
	var pregunta = "";
	if(tipoAutorizacion == 1){
		pregunta = "\u00BFEst\u00e1 seguro que desea Autorizar las Solicitudes\u003F ";
	}else if(tipoAutorizacion == 2){
		pregunta = "\u00BFEst\u00e1 seguro que desea Rechazar las Solicitudes\u003F ";
	}else if(tipoAutorizacion == 3){
		pregunta = "\u00BFEst\u00e1 seguro que desea Cancelar las Solicitudes\u003F ";
	}
	if(ids == null || ids == ""){
		alert("Favor de seleccionar al menos una Solicitud de Autorizacion");
	}else{		
		if(confirm(pregunta)){
			sendRequestJQTarifa(null, terminarSolicitudesPath + "?tipoAutorizacion=" + tipoAutorizacion + "&" + ids, 'contenido', null);
		}
	}
}

function cargaIdAutorizacionSeleccionadas(){
	var idSolicitudes = "";
	var num = 0;
	var idCheck = listadoSolicitudAutGrid.getCheckedRows(0);
	if(idCheck != ""){
		var ids = idCheck.split(",");
		var num = 0;
		for(i = 0; i < ids.length; i++){
	   	 	if(idSolicitudes != ""){
	   	 		idSolicitudes+="&";
	   	 	}
			idSolicitudes += "solicitudes["+ num+ "].id=" + ids[i];
	   	 	num = num + 1;		
		}
	}
	return idSolicitudes;
}