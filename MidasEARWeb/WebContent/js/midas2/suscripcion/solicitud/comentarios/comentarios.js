var comentariosDisponiblesGrid;
var ventanaProductos = null;
var comentariosGrid;

function abrirComentarios(idSolicitud, tipoComentario){	
	if(tipoComentario == undefined) tipoComentario = 0;
	if(tipoComentario == 0 || tipoComentario == 3){
		var path = "/MidasWeb/suscripcion/solicitud/comentarios/mostrar.action?solicitudDTO.idToSolicitud="
				+ idSolicitud
				+ "&claveNegocio="
				+ dwr.util.getValue("claveNegocio")
				+ "&tipoComentario="
				+ tipoComentario
				+ "&seleccion="
				+ dwr.util.getValue('seleccion');	
		sendRequestJQ(null,path,targetWorkArea,null);	
	}
	if(tipoComentario == 1){
		var path= "/MidasWeb/suscripcion/solicitud/comentarios/mostrar.action?idToCotizacion="
			+ idSolicitud + "&claveNegocio=" + dwr.util.getValue("claveNegocio") + "&tipoComentario="+tipoComentario;	
		sendRequestJQ(null,path,targetWorkArea,null);
	}
	if(tipoComentario == 2){
		mostrarMensajeConfirm("\u00BFEst\u00e1 seguro que deseas Cancelar la cotizaci\u00F3n n\u00FAmero " +idSolicitud +"?", "20", "cancelarCotizacionAction("+idSolicitud+","+tipoComentario+")", null);
	}
	
}

function cancelarCotizacionAction(idSolicitud,tipoComentario){
	var path= "/MidasWeb/suscripcion/solicitud/comentarios/mostrar.action?idToCotizacion="
		+ idSolicitud + "&claveNegocio=" + dwr.util.getValue("claveNegocio") + "&tipoComentario="+tipoComentario;	
	sendRequestJQ(null,path,targetWorkArea,null);
}

function salirComentarios(){	
	if(jQuery("#tipoComentario").val() == 0){
		var path= "/MidasWeb/suscripcion/solicitud/mostrar.action?claveNegocio=" + 
		  dwr.util.getValue("claveNegocio")+ "&esRetorno=1";
		sendRequestJQ(null,path,'contenido',"pageGridPaginadoEmision(1, true)");	
	}else if (jQuery("#tipoComentario").val() == 1){
		var path= "/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=" + 
		  dwr.util.getValue("claveNegocio");
		sendRequestJQ(null,path,'contenido',null);		
	}else if (jQuery("#tipoComentario").val() == 2){
		var path= "/MidasWeb/suscripcion/cotizacion/auto/listar.action?claveNegocio=" + 
		  dwr.util.getValue("claveNegocio");
		sendRequestJQ(null,path,'contenido',null);		
	}else if (jQuery("#tipoComentario").val() == 3){
		var path= "/MidasWeb/suscripcion/solicitud/mostrar.action?claveNegocio=" + 
		  dwr.util.getValue("claveNegocio") + "&esRetorno=1";
		sendRequestJQ(null,path,'contenido',"compareDates();");		
	}
}


function guardarComentario() {
	var path= "/MidasWeb/suscripcion/solicitud/comentarios/guardarComentarios.action?"
		+ jQuery(document.comentariosForm).serialize();	
	sendRequestJQ(null,path,targetWorkArea,null);
}

function eliminarComentarios() {
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el comentario?')){
	var path= "/MidasWeb/suscripcion/solicitud/comentarios/eliminarComentarios.action";	
	var idComentario = getIdFromGrid(comentariosDisponiblesGrid, 0);	
	var idSolicitud = getIdFromGrid(comentariosDisponiblesGrid, 1);
		sendRequestJQ(null, path + "?comentario.solicitudDTO.idToSolicitud=" + idSolicitud + "&comentario.id=" + idComentario, targetWorkArea, null);	
	}
	}

function refrescarGridComentarios(sid,action,tid,node){
	obtenerComentarios();
	return true;
}

function initGridsComentarios(){
		refrescarGridComentarios(null,null,null,null);	
}

function obtenerComentarios(){
	document.getElementById('comentariosGrid').innerHTML = '';
	comentariosDisponiblesGrid = new dhtmlXGridObject('comentariosGrid');
	comentariosDisponiblesGrid.load(obtenerComentariosPath);
}

function descargarDocumentoComentarios(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function adjuntarDocumentos(idComentario,idSolicitud) {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("AdjuntarDocumentoWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Adjuntar documento");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/spn/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.strings.remove = "Eliminar"; // Remove 
    vault.strings.done = "Hecho"; // Done 
    vault.strings.error = "Error"; // Error 
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;        			
        			var path= "/MidasWeb/suscripcion/solicitud/comentarios/guardarComentarios.action?comentario.id="+idComentario 
        			+ "&comentario.documentoDigitalSolicitudDTO.controlArchivo.idToControlArchivo=" + idToControlArchivo
        			+ "&comentario.documentoDigitalSolicitudDTO.controlArchivo.nombreArchivoOriginal=" + nombreArchivo
        			+ "&comentario.solicitudDTO.idToSolicitud=" + idSolicitud;	
        			sendRequestJQ(null,path,targetWorkArea,null);
        			mostrarVentanaMensaje('30', 'Documento guardado exitosamente', null);
    			}
    			else{
    				mostrarVentanaMensaje('10', 'Error al adjuntar el archivo intentelo nuevamente', null);
    			}
    		} // End of onSuccess    		
    	});
        parent.dhxWins.window("AdjuntarDocumentoWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "18");
    vault.setFormField("idSolicitud",idSolicitud);
}


function cancelarComentario() {
	var respuesta = confirm("Los cambios efectuados se perderán. ¿Está Seguro de Cancelar?");
	if (respuesta) salirComentarios();
}

