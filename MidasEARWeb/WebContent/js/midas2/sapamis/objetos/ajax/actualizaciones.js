/************************************************************
 *	Nombre del Archivo: actualizaciones.js
 *
 *	Proposito: 	Realizar actualizaciones de Datos a traves de
 * 				Peticiones AJAX.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
 function updateEstatusSistema(catSistemaActualizar, callBack){
	var catSistema = {};
	    catSistema.catSistemas = catSistemaActualizar;
	$.ajax({
        type: peticionesAjax.actualizaciones.type,
        dataType: peticionesAjax.actualizaciones.dataType,
        contentType: peticionesAjax.actualizaciones.contentType,
	    url: peticionesAjax.actualizaciones.estatusSistemas.path,
        cache: false,
        data: JSON.stringify(catSistema),
	    async: true,
	    success: function() {
	    	callBack();
	    },
	    error:function () {
	    	alert(peticionesAjax.actualizaciones.estatusSistemas.mensajeError + catSistema.catSistemas.descCatSistemas);
	    	callBack();
        }    
	});
}