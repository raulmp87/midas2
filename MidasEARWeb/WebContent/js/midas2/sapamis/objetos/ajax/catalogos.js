/************************************************************
 *	Nombre del Archivo: catalogos.js
 *
 *	Proposito: 	Obtener los datos de los catalogos requeridos, 
 *				para el uso de la pantalla a traves de 
 *				peticiones AJAX.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
 function findByStatusCatEstatusBitacora(callBack){
	if(catalogos.catEstatusBitacoraArr == null){
		jQuery.ajax({
	        type: peticionesAjax.catalogos.type,
	        dataType: peticionesAjax.catalogos.dataType,
		    url: peticionesAjax.catalogos.EstatusBitacora.path,
	        cache: false,
	        data: null,
		    async: true,
		    success: function(catEstatusBitacora) {
		    	catalogos.catEstatusBitacoraArr = catEstatusBitacora.catEstatusBitacoraList;
				callBack();
		    },
		    error:function () {
	            alert(peticionesAjax.catalogos.EstatusBitacora.mensajeError);
				callBack();
	        }
		});
	}else{
		callBack();
	}
}

function findByStatusRelSistemaOperacion(callBack){
	if(catalogos.relSistemaOperacionArr == null){
		jQuery.ajax({
	        type: peticionesAjax.catalogos.type,
	        dataType: peticionesAjax.catalogos.dataType,
		    url: peticionesAjax.catalogos.RelSistemaOperacion.path + '?modulo=' + infoPortal.modulo,
	        cache: false,
	        data: '',
		    async: true,
		    success: function(relSistemaOperacion) {
		    	catalogos.relSistemaOperacionArr = relSistemaOperacion.relSistemaOperacionList;
		    	getSistemas(function(){
		    		callBack();
		    	});
				
		    },
		    error:function () {
	            alert(peticionesAjax.catalogos.RelSistemaOperacion.mensajeError);
				callBack();
	        }
		});
	}else{
		callBack();
	}
}
function getSistemas(callBack){
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		switch(catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas){
			case 'EMISION':
			visionGeneral.elementHTML.subTitle.list[1].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'SINIESTRO':
			visionGeneral.elementHTML.subTitle.list[2].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'PREVENCION':
			visionGeneral.elementHTML.subTitle.list[3].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'PPT':
			visionGeneral.elementHTML.subTitle.list[4].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'RECHAZOS':
			visionGeneral.elementHTML.subTitle.list[5].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'SALVAMENTO':
			visionGeneral.elementHTML.subTitle.list[6].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'VALUACION':
			visionGeneral.elementHTML.subTitle.list[7].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'ROBO':
			visionGeneral.elementHTML.subTitle.list[8].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
			case 'RECUPERACION':
			visionGeneral.elementHTML.subTitle.list[9].idSistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			break;
		}
	}
	callBack();

}