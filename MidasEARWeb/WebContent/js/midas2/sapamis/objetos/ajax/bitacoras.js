/************************************************************
 *	Nombre del Archivo: bitacoras.js
 *
 *	Proposito: Obtener los datos de Bitacoras Generales y/o
 *			   para los diferentes Modulos procesados, a 
 *             a traves de peticiones AJAX.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function obtenerCifrasByFilters(callBack){
	if(infoPortal.modulo == infoPortal.modulos[2].clave && banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id){
		callBack();
	}else{
		parametros = {};
		parametros.parametrosConsulta = parametrosConsulta;
		jQuery.ajax({
	        type: peticionesAjax.sistemas.type,
	        dataType: peticionesAjax.sistemas.dataType,
	        contentType: peticionesAjax.sistemas.contentType,
		    url: peticionesAjax.sistemas.cifras.path,
	        cache: false,
	        data: JSON.stringify(parametros),
		    async: true,
		    success: function(cifrasResponse) {
		    	resultadosJSON.cifrasArr = cifrasResponse.sapAmisBitacorasList;
		    	calculosIniciales(function(){
					callBack();
		    	});
		    },
		    error:function () {
	            alert(peticionesAjax.sistemas.cifras.mensajeError);
	            callBack();
	        }
		});
	}
}

function obtenerEmisionPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: infoPortal.modulo == infoPortal.modulos[1].clave ? peticionesAjax.sistemas.emision.path : peticionesAjax.sistemas.repuveEmision.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(emision) {
            resultadosJSON.emisionArr = infoPortal.modulo == infoPortal.modulos[1].clave ? emision.sapAmisEmisionList : emision.repuveSisEmisionList;
			resultadosJSON.siniestrosArr = {};
			resultadosJSON.prevencionesArr = {};
			resultadosJSON.perdidasTotalesArr = {};
			resultadosJSON.rechazosArr = {};
			resultadosJSON.salvamentosArr = {};
			resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(infoPortal.modulo == infoPortal.modulos[1].clave ? peticionesAjax.sistemas.emision.mensajeError : peticionesAjax.sistemas.repuveEmision.mensajeError);
            callBack();
        }
	});
}

function obtenerSiniestrosPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.siniestros.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(siniestros) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = siniestros.sapAmisSiniestrosList;
			resultadosJSON.prevencionesArr = {};
			resultadosJSON.perdidasTotalesArr = {};
			resultadosJSON.rechazosArr = {};
			resultadosJSON.salvamentosArr = {};
			resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.siniestros.mensajeError);
            callBack();
        }
	});
}

function obtenerPrevencionesPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.prevenciones.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(prevenciones) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = prevenciones.sapAmisPrevencionesList;
			resultadosJSON.perdidasTotalesArr = {};
			resultadosJSON.rechazosArr = {};
			resultadosJSON.salvamentosArr = {};
			resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.prevenciones.mensajeError);
            callBack();
        }
	});
}

function obtenerPerdidasTotalesPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: infoPortal.modulo == infoPortal.modulos[1].clave ? peticionesAjax.sistemas.perdidastotales.path : peticionesAjax.sistemas.repuvePTT.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(ptt) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = infoPortal.modulo == infoPortal.modulos[1].clave ? ptt.sapAmisPTTList : ptt.repuveSisPTTList;
			resultadosJSON.rechazosArr = {};
			resultadosJSON.salvamentosArr = {};
			resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(infoPortal.modulo == infoPortal.modulos[1].clave ? peticionesAjax.sistemas.perdidastotales.mensajeError : peticionesAjax.sistemas.repuvePTT.mensajeError);
            callBack();
        }
	});
}

function obtenerRechazosPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.rechazos.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(rechazos) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = {};
            resultadosJSON.rechazosArr = rechazos.sapAmisRechazosList;
			resultadosJSON.salvamentosArr = {};
			resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.rechazos.mensajeError);
            callBack();
        }
	});
}

function obtenerSalvamentoPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.salvamento.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(salvamento) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = {};
            resultadosJSON.rechazosArr = {};
            resultadosJSON.salvamentosArr = salvamento.sapAmisSalvamentoList;
            resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.salvamento.mensajeError);
            callBack();
        }
	});
}
function obtenerValuacionPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.valuacion.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(valuacion) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = {};
            resultadosJSON.rechazosArr = {};
            resultadosJSON.salvamentoArr = {};
            resultadosJSON.valuacionArr = valuacion.sapAmisValuacionList;
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = {};
			
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.valuacion.mensajeError);
            callBack();
        }
	});
}

function obtenerRoboPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.repuveRobo.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(robo) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = {};
            resultadosJSON.rechazosArr = {};
            resultadosJSON.salvamentosArr = {};
			resultadosJSON.roboArr = robo.repuveSisRoboList;
			resultadosJSON.recuperacionArr = {};
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.repuveRobo.mensajeError);
            callBack();
        }
	});
}

function obtenerRecuperacionPorFiltros(callBack){
	var parametros = {};
	parametros.parametrosConsulta = parametrosConsulta;
	parametros.numRegXPag = paginacion.numRegXPagina;
	parametros.numPagina = paginacion.paginaSeleccionada;
	jQuery.ajax({
        type: peticionesAjax.sistemas.type,
        dataType: peticionesAjax.sistemas.dataType,
        contentType: peticionesAjax.sistemas.contentType,
	    url: peticionesAjax.sistemas.repuveRecuperacion.path,
        cache: false,
        data: JSON.stringify(parametros),
	    async: true,
	    success: function(recuperacion) {
            resultadosJSON.emisionArr = {};
            resultadosJSON.siniestrosArr = {};
            resultadosJSON.prevencionesArr = {};
            resultadosJSON.perdidasTotalesArr = {};
            resultadosJSON.rechazosArr = {};
            resultadosJSON.salvamentosArr = {};
            resultadosJSON.valuacionArr = {};
			resultadosJSON.roboArr = {};
			resultadosJSON.recuperacionArr = recuperacion.repuveSisRecuperacionList;
            callBack();
	    },
	    error:function () {
            alert(peticionesAjax.sistemas.repuveRecuperacion.mensajeError);
            callBack();
        }
	});
}

function upload(callBack){
    var parametros = new FormData();
    	parametros.append('myFile', jQuery('input[type=file]')[0].files[0]);
    	parametros.append('fileName', jQuery('input[type=file]')[0].files[0].name);
    	parametros.append('sizeBytes', jQuery('input[type=file]')[0].files[0].size);
	if (jQuery('input[type=file]')[0].files[0].type == 'text/plain'){
		jQuery.ajax({
		    url: peticionesAjax.archivos.upload,
	        type: peticionesAjax.sistemas.type,
	        data: parametros,
	    	processData: false,
	    	contentType: false,
		    success: function(file) {
		    	alert(file.estatus==0 ? 'El archivo se cargado exitosamente. ' : 'Ocurrio un error al Cargar el Archivo. ');
	            callBack();
		    },
		    error:function () {
	            alert('Ocurrio un error inesperado.');
	            callBack();
	        }
		});
	} else {
	    alert('Solo esta permitido cargar archivos de texto con extensión TXT.');
	}
}