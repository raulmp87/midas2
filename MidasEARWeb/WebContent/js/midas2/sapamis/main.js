/************************************************************
 *	Nombre del Archivo: main.js
 *
 *	Proposito: 	Archivo JavaScrip principal.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function inicializaMain(){
	infoPortal.modulo = (($_GET('modulo') == 'SAPAMIS' || $_GET('modulo') == 'SAPAMIS#') ? infoPortal.modulos[1].clave : (($_GET('modulo') == 'REPUVE' || $_GET('modulo') == 'REPUVE#') ? infoPortal.modulos[2].clave : infoPortal.modulos[0].clave ));
	document.onclick = function(event){
		onClickTodo(event);
	};
	inicializarFechas(new Date());
	crearEstructuraPrincipal(function(){
		if(infoPortal.modulo != infoPortal.modulos[0].clave){
			inicializaBitacoras();	
		}
	});
}

function inicializaBitacoras(){
	if(estatusCarga){
		loadingData(1);
		inicializarDatos(function(){
			inicializaContenido(function(){
				loadingData(0);
			});
		});
	}
}

function inicializaContenido(callback){
	document.getElementById('titulo').innerHTML = infoPortal.elementHTML.portal.subElements[3].subElements[0].title + infoPortal.modulos[infoPortal.modulo].sufijoTitulos;
	document.getElementById(infoPortal.elementHTML.portal.subElements[3].subElements[1].id).innerHTML = '';
	if( (banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id) ||
		banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[1].id){
		crearFiltro(function(){
			obtenerCifrasByFilters(function(){
				if(banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id){
					if(infoPortal.modulo == infoPortal.modulos[1].clave){
						inicializaVisionGeneral(function(){
							callback();
						});
					}else if(infoPortal.modulo == infoPortal.modulos[2].clave){
						cargarArchivo(function(){
  	 						callBack();
		    			});
						crearTablas(function(){
  	 						callBack();
		    			});
						callback();
					}
				}
				else if(banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[1].id){
					inicializaConsultas(function(){
						callback();
					});
				}
			});
		});
	}else{
		if(banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[2].id){
			inicializaPanelControlProcesos(function(){
				callback();
			});
		}
	}
}

function inicializarDatos(callback){
	obtenerCatalogosIniciales(function(){
		callback();
	});
}

function obtenerCatalogosIniciales(callback){
	findByStatusRelSistemaOperacion(function(){
		findByStatusCatEstatusBitacora(function(){
			callback();
		})
	})
}

function crearEstructuraPrincipal(callback){
	document.title = infoPortal.modulos[infoPortal.modulo].title;
	infoPortal.elementHTML.portal.subElements[1].innerHTML = '<h1 style="position: absolute; top: 12px; left: 10px;">' + infoPortal.modulos[infoPortal.modulo].title + '</h1>';
	actualizarReferenciasHTML(function(){
		createChild(infoPortal.elementHTML.portal.id, 'div', 'body', 'tagName', infoPortal.elementHTML.portal.class, null, null);
		createChild(infoPortal.elementHTML.portal.subElements[0].id, 'div', infoPortal.elementHTML.portal.id, null, infoPortal.elementHTML.portal.subElements[0].class, infoPortal.elementHTML.portal.subElements[0].innerHTML, null);
		createChild(infoPortal.elementHTML.portal.subElements[1].id, 'div', infoPortal.elementHTML.portal.id, null, infoPortal.elementHTML.portal.subElements[1].class, infoPortal.elementHTML.portal.subElements[1].innerHTML, null);
		createChild(infoPortal.elementHTML.portal.subElements[2].id, 'div', infoPortal.elementHTML.portal.id, null, infoPortal.elementHTML.portal.subElements[2].class, null, null);
		createChild('menuNav', 'nav', infoPortal.elementHTML.portal.subElements[2].id, null, null, null, null);
		createChild('menuUl', 'ul', 'menuNav', null, null, null, null);
		createChild(infoPortal.elementHTML.portal.subElements[2].menu.id, 'li', 'menuUl', null, infoPortal.elementHTML.portal.subElements[2].menu.class, 
			'<div>' + infoPortal.elementHTML.portal.subElements[2].menu.text + '<div>', 
			infoPortal.elementHTML.portal.subElements[2].menu.onclick);
		for(var i=0; i<infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista.length; i++){
			if(infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[i].aplicaModulo[infoPortal.modulo]){
				createChild(infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[i].id, 'li', 'menuUl', null, infoPortal.elementHTML.portal.subElements[2].menu.subMenu.class, 
					'<img src="' + infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[i].icon + '"/><a href="#">'+infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[i].text+'</a>', 
					infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[i].onclick);
			}
		}
		createChild(infoPortal.elementHTML.portal.subElements[3].id, 'div', infoPortal.elementHTML.portal.id, null, infoPortal.elementHTML.portal.subElements[3].class, null, null);
		createChild(infoPortal.elementHTML.portal.subElements[3].subElements[0].id, 'div', infoPortal.elementHTML.portal.subElements[3].id, null, infoPortal.elementHTML.portal.subElements[3].subElements[0].class, null, null);
		createChild('titulo', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[0].id, null, 'titulo', infoPortal.elementHTML.portal.subElements[3].subElements[0].title, null);
		createChild(infoPortal.elementHTML.portal.subElements[3].subElements[1].id, 'div', infoPortal.elementHTML.portal.subElements[3].id, null, infoPortal.elementHTML.portal.subElements[3].subElements[1].class, null, null);
		createChild('contenedorBotonesBarra', 'div', infoPortal.elementHTML.portal.subElements[1].id, null, null, null, null);
		createChild(infoPortal.elementHTML.portal.subElements[1].subElements[0].id, 'div', 'contenedorBotonesBarra', null, null, datosFechas.fechaSeleccionada.formal + '    <img src="/MidasWeb/img/sapamis/icons/triangle.png"></img>', infoPortal.elementHTML.portal.subElements[1].subElements[0].onclick);
		callback();
	});
}

function inicializaVisionGeneral(callback){
	if(calculos.totales > 0 ){
		createChild('vgDiv', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null,null, null, null);
		if(infoPortal.modulo == infoPortal.modulos[2].clave){
			createChild('vgDivTitle', 'div', 'vgDiv', null, 'title', 'A continuacion se muestra un Informe Historico de los Envios SAP-AMIS.', null);
		}else{
			if(filtrosConfig.sistema == TOTALES){
				createChild('vgDivTitle', 'div', 'vgDiv', null, 'title', 'A continuacion se muestra un Informe Historico de los Envios SAP-AMIS.', null);
				createChild('vgDivEmisionTitle', 'div', 'vgDiv', null, 'subTitle', 'Emisiones.', null);
				createChild('vgDivEmisionTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.emision.totales + '</b> Emisiones', null);
				createChild('vgDivEmisionEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.emision.estatusEnviado + '</b> Emisiones', null);
				createChild('vgDivEmisionFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.emision.estatusError + '</b> Errores en el Envio Emisiones', null);
				createChild('vgDivEmisionPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.emision.estatusPendiente + '</b> Emisiones Pendientes de Enviarse', null);
				createChild('vgDivSiniestrosTitle', 'div', 'vgDiv', null, 'subTitle', 'Siniestros.', null);
				createChild('vgDivSiniestrosTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.siniestros.totales + '</b> Siniestros', null);
				createChild('vgDivSiniestrosEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.siniestros.estatusEnviado + '</b> Siniestros', null);
				createChild('vgDivSiniestrosFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.siniestros.estatusError + '</b> Errores en el Envio Siniestros', null);
				createChild('vgDivSiniestrosPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.siniestros.estatusPendiente + '</b> Siniestros Pendientes de Enviarse', null);
				createChild('vgDivPrevencionesTitle', 'div', 'vgDiv', null, 'subTitle', 'Prevenciones.', null);
				createChild('vgDivPrevencionesTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.prevenciones.totales + '</b> Prevenciones', null);
				createChild('vgDivPrevencionesEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.prevenciones.estatusEnviado + '</b> Prevenciones', null);
				createChild('vgDivPrevencionesFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.prevenciones.estatusError + '</b> Errores en el Envio Prevenciones', null);
				createChild('vgDivPrevencionesPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.prevenciones.estatusPendiente + '</b> Prevenciones Pendientes de Enviarse', null);
				createChild('vgDivPerdidasTotalesTitle', 'div', 'vgDiv', null, 'subTitle', 'Perdidas Totales.', null);
				createChild('vgDivPerdidasTotalesTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.perdidasTotales.totales + '</b> Perdidas Totales', null);
				createChild('vgDivPerdidasTotalesEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.perdidasTotales.estatusEnviado + '</b> Perdidas Totales', null);
				createChild('vgDivPerdidasTotalesFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.perdidasTotales.estatusError + '</b> Errores en el Envio Perdidas Totales', null);
				createChild('vgDivPerdidasTotalesPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.perdidasTotales.estatusPendiente + '</b> Perdidas Totales Pendientes de Enviarse', null);
				createChild('vgDivRechazosTitle', 'div', 'vgDiv', null, 'subTitle', 'Rechazos.', null);
				createChild('vgDivRechazosTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.rechazos.totales + '</b> Rechazos', null);
				createChild('vgDivRechazosEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.rechazos.estatusEnviado + '</b> Rechazos', null);
				createChild('vgDivRechazosFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.rechazos.estatusError + '</b> Errores en el Envio Rechazos', null);
				createChild('vgDivRechazosPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.rechazos.estatusPendiente + '</b> Rechazos Pendientes de Enviarse', null);
				createChild('vgDivSalvamentosTitle', 'div', 'vgDiv', null, 'subTitle', 'Salvamentos.', null);
				createChild('vgDivSalvamentosTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.salvamento.totales + '</b> Salvamentos', null);
				createChild('vgDivSalvamentosEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.salvamento.estatusEnviado + '</b> Salvamentos', null);
				createChild('vgDivSalvamentosFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.salvamento.estatusError + '</b> Errores en el Envio Salvamentos', null);
				createChild('vgDivSalvamentosPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.salvamento.estatusPendiente + '</b> Salvamentos Pendientes de Enviarse', null);
				createChild('vgDivValuacionesTitle', 'div', 'vgDiv', null, 'subTitle', 'Valuaciones.', null);
				createChild('vgDivValuacionesTotal', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + calculos.valuacion.totales + '</b> Valuaciones', null);
				createChild('vgDivValuacionesEnviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + calculos.valuacion.estatusEnviado + '</b> Valuaciones', null);
				createChild('vgDivValuacionesFallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + calculos.valuacion.estatusError + '</b> Errores en el Envio Valuaciones', null);
				createChild('vgDivValuacionesPendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + calculos.valuacion.estatusPendiente + '</b> Valuaciones Pendientes de Enviarse', null);
			}else{
				if(filtrosConfig.sistema != TOTALES){
					for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
						var totales = 0;
						var estatusEnviado = 0;
						var estatusError = 0;
						var estatusPendiente = 0;
						if('FiltrosSistema' + catalogos.relSistemaOperacionArr[i].catSistemas.id == filtrosSeleccionadosJSON.filtroSeleccionadoSistema){
							switch(catalogos.relSistemaOperacionArr[i].catSistemas.id){
								case 1:
									totales = calculos.emision.estatusEnviado;
									estatusEnviado = calculos.emision.estatusEnviado;
									estatusError = calculos.emision.estatusError;
									estatusPendiente = calculos.emision.estatusPendiente;
									switch(filtrosSeleccionadosJSON.filtroSeleccionadoOperacion){
										case 1:
											totales = calculos.alta.emision.estatusEnviado;
											estatusEnviado = calculos.alta.emision.estatusEnviado;
											estatusError = calculos.alta.emision.estatusError;
											estatusPendiente = calculos.alta.emision.estatusPendiente;
											break;
										case 2:
											totales = calculos.modificacion.emision.estatusEnviado;
											estatusEnviado = calculos.modificacion.emision.estatusEnviado;
											estatusError = calculos.modificacion.emision.estatusError;
											estatusPendiente = calculos.modificacion.emision.estatusPendiente;
											break;
										case 3:
											totales = calculos.cancelacion.emision.estatusEnviado;
											estatusEnviado = calculos.cancelacion.emision.estatusEnviado;
											estatusError = calculos.cancelacion.emision.estatusError;
											estatusPendiente = calculos.cancelacion.emision.estatusPendiente;
											break;
										case 4:
											totales = calculos.rehabilitacion.emision.estatusEnviado;
											estatusEnviado = calculos.rehabilitacion.emision.estatusEnviado;
											estatusError = calculos.rehabilitacion.emision.estatusError;
											estatusPendiente = calculos.rehabilitacion.emision.estatusPendiente;
											break;
									}
									break;
								case 2:
									totales = calculos.siniestros.estatusEnviado;
									estatusEnviado = calculos.siniestros.estatusEnviado;
									estatusError = calculos.siniestros.estatusError;
									estatusPendiente = calculos.siniestros.estatusPendiente;
									switch(filtrosSeleccionadosJSON.filtroSeleccionadoOperacion){
										case 1:
											totales = calculos.alta.siniestros.estatusEnviado;
											estatusEnviado = calculos.alta.siniestros.estatusEnviado;
											estatusError = calculos.alta.siniestros.estatusError;
											estatusPendiente = calculos.alta.siniestros.estatusPendiente;
											break;
										case 2:
											totales = calculos.modificacion.siniestros.estatusEnviado;
											estatusEnviado = calculos.modificacion.siniestros.estatusEnviado;
											estatusError = calculos.modificacion.siniestros.estatusError;
											estatusPendiente = calculos.modificacion.siniestros.estatusPendiente;
											break;
										case 3:
											totales = calculos.cancelacion.siniestros.estatusEnviado;
											estatusEnviado = calculos.cancelacion.siniestros.estatusEnviado;
											estatusError = calculos.cancelacion.siniestros.estatusError;
											estatusPendiente = calculos.cancelacion.siniestros.estatusPendiente;
											break;
									}
									break;
								case 4:
									totales = calculos.prevenciones.estatusEnviado;
									estatusEnviado = calculos.prevenciones.estatusEnviado;
									estatusError = calculos.prevenciones.estatusError;
									estatusPendiente = calculos.prevenciones.estatusPendiente;
									break;
								case 5:
									totales = calculos.perdidasTotales.estatusEnviado;
									estatusEnviado = calculos.perdidasTotales.estatusEnviado;
									estatusError = calculos.perdidasTotales.estatusError;
									estatusPendiente = calculos.perdidasTotales.estatusPendiente;
									switch(filtrosSeleccionadosJSON.filtroSeleccionadoOperacion){
										case 1:
											totales = calculos.alta.perdidasTotales.estatusEnviado;
											estatusEnviado = calculos.alta.perdidasTotales.estatusEnviado;
											estatusError = calculos.alta.perdidasTotales.estatusError;
											estatusPendiente = calculos.alta.perdidasTotales.estatusPendiente;
											break;
										case 2:
											totales = calculos.modificacion.perdidasTotales.estatusEnviado;
											estatusEnviado = calculos.modificacion.perdidasTotales.estatusEnviado;
											estatusError = calculos.modificacion.perdidasTotales.estatusError;
											estatusPendiente = calculos.modificacion.perdidasTotales.estatusPendiente;
											break;
										case 3:
											totales = calculos.cancelacion.perdidasTotales.estatusEnviado;
											estatusEnviado = calculos.cancelacion.perdidasTotales.estatusEnviado;
											estatusError = calculos.cancelacion.perdidasTotales.estatusError;
											estatusPendiente = calculos.cancelacion.perdidasTotales.estatusPendiente;
											break;
									}
									break;
								case 8:
									totales = calculos.rechazos.estatusEnviado;
									estatusEnviado = calculos.rechazos.estatusEnviado;
									estatusError = calculos.rechazos.estatusError;
									estatusPendiente = calculos.rechazos.estatusPendiente;
									break;
								case 9:
									totales = calculos.salvamento.estatusEnviado;
									estatusEnviado = calculos.salvamento.estatusEnviado;
									estatusError = calculos.salvamento.estatusError;
									estatusPendiente = calculos.salvamento.estatusPendiente;
									break;
								case 10:
									totales = calculos.valuacion.estatusEnviado;
									estatusEnviado = calculos.valuacion.estatusEnviado;
									estatusError = calculos.valuacion.estatusError;
									estatusPendiente = calculos.valuacion.estatusPendiente;
									break;

							}
							createChild('vgDivTitle', 'div', 'vgDiv', null, 'title', 'A continuacion se muestra un Informe Historico de los Envios SAP-AMIS.', null);
							createChild('vgDiv' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Title', 'div', 'vgDiv', null, 'subTitle', catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas + '.', null);
							createChild('vgDiv' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Total', 'div', 'vgDiv', null, 'leyenda', 'Se han procesado un total de <b>' + totales + '</b> ' + catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas + '.', null);
							createChild('vgDiv' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Enviados', 'div', 'vgDiv', null, 'leyenda', 'Se han Enviado Correctamente un total de <b>' + estatusEnviado + '</b> ' + catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas + '.', null);
							createChild('vgDiv' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Fallidos', 'div', 'vgDiv', null, 'leyenda', 'Se han Producido <b>' + estatusError + '</b> Errores en el Envio ' + catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas + '.', null);
							createChild('vgDiv' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Pendientes', 'div', 'vgDiv', null, 'leyenda', 'Existen <b>' + estatusPendiente + '</b> ' + catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas + '.Pendientes de Enviarse', null);
							i = catalogos.relSistemaOperacionArr.length;
						}
					}
				}
			}	
		}
		callback();
	}else{
		createChild('sinResultados', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null, 'sinResultados noSelect', 'No se han encontrado resultados con los filtros seleccionados.', null);
		callback();
	}
}


function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}