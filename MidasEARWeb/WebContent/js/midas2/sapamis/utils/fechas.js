/************************************************************
 *	Nombre del Archivo: fechas.js
 *
 *	Proposito: 	Inicializa los Datos para el Manejo de las 
 *				Fechas y Calendarios del Portal Web.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function inicializarFechas(fechaInicial){
	//DATOS FECHA SELECCIONADA
	datosFechas = {};
	datosFechas.fechaSeleccionada = {}
	datosFechas.fechaSeleccionada.date = fechaInicial;
	datosFechas.fechaSeleccionada.dia = {};
	datosFechas.fechaSeleccionada.dia.number = datosFechas.fechaSeleccionada.date.getDate();
	datosFechas.fechaSeleccionada.dia.semanaId = datosFechas.fechaSeleccionada.date.getDay();
	datosFechas.fechaSeleccionada.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.fechaSeleccionada.dia.semanaId);
	datosFechas.fechaSeleccionada.mes = {};
	datosFechas.fechaSeleccionada.mes.id = datosFechas.fechaSeleccionada.date.getMonth();
	datosFechas.fechaSeleccionada.mes.nombre =  obtenerMesFormal(datosFechas.fechaSeleccionada.mes.id,1);
	datosFechas.fechaSeleccionada.mes.nombreAbr =  obtenerMesFormal(datosFechas.fechaSeleccionada.mes.id,2);
	datosFechas.fechaSeleccionada.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.fechaSeleccionada.mes.id + 1);
	datosFechas.fechaSeleccionada.anio = datosFechas.fechaSeleccionada.date.getFullYear();
	datosFechas.fechaSeleccionada.formal = datosFechas.fechaSeleccionada.dia.semanaNombre + ', ' + datosFechas.fechaSeleccionada.dia.number + ' de ' + datosFechas.fechaSeleccionada.mes.nombre + ' del ' + datosFechas.fechaSeleccionada.anio;
	datosFechas.fechaSeleccionada.param = datosFechas.fechaSeleccionada.anio + '-' + datosFechas.fechaSeleccionada.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.fechaSeleccionada.dia.number);
	//DATOS fFECHA ACTUAL
	
	datosFechas.fechaActual = {}
	datosFechas.fechaActual.date = new Date();
	datosFechas.fechaActual.dia = {};
	datosFechas.fechaActual.dia.number = datosFechas.fechaActual.date.getDate();
	datosFechas.fechaActual.dia.semanaId = datosFechas.fechaActual.date.getDay();
	datosFechas.fechaActual.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.fechaActual.dia.semanaId);
	datosFechas.fechaActual.mes = {};
	datosFechas.fechaActual.mes.id = datosFechas.fechaActual.date.getMonth();
	datosFechas.fechaActual.mes.nombre =  obtenerMesFormal(datosFechas.fechaActual.mes.id,1);
	datosFechas.fechaActual.mes.nombreAbr =  obtenerMesFormal(datosFechas.fechaActual.mes.id,2);
	datosFechas.fechaActual.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.fechaActual.mes.id + 1);
	datosFechas.fechaActual.anio = datosFechas.fechaActual.date.getFullYear();
	datosFechas.fechaActual.formal = datosFechas.fechaActual.dia.semanaNombre + ', ' + datosFechas.fechaActual.dia.number + ' de ' + datosFechas.fechaActual.mes.nombre + ' del ' + datosFechas.fechaActual.anio;
	datosFechas.fechaActual.param = datosFechas.fechaActual.anio + '-' + datosFechas.fechaActual.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.fechaActual.dia.number);
	
	//DATOS PRIMER FECHA SEMANA DE LA FECHA SELECCIONADA
	datosFechas.primerFechaSemana = {};
	datosFechas.primerFechaSemana.date = new Date(datosFechas.fechaSeleccionada.date.getFullYear(), datosFechas.fechaSeleccionada.date.getMonth(), datosFechas.fechaSeleccionada.date.getDate() - datosFechas.fechaSeleccionada.date.getDay());
	datosFechas.primerFechaSemana.dia = {};
	datosFechas.primerFechaSemana.dia.number = datosFechas.primerFechaSemana.date.getDate();
	datosFechas.primerFechaSemana.dia.semanaId = datosFechas.primerFechaSemana.date.getDay();
	datosFechas.primerFechaSemana.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.primerFechaSemana.dia.semanaId);
	datosFechas.primerFechaSemana.mes = {};
	datosFechas.primerFechaSemana.mes.id = datosFechas.primerFechaSemana.date.getMonth();
	datosFechas.primerFechaSemana.mes.nombre =  obtenerMesFormal(datosFechas.primerFechaSemana.mes.id,1);
	datosFechas.primerFechaSemana.mes.nombreAbr =  obtenerMesFormal(datosFechas.primerFechaSemana.mes.id,2);
	datosFechas.primerFechaSemana.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.primerFechaSemana.mes.id + 1);
	datosFechas.primerFechaSemana.anio = datosFechas.primerFechaSemana.date.getFullYear();
	datosFechas.primerFechaSemana.formal = datosFechas.primerFechaSemana.dia.semanaNombre + ', ' + datosFechas.primerFechaSemana.anio + '-' + datosFechas.primerFechaSemana.mes.nombreAbr+ '-' + datosFechas.primerFechaSemana.dia.number;
	datosFechas.primerFechaSemana.param = datosFechas.primerFechaSemana.anio + '-' + datosFechas.primerFechaSemana.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.primerFechaSemana.dia.number);
	//DATOS ULTIMA FECHA SEMANA DE LA FECHA SELECCIONADA
	datosFechas.ultimaFechaSemana = {};
	datosFechas.ultimaFechaSemana.date = fechaRetorno = new Date(datosFechas.fechaSeleccionada.date.getFullYear(), datosFechas.fechaSeleccionada.date.getMonth(), datosFechas.fechaSeleccionada.date.getDate() + (6 - datosFechas.fechaSeleccionada.date.getDay()));
	datosFechas.ultimaFechaSemana.dia = {};
	datosFechas.ultimaFechaSemana.dia.number = datosFechas.ultimaFechaSemana.date.getDate();
	datosFechas.ultimaFechaSemana.dia.semanaId = datosFechas.ultimaFechaSemana.date.getDay();
	datosFechas.ultimaFechaSemana.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.ultimaFechaSemana.dia.semanaId);
	datosFechas.ultimaFechaSemana.mes = {};
	datosFechas.ultimaFechaSemana.mes.id = datosFechas.ultimaFechaSemana.date.getMonth();
	datosFechas.ultimaFechaSemana.mes.nombre =  obtenerMesFormal(datosFechas.ultimaFechaSemana.mes.id,1);
	datosFechas.ultimaFechaSemana.mes.nombreAbr =  obtenerMesFormal(datosFechas.ultimaFechaSemana.mes.id,2);
	datosFechas.ultimaFechaSemana.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.ultimaFechaSemana.mes.id + 1);
	datosFechas.ultimaFechaSemana.anio = datosFechas.ultimaFechaSemana.date.getFullYear();
	datosFechas.ultimaFechaSemana.formal = datosFechas.ultimaFechaSemana.dia.semanaNombre + ', ' + datosFechas.ultimaFechaSemana.anio + '-' + datosFechas.ultimaFechaSemana.mes.nombreAbr+ '-' + datosFechas.ultimaFechaSemana.dia.number;
	datosFechas.ultimaFechaSemana.param = datosFechas.ultimaFechaSemana.anio + '-' + datosFechas.ultimaFechaSemana.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.ultimaFechaSemana.dia.number);
	//DATOS PRIMER FECHA MES DE LA FECHA SELECCIONADA
	datosFechas.primerFechaMes = {};
	datosFechas.primerFechaMes.date = new Date(datosFechas.fechaSeleccionada.date.getFullYear(), datosFechas.fechaSeleccionada.date.getMonth(), 1);
	datosFechas.primerFechaMes.dia = {};
	datosFechas.primerFechaMes.dia.number = datosFechas.primerFechaMes.date.getDate();
	datosFechas.primerFechaMes.dia.semanaId = datosFechas.primerFechaMes.date.getDay();
	datosFechas.primerFechaMes.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.primerFechaMes.dia.semanaId);
	datosFechas.primerFechaMes.mes = {};
	datosFechas.primerFechaMes.mes.id = datosFechas.primerFechaMes.date.getMonth();
	datosFechas.primerFechaMes.mes.nombre =  obtenerMesFormal(datosFechas.primerFechaMes.mes.id,1);
	datosFechas.primerFechaMes.mes.nombreAbr =  obtenerMesFormal(datosFechas.primerFechaMes.mes.id,2);
	datosFechas.primerFechaMes.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.primerFechaMes.mes.id + 1);
	datosFechas.primerFechaMes.anio = datosFechas.primerFechaMes.date.getFullYear();
	datosFechas.primerFechaMes.formal = datosFechas.primerFechaMes.dia.semanaNombre + ', ' + datosFechas.primerFechaMes.anio + '-' + datosFechas.primerFechaMes.mes.nombreAbr+ '-' + datosFechas.primerFechaMes.dia.number;
	datosFechas.primerFechaMes.param = datosFechas.primerFechaMes.anio + '-' + datosFechas.primerFechaMes.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.primerFechaMes.dia.number);
	//DATOS ULTIMA FECHA MES DE LA FECHA SELECCIONADA
	datosFechas.ultimaFechaMes = {};
	datosFechas.ultimaFechaMes.date = new Date(datosFechas.fechaSeleccionada.date.getFullYear(), datosFechas.fechaSeleccionada.date.getMonth() + 1, 0);
	datosFechas.ultimaFechaMes.dia = {};
	datosFechas.ultimaFechaMes.dia.number = datosFechas.ultimaFechaMes.date.getDate();
	datosFechas.ultimaFechaMes.dia.semanaId = datosFechas.ultimaFechaMes.date.getDay();
	datosFechas.ultimaFechaMes.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.ultimaFechaMes.dia.semanaId);
	datosFechas.ultimaFechaMes.mes = {};
	datosFechas.ultimaFechaMes.mes.id = datosFechas.ultimaFechaMes.date.getMonth();
	datosFechas.ultimaFechaMes.mes.nombre =  obtenerMesFormal(datosFechas.ultimaFechaMes.mes.id,1);
	datosFechas.ultimaFechaMes.mes.nombreAbr =  obtenerMesFormal(datosFechas.ultimaFechaMes.mes.id,2);
	datosFechas.ultimaFechaMes.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.ultimaFechaMes.mes.id + 1);
	datosFechas.ultimaFechaMes.anio = datosFechas.ultimaFechaMes.date.getFullYear();
	datosFechas.ultimaFechaMes.formal = datosFechas.ultimaFechaMes.dia.semanaNombre + ', ' + datosFechas.ultimaFechaMes.anio + '-' + datosFechas.ultimaFechaMes.mes.nombreAbr+ '-' + obtenerFormatoNumero2Dig(datosFechas.ultimaFechaMes.dia.number);
	datosFechas.ultimaFechaMes.param = datosFechas.ultimaFechaMes.anio + '-' + datosFechas.ultimaFechaMes.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.ultimaFechaMes.dia.number);
	//DATOS PRIMER FECHA ANIO DE LA FECHA SELECCIONADA
	datosFechas.primerFechaAnio = {};
	datosFechas.primerFechaAnio.date = new Date(datosFechas.fechaSeleccionada.date.getFullYear(), 0, 1);
	datosFechas.primerFechaAnio.dia = {};
	datosFechas.primerFechaAnio.dia.number = datosFechas.primerFechaAnio.date.getDate();
	datosFechas.primerFechaAnio.dia.semanaId = datosFechas.primerFechaAnio.date.getDay();
	datosFechas.primerFechaAnio.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.primerFechaAnio.dia.semanaId);
	datosFechas.primerFechaAnio.mes = {};
	datosFechas.primerFechaAnio.mes.id = datosFechas.primerFechaAnio.date.getMonth();
	datosFechas.primerFechaAnio.mes.nombre =  obtenerMesFormal(datosFechas.primerFechaAnio.mes.id,1);
	datosFechas.primerFechaAnio.mes.nombreAbr =  obtenerMesFormal(datosFechas.primerFechaAnio.mes.id,2);
	datosFechas.primerFechaAnio.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.primerFechaAnio.mes.id + 1);
	datosFechas.primerFechaAnio.anio = datosFechas.primerFechaAnio.date.getFullYear();
	datosFechas.primerFechaAnio.formal = datosFechas.primerFechaAnio.dia.semanaNombre + ', ' + datosFechas.primerFechaAnio.anio + '-' + datosFechas.primerFechaAnio.mes.nombreAbr+ '-' + obtenerFormatoNumero2Dig(datosFechas.primerFechaAnio.dia.number);
	datosFechas.primerFechaAnio.param = datosFechas.primerFechaAnio.anio + '-' + datosFechas.primerFechaAnio.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.primerFechaAnio.dia.number);
	//DATOS ULTIMA FECHA ANIO DE LA FECHA SELECCIONADA
	datosFechas.ultimaFechaAnio = {};
	datosFechas.ultimaFechaAnio.date = new Date(datosFechas.fechaSeleccionada.date.getFullYear() + 1, 0, 0);
	datosFechas.ultimaFechaAnio.dia = {};
	datosFechas.ultimaFechaAnio.dia.number = datosFechas.ultimaFechaAnio.date.getDate();
	datosFechas.ultimaFechaAnio.dia.semanaId = datosFechas.ultimaFechaAnio.date.getDay();
	datosFechas.ultimaFechaAnio.dia.semanaNombre = obtenerDiaSemanaNombre(datosFechas.ultimaFechaAnio.dia.semanaId);
	datosFechas.ultimaFechaAnio.mes = {};
	datosFechas.ultimaFechaAnio.mes.id = datosFechas.ultimaFechaAnio.date.getMonth();
	datosFechas.ultimaFechaAnio.mes.nombre =  obtenerMesFormal(datosFechas.ultimaFechaAnio.mes.id,1);
	datosFechas.ultimaFechaAnio.mes.nombreAbr =  obtenerMesFormal(datosFechas.ultimaFechaAnio.mes.id,2);
	datosFechas.ultimaFechaAnio.mes.formatoNumero = obtenerFormatoNumero2Dig(datosFechas.ultimaFechaAnio.mes.id + 1);
	datosFechas.ultimaFechaAnio.anio = datosFechas.ultimaFechaAnio.date.getFullYear();
	datosFechas.ultimaFechaAnio.formal = datosFechas.ultimaFechaAnio.dia.semanaNombre + ', ' + datosFechas.ultimaFechaAnio.anio + '-' + datosFechas.ultimaFechaAnio.mes.nombreAbr+ '-' + obtenerFormatoNumero2Dig(datosFechas.ultimaFechaAnio.dia.number);
	datosFechas.ultimaFechaAnio.param = datosFechas.ultimaFechaAnio.anio + '-' + datosFechas.ultimaFechaAnio.mes.formatoNumero + '-' + obtenerFormatoNumero2Dig(datosFechas.ultimaFechaAnio.dia.number);
}

function obtenerMesFormal(mesId, tipo){
	//Nombre Completo
	if(tipo == 1){
		if(mesId == 0) {return 'Enero'}
		if(mesId == 1) {return 'Febrero'}
		if(mesId == 2) {return 'Marzo'}
		if(mesId == 3) {return 'Abril'}
		if(mesId == 4) {return 'Mayo'}
		if(mesId == 5) {return 'Junio'}
		if(mesId == 6) {return 'Julio'}
		if(mesId == 7) {return 'Agosto'}
		if(mesId == 8) {return 'Septiembre'}
		if(mesId == 9) {return 'Octubre'}
		if(mesId == 10){return 'Noviembre'}
		if(mesId == 11){return 'Diciembre'}
	}
	//Nombre Abreviado
	if(tipo == 2){
		if(mesId == 0) {return 'Ene.'}
		if(mesId == 1) {return 'Feb.'}
		if(mesId == 2) {return 'Mar.'}
		if(mesId == 3) {return 'Abr.'}
		if(mesId == 4) {return 'May.'}
		if(mesId == 5) {return 'Jun.'}
		if(mesId == 6) {return 'Jul.'}
		if(mesId == 7) {return 'Ago.'}
		if(mesId == 8) {return 'Sep.'}
		if(mesId == 9) {return 'Oct.'}
		if(mesId == 10){return 'Nov.'}
		if(mesId == 11){return 'Dic.'}
	}
}

function obtenerFormatoNumero2Dig(mesId){
	if((mesId+'').length<2){
		return '0' + mesId;
	}else{
		return mesId;
	}
}

function obtenerDiaSemanaNombre(dayWeekId){
	if(dayWeekId == 0) {return 'Domingo'}
	if(dayWeekId == 1) {return 'Lunes'}
	if(dayWeekId == 2) {return 'Martes'}
	if(dayWeekId == 3) {return 'Miércoles'}
	if(dayWeekId == 4) {return 'Jueves'}
	if(dayWeekId == 5) {return 'Viernes'}
	if(dayWeekId == 6) {return 'Sábado'}
}

function restaFecha(){
	fechaIniAnt = new Date();
	fechaIniAnt.setDate(fechaIni.getDate());
	fechaIniAnt.setMonth(fechaIni.getMonth()-1);
	if(fechaIni.getFullYear() < fechaIniAnt.getFullYear() ){
		fechaIniAnt.setFullYear(fechaIni.getFullYear());
	}

	fechaFinAnt = new Date();		
	fechaFinAnt.setMonth(fechaIniAnt.getMonth()+1);	
	fechaFinAnt.setDate(0);
	fechaFinAnt.setFullYear(fechaIniAnt.getFullYear());
	if(fechaFin.getFullYear() < fechaFinAnt.getFullYear() ){
		fechaFinAnt.setFullYear(fechaFin.getFullYear());
	}

	fechaIniFormal = fechaIniAnt.getDate() + ' ' + obtenerMesFormal(fechaIniAnt.getMonth()) + ' ' + fechaIniAnt.getFullYear();
	fechaFinFormal = fechaFinAnt.getDate() + ' ' + obtenerMesFormal(fechaFinAnt.getMonth()) + ' ' + fechaFinAnt.getFullYear();	
	var mesIni = (fechaIniAnt.getMonth() + 1) + '';
	var mesFin = (fechaFinAnt.getMonth() + 1) + '';
	var diaIni = fechaIniAnt.getDate() + '';
	var diaFin = fechaFinAnt.getDate() + '';
	var diaFin = (fechaFinAnt.getDate() + 1) + '';
	if(mesIni.length<2){
		mesIni = '0'+mesIni;
	}
	if(mesFin.length<2){
		mesFin = '0'+mesFin;
	}
	if(diaIni.length<2){
		diaIni = '0'+diaIni;
	}
	if(diaFin.length<2){
		diaFin = '0'+diaFin;
	}

	fechaIni = fechaIniAnt;
	fechaFin = fechaFinAnt;

	fechaIniParam = fechaIniAnt.getFullYear() + '-' + mesIni + '-' + diaIni;
	fechaFinParam = fechaFinAnt.getFullYear() + '-' + mesFin + '-' + diaFin;
	document.getElementById(infoPortal.elementHTML.portal.subElements[1].subElements[0].id).innerHTML = fechaIniFormal + ' - ' + fechaFinFormal;
	inicializaBitacoras();
}

function sumarFecha(){
	fechaIniSig = new Date();
	fechaIniSig.setDate(fechaIni.getDate());
	fechaIniSig.setMonth(fechaIni.getMonth()+1);
	if(fechaIni.getFullYear() > fechaIniSig.getFullYear() ){
		fechaIniSig.setFullYear(fechaIni.getFullYear());
	}

	fechaFinSig = new Date();		
	fechaFinSig.setMonth(fechaIniSig.getMonth()+1);	
	fechaFinSig.setDate(0);
	fechaFinSig.setFullYear(fechaIniSig.getFullYear());
	if(fechaFin.getFullYear() > fechaFinSig.getFullYear() ){
		fechaFinSig.setFullYear(fechaFin.getFullYear());
	}

	fechaIniFormal = fechaIniSig.getDate() + ' ' + obtenerMesFormal(fechaIniSig.getMonth()) + ' ' + fechaIniSig.getFullYear();
	fechaFinFormal = fechaFinSig.getDate() + ' ' + obtenerMesFormal(fechaFinSig.getMonth()) + ' ' + fechaFinSig.getFullYear();	
	var mesIni = (fechaIniSig.getMonth() + 1) + '';
	var mesFin = (fechaFinSig.getMonth() + 1) + '';
	var diaIni = fechaIniSig.getDate() + '';
	var diaFin = fechaFinSig.getDate() + '';
	var diaFin = (fechaFinSig.getDate() + 1) + '';
	if(mesIni.length<2){
		mesIni = '0'+mesIni;
	}
	if(mesFin.length<2){
		mesFin = '0'+mesFin;
	}
	if(diaIni.length<2){
		diaIni = '0'+diaIni;
	}
	if(diaFin.length<2){
		diaFin = '0'+diaFin;
	}

	fechaIni = fechaIniSig;
	fechaFin = fechaFinSig;

	fechaIniParam = fechaIniSig.getFullYear() + '-' + mesIni + '-' + diaIni;
	fechaFinParam = fechaFinSig.getFullYear() + '-' + mesFin + '-' + diaFin;
	document.getElementById(infoPortal.elementHTML.portal.subElements[1].subElements[0].id).innerHTML = fechaIniFormal + ' - ' + fechaFinFormal;
	inicializaBitacoras();
}

function inicializarCalendarioDatosTemporales(fechaCalendarioParam, callback){
	elementHTML.lista[0].existe = true;
	calendarioDatosTemporales.date = fechaCalendarioParam;
	calendarioDatosTemporales.anioCalendar = calendarioDatosTemporales.date.getFullYear();
	calendarioDatosTemporales.mesCalendar = {};
	calendarioDatosTemporales.mesCalendar.id = calendarioDatosTemporales.date.getMonth();
	calendarioDatosTemporales.mesCalendar.nombre = obtenerMesFormal(calendarioDatosTemporales.mesCalendar.id,1);
	calendarioDatosTemporales.mesCalendar.primerDiaSemana = new Date(calendarioDatosTemporales.anioCalendar, calendarioDatosTemporales.mesCalendar.id, 1).getDay();
	calendarioDatosTemporales.mesCalendar.ultimaDiaMes = new Date(calendarioDatosTemporales.anioCalendar, calendarioDatosTemporales.mesCalendar.id + 1, 0).getDate();
	callback();
}

function inicializarCalendario(){
	if(elementHTML.lista[0].existe){
		document.getElementById('calendario').remove();
		elementHTML.lista[0].existe = false;
	}else{
		inicializarCalendarioDatosTemporales(datosFechas.fechaSeleccionada.date, function(){
		createChild('calendario', 'div', infoPortal.elementHTML.portal.subElements[3].id, null, 'noSelect', null, function(event){
			elementHTML.elementosClickActual = 1;
		});
			createChild('anio', 'div', 'calendario', null, 'anio', null, null);
				createChild('anioPrev', 'div', 'anio', null, 'prev', '<img src="/MidasWeb/img/sapamis/icons/flecha1.png"></img>', function(){actualizarCalendario('Y', 'prev');});
				createChild('anioText', 'div', 'anio', null, 'title', calendarioDatosTemporales.anioCalendar, null);
				createChild('anioNext', 'div', 'anio', null, 'next', '<img src="/MidasWeb/img/sapamis/icons/flecha2.png"></img>', function(){actualizarCalendario('Y', 'next');});
			createChild('mes', 'div', 'calendario', null, 'mes', null, null);
				createChild('mesPrev', 'div', 'mes', null, 'prev', '<img src="/MidasWeb/img/sapamis/icons/flecha1.png"></img>', function(){actualizarCalendario('M', 'prev');});
				createChild('mesText', 'div', 'mes', null, 'title', calendarioDatosTemporales.mesCalendar.nombre, null);
				createChild('mesNext', 'div', 'mes', null, 'next', '<img src="/MidasWeb/img/sapamis/icons/flecha2.png"></img>', function(){actualizarCalendario('M', 'next');});
			createChild('semana', 'ul', 'calendario', null, 'semana', null, null);
				createChild('do', 'li', 'semana', null, null, 'Do', null);
				createChild('lu', 'li', 'semana', null, null, 'Lu', null);
				createChild('ma', 'li', 'semana', null, null, 'Ma', null);
				createChild('mi', 'li', 'semana', null, null, 'Mi', null);
				createChild('ju', 'li', 'semana', null, null, 'Ju', null);
				createChild('vi', 'li', 'semana', null, null, 'Vi', null);
				createChild('sa', 'li', 'semana', null, null, 'Sa', null);
			createChild('dias', 'ul', 'calendario', null, 'dias', null, null);
			actualizarDias();
		});
	}
}

function actualizarCalendario(mesOAnio, direccion){
	if(mesOAnio == 'Y'){
		if(direccion == 'next'){
			calendarioDatosTemporales.anioCalendar = calendarioDatosTemporales.anioCalendar + 1;
		}
		if(direccion == 'prev'){
			calendarioDatosTemporales.anioCalendar = calendarioDatosTemporales.anioCalendar - 1;
		}
	}
	if(mesOAnio == 'M'){
		if(direccion == 'next'){
			calendarioDatosTemporales.mesCalendar.id = calendarioDatosTemporales.mesCalendar.id + 1;
		}
		if(direccion == 'prev'){
			calendarioDatosTemporales.mesCalendar.id = calendarioDatosTemporales.mesCalendar.id - 1;
		}
	}
	inicializarCalendarioDatosTemporales(new Date(calendarioDatosTemporales.anioCalendar, calendarioDatosTemporales.mesCalendar.id, 1), function(){
		document.getElementById('anioText').innerHTML = calendarioDatosTemporales.anioCalendar;
		document.getElementById('mesText').innerHTML = calendarioDatosTemporales.mesCalendar.nombre;
		actualizarDias();
	});
}

function actualizarDias(){
	document.getElementById('dias').innerHTML = '';
	var dia;
	for(var c=0; c<calendarioDatosTemporales.mesCalendar.primerDiaSemana; c++){
		createChild('dia'+dia, 'li', 'dias', null, null, null);
	}
	for(var c=0; c <calendarioDatosTemporales.mesCalendar.ultimaDiaMes; c++){
		dia = c + 1;
		if(dia == datosFechas.fechaSeleccionada.dia.number && calendarioDatosTemporales.anioCalendar == datosFechas.fechaSeleccionada.anio && calendarioDatosTemporales.mesCalendar.id == datosFechas.fechaSeleccionada.mes.id){
			createChild('dia'+dia, 'li', 'dias', null, 'active', dia, null);
		}else{
			createChild('dia'+dia, 'li', 'dias', null, null, dia, function(){
				actualizaFechaGlobal(this.id);
			});
		}
	}
}

function actualizaFechaGlobal(dia){
	var functionName = 'actualizaFechaGlobal';
	var nuevaFecha = new Date(calendarioDatosTemporales.anioCalendar, calendarioDatosTemporales.mesCalendar.id, dia.substring(3));
	inicializarFechas(nuevaFecha);
	actualizarDias();
	actualizarContenido();
}

function actualizarContenido(){
	loadingData(1);
	inicializaContenido(function(){
		loadingData(0);
		document.getElementById(infoPortal.elementHTML.portal.subElements[1].subElements[0].id).innerHTML = datosFechas.fechaSeleccionada.formal + '    <img src="/MidasWeb/img/sapamis/icons/triangle.png"></img>';
	});
}

function obtenerFechaFormalJSON(fechaJSON){
	if(fechaJSON==null){
		return '';
	}else{
		var split = fechaJSON.split("-");
		return split[0] + '-' +  split[1] + '-' +  split[2].substring(0, 2);
	}
}