/************************************************************
 *	Nombre del Archivo: filtros.js
 *
 *	Proposito: 	Contruir y gestionar los Filtros permitidos 
 *				para la consulta de Bitacoras.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function crearFiltro(callback){
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL, 'div', estructuraFiltros.ELEMENTO_PADRE, null, estructuraFiltros.CLASS_CONTENEDOR_PRINCIPAL,null, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + 'Espacio', 'div', estructuraFiltros.ELEMENTO_PADRE, null, 'filtroEspacio',null, null);
	if(infoPortal.modulo == infoPortal.modulos[2].clave && banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id){
		filtrosConfig.periodo = PERIODO_MES;
		filtrosSeleccionadosJSON.filtroSeleccionadoPeriodo = estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_MES;
	}else{
		crearFiltroPeriodo();
		crearFiltroSistema();
		crearFiltroOperacion(1);
		crearFiltroEstatus();
	}
	actualizarFiltros(TOTALES, null, function(){
		callback();
	});
}

function crearFiltroPeriodo(){
	//SE CREA EL CONTENEDOR DEL FILTRO PERIODO.
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, 
		'ul', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL, null, estructuraFiltros.CLASS_FILTROS, null, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + 'Titulo', 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, estructuraFiltros.CLASS_FILTRO_TITULO, 'Periodo', null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + TOTALES, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, 
		(filtrosConfig.periodo == TOTALES)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
		'Todos', 
		function (){
			actualizarFiltros(FILTRO_PERIODO, this.id, function(){});
		});
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_DIA, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, 
		(filtrosConfig.periodo == PERIODO_DIA)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
		'Dia', 
		function (){
			actualizarFiltros(FILTRO_PERIODO, this.id, function(){});
		});
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_SEMANA, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, 
		(filtrosConfig.periodo == PERIODO_SEMANA)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
		'Semana', 
		function (){
			actualizarFiltros(FILTRO_PERIODO, this.id, function(){});
		});
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_MES, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, 
		(filtrosConfig.periodo == PERIODO_MES)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
		'Mes', 
		function (){
			actualizarFiltros(FILTRO_PERIODO, this.id, function(){});
		});
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_ANIO, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO, null, 
		(filtrosConfig.periodo == PERIODO_ANIO)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
		'Año', 
		function (){
			actualizarFiltros(FILTRO_PERIODO, this.id, function(){});
		});
}

function crearFiltroSistema(){
	//SE CREA EL CONTENEDOR DEL FILTRO SISTEMA.
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA, 'ul', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL, null, estructuraFiltros.CLASS_FILTROS, null, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + 'Titulo', 'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA, null, estructuraFiltros.CLASS_FILTRO_TITULO, estructuraFiltros.ID_SISTEMA, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + TOTALES, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA, null, 
		(filtrosConfig.sistema==TOTALES)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE, 
		'Todos', 
		function (){
			actualizarFiltros(FILTRO_SISTEMA, this.id, function(){});
		});
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		if(catalogos.relSistemaOperacionArr[i].catOperaciones.id == 1){
			createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + catalogos.relSistemaOperacionArr[i].catSistemas.id, 
				'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA, null, 
				(filtrosConfig.sistema==catalogos.relSistemaOperacionArr[i].catSistemas.id)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE,
				catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas, 
				function (){
					actualizarFiltros(FILTRO_SISTEMA, this.id, function(){});
				});
		}
	}
}

function crearFiltroOperacion(sistema){
	//SE CREA EL CONTENEDOR DEL FILTRO OPERACION.
	if(document.getElementById(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION) != null){
		document.getElementById(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION).innerHTML = '';
	}else{
		createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION, 'ul', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL, null, estructuraFiltros.CLASS_FILTROS, null, null);
	}
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + 'Titulo', 'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION, null, estructuraFiltros.CLASS_FILTRO_TITULO, estructuraFiltros.ID_OPERACION, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + TOTALES, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION, null, 
		(filtrosConfig.operacion == TOTALES)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE, 
		'Todos', 
		function (){
			actualizarFiltros(FILTRO_OPERACION, this.id, function(){});
		});
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		if(catalogos.relSistemaOperacionArr[i].catSistemas.id == sistema){
			createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + catalogos.relSistemaOperacionArr[i].catOperaciones.id,
				'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION, null, 
				(filtrosConfig.operacion == catalogos.relSistemaOperacionArr[i].catOperaciones.id)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE, 
				catalogos.relSistemaOperacionArr[i].catOperaciones.descCatOperaciones, 
				function (){
					actualizarFiltros(FILTRO_OPERACION, this.id, function(){})
				});
		}
	}
}

function crearFiltroEstatus(){
	//SE CREA EL CONTENEDOR DEL FILTRO ESTATUS.
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS, 'ul', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL, null, estructuraFiltros.CLASS_FILTROS, null, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + 'Titulo', 'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS, null, estructuraFiltros.CLASS_FILTRO_TITULO, estructuraFiltros.ID_ESTATUS, null);
	createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + TOTALES, 
		'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS, null, 
		(filtrosConfig.estatus == TOTALES)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE, 
		'Todos', 
		function (){
			actualizarFiltros(FILTRO_ESTATUS, this.id, function(){});
		});
	for(var i=0; i<catalogos.catEstatusBitacoraArr.length; i++){
		createChild(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + catalogos.catEstatusBitacoraArr[i].id, 
			'li', estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS, null, 
			(filtrosConfig.estatus == catalogos.catEstatusBitacoraArr[i].id)?estructuraFiltros.CLASS_SELECCIONADO:estructuraFiltros.CLASS_FILTRO_SIMPLE, 
			catalogos.catEstatusBitacoraArr[i].descCatEstatusBitacora, 
			function (){
				actualizarFiltros(FILTRO_ESTATUS, this.id, function(){});
			});
	}
}

function modificarFiltrosOperacionPorSistema(){
	filtrosConfig.operacion = TOTALES;
	filtrosSeleccionadosJSON.filtroSeleccionadoOperacion = estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + TOTALES;
	crearFiltroOperacion(filtrosConfig.sistema);
}

function actualizarFiltros(tipoFiltro, idElementHTML, callback){
	switch(tipoFiltro) {
	    case TOTALES:
			construirJSONCifrasByFilters(function(){
				callback();
			});
	        break;
	    case FILTRO_PERIODO:
			if(idElementHTML != filtrosSeleccionadosJSON.filtroSeleccionadoPeriodo){
				actualizarFiltroPeriodo(idElementHTML, function(){
					construirJSONCifrasByFilters(function(){
						loadingData(1);
						inicializaContenido(function(){
							loadingData(0);
							callback();
						});
					});
				});
			}
	        break;
	    case FILTRO_SISTEMA:
			if(idElementHTML != filtrosSeleccionadosJSON.filtroSeleccionadoSistema){
				actualizarFiltroSistema(idElementHTML, function(){
					modificarFiltrosOperacionPorSistema();
					construirJSONCifrasByFilters(function(){
						loadingData(1);
						inicializaContenido(function(){
							loadingData(0);
							callback();
						});
					});
				});
			}
	        break;
	    case FILTRO_OPERACION:
			if(idElementHTML != filtrosSeleccionadosJSON.filtroSeleccionadoOperacion){
				actualizarFiltroOperacion(idElementHTML, function(){
					construirJSONCifrasByFilters(function(){
						loadingData(1);
						inicializaContenido(function(){
							loadingData(0);
							callback();
						});
					});
				});
			}
	        break;
	    case FILTRO_ESTATUS:
			if(idElementHTML != filtrosSeleccionadosJSON.filtroSeleccionadoEstatus){
				actualizarFiltroEstatus(idElementHTML, function(){
					construirJSONCifrasByFilters(function(){
						loadingData(1);
						inicializaContenido(function(){
							loadingData(0);
							callback();
						});
					});
				});
			}
	        break;
	    default:
			callback();
	}
}

function actualizarFiltroPeriodo(idElementHTML, callback){
	switch(idElementHTML) {
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_DIA:
			filtrosConfig.periodo = PERIODO_DIA;
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_SEMANA:
			filtrosConfig.periodo = PERIODO_SEMANA;
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_MES:
			filtrosConfig.periodo = PERIODO_MES;
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_ANIO:
			filtrosConfig.periodo = PERIODO_ANIO;
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + TOTALES:
			filtrosConfig.periodo = TOTALES;
			break;
	}
	document.getElementById(filtrosSeleccionadosJSON.filtroSeleccionadoPeriodo).className = estructuraFiltros.CLASS_FILTRO_SIMPLE;
	filtrosSeleccionadosJSON.filtroSeleccionadoPeriodo = (infoPortal.modulo == infoPortal.modulos[2].clave && banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id) ?  estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_MES : idElementHTML;
	document.getElementById(idElementHTML).className = estructuraFiltros.CLASS_SELECCIONADO;
	callback();
}

function actualizarFiltroSistema(idElementHTML, callback){
	if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + TOTALES == idElementHTML){
		filtrosConfig.sistema = filtrosConfig.sistema = TOTALES;
	}else{
		for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + catalogos.relSistemaOperacionArr[i].catSistemas.id == idElementHTML){
				filtrosConfig.sistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
			}
		}
	}
	document.getElementById(filtrosSeleccionadosJSON.filtroSeleccionadoSistema).className = estructuraFiltros.CLASS_FILTRO_SIMPLE;
	filtrosSeleccionadosJSON.filtroSeleccionadoSistema = idElementHTML;
	document.getElementById(idElementHTML).className = estructuraFiltros.CLASS_SELECCIONADO;
	callback();
}

function actualizarFiltroOperacion(idElementHTML, callback){
	if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + TOTALES == idElementHTML){
		filtrosConfig.operacion = filtrosConfig.operacion = TOTALES;
	}else{
		for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + catalogos.relSistemaOperacionArr[i].catOperaciones.id == idElementHTML){filtrosConfig.operacion = catalogos.relSistemaOperacionArr[i].catOperaciones.id;
			}
		}
	}
	document.getElementById(filtrosSeleccionadosJSON.filtroSeleccionadoOperacion).className = estructuraFiltros.CLASS_FILTRO_SIMPLE;
	filtrosSeleccionadosJSON.filtroSeleccionadoOperacion = idElementHTML;
	document.getElementById(idElementHTML).className = estructuraFiltros.CLASS_SELECCIONADO;
	callback();
}

function actualizarFiltroEstatus(idElementHTML, callback){
	document.getElementById(filtrosSeleccionadosJSON.filtroSeleccionadoEstatus).className = estructuraFiltros.CLASS_FILTRO_SIMPLE;
	filtrosSeleccionadosJSON.filtroSeleccionadoEstatus = idElementHTML;
	document.getElementById(idElementHTML).className = estructuraFiltros.CLASS_SELECCIONADO;
	if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + TOTALES == idElementHTML){
		filtrosConfig.estatus = filtrosConfig.estatus = TOTALES;
	}else{	
		for(var i=0; i<catalogos.catEstatusBitacoraArr.length; i++){
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + catalogos.catEstatusBitacoraArr[i].id == idElementHTML){
				filtrosConfig.estatus = catalogos.catEstatusBitacoraArr[i].id;
			}
		}
	}
	callback();
}