function cargarArchivo(callback){
	createChild('vgUpdateDiv', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null,null, null, null);
	createChild('iniForm','div','vgUpdateDiv',null,'form','<form action="javascript:upload(function(){});" method="post" enctype="multipart/form-data">',null)
	createChild('lbTxt',null,'iniForm',null,'form','  <label for="upload">Arhivos de Respuesta del REPUVE: </label>', null);
	createChild('inFile',null,'iniForm',null,'form','	<input type="file" name="myFile" id="myFile"/>', null);
	createChild('inSubir',null,'iniForm',null,'form','	<input type="submit" value="Subir"/>',  function(){upload(function(){});});
	createChild('cerrarForm',null,'iniForm',null,'form', '</form>', null);

}
function crearTablas(callback){
	createChild('tablaArchivosDescarga', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null,null, null, null);	
	createChild('tablaArchivos', 'table', 'tablaArchivosDescarga', null,null, null , null);	
	createChild('abrirEnc','tr','tablaArchivos',null,'gridTh thSistema',null, null);
	createChild('columna1Enc','th','abrirEnc',null,'gridTh thSistema','Avisos', null);
	createChild('columna2Enc','th','abrirEnc',null,'gridTh thSistema','Consultas', null);
	createChild('cerrarEnc','tr','tablaArchivos',null,'gridTh thSistema',null, null);
	var diasMes ;
	if((datosFechas.fechaSeleccionada.formal && datosFechas.fechaSeleccionada.mes.formatoNumero)==(datosFechas.fechaActual.formal 
		&&datosFechas.fechaActual.mes.formatoNumero)&&(datosFechas.fechaSeleccionada.anio==datosFechas.fechaActual.anio)){
	diasMes =obtenerFormatoNumero2Dig(datosFechas.fechaActual.dia.number);
	}else{
	diasMes =obtenerFormatoNumero2Dig(datosFechas.ultimaFechaMes.dia.number);
	}
	for(var i=1;i<=diasMes;i++){
	createChild('abrirFila'+i,'tr','tablaArchivos',null,'tbody',null, null);
	createChild('fila1_1','td','abrirFila'+i,null,'gridTd tdBitacora','<a href="/MidasWeb/repuve/file/download.action?fechaProcesarArchivo='+datosFechas.fechaSeleccionada.anio + 
		'-' + datosFechas.fechaSeleccionada.mes.formatoNumero + 
		'-' + (obtenerFormatoNumero2Dig(diasMes-i+1))+'&tipoArchivo=1 ">AvisosRepuve_'
		    +datosFechas.fechaSeleccionada.anio + 
		'-' + datosFechas.fechaSeleccionada.mes.formatoNumero + 
		'-' + (obtenerFormatoNumero2Dig(diasMes-i+1))+'.txt</a>', null);

	createChild('fila2_2','td','abrirFila'+i,null,'gridTd tdBitacora','<a href="/MidasWeb/repuve/file/download.action?fechaProcesarArchivo='+datosFechas.fechaSeleccionada.anio + 
		'-' + datosFechas.fechaSeleccionada.mes.formatoNumero + 
		'-' + (obtenerFormatoNumero2Dig(diasMes-i+1))+'&tipoArchivo=2 ">ConsultasRepuve_'+datosFechas.fechaSeleccionada.anio + 
		'-' + datosFechas.fechaSeleccionada.mes.formatoNumero + 
		'-' + (obtenerFormatoNumero2Dig(diasMes-i+1))+'.txt', null);
						}
	}