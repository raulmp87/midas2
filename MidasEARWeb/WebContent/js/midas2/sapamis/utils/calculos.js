/************************************************************
 *	Nombre del Archivo: calculos.js
 *
 *	Proposito: 	Realizar los calculos correspondientes para
 *				obtener las cifras precisas de las Bitacoras 
 *				segun los filtros que se manejan.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/

function calculosIniciales(callback){
	resetCalculos(function(){
		for(var i=0; i<resultadosJSON.cifrasArr.length; i++){
			calculos.totales = calculos.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
			if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
				switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
					case -1: calculos.estatusError = calculos.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						break;
					case 0: calculos.estatusEnviado = calculos.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						break;
					case 1: calculos.estatusPendiente = calculos.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						break;
					case 2: calculos.estatusDatosPendientes = calculos.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						break;
				}
			}
			if(resultadosJSON.cifrasArr[i].catSistemas != null){
				switch(resultadosJSON.cifrasArr[i].catSistemas.id){
					case 1:
						calculos.emision.totales = calculos.emision.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.emision.estatusError = calculos.emision.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case  0: calculos.emision.estatusEnviado = calculos.emision.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case  1: calculos.emision.estatusPendiente = calculos.emision.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case  2: calculos.emision.estatusDatosPendientes = calculos.emision.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.emision.alta.totales = calculos.emision.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.emision.alta.estatusError = calculos.emision.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;			
											case 0: calculos.emision.alta.estatusEnviado = calculos.emision.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.emision.alta.estatusPendiente = calculos.emision.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.emision.alta.estatusDatosPendientes = calculos.emision.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 2: calculos.emision.modificacion.totales = calculos.emision.modificacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.emision.modificacion.estatusError = calculos.emision.modificacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;								
											case 0: calculos.emision.modificacion.estatusEnviado = calculos.emision.modificacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.emision.modificacion.estatusPendiente = calculos.emision.modificacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.emision.modificacion.estatusDatosPendientes = calculos.emision.modificacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 3: calculos.emision.cancelacion.totales = calculos.emision.cancelacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.emision.cancelacion.estatusError = calculos.emision.cancelacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;							
											case 0: calculos.emision.cancelacion.estatusEnviado = calculos.emision.cancelacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.emision.cancelacion.estatusPendiente = calculos.emision.cancelacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.emision.cancelacion.estatusDatosPendientes = calculos.emision.cancelacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 4: calculos.emision.rehabilitacion.totales = calculos.emision.rehabilitacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.emision.rehabilitacion.estatusError = calculos.emision.rehabilitacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;								
											case 0: calculos.emision.rehabilitacion.estatusEnviado = calculos.emision.rehabilitacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.emision.rehabilitacion.estatusPendiente = calculos.emision.rehabilitacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.emision.rehabilitacion.estatusDatosPendientes = calculos.emision.rehabilitacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 2:
						calculos.siniestros.totales = calculos.siniestros.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.siniestros.estatusError = calculos.siniestros.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.siniestros.estatusEnviado = calculos.siniestros.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.siniestros.estatusPendiente = calculos.siniestros.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.siniestros.estatusDatosPendientes = calculos.siniestros.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.siniestros.alta.totales = calculos.siniestros.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.siniestros.alta.estatusError = calculos.siniestros.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.siniestros.alta.estatusEnviado = calculos.siniestros.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.siniestros.alta.estatusPendiente = calculos.siniestros.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.siniestros.alta.estatusDatosPendientes = calculos.siniestros.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 2: calculos.siniestros.modificacion.totales = calculos.siniestros.modificacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.siniestros.modificacion.estatusError = calculos.siniestros.modificacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.siniestros.modificacion.estatusEnviado = calculos.siniestros.modificacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.siniestros.modificacion.estatusPendiente = calculos.siniestros.modificacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.siniestros.modificacion.estatusDatosPendientes = calculos.siniestros.modificacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 3: calculos.siniestros.cancelacion.totales = calculos.siniestros.cancelacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.siniestros.cancelacion.estatusError = calculos.siniestros.cancelacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.siniestros.cancelacion.estatusEnviado = calculos.siniestros.cancelacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.siniestros.cancelacion.estatusPendiente = calculos.siniestros.cancelacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.siniestros.cancelacion.estatusDatosPendientes = calculos.siniestros.cancelacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 4:
						calculos.prevenciones.totales = calculos.prevenciones.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.prevenciones.estatusError = calculos.prevenciones.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.prevenciones.estatusEnviado = calculos.prevenciones.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.prevenciones.estatusPendiente = calculos.prevenciones.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.prevenciones.estatusDatosPendientes = calculos.prevenciones.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.prevenciones.alta.totales = calculos.prevenciones.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.prevenciones.alta.estatusError = calculos.prevenciones.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.prevenciones.alta.estatusEnviado = calculos.prevenciones.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.prevenciones.alta.estatusPendiente = calculos.prevenciones.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.prevenciones.alta.estatusDatosPendientes = calculos.prevenciones.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 5:
						calculos.perdidasTotales.totales = calculos.perdidasTotales.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.perdidasTotales.estatusError = calculos.perdidasTotales.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.perdidasTotales.estatusEnviado = calculos.perdidasTotales.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.perdidasTotales.estatusPendiente = calculos.perdidasTotales.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.perdidasTotales.estatusDatosPendientes = calculos.perdidasTotales.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.perdidasTotales.alta.totales = calculos.perdidasTotales.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.perdidasTotales.alta.estatusError = calculos.perdidasTotales.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.perdidasTotales.alta.estatusEnviado = calculos.perdidasTotales.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.perdidasTotales.alta.estatusPendiente = calculos.perdidasTotales.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.perdidasTotales.alta.estatusDatosPendientes = calculos.perdidasTotales.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 2: calculos.perdidasTotales.modificacion.totales = calculos.perdidasTotales.modificacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.perdidasTotales.modificacion.estatusError = calculos.perdidasTotales.modificacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.perdidasTotales.modificacion.estatusEnviado = calculos.perdidasTotales.modificacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.perdidasTotales.modificacion.estatusPendiente = calculos.perdidasTotales.modificacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.perdidasTotales.modificacion.estatusDatosPendientes = calculos.perdidasTotales.modificacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
								case 3: calculos.perdidasTotales.cancelacion.totales = calculos.perdidasTotales.cancelacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.perdidasTotales.cancelacion.estatusError = calculos.perdidasTotales.cancelacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.perdidasTotales.cancelacion.estatusEnviado = calculos.perdidasTotales.cancelacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.perdidasTotales.cancelacion.estatusPendiente = calculos.perdidasTotales.cancelacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.perdidasTotales.cancelacion.estatusDatosPendientes = calculos.perdidasTotales.cancelacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 8:
						calculos.rechazos.totales = calculos.rechazos.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.rechazos.estatusError = calculos.rechazos.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.rechazos.estatusEnviado = calculos.rechazos.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.rechazos.estatusPendiente = calculos.rechazos.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.rechazos.estatusDatosPendientes = calculos.rechazos.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.rechazos.alta.totales = calculos.rechazos.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.rechazos.alta.estatusError = calculos.rechazos.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.rechazos.alta.estatusEnviado = calculos.rechazos.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.rechazos.alta.estatusPendiente = calculos.rechazos.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.rechazos.alta.estatusDatosPendientes = calculos.rechazos.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 9:
						calculos.salvamento.totales = calculos.salvamento.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.salvamento.estatusError = calculos.salvamento.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.salvamento.estatusEnviado = calculos.salvamento.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.salvamento.estatusPendiente = calculos.salvamento.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.salvamento.estatusDatosPendientes = calculos.salvamento.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.salvamento.alta.totales = calculos.salvamento.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.salvamento.alta.estatusError = calculos.salvamento.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.salvamento.alta.estatusEnviado = calculos.salvamento.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.salvamento.alta.estatusPendiente = calculos.salvamento.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.salvamento.alta.estatusDatosPendientes = calculos.salvamento.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
					case 10:
						calculos.valuacion.totales = calculos.valuacion.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
						if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
							switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
								case -1: calculos.valuacion.estatusError = calculos.valuacion.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 0: calculos.valuacion.estatusEnviado = calculos.valuacion.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 1: calculos.valuacion.estatusPendiente = calculos.valuacion.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
								case 2: calculos.valuacion.estatusDatosPendientes = calculos.valuacion.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
								break;
							}
						}
						if(resultadosJSON.cifrasArr[i].catOperaciones != null){
							switch(resultadosJSON.cifrasArr[i].catOperaciones.id){
								case 1: calculos.valuacion.alta.totales = calculos.valuacion.alta.totales + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
									if(resultadosJSON.cifrasArr[i].catEstatusBitacora != null){
										switch(resultadosJSON.cifrasArr[i].catEstatusBitacora.id){
											case -1: calculos.valuacion.alta.estatusError = calculos.valuacion.alta.estatusError + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 0: calculos.valuacion.alta.estatusEnviado = calculos.valuacion.alta.estatusEnviado + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 1: calculos.valuacion.alta.estatusPendiente = calculos.valuacion.alta.estatusPendiente + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
											case 2: calculos.valuacion.alta.estatusDatosPendientes = calculos.valuacion.alta.estatusDatosPendientes + resultadosJSON.cifrasArr[i].idSapAmisRegistro;
											break;
										}
									}
								break;
							}
						}
					break;
				}
			}
		}
		callback();
	});
}

function resetCalculos(callback){	
	//TOTALES
	calculos.totales = 0;
	calculos.estatusEnviado = 0;
	calculos.estatusPendiente = 0;
	calculos.estatusError = 0;
	calculos.estatusDatosPendientes = 0;
	//EMISION
	calculos.emision = {};
	calculos.emision.totales = 0;
	calculos.emision.estatusEnviado = 0;
	calculos.emision.estatusPendiente = 0;
	calculos.emision.estatusError = 0;
	calculos.emision.estatusDatosPendientes = 0;
	//ALTA
	calculos.emision.alta = {}
	calculos.emision.alta.totales = 0;
	calculos.emision.alta.estatusEnviado = 0;
	calculos.emision.alta.estatusPendiente = 0;
	calculos.emision.alta.estatusError = 0;
	calculos.emision.alta.estatusDatosPendientes = 0;
	//MODIFICACION
	calculos.emision.modificacion = {}
	calculos.emision.modificacion.totales = 0;
	calculos.emision.modificacion.estatusEnviado = 0;
	calculos.emision.modificacion.estatusPendiente = 0;
	calculos.emision.modificacion.estatusError = 0;
	calculos.emision.modificacion.estatusDatosPendientes = 0;
	//CANCELACION
	calculos.emision.cancelacion = {}
	calculos.emision.cancelacion.totales = 0;
	calculos.emision.cancelacion.estatusEnviado = 0;
	calculos.emision.cancelacion.estatusPendiente = 0;
	calculos.emision.cancelacion.estatusError = 0;
	calculos.emision.cancelacion.estatusDatosPendientes = 0;
	//REHABILITACION
	calculos.emision.rehabilitacion = {}
	calculos.emision.rehabilitacion.totales = 0;
	calculos.emision.rehabilitacion.estatusEnviado = 0;
	calculos.emision.rehabilitacion.estatusPendiente = 0;
	calculos.emision.rehabilitacion.estatusError = 0;
	calculos.emision.rehabilitacion.estatusDatosPendientes = 0;
	//SINIESTROS
	calculos.siniestros = {};
	calculos.siniestros.totales = 0;
	calculos.siniestros.estatusEnviado = 0;
	calculos.siniestros.estatusPendiente = 0;
	calculos.siniestros.estatusError = 0;
	calculos.siniestros.estatusDatosPendientes = 0;
	//ALTA
	calculos.siniestros.alta = {}
	calculos.siniestros.alta.totales = 0;
	calculos.siniestros.alta.estatusEnviado = 0;
	calculos.siniestros.alta.estatusPendiente = 0;
	calculos.siniestros.alta.estatusError = 0;
	calculos.siniestros.alta.estatusDatosPendientes = 0;
	//MODIFICACION
	calculos.siniestros.modificacion = {}
	calculos.siniestros.modificacion.totales = 0;
	calculos.siniestros.modificacion.estatusEnviado = 0;
	calculos.siniestros.modificacion.estatusPendiente = 0;
	calculos.siniestros.modificacion.estatusError = 0;
	calculos.siniestros.modificacion.estatusDatosPendientes = 0;
	//CANCELACION
	calculos.siniestros.cancelacion = {}
	calculos.siniestros.cancelacion.totales = 0;
	calculos.siniestros.cancelacion.estatusEnviado = 0;
	calculos.siniestros.cancelacion.estatusPendiente = 0;
	calculos.siniestros.cancelacion.estatusError = 0;
	calculos.siniestros.cancelacion.estatusDatosPendientes = 0;
	//PREVENCIONES
	calculos.prevenciones = {};
	calculos.prevenciones.totales = 0;
	calculos.prevenciones.estatusEnviado = 0;
	calculos.prevenciones.estatusPendiente = 0;
	calculos.prevenciones.estatusError = 0;
	calculos.prevenciones.estatusDatosPendientes = 0;
	//ALTA
	calculos.prevenciones.alta = {}
	calculos.prevenciones.alta.totales = 0;
	calculos.prevenciones.alta.estatusEnviado = 0;
	calculos.prevenciones.alta.estatusPendiente = 0;
	calculos.prevenciones.alta.estatusError = 0;
	calculos.prevenciones.alta.estatusDatosPendientes = 0;
	//PERDIDAS TOTALES
	calculos.perdidasTotales = {}; 
	calculos.perdidasTotales.totales = 0;
	calculos.perdidasTotales.estatusEnviado = 0;
	calculos.perdidasTotales.estatusPendiente = 0;
	calculos.perdidasTotales.estatusError = 0;
	calculos.perdidasTotales.estatusDatosPendientes = 0;
	//ALTA
	calculos.perdidasTotales.alta = {}
	calculos.perdidasTotales.alta.totales = 0;
	calculos.perdidasTotales.alta.estatusEnviado = 0;
	calculos.perdidasTotales.alta.estatusPendiente = 0;
	calculos.perdidasTotales.alta.estatusError = 0;
	calculos.perdidasTotales.alta.estatusDatosPendientes = 0;
	//MODIFICACION
	calculos.perdidasTotales.modificacion = {}
	calculos.perdidasTotales.modificacion.totales = 0;
	calculos.perdidasTotales.modificacion.estatusEnviado = 0;
	calculos.perdidasTotales.modificacion.estatusPendiente = 0;
	calculos.perdidasTotales.modificacion.estatusError = 0;
	calculos.perdidasTotales.modificacion.estatusDatosPendientes = 0;
	//CANCELACION
	calculos.perdidasTotales.cancelacion = {}
	calculos.perdidasTotales.cancelacion.totales = 0;
	calculos.perdidasTotales.cancelacion.estatusEnviado = 0;
	calculos.perdidasTotales.cancelacion.estatusPendiente = 0;
	calculos.perdidasTotales.cancelacion.estatusError = 0;
	calculos.perdidasTotales.cancelacion.estatusDatosPendientes = 0;
	//RECHAZOS
	calculos.rechazos = {};
	calculos.rechazos.totales = 0;
	calculos.rechazos.estatusEnviado = 0;
	calculos.rechazos.estatusPendiente = 0;
	calculos.rechazos.estatusError = 0;
	calculos.rechazos.estatusDatosPendientes = 0;
	//ALTA
	calculos.rechazos.alta = {}
	calculos.rechazos.alta.totales = 0;
	calculos.rechazos.alta.estatusEnviado = 0;
	calculos.rechazos.alta.estatusPendiente = 0;
	calculos.rechazos.alta.estatusError = 0;
	calculos.rechazos.alta.estatusDatosPendientes = 0;
	//SALVAMENTO
	calculos.salvamento = {};
	calculos.salvamento.totales = 0;
	calculos.salvamento.estatusEnviado = 0;
	calculos.salvamento.estatusPendiente = 0;
	calculos.salvamento.estatusError = 0;
	calculos.salvamento.estatusDatosPendientes = 0;
	//ALTA
	calculos.salvamento.alta = {}
	calculos.salvamento.alta.totales = 0;
	calculos.salvamento.alta.estatusEnviado = 0;
	calculos.salvamento.alta.estatusPendiente = 0;
	calculos.salvamento.alta.estatusError = 0;
	calculos.salvamento.alta.estatusDatosPendientes = 0;
	//VALUACION
	calculos.valuacion = {};
	calculos.valuacion.totales = 0;
	calculos.valuacion.estatusEnviado = 0;
	calculos.valuacion.estatusPendiente = 0;
	calculos.valuacion.estatusError = 0;
	calculos.valuacion.estatusDatosPendientes = 0;
	//ALTA
	calculos.valuacion.alta = {}
	calculos.valuacion.alta.totales = 0;
	calculos.valuacion.alta.estatusEnviado = 0;
	calculos.valuacion.alta.estatusPendiente = 0;
	calculos.valuacion.alta.estatusError = 0;
	calculos.valuacion.alta.estatusDatosPendientes = 0;
	
	callback();
}