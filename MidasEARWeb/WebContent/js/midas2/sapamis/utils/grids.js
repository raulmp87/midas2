/************************************************************
 *	Nombre del Archivo: grids.js
 *
 *	Proposito: 	Construccion y manejo de los GRIDS que podran
 *				desplegarse dentro del Portal WEB.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/

function inicializaConsultas(callback){
	switch(filtrosSeleccionadosJSON.filtroSeleccionadoSistema){
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[1].idSistema:
			inicializaConsultaEmision(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[2].idSistema:
			inicializaConsultaSiniestros(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[3].idSistema:
			inicializaConsultaPrevenciones(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[4].idSistema:
			inicializaConsultaPerdidasTotales(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[5].idSistema:
			inicializaConsultaRechazos(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[6].idSistema:
			inicializaConsultaSalvamento(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[8].idSistema:
			inicializaConsultaRobo(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[9].idSistema :
			inicializaConsultaRecuperacion(function(){
				callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + visionGeneral.elementHTML.subTitle.list[7].idSistema:
			inicializaConsultaValuacion(function(){
			callback();
			});
			break;
		case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + TOTALES:
			desplegarSeleccionarSistema(function(){
				callback();
			});
			break;
		
	}
}

function inicializaConsultaEmision(callback){
	grids.gridSeleccionado = grids.sistemas.lista[0].modulo[infoPortal.modulo].descripcion;
	obtenerEmisionPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaSiniestros(callback){
	grids.gridSeleccionado = grids.sistemas.lista[1].modulo[infoPortal.modulo].descripcion;
	obtenerSiniestrosPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaPrevenciones(callback){
	grids.gridSeleccionado = grids.sistemas.lista[2].modulo[infoPortal.modulo].descripcion;
	obtenerPrevencionesPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaPerdidasTotales(callback){
	grids.gridSeleccionado = grids.sistemas.lista[3].modulo[infoPortal.modulo].descripcion;
	obtenerPerdidasTotalesPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaRechazos(callback){
	grids.gridSeleccionado = grids.sistemas.lista[4].modulo[infoPortal.modulo].descripcion;
	obtenerRechazosPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaSalvamento(callback){
	grids.gridSeleccionado = grids.sistemas.lista[5].modulo[infoPortal.modulo].descripcion;
	obtenerSalvamentoPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaRobo(callback){
	grids.gridSeleccionado = grids.sistemas.lista[6].modulo[infoPortal.modulo].descripcion;
	obtenerRoboPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaRecuperacion(callback){ 
	grids.gridSeleccionado = grids.sistemas.lista[7].modulo[infoPortal.modulo].descripcion;
	obtenerRecuperacionPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function inicializaConsultaValuacion(callback){
	grids.gridSeleccionado = grids.sistemas.lista[8].modulo[infoPortal.modulo].descripcion;
	obtenerValuacionPorFiltros(function(){
		if(calculos.totales>0){
			construirGrid(function(){
				callback();
			});
		}else{
			desplegarSinResultados(function(){
				callback();
			});
		}
	});
}

function construirGrid(callback){
	createChild(grids.gridSeleccionado, 'table', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null, 'tablaSistema');
	createChild(grids.gridSeleccionado + 'thead', 'thead', grids.gridSeleccionado, null, 'gridTHead');
	createChild(grids.gridSeleccionado + 'tbody', 'tbody', grids.gridSeleccionado, null, 'gridTBody');
	createChild(grids.gridSeleccionado + 'theadtr', 'tr', grids.gridSeleccionado + 'thead', null, 'gridTr');

	if(infoPortal.modulo == 2 && banderas.menuSeleccionado == infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id){

	}else{
		for(var i=0; i<grids.bitacoras.columnas.length; i++){
			if(i!=3 && i !=7){
				createChild(grids.gridSeleccionado + 'thBit' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thBitacora', grids.bitacoras.columnas[i].descripcion);
			}
		}
	}
	switch(grids.gridSeleccionado){
		case grids.sistemas.lista[0].modulo[infoPortal.modulo].descripcion:
			construirColumnasEmision(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[1].modulo[infoPortal.modulo].descripcion:
			construirColumnasSiniestros(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[2].modulo[infoPortal.modulo].descripcion:
			construirColumnasPrevenciones(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[3].modulo[infoPortal.modulo].descripcion:
			construirColumnasPerdidasTotales(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[4].modulo[infoPortal.modulo].descripcion:
			construirColumnasRechazos(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[5].modulo[infoPortal.modulo].descripcion:
			construirColumnasSalvamento(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[6].modulo[infoPortal.modulo].descripcion:
			construirColumnasRobo(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[7].modulo[infoPortal.modulo].descripcion:
			construirColumnasRecuperacion(function(){
				callback();
			});
			break;
		case grids.sistemas.lista[8].modulo[infoPortal.modulo].descripcion:
			construirColumnasValuacion(function(){
				callback();
			});
			break;
	}
}

function construirColumnasEmision(callback){
	for(var i=0; i<grids.sistemas.lista[0].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[0].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.emisionArr.length; i++){
		if(	resultadosJSON.emisionArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.emisionArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.emisionArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.emisionArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.emisionArr[i].sapAmisBitacoras.observaciones);
					break;
				case 2:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusDatosPendientes.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.emisionArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE EMISION
			if(infoPortal.modulo == 1){
				createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].noPoliza==null)||(resultadosJSON.emisionArr[i].noPoliza=='-')||(resultadosJSON.emisionArr[i].noPoliza=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].noPoliza);
				createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].inciso==null)||(resultadosJSON.emisionArr[i].inciso=='-')||(resultadosJSON.emisionArr[i].inciso=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].inciso);
				createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].fechaVigenciaInicio==null)||(resultadosJSON.emisionArr[i].fechaVigenciaInicio=='-')||(resultadosJSON.emisionArr[i].fechaVigenciaInicio=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].fechaVigenciaInicio));
				createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].fechaVigenciaFin==null)||(resultadosJSON.emisionArr[i].fechaVigenciaFin=='-')||(resultadosJSON.emisionArr[i].fechaVigenciaFin=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].fechaVigenciaFin));
				createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].noSerie==null)||(resultadosJSON.emisionArr[i].noSerie=='-')||(resultadosJSON.emisionArr[i].noSerie=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].noSerie);
				createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].vehiculo.catMarcaVehiculo.descCatMarcaVehiculo==null)||(resultadosJSON.emisionArr[i].vehiculo.catMarcaVehiculo.descCatMarcaVehiculo=='-')||(resultadosJSON.emisionArr[i].vehiculo.catMarcaVehiculo.descCatMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].vehiculo.catMarcaVehiculo.descCatMarcaVehiculo);
				createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].vehiculo.descCatSubMarcaVehiculo==null)||(resultadosJSON.emisionArr[i].vehiculo.descCatSubMarcaVehiculo=='-')||(resultadosJSON.emisionArr[i].vehiculo.descCatSubMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].vehiculo.descCatSubMarcaVehiculo);
				createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].vehiculo.catTipoTransporte.descCatTipoTransporte==null)||(resultadosJSON.emisionArr[i].vehiculo.catTipoTransporte.descCatTipoTransporte=='-')||(resultadosJSON.emisionArr[i].vehiculo.catTipoTransporte.descCatTipoTransporte=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].vehiculo.catTipoTransporte.descCatTipoTransporte);
				createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].modelo==null)||(resultadosJSON.emisionArr[i].modelo=='-')||(resultadosJSON.emisionArr[i].modelo=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].modelo);
				createChild(grids.gridSeleccionado + 'td' + i + '_16', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].tipoPersona.descCatTipoPersona==null)||(resultadosJSON.emisionArr[i].tipoPersona.descCatTipoPersona=='-')||(resultadosJSON.emisionArr[i].tipoPersona.descCatTipoPersona=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].tipoPersona.descCatTipoPersona);
				createChild(grids.gridSeleccionado + 'td' + i + '_17', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catTipoServicio.descCatTipoServicio==null)||(resultadosJSON.emisionArr[i].catTipoServicio.descCatTipoServicio=='-')||(resultadosJSON.emisionArr[i].catTipoServicio.descCatTipoServicio=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catTipoServicio.descCatTipoServicio);
				createChild(grids.gridSeleccionado + 'td' + i + '_18', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente1ApPaterno==null)||(resultadosJSON.emisionArr[i].cliente1ApPaterno=='-')||(resultadosJSON.emisionArr[i].cliente1ApPaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente1ApPaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_19', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente1ApMaterno==null)||(resultadosJSON.emisionArr[i].cliente1ApMaterno=='-')||(resultadosJSON.emisionArr[i].cliente1ApMaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente1ApMaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_20', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente1Nombre==null)||(resultadosJSON.emisionArr[i].cliente1Nombre=='-')||(resultadosJSON.emisionArr[i].cliente1Nombre=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente1Nombre);
				createChild(grids.gridSeleccionado + 'td' + i + '_21', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente1RFC==null)||(resultadosJSON.emisionArr[i].cliente1RFC=='-')||(resultadosJSON.emisionArr[i].cliente1RFC=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente1RFC);
				createChild(grids.gridSeleccionado + 'td' + i + '_22', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente1CURP==null)||(resultadosJSON.emisionArr[i].cliente1CURP=='-')||(resultadosJSON.emisionArr[i].cliente1CURP=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente1CURP);
				createChild(grids.gridSeleccionado + 'td' + i + '_23', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente2ApPaterno==null)||(resultadosJSON.emisionArr[i].cliente2ApPaterno=='-')||(resultadosJSON.emisionArr[i].cliente2ApPaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente2ApPaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_24', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente2ApMaterno==null)||(resultadosJSON.emisionArr[i].cliente2ApMaterno=='-')||(resultadosJSON.emisionArr[i].cliente2ApMaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente2ApMaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_25', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].cliente2Nombre==null)||(resultadosJSON.emisionArr[i].cliente2Nombre=='-')||(resultadosJSON.emisionArr[i].cliente2Nombre=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].cliente2Nombre);
				createChild(grids.gridSeleccionado + 'td' + i + '_26', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioTipoPersona.descCatTipoPersona==null)||(resultadosJSON.emisionArr[i].beneficiarioTipoPersona.descCatTipoPersona=='-')||(resultadosJSON.emisionArr[i].beneficiarioTipoPersona.descCatTipoPersona=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioTipoPersona.descCatTipoPersona);
				createChild(grids.gridSeleccionado + 'td' + i + '_27', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioApPaterno==null)||(resultadosJSON.emisionArr[i].beneficiarioApPaterno=='-')||(resultadosJSON.emisionArr[i].beneficiarioApPaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioApPaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_28', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioApMaterno==null)||(resultadosJSON.emisionArr[i].beneficiarioApMaterno=='-')||(resultadosJSON.emisionArr[i].beneficiarioApMaterno=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioApMaterno);
				createChild(grids.gridSeleccionado + 'td' + i + '_29', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioNombre==null)||(resultadosJSON.emisionArr[i].beneficiarioNombre=='-')||(resultadosJSON.emisionArr[i].beneficiarioNombre=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioNombre);
				createChild(grids.gridSeleccionado + 'td' + i + '_31', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioRFC==null)||(resultadosJSON.emisionArr[i].beneficiarioRFC=='-')||(resultadosJSON.emisionArr[i].beneficiarioRFC=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioRFC);
				createChild(grids.gridSeleccionado + 'td' + i + '_32', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].beneficiarioCURP==null)||(resultadosJSON.emisionArr[i].beneficiarioCURP=='-')||(resultadosJSON.emisionArr[i].beneficiarioCURP=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].beneficiarioCURP);
				createChild(grids.gridSeleccionado + 'td' + i + '_33', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].canalVentas.descCatCanalVentas==null)||(resultadosJSON.emisionArr[i].canalVentas.descCatCanalVentas=='-')||(resultadosJSON.emisionArr[i].canalVentas.descCatCanalVentas=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].canalVentas.descCatCanalVentas);
				createChild(grids.gridSeleccionado + 'td' + i + '_34', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].agente==null)||(resultadosJSON.emisionArr[i].agente=='-')||(resultadosJSON.emisionArr[i].agente=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].agente);
				createChild(grids.gridSeleccionado + 'td' + i + '_36', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].fechaEmision==null)||(resultadosJSON.emisionArr[i].fechaEmision=='-')||(resultadosJSON.emisionArr[i].fechaEmision=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.emisionArr[i].fechaEmision));
				createChild(grids.gridSeleccionado + 'td' + i + '_37', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionAsegurado.descCatUbicacionMunicipio==null)||(resultadosJSON.emisionArr[i].ubicacionAsegurado.descCatUbicacionMunicipio=='-')||(resultadosJSON.emisionArr[i].ubicacionAsegurado.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionAsegurado.descCatUbicacionMunicipio);
				createChild(grids.gridSeleccionado + 'td' + i + '_38', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.descCatUbicacionEstado);
				createChild(grids.gridSeleccionado + 'td' + i + '_39', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais==null)||(resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='-')||(resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionAsegurado.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais);
				createChild(grids.gridSeleccionado + 'td' + i + '_41', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionEmision.descCatUbicacionMunicipio==null)||(resultadosJSON.emisionArr[i].ubicacionEmision.descCatUbicacionMunicipio=='-')||(resultadosJSON.emisionArr[i].ubicacionEmision.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionEmision.descCatUbicacionMunicipio);
				createChild(grids.gridSeleccionado + 'td' + i + '_42', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.descCatUbicacionEstado);
				createChild(grids.gridSeleccionado + 'td' + i + '_43', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais==null)||(resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='-')||(resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].ubicacionEmision.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais);
			}else if (infoPortal.modulo == 2){
				createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].repuveNivNci.niv==null)||(resultadosJSON.emisionArr[i].repuveNivNci.niv=='-')||(resultadosJSON.emisionArr[i].repuveNivNci.niv=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].repuveNivNci.niv);
				createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].repuveNivNci.nci==null)||(resultadosJSON.emisionArr[i].repuveNivNci.nci=='-')||(resultadosJSON.emisionArr[i].repuveNivNci.nci=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].repuveNivNci.nci);
				createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].numeroPoliza==null)||(resultadosJSON.emisionArr[i].numeroPoliza=='-')||(resultadosJSON.emisionArr[i].numeroPoliza=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].numeroPoliza);
				createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catTerminosCobertura.descripcion==null)||(resultadosJSON.emisionArr[i].catTerminosCobertura.descripcion=='-')||(resultadosJSON.emisionArr[i].catTerminosCobertura.descripcion=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catTerminosCobertura.descripcion);
				createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.emisionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.emisionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado);
				createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio==null)||(resultadosJSON.emisionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='-')||(resultadosJSON.emisionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio);
				createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catTipoPersona.descCatTipoPersona==null)||(resultadosJSON.emisionArr[i].catTipoPersona.descCatTipoPersona=='-')||(resultadosJSON.emisionArr[i].catTipoPersona.descCatTipoPersona=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catTipoPersona.descCatTipoPersona);
				createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].titular==null)||(resultadosJSON.emisionArr[i].titular=='-')||(resultadosJSON.emisionArr[i].titular=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].titular);
				createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].fechaVigenciaInicio==null)||(resultadosJSON.emisionArr[i].fechaVigenciaInicio=='-')||(resultadosJSON.emisionArr[i].fechaVigenciaInicio=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].fechaVigenciaInicio);
				createChild(grids.gridSeleccionado + 'td' + i + '_16', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].fechaVigenciaFin==null)||(resultadosJSON.emisionArr[i].fechaVigenciaFin=='-')||(resultadosJSON.emisionArr[i].fechaVigenciaFin=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].fechaVigenciaFin);
				createChild(grids.gridSeleccionado + 'td' + i + '_17', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.emisionArr[i].catMotivoCancelacion==null)||(resultadosJSON.emisionArr[i].catMotivoCancelacion.descripcion=='-')||(resultadosJSON.emisionArr[i].catMotivoCancelacion.descripcion=='')) ? ' tdVacio' : ''), resultadosJSON.emisionArr[i].catMotivoCancelacion==null?'':resultadosJSON.emisionArr[i].catMotivoCancelacion.descripcion);
			}
		}
		if(i+1 == resultadosJSON.emisionArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasSiniestros(callback){
	for(var i=0; i<grids.sistemas.lista[1].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[1].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.siniestrosArr.length; i++){
		if(	resultadosJSON.siniestrosArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.siniestrosArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.siniestrosArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.siniestrosArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.siniestrosArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.siniestrosArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.siniestrosArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.siniestrosArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE SINIESTROS
			createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].numSiniestro==null)||(resultadosJSON.siniestrosArr[i].numSiniestro=='-')||(resultadosJSON.siniestrosArr[i].numSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].numSiniestro);
			createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].poliza==null)||(resultadosJSON.siniestrosArr[i].poliza=='-')||(resultadosJSON.siniestrosArr[i].poliza=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].poliza);
			createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].inciso==null)||(resultadosJSON.siniestrosArr[i].inciso=='-')||(resultadosJSON.siniestrosArr[i].inciso=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].inciso);
			createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].fechaSiniestro==null)||(resultadosJSON.siniestrosArr[i].fechaSiniestro=='-')||(resultadosJSON.siniestrosArr[i].fechaSiniestro=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.siniestrosArr[i].fechaSiniestro));
			createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].serie==null)||(resultadosJSON.siniestrosArr[i].serie=='-')||(resultadosJSON.siniestrosArr[i].serie=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].serie);
			createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].causaSiniestro.descCatCausaSiniestro==null)||(resultadosJSON.siniestrosArr[i].causaSiniestro.descCatCausaSiniestro=='-')||(resultadosJSON.siniestrosArr[i].causaSiniestro.descCatCausaSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].causaSiniestro.descCatCausaSiniestro);
			createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].monto==null)||(resultadosJSON.siniestrosArr[i].monto=='-')||(resultadosJSON.siniestrosArr[i].monto=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].monto.formatMoney());
			createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais==null)||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='-')||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.catUbicacionPais.descCatUbicacionPais);
			createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].ubicacionSiniestro.catUbicacionEstado.descCatUbicacionEstado);
			createChild(grids.gridSeleccionado + 'td' + i + '_16', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].ubicacionSiniestro.descCatUbicacionMunicipio==null)||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.descCatUbicacionMunicipio=='-')||(resultadosJSON.siniestrosArr[i].ubicacionSiniestro.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].ubicacionSiniestro.descCatUbicacionMunicipio);
			createChild(grids.gridSeleccionado + 'td' + i + '_17', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorApPaterno==null)||(resultadosJSON.siniestrosArr[i].conductorApPaterno=='-')||(resultadosJSON.siniestrosArr[i].conductorApPaterno=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorApPaterno);
			createChild(grids.gridSeleccionado + 'td' + i + '_18', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorApMaterno==null)||(resultadosJSON.siniestrosArr[i].conductorApMaterno=='-')||(resultadosJSON.siniestrosArr[i].conductorApMaterno=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorApMaterno);
			createChild(grids.gridSeleccionado + 'td' + i + '_19', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorNombre==null)||(resultadosJSON.siniestrosArr[i].conductorNombre=='-')||(resultadosJSON.siniestrosArr[i].conductorNombre=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorNombre);
			createChild(grids.gridSeleccionado + 'td' + i + '_20', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorRfc==null)||(resultadosJSON.siniestrosArr[i].conductorRfc=='-')||(resultadosJSON.siniestrosArr[i].conductorRfc=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorRfc);
			createChild(grids.gridSeleccionado + 'td' + i + '_21', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorCurp==null)||(resultadosJSON.siniestrosArr[i].conductorCurp=='-')||(resultadosJSON.siniestrosArr[i].conductorCurp=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorCurp);
			createChild(grids.gridSeleccionado + 'td' + i + '_22', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].conductorTelefono==null)||(resultadosJSON.siniestrosArr[i].conductorTelefono=='-')||(resultadosJSON.siniestrosArr[i].conductorTelefono=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].conductorTelefono);
			createChild(grids.gridSeleccionado + 'td' + i + '_23', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.siniestrosArr[i].involucradoSiniestro.descCatTipoAfectado==null)||(resultadosJSON.siniestrosArr[i].involucradoSiniestro.descCatTipoAfectado=='-')||(resultadosJSON.siniestrosArr[i].involucradoSiniestro.descCatTipoAfectado=='')) ? ' tdVacio' : ''), resultadosJSON.siniestrosArr[i].involucradoSiniestro.descCatTipoAfectado);
		}
		if(i+1 == resultadosJSON.siniestrosArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasPrevenciones(callback){
	for(var i=0; i<grids.sistemas.lista[2].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[2].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.prevencionesArr.length; i++){
		if(	resultadosJSON.prevencionesArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.prevencionesArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.prevencionesArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.prevencionesArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.prevencionesArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.prevencionesArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.prevencionesArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.prevencionesArr[i].sapAmisBitacoras.observaciones);
					break;
				case 2:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusDatosPendientes.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.prevencionesArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE SINIESTROS
			createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.prevencionesArr[i].numSerie==null)||(resultadosJSON.prevencionesArr[i].numSerie=='-')||(resultadosJSON.prevencionesArr[i].numSerie=='')) ? ' tdVacio' : ''), resultadosJSON.prevencionesArr[i].numSerie);
			createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.prevencionesArr[i].causaPrevencion.descCatCausaPrevencion==null)||(resultadosJSON.prevencionesArr[i].causaPrevencion.descCatCausaPrevencion=='-')||(resultadosJSON.prevencionesArr[i].causaPrevencion.descCatCausaPrevencion=='')) ? ' tdVacio' : ''), resultadosJSON.prevencionesArr[i].causaPrevencion.descCatCausaPrevencion);
			createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.prevencionesArr[i].placa==null)||(resultadosJSON.prevencionesArr[i].placa=='-')||(resultadosJSON.prevencionesArr[i].placa=='')) ? ' tdVacio' : ''), resultadosJSON.prevencionesArr[i].placa);
			createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.prevencionesArr[i].observaciones==null)||(resultadosJSON.prevencionesArr[i].observaciones=='-')||(resultadosJSON.prevencionesArr[i].observaciones=='')) ? ' tdVacio' : ''), resultadosJSON.prevencionesArr[i].observaciones);
		}
		if(i+1 == resultadosJSON.prevencionesArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasPerdidasTotales(callback){
	for(var i=0; i<grids.sistemas.lista[3].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[3].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.perdidasTotalesArr.length; i++){
		if(	resultadosJSON.perdidasTotalesArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.perdidasTotalesArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE PT
			if(infoPortal.modulo == 1){
				createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].noSerie==null)||(resultadosJSON.perdidasTotalesArr[i].noSerie=='-')||(resultadosJSON.perdidasTotalesArr[i].noSerie=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].noSerie);
				createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].noSiniestro==null)||(resultadosJSON.perdidasTotalesArr[i].noSiniestro=='-')||(resultadosJSON.perdidasTotalesArr[i].noSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].noSiniestro);
				createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].fechaSiniestro==null)||(resultadosJSON.perdidasTotalesArr[i].fechaSiniestro=='-')||(resultadosJSON.perdidasTotalesArr[i].fechaSiniestro=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.perdidasTotalesArr[i].fechaSiniestro));
				createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].porcentajePerdida==null)||(resultadosJSON.perdidasTotalesArr[i].porcentajePerdida=='-')||(resultadosJSON.perdidasTotalesArr[i].porcentajePerdida=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].porcentajePerdida + '%');
				createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catTipoAfectado.descCatTipoAfectado==null)||(resultadosJSON.perdidasTotalesArr[i].catTipoAfectado.descCatTipoAfectado=='-')||(resultadosJSON.perdidasTotalesArr[i].catTipoAfectado.descCatTipoAfectado=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catTipoAfectado.descCatTipoAfectado);
				createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catCausaSiniestro.descCatCausaSiniestro==null)||(resultadosJSON.perdidasTotalesArr[i].catCausaSiniestro.descCatCausaSiniestro=='-')||(resultadosJSON.perdidasTotalesArr[i].catCausaSiniestro.descCatCausaSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catCausaSiniestro.descCatCausaSiniestro);
				createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].noMotor==null)||(resultadosJSON.perdidasTotalesArr[i].noMotor=='-')||(resultadosJSON.perdidasTotalesArr[i].noMotor=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].noMotor);
				createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catTipoServicio.descCatTipoServicio==null)||(resultadosJSON.perdidasTotalesArr[i].catTipoServicio.descCatTipoServicio=='-')||(resultadosJSON.perdidasTotalesArr[i].catTipoServicio.descCatTipoServicio=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catTipoServicio.descCatTipoServicio);
				createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catDano.descCatDano==null)||(resultadosJSON.perdidasTotalesArr[i].catDano.descCatDano=='-')||(resultadosJSON.perdidasTotalesArr[i].catDano.descCatDano=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catDano.descCatDano);
				createChild(grids.gridSeleccionado + 'td' + i + '_16', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catTipoMonto.descCatTipoMonto==null)||(resultadosJSON.perdidasTotalesArr[i].catTipoMonto.descCatTipoMonto=='-')||(resultadosJSON.perdidasTotalesArr[i].catTipoMonto.descCatTipoMonto=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catTipoMonto.descCatTipoMonto);
				createChild(grids.gridSeleccionado + 'td' + i + '_17', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].montoPagado==null)||(resultadosJSON.perdidasTotalesArr[i].montoPagado=='-')||(resultadosJSON.perdidasTotalesArr[i].montoPagado=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].montoPagado);
				createChild(grids.gridSeleccionado + 'td' + i + '_18', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catTipoPago.descCatTipoPago==null)||(resultadosJSON.perdidasTotalesArr[i].catTipoPago.descCatTipoPago=='-')||(resultadosJSON.perdidasTotalesArr[i].catTipoPago.descCatTipoPago=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catTipoPago.descCatTipoPago);
				createChild(grids.gridSeleccionado + 'td' + i + '_19', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].fechaPago==null)||(resultadosJSON.perdidasTotalesArr[i].fechaPago=='-')||(resultadosJSON.perdidasTotalesArr[i].fechaPago=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.perdidasTotalesArr[i].fechaPago));
				createChild(grids.gridSeleccionado + 'td' + i + '_20', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catEstVehPago.descCatEstVehPago==null)||(resultadosJSON.perdidasTotalesArr[i].catEstVehPago.descCatEstVehPago=='-')||(resultadosJSON.perdidasTotalesArr[i].catEstVehPago.descCatEstVehPago=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catEstVehPago.descCatEstVehPago);
				createChild(grids.gridSeleccionado + 'td' + i + '_21', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catTipoTransporte.descCatTipoTransporte==null)||(resultadosJSON.perdidasTotalesArr[i].catTipoTransporte.descCatTipoTransporte=='-')||(resultadosJSON.perdidasTotalesArr[i].catTipoTransporte.descCatTipoTransporte=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catTipoTransporte.descCatTipoTransporte);
				createChild(grids.gridSeleccionado + 'td' + i + '_22', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catMarcaVehiculo.descCatMarcaVehiculo==null)||(resultadosJSON.perdidasTotalesArr[i].catMarcaVehiculo.descCatMarcaVehiculo=='-')||(resultadosJSON.perdidasTotalesArr[i].catMarcaVehiculo.descCatMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catMarcaVehiculo.descCatMarcaVehiculo);
				createChild(grids.gridSeleccionado + 'td' + i + '_23', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo==null)||(resultadosJSON.perdidasTotalesArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo=='-')||(resultadosJSON.perdidasTotalesArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo);
				createChild(grids.gridSeleccionado + 'td' + i + '_24', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].anoModelo==null)||(resultadosJSON.perdidasTotalesArr[i].anoModelo=='-')||(resultadosJSON.perdidasTotalesArr[i].anoModelo=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].anoModelo);
				createChild(grids.gridSeleccionado + 'td' + i + '_25', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catCausaCancelacion.descCatCausaCancelacion==null)||(resultadosJSON.perdidasTotalesArr[i].catCausaCancelacion.descCatCausaCancelacion=='-')||(resultadosJSON.perdidasTotalesArr[i].catCausaCancelacion.descCatCausaCancelacion=='')) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].catCausaCancelacion.descCatCausaCancelacion);
			}else if(infoPortal.modulo == 2){
				createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].repuveNivNci==null)||(resultadosJSON.perdidasTotalesArr[i].repuveNivNci.niv==null)) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].repuveNivNci.niv);
				createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].repuveNivNci==null)||(resultadosJSON.perdidasTotalesArr[i].repuveNivNci.nci==null)) ? ' tdVacio' : ''), resultadosJSON.perdidasTotalesArr[i].repuveNivNci.nci);
				createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].numeroPoliza==null)||(resultadosJSON.perdidasTotalesArr[i].numeroPoliza==null))||(resultadosJSON.perdidasTotalesArr[i].numeroPoliza=='')), resultadosJSON.perdidasTotalesArr[i].numeroPoliza);
				createChild(grids.gridSeleccionado + 'td' + i + '_10' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado==null))||(resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='')), resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado);
				createChild(grids.gridSeleccionado + 'td' + i + '_11' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio==null)||(resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio==null))||(resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='')), resultadosJSON.perdidasTotalesArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio);
				createChild(grids.gridSeleccionado + 'td' + i + '_12' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].fechaPTT==null)||(resultadosJSON.perdidasTotalesArr[i].fechaPTT==null))||(resultadosJSON.perdidasTotalesArr[i].fechaPTT=='')), resultadosJSON.perdidasTotalesArr[i].fechaPTT);
				createChild(grids.gridSeleccionado + 'td' + i + '_13' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].numeroAveriguacionPrevia==null)||(resultadosJSON.perdidasTotalesArr[i].numeroAveriguacionPrevia==null))||(resultadosJSON.perdidasTotalesArr[i].numeroAveriguacionPrevia=='')), resultadosJSON.perdidasTotalesArr[i].numeroAveriguacionPrevia);
				createChild(grids.gridSeleccionado + 'td' + i + '_14' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].fechaAveriguacionPrevia==null)||(resultadosJSON.perdidasTotalesArr[i].fechaAveriguacionPrevia==null))||(resultadosJSON.perdidasTotalesArr[i].fechaAveriguacionPrevia=='')), resultadosJSON.perdidasTotalesArr[i].fechaAveriguacionPrevia);
				createChild(grids.gridSeleccionado + 'td' + i + '_15' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].porcentajePerdida==null)||(resultadosJSON.perdidasTotalesArr[i].porcentajePerdida==null))||(resultadosJSON.perdidasTotalesArr[i].porcentajePerdida=='')), resultadosJSON.perdidasTotalesArr[i].porcentajePerdida);
				createChild(grids.gridSeleccionado + 'td' + i + '_16' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].catMotivoPTT.motivoPerdidaTotal==null)||(resultadosJSON.perdidasTotalesArr[i].catMotivoPTT.motivoPerdidaTotal==null))||(resultadosJSON.perdidasTotalesArr[i].catMotivoPTT.motivoPerdidaTotal=='')), resultadosJSON.perdidasTotalesArr[i].catMotivoPTT.motivoPerdidaTotal);
				createChild(grids.gridSeleccionado + 'td' + i + '_17' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.perdidasTotalesArr[i].numeroSiniestro==null)||(resultadosJSON.perdidasTotalesArr[i].numeroSiniestro==null))||(resultadosJSON.perdidasTotalesArr[i].numeroSiniestro=='')), resultadosJSON.perdidasTotalesArr[i].numeroSiniestro);
				
			}
		} callback();
		if(i+1 == resultadosJSON.perdidasTotalesArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasRechazos(callback){
	for(var i=0; i<grids.sistemas.lista[4].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[4].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.rechazosArr.length; i++){
		if(	resultadosJSON.rechazosArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.rechazosArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.rechazosArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.rechazosArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.rechazosArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.rechazosArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.rechazosArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.rechazosArr[i].sapAmisBitacoras.observaciones);
					break;
				case 2:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusDatosPendientes.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.rechazosArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE SINIESTROS
			createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].numeroSiniestro==null)||(resultadosJSON.rechazosArr[i].numeroSiniestro=='-')||(resultadosJSON.rechazosArr[i].numeroSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].numeroSiniestro);
			createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].catBeneficioRelacion.catTipoBeneficio.descCatTipoBeneficio==null)||(resultadosJSON.rechazosArr[i].catBeneficioRelacion.catTipoBeneficio.descCatTipoBeneficio=='0')||(resultadosJSON.rechazosArr[i].catBeneficioRelacion.catTipoBeneficio.descCatTipoBeneficio=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].catBeneficioRelacion.catTipoBeneficio.descCatTipoBeneficio=='0'?'':resultadosJSON.rechazosArr[i].catBeneficioRelacion.catTipoBeneficio.descCatTipoBeneficio);
			createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].catBeneficioRelacion.catCausaBeneficio.descCatCausaBeneficio==null)||(resultadosJSON.rechazosArr[i].catBeneficioRelacion.catCausaBeneficio.descCatCausaBeneficio=='0')||(resultadosJSON.rechazosArr[i].catBeneficioRelacion.catCausaBeneficio.descCatCausaBeneficio=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].catBeneficioRelacion.catCausaBeneficio.descCatCausaBeneficio=='0'?'':resultadosJSON.rechazosArr[i].catBeneficioRelacion.catCausaBeneficio.descCatCausaBeneficio);
			createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].desistimiento==null)||(resultadosJSON.rechazosArr[i].desistimiento=='-')||(resultadosJSON.rechazosArr[i].desistimiento=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].desistimiento);
			createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].montoRechazo==null)||(resultadosJSON.rechazosArr[i].montoRechazo=='-')||(resultadosJSON.rechazosArr[i].montoRechazo=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].montoRechazo);
			createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].detectadoSAP==null)||(resultadosJSON.rechazosArr[i].detectadoSAP=='-')||(resultadosJSON.rechazosArr[i].detectadoSAP=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].detectadoSAP=='S'?'Si':'No');
			createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].observaciones==null)||(resultadosJSON.rechazosArr[i].observaciones=='-')||(resultadosJSON.rechazosArr[i].observaciones=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].observaciones);
			createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].catAlertasRechazos.descCatAlertasRechazos==null)||(resultadosJSON.rechazosArr[i].catAlertasRechazos.descCatAlertasRechazos=='0')||(resultadosJSON.rechazosArr[i].catAlertasRechazos.descCatAlertasRechazos=='')) ? ' tdVacio' : ''), resultadosJSON.rechazosArr[i].catAlertasRechazos.descCatAlertasRechazos=='0'?'':resultadosJSON.rechazosArr[i].catAlertasRechazos.descCatAlertasRechazos);
			createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.rechazosArr[i].fecha==null)||(resultadosJSON.rechazosArr[i].fecha=='-')||(resultadosJSON.rechazosArr[i].fecha=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.rechazosArr[i].fecha));
		}
		if(i+1 == resultadosJSON.rechazosArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasSalvamento(callback){
	for(var i=0; i<grids.sistemas.lista[5].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[5].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.salvamentosArr.length; i++){
		if(	resultadosJSON.salvamentosArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.salvamentosArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.salvamentosArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.salvamentosArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.salvamentosArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.salvamentosArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.salvamentosArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.salvamentosArr[i].sapAmisBitacoras.observaciones);
					break;
				case 2:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusDatosPendientes.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.salvamentosArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE SINIESTROS
			createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].noSerie==null)||(resultadosJSON.salvamentosArr[i].noSerie=='-')||(resultadosJSON.salvamentosArr[i].noSerie=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].noSerie);
			createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].noSiniestro==null)||(resultadosJSON.salvamentosArr[i].noSiniestro=='-')||(resultadosJSON.salvamentosArr[i].noSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].noSiniestro);
			createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catTipoAfectado.descCatTipoAfectado==null)||(resultadosJSON.salvamentosArr[i].catTipoAfectado.descCatTipoAfectado=='-')||(resultadosJSON.salvamentosArr[i].catTipoAfectado.descCatTipoAfectado=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catTipoAfectado.descCatTipoAfectado);
			createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catTipoVenta.descCatTipoVenta==null)||(resultadosJSON.salvamentosArr[i].catTipoVenta.descCatTipoVenta=='-')||(resultadosJSON.salvamentosArr[i].catTipoVenta.descCatTipoVenta=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catTipoVenta.descCatTipoVenta);
			createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].importeVenta==null)||(resultadosJSON.salvamentosArr[i].importeVenta=='-')||(resultadosJSON.salvamentosArr[i].importeVenta=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].importeVenta==0?'':resultadosJSON.salvamentosArr[i].importeVenta);
			createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].fechaVenta==null)||(resultadosJSON.salvamentosArr[i].fechaVenta=='-')||(resultadosJSON.salvamentosArr[i].fechaVenta=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.salvamentosArr[i].fechaVenta));
			createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].noFactura==null)||(resultadosJSON.salvamentosArr[i].noFactura=='-')||(resultadosJSON.salvamentosArr[i].noFactura=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].noFactura);
			createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].rfcFactura==null)||(resultadosJSON.salvamentosArr[i].rfcFactura=='-')||(resultadosJSON.salvamentosArr[i].rfcFactura=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].rfcFactura);
			createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catTipoPersona.descCatTipoPersona==null)||(resultadosJSON.salvamentosArr[i].catTipoPersona.descCatTipoPersona=='0')||(resultadosJSON.salvamentosArr[i].catTipoPersona.descCatTipoPersona=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catTipoPersona.descCatTipoPersona==0?'':resultadosJSON.salvamentosArr[i].catTipoPersona.descCatTipoPersona);
			createChild(grids.gridSeleccionado + 'td' + i + '_16', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catEstVehPago.descCatEstVehPago==null)||(resultadosJSON.salvamentosArr[i].catEstVehPago.descCatEstVehPago=='-')||(resultadosJSON.salvamentosArr[i].catEstVehPago.descCatEstVehPago=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catEstVehPago.descCatEstVehPago);
			createChild(grids.gridSeleccionado + 'td' + i + '_18', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catTipoTransporte.descCatTipoTransporte==null)||(resultadosJSON.salvamentosArr[i].catTipoTransporte.descCatTipoTransporte=='-')||(resultadosJSON.salvamentosArr[i].catTipoTransporte.descCatTipoTransporte=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catTipoTransporte.descCatTipoTransporte);
			createChild(grids.gridSeleccionado + 'td' + i + '_19', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catMarcaVehiculo.descCatMarcaVehiculo==null)||(resultadosJSON.salvamentosArr[i].catMarcaVehiculo.descCatMarcaVehiculo=='-')||(resultadosJSON.salvamentosArr[i].catMarcaVehiculo.descCatMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catMarcaVehiculo.descCatMarcaVehiculo);
			createChild(grids.gridSeleccionado + 'td' + i + '_20', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo==null)||(resultadosJSON.salvamentosArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo=='-')||(resultadosJSON.salvamentosArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].catSubMarcaVehiculo.descCatSubMarcaVehiculo);
			createChild(grids.gridSeleccionado + 'td' + i + '_21', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.salvamentosArr[i].anoModelo==null)||(resultadosJSON.salvamentosArr[i].anoModelo=='-')||(resultadosJSON.salvamentosArr[i].anoModelo=='')) ? ' tdVacio' : ''), resultadosJSON.salvamentosArr[i].anoModelo);
		}
		if(i+1 == resultadosJSON.salvamentosArr.length){
			paginar();
			callback();
		}
	}
}


function construirColumnasRobo(callback){
	for(var i=0; i<grids.sistemas.lista[6].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[6].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.roboArr.length; i++){
		if(	resultadosJSON.roboArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.roboArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.roboArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.roboArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.roboArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.roboArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.roboArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.roboArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE ROBO
				createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].repuveNivNci.niv==null)||(resultadosJSON.roboArr[i].repuveNivNci.niv=='-')||(resultadosJSON.roboArr[i].repuveNivNci.niv=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].repuveNivNci.niv);
				createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].repuveNivNci.nci==null)||(resultadosJSON.roboArr[i].repuveNivNci.nci=='-')||(resultadosJSON.roboArr[i].repuveNivNci.nci=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].repuveNivNci.nci);
				createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].numeroPoliza==null)||(resultadosJSON.roboArr[i].numeroPoliza=='-')||(resultadosJSON.roboArr[i].numeroPoliza=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].numeroPoliza);
				createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.roboArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.roboArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado);
				createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio==null)||(resultadosJSON.roboArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='-')||(resultadosJSON.roboArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio);
				createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].fechaRobo==null)||(resultadosJSON.roboArr[i].fechaRobo=='-')||(resultadosJSON.roboArr[i].fechaRobo=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].fechaRobo);
				createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].numeroAveriguacionPrevia==null)||(resultadosJSON.roboArr[i].numeroAveriguacionPrevia=='-')||(resultadosJSON.roboArr[i].numeroAveriguacionPrevia=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].numeroAveriguacionPrevia);
				createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].fechaAveriguacionPrevia==null)||(resultadosJSON.roboArr[i].fechaAveriguacionPrevia=='-')||(resultadosJSON.roboArr[i].fechaAveriguacionPrevia=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].fechaAveriguacionPrevia);
				createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.roboArr[i].numeroSiniestro==null)||(resultadosJSON.roboArr[i].numeroSiniestro=='-')||(resultadosJSON.roboArr[i].numeroSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.roboArr[i].numeroSiniestro);
		}
		if(i+1 == resultadosJSON.roboArr.length){
			paginar();
			callback();
		}
	}
}

function construirColumnasRecuperacion(callback){
	for(var i=0; i<grids.sistemas.lista[7].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[7].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.recuperacionArr.length; i++){
		if(	resultadosJSON.recuperacionArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.recuperacionArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.recuperacionArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.recuperacionArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.recuperacionArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.recuperacionArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.recuperacionArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.recuperacionArr[i].sapAmisBitacoras.observaciones);
					break;
				}
				//DATOS DE RECUPERACION
					createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.niv==null)||(resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.niv=='-')||(resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.niv=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.niv);
					createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.nci==null)||(resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.nci=='-')||(resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.nci=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].repuveSisRobo.repuveNivNci.nci);
					createChild(grids.gridSeleccionado + 'td' + i + '_9' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroPoliza==null)||(resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroPoliza=='-')||(resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroPoliza=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroPoliza);
					createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado==null)||(resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='-')||(resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.catUbicacionEstado.descCatUbicacionEstado);
					createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio==null)||(resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='-')||(resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].catUbicacionMunicipio.descCatUbicacionMunicipio);
					createChild(grids.gridSeleccionado + 'td' + i + '_12', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].fechaRecuperacion==null)||(resultadosJSON.recuperacionArr[i].fechaRecuperacion=='-')||(resultadosJSON.recuperacionArr[i].fechaRecuperacion=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].fechaRecuperacion);
					createChild(grids.gridSeleccionado + 'td' + i + '_13', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroAveriguacionPrevia==null)||(resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroAveriguacionPrevia=='-')||(resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroAveriguacionPrevia=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].repuveSisRobo.numeroAveriguacionPrevia);
					createChild(grids.gridSeleccionado + 'td' + i + '_14', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].fechaAveriguacionPrevia==null)||(resultadosJSON.recuperacionArr[i].fechaAveriguacionPrevia=='-')||(resultadosJSON.recuperacionArr[i].fechaAveriguacionPrevia=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].fechaAveriguacionPrevia);
					createChild(grids.gridSeleccionado + 'td' + i + '_15', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.recuperacionArr[i].numeroSiniestro==null)||(resultadosJSON.recuperacionArr[i].numeroSiniestro=='-')||(resultadosJSON.recuperacionArr[i].numeroSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.recuperacionArr[i].numeroSiniestro);
			if(i+1 == resultadosJSON.recuperacionArr.length){
				paginar();
				callback();
			}
		}
	}
}

function construirColumnasValuacion(callback){
	for(var i=0; i<grids.sistemas.lista[8].modulo[infoPortal.modulo].columnas.length; i++){
		createChild(grids.gridSeleccionado + 'thSis' + i, 'th', grids.gridSeleccionado + 'theadtr', null, 'gridTh thSistema', grids.sistemas.lista[8].modulo[infoPortal.modulo].columnas[i].descripcion);
	}
	for(var i=0; i<resultadosJSON.valuacionArr.length; i++){
		if(	resultadosJSON.valuacionArr[i].id!=0){
			//DATOS DE BITACORAS
			createChild(grids.gridSeleccionado + 'tbodytr' + i, 'tr', grids.gridSeleccionado + 'tbody', null, 'gridTr');
			createChild(grids.gridSeleccionado + 'td' + i + '_1', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.valuacionArr[i].sapAmisBitacoras.fechaOperacion));
			createChild(grids.gridSeleccionado + 'td' + i + '_2', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.valuacionArr[i].sapAmisBitacoras.fechaRegistro));
			createChild(grids.gridSeleccionado + 'td' + i + '_3', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', obtenerFechaFormalJSON(resultadosJSON.valuacionArr[i].sapAmisBitacoras.fechaEnvio));
			createChild(grids.gridSeleccionado + 'td' + i + '_4', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora', resultadosJSON.valuacionArr[i].sapAmisBitacoras.catOperaciones.descCatOperaciones);
			switch (resultadosJSON.valuacionArr[i].sapAmisBitacoras.catEstatusBitacora.id) {
				case 0:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusOK.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', 'El proceso de envio al SAP-AMIS ha finalizado correctamente.');
					break;
				case 1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusPendiente.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.valuacionArr[i].sapAmisBitacoras.observaciones);
					break;
				case -1:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusError.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.valuacionArr[i].sapAmisBitacoras.observaciones);
					break;
				case 2:
					createChild(grids.gridSeleccionado + 'td' + i + '5', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora estatus', '<img src="/MidasWeb/img/sapamis/icons/estatusDatosPendientes.png"/>');
					createChild(grids.gridSeleccionado + 'td' + i + '6'	, 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdBitacora observaciones', resultadosJSON.valuacionArr[i].sapAmisBitacoras.observaciones);
					break;
			}
			//DATOS DE SINIESTROS
			createChild(grids.gridSeleccionado + 'td' + i + '_7' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].numeroSiniestro==null)||(resultadosJSON.valuacionArr[i].numeroSiniestro=='-')||(resultadosJSON.valuacionArr[i].numeroSiniestro=='')) ? ' tdVacio' : ''), resultadosJSON.valuacionArr[i].numeroSiniestro);
			createChild(grids.gridSeleccionado + 'td' + i + '_8' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].numeroValuacion==null)||(resultadosJSON.valuacionArr[i].numeroValuacion=='-')||(resultadosJSON.valuacionArr[i].numeroValuacion=='')) ? ' tdVacio' : ''), resultadosJSON.valuacionArr[i].numeroValuacion);
			createChild(grids.gridSeleccionado + 'td' + i + '_9', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].fechaValuacion==null)||(resultadosJSON.valuacionArr[i].fechaValuacion=='-')||(resultadosJSON.valuacionArr[i].fechaValuacion=='')) ? ' tdVacio' : ''), resultadosJSON.valuacionArr[i].fechaValuacion);
			createChild(grids.gridSeleccionado + 'td' + i + '_10', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].montoValuacion==null)||(resultadosJSON.valuacionArr[i].montoValuacion=='-')||(resultadosJSON.valuacionArr[i].montoValuacion=='')) ? ' tdVacio' : ''), resultadosJSON.valuacionArr[i].montoValuacion==0?'':resultadosJSON.valuacionArr[i].montoValuacion);
			createChild(grids.gridSeleccionado + 'td' + i + '_11', 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].fechaVenta==null)||(resultadosJSON.valuacionArr[i].fechaVenta=='-')||(resultadosJSON.valuacionArr[i].fechaVenta=='')) ? ' tdVacio' : ''), obtenerFechaFormalJSON(resultadosJSON.valuacionArr[i].fechaVenta));
			createChild(grids.gridSeleccionado + 'td' + i + '_12' , 'td', grids.gridSeleccionado + 'tbodytr' + i, null, 'gridTd tdSistema' + (((resultadosJSON.valuacionArr[i].repuveNivNci.niv==null)||(resultadosJSON.valuacionArr[i].repuveNivNci.niv=='-')||(resultadosJSON.valuacionArr[i].repuveNivNci.niv=='')) ? ' tdVacio' : ''), resultadosJSON.valuacionArr[i].repuveNivNci.niv);
			}
		if(i+1 == resultadosJSON.valuacionArr.length){
			paginar();
			callback();
		}
	}
}

function desplegarSinResultados(callback){
	createChild(grids.gridSeleccionado, 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null, 'sinResultados noSelect', 'No se encontraron resultados');
	callback();
}

function desplegarSeleccionarSistema(callback){
	createChild('seleccionarSistema', 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null, 'sinResultados noSelect', 'Es requerido seleccionar al menos un Sistema para comenzar la consulta');
	callback();
}