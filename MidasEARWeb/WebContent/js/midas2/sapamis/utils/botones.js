/************************************************************
 *	Nombre del Archivo: botones.js
 *
 *	Proposito: 	Accionar las funciones requeridas por los 
 *				Botones del Menu Principal.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function botonVisionGeneral(){
	banderas.menuSeleccionado = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].id;
	infoPortal.elementHTML.portal.subElements[3].subElements[0].title = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[0].title;
	if(estatusCarga){
		loadingData(1);
		inicializaContenido(function(){
			loadingData(0);
		});
	}
}

function botonConsultas(){
	banderas.menuSeleccionado = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[1].id;
	infoPortal.elementHTML.portal.subElements[3].subElements[0].title = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[1].title;
	if(estatusCarga){
		loadingData(1);
		inicializaContenido(function(){
			loadingData(0);
		});
	}
}

function botonNotificaciones(){
	banderas.menuSeleccionado = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[2].id
	infoPortal.elementHTML.portal.subElements[3].subElements[0].title = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[2].title;
	if(estatusCarga){
		loadingData(1);
		inicializaContenido(function(){
			loadingData(0);
		});
	}
}

function botonProcesosManuales(){
	banderas.menuSeleccionado = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[2].id
	infoPortal.elementHTML.portal.subElements[3].subElements[0].title = infoPortal.elementHTML.portal.subElements[2].menu.subMenu.lista[2].title;
	if(estatusCarga){
		loadingData(1);
		inicializaContenido(function(){
			loadingData(0);
		});
	}
}