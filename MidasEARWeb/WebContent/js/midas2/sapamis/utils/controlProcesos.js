/************************************************************
 *	Nombre del Archivo: controlProcesos.js
 *
 *	Proposito: 	Permitir Controlar los Procesos Manuales de 
 *				Extracción y Envío de los Modulos soportados 
 *				para el SAP-AMIS.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
 function inicializaPanelControlProcesos(callback){
	createChild(controlProcesos.id, 'div', infoPortal.elementHTML.portal.subElements[3].subElements[1].id, null,null, null, null);
	createChild(controlProcesos.paneles[0].id, 'div', controlProcesos.id, null, controlProcesos.paneles[0].class, null, null);
	createChild(controlProcesos.paneles[0].id + 'Title', 'div', controlProcesos.paneles[0].id, null, 'title', controlProcesos.paneles[0].title, null);
	createChild(controlProcesos.paneles[0].id + 'Contenido', 'div', controlProcesos.paneles[0].id, null, 'contenido', null, null);
	createChild(controlProcesos.paneles[0].id + 'Tabla', 'table', controlProcesos.paneles[0].id + 'Contenido', null, controlProcesos.tablasGenerales.classTable, null, null);
	createChild(controlProcesos.paneles[0].id + 'TrTitles', 'tr', controlProcesos.paneles[0].id + 'Tabla', null, controlProcesos.tablasGenerales.classTR, null, null);
	createChild(controlProcesos.paneles[0].id + 'TitleSistema', 'th', controlProcesos.paneles[0].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Sistemas', null);
	createChild(controlProcesos.paneles[0].id + 'TitleExtraccion', 'th', controlProcesos.paneles[0].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Proceso de Extracción', null);
	createChild(controlProcesos.paneles[0].id + 'TitleEnvio', 'th', controlProcesos.paneles[0].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Proceso de Envío', null);
	createChild(controlProcesos.paneles[1].id, 'div', controlProcesos.id, null, controlProcesos.paneles[1].class, null, null);
	createChild(controlProcesos.paneles[1].id + 'Title', 'div', controlProcesos.paneles[1].id, null, 'title', controlProcesos.paneles[1].title, null);
	createChild(controlProcesos.paneles[1].id + 'Contenido', 'div', controlProcesos.paneles[1].id, null, 'contenido', null, null);
	createChild(controlProcesos.paneles[1].id + 'Leyenda1', 'div', controlProcesos.paneles[1].id + 'Contenido', null, 'leyenda', '<p>Esta herramienta permite ejecutar de Manera Manual los procesos de Extracción de Datos y Envío de Datos al SAP-AMIS.</p>', null);
	createChild(controlProcesos.paneles[1].id + 'Leyenda2', 'div', controlProcesos.paneles[1].id + 'Contenido', null, 'leyenda', '<p>La ejecución de estos mismos permite procesar los datos por Dia, <b>Segun la fecha seleccionada en el Encabezado</b>.</p>', null);
	createChild(controlProcesos.paneles[1].id + 'Tabla', 'table', controlProcesos.paneles[1].id + 'Contenido', null, controlProcesos.tablasGenerales.classTable, null, null);
	createChild(controlProcesos.paneles[1].id + 'TrTitles', 'tr', controlProcesos.paneles[1].id + 'Tabla', null, controlProcesos.tablasGenerales.classTR, null, null);
	createChild(controlProcesos.paneles[1].id + 'TitleSistema', 'th', controlProcesos.paneles[1].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Sistemas', null);
	createChild(controlProcesos.paneles[1].id + 'TitleExtraccion', 'th', controlProcesos.paneles[1].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Proceso de Extracción', null);
	createChild(controlProcesos.paneles[1].id + 'TitleEnvio', 'th', controlProcesos.paneles[1].id + 'TrTitles', null, controlProcesos.tablasGenerales.classTH, 'Proceso de Envío', null);
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		if(catalogos.relSistemaOperacionArr[i].catOperaciones.id == 1){
			createChild('automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, 'tr', controlProcesos.paneles[0].id + 'Tabla', null, controlProcesos.tablasGenerales.classTR, null, null);
			createChild('automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col1', 'td', 'automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas, null);
			createChild('automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col2', 'td', 'automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, '<div class="botonSimple ' + (catalogos.relSistemaOperacionArr[i].catSistemas.estatusProcesoExtraccion==0?'botonEncendido">Encendido.':'botonApagado">Apagado.') + '</div>', function(){ejecutarProcesos(this.id, 1);});
			createChild('automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col3', 'td', 'automaticosRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, '<div class="botonSimple ' + (catalogos.relSistemaOperacionArr[i].catSistemas.estatusProcesoEnvio==0?'botonEncendido">Encendido.':'botonApagado"">Apagado.') + '</div>', function(){ejecutarProcesos(this.id, 2);});
			//createChild('ManulaesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, 'div', controlProcesos.paneles[1].id + 'Tabla', null, 'sistema', catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas, null);


			createChild('manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, 'tr', controlProcesos.paneles[1].id + 'Tabla', null, controlProcesos.tablasGenerales.classTR, null, null);
			createChild('manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col1', 'td', 'manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, catalogos.relSistemaOperacionArr[i].catSistemas.descCatSistemas, null);
			createChild('manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col2', 'td', 'manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, '<div class="botonSimple">Ejecutar.</div>', function(){ejecutarProcesos(this.id, 3);});
			createChild('manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id + 'Col3', 'td', 'manualesRow' + catalogos.relSistemaOperacionArr[i].catSistemas.id, null, controlProcesos.tablasGenerales.classTD, '<div class="botonSimple">Ejecutar.</div>', function(){ejecutarProcesos(this.id, 4);});
		}
	}
	callback();
}

function ejecutarProcesos(idDivSistema, tipoProcesos){
	/*Tipo Proceso 
		1 = Interruptor Extraccion
		2 = Interruptor Envio
		3 = Ejecutar Extraccion
		4 = Ejecutar Envio
	*/
	switch(tipoProcesos){
		case 1:
			switch(idDivSistema){
				case 'automaticosRow1Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 1;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Emisión.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow2Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 2;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Siniestros.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow4Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 4;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Prevenciones.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow5Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 5;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Perdidas Totales.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow8Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 8;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Rechazos.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow9Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 9;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Salvamento.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
				case 'automaticosRow10Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 10;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Extraccion del Modulo de Valuacion.', 
						function(){
							inicializaInterruptorExtraccion();
						});
					break;
			}
			break;
		case 2:
			switch(idDivSistema){
				case 'automaticosRow1Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 1;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Emision.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow2Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 2;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Siniestros.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow4Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 4;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Prevenciones.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow5Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 5;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Perdidas Totales.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow8Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 8;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Rechazos.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow9Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 9;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Salvamento.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
				case 'automaticosRow10Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 10;
					inicializaPopUpSimple(
						'Este proceso Encendera/Apagara la ejecución Automatica del Proceso de Envios del Modulo de Valuacion.', 
						function(){
							inicializaInterruptorEnvio();
						});
					break;
			}
			break;
		case 3:
			switch(idDivSistema){
				case 'manualesRow1Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 1;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Emisión del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow2Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 2;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Siniestros del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow4Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 4;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Prevenciones del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow5Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 5;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Perdidas Totales del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow8Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 8;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Rechazos del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow9Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 9;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Salvamento del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
				case 'manualesRow10Col2':
					controlProcesos.sistemaSeleccionadoProcesar = 10;
					inicializaPopUpSimple(
						'Este proceso Extraera la informacion del Modulo de Valuacion del dia ' 
						+ datosFechas.fechaSeleccionada.formal, 
						function(){
							inicializaProcesoExtraccion();
						});
					break;
			}
			break;
		case 4:
			switch(idDivSistema){
				case 'manualesRow1Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 1;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Emision del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});
					break;
				case 'manualesRow2Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 2;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Siniestros del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});
					break;
				case 'manualesRow4Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 4;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Prevenciones del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});
					break;
				case 'manualesRow5Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 5;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Perdidas Totales del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});
					break;
				case 'manualesRow8Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 8;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Rechazos del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});
					break;
				case 'manualesRow9Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 9;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Salvamento del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});			
					break;
				case 'manualesRow10Col3':
					controlProcesos.sistemaSeleccionadoProcesar = 10;
					inicializaPopUpSimple(
						'Este proceso Enviara la informacion del Modulo de Valuacion del dia ' 
						+ datosFechas.fechaSeleccionada.formal + ' al SAP-AMIS', 
						function(){
							inicializaProcesoEnvio();
					});			
					break;
			}
			break;
	}
}

function inicializaPopUpSimple(leyenda, functionAceptar){
	if(confirm(leyenda + '\n\n¿Esta seguro que desea continuar?\n')){
		functionAceptar();
	}
}

function inicializaProcesoExtraccion(){
	var path;
	var pathPFijo = '/MidasWeb/sapamis/procesos/stp/';
	var pathSFijo = 'Poblado.action?fechaInicio='+datosFechas.fechaSeleccionada.param+'&fechaFin='+datosFechas.fechaSeleccionada.param;
	switch(controlProcesos.sistemaSeleccionadoProcesar){
		case 1:
			path = pathPFijo + 'emision' + pathSFijo;
			break;
		case 2:
			path = pathPFijo + 'siniestros' + pathSFijo;
			break;
		case 4:
			path = pathPFijo + 'prevenciones' + pathSFijo;
			break;
		case 5:
			path = pathPFijo + 'ptt' + pathSFijo;
			break;
		case 8:
			path = pathPFijo + 'rechazos' + pathSFijo;
			break;
		case 9:
			path = pathPFijo + 'salvamento' + pathSFijo;
			break;
		case 10:
			path = pathPFijo + 'valuacion' + pathSFijo;
			break;	
	}
	if(controlProcesos.sistemaSeleccionadoProcesar != 4){
		$.ajax({
	        type: 'post',
		    url: path,
	        cache: false,
		    async: true,
		    success: function() {
	            alert('El proceso a finalizado correctamente.');
		    },
		    error:function () {
	            alert('Se ha producido un error en la ejecución del Proceso');
	        }
		});
	}
}

function inicializaProcesoEnvio(){
	var path;
	var pathPFijo = '/MidasWeb/sapamis/sistemas/';
	var pathSFijo = '/sendRegSapAmis.action?fechaInicio='+datosFechas.fechaSeleccionada.param+'&fechaFin='+datosFechas.fechaSeleccionada.param;
	switch(controlProcesos.sistemaSeleccionadoProcesar){
		case 1:
			path = pathPFijo + 'emision' + pathSFijo;
			break;
		case 2:
			path = pathPFijo + 'siniestros' + pathSFijo;
			break;
		case 4:
			path = pathPFijo + 'prevenciones' + pathSFijo;
			break;
		case 5:
			path = pathPFijo + 'ptt' + pathSFijo;
			break;
		case 8:
			path = pathPFijo + 'rechazos' + pathSFijo;
			break;
		case 9:
			path = pathPFijo + 'salvamento' + pathSFijo;
			break;
		case 10:
			path = pathPFijo + 'valuacion' + pathSFijo;
			break;
	}
	if(controlProcesos.sistemaSeleccionadoProcesar != 4){
		$.ajax({
	        type: 'post',
		    url: path,
	        cache: false,
		    async: true,
		    success: function() {
	            alert('El proceso a finalizado correctamente.');
		    },
		    error:function () {
	            alert('Se ha producido un error en la ejecución del Proceso');
	        }
		});
	}
}

function inicializaInterruptorExtraccion(){
	var catSistemasActualizar;
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		if(catalogos.relSistemaOperacionArr[i].catSistemas.id == controlProcesos.sistemaSeleccionadoProcesar){
			catSistemasActualizar = catalogos.relSistemaOperacionArr[i].catSistemas;
			if(catSistemasActualizar.estatusProcesoExtraccion == 1){
				catSistemasActualizar.estatusProcesoExtraccion = 0;
			}else{
				catSistemasActualizar.estatusProcesoExtraccion = 1;
			}
			i = catalogos.relSistemaOperacionArr.length;
		}
	}
	updateEstatusSistema(catSistemasActualizar, 
		function(){
			loadingData(1);
			inicializaContenido(function(){
				loadingData(0);
			});
		});
}

function inicializaInterruptorEnvio(){
	var catSistemasActualizar;
	for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
		if(catalogos.relSistemaOperacionArr[i].catSistemas.id == controlProcesos.sistemaSeleccionadoProcesar){
			catSistemasActualizar = catalogos.relSistemaOperacionArr[i].catSistemas;
			if(catSistemasActualizar.estatusProcesoEnvio == 1){
				catSistemasActualizar.estatusProcesoEnvio = 0;
			}else{
				catSistemasActualizar.estatusProcesoEnvio = 1;
			}
			i = catalogos.relSistemaOperacionArr.length;
		}
	}
	updateEstatusSistema(catSistemasActualizar, function(){
		loadingData(1);
		catalogos.relSistemaOperacionArr = null;
		findByStatusRelSistemaOperacion(function(){
			inicializaContenido(function(){
				loadingData(0);
			});
		});
	});
}