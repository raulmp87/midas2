/************************************************************
 *	Nombre del Archivo: Paginacion.js
 *
 *	Proposito: 	Construccion de paginación para los GRIDS Generados.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function paginar(){
	calcularNumeroPaginas(function(){
		createChild('panelPaginacionSpacing' + grids.gridSeleccionado, 'div', 'ecoContenedorContenidoCuerpo', null, 'panelPaginacionSpacing');
		createChild('panelPaginacion' + grids.gridSeleccionado, 'div', 'ecoContenedorContenidoCuerpo', null, 'gridPanelPagination');
		createChild('paginaNumContenedor' + grids.gridSeleccionado, 'div', 'panelPaginacion' + grids.gridSeleccionado, null, 'paginaNumContenedor');
		var htmlContent = '';
		htmlContent = htmlContent + crearHtmlBotonPagina('registrosPorPagina', 'Se muestran <b>' + paginacion.numRegXPagina + '</b> registros por pagina. ' , '');
		htmlContent = htmlContent + crearHtmlBotonPagina('totalResultados', 'Se han obtenido un Total de <b>' + calculos.totales + '</b> Resultados. ' , '');
		htmlContent = htmlContent + crearHtmlBotonPagina('flecha', '<', '');
		htmlContent = htmlContent + crearHtmlBotonPagina('paginarAnterior', 'Anterior', 'navPaginacion('+(paginacion.paginaSeleccionada-1)+')');
			
		if(paginacion.numeroPaginas >= 9){
			if(paginacion.paginaSeleccionada < 5){
				for(var i = 1; i <= 5; i++){
					if(i == paginacion.paginaSeleccionada){
			        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaSeleccionada', i, 'navPaginacion('+i+')');				
					}else{
			        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+i, i, 'navPaginacion('+i+')');
					}
				}
		        htmlContent = htmlContent + crearHtmlBotonPagina('paginarPuntos1', '...', '');
		        htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+paginacion.numeroPaginas, paginacion.numeroPaginas, 'navPaginacion('+paginacion.numeroPaginas+')');
			}else{
		        htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum1', '1', 'navPaginacion(1)');
		        htmlContent = htmlContent + crearHtmlBotonPagina('paginarPuntos1', '...', '');
				if(paginacion.paginaSeleccionada < paginacion.numeroPaginas-3){
					for(var i = paginacion.paginaSeleccionada-1; i <= paginacion.paginaSeleccionada+1; i++){
						if(i == paginacion.paginaSeleccionada){
				        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaSeleccionada', i, 'navPaginacion('+i+')');				
						}else{
				        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+i, i, 'navPaginacion('+i+')');
						}
					}
		        	htmlContent = htmlContent + crearHtmlBotonPagina('paginarPuntos1', '...', '');
		        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+paginacion.numeroPaginas, paginacion.numeroPaginas, 'navPaginacion('+paginacion.numeroPaginas+')');
				}else{
					for(var i = paginacion.numeroPaginas-4; i <= paginacion.numeroPaginas; i++){
						if(i == paginacion.paginaSeleccionada){
				        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaSeleccionada', i, 'navPaginacion('+i+')');				
						}else{
				        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+i, i, 'navPaginacion('+i+')');
						}
					}
				}
			}
		}else{
			for(var i = 1; i <= paginacion.numeroPaginas; i++){
				if(i == paginacion.paginaSeleccionada){
		        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaSeleccionada', i, 'navPaginacion('+i+')');
				}else{
		        	htmlContent = htmlContent + crearHtmlBotonPagina('paginaNum'+i, i, 'navPaginacion('+i+')');
				}
			}
		}
	    htmlContent = htmlContent + crearHtmlBotonPagina('paginarSiguiente', 'Siguiente', 'navPaginacion('+ (paginacion.paginaSeleccionada+1) +')');
	    htmlContent = htmlContent + crearHtmlBotonPagina('flecha2', '>', '');
		document.getElementById('paginaNumContenedor' + grids.gridSeleccionado).innerHTML = htmlContent;
		paginacion.paginaSeleccionada = 1;
	});
}

function calcularNumeroPaginas(callback){
	paginacion.numeroPaginas = 0;
	switch(filtrosSeleccionadosJSON.filtroSeleccionadoSistema){
		case 'FiltrosSistema1':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.emision.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.emision.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.emision.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.emision.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.emision.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.emision.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.emision.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.emision.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.emision.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.emision.alta.estatusDatosPendientes;
							break;
					}
					break;
				case 2:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.emision.modificacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.emision.modificacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.emision.modificacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.emision.modificacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.emision.modificacion.estatusDatosPendientes;
							break;
					}
					break;
				case 3:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.emision.cancelacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.emision.cancelacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.emision.cancelacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.emision.cancelacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.emision.cancelacion.estatusDatosPendientes;
							break;
					}
					break;
				case 4:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.emision.rehabilitacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.emision.rehabilitacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.emision.rehabilitacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.emision.rehabilitacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.emision.rehabilitacion.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
		case 'FiltrosSistema2':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusDatosPendientes;
							break;
					}
					break;
				case 2:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.modificacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.modificacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.modificacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.modificacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.modificacion.estatusDatosPendientes;
							break;
					}
					break;
				case 3:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.cancelacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.cancelacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.cancelacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.cancelacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.cancelacion.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
		case 'FiltrosSistema4':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.siniestros.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.siniestros.alta.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
		case 'FiltrosSistema5':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.perdidasTotales.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.perdidasTotales.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.perdidasTotales.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.perdidasTotales.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.perdidasTotales.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.perdidasTotales.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.perdidasTotales.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.perdidasTotales.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.perdidasTotales.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.perdidasTotales.alta.estatusDatosPendientes;
							break;
					}
					break;
				case 2:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.perdidasTotales.modificacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.perdidasTotales.modificacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.perdidasTotales.modificacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.perdidasTotales.modificacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.perdidasTotales.modificacion.estatusDatosPendientes;
							break;
					}
					break;
				case 3:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.perdidasTotales.cancelacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.perdidasTotales.cancelacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.perdidasTotales.cancelacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.perdidasTotales.cancelacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.perdidasTotales.cancelacion.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
		case 'FiltrosSistema8':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.rechazos.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.rechazos.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.rechazos.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.rechazos.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.rechazos.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.rechazos.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.rechazos.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.rechazos.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.rechazos.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.rechazos.alta.estatusDatosPendientes;
							break;
					}
					break;z
			}
			break;
		case 'FiltrosSistema9':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.salvamento.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.salvamento.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.salvamento.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.salvamento.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.salvamento.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.salvamento.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.salvamento.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.salvamento.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.salvamento.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.salvamento.alta.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
		case 'FiltrosSistema10':
			switch(filtrosConfig.operacion){
				case 'Total':
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.valuacion.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.valuacion.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.valuacion.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.valuacion.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.valuacion.estatusDatosPendientes;
							break;
					}
					break;
				case 1:
					switch(filtrosConfig.estatus){
						case 'Total':
							paginacion.registrosTotales = calculos.valuacion.alta.totales;
							break;
						case -1:
							paginacion.registrosTotales = calculos.valuacion.alta.estatusError;
							break;
						case 0:
							paginacion.registrosTotales = calculos.valuacion.alta.estatusEnviado;
							break;
						case 1:
							paginacion.registrosTotales = calculos.valuacion.alta.estatusPendiente;
							break;
						case 2:
							paginacion.registrosTotales = calculos.valuacion.alta.estatusDatosPendientes;
							break;
					}
					break;
			}
			break;
	}
	paginacion.numeroPaginas = Math.ceil(paginacion.registrosTotales/paginacion.numRegXPagina);
	callback();
}

function crearHtmlBotonPagina(id, contenido, onclick){
	return '<div id="'+id+'" class="paginaNum" onclick="'+onclick+'"><span>'+contenido+'</span></div>';
}

function navPaginacion(numPagina){
	if(numPagina > 0 && numPagina <= paginacion.numeroPaginas){
		paginacion.paginaSeleccionada = numPagina;
		if(estatusCarga){
			loadingData(1);
			paginacion.paginaSeleccionada = numPagina;
			inicializaContenido(function(){
				loadingData(0);
			});
		}
	}
}
