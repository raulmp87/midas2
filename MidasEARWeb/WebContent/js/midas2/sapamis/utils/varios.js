/************************************************************
 *	Nombre del Archivo: varios.js
 *
 *	Proposito: 	Varias Funciones Utiles.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
function createChild(id, type, father, by, classdiv, innerHTML, onClickFunction){
	iElement = document.createElement(type);
	iElement.id = id;
	if(classdiv != null){
		iElement.className = classdiv;
	}
	if(onClickFunction != null){
		iElement.onclick = onClickFunction;
	}
	if(innerHTML != null){
		iElement.innerHTML = innerHTML;
	}
	if(by == 'tagName'){
		document.getElementsByTagName(father)[0].appendChild(iElement);
	}else{
		document.getElementById(father).appendChild(iElement);
	}
}

function loadingData(bandera){
	if(bandera == 1){
		estatusCarga = false;
		createChild('fondoNegroLoading', 'div', 'ecoContenedorCentral', null, 'fondoNegro');
		createChild('loadingData', 'div', 'fondoNegroLoading', null, null);
		document.getElementById('loadingData').innerHTML = '<img src="/MidasWeb/img/bitacorassapamis/icons/loading.gif">';
	}
	if(bandera == 0){
		estatusCarga = true;
		$('#fondoNegroLoading').fadeOut(function() {
	       $('#fondoNegroLoading').remove();
	    });
	}
}

Date.prototype.addHours= function(h){
    var copiedDate = new Date(this.getTime());
    copiedDate.setHours(copiedDate.getHours()+h);
    return copiedDate;
}

Number.prototype.formatMoney = function(lugares, symbol, miles, decimal) {
	lugares = !isNaN(lugares = Math.abs(lugares)) ? lugares : 2;
	symbol = symbol !== undefined ? symbol : "$";
	miles = miles || ",";
	decimal = decimal || ".";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(lugares), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + miles : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + miles) + (lugares ? decimal + Math.abs(number - i).toFixed(lugares).slice(2) : "");
};

function actualizarReferenciasHTML(callback){
	for(var i=0; i<infoPortal.ref.css.length; i++){
		if(infoPortal.ref.css[i].estatus == 0){
			createReferenceCSS(infoPortal.path.css + infoPortal.ref.css[i].reference);
		}
	}
	callback();
}

function createReferenceCSS(reference){
	iElement = document.createElement('link');
	iElement.rel = 'stylesheet';
	iElement.type = 'text/css';
	iElement.href = reference;
	document.getElementsByTagName('head')[0].appendChild(iElement);
}

function onClickTodo(event){
	switch(elementHTML.elementosClickActual){
		case 0:
			limpiarDivsEmergentes();
			break;
		case 1:
			//console.log(functionName + 'Clic dentro Calendario. ');
			break;
		case 2:
			//console.log(functionName + 'Clic fecha formal. ');
			inicializarCalendario();
			break;
		default:
			break;
	}
	elementHTML.elementosClickActual = 0;
}


function limpiarDivsEmergentes(){
	for(var c=0; c<elementHTML.lista.length; c++){
		if(elementHTML.lista[c].tipoElemento.id=1){
			if(elementHTML.lista[c].existe){
				elementHTML.lista[c].existe = false;
				document.getElementById(elementHTML.lista[c].idDiv).remove();
			}
		}
	}
}