/************************************************************
 *	Nombre del Archivo: constructor.js
 *
 *	Proposito: 	Construye el objeto parametrosConsulta segun 
 *				los Filtros seleccionados para las peticiones
 *				AJAX de la consulta de las Bitacoras.
 *
 *	Autor: 		Eduardo Valentín  Chávez Oliveros.
 *	Fabrica: 	Avance Solution Corporation.
 * 
 ************************************************************/
 function construirJSONCifrasByFilters(callback){
 	limpiarParametros();
	if(filtrosConfig.periodo != TOTALES){
		switch(filtrosSeleccionadosJSON.filtroSeleccionadoPeriodo) {
			case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_DIA:
				parametrosConsulta.fechaOperacionIni = datosFechas.fechaSeleccionada.date;
				parametrosConsulta.fechaOperacionFin = datosFechas.fechaSeleccionada.date;
				break;
			case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_SEMANA:
				parametrosConsulta.fechaOperacionIni = datosFechas.primerFechaSemana.date;
				parametrosConsulta.fechaOperacionFin = datosFechas.ultimaFechaSemana.date;
				break; 
			case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_MES:
				parametrosConsulta.fechaOperacionIni = datosFechas.primerFechaMes.date;
				parametrosConsulta.fechaOperacionFin = datosFechas.ultimaFechaMes.date;
				break;
			case estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_PERIODO + PERIODO_ANIO:
				parametrosConsulta.fechaOperacionIni = datosFechas.primerFechaAnio.date;
				parametrosConsulta.fechaOperacionFin = datosFechas.ultimaFechaAnio.date;
				break;
			default:
				break;
		}
	}
	if(filtrosConfig.sistema != TOTALES){
		for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_SISTEMA + catalogos.relSistemaOperacionArr[i].catSistemas.id == filtrosSeleccionadosJSON.filtroSeleccionadoSistema){
				parametrosConsulta.sistema = catalogos.relSistemaOperacionArr[i].catSistemas.id;
				break;
			}
		}
	}
	if(filtrosConfig.operacion != TOTALES){
		for(var i=0; i<catalogos.relSistemaOperacionArr.length; i++){
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_OPERACION + catalogos.relSistemaOperacionArr[i].catOperaciones.id == filtrosSeleccionadosJSON.filtroSeleccionadoOperacion){
				parametrosConsulta.operacion = catalogos.relSistemaOperacionArr[i].catOperaciones.id;
				break;
			}
		}
	}
	if(filtrosConfig.estatus != TOTALES){	
		for(var i=0; i<catalogos.catEstatusBitacoraArr.length; i++){	
			if(estructuraFiltros.ID_CONTENEDOR_PRINCIPAL + estructuraFiltros.ID_ESTATUS + catalogos.catEstatusBitacoraArr[i].id == filtrosSeleccionadosJSON.filtroSeleccionadoEstatus){
				parametrosConsulta.estatus = catalogos.catEstatusBitacoraArr[i].id;
				break;
			}
		}
	}
	parametrosConsulta.modulo = infoPortal.modulo;
	callback();
}

function limpiarParametros(){
	parametrosConsulta.fechaOperacionIni = null;
	parametrosConsulta.fechaOperacionFin = null;
	parametrosConsulta.fechaRegistroIni = null;
	parametrosConsulta.fechaRegistroFin = null;
	parametrosConsulta.fechaEnvioIni = null;
	parametrosConsulta.fechaEnvioFin = null;
	parametrosConsulta.sistema = null;
	parametrosConsulta.operacion = null;
	parametrosConsulta.estatus = null;
	parametrosConsulta.observaciones = null;
	parametrosConsulta.modulo = null;
}