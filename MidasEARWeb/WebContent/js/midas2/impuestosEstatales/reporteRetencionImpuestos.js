var ventanaBusquedaAgente;
var urlContenedorAyudaAgentes = "/MidasWeb/fuerzaventa/agente/mostrarContenedor.action";
var urlSeleccionarAgente = "/MidasWeb/fuerzaventa/configuracionBono/seleccionarAgente.action";
var urlContenedorAyudaPromotorias = "/MidasWeb/fuerzaventa/promotoria/mostrarContenedor.action";
var urlSeleccionarPromotoria = "/MidasWeb/fuerzaventa/configuracionBono/seleccionarPromotoria.action";

jQuery(document).ready(function() {
	listadoService.getMapMonths(function(data) {
		addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
	});
	listadoService.getMapYears(9, function(data) {
		addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
	});
});

function pantallaModalBusquedaAgente() {
	var idAgente = jQuery("#idAgente").val();
	
	if(idAgente == ""){
		var url = urlContenedorAyudaAgentes + '?tipoAccion=consulta&idField=txtId';
		sendRequestWindow(null, url, obtenerVentanaBusquedaAgente);
	}else{
		var url="/MidasWeb/fuerzaventa/reportePreviewBonos/obtenerAgente.action";
		var data={"agente.idAgente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}


function obtenerVentanaBusquedaAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaBusquedaAgente= wins.createWindow("agenteModal", 10, 320, 900, 450);
	ventanaBusquedaAgente.centerOnScreen();
	ventanaBusquedaAgente.setModal(true);
	ventanaBusquedaAgente.setText("Buscar Agente");
	return ventanaBusquedaAgente;
}

function onChangeAgente() {
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/cargos/cargosAgentes/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgente() {
	var id = jQuery("#idAgente").val();
	if(jQuery.isValid(id)) {
		var url="/MidasWeb/fuerzaventa/reportePreviewBonos/obtenerAgente.action";
		var data={"agente.idAgente":id,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	} else {
		jQuery("#id").val("");
		jQuery("#claveAgente").val("");
		jQuery().val("#nombreAgente").val("");
	}
}

function loadInfoAgente(json) {
	var agenteObj=json.agente;
	if(agenteObj) {
		var id = agenteObj.id;
		var idAgen = agenteObj.idAgente;
		var nombreAgente = agenteObj.persona.nombreCompleto;
		
		jQuery("#idAgente").val(idAgen);
		jQuery("#claveAgente").val(id);
		jQuery("#nombreAgente").val(nombreAgente);
	} else {
		jQuery("#idAgente").val("");
		jQuery("#claveAgente").val("");
		jQuery("#nombreAgente").val("");
	}
}

function exportTo(){
	var anio = jQuery("#anios").val();
	var mes = jQuery("#meses").val();
	var estado = jQuery('#estado').val();
	var id = jQuery("#idAgente").val();
	
	if(!anio) {
		parent.mostrarMensajeInformativo("Seleccione un año, por favor.", "20");
		return false;
	}
	if(!mes) {
		parent.mostrarMensajeInformativo("Seleccione un mes, por favor.", "20");
		return false;
	}
	if(!estado) {
		parent.mostrarMensajeInformativo("Seleccione un estado, por favor.", "20");
		return false;
	}
	
	var path = "/MidasWeb/fuerzaventa/reporteDetallePrimas/exportarToExcel.action?"
		+ "reporteRetencionImpuestosEstatales.anio=" + anio
		+ "&reporteRetencionImpuestosEstatales.mes=" + mes
		+ "&reporteRetencionImpuestosEstatales.estado=" + estado
		+ "&reporteRetencionImpuestosEstatales.idAgente=" + id;
	
	downloadFileWithoutBlocking(path);
}