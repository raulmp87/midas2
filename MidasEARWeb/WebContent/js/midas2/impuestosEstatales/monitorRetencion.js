/**
 * 
 **/
var grid;

jQuery(function() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/listarMonitorRetencionImpuestos.action";
	grid = listarFiltradoGenerico(url, "monitorRetencionGrid", null, null, 'ejecutivoModal');
});

function actualizar() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/monitorRetencionImpuestos.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function regresar() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/mostrarRetencionImpuestos.action";
	sendRequestJQ(null, url, targetWorkArea, null);
}