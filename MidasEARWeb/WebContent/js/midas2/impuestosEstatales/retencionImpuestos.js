/**
 * 
 */
var gridDetalle;

jQuery(function() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/listarFiltrado.action?load=false";
	gridDetalle = new dhtmlXGridObject("detalleRetGrid");
	gridDetalle = listarFiltradoGenerico(url, 'detalleRetGrid', null, null, null);
});

function guardar() {
	if(!validarCamposForma()) {
		return;
	}
	enviarGuardar();
}

function validarCamposForma() {
	var hoy = new Date();
	hoy = new Date(hoy.getFullYear(), hoy.getMonth(), hoy.getDate());
	var fechaInicio = StringToDate(document.getElementById("dtFechaInicioVigencia").value);
	if(jQuery('#stateId').val() == "") {
		parent.mostrarMensajeInformativo("Seleccione una opci&oacute;n del campo Estado, por favor.", "20");
		return false;
	}
	if(jQuery('#txPorcentaje').val() <= 0 || jQuery('#txPorcentaje').val() >= 100) {
		parent.mostrarMensajeInformativo("Ingrese valor num&eacute;rico mayor a 0 y menor a 100 para el campo Porcentaje, por favor.", "20");
		return false;
	}
	if(jQuery('#txConcepto').val() == "") {
		parent.mostrarMensajeInformativo("Ingrese un valor para el campo Concepto, por favor.", "20");
		return false;
	}
	if(jQuery('#comision').val() == "") {
		parent.mostrarMensajeInformativo("Seleccione una opci&oacute;n del campo Comisi&oacute;n, por favor.", "20");
		return false;
	}
	if(jQuery('#juridica').val() == "") {
		parent.mostrarMensajeInformativo("Seleccione una opci&oacute;n del campo Persona Jur&iacute;dica, por favor.", "20");
		return false;
	}
	if(jQuery('#dtFechaInicioVigencia').val() == "") {
		parent.mostrarMensajeInformativo("Ingrese un valor para el campo Fecha de Inicio Vigencia, por favor.", "20");
		return false;
	} else if(!jQuery('#idIE').val() && fechaInicio < hoy) {
		parent.mostrarMensajeInformativo("Ingrese un valor mayor o igual al d&iacute;a de hoy para el campo Inicio Vigencia, por favor.", "20");
		return false;
	}
	if(jQuery('#dtFechaFinVigencia').val()) {
		var fechaFin = jQuery('#dtFechaFinVigencia').val();
		fechaFin = StringToDate(fechaFin);
		if(fechaFin < fechaInicio || fechaFin < hoy) {
			parent.mostrarMensajeInformativo("Ingrese un valor mayor o igual a Fecha Fin Vigencia y a la actual, por favor.", "20");
			return false;
		}
	}
	return true;
}

function enviarGuardar() {
	var isConfigActive = "0";
	if(document.mostrarRetencionImpuestosForm.active.checked) {
		isConfigActive = "1";
	}
	var path = "/MidasWeb/fuerzaventa/retencionImpuestos/guardar.action?filtroConfiguracion.id="+jQuery("#idIE").val()+
	"&filtroConfiguracion.estado.stateId="+jQuery("#stateId").val()+
	"&filtroConfiguracion.porcentaje="+jQuery("#txPorcentaje").val()+
	"&filtroConfiguracion.concepto="+jQuery("#txConcepto").val()+
	"&filtroConfiguracion.tipoComision.id="+jQuery("#comision").val()+
	"&filtroConfiguracion.personalidadJuridica.id="+jQuery("#juridica").val()+
	"&filtroConfiguracion.estatus="+isConfigActive+
	"&filtroConfiguracion.fechaInicioVig="+jQuery("#dtFechaInicioVigencia").val()+
	"&filtroConfiguracion.fechaFinVig=" + jQuery("#dtFechaFinVigencia").val();
	sendRequestJQ(null, path, targetWorkArea, null);
}

function buscar() {
	var url;
	jQuery("#load").val("true");
	url = "/MidasWeb/fuerzaventa/retencionImpuestos/listarFiltrado.action?load=" + jQuery("#load").val() + "&" + jQuery("#mostrarRetencionImpuestosForm").serialize();
	gridDetalle.load(url);
}

function cancel() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/listarFiltrado.action?load=false";
	
	enableField('stateId');
	enableField('dtFechaInicioVigencia');
	enableField('txPorcentaje');
	enableField('dtFechaFinVigencia');
	enableField('txConcepto');
	enableField('active');
	enableField('comision');
	enableField('juridica');
	
	if(jQuery("#dtFechaInicioVigencia").next().attr('disabled')) {
		jQuery("#dtFechaInicioVigencia").next().attr('disabled', false)
	}
	if(jQuery("#dtFechaFinVigencia").next().attr('disabled')) {
		jQuery("#dtFechaFinVigencia").next().attr('disabled', false);
	}
	
	jQuery("#idIE").val("");
	jQuery("#dtFechaInicioVigencia").val("");
	jQuery("#txPorcentaje").val("");
	jQuery("#dtFechaFinVigencia").val("");
	jQuery("#txConcepto").val("");
	jQuery('#stateId')[0].selectedIndex = 0;
	jQuery('#comision')[0].selectedIndex = 0;
	jQuery('#juridica')[0].selectedIndex = 0;
	jQuery('#active').attr('checked', false);
	
	gridDetalle = listarFiltradoGenerico(url, "detalleRetGrid", null, null, null);
}

function enableField(fieldId) {
	if(jQuery("#" + fieldId).attr('disabled')) {
		jQuery("#" + fieldId).attr('disabled', false);
	}
}

function monitor() {
	var path = "/MidasWeb/fuerzaventa/retencionImpuestos/monitorRetencionImpuestos.action";
	sendRequestJQ(null, path, targetWorkArea, null);
}

function editarConfiguracion(id) {
	var d = new Date();
	var dString = (d.getDate() < 10 ? "0" + d.getDate() : d.getDate()) + "/" + ((d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1)) + "/" + d.getFullYear();
	
	var idConfiguracion = gridDetalle.cellById(id, 0).getValue();
	var estado = gridDetalle.cellById(id, 1).getValue();
	var porcentaje = gridDetalle.cellById(id, 3).getValue();
	var concepto = gridDetalle.cellById(id, 4).getValue();
	var tipoComision = gridDetalle.cellById(id, 5).getValue();
	var personalidadJuridica = gridDetalle.cellById(id, 7).getValue();
	var fechaInicioVig = gridDetalle.cellById(id, 9).getValue();
	var fechaFinVig = gridDetalle.cellById(id, 10).getValue();
	var estatus = gridDetalle.cellById(id, 13).getValue();
		
	jQuery("#stateId").attr('disabled', true); 
    jQuery("#txPorcentaje").attr('disabled', true); 
    jQuery("#txConcepto").attr('disabled', true); 
    jQuery("#comision").attr('disabled', true); 
    jQuery("#juridica").attr('disabled', true); 
    jQuery("#dtFechaInicioVigencia").attr('disabled', true);
    jQuery("#dtFechaInicioVigencia").next().attr('disabled', true);
    
    if (estatus == 1) {
    	jQuery("#active").attr('checked', true);
    	jQuery("#active").attr('disabled', true); 
    } else {
    	jQuery("#active").attr('checked', false);
    	if(fechaFinVig) {
    		var fechaFin = StringToDate(fechaFinVig);
    		if(fechaFin < StringToDate(dString)) {
    			jQuery("#active").attr('disabled', true);
    		} else {
    			jQuery('#active').attr('disabled', false);
    		}
    	} else {
    		jQuery("#active").attr('checked', false);
        	jQuery("#active").attr('disabled', false);
    	}
    }
	
	var dateCurrent = new Date();
	dateCurrent = new Date(dateCurrent.getFullYear(), dateCurrent.getMonth(), dateCurrent.getDate());
	var dateFinVigencia = StringToDate(fechaFinVig);
	
	if(fechaFinVig) {
		if(dateFinVigencia < dateCurrent) {
			jQuery("#dtFechaFinVigencia").attr('disabled', true);
			jQuery("#dtFechaFinVigencia").next().attr('disabled', true);
		} else {
			jQuery("#dtFechaFinVigencia").attr('disabled', false);
			jQuery("#dtFechaFinVigencia").next().attr('disabled', false);
		}
	} else {
			jQuery("#dtFechaFinVigencia").attr('disabled', false);
			jQuery("#dtFechaFinVigencia").next().attr('disabled', false);
	}
	
	jQuery("#idIE").val(idConfiguracion);
	jQuery("#stateId option[value='" + estado + "']").attr("selected", true);
	jQuery("#dtFechaInicioVigencia").val(fechaInicioVig);
	jQuery("#txPorcentaje").val(porcentaje);
	jQuery("#dtFechaFinVigencia").val(fechaFinVig);
	jQuery("#txConcepto").val(concepto);
	jQuery("#comision option[value='" + tipoComision + "']").attr("selected", true);
	jQuery("#juridica option[value='" + personalidadJuridica + "']").attr("selected", true);
}

function eliminar(id) {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/eliminar.action?filtroConfiguracion.id=" + id;
	sendRequestJQ(null, url, targetWorkArea, null);
	
}

function listarHistorico() {
	var url = "/MidasWeb/fuerzaventa/retencionImpuestos/listarHistorico.action?filtroConfiguracion.id=" + dwr.util.getValue("filtroConfiguracion.id");
	listarFiltradoGenerico(url, 'historicoRetGrid', null, null, null);
}

function StringToDate(dateStrDDMMYYYY) {
	var result = new Date(dateStrDDMMYYYY.substring(6), dateStrDDMMYYYY.substring(3,5) - 1, dateStrDDMMYYYY.substring(0,2));
	return result;
}

function mostrarHistorico(){
	if(jQuery("#idIE").val()) {
		var url = "/MidasWeb/fuerzaventa/retencionImpuestos/mostrarHistorico.action?filtroConfiguracion.id=" + jQuery("#idIE").val();
		parent.mostrarVentanaModal("historicoRetGrid", 'Historial', 100, 200, 700, 400, url);
	} else {
		parent.mostrarMensajeInformativo("Seleccione una configuracion, por favor", "20");
	}
}