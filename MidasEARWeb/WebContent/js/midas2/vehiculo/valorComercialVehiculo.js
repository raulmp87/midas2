var valorComercialGrid;

function anexarArchivoAdjunto(){
	paramenters = "tipo=1";
	mostrarVentanaVault('/MidasWeb/vehiculo/valorComercialVehiculo/validaCarga.action', paramenters, targetWorkArea, '30', null, null);
}

function cargaPantallaPrincipal(){
	var url="/MidasWeb/vehiculo/valorComercialVehiculo/mostrar.action";
	sendRequestJQ(null, url ,"contenido", null);
}

function busquedaDatos(){
	
	if ( validarDatos() ){
		
		var data = jQuery("#busquedaServicio").serialize();
		
		document.getElementById("valorComercialGrid").innerHTML = '';
		valorComercialGrid = new dhtmlXGridObject("valorComercialGrid");
		valorComercialGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		valorComercialGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		valorComercialGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	    });
		valorComercialGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	    });		
		valorComercialGrid.load("/MidasWeb/vehiculo/valorComercialVehiculo/buscar.action?"+data);
	
	}else{
		mostrarMensajeInformativo('Debe ingresar o seleccionar algun rango de busqueda', '10');
	}
}


function descargarLogCargaMasiva() {
	
	var idToControlArchivo = jQuery("#idToControlArchivo").val();
	if(jQuery("#logErrors").val() == 'true'){
		var location ="/MidasWeb/vehiculo/valorComercialVehiculo/descargarLogErrores.action?idToControlArchivo="+idToControlArchivo;
		window.open(location, "Sumas_Aseguradas");
	}
	
}


function desactivarRegistro(id){
	mostrarMensajeConfirm("¿Desea desactivar el registro?", "20", "bajaRegistro("+id+");", null, null);
}

function bajaRegistro(id){
	var url="/MidasWeb/vehiculo/valorComercialVehiculo/desactivar.action?keySumasAseguradas=" + id;
	sendRequestJQ(null, url ,"contenido", null);
}

function validarDatos(){
	
	var numeroAmis          = jQuery("#numeroAmis").val();
	var descripcionVehiculo = jQuery("#descripcionVehiculo").val();
	var anio                = jQuery("#anio").val();
	var meses               = jQuery("#meses").val();
	var modelo              = jQuery("#modelo").val();

	if ( numeroAmis == "" & descripcionVehiculo == "" & anio == "0" & meses == "0" & modelo == "0" ){
		return false;
	}else{
		jQuery("#exportar").show();
		return true;
	}
}

function exportarExcel(){
	var data=jQuery("#busquedaServicio").serialize();
	var url="/MidasWeb/vehiculo/valorComercialVehiculo/exportarExcel.action?" + data;
	window.open(url, mensaje);
}

function registroDesactivado(tipo){
	if ( tipo == 1){
		mostrarMensajeInformativo('El registro no se puede volver a activar', '10');
	}else if ( tipo == 2){
		mostrarMensajeInformativo('Sin permisos', '10');
	}
}