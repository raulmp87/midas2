function ventanaFortimax(cveProceso, folio, tituloVentana,idFortimax, folioExpediente){
	if(null==idFortimax || idFortimax=='' ){
		mostrarMensajeInformativo('No puede consultarse, Requiere identificador asignado.', '20');
	}
	else{
		var url = '/MidasWeb/siniestros/repositorioarchivos/adminArchivos.action?cveProceso=' + cveProceso + '&subCarpeta=' +folio+ '&tituloVentana=' + tituloVentana+ '&idFortimax='+idFortimax+ '&folioExpediente='+ folioExpediente;
		mostrarVentanaModal("vm_AdminArchivos", tituloVentana, 100, 100, 680, 420, url , null);
	}
}


//Inicializa el Greid de la lista de Oficinas
function iniContenedorGrid(cveProceso, subCarpeta, tituloVentana,idFortimax) {
	var docGrid = new dhtmlXGridObject("archivosGrid");
	docGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	docGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	docGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
  });
	docGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
  });		
	var formParam = null;
	formParam = jQuery(document.contenedorForm).serialize();
	formParam ='?cveProceso=' + cveProceso + '&subCarpeta=' +subCarpeta+ '&tituloVentana=' + tituloVentana+ '&idFortimax='+idFortimax;
	docGrid.load("/MidasWeb/siniestros/repositorioarchivos/gridArchivo.action"+formParam);
}

function verExpediente(cveProceso, subCarpeta, tituloVentana,idFortimax){
	var url = '/MidasWeb/siniestros/repositorioarchivos/verExpediente.action?cveProceso=' + cveProceso + '&subCarpeta=' +subCarpeta+ '&tituloVentana=' + tituloVentana+ '&idFortimax='+idFortimax;
	var w = window.open(url,'Fortimax');
	
	}

function abrirLigaIfimax(){
  document.forms[0].action=dwr.util.getValue("liga");
  document.forms[0].submit(); 
}


function verLigaDocumento(cveProceso, folio, tituloVentana,idFortimax, folioExpediente, documento){
		var url = '/MidasWeb/siniestros/repositorioarchivos/verExpediente.action?cveProceso=' + cveProceso + '&tituloVentana=' + tituloVentana+ '&idFortimax='+idFortimax+ '&folioExpediente='+ folioExpediente+'&nombreArchivo='+ documento;
		var w = window.open(url,'Fortimax');
	 
} 