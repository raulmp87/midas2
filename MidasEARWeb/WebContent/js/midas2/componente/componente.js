
var gridRegistrosDinamicos;

var ventanaFormRegDin = null;
var DIV_WORKAREA_REG_DIN = null;
var ID_VENTANA = "RegistroDin";
var VISTA_ACTUAL = null;
/**
 * ADDED
 * Funcion que se manda a llamar desde el menuMIDAS.xml
 * @param divResultado
 */
function cargarContenedorRegiDin(divResultado,prefix){
	pNextFunction = "poblarGridComponentes('"+prefix+"')";
	PREFIX=prefix;
	DIV_WORKAREA_REG_DIN = divResultado;
	sendRequestJQ(null, mostrarContenedorRegDinPath+"?clave="+prefix, DIV_WORKAREA_REG_DIN, pNextFunction);
}

function cargarContenedorRegiDinOld(divResultado, vista){
	
	if (vista != null) {
		VISTA_ACTUAL = vista;
	}
	
	pNextFunction = "poblarGridComponentesOld()";
	PREFIX=getIdTarifaPorRegistrar();
	DIV_WORKAREA_REG_DIN = divResultado;
	sendRequestJQTarifa(null, mostrarContenedorRegDinPath, DIV_WORKAREA_REG_DIN, pNextFunction);
}

function poblarGridComponentes(prefix){
//	document.getElementById('registrosDinGrid').innerHTML = '';
//	document.getElementById('pagingArea').innerHTML = '';
//	document.getElementById('infoArea').innerHTML = '';
	jQuery("registrosDinGrid").html("");
	jQuery("pagingArea").html("");
	jQuery("infoArea").html("");
	PREFIX=prefix;
	gridRegistrosDinamicos = new dhtmlXGridObject('registrosDinGrid');
	mostrarIndicadorCarga('indicador');	
	
	gridRegistrosDinamicos.attachEvent("onXLE", function(grid_obj,count){
		ocultarIndicadorCarga('indicador');
    });
	try{
		gridRegistrosDinamicos.load(listarRegistrosDinPath+"?clave="+prefix);
	}catch(error){
		unblockPage();
	}
	
}

function poblarGridComponentesOld(){
//	document.getElementById('registrosDinGrid').innerHTML = '';
//	document.getElementById('pagingArea').innerHTML = '';
//	document.getElementById('infoArea').innerHTML = '';
	jQuery("registrosDinGrid").html("");
	jQuery("pagingArea").html("");
	jQuery("infoArea").html("");
	mostrarIndicadorCarga('indicador');	
	gridRegistrosDinamicos = new dhtmlXGridObject('registrosDinGrid');
	
//	gridRegistrosDinamicos.attachEvent("onXLS", function(grid){
//		blockPage();
//	});
	
	gridRegistrosDinamicos.attachEvent("onXLE", function(grid_obj,count){
		ocultarIndicadorCarga('indicador');
	});
	try{
	gridRegistrosDinamicos.load(listarRegistrosDinPath+"?"+getClaveTarifaPorRegistrar() + "&claveTipoVista=" + VISTA_ACTUAL);
	}catch(error){
		unblockPage();
	}
	
	dwr.util.setValue("claveTipoVista", VISTA_ACTUAL);
}

function verDetalleRegistroDinamico(accion,claveRegistro){
	var cTV = dwr.util.getValue('claveTipoVista');
	
	var tituloVentana="";
	if(accion=="1"){
		tituloVentana="Agregar."
	}
	if(accion=="4"){
		tituloVentana="Editar."
	}
	if(accion=="3"){
		tituloVentana="Borrar."
	}
	
	if(claveRegistro == null || claveRegistro == undefined){
		claveRegistro = "clave="+gridRegistrosDinamicos.getSelectedId();
	}
	if (dhxWins==null){
		dhxWins = new dhtmlXWindows();
		dhxWins.enableAutoViewport(true);
		dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	if (ventanaFormRegDin ==null || parent.dhxWins.window(ID_VENTANA) == null || parent.dhxWins.window(ID_VENTANA).isHidden()){
		ventanaFormRegDin = dhxWins.createWindow(ID_VENTANA, 400, 320, 610, 260);
		ventanaFormRegDin.center();
		ventanaFormRegDin.setText(tituloVentana);
		ventanaFormRegDin.setModal(true);
		ventanaFormRegDin.attachEvent("onClose", cerrarVentanaFormDin);
		ventanaFormRegDin.attachURL(verDetalleRegDinPath+"?accion="+accion+
				"&"+claveRegistro+"&claveTipoVista="+cTV+"&prefix="+PREFIX);
	}
}

/*
 * En esta función se deben consultar los campos que conforman el tarifaVersionId,
 * y generar una cadena que conforma la clave del tipo de tarifa que se desea agregar.
 */
function getClaveTarifaPorRegistrar(){
	var idTarifaVersion =getIdTarifaPorRegistrar();
	return "clave="+idTarifaVersion ;
}

function getIdTarifaPorRegistrar(){
	var idTarifaVersion = "";
	idTarifaVersion += dwr.util.getValue('tarifaVersion.id.idMoneda') + "_";
	idTarifaVersion += dwr.util.getValue('tarifaVersion.id.idRiesgo') + "_";
	idTarifaVersion += dwr.util.getValue('tarifaVersion.id.idConcepto') + "_";
	idTarifaVersion += dwr.util.getValue('tarifaVersion.id.version');
	return "T_"+idTarifaVersion;
}

function getClave(){
	if(PREFIX!=null){
		var parts=PREFIX.split("_");
		var key="_";
		var ids="";
		if(parts.length>1){
			if(parts[0]!="T"){
				key=parts[0];
				for(var i=1;i<parts.length;i++){
					var value=parts[i];
					ids+="_"+value;
				}
			}else{
				return getClaveTarifaPorRegistrar();
			}
		}
	}else{
		return getClaveTarifaPorRegistrar();
	}
	return "clave="+key+ids;
}


function guardarRegistroDinamicoOld(accion){
    var clave="VMP_";
    var contenido="contenido";
	pNextFunction = "cerrarVentanaFormDin();cargarContenedorRegiDin("+contenido,clave+")";
	var guardar = true;
	//Poner el confirm aqui en caso de que la accion sea borrar
	if (accion == "3") { //Borrar
		if (!confirm("Seguro desea borrar este registro?")) {
			guardar = false;
		}
	} 
	if (guardar) {
		sendRequestJQ(document.getElementById("formaDinamica"),
				guardarRegistroDinamicoPath, "ventanaDetalleDinamico", 
				pNextFunction);


	}
}

function guardarRegistroDinamico(accion){
	PREFIX=($('prefix').value!="null" && $('prefix').value!=null)?$('prefix').value:null;
	pNextFunction = "cerrarVentanaFormDin();poblarGridComponentes('"+PREFIX+"')";
	var guardar = true;
	//Poner el confirm aqui en caso de que la accion sea borrar
	if (accion == "3") { //Borrar
		if (!confirm("\u00BFSeguro desea borrar este registro?")) {
			guardar = false;
		}
	} 
	
	if (guardar) {
		
		sendRequestJQ(document.getElementById("formaDinamica"),guardarRegistroDinamicoPath, "ventanaDetalleDinamico", pNextFunction);
		
		if(gridRegistrosDinamicos != undefined){
			gridRegistrosDinamicos.updateFromXML(null,guardarRegistroDinamicoPath, null);
		}
	}
}

function cerrarVentanaFormDin(){
	parent.dhxWins.window(ID_VENTANA).setModal(false);
	parent.dhxWins.window(ID_VENTANA).hide();
	parent.ventanaFormRegDin=null;
	if(PREFIX!="T" && PREFIX!=null){
		parent.cargarContenedorRegiDin(parent.DIV_WORKAREA_REG_DIN,PREFIX);
	}else{
		parent.cargarContenedorRegiDinOld(parent.DIV_WORKAREA_REG_DIN);
	}
}

/*
 * Funcion utilizada para cascadeo generico
 */
function cascadeoGenerico(objectoSelect,claveCatalogo,idControlHijo){
	var valorPadre = objectoSelect.value;
	componenteService.getListadoRegistros(
			claveCatalogo+"_"+valorPadre, 
				function (data){
					var combo = document.getElementsByName('registro.'+idControlHijo)[0];
					addOptions(combo, data);
				}
			);
}

function habilitarCamposPorGrupo(objetoGrupo){
	var id=objetoGrupo.value;
	if(id!=headerValue){
		listadoService.getClaveTipoDetalleGrupo(id,revisarGrupo);	
	}else{
		$('formaDinamica_registro_valor').readOnly=true;
		$('formaDinamica_registro_numeroSecuencia').readOnly=true;
		$('formaDinamica_registro_rangoMinimo').readOnly=true;
		$('formaDinamica_registro_rangoMaximo').readOnly=true;	
	}
	$('formaDinamica_registro_numeroSecuencia').value="";
	$('formaDinamica_registro_rangoMinimo').value="";
	$('formaDinamica_registro_rangoMinimo').value="";
	$('formaDinamica_registro_rangoMaximo').value="";
	$('formaDinamica_registro_valor').value="";
	
}

function revisarGrupo(tipoClave){
	if("0"==tipoClave){
		$('formaDinamica_registro_rangoMinimo').value="-";
		$('formaDinamica_registro_rangoMaximo').value="-";
		$('formaDinamica_registro_valor').readOnly=false;
		$('formaDinamica_registro_numeroSecuencia').readOnly=false;
	}else{
		$('formaDinamica_registro_valor').value="-";
		$('formaDinamica_registro_rangoMinimo').readOnly=false;
		$('formaDinamica_registro_rangoMaximo').readOnly=false;
		$('formaDinamica_registro_numeroSecuencia').readOnly=false;
	}
	
}

function filtrarRegistroDinamico(){
	//Lo que se hace es poblar nuevamente el grid, enviando el formulario para que se haga el filtrado
	gridRegistrosDinamicos = new dhtmlXGridObject('registrosDinGrid');
	
//	gridRegistrosDinamicos.attachEvent("onXLS", function(grid){
//		blockPage();
//    });
//	
//	gridRegistrosDinamicos.attachEvent("onXLE", function(grid_obj,count){
//		unblockPage();
//    });
//	try{
		gridRegistrosDinamicos.load(filtrarRegistrosDinPath+
				"?"+jQuery(document.formaDinamica).serialize(true));
//	}catch(error){
//		unblockPage();
//	}
	
}

function popUpEnvioCorreo(){	
		if (parent.dhxWins==null){
			parent.dhxWins = new dhtmlXWindows();
			parent.dhxWins.enableAutoViewport(true);
			parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
		}
		parent.ventanaRechazo = parent.dhxWins.createWindow("rechazar", 200, 320, 550, 200);
		parent.ventanaRechazo.setText("Enviar Correo");
		parent.ventanaRechazo.center();
		parent.ventanaRechazo.setModal(true);
		parent.ventanaRechazo.attachEvent("onClose", onCloseFunction);
		parent.ventanaRechazo.attachURL("/MidasWeb/suscripcion/cotizacion-express/auto/capturarDatosCorreo.action");
		
		parent.ventanaRechazo.button("minmax1").hide();
}