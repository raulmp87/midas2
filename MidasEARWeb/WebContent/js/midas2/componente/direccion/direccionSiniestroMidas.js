function cargarCombosDireccionSin(){
            var path= "/MidasWeb/componente/direccionSiniestroMidas/combosDireccion.action";      
            sendRequestJQ(null,path,'contenido',null);     
}

function onChangePaisSin(target,targetCiudad,targetColonia,targetCP,targetCalle, targetNumero, targetnumeroInt, targetReferencia, paisSelect){
      var idPais = dwr.util.getValue(jQuery(paisSelect).selector);
      
      if(idPais != null  && idPais != headerValue){
            
            listadoService.getMapEstadosPorPaisMidas(idPais,
                        function(data){
                             addOptionsDireccionSin(target,data);
                        });
            
      }else{
            addOptionsDireccionSin(target,"");
      }     
      if(targetCiudad){
            addOptionsDireccionSin(targetCiudad,"");
      }
      if(targetColonia){
            addOptionsDireccionSin(targetColonia,"");
      }
      if(targetCP){
            dwr.util.setValue(targetCP, "");
      }
      if(targetCalle){
            dwr.util.setValue(targetCalle, "");
      }
      if(targetNumero){
            dwr.util.setValue(targetNumero, "");
      }
      if(targetnumeroInt){
            dwr.util.setValue(targetnumeroInt, "");
      }
      if(targetReferencia){
            dwr.util.setValue(targetReferencia, "");
      }
}
/**
* Funcion que pinta en el jsp los elemntos de un combo box
* ordenados por el texto de la opcion y no por el value
* @param ele
* @param data
*/
function addOptionOderByElementSin(ele, data) {
        dwr.util.removeAllOptions(jQuery(ele).selector);
        ele = dwr.util._getElementById(ele, "addOptions()");
        if (ele == null) return;
        var useOptions = dwr.util._isHTMLElement(ele, "select");
        if (!useOptions) {
          dwr.util._debug("addOptions() can only be used with select elements. Attempt to use: " + dwr.util._detailedTypeOf(ele));
          return;
        }
        if (data == null) return;
        
        var options = {}; 
        var orderItem=new Array();
        if (!options.optionCreator && useOptions) options.optionCreator = dwr.util._defaultOptionCreator;
        options.document = ele.ownerDocument;

          if (!useOptions) {
            dwr.util._debug("dwr.util.addOptions can only create select lists from objects.");
            return;
          }
          for (var prop in data) {
            if (typeof data[prop] == "function") continue;
                  options.data = data[prop];
              options.value = prop;
              options.text = data[prop];
            if (options.text != null || options.value) {
              orderItem[orderItem.length]=new Array(options.text,options.value);
            }
          }
          orderItem.sort(function compara(a, b) {
              return (a[0]<b[0]?"-1":"1");
          });
          
          var opt = "";
          var i = 0;
          for(i; i<orderItem.length; i++){
            if(i==0){
                  opt = options.optionCreator(options);
                  opt.text = "seleccione...";
                    opt.value = "";
                    ele.options[ele.options.length] = opt;
            }
            opt = options.optionCreator(options);
            opt.text = orderItem[i][0];
              opt.value = orderItem[i][1];
              ele.options[ele.options.length] = opt;
          }
        // All error routes through this function result in a return, so highlight now
        dwr.util.highlight(ele, options); 
      }

function onChangeEstadoGeneralSin(target,targetColonia,targetCP,targetCalle, targetNumero, targetnumeroInt, targetReferencia, estadoSelect){
	var idEstado = dwr.util.getValue(jQuery(estadoSelect).selector);
      if(idEstado != null  && idEstado != headerValue){
            
            listadoService.getMapMunicipiosPorEstadoMidas(idEstado,
                        function(data){
//                           addOptions(target,data);
                             addOptionOderByElementSin(target,data);
                        });
            
      }else{
            addOptionsDireccionSin(target,"");
      }
      
      if(targetColonia){
            addOptionsDireccionSin(targetColonia,"");
      }
      if(targetCP){
            dwr.util.setValue(targetCP, "");
      }
      if(targetCalle){
            dwr.util.setValue(targetCalle, "");
      }
      if(targetNumero){
            dwr.util.setValue(targetNumero, "");
      }
      if(targetnumeroInt){
            dwr.util.setValue(targetnumeroInt, "");
      }
      if(targetReferencia){
            dwr.util.setValue(targetReferencia, "");
      }
}

function onChangeCiudadSin(target,targetCP,targetCalle, targetNumero, targetnumeroInt, targetReferencia, ciudadSelect){
      var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
      if(idCiudad != null  && idCiudad != headerValue){
            
            listadoService.getMapColoniasPorCiudadMidas(idCiudad,
                        function(data){
                  addOptionsDireccionSin(target,data);
                        });
            
      }else{
            addOptionsDireccionSin(target,"");
      }
      if(targetCP){
            dwr.util.setValue(targetCP, "");
      }
      if(targetCalle){
            dwr.util.setValue(targetCalle, "");
      }
      if(targetNumero){
            dwr.util.setValue(targetNumero, "");
      }
      if(targetnumeroInt){
            dwr.util.setValue(targetnumeroInt, "");
      }
      if(targetReferencia){
            dwr.util.setValue(targetReferencia, "");
      }
}

function onChangeColoniaSin(target,targetCalle, targetNumero, targetnumeroInt, targetReferencia, coloniaSelect,ciudadSelect){

      var idColonia = dwr.util.getValue(jQuery(coloniaSelect).selector);
      var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
      if(idColonia != null  && idColonia != headerValue){        
            
            listadoService.getCodigoPostalPorColoniaMidas(idColonia,
                        function(data){
                              dwr.util.setValue(target, data);
                        });
      
      }
      if(targetCalle){
            dwr.util.setValue(targetCalle, "");
      }
      if(targetNumero){
            dwr.util.setValue(targetNumero, "");
      }
      if(targetnumeroInt){
            dwr.util.setValue(targetnumeroInt, "");
      }
      if(targetReferencia){
            dwr.util.setValue(targetReferencia, "");
      }
}

function onChangeCodigoPostalSin(value,targetColonia,targetMunicipio,targetIdEstado,targetCalle, targetNumero, targetNumeroInt, targetReferencia, targetPais,targetCP, targetOtraColonia, targetOtraColoniaCheck){
    if(value != ""){
		if (dwr.util.getValue(targetPais)==""){
	            dwr.util.setValue(targetPais,'MEXICO');
	            onChangeCP_paisSin(targetIdEstado);
	      }
	
	      getCombosDireccionMidas(value, targetColonia, targetMunicipio);
	      
	      
	      listadoService.getEstadoPorCPMidas(value, 
	                  						 function(data){
	    	  									dwr.util.setValue(targetIdEstado,data);
	    	  									listadoService.getCiudadPorCPMidas(value, 
	    	  																	   function(data){
	    	  																			dwr.util.setValue(targetMunicipio,data);
	    	  																		})
	    	  								 });
	      
	      var ciudadActual=dwr.util.getValue(targetMunicipio);

	      if(ciudadActual == null || ciudadActual == ''){
	            dwr.util.setValue(targetIdEstado,"")
	            dwr.util.setValue(targetIdEstado,"")
	      }else{
	            dwr.util.setValue(targetCP,value);
	      }
	            dwr.util.setValue(targetCalle,"");
	            dwr.util.setValue(targetNumero,""); 
	            dwr.util.setValue(targetNumeroInt,"");
	            dwr.util.setValue(targetReferencia,"");
	            dwr.util.setValue(targetOtraColonia,"");
	            document.getElementById(targetColonia).disabled=false;
	            document.getElementById(targetOtraColonia).disabled=true;
	            document.getElementById(targetOtraColoniaCheck).checked=false;
    	}
      }

function getCombosDireccionMidas(value, targetColonia, targetMunicipio){

      listadoService.getMapCiudadesPorCPMidas(value, 
                  function(data){addOptionsDireccionSin(targetMunicipio,data);});

      listadoService.getMapColoniasPorCPMidas(value, 
                  function(data){addOptionsDireccionSin(targetColonia,data);});
      
}

function onChangeCP_paisSin(target){
            
            listadoService.getMapEstadosPorPaisMidas("PAMEXI",
                        function(data){
                             addOptionsDireccionSin(target,data);
                        });
            
}

function onChangeNuevaColoniaSin(nuevaColoniaId,coloniaId,idCheckbox, action){
      var checked;
      if (action == "text"){
            var textOtraColonia = document.getElementById(nuevaColoniaId).value;
            if (textOtraColonia != "" && textOtraColonia != "undefinded" && textOtraColonia != null){
                  checked = true;
                  document.getElementById(idCheckbox).checked = checked;
            }
      }
      if (action == "check"){
            checked=document.getElementById(idCheckbox).checked;       
      }

      document.getElementById(nuevaColoniaId).disabled=!checked;
      document.getElementById(coloniaId).disabled=checked;
      if(checked){
            jQuery(document.getElementById(nuevaColoniaId)).addClass("jQrequired");
            jQuery(document.getElementById(coloniaId)).removeClass("jQrequired");
            jQuery(document.getElementById(coloniaId)).val("");
      }else{
            jQuery(document.getElementById(nuevaColoniaId)).val("");
            jQuery(document.getElementById(nuevaColoniaId)).removeClass("jQrequired");
            jQuery(document.getElementById(coloniaId)).addClass("jQrequired");
      }
      
      ('jQrequired');
}

function addOptionsDireccionSin(target, map) {
      dwr.util.removeAllOptions(jQuery(target).selector);
      addSelectHeader(target);
      if(map != null){
            dwr.util.addOptions(jQuery(target).selector, map);         
      }
}

function getNameColoniasPorCPMidas(codigoPostal, colonia, ciudad, estado) {
      alert('Cargo combos Midas');
      if (codigoPostal != null || codigoPostal != "") {
            colonyCombo = colonia
            cityCombo = ciudad
            stateCombo = estado
//          new Ajax.Request("/MidasWeb/codigoPostal.do", {
            new Ajax.Request("/MidasWeb/componente/direccionSiniestroMidas/escribeCombo.action", {
                  method : "post",
                  asynchronous : false,
                  parameters : "cpName=" + codigoPostal,
                  success : function(data) {
                  jQuery("#" + 'componenteDireccionMidas').html("");
                  jQuery("#" + 'componenteDireccionMidas').html(data);
                        //loadCombosDomicilioMidas(transport.responseXML);
                  } // End of onSuccess
            });
      }     
}
