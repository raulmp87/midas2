var gridProgPagos;
var grid;

// VARIABLE IDS ASIGNAR PERSONA
var personaIdToCotizacion;
var personaIdToProgPago;
//------------------------------------------------------------


// VARIABLE DE ID DE COTIZACION
var idCotizacion;
// ------------------------------------------------------------

// VARIABLE DE CONTROL READ ONLY
var readOnly;
// ------------------------------------------------------------

// MINIMO DE PRIMA DE RECIBOS PARA DISTRIBUCION
var minimoRecibos = 1;
// ------------------------------------------------------------

// VARIABLE PARA ALMACENAR EL NUMERO DE INCISO DEL PROGRAMA DE PAGO SELECCIONADO
var indexSeleccionado;

// VARIABLE PARA INDICAR EL TIPO DE MOVIMIENTO
var tipoMov;

// -----------------------------------------------

// CONSTANTES DE BOTONES PARA INDICAR REGISTROS EDITADOS
var botonEditado = "<a href='javascript: void(0)' onclick='javascript: liberarRow(rowId)' ><img alt='Registro Editado' title='Registro Editado' src='/MidasWeb/img/common/b_no.jpg' border='0px' height='15' width='15'></a>";
var botonNoEditado = "<a href='javascript: void(0)' onclick='javascript: marcarRow(rowId)' ><img alt='Registro No Editado' title='Registro No Editado' src='/MidasWeb/img/common/b_ok.jpg' border='0px' height='15' width='15'></a>";

var primaTotalCot;
var recargoTotalCot;
var derechosTotalCot;
var ivaTotalCot;
var totalTotalCot;

var primaTotal;
var recargoTotal;
var derechosTotal;
var ivaTotal;
var totalTotal;

//variable para acumular el porcentaje de pago fraccionado
var porcentajePagoFraccionado;

var prima;
var recargo;
var derechos;
var iva;
var total;

var primaDif;
var recargoDif;
var derechosDif;
var ivaDif;
var totalDif;

var primaTotalPP;
var recargoTotalPP;
var derechosTotalPP;
var ivaTotalPP;
var totalTotalPP;

// Variables para calculo de diferiencia de programas de pago
var primaDifPP;
var recargoDifPP;
var derechosDifPP;
var ivaDifPP;
var totalDifPP;

var primaPP;
var recargoPP;
var derechosPP;
var ivaPP;
var totalPP;

// Variables de totales de mi programa de pago deslogado en pantalla de recibos
var primaPPEnVista;
var recargoPPEnVista;
var derechosPPEnVista;
var ivaPPEnVista;
var totalPPEnVista;

var miMatriz;
var miArraygrid;

var flagDistribuyePrima;

//VARIABLES DE DISTRIBUCION DE DERECHOS Y RECARGOS
var flagDistribuyeRecargos;
var flagDistribuyeDerechos;


//VARIABLE DE TIPO DE CORTE
var flagTipoCorte = "am";

var myForm;

var flagTotalModificado=false;
//VARIABLE DE CONTROL DE PERMISO PARA MODIFICAR COLUMNA PRIMA TOTAL
var hasEditPrimaTotalPermit;

//VARIABLE DE CONTROL DE AJUSTE PERMITIDO POR EL NEGOCIO PARA LA COLUMNA PRIMA TOTAL
var ajustePermitidoPrimaTotal
// ------------------------------------------------------------

function actualizarBanderas() {	
	if (flagDistribuyePrima == '1') {

		for (var i = 0; i < miArraygrid.length; i++) {
			var index = miArraygrid[i]["index"];
			var boton = miArraygrid[i]["icono"];
			grid.cells2(index, 11).setValue(boton);
		}
	}// if
}// actualizar banderas

function agregaProgramaPago() {

	// validar que tenga saldo
	if (primaDifPP > 0 || recargoDifPP > 0 || derechosDifPP > 0) {

		var newId = (new Date()).valueOf()
		// index siguiente
		var count = gridProgPagos.getRowsNum();
		// obtenemos el rowid del index anterior
		var ofRow = gridProgPagos.getRowId(count - 1);
		// agregamos el row
		gridProgPagos.addRow(newId, [], count);
		// clonamos row
		gridProgPagos.copyRowContent(ofRow, newId);
		// vamos a setear valores delfault del nuevo row los tomamos de la
		// Diferencia
		// No de Exhibicion asignamos siguiente numero
		gridProgPagos.cells(newId, 0).setValue(count + 1);
		// Prima Neta
		gridProgPagos.cells(newId, 5).setValue(primaDifPP);
		// Recargo
		gridProgPagos.cells(newId, 6).setValue(recargoDifPP);
		// Derechos
		gridProgPagos.cells(newId, 7).setValue(derechosDifPP);
		// IVA
		gridProgPagos.cells(newId, 8).setValue(ivaDifPP);
		// primatotal
		gridProgPagos.cells(newId, 9).setValue(totalDifPP);

		var htmlBoton = gridProgPagos.cells2(gridProgPagos.getRowsNum() - 1, 10).getValue();

		recalculaTotalesProgramaPago();

	} else {
		alert("No se puede agregar programa de pago porque no hay saldo disponible");
	}
}

function agregaProgramaPago2(idToCot, tipoMov, idToSolicitud, idToPoliza,  idContinuidad, tipoFormaPago, numeroEndoso) {
	
	if(gridProgPagos.getSelectedRowId() != null){
		
		// Se valida que exista saldo para agregar programa de pago
		if (primaDifPP > 0 || recargoDifPP > 0 || derechosDifPP > 0) {

			// obtenemos el id del programa de pago original
			//var progPago = gridProgPagos.getRowId(gridProgPagos.getRowsNum() - 1);
			var progPago = indexSeleccionado;
			// Se valida que exista un registro del Grid Seleccionado para obtener el Inciso sobre el cual se agregara el Programa de Pago;
			
			
			
			
				
				// Tiramos el ajax al action
				jQuery.ajax( {
					type : "GET",
					url : '/MidasWeb/componente/programapago/agregarProgramaPago.action?'+
					      'impPrimaNeta='+ primaDifPP + 
					      '&impDerechos=' + derechosDifPP+ 
					      '&impRecargos=' + recargoDifPP + 
					      '&impIva=' + ivaDifPP + 
					      '&impPrimaTotal=' + totalDifPP + 
					      '&idProgPago=' + progPago + 
					      '&id=' + idCotizacion +
					      '&tipoMov='+tipoMov +
					      '&tipoFormaPago='+tipoFormaPago,
					dataType : "json",
					async : false
				});

				var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?'+
				'idToCotizacion=' + idCotizacion +
				'&tipoMov='+tipoMov+
				'&idToSolicitud='+idToSolicitud+
				'&idToPoliza='+idToPoliza+
				'&idContinuity='+idContinuidad+
				'&tipoFormaPago='+tipoFormaPago+
				'&numeroEndoso='+numeroEndoso;
				redirectVentanaModal("programaPago", url, null);
		} else {
			alert("No hay saldos disponibles para agregar programa de pago");
		}
		
	}else{
		alert("Se requiere seleccionar un programa de pago, para agregar el nuevo sobre el mismo Inciso");
	}
	
	
}

function agregaRecibo() {

	if (!validaFechasVaciasEnGrid()) {
		return false;
	}

	// validar que tenga saldo
	if (primaDif.html() > 0 || recargoDif.html() > 0 || derechosDif.html() > 0) {

		var newId = (new Date()).valueOf()
		// index siguiente
		var count = grid.getRowsNum();
		// obteneos el rowid del index anterior
		var ofRow = grid.getRowId(count - 1);
		// agregamos el row
		grid.addRow(newId, [], count);
		// clonamos row
		grid.copyRowContent(ofRow, newId);
		// vamos a setear valores delfault del nuevo row los tomamos de la
		// Diferencia
		// No de Exhibicion asignamos siguiente numero
		grid.cells(newId, 0).setValue(count + 1);
		// Prima Neta
		grid.cells(newId, 5).setValue(primaDif.html());
		// Recargo
		grid.cells(newId, 6).setValue(recargoDif.html());
		// Derechos
		grid.cells(newId, 7).setValue(derechosDif.html());
		// IVA
		grid.cells(newId, 8).setValue(ivaDif.html());
		// primatotal
		grid.cells(newId, 9).setValue(totalDif.html());

		// Hacemos un replace del nuevo id, en el html de las acciones del
		// registro
		var htmlBoton = grid.cells2(grid.getRowsNum() - 1, 10).getValue();
		var newBoton = htmlBoton.replace(ofRow, newId);
		grid.cells(newId, 10).setValue(newBoton);

		// Seteamos el boton de editado con el nuevo id del registro
		// miBoton = botonEditado.replace("rowId", newId);
		// Seteamos el boton de no editado
		
		if (flagDistribuyePrima == '0'){
			//quitamos el boton de distribucion
			grid.cells(newId, 11).setValue('</br>');
		}else{
			grid.cells(newId, 11).setValue(botonNoEditado);
		}
		// Seteamos en la fecha desde del nuevo recibo, valor vacio
		grid.cells(newId, 3).setValue("");
		// Seteamos la fecha de vencimiento del row nuevo, valor vacion
		grid.cells(newId, 2).setValue("");

		// Seteamos en la fecha hasta del row anterior, un valor vacio
		grid.cells(ofRow, 4).setValue("");

		agregarReciboAMatrizDeDistribucion();
		if (flagDistribuyeRecargos == '1'){
			recalculaRecargos();
		}if (flagDistribuyeDerechos == '1'){
			recalculaDerechos();
		}
		calculateFooterValues(0);
		} else {
		alert("No se puede agregar registros porque no hay saldo disponible");
	}
}

function agregarReciboAMatrizDeDistribucion() {
	var elementosEnArray = miArraygrid.length;
	
	if (elementosEnArray == 0){
		calculateFooterValues();
	}
	
	var registrosEnGrid = grid.getRowsNum();

	// Creamos una copia
	var copia = new Array(registrosEnGrid);

	// En cada elemento le intego otro arreglo
	for (i = 0; i < registrosEnGrid; i++) {
		copia[i] = new Array(5);
	}
	// llenamos la copia con el grid original
	for (i = 0; i < registrosEnGrid - 1; i++) {
		copia[i]["cell"] = miArraygrid[i]["cell"];
		copia[i]["index"] = miArraygrid[i]["index"];
		copia[i]["flag"] = miArraygrid[i]["flag"];
		copia[i]["editado"] = miArraygrid[i]["editado"];
		copia[i]["icono"] = miArraygrid[i]["icono"];
	}

	// agregamos el nuevo registro
	nuevoIndex = registrosEnGrid - 1;
	copia[nuevoIndex]["cell"] = parseFloat(grid.cells2(nuevoIndex, 5)
			.getValue());
	copia[nuevoIndex]["index"] = nuevoIndex;
	copia[nuevoIndex]["flag"] = false;
	copia[nuevoIndex]["editado"] = false;
	// Valor de boton
	copia[nuevoIndex]["icono"] = grid.cells2(nuevoIndex, 11).getValue();

	miArraygrid = copia;

	miArraygrid.sort(function(a, b) {
		return parseInt(a.cell, 10) - parseInt(b.cell, 10);
	});
}

function borrarBanderasGrid() {
	var items = grid.getRowsNum();
	for ( var i = 0; i < items; i++) {
		grid.cells2(i, 11).setValue("");
	}// fin for
}

function calculaDiferencia() {

	primaDif = jQuery('#primaDif');
	
	
	primaDif.html(redondeo(primaTotal - prima.html()
			- (primaPP - primaPPEnVista), 2));
	
	recargoDif = jQuery('#recargoDif');
	

	recargoDif.html(redondeo(recargoTotal - recargo.html()
			- (recargoPP - recargoPPEnVista), 2));

	derechosDif = jQuery('#derechosDif');
	

	derechosDif.html(redondeo(derechosTotal - derechos.html()
			- (derechosPP - derechosPPEnVista), 2));

	ivaDif = jQuery('#ivaDif');
	

	ivaDif.html(redondeo(ivaTotal - iva.html() - (ivaPP - ivaPPEnVista), 2));

	totalDif = jQuery('#totalDif');
	
   
    //totalPP (total programa pago)
    //totalPPEnVista (total desplegado)
	totalDif.html(redondeo(totalTotal - total.html()
			- (totalPP - totalPPEnVista), 2));
	
	var saldoPrimaNeta = parseFloat(primaDif.html());
	var saldoRecargo = parseFloat(recargoDif.html());
	var saldoDerecho = parseFloat(derechosDif.html());
	var saldoIva = parseFloat(ivaDif.html());
	var saldoTotal = parseFloat(totalDif.html());
   
    //esas variables que comienzan con la palabra saldo se refieren a la diferencia en la vista derivada de movimientos
	//Si saldo prima neta == 0 asignamos los otros saldos al primer recibo
   
	if (saldoPrimaNeta == 0 && (saldoIva != 0 || saldoTotal != 0) ){
		var i1pn = redondeo(parseFloat(grid.cells2(0, 5).getValue()), 2);
		var i1r = redondeo(parseFloat(grid.cells2(0, 6).getValue()), 2);
		var i1d = redondeo(parseFloat(grid.cells2(0, 7).getValue()), 2);
		var i1i = redondeo(parseFloat(grid.cells2(0, 8).getValue()), 2);
		var i1t = redondeo(parseFloat(grid.cells2(0, 9).getValue()), 2);

		var r1 = i1pn + saldoPrimaNeta;
		var r2 = i1r + saldoRecargo;
		var r3 = i1d + saldoDerecho;
		var r4 = i1i + saldoIva;
		var r5 = i1t + saldoTotal;
		
		/** @author MGGE
		 * Se agrega esto para poder editar la prima total de algun recibo y que no se envie la diferencia al primer recibo,
		 * sino que el usuario pueda distribuir la diferencia entre los demas recibos
		 */ 
		if (flagTotalModificado){
			r5 = i1t + (saldoTotal-totalDif.html());
		}

		
		grid.cells2(0, 8).setValue(redondeo(r4,2));
		grid.cells2(0, 9).setValue(redondeo(r5,2));
				
		var d1 = saldoPrimaNeta - saldoPrimaNeta;
		var d2 = saldoRecargo - saldoRecargo;
		var d3 = saldoDerecho - saldoDerecho;
		var d4 = saldoIva  - saldoIva;
		var d5 = saldoTotal - saldoTotal;
		
		
		/** @author MGGE
		 * Se agrega esto para que el saldo de la prima total no se envie al primer recibo, sino que permanezca en los totales
		 * y el usuario pueda verlo y distribuirlo
		 */
		if (flagTotalModificado)
			d5 = saldoTotal;

		ivaDif.html( redondeo(d4,2) );
		totalDif.html( redondeo(d5,2) );
		
		recalculaFooter();
	}	
}

function calculateFooterValues(stage) {
	
	if (stage != 0 && stage != 1 && stage != 2) {
		llenaGlobales();
	}
	//estas variables son las totales de la vista
	prima = jQuery('#prima');
	recargo = jQuery('#recargo');
	derechos = jQuery('#derechos');
	iva = jQuery('#iva');
	total = jQuery('#total');

	var registrosEnGrid = grid.getRowsNum();
  
    // Si no hay rows en el grid (Primera carga, se trae los valores de servicio)
	if (registrosEnGrid > 0) {
		prima.html(redondeo(sumColumn(5), 2));
		recargo.html(redondeo(sumColumn(6), 2));
		derechos.html(redondeo(sumColumn(7), 2));
		iva.html(redondeo(sumColumn(8), 2));
		total.html(redondeo(sumColumn(9), 2));
	    //Se hace la sumatoria de cada columna (se recalculan los footers o primas totales)
	}

	// Si hay rows en el grid, el resultado es la suma de los mismos
	else {
		prima.html(primaPPEnVista);
		recargo.html(recargoPPEnVista);
		derechos.html(derechosPPEnVista);
		iva.html(ivaPPEnVista);
		total.html(totalPPEnVista);

	}

	calculaDiferencia();
	return true;
}

function calculateIvaTotales() {
	for ( var i = 0; i < grid.getRowsNum(); i++) {

		var primacell = parseFloat(grid.cells2(i, 5).getValue());
		var recargoscell = parseFloat(grid.cells2(i, 6).getValue());
		var derechoscell = parseFloat(grid.cells2(i, 7).getValue());
		var primaTcell = parseFloat(grid.cells2(i, 9).getValue());
		
		var iva = redondeo((primacell + recargoscell + derechoscell) * 0.16, 2);
		var total = redondeo((primacell + recargoscell + derechoscell + iva), 2);
	
		// traemos el rowid seleccionado
		// rowId=grid.getSelectedRowId();traemos el rowid seleccionado
		var rowId = grid.getRowId(i);

		// IVA
		grid.cells(rowId, 8).setValue(iva);
		// primatotal
		//agragar funcionalidad para no recalcular total cuando se ha modificado manualmente
		 if(!flagTotalModificado){
			 //el total no fue modificado entonces se recalcula
			 grid.cells(rowId, 9).setValue(total);
		 }

	}

}

/** @author MGGE
 * Se agrega esto para que el saldo de la prima total no se envie al primer recibo, sino que permanezca en los totales
 * y el usuario pueda verlo y distribuirlo
 */
function validarAjusteNegocio(rId, cId, valorViejo){
	var totalModificado= redondeo(sumColumn(cId), 2);
	var diferencia=Math.abs( totalModificado -totalPPEnVista);
	if(  diferencia > ajustePermitidoPrimaTotal)   {
    	alert('El monto de ajuste ha excedido el m\u00E1ximo permitido para el negocio.');
    	grid.cells(rId,cId).setValue(valorViejo);
    	return false;
    }else
    	return true;
}

function cargarGridResultdos(id) {
	var seccionGrid = new dhtmlXGridObject('resultadosGrid');

	seccionGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionGrid.setEditable(true);
	seccionGrid.setSkin("light");

	seccionGrid.setHeader("Prima Neta,Recargos,Derechos,IVA, Prima Total");
	seccionGrid
			.setColumnIds("impPrimaNeta, impRcgosPagoFR, impDerechos, impIVA, impPrimaTotal");
	seccionGrid.setInitWidths("120,120,120,120,120");
	seccionGrid.setColAlign("left,left,left,left,left");
	seccionGrid.setColSorting("str,str,str,str,str");
	seccionGrid.setColTypes("ed,ed,ed,ed,ed");

	seccionGrid.init();

	seccionGrid.load(
			"/MidasWeb/programaPago/obtenerSaldoOriginal.do?idToCotizacion="
					+ id, null, 'json');
}

function cargarProgramaDePagoOriginal(idCotizacion, tm, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {
	tipoMov = tm;
	if (confirm("Se eliminaran los programas de pago actuales y se generar\u00E1 nueva informaci\u00F3n. \u00BFDesea Continuar?")) {
		readOnly = false;
		var url = '/MidasWeb/componente/programapago/cargarProgramaPagoOriginal.action?'+
		'idToCotizacion=' + idCotizacion + 
		'&readOnly=' + readOnly + 
		'&banderaRecuitifica=1'+
		'&tipoMov='+tipoMov+
		'&idToSolicitud='+idToSolicitud+
		'&idToPoliza='+idToPoliza +
		'&idContinuity='+idContinuidad+
		'&tipoFormaPago='+tipoFormaPago+
		'&numeroEndoso='+numeroEndoso;;
		redirectVentanaModal("programaPago", url, null);
	}
}

function cargarProgramasDePago(id, tipoMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {
    gridProgPagos = new dhtmlXGridObject('gridProgramasDePago');
    gridProgPagos.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	gridProgPagos.setEditable(true);
	gridProgPagos.setSkin("light");	
	gridProgPagos.setHeader("Numero Programa Pago, Numero Inciso, Nombre Contratante,No. Recibos,Inicio Programa,Fin Programa,Prima Neta,Recargos,Derechos,IVA,Prima Total,Acciones");
	gridProgPagos.setColumnIds("numProgPago,numInciso,nombreContratante,numRecibos,inicioPrograma,finPrograma,impPrimaNeta,impRcgosPagoFR,impDerechos,impIVA,impPrimaTotal,acciones");
	gridProgPagos.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*");
	gridProgPagos.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left");
	gridProgPagos.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
	gridProgPagos.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");

	gridProgPagos.enablePaging(true,15,3,"paginador",true);
	gridProgPagos.setPagingSkin("bricks");
	//grid.preventIECaching(false);
	gridProgPagos.init();
	gridProgPagos.load(
			'/MidasWeb/componente/programapago/obtenerProgramasDePagoJSON.action?'+
			'idToCotizacion='+ id + 
			'&readOnly=' + readOnly +
			'&tipoMov=' +tipoMov+ 
			'&idToSolicitud=' +idToSolicitud+ 
			'&idToPoliza=' +idToPoliza +
			'&idContinuity='+idContinuidad+
			'&tipoFormaPago='+tipoFormaPago+
			'&numeroEndoso='+numeroEndoso, null, 'json');
	gridProgPagos.setNumberFormat("$0,000.00",5);
	gridProgPagos.setNumberFormat("$0,000.00",6);
	gridProgPagos.setNumberFormat("$0,000.00",7);
	gridProgPagos.setNumberFormat("$0,000.00",8);
	gridProgPagos.setNumberFormat("$0,000.00",9);
	
	gridProgPagos
			.attachFooter(
					"TOTAL COTIZACION,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaInciso'>0</div>,<div id='recargoInciso'>0</div>,<div id='derechosInciso'>0</div>,<div id='ivaInciso'>0</div>,<div id='totalInciso'>0</div>",
					[ "text-align:left;,background-color:#D3F3B4;" ]);

	if (!readOnly) {
		gridProgPagos
				.attachFooter(
						"TOTAL PROGRAMA PAGO,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaPP'>0</div>,<div id='recargoPP'>0</div>,<div id='derechosPP'>0</div>,<div id='ivaPP'>0</div>,<div id='totalPP'>0</div>",
						[ "text-align:left;" ]);		
		gridProgPagos
		.attachFooter(
				"TOTAL INCISO,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaTI'>0</div>,<div id='recargoTI'>0</div>,<div id='derechosTI'>0</div>,<div id='ivaTI'>0</div>,<div id='totalTI'>0</div>",
				[ "text-align:left;" ]);		
		gridProgPagos
				.attachFooter(
						"SALDO O DIFERENCIA,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaDifPP'>0</div>,<div id='recargoDifPP'>0</div>,<div id='derechosDifPP'>0</div>,<div id='ivaDifPP'>0</div>,<div id='totalDifPP'>0</div>",
						[ "text-align:left;" ]);
		gridProgPagos
		.attachEvent("onRowSelect", function(id,ind) {
				//indexSeleccionado = gridProgPagos.getRowId(ind);
				indexSeleccionado = id;
				calculoSaldosIncisos(id);
				
				return true;
			});
	}

}

function calculoSaldosIncisos(idProgPago){
	
	//Saldo de los Recibos de un programa de pago en particular (Original mente Saldo de todos los recibos de la cotizacion)
	getSaldoProgPagoInciso(idProgPago);
	//Saldo de los Recibos de un programa de pago en particular (Original mente Saldo de todos los recibos de la cotizacion)
	getSaldoRecibosInciso(idProgPago);
	//Obtener Saldo programa pago
	obtenerSaldoDiferenciaInciso(idProgPago);
	
	
}

function obtenerSaldoDiferenciaInciso(idProgPago) {
	jQuery
			.ajax( {
				url : "/MidasWeb/componente/programapago/obtenerSaldoDeDiferenciaInciso.action?idProgPago="
						+ idProgPago,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaDifPP = redondeo(data.impPrimaNeta, 2);
					recargoDifPP = redondeo(data.impRcgosPagoFR, 2);
					derechosDifPP = redondeo(data.impDerechos, 2);
					ivaDifPP = redondeo(data.impIVA, 2);
					totalDifPP = redondeo(data.impPrimaTotal, 2);

					jQuery('#primaDifPP').html(primaDifPP);
					jQuery('#recargoDifPP').html(recargoDifPP);
					jQuery('#derechosDifPP').html(derechosDifPP);
					jQuery('#ivaDifPP').html(ivaDifPP);
					jQuery('#totalDifPP').html(totalDifPP);
				}
			});
}
//Obtiene los totales del inciso
function getSaldoRecibosInciso(idProgPago) {

	jQuery
			.ajax( {
				url : "/MidasWeb/componente/programapago/getSaldoRecibosIn.action?idProgPago=" // 
						+ idProgPago,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaPP = redondeo(data.impPrimaNeta, 2);
					recargoPP = redondeo(data.impRcgosPagoFR, 2);
					derechosPP = redondeo(data.impDerechos, 2);
					ivaPP = redondeo(data.impIVA, 2);
					totalPP = redondeo(data.impPrimaTotal, 2);

					jQuery('#primaTI').html(primaPP);
					jQuery('#recargoTI').html(recargoPP);
					jQuery('#derechosTI').html(derechosPP);
					jQuery('#ivaTI').html(ivaPP);
					jQuery('#totalTI').html(totalPP);
					
				}
			});

}

//Va por los totales del programa de pago seleccionado
function getSaldoProgPagoInciso(idProgPago) {
	jQuery
			.ajax( {
				url : "/MidasWeb/componente/programapago/obtenerSaldoProgPagoIn.action?idProgPago=" // 
						+ idProgPago,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaPP = redondeo(data.impPrimaNeta, 2);
					recargoPP = redondeo(data.impRcgosPagoFR, 2);
					derechosPP = redondeo(data.impDerechos, 2);
					ivaPP = redondeo(data.impIVA, 2);
					totalPP = redondeo(data.impPrimaTotal, 2);

					jQuery('#primaPP').html(primaPP);
					jQuery('#recargoPP').html(recargoPP);
					jQuery('#derechosPP').html(derechosPP);
					jQuery('#ivaPP').html(ivaPP);
					jQuery('#totalPP').html(totalPP);
				}
			});
}

function cargarSaldoOriginal(idToCotizacion) {
	idCotizacion = idToCotizacion;

	jQuery.ajax( {
		url : "/MidasWeb/componente/programapago/obtenerSaldoOriginal.action?idToCotizacion="
				+ idToCotizacion,
		dataType : 'json',
		async : false,
		success : function(data) {

			primaTotal = redondeo(data.impPrimaNeta, 2);
			recargoTotal = redondeo(data.impRcgosPagoFR, 2);
			derechosTotal = redondeo(data.impDerechos, 2);
			ivaTotal = redondeo(data.impIVA, 2);
			totalTotal = redondeo(data.impPrimaTotal, 2);
			var porcentaje = redondeo(data.porcentajePagoFraccionado, 2);
			if (porcentaje != 0 ){
				porcentajePagoFraccionado = porcentaje;
			}
		
			
			
			jQuery('#primaInciso').html(primaTotal);
			jQuery('#recargoInciso').html(recargoTotal);
			jQuery('#derechosInciso').html(derechosTotal);
			jQuery('#ivaInciso').html(ivaTotal);
			jQuery('#totalInciso').html(totalTotal);
		}
	});
}

function cargarSaltoOriginalInciso(idToCotizacion, idProgPago) {
	idCotizacion = idToCotizacion;

	jQuery.ajax( {
		url : "/MidasWeb/componente/programapago/obtenerSaldoOriginal.action?idToCotizacion="
				+ idToCotizacion + "&idProgPago=" + idProgPago,
		dataType : 'json',
		async : false,
		success : function(data) {

			primaTotal = redondeo(data.impPrimaNeta, 2);
			recargoTotal = redondeo(data.impRcgosPagoFR, 2);
			derechosTotal = redondeo(data.impDerechos, 2);
			ivaTotal = redondeo(data.impIVA, 2);
			totalTotal = redondeo(data.impPrimaTotal, 2);
			var porcentaje = redondeo(data.porcentajePagoFraccionado, 2);
			if (porcentaje != 0 ){
				porcentajePagoFraccionado = porcentaje;
			}
			jQuery('#prima').html(primaTotal);
			jQuery('#recargo').html(recargoTotal);
			jQuery('#derechos').html(derechosTotal);
			jQuery('#iva').html(ivaTotal);
			jQuery('#total').html(totalTotal);
		}
	});
	

}

function configurarReadOnly() {
	if (readOnly) {
		//jQuery('#linkCargarProgramaOriginal').attr('onclick', '').unbind('click');
		//jQuery('#linkAgregarProgramaPago').attr('onclick', '').unbind('click');
		//jQuery('#b_agregarProgramaPago').hide();
		//jQuery('#b_cargarPPoriginal').hide();
		jQuery('#b_agregarProgramaPago').remove();
		jQuery('#b_cargarPPoriginal').remove();
	} else {
		//jQuery('#b_recuotificar').hide();
		jQuery('#b_recuotificar').remove();
	}
}

function validacionDeBanderas(){
	if (flagDistribuyePrima == '0'){
		borrarBanderasGrid();
	}
}

function configurarReadOnlyRecibos() {
	
	if (readOnly) {
		// Desactivamos los botones de guardar y agregar
		
		//jQuery('#b_agregarRecibo').hide();
		//jQuery('#b_guardarRecibos').hide();
		
		jQuery('#b_agregarRecibo').remove();
		jQuery('#b_guardarRecibos').remove();
		
		// Ocultamos panel de control read only
		//jQuery('#tablaDistribucionAutomatica').hide();
		jQuery('#tablaDistribucionAutomatica').remove();
		
	}
}

function eliminarProgramaPago(idProgPago, idCot) {
	var count = gridProgPagos.getRowsNum();
	if (count == 1) {
		alert("No se puede eliminar. Al menos debe existir un programa de pago");
		return;
	}

	if (confirm("El programa de pago se eliminar\u00E1. \u00BFDesea Continuar?")) {
		// Peticion ajax para eliminar Recibo
		jQuery
				.ajax( {
					type : "GET",
					url : '/MidasWeb/componente/programapago/eliminarProgramaPago.action?&idProgPago=' + idProgPago,
					dataType : "json",
					async : false
				});

		var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?idToCotizacion='
				+ idCot + "&readOnly=" + readOnly;
		redirectVentanaModal("programaPago", url, null);
	}
}

function eliminarRecibo(idNumExhibicion) {
	eliminaRecibo(idNumExhibicion);
}

function eliminaRecibo(idNumExhibicion) {

	var count = grid.getRowsNum();
	if (count == 1) {
		alert("No se puede eliminar. Al menos debe quedar un registro");
		return;
	}

	// traemos el index
	rowIndex = grid.getRowIndex(idNumExhibicion);
	
	// ese index hay que ir a buscarlo al arreglo y borrar la linea completa
	// eliminaElementoArray(rowIndex);
	marcaRowEditado(rowIndex, true);
	
	var count = grid.getRowsNum();
	if (count == rowIndex + 1) {
		if (confirm("Esta seguro de eliminar el registro?")) {
			var fechaVencimiento = grid.cells2(rowIndex, 4).getValue();
			
			var rid = grid.getRowId(rowIndex - 1);
			grid.cells(rid, 4).setValue(fechaVencimiento);
			grid.deleteRow(idNumExhibicion);
			calculateFooterValues(0);
		}
	} else {
		alert("No se puede eliminar. Puede eliminar el Ultimo registro solamente");
	}
}

function eventoRadio(radio) {
	flagDistribuyePrima = radio.value;

	if (flagDistribuyePrima == '1') {
		
		//Si hay saldos negativos, alerta, no reinicia
		if(!validaSaldosNegativos() || !validaSaldosPositivos()){
			var bolSi = jQuery("#distribuyeSi");
			bolSi.attr('checked', false);
			var bolNo = jQuery("#distribuyeNo");
			bolNo.attr('checked', true);
			flagDistribuyePrima = '0';
			alert("No es posible iniciar la distribuci\u00F3n autom\u00E1tica, No pueden existir saldos");
		}else{
			if (confirm("Al aplicar la distribuci\u00F3n autom\u00E1tica cualquier modificaci\u00F3n a la prima puede alterar los valores definidos previamente. \u00BFDesea continuar?")){
				inicializaMatrizDistribucion(5);
				actualizarBanderas();
			}
		}
	} else {
		borrarBanderasGrid();
	}
}

function eventoRadioRecargos(radio) {
	flagDistribuyeRecargos = radio.value;
	
	if (flagDistribuyeRecargos == '1'){
		
		var saldoRecargo = parseFloat(jQuery('#recargoDif').html());
		if (saldoRecargo != 0){
			var bolSi = jQuery("#distribuyeRecargosSi");
			bolSi.attr('checked', false);
			var bolNo = jQuery("#distribuyeRecargosNo");
			bolNo.attr('checked', true);
			flagDistribuyeRecargos = '0';
			alert("No es posible iniciar la distribuci\u00F3n autom\u00E1tica de recargos, No pueden existir saldos");
			return;
		}else{
			actualizarEdicionDeGrid();
		}
	}else{
		actualizarEdicionDeGrid();
	}
}


function eventoRadioTipoCorte(radio) {
	flagTipoCorte = radio.value;
}

function actualizarEdicionDeGrid(){
	//si ambas estan activas, ambas columnas son read only
	if (flagDistribuyeRecargos == '1' && flagDistribuyeDerechos == '1'){
		grid.setColTypes("ro,ro,ro,ed,ro,edn,ron,ron,ron,ron,ro,ro");
	//solo read only derechos
	}else if (flagDistribuyeRecargos == '0' && flagDistribuyeDerechos == '1'){
		grid.setColTypes("ro,ro,ro,ed,ro,edn,edn,ron,ron,ron,ro,ro");
	//Solo editable recargos
	}else if (flagDistribuyeRecargos == '1' && flagDistribuyeDerechos == '0'){
		grid.setColTypes("ro,ro,ro,ed,ro,edn,ron,edn,ron,ron,ro,ro");
	//ambas editables
	}else{
		grid.setColTypes("ro,ro,ro,ed,ro,edn,edn,edn,ron,ron,ro,ro");
	}
}

function eventoRadioDerechos(radio) {
	flagDistribuyeDerechos = radio.value;
	
	//se se esta activando, se toman los totales 
	if(flagDistribuyeDerechos == '1'){
	
		//Validamos que no exista saldo de derechos
		var saldoDerecho = parseFloat(jQuery('#derechosDif').html());
		if (saldoDerecho != 0){
			var bolSi = jQuery("#distribuyeDerechosSi");
			bolSi.attr('checked', false);
			var bolNo = jQuery("#distribuyeDerechosNo");
			bolNo.attr('checked', true);
			flagDistribuyeDerechos = '0';
			alert("No es posible activar la distribuci\u00F3n de Derechos. Existe Saldo de derechos.");
			return;
		}else{
			if (confirm("El valor de derechos ser\u00E1 distribuido de forma automatica en los recibos. \u00BFDesea Continuar?")){
				recalculaDerechos();
			}
		}
	}else{
		actualizarEdicionDeGrid();
	}
}

function recalculaRecargos(){
	var recibos = grid.getRowsNum();
	
	var sumatoriaRecargosCalculados = 0;

	for (var i = 0; i < grid.getRowsNum(); i++ ){

		var primaNeta = grid.cells2(i, 5).getValue();
		if(porcentajePagoFraccionado > 0 ){
			var nuevoRecargo = redondeo (((primaNeta * porcentajePagoFraccionado) / 100), 2);

		}else{
			var nuevoRecargo = 0;

		}
		sumatoriaRecargosCalculados = sumatoriaRecargosCalculados + nuevoRecargo;
		var rid = grid.getRowId(i);
		grid.cells(rid, 6).setValue(nuevoRecargo);
	}
	
	
	var dif = recargoPP - sumatoriaRecargosCalculados;
	var recargosPrimerRecibo = parseFloat(grid.cells2(0, 6).getValue());
	var nrc = recargosPrimerRecibo + dif;
	var nuevoRecargoCalc = redondeo(nrc,2);
	grid.cells(grid.getRowId(0), 6).setValue(nuevoRecargoCalc);
    console.log('##recalculaRecargos|'+dif+'|'+recargosPrimerRecibo+'|'+nrc+'|'+nuevoRecargoCalc);

	//Seteamos valor read only a la columna
	actualizarEdicionDeGrid();
}

function recalculaDerechos(){
	// obtenemos el total de derechos del programa de pago
	// obtenemos el total de registros en el grid
	var recibos = grid.getRowsNum();
	
	var valorPorRecibo = redondeo(parseFloat(derechosPP / recibos), 2);
	var sumatoria = valorPorRecibo * recibos;
	var diferencia = derechosPP - sumatoria;
	
	var valorPrimerRecibo = redondeo(valorPorRecibo + diferencia, 2);
	
	var sumatoriaDos = (valorPorRecibo * (recibos - 1)) + valorPrimerRecibo;
	
	var difDos = derechosPP - sumatoriaDos;

	//actualizamos los registros en el grid
	for ( var i = 0; i < grid.getRowsNum(); i++) {
		var rid = grid.getRowId(i);
		//si es el primer recibo, seteamos el valor de primer recibo
		if (i == 0){
			grid.cells(rid, 7).setValue(valorPrimerRecibo);
		}else{
			grid.cells(rid, 7).setValue(valorPorRecibo);
		}
	}
	//Seteamos valor read only a la columna
	actualizarEdicionDeGrid();
	calculateIvaTotales();
	calculateFooterValues(0);
}

function sumColumn(ind) {
	var out = 0;
	for ( var i = 0; i < grid.getRowsNum(); i++) {
		var valorEnRow = grid.cells2(i, ind).getValue();
		out += redondeo(parseFloat(valorEnRow), 2);
	}
	return out;
}


function validaSaldosNegativos(){	
	var saldoPrimaNeta = parseFloat(jQuery('#primaDif').html());
	var saldoRecargo = parseFloat(jQuery('#recargoDif').html());
	var saldoDerecho = parseFloat(jQuery('#derechosDif').html());
	var saldoIva = parseFloat(jQuery('#ivaDif').html());
	var saldoTotal = parseFloat(jQuery('#totalDif').html());
	
	if (saldoPrimaNeta < 0 || saldoRecargo < 0 || saldoDerecho < 0 || saldoIva < 0 || saldoTotal < 0){
		return false;
	}else{
		return true;
	}
}

function validaSaldosPositivos(){	
	var saldoPrimaNeta = parseFloat(jQuery('#primaDif').html());
	var saldoRecargo = parseFloat(jQuery('#recargoDif').html());
	var saldoDerecho = parseFloat(jQuery('#derechosDif').html());
	var saldoIva = parseFloat(jQuery('#ivaDif').html());
	var saldoTotal = parseFloat(jQuery('#totalDif').html());
	
	if (saldoPrimaNeta > 0 || saldoRecargo > 0 || saldoDerecho > 0 || saldoIva > 0 || saldoTotal > 0){
		return false;
	}else{
		return true;
	}
}

/**
 * @author MGGE
 * Método auxiliar para validar que no haya saldo en prima total al guardar el pp (programa de pago)
 */
function tieneSaldoPT(){	
	var saldoTotal = parseFloat(jQuery('#totalDif').html());

	if (saldoTotal != 0){
		return true;
	}else{
		return false;
	}
}

/**
 * @author MGGE
 * Método auxiliar para validar si hay saldo en prima neta al guardar el pp (programa de pago)
 */
function tieneSaldoPN(){	
	var saldoPrimaNeta = parseFloat(jQuery('#primaDif').html());
	if (saldoPrimaNeta == 0){
		return false;
	}else{
		return true;
	}
}

function existenSaldosEnProgramaDePago(){	
	var saldoPrimaNeta = parseFloat(jQuery('#primaDifPP').html());
	var saldoRecargo = parseFloat(jQuery('#recargoDifPP').html());
	var saldoDerecho = parseFloat(jQuery('#derechosDifPP').html());
	var saldoIva = parseFloat(jQuery('#ivaDifPP').html());
	var saldoTotal = parseFloat(jQuery('#totalDifPP').html());
	
	if (saldoPrimaNeta == 0 && saldoRecargo == 0 && saldoDerecho == 0 && saldoIva == 0 && saldoTotal == 0){
		return false;
	}else{
		return true;
	}
}


function generaDistribucion(saldo, rindex, array) {
	var numNe = getRowsNoEditados();

	if (numNe != 0) {
		var pR = redondeo(saldo / numNe, 2);
		var rem = 0;

		// Pido un ID Falso
		var rindex = getRowFalso();
		
		var valorActualDeCelda = array[rindex]["cell"];
		// le sumamos el valor a aumentar
		array[rindex]["cell"] = redondeo(array[rindex]["cell"] + pR, 2);
		var nuevaSumatoria = array[rindex]["cell"];
		var rid = grid.getRowId(array[rindex]["index"]);
		// grid.cells(
		// miArraygrid[rindex]["index"],5).cell.innerHTML=miArraygrid[rindex]["cell"]
		// + pR;

		// alert("voy a setear"+miArraygrid[rindex]["cell"] + pR);
		grid.cells(rid, 5).setValue(array[rindex]["cell"]);
		// alert("ahora le sumo el proporcional "+rindex+"vale
		// "+miArraygrid[rindex]["cell"]);

		if (array[rindex]["cell"] < 0) {
			rem = array[rindex]["cell"] - 1;
			array[rindex]["cell"] = 1;
			grid.cells(rid, 5).setValue(1);

		}
		// alert("mi remanenete es "+rem);
		// aqui vamos a settear el valor del grid con el valor nuevo de la cell

		var saldonuevo = saldo - pR + rem;
		saldonuevo = redondeo(parseFloat(saldonuevo),2);
		// alert("saldonuevo "+saldonuevo);
		marcaRowEditado(array[rindex]["index"], false);
		generaDistribucion(saldonuevo, rindex, array);

	}
}

function getAllRowsAsJson(vargrid) {

	var json = "[";

	for ( var rowIndex = 0; rowIndex < vargrid.getRowsNum(); rowIndex++) {
		json = json + "{id:" + vargrid.getRowId(rowIndex);
		for ( var cellIndex = 0; cellIndex < vargrid.getColumnsNum() - 2; cellIndex++) {
			var colId = vargrid.getColumnId(cellIndex);
			
			json = json + "," + colId + ':"'
					+ vargrid.cells2(rowIndex, cellIndex).getValue() + '"';
			
		}
		if (rowIndex < (vargrid.getRowsNum() - 1)) {
			json = json + "},";
		} else {
			json = json + "}";
		}
	}
	json = json + "]";

	return json.toString();
}

function getGridRecibos(idToCotizacion, idProgramaPago) {
   
	/** * @author MGGE * Se hace editable la columna de Prima Total (la 9, comenzando a contar en 0)* **/
    var colTypes="ro,ro,ro,ed,ro,edn,edn,edn,ron,"+(hasEditPrimaTotalPermit?"edn":"ro")+",ro,ro";
	grid = new dhtmlXGridObject('reciboListadoGrid');

	grid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	grid.setEditable(true);
	grid.setSkin("light");

	// Si no es read Only, lo mostramos normal
	if (!readOnly) {
		grid.setHeader("No. Exhibición,No. Folio,Fecha Vencimiento,Inicio Programa,Fin Programa,Prima Neta,Recargos,Derechos,IVA,Prima Total,Acciones,Dist. en prima");
		grid.setColumnIds("numExhibicion,cveRecibo,fechaVencimiento,fechaCubreDesde,fechaCubreHasta,impPrimaNeta,impRcgosPagoFR,impDerechos,impIVA,impPrimaTotal,acciones,editado");
		grid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*");
		grid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left");
		grid.setColSorting("int,na,na,na,na,na,na,na,na,na,na,na");
	
		grid.attachFooter("PRIMAS TOTALES,#cspan,#cspan,#cspan,#cspan,<div id='prima'>0</div>,<div id='recargo'>0</div>,<div id='derechos'>0</div>,<div id='iva'>0</div>,<div id='total'>0</div>",[ "text-align:right; font-weight:bold;" ]);
		grid.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,#cspan,#cspan,<div id='primaDif'>0</div>,<div id='recargoDif'>0</div>,<div id='derechosDif'>0</div>,<div id='ivaDif'>0</div>,<div id='totalDif'>0</div>",[ "text-align:right; font-weight:bold;" ]);	
		/** * @author MGGE * Se hace editable la columna de Prima Total (la 9, comenzando a contar en 0)* **/
		grid.setColTypes(colTypes);
		
	// Si si, eliminamos headers de acciones y distribucion de prima
	} else {
		grid.setHeader("No. Exhibición,No. Folio,Fecha Vencimiento,Inicio Programa,Fin Programa,Prima Neta,Recargos,Derechos,IVA,Prima Total");
		grid.setColumnIds("numExhibicion,cveRecibo,fechaVencimiento,fechaCubreDesde,fechaCubreHasta,impPrimaNeta,impRcgosPagoFR,impDerechos,impIVA,impPrimaTotal");
		grid.setInitWidths("*,*,*,*,*,*,*,*,*,*");
		grid.setColAlign("left,left,left,left,left,left,left,left,left,left");
		grid.setColSorting("int,na,na,na,na,na,na,na,na,na");
		grid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	
   

	cargarSaltoOriginalInciso(idToCotizacion, idProgramaPago);	
	getSaldoRecibosInciso(idProgramaPago);

	//obtenemos los totales del programa de pago desglosado
	obtenerSaldoDeProgramaDePagoDesglosado(idProgramaPago);
	
	grid.load("/MidasWeb/componente/programapago/obtenerRecibosJSON.action?idToCotizacion="
			+ idToCotizacion + "&idProgPago=" + idProgramaPago + "&readOnly="
			+ readOnly, null, 'json');

	grid.setNumberFormat("$0,000.00",5);
	grid.setNumberFormat("$0,000.00",6);
	grid.setNumberFormat("$0,000.00",7);
	grid.setNumberFormat("$0,000.00",8);
	grid.setNumberFormat("$0,000.00",9);

	grid
			.attachEvent("onEditCell", function(stage, rId, cInd, nValue,
					oValue) {
			    console.log('##getGridRecibos-onEditCell|'+stage+'|'+rId.html+'|'+cInd+'|'+nValue+'|'+oValue);

				var tamArray = miArraygrid.length;
				var indexActual = grid.getRowIndex(rId);

				// Si el array esta en cero lo inicializamos
					if (tamArray == 0) {
						calculateFooterValues();
					}

					// alert('stage'+stage);
					if (stage == 2) {
						/*** @author MGGE * Se agrega la columna de Prima Total a la validacion de celdas numericas* ***/
						// Validacion de valor numerico para columnas 5,6,7
						if (cInd == 5 || cInd == 6 || cInd == 7|| cInd == 9) {
							/*
							if (nValue == ''){
								alert("Debe introducir un valor. Verifique.");
								grid.cells(rId, cInd).setValue(oValue);
								return false;
							}
							**/
						  
							if (!isNumber(nValue)) {
								alert("Valor ingresado no es numérico o excede el número de decimales permitidas. Verifique.");
								grid.cells(rId, cInd).setValue(oValue);
								return false;
							} else {
								if (!(validanegativos(nValue))) {
									return false;
								}
							}
						}

						// Validacion para fechas
						if (cInd == 2 || cInd == 3 || cInd == 4) {

							//No dejar editar la fecha de incio del primer recibo
							//si modificamos la columna de fecha de inicio
							if (cInd == 3){
								//Si index == 0  -> primer recibo, no se deja editar
								if (indexActual == 0){
									alert("No es posible ajustar la fecha de Inicio de Programa del Primer Recibo.");
									grid.cells(rId, cInd).setValue(oValue);
									return false;
								}
							}
							
							// Si el valor nuevo no presenta formato de fecha,
							// va para atras
							if (!isDate(nValue)) {
								alert("Valor ingresado no presenta formato de fecha dd/MM/yyyy. Verifique.");
								grid.cells(rId, cInd).setValue(oValue);
								return false;
							}
							// Si es valor correcto, validamos valor anterior de
							// la celda
							else {
								// Si el valor anterior era vacio, hay que
								// autocompletar recibo anterior o posterior
								if (oValue == '') {

									// Si inserto la fecha de fin de programa(columna Fin Prog)
									// Insertamos fecha inicial y de vencimiento en recibo posterior
									//OBSOLETO, YA NO SE PUEDE EDITAR LA FECHA FINAL
									/*
									if (cInd == 4) {
										var fechaInicial = getDateFromString(grid
												.cells2(indexActual, 3)
												.getValue());
										var fechaFinal = getDateFromString(grid
												.cells2(indexActual + 1, 4)
												.getValue());
										var inDate = getDateFromString(nValue);


										if (!isDateBefore(fechaInicial, inDate)
												|| !isDateAfter(fechaFinal,
														inDate)) {
											alert("La fecha ingresada no esta dentro del rango permitido: "
													+ getStringFromDate(fechaInicial)
													+ " - "
													+ getStringFromDate(fechaFinal));
											grid.cells(rId, cInd).setValue(
													oValue);
											return false;
										}

										// Insertamos fecha de vencimiento
										grid.cells2(indexActual + 1, 2)
												.setValue(nValue);
										// Insertamos fecha inicial
										grid.cells2(indexActual + 1, 3)
												.setValue(nValue);
									}
									*/

									// Validamos que valor caiga dentro del rango
									if (cInd == 3 || cInd == 2) {
										var initialDate = getDateFromString(grid.cells2(indexActual - 1, 3).getValue());
										var finalDate = getDateFromString(grid.cells2(indexActual, 4).getValue());
										var inDate = getDateFromString(nValue);
										
										//Validacion de fecha al agregar recibo
										newDateArray = getDateArray(nValue);
										var dia = parseInt(newDateArray[0]);
										var mes = parseInt(newDateArray[1]) - 1;
										var anio = parseInt(newDateArray[2]);
										var esFechaValida = isValid(dia, mes, anio);
										if (!esFechaValida){
											alert("La fecha ingresada no es valida. Verifique.");
											grid.cells(rId, cInd).setValue(oValue);
											return false;
										}
										

										//Si el corte es am, 11.59 am, puede ser el mismo dia, si es pm 11.59 pm, se debe acortar la vigencia un dia
									    if (flagTipoCorte == "pm"){
									    	initialDate = addDaysToDate(initialDate, 1);
									    }

										if (!isDateBefore(initialDate, inDate) || !isDateAfter(finalDate, inDate)) {
											alert("La fecha ingresada no esta dentro del rango permitido: "
													+ getStringFromDate(initialDate)
													+ " - "
													+ getStringFromDate(finalDate));
											grid.cells(rId, cInd).setValue(oValue);
											return false;
										}
									}

									// si es la fecha desde, actualizamos fecha
									// de vencimiento y hasta de row anterior
									if (cInd == 3) {
										grid.cells2(indexActual, 2).setValue(nValue);
										//grid.cells2(indexActual - 1, 4).setValue(nValue);
										
										if (flagTipoCorte == "am"){
											grid.cells2(indexActual - 1, 4).setValue(nValue);
										}else{
											var calculatedDate = substractDaysToDate(inDate, 1); 
											grid.cells2(indexActual - 1, 4).setValue(getStringFromDate(calculatedDate));
										}
									
									}
									//Si es la fecha de vencimiento, actualizamos fecha desde y hasta
									//OBSOLETO, FECHA VENCIMIENTO YA NO SE EDITA
									/*
									if (cInd == 2) {
										grid.cells2(indexActual, 3).setValue(
												nValue);
										grid.cells2(indexActual - 1, 4)
												.setValue(nValue);
									}
									*/
								} 
								
								else{
									//Obtenemos fechas de mismo row
									var initialDate = getDateFromString(grid.cells2(indexActual - 1, 3).getValue());
									var finalDate = getDateFromString(grid.cells2(indexActual, 4).getValue());
									var inDate = getDateFromString(nValue);
								
								    newDateArray = getDateArray(nValue);
								    var dia = parseInt(newDateArray[0]);
								    var mes = parseInt(newDateArray[1]) - 1;
								    var anio = parseInt(newDateArray[2]);
								    var esFechaValida = isValid(dia, mes, anio);
									if (!esFechaValida){
										alert("La fecha ingresada no es valida. Verifique.");
										grid.cells(rId, cInd).setValue(oValue);
										return false;
									}
									
									//Si el corte es am, 11.59 am, puede ser el mismo dia, si es pm 11.59 pm, se debe acortar la vigencia un dia
								    if (flagTipoCorte == "pm"){
								    	initialDate = addDaysToDate(initialDate, 1);
								    }
									
									if (!isDateBefore(initialDate, inDate) || !isDateAfter(finalDate, inDate)) {
										alert("La fecha ingresada no esta dentro del rango permitido: "
												+ getStringFromDate(initialDate)
												+ " - "
												+ getStringFromDate(finalDate));
										grid.cells(rId, cInd).setValue(oValue);
										return false;
									//actualizamos la fecha de vencimiento del registro actual, y la fecha final anterior
									}else{
										grid.cells2(indexActual, 2).setValue(nValue);
										//grid.cells2(indexActual - 1, 4).setValue(nValue);
									
										if (flagTipoCorte == "am"){
											grid.cells2(indexActual - 1, 4).setValue(nValue);
										}else{
											var calculatedDate = substractDaysToDate(inDate, 1);
											grid.cells2(indexActual - 1, 4).setValue(getStringFromDate(calculatedDate));
										}
									}
								}
								
								
								//OBSOLETO, LA COLUMNA DOS YA NO ES EDITABLE
								/*
								if (cInd == 2 && oValue != ""){
									var fechaDesde = grid.cells2(indexActual, 3).getValue();
									var fechaHasta = grid.cells2(indexActual - 1, 4).getValue();
									
									if ( !isDate(fechaDesde) || !isDate(fechaHasta) || !isDateBefore(getDateFromString(fechaDesde), getDateFromString(nValue)) || !isDateAfter(getDateFromString(fechaHasta), getDateFromString(nValue))  ){
										alert("La fecha ingresada no es valida si no se encuentra dentro del periodo de pago del recibo. Verifique.");
										grid.cells2(indexActual, 2).setValue(oValue);
									}		
								}*/

							}
						}

						if (nValue != oValue && cInd == 5) {
						 
							if (flagDistribuyePrima == '1') {

								var rindex = grid.getRowIndex(rId);
								// VALIDAR QUE NO SOBRE EN POSIBLE DISTRIBUCION
								if (validarDistribucion(nValue, rindex)) {
									
									var saldo = oValue - nValue;
									saldo = redondeo(parseFloat(saldo), 2);
									
									inicializaFlag();
									marcaRowEditado(rindex, true);
									generaDistribucion(saldo, rindex,
											miArraygrid);

								} else {
									alert("El monto capturado excede el total de programa de pago y no podr\u00E1 generar una distribuci\u00F3n uniforme de recibos.");
									grid.cells(rId, cInd).setValue(oValue);
								}
							}
						}
						/*** @author MGGE * Esto se agrega para poder agregar modificar el monto de prima total de algun recibo* ***/
						if (nValue != oValue && cInd == 9 && validarAjusteNegocio(rId,cInd,oValue)) {
						   flagTotalModificado=true;
						    calculateIvaTotales();
							calculateFooterValues(0);

						}
						if(flagDistribuyeRecargos == '1'){
							recalculaRecargos();
						}
						/*** @author MGGE * Esto se agrega para que no recalcule el monto de la prima total cuando se hizo un cambio en algun recibo* ***/
						
						if (nValue != oValue &&  cInd != 9) {
						   if(flagTotalModificado){
						    	alert('Se perderan los ajustes realizados a la columna Prima Total.');
						    }
						    flagTotalModificado=false;
							calculateIvaTotales();
							calculateFooterValues(0);
							
						}
						// Actualizamos las banderas en el grid
						actualizarBanderas();
					}
					return true;
				});
	
	grid.enablePaging(true,15,3,"paginador",true);
	grid.setPagingSkin("bricks");
	
	grid.init();
	
	calculateFooterValues();
}

function getRowFalso() {
	var out = null;
	for (i = 0; i < miArraygrid.length; i++) {
		if (miArraygrid[i]["flag"] == false
				&& miArraygrid[i]["editado"] == false) {
			out = i;
			return out;
		}
	}
	return out;

}

function getRowsNoEditados() {

	var items = miArraygrid.length;
	var out = 0;
	for (i = 0; i < items; i++) {
		if (miArraygrid[i]["flag"] == false
				&& miArraygrid[i]["editado"] == false) {
			out += 1;
		}
	}
	return out;
}

function getSaldoPeriodoPago(idToCotizacion) {
	jQuery
			.ajax( {
				url : "/MidasWeb/componente/programapago/obtenerSaldoPeriodoDePago.action?idToCotizacion="
						+ idToCotizacion,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaPP = redondeo(data.impPrimaNeta, 2);
					recargoPP = redondeo(data.impRcgosPagoFR, 2);
					derechosPP = redondeo(data.impDerechos, 2);
					ivaPP = redondeo(data.impIVA, 2);
					totalPP = redondeo(data.impPrimaTotal, 2);

					jQuery('#primaPP').html(primaPP);
					jQuery('#recargoPP').html(recargoPP);
					jQuery('#derechosPP').html(derechosPP);
					jQuery('#ivaPP').html(ivaPP);
					jQuery('#totalPP').html(totalPP);
				}
			});
}

function guardarRecibo(idProgPago,tipoMov,idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {
	if (!validaFechasVaciasEnGrid()) {
		return false;
	}

	if(!validaSaldosNegativos()){
		alert("No es posible guardar la recuotificaci\u00F3n actual. Existen saldos Negativos.");
		return false;
	}
	
	/** * @author MGGE * Se agrega esto para forzar al usuario a que cualquier ajuste que haga solo en la columna PT (Prima Total) cuadre**/
	if(tieneSaldoPT() && !tieneSaldoPN()){
	    alert("No es posible guardar la recuotificaci\u00F3n actual. Existe saldo en la calumna Prima Total.");
		return false;
	}
	
	if (validaGuardar()) {
		var jsonx = getAllRowsAsJson(grid);
		
		jQuery
				.ajax( {
					type : "GET",
					url : '/MidasWeb/componente/programapago/guardarRecibo.action?listJsonRecibosEditada='
							+ jsonx + '&idProgPago=' + idProgPago,
					dataType : "json",
					async : false
				});
	}
	
	var url = '/MidasWeb/componente/programapago/mostrarRecibos.action?'+
	          'idToCotizacion='+ idCotizacion + 
	          '&idProgPago=' + idProgPago +
			  '&tipoMov='+tipoMov+
			  '&idToSolicitud='+idToSolicitud+
			  '&idToPoliza='+idToPoliza +
			  '&idContinuity='+idContinuidad+
			  '&tipoFormaPago='+tipoFormaPago+
			  '&numeroEndoso='+numeroEndoso;
	redirectVentanaModal("programaPago", url, null);
}

function imprimirProgramaPago(idCotizacion) {
	var location = "/MidasWeb/componente/programapago/imprimirProgramaPagos.action?idToCotizacion="
			+ idCotizacion;
	window.open(location, "ImpresionProgramasDePago_" + idCotizacion);
}

function inicializaFlag() {
	for (i = 0; i < miArraygrid.length; i++) {
		miArraygrid[i]["flag"] = false;
	}
}

function inicializaMatrizDistribucion(ind) {
    var items = grid.getRowsNum();
	// Declaro mi arreglo unidimiencional
	miArraygrid = new Array(items);

	// En cada elemento le intego otro arreglo
	for (i = 0; i < items; i++) {
		miArraygrid[i] = new Array(5);
	}
	// lo llenamos
	for (i = 0; i < items; i++) {
		miArraygrid[i]["cell"] = parseFloat(grid.cells2(i, ind).getValue());
		miArraygrid[i]["index"] = i;
		miArraygrid[i]["flag"] = false;
		miArraygrid[i]["editado"] = false;
		// Valor de boton
		
		var rowId = grid.getRowId(i);

		miEditado = botonNoEditado;
		miEditado = miEditado.replace("rowId", rowId);
		miArraygrid[i]["icono"] = miEditado;
	}

	miArraygrid.sort(function(a, b) {
		return parseInt(a.cell, 10) - parseInt(b.cell, 10);
	});

}

function liberarRow(rowId) {
	var rindex = grid.getRowIndex(rowId);
	
	registrosEnArray = miArraygrid.length;

	for (i = 0; i < registrosEnArray; i++) {
		if (miArraygrid[i]["index"] == rindex) {
			miArraygrid[i]["flag"] = false;
			miArraygrid[i]["editado"] = false;
			
			miEditado = botonNoEditado;
			miEditado = miEditado.replace("rowId", rowId);
			miArraygrid[i]["icono"] = miEditado;
			
			var valorActualEnCelda = redondeo(parseFloat(grid.cells2(rindex, 5).getValue()), 2);
			miArraygrid[i]["cell"] = valorActualEnCelda;
		}
	}
	actualizarBanderas();
}

function llenaGlobales() {
	inicializaMatrizDistribucion(5);
}

function marcarRow(rowId) {
	var rindex = grid.getRowIndex(rowId);
	registrosEnArray = miArraygrid.length;
	
	if(registrosEnArray <= 0){
		calculateFooterValues();	
	}
	registrosEnArray = miArraygrid.length;
	var rindex = grid.getRowIndex(rowId);
	
	for (i = 0; i < registrosEnArray; i++) {
		if (miArraygrid[i]["index"] == rindex) {
			miArraygrid[i]["flag"] = true;
			miArraygrid[i]["editado"] = true;
			var valorActualEnCelda = redondeo(parseFloat(grid.cells2(rindex, 5).getValue()), 2);
			miArraygrid[i]["cell"] = valorActualEnCelda;
						
			miEditado = botonEditado;
			miEditado = miEditado.replace("rowId", rowId);
			miArraygrid[i]["icono"] = miEditado;
		}
	}
	actualizarBanderas();
}

function marcaRowEditado(rIndex, editado) {

	var items = miArraygrid.length;
	for (i = 0; i < items; i++) {
		if (miArraygrid[i]["index"] == rIndex) {
			miArraygrid[i]["flag"] = true;
			miArraygrid[i]["editado"] = editado;

			var rowId = grid.getRowId(rIndex);
			
			if (editado) {
				miEditado = botonEditado;
				miEditado = miEditado.replace("rowId", rowId);
			} else {
				miEditado = botonNoEditado;
				miEditado = miEditado.replace("rowId", rowId);
			}
			miArraygrid[i]["icono"] = miEditado;
		}
	}
}

function mostrarRecibos(idProgPago, idCot, tipoMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {

	var url = '/MidasWeb/componente/programapago/mostrarRecibos.action?'+
	          'idToCotizacion='+ idCot + 
	          '&idProgPago=' + idProgPago + 
	          '&readOnly='+ readOnly +
			  '&tipoMov=' + tipoMov + 
			  '&idToSolicitud=' + idToSolicitud + 
			  '&idToPoliza=' + idToPoliza +
			  '&idContinuity='+idContinuidad+
			  '&tipoFormaPago='+tipoFormaPago+
			  '&numeroEndoso='+numeroEndoso;

	redirectVentanaModal("programaPago", url, null);
}

function obtenerSaldoDeProgramaDePagoDesglosado(idProgPago) {

	jQuery.ajax( {
				url : "/MidasWeb/componente/programapago/obtenerSumatoriaDeUnProgramaPago.action?idProgPago="
						+ idProgPago,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaPPEnVista = redondeo(data.impPrimaNeta, 2);
					recargoPPEnVista = redondeo(data.impRcgosPagoFR, 2);
					derechosPPEnVista = redondeo(data.impDerechos, 2);
					ivaPPEnVista = redondeo(data.impIVA, 2);
					totalPPEnVista = redondeo(data.impPrimaTotal, 2);
					
					
			}
			});

}

function obtenerSaldoDiferencia(idToCotizacion) {
	jQuery
			.ajax( {
				url : "/MidasWeb/componente/programapago/obtenerSaldoDeDiferencia.action?idToCotizacion="
						+ idToCotizacion,
				dataType : 'json',
				async : false,
				success : function(data) {

					primaDifPP = redondeo(data.impPrimaNeta, 2);
					recargoDifPP = redondeo(data.impRcgosPagoFR, 2);
					derechosDifPP = redondeo(data.impDerechos, 2);
					ivaDifPP = redondeo(data.impIVA, 2);
					totalDifPP = redondeo(data.impPrimaTotal, 2);

					
					jQuery('#primaDifPP').html(primaDifPP);
					jQuery('#recargoDifPP').html(recargoDifPP);
					jQuery('#derechosDifPP').html(derechosDifPP);
					jQuery('#ivaDifPP').html(ivaDifPP);
					jQuery('#totalDifPP').html(totalDifPP);
				}
			});
}

function ocultarVentanaRecibos() {
	parent.dhxWins.window("MostrarRecibos").setModal(false);
	parent.dhxWins.window("MostrarRecibos").hide();
}

function recalculaTotalesGridProgPago(cIndex) {
	var out = 0;
	for ( var i = 0; i < gridProgPagos.getRowsNum(); i++) {
		var valorEnRow = gridProgPagos.cells2(i, cIndex).getValue();
		out += redondeo(parseFloat(valorEnRow), 2);
	}
	return out;
}

function recalculaTotalesProgramaPago() {

	// Recalculamos totales de programas de pago, en base a valores en grid
	primaPP = recalculaTotalesGridProgPago(5);
	recargoPP = recalculaTotalesGridProgPago(6);
	derechosPP = recalculaTotalesGridProgPago(7);
	ivaPP = recalculaTotalesGridProgPago(8);
	totalPP = recalculaTotalesGridProgPago(9);

	// Seteamos nuevos totales a segunda columna de footer
	jQuery('#primaPP').html(primaPP);
	jQuery('#recargoPP').html(recargoPP);
	jQuery('#derechosPP').html(derechosPP);
	jQuery('#ivaPP').html(ivaPP);
	jQuery('#totalPP').html(totalPP);

	// Calculamos diferiencia
	primaDifPP = redondeo((primaTotal - primaPP), 2);
	recargoDifPP = redondeo((recargoTotal - recargoPP), 2);
	derechosDifPP = redondeo((derechosTotal - derechosPP), 2);
	ivaDifPP = redondeo((ivaTotal - ivaPP), 2);
	totalDifPP = redondeo((totalTotal - totalPP), 2);

	// Seteamos a footer la diferiencia nueva
	jQuery('#primaDifPP').html(primaDifPP);
	jQuery('#recargoDifPP').html(recargoDifPP);
	jQuery('#derechosDifPP').html(derechosDifPP);
	jQuery('#ivaDifPP').html(ivaDifPP);
	jQuery('#totalDifPP').html(totalDifPP);

}

function recalculaFooter(){
	
	prima = jQuery('#prima');
	recargo = jQuery('#recargo');
	derechos = jQuery('#derechos');
	iva = jQuery('#iva');
	total = jQuery('#total');

	var registrosEnGrid = grid.getRowsNum();

	// Si no hay rows en el grid (Primera carga, se trae los valores de
	// servicio)
	if (registrosEnGrid > 0) {
		prima.html(redondeo(sumColumn(5), 2));
		recargo.html(redondeo(sumColumn(6), 2));
		derechos.html(redondeo(sumColumn(7), 2));
		iva.html(redondeo(sumColumn(8), 2));
		total.html(redondeo(sumColumn(9), 2));

	}

	// Si hay rows en el grid, el resultado es la suma de los mismos
	else {
		prima.html(primaPPEnVista);
		recargo.html(recargoPPEnVista);
		derechos.html(derechosPPEnVista);
		iva.html(ivaPPEnVista);
		total.html(totalPPEnVista);

	}
}

function recuotificar(idCotizacion, tMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {
	readOnly = false;
	tipoMov = tMov;
	var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?'+
	'idToCotizacion='+ idCotizacion + 
	'&readOnly=' + readOnly + 
	'&tipoMov='+ tipoMov + 
	'&idToSolicitud=' + idToSolicitud + 
	'&idToPoliza=' + idToPoliza+
	'&idContinuity='+idContinuidad+
	'&tipoFormaPago='+tipoFormaPago+
	'&numeroEndoso='+numeroEndoso;
	redirectVentanaModal("programaPago", url, null);

}

function redondeo(numero, decimales) {
	var flotante = parseFloat(numero);
	var resultado = Math.round(flotante * Math.pow(10, decimales))
			/ Math.pow(10, decimales);
	return resultado;
}

function regresarProgramaPago(idToCotizacion, tipoMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {
	
	if(!validaSaldosNegativos()){
		alert("No pueden existir saldos Negativos. Verifique.");
		return;
	}
	
	var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?'+
	          'idToCotizacion='+ idToCotizacion + 
	          '&readOnly=' + readOnly + 
	          '&tipoMov='+tipoMov+
	          '&idToSolicitud='+idToSolicitud+
	          '&idToPoliza='+idToPoliza+
	          '&idContinuity='+idContinuidad+
	      	  '&tipoFormaPago='+tipoFormaPago+
	      	  '&numeroEndoso='+numeroEndoso;
	
	if (readOnly){
		redirectVentanaModal("programaPago", url, null);
	}else{
		if (confirm("Si regresa se perder\u00E1n los cambios. \u00BFDesea Continuar?")) {
			redirectVentanaModal("programaPago", url, null);
		}
	}
}

function salir() {

	id = "programaPago";

	if (readOnly) {
		parent.cerrarVentanaModal("programaPago", true);
	} else {
		
		//validamos diferiecias
		//if(!existenSaldosEnProgramaDePago()){
		if (validaSaldosExistentesEnCotizacion() == 0){
		
		
			if (confirm("Se perder\u00E1n los cambios no guardados \u00BFDesea Continuar?")) {
				parent.cerrarVentanaModal("programaPago", true);
			}
		}else{
			alert("No pueden existir saldos en los programas de pago. Verifique.");
		}
	}
}

function validaSaldosExistentesEnCotizacion(){
	var res = -1;
	jQuery
	.ajax( {
		url : "/MidasWeb/componente/programapago/validarTotalesCotizacion.action?idToCotizacion="
				+ idCotizacion,
		dataType : 'json',
		async : false,
		success : function(data) {
			res = data.resultado;
		}
	});
	return res;
}

function salir2() {
	parent.cerrarVentanaModal("programaPago", true);
}

function setReadOnlyFlag(flag) {
	readOnly = flag;
}

/**
 * @author MGGE
 * Set de bandera configurable para permitir o no editar la prima total
 */
function setHasEditPrimaTotalPermitFlag(flag) {
	hasEditPrimaTotalPermit = flag;
}

/**
 * @author MGGE
 * set de variable configurable de monto de ajuste permitido en la columna prima total
 */
function setAjustePermitidoPrimaTotal(flag) {
	ajustePermitidoPrimaTotal = flag;
}
function sumColumn(ind) {
	var out = 0;
	for ( var i = 0; i < grid.getRowsNum(); i++) {
		var valorEnRow = grid.cells2(i, ind).getValue();
		out += redondeo(parseFloat(valorEnRow), 2);
	}
	return out;
}

function verRecibos(idProgPago, idCot, tipoMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso) {

	mostrarRecibos(idProgPago, idCot, tipoMov, idToSolicitud, idToPoliza, idContinuidad, tipoFormaPago, numeroEndoso);
}

function validaGuardar() {
	return true;
}

function validaFechasVaciasEnGrid() {

	// for por cada columna
	for ( var col = 2; col < 5; col++) {

		// for por todos los registros en el grid
		for ( var i = 0; i < grid.getRowsNum(); i++) {
			var valorEnRow = grid.cells2(i, col).getValue();
			if (valorEnRow == '') {
				alert("Completar Todos los campos del Grid de Recibos!");
				return false;
			}// en if
		}// end for row
	}// end for col
	return true;
}

function validanegativos(numero) {
	var out = true;
	if (numero < 0) {
		alert("El monto debe ser igual o mayor que cero");
		out = false;
	}
	return out;
}

function validarDistribucion(valorRow, index) {
	var totalProgramaPago = primaPPEnVista;
	var elementosEnArray = miArraygrid.length;
	var elementosNoEditados = 0;
	var elementosEditados = 0;
	var sumatoriaEditados = 0;
	
	

	for ( var i = 0; i < elementosEnArray; i++) {

		// Discriminamos el index actual
		if (miArraygrid[i]["index"] != index) {
			var currentIndex = miArraygrid[i]["index"];
			var flag = miArraygrid[i]["flag"];
			var editado = miArraygrid[i]["editado"];
			// Si no esta editado, aumentamos el contador de no editados

			if (miArraygrid[i]["editado"] == false) {
				elementosNoEditados = elementosNoEditados + 1;

			}
			// Si esta editado, aumentamos contador de editados y sacamos si
			// valor de grid
			else {
				elementosEditados = elementosEditados + 1;
				var valorEnGrid = redondeo(parseFloat(grid.cells2(currentIndex,
						5).getValue()), 2);
				sumatoriaEditados = sumatoriaEditados + valorEnGrid;

			}
		}
	}

	var totalConNuevoValor = redondeo(parseFloat(valorRow), 2)
			+ sumatoriaEditados + (elementosNoEditados * minimoRecibos);
	var res = totalConNuevoValor <= totalProgramaPago;

	return res;
}


//ENDOSOS AUTOS
function mostrarProgramaPagoEndoso(idToSolicitud, idContinuidad, idToPoliza, tm){
	tipoMov = tm;	
	//validacion de primas de endoso
	var res = -1;
	jQuery
	.ajax( {
		url : '/MidasWeb/componente/programapago/validaPrimasDeEndoso.action?' +
		'idToSolicitud='+ idToSolicitud + 
		'&tipoMov=E',
		dataType : 'json',
		async : false,
		success : function(data) {
			res = data.resultado;
		}
	});
//if (res > 0){
		var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?idToSolicitud='+idToSolicitud+"&readOnly=true&tipoMov=E&idToPoliza="+idToPoliza;
		mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1100, 650, url, "");
//	}else{
//		alert("Recuotificaci\u00F3n no aplica para movimientos con prima negativa. Verifique.");
//	}
}

function openProgramaPago(idToCotizacion, tm){
	// codigo del componenete original	
	//   var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
	//   mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1020, 550, url, "");
	tipoMov = tm;
		var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?idToCotizacion='+idToCotizacion+"&readOnly=true&tipoMov=P";
		mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1100, 650, url, ""); 
		
	}

function mostrarProgramaPagoExtensionDeVigencia(idToSolicitud, idContinuidad, idToPoliza){
}

function mostrarProgramaPagoAjusteDePrima(idContinuidad, tipoFormaPago, polizaId, numeroEndoso){	
	//validacion de primas de endoso
	var res = -1;
	jQuery
	.ajax( {
		url : '/MidasWeb/componente/programapago/validaPrimasDeEndoso.action?' +
		'idContinuity='+ idContinuidad + 
		'&tipoMov=AP',
		dataType : 'json',
		async : false,
		success : function(data) {
			res = data.resultado;
		}
	});
	
	if (res > 0){
	var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?'+
	'readOnly=true'+
	'&tipoMov=AP'+
	'&idToPoliza='+polizaId+
	'&idContinuity='+idContinuidad+
	'&tipoFormaPago='+tipoFormaPago+
	'&numeroEndoso='+numeroEndoso;
	mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1100, 650, url, ""); 
	}else{
		alert("Recuotificaci\u00F3n no aplica para movimientos con prima negativa. Verifique.");
	}
}

function modificaPersona( idToProgramaPago, idToCotizacion) {
	var url = '/MidasWeb/componente/programapago/modificaPersona.action?idToProgramaPago='+idToProgramaPago + '&idToCotizacion='+idToCotizacion;
	parent.mostrarVentanaModal("contratanteProgramaPago", "Modificar Dueño del Programa de Pago", null, null, 1000, 550, url, "");    
}

function asignarClienteAProgramaPago(idCliente, idProgramaPago, idClienteAsegurado){
	var path = "/MidasWeb/componente/programapago/asignarClienteAProgramaPago.action?idCliente="+idCliente+"&idToProgramaPago="+idProgramaPago;
	parent.redirectVentanaModal("contratanteProgramaPago", path, null);
}

function validaEstatusRecuotificacion(){
	// Se quito por que a) No se esta usando y b) a veces
	// despues de ejecutar la url a la que no se tiene permiso
	// invalida la sesion y manda a la pantalla de login
}

