 
var gridProgPagos;
var grid;


var primaTotalCot;
var recargoTotalCot;
var derechosTotalCot;
var ivaTotalCot;
var totalTotalCot;

var primaTotal;
var recargoTotal;
var derechosTotal;
var ivaTotal;
var totalTotal;

var prima;
var recargo;
var derechos;
var iva;
var total;

var primaDif;
var recargoDif;
var derechosDif;
var ivaDif;
var totalDif;

var primaTotalPP;
var recargoTotalPP;
var derechosTotalPP;
var ivaTotalPP;
var totalTotalPP;

var primaPP;
var recargoPP;
var derechosPP;
var ivaPP;
var totalPP;

var miMatriz;
var miArraygrid;

var flagDistribuyePrima;
//declaracion de una matriz bidimiencional
//var getRowsNum = grid.getRowsNum() ; 
//for (i = 0; i < getRowsNum; i++){
//    miMatriz[i]=new Array(getRowsNum);
//     }

var myForm;

//var formData = [
//		{type: "label", label: "Agrupacion Automatica Prima"},
//		{type: "checkbox", label: "Si", checked: true},
//		{type: "checkbox", label: "No", checked: false}
//];


function openProgramaPago(idToCotizacion){
	
// codigo del componenete original	
//   var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
//   mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1020, 550, url, "");
	
	var url = '/MidasWeb/componente/programapago/cargarProgramaPago.action?idToCotizacion='+idToCotizacion+"&readOnly=true&tipoMov=P";
	mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1050, 600, url, ""); 
	
}

function openProgramaPago2(idToCotizacion){    
	   var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
	   mostrarVentanaModal("programaPago2", "Resumen del Programa de Pagos", null, null, 100, 100, url, "");    
	}


function salir(){  	
	
	id = "programaPago";
	parent.mostrarMensajeConfirm(
			"Se perderán los cambios no guardados ¿Desea Continuar?",
			"20", "salir2()", null, null);
	
	//parent.mostrarMensajeConfirm("Se perderán los cambios no guardados ¿Desea Continuar?","20","salir2()");
	
	//   cerrarVentanaModal("programaPago", onCloseFunction);
	  // var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
	  // mostrarVentanaModal("programaPago", "RESUMEN PROGRAMA DE PAGOS", null, null, 1020, 380, url, "");    
	}

function salir2(){  
	parent.cerrarVentanaModal("programaPago",true);
	
	//   cerrarVentanaModal("programaPago", onCloseFunction);
	  // var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
	  // mostrarVentanaModal("programaPago", "RESUMEN PROGRAMA DE PAGOS", null, null, 1020, 380, url, "");    
	}


function openRecibos(idToProgramaPago, idToCotizacion){   
   var url = '/MidasWeb/componente/programapago/getlistaRecibos.action?idToProgramaPago=' + idToProgramaPago + '&idToCotizacion=' + idToCotizacion ;
   redirectVentanaModal ("programaPago", url, null);
	}


function getGridProgramaPagos(idToCotizacion)
{
   document.getElementById("programaPagoListadoGrid").innerHTML = '';
   gridProgPagos = new dhtmlXGridObject('programaPagoListadoGrid');
   var posPath = 'llenaListadoProgramaPagos.action?idToCotizacion='+idToCotizacion;

   gridProgPagos.loadXML(posPath,function(){
	   gridProgPagos.attachFooter("TOTAL COTIZACION,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaInciso'>0</div>,<div id='recargoInciso'>0</div>,<div id='derechosInciso'>0</div>,<div id='ivaInciso'>0</div>,<div id='totalInciso'>0</div>", ["text-align:left;,background-color:#D3F3B4;"]);
	   gridProgPagos.attachFooter("TOTAL PROGRAMA PAGO,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaPP'>0</div>,<div id='recargoPP'>0</div>,<div id='derechosPP'>0</div>,<div id='ivaPP'>0</div>,<div id='totalPP'>0</div>", ["text-align:left;"]);
	   gridProgPagos.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='primaDifPP'>0</div>,<div id='recargoDifPP'>0</div>,<div id='derechosDifPP'>0</div>,<div id='ivaDifPP'>0</div>,<div id='totalDifPP'>0</div>", ["text-align:left;"]);
	   getSaldoCotizacionOriginal(idToCotizacion);
	   getSaldoPeriodoPago(1);
	   //alert(1);
	   getSaldoCotizacionActual(idToCotizacion);
	   getSaldoDiferencia(idToCotizacion);
	   
   });
   
   gridProgPagos.init();

    //var browserName=navigator.appName;
   //if (browserName=="Microsoft Internet Explorer") {
   //	gridProgPagos.attachFooter("Totales,#cspan,#cspan,#cspan,<div id='totIntOrd'>0</div>,<div id='totIntMor'>0</div>,<div id='totAbonoCap'>0</div>,<div id='totIva'>0</div>,<div id='totImpPago'>0</div>,Por Pagar:,<div id='totImpPorPagar' style='color:green;'>0</div>,#cspan,#cspan", ["text-align:right;height:30px;"]);
   //}else{
   //	gridProgPagos.attachFooter("Totales,#cspan,#cspan,<div id='totIntOrd'>0</div>,<div id='totIntMor'>0</div>,<div id='totAbonoCap'>0</div>,<div id='totIva'>0</div>,<div id='totImpPago'>0</div>,Por Pagar:,<div id='totImpPorPagar' style='color:green;'>0</div>,#cspan,#cspan", ["text-align:right;height:30px;"]);
   //}
   
  // gridProgPagos.attachEvent("onXLS", function(grid_obj){blockPage()});
  // gridProgPagos.attachEvent("onXLE", function(grid_obj){unblockPage()});
 
}


function getGridRecibos(idToProgramaPago, idToCotizacion){
   grid = new dhtmlXGridObject('reciboListadoGrid');
   //grid.setImagePath("../imgs/");
   //grid.setSkin("dhx_skyblue");      
   grid.init();
   grid.attachFooter("PRIMAS TOTALES,#cspan,#cspan,#cspan,#cspan,<div id='prima'>0</div>,<div id='recargo'>0</div>,<div id='derechos'>0</div>,<div id='iva'>0</div>,<div id='total'>0</div>", ["text-align:right; font-weight:bold;"]);
   grid.attachFooter("SALDO O DIFERENCIA,#cspan,#cspan,#cspan,#cspan,<div id='primaDif'>0</div>,<div id='recargoDif'>0</div>,<div id='derechosDif'>0</div>,<div id='ivaDif'>0</div>,<div id='totalDif'>0</div>", ["text-align:right; font-weight:bold;"]);
   getSaldoCotizacionOriginal(idToCotizacion);
   //getTotalesRealesPeriodoPago(idToProgramaPago,idToCotizacion);
   //grid.loadXML("llenaListadoPolizas.action?idProgramaPago="+idProgramaPago,calculateFooterValues);
   grid.loadXML("llenaListadoRecibos.action?idToProgramaPago="+idToProgramaPago + '&idToCotizacion=' + idToCotizacion,calculateFooterValues) ;
   grid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue)
   {
	  //alert('stage'+stage);
      if (stage== 2) 
      {
         if (!(validanegativos(nValue)))
         {   
            return false;
         }
         if (nValue!=oValue && cInd==5) 
         {   
            if (flagDistribuyePrima == '1' && primaDif.html() >= 0)
            {
               var rindex = grid.getRowIndex(rId);
               var saldo = oValue - nValue;
               inicializaFlag();
               marcaRowEditado(rindex,true);
               generaDistribucion(saldo, rindex, miArraygrid)
            }               
         }
         calculateIvaTotales();
         calculateFooterValues(0);  
      }
      return true;  
    });
}   

//Funcion que inicializa variables globales de suma y que ejecuta la incializacion de array
function llenaGlobales() {
//   primaTotal    = redondeo(sumColumn(5),4);
//   recargoTotal  = redondeo(sumColumn(6),4);
//   derechosTotal = redondeo(sumColumn(7),4);
//   ivaTotal      = redondeo(sumColumn(8),4);
//   totalTotal    = redondeo(sumColumn(9),4);
 
  // getTotalesRealesPeriodoPago(idpp);
   inicializaMatrizDistribucion(5);
   //alert('llenaGlobales');
}

function calculateFooterValues(stage) {
    //alert("estamos ");
    //Esto con el fin de que cuando sea editable ya no reinicie los totales
    if (stage!= 0 && stage!= 1 && stage != 2 )
    {  
    llenaGlobales();
    }
   
  //  alert("primaTotal: "+primaTotal);
   // alert("stage1:  "+stage);
    //if (stage && stage != 2);
    //return true;
    
    //Llenamos el Primer Footeer
//    if (stage && stage != 2);
//    alert('prima'+prima);
//    return true;

    prima = $('#prima'); 
    prima.html(redondeo(sumColumn(5),2));
    
    recargo = $('#recargo'); 
    recargo.html(redondeo(sumColumn(6),2));
    
    derechos = $('#derechos'); 
    derechos.html(redondeo(sumColumn(7),2));
   
    iva = $('#iva'); 
    iva.html(redondeo(sumColumn(8),2));
    
    total = $('#total'); 
    total.html(redondeo(sumColumn(9),2));
    
//    
//    alert('primass '+ prima);
//    prima = $('#prima');
//    alert(prima);
//    alert(prima.html());
//    alert(prima.html(8));
//    
//    //prima = document.getElementById("prima");
//    alert('prima '+ prima);
//    prima.innerHTML = redondeo(sumColumn(5),2);
//    recargo = document.getElementById("recargo");
//    recargo.innerHTML = redondeo(sumColumn(6),2);
//    derechos = document.getElementById("derechos");
//    derechos.innerHTML = redondeo(sumColumn(7),2);
//    iva = document.getElementById("iva");
//    iva.innerHTML = redondeo(sumColumn(8),2);
//    total = document.getElementById("total");
//    total.innerHTML = redondeo(sumColumn(9),2);
    
    //Llenamos el Segundo Footeer
    calculaDiferencia();   
    return true;
    
}


function calculaDiferencia()
{
   // alert("estamos calculaDiferencia");
     //   alert("primaTotal"+primaTotal);
     // alert("primaDif"+primaDif.html());
    
    //este debe de ser la suma de todos l
     //prima.html()
     


    //Para calcular la diferencia restamos el total de inciso menos todos os recibos menos la diferencia de los demas periodos de pago con esos recibos
    primaDif = $('#primaDif');
    primaDif.html(redondeo(primaTotal - prima.html() /*- (primaTotalPP - primaPP)*/,2));
    
    recargoDif = $('#recargoDif'); 
    recargoDif.html(redondeo(recargoTotal - recargo.html()/* - (recargoTotalPP - recargoPP)*/,2));
   // alert('recargoTotal'+recargoTotal);
   // alert('recargo.html()'+recargo.html());
   // alert('recargoTotalPP'+recargoTotalPP);
    
    
    derechosDif = $('#derechosDif');   
    derechosDif.html(redondeo(derechosTotal - derechos.html()/*- (derechosTotalPP - derechosPP)*/,2));        
    
    ivaDif = $('#ivaDif');
    ivaDif.html  (redondeo(ivaTotal - iva.html()/*- (ivaTotalPP - ivaPP)*/,2));        
   
    totalDif = $('#totalDif');   
    totalDif.html  (redondeo(totalTotal - total.html()/*- (totalTotalPP - totalPP)*/,2));  
    
    //primaDif = document.getElementById("primaDif");   
    //primaDif.innerHTML  = redondeo(primaTotal - prima.innerHTML,2)  ;
//    
//    recargoDif = document.getElementById("recargoDif");   
//    recargoDif.innerHTML  = redondeo(recargoTotal - recargo.innerHTML,2)  ;
//    
//    derechosDif = document.getElementById("derechosDif");   
//    derechosDif.innerHTML  = redondeo(derechosTotal - derechos.innerHTML,2)  ;
//     
//    ivaDif  = document.getElementById("ivaDif");   
//    ivaDif.innerHTML  = redondeo(ivaTotal - iva.innerHTML ,2)  ;
//     
//    totalDif   = document.getElementById("totalDif");   
//    totalDif.innerHTML  = redondeo(totalTotal - total.innerHTML,2)  ;
 // alert("vaos a salir de  calculaDiferencia");
}



function inicializaFlag()
{ 
 // alert("vamos a inicializar");
    for (i=0; i<miArraygrid.length; i++)
    {
        miArraygrid[i]["flag"]=false;
    } 
}

function inicializaMatrizDistribucion(ind) {
    
    var items = grid.getRowsNum() ; 
    //Declaro mi arreglo unidimiencional
    miArraygrid = new Array(items); 

    //En cada elemento le intego otro arreglo
    for (i = 0; i < items; i++){
       miArraygrid[i]=new Array(4);
    }
    //lo llenamos
    for (i=0; i<items; i++)
    {
    //for (e=0; e<4; e++)
    // {
        miArraygrid[i]["cell"]    =  parseFloat(grid.cells2(i, ind).getValue()) ;
        miArraygrid[i]["index"]   =  i;
        miArraygrid[i]["flag"]    =  false;
        miArraygrid[i]["editado"] =  false;
            
//       if(e==0)
//       {miArraygrid[i]["cell"] =  parseFloat(grid.cells2(i, ind).getValue()) ;}
//       else if (e==1)
//       {miArraygrid[i]["index"] =  i; }
//       else if (e==2)
//       {miArraygrid[i]["flag"] =  false; }
//       else if (e==3)
//       {
//           miArraygrid[i]["editado"] =  false;
//       }
//    }
    }
//    alert("se ha terminado de llenar");
//    
//    for (i=0; i<items; i++)
//{
//for (e=0; e<3; e++)
//{
//     if(e==0)
//       {var val = "cell"}
//        else if (e==1)
//      {var val = "index"}
//       else
//       {var val = "flag"}
//    alert("elemento i"+i+" elemento val"+val+ "}= "+miArraygrid[i][val]);
//}
//}

     miArraygrid.sort(function(a,b){
     // return b-a
     return parseInt(a.cell,10) - parseInt(b.cell,10);

  });
//   alert("ordenado");
//    for (i=0; i<items; i++)
//{
//for (e=0; e<3; e++)
//{
//     if(e==0)
//       {var val = "cell"}
//        else if (e==1)
//      {var val = "index"}
//       else
//       {var val = "flag"}
//       alert("elemento i"+i+" elemento val "+val+ "}= "+miArraygrid[i][val]);
//}
//}
    
    
    
    
    
    
    
    
//
//    //lleno arrego con datos del grid
//    for (var i = 0; i < items; i++) 
//    {
//        miArraygrid[i] = parseFloat(grid.cells2(i, ind).getValue());
//        alert("original"+miArraygrid[i]);
//    }
//    
//    
//    
//    
//    
//    //ordenamos el grid
//    miArraygrid.sort(function(a,b){return b-a});
//   
//   
//   
//   
//   
//   
//   
//    //lleno matriz e inicializo en falso
//    for (var i = 0; i < items; i++) 
//    {
//        miMatriz = [miArraygrid[i],false];
//    }
//    
//
//    for (var i = 0; i < items; i++) 
//    {
//     
//        alert("ordenado"+miArraygrid[i]);
//    }

//    
//    miArray[i,0].sort(function(a,b){return b-a});
//
//     for (var i = 0; i < items; i++) 
//    {
//     
//        alert("ordenado"+miArray[i]);
//    }
	
//
//// your items array
//var items = [{a:0,b:0},{a:2,b:1},{a:1,b:2}];
//// function we can use as a sort callback
//var compareItemsBy_a_Descending = function(x,y) {
//  return y.a - x.a;
//};
//// function to alert the items array
//var displayItems = function(items) {
//  var out = [];
//  for (var i=0;i<items.length;i++) {
//    out.push('{a:' + items[i].a + ',b:' + items[i].b + '}');
//  }
//  alert('[' +out.join(',') + ']');
//};
//    
//    var iMax = 20;
//var jMax = 10;
//var f = new Array();
//
//for (i=0;i<iMax;i++) {
// f[i]=new Array();
// for (j=0;j<jMax;j++) {
//  f[i][j]=0;
// }
//}
//
// var miArray = new Array(count); 

//var sorted = [{a:0,b:0},{a:2,b:1},{a:1,b:2}].sort( function( a, b )
//{
//  if ( a.a == b.a ) return 0;
//  return ( a.a > b.a ) ? 1 : -1;
//}).reverse();
//  
//   if ([0])
//   {
//       alert("true");
//   }
//   else
//   {
//       alert("false");  
//   }
   
  } ;
    
    
    function validanegativos(numero)
    {
       out = true;
       if (numero < 0)
       {
       alert("El monto debe ser igual o mayor que cero");
       out = false;
       }
       
       return out;
    }
    
    function sumColumn(ind) {
    	 //   alert("entramos a la funcion sumColumn");
    	    var out = 0;
    	    for (var i = 0; i < grid.getRowsNum(); i++) {
    	        out += redondeo(parseFloat(grid.cells2(i, ind).getValue()),2);
    	    }
    	    return out;
    	}
    
    function redondeo(numero, decimales)
    {
    var flotante = parseFloat(numero);
    var resultado = Math.round(flotante*Math.pow(10,decimales))/Math.pow(10,decimales);
    return resultado;
    }
    
    function getTotalesRealesPeriodoPago(idToProgramaPago, idToCotizacion)
    {
      $.ajax({
      url: "getTotalesRealesPeriodoPago.action?idToProgramaPago=" + idToProgramaPago+ '&idToCotizacion='+idToCotizacion,   
      dataType: 'json',
      async: false,
      success: function(data) {
    	  
    	  //alert(data.saldo.impPrimaNeta);
    	  //primaDif.html(redondeo(primaTotal - prima.html() - (primaTotalPP - primaPP),2));
          //alert("idpp"+idpp)  ;
         

    	  //// $('#primaPP').html( data.footter.primaNetaTotalActual );
    	  //// $('#recargoPP').html( data.footter.recargosTotalActual );
    	  //// $('#derechosPP').html( data.footter.derechosTotalActual );
    	  //// $('#ivaPP').html( data.footter.ivaTotalActual );
    	  //// $('#totalPP').html( data.footter.primaNetaTotalTotalesActual );
          
    	  ////$('#primaDifPP').html( data.footter.primaNetaTotalRealDiff );
    	  ////$('#recargoDifPP').html( data.footter.recargosTotalRealDiff );
    	  ////$('#derechosDifPP').html( data.footter.derechosTotalRealDiff );
    	  ////$('#ivaDifPP').html( data.footter.ivaTotalRealDiff );
    	  ////$('#totalDifPP').html( data.footter.primaNetaTotalTotalesRealDiff );  
          
    	  ////$('#primaInciso').html( data.footter.primaNetaTotalInciso );
    	  ////$('#recargoInciso').html( data.footter.recargosTotalInciso );
    	  ////$('#derechosInciso').html( data.footter.derechosTotalInciso );
    	  ////$('#ivaInciso').html( data.footter.ivaTotalInciso );
    	  ////$('#totalInciso').html( data.footter.primaNetaTotalTotalesInciso );
          
//          $('#primaInciso').html( data.footter.primaNetaTotalReal );
//          $('#recargoInciso').html( data.footter.recargosTotalReal );
//          $('#derechosInciso').html( data.footter.derechosTotalReal );
//          $('#ivaInciso').html( data.footter.ivaTotalReal );
//          $('#totalInciso').html( data.footter.primaNetaTotalTotalesReal );
          
//          
//           private BigDecimal primaNetaTotalInciso;
//        private BigDecimal recargosTotalInciso;
//        private BigDecimal derechosTotalInciso;
//        private BigDecimal ivaTotalInciso;
//        private BigDecimal primaNetaTotalTotalesInciso;

    	  //// primaPP = redondeo(data.footter.primaNetaTotalActual,2);
    	  //// recargoPP = redondeo(data.footter.recargosTotalActual ,2);
    	  ////derechosPP = redondeo(data.footter.derechosTotalActual,2);
    	  ////ivaPP = redondeo(data.footter.ivaTotalActual,2);
    	  ////totalPP = redondeo(data.footter.primaNetaTotalTotalesActual,2);

    	  ////primaTotalPP    =  redondeo(data.footter.primaNetaTotalReal ,2);
    	  ////recargoTotalPP   =  redondeo(data.footter.recargosTotalReal,2);
    	  ////derechosTotalPP  =  redondeo(data.footter.derechosTotalReal,2);
    	  ////ivaTotalPP       =  redondeo(data.footter.ivaTotalReal,2);
    	  ////totalTotalPP     =  redondeo(data.footter.primaNetaTotalTotalesReal,2);
         
    	  ////primaTotal    =  redondeo(data.footter.primaNetaTotalInciso,2);
    	  ////recargoTotal  =  redondeo(data.footter.recargosTotalInciso,2);
    	  ////derechosTotal =  redondeo(data.footter.derechosTotalInciso,2);
    	  ////ivaTotal      =  redondeo(data.footter.ivaTotalInciso,2);
    	  ////totalTotal    =  redondeo(data.footter.primaNetaTotalTotalesInciso,2);
      }
    });
    }
    
//    function getSaldoCotizacionActual(idToProgramaPago, idToCotizacion)
//    {
//      $.ajax({
//      url: "getSaldoCotizacionActual.action?idToProgramaPago=" + idToProgramaPago+ '&idToCotizacion='+idToCotizacion,   
//      dataType: 'json',
//      async: false,
//      success: function(data) {
//    	    	  
//    	  primaTotal    =  redondeo(data.saldo.impPrimaNeta,2);
//    	  recargoTotal  =  redondeo(data.saldo.impRcgosPagoFR,2);
//    	  derechosTotal =  redondeo(data.saldo.impDerechos,2);
//    	  ivaTotal      =  redondeo(data.saldo.impIVA,2);
//    	  totalTotal    =  redondeo(data.saldo.impPrimaTotal,2);
//    	  
//    }
//    });
//    }
//    
  
    function getSaldoCotizacionActual(idToCotizacion)
    {
      $.ajax({
      url: "getSaldoCotizacionActual.action?idToCotizacion=" + idToCotizacion,   
      dataType: 'json',
      async: false,
      success: function(data) {
     
      }
    });
    }
    
    
    function getSaldoCotizacionOriginal(idToCotizacion)
    {
      $.ajax({
      url: "getSaldoCotizacionOriginal.action?idToCotizacion=" + idToCotizacion,   
      dataType: 'json',
      async: false,
      success: function(data) {

		  primaTotal    =  redondeo(data.saldo.impPrimaNeta,2);
		  recargoTotal  =  redondeo(data.saldo.impRcgosPagoFR,2);
		  derechosTotal =  redondeo(data.saldo.impDerechos,2);
		  ivaTotal      =  redondeo(data.saldo.impIVA,2);
		  totalTotal    =  redondeo(data.saldo.impPrimaTotal,2);
		 
		  $('#primaInciso').html( primaTotal );
		  $('#recargoInciso').html( recargoTotal );
		  $('#derechosInciso').html( derechosTotal );
		  $('#ivaInciso').html( ivaTotal );
		  $('#totalInciso').html( totalTotal);       
      }
    });
    }
    
    function getSaldoDiferencia(idToCotizacion)
    {
      $.ajax({
      url: "getSaldoDiferencia.action?idToCotizacion=" + idToCotizacion,   
      dataType: 'json',
      async: false,
      success: function(data) {
    	  
       	  $('#primaDifPP').html( redondeo(data.saldo.impPrimaNeta,2) );
    	  $('#recargoDifPP').html( redondeo(data.saldo.impRcgosPagoFR,2) );
    	  $('#derechosDifPP').html( redondeo(data.saldo.impDerechos,2) );
    	  $('#ivaDifPP').html( redondeo(data.saldo.impIVA,2) );
    	  $('#totalDifPP').html( redondeo(data.saldo.impPrimaTotal,2));     
      }
    });
    }

    function getSaldoPeriodoPago(idToProgramaPago)
    {
      $.ajax({
      url: "getSaldoPeriodoPago.action?idToProgramaPago=" + idToProgramaPago,   
      dataType: 'json',
      async: false,
      success: function(data) {
    	  
    	  primaPP    =  redondeo(data.saldo.impPrimaNeta,2);
    	  recargoPP  =  redondeo(data.saldo.ImpRcgosPagoFR,2);
    	  derechosPP =  redondeo(data.saldo.impDerechos,2);
    	  ivaPP      =  redondeo(data.saldo.impIVA,2);
    	  totalPP    =  redondeo(data.saldo.impPrimaTotal,2);
    	  
		  $('#primaPP').html( primaPP );
		  $('#recargoPP').html( recargoPP );
		  $('#derechosPP').html( derechosPP );
		  $('#ivaPP').html( ivaPP );
		  $('#totalPP').html( totalPP); 
      }
    });
    }
    

    
    function generaDistribucion(saldo, rindex, array)
    {
        var numNe = getRowsNoEditados();
        if(numNe != 0) 
        {
        var pR = redondeo(saldo/numNe,2);
        var rem = 0;
         // alert("numNe"+numNe);
         //alert("pR"+pR);
         // alert("saldo"+saldo);
            
                 //Pido un ID Falso
        var rindex = getRowFalso();

            //      alert("el row falso a traer es" + rindex );
             //     alert("cell"+miArraygrid[rindex]["cell"]);
             //   alert("index"+miArraygrid[rindex]["index"]);
             //   alert("flag"+miArraygrid[rindex]["flag"]);

              //   alert("la celda con index "+rindex+"vale "+miArraygrid[rindex]["cell"]);
               
        array[rindex]["cell"]=redondeo(array[rindex]["cell"] + pR,2);    
                
        var rid = grid.getRowId(array[rindex]["index"]);
                //grid.cells( miArraygrid[rindex]["index"],5).cell.innerHTML=miArraygrid[rindex]["cell"] + pR;
                
               //  alert("voy a setear"+miArraygrid[rindex]["cell"] + pR);
        grid.cells(rid,5).setValue(array[rindex]["cell"]);
                
              //  alert("ahora le sumo el proporcional "+rindex+"vale "+miArraygrid[rindex]["cell"]);
             
        if (array[rindex]["cell"] < 0)
                {
                  rem = array[rindex]["cell"];
                  array[rindex]["cell"] = 0;
                  grid.cells(rid,5).setValue(0);
                  
                }
             //   alert("mi remanenete es "+rem);
               //aqui vamos a settear el valor del grid con el valor nuevo de la cell
            
                var saldonuevo =   saldo - pR + rem;
               // alert("saldonuevo "+saldonuevo);
                marcaRowEditado (array[rindex]["index"],false);
                generaDistribucion(saldonuevo, rindex,array);
                
           } 
       }
    
    function marcaRowEditado (rIndex,editado){
        
    	   // var items = grid.getRowsNum() ; 
    	    var items = miArraygrid.length;
    	    for (i=0; i<items; i++)
    	    {
    	       if(miArraygrid[i]["index"]==rIndex)
    	         {miArraygrid[i]["flag"]=true;
    	          miArraygrid[i]["editado"]=editado;
    	       }
    	    }
    	}
    
    
    function getRowFalso()
    {
        var out = null;
        for (i=0; i<miArraygrid.length; i++)
        {
           // alert("barriendo flag "+ i+ " " +miArraygrid[i]["flag"]);
           if(miArraygrid[i]["flag"]==false && miArraygrid[i]["editado"]==false)
            {
                out = i;//miArraygrid[i]["index"];
                return out;
            }
        }
        return out;
        
    }
    
    function getRowsNoEditados (){
        
    	  // var items = grid.getRowsNum() ; 
    	   var items = miArraygrid.length;
    	   // alert("miArraygrid.length"+miArraygrid.length)
    	   var out = 0;
    	   for (i=0; i<items; i++)
    	   {
    	      // if(miArraygrid[i]["flag"]==false)
    	      if(miArraygrid[i]["flag"]==false && miArraygrid[i]["editado"]==false)
    	      {out += 1;}
    	   }
    	   return out;
    	}
    
    function eliminaRecibo() {
        
    	   var count=grid.getRowsNum() ; 
    	   if (count==1){
    	       alert("No se puede eliminar. Al menos debe quedar un registro");
    	       return;
    	   }
    	   

    	   //traemos el rowid seleccionado
    	   rowId=grid.getSelectedRowId();
    	    
    	   //traemos el index
    	   rowIndex  =grid.getRowIndex(rowId);
    	   
    	   //ese index hay que ir a buscarlo al arreglo y borrar la linea completa
    	  // eliminaElementoArray(rowIndex);
    	   marcaRowEditado(rowIndex,true);
    	     
    	   var count=grid.getRowsNum() ; 
    	   if(count == rowIndex + 1){  
    	      if(confirm("Esta Seguro de Eliminar Registro?")) 
    	      {
    	      grid.deleteRow(rowId);     
    	      calculateFooterValues(0);
    	      }
    	   }else{
    	      alert("No se puede eliminar. Puede eliminar el último registro solamente");
    	   }        
    	}
    
    
    function agregaRecibo() {
        
        
    	   //validar que tenga saldo
    	   if( primaDif.html() > 0   || 
    	       recargoDif.html() > 0 || 
    	       derechosDif.html() >0 ){       
    	         
    	       var newId = (new Date()).valueOf()
    	       //index siguiente
    	       var count= grid.getRowsNum();
    	       //obteneos el rowid del index anterior
    	       var ofRow = grid.getRowId(count-1);
    	       //agregamos el row
    	       grid.addRow(newId,[],count);
    	       //clonamos row
    	       grid.copyRowContent(ofRow, newId);
    	       //vamos a setear valores delfault del nuevo row los tomamos de la Diferencia
    	       //No de Exhibicion asignamos siguiente numero
    	       grid.cells(newId,0).setValue(count+1);
    	       //Prima Neta 
    	       grid.cells(newId,5).setValue(primaDif.html());
    	       //Recargo
    	       grid.cells(newId,6).setValue(recargoDif.html());
    	       //Derechos  
    	       grid.cells(newId,7).setValue(derechosDif.html()); 
    	       //IVA  
    	       grid.cells(newId,8).setValue(ivaDif.html()); 
    	       //primatotal  
    	       grid.cells(newId,9).setValue(totalDif.html()); 
    	       calculateFooterValues(0);
    	    }else{
    	        alert("No se puede agregar registros porque no hay saldo disponible");
    	    }
    	}
    
    
    function check(browser)
    {
       alert(browser);
       flagDistribuyePrima=browser ;
       if (flagDistribuyePrima=='1')
    	   alert('inicixlizamos');
       {inicializaMatrizDistribucion(5);}
      
    //document.getElementById("answer").value=browser;
    }
    
    function calculateIvaTotales()
    {
        
        //   alert("entramos a la funcion sumColumn");
        for (var i = 0; i < grid.getRowsNum(); i++) {
            
            var primacell  = redondeo(parseFloat(grid.cells2(i, 5).getValue()),2);
            var recargoscell = redondeo(parseFloat(grid.cells2(i, 6).getValue()),2);
            var derechoscell = redondeo(parseFloat(grid.cells2(i, 7).getValue()),2);
            
            var iva = redondeo((primacell + recargoscell + derechoscell) * 0.16,2);
            var total = redondeo((primacell + recargoscell + derechoscell + iva),2);
            
           // traemos el rowid seleccionado
       //rowId=grid.getSelectedRowId();traemos el rowid seleccionado
           var rowId=grid.getRowId(i);
          
            //IVA  
           grid.cells(rowId,8).setValue(iva); 
           //primatotal  
           grid.cells(rowId,9).setValue(total); 
        
        
        }
        
    }
    
    function guardarRecibo(idToCotizacion)
    {
         if(validaGuardar())
            {
            
        alert("armamos el json");
         var jsonx = getAllRowsAsJson(grid);
    //
        // alert(jsonx);
            $.ajax({
               type:"GET",
               url: 'guardarRecibo.action?listJsonRecibosEditada=' +jsonx + '&idToCotizacion='+idToCotizacion,
               dataType: "json",
               async: false

                       });
            }
    }
    
    
    function regresaProgramaPago(idToCotizacion){ 
    	   if(confirm("Si regresa se perderan los cambios. ¿Desea Continuar?"))
    	   {
    		  // guardarRecibo(idToCotizacion);
    		   var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
    		   redirectVentanaModal("programaPago", url, null);
    		   
    		   
    	       
    	   }
    	   //else
    	   //{
    	   //   var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
    	   //   redirectVentanaModal("programaPago", url, null);

    	   //}
    	  
    	}

    function validaGuardar(){    
       return true;
     }
    
    
    function getAllRowsAsJsonOriginal() {
    	   
        var json = "{rows:[";
       
        for (var rowIndex=0; rowIndex<grid.getRowsNum(); rowIndex++) {
            json = json + "{id:" + grid.getRowId(rowIndex) + ",row:[";
         //   for(var cellIndex = 0; cellIndex < grid.getColumnsNum(); cellIndex++){
         for(var cellIndex = 0; cellIndex < 10; cellIndex++){
                var colId=grid.getColumnId(cellIndex);
                if (cellIndex==0){
                    
                    json = json + colId + '="' + grid.cells2(rowIndex,cellIndex).getValue() + '"';
                }
                else {
                    json = json +  "," + colId + '="' + grid.cells2(rowIndex,cellIndex).getValue() + '"';
                }
            }
            if (rowIndex<(grid.getRowsNum()-1)){
                json = json + "]},";
            }
            else {
                json = json + "]}";
            }      
        }
        json = json + "]}";
       
        return json.toString();

    }

    

    function getAllRowsAsJson(vargrid) {
       
        var json = "[";
       
        for (var rowIndex=0; rowIndex<vargrid.getRowsNum(); rowIndex++) {
            json = json + "{id:" + vargrid.getRowId(rowIndex);
         for(var cellIndex = 0; cellIndex < vargrid.getColumnsNum() -1 ; cellIndex++){
                var colId=vargrid.getColumnId(cellIndex);
               // if (cellIndex==0){
                    
                  //  json = json + colId + '="' + grid.cells2(rowIndex,cellIndex).getValue() + '"';
              //  }
                //else {
                    json = json +  "," + colId + ':"' + vargrid.cells2(rowIndex,cellIndex).getValue() + '"';
               // }
            }
            if (rowIndex<(vargrid.getRowsNum()-1)){
                json = json + "},";
            }
            else {
                json = json + "}";
            }      
        }
        json = json + "]";
       
        return json.toString();

    }

    function getRowAsJson(vargrid, rowIndex) {
       
        var json = "[";
       
      //  for (var rowIndex=0; rowIndex<vargrid.getRowsNum(); rowIndex++) {
           
           
            json = json + "{id:" + vargrid.getRowId(rowIndex);
         for(var cellIndex = 0; cellIndex < vargrid.getColumnsNum() -2 ; cellIndex++){
                var colId=vargrid.getColumnId(cellIndex);
               // if (cellIndex==0){
                    
                  //  json = json + colId + '="' + grid.cells2(rowIndex,cellIndex).getValue() + '"';
              //  }
                //else {
                    json = json +  "," + colId + ':"' + vargrid.cells2(rowIndex,cellIndex).getValue() + '"';
               // }
            }
            if (rowIndex<(vargrid.getRowsNum()-1)){
                json = json + "},";
            }
            else {
                json = json + "}";
            }      
       // }
        json = json + "]";
       
        return json.toString();
    }
     
    
    function modificaPersona( idToProgramaPago, idToCotizacion) {
    	var url = '/MidasWeb/componente/programapago/modificaPersona.action?idToProgramaPago='+idToProgramaPago + '&idToCotizacion='+idToCotizacion;
    	parent.mostrarVentanaModal("contratanteProgramaPago", "Modificar Dueño del Programa de Pago", null, null, 1000, 550, url, "");    
    }
    
    function imprimirProgramaPagos(idToCotizacion){
    	//var idCondicionEspecial = jQuery("#idCondicionEspecial").val();
    	//var url="/MidasWeb/catalogos/condicionespecial/imprimirCondicion.action?idCondicionEspecial="+idCondicionEspecial;
    	
    	 var url = '/MidasWeb/componente/programapago/imprimirProgramaPagos.action?idToCotizacion='+idToCotizacion;
    	 window.open(url, "ProgramaPagos");
    }
    
    
    
    /**
     * Llena el compo de estados
     */
    function onchangePaisCobranza() {
    	var idPais = jQuery('#idPais').val();
    	if (idPais != "" && idPais != null) {
    		listadoService.getMapEstados(idPais, function(data) {
    			addOptions(document.getElementById('idEstadoCobranza'), data);
    		});
    	} else {
    		removeAllOptionsAndSetHeaderDefault('idEstadoCobranza');
    		jQuery('#idEstadoCobranza').change();
    	}
    }
    /**
     * Llena el combo de municipios
     */
    function onchangeEstadoCobranza() {
    	var idEstado = jQuery('#idEstadoCobranza').val();
    	if (idEstado != "" && idEstado != null) {
    		listadoService.getMapMunicipiosPorEstado(idEstado, function(data) {
    			addOptions(document.getElementById('idMunicipioCobranza'), data);
    		});
    	} else {
    		removeAllOptionsAndSetHeaderDefault('idMunicipioCobranza');
    		jQuery('#idMunicipioCobranza').change();
    	}
    }
    /**
     * Llena el combo de colonias
     */
    function onchangeMunicipioCobranza() {
    	var idMunicipio = jQuery('#idMunicipioCobranza').val();
    	if (idMunicipio != "" && idMunicipio != null) {
    		listadoService.getMapColonias(idMunicipio, function(data) {
    			addOptions(document.getElementById('idColoniaCobranza'), data);
    		});
    	} else {
    		removeAllOptionsAndSetHeaderDefault('idColoniaCobranza');
    		jQuery('#idColoniaCobranza').change();
    	}
    }

    function onchangeCp(){
    	listadoService.getEstadoIdPorCp(jQuery('#codigoPostal').val(),function(data){
    		if (data != null) {
    			jQuery('#idEstadoCobranza').val(data);
    			jQuery('#idEstadoCobranza').change();
    		}	
    	});
    }
    
    function switchPorTipoPersona(idTipoPersona,divCarga){
    	var idDiv=(divCarga!=null)?divCarga:'contenido';
    	var params=jQuery("#clientForm").serialize()+"&divCarga="+idDiv;
    	sendRequestJQ(
    			null,
    			'/MidasWeb/catalogoCliente/switchTipoPersona.action?tipoPersona='+idTipoPersona+"&"+params,
    			'workAreaClientes',''
    			//'dhx_init_tabbars();jQuery("#btnGuardar").bind("click",guardarCliente);rescribirEtiquetas('+idTipoPersona+');'
    	);
    }
    
    function ocultarCamposTipoPersona(tipoPersona){
    	if(tipoPersona==1){
    		var trs=jQuery(".pf");
    		if(trs!=null && trs.length>0){
    			for(var i=0;i<trs.length;i++){
    				var row=trs[i];
    				jQuery(row).css("display","");
    				jQuery(".pm").css("display","none");
    			}
    		}
    		jQuery(".pm").find("input").val("");
    	}else if(tipoPersona==2){
    		jQuery(".pf").css("display","none");
    		jQuery(".pf").find("input").val("");
    		jQuery(".pm").css("display","");
    	}
    	
    	jQuery("#clienteGrid").html("");
//    	jQuery("#pagingArea").html("");
    	jQuery("#infoArea").html("");
    	jQuery("#pagingArea_Clientes").html("");
    	
    }

    
    function getPersonasCotizacion() {
        $.ajax({
            url: 'getPersonasCotizacion.action',
            dataType: "html",
            success: function(data) {
                $('#div_combo').html( data );
//                var tab = document.getElementById("div_combo").parentNode.getElementsByTagName("TABLE")[0];
//                var tr =  document.getElementById("div_combo").getElementsByTagName("tr")[0];
//                var firstTR = tab.getElementsByTagName("tbody")[0].firstChild;
//                tab.getElementsByTagName("tbody")[0].insertBefore( tr, firstTR );
            }
        });
    }    
    
    function cargarProgramaOrigina(idToCotizacion){
    	alert('modal nuevo');
    	form = null;
    	var url = '/MidasWeb/componente/programapago/getlistaProgramaPagos.action?idToCotizacion='+idToCotizacion;
    	   mostrarVentanaModal("programaPago", "Resumen del Programa de Pagos", null, null, 1020, 550, url, "");  
    	   
    	redirectVentanaModal("programaPago", url, form);
    	
    }
    
    function cargarCliente(idClienteAsegurado, idToProgramaPago, idToCotizacion){
    	
    	var url = '/MidasWeb/componente/programapago/modificaPersona.action?idToProgramaPago='+idToProgramaPago + '&idToCotizacion='+idToCotizacion + '&idClienteAsegurado='+idClienteAsegurado;
    	//var url = '/MidasWeb/componente/programapago/modificaPersona.action?idToProgramaPago='+idToProgramaPago + '&idToCotizacion='+idToCotizacion + '&idClienteAsegurado=' + idClienteAsegurado;
    	parent.redirectVentanaModal("contratanteProgramaPago", url, null);
    	
    }
    
    function guardarNuevoCliente(){
     	formParams = jQuery(document.personaForm).serialize();
    	var path = "/MidasWeb/componente/programapago/guardarCliente.action?" + formParams;
    	parent.redirectVentanaModal("contratanteProgramaPago", path, null);
    	//sendRequestJQ(null, path, 'contenido', null);
    	
    }
    
    function cerrarModificarCliente(){
    	
    	parent.cerrarVentanaModal("contratanteProgramaPago", null);
    }
    


    function validaCURPPersonaClientesJS(){
    	var resultado=false;
    	var estadoNacimiento = jQuery('#cliente\\.claveEstadoNacimiento option:selected').text();
    	var curp = dwr.util.getValue("cliente.codigoCURP");
    	var fechaNac = dwr.util.getValue("cliente.fechaNacimiento");
    	var nombre = dwr.util.getValue("cliente.nombre");
    	var apellidoPat = dwr.util.getValue("cliente.apellidoPaterno");
    	var apellidoMat = dwr.util.getValue("cliente.apellidoMaterno");
    	var sexo = dwr.util.getValue("cliente.sexo");	
    	var tipoPersona = dwr.util.getValue("cliente.claveTipoPersona");
    	if(tipoPersona ==1 &&
    	   estadoNacimiento!=null && estadoNacimiento!='' &&
    	   curp!=null 			  && curp!='' 			  &&
    	   fechaNac!=null 		  && fechaNac!='' &&
    	   nombre!=null 		  && nombre!='' &&
    	   apellidoPat!=null 	  && apellidoPat!='' &&
    	   apellidoMat!=null 	  && apellidoMat!='' &&
    	   sexo!=null 			  && sexo!=''){		
    	   validacionService.validaCURPPersonaAgentes(estadoNacimiento,curp,fechaNac,nombre,apellidoPat,apellidoMat,sexo,tipoPersona,function(data){
    			 if(!data){
    				 parent.mostrarMensajeInformativo("CURP invalida, favor de verificar","10");	
    				 return false;
    			 }else{
    				 if(tipoPersona==1){
    					 parent.mostrarMensajeInformativo('CURP valida para los datos proporcionados',"30");				 
    				 }			 
    				 return true;
    			 }
    	   });
    	}else{
    		parent.mostrarMensajeInformativo('Datos insuficientes para validacion de CURP',"10");	
    		return false;
    	}	
    }
    
    function copiarDatosGenerales(){
    	var tipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
    	if(tipoPersonaMoral){
    		dwr.util.setValue("cliente.razonSocialFiscal",dwr.util.getValue("cliente.razonSocial"));
    		dwr.util.setValue("cliente.fechaNacimientoFiscal",dwr.util.getValue("cliente.fechaConstitucion"));		
    	}else{
    		dwr.util.setValue("cliente.nombreFiscal",dwr.util.getValue("cliente.nombre"));
    		dwr.util.setValue("cliente.apellidoPaternoFiscal",dwr.util.getValue("cliente.apellidoPaterno"));	
    		dwr.util.setValue("cliente.apellidoMaternoFiscal",dwr.util.getValue("cliente.apellidoMaterno"));
    	}
    	
    	var idPais=dwr.util.getValue("cliente.idPaisString");
    	var idEstado=dwr.util.getValue("cliente.idEstadoString");
    	var idCiudad=dwr.util.getValue("cliente.idMunicipioString");
    	var idColonia=dwr.util.getValue("cliente.nombreColonia");
    	var calleNumero=dwr.util.getValue("cliente.nombreCalle");
    	var codPostal=dwr.util.getValue("cliente.codigoPostal");
    	var esNuevaColonia=jQuery("#idColoniaCheck").attr("checked");
    	var nuevaColonia=dwr.util.getValue("cliente.nombreColoniaDiferente");
    	if(calleNumero!='' && (idColonia!=''|| (nuevaColonia!='' && esNuevaColonia==true)) && codPostal!=''){
    		var estadoName='cliente.idEstadoFiscal';		
    		var ciudadName='cliente.idMunicipioFiscal';
    		var coloniaName='cliente.nombreColoniaFiscal';
    		var calleNumeroName = 'cliente.nombreCalleFiscal';
    		var codigoPostalName = 'cliente.codigoPostalFiscal';		
    		var coloniaDiferenteName='cliente.nombreColoniaDiferenteFiscal';
    		dwr.util.setValue(estadoName, idEstado);
    		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
    		dwr.util.setValue(ciudadName, idCiudad);
    		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
    		dwr.util.setValue(coloniaName, idColonia);
    		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
    		dwr.util.setValue(calleNumeroName, calleNumero);		
    		dwr.util.setValue(codigoPostalName, codPostal);
    		jQuery('#idColoniaCheckFiscal').attr("checked",esNuevaColonia);
    		dwr.util.setValue(coloniaDiferenteName, nuevaColonia);
    		}
    		else{
    			alert('El domicilio a copiar esta incompleto, favor de verificar.');
    		}
    }
    
    function asignarClienteAProgramaPago(idCliente, idProgramaPago, idToCotizacion){
    	var path = "/MidasWeb/componente/programapago/asignarClienteAProgramaPago.action?idCliente="+idCliente+"&idToProgramaPago="+idProgramaPago+"&idToCotizacion="+idToCotizacion;
    	parent.redirectVentanaModal("contratanteProgramaPago", path, null);
    }