/**
 * 
 */
var incisosGrid;
function buscarIncisos(page, nuevoFiltro, filtrar)
{
	var selectedRowsIds = "";
	
	//Se revisan las que se pudieron haber "des-seleccionado"
	var unchecked = "";
	
	if (incisosGrid != null && !nuevoFiltro) {
		selectedRowsIds += incisosGrid.getCheckedRows(0);
		
		incisosGrid.forEachRow(function(id){
			if (incisosGrid.cells(id,0).getValue()=="0") unchecked +=id + ",";
			});
		
	}
	
	unchecked += "X";
	
	if (jQuery("#elementosSeleccionados").val() != null && jQuery("#elementosSeleccionados").val() != "") {
		selectedRowsIds += "," + jQuery("#elementosSeleccionados").val();
	}
	
	selectedRowsIds = unchecked + selectedRowsIds;
		
	var posPath = 'posActual='+page+'&elementosSeleccionados=' + selectedRowsIds + '&funcionPaginar='+'buscarIncisos'+'&divGridPaginar='+'incisosListadoGrid';
	var parametrosForm = '&' + jQuery(document.listadoIncisosFiltrosForm).serialize();
	
	if(!nuevoFiltro){
	
		posPath = 'posActual='+page+'&elementosSeleccionados=' + selectedRowsIds + '&' + jQuery(document.paginadoGridForm).serialize();
		filtrar = true;
	}	
	sendRequestJQ(null, "/MidasWeb/componente/incisos/busquedaIncisosPaginacion.action" + "?filtrar=" + filtrar + '&' + posPath + parametrosForm, 'gridIncisosPaginado', 'obtenerListadoIncisos(' + filtrar +');');
}

function obtenerListadoIncisos(filtrar)
{
	
	var parametrosForm = '&' + jQuery(document.listadoIncisosFiltrosForm).serialize();
	
	document.getElementById("incisosListadoGrid").innerHTML = '';	
	incisosGrid = new dhtmlXGridObject('incisosListadoGrid');
	mostrarIndicadorCarga('indicador');	
	incisosGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');		
    });	
	
	if(jQuery("#tipoEndoso").val() == 24 || jQuery("#tipoEndoso").val() == 25)
	{
		incisosGrid.attachEvent("onCheckbox",actualizarFechaInicioVigenciaYMotivoEndoso);	
		incisosGrid.attachEvent("onXLE",actualizarFechaInicioVigenciaYMotivoEndosoOnLoad);
		incisosGrid.attachEvent("onXLE",setCheckedRowOrDefault);
		
	}else if (jQuery("#tipoEndoso").val() == 26)
	{
		incisosGrid.attachEvent("onXLE",setCheckedRowOrDefault);		
	}
	
	
	var posPath = 'posActual='+jQuery("#posActual").val() +'&elementosSeleccionados=' + jQuery("#elementosSeleccionados").val() +  '&'+ jQuery(document.paginadoGridForm).serialize();	
	
	incisosGrid.load("/MidasWeb/componente/incisos/busquedaIncisos.action" + "?filtrar="  + filtrar + '&' + posPath + parametrosForm);	
	
	var accionE = dwr.util.getValue("accionEndoso"); 
	if(accionE == "3")//Solo Consulta
	{
		incisosGrid.attachEvent("onRowCreated",function(id){
		     var cell = incisosGrid.cells(id,0); //checkbox cell
		     if (cell.getAttribute("disabled")) cell.setDisabled(true);
		})	    	
	}
	


}

function limpiarFiltrosListadoDinamicoIncisos()
{
	jQuery('#listadoIncisosFiltrosForm').each (function(){
		  this.reset();
	});	
	
	removeAllOptionsAndSetHeader(document.getElementById('filtros.incisoAutoCot.negocioPaqueteId'), 
			'', 
			"SELECCIONE...");
}

function actualizarFechaInicioVigenciaYMotivoEndoso(rowId,cellInd,state)
{
	jQuery("#idSiniestroPT").val(incisosGrid.cellById(rowId,1).getValue());
	jQuery("#numeroSiniestro").val(incisosGrid.cellById(rowId,2).getValue());
	jQuery("#fechaIniVigenciaEndoso").val(incisosGrid.cellById(rowId,3).getValue());
	jQuery("#tipoIndemnizacion").val(incisosGrid.cellById(rowId,4).getValue());		
}

function actualizarFechaInicioVigenciaYMotivoEndosoOnLoad()
{
	var rowIdSeleccionado = incisosGrid.getCheckedRows(0);
	if(rowIdSeleccionado != null && rowIdSeleccionado != "")
	{
		jQuery("#idSiniestroPT").val(incisosGrid.cellById(rowIdSeleccionado,1).getValue());
		jQuery("#numeroSiniestro").val(incisosGrid.cellById(rowIdSeleccionado,2).getValue());
		jQuery("#fechaIniVigenciaEndoso").val(incisosGrid.cellById(rowIdSeleccionado,3).getValue());
		jQuery("#tipoIndemnizacion").val(incisosGrid.cellById(rowIdSeleccionado,4).getValue());	
		
		//incisosGrid.cellById(rowIdSeleccionado,0).setValue(1);
				
	}else
	{
		//Precargar los valores del primer elemento.
		jQuery("#idSiniestroPT").val(incisosGrid.cellByIndex(0,1).getValue());
		jQuery("#numeroSiniestro").val(incisosGrid.cellByIndex(0,2).getValue());
		jQuery("#fechaIniVigenciaEndoso").val(incisosGrid.cellByIndex(0,3).getValue());
		jQuery("#tipoIndemnizacion").val(incisosGrid.cellByIndex(0,4).getValue());
		
		//incisosGrid.cellByIndex(0,0).setValue(1);		
	}	
}

function setCheckedRowOrDefault()
{
	var rowIdSeleccionado = incisosGrid.getCheckedRows(0);
	if(rowIdSeleccionado != null && rowIdSeleccionado != "")
	{
		incisosGrid.cellById(rowIdSeleccionado,0).setValue(1);		
	}else
	{
		incisosGrid.cellByIndex(0,0).setValue(1);		
	}	
}



