
var targetNegocioSeccion;
var targetMarcaVehiculo;
var targetEstiloVehiculo;
var targetModelo;
var targetTipoUso;
var targetPaquete;
var targetDescripcion;
var targetModificadoresDescripcion;
var win; 
var dhxWinsCaracteristicas;
var dataResult = null;
//muestra - oculta el buscador dinamico
function mostrarBuscador(){
	if(!jQuery('#buscador').is(":visible")){
		jQuery('#buscador').show("slow");
		jQuery('#descripcionBusqueda').focus();
	}
}


function onChangeEstado(target, estadoSelect){
	$('#idCodigo').val("");
	var frame = parent.getWindowContainerFrame('inciso');
	var idEstado = dwr.util.getValue(jQuery(estadoSelect).selector);
	var idToCotizacion = dwr.util.getValue('idCotizacion');
	if(idEstado != null  && idEstado != headerValue){
		listadoService.getMunicipiosPorEstadoId(idToCotizacion,idEstado,
				function(data){
					addOptions(target,data, frame);
				});	
	}else{
		addOptions(target,null, frame);
	}
	
}


function onChangeLineaNegocio(target, negocioSeccionSelect){
	var idNegocioSeccion = null;
	var frame = parent.getWindowContainerFrame('inciso');
	jQuery('#divMessageInfo').hide();
	jQuery('#descripcionBusqueda').val('');	
	if(negocioSeccionSelect != null){
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}
	targetNegocioSeccion = target;
	restartCombo(target, headerValue, "Cargando", "id", "value", frame);
	if(idNegocioSeccion != null && idNegocioSeccion != headerValue){
		listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion,
				function(data){
					if (sizeMap(data) == 0) {
						if(targetNegocioSeccion != null){
							restartCombo(targetNegocioSeccion, headerValue, "No se encontraron registros ...", "id", "value", frame);							
						}
					} else {
						addOptions(targetNegocioSeccion, data, frame);
						listadoService.getCountNegocioEstiloVehiculo(idNegocioSeccion,dwr.util.getValue("idMonedaName"),function(data){
							if(data > 0){
								 jQuery('#divMessageInfo').show();
				            	 jQuery('span[id=message]').text('Este negocio tiene estilos limitados');
							}
						});
						if (targetMarcaVehiculo != null) {
							onChangeMarcaVehiculo(targetMarcaVehiculo, null,
									null);
						}
						if(frame == null){							
							dwr.util.setValue(targetDescripcion, '');
						}else{
							frame.contentWindow.dwr.util.setValue(targetDescripcion, '');
						}
					}
				});	
	}else{
		addOptions(targetNegocioSeccion,null, frame);
		onChangeMarcaVehiculo(targetMarcaVehiculo,null,null);
	}
}

function onChangeMarcaVehiculo(target, marcaVehiculoSelect, negocioSeccionSelect){
	var idMarcaVehiculo = null;	
	var idNegocioSeccion = null;
	var frame = parent.getWindowContainerFrame('inciso');
	jQuery('#descripcionBusqueda').val('');
	if(marcaVehiculoSelect != null){
		idMarcaVehiculo = dwr.util.getValue(jQuery(marcaVehiculoSelect).selector);
	}	
	if(dwr.util.getValue("cotizacionExpress")=="false"){
		restartCombo(target, headerValue, "Cargando", "id", "value", frame);
		if(negocioSeccionSelect != null){
			idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
		}
	}else{
		if(jQuery('#cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.descripcionEstilo') != null){
			jQuery('#cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.descripcionEstilo').val('');
		}
		restartCombo(target, headerValue, "Cargando", "id", "value", frame);
		if(negocioSeccionSelect != null){
			idNegocioSeccion = dwr.util.getValue(negocioSeccionSelect);
		}
	}
	var idMoneda = dwr.util.getValue("idMonedaName");
	targetMarcaVehiculo = target;
	if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idMarcaVehiculo != headerValue  ){
		listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion,idMoneda,
				function(data){
					if (sizeMap(data) == 0) {
						restartCombo(targetMarcaVehiculo, headerValue, "No se encontraron registros ...", "id", "value", frame);						
					}else{
						addOptions(targetMarcaVehiculo,data, frame);
						if(targetEstiloVehiculo!=null){
							onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
						}
					}
				});	
	}else{
		addOptions(targetMarcaVehiculo,null, frame);
		onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
	}	
}


function onChangeMarcaVehiculoSetEstiloId(target, marcaVehiculoSelect, negocioSeccionSelect,estiloId){
	var idNegocioSeccion = null;
	var idMarcaVehiculo = null;
	var frame = parent.getWindowContainerFrame('inciso');
	var idMoneda = dwr.util.getValue("idMonedaName");
	restartCombo(target, headerValue, "Cargando", "id", "value", frame);
	if(marcaVehiculoSelect != null){
		idMarcaVehiculo = dwr.util.getValue(jQuery(marcaVehiculoSelect).selector);
	}	
	if(negocioSeccionSelect != null){		
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}	
	targetMarcaVehiculo = target;
	if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idMarcaVehiculo != headerValue ){		
		listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion,idMoneda,
				function(data){						
						if (sizeMap(data) == 0) {
							if(targetMarcaVehiculo != null){
								restartCombo(targetMarcaVehiculo, headerValue, "No se encontraron registros ...", "id", "value", frame);							
							}
						}else{														
							addOptionAndSelect(targetMarcaVehiculo,data,estiloId, frame);
							onChangeEstiloVehiculo(elementoModeloId,elementoEstiloId,elementoNegocioSeccion);
						}
				});	
	}else{
		addOptions(targetMarcaVehiculo,null, frame);
		onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
		dwr.util.setValue(jQuery('incisoCotizacion.incisoAutoCot.descripcionFinal').selector, '');
	}
	
}

function onChangeEstiloVehiculo(target, estiloVehiculoSelect, negocioSeccionSelect){	
	var idEstiloVehiculo = null;
	var idNegocioSeccion = null;
	var frame = parent.getWindowContainerFrame('inciso');
	var idMoneda = dwr.util.getValue("idMonedaName");
	restartCombo(target, headerValue, "Cargando", "id", "value", frame);
	if(estiloVehiculoSelect == null){
		estiloVehiculoSelect = dwr.util.getValue("idEstiloVehiculoNameRef");
	}
	if(negocioSeccionSelect == null){
		negocioSeccionSelect = dwr.util.getValue("idNegocioSeccionNameRef");
	}
	if(estiloVehiculoSelect != null){
		idEstiloVehiculo = dwr.util.getValue(jQuery(estiloVehiculoSelect).selector);
	}
	if(negocioSeccionSelect != null){
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}	
	targetEstiloVehiculo = target;	
	if(idEstiloVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idEstiloVehiculo != headerValue ){		
		try{
			listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(idMoneda,idEstiloVehiculo, idNegocioSeccion,
					function(data){
						if (sizeMap(data) == 0) {
							restartCombo(targetEstiloVehiculo, headerValue, "No se encontraron registros ...", "id", "value", frame);							
						}else{
							addOptions(targetEstiloVehiculo,data, frame);
							if(targetModelo!=null && dwr.util.getValue("cotizacionExpress")=="false"){
								onChangeModeloVehiculo(targetModelo,estiloVehiculoSelect, negocioSeccionSelect);
							}							
						}
					});	
		}catch(e){
			
		}
	}else{
		addOptions(targetEstiloVehiculo,null, frame);
		if(dwr.util.getValue("cotizacionExpress")=="false"){
			onChangeModeloVehiculo(targetModelo,null,null);
		}
	}	

}

function onChangeModeloVehiculo(target, estiloVehiculoSelect, negocioSeccionSelect){
	var idNegocioSeccion = null;
	var idEstiloVehiculo = null;
	var frame = parent.getWindowContainerFrame('inciso');
	restartCombo(target, headerValue, "Cargando", "id", "value", frame);
	if(negocioSeccionSelect != null){
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}
	if(dwr.util.getValue("cotizacionExpress")=="false"){
		if(estiloVehiculoSelect != null){	
			idEstiloVehiculo = dwr.util.getValue(jQuery(estiloVehiculoSelect).selector);
		}
		targetModelo = target;		
		if(idEstiloVehiculo != null && idEstiloVehiculo != headerValue ){
			listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(idEstiloVehiculo,idNegocioSeccion,
					function(data){
				
				if (sizeMap(data) == 0) {
					restartCombo(targetModelo, headerValue, "No se encontraron registros ...", "id", "value", frame);				
				}else{	
						listadoService.getTipoUsoDefault(idNegocioSeccion,function(value){
							addOptionAndSelect(targetModelo,data, value, frame);							
							excuteOnchange(targetModelo);
							});
						if(targetTipoUso != null){
							onChangeTipoUsoVehiculo(targetTipoUso, negocioSeccionSelect);
						}
						setDescripcionVehiculo(estiloVehiculoSelect, targetDescripcion);
				}
			});	
		}else{
			addOptions(targetModelo,null, frame);
			onChangeTipoUsoVehiculo(targetTipoUso,null);
		}	
	}
}


function onChangeTipoUsoVehiculo(target, negocioSeccionSelect){
	var idNegocioSeccion = null; 
	var frame = parent.getWindowContainerFrame('inciso');
	if(negocioSeccionSelect != null){
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}
	targetTipoUso = target;	
	//validataSelects();
	restartCombo(target, headerValue, "Cargando", "id", "value", frame);		
	if(idNegocioSeccion != null && idNegocioSeccion != headerValue ){
		listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion,
				function(data){
			if (sizeMap(data) == 0) {
				restartCombo(targetTipoUso, headerValue, "No se encontraron registros ...", "id", "value", frame);				
			}else{				
				addOptions(targetTipoUso, data, frame);
			}
			eval(dwr.util.getValue("onChangePaquete"));
			mostrarPorcentajeDescuentoEstado();
		});	
	}else{
		addOptions(targetTipoUso,null, frame);
		eval(dwr.util.getValue("onChangePaquete"));
		mostrarPorcentajeDescuentoEstado();
	}	
	
}


function ajustarCaracteristicasVehiculo(targetEstilo){
	var descripcion = null;
	var estiloId = null;
	var modelo = null;
	var modificadoresDescripcion = null;
	if(targetDescripcion != null){		
		var frame = parent.getWindowContainerFrame('inciso');		
		if(frame == null){	
			descripcion = dwr.util.getValue(targetDescripcion);	
			estiloId = dwr.util.getValue(targetEstilo);
			modelo = dwr.util.getValue(targetModelo);
			modificadoresDescripcion = dwr.util.getValue(targetModificadoresDescripcion);
		}else{			
			descripcion = frame.contentWindow.dwr.util.getValue(targetDescripcion);
			estiloId = frame.contentWindow.dwr.util.getValue(targetEstilo);
			modelo = frame.contentWindow.dwr.util.getValue(targetEstiloVehiculo);
			modificadoresDescripcion = frame.contentWindow.dwr.util.getValue(targetModificadoresDescripcion);
		}
	
		if(descripcion == null || descripcion == '' || modelo == null || modelo == ''){
			alert('Es necesario seleccionar un veh\u00EDculo y modelo antes de ajustar sus caracter\u00EDsticas');
		}else{				
			var path= "/MidasWeb/componente/vehiculo/mostrarOtrasCaract.action?modificadoresDescripcionName="
				+dwr.util.getValue("modificadoresDescripcionName")
				+"&descripcionFinalName="+dwr.util.getValue("descripcionFinalName")
				+"&modificadoresDescripcion="+modificadoresDescripcion
				+"&cotizacionExpress="+dwr.util.getValue("cotizacionExpress")
				+"&estiloSeleccionado="+estiloId;	
			parent.mostrarVentanaModal("ajustarCaracteristicas", "Ajustar Caracter\u00EDsticas", 50, 50, 600, 530, path);
		}		
	}	
}

function definirOtrasCaract(){	
	parent.submitVentanaModal("ajustarCaracteristicas", document.guardarOtrasCaractForm);
}


function mostrarModalComponenteVehiculo(id, text, x, y, width, height, url){
	
	if(parent.incisoWindow == null){
		dhxWinsCaracteristicas = new dhtmlXWindows();
		dhxWinsCaracteristicas.enableAutoViewport(true);
		dhxWinsCaracteristicas.setImagePath("/MidasWeb/img/dhxwindow/");
		win = dhxWinsCaracteristicas.createWindow(id, x, y, width, height);
	}else{
		win = parent.incisoWindow.createWindow(id, x, y, width, height);
	}
	win.setText(text);
	win.center();
	win.setModal(true);
	win.attachURL(url);
}



function sendRequestModal(fobj, actionURL, pNextFunction, dataTypeParam) {
	blockPage();
	var ifr; 
	if (_isIE) {
		ifr = parent.incisoWindow.window('inciso')._frame.contentWindow.documentgetElementById("ajustarCaracteristicasDiv");
	 } else {
		ifr = parent.incisoWindow.window('inciso')._frame.contentDocument.getElementById("ajustarCaracteristicasDiv");
	 }

	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: true,
	    success: function(data) {
	    	ifr.innerHTML= '';
	    	ifr.innerHTML = data;
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (pNextFunction !== null) {
					pNextFunction();
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   	    
	});

}

function closeCaracteristicas(){
	parent.cerrarVentanaModal('ajustarCaracteristicas');
	/*
	if(parent.incisoWindow == null){
		dhxWinsCaracteristicas.window('ajustarCaracteristicas').setModal(false);
		dhxWinsCaracteristicas.window('ajustarCaracteristicas').hide();
	}else{
		parent.incisoWindow.window('ajustarCaracteristicas').setModal(false);
		parent.incisoWindow.window('ajustarCaracteristicas').hide();
	}*/
}

function setDescripcionVehiculo(source, target){
	var frame = parent.getWindowContainerFrame('inciso');
	var w;
	var selected_text;
	/*lert(frame);
	alert(source);
	alert(target);*/
	if(frame == null){	
		w =	document.getElementById(source).selectedIndex;
		selected_text = document.getElementById(source).options[w].text;
		//var descripcion = selected_text.split('-');
		//if(descripcion.length>1){
		//	selected_text =  jQuery.trim(selected_text.substring(7));
		//}
		document.getElementById(target).value = selected_text;
	}else{
		w =	frame.contentWindow.document.getElementById(source).selectedIndex;
		selected_text = frame.contentWindow.document.getElementById(source).options[w].text;
		//var descripcion = selected_text.split('-');
		//if(descripcion.length>1){
		//	selected_text =  jQuery.trim(selected_text.substring(7));
		//}
		frame.contentWindow.dwr.util.setValue(target, selected_text);		
	}
	
	
}


function mostrarPorcentajeDescuentoEstado(){
	var idToNegPaqueteSeccion = 0;
	var negocioPaqueteSeccionSelect = dwr.util.getValue("idNegocioPaqueteName");
	if(negocioPaqueteSeccionSelect != null){
		idToNegPaqueteSeccion = dwr.util.getValue(jQuery(negocioPaqueteSeccionSelect).selector);
	}
	
	listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion,function(data){
		if(data){
			if(!jQuery('#porcentajeDescuentoEstado').is(":visible")){
				jQuery('#porcentajeDescuentoEstado').show("slow");
			}
		} else {
			if(jQuery('#porcentajeDescuentoEstado').is(":visible")){
				jQuery('#porcentajeDescuentoEstado').hide("slow");
			}
		}
	});
}

function validaPorcentajeDescuentoEstado(target) {
	var idToNegocio = jQuery('#negocio').val();
	var idToCotizacion = dwr.util.getValue('idCotizacion');
	var negocioEstadoSelect = dwr.util.getValue("idEstadoName");
	if(negocioEstadoSelect != null){
		var idEstado = dwr.util.getValue(jQuery(negocioEstadoSelect).selector);
	}
	var descuento = dwr.util.getValue(target);
	if ( descuento > 100){
		parent.mostrarMensajeInformativo(
				"El Descuento por Estado no es v\u00e1lido.", "20", null, null);
		dwr.util.setValue(target, 0.0);
	} else {
		listadoService.getPcteDescuentoMaximoPorEstado(idToCotizacion, idToNegocio, idEstado,function(data){
			if(descuento > data){
				parent.mostrarMensajeInformativo(
						"Se sugiere que el m\u00e1ximo porcentaje de Descuento por Estado sea de " + data + " %", "20", null, null);
			}
		});
	}
}

function obtenerPctDescuentoEstadoDefault(targetPctDescuentoEstado) {
	var idToCotizacion = dwr.util.getValue('idCotizacion');
	var negocioEstadoSelect = dwr.util.getValue("idEstadoName");
	if(negocioEstadoSelect != null){
		idEstado = dwr.util.getValue(jQuery(negocioEstadoSelect).selector);
	}
	listadoService.getPcteDescuentoDefaultPorEstado(idToCotizacion, idEstado,function(data){
		if(data){
			dwr.util.setValue(targetPctDescuentoEstado, data);			
		}else{
			dwr.util.setValue(targetPctDescuentoEstado, 0.0);
		}
	});
}

function mostrarPorcentajeDescuentoEstadoDefault(){
    var targetPctDescuentoEstado = dwr.util.getValue("pctDescuentoEstadoName");
	var idToNegPaqueteSeccion = 0;
	var negocioPaqueteSeccionSelect = dwr.util.getValue("idNegocioPaqueteName");
	if(negocioPaqueteSeccionSelect != null){
		idToNegPaqueteSeccion = dwr.util.getValue(jQuery(negocioPaqueteSeccionSelect).selector);
	}
	
	listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion,function(data){
		if(data){
			obtenerPctDescuentoEstadoDefault(targetPctDescuentoEstado);
		} else {
			dwr.util.setValue(targetPctDescuentoEstado, 0.0);
		}
	});
	ocultaGuardar();
}
