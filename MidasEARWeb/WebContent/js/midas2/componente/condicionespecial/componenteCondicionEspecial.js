/**
 * 
 */
var componenteCondicionEspecialAsociadoGrid;
var componenteCondicionEspecialDisponibleGrid;

function initGridsComponenteCondicionEspecial(){
	jQuery("#condicionesEspecialesDisponiblesGrid").empty();
	getCondicionesEspecialesDisponibles();
	getCondicionesEspecialesAsociadas();
}

function getCondicionesEspecialesDisponibles(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	componenteCondicionEspecialDisponibleGrid = new dhtmlXGridObject('condicionesEspecialesDisponiblesGrid');
	componenteCondicionEspecialDisponibleGrid.load( "/MidasWeb/componente/condicionespecial/obtenerCondicionesDisponibles.action?idToNegocio=" + idToNegocio + "&idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso);
}


function getCondicionesEspecialesAsociadas(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	componenteCondicionEspecialAsociadoGrid = new dhtmlXGridObject('condicionesEspecialesAsociadasGrid');
	componenteCondicionEspecialAsociadoGrid.load("/MidasWeb/componente/condicionespecial/obtenerCondicionesAsociadas.action?idToNegocio=" + idToNegocio + "&idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso);
	// Creacion del DataProcessor
	url = "/MidasWeb/componente/condicionespecial/relacionarCondicionEspecial.action?idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso;
	url.concat(jQuery(document.condicionEspecialForm).serialize());
	componenteCondicionEspecialProcessor = new dataProcessor(url);
	componenteCondicionEspecialProcessor.enableDataNames(true);
	componenteCondicionEspecialProcessor.setTransactionMode("POST");
	componenteCondicionEspecialProcessor.setUpdateMode("row");
	componenteCondicionEspecialProcessor.attachEvent("onRowMark", function(){
		componenteCondicionEspecialProcessor.sendAllData();
		if(componenteCondicionEspecialProcessor.getSyncState()){
			initGridsComponenteCondicionEspecial();
		}else{
			setTimeout(function(){initGridsComponenteCondicionEspecial()}, 1000);	
		}
	});

	componenteCondicionEspecialProcessor.init(componenteCondicionEspecialAsociadoGrid);
}

function searchCondicionesEspeciales(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	var codigoCondicionEspecial = jQuery("#codigoCondicionEspecial").val();
	var url = "/MidasWeb/componente/condicionespecial/obtenerCondicionesDisponibles.action?idToNegocio=" + idToNegocio + "&idToCotizacion=" + idToCotizacion + "&codigoNombre=" + codigoCondicionEspecial + "&numeroInciso=" + numeroInciso;
	
	componenteCondicionEspecialDisponibleGrid.load(url);
	document.getElementById("esFiltrado").value = 1;
}

function borrarAsociadas(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var codNombre = jQuery("#codigoCondicionEspecial").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	var accion = 'inserted';
	jQuery.post(
			"/MidasWeb/componente/condicionespecial/asociarTodas.action", 
			{
				codigoNombre : codNombre,
				accion : 'deleted',
				idToNegocio : idToNegocio,
				idToCotizacion : idToCotizacion,
				numeroInciso : numeroInciso
			},	function( response ){
					initGridsComponenteCondicionEspecial();
				}
			);
	
}


function asociarTodas(){
	var idToNegocio = jQuery("#idToNegocio").val();
	var idToCotizacion = jQuery("#idToCotizacion").val();
	var codNombre = jQuery("#codigoCondicionEspecial").val();
	var numeroInciso = jQuery("#numeroInciso").val();
	var accion = 'inserted';
	var esFiltrado = jQuery("#esFiltrado").val();
	jQuery.post(
			"/MidasWeb/componente/condicionespecial/asociarTodas.action", 
			{
				codigoNombre : codNombre,
				accion : 'inserted',
				idToNegocio : idToNegocio,
				idToCotizacion : idToCotizacion,
				numeroInciso : numeroInciso,
				esFiltrado : esFiltrado
			},	function( response ){
					initGridsComponenteCondicionEspecial();
				}
			);

}