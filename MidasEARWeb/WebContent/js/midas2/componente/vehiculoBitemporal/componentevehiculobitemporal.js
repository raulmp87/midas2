
var targetNegocioSeccion;
var targetMarcaVehiculo;
var targetEstiloVehiculo;
var targetModelo;
var targetTipoUso;
var targetPaquete;
var targetDescripcion;
var win; 
var dhxWinsCaracteristicas;
var dataResult = null;
//muestra - oculta el buscador dinamico
function mostrarBuscador(){
	if(!jQuery('#buscador').is(":visible")){
		jQuery('#buscador').show("slow");
		jQuery('#descripcionBusqueda').focus();
	}
	
}


function onChangeEstado(target, estadoSelect){
	$('#idCodigo').val("");
	var idEstado = dwr.util.getValue(jQuery(estadoSelect).selector);
	var idToCotizacion = dwr.util.getValue('incisoCotizacion.id.idToCotizacion');
	if(idEstado != null  && idEstado != headerValue){
		listadoService.getMunicipiosPorEstadoId(idToCotizacion,idEstado,
				function(data){
					addOptions(target,data);
				});	
	}else{
		addOptions(target,null);
	}
	
}


function onChangeLineaNegocio(target, negocioSeccionSelect){
	jQuery('#divMessageInfo').hide();
	var idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	targetNegocioSeccion = target;
	parent.removeAllOptionsAndSetHeader(
			document.getElementById(target), parent.headerValue,
			"Cargando...");
	if(idNegocioSeccion !=null && idNegocioSeccion != headerValue){
		listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion,
				function(data){
					if (sizeMap(data) == 0) {
						dwr.util.removeAllOptions(targetNegocioSeccion);
						dwr.util.addOptions(targetNegocioSeccion, [ {
							id : headerValue,
							value : "No se encontraron registros ..."
						} ], "id", "value");
					} else {
						addOptions(targetNegocioSeccion, data);
						listadoService.getCountNegocioEstiloVehiculo(idNegocioSeccion,dwr.util.getValue("idMonedaName"),function(data){
							if(data > 0){
								 jQuery('#divMessageInfo').show();
				            	 jQuery('span[id=message]').text('Este negocio tiene estilos limitados');
							}
						});
						if (targetMarcaVehiculo != null) {
							onChangeMarcaVehiculo(targetMarcaVehiculo, null,
									null);
						}
						document.getElementById(targetDescripcion).value = "";
					}
				});	
	}else{
		addOptions(targetNegocioSeccion,null);
		onChangeMarcaVehiculo(targetMarcaVehiculo,null,null);
	}
}

function onChangeMarcaVehiculo(target, marcaVehiculoSelect, negocioSeccionSelect){
	var idMarcaVehiculo = dwr.util.getValue(jQuery(marcaVehiculoSelect).selector);
	var idNegocioSeccion;

	if(dwr.util.getValue("cotizacionExpress")=="false"){
		removeAllOptionsAndSetHeader(
				document.getElementById(target), parent.headerValue,
				"Cargando...");		
		idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	}else{
		removeAllOptionsAndSetHeader(
				document.getElementById(target), parent.headerValue,
				"Cargando...");				
		idNegocioSeccion = dwr.util.getValue(negocioSeccionSelect);
	}
	var idMoneda = dwr.util.getValue("idMonedaName");
	targetMarcaVehiculo = target;
	if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idMarcaVehiculo != headerValue  ){
		listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion,idMoneda,
				function(data){
					if (sizeMap(data) == 0) {
						dwr.util.removeAllOptions(targetMarcaVehiculo);
						dwr.util.addOptions(targetMarcaVehiculo, [{id: headerValue, value:"No se encontraron registros ..." }],"id", "value");
					}else{
						addOptions(targetMarcaVehiculo,data);
						if(targetEstiloVehiculo!=null){
							onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
						}
					}
				});	
	}else{
		addOptions(targetMarcaVehiculo,null);
		onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
	}	
}


function onChangeMarcaVehiculoSetEstiloId(target, marcaVehiculoSelect, negocioSeccionSelect,estiloId){
	var idMarcaVehiculo = dwr.util.getValue(jQuery(marcaVehiculoSelect).selector);
	var idNegocioSeccion;
	parent.removeAllOptionsAndSetHeader(
			document.getElementById(target), parent.headerValue,
			"CARGANDO...");		
	
	idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);

	var idMoneda = dwr.util.getValue("idMonedaName");
	targetMarcaVehiculo = target;
	if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idMarcaVehiculo != headerValue ){
		listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion,idMoneda,
				function(data){
						if (sizeMap(data) == 0) {
							dwr.util.removeAllOptions(targetMarcaVehiculo);
							dwr.util.addOptions(targetMarcaVehiculo, [{id: headerValue, value:"No se encontraron registros ..." }],"id", "value");
						}else{
								addOptionAndSelect(targetMarcaVehiculo,data,estiloId);
								onChangeEstiloVehiculo(elementoModeloId,elementoEstiloId,elementoNegocioSeccion);
						}
				});	
	}else{
		addOptions(targetMarcaVehiculo,null);
		onChangeEstiloVehiculo(targetEstiloVehiculo, null, null);
	}
	
}

function onChangeEstiloVehiculo(target, estiloVehiculoSelect, negocioSeccionSelect){
	var idEstiloVehiculo = dwr.util.getValue(jQuery(estiloVehiculoSelect).selector);
	var idNegocioSeccion;
	
	parent.removeAllOptionsAndSetHeader(
			document.getElementById(target), parent.headerValue,
			"Cargando...");				
	idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);

	var idMoneda = dwr.util.getValue("idMonedaName");
	targetEstiloVehiculo = target;
	
	if(idEstiloVehiculo != null && idNegocioSeccion != null && idMoneda != null && idNegocioSeccion != headerValue && idEstiloVehiculo != headerValue ){
		listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(idMoneda,idEstiloVehiculo, idNegocioSeccion,
				function(data){
					if (sizeMap(data) == 0) {
						dwr.util.removeAllOptions(targetEstiloVehiculo);
						dwr.util.addOptions(targetEstiloVehiculo, [{id: headerValue, value:"No se encontraron registros ..." }],"id", "value");
					}else{
							addOptions(targetEstiloVehiculo,data);
							if(targetModelo!=null && dwr.util.getValue("cotizacionExpress")=="false"){
								onChangeModeloVehiculo(targetModelo,null, null);
							}
					}
				});	
		
	}else{
		addOptions(targetEstiloVehiculo,null);
		if(dwr.util.getValue("cotizacionExpress")=="false"){
			onChangeModeloVehiculo(targetModelo,null,null);
		}
	}	

}

function onChangeModeloVehiculo(target, estiloVehiculoSelect, negocioSeccionSelect){

	removeAllOptionsAndSetHeader(
			document.getElementById(target), parent.headerValue,
			"Cargando...");	
	var idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	
	if(dwr.util.getValue("cotizacionExpress")=="false"){
		idEstiloVehiculo = dwr.util.getValue(jQuery(estiloVehiculoSelect).selector);
		targetModelo = target;
		if(idEstiloVehiculo != null && idEstiloVehiculo != headerValue ){
			listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(idEstiloVehiculo,idNegocioSeccion,
					function(data){
				if (sizeMap(data) == 0) {
					dwr.util.removeAllOptions(targetModelo);
					dwr.util.addOptions(targetModelo, [{id: headerValue, value:"No se encontraron registros ..." }],"id", "value");
				}else{	
						listadoService.getTipoUsoDefault(idNegocioSeccion,function(value){
								dwr.util.addOptions(targetModelo, [{id: headerValue, value:"Cargando ..." }],"id", "value");
								addOptionsHeaderAndSelect(targetModelo, data, value,"", "Seleccione ...");
								excuteOnchange(targetModelo);
							});
						if(targetTipoUso != null){
							onChangeTipoUsoVehiculo(targetTipoUso,null);
						}
				}
					});	
		}else{
			addOptions(targetModelo,null);
			onChangeTipoUsoVehiculo(targetTipoUso,null);
		}	
	}
}


function onChangeTipoUsoVehiculo(target, negocioSeccionSelect){
	var idNegocioSeccion = dwr.util.getValue(jQuery(negocioSeccionSelect).selector);
	targetTipoUso = target;
	
	parent.removeAllOptionsAndSetHeader(
			document.getElementById(target), parent.headerValue,
			"Cargando...");		

	if(idNegocioSeccion != null && idNegocioSeccion != headerValue ){
		listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion,
				function(data){
			if (sizeMap(data) == 0) {
				dwr.util.removeAllOptions(targetTipoUso);
				dwr.util.addOptions(targetTipoUso, [{id: headerValue, value:"No se encontraron registros ..." }],"id", "value");
			}else{
					addOptions(targetTipoUso,data);
			}
				});	
	}else{
		addOptions(targetTipoUso,null);
	}
		eval(dwr.util.getValue("onChangePaquete"));

}


function ajustarCaracteristicasVehiculo(){
	var path= "/MidasWeb/componente/vehiculo/mostrarOtrasCaract.action?modificadoresDescripcionName="+dwr.util.getValue("modificadoresDescripcionName")
	+"&descripcionFinalName="+dwr.util.getValue("descripcionFinalName")
	+"&cotizacionExpress="+dwr.util.getValue("cotizacionExpress");	
	parent.mostrarVentanaModal("ajustarCaracteristicas", "Ajustar Caracter\u00EDsticas", 50, 50, 600, 530, path);	
	//mostrarModalComponenteVehiculo("ajustarCaracteristicas", 'Ajustar Caracteristicas', 50, 50, 600, 530, path);
}

function definirOtrasCaract(){
	//var path= "/MidasWeb/componente/vehiculo/definirOtrasCaract.action";
	parent.submitVentanaModal("ajustarCaracteristicas", document.guardarOtrasCaractForm);
	/*
	+ jQuery(document.guardarOtrasCaractForm).serialize();
	if(dwr.util.getValue("cotizacionExpress")=="false"){
		sendRequestModal(null,path,closeCaracteristicas,"html");
	}else{
		sendRequestJQAsync(null,path,"ajustarCaracteristicasDiv",closeCaracteristicas);
	}
	*/
}


function mostrarModalComponenteVehiculo(id, text, x, y, width, height, url){
	
	if(parent.incisoWindow == null){
		dhxWinsCaracteristicas = new dhtmlXWindows();
		dhxWinsCaracteristicas.enableAutoViewport(true);
		dhxWinsCaracteristicas.setImagePath("/MidasWeb/img/dhxwindow/");
		win = dhxWinsCaracteristicas.createWindow(id, x, y, width, height);
	}else{
		win = parent.incisoWindow.createWindow(id, x, y, width, height);
	}
	win.setText(text);
	win.center();
	win.setModal(true);
	win.attachURL(url);
}



function sendRequestModal(fobj, actionURL, pNextFunction, dataTypeParam) {
	blockPage();
	var ifr; 
	if (_isIE) {
		ifr = parent.incisoWindow.window('inciso')._frame.contentWindow.documentgetElementById("ajustarCaracteristicasDiv");
	 } else {
		ifr = parent.incisoWindow.window('inciso')._frame.contentDocument.getElementById("ajustarCaracteristicasDiv");
	 }

	jQuery.ajax({
	    type: "POST",
	    url: actionURL,
	    data: (fobj !== undefined && fobj !== null) ? jQuery(fobj).serialize(true) : null,
	    dataType: dataTypeParam,
	    async: true,
	    success: function(data) {
	    	ifr.innerHTML= '';
	    	ifr.innerHTML = data;
	    },
	    complete: function(jqXHR) {
	    	unblockPage();
	    	var mensaje = jQuery("#mensaje").text();
	    	var tipoMensaje = jQuery("#tipoMensaje").text();
	    	
	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
			}
			
			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
				if (pNextFunction !== null) {
					pNextFunction();
				}
			}
			
	    },
	    error:function (xhr, ajaxOptions, thrownError){
            unblockPage();
        }   	    
	});

}

function closeCaracteristicas(){
	parent.cerrarVentanaModal('ajustarCaracteristicas');
	/*
	if(parent.incisoWindow == null){
		dhxWinsCaracteristicas.window('ajustarCaracteristicas').setModal(false);
		dhxWinsCaracteristicas.window('ajustarCaracteristicas').hide();
	}else{
		parent.incisoWindow.window('ajustarCaracteristicas').setModal(false);
		parent.incisoWindow.window('ajustarCaracteristicas').hide();
	}*/
}

function setDescripcionVehiculo(source, target){	
	var w =	document.getElementById(source).selectedIndex;
	var selected_text = document.getElementById(source).options[w].text;
	var descripcion = selected_text.split('-');
	if(descripcion.length>1){
		selected_text =  jQuery.trim(selected_text.substring(7));
	}
	document.getElementById(target).value = selected_text;
}
