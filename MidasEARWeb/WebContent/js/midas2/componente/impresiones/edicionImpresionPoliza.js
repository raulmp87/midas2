 function mostrarIncluirEdicionPoliza(){
	 var existeEdicionPoliza = jQuery("#existeEdicionPoliza").val();
	 if(existeEdicionPoliza == 'true'){
		 var windowContenedorImpresion = parent.mainDhxWindow.window("mostrarContenedorImpresion");
		 jQuery("#existeEdicionPoliza",windowContenedorImpresion._frame.contentWindow.document).val(existeEdicionPoliza);
		 jQuery("#contenedorChkEdicionPoliza",windowContenedorImpresion._frame.contentWindow.document).show();
	 }
 }
 
 function mostrarIncluirEdicionInciso(){
	 var existeEdicionInciso = jQuery("#existeEdicionInciso").val();
	 if(existeEdicionInciso == 'true'){
		 var windowContenedorImpresion = parent.mainDhxWindow.window("mostrarContenedorImpresion");
		 jQuery("#existeEdicionInciso",windowContenedorImpresion._frame.contentWindow.document).val(existeEdicionInciso);
		 jQuery("#contenedorChkEdicionInciso",windowContenedorImpresion._frame.contentWindow.document).show();
	 }
 }
 
 function borrarAgente(){
	 	jQuery("#datosAgenteCaratula").val(' ');
 }
 
 function mostrarIncluirEdicionEndoso(){
	 var existeEdicionEndoso = jQuery("#existeEdicionEndoso").val();
	 var idBotonAMostrar = "#" + jQuery("#id").val() + jQuery("#recordFromMillis").val() + jQuery("#claveTipoEndoso").val();
	 console.log("EXISTE EDICION ENDOSO:" + existeEdicionEndoso);
	 console.log("BOTON A MOSTRAR:" + idBotonAMostrar);
	 if(existeEdicionEndoso == 'true'){
		 var windowContenedorImpresion = parent.mainDhxWindow.window("impresionPoliza");
		 jQuery("#existeEdicionEndoso",windowContenedorImpresion._frame.contentWindow.document).val(existeEdicionEndoso);
		 jQuery(idBotonAMostrar,windowContenedorImpresion._frame.contentWindow.document).css("display", "inline");;
	 }
 }
 
 function mostrarEditarImpresionEndoso(tipoImpresion,idToPoliza,validOn,validOnMillis,
			recordFromMillis, recordFrom, claveTipoEndoso){
	console.log(tipoImpresion + ", " + idToPoliza + ", " + validOn + ", " + validOnMillis + ", " + recordFromMillis + ", " + recordFrom);
	var url = '/MidasWeb/impresiones/componente/mostrarEditarImpresionEndoso.action?tipoImpresion='+tipoImpresion+"&idToPoliza="+idToPoliza;
	if(validOn){
		url = url + "&validOn=" + validOn;
	}
	if(validOnMillis){
		url = url + "&validOnMillis=" + validOnMillis;
	}
	if(recordFromMillis){
		url = url + "&recordFromMillis=" + recordFromMillis;
	}
	if(recordFrom){
		url = url + "&recordFrom=" + recordFrom;
	}
	if(claveTipoEndoso){
		url = url + "&claveTipoEndoso=" + claveTipoEndoso;
	}
	parent.mostrarVentanaModal("mostrarEditarImpresionEndoso", 'Impresiones', 1, 1, 800, 600, url, null);
 }
 
 function imprimirEndosoEditado(idToCotizacion, recordFrom, recordFromMillis, claveTipoEndoso){
		var url = '/MidasWeb/impresiones/poliza/imprimirEndosoEditado.action?idToCotizacion=' + idToCotizacion + "&recordFrom=" + recordFrom + "&recordFromMillis="+recordFromMillis + "&claveTipoEndoso=" + claveTipoEndoso;
		window.open(url);
	}
 
 //mainDhxWindow.window("mostrarContenedorImpresion")._frame.contentWindow.document.getElementById("existeEdicionPoliza").value = 'false'
 //mainDhxWindow.window("mostrarContenedorImpresion")._frame.contentWindow.mostrarIncluirEdicionPoliza();