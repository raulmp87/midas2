var emisionesPendientesListadoGrid;

function exportarExcelCancelaciones() {
	
	var fechaValidacion = dwr.util.getValue('fechaValidacion');	
	
	if(!validarCampoObligatorioFecha(fechaValidacion))
	{
	    return;	
	}
	
	window.open(excelCancelacionesPendientesPath + "?fechaValidacion=" + fechaValidacion,"Cancelaciones");
}

function emitirPendiente(idEmisionPendiente) {
	
	var posActual = jQuery("#posActual").val();
		
	sendRequestJQ(null, emitirPendientePath + "?id=" + idEmisionPendiente, null, 'pageGridPaginadoEmisionPendiente(' + posActual +', true, false)');
}

function desbloquearEmision(idEmisionPendiente) {
	bloquearDesbloquearEmision(idEmisionPendiente,0);
}

function bloquearEmision(idEmisionPendiente) {
	bloquearDesbloquearEmision(idEmisionPendiente,1);
}

function bloquearDesbloquearEmision(idEmisionPendiente, bloqueo) {
	
	var posActual = jQuery("#posActual").val();
	
	sendRequestJQ(null, actualizarBloqueoPath + "?id=" + idEmisionPendiente + "&bloqueo=" + bloqueo, null, 'pageGridPaginadoEmisionPendiente(' + posActual +', true, false)');
}


function iniciaPrevioEmisionPendienteCancelacion() {
	pageGridPaginadoEmisionPendiente(1, true, true);
}


function iniciaPrevioEmisionPendienteCancelacionValidacion() {	
	iniciarPrevioEmision(true);
}

function iniciaPrevioEnEmsionPendiente() {	
	iniciarPrevioEmision(false);
}

function iniciarPrevioEmision(regenerar){
	var fechaValidacion;
	fechaValidacion = dwr.util.getValue('fechaValidacion');	
	if(!validarCampoObligatorioFecha(fechaValidacion))
	{
	    return;	
	}	
	pageGridPaginadoEmisionPendiente(1, true, regenerar);	
}


function pageGridPaginadoEmisionPendiente(page, nuevoFiltro, actualizarEmisiones) {
	var fechaValidacion;
	var posPath = 'posActual=' + page + '&funcionPaginar=' + 'pageGridPaginadoEmisionPendiente' + '&divGridPaginar=' + 'emisionesPendientesListadoGrid';
	
	//TODO cambiar con nombres mas apropiados. Esto es para evitar la invocacion del SP de Seycos cuando no sea una busqueda explicita
	if (actualizarEmisiones) {
		posPath +="&fileName=1"
	}
	

	var fechaValidacion = dwr.util.getValue('fechaValidacion');
	var numeroPoliza = dwr.util.getValue('filtroNumeroPoliza');
	var nombreContratante = dwr.util.getValue('filtroNombreContratante');
	var tipoEmision = dwr.util.getValue("tiposEmisiones");
	var numeroPolizaSeycos = dwr.util.getValue('filtroNumeroPolizaSeycos');
	var idAgente = dwr.util.getValue('idAgentePol');
	
	if (!nuevoFiltro) {
		posPath = 'posActual=' + page + '&' + jQuery(document.paginadoGridForm).serialize();
	}
	mostrarIndicadorCarga('indicador');
	sendRequestJQTarifa (null, 
			buscarCancelacionesPendientesPaginadoPath + "?"+ posPath + "&fechaValidacion=" + fechaValidacion + "&filtros.numeroPoliza=" + numeroPoliza + 
			"&filtros.nombreContratante=" + nombreContratante + "&filtros.tipoEmision=" + tipoEmision + "&filtros.codigoAgente=" + idAgente + 
			"&filtros.numeroPolizaSeycos=" + numeroPolizaSeycos, 
										'gridEmisionesPendientesPaginado', 
										'buscarCancelacionesPendientes();');
	
}

function buscarCancelacionesPendientes(){
	var fechaValidacion;	
	
	document.getElementById("emisionesPendientesListadoGrid").innerHTML = '';	
	emisionesPendientesListadoGrid = new dhtmlXGridObject('emisionesPendientesListadoGrid');
		
	emisionesPendientesListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	
		
	var fechaValidacion = dwr.util.getValue('fechaValidacion');
	var numeroPoliza = dwr.util.getValue('filtroNumeroPoliza');
	var nombreContratante = dwr.util.getValue('filtroNombreContratante');
	var tipoEmision = dwr.util.getValue('tiposEmisiones');
	var numeroPolizaSeycos = dwr.util.getValue('filtroNumeroPolizaSeycos');
	var idAgente = dwr.util.getValue('idAgentePol');
	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	var url_local = buscarCancelacionesPendientesPath + "?" + posPath + "&fechaValidacion=" + fechaValidacion + "&filtros.numeroPoliza=" + numeroPoliza + 
	"&filtros.nombreContratante=" + nombreContratante + "&filtros.tipoEmision=" + tipoEmision + "&filtros.codigoAgente=" + idAgente + 
	"&filtros.numeroPolizaSeycos=" + numeroPolizaSeycos;
	emisionesPendientesListadoGrid.load(url_local);
		
}

function aplicarAccionSeleccionadas(accionSeleccionada, bloqueo)
{
	var posActual = jQuery("#posActual").val();
	
    var selectedRowsIds = emisionesPendientesListadoGrid.getCheckedRows(0);    
    
	if (selectedRowsIds == null || selectedRowsIds == '') 
	{
		alert("Debe seleccionar al menos un registro para aplicar la acci\u00F3n seleccionada");
		return;		 		
	}		
	
	sendRequestJQ(null, aplicarAccionSeleccionadasPath + '?' + 'idsSeleccionados =' + selectedRowsIds + '&bloqueo=' + bloqueo + '&accionSeleccionada=' + accionSeleccionada, null, 'pageGridPaginadoEmisionPendiente(' + posActual +', true, false)');
}

function  validarCampoObligatorioFecha(fecha)
{
	if(fecha == null || fecha == '')
	{
		alert("El campo  'Cancelables al d\u00EDa' es Obligatorio.");	
		return false;
	}
	return true;
}

function arrancarCancelacionAutomatica()
{	
	var mensaje = "\u00BFEst\u00E1 seguro que desea iniciar el Proceso de Cancelaci\u00F3n Autom\u00E1tica?";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, iniciarCancelacionAutomaticaPath, targetWorkArea, null);						
	}	
}

function mostrarVentanaMonitoreo() {		
	var fechaValidacion;
	fechaValidacion = dwr.util.getValue('fechaValidacion');	
	if(validarCampoObligatorioFecha(fechaValidacion))
	{
		mostrarVentanaModal("monitor", "Monitoreo proceso cancelaci\u00f3n " , 50, 50, 700, 150, monitorearCancelacionAutomaticaPath, null);	
	}	
		
}

function seleccionarAgentePrevioCancelacion(bandera){	
	mostrarVentanaModal("agente","Selecci\u00F3n Agente",200,320, 710, 155, "/MidasWeb/poliza/ventanaAgentes.action?bandera="+bandera);	
}

function cleanInput(id) {
	jQuery('#' + id).val('');
}
function cleanInputDiv(id) {
	jQuery('#' + id).text('');
}


function generarReporteEndososCancelables(){
	var fechaCorteObj = jQuery('#fechaCorteDanios');	
	if(fechaCorteObj != null && fechaCorteObj.val() !== ''){	
		var url = generarCancelacionesPendientesDaniosPath + "?fechaCorteDanios="+fechaCorteObj.val();
		window.open(url);
	}else{
		alert("El campo 'Fecha de Corte' es Obligatorio.");	
		return false;		
	}
}