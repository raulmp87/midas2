
function verDetalleEndosoMovimientos() {
	sendRequestJQ(null, mostrarDetalleActionPath + "?"+ jQuery(document.contenedorEndosoMovimientosForm).serialize(),'contenido_detalle', null);
}

function cotizar()
{
	sendRequestJQ(null, cotizarActionPath + "?"+ jQuery(document.cotizacionForm).serialize(),targetWorkArea, null);
}

function terminar()
{
	sendRequestJQ(null, terminarActionPath + "?"+ jQuery(document.cotizacionForm).serialize(),targetWorkArea,null);
}

function emitir()
{
	sendRequestJQ(null,emitirActionPath + "?"+ jQuery(document.cotizacionForm).serialize(),targetWorkArea, null);	
}

/**
 * 
 */
function cancelar(){
	
	var preguntaConfirmacionCancelar = "\u00BF" + "Est" + "\u00E1" + " seguro que desea cancelar el Endoso de Movimientos?, se perder" + "\u00E1" 
	+ "la configuraci" + "\u00f3" + "n de Endoso de Movimientos si ya ha definido alguna."
	;
	
	var formObject = null;
	var pNextFunction_local = null;
	if(confirm(preguntaConfirmacionCancelar)){
		var url_action = cancelarActionPath  + "?" + jQuery(document.cotizacionForm).serialize();
		sendRequestJQ(formObject, url_action, targetWorkArea, pNextFunction_local);
	}
	
}

function mostrarIgualarMovimientoEndoso(){
	mostrarVentanaModal("igualarPrimas","Igualar Movimientos Endoso",200,320, 710, 320, mostrarIgualarActionPath+ "?"+ jQuery(document.cotizacionForm).serialize(),"mostrarEndosoMovimiento();");
}

function mostrarEndosoMovimiento(){
	sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos/mostrar.action?"+ jQuery(document.cotizacionForm).serialize(),targetWorkArea, null);
}
