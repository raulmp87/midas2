/**
 * 
 */
var idContratante = 0;

function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el Alta de Incisos?, se perder\u00E1 la configuraci\u00F3n de Alta de Incisos si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.altaIncisoForm).serialize(), targetWorkArea,null);				
	}
}

function cotizar() {
	var rowsNum = incisosGrid.getRowsNum();
	
	if(jQuery("#derechos").val() === undefined || jQuery("#derechos").val() === ''){
		alert("Debe seleccionar un Derecho para cotizar");
		return;
	}
	if (rowsNum == null || rowsNum == 0) {
		alert("Debe de existir al menos un inciso para cotizar");
		return;
	}	
	sendRequestJQ(null, cotizarActionPath + "?"+ jQuery(document.altaIncisoForm).serialize(),targetWorkArea,null);	
}

function terminar()
{
	sendRequestJQ(null, terminarActionPath + "?"+ jQuery(document.altaIncisoForm).serialize(),targetWorkArea,null);
}

function emitir()
{		
	var nuevoContratanteId = dwr.util.getValue('idCliente');
	sendRequestJQ(null, emitirActionPath + "?"+ jQuery(document.altaIncisoForm).serialize() + "&nuevoContratanteId=" + nuevoContratanteId,targetWorkArea,null);
}

function regresarAProceso()
{
	sendRequestJQ(null, regresarProcesoActionPath + "?"+ jQuery(document.altaIncisoForm).serialize(),targetWorkArea,null);
}

function cargaMasiva(){
	var polizaId = $("#polizaId").val();
	var validoEn = $("#fechaIniVigenciaEndoso").val();
	var cotizacionContinuityId = $("#cotizacionContinuityId").val();
	
	var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/mostrarContenedor.action?"
		+"id="+polizaId
		+"&fechaIniVigenciaEndoso="+validoEn
		+"&nivelAplicacion=0"
		+"&tipoRegreso=2"
		+"&accionEndoso=0"
		+"&cotizacionContinuityId="+cotizacionContinuityId;
	
		//alert(url);
		sendRequestJQ(null, url,"contenido", null);
	
}

//Funcion Habilitar/Deshabilitar boton Terminar
$(document).ready(function () 
{	
	var estatusCotizacion = dwr.util.getValue('estatusCotizacion');	
	if(estatusCotizacion != 10)
    {
		$('#botonTerminar').attr("disabled", true);		
    }  
	else 
	{ 
		$('#botonTerminar').removeAttr("disabled"); 	
	}	
});

function procesarPlantillaComplementariosCallBack(message) {
	
	if (downloadFileWithBlockingCallback(message)) {
		
		closeVentanaAseguradoEndosoCambioDatos(); 
			
	}
	
}

function descargarPlantillaComplementarios() {
	var validoEn = $("#fechaIniVigenciaEndoso").val();
	downloadFileWithoutBlocking(descargarPlantillaComplementariosPath+'?polizaId=' + jQuery('#polizaId').val()+"&fechaIniVigenciaEndoso="+validoEn);
	
}

function procesarPlantillaComplementariosInner(idToControlArchivo) {
	var validoEn = $("#fechaIniVigenciaEndoso").val();
	downloadFileCustom(procesarPlantillaComplementariosPath
			+'?polizaId=' + jQuery('#polizaId').val() + '&idToControlArchivo='+ idToControlArchivo
			+"&fechaIniVigenciaEndoso="+validoEn
			, procesarPlantillaComplementariosCallBack);
	
}

function procesarPlantillaComplementarios(){
	
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaInfo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Cargar plantilla");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			
    				procesarPlantillaComplementariosInner(idToControlArchivo);
    				
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaInfo").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls" && ext != "xlsx") { 
           alert("Solo puede importar archivos Excel (.xls, .xlsx). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea cargar este archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}


function mostrarAltaIncisoCargaMsva()
{
	sendRequestJQ(null, mostrarAltaIncisoCargaMsvaPath + "?"+ jQuery(document.altaIncisoForm).serialize(),targetWorkArea,null);
}


