var endososGrid;
var obtenerEndososPath ;
var obtenerEndososPaginadasPath;
var cancelarActionPath ;
var emitirActionPath ;  
var cotizarActionPath ;  
var consultarEndosoActionPath ; 

var cotizarDummyActionPath ;  


function iniciaLlenadoEndosos()
{
	pageGridPaginadoEndosos(1, true);
}
function pageGridPaginadoEndosos(page, nuevoFiltro)
{
	var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEndosos'+'&divGridPaginar='+'cotizacionEndososListadoGrid';
	if(nuevoFiltro){
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	

	sendRequestJQTarifa(null, obtenerEndososPaginadasPath + "?"+ posPath, 'gridEndososListadoPaginado', 'obtenerEndosos();');
}
function obtenerEndosos()
{
	document.getElementById("cotizacionEndososListadoGrid").innerHTML = '';	
	endososGrid = new dhtmlXGridObject('cotizacionEndososListadoGrid');
	mostrarIndicadorCarga('indicador');	
	endososGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	//alert(obtenerEndososPath);
	endososGrid.load(obtenerEndososPath + "?" + posPath);

}

function cancelarEndosos()
{	
	var pars = obtenerEndososIdSeleccionados();
	sendRequestJQ(null, cancelarActionPath+"?endosoIdsApply="+pars+"&"+jQuery(document.variablesEndosos).serialize(), targetWorkArea,'obtenerEndosos();');				
}

function emitirEndosos ()
{	
	var pars = obtenerEndososIdSeleccionados();
	sendRequestJQ(null, emitirActionPath+"?endosoIdsApply="+pars+"&"+jQuery(document.variablesEndosos).serialize(), targetWorkArea,'obtenerEndosos();');
}

function cotizarEndosos ()
{	
	var pars = obtenerEndososIdSeleccionados();
	//var page = 1;
	
	//var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEndosos'+'&divGridPaginar='+'cotizacionEndososListadoGrid';
	//+ 	"&endosoIdsApply="+obtenerEndososIdSeleccionados();
	//sendRequestJQTarifa(null, cotizarActionPath + "?"+ posPath, 'gridEndososListadoPaginado', 'obtenerEndosos();');
	
	/*
	document.getElementById("cotizacionEndososListadoGrid").innerHTML = '';	
	endososGrid = new dhtmlXGridObject('cotizacionEndososListadoGrid');
	mostrarIndicadorCarga('indicador');	
	endososGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() +"&endosoIdsApply="+pars;
	//alert(obtenerEndososPath);
	endososGrid.load(cotizarActionPath + "?" + posPath);
	*/
	sendRequestJQTarifa(null, cotizarActionPath+"?endosoIdsApply="+pars+"&"+jQuery(document.variablesEndosos).serialize(), targetWorkArea,null);	

}
function cotizarEndososDummy ()
{	
	var pars = obtenerEndososIdSeleccionados();
	//var page = 1;
	
	//var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEndosos'+'&divGridPaginar='+'cotizacionEndososListadoGrid';
	//+ 	"&endosoIdsApply="+obtenerEndososIdSeleccionados();
	//sendRequestJQTarifa(null, cotizarActionPath + "?"+ posPath, 'gridEndososListadoPaginado', 'obtenerEndosos();');
	
	
	document.getElementById("cotizacionEndososListadoGrid").innerHTML = '';	
	endososGrid = new dhtmlXGridObject('cotizacionEndososListadoGrid');
	mostrarIndicadorCarga('indicador');	
	endososGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() +"&endosoIdsApply="+pars;
	//alert(obtenerEndososPath);
	endososGrid.load(cotizarDummyActionPath + "?" + posPath);
	
}
function evalRedirectPageEndoso(){
	var red = document.getElementById("redirectPageEndoso").value;
	if(red== 'true'){
		var ns =    document.getElementById("namespaceOrigen").value;
		var action =  document.getElementById("actionNameOrigen").value;
		sendRequestJQ(null, ns+"/"+action+".action", targetWorkArea, null);
	}
}

function mostrarMsg(data){
	var res = dwr.util.getValue('endosos');
	//alert(data);
	sendRequestJQ(obj, consultarEndosoActionPath + "? noEndoso="+ data +"&endosos="+res+"&"+jQuery(document.variablesEndosos).serialize(), targetWorkArea, null);
}

function initEndosoCancelacionPage(){
	var nameSpaceCanEndosos = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionEndoso/";
	var isWinCan = document.getElementById("isCancelacionWindow");
	try {
		//alert(isWinCan );
		if(isWinCan != null){
			isWinCan = isWinCan.value;
			//alert(isWinCan );
			if(isWinCan == "true")
			{
				//alert('Entro a Cancelación');
				nameSpaceCanEndosos = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionEndoso/";
			}else
			{
				//alert('Entro a Rehabilitación');
				nameSpaceCanEndosos = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabCancelacionEndoso/";
			}
			//alert(nameSpaceCanEndosos);
		}
	} catch (e) {
		alert(nameSpaceCanEndosos );
	}
	//alert(nameSpaceCanEndosos );
	obtenerEndososPath = nameSpaceCanEndosos+'obtenerEndosos.action';
	obtenerEndososPaginadasPath = nameSpaceCanEndosos+'obtenerEndososPaginado.action';
	cancelarActionPath = nameSpaceCanEndosos+'cancelar.action';
	emitirActionPath = nameSpaceCanEndosos+'emitir.action';  
	cotizarActionPath =nameSpaceCanEndosos+'cotizar.action';  
	consultarEndosoActionPath = nameSpaceCanEndosos+'consultarEndoso.action'; 	
	
	cotizarDummyActionPath =nameSpaceCanEndosos+'cotizarDummy.action';
}


function obtenerEndososIdSeleccionados(){
	var checkColum = 1;
	var rowsIdSel = endososGrid.getCheckedRows(checkColum)
	
	return rowsIdSel;
}


