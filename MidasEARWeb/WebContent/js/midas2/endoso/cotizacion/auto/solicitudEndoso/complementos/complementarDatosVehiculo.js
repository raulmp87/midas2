var TIPO_ENDOSO_ALTA_INCISO = 5;
var TIPO_ENDOSO_CAMBIO_DATOS = 7;
var TIPO_ENDOSO_MOVIMIENTOS = 15;


function mostrarVentanaVehiculoListadoIncisos(incisoId, validoEn, cotizacionId, accionEndoso, cargaDatosCliente, claveTipoEndoso) {
	
	var accionE = 4;
	if(accionEndoso != null)
	{
		accionE = accionEndoso;		
	}
	
	var url =  "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso/mostrar.action" 
		+ "?incisoId=" + incisoId + "&accionEndoso=" + accionE;
	
	if (validoEn != null && validoEn != "") {
		url+= "&validoEn=" + validoEn;
	}
	
	if (cotizacionId != null && cotizacionId != "") {
		url+= "&cotizacionId=" + cotizacionId;
	}
	
	if (cargaDatosCliente != null && cargaDatosCliente != "") {
		url+="&copiarDatos="+cargaDatosCliente;
	}else{
		url+="&copiarDatos=1";
	}
	if(claveTipoEndoso != null && claveTipoEndoso != undefined && claveTipoEndoso !=""){
		url+= "&claveTipoEndoso=" + claveTipoEndoso;
	}
	
	mostrarVentanaModal('ventanaVehiculo', 'Datos Complementarios Veh\u00edculo', 50, 50, 800, 450, url, null );
}

function cerrarComplementarDatosInciso()
{
    var tipoEndoso = jQuery('#tipoEndoso').val();
	
	if(tipoEndoso == TIPO_ENDOSO_ALTA_INCISO)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
		
		sendRequestJQ(null, path, 'contenido_detalle', null);		
	}else if(tipoEndoso == TIPO_ENDOSO_CAMBIO_DATOS)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&idPolizaBusqueda=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + 4 + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
		
		sendRequestJQ(null, path, targetWorkArea, null);
	}else if(tipoEndoso == TIPO_ENDOSO_MOVIMIENTOS)
	{
		parent.cerrarVentanaModal("winCargaControles");
		var ifr = parent.getWindowContainerFrame('inciso');
		ifr.contentWindow.refrescarVentanaInciso();
	}
	
	cerrarVentanaModal('ventanaVehiculo');	
}


function recargarVentanaVehiculoListadoIncisos(incisoId, validEn, cotizacionId, cargaCliente, claveTipoEndoso){
	//closeVehiculo();
	cerrarVentanaModal('ventanaVehiculo');
	cargaDatosCliente = cargaCliente;
	mostrarVentanaVehiculoListadoIncisos(incisoId, validEn, cotizacionId, null,cargaDatosCliente, claveTipoEndoso);
}