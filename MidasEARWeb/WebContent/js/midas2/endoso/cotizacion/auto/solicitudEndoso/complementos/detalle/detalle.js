function mostrarDetalle(polizaId, validoEn) {
	var url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/mostrarDetalle.action?polizaId=' + polizaId +  "&fechaIniVigenciaEndoso=" + validoEn;
	
	sendRequestJQ(null, url, parent.targetWorkArea, null);
}


function desplegarDetalle() {
	var polizaId = jQuery('#polizaId').val();
	var fechaIniVigenciaEndoso = jQuery('#fechaIniVigenciaEndoso').val();
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
		+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + polizaId + '&fechaIniVigenciaEndoso=' + fechaIniVigenciaEndoso
		+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
		'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();

	sendRequestJQ(null, path, 'contenido_detalle', null);
}

function desplegarAnexos() {
	var polizaId = jQuery('#polizaId').val();
	var fechaIniVigenciaEndoso = jQuery('#fechaIniVigenciaEndoso').val();
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/mostrarInclusionAnexo.action"
		+ "?polizaId=" + polizaId + "&accionEndoso=4";
	if (fechaIniVigenciaEndoso != null && fechaIniVigenciaEndoso != "") {
		path+= "&fechaIniVigenciaEndoso=" + fechaIniVigenciaEndoso ;
	}
	sendRequestJQ(null, path, 'contenido_anexos', null);
}

function desplegarTextos() {
	var polizaId = jQuery('#polizaId').val();
	var fechaIniVigenciaEndoso = jQuery('#fechaIniVigenciaEndoso').val();
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/mostrarInclusionTexto.action"
		+ "?polizaId=" + polizaId + "&accionEndoso=4";
	if (fechaIniVigenciaEndoso != null && fechaIniVigenciaEndoso != "") {
		path+= "&fechaIniVigenciaEndoso=" +fechaIniVigenciaEndoso;
	}
	sendRequestJQ(null, path, 'contenido_textos', null);
}
