var texAdicionalGrid;
var texAdicionalProcGrid;
var inclusionTexProcessor;

function iniciaGridTextos() {

	texAdicionalGrid = new dhtmlXGridObject('contenido_textosAdicionalesGrid');	
	texAdicionalGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto/obtenertexAdicionalGrid.action?" 			
								+jQuery(document.endosoExclusionTextoForm).serialize());
	
	texAdicionalProcGrid = new dhtmlXGridObject('contenido_textosAdicionalesProcGrid');	
	texAdicionalProcGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto/obtenertexAdicionalProcGrid.action?" 			
								+jQuery(document.endosoExclusionTextoForm).serialize());
	
}

function cotizar(accion) {
	var selectedRowsIds = "";
	
	//Se revisan las que se pudieron haber "des-seleccionado"
	var unchecked = "";
	
	if (texAdicionalGrid != null) {
		selectedRowsIds += texAdicionalGrid.getCheckedRows(0);
		
		texAdicionalGrid.forEachRow(function(id){
			if (texAdicionalGrid.cells(id,0).getValue()=="0") unchecked +=id + ",";
			});
		
	}
	
	unchecked += "X";
	
	if (jQuery("#elementosSeleccionados").val() != null && jQuery("#elementosSeleccionados").val() != "") {
		selectedRowsIds += "," + jQuery("#elementosSeleccionados").val();
	}
	
	selectedRowsIds = unchecked + selectedRowsIds;
	
	/////
	
	if (selectedRowsIds == null || selectedRowsIds == "") {
		alert("Favor de seleccionar al menos un Texto Adicional");
		return;
	}
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto/cotizar.action";
	sendRequestJQ(null, path + "?"+ jQuery(document.endosoExclusionTextoForm).serialize() +'&elementosSeleccionados=' + selectedRowsIds,targetWorkArea,null);
}

function emitir() {
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto/emitir.action?";
	sendRequestJQ(null, path + jQuery(document.endosoExclusionTextoForm).serialize(), targetWorkArea, null);

}


function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Inclusi\u00F3n de Textos?, " +
			"se perder\u00E1 la configuraci\u00F3n de Textos si ya ha definido alguna.";
	
	var formSerialized = jQuery(document.endosoExclusionTextoForm).serialize();
	
	if (confirm(mensaje)) {
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto/cancelar.action?" +  formSerialized;
		sendRequestJQ(null, path, targetWorkArea,null);				
	}
}

