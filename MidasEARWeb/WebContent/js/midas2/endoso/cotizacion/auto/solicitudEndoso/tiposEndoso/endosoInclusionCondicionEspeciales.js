/**
 * 
 */

var endosoInclusionAsociadasCondEspGrid;
var endosoInclusionDisponiblesCondEspGrid;
var endosoInclusionDataProcessor;

function initGridsInclusionCondicionEspecial(){	
	getCondicionesEspecialesDisponibles();
	getCondicionesEspecialesAsociadas();
	
}

function getParamsUrl(){
	var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
	var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
	var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
	var polizaId 						= jQuery('#polizaId').val();
	var accionEndoso 					= jQuery("#accionEndoso").val()
	var codigosInciso 					= jQuery("#codigosInciso").val();
	var nombreCondicion 				=  jQuery("#nombreCondicion").val(); 
	
	var resultado = "cotizacionContinuityId=" + cotizacionContinuityId 
	+ "&fechaIniVigenciaEndoso="+ fechaIniVigenciaEndoso
	+ "&nivelAplicacion=" + nivelAplicacion 
	+ "&polizaId=" + polizaId
	+ "&codigosInciso=" + codigosInciso
	+ "&nombreCondicion=" + nombreCondicion
	+ "&accionEndoso=" + accionEndoso;
	
	return resultado;
}

function getCondicionesEspecialesDisponibles( ){
	jQuery("#condicionesEspecialesDisponiblesGrid").empty();
	var parametros = getParamsUrl();
	
	endosoInclusionDisponiblesCondEspGrid = new dhtmlXGridObject('condicionesEspecialesDisponiblesGrid');
	
	endosoInclusionDisponiblesCondEspGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	endosoInclusionDisponiblesCondEspGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	endosoInclusionDisponiblesCondEspGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorCondicionesDisponibles");
    });
	endosoInclusionDisponiblesCondEspGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorCondicionesDisponibles');
    });

	endosoInclusionDisponiblesCondEspGrid.load( "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/obtenerCondicionesDisponibles.action?" 
			+ parametros );
	
}

function getCondicionesEspecialesAsociadas(){
	jQuery("#condicionesEspecialesAsociadasGrid").empty();
	var parametros = getParamsUrl();

	endosoInclusionAsociadasCondEspGrid = new dhtmlXGridObject('condicionesEspecialesAsociadasGrid');
	endosoInclusionAsociadasCondEspGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	endosoInclusionAsociadasCondEspGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});	
	endosoInclusionAsociadasCondEspGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicadorCondicionesAsociadas");
    });
	endosoInclusionAsociadasCondEspGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicadorCondicionesAsociadas');
    });
	
	endosoInclusionAsociadasCondEspGrid.load( "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/obtenerCondicionesAsociadas.action?" 
			+  parametros );
	
	endosoInclusionAsociadasCondEspGrid.attachEvent("onBeforeDrag",function(id){
        if (endosoInclusionAsociadasCondEspGrid.getUserData(id,"obligatoria").trim() == '0'){ return true;   }     // allow drag if user data exists
        else {mostrarMensajeInformativo('No se puede Eliminar, Es Condicion Obligatoria', '20'); 
        	return false;        }                                                    // deny drag for any other case
    });

	// Creacion del DataProcessor
	var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/accionRelacionarCondiciones.action?"
		+  parametros ;
	
	endosoInclusionDataProcessor = new dataProcessor(url);
	endosoInclusionDataProcessor.enableDataNames(true);
	endosoInclusionDataProcessor.setTransactionMode("POST");
	endosoInclusionDataProcessor.setUpdateMode("cell");
	endosoInclusionDataProcessor.attachEvent("onAfterUpdate", reload);
	endosoInclusionDataProcessor.init(endosoInclusionAsociadasCondEspGrid);
}



	function buscarCondicionesEspeciales() {
		var parametros = getParamsUrl();
		
		document.getElementById("condicionesEspecialesDisponiblesGrid").innerHTML = '';
		endosoInclusionDisponiblesCondEspGrid = new dhtmlXGridObject('condicionesEspecialesDisponiblesGrid');
		url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/buscarCondicion.action?"
			+ parametros ;

		endosoInclusionDisponiblesCondEspGrid.load(url);
	}
	
	
	
	function cotizar() {
		var parametros = getParamsUrl();
		
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/cotizar.action?"
			+ parametros;
		
		sendRequestJQ(null, path, targetWorkArea, null);
		
	}



	function validateNivelSeleccted(valor){
	var accionEndoso = jQuery('#accionEndoso').val();
		
		if(valor == 1){
			jQuery('#codigos').attr('disabled','');
			jQuery('#codigosAfectar').show('slow');
			jQuery("#condicionesEspecialesDisponiblesGrid").empty();
			jQuery("#condicionesEspecialesAsociadasGrid").empty();
			if(accionEndoso == 2 ){
				loadInfoIncisoInclusion();
			}
		}else{
			jQuery('#codigosAfectar').hide('slow');
			jQuery('#codigos').attr('disabled','disabled');
			initGridsInclusionCondicionEspecial();
		}
	}
	
	function deleteInfoPrevia( valor ){

		jQuery('#nivelAplicacion').val(valor);
		
		if(valor == 1){
			limpiarDatosAsociadosChangeRadio(0);
			jQuery('#codigos').attr('disabled','');
			jQuery('#codigosAfectar').show('slow');
			jQuery("#condicionesEspecialesDisponiblesGrid").empty();
			jQuery("#condicionesEspecialesAsociadasGrid").empty();
		}else{
			limpiarDatosAsociadosChangeRadio(1);
			jQuery('#codigosAfectar').hide('slow');
			jQuery('#codigos').attr('disabled','disabled');
			initGridsInclusionCondicionEspecial();
		}
		
	}
	
	
	function loadInfoIncisoInclusion(){
		initGridsInclusionCondicionEspecial();
	}
	
	
	function validateIncisosInclusion(){
		var fechaIniVigenciaEndoso = jQuery("#codigosInciso").val();
		
		if(fechaIniVigenciaEndoso != ''){
			initGridsInclusionCondicionEspecial();
		}else{
			alert('debe de proporcionar al menos un inciso');
		}
	}
	
	
	
	function reload(){
		var nivelAplicacion		= jQuery("#nivelAplicacion").val();

		validateNivelSeleccted(nivelAplicacion);
		initGridsInclusionCondicionEspecial();
		
		return true;
	}
	
	
	
	function cancelar() {
		var parametros = getParamsUrl();
		var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Inclusi\u00F3n de Condiciones Especiales?, " +
				"se perder\u00E1 la configuraci\u00F3n de Condiciones si ya ha definido alguna.";
		

		
		if (confirm(mensaje)) {
			var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/cancelar.action?"
				+ parametros ;
			sendRequestJQ(null, url, targetWorkArea,null);				
		}
	}
	
	
	
	function emitir() {	
		var parametros = getParamsUrl();
		var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/emitir.action?"
			+ parametros ;
		
		sendRequestJQ(null, url,targetWorkArea,null);
	}
	
	
	
	function asociarTodas(){
		var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
		var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
		var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
		var polizaId 						= jQuery('#polizaId').val();
		var accionEndoso 					= jQuery("#accionEndoso").val()
		var codigosInciso 					= jQuery("#codigosInciso").val();
	
		
		blockPage();
		
		jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/accionRelacionarTodasCondiciones.action", 
				{
			fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
			cotizacionContinuityId : cotizacionContinuityId,
			nivelAplicacion : nivelAplicacion,
			polizaId : polizaId,
			accionEndoso : accionEndoso ,
			codigosInciso : codigosInciso
				},
				function( response ){
					initGridsInclusionCondicionEspecial();
				}
			);
		
		unblockPage();
	}
	
	
	function limpiarDatosAsociados(){
		var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
		var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
		var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
		var polizaId 						= jQuery('#polizaId').val();
		var accionEndoso 					= jQuery("#accionEndoso").val()
		var codigosInciso 					= jQuery("#codigosInciso").val();
	
		
		blockPage();
		
		jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/eliminarPrevios.action", 
				{
			fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
			cotizacionContinuityId : cotizacionContinuityId,
			nivelAplicacion : nivelAplicacion,
			polizaId : polizaId,
			accionEndoso : accionEndoso ,
			codigosInciso : codigosInciso
				},
				function( response ){
					initGridsInclusionCondicionEspecial();
				}
			);
		
		unblockPage();
	}
	
	

	function limpiarDatosAsociadosChangeRadio( nivelAplicacion ){
		var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
		var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
		var nivelAplicacion 				= nivelAplicacion;
		var polizaId 						= jQuery('#polizaId').val();
		var accionEndoso 					= jQuery("#accionEndoso").val()
		var codigosInciso 					= jQuery("#codigosInciso").val();
	
		
		blockPage();
		
		jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/eliminarPrevios.action", 
				{
			fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
			cotizacionContinuityId : cotizacionContinuityId,
			nivelAplicacion : nivelAplicacion,
			polizaId : polizaId,
			accionEndoso : accionEndoso ,
			codigosInciso : codigosInciso
				},
				function( response ){

				}
			);
		
		unblockPage();
	}
	
	
	function cargaMasiva(){
		
		var polizaId =               $("#polizaId").val();
		var validoEn =               $("#fechaIniVigenciaEndoso").val();
		var nivelAplicacion =        $('input[name=nivelAplicacion]:checked').val(); // # SOLO USADO PARA VALIDACION ALERT JS, EN PARAMETRO PASA FIJO
		var accionEndoso =           $("#accionEndoso").val();
		var tipoRegreso =            $("#tipoRegreso").val();
		var tipoEndoso =             $("#tipoEndoso").val();
		var cotizacionContinuityId = $("#cotizacionContinuityId").val();
		
		if(accionEndoso == 1 & nivelAplicacion == 0){
			mostrarMensajeInformativo('Alta de condiciones especiales solo se permite por INCISO', '10');
		}else{
			sendRequestJQ(null, 
				"/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/mostrarContenedor.action?"
							+"id="+polizaId
							+"&fechaIniVigenciaEndoso="+validoEn
							+"&nivelAplicacion=1"
							+"&accionEndoso=0"
							+"&cotizacionContinuityId="+cotizacionContinuityId
							+"&tipoRegreso="+tipoRegreso
							+"&tipoEndoso="+tipoEndoso
				, 
				"contenido", null);
		}
	}
	
	
	
