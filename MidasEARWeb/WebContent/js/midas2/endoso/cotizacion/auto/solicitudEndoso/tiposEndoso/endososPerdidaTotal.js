/**
 * 
 */
var tipoEndosoBajaIncisoPerdidaTotal = '24';
var tipoEndosoCancelacionPolizaPerdidaTotal = '25';

function cotizar(tipoEndoso)
{ 
	var actionPath;
	var selectedRowId = "";	
	
	if (incisosGrid != null) {
		selectedRowId = incisosGrid.getCheckedRows(0);				
	}	
	
	//Endoso Cancelacion Poliza PT
	
	if (selectedRowId == null || selectedRowId == "") {
			alert("Debe seleccionar al menos un inciso");
		return;
	}	
	
	if(tipoEndoso == tipoEndosoCancelacionPolizaPerdidaTotal)
	{
		actionPath = cotizarEndCancPolizaPTActionPath;
		
	}else if(tipoEndoso == tipoEndosoBajaIncisoPerdidaTotal)
	{
		actionPath = cotizarEndBajaIncisoPTActionPath;				
	}
	
	sendRequestJQ(null, actionPath + "?"+ jQuery(document.endososPerdidaTotalForm).serialize() 
			+'&elementoSeleccionado=' + selectedRowId + '&fechaIniVigenciaEndoso=' + jQuery("#fechaIniVigenciaEndoso").val(),targetWorkArea,null);
}

function cancelar(validar,tipoEndoso) {
	
	var cancelarEndosoPTActionPath;
	
	if(tipoEndoso == tipoEndosoCancelacionPolizaPerdidaTotal)
	{
		cancelarEndosoPTActionPath = cancelarEndCancPolizaPTActionPath;
		
	}else if(tipoEndoso == tipoEndosoBajaIncisoPerdidaTotal)
	{
		cancelarEndosoPTActionPath = cancelarEndBajaIncisoPTActionPath;				
	}	
	
	if (validar) {
		var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el endoso de P\u00E9rdida Total?, se perder\u00E1 la configuraci\u00F3n del endoso, si ya ha definido alguna.";
		if (confirm(mensaje)) {
			sendRequestJQ(null, cancelarEndosoPTActionPath+"?"+jQuery(document.endososPerdidaTotalForm).serialize(), targetWorkArea,null);				
		}
	} else {
		sendRequestJQ(null, cancelarEndosoPTActionPath+"?"+jQuery(document.endososPerdidaTotalForm).serialize(), targetWorkArea,null);	
	}	
}

function imprimirDesgloseCoberturas(tipoEndoso)
{
	var actionPath;
	
	if(tipoEndoso == tipoEndosoCancelacionPolizaPerdidaTotal)
	{
		actionPath = imprimirDesgCoberturasEndCancIncisoPTActionPath;
		
	}else if(tipoEndoso == tipoEndosoBajaIncisoPerdidaTotal)
	{
		actionPath = imprimirDesgCoberturasEndBajaIncisoPTActionPath;				
	}	
	
	var selectedRowId = "";	
	
	if (incisosGrid != null) {
		selectedRowId = incisosGrid.getCheckedRows(0);				
	}	
	
	//Endoso Cancelacion Poliza PT
	
	if (selectedRowId == null || selectedRowId == "") {
			alert("Debe seleccionar al menos un inciso");
		return;
	}	
	
	window.open(actionPath + "?" + jQuery(document.endososPerdidaTotalForm).serialize() 
			+'&elementoSeleccionado=' + selectedRowId + '&fechaIniVigenciaEndoso=' + jQuery("#fechaIniVigenciaEndoso").val(),"Documento");
}

function emitir(tipoEndoso)
{	
	var actionPath;
	
	if(tipoEndoso == tipoEndosoCancelacionPolizaPerdidaTotal)
	{
		actionPath = emitirEndCancPolizaPTActionPath;
		
	}else if(tipoEndoso == tipoEndosoBajaIncisoPerdidaTotal)
	{
		actionPath = emitirEndBajaIncisoPTActionPath;				
	}
	
	sendRequestJQ(null, actionPath + "?"+ jQuery(document.endososPerdidaTotalForm).serialize() + '&fechaIniVigenciaEndoso=' 
			+ jQuery("#fechaIniVigenciaEndoso").val(),targetWorkArea,null);
}