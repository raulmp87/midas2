var TIPO_ENDOSO_ALTA_INCISO = 5;
var TIPO_ENDOSO_CAMBIO_DATOS = 7;
var TIPO_ENDOSO_MOVIMIENTOS = 15;


function mostrarVentanaMedioPago(incisoContinuityId, validoEn) {
	var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado/mostrarVentanaMedioPago.action"
		+ "?incisoContinuityId=" + incisoContinuityId + "&validoEn=" + validoEn;;
	mostrarVentanaModal('ventanaMedioPago', 'Complementar Medio Pago', 200, 400, 460, 270, url, null );
}

function mostrarVentanaAseguradoListadoIncisos(incisoContinuityId, validoEn) {
	var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado/mostrarVentanaAsegurado.action"
		+ "?incisoContinuityId=" + incisoContinuityId + "&validoEn=" + validoEn;;
	mostrarVentanaModal('ventanaAsegurado', 'Complementar Datos Asegurado', 200, 400, 450, 240, url, null );	
}


function muestraResultadoAgregarCliente(idCliente, idDomCliente){
		var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado/actualizarDatosAsegurados.action"
			+ "?incisoContinuityId=" + dwr.util.getValue("incisoContinuityId")
			+ "&validoEn=" + dwr.util.getValue("validoEn")
			+ "&biAutoInciso.value.personaAseguradoId="
			+ idCliente
			+ "&radioAsegurado=" + dwr.util.getValue("radioAsegurado")
			+ "&nextFunction=closeVentanaAsegurado"
			;
		redirectVentanaModal('ventanaAsegurado', url, null);	
		
}


function mostrarAgregarCliente(){
	var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?tipoAccion=consulta&idField=idAsegurado&tipoBusqueda=1" +
			"&divCarga=clienteModal2&ocultarAgregar=1";
	parent.sendRequestWindow(null, url, obtenerVentanaCliente);
}

function obtenerVentanaCliente(){
	var wins = parent.obtenerContenedorVentanas();
	ventanaClientes= wins.createWindow("clienteModal", 400, 320, 950, 450);
	ventanaClientes.center();
	ventanaClientes.setModal(true);
	ventanaClientes.setText("Consulta de clientes");
	return ventanaClientes;
}

function closeVentanaMedioPago(){
	
	var tipoEndoso = jQuery('#tipoEndoso').val();
	
	if(tipoEndoso == TIPO_ENDOSO_ALTA_INCISO)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();	
		
		sendRequestJQ(null, path, 'contenido_detalle', null);				
	}	
	
	cerrarVentanaModal('ventanaMedioPago');	
}

function closeVentanaAseguradoEndosoCambioDatos(){
	
	var tipoEndoso = jQuery('#tipoEndoso').val();
	
	if(tipoEndoso == TIPO_ENDOSO_ALTA_INCISO)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();	
		
		sendRequestJQ(null, path, 'contenido_detalle', null);				
	}
	else if(tipoEndoso == TIPO_ENDOSO_CAMBIO_DATOS)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&idPolizaBusqueda=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + 4 + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
		
		sendRequestJQ(null, path, targetWorkArea, null);
	} else if(tipoEndoso == TIPO_ENDOSO_MOVIMIENTOS)
	{
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
			+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
			+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
			'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();	
		
		sendRequestJQ(null, path, 'contenido_detalle', null);				
	}
	
	cerrarVentanaModal('ventanaAsegurado');	
}