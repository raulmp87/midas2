/**
 * 
 */
function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el endoso Cambio de Datos?, se perder\u00E1 la configuraci\u00F3n de Cambio de Datos si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.cambioDatosForm).serialize(), targetWorkArea,null);				
	}
}

function cotizar()
{		
	var nuevoContratanteId = dwr.util.getValue('idCliente');
	sendRequestJQ(null, cotizarActionPath + "?"+ jQuery(document.cambioDatosForm).serialize() + "&nuevoContratanteId=" + nuevoContratanteId,targetWorkArea,null);
}

function emitir()
{		
	sendRequestJQ(null, emitirActionPath + "?"+ jQuery(document.cambioDatosForm).serialize(),targetWorkArea,null);
}

//Funcion Habilitar/Deshabilitar boton Emitir
$(document).ready(function () 
{	
	var idAccionE = dwr.util.getValue('accionEndoso');
		
	if(idAccionE != 2)
    {
		$('#botonEmision').attr("disabled", true);		
    }  
	else 
	{ 
		$('#botonEmision').removeAttr("disabled"); 	
	}	
});