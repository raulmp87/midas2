function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el Cambio de Forma de Pago?, se perder\u00E1 la configuraci\u00F3n de Forma de Pago si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.endosoCambioFormaPagoForm).serialize(), targetWorkArea,null);				
	}
}

function emitir()
{
	sendRequestJQ(null, emitirActionPath+"?"+jQuery(document.endosoCambioFormaPagoForm).serialize(), targetWorkArea, null);		
}


function cotizarEndosoCFP(accion)
{
	sendRequestJQ(null,cotizarActionPath+"?"+jQuery(document.endosoCambioFormaPagoForm).serialize()+"&accionEndoso="+accion, targetWorkArea, null);		
}


function cargarRecargoPagoFraccionadoECFP(value, idMoneda){		
	if(value != null && value != '' && value != '-1'){
		blockPage();
		listadoService.getPctePagoFraccionado(value, idMoneda, function(data){
			jQuery('#porcentajePagoFraccionado').val(data);
			unblockPage();
		});
	}else{
		jQuery('#porcentajePagoFraccionado').val('');
	}		
}


