/**
 * 
 */

var endosoBajaAsociadasCondEspGrid;
var endosoBajaEliminadasCondEspGrid;
var endosoBajaDataProcessor;

function getParamsUrl(){
	var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
	var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
	var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
	var polizaId 						= jQuery('#polizaId').val();
	var accionEndoso 					= jQuery("#accionEndoso").val()
	var codigosInciso 					= jQuery("#codigosInciso").val();
	var nombreCondicion 				=  jQuery("#nombreCondicion").val(); 
	
	var resultado = "cotizacionContinuityId=" + cotizacionContinuityId 
	+ "&fechaIniVigenciaEndoso="+ fechaIniVigenciaEndoso
	+ "&nivelAplicacion=" + nivelAplicacion 
	+ "&polizaId=" + polizaId
	+ "&codigosInciso=" + codigosInciso
	+ "&nombreCondicion=" + nombreCondicion
	+ "&accionEndoso=" + accionEndoso;
	
	return resultado;
}



	function initGridsBajaCondicionEspecial(){
		getCondicionesEspecialesAsociadas();
		getCondicionesEspecialesEliminadas();
	}
	
	

	function getCondicionesEspecialesAsociadas(){
		jQuery("#endosoBajacondicionesEspecialesAsociadasGrid").empty();
		
		var parametros 	= getParamsUrl();
	
		endosoBajaAsociadasCondEspGrid = new dhtmlXGridObject('endosoBajacondicionesEspecialesAsociadasGrid');
		endosoBajaAsociadasCondEspGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		endosoBajaAsociadasCondEspGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		endosoBajaAsociadasCondEspGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicadorCondicionesAsociadas");
	    });
		endosoBajaAsociadasCondEspGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicadorCondicionesAsociadas');
	    });
		
		endosoBajaAsociadasCondEspGrid.load( "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/obtenerCondicionesAsociadas.action?" 
				+ parametros);
		
		
		endosoBajaAsociadasCondEspGrid.attachEvent("onBeforeDrag",function(id){
		        if (endosoBajaAsociadasCondEspGrid.getUserData(id,"obligatoria").trim() == '0')
		        	{ 
		        		return true;   
		        	}     // allow drag if user data exists
		        else { 
		        	alert("No se puede Eliminar, Es Condicion Obligatoria");
		        	return false;        
		        	}                                                    // deny drag for any other case
		    });
		
	}


	
	 function getCondicionesEspecialesEliminadas(){
		jQuery("#endosoBajacondicionesEspecialesEliminadasGrid").empty(); 
		var parametros 	= getParamsUrl();
		
		endosoBajaEliminadasCondEspGrid = new dhtmlXGridObject('endosoBajacondicionesEspecialesEliminadasGrid');
		endosoBajaEliminadasCondEspGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		endosoBajaEliminadasCondEspGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		endosoBajaEliminadasCondEspGrid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicadorCondicionesAEliminar");
	    });
		endosoBajaEliminadasCondEspGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicadorCondicionesAEliminar');
	    });
		
		endosoBajaEliminadasCondEspGrid.load( "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/obtenerCondicionesEliminadas.action?" 
				+ parametros);
		 

		// Creacion del DataProcessor
		var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/accionEliminarRelacionCondicion.action?"
			+ parametros;
		
		endosoBajaDataProcessor = new dataProcessor(url);
		endosoBajaDataProcessor.enableDataNames(true);
		endosoBajaDataProcessor.setTransactionMode("POST");
		endosoBajaDataProcessor.setUpdateMode("cell");
		endosoBajaDataProcessor.attachEvent("onAfterUpdate", reload);
		endosoBajaDataProcessor.init(endosoBajaEliminadasCondEspGrid);
	}
	 
	 
	 
	 function cotizar() {
		 var parametros 	= getParamsUrl();
			
			var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/cotizar.action?"
				+ parametros;
			sendRequestJQ(null, path, targetWorkArea, null);
			
		}



	 function validateNivelSeleccted(valor){
		 var accionEndoso = jQuery('#accionEndoso').val();
		 
			if(valor == 1){
				jQuery('#codigos').attr('disabled','');
				jQuery('#codigosAfectar').show('slow');
				jQuery("#endosoBajacondicionesEspecialesAsociadasGrid").empty();
				jQuery("#endosoBajacondicionesEspecialesEliminadasGrid").empty();
				if(accionEndoso == 2 ){
					loadInfoIncisoBaja();
				}
			}else{
				jQuery('#codigosAfectar').hide('slow');
				jQuery('#codigos').attr('disabled','disabled');
				initGridsBajaCondicionEspecial();
			}
		}
	 
	 
	 
		function deleteInfoPrevia( valor ){

			jQuery('#nivelAplicacion').val(valor);
			
			if(valor == 1){
				limpiarDatosEliminadosChangeRadio(0);
				jQuery('#codigos').attr('disabled','');
				jQuery('#codigosAfectar').show('slow');
				jQuery("#endosoBajacondicionesEspecialesAsociadasGrid").empty();
				jQuery("#endosoBajacondicionesEspecialesEliminadasGrid").empty();
			}else{
				limpiarDatosEliminadosChangeRadio(1);
				jQuery('#codigosAfectar').hide('slow');
				jQuery('#codigos').attr('disabled','disabled');
				initGridsBajaCondicionEspecial();
			}
			
		}
	 
	 
	 function loadInfoIncisoBaja(){
		 initGridsBajaCondicionEspecial();
		}
	 
	 
	 
	 function validateIncisosBaja(){
			var fechaIniVigenciaEndoso = jQuery("#codigosInciso").val();
			
			if(fechaIniVigenciaEndoso != ''){
				initGridsBajaCondicionEspecial();
			}else{
				alert('debe de proporcionar al menos un inciso');
			}
		}
		
		
		
		function reload(){
			var nivelAplicacion		= jQuery("#nivelAplicacion").val();

			validateNivelSeleccted(nivelAplicacion);
			initGridsBajaCondicionEspecial();
			
			return true;
		}
		
		
		
		function cancelar() {
			var parametros = getParamsUrl();
			var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Baja de Condiciones Especiales?, " +
					"se perder\u00E1 la configuraci\u00F3n de Condiciones si ya ha definido alguna.";
			

			
			if (confirm(mensaje)) {
				var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/cancelar.action?"
					+ parametros ;
				sendRequestJQ(null, url, targetWorkArea,null);				
			}
		}
		
		
		
		function emitir() {	
			
			var parametros = getParamsUrl();
			
			var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/emitir.action?"
				+ parametros;
			
			
			sendRequestJQ(null, url,targetWorkArea,null);
		}
		
		
		
		function eliminarTodas(){
			var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
			var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
			var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
			var polizaId 						= jQuery('#polizaId').val();
			var accionEndoso 					= jQuery("#accionEndoso").val()
			var codigosInciso 					= jQuery("#codigosInciso").val();
		
			blockPage();
			
			jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/accionEliminarTodasCondiciones.action", 
					{
				fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
				cotizacionContinuityId : cotizacionContinuityId,
				nivelAplicacion : nivelAplicacion,
				polizaId : polizaId,
				accionEndoso : accionEndoso ,
				codigosInciso : codigosInciso
					},
					function( response ){
						initGridsBajaCondicionEspecial();
					}
				);
			
			unblockPage();
		}
		
		
		function limpiarDatosEliminados(){
			var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
			var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
			var nivelAplicacion 				= jQuery("#nivelAplicacion").val();
			var polizaId 						= jQuery('#polizaId').val();
			var accionEndoso 					= jQuery("#accionEndoso").val()
			var codigosInciso 					= jQuery("#codigosInciso").val();
		
			blockPage();
			
			jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/eliminarPrevios.action", 
					{
				fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
				cotizacionContinuityId : cotizacionContinuityId,
				nivelAplicacion : nivelAplicacion,
				polizaId : polizaId,
				accionEndoso : accionEndoso ,
				codigosInciso : codigosInciso
					},
					function( response ){
						initGridsBajaCondicionEspecial();
						jQuery("#endosoBajacondicionesEspecialesEliminadasGrid").empty(); 
						getCondicionesEspecialesEliminadas();
					}
				);
			unblockPage();
		}
		
		

		function limpiarDatosEliminadosChangeRadio( nivelAplicacion ){
			var fechaIniVigenciaEndoso 			= jQuery("#fechaIniVigenciaEndoso").val();
			var cotizacionContinuityId 			= jQuery("#cotizacionContinuityId").val();
			var nivelAplicacion 				= nivelAplicacion;
			var polizaId 						= jQuery('#polizaId').val();
			var accionEndoso 					= jQuery("#accionEndoso").val()
			var codigosInciso 					= jQuery("#codigosInciso").val();
		
			
			blockPage();
			
			jQuery.post("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/eliminarPrevios.action", 
					{
				fechaIniVigenciaEndoso: fechaIniVigenciaEndoso,
				cotizacionContinuityId : cotizacionContinuityId,
				nivelAplicacion : nivelAplicacion,
				polizaId : polizaId,
				accionEndoso : accionEndoso ,
				codigosInciso : codigosInciso
					},
					function( response ){

					}
				);
			
			unblockPage();
		}
		
		function redireccionarBorradoCompleto(){
			var parametros = getParamsUrl();
			
			var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/mostrarBajaCondiciones.action?"
				+ parametros;
			
			
			sendRequestJQ(null, url,targetWorkArea,null);
			
		}
	
		
		function cargaMasiva(){
			
			var polizaId =               $("#polizaId").val();
			var validoEn =               $("#fechaIniVigenciaEndoso").val();
			var nivelAplicacion =        $('input[name=nivelAplicacion]:checked').val(); // # SOLO USADO PARA VALIDACION ALERT JS, EN PARAMETRO PASA FIJO
			var accionEndoso =           $("#accionEndoso").val();
			var tipoRegreso =            $("#tipoRegreso").val();
			var tipoEndoso =             $("#tipoEndoso").val();
			var cotizacionContinuityId = $("#cotizacionContinuityId").val();

			sendRequestJQ(null, 
				"/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/mostrarContenedor.action?"
						+"id="+polizaId
						+"&fechaIniVigenciaEndoso="+validoEn
						+"&nivelAplicacion="+nivelAplicacion
						+"&accionEndoso=1"
						+"&cotizacionContinuityId="+cotizacionContinuityId
						+"&tipoRegreso="+tipoRegreso
						+"&tipoEndoso="+tipoEndoso
					, 
				"contenido", null);
		}		
		