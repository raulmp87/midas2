/**
 * 
 */

function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el cambio en la Extension de Vigencia?, se perder\u00E1 la configuraci\u00F3n de la Extension de Vigencia si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(document.endosoExtensionVigPolizaForm, cancelarActionPath, targetWorkArea,null);				
	}
}

function emitir()
{
	sendRequestJQ(document.endosoExtensionVigPolizaForm, emitirActionPath, targetWorkArea, null);		
}


function cotizar()
{
	var fechaExtensionVigencia = dwr.util.getValue("fechaExtensionVigencia");
	var idPoliza = dwr.util.getValue('numeroPoliza');
	if( idPoliza != ''){  
		if(fechaExtensionVigencia != '' )
		{
			sendRequestJQ(document.endosoExtensionVigPolizaForm, cotizarActionPath, targetWorkArea, null);		
		}else
		{
			mostrarMensajeInformativo("Favor de seleccionar la fecha de extensión.", "20", null);
		}
	}else
	{
		mostrarMensajeInformativo("No esta definida la P\u00f3liza.", "20", null);
	}
}


function previsualizarCobranza()
{
	sendRequestJQ(document.endosoExtensionVigPolizaForm, cotizarActionPath, targetWorkArea, null);		
}

function refrescarPaginaPadre(polizaId){
	sendRequestJQ(null, '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia/refrescarResumenCostos.action?polizaId='+polizaId,'resumenTotalesCotizacionGrid', null);
}

function igualarPrima()
{
	var primaNetaIgualar = dwr.util.getValue("primaNetaIgualar");
	if( primaNetaIgualar != ''){  
		sendRequestJQ(document.endosoExtensionVigPolizaForm, igualarPrimaActionPath, targetWorkArea, null);
	}else
	{
		mostrarMensajeInformativo("Favor de capturar la prime neta a igualar.", "20", null);
	}
}

function mostrarIgualarMovimientoEndosoEV(){
	var mostrarIgualarActionPath = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas/mostrarIgualacionPrimas.action';
	mostrarVentanaModal("igualarPrimas","Igualar Movimientos Endoso",200,320, 710, 320, mostrarIgualarActionPath+ "?"+ jQuery(document.endosoExtensionVigPolizaForm).serialize(),"mostrarEndosoExtensionVigencia();");
}

function mostrarEndosoExtensionVigencia(){
	sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia/mostrarExtensionVigencia.action?"+ jQuery(document.endosoExtensionVigPolizaForm).serialize(),targetWorkArea, null);
}
