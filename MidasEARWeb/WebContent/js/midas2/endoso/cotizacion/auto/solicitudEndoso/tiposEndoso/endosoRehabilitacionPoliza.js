/**
 * 
 */


function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Rehabilitacion de la Poliza?, se perder\u00E1 la configuraci\u00F3n de la Rehabilitacion de Poliza si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize(), targetWorkArea,null);				
	}
}

function emitir()
{
	sendRequestJQ(null, emitirActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize(), targetWorkArea, null);		
}


function cotizar()
{
	sendRequestJQ(null, cotizarActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize(), targetWorkArea, null);		
}


function previsualizarCobranza()
{
	sendRequestJQ(null, cotizarActionPath, targetWorkArea, null);		
}

function muestraCorrimiento() {
	
	var valorRadio = jQuery('input[name=cotizacionEndoso.requiereCorrimientoVigencias]:checked', '#solicitudPolizaForm').val()

	if (valorRadio == 1) {
		jQuery('#sinCorrimiento').css('display', 'none');
		jQuery('#aplicaCorrimiento').css('display', 'block');
	} else {
		jQuery('#sinCorrimiento').css('display', 'block');
		jQuery('#aplicaCorrimiento').css('display', 'none');
	}
	
	
}