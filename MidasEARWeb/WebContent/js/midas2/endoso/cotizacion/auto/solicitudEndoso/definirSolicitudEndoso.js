var selectValue = '';
var selectDescripcion = "SELECCIONE...";
var solicitudesPolizaGrid;
var paginadoParamsPath;
var constantTipoEndosoExtensionVig = '12';
var constantTipoEndosoCancelacionEndoso = '10';
var constantTipoEndosoRehabilitacionEndoso = '17';
var constantTipoEndosoCambioAgente = '8';
var constantTipoEndosoBajaInciso = '6';
var constantTipoEndosoCancelacionPoliza = '11';
var constantTipoEndosoAjustePrima = '19';
var constantTipoEndosoBajaIncisoPerdidaTotal = '24';
var constantTipoEndosoCancelacionPolizaPerdidaTotal = '25';
var constantTipoEndosoDesagrupacionRecibos = '26';
var currentNamespace = '/endoso/cotizacion/auto/solicitudEndoso';
var currentMainAction = 'mostrarDefTipoModificacion';

function verTipoEndoso(accionEndoso) {
	var idSeleccionado = dwr.util.getValue("tipoEndoso");  
    var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
    var motivoEndoso = dwr.util.getValue('motivoEndoso');
    var idPoliza = dwr.util.getValue('numeroPoliza');
       
    if(idSeleccionado != '' && idPoliza != '') {
    	if(idSeleccionado == constantTipoEndosoBajaInciso ||
    			idSeleccionado == constantTipoEndosoCancelacionPoliza) {
    		if (motivoEndoso != '') {
    			sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action?tipoEndoso=' + 
            			idSeleccionado + '&idPolizaBusqueda=' + idPoliza + '&fechaIniVigenciaEndoso=' + fechaIniVigenciaEndoso + 
            			'&accionEndoso=' + accionEndoso + '&namespaceOrigen=' + currentNamespace + '&actionNameOrigen=' + 
            			currentMainAction + '&motivoEndoso=' + motivoEndoso,targetWorkArea,null);
    		} else {
    			mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios y asegurarse de validar el N\u00FAmero de P\u00F3liza.", "20", null); 
    			return false;
    		}    		
    		
    	} else if(idSeleccionado == constantTipoEndosoExtensionVig ||
    			idSeleccionado == constantTipoEndosoCancelacionEndoso ||
    			idSeleccionado == constantTipoEndosoRehabilitacionEndoso ||
    			idSeleccionado == constantTipoEndosoCambioAgente ||
    			idSeleccionado == constantTipoEndosoAjustePrima ||
    			idSeleccionado == constantTipoEndosoBajaIncisoPerdidaTotal ||
    			idSeleccionado == constantTipoEndosoCancelacionPolizaPerdidaTotal    			
    			|| fechaIniVigenciaEndoso != '') {
        	sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action?tipoEndoso=' + 
        			idSeleccionado + '&idPolizaBusqueda=' + idPoliza + '&fechaIniVigenciaEndoso=' + fechaIniVigenciaEndoso + 
        			'&accionEndoso=' + accionEndoso + '&namespaceOrigen=' + currentNamespace + '&actionNameOrigen=' + 
        			currentMainAction + '&motivoEndoso=' + motivoEndoso,targetWorkArea,null);
     	} else {
    		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios y asegurarse de validar el N\u00FAmero de P\u00F3liza.", "20", null);    		
    	}    	
    } else {
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios y asegurarse de validar el N\u00FAmero de P\u00F3liza.", "20", null);
	}
}

function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la definicion de Solicitud de Endoso?, se perder\u00E1 la configuraci\u00F3n si ya ha definido alguna.";
	if(confirm(mensaje)) {
		sendRequestJQ(null, cancelarActionPath + "?esRetorno=1", targetWorkArea,null);				
	}
}

function iniciaDefinirSolicitudEndoso() {
	pageGridPaginadoDefinirEndoso(1, true);
}

function pageGridPaginadoDefinirEndoso(page, nuevoFiltro) {
	var numeroPoliza;
	var posPath = 'posActual=' + page + '&funcionPaginar=' + 'pageGridPaginadoDefinirEndoso' + '&divGridPaginar=' + 'cotizacionEndososListadoGrid';
	numeroPoliza = dwr.util.getValue('numeroPoliza');
	
	if (!nuevoFiltro)
	{
		posPath = 'posActual=' + page + '&' + jQuery(document.paginadoGridForm).serialize();
	}
	
	sendRequestJQTarifa (null, 
										solicitudesPaginadasRapidaPath + "?"+ posPath + "&idPolizaBusqueda=" + numeroPoliza + "&filtrar=true", 
										'gridEndososListadoPaginado', 
										'obtenerSolicitudesDefinirEndoso();');
	
}

function obtenerSolicitudesDefinirEndoso(){
	document.getElementById("cotizacionEndososListadoGrid").innerHTML = '';	
	solicitudesPolizaGrid = new dhtmlXGridObject('cotizacionEndososListadoGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var numeroPoliza;
	numeroPoliza = dwr.util.getValue('numeroPoliza');
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	
	var url_local = busquedaRapidaPath + "?" + posPath + "&filtrar=true"+ "&idPolizaBusqueda=" + numeroPoliza;
	solicitudesPolizaGrid.load(url_local);
		
}

function validar()
{
	var numeroPolizaFormateado = dwr.util.getValue('idPolizaBusqueda');
	var idSeleccionado = dwr.util.getValue("tipoEndoso");  
    var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
   
    if(validateNumeroPoliza(numeroPolizaFormateado))
    {
    	sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/mostrarDefTipoModificacion.action?tipoEndoso=' + idSeleccionado+'&numeroPolizaFormateado='+numeroPolizaFormateado+'&fechaIniVigenciaEndoso='+fechaIniVigenciaEndoso,targetWorkArea,null);    	
    }
    else
    {
    	mostrarMensajeInformativo("El Formato del N\u00famero de P\u00f3liza es Incorrecto.", "20", null);    	
    } 	    	
}


//Funcion para habilitar o deshabilitar boton Validar Numero
$(document).ready(function () 
{  	
	$('#idPolizaBusqueda').bind('keyup blur',function () 
		{ if ($.trim(this.value) == "") 
		{ $('#botonValidar').attr("disabled", true);}  
		else 
		{ $('#botonValidar').removeAttr("disabled"); }
		});	
	
	var idPoliza = dwr.util.getValue('idPolizaBusqueda');
	
	if(idPoliza == "")
    {
		$('#botonValidar').attr("disabled", true);
		
    }  
	else 
	{ $('#botonValidar').removeAttr("disabled"); 
	
	}	
});  
//Funcion para habilitar o deshabilitar datePicker fechaIniVigenciaEndoso
$('#tipoEndoso').change(function ()  { 
	if ($.trim(this.value) != '')  {
		if (this.value == constantTipoEndosoExtensionVig
		|| this.value == constantTipoEndosoCancelacionEndoso
		|| this.value == constantTipoEndosoRehabilitacionEndoso
		|| this.value == constantTipoEndosoCambioAgente
		|| this.value == constantTipoEndosoAjustePrima
		|| this.value == constantTipoEndosoBajaIncisoPerdidaTotal
		|| this.value == constantTipoEndosoCancelacionPolizaPerdidaTotal) {
			//$('#fechaIniVigenciaEndoso').attr("disabled", true);
			//$('#fechaIniVigenciaEndoso').attr("showOn", 'focus');
			//$('#fechaIniVigenciaEndoso').attr("required", false);
			$('#fechaIniVigenciaEndoso').val("");
			$('#dtRow').hide();
			
		} else {
			//$('#fechaIniVigenciaEndoso').removeAttr("disabled");
			//$('#fechaIniVigenciaEndoso').removeAttr("showOn");
			$('#dtRow').show();
			//$('#fechaIniVigenciaEndoso').attr("required", true);
		}
		
		if (this.value == constantTipoEndosoBajaInciso
				|| this.value == constantTipoEndosoCancelacionPoliza) {
			
			var idPoliza = dwr.util.getValue('numeroPoliza');
			
			listadoService.listadoMotivoEndoso(this.value, idPoliza,function(data){
				var combo = document.getElementById('motivoEndoso');
				addOptions(combo,data);
			});
			$('#tdMotivo').show();
		} else {
			$('#tdMotivo').hide();
		}
	}		
});

function validateNumeroPoliza(numeroPoliza) {  
    var re = new RegExp("[0-9]{4}-[0-9]{8}-[0-9]{2}"); 
    return re.test(numeroPoliza); 
} 

function cancelarCotizacion(idToPoliza, numeroPolizaFormateado, idCotizacionContinuity, numeroCotizacion)
{
	
	var mensaje = "\u00BFEst\u00E1 Seguro que desea Cancelar la Cotizaci\u00F3n de Endoso ?";
	if(confirm(mensaje))
	{		
		sendRequestJQ(null,cancelarCotizacionPath+'?namespaceOrigen='+currentNamespace+'&actionNameOrigen='+currentMainAction + '&cotizacion.continuity.id=' + idCotizacionContinuity + '&idPolizaBusqueda='+idToPoliza+'&numeroPolizaFormateado='+numeroPolizaFormateado + '&cotizacion.continuity.numero=' + numeroCotizacion ,targetWorkArea ,null);
	}
}

function consultarCotizacion(idPoliza,idTipoEndoso,accionEndoso,fechaIniVigenciaEndoso)
{
	sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action?tipoEndoso=' + idTipoEndoso +'&idPolizaBusqueda='+idPoliza+'&fechaIniVigenciaEndoso='+fechaIniVigenciaEndoso+'&accionEndoso=' + accionEndoso + '&namespaceOrigen=' + currentNamespace + '&actionNameOrigen=' + currentMainAction,targetWorkArea,null);
}

function loadFortimax(){
	var url = dwr.util.getValue("linkFortimax");
	window.open(url,'Fortimax');
	
}
function verComplementarCotizacionDeEntrevista(){
	var idToPersonaContratante = dwr.util.getValue("cotizacion.idToPersonaContratante");
	var idToCotizacion = dwr.util.getValue("cotizacion.idToCotizacion");
	var moneda = dwr.util.getValue("entrevista.moneda");
	var primaEstimada = dwr.util.getValue("entrevista.primaEstimada");
	var numeroSerieFiel = dwr.util.getValue("entrevista.numeroSerieFiel");
	var pep = dwr.util.getValue("entrevista.pep");
	var parentesco = dwr.util.getValue("entrevista.parentesco");
	var nombrePuesto = dwr.util.getValue("entrevista.nombrePuesto");
	var cuentaPropia = dwr.util.getValue("entrevista.cuentaPropia");
	var documentoRepresentacion = dwr.util.getValue("entrevista.documentoRepresentacion");
	var folioMercantil = dwr.util.getValue("entrevista.folioMercantil");
	var nombreRepresentado=dwr.util.getValue("entrevista.nombreRepresentado");
	var pdfDownload ='pdfDownload';
	var	periodoFunciones=dwr.util.getValue("entrevista.periodoFunciones");
	var parms=null;
	parms = jQuery('#entrevistaForm').serialize();
	if(numeroSerieFiel===''){
		alert('Es necesaria la FIEL');
		return;
	}

	if(document.getElementById("personaFisica")){
		
		
		
		if(jQuery('input[name="entrevista\\.pep"]:checked').val() == undefined){
			alert('La pregunta "PEP" es obligatoria.');
			return;
		}
		
		if(pep=='true'){
			
			if(parentesco=='' ||nombrePuesto==''||periodoFunciones==''){
				alert("Los campos:Parentesco,Puesto y Periodo de funciones son obligatorios ");
				return;
			}
			
		} 
		
		if(jQuery('input[name="entrevista\\.cuentaPropia"]:checked').val() == undefined){
			alert('La pregunta "Actúa por cuenta propia" es obligatoria.');
			return;
		}
		
		if(cuentaPropia=='true'){
			
				if(nombreRepresentado==''||jQuery('input[name="entrevista\\.documentoRepresentacion"]:checked').val() == undefined)
				{
					alert("Los campos: Documento Juridico y nombre por quién actua son obligatorios ");
					return;
				}
				
			
		}
	}
	
	if(document.getElementById("personaMoral")){
		if(folioMercantil=='')
		{
			alert("El Campo Folio Mercantil es Obligatorio.");
			return;
		}
	}
	

	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/emision/guardarEntrevista.action?cotizacion.idToPersonaContratante='+idToPersonaContratante
			+'&cotizacion.idToCotizacion='+idToCotizacion
			+'&'+parms
			+'&pdfDownload = '+pdfDownload
			,'contenido', null);
	
	
	
}

function checkBoxSeleccioneDocumento(indiceDocumento){
	 if($("#checkBox["+indiceDocumento+"].seleccionado").val()=="true") {

	jQuery("#documentacion"+indiceDocumento+"].descripcion ").removeAttr("disabled");
	jQuery("#documentacion"+indiceDocumento+"].fechaVigencia ").removeAttr("disabled");
	
	 }else {
		
		 jQuery("#documentacion"+indiceDocumento+"].descripcion ").attr("disabled","disabled");
			jQuery("#documentacion"+indiceDocumento+"].fechaVigencia ").attr("disabled","disabled");
	}
}
function verComplementarCotizacionDeDocumentacionEndosos(){
	var idToPersonaContratante = dwr.util.getValue("polizaDTO.cotizacionDTO.idToPersonaContratante");
	var polizaId = dwr.util.getValue("polizaId");
	var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
	var fechaVigencia5 = dwr.util.getValue("documentacion[5].fechaVigencia");
	var temp= fechaVigencia5.split('/');
	var dt = new Date(temp[1]+'/'+temp[0]+'/'+temp[2]);
	var myDate = new Date();	
	var startMsec = myDate.getTime();
	var mils =86400000 *91;
	startMsec=startMsec-mils;
	var milsDate = dt.getTime();
	if(milsDate<startMsec){
		alert("La  fecha de vigencia no debe ser menor  a tres");
		return;
	}
	var parms=null;
	parms = jQuery('#DocumentacionForm').serialize();
	parent.sendRequestJQ(null, '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos/guardarDocumentacion.action?'+parms
									,'contenido', null);
	
	
	
}
function verDetallePolizaActual(idToPoliza, tipoRegreso) {
	if (idToPoliza != null && idToPoliza != "") {
		var fullDate = new Date();
		var currentDate = getDateFormat(fullDate);
		var dateMillis = fullDate.getTime(); 
		verDetallePoliza(idToPoliza, currentDate, dateMillis, currentDate, dateMillis, tipoRegreso, null, true, true);	
	} else {
		mostrarMensajeInformativo("Favor de validar el N\u00FAmero de P\u00F3liza.", "20", null);
	}
	
}

function emitirEndoso(id, polizaId, fechaIniVigenciaEndoso, accionEndoso, idTipoEndoso)
{
	var actionNameOrigen = "mostrarDefTipoModificacion";
	var namespaceOrigen = "/endoso/cotizacion/auto/solicitudEndoso";
	var path = null;
	var extraParam = "";
	if(idTipoEndoso === 5){
		path = emitirAltaIncisoPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 6){
		path = emitirBajaIncisoPath;
	}
	if(idTipoEndoso === 7){
		path = emitirCambioDatosPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 8){
		path = emitirCambioAgentePath;
		extraParam = "&cotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 9){
		path = emitirCambioFormaPagoPath;
		extraParam = "&cotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 10){
		path = emitirEndosoCancelacionEndosoPath;
		extraParam = "&cotizacionContinuityId="+id;
	}
	if(idTipoEndoso === 11){
		path = emitirEndosoCancelacionPolizaPath;
	}
	if(idTipoEndoso === 12){
		path = emitirExtensionVigenciaPath;
	}
	if(idTipoEndoso === 13){
		path = emitirInclusionAnexoPath;
		extraParam = "&biCotizacion.continuity.id="+id+"&biCotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 14){
		path = emitirInclusionTextoPath;
		extraParam = "&biCotizacion.continuity.id="+id+"&biCotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 15){
		path = emitirMovimientosPath;
	}
	if(idTipoEndoso === 16){
		path = emitirRehabilitacionIncisoPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 17){
		path = emitirRehabilitacionEndosoPath;
		extraParam = "&cotizacionContinuityId="+id;
	}
	if(idTipoEndoso === 18){
		path = emitirRehabilitacionPolizaPath;
	}
	
	if(idTipoEndoso === 19){
		path = emitirAjustePrimaPath;
	}	
		
	if(path != null){
		var mensaje = "\u00BFSolicitar Emisi\u00F3n?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, path + "?cotizacion.continuity.id="+id+"&polizaId="+polizaId+"&fechaIniVigenciaEndoso="+fechaIniVigenciaEndoso+"&accionEndoso="+accionEndoso+extraParam,targetWorkArea,null);
		}
	}
	
}



$(function(){
	$("#fechaIniVigenciaEndoso").bind("keypress", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();         
	        return false;
	    }
	});
});

