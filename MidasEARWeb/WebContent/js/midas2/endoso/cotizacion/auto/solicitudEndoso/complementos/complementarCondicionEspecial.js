var condicionesAsociadasGrid;
var condicionesDisponiblesGrid;

function initGrids() {
	obtenerCondicionesAsociadas();
	obtenerCondicionesDisponibles();
}

function obtenerCondicionesAsociadas() {
	document.getElementById("condicionesAsociadasGrid").innerHTML = '';
	condicionesAsociadasGrid = new dhtmlXGridObject('condicionesAsociadasGrid');
	condicionesAsociadasGrid.load(condicionesAsociadasPath + "?" + jQuery(document.condicionEspecialForm).serialize());
	
	//Solo debe permitir eliminar las condiciones no obligatorias
	condicionesAsociadasGrid.attachEvent("onBeforeDrag", 
		function(id) {
        	if (condicionesAsociadasGrid.getUserData(id,"drag") == 0) {
        		return true;
        	}
        	return false;
		}
    )

	var condicionesProcessor = new dataProcessor(relacionarCondicionesPath +"?" +jQuery(document.condicionEspecialForm).serialize());

	condicionesProcessor.enableDataNames(true);
	condicionesProcessor.setTransactionMode("POST");
	condicionesProcessor.setUpdateMode("cell");
	condicionesProcessor.attachEvent("onAfterUpdate", initGrids);
	condicionesProcessor.init(condicionesAsociadasGrid);
}

function obtenerCondicionesDisponibles() {
	document.getElementById("condicionesDisponiblesGrid").innerHTML = '';
	condicionesDisponiblesGrid = new dhtmlXGridObject('condicionesDisponiblesGrid');
	condicionesDisponiblesGrid.load(condicionesDisponiblesPath + "?" + jQuery(document.condicionEspecialForm).serialize());
}

function buscarCondiciones() {
	document.getElementById("condicionesDisponiblesGrid").innerHTML = '';
	condicionesDisponiblesGrid = new dhtmlXGridObject('condicionesDisponiblesGrid');
	condicionesDisponiblesGrid.load(buscarCondicionPath + "?" + jQuery(document.condicionEspecialForm).serialize());
}

function asociarTodas() {	
	sendRequestJQ(null, asociarTodasPath + "?" + jQuery(document.condicionEspecialForm).serialize(), 'contenido_condicionesEspeciales', null);	
}

function eliminarTodas() {
	sendRequestJQ(null, eliminarTodasPath + "?" + jQuery(document.condicionEspecialForm).serialize(), 'contenido_condicionesEspeciales', null);	
}

