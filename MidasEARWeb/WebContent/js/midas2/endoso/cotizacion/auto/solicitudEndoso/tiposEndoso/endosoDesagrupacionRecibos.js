/**
 * 
 */

function cotizar()
{ 
	var actionPath;
	var selectedRowId = "";	
	
	if (incisosGrid != null) {
		selectedRowId = incisosGrid.getCheckedRows(0);				
	}		
	
	if (selectedRowId == null || selectedRowId == "") {
			alert("Debe seleccionar al menos un inciso");
		return;
	}	
	
	sendRequestJQ(null, cotizarEndDesagrupaRecibosActionPath + "?"+ jQuery(document.endosoDesagrupacionRecibosForm).serialize() 
			+'&elementoSeleccionado=' + selectedRowId + '&fechaIniVigenciaEndoso=' + jQuery("#fechaIniVigenciaEndoso").val(),targetWorkArea,null);
}

function cancelarEndosoDesagrupacionRecibos(validar) {	
	if (validar) {
		var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el endoso de Desagrupaci\u00F3n de Recibos?, se perder\u00E1 la configuraci\u00F3n si ya ha definido alguna.";
		if (confirm(mensaje)) {
			sendRequestJQ(null, cancelarEndDesagrupaRecibosActionPath+"?"+jQuery(document.endosoDesagrupacionRecibosForm).serialize(), targetWorkArea,null);				
		}
	} else {
		sendRequestJQ(null, cancelarEndDesagrupaRecibosActionPath+"?"+jQuery(document.endosoDesagrupacionRecibosForm).serialize(), targetWorkArea,null);	
	}	
}

function emitir(tipoEndoso)
{	
	var mensaje = "\u00BFEst\u00E1 seguro que desea emitir el endoso de Desagrupaci\u00F3n de Recibos?, " +
			"una vez desagrupados los recibos no habr\u00E1 forma de agruparlos nuevamente.";
	if (confirm(mensaje)) {
		sendRequestJQ(null, emitirEndDesagrupaRecibosActionPath + "?"+ jQuery(document.endosoDesagrupacionRecibosForm).serialize() + '&fechaIniVigenciaEndoso=' 
				+ jQuery("#fechaIniVigenciaEndoso").val(),targetWorkArea,null);
	}
}