/**
 * 
 */
var cargaMasivasGrid;
var cargaMasivasDetalleGrid;
var polizaId;
var validoEn;
var accionEndoso;
var nivelAplicacion;
var numeroPolizaFormateado;
var cotizacionContinuityId;
var tipoEndoso;
var tipoRegreso;

function descargarCargaMasiva(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function mostrarResumenCargaMasiva(idToCargaMasiva){
	var idToCotizacion = jQuery("#id").val();
	var tipoRegreso = jQuery("#tipoRegreso").val();
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/verDetalleCarga.action?id=" + idToCotizacion + "&idToCargaMasivaAutoCot=" + idToCargaMasiva + "&tipoRegreso="+tipoRegreso, "contenido", null);
}

function iniciaListadoDetalle(){
	var idToCargaMasivaAutoCot = jQuery("#idToCargaMasivaAutoCot").val();
	document.getElementById("cargasMasivasDetalleGrid").innerHTML = '';
	cargaMasivasDetalleGrid = new dhtmlXGridObject("cargasMasivasDetalleGrid");
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasDetalleGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasDetalleGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasDetalleGrid.load("/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/listarDetalleCarga.action?idToCargaMasivaAutoCot="+ idToCargaMasivaAutoCot);
}

/*function regresarACargaMasivaAuto(){
	var idToCotizacion = jQuery("#id").val();
	var tipoRegreso = jQuery("#tipoRegreso").val();
	sendRequestJQ(null, "/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/mostrarCarga.action?id=" + idToCotizacion + "&tipoRegreso="+tipoRegreso, "contenido", null);	
}*/


function descargarPlantillaCarga(){
	var idToCotizacion = jQuery("#id").val();
	// # OBTIENES VALORES DEL DOM
	getVariablesEntorno();
	
	var location ="/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/descargarPlantilla.action?"
			+"id=" +idToCotizacion
			+"&numeroPolizaFormateado="+numeroPolizaFormateado
			+"&fechaIniVigenciaEndoso="+validoEn
			+"&nivelAplicacion="+nivelAplicacion
			+"&accionEndoso="+accionEndoso
			+"&cotizacionContinuityId="+cotizacionContinuityId;
	
	window.open(location, "Cotizacion_COT" + idToCotizacion);
}

function iniciaListadoMasiva(){
	
	var idCotizacion = jQuery("#id").val();
	
	document.getElementById("cargasMasivasGrid").innerHTML = '';
	cargaMasivasGrid = new dhtmlXGridObject("cargasMasivasGrid");
	cargaMasivasGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivasGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivasGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivasGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivasGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/listarCarga.action?id="+ idCotizacion);
}

function importarArchivo(tipoCarga){
	//var idToCotizacion = 12056;
	var idToCotizacion = jQuery("#id").val();
	//alert(idToCotizacion);
	
	// # OBTIENES VALORES DEL DOM
	getVariablesEntorno();
	
	var tipoRegreso = jQuery("#tipoRegreso").val();
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaPolizaIncisos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva Poliza de Incisos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			
        			sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/validaCarga.action?"
        					+ "id=" + idToCotizacion 
        					+ "&idToControlArchivo=" + idToControlArchivo 
        					+ "&tipoCarga="    + tipoCarga 
        					+ "&tipoRegreso="  + tipoRegreso
        					+ "&accionEndoso=" +accionEndoso
        					+ "&numeroPolizaFormateado="+numeroPolizaFormateado
        					+ "&fechaIniVigenciaEndoso="+validoEn
        					+ "&nivelAplicacion="+nivelAplicacion
        					+ "&cotizacionContinuityId="+cotizacionContinuityId
        			
        			, "contenido", null);
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaPolizaIncisos").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "16");
}



function mostrarErrorCargaMasiva(estatus, idToDetalleCargaMasivaAutoCot){
	if(estatus == 0){
		var idToCotizacion = jQuery("#id").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarLogErroresInciso.action?idToDetalleCargaMasivaAutoCot=" + idToDetalleCargaMasivaAutoCot;
		window.open(location, "Cotizacion_COT" + idToCotizacion);
	}
	if(estatus == 1){
		mostrarMensajeExito();
	}
	if(estatus == 2){
		mostrarVentanaMensaje('30', 'Pendiente de Procesar');
	}
}

function descargarLogCargaMasiva() {
	
	if(jQuery("#logErrors").val() == 'true'){
		var idToCotizacion = jQuery("#id").val();
		
		var idToControlArchivo = jQuery("#idToControlArchivo").val();
		var location ="/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp/descargarLogErrores.action?id=" + idToCotizacion + "&idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT" + idToCotizacion);
	}
}

// # ESTAS VARIABLES SE CARGAN SIEMPRE EN LA HOJA DE CARGA MASICA
function getVariablesEntorno(){
	validoEn =               jQuery("#fechaIniVigenciaEndoso").val();
	accionEndoso =           jQuery("#accionEndoso").val();
	nivelAplicacion =        jQuery("#nivelAplicacion").val();
	numeroPolizaFormateado = jQuery("#numeroPolizaFormateado").val();
	cotizacionContinuityId = jQuery("#cotizacionContinuityId").val();
	tipoRegreso =            jQuery("#tipoRegreso").val();
	polizaId =               jQuery("#polizaId").val();
	tipoEndoso =             jQuery("#tipoEndoso").val();
}


function regresarAPoliza(){
	
    getVariablesEntorno(); // # OBTIENE VALORES DEL DOM
    var url            = "";
    var tipoContenedor = "";
    
    //alert(tipoRegreso);
	
	if( tipoRegreso == "0"){
		
		tipoContenedor   = "contenido";
		var listaIncisos = jQuery("#listaIncisos").val();
		
		url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial/mostrarInclusionCondiciones.action?" +
				"motivoEndoso=&namespaceOrigen=/endoso/cotizacion/auto/solicitudEndoso" 
				+"&actionNameOrigen=mostrarDefTipoModificacion" 
				+"&nextFunction=iniciaDefinirSolicitudEndoso" 
				+"&tipoMensaje=" 
				+"&fechaIniVigenciaEndoso=" + validoEn
				+"&negocioDerechoEndosoId=" 
				+"&accionEndoso=2" 
				+"&numeroPolizaFormateado=" + numeroPolizaFormateado
				+"&polizaId=" + polizaId
				+"&mensaje=" 
				+"&nivelAplicacion=1"
				+"&codigosInciso="+ listaIncisos;
		
	}else if ( tipoRegreso == "1" ){
		
		tipoContenedor   = "contenido";
		
		// # SI NIVEL APLICACION ES INCISO OBTENER LOS INCISOS DEL DOM
		
		url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial/mostrarBajaCondiciones.action?" +
		"motivoEndoso=&namespaceOrigen=/endoso/cotizacion/auto/solicitudEndoso" 
		+"&actionNameOrigen=mostrarDefTipoModificacion" 
		+"&nextFunction=iniciaDefinirSolicitudEndoso" 
		+"&tipoMensaje=" 
		+"&fechaIniVigenciaEndoso=" + validoEn
		+"&negocioDerechoEndosoId=" 
		+"&accionEndoso=2" 
		+"&numeroPolizaFormateado=" + numeroPolizaFormateado
		+"&polizaId=" + polizaId
		+"&mensaje=" 
		+"&nivelAplicacion="+nivelAplicacion;
		
		if( nivelAplicacion == 1){
			var listaIncisos = jQuery("#listaIncisos").val();
			url += "&codigosInciso="+listaIncisos; 
		}
		
	}else if ( tipoRegreso == "2" ){
		
		tipoContenedor = "targetWorkArea";
		url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/mostrarDetalle.action?" 
				+"tipoEndoso=5" 
				+"&polizaId="+polizaId
				+"&fechaIniVigenciaEndoso="+validoEn
				+"&accionEndoso=0"
				+"&cotizacion.continuity.id=" + cotizacionContinuityId
				+"&namespaceOrigen=/endoso/cotizacion/auto/solicitudEndoso" 
				+"&actionNameOrigen=mostrarDefTipoModificacion";
	}
	
	//alert(url);
	sendRequestJQ(null,url,tipoContenedor, null);
	
}