/**
 * Javascript para manejar la pantalla Cambio de Agente del modulo de Endosos.
 */

function cotizarEndosoCambioAgente(accion) {
	var idAgenteNuevo = $("#idAgenteCot").val();
	var motivoCambio = $("#motivoCambio").val();
	if (idAgenteNuevo && motivoCambio) {
		sendRequestJQ(null, cotizarActionPath + "?" + jQuery("#endosoCambioAgenteForm").serialize() 
				+ "&accionEndoso=" + accion, targetWorkArea, null);		
	} else {
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios.", "20", null);
	}
}

function emitirEndoso() {
	var idAgenteNuevo = $("#idAgenteCot").val();
	var motivoCambio = $("#motivoCambio").val();
	if (idAgenteNuevo && motivoCambio) {
    	sendRequestJQ(null, emitirActionPath + "?" + jQuery("#endosoCambioAgenteForm").serialize(), targetWorkArea, null);	
	} else {
		mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios.", "20", null);
	}			
}

function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar el Cambio de Agente?, se perder\u00E1 la configuraci\u00F3n de agentes si ya ha definido alguna.";
	if (confirm(mensaje)) {
		sendRequestJQ("#endosoCambioAgenteForm", cancelarActionPath, targetWorkArea, null);				
	}
}

function findItem (term, devices) {
    var items = [];
    for (var i=0;i<devices.length;i++) {
        var item = devices[i];
        for (var prop in item) {
            var detail = item[prop].toString().toLowerCase();
            if (detail.indexOf(term)>-1) {
                items.push(item);
                break;               
            }
        }
    }
    return items;
}

$(function(){
	// buscarAgentePath + "?nombreAgente=" + this.value,
	$("#nombreAgente").autocomplete({
        source: listaAgentes.size() > 0 ? listaAgentes : function(request, response){
        	jQuery.ajax({
        		"url" : buscarAgentePath,
        		"data" : "nombreAgente=" + $("#nombreAgente").val(),
        		"dataType" : "json",
        		"success": function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.label,
                            id: item.id
                        }
                    }))
                }
        	});
        	
        },
        minLength: 3,
        delay: 500,
        select: function(event, ui) {
              $("#idAgenteCot").val(ui.item.id);              
        },
        search: function(event, ui) {
        	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
            var busqueda = jQuery("#nombreAgente").val();
            if(findItem(busqueda.toString().toLowerCase(), listaAgentes).length == 0){
            	alert("Agente ("+busqueda.toString().toUpperCase()+") no existe o no disponible para la poliza");
            	jQuery("#nombreAgente").val("");
            	return false;
            }
        }
    });
});

$(function(){
	$("#nombreAgente").bind("keypress", function(e) {
	    if (e.keyCode == 13) {
	        e.preventDefault();         
	        return false;
	    }
	});
});

