var TIPO_ENDOSO_ALTA_INCISO = 5;
var TIPO_ENDOSO_CAMBIO_DATOS = 7;

function mostrarDatosContratante(cotizacionContinuityId, validoEn) {
	var validoEn = jQuery('#validoEn').val();
	var url = mostrarDatosContratanteURL + "?cotizacionContinuityId=" + cotizacionContinuityId
	+ "&fechaIniVigenciaEndoso=" + validoEn;
	mostrarVentanaModal('ventanaAsegurado', "Datos del Contratante", 0,0,700,400, url);	
}


function mostrarClientesAsociados(){
	var url = getClientesAsociadosURL + "?idToNegocio=" + dwr.util.getValue("idToNegocio");
	mostrarVentanaModal("winAgentesAsociados", "Clientes asociados al Negocio", 0,0,700,400, url);	
}

function cargaClienteAsociado(idCliente,nombreCliente){
	dwr.util.setValue("cotizacion.value.personaContratanteId", idCliente);
	dwr.util.setValue("cliente.nombreCliente", nombreCliente);
	seleccionarCliente(idCliente);
	cerrarVentanaModal("winAgentesAsociados");
}

function seleccionarCliente(idCliente) {
	
	tipoEndoso = jQuery('#tipoEndoso').val();
	var nextFunc = null;
	if(tipoEndoso == TIPO_ENDOSO_ALTA_INCISO)
	{
		nextFunc = 'refreshComplementarEmision()';		
	}else if(tipoEndoso == TIPO_ENDOSO_ALTA_INCISO)
	{
		nextFunc = 'refreshComplementarEmisionCD()';		
	}
	
	sendRequestJQ(null, seleccionarContratanteURL + '?cotizacionContinuityId=' + jQuery("#cotizacionContinuityId").val() 
			+ '&idCliente=' + idCliente + "&fechaIniVigenciaEndoso=" + dwr.util.getValue("fechaIniVigenciaEndoso"),
			'datosContratante', nextFunc);
}


function mostrarAgregarCliente(){
	var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?tipoAccion=consulta&idField=idAsegurado&tipoBusqueda=1" +
			"&divCarga=clienteModal2";
	parent.sendRequestWindow(null, url, obtenerVentanaCliente);
}

function obtenerVentanaCliente(){
	var wins = parent.obtenerContenedorVentanas();
	ventanaClientes= wins.createWindow("clienteModal", 400, 320, 950, 450);
	ventanaClientes.center();
	ventanaClientes.setModal(true);
	ventanaClientes.setText("Consulta de clientes");
	return ventanaClientes;
}

function asignarIdCliente(idCliente, idDomCliente) {
	muestraResultadoAgregarCliente(idCliente, idDomCliente);		
}

function muestraResultadoAgregarCliente(idCliente, idDomCliente){
		var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/seleccionarContratante.action"
			+ "?cotizacionContinuityId=" + dwr.util.getValue("cotizacionContinuityId")
			+ "&fechaIniVigenciaEndoso=" + dwr.util.getValue("fechaIniVigenciaEndoso")
			+ "&idCliente="
			+ idCliente
			
			//+ "&nextFunction=closeVentanaAsegurado"
			;
		redirectVentanaModal('ventanaAsegurado', url, null);	
		
}

function closeVentana(){
	
	cerrarVentanaModal('ventanaAsegurado');	
}


function refreshComplementarEmision(){
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/validarDatosComplementarios.action"
		+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
		+ '&accionEndoso=' + jQuery('#accionEndoso').val() + '&cotizacion.continuity.id=' + jQuery('#idCotizacionContinuity').val() + 
		'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
	
	sendRequestJQ(null, path, 'contenido_detalle', null);	
}

function refreshComplementarEmisionCD(){
	
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action"
		+ '?tipoEndoso=' + jQuery('#tipoEndoso').val() + '&idPolizaBusqueda=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
		+ '&accionEndoso=' + 4 + 
		'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
	
	sendRequestJQ(null, path, targetWorkArea, null);	
}