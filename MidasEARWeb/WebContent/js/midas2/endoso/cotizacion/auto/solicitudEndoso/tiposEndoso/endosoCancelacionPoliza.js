/**
 * 
 */

function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea abandonar la Cancelacion de la Poliza?, se perder\u00E1 la configuraci\u00F3n de la Cancelacion de Poliza si ya ha definido alguna.";
	if (confirm(mensaje)) {
		sendRequestJQ(null, cancelarActionPath + "?" + jQuery(document.solicitudPolizaForm).serialize(), targetWorkArea,null);				
	}
}

function emitir(accion) {
	sendRequestJQ(null, emitirActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize()+"&accionEndoso="+accion, targetWorkArea, null);		
}


function cotizar(accion) {
	var primaIgualar = new Number(jQuery("#primaTotalIgualar").val());
	var primaTotalPoliza = new Number(jQuery("#primaTotalPoliza").val());
	
	if(primaIgualar != "" && primaIgualar >= 0){
		alert("La prima total a igualar debe ser menor que 0.");
	} else if (primaIgualar != "" && primaIgualar < primaTotalPoliza){
		alert("La prima total a igualar no debe superar la prima de la poliza");
	}else{
		sendRequestJQ(null, cotizarActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize()+"&accionEndoso="+accion, targetWorkArea, null);
	}
}


function previsualizarCobranza(cotizacionContinuityId, validoEn, accionEndoso, tipoEndoso) {
	var url = previsualizarCobranzaActionPath +  "?cotizacionContinuityId=" + cotizacionContinuityId
									+ "&validoEn=" + validoEn + "&accionEndoso=" + accionEndoso + "&tipoEndoso=" + tipoEndoso;
	mostrarVentanaModal("Cobranza", 'Cobranza', 100, 100, 500, 300, url);

}