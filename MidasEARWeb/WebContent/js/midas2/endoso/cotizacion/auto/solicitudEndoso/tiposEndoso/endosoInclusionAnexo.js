var docAnexosGrid;
var docAnexosDisponiblesGrid;
var docAnexosProcessor;

function emitir() {
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/emitir.action?";
	
	docAnexosGrid.setCSVDelimiter('-');	
	var anexos= docAnexosGrid.serializeToCSV();
	
	sendRequestJQ(null, path + jQuery(document.endosoInclusionAnexoForm).serialize() + "&anexos=" + anexos, targetWorkArea, null);
}

function cotizar() {

	if (docAnexosGrid.getRowsNum() > 0 ) {
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/cotizar.action?";
		
		sendRequestJQ(null, path + jQuery(document.endosoInclusionAnexoForm).serialize(), targetWorkArea, null);
	} else  {
		mostrarVentanaMensaje('20', 'Asocie al menos un documento');
		valido =  false;
		return;		
	}
}

function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Inclusi\u00F3n de Documentos Anexos?, " +
			"se perder\u00E1 la configuraci\u00F3n de Documentos si ya ha definido alguna.";
	
	if (confirm(mensaje)) {
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/cancelar.action?polizaId=" +  dwr.util.getValue("polizaId");
		sendRequestJQ(null, path, targetWorkArea,null);				
	}
}


/**
 * Relaciones de paquete
 */
var negocioTipoPolizaAsociadasGrid;
var negocioTipoPolizaDisponiblesGrid;	
var  negocioTipoPolizaProcessor;
/**
 * Tipo Poliza
 */

function obtenerDocAnexosAsociados(){
	document.getElementById("docAnexosAsociadosGrid").innerHTML = '';
	docAnexosGrid = new dhtmlXGridObject('docAnexosAsociadosGrid');
	docAnexosGrid.attachHeader("&nbsp,#text_filter,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp");
	docAnexosGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/obtenerDocAnexosAsociados.action?" +
			jQuery(document.endosoInclusionAnexoForm).serialize());
	
	//Creacion del DataProcessor
	negocioTipoPolizaProcessor = new dataProcessor("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/accionAnexoAdicional.action?"
			+jQuery(document.endosoInclusionAnexoForm).serialize());

	negocioTipoPolizaProcessor.enableDataNames(true);
	negocioTipoPolizaProcessor.setTransactionMode("POST");
	negocioTipoPolizaProcessor.setUpdateMode("cell");
	negocioTipoPolizaProcessor.attachEvent("onAfterUpdate", refrescarGridsDocAnexos);
	negocioTipoPolizaProcessor.init(docAnexosGrid);
}


function obtenerDocAnexosDisponibles(){
	document.getElementById("docAnexosDisponiblesGrid").innerHTML = '';
	docAnexosDisponiblesGrid = new dhtmlXGridObject('docAnexosDisponiblesGrid');
	docAnexosDisponiblesGrid.attachHeader("&nbsp,#text_filter,#text_filter,&nbsp,&nbsp,&nbsp,&nbsp,&nbsp");
	docAnexosDisponiblesGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo/obtenerDocAnexosDisponibles.action?" +
			jQuery(document.endosoInclusionAnexoForm).serialize());
}

function iniciaGridsDocAnexos() {
	refrescarGridsDocAnexos();
}


function refrescarGridsDocAnexos(){

	obtenerDocAnexosAsociados();
	obtenerDocAnexosDisponibles();
	return true; 
}




