var listadoSolicitudGrid;
var ventanaVehiculo;

var TIPO_ENDOSO_ALTA_INCISO = 5;


function agregarInciso(idPoliza, validoEn, tipoEndoso) {
	var nuevoEndosoCot = "1";
	mostrarVentanaInciso(idPoliza,validoEn,nuevoEndosoCot,tipoEndoso, null,'parent.closeInciso();');		
}

function mostrarInciso(idPoliza,validoEn, accionEndoso,tipoEndoso,incisoContinuityId, numero){
	mostrarVentanaInciso(idPoliza, validoEn, accionEndoso, tipoEndoso,incisoContinuityId, numero, 'parent.closeInciso();');	
}

function mostrarVentanaConsultaInciso(incisoId, validoEn, validoEnMillis, recordFromMillis) {
	var url = '/MidasWeb/poliza/inciso/verDetalleInciso.action' + "?incisoId=" + incisoId
	
	if (validoEn != null && validoEn != "") {
		url+= "&validoEn=" + validoEn;
	}
	
	if (validoEnMillis != null && validoEnMillis != "") {
		url += "&validoEnMillis=" + validoEnMillis;
	}
		
	if (recordFromMillis != null && recordFromMillis != "") {
		url += "&recordFromMillis=" + recordFromMillis;
	}
	
	mostrarVentanaModal("Datos Vehiculo", 'Datos Vehiculo', 50, 50, 800, 600, url);;
}

function mostrarVentanaInciso(idPoliza,validoEn, accionEndoso, tipoEndoso, incisoContinuityId, numero, onCloseFunction) {
	var url;
	var encabezado = "Inciso para la cotizaci\u00f3n: " + jQuery("#numeroCotizacion").val();
	if(incisoContinuityId == null){
		url = verDetalleIncisoPath + "?polizaId="+idPoliza+"&validoEn="+validoEn+"&accionEndoso="+accionEndoso+"&tipoEndoso="+tipoEndoso;
	}else{
		encabezado = encabezado + " n\u00famero de inciso: " + numero;
		url = verDetalleIncisoPath + "?polizaId="+idPoliza+"&validoEn="+validoEn+"&accionEndoso="+accionEndoso+"&tipoEndoso="+tipoEndoso+"&bitemporalInciso.continuity.id="+incisoContinuityId;
	}
	mostrarVentanaModal("inciso", encabezado , 50, 50, 800, 600, url, onCloseFunction);	
}

function closeInciso(){	
	var tipoEndoso = dwr.util.getValue("tipoEndoso");
	var idPoliza = dwr.util.getValue("polizaId");
	var fechaIniVigenciaEndoso = dwr.util.getValue("validoEn");
	var accionEndoso = 4; //Edicion sin cotizar
	var mensaje = dwr.util.getValue("mensaje");
	var tipoMensaje = dwr.util.getValue("tipoMensaje");
	var negocioDerechoEndosoId = jQuery("#derechos").val();
	if(negocioDerechoEndosoId === undefined){
		negocioDerechoEndosoId = parent.jQuery("#derechos").val();
	}
	if(negocioDerechoEndosoId === undefined){
		negocioDerechoEndosoId = '';
	}
	
	var accionE = dwr.util.getValue("accionEndoso");
	if(accionE == 3)//Solo Consulta
	{
		accionEndoso = accionE;
	}
	
	parent.sendRequestJQ(null, verDetalleTipoEndosoOrigenPath+'?tipoEndoso='+ tipoEndoso+'&idPolizaBusqueda='+idPoliza+'&fechaIniVigenciaEndoso='+fechaIniVigenciaEndoso+'&accionEndoso='+accionEndoso+'&mensaje='+mensaje+'&tipoMensaje='+tipoMensaje+"&negocioDerechoEndosoId="+negocioDerechoEndosoId,targetWorkArea,null);
	parent.cerrarMensajeInformativo();
	parent.cerrarVentanaModal('inciso');		
}

function mostrarDatosRiesgo(incisoContinuityId, validoEn, tipoEndoso, accionEndoso){
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/mostrarControlDinamicoRiesgo.action";
	var url = path+"?bitemporalInciso.continuity.id=" +
	incisoContinuityId+"&validoEn=" + validoEn + "&tipoEndoso=" + tipoEndoso + "&accionEndoso=" + accionEndoso;

	parent.mostrarVentanaModal("winCargaControles", "Complementar Datos de Riesgo", 200, 220, 800, 500, url);
}


function guardarDatosRiesgo(){
	parent.submitVentanaModal("winCargaControles", jQuery('#complementarIncisoForm')[0]);
}

function cerrarRiesgoRefrescarInciso(){	
	parent.cerrarVentanaModal("winCargaControles");
	var ifr = parent.getWindowContainerFrame('inciso');
	ifr.contentWindow.validaRecalcular();
}

function recalcular(){
	parent.submitVentanaModal("inciso", document.incisoForm);	
}

function guardarInciso(){
	parent.sendRequestJQAsync(document.incisoForm,guardarIncisoPath,null,evaluaTipoPolizaInciso);	
}

function evaluaTipoPolizaInciso(){
	var isFlotilla =  document.getElementById("bitemporalCotizacion.value.tipoPoliza.claveAplicaFlotillas").value;
	var tipoEndoso =  document.getElementById("tipoEndoso").value;
	var negocioDerechoEndosoId = jQuery("#derechos").val();
	if(negocioDerechoEndosoId === undefined){
		negocioDerechoEndosoId = parent.jQuery("#derechos").val();
	}
	if(negocioDerechoEndosoId === undefined){
		negocioDerechoEndosoId = '';
	}
	
	if(isFlotilla == 1 && tipoEndoso == TIPO_ENDOSO_ALTA_INCISO){
		if(confirm("\u00BFDesea agregar otro inciso\u003F")){
			document.getElementById("bitemporalInciso.continuity.id").value = '';	
			document.getElementById("bitemporalAutoInciso.continuity.id").value = '';	
			
			
			url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/verDetalleInciso.action" + "?polizaId="+jQuery('#polizaId').val()+"&validoEn="+jQuery('#validoEn').val()+"&accionEndoso="+'1'+"&tipoEndoso="+jQuery('#tipoEndoso').val()+"&negocioDerechoEndosoId="+negocioDerechoEndosoId;
	
			parent.redirectVentanaModal('inciso', url, null);
			
			//sendRequestJQ(null, url, 'dhtmlxWindowMainContent', null);	
			
		}else{
			closeInciso();
		}		
	}else{
		closeInciso();
	}
}

function mostrarCoberturasInciso(){
	if (dwr.util.getValue('bitemporalAutoInciso.value.negocioPaqueteId') == -2) {
		return false;
	}
	document.getElementById("coberturasIncisoGrid").innerHTML = '';
	jQuery('#cargando').css('display','block');
	jQuery('#btnGuardar').css('display', 'none');
	var incisoContinuityId = dwr.util.getValue('bitemporalInciso.continuity.id') ;
	var idNegocioPaqueteId = dwr.util.getValue('bitemporalAutoInciso.value.negocioPaqueteId'); 
	var idEstado = dwr.util.getValue('bitemporalAutoInciso.value.estadoId');
	var idMunicipio = dwr.util.getValue('bitemporalAutoInciso.value.municipioId');
	if( incisoContinuityId != ''){
		jQuery('#btnGuardar').css('display','block');	
	}
		
	if(idNegocioPaqueteId != ''&& idEstado != '' && idMunicipio != ''){
		var path = "?"+ jQuery(document.incisoForm).serialize();
			jQuery('#error').hide();
			sendRequestJQTarifa(null,
					mostrarCoberturasIncisoPath + path,
					"coberturasIncisoGrid", null);
	}else if(idEstado == '' && idMunicipio == ''){
		parent.mostrarMensajeInformativo('Favor de seleccionar un Estado y/o Municipio',"20");
	}
	
	// muestra el boton actualizar
	jQuery('#btnRecalcular').css('display', 'block');
	jQuery('#cargando').css('display','none');
}

function cargaValoresInciso(){
	mostrarCoberturasInciso();
}

function validaDisable(combo, index){	
	if(jQuery("#claveContratoBoolean_"+index).is(":checked") || jQuery('#bitemporalCoberturaSeccionList_'+index+'__value_claveObligatoriedad').val() == '0'){
		ocultaGuardar();
	}else{
		alert('Se necesita contratar la cobertura antes de modificar el valor');
		combo.selectedIndex = 0;		
	}
	
}
function eliminarInciso(idPoliza, validoEn, accionEndoso, tipoEndoso, incisoContinuityId, numeroInciso, cotizacionContinuityId)
{
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el inciso '+ numeroInciso +'?'))
	{
		var url = 	eliminarIncisoPath + '?polizaId=' + idPoliza + '&validoEn=' + validoEn + '&accionEndoso=' + accionEndoso + '&tipoEndoso=' + tipoEndoso + '&bitemporalInciso.continuity.id=' + incisoContinuityId + '&bitemporalInciso.continuity.numero=' + numeroInciso + '&bitemporalInciso.continuity.cotizacionContinuity.Id=' + cotizacionContinuityId;
		sendRequestJQ(null, url,targetWorkArea, null);
	}
}


//Abre la ventana Multiplicar inciso en modal
function mostrarMultiplicarInciso(incisoContinuityId, validoEn){
	var url = mostrarMultiplicarIncisoPath + '?bitemporalInciso.continuity.id=' + incisoContinuityId + '&validoEn=' + validoEn;
	mostrarVentanaModal('multiplicarInciso', 'Multiplicar Inciso', 200, 220, 500, 200, url);	
}

/**
 * Abre la ventana para igualar la prima total de un inciso
 */
function mostrarIgualarInciso(incisoContinuityId, validoEn, idToPoliza, tipoEndoso, tipoAccion, fechaIniVigencia, valorPrimaTotal) {
	var url = mostrarIgualarIncisoPath + "?bitemporalInciso.continuity.id=" + incisoContinuityId 
		+  "&validoEn=" + validoEn + "&tipoEndoso="+ tipoEndoso + "&bitemporalInciso.value.valorPrimaTotal=" + valorPrimaTotal
	mostrarVentanaModal('igualarInciso', 'Igualar Inciso', 200, 220, 500, 200, url ,
			"editarCotizacion('"+ idToPoliza + "','" + tipoEndoso + "','" + tipoAccion + "','" + fechaIniVigencia + "')");	
}


function funcionRefrescarAltaInciso(){
	
	parent.cerrarVentanaModal('multiplicarInciso');
	parent.cerrarVentanaModal('igualarInciso');
	
	var tipoE = jQuery('#tipoEndoso').val();
	var urlAction = "";
	var accionEndoso = jQuery('#accionEndoso').val();
	if (accionEndoso == null || accionEndoso == 1) {
		accionEndoso = 4;
	}
	
	
	if(tipoE == TIPO_ENDOSO_ALTA_INCISO)
	{
		urlAction = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/mostrarAltaInciso.action";		
	}
	else // se asume Endoso de Movimientos
	{
		urlAction = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos/mostrar.action";		
	}
	
	var path = urlAction
		+ '?tipoEndoso=' + tipoE + '&polizaId=' + jQuery('#polizaId').val() + '&fechaIniVigenciaEndoso=' + jQuery('#fechaIniVigenciaEndoso').val()
		+ '&accionEndoso=' + accionEndoso + 
		'&namespaceOrigen=' + jQuery('#namespaceOrigen').val()+ '&actionNameOrigen=' + jQuery('#actionNameOrigen').val();
	
	sendRequestJQ(null, path, targetWorkArea, null);
}

//Las funciones que siguen aun no han sido transformadas para entidades bitemporales





/**
 * Multiplica un inciso n veces
 * recibe dos parametros para formar el incisoCotizaconId
 * y el numero de copias que se van hacer
 * @param numeroInciso 
 * @param idToCotizacion
 * @param numeroCopias
 */
function multiplicarInciso(numeroInciso,idToCotizacion,numeroCopias){
	var path="/MidasWeb/vehiculo/inciso/multiplicarInciso.action?incisoCotizacion.id.numeroInciso=" + numeroInciso + 
				"&incisoCotizacion.id.idToCotizacion=" + idToCotizacion + '&numeroCopias=' + numeroCopias;
	parent.redirectVentanaModal('multiplicarInciso', path, null)		
}

function funcionCierreMultiplicarInciso(idToCotizacion){
	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/verDetalleCotizacionContenedor.action?id='+idToCotizacion,'contenido', null);
	parent.cerrarVentanaModal('multiplicarInciso');
}


function mostrarBotonGuardar(){
	jQuery('#btnGuardar').css('display', 'block');
}

function ocultarBotonGuardar(){
	jQuery('#btnGuardar').css('display', 'none');
}

function muestraDatosAdicionalesVehiculo() {
	var validoEn = dwr.util.getValue("validoEn");
	var dateParts = validoEn.split("/");  
    var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]); 
      
	listadoService.obtieneNumeroDeRiesgosBitemporal(dwr.util.getValue("bitemporalInciso.continuity.id"),
																	date, dwr.util.getValue("tipoEndoso"),
			function(data){
				if(data == 2){
					jQuery('#btnDatosAdicionalesPaquete').css('display', 'block');
				}
	});
}

function muestraBotonDatosConductor() {
	var validoEn = dwr.util.getValue("validoEn");
	var dateParts = validoEn.split("/");  
    var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]); 
	
	listadoService.mostrarDatosConductor(dwr.util.getValue("bitemporalInciso.continuity.id"), date,
		function(data){
			if (data) {
				jQuery('#btnDatosConductor').css('display', 'block');
			}
		});
}

function mostrarDatosConductor(incisoId, validoEn,  accionEndoso, tipoEndoso) {
	
	var url =  "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso/mostrar.action" 
		+ "?incisoId=" + incisoId + "&accionEndoso=" + accionEndoso + "&claveTipoEndoso=" + tipoEndoso;
	
	if (validoEn != null && validoEn != "") {
		url+= "&validoEn=" + validoEn;
	}
	
	url+="&copiarDatos=1";
	parent.mostrarVentanaModal("ventanaVehiculo", "Complementar Datos de Conductor", 200, 220, 800, 500, url);	
}


/*
function eliminarInciso(idCotizacion,numeroSecuencia,seccion,filtros){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el inciso '+ numeroSecuencia +'?')){
		var path = 	'/MidasWeb/vehiculo/inciso/eliminarInciso.action?incisoCotizacion.id.idToCotizacion=' + idCotizacion + '&incisoCotizacion.numeroSecuencia=' + numeroSecuencia + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccion 
			+ '&descripcionInciso=' + filtros.descripcion
			+ '&incisoCotizacion.incisoAutoCot.negocioPaqueteId='+ filtros.paqueteId;
		parent.sendRequestJQAsync(null,path,'contenido',null);
	}
}
*/
function eliminarIncisoIndividual(idCotizacion,numeroSecuencia,seccion){
	if(confirm('\u00BFEst\u00e1 seguro que desea eliminar el inciso?')){
		var path = 	'/MidasWeb/vehiculo/inciso/eliminarInciso.action?incisoCotizacion.id.idToCotizacion=' + idCotizacion + '&incisoCotizacion.numeroSecuencia=' + numeroSecuencia + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccion;
		parent.sendRequestJQAsync(null,path,'contenido',null);
	}
}

// Abre la ventana Multiplicar inciso en modal
/*function mostrarMultiplicarInciso(numeroInciso, idToCotizacion,seccionId){
	var url = '/MidasWeb/vehiculo/inciso/mostrarMultiplicarInciso.action?incisoCotizacion.id.numeroInciso=' + numeroInciso + '&incisoCotizacion.id.idToCotizacion=' + idToCotizacion + '&incisoCotizacion.incisoAutoCot.negocioSeccionId=' + seccionId;
	mostrarVentanaModal('multiplicarInciso', 'Multiplicar Inciso', 200, 220, 500, 200, url);	
}*/


function muestraDetalleInciso(idToCotizacion, numeroInciso){
	var url;
	var porcentajeIva = "&incisoCotizacion.cotizacionDTO.porcentajeIva="+dwr.util.getValue("porcentajeIva");
	if(numeroInciso == null){
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+porcentajeIva;
	}else{
		url = verDetalleIncisoPath + "?incisoCotizacion.id.idToCotizacion="+dwr.util.getValue("idToCotizacion")+"&incisoCotizacion.id.numeroInciso="+numeroInciso+porcentajeIva;
	}
	parent.redirectVentanaModal("inciso", url, null);	
}



function CantidadRegistrosGrid(){
	if (listadoSolicitudGrid.getRowsNum()==0){
		 jQuery("#esquemadePago").hide();
		 jQuery("#imprimeCotizacion").hide();
		 jQuery("#enviaEmail").hide();
		 jQuery("#solicitaEmision").hide();
		 jQuery("#guardaInciso").show();
		 jQuery("#cargaMrasiva").hide();
		 jQuery("#agregarInciso").show();
	 }else{
		 jQuery("#esquemadePago").show();
		 jQuery("#imprimeCotizacion").show();
		 jQuery("#enviaEmail").show();
		 jQuery("#solicitaEmision").show();
		 jQuery("#guardarInciso").show();
		 jQuery("#cargaMasiva").show();
		 jQuery("#agregarInciso").show();
	 }		
}


function closeVehiculo(cotizacionId){
	cerrarVentanaModal('ventanaVehiculo');
	parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+dwr.util.getValue("idToCotizacion"),'contenido', null);
}

function recargarVentanaVehiculo(idToCotizacion, numeroInciso, cargaCliente){
	closeVehiculo();
	cargaDatosCliente = cargaCliente;
	mostrarDatosVehiculo(idToCotizacion, numeroInciso);
}

function closeVentanaAsegurado(cotizacionId){
	cerrarVentanaModal('ventanaAsegurado');
	sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+cotizacionId,'contenido', null);
}

function mostrarVentanaDatosAdicionalesPaquete(){
	mostrarVentanaCargaControles(dwr.util.getValue("incisoCotizacion.id.idToCotizacion"),dwr.util.getValue("incisoCotizacion.id.numeroInciso"));
}	

function validaRangoSumaAsegurada(obj,min,max,valorDefault,index){
	aplicaRedondeo(index);
	var valor = removeFormatNumber(dwr.util.getValue(obj));
	if(parseFloat(valor) > parseFloat(max)){
		alert("El valor m\u00e1ximo permitido es: " + dwr.util.getValue("valorSumaAseguradaMax_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}else if(parseFloat(valor) < parseFloat(min)){
		alert("El valor m\u00ednimo permitido es: " + dwr.util.getValue("valorSumaAseguradaMin_"+index));
		dwr.util.setValue(obj,valorDefault);
		aplicaRedondeo(index);
	}
}

function aplicaFormatoKeyUp(index){
	jQuery("#valorSumaAseguradaStr_"+index).keyup(function(e) {
		var e = window.event || e;
		var keyUnicode = e.charCode || e.keyCode;
		if (e !== undefined) {
			switch (keyUnicode) {
				case 16: break; // Shift
				case 17: break; // Ctrl
				case 18: break; // Alt
				case 27: this.value = ''; break; // Esc: clear entry
				case 35: break; // End
				case 36: break; // Home
				case 37: break; // cursor left
				case 38: break; // cursor up
				case 39: break; // cursor right
				case 40: break; // cursor down
				case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
				case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
				case 190: break; // .
				default: jQuery("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1 });
			}
		}
	});
}		

function aplicaRedondeo(index){
	jQuery("#valorSumaAseguradaStr_"+index).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
}

function habilitaRegistroCobertura(index){
	if(index!=null){
		if(jQuery("#claveContratoBoolean_"+index).is(":checked")){
			jQuery("#valorSumaAseguradaStr_"+index).removeAttr("readonly");
			jQuery("#valorDeducible_"+index).removeAttr("readonly");
		}else{
			jQuery("#valorSumaAseguradaStr_"+index).attr("readonly","readonly");
			jQuery("#valorDeducible_"+index).attr("readonly","readonly");
		}
	}
}



function mostrarModal(id, text, x, y, width, height, url){
	mostrarVentanaModal(id, text, x, y, width, height, url);
}

function mostrarVentanaCondicionesEspecialesInciso(incisoContinuityId, validoEn, accionEndoso) {
	var url =  "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial/mostrarCondiciones.action" 
		+ "?incisoContinuityId=" + incisoContinuityId + "&accionEndoso=" + accionEndoso;
	
	if (validoEn != null && validoEn != "") {
		url+= "&validoEn=" + validoEn;
	}
	mostrarVentanaModal("condicionEspecial", 'Condiciones Especiales' , 50, 50, 1000, 500, url);	
}

function mostrarIgualacionMasiva(idPoliza, validoEn, tipoEndoso) {
	var url;
	var encabezado = "Igualacion Masiva";
	url = mostrarIgualarMasivoPath + "?polizaId="+idPoliza+"&validoEn="+validoEn+"&tipoEndoso="+tipoEndoso;
	mostrarVentanaModal("igualarMasivo", encabezado , 50, 50, 450, 450, url, null);
}

