/**
 * 
 */

var idContratante = 0;

function cotizar(accion)
{	
	
	var selectedRowsIds = "";
	
	//Se revisan las que se pudieron haber "des-seleccionado"
	var unchecked = "";
	
	if (incisosGrid != null) {
		selectedRowsIds += incisosGrid.getCheckedRows(0);
		
		incisosGrid.forEachRow(function(id){
			if (incisosGrid.cells(id,0).getValue()=="0") unchecked +=id + ",";
			});
		
	}
	
	unchecked += "X";
	
	if (jQuery("#elementosSeleccionados").val() != null && jQuery("#elementosSeleccionados").val() != "") {
		selectedRowsIds += "," + jQuery("#elementosSeleccionados").val();
	}
	
	selectedRowsIds = unchecked + selectedRowsIds;
	
	
	
	/////
	
	sendRequestJQ(null, cotizarActionPath + "?"+ jQuery(document.rehabIncisoForm).serialize() +'&elementosSeleccionados=' + selectedRowsIds+"&accionEndoso="+accion,targetWorkArea,null);
}

function emitir()
{		
	sendRequestJQ(null, emitirActionPath + "?"+ jQuery(document.rehabIncisoForm).serialize(),targetWorkArea,null);
}

function cancelar()
{
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Baja de Incisos?, se perder\u00E1 la configuraci\u00F3n de Baja de Incisos si ya ha definido alguna.";
	if(confirm(mensaje))
	{
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.rehabIncisoForm).serialize(), targetWorkArea,null);				
	}
}