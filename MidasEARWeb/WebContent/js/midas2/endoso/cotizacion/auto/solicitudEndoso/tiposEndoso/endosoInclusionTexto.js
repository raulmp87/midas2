var texAdicionalGrid;
var inclusionTexProcessor;

function iniciaGridTextos() {

	texAdicionalGrid = new dhtmlXGridObject('contenido_textosAdicionalesGrid');
	
	texAdicionalGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/obtenertexAdicionalGrid.action?" 			
								+jQuery(document.endosoInclusionTextoForm).serialize());

	inclusionTexProcessor = new dataProcessor("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/accionTextoAdicional.action?polizaId=" + dwr.util.getValue("polizaId"));
	inclusionTexProcessor.enableDataNames(true);
	//inclusionTexProcessor.sendAllData(true);
	inclusionTexProcessor.setTransactionMode("POST");
	inclusionTexProcessor.setUpdateMode("cell");
	inclusionTexProcessor.setVerificator(1, function(value,row) {
		var valido = true;
		texAdicionalGrid.forEachRow(function(id){ 
			var cellVal = texAdicionalGrid.cellById(id, 1).getValue();
			if(cellVal == value && id != row){
				mostrarVentanaMensaje('20', 'El valor ingresado esta duplicado');
				valido =  false;
				value = null;
				return;
			}
        });
		if (value <= 0) {
			valido =  false;
		}
		return valido;
	})
	
	inclusionTexProcessor.init(texAdicionalGrid);
	
}

function agregarTexto() {
	if (texAdicionalGrid.getRowsNum() ==0 ) {
		texAdicionalGrid.addRow(1,[]);
	} else {
		var secuencia = parseInt(texAdicionalGrid.getRowId(texAdicionalGrid.getRowsNum() - 1)) + 1;
		var valido = true;
		texAdicionalGrid.forEachRow(function(id){ 
			var cellVal = texAdicionalGrid.cellById(id, 1).getValue();
			if(cellVal == ""){
				mostrarVentanaMensaje('20', 'Capture la descripción para todos los textos');
				valido =  false;
				return;
			}
        });
		if (valido) {
			texAdicionalGrid.addRow(secuencia, []);
		}
	}
	habilitaEliminar();
}

function eliminarTexto() {
	if (confirm('\u00BFDesea eliminar el elemento seleccionado?')) {
		//texAdicionalGrid.deleteSelectedItem();
		 var id = texAdicionalGrid.getSelectedRowId();
		 var rowPath = "";
		 rowPath += "biTexAdicionalCot.continuity.id="+texAdicionalGrid.cellById(id, 0).getValue();
		 rowPath += "&biTexAdicionalCot.value.descripcionTexto='"+ texAdicionalGrid.cellById(id, 1).getValue() +"'";
		 rowPath += "&biTexAdicionalCot.value.nombreUsuarioCreacion='"+ texAdicionalGrid.cellById(id, 2).getValue()+"'";
		 rowPath += "&biTexAdicionalCot.value.codigoUsuarioAutorizacion='"+ texAdicionalGrid.cellById(id, 3).getValue()+"'";
		 rowPath += "&biTexAdicionalCot.value.descripcionEstatusAut='"+ texAdicionalGrid.cellById(id, 4).getValue()+"'";
		 rowPath += "&biTexAdicionalCot.value.fechaCreacion="+ texAdicionalGrid.cellById(id, 5).getValue();
		 
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/eliminarTextoAdicional.action?";
		sendRequestJQ(null, path + jQuery(document.endosoInclusionTextoForm).serialize() + "&" + rowPath, targetWorkArea, null);
	}
}

function cotizar(accion) {
	var valido = true;
	var hasRecords = false;
	texAdicionalGrid.forEachRow(function(id){ 
		var cellVal = texAdicionalGrid.cellById(id, 1).getValue();
		hasRecords = true;
		if(cellVal == ""){
			mostrarVentanaMensaje('20', 'Capture la descripci\u00F3n para todos los textos');
			valido =  false;
			return;
		}
    });
	
	if (!hasRecords) {
		mostrarVentanaMensaje('20', 'Capture al menos un texto');
		valido =  false;
		return;
	}
	
	if (valido) {
		var mensaje = "Se requiere de una solicitud de autorizaci\u00F3n para autorizar el \u00F3 los textos adicionales agregados. " +
							"Desea Continúar?";

		if (confirm(mensaje)) {
			texAdicionalGrid.setCSVDelimiter('|');
			
			texAdicionalGrid.enableCSVAutoID(false);
			
			var textos=texAdicionalGrid.serializeToCSV();

			var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/cotizar.action?";
			
			if (accion == 1) {
				sendRequestJQ(null, path + jQuery(document.endosoInclusionTextoForm).serialize() + "&textos=" + textos, "contenido_textos", null);
			} else {
				sendRequestJQ(null, path + jQuery(document.endosoInclusionTextoForm).serialize() + "&textos=" + textos, targetWorkArea, null);
			}
			
			
		}
	}
}

function emitir() {
	var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/emitir.action?";
	sendRequestJQ(null, path + jQuery(document.endosoInclusionTextoForm).serialize(), targetWorkArea, null);

}


function cancelar() {
	var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Inclusi\u00F3n de Textos?, " +
			"se perder\u00E1 la configuraci\u00F3n de Textos si ya ha definido alguna.";
	
	var formSerialized = jQuery(document.endosoInclusionTextoForm).serialize();
	
	if (confirm(mensaje)) {
		var path = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto/cancelar.action?" +  formSerialized;
		sendRequestJQ(null, path, targetWorkArea,null);				
	}
}

function habilitaEliminar() {
	if (texAdicionalGrid.getRowsNum() ==0 ) {
		$('#b_borrar').hide();
	} else {
		$('#b_borrar').show();
	}
	
}
