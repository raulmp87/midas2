/**
 * 
 */

var idContratante = 0;

function cotizar() {	
	var selectedRowsIds = "";
		
	//Se revisan las que se pudieron haber "des-seleccionado"
	var unchecked = "";
	
	if (incisosGrid != null) {
		selectedRowsIds += incisosGrid.getCheckedRows(0);
		
		incisosGrid.forEachRow(function(id){
			if (incisosGrid.cells(id,0).getValue()=="0") unchecked +=id + ",";
			});
		
	}
	
	unchecked += "X";
	
	if (jQuery("#elementosSeleccionados").val() != null && jQuery("#elementosSeleccionados").val() != "") {
		selectedRowsIds += "," + jQuery("#elementosSeleccionados").val();
	}
	
	selectedRowsIds = unchecked + selectedRowsIds;
	
	/////
	
	if (selectedRowsIds == null || selectedRowsIds == "") {
		alert("Favor de seleccionar al menos un inciso");
		return;
	}
	
	sendRequestJQ(null, cotizarActionPath + "?"+ jQuery(document.bajaIncisoForm).serialize() +'&elementosSeleccionados=' + selectedRowsIds,targetWorkArea,null);
}

function emitir() {		
	sendRequestJQ(null, emitirActionPath + "?"+ jQuery(document.bajaIncisoForm).serialize(),targetWorkArea,null);
}

function cancelar(validar) {	
	if (validar) {
		var mensaje = "\u00BFEst\u00E1 seguro que desea cancelar la Baja de Incisos?, se perder\u00E1 la configuraci\u00F3n de Baja de Incisos si ya ha definido alguna.";
		if (confirm(mensaje)) {
			sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.bajaIncisoForm).serialize(), targetWorkArea,null);				
		}
	} else {
		sendRequestJQ(null, cancelarActionPath+"?"+jQuery(document.bajaIncisoForm).serialize(), targetWorkArea,null);	
	}
	
}

function previsualizarCobranza(cotizacionContinuityId, validoEn, accionEndoso, tipoEndoso) {
	var url = previsualizarCobranzaActionPath +  "?cotizacionContinuityId=" + cotizacionContinuityId
									+ "&validoEn=" + validoEn + "&accionEndoso=" + accionEndoso + "&tipoEndoso=" + tipoEndoso;
	mostrarVentanaModal("Cobranza", 'Cobranza', 100, 100, 500, 300, url);

}

/*
function iniciaDefinirSolicitudEndoso()
{
	pageGridPaginadoEmision(1, true);
}

function pageGridPaginadoEmision(page, nuevoFiltro)
{
	var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEmision'+'&divGridPaginar='+'incisosListadoGrid';
	if(nuevoFiltro){
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, solicitudesPaginadasRapidaPath + "?"+ posPath, 'gridIncisosPaginado', 'obtenerSolicitudesEmision();');
}

function obtenerSolicitudesEmision()
{
	document.getElementById("incisosListadoGrid").innerHTML = '';	
	solicitudesPolizaGrid = new dhtmlXGridObject('incisosListadoGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	solicitudesPolizaGrid.load(busquedaRapidaPath + "?" + posPath);
		
}

*/

function mostrarIgualarBajaEndoso(){
	mostrarVentanaModal("igualarPrimas","Igualar Movimientos Endoso",200,320, 710, 320, mostrarIgualarActionPath+ "?"+ jQuery(document.bajaIncisoForm).serialize(),"mostrarEndosoBaja();");
}

function mostrarEndosoBaja(){
	sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso/mostrarBajaInciso.action?"+ jQuery(document.bajaIncisoForm).serialize(),targetWorkArea, null);
}