/**
 * 
 */
var cargaMasivaAltaIncisoGrid;
var cargaMasivaAltaIncisoDetalleGrid;

function iniciaListadoEndosoCargaMsva(){
	var polizaId = jQuery("#polizaId").val();
	document.getElementById("cargasMasivaAltaIncisoGrid").innerHTML = '';
	cargaMasivaAltaIncisoGrid = new dhtmlXGridObject("cargasMasivaAltaIncisoGrid");
	cargaMasivaAltaIncisoGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	cargaMasivaAltaIncisoGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	cargaMasivaAltaIncisoGrid.attachEvent("onXLS", function(grid){
		mostrarIndicadorCarga("indicador");
    });
	cargaMasivaAltaIncisoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });		
	cargaMasivaAltaIncisoGrid.load("/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/listarCargaMasivaAltaInciso.action?polizaId="+ polizaId);
}

function importarArchivoEndosoAI(tipoCarga){
	var polizaId = jQuery("#polizaId").val();
	var fechaIniVigenciaEndoso = jQuery("#fechaIniVigenciaEndoso").val();
	var idToSolicitud = document.getElementById("cotizacion.value.solicitud.idToSolicitud").value;
	var negocioDerechoEndosoId = document.getElementById("cotizacion.value.negocioDerechoEndosoId").value;

	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("cargaMasivaIncisos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Carga Masiva de Incisos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
        			var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
        			sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/procesaCargaMasivaAltaInciso.action?polizaId=" + 
        					polizaId + "&idToControlArchivo=" + idToControlArchivo + "&fechaIniVigenciaEndoso=" + fechaIniVigenciaEndoso + 
        					"&cotizacion.value.solicitud.idToSolicitud=" + idToSolicitud + "&cotizacion.value.negocioDerechoEndosoId=" + negocioDerechoEndosoId, "contenido", null);
    			}
    		} // End of onSuccess
    	});
        parent.dhxWins.window("cargaMasivaIncisos").close();
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }

    vault.create("vault");
    vault.setFormField("claveTipo", "40");
}

function descargarCargaMasivaEndosoAI(idToControlArchivo) {
	window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=' + idToControlArchivo, 'download');
}

function descargarLogCargaMasivaEndosoAI() {
	if(jQuery("#logErrors").val() == 'true'){
		var polizaId = jQuery("#polizaId").val();
		var idToControlArchivo = jQuery("#idToControlArchivo").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarLogErrores.action?polizaId=" + polizaId + "&idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT" + polizaId);
	}
}

function descargarLogCargaMasivaEndosoAICarga(idToControlArchivo) {
		var polizaId = jQuery("#polizaId").val();
		var location ="/MidasWeb/suscripcion/cotizacion/auto/cargamasiva/descargarLogErrores.action?polizaId=" + polizaId + "&idToControlArchivo=" + idToControlArchivo;
		window.open(location, "Cotizacion_COT" + polizaId);
}

function descargarPlantillaCargaEndosoAI(){
	var polizaId = jQuery("#polizaId").val();
	var location ="/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/descargarPlantillaCargaMsva.action?polizaId=" + polizaId;
	window.open(location, "Poliza_" + polizaId);
}


function regresarACotizacionEndosoAI(tipoRegreso){
	var polizaId = jQuery("#polizaId").val();
	var fechaIniVigenciaEndoso = jQuery("#fechaIniVigenciaEndoso").val();
	var accionEndoso = jQuery("#accionEndoso").val();

	sendRequestJQ(null, "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso/mostrarAltaInciso.action?polizaId=" + polizaId + "&fechaIniVigenciaEndoso="+ fechaIniVigenciaEndoso + "&accionEndoso="+accionEndoso, "contenido", null);
	
}