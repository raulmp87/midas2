/**
 * 
 */

function cancelar() {
	var polizaId = dwr.util.getValue("polizaId");
	var mensaje = "\u00BFEst\u00E1 seguro que desea abandonar la cotizaci\u00F3n?, se perder\u00E1n los cambios realizados.";
	if(confirm(mensaje)) {
		sendRequestJQ(null, cancelarActionPath + "?polizaId=" +polizaId, targetWorkArea,null);				
	}
}

function emitir() {

	sendRequestJQ(null, emitirActionPath+"?"+jQuery(document.solicitudPolizaForm).serialize(), targetWorkArea, null);		
}


function cotizar() {
	 var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
	 var numeroEndoso = getNumeroEndoso();
	 if (fechaIniVigenciaEndoso != '' && numeroEndoso != -1) {
		 sendRequestJQ(null, cotizarActionPath + "?" + jQuery(document.solicitudPolizaForm).serialize() + 
					"&numeroEndoso=" + numeroEndoso, targetWorkArea, null);	
	 } else {
			mostrarMensajeInformativo("Favor de seleccionar los campos obligatorios.", "20", null);
	 }		
}


function iniciaLlenadoEndosos() {
	pageGridPaginadoEndosos(1, true);
}

function pageGridPaginadoEndosos(page, nuevoFiltro) {
	var polizaId = dwr.util.getValue("polizaId");
	var cotizacionContinuityId = dwr.util.getValue("cotizacionContinuityId");
	var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
	var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEndosos'+'&divGridPaginar='+'cotizacionEndososListadoGrid&'
		+ "cotizacionContinuityId=" + cotizacionContinuityId + "&polizaId=" + polizaId + "&fechaIniVigenciaEndoso=" + fechaIniVigenciaEndoso;

	if (nuevoFiltro) {
	} else {
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}
	sendRequestJQ(null, obtenerEndososPaginadasPath + "?"+ posPath, 'gridEndososListadoPaginado', 'obtenerEndosos();');
}

function obtenerEndosos() {
	var polizaId = dwr.util.getValue("polizaId");
	var cotizacionContinuityId = dwr.util.getValue("cotizacionContinuityId");
	var fechaIniVigenciaEndoso = dwr.util.getValue("fechaIniVigenciaEndoso");
	
	document.getElementById("cotizacionEndososListadoGrid").innerHTML = '';	
	endososGrid = new dhtmlXGridObject('cotizacionEndososListadoGrid');
	mostrarIndicadorCarga('indicador');	
	endososGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
	});	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize() + '&'
		+ "cotizacionContinuityId=" + cotizacionContinuityId + "&polizaId=" + polizaId + "&fechaIniVigenciaEndoso=" + fechaIniVigenciaEndoso;

	endososGrid.load(obtenerEndososPath + "?" + posPath);
	
	var accionE = dwr.util.getValue("accionEndoso"); 
	if(accionE == "3")//Solo Consulta
	{
		endososGrid.attachEvent("onRowCreated",function(id){
		     var cell = endososGrid.cells(id,1); //checkbox cell
		     if (cell.getAttribute("disabled")) cell.setDisabled(true);
		})	    	
	}
}

function getNumeroEndoso() {
	var rowsIdSel = endososGrid.getCheckedRows(1);
	if (rowsIdSel == null) {
		return -1;
	} else {
		var numeroEndoso = endososGrid.cells(rowsIdSel, 2).getValue();
		return numeroEndoso;
	}
}

function previsualizarCobranza(cotizacionContinuityId, validoEn, accionEndoso, tipoEndoso) {
	var url = previsualizarCobranzaActionPath +  "?cotizacionContinuityId=" + cotizacionContinuityId
									+ "&validoEn=" + validoEn + "&accionEndoso=" + accionEndoso + "&tipoEndoso=" + tipoEndoso;
	mostrarVentanaModal("Cobranza", 'Cobranza', 100, 100, 500, 300, url);

}