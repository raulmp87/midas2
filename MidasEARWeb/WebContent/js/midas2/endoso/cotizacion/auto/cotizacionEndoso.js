var selectValue = 0;
var selectDescripcion = "SELECCIONE...";
var solicitudesPolizaGrid;
var ventanaRechazo;
var ventanaAsignacion;
var dhxWins= new dhtmlXWindows();
dhxWins.enableAutoViewport(true);
dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
var paginadoParamsPath;
var currentNamespace = '/endoso/cotizacion/auto';
var currentMainAction = 'listar';

function opcionCaptura(opcion,id){
	var script = '';
	var path = opcionCapturaPath +"?seleccion="+opcion+"&claveNegocio="+dwr.util.getValue("claveNegocio");
	if(id != null && id != ''){
		path += "&id="+id;
		script = "obtenerDatosAgente(jQuery('#idTcAgente').val())"
	}
	sendRequestJQ(null, path ,'contenido', script);	
}
function enConstruccion(){
	alert("En construccion");
}

function limpiar(){
	jQuery('#cotizacionEndosoForm').each (function(){
		  this.reset();
	});
}

function pageGridPaginado(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'pageGridPaginado'+'&divGridPaginar='+'cotizacionEndosoListadoGrid';
	if(nuevoFiltro){
		paginadoParamsPath = jQuery(document.cotizacionEndosoForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'obtenerSolicitudes();selectGridSize();';
	
	sendRequestJQTarifa(null, solicitudesPaginadasPath + "?filtrar=true" + "&" + paginadoParamsPath + posPath, 'gridSolicitudesPaginado', nextFunc);
}

function obtenerSolicitudes(){
	document.getElementById("cotizacionEndosoListadoGrid").innerHTML = '';
	solicitudesPolizaGrid = new dhtmlXGridObject('cotizacionEndosoListadoGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	solicitudesPolizaGrid.load(buscarSolicitudPath + "?filtrar=true" + "&" + paginadoParamsPath + '&' +posPath);
}


function pageGridPaginadoEmision(page, nuevoFiltro){
	var posPath = 'posActual='+page+'&funcionPaginar='+'pageGridPaginadoEmision'+'&divGridPaginar='+'cotizacionEndosoListadoGrid';
	if(nuevoFiltro){
	}else{
		posPath = 'posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	sendRequestJQTarifa(null, solicitudesPaginadasRapidaPath + "?"+ posPath, 'gridSolicitudesPaginado', 'obtenerSolicitudesEmision();');
}

function obtenerSolicitudesEmision(){
	document.getElementById("cotizacionEndosoListadoGrid").innerHTML = '';	
	solicitudesPolizaGrid = new dhtmlXGridObject('cotizacionEndosoListadoGrid');
	mostrarIndicadorCarga('indicador');	
	solicitudesPolizaGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });	
	var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
	solicitudesPolizaGrid.load(busquedaRapidaPath + "?" + posPath);
		
}

function refrescarGridSolicitudPoliza(sid, action, tid, node){
	obtenerSolicitudes();
	return true;
}

function initOficina(){
	listadoService.getMapEjecutivosPorGerencia(null, function(data){
		var combo = document.getElementById('codigoEjecutivo');
		addOptions(combo,data);
	});
}

function obtieneProductos(id){
	listadoService.getMapProductos(id,1,function(data){
		var combo = document.getElementById('idToProducto');
		addOptions(combo,data);
	});	
}


function obtenerDatosAgente(id){
	if(id != null && id != ''){
		transporteService.obtenerDatosAgente(id, {
		callback:function(agente){
			if(agente != null){
				jQuery('#codigoEjecutivo').val(agente.promotoria.ejecutivo.id);
				dwr.util.addOptions(document.getElementById('codigoAgente'),[{id: agente.id, value:agente.persona.nombreCompleto}],"id", "value");
				jQuery('#codigoAgente').val(agente.id);
				if(agente.promotoria.ejecutivo.gerencia != null){
					jQuery('#nombreOficina').val(agente.promotoria.ejecutivo.gerencia.descripcion);
					jQuery('#idOficina').val(agente.promotoria.ejecutivo.gerencia.id);
				}
				jQuery('#nombreOficinaAgente').val(agente.promotoria.descripcion);
			}else{				
				removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], 
						selectValue, 
						selectDescripcion);
			}
		},
		errorHandler:function(message){console.log(message);},
		exceptionHandler:function(errorString, exception){console.log(errorString);}
		});		
	}
}

function obtieneAgentes(id){
	if(id!=null&&id!=''){
		listadoService.getMapAgentesPorGerenciaOficinaPromotoria(null, dwr.util.getValue("codigoEjecutivo"), null, function(data){
			var combo = jQuery('#codigoAgente')[0];
			addOptions(combo,data);
		});
	}else{
		removeAllOptionsAndSetHeader(jQuery('#codigoAgente')[0], 
				selectValue, 
				selectDescripcion);
	}
}

function enviar(tipoAccion) {
	var valido = true;
	var msg = "<ul>";
	if(jQuery("#idToProducto").val() == ''){
		msg+="<li>Seleccione un producto.</li>"
		valido = false;
	}
	if(jQuery.trim(jQuery("#codigoAgente").val()) == ''){
		msg+="<li>Ingrese Clave del Agente o seleccione un agente a trav&eacute;s de los combos de selecci&oacute;n</li>"
		valido = false;
	}
	if(jQuery('#razonSocialText').is(':visible')){
		if(jQuery.trim(jQuery("#razonSocialText").val()) == '' && valido){
			msg+="<li>Por favor ingrese La Razon Social.</li>";
			valido = false;
		}
	}
	
	if(!jQuery('#nombrePersona').attr('disabled')){
		if(jQuery.trim(jQuery("#nombrePersona").val()) == '' || jQuery.trim(jQuery("#apellidoPaterno").val()) == '' || jQuery.trim(jQuery("#apellidoMaterno").val()) == ''){
		msg+="<li>Ingrese Nombre completo del Cliente.</li>"
		valido = false;
		}
	}
	
	if(jQuery.trim(jQuery("#tiposSolicitud").val()) == ''){
		msg+="<li>Ingrese Tipo de Movimiento.</li>"
		valido = false;
	}
	if(!valido){
		msg+="</ul>";
		mostrarVentanaMensaje('10', msg, null);
	}
		
	if(valido){
		sendRequestJQAsync(null, "/MidasWeb/suscripcion/solicitud/agregarSolicitud.action?"
			+ jQuery(document.cotizacionEndosoForm).serialize(),'contenido',null);
	}
}
//Nombre del div = cotizacionesEndososGrid
function iniciaSolicitudPoliza(){	
	//document.getElementById("solicitudesPolizaGrid").innerHTML = '';
	//solicitudesPolizaGrid = new dhtmlXGridObject('solicitudesPolizaGrid');
	//refrescarGridSolicitudPoliza(null, null, null, null);
//initOficina();
	//obtenerSolicitudesEmision();
	pageGridPaginadoEmision(1, true);
}



function mostrarVentanaAsignarSolicitud(idSolicitud,onCloseFunction){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaAsignacion = parent.dhxWins.createWindow("asignar", 200, 320, 550, 200);
	parent.ventanaAsignacion.setText("Asignar Solicitud");
	parent.ventanaAsignacion.center();
	parent.ventanaAsignacion.setModal(true);
	parent.ventanaAsignacion.attachEvent("onClose", onCloseFunction);
	parent.ventanaAsignacion.attachURL("/MidasWeb/suscripcion/solicitud/asignar/mostrarVentana.action?id="+idSolicitud);
	
	parent.ventanaAsignacion.button("minmax1").hide();	
}

function closeAsignarSolicitud(){
	parent.dhxWins.window('asignar').setModal(false);
	parent.dhxWins.window('asignar').hide();
	parent.ventanaAsignacion=null;
	iniciaSolicitudPoliza();
}

function mostrarVentanaRechazar(idSolicitud,onCloseFunction){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaRechazo = parent.dhxWins.createWindow("rechazar", 200, 320, 550, 200);
	parent.ventanaRechazo.setText("Rechazar Solicitud");
	parent.ventanaRechazo.center();
	parent.ventanaRechazo.setModal(true);
	parent.ventanaRechazo.attachEvent("onClose", onCloseFunction);
	parent.ventanaRechazo.attachURL("/MidasWeb/suscripcion/solicitud/rechazar/mostrarVentana.action?id="+idSolicitud);
	
	parent.ventanaRechazo.button("minmax1").hide();	
}

function verNuevaSolicitudEndoso()
{
	sendRequestJQ(null,definirTipoModificacionPath+'?namespaceOrigen='+currentNamespace+'&actionNameOrigen='+currentMainAction,targetWorkArea,null);

}

function editarCotizacion(idPoliza,idTipoEndoso,accionEndoso,fechaIniVigenciaEndoso)
{
	sendRequestJQ(null,'/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/verDetalleTipoEndoso.action?tipoEndoso=' + idTipoEndoso +'&idPolizaBusqueda='+idPoliza+'&fechaIniVigenciaEndoso='+fechaIniVigenciaEndoso+'&accionEndoso=' + accionEndoso + '&namespaceOrigen=' + currentNamespace + '&actionNameOrigen=' + currentMainAction,targetWorkArea,null);
}

function cancelarCotizacion(idCotizacionContinuity, numeroCotizacion)
{
	var mensaje = "\u00BFEst\u00E1 Seguro que desea Cancelar la Cotizaci\u00F3n de Endoso " + numeroCotizacion + " ?";
	if(confirm(mensaje))
	{		
		sendRequestJQ(null,cancelarCotizacionPath+'?namespaceOrigen='+currentNamespace+'&actionNameOrigen='+currentMainAction + '&cotizacion.continuity.id=' + idCotizacionContinuity,targetWorkArea,null);
	}
}

function emitirEndoso(id, polizaId, fechaIniVigenciaEndoso, accionEndoso, idTipoEndoso)
{
	var actionNameOrigen = "mostrarDefTipoModificacion";
	var namespaceOrigen = "/endoso/cotizacion/auto/solicitudEndoso";
	var path = null;
	var extraParam = "";
	if(idTipoEndoso === 5){
		path = emitirAltaIncisoPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 6){
		path = emitirBajaIncisoPath;
	}
	if(idTipoEndoso === 7){
		path = emitirCambioDatosPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 8){
		path = emitirCambioAgentePath;
		extraParam = "&cotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 9){
		path = emitirCambioFormaPagoPath;
		extraParam = "&cotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 10){
		path = emitirEndosoCancelacionEndosoPath;
		extraParam = "&cotizacionContinuityId="+id;
	}
	if(idTipoEndoso === 11){
		path = emitirEndosoCancelacionPolizaPath;
	}
	if(idTipoEndoso === 12){
		path = emitirExtensionVigenciaPath;
	}
	if(idTipoEndoso === 13){
		path = emitirInclusionAnexoPath;
		extraParam = "&biCotizacion.continuity.id="+id+"&biCotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 14){
		path = emitirInclusionTextoPath;
		extraParam = "&biCotizacion.continuity.id="+id+"&biCotizacion.value.fechaInicioVigencia="+fechaIniVigenciaEndoso;
	}
	if(idTipoEndoso === 15){
		path = emitirMovimientosPath;
	}
	if(idTipoEndoso === 16){
		path = emitirRehabilitacionIncisoPath;
		extraParam = "&actionNameOrigen="+actionNameOrigen+"&namespaceOrigen="+namespaceOrigen;
	}
	if(idTipoEndoso === 17){
		path = emitirRehabilitacionEndosoPath;
		extraParam = "&cotizacionContinuityId="+id;
	}
	if(idTipoEndoso === 18){
		path = emitirRehabilitacionPolizaPath;
	}
	if(path != null){
		if(confirm('\u00BFSolicitar Emisi\u00F3n?')){
			sendRequestJQ(null, path + "?cotizacion.continuity.id="+id+"&polizaId="+polizaId+"&fechaIniVigenciaEndoso="+fechaIniVigenciaEndoso+"&accionEndoso="+accionEndoso+extraParam,targetWorkArea,null);
		}
	}
	
}

function emitirCotizacion(idPoliza,idTipoEndoso,accionEndoso,fechaIniVigenciaEndoso)
{
	// Agregar el codigo que ejecuta la accion de Emitir Endoso
}

function imprimirCotizacion(idPoliza,idTipoEndoso,accionEndoso,fechaIniVigenciaEndoso)
{
	// Agregar el codigo que ejecuta la accion de Imprimir Endoso
}



function displayFilterSolicitud(){
	jQuery('#contenedorFiltros').toggle('fast', function() {		
		if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
			jQuery('#mostrarFiltros').html('Mostrar Filtros')
			sizeGridSolicitud();
		} else {
			jQuery('#mostrarFiltros').html('Ocultar Filtros');
			reSizeGridSolicitud();
		}
	})
}



function sizeGridSolicitud(){
	jQuery(document).ready(function(){
		jQuery('#cotizacionEndosoListadoGrid').css('height','300px');
		jQuery('div[class=objbox]').css('height','275px');
	})
}

function reSizeGridSolicitud(){
	jQuery(document).ready(function(){
		jQuery('#cotizacionEndosoListadoGrid').css('height','130px');
		jQuery('div[class=objbox]').css('height','105px');
	})
}

function selectGridSize(){
	
	if (jQuery('#contenedorFiltros')[0].style.display == 'none') {
		jQuery('#mostrarFiltros').html('Mostrar Filtros')
		sizeGridSolicitud();
	} else {
		jQuery('#mostrarFiltros').html('Ocultar Filtros');
		reSizeGridSolicitud();
	}	
}

function pageGridPaginadoBtnBuscar(page, nuevoFiltro){
	var posPath = '&posActual='+page+'&funcionPaginar='+'pageGridPaginado'+'&divGridPaginar='+'cotizacionEndosoListadoGrid';
	if(nuevoFiltro){
		paginadoParamsPath = jQuery(document.cotizacionEndosoForm).serialize();
	}else{
		posPath = '&posActual='+page+'&' + jQuery(document.paginadoGridForm).serialize();
	}	
	
	var nextFunc = 'obtenerSolicitudes();displayFilterSolicitud();';
	
	sendRequestJQTarifa(null, solicitudesPaginadasPath + "?filtrar=true" + "&" + paginadoParamsPath + posPath, 'gridSolicitudesPaginado', nextFunc);
}
