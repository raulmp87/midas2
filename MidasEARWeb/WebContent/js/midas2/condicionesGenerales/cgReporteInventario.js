/****
 * 
 * 
 * *****/


function obtenerReporteInventario(){
	
	var idCentro=jQuery("#idCentro").val();
	
	if ( idCentro == null || idCentro == "" ){
		
		mostrarMensajeInformativo("Elija un centro emisor.", 10, null);
	}
	else{
		
		var location ="/MidasWeb/condicionesGenerales/obtenerReporteInventario.action?"+"idToCentro="+idCentro;
		window.open(location, "Reporte de Inventario por Centro Emisor");		
	}
}

function mostrarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='block';
	}
}

function ocultarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='none';
	}
}