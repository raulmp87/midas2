/****
 * 
 * 
 * *****/

function traerListaOrden(){
	document.getElementById("cgOrdenGrid").innerHTML = '';
	var cgOrdenGrid = new dhtmlXGridObject('cgOrdenGrid');
	mostrarIndicadorCarga('indicador');	
	cgOrdenGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		mostrarGrid('cgOrdenGrid');
    });	
	
	var url = "/MidasWeb/condicionesGenerales/traerListaOrden.action";
	cgOrdenGrid.load(url+"?"+jQuery("#cgOrdenForm").serialize());
}

function mostrarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='block';
	}
}

function ocultarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='none';
	}
}

function confirmarCgOrden(idToOrden){
	mostrarMensajeConfirm('Se confirmará la orden elegida, ¿Desea continuar?', '20', 
			'guardarCondicionConfirm('+ idToOrden +')', null, null);
	
}

function crearCgOrden(){
	var url="/MidasWeb/condicionesGenerales/editarOrden.action";
	sendRequestJQ(null,url,'contenido',null);
}

function irInicioOrden(){
	
	var url = "/MidasWeb/condicionesGenerales/listarOrden.action";
	sendRequestJQ(null,url,'contenido',null);
}

function actualizarGuardarOrden(){
	
	if ( this.validar() ){		
		var form=jQuery("#cgOrdenForm");
		var url="/MidasWeb/condicionesGenerales/actualizarGuardarOrden.action";
		sendRequestJQ(form,url,'contenido',null);		
	}
}

function validar(){
	
	var esAgente = document.getElementById("esAgente").value;
	var cantidad = document.getElementById("cgOrden.cantidad").value;
	var fecha = document.getElementById("cgOrden.fecha").value;
	var motivos = document.getElementById("cgOrden.motivos").value;
	var centro = document.getElementById("cgOrden.tocgcentroId").value;
	var noPasa = false;
	
	if ( esAgente != "true" && ( centro == null || centro == '' ) ){
		noPasa = true;
	}
	
	if ( cantidad == null || cantidad == '' || fecha == null || fecha == '' || motivos == null || motivos == '' || noPasa ){
		
		mostrarMensajeInformativo("Alguno de los campos está vacío. Verificar.", 10, null);
		return false;
	}
	
	return true;
}

function guardarCondicionConfirm(idToOrden){
	
	jQuery.ajax({
		  type     : 'POST',
		  url      : '/MidasWeb/condicionesGenerales/confirmarOrden.action',
		  data     : {idToOrden:idToOrden},
		  dataType : 'json',
		  async    : true,
		  success  : function(data){
			  traerListaOrden();
			  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
		  }
	});
}
