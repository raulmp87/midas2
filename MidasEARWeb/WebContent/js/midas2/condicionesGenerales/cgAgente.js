/****
 * 
 * 
 * *****/


function traerListaAgente(){
	document.getElementById("cgAgenteGrid").innerHTML = '';
	var cgAgenteGrid = new dhtmlXGridObject('cgAgenteGrid');
	mostrarIndicadorCarga('indicador');	
	cgAgenteGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		mostrarGrid('cgAgenteGrid');
    });	
	
	var url = "/MidasWeb/condicionesGenerales/traerListaAgente.action";
	cgAgenteGrid.load(url+"?"+jQuery("#cgAgenteForm").serialize());
}

function mostrarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='block';
	}
}

function ocultarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='none';
	}
}

function editarCgAgente(idToAgente){
	var url="/MidasWeb/condicionesGenerales/editarAgente.action"+"?"+"idToAgente="+idToAgente+"&"+jQuery("#cgAgenteForm").serialize();
	sendRequestJQ(null,url,'contenido',null);
}

function actualizarCgAgente(){
	
	if (this.validar()){		
		var form=jQuery("#cgAgenteForm");
		var url="/MidasWeb/condicionesGenerales/actualizarAgente.action";
		sendRequestJQ(form,url,'contenido',null);		
	}
}

function eliminarCgAgente(idToAgente){	
	mostrarMensajeConfirm('¿Seguro que desear eliminar al agente de condiciones generales elegido?', '20', 
			'eliminarCgAgenteDo('+ idToAgente +')', null, null);
}

function eliminarCgAgenteDo(idToAgente){
	
	jQuery.ajax({
		  type     : 'POST',
		  url      : '/MidasWeb/condicionesGenerales/eliminarAgente.action',
		  data     : {idToAgente:idToAgente},
		  dataType : 'json',
		  async    : true,
		  success  : function(data){
			  traerListaAgente();
			  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
		  }
	});
}

function validar(){
	
	return true;
}

function irInicioAgente(){
	
	var url = "/MidasWeb/condicionesGenerales/listarAgente.action";
	sendRequestJQ(null,url,'contenido',null);
}

function onChangeGerencia(target,target2,value){
	var idgerencia = value;
	if(idgerencia != null  && idgerencia != headerValue){
		listadoService.getMapEjecutivosPorGerencia(idgerencia,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});
	}else{
		addOptions(target,null);
	}
	var idEjecutivo = jQuery("#idEjecutivoCatalogo option:selected").val();
	if(idEjecutivo!=''){		
		addOptions(target2,null);
	}
}

function onChangeEjecutivo(target,value){
	var idEjecutivo = value;
	if(idEjecutivo != null  && idEjecutivo != headerValue){
		listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo,
				function(data){
					addOptions(target,data);
					addToolTipOnchange(target);
				});	
	}else{
		addOptions(target,null);
	}
}

/****   componentedireccion   ****/
function onChangePais(target,targetCiudad,targetColonia,targetCP,targetCalleNumero, paisSelect){
	var idPais = dwr.util.getValue(jQuery(paisSelect).selector);
	
	if(idPais != null  && idPais != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapEstados(idPais,
				function(data){
					addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}	
	if(targetCiudad){
		addOptionsDireccion(targetCiudad,"");
	}
	if(targetColonia){
		addOptionsDireccion(targetColonia,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}
/**
 * Funcion que pinta en el jsp los elemntos de un combo box
 * ordenados por el texto de la opcion y no por el value
 * @param ele
 * @param data
 */
function addOptionOderByElement(ele, data) {
	  dwr.util.removeAllOptions(jQuery(ele).selector);
	  ele = dwr.util._getElementById(ele, "addOptions()");
	  if (ele == null) return;
	  var useOptions = dwr.util._isHTMLElement(ele, "select");
	  if (!useOptions) {
	    dwr.util._debug("addOptions() can only be used with select elements. Attempt to use: " + dwr.util._detailedTypeOf(ele));
	    return;
	  }
	  if (data == null) return;
	  
	  var options = {}; 
	  var orderItem=new Array();
	  if (!options.optionCreator && useOptions) options.optionCreator = dwr.util._defaultOptionCreator;
	  options.document = ele.ownerDocument;

	    if (!useOptions) {
	      dwr.util._debug("dwr.util.addOptions can only create select lists from objects.");
	      return;
	    }
	    for (var prop in data) {
	      if (typeof data[prop] == "function") continue;
	      	options.data = data[prop];
	        options.value = prop;
	        options.text = data[prop];
	      if (options.text != null || options.value) {
	    	  orderItem[orderItem.length]=new Array(options.text,options.value);
	      }
	    }
	    orderItem.sort(function compara(a, b) {
	        return (a[0]<b[0]?"-1":"1");
	    });
	    
	    var opt = "";
	    var i = 0;
	    for(i; i<orderItem.length; i++){
	    	if(i==0){
	    		opt = options.optionCreator(options);
		    	opt.text = "seleccione...";
		        opt.value = "";
		        ele.options[ele.options.length] = opt;
	    	}
	    	opt = options.optionCreator(options);
	    	opt.text = orderItem[i][0];
	        opt.value = orderItem[i][1];
	        ele.options[ele.options.length] = opt;
	    }
	  // All error routes through this function result in a return, so highlight now
	  dwr.util.highlight(ele, options); 
	}

function onChangeEstadoGeneral(target,targetColonia,targetCP,targetCalleNumero,estadoSelect){
	var idEstado = dwr.util.getValue(jQuery(estadoSelect).selector);
	if(idEstado != null  && idEstado != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapMunicipiosPorEstado(idEstado,
				function(data){
//					addOptions(target,data);
					addOptionOderByElement(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}
	
	if(targetColonia){
		addOptionsDireccion(targetColonia,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeCiudad(target,targetCP,targetCalleNumero,ciudadSelect){
	var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
	if(idCiudad != null  && idCiudad != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapColoniasSameValue(idCiudad,
				function(data){
			addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeColonia(target,targetCalleNumero, coloniaSelect,ciudadSelect){

	var idColonia = dwr.util.getValue(jQuery(coloniaSelect).selector);
	var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
	if(idColonia != null  && idColonia != headerValue){		
		dwr.engine.beginBatch();
		listadoService.getCodigoPostalByColonyNameAndCityId(idColonia,idCiudad,
				function(data){
					dwr.util.setValue(target, data);
				});
		dwr.engine.endBatch({async:false});
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeCodigoPostal(value,targetColonia,targetMunicipio,targetIdEstado,targetCalleNumero,targetPais,targetCP){
	if (dwr.util.getValue(targetPais)==""){
		dwr.util.setValue(targetPais,'MEXICO');
		onChangeCP_pais(targetIdEstado);
	}
	var idColonia = dwr.util.getValue(targetColonia);
	var idCiudad = dwr.util.getValue(targetMunicipio);
	var idEstado = dwr.util.getValue(targetIdEstado);
	//Verificamos si alguno de los combos antes del codigo postal no esta completo, si es asi, entonces
	//se lanza la funcion de llenar la direccion por cp
	//if((!jQuery.isValid(idColonia) || !jQuery.isValid(idCiudad) || !jQuery.isValid(idEstado)) && jQuery.isValid(value)){
		getNameColoniasPorCP(value,targetColonia ,targetMunicipio,targetIdEstado);
	//}
	
	//si no encuentra direccion por codigo postal se resetea el estado a "seleccione"
	//se agregarn las css class jQ_ciudad y jQ_estado para la validacion
	var ciudadActual=dwr.util.getValue(targetMunicipio);
	if(!jQuery.isValid(ciudadActual)){
		dwr.util.setValue(targetIdEstado,"")
		dwr.util.setValue(targetCP,"");
	}else{
	dwr.util.setValue(targetCP,value);
	}
	dwr.util.setValue(targetCalleNumero,"");	
}

function onChangeCP_pais(target){
		dwr.engine.beginBatch();
		listadoService.getMapEstados("PAMEXI",
				function(data){
					addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
}

function onChangeNuevaColonia(nuevaColoniaId,coloniaId,idCheckbox){
	var checked=document.getElementById(idCheckbox).checked;
	document.getElementById(nuevaColoniaId).disabled=!checked;
	document.getElementById(coloniaId).disabled=checked;
	if(checked){
		jQuery(document.getElementById(nuevaColoniaId)).addClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).removeClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).val("");
	}else{
		jQuery(document.getElementById(nuevaColoniaId)).val("");
		jQuery(document.getElementById(nuevaColoniaId)).removeClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).addClass("jQrequired");
	}
	
	('jQrequired');
}
function addOptionsDireccion(target, map) {
	dwr.util.removeAllOptions(jQuery(target).selector);
	addSelectHeader(target);
	dwr.util.addOptions(jQuery(target).selector, map);
}

/**** cargar excel ****/


function cargarExcelActualizarDatos(){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("documentoActualizarDatos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Excel Actualizar Datos Agentes");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == 1){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    				
    				//parent.mostrarErrores(idToControlArchivo);
    				parent.mostrarMensajes(idToControlArchivo);
    			}
    		} // End of onSuccess    		
    	});
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }
    vault.create("vault");
    vault.setFormField("claveTipo", 22);
}

function mostrarMensajes(idToControlArchivo){
	var url="/MidasWeb/condicionesGenerales/actualizarDatos.action"+"?"+"idToControlArchivo="+idToControlArchivo;
	sendRequestJQ(null,url,'contenido',null);
}

function descargarLayout(){
	
	var location ="/MidasWeb/condicionesGenerales/descargarLayout.action";
	window.open(location, "Layout Agentes Condiciones Generales");

}

/*****  *******/



