/****
 * 
 * 
 * *****/
var cgProveedorGrid;
 
function traerListaProveedor(){
	document.getElementById("cgProveedorGrid").innerHTML = '';
	cgProveedorGrid = new dhtmlXGridObject('cgProveedorGrid');
	mostrarIndicadorCarga('indicador');	
	cgProveedorGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		mostrarGrid('cgProveedorGrid');
    });	
	
	var url = "/MidasWeb/condicionesGenerales/traerListaProveedor.action";
	cgProveedorGrid.load(url+"?"+jQuery("#cgPform").serialize());
}

function mostrarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='block';
	}
}

function ocultarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='none';
	}
}

function crearCgProveedor(idCgProveedor){
	var url="/MidasWeb/condicionesGenerales/crearProveedor.action";
	sendRequestJQ(null,url,'contenido',null);
}

function guardarCgProveedor(){
	
	if (this.validar()){		
		var form=jQuery("#cgPform");
		var url="/MidasWeb/condicionesGenerales/guardarProveedor.action";
		sendRequestJQ(form,url,'contenido',null);		
	}
}

function editarCgProveedor(idCgProveedor){
	var url="/MidasWeb/condicionesGenerales/editarProveedor.action"+"?"+"idCgProveedor="+idCgProveedor;
	sendRequestJQ(null,url,'contenido',null);
}

function eliminarCgProveedor(idCgProveedor){	
	mostrarMensajeConfirm('¿Seguro que desea eliminar el Proveedor de Condiciones Generales?', '20', 
			'eliminarCgProveedorDo('+ idCgProveedor +')', null, null);
}

function eliminarCgProveedorDo(idCgProveedor){	
	var url="/MidasWeb/condicionesGenerales/eliminarProveedor.action"+"?"+"idCgProveedor="+idCgProveedor;
	sendRequestJQ(null,url,'contenido',null);
}

function irInicioProveedor(){
	
	var url = "/MidasWeb/condicionesGenerales/listarProveedor.action";
	sendRequestJQ(null,url,'contenido',null);
}

function validar(){
	
	var proveedor = document.getElementById("cgProveedor.tcprestadorservicioId").value;
	var contacto = document.getElementById("cgProveedor.contacto").value;
	var observaciones = document.getElementById("cgProveedor.observaciones").value;
	
	if ( proveedor != null && proveedor != '' ){
		if ( contacto != null && contacto != '' ){			
			return true;			
		}
		else{
			mostrarMensajeInformativo("El contacto no puede estar vacío.", 10, null);
		}
	}
	else{
		mostrarMensajeInformativo("Elija un proveedor.", 10, null);
	}
	return false;
}

function cargarlistaCentro(){
	var idProveedor = document.getElementById("idCgProveedor").value;
	document.getElementById("listCentroEmisorGrid").innerHTML = '';
	var centroEmisorGrid = new dhtmlXGridObject('listCentroEmisorGrid');
	mostrarIndicadorCarga('indicador');	
	centroEmisorGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		mostrarGrid('listCentroEmisorGrid');
    });	
	
	var url = "/MidasWeb/condicionesGenerales/traerListaCProveedor.action";
	centroEmisorGrid.load(url+"?idCgProveedor="+idProveedor);
}

function eliminarCentroProveedor(idToCentro){	
	mostrarMensajeConfirm('¿Seguro que desea eliminar el Centro Emisor para este Proveedor?', '20', 
			'eliminarCentroProveedorDo('+ idToCentro +')', null, null);
}

function eliminarCentroProveedorDo(idToCentro){
	
	var idProveedor = document.getElementById("idCgProveedor").value;
	
	jQuery.ajax({
		  type     : 'POST',
		  url      : '/MidasWeb/condicionesGenerales/eliminarCentroProveedor.action',
		  data     : {idToCentro:idToCentro, idCgProveedor:idProveedor},
		  dataType : 'json',
		  async    : true,
		  success  : function(data){
			  cargarlistaCentro();
			  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
		  }
	});
}

function guardarCentroProveedor(){
	
	var idProveedor = document.getElementById("idCgProveedor").value;
	var idToCentro = document.getElementById("idToCentro").value;
	
	if (this.validarGuardarCentro(idToCentro ) ){
	
		jQuery.ajax({
			  type     : 'POST',
			  url      : '/MidasWeb/condicionesGenerales/guardarCentroProveedor.action',
			  data     : {idToCentro:idToCentro, idCgProveedor:idProveedor},
			  dataType : 'json',
			  async    : true,
			  success  : function(data){
				  cargarlistaCentro();
				  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
			  }
		});
	}
}

function validarGuardarCentro( idToCentro ){
	
	if ( idToCentro != null && idToCentro != '' ){
		return true;
	}
	
	else{
		mostrarMensajeInformativo("Elija un centro emisor.", 10, null);
		return false;
	}
	
}

function ocultarMostrarBoton(div){
	
	if (div=="masCentros"){
		jQuery("#masCentros").css("display", "none");
		jQuery("#menosCentros").css("display", "block");
				
	}else{
		jQuery("#masCentros").css("display", "block");
		jQuery("#menosCentros").css("display", "none");
	}
}