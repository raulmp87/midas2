/****
 * 
 * 
 * *****/

function traerListaCentro(){
	document.getElementById("cgCentroGrid").innerHTML = '';
	var cgCentroGrid = new dhtmlXGridObject('cgCentroGrid');
	mostrarIndicadorCarga('indicador');	
	cgCentroGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');	
		mostrarGrid('cgCentroGrid');
    });	
	
	var url = "/MidasWeb/condicionesGenerales/traerListaCentro.action";
	cgCentroGrid.load(url+"?"+jQuery("#cgCentroForm").serialize());
}

function mostrarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='block';
	}
}

function ocultarGrid(divName){
	
	if (document.getElementById(divName).innerHTML != null){
		document.getElementById(divName).style.display='none';
	}
}

function editarCgCentro(idToCentro){
	var url="/MidasWeb/condicionesGenerales/editarCentro.action"+"?"+"idToCentro="+idToCentro+"&"+jQuery("#cgCentroForm").serialize();
	sendRequestJQ(null,url,'contenido',null);
}

function crearCgCentro(){
	var url="/MidasWeb/condicionesGenerales/editarCentro.action";
	sendRequestJQ(null,url,'contenido',null);
}

function irInicioCentro(){
	
	var url = "/MidasWeb/condicionesGenerales/listarCentro.action";
	sendRequestJQ(null,url,'contenido',null);
}

function actualizarGuardarCentro(){
	
	if (this.validar()){		
		var form=jQuery("#cgCentroForm");
		var url="/MidasWeb/condicionesGenerales/actualizarGuardarCentro.action";
		sendRequestJQ(form,url,'contenido',null);		
	}
}

function validar(){
	
	return true;
}

/****   componentedireccion   ****/
function onChangePais(target,targetCiudad,targetColonia,targetCP,targetCalleNumero, paisSelect){
	var idPais = dwr.util.getValue(jQuery(paisSelect).selector);
	
	if(idPais != null  && idPais != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapEstados(idPais,
				function(data){
					addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}	
	if(targetCiudad){
		addOptionsDireccion(targetCiudad,"");
	}
	if(targetColonia){
		addOptionsDireccion(targetColonia,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}
/**
 * Funcion que pinta en el jsp los elemntos de un combo box
 * ordenados por el texto de la opcion y no por el value
 * @param ele
 * @param data
 */
function addOptionOderByElement(ele, data) {
	  dwr.util.removeAllOptions(jQuery(ele).selector);
	  ele = dwr.util._getElementById(ele, "addOptions()");
	  if (ele == null) return;
	  var useOptions = dwr.util._isHTMLElement(ele, "select");
	  if (!useOptions) {
	    dwr.util._debug("addOptions() can only be used with select elements. Attempt to use: " + dwr.util._detailedTypeOf(ele));
	    return;
	  }
	  if (data == null) return;
	  
	  var options = {}; 
	  var orderItem=new Array();
	  if (!options.optionCreator && useOptions) options.optionCreator = dwr.util._defaultOptionCreator;
	  options.document = ele.ownerDocument;

	    if (!useOptions) {
	      dwr.util._debug("dwr.util.addOptions can only create select lists from objects.");
	      return;
	    }
	    for (var prop in data) {
	      if (typeof data[prop] == "function") continue;
	      	options.data = data[prop];
	        options.value = prop;
	        options.text = data[prop];
	      if (options.text != null || options.value) {
	    	  orderItem[orderItem.length]=new Array(options.text,options.value);
	      }
	    }
	    orderItem.sort(function compara(a, b) {
	        return (a[0]<b[0]?"-1":"1");
	    });
	    
	    var opt = "";
	    var i = 0;
	    for(i; i<orderItem.length; i++){
	    	if(i==0){
	    		opt = options.optionCreator(options);
		    	opt.text = "seleccione...";
		        opt.value = "";
		        ele.options[ele.options.length] = opt;
	    	}
	    	opt = options.optionCreator(options);
	    	opt.text = orderItem[i][0];
	        opt.value = orderItem[i][1];
	        ele.options[ele.options.length] = opt;
	    }
	  // All error routes through this function result in a return, so highlight now
	  dwr.util.highlight(ele, options); 
	}

function onChangeEstadoGeneral(target,targetColonia,targetCP,targetCalleNumero,estadoSelect){
	var idEstado = dwr.util.getValue(jQuery(estadoSelect).selector);
	if(idEstado != null  && idEstado != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapMunicipiosPorEstado(idEstado,
				function(data){
//					addOptions(target,data);
					addOptionOderByElement(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}
	
	if(targetColonia){
		addOptionsDireccion(targetColonia,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeCiudad(target,targetCP,targetCalleNumero,ciudadSelect){
	var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
	if(idCiudad != null  && idCiudad != headerValue){
		dwr.engine.beginBatch();
		listadoService.getMapColoniasSameValue(idCiudad,
				function(data){
			addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptionsDireccion(target,"");
	}
	if(targetCP){
		dwr.util.setValue(targetCP, "");
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeColonia(target,targetCalleNumero, coloniaSelect,ciudadSelect){

	var idColonia = dwr.util.getValue(jQuery(coloniaSelect).selector);
	var idCiudad = dwr.util.getValue(jQuery(ciudadSelect).selector);
	if(idColonia != null  && idColonia != headerValue){		
		dwr.engine.beginBatch();
		listadoService.getCodigoPostalByColonyNameAndCityId(idColonia,idCiudad,
				function(data){
					dwr.util.setValue(target, data);
				});
		dwr.engine.endBatch({async:false});
	}
	if(targetCalleNumero){
		dwr.util.setValue(targetCalleNumero, "");
	}
}

function onChangeCodigoPostal(value,targetColonia,targetMunicipio,targetIdEstado,targetCalleNumero,targetPais,targetCP){
	if (dwr.util.getValue(targetPais)==""){
		dwr.util.setValue(targetPais,'MEXICO');
		onChangeCP_pais(targetIdEstado);
	}
	var idColonia = dwr.util.getValue(targetColonia);
	var idCiudad = dwr.util.getValue(targetMunicipio);
	var idEstado = dwr.util.getValue(targetIdEstado);
	//Verificamos si alguno de los combos antes del codigo postal no esta completo, si es asi, entonces
	//se lanza la funcion de llenar la direccion por cp
	//if((!jQuery.isValid(idColonia) || !jQuery.isValid(idCiudad) || !jQuery.isValid(idEstado)) && jQuery.isValid(value)){
		getNameColoniasPorCP(value,targetColonia ,targetMunicipio,targetIdEstado);
	//}
	
	//si no encuentra direccion por codigo postal se resetea el estado a "seleccione"
	//se agregarn las css class jQ_ciudad y jQ_estado para la validacion
	var ciudadActual=dwr.util.getValue(targetMunicipio);
	if(!jQuery.isValid(ciudadActual)){
		dwr.util.setValue(targetIdEstado,"")
		dwr.util.setValue(targetCP,"");
	}else{
	dwr.util.setValue(targetCP,value);
	}
	dwr.util.setValue(targetCalleNumero,"");	
}

function onChangeCP_pais(target){
		dwr.engine.beginBatch();
		listadoService.getMapEstados("PAMEXI",
				function(data){
					addOptionsDireccion(target,data);
				});
		dwr.engine.endBatch({async:false});
}

function onChangeNuevaColonia(nuevaColoniaId,coloniaId,idCheckbox){
	var checked=document.getElementById(idCheckbox).checked;
	document.getElementById(nuevaColoniaId).disabled=!checked;
	document.getElementById(coloniaId).disabled=checked;
	if(checked){
		jQuery(document.getElementById(nuevaColoniaId)).addClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).removeClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).val("");
	}else{
		jQuery(document.getElementById(nuevaColoniaId)).val("");
		jQuery(document.getElementById(nuevaColoniaId)).removeClass("jQrequired");
		jQuery(document.getElementById(coloniaId)).addClass("jQrequired");
	}
	
	('jQrequired');
}
function addOptionsDireccion(target, map) {
	dwr.util.removeAllOptions(jQuery(target).selector);
	addSelectHeader(target);
	dwr.util.addOptions(jQuery(target).selector, map);
}

function eliminarCgCentro(idToCentro){
	mostrarMensajeConfirm('Se cambiará a estatus INACTIVO el centro emisor elegido, ¿Desea continuar?', '20', 
			'guardarCondicionConfirm('+ idToCentro +')', null, null);
	
}

function guardarCondicionConfirm(idToCentro){
	jQuery.ajax({
		  type     : 'POST',
		  url      : '/MidasWeb/condicionesGenerales/eliminarCentro.action',
		  data     : {idToCentro:idToCentro},
		  dataType : 'json',
		  async    : true,
		  success  : function(data){
			  traerListaCentro();
			  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
		  }
	});
}

function activarCgCentro(idToCentro){
	mostrarMensajeConfirm('Se cambiará a estatus ACTIVO el centro emisor elegido, ¿Desea continuar?', '20', 
			'activarCgCentroConfirm('+ idToCentro +')', null, null);
	
}

function activarCgCentroConfirm(idToCentro){
	jQuery.ajax({
		  type     : 'POST',
		  url      : '/MidasWeb/condicionesGenerales/activarCentro.action',
		  data     : {idToCentro:idToCentro},
		  dataType : 'json',
		  async    : true,
		  success  : function(data){
			  traerListaCentro();
			  mostrarMensajeInformativo(data.mensaje, data.tipoMensaje);
		  }
	});
}