var jquery143;

//CONFIGURACION LIGAS AGENTES ---------------------------------------------------------------
//-------------------------------------------------------------------------------------------

function mostrarBusquedaConfiguracionesLigaAgente(){
	var url = mostrarBusquedaConfiguracionesLigaAgentePath;
	sendRequestJQ(null, url, targetWorkArea, null);
}

var listadoConfigLigasAgentesGrid;
function listarLigasAgentes(mostrarListadoVacio){
	if(!mostrarListadoVacio){
		mostrarListadoVacio = false;
	}
	jQuery("#listadoConfigLigasAgentesGrid").empty();
	listadoConfigLigasAgentesGrid = new dhtmlXGridObject('listadoConfigLigasAgentesGrid');
	listadoConfigLigasAgentesGrid.attachEvent("onXLS", function(grid_obj){blockPage();});
	listadoConfigLigasAgentesGrid.attachEvent("onXLE", function(grid_obj){unblockPage();});
	
	var formParams = jQuery(document.ligasAgentesForm).serialize();
	var url = listarLigasAgentesPath + '?' +  formParams + "&mostrarListadoVacio=" + mostrarListadoVacio;
	listadoConfigLigasAgentesGrid.load( url );
}

function limpiarFiltrosConfigLigasAgentes(){
	jQuery('#ligasAgentesForm').each (function(){
		  this.reset();
	});
}

function exportarExcelConfigLigasAgentes(){
	var formParams = jQuery(document.ligasAgentesForm).serialize();
	var url= exportarLigasAgentesPath + "?" + formParams;
	window.open(url, "Ligas Agentes");
}

function mostrarAltaConfiguracion(){
	sendRequestJQ(null, mostrarRegistroLigasAgentesPath, targetWorkArea, null);
}

function consultarConfigLigaAgente(idConfiguracion){
	var url = mostrarRegistroLigasAgentesPath + '?configuracion.id=' + idConfiguracion + "&esConsulta=true";
	sendRequestJQ(null, url, targetWorkArea, null);
}

function editarConfigLigaAgente(idConfiguracion){
	var url = mostrarRegistroLigasAgentesPath + '?configuracion.id=' + idConfiguracion;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function monitorConfigLigaAgente(idConfiguracion){
	var url = monitorLigasAgentesPath + '?monitorLigaFiltro.id=' + idConfiguracion;
	sendRequestJQ(null, url, targetWorkArea, "listarMonitorLigasAgentes();");
}

function enviarConfigLigaAgente(idConfiguracion){
	var urlEnvio = enviarLigaAgentePath + "?configuracion.id=" + idConfiguracion;
	enviarLigaAgentePorAjax(urlEnvio);
}

function enviarLigaAgentePorAjax(urlEnvio){
	jQuery.ajax({
        url: urlEnvio,
        dataType: 'json',
        async:false,
        type:"POST",
        data: null,
        success: function(json){
      	 unblockPage();
      	 mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);     	
        },
        input: function(json){
          unblockPage();
      	  mostrarMensajeInformativo(json.mensaje, json.tipoMensaje);
        },
        beforeSend: function(){
				blockPage();
			}	
  });
}

//MONITOR DE LIGAS AGENTES ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

var listadoMonitorLigasAgentesGrid;
function listarMonitorLigasAgentes(mostrarListadoVacio){
	if(!mostrarListadoVacio){
		mostrarListadoVacio = false;
	}
	jQuery("#listadoMonitorLigasAgentesGrid").empty();
	listadoMonitorLigasAgentesGrid = new dhtmlXGridObject('listadoMonitorLigasAgentesGrid');
	listadoMonitorLigasAgentesGrid.setImagePath('/MidasWeb/img/dhtmlxgrid/');
	listadoMonitorLigasAgentesGrid.setSkin('light');
	listadoMonitorLigasAgentesGrid.setHeader("Código, Nombre, Id Agente, Nombre Agente, Negocio, Cotización, Fecha Cotización, Prospecto, Teléfono Prospecto, Correo Prospecto, Producto, Tipo Póliza, Línea de Negocio, Marca, Modelo, Descripción Final, Número de Serie, Prima Neta, Prima Total, Póliza, Fecha Emisión");
	listadoMonitorLigasAgentesGrid.setInitWidths("70,110,100,110,110,100,100,110,100,100,100,100,100,100,100,100,100,100,100,100,100");
	listadoMonitorLigasAgentesGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center");
	listadoMonitorLigasAgentesGrid.setColSorting("server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server,server");
	listadoMonitorLigasAgentesGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ron,ro,ro");			
	listadoMonitorLigasAgentesGrid.init();
	listadoMonitorLigasAgentesGrid.attachEvent("onBeforePageChanged",function(){
		if (!this.getRowsNum()) return false;
		return true;
	});		
	listadoMonitorLigasAgentesGrid.enablePaging(true,20,5,"pagingArea",true,"infoArea");
	listadoMonitorLigasAgentesGrid.setPagingSkin("bricks");
	listadoMonitorLigasAgentesGrid.attachEvent("onXLS", function(grid_obj){blockPage();});
	listadoMonitorLigasAgentesGrid.attachEvent("onXLE", function(grid_obj){actualizarConteoCotizacionesPolizas();unblockPage();});
	
	var formParams = jQuery(document.monitorLigasAgentesForm).serialize();
	var url = listarMonitorLigasAgentesPath + '?' +  formParams + "&mostrarListadoVacio=" + mostrarListadoVacio;
	
	listadoMonitorLigasAgentesGrid.attachEvent("onBeforeSorting",function(ind, server, direct){			
		server = url;			
		listadoMonitorLigasAgentesGrid.clearAll();
		server = server+(server.indexOf("?")>=0?"&":"?")+"orderBy="+ind+"&direct="+direct;
		console.log(server);
		listadoMonitorLigasAgentesGrid.load(server);			
		listadoMonitorLigasAgentesGrid.setSortImgState(true,ind,direct);
	    return false;
	});
	
	listadoMonitorLigasAgentesGrid.load( url );
}

function limpiarFiltrosMonitorLigasAgentes(){
	jQuery('#monitorLigasAgentesForm').each (function(){
		  this.reset();
	});
}

function exportarExcelMonitorLigasAgentes(){
	var formParams = jQuery(document.monitorLigasAgentesForm).serialize();
	var url= exportarMonitorLigasAgentesPath + "?" + formParams;
	window.open(url, "Monitor Ligas Agentes");
}

function actualizarConteoCotizacionesPolizas(){
	var numeroCotizaciones = listadoMonitorLigasAgentesGrid.getUserData("","userData_numeroCotizaciones");
	jQuery("#numeroCotizaciones").val(numeroCotizaciones);
	var numeroPolizas = listadoMonitorLigasAgentesGrid.getUserData("","userData_numeroPolizas");
	jQuery("#numeroPolizas").val(numeroPolizas);
}

//REGISTRO DE LIGAS AGENTES -----------------------------------------------------------------
//-------------------------------------------------------------------------------------------

function onChangeAgente(target){
	var idAgente = jQuery("#agenteId").val();
	if(idAgente){
		dwr.engine.beginBatch();
		listadoService.getNegociosPorAgenteParaAutos(idAgente,
				function(data){
					addOptions(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(target,"");
	}
}

function guardarConfiguracionLigaAgente(){
	if(validateAll(true)){
		var formParams = jQuery(document.registroConfiguaracionForm).serialize();
		var url= guardarConfiguracionLigaAgentePath + "?" + formParams;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function inicializarRegistroLigas(){
	configurarModoConsulta();
	onChangeEstatus();
}

function configurarModoConsulta(){
	var esConsulta = jQuery("#esConsulta").val();
	if(esConsulta == 'true'){
		jQuery(".esConsulta").attr("readonly","true");
		jQuery(".deshabilitar").attr("disabled","disabled");
		jQuery("#btnGuardar").remove();
		jQuery("#btnGenerar").remove();
	}
}

function enviarLigaAgente(){
	var formParams = jQuery(document.registroConfiguaracionForm).serialize();
	var url= enviarLigaAgentePath + "?" + formParams;
	enviarLigaAgentePorAjax(url);
}

function copiarLigaAgente(){
	jQuery("#liga").select();
	document.execCommand("copy");
	jQuery("#liga").blur();
}

function generarLigaAgente(){
	var liga = jQuery("#liga").val();
	if(liga){
		var confirmacion = confirm('Si continua se reemplazar\u00E1 la liga actual por una nueva. \u00BFDesea continuar?');
	}
	if( !liga 
			|| confirmacion ){
		var idConfiguracion = jQuery("#codigo").val();
		var formParams = jQuery(document.registroConfiguaracionForm).serialize();
		var url= generarLigaAgentePath + "?" + formParams;
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function habilitarAutocompletarBusquedaAgente(){
	jQuery(function(){
		jquery143( '#descripcionBusquedaAgente' ).autocomplete({
	               source: function(request, response){
	            	   jquery143.ajax({
				            type: "POST",
				            url: autocompletarBusquedaAgentesPath,
				            data: {descripcionAgente:request.term},              
				            dataType: "xml",	                
				            success: function( xmlResponse ) {
				           		response( jquery143( "item", xmlResponse ).map( function() {
									return {
										idAgente: jquery143("idAgente",this).text(),
										value: jquery143( "descripcion", this ).text(),
					                    id: jquery143( "id", this ).text()
									}
								}));			           
	               		}
	               	})},
	               minLength: 3,
	               delay: 1000,
	               select: function( event, ui ) {
	            	   jquery143('#agenteId').val(ui.item.id);
	            	   onChangeAgente("negocios");
	            	   jQuery("#descripcionBusquedaAgente").attr("readonly","true");
	               }		          
	         });
	 });
}

function limpiarAgente(){
	jQuery("#descripcionBusquedaAgente").val("");
	addOptions("negocios","");
	jQuery("#agenteId").val("");
	jQuery("#descripcionBusquedaAgente").removeAttr("readonly");
}

function limpiarAgenteSiEsVacioAlInicio(){
	descripcionAgente = jQuery("#descripcionBusquedaAgente").val();
	if(descripcionAgente == " - "){
		jQuery("#descripcionBusquedaAgente").val("");
	}
}

function onChangeEstatus(){
	estatus = jQuery("#estatus").val();
	if(estatus == "SUSPENDIDO"){
		jQuery("#comentario").show();
		jQuery("#comentario_titulo").show();
		jQuery("#comentario").addClass("jQrequired");
	}else{
		jQuery("#comentario").val("");
		jQuery("#comentario").hide();
		jQuery("#comentario_titulo").hide();
		jQuery("#comentario").removeClass("jQrequired");
	}
}

function cargarInformacionContactoAgente(){
	var telefono = jQuery("#telefono").val();
 	var email = jQuery("#correo").val();
 	var idAgente = jQuery("#agenteId").val();
 	
 	if(idAgente
 			&& (!telefono
 			|| !email)){
		var formParams = jQuery(document.registroConfiguaracionForm).serialize();
		var urlEnvio= cargarInfoContactoAgentePath + "?" + formParams;
		jQuery.ajax({
	        url: urlEnvio,
	        dataType: 'json',
	        async:false,
	        type:"POST",
	        data: null,
	        success: function(json){
	      	 unblockPage();
	      	 if(!telefono){
	      		 jQuery("#telefono").val(json.telefonoAgente);
	      	 }
	      	 if(!email){
	      		 jQuery("#correo").val(json.emailAgente);
	      	 }
	        },
	        input: function(json){
	          unblockPage();
	        },
	        beforeSend: function(){
					blockPage();
				}	
	  });
 	}
}