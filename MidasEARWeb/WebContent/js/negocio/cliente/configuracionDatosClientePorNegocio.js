
var treeConfigDatosClientePorNeg;

function initTreeConfigDatosCliente(idNegocio){
	
	treeConfigDatosClientePorNeg = new dhtmlXTreeObject("ConfigDatosClienteTree","100%","100%",0);
	treeConfigDatosClientePorNeg.setImagePath("/MidasWeb/img/csh_winstyle/");
    //enable checkboxes
	treeConfigDatosClientePorNeg.enableCheckBoxes(true);
	treeConfigDatosClientePorNeg.loadXML(listarConfigSecPorNeg+"?idNegocio="+idNegocio);
    
//	treeConfigDatosClientePorNeg.enableThreeStateCheckboxes(true);//false to disable
//    treeConfigDatosClientePorNeg.showItemCheckbox(null,true);  //show checkbox
	
	treeConfigDatosClientePorNeg.setOnCheckHandler(refreshNodeTDCC);
}

function refreshNodeTDCC(id,value){
	sendRequestJQ(null, 
			actualzarNodoDatoClientePorNeg+'?idNodo='+id+'&claveObligatorio='+value,
			null, null);
}