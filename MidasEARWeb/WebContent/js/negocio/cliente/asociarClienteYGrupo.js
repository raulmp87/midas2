
	var accordionClientesNeg = null;
	var gridClientesNeg001 = null;
	var gridGpoCtesNeg001 = null;
	
	var activeCell = null;
	var PARAMETRO_ID_CLIENTE = "idCliente";
	var PARAMETRO_ID_GPO_CLIENTE = "idGrupoCliente";
	var PARAMETRO_ID_NEGOCIO = "idNegocio";
	var PARAMETRO_TIPO_PERSONA = "tipoPersona";
	
	var ID_CELDA_CLIENTES_ASOCIADOS ="clNeg1";
	var ID_CELDA_GRUPOS_ASOCIADOS ="grNeg2";
	var ventanaClientes = null;
	
	
	function actualizarTipoPersona(tipoPersona) {
		var parametros = getParametrosInicializacion() + "&" + PARAMETRO_TIPO_PERSONA + "=" + tipoPersona;

		blockPage();
		jQuery.ajax({
		    type: "POST",
		    url: cambiarTipoPersonaURL+"?"+parametros,
		    data:  null,
		    dataType: "text",
		    async: true,
		    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		    complete: function(jqXHR) {
		    	unblockPage();	
		    },
		    success: function(data) {
		    	// si la operacion se realizó con éxito
		    	if (data == "success") {
		    		jQuery("#tipoPersona").val(tipoPersona);
		    		jQuery("#mensajeTipoPersonaNegocio").hide();
		    	} else { // si hubo un error
		    		alert("Hubo un error, intentalo de nuevo más tarde.");
		    		if (tipoPersona == 1) {
		    			jQuery("#claveTipoPersona1").attr("checked", false);
		    			if (jQuery("#tipoPersona").val() == 2)
		    				jQuery("#claveTipoPersona2").attr("checked", true);
		    		} else if (tipoPersona == 2) {
		    			jQuery("#claveTipoPersona2").attr("checked", false);
		    			if (jQuery("#tipoPersona").val() == 1)
		    				jQuery("#claveTipoPersona1").attr("checked", true);
		    		}
		    	}
		    },
		    error:function (xhr, ajaxOptions, thrownError){
	            unblockPage();
	        }     
		});
	}
	
	function inicializarClientesAccordion(){
		
		mostrarIndicadorCarga('indicador');	
		accordionClientesNeg = new dhtmlXAccordion("accordionClientesNegocio");
		accordionClientesNeg.addItem(ID_CELDA_CLIENTES_ASOCIADOS, "Clientes");
		accordionClientesNeg.addItem(ID_CELDA_GRUPOS_ASOCIADOS, "Grupos");
		accordionClientesNeg.setEffect(true);
		accordionClientesNeg.attachEvent("onActive", function(itemId){
			activeCell = itemId;
	        return true;
	     });  
		var parametros = getParametrosInicializacion();
		gridClientesNeg001 = accordionClientesNeg.cells(ID_CELDA_CLIENTES_ASOCIADOS).attachGrid();
		gridClientesNeg001.attachEvent("onXLE", function(grid_obj,count){
			ocultarIndicadorCarga('indicador');
			var total = grid_obj.getRowsNum();
			if(total > 0){
				jQuery("#labelValidaClientes").html("Este negocio opera para los siguientes clientes \u00FAnicamente");
			}else{
				jQuery("#labelValidaClientes").html("Este Negocio opera para todos los Clientes");
			}
		}); 
		gridClientesNeg001.load(getClientesAsociadosURL+"?"+parametros);
		gridGpoCtesNeg001 = accordionClientesNeg.cells(ID_CELDA_GRUPOS_ASOCIADOS).attachGrid();
		gridGpoCtesNeg001.attachEvent("onXLE", function(grid_obj,count){
			ocultarIndicadorCarga('indicador');
			var total = grid_obj.getRowsNum();
			if(total > 0){
				jQuery("#labelValidaClientes").html("Este negocio opera para los siguientes clientes \u00FAnicamente");
			}
		}); 
		gridGpoCtesNeg001.load(getGruposAsociadosURL+"?"+parametros);
		if(nivelActivo == "2"){
			accordionClientesNeg.openItem(ID_CELDA_GRUPOS_ASOCIADOS);
		}else{
			accordionClientesNeg.openItem(ID_CELDA_CLIENTES_ASOCIADOS);
		}
		
	}

	
	function mostrarAgregar(){
		var ventanaClientes = parent.obtenerVentanaClientes();
		var tipoVentana = 1;
		var funcionPosteriorConsulta = registrarNuevoCliente;
		if(activeCell == null){
			if(nivelActivo == 2){
				tipoVentana = 2;
				funcionPosteriorConsulta = registrarNuevoGrupo;
				ventanaClientes.setFuncionPosteriorConsulta(funcionPosteriorConsulta);
				ventanaClientes.mostrarVentanaClientes(tipoVentana);
			}else{
				mostrarModalCliente();
			}
		}else if(activeCell == ID_CELDA_GRUPOS_ASOCIADOS) {
			tipoVentana = 2;
			funcionPosteriorConsulta = registrarNuevoGrupo;
			ventanaClientes.setFuncionPosteriorConsulta(funcionPosteriorConsulta);
			ventanaClientes.mostrarVentanaClientes(tipoVentana);
		} else {
			mostrarModalCliente();
		}
	}
	// Se usa para complementar un asegurado
	function mostrarAgregarCliente(){
		var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?tipoAccion=consulta&idField=idAsegurado&tipoBusqueda=1&divCarga=clienteModal2&ocultarAgregar=1";
		parent.sendRequestWindow(null, url, obtenerVentanaCliente);
	}
	
	function mostrarModalCliente(){
		var idNegocio = jQuery("#idNegocio").val();
		var pathNegocio = "";
		if(idNegocio != null && idNegocio != undefined){
			pathNegocio = "&idNegocio="+idNegocio;
		}
		var url="/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action?tipoAccion=consulta&idField=idClienteResponsable&tipoBusqueda=1&tipoRegreso=1"+pathNegocio;
		sendRequestWindow(null, url, obtenerVentanaCliente2);
	}

	function obtenerVentanaCliente(){
		var wins = parent.obtenerContenedorVentanas();
		ventanaClientes= wins.createWindow("clienteModal", 400, 250, 950, 500);
		ventanaClientes.center();
		ventanaClientes.setModal(true);
		ventanaClientes.setText("Consulta de clientes");
		return ventanaClientes;
	}
	
	function obtenerVentanaCliente2(){
		var wins = obtenerContenedorVentanas();
		ventanaClientes= wins.createWindow("clienteModal", 400, 250, 950, 500);
		ventanaClientes.center();
		ventanaClientes.setModal(true);
		ventanaClientes.setText("Consulta de clientes");
		return ventanaClientes;
	}
	
	function muestraResultadoAgregarCliente(idCliente, idDomCliente) {
		
		//var origen = dwr.util.getValue("origen");
		var origen = dwr.util.getValue("origen");
		//alert(origen);
		if (origen == 2){
			muestraResultadoAgregarClienteEndoso(idCliente, idDomCliente);
		} else {
			var url = "/MidasWeb/vehiculo/inciso/asegurado.actualizarDatosAsegurados.action"
				+ "?incisoCotizacion.id.idToCotizacion=" + dwr.util.getValue("idToCotizacion")
				+ "&incisoCotizacion.id.numeroInciso=" + dwr.util.getValue("numeroInciso")
				+ "&incisoCotizacion.incisoAutoCot.personaAseguradoId="
				+ idCliente
				//+ "&incisoCotizacion.direccionDTO.idToDireccion="
				//+ idDomCliente //no presente en busquedacliente
				+ "&radioAsegurado=" + dwr.util.getValue("radioAsegurado")
				+ "&nextFunction=closeVentanaAsegurado('" + dwr.util.getValue("idToCotizacion") + "')";
			redirectVentanaModal('ventanaAsegurado', url, null);	
			/*
			
			sendRequestJQ(null, "/MidasWeb/vehiculo/inciso/asegurado.actualizarDatosAsegurados.action"
				+ "?incisoCotizacion.id.idToCotizacion=" + dwr.util.getValue("idToCotizacion")
				+ "&incisoCotizacion.id.numeroInciso=" + dwr.util.getValue("numeroInciso")
				+ "&incisoCotizacion.incisoAutoCot.personaAseguradoId="
				+ idCliente
				+ "&incisoCotizacion.direccionDTO.idToDireccion="
				+ idDomCliente
				+ "&radioAsegurado=" + dwr.util.getValue("radioAsegurado")
				+ "&nextFunction=closeVentanaAsegurado(" + dwr.util.getValue("idToCotizacion") + ")", 'ventana', null);*/
		}
		
	}
	
	function muestraResultadoAgregarClienteEndoso(idCliente, idDomCliente){
		var url = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado/actualizarDatosAsegurados.action"
			+ "?biAutoInciso.value.personaAseguradoId="
			+ idCliente
			+ "&radioAsegurado=" + dwr.util.getValue("radioAsegurado")
			+ "&incisoContinuityId=" + dwr.util.getValue("incisoContinuityId")
			+ "&validoEn=" + dwr.util.getValue("validoEn")
			+ "&nextFunction=closeVentanaAsegurado()"
			
			alert(idCliente);
		redirectVentanaModal('ventanaAsegurado', url, null);	
	}

	function borrarSeleccionado(){
		var selectedId = null;
		var parametros = getParametrosInicializacion();
		var urlInvocacion = null;
		var pNextFunction = "";
		if(activeCell == null){
			if(nivelActivo == 1){
				selectedId = gridClientesNeg001.getSelectedRowId();
				parametros += "&"+PARAMETRO_ID_CLIENTE+"="+selectedId;
				urlInvocacion = desasociarClienteNegocioURL;
			}else{
				selectedId = gridGpoCtesNeg001.getSelectedRowId();
				parametros += "&"+PARAMETRO_ID_GPO_CLIENTE+"="+selectedId;
				urlInvocacion = desasociarGrupoNegocioURL;
			}
		}else{
			if(activeCell == ID_CELDA_CLIENTES_ASOCIADOS){
				selectedId = gridClientesNeg001.getSelectedRowId();
				parametros += "&"+PARAMETRO_ID_CLIENTE+"="+selectedId;
				urlInvocacion = desasociarClienteNegocioURL;
			}
			else if (activeCell == ID_CELDA_GRUPOS_ASOCIADOS){
				selectedId = gridGpoCtesNeg001.getSelectedRowId();
				parametros += "&"+PARAMETRO_ID_GPO_CLIENTE+"="+selectedId;
				urlInvocacion = desasociarGrupoNegocioURL;
			}
		}
		
		if(selectedId == null){
			mostrarVentanaMensaje('10', 'Seleccione un registro en la cuadricula.');
			return;
		}
		else{
			sendRequestJQ(null, urlInvocacion+"?"+parametros, 'contenido', pNextFunction);
		}
	}
	
	function registrarNuevoCliente(idCliente){
		var pNextFunction = "recargarGrid();";
		var parametrosGuardar = getParametrosInicializacion()+"&"+PARAMETRO_ID_CLIENTE+"="+idCliente;
		
		sendRequestJQ(null, asociarClienteNegocioURL+"?"+parametrosGuardar, 'contenido', pNextFunction);
		
	}
	
	function borrarRelaciones(){
		var parametros = getParametrosInicializacion();
		var urlInvocacion = eliminarRelacionesNegocioURL;
	
		if(confirm("\u00BF Esta seguro de hacer disponible para todo cliente?, se perder\u00e1 la configuraci\u00f3n de clientes si ya ha definido alguna" )){
			sendRequestJQ(null, urlInvocacion+"?"+parametros, 'contenido');
		}

	}
	
	function registrarNuevoGrupo(objGpoCliente){
		var pNextFunction = "recargarGrid()";
		
		var parametrosGuardar = getParametrosInicializacion()+"&"+PARAMETRO_ID_GPO_CLIENTE+"="+objGpoCliente.idGrupo;
		
		sendRequestJQ(null, asociarGrupoNegocioURL+"?"+parametrosGuardar, targetWorkArea, pNextFunction);
	}
	
	function recargarGrid(){
		var parametros = getParametrosInicializacion();
		
		if(activeCell == ID_CELDA_CLIENTES_ASOCIADOS){
			gridClientesNeg001.load(getClientesAsociadosURL+"?"+parametros);
		}
		else if (activeCell == ID_CELDA_GRUPOS_ASOCIADOS){
			gridGpoCtesNeg001.load(getGruposAsociadosURL+"?"+parametros);
		}
	}
	
	function recargarAmbosGrids(){
		var parametros = getParametrosInicializacion();
		gridClientesNeg001.load(getClientesAsociadosURL+"?"+parametros);
		gridGpoCtesNeg001.load(getGruposAsociadosURL+"?"+parametros);
	}
	
	function getParametrosInicializacion(){
		return PARAMETRO_ID_NEGOCIO+"="+getIdNegocio();
	}
	
	function getIdNegocio(){
		return idNegocio;
	}