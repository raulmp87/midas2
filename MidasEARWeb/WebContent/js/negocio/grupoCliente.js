var grupoClienteGrid;

function guardarGrupoCliente() {
	var url = appendQueryString(guardarGrupoClienteUrl, generarQueryStringDeGrid(clienteGrupoClienteGrid, "clienteIds"));
	sendRequestJQ(jQuery("#grupoClienteForm"), url, "contenido");
}

var clienteGrupoClienteGrid;
function inicializarGrupoClienteDetalle() {
	inicializarClienteGrupoClienteGrid();
}

function inicializarClienteGrupoClienteGrid() {
	clienteGrupoClienteGrid = new dhtmlXGridObject("clienteGrupoClienteGrid");
	clienteGrupoClienteGrid.load(mostrarClientesDhtmlxUrl);
}

function agregarClienteGrupoCliente(idCliente) {
	//No agregar ningun cliente al grid si este ya existe.
	if (clienteGrupoClienteGrid.doesRowExist(idCliente)) {
		return;
	}
	
	var nombre = jQuery("#nombreidClienteResponsable").val();
	
	var gruposUrl = mostrarClienteGruposDhtmlxUrl;
	var params = {id:idCliente};
	gruposUrl = appendQueryString(gruposUrl, jQuery.param(params));
	var borrarCellValue = borrarImgUrl + "^" + borrarAltTxt + "^"
			+ "javascript:borrarFilaGrid(clienteGrupoClienteGrid, \"" + idCliente + "\")^_self";
	clienteGrupoClienteGrid.addRow(idCliente, [gruposUrl, nombre, borrarCellValue]);
}


function listarGrupoCliente(){
	grupoClienteGrid = new dhtmlXGridObject("grupoClienteGrid");
	grupoClienteGrid.load(listarClienteGruposDhtmlxUrl);
}

function agregarGrupoCliente(tipoAccion){
	sendRequestJQAsync(null,'/MidasWeb/negocio/grupo-cliente/mostrar.action?tipoAccion='+tipoAccion,'contenido',null);
}

function editarGrupoCliente(tipoAccion){
	if(grupoClienteGrid.getSelectedId() != null){
		var id = getIdFromGrid(grupoClienteGrid, 0);
		var url = '/MidasWeb/negocio/grupo-cliente/mostrar.action?id='+id+"&tipoAccion="+tipoAccion;
		sendRequestJQAsync(null,url,'contenido',null);
	}
}

function eliminarGrupoCliente(tipoAccion){
	if(grupoClienteGrid.getSelectedId() != null){
		var respuesta = confirm("\u00BFEst\u00E1 seguro que desea eliminar el Grupo Cliente?");
		if(respuesta){
			var id = getIdFromGrid(grupoClienteGrid, 0);
			var url = '/MidasWeb/negocio/grupo-cliente/eliminar.action?id='+id+"&tipoAccion="+tipoAccion;
			sendRequestJQAsync(null,url,'contenido',null);
		}
	}
}


function regresarGrupoCliente() {
	sendRequestJQAsync(null,'/MidasWeb/negocio/grupo-cliente/listarGrupos.action','contenido',null);
}