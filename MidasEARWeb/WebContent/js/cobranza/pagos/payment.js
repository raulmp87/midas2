var retValue = window;
var iframeparent = window.parent.document.getElementById("FramePortPag");
$(document).ready(function(){
	$('#pay').click(function (e) {
		$('#payForm').submit();
	});

	$('#btnSearch').click(function(e){
		e.preventDefault();
		$(this).html('<i class="fa fa-spinner fa-spin"></i>');
		$('#searchForm').submit();
	});

	$('#insurancePolicy').blur(function(){
		formatPolicy(this);
	});

	$('#includeSubsection').click(function(){
		var inciso = $('#subsection');
		if($(this).is(":checked")){
			inciso.prop('disabled', '');
			inciso.prop('required', 'required');
		}else{
			inciso.prop('disabled', 'disabled');
			inciso.prop('required', '');
			inciso.val('');
		}
	});
});

function formatPolicy(target){
	var poliza = $(target).val();
	var parts = poliza.split('-');
	
	var validate = function(input){
		input.value = input.toUpperCase();
	}
	
	if(parts.length == 3){
		parts[0] = ('00000' + parts[0]).slice(-'00000'.length);
		parts[1] = ('000000000000' + parts[1]).slice(-'000000000000'.length);
		parts[2] = ('00' + parts[2]).slice(-'00'.length);
	}
	$(target).val(parts.join('-'));
};
 

$(document).ready(function(){
	$('#searchForm').submit(function (e){
		var rfc = $('#siglasRFC').val().toUpperCase() + $('#fechaRFC').val().toUpperCase();
		var len = rfc.length;
		if (len < 9 || len > 10 ){
			$('#textDinamico').text("La longitud del RFC sin homoclave es incorrecta. Favor de validarlo.");
			$('#mensajeCustom').modal();
			e.preventDefault();
		}			
	});
 
	if (document.getElementById('ifrProsa') != undefined) {
	    if (document.getElementById('ifrProsa').getAttribute('src') != undefined && document.getElementById('ifrProsa').getAttribute('src') != '') {
	        var iUrlProsa = document.getElementById('ifrProsa').getAttribute('src');
	        document.getElementById("ifrProsa").style.visibility = "hidden";
	        //openWindows(iUrlProsa);
	    }
	}
});

function openWindows() {
	var oldLocation = window.parent.location.href;
	var urldesd = $("#vUrl").val();
	
	if( oldLocation.substring(oldLocation.length-3,oldLocation.length) != ".do"){
		window.open('','_parent','');
        window.close();
		retValue = window.open(urldesd, "ProcesarPago", "width=900px,height=500,left=300");	
	}
	else{
		iframeparent.parentNode.removeChild(window.parent.document.getElementById("FramePortPag"));
		retValue = window.open(urldesd, "ProcesarPago", "width=900px,height=500,left=300");	
		}
	};



	




