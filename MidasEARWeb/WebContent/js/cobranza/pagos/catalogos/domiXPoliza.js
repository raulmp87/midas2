var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";

function filtrar(){
	$('#searchForm').submit();
}

function modifica(centroEmisor, nuemroPoliza, numeroRenovacion, numeroCuenta){
	$('#window').modal();
	$('#window').find('#centroEmisor').val(centroEmisor);
	$('#window').find('#idNumeroPoliza').val(nuemroPoliza);
	$('#window').find('#renovacion').val(numeroRenovacion);
	$('#window').find('#referencia').val(numeroCuenta);
	$('#window').find('#idEstatus').val(1);
	$('#window').find('.modal-title').text("Modificaci\u00f3n de Configuraci\u00f3n de P\u00f3lizas");
}

function baja(centroEmisor, numeroPoliza, numeroRenovacion, numeroCuenta){
	if (confirm('\u00BFEst\u00e1 seguro que desea eliminar el registro?')) {
		sendRequest(null,bajaURL+'?relacionGrupoAgente.id.centroEmisor='+centroEmisor+'&relacionGrupoAgente.id.numeroPoliza='+numeroPoliza+'&relacionGrupoAgente.id.numeroRenovacion='+numeroRenovacion+'&relacionGrupoAgente.numeroCuenta='+numeroCuenta, 'contenido', null, null);
	}
}

function agregar(){
	$('#window').modal();
	$("#window").find('input').val("");
	$('#window').find('#idEstatus').val(1);
	$('#window').find('.modal-title').text("Alta de Configuraci\u00f3n de P\u00f3lizas");
}

function sendRequest(param, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {
	$.ajax({
		    type: "POST",
		    url: actionURL,
		    data: (param !== undefined && param !== null) ? param : null,
		    dataType: dataTypeParam,
		    async: true,
		    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
		success : function(data) {		
			filtrar();
		} // End of onSuccess
		
	});
}
