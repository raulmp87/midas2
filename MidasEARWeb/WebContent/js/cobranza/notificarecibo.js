/**
 * 
 */
function mostrarListadoAgentes(){
	var idAgente = jQuery("#Agente").val();
	var field="txtId";
	if(idAgente == ""){
		var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField="+field;
		sendRequestWindow(null, url, obtenerVentanaAgentes);
	}else{
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.Agente":idAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeAgente(){
	var id = jQuery("#txtId").val();
	if(jQuery.isValid(id)){
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.id":id,"agente.idAgente":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

function onChangeIdAgt(){
	var calveAgente = jQuery("#Agente").val();
	if(jQuery.isValid(calveAgente)){
		var url="/MidasWeb/agtSaldoMovto/obtenerAgente.action";
		var data={"agente.idAgente":calveAgente,"agente.id":""};
		jQuery.asyncPostJSON(url,data,loadInfoAgente);
	}
}

//Notificacion para el job diario
function notifica(){
	var url ="/MidasWeb/cobranza/recibos/notificaFacturaMasivo.action";
	//jQuery.asyncPostJSON(url,null,loadInfoAgente);
	sendRequestJQ(null, url, null, null);	
	}


//Notifica a nivel poliza
function notificapoliza(){	
	
	var vPoliza      =jQuery("#Poliza").val();	
	var url ="/MidasWeb/cobranza/recibos/notificaFacturaPorUsuario.action?pPoliza="+vPoliza+"&pAsegurado=0&pAgente=0&pTipo=0";
	
	//jQuery.asyncPostJSON(url,null,loadInfoAgente);
	sendRequestJQ(null, url, null, null);
}