var defaultContentType = "application/x-www-form-urlencoded;charset=UTF-8";

function generarReporte(){
	window.open(generarExcelURL, 'download');
}

function openModalConfirm(){	
	$('#window').modal();
	$('#window').find('.modal-title').text("Confirmar Programacion para Carga de Domi y TC");
}
	
function confirmaProgramacion(){
	if (confirm('\u00BFEst\u00e1 seguro que desea confirmar la Programaci\u00F3n Automatica de Domi y TC ?')) {
		sendRequest(null,confirmProgURL+'?calendarioGonherDTO.confirmUser='+($('#confirmUsuario')[0].checked?1:0)+'&calendarioGonherDTO.idCalendar='+$('#idCalendario').val(), 'contenido', null, null);
		//.is(':checked')
	}
	
}

function sendRequest(param, actionURL, targetId, pNextFunction, dataTypeParam, contentTypeParam) {
	$.ajax({
		    type: "POST",
		    url: actionURL,
		    data: (param !== undefined && param !== null) ? param : null,
		    dataType: dataTypeParam,
		    async: true,
		    contentType: (contentTypeParam != null && contentTypeParam != '') ? contentTypeParam : defaultContentType,
		success : function(data) {		
	    	$('#searchForm').submit();
		} // End of onSuccess
		
	});
}
