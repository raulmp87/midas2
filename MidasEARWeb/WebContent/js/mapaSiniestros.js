var map;

jQuery(document).ready(
	function(){
		var zoomMapa = 12;
		var idContenedorMapa = "map";
		map = initMap( idContenedorMapa , zoomMapa );
		
		var oficinaId = parent.jQuery('#ofinasActivas').val()
		if(oficinaId != null){
			setMapaCoordenadasOficina(oficinaId);
		}
		
		//crear cuadro de busqueda
		var input = document.getElementById('pac-input');
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);

		//var mark = new google.maps.Marker({map: map});

	    google.maps.event.addListener(autocomplete, "place_changed", function()
	    {
	        var place = autocomplete.getPlace();

	        if (place.geometry.viewport) {
	            map.fitBounds(place.geometry.viewport);
	        } else {
	            map.setCenter(place.geometry.location);
	            map.setZoom(15);
	        }
	        if(marker){
	        	marker.setPosition(place.geometry.location);
	        	actualizarPosicionMarcador( marker );
	        }
	    });

	    google.maps.event.addListener(map, "click", function(event)
	    {
	    	if(marker){
	    		marker.setPosition(event.latLng);
	    		setEventAndLatLng( marker );
	    	}
	    });
				

	}
);
