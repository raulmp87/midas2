var facturasPtecomplementosPagoGrid;

function buscarFacturasPendientesComplemento(){
	if (validarFechasErrores()){
		var form = jQuery('#monitorForm').serialize();	
		var url = listarPendientesMonitor+ "?" + form;
		
		mostrarIndicadorCarga('indicador');
	
		document.getElementById("facturasPtecomplementosPagoGrid").innerHTML = '';	
		facturasPtecomplementosPagoGrid = new dhtmlXGridObject('facturasPtecomplementosPagoGrid');
			
		facturasPtecomplementosPagoGrid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');	
	    });		
		
		facturasPtecomplementosPagoGrid.enableEditEvents(true); 	
		facturasPtecomplementosPagoGrid.load(url);	
	}
}

function envioNotificacionComplementos(){
	var form = jQuery('#monitorForm').serialize();
	blockPage();
	jQuery.ajax({
		url:  enviarNotificacionMonitor+ "?" + form,				
		success: function(data){			
			unblockPage();
			var respuesta = JSON.parse(data); 			
			mostrarMensajeInformativo(respuesta.mensaje,  respuesta.tipoMensaje.toString());			
		},
		error: function(data){
			unblockPage()
			mostrarMensajeInformativo('Ha ocurrido un error al intentar inviar la noificacion a correos', '20');			
		}
	});	
}

function validarFechasErrores(){
    var fechaPagoInicio = jQuery("#fechaPagoInicio").val();
    var fechaPagoFin = jQuery("#fechaPagoFin").val();

    if (fechaPagoInicio==""&&fechaPagoFin=="") {
      alert("Debes ingresar las fechas requeridas.")
      return false;
    }else {
      if (compare_dates_period(fechaPagoInicio,fechaPagoFin)){
        alert("El periódo entre la fecha de pago inicial y la fecha de pago final no debe ser mayor a 1 año. Verificar.")
        return false;
      }else if (compare_dates(fechaPagoInicio,fechaPagoFin)) {
        alert("Error en las fechas")
        return false;
      }else{
        return true;
      }
    }
}