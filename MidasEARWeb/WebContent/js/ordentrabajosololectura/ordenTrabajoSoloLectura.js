
var accordionOT=null;
var ordenTrabajoSoloLecturaTreeAbreElementById = '';
function creaArbolOrdenesDeTrabajoSoloLectura(parametros){
	// Modificando
	var ordenTrabajoTreeSoloLectura = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = 1;
	if ($('idToCotizacion')==null){
		if (parametros!=null && parametros>0)
			idToCotizacion = parametros;
	}else
		idToCotizacion = $('idToCotizacion').value;
	var url = '/MidasWeb/ordentrabajosololectura/cargarArbolOrdenTrabajo.do?idToCotizacion='+idToCotizacion;
	var funcion = 'manejadorArbolOrdenTrabajoSoloLectura';
	ordenTrabajoTreeSoloLectura.setImagePath("/MidasWeb/img/csh_winstyle/");
	ordenTrabajoTreeSoloLectura.enableCheckBoxes(0);
	ordenTrabajoTreeSoloLectura.enableDragAndDrop(0);
	mostrarIndicadorCargaComps();
	
	
	ordenTrabajoTreeSoloLectura.attachEvent("onClick",function(id){
		if(funcion != ''){
			manejadorArbolOrdenTrabajoSoloLectura(splitIdArbol(id),ordenTrabajoTreeSoloLectura.getLevel(id));
		}
	});

	ordenTrabajoTreeSoloLectura.attachEvent("onXLE",function(id){
		ordenTrabajoTreeSoloLectura.openItem(idToCotizacion);
		if (ordenTrabajoSoloLecturaTreeAbreElementById!=''){
			ordenTrabajoTreeSoloLectura.openItem(ordenTrabajoSoloLecturaTreeAbreElementById);
		}
		ocultarIndicadorCargaComps();
	});
	
	new Ajax.Request(url, {
		   method : "post",onComplete : function(transport) {
		ordenTrabajoTreeSoloLectura.loadXMLString(transport.responseText);
		} 
	});
}


function manejadorArbolOrdenTrabajoSoloLectura(ids,level){
	switch(level){
		case 1:
			sendRequest(null,'/MidasWeb/ordentrabajosololectura/mostrarODTDetalle.do?id='+ids,'configuracion_detalle','dhx_init_tabbars();inicializaObjetosEdicionOTSoloLectura('+ids+');');
			break;
		case 2:
			sendRequest(null, '/MidasWeb/ordentrabajosololectura/mostrarModificarSeccionesODT.do?idToCotizacion='+ids[0],'configuracion_detalle',	'cargaSeccionesPorIncisosODTSoloLectura('+ids[1]+','+level+','+ids[0]+');');
			break;
		case 3:
			sendRequest(null,'/MidasWeb/ordentrabajosololectura/listarCoberturasODT.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2] ,'configuracion_detalle', 'mostrarCoberturaODTGridsSoloLectura('+ids[0]+','+ids[1]+','+ids[2]+')');
			break;
	}
}


function inicializaObjetosEdicionOTSoloLectura(idToCotizacion){
	accordionOT = new dhtmlXAccordion("accordionOT");
	accordionOT.addItem("a1", "Datos del Asegurado");
	accordionOT.addItem("a2", "Datos del Contratante");
	accordionOT.addItem("a3", "Documentos digitales complementarios");
	accordionOT.cells("a1").attachURL("/MidasWeb/ordentrabajosololectura/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
	accordionOT.cells("a2").attachURL("/MidasWeb/ordentrabajosololectura/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
	accordionOT.cells("a3").attachURL("/MidasWeb/ordentrabajosololectura/listarDocumentos.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion");
	accordionOT.openItem("a1");
	accordionOT.setEffect(true);
}

function cargaSeccionesPorIncisosODTSoloLectura(numeroInciso, nivel, idToCotizacion){
	if (nivel == 2){
		document.getElementById('configuracion_detalle').style.display ="block";
		mostrarSeccionesPorIncisoODTSoloLectura(numeroInciso,idToCotizacion);
	}
}

function mostrarSeccionesPorIncisoODTSoloLectura(numeroInciso,idToCotizacion){
	seccionesPorInciso = new dhtmlXGridObject('seccionesPorIncisoGrid');									  
	seccionesPorInciso.setHeader("Seccion,Contratada,Clave Obligatoriedad,Suma Asegurada,Prima Neta,Subincisos,puedeTenerSubIncisos");
	seccionesPorInciso.setColumnIds("seccion,claveContrato,claveObligatoriedad,sumaAsegurada,primaNeta,seleccionado,puedeTenerSubIncisos");
	seccionesPorInciso.setInitWidths("200,100,0,130,0,100,100");
	seccionesPorInciso.setColAlign("left,center,center,center,center,center,center");
	seccionesPorInciso.setColSorting("str,str,int,str,str,str,int");
	seccionesPorInciso.setColTypes("ro,ch,ro,ro,ro,ra,ro");
	seccionesPorInciso.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	seccionesPorInciso.setColumnHidden(2, true);
	seccionesPorInciso.setColumnHidden(4, true);	
	seccionesPorInciso.setColumnHidden(6, true);
	seccionesPorInciso.setNumberFormat("$0,000.00", 3);
	seccionesPorInciso.setNumberFormat("$0,000.00", 4);
	seccionesPorInciso.setSkin("light");
	seccionesPorInciso.attachEvent("onRowCreated", function(rId,rObj,rXml){
		var claveObligatoriedad = seccionesPorInciso.cellById(rId, 2);
		
		seccionesPorInciso.cellById(rId, 1).setDisabled(true);
		
		var puedeTenerSubIncisos = seccionesPorInciso.cellById(rId, 6);
		if (puedeTenerSubIncisos.getValue() > 1){
			seccionesPorInciso.setCellExcellType(rId,5,"ro");
			seccionesPorInciso.cellById(rId, 5).setValue('');
		}
		seccionesPorInciso.cellById(rId, 3).setDisabled(true);
		seccionesPorInciso.cellById(rId, 4).setDisabled(true);
	});



	seccionesPorInciso.attachEvent("onCellChanged",function(rId,cInd,nValue){
		var claveContrato = seccionesPorInciso.cellById(rId, 1);
		if(cInd == 5 && nValue== 1 && claveContrato.getValue() == '0') {
			cellInTurn = seccionesPorInciso.cellById(rId,5);
			document.getElementById('resultados').style.display ="none";
			return false;
		}
		cellInTurn = seccionesPorInciso.cellById(rId,6);
		if (cellInTurn.getValue() < 2){ 
			if (cInd == 5 && nValue== 1){
				cellInTurn = seccionesPorInciso.cellById(rId,1);
				ids=rId.split(',');
				$('idToCotizacion').value=ids[0];
				$('numeroInciso').value=ids[1];
				$('idToSeccion').value=ids[2];
							document.getElementById('resultados').style.display ="block";
				sendRequest(null,'/MidasWeb/ordentrabajosololectura/listarSubIncisosPorSeccion.do?idSeccionCot=' + rId,'resultados',null);
				
			}
		}
	});
	
	
	seccionesPorInciso.enableDragAndDrop(false);	
	seccionesPorInciso.enableLightMouseNavigation(false);
	seccionesPorInciso.init();
	seccionesPorInciso.load('/MidasWeb/cotizacion/cargarSeccionesPorInciso.do?numeroInciso='+numeroInciso+'&idToCotizacion='+idToCotizacion, null, 'json');

}

function mostrarCoberturaODTGridsSoloLectura(idToCotizacion,numeroInciso,idToSeccion){
	coberturaCotizacionGrids = new dhtmlXGridObject("cotizacionCoberturasGrid");
	coberturaCotizacionGrids.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	coberturaCotizacionGrids.setEditable(true);
	coberturaCotizacionGrids.setSkin("light");
	coberturaCotizacionGrids.setHeader(",,,,,,,,Cobertura,Contratada,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,,A/R/D,,");
	coberturaCotizacionGrids.setInitWidths("0,0,0,0,0,0,0,0,300,100,180,0,0,0,0,0,0,0,0,0,0");
	coberturaCotizacionGrids.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ch,ro,ro,ro,ro,co,img,co,img,img,ro,ro");
	coberturaCotizacionGrids.setColAlign("center,center,center,center,center,center,center,center,left,center,left,left,left,left,left,left,left,left,left,center,center");
	coberturaCotizacionGrids.setColSorting("int,int,int,int,int,int,na,na,na,na,na,na,na,na,na,na,na,na,na,int,int");
	coberturaCotizacionGrids.enableResizing("false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,false,false");
	coberturaCotizacionGrids.setColumnIds("idToCotizacion,numeroInciso,idToSeccion,idToCobertura,claveObligatoriedad,claveTipoSumaAsegurada,listaCoberturaRequeridas,listaCoberturaExcluidas," +
	"nombreComercial,claveContrato,sumaAsegurada,cuota,primaNeta,comision,coaseguro,img1,deducible,img2,img3,desglosaRiesgo,claveSubIncisos");	
	coberturaCotizacionGrids.setColumnHidden(0,true);
	coberturaCotizacionGrids.setColumnHidden(1,true);
	coberturaCotizacionGrids.setColumnHidden(2,true);
	coberturaCotizacionGrids.setColumnHidden(3,true);
	coberturaCotizacionGrids.setColumnHidden(4,true);
	coberturaCotizacionGrids.setColumnHidden(5,true);
	coberturaCotizacionGrids.setColumnHidden(6,true);
	coberturaCotizacionGrids.setColumnHidden(7,true);

	coberturaCotizacionGrids.setColumnHidden(11,true);
	coberturaCotizacionGrids.setColumnHidden(12,true);	
	coberturaCotizacionGrids.setColumnHidden(13,true);
	coberturaCotizacionGrids.setColumnHidden(14,true);
	coberturaCotizacionGrids.setColumnHidden(15,true);
	coberturaCotizacionGrids.setColumnHidden(16,true);
	coberturaCotizacionGrids.setColumnHidden(17,true);
	coberturaCotizacionGrids.setColumnHidden(18,true);
	coberturaCotizacionGrids.setColumnHidden(19,true);
	coberturaCotizacionGrids.setColumnHidden(20,true);
	coberturaCotizacionGrids.setNumberFormat("$0,000.00",10);
	coberturaCotizacionGrids.setNumberFormat("$0,000.00",12);	

	coberturaCotizacionGrids.attachEvent("onRowCreated",function(rowId, rowObj) {
		//coberturaCotizacionGrids.cellById(rowId, 12).setDisabled(true);
		var tipoSumaAsegurada = coberturaCotizacionGrids.cellById(rowId, 5);
		var desglosaRiesgo = coberturaCotizacionGrids.cellById(rowId, 19);
		var requiereSubIncisos = coberturaCotizacionGrids.cellById(rowId, 20);
		var obligatoriedad = coberturaCotizacionGrids.cellById(rowId, 4);
		var sumaAsegurada = coberturaCotizacionGrids.cellById(rowId, 10);
		var claveContrato = coberturaCotizacionGrids.cellById(rowId, 9);
		var coaseguro = coberturaCotizacionGrids.cellById(rowId, 14);
		var deducible = coberturaCotizacionGrids.cellById(rowId, 16);
		var cellARD = coberturaCotizacionGrids.cellById(rowId, 18);
		
		
				/*desglosaRiesgo.getValue() === '1' || 
				requiereSubIncisos.getValue() === '1'){*/
			sumaAsegurada.setDisabled(true);
			coaseguro.setDisabled(true);
			deducible.setDisabled(true);				
		

		
			claveContrato.setDisabled(true);
	
					
		
		return true;
	});	
	coberturaCotizacionGrids.init();	
	
	var cotizacionCoberturaPath = '/MidasWeb/cotizacion/cobertura/mostrarCoberturas.do?idToCotizacion='+idToCotizacion+'&numeroInciso='+numeroInciso+'&idToSeccion='+idToSeccion;	
	coberturaCotizacionGrids.load(cotizacionCoberturaPath,null, 'json');	
			
}




