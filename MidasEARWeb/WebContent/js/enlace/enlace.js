jQuery(function() {
	cometdUtil.subscribe("/notification/" + user.userName, function(message){
		var data = jQuery.parseJSON(message.data);
		var event = data.event;
		if(event == '/enlace/addMessage' || event == '/enlace/assign' 
			|| event == '/enlace/reassign' ||  event == '/enlace/changeStatus') {
			var caseId = data.caseId;
			toastr.info(data.message, caseId + " " + data.sender, {"onclick": function(){
				openCase(caseId);
			}});
		} 
	});
});

function openAddCase() {
	sendRequestJQ(null, config.contextPath + "/enlace/add.action", targetWorkArea, null, "html", defaultContentType);
}


function openMain() {
	sendRequestJQ(null, config.contextPath + "/enlace/main.action", targetWorkArea, null, "html", defaultContentType);
}

function addCase() {
	blockPage();
	var url = config.contextPath + "/rest/enlace/newCase.action?caso.caseTypeId=" + jQuery("#caseType").val()
		+ "&caso.caseSeverity=" + jQuery("#severity").val() + "&caso.description=" + jQuery("#caseDescription").val()
		+ "&createUser=" + jQuery("#createUser").val();
	sendRequestJQ(null, url, null, "unblockPage(); openMain();", "json", defaultContentType);
}

function openCase(caseId) {
	sendRequestJQ(null, config.contextPath + "/enlace/tracing.action?caso.id=" + caseId, targetWorkArea, "scrollDown();", "html", defaultContentType);
	
}

function closeCase(caseId) {
	var respuesta = confirm("¿Estás seguro que deseas cerrar el caso? No se podrá comentar mas en el");
	if(respuesta) {
		var url = config.contextPath +  "/rest/enlace/closeCase.action?caso.id=" + caseId + "&createUser=" + jQuery("#createUser").val();
		sendRequestJQ(null, url, null, "unblockPage();", "json", defaultContentType);
	}
}

function openReassignCase(caseId) {
	// Abrir popup
	dhxWinsEnlace = new dhtmlXWindows();
	var reassignWin = dhxWinsEnlace.createWindow("reassignWin", 400, 320, 250, 150);
	reassignWin.centerOnScreen();
	reassignWin.setModal(true);
	reassignWin.setText("Reasignar caso #" + caseId);
	blockPage();
	var url = config.contextPath  +  "/rest/enlace/getDeviation.action";
	jQuery.ajax({
	    type: "POST"
	    ,url: url
	    ,data: {"caseDeviation.createUser" : jQuery("#createUser").val() }
	    ,dataType: "json"
	    ,contentType: defaultContentType
	    ,success: function(data) {
	    	var html = "<div style='padding:10px;'><label for='deviationUser'>Reasignar caso a:</label><br /><select id='deviationUser' style='margin-top:10px'>";
	    	jQuery.each(data.data, function(index, value){
	    		html = html + "<option value='" + value.nombreUsuario+ "'>" + value.nombreCompleto +"</option>";
	    	});
	    	html = html + "</select><br /><button type='button' style='margin:10px 0px 0px 75px' onclick='reassignCase();'>Reasignar</button></div>"
	    	reassignWin.attachHTMLString(html);
	    }
	    ,complete: function(jqXHR) {
	    	unblockPage();
	    }
	    ,error:function (xhr, ajaxOptions, thrownError){
	    	mostrarMensajeInformativo(thrownError, "10");
        }   	    
	});


	//reassignWin.attachHTMLString("<div>Hola</div>");
	//sendRequestJQ(null, config.contextPath + "/rest/enlace/reassign.action", targetWorkArea, null, "html", defaultContentType);
	// TODO
}

function reassignCase(){
	var url = config.contextPath  +  "/rest/enlace/reassign.action";
	blockPage();
	jQuery.ajax({
	    type: "POST"
	    ,url: url
	    ,data: {"caso.id": jQuery("#caseId").val()
	    	  ,"createUser" : jQuery("#deviationUser").val()
	    }
	    ,dataType: "json"
	    ,contentType: defaultContentType
	    ,success: function(data) {
	    	alert("Se reasignó el caso correctamente");
	    	dhxWinsEnlace.window("reassignWin").close();
	    	sendRequestJQ(null, config.contextPath + "/enlace/main.action", targetWorkArea, null, "html", defaultContentType);
	    }
	    ,complete: function(jqXHR) {
	    	unblockPage();
	    }
	    ,error:function (xhr, ajaxOptions, thrownError){
	    	mostrarMensajeInformativo(thrownError, "10");
        }   	    
	});
}

function addMessage(){
	blockPage();
	var url = config.contextPath  +  "/rest/enlace/addMessage.action";
	jQuery.ajax({
	    type: "POST"
	    ,url: url
	    ,data: {"caso.id": jQuery("#caseId").val()
	    	  ,"caso.description" : jQuery("#newMessage").val()
	    	  ,"createUser" : jQuery("#createUser").val()
	    }
	    ,dataType: "json"
	    ,contentType: defaultContentType
	    ,success: function(data) {
	    	jQuery("#newMessage").val("");
	    	openCase(jQuery("#caseId").val());
	    }
	    ,complete: function(jqXHR) {
	    	unblockPage();
	    }
	    ,error:function (xhr, ajaxOptions, thrownError){
	    	mostrarMensajeInformativo(thrownError, "10");
        }   	    
	});
}

function openSetupCase() {
	sendRequestJQ(null, config.contextPath + "/enlace/setup.action", targetWorkArea, null, "html", defaultContentType);
}

function addDeviation() {
	blockPage();
	var url = config.contextPath + "/rest/enlace/addDeviation.action?caseDeviation.deviationUser=" + jQuery("#deviationUser").val()
		+ "&caseDeviation.iniDate=" + jQuery("#iniDate").val() + "&caseDeviation.endDate=" + jQuery("#endDate").val()
		+ "&caseDeviation.createUser=" + jQuery("#createUser").val();
	sendRequestJQ(null, url, null, "unblockPage();alert('Desvío guardado');", "json", defaultContentType);
}

function scrollDown() {
	//var offset = jQuery(".message").last().offset().top;
	jQuery("#contenido").scrollTop(42000);
}