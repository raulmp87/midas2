function iniContenedorEnvioProveedorGrid() {
	
	var envioProveedorGrid;
	var urlConsulta = "/MidasWeb/vida/asistencia/gridEnvioProveedor.action" ;
	envioProveedorGrid = new dhtmlXGridObject("envioProveedorGrid");
	envioProveedorGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
	envioProveedorGrid.setSkin("light");	
	envioProveedorGrid.attachEvent("onXLS", function(grid){	
		blockPage();
    });
	envioProveedorGrid.attachEvent("onXLE", function(grid){		
		unblockPage();
    });		
	envioProveedorGrid.init();
	envioProveedorGrid.load(urlConsulta);
	
}

function buscarEnvioProveedor(){
	if(validateAll(true)){	
		formParams = encodeForm(jQuery(document.contenedorForm));
		var urlConsulta = "/MidasWeb/vida/asistencia/buscarEnvioProveedor.action?" + formParams;
		var envioProveedorGrid = new dhtmlXGridObject("envioProveedorGrid");
		envioProveedorGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
		envioProveedorGrid.setSkin("light");
		envioProveedorGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		envioProveedorGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
	    });			
		envioProveedorGrid.init();
		envioProveedorGrid.load(urlConsulta);
	}
}

function exportarEnvioProveedor(){
	 formParams = encodeForm(jQuery(document.contenedorForm));
	 var url="/MidasWeb/vida/asistencia/exportarEnvioProveedor.action?" + formParams;
	 	window.open(url, "Envío a Proveedor");
}

function exportarEnvioProveedorDetalle(idBatch){
	 var url="/MidasWeb/vida/asistencia/exportarEnvioProveedorDetalle.action?idBatch=" + idBatch;
	 	window.open(url, "Exportar envío a Proveedor");
}

function enviarProveedor(idBatch){
	 var url="/MidasWeb/vida/asistencia/enviarProveedor.action?idBatch=" + idBatch;
	 	sendRequestJQ(null, url, null, null);
}

function exportarEnvioProveedorDetalleCons(){
    var fini = jQuery("#fechaRecepcionDesde").val();
	var ffin = jQuery("#fechaRecepcionHasta").val();
	if(fini != "" && ffin != ""){
		 formParams = encodeForm(jQuery(document.contenedorForm));
		 var url="/MidasWeb/vida/asistencia/exportarEnvioProveedorDetalleCons.action?" + formParams;
		 	window.open(url, "Envío a Proveedor");
	}else{
		mostrarMensajeInformativo('Es necesario capturar un periodo para realizar la descarga','20');		
	}
	

}