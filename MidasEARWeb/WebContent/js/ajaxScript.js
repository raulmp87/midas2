var totalRequests = 0;
var colonyCombo = null;
var cityCombo = null;
var stateCombo = null;
var ventanaClientesPorCURP = null;

function mostrarAutorizacionesRiesgos() {
	sendRequest(null, '/MidasWeb/cotizacion/riesgo/mostrarRiesgosPorCobertura.do?idToCotizacion=' + document.getElementById('idCotizacion').value + 
			'&numeroInciso=' + document.getElementById('numeroInciso').value + 
			'&idToSeccion=' + document.getElementById('idToSeccion').value + 
			'&idToCobertura=' + document.getElementById('idToCobertura').value + 
			'&autorizar=1', 
			'configuracion_detalle', 'initCotizacionRiesgoGrid(' + document.getElementById('idCotizacion').value + ',' + 
			document.getElementById('numeroInciso').value + ',' + document.getElementById('idToSeccion').value + ',' + 
			document.getElementById('idToCobertura').value + ')');
}

function seleccionarAgente(idAgente) {
	document.getElementById('idAgente').value = idAgente;
}

function escribirCodigoAgente(idAgente) {
	document.getElementById('codigoAgente').value = idAgente;
}

function mostrarVentanaMensajeValidar() {
	mostrarVentanaMensaje($('tipoMensajeValidar').value, $('mensajeValidar').value);
}

function calcularRiesgosHidro(idToCotizacion, numeroInciso) {
	new Ajax.Request('/MidasWeb/cotizacion/inciso/calcularRiesgosHidro.do?idToCotizacion=' + idToCotizacion + '&numeroInciso=' + numeroInciso, {
		method : "post", encoding : "UTF-8", asinchronous:false,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
		} // End of onSuccess
	});
}

function validarDatosIncisoEndoso(origen, accion) {
	var direccion = document.getElementsByName('direccionGeneral.idToDireccion');
	if(direccion[0] == null || direccion[0].value ==  '') {
		procesarRespuesta(null);
		return false;
	}
	
	var selects = document.getElementsByTagName('select');
	for(var i = 0 ; i < selects.length; i++) {
		if(selects[i] == null || selects[i].value == '') {
			procesarRespuesta(null);
			return false;
		}
	}
	var texts = document.getElementsByTagName('input');
	for(var i = 0 ; i < texts.length; i++) {
		if(texts[i].type == 'text') {
			if(texts[i] == null || texts[i].value == '') {
				procesarRespuesta(null);
				return false;
			}
		}
	}
	var textareas = document.getElementsByTagName('textarea');
	for(var i = 0 ; i < textareas.length; i++) {
		if(textareas[i] == null || textareas[i].value == '') {
			procesarRespuesta(null);
			return false;
		}
	}
	
	var nextFunction = "regresarAIncisoEndoso("+document.incisoCotizacionForm.idCotizacion.value+",'"+origen+"',"+document.incisoCotizacionForm.claveTipoEndoso.value+" );procesarRespuesta(null);"
	if (origen == 'COT'){
		if(accion == 'agregar') {
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/endoso/inciso/agregar.do?origen=COT','configuracion_detalle',nextFunction);
		} else {
			calcularRiesgosHidro(document.incisoCotizacionForm.idCotizacion.value, document.incisoCotizacionForm.numeroInciso.value);
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/endoso/inciso/modificar.do?origen=COT', 'configuracion_detalle',nextFunction);
		}		
	} else {
		if(accion == 'agregar') {
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/endoso/inciso/agregar.do?origen=ODT', 'configuracion_detalle',nextFunction);
		} else {
			calcularRiesgosHidro(document.incisoCotizacionForm.idCotizacion.value, document.incisoCotizacionForm.numeroInciso.value);
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/endoso/inciso/modificar.do?origen=ODT', 'configuracion_detalle',nextFunction);
		}
	}
}
function regresarAIncisoEndoso(idCotizacion, origen, tipoEndoso){
	if (origen == 'ODT'){
		sendRequest(null,'/MidasWeb/cotizacion/endoso/mostrarDetalle.do?id='+idCotizacion,'configuracion_detalle','setTabIncisosODT('+idCotizacion+');');
		creaArbolCotizacionEndoso(idCotizacion, tipoEndoso);
	}else{
		sendRequest(null,'/MidasWeb/cotizacion/endoso/mostrarDetalle.do?id='+idCotizacion,'configuracion_detalle','setTabIncisosCOT('+idCotizacion+');');
		creaArbolCotizacionEndoso(idCotizacion, tipoEndoso);
	}
}

function validarDatosInciso(origen, accion) {
	var direccion = document.getElementsByName('direccionGeneral.idToDireccion');
	if(direccion[0] == null || direccion[0].value ==  '') {
		procesarRespuesta(null);
		return false;
	}
	
	var selects = document.getElementsByTagName('select');
	for(var i = 0 ; i < selects.length; i++) {
		if(selects[i] == null || selects[i].value == '') {
			procesarRespuesta(null);
			return false;
		}
	}
	var divDetalle = document.getElementById('configuracion_detalle');
	var texts = divDetalle.getElementsByTagName('input');
	for(var i = 0 ; i < texts.length; i++) {
		if(texts[i].type == 'text') {
			if(texts[i] == null || texts[i].value == '') {
				procesarRespuesta(null);
				return false;
			}
		}
	}
	var textareas = document.getElementsByTagName('textarea');
	for(var i = 0 ; i < textareas.length; i++) {
		if(textareas[i] == null || textareas[i].value == '') {
			procesarRespuesta(null);
			return false;
		}
	}
	
	var nextFunction = "regresarAInciso("+document.incisoCotizacionForm.idCotizacion.value+",'"+origen+"');mostrarVentanaMensajeInciso();"
	if (origen == 'COT'){
		if(accion == 'agregar') {
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/agregar.do?origen=COT','configuracion_detalle',nextFunction);
		} else {
			calcularRiesgosHidro(document.incisoCotizacionForm.idCotizacion.value, document.incisoCotizacionForm.numeroInciso.value);
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/modificar.do?origen=COT', 'configuracion_detalle',nextFunction);
		}		
	} else {
		if(accion == 'agregar') {
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/agregar.do?origen=ODT', 'configuracion_detalle',nextFunction);
		} else {
			calcularRiesgosHidro(document.incisoCotizacionForm.idCotizacion.value, document.incisoCotizacionForm.numeroInciso.value);
			sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/modificar.do?origen=ODT', 'configuracion_detalle',nextFunction);
		}
	}
}

function validarDatosSubInciso(accion, origen, tipoEndoso) {
	if(document.getElementById('sumaAsegurada').value == '' || document.getElementById('descripcion').value == '') {
		alert('Debe introducir todos los datos. Favor de verificar.');
		return false;
	}
	if (!validarNumeroMaximoYDecimales(document.getElementById('sumaAsegurada').value, 11, 3, false)) {
		alert('El campo Suma Asegurada excede la cantidad m\u00e1xima permitida, realice el cambio necesario y vuelva a intentar guardar');
		return false;
	}
	var selects = document.getElementsByTagName('select');
	for(var i = 0 ; i < selects.length; i++) {
		if(selects[i] == null || selects[i].value == '') {
			alert('Debe introducir todos los datos. Favor de verificar.');
			return false;
		}
	}
	var texts = document.getElementsByTagName('input');
	for(var i = 0 ; i < texts.length; i++) {
		if(texts[i].type == 'text') {
			if(texts[i].id != null && texts[i].id != '' && (texts[i] == null || texts[i].value == '')) {
				alert('Debe introducir todos los datos. Favor de verificar.');
				return false;
			}
			if(texts[i].onblur != null && texts[i].onblur.toString().match('validarDecimal')) {
				var fieldValue = texts[i].value;
				if (isNaN(fieldValue) || fieldValue == "") {
					alert("Debe introducir un valor num&eacute;rico sin formato(sin comas).");
					return false;
				}
			}
		}
	}
	var textareas = document.getElementsByTagName('textarea');
	for(var i = 0 ; i < textareas.length; i++) {
		if(textareas[i] == null || textareas[i].value == '') {
			alert('Debe introducir todos los datos. Favor de verificar.');
			return false;
		}
	}
	if(accion == 'agregar') {
		sendRequest(document.subIncisoForm,"/MidasWeb/cotizacion/subinciso/agregar.do?origen=" + origen,"configuracion_detalle","mostrarSeccionesPorInciso(" + document.subIncisoForm.numeroInciso.value + ", " + document.subIncisoForm.idToCotizacion.value + ", '" + origen + "', '" + tipoEndoso + "')");
	} else {
		sendRequest(document.subIncisoForm,"/MidasWeb/cotizacion/subinciso/modificar.do?origen=" + origen,"configuracion_detalle","mostrarSeccionesPorInciso(" + document.subIncisoForm.numeroInciso.value + ", " + document.subIncisoForm.idToCotizacion.value  + ", '" + origen + "', '" + tipoEndoso + "')");
	}
}

function regresarAInciso(idCotizacion, origen){
	if (origen == 'ODT'){
		sendRequest(null,'/MidasWeb/cotizacion/mostrarODTDetalle.do?id='+idCotizacion,'configuracion_detalle','setTabIncisosODT('+idCotizacion+');');
		creaArbolOrdenesDeTrabajoIncisos(idCotizacion, origen);
	}else{
		sendRequest(null,'/MidasWeb/cotizacion/mostrarDetalle.do?id='+idCotizacion,'configuracion_detalle','setTabIncisosCOT('+idCotizacion+');');
		creaArbolCotizacion(idCotizacion);
	}
}
function borraInciso(origen, idCotizacion){
	var nextFunction ="regresarAInciso("+document.incisoCotizacionForm.idCotizacion.value+",'"+origen+"'); mostrarVentanaMensajeInciso();"
	sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/inciso/borrar.do', 'configuracion_detalle',nextFunction);
}
function borraIncisoEndoso(origen, idCotizacion){
	var nextFunction ="regresarAIncisoEndoso("+document.incisoCotizacionForm.idCotizacion.value+",'"+origen+"', "+document.incisoCotizacionForm.claveTipoEndoso.value+");procesarRespuesta(null);"
	sendRequest(document.incisoCotizacionForm,'/MidasWeb/cotizacion/endoso/inciso/borrar.do', 'configuracion_detalle',nextFunction);
}
function trim(str) {
	// generic method
	return str.replace(/^\s+|\s+$/g,'');
}

function loadScript(url){
	 var script = document.createElement("script")
	 script.type = "text/javascript";
	 script.src = url;
	 document.getElementsByTagName("head")[0].appendChild(script);
}

var ordenTrabajoTreeAbreElementById = '';
function creaArbolOrdenesDeTrabajoIncisos(parametros, origen){
	
	// Modificando
	var ordenTrabajoTree = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = 1;
	if ($('idToCotizacion')==null){
		if (parametros!=null && parametros>0)
			idToCotizacion = parametros;
	}else
		idToCotizacion = $('idToCotizacion').value;
	var url = '/MidasWeb/cotizacion/cargarArbolOrdenTrabajo.do?idToCotizacion='+idToCotizacion;
	var funcion = 'manejadorArbolOrdenTrabajo';
	ordenTrabajoTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	ordenTrabajoTree.enableCheckBoxes(0);
	ordenTrabajoTree.enableDragAndDrop(0);
	mostrarIndicadorCargaComps();
	
	
	ordenTrabajoTree.attachEvent("onClick",function(id){
		if(funcion != ''){
			manejadorArbolOrdenTrabajo(splitIdArbol(id),ordenTrabajoTree.getLevel(id));
		}
	});
	ordenTrabajoTree.attachEvent("onXLS",function(id,m){
    	mostrarIndicadorCargaComps();
    	    }); 
	ordenTrabajoTree.attachEvent("onXLE",function(id){
		ordenTrabajoTree.openItem(idToCotizacion);
		if (ordenTrabajoTreeAbreElementById!=''){
			ordenTrabajoTree.openItem(ordenTrabajoTreeAbreElementById);
		}
		ocultarIndicadorCargaComps();
	});
	
	new Ajax.Request(url, {
		   method : "post",onComplete : function(transport) {
		ordenTrabajoTree.loadXMLString(transport.responseText);
		if (origen == 'ODT')
			sendRequest(null,'/MidasWeb/cotizacion/mostrarODTDetalle.do?id='+idToCotizacion,'configuracion_detalle','setTabIncisosODT();');
		else
			sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id='+idToCotizacion,'configuracion_detalle','setTabIncisosCOT();');
		} 
	});
}

function creaArbolOrdenesDeTrabajo(parametros){
	// Modificando
	var ordenTrabajoTree = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = -1;
	
	var contextoMenu = 'estructuraODT';
	
	if ($('idToCotizacion')==null || $('idToCotizacion').value == ''){
		if (parametros!=null && parametros>0) {
			idToCotizacion = parametros;
		}
	} else {
		idToCotizacion = $('idToCotizacion').value;
	}
	
	ordenTrabajoTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	ordenTrabajoTree.enableCheckBoxes(0);
	ordenTrabajoTree.enableDragAndDrop(0);
	
	ordenTrabajoTree.attachEvent("onClick",function(id){
		manejadorArbolOrdenTrabajo(splitIdArbol(id),ordenTrabajoTree.getLevel(id));
	});	
	
	ordenTrabajoTree.attachEvent("onXLS",function(id,m){
		mostrarIndicadorCargaComps();
	}); 
	ordenTrabajoTree.attachEvent("onXLE",function(id,m){ 
    	ocultarIndicadorCargaComps();
    	ordenTrabajoTree.openItem(idToCotizacion);
    	if (ordenTrabajoTreeAbreElementById!=''){
			ordenTrabajoTree.openItem(ordenTrabajoTreeAbreElementById);
		}
    });
	
	ordenTrabajoTree.setXMLAutoLoading("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion);
	ordenTrabajoTree.loadXML("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion + "&id=" + idToCotizacion);
}

function creaArbolCotizacionEndoso(parametros, tipoEndoso){
	cotizacionSoloLectura = false;
	creaArbolCotizacionGeneral(parametros, tipoEndoso);
	
}


var cotizacionSoloLectura = null;
function creaArbolCotizacion(parametros){
	cotizacionSoloLectura = false;
	creaArbolCotizacionGeneral(parametros, null);
}

function creaArbolCotizacionGeneral(parametros, tipoEndoso){
	// Modificando
	var cotizacionTree = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = -1;
	
	var contextoMenu = 'estructuraCotizacion';
	
	if ($('idToCotizacion')==null || $('idToCotizacion').value == ''){
		if (parametros!=null && parametros>0)
			idToCotizacion = parametros;
	}else
		idToCotizacion = $('idToCotizacion').value;
	
	
	var funcion = 'manejadorArbolCotizacion';
	
	cotizacionTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	cotizacionTree.enableCheckBoxes(0);
	cotizacionTree.enableDragAndDrop(0);
	
	cotizacionTree.attachEvent("onClick",function(id){
		if(cotizacionSoloLectura){
			manejadorArbolCotizacionSoloLectura(splitIdArbol(id),cotizacionTree.getLevel(id));
		} else if (tipoEndoso != null) { 
			manejadorArbolCotizacionEndoso(splitIdArbol(id),cotizacionTree.getLevel(id), tipoEndoso);
		} else {
			manejadorArbolCotizacion(splitIdArbol(id),cotizacionTree.getLevel(id));
		}
	});	
	
	cotizacionTree.attachEvent("onXLS",function(id,m){
		mostrarIndicadorCargaComps();
	}); 
	cotizacionTree.attachEvent("onXLE",function(id,m){ 
    	ocultarIndicadorCargaComps();
    	cotizacionTree.openItem(idToCotizacion);
    });
	
	cotizacionTree.setXMLAutoLoading("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion);
	cotizacionTree.loadXML("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion + "&id=" + idToCotizacion);
	
	
}

function creaArbolCotizacionIncisos(parametros){
	// Modificando
	var cotizacionTree = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = -1;
	if ($('idToCotizacion')==null){
		if (parametros!=null && parametros>0)
			idToCotizacion = parametros;
	}else
		idToCotizacion = $('idToCotizacion').value;
	var url = '/MidasWeb/cotizacion/cargarArbolCotizacion.do?idToCotizacion='+idToCotizacion;
	var funcion = 'manejadorArbolCotizacion';
	
	cotizacionTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	cotizacionTree.enableCheckBoxes(0);
	cotizacionTree.enableDragAndDrop(0);
	
	new Ajax.Request(url, {
		   method : "post",onComplete : function(transport) {
		cotizacionTree.loadXMLString(transport.responseText);
		sendRequest(null,'/MidasWeb/cotizacion/mostrarDetalle.do?id='+idToCotizacion,'configuracion_detalle','setTabIncisosCOT();');
		} 
	});

	cotizacionTree.attachEvent("onClick",function(id){
		if(funcion != ''){
			manejadorArbolCotizacion(splitIdArbol(id),cotizacionTree.getLevel(id));
		}
	});	
	cotizacionTree.attachEvent("onXLE",function(id){
		cotizacionTree.openAllItems(idToCotizacion);
		ocultarIndicadorCargaComps();
	});
	cotizacionTree.attachEvent("onXLS",function(id,m){
    	mostrarIndicadorCargaComps();
    	    }); 
}

function splitIdArbol(id){
	return id.split('_');
}
function manejadorArbolCotizacionEndoso(ids,level,tipoEndoso){
	
	switch(level){
		case 1:
			sendRequest(null,'/MidasWeb/cotizacion/endoso/mostrar.do?id='+ids[0],'contenido','creaArbolCotizacionEndoso('+ids[0]+', '+tipoEndoso+');dhx_init_tabbars();');
			break;	
		case 2:
			if(tipoEndoso == 3)
				sendRequest(null, '/MidasWeb/cotizacion/endoso/mostrarModificarSecciones.do?idToCotizacion='+ids[0],'configuracion_detalle',	'mostrarSeccionesPorIncisoEndoso('+ids[1]+','+ids[0]+','+tipoEndoso+');');
			break;
		case 3:
			if(tipoEndoso == 3)
				sendRequest(null,'/MidasWeb/cotizacion/endoso/cobertura/listarCoberturas.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2]+'&tipoEndoso='+tipoEndoso  ,'configuracion_detalle', 'mostrarCoberturasPorSeccionEndoso('+ids[0]+','+ids[1]+','+ids[2]+','+tipoEndoso+')');
			break;
		case 4:
			if(tipoEndoso == 3)
				sendRequest(null,'/MidasWeb/cotizacion/endoso/riesgo/mostrarRiesgosPorCobertura.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2]+'&idToCobertura='+ids[3] ,'configuracion_detalle', 'initCotizacionEndosoRiesgoGrid('+ids[0]+','+ids[1]+','+ids[2]+','+ids[3]+','+tipoEndoso+')');
			break;
	}
}
function manejadorArbolCotizacion(ids,level){
	
	switch(level){
		case 1:
			//sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id='+ids,'contenido','creaArbolCotizacion('+ids+');dhx_init_tabbars();');
			sendRequest(null,'/MidasWeb/cotizacion/mostrarDetalle.do?id='+ids,'configuracion_detalle','dhx_init_tabbars();');
			break;	
		case 2:
			sendRequest(null, '/MidasWeb/cotizacion/mostrarModificarSecciones.do?idToCotizacion='+ids[0],'configuracion_detalle',	'mostrarSeccionesPorIncisoCOT('+ids[1]+','+ids[0]+');');
			break;
		case 3:
			sendRequest(null,'/MidasWeb/cotizacion/cobertura/listarCoberturas.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2] ,'configuracion_detalle', 'mostrarCoberturasPorSeccion('+ids[0]+','+ids[1]+','+ids[2]+')');
			break;
		case 4:
			sendRequest(null,'/MidasWeb/cotizacion/riesgo/mostrarRiesgosPorCobertura.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2]+'&idToCobertura='+ids[3] ,'configuracion_detalle', 'initCotizacionRiesgoGrid('+ids[0]+','+ids[1]+','+ids[2]+','+ids[3]+')');
			break;
	}
	
}
function manejadorArbolOrdenTrabajo(ids,level){
	switch(level){
		case 1:
			sendRequest(null,'/MidasWeb/cotizacion/mostrarODTDetalle.do?id='+ids,
					'configuracion_detalle','dhx_init_tabbars();inicializaObjetosEdicionOT('+ids+');');
			break;
		case 2:
			sendRequest(null, '/MidasWeb/cotizacion/mostrarModificarSeccionesODT.do?idToCotizacion='+ids[0],'configuracion_detalle',	'cargaSeccionesPorInciso('+ids[1]+','+level+','+ids[0]+');');
			break;
		case 3:
			sendRequest(null,'/MidasWeb/cotizacion/cobertura/listarCoberturasODT.do?idToCotizacion='+ids[0] +'&numeroInciso='+ids[1]+'&idToSeccion='+ids[2] ,'configuracion_detalle', 'mostrarCoberturaODTGrids('+ids[0]+','+ids[1]+','+ids[2]+')');
			break;
	}
}

/*Se movio el codigo relacionado con los datos de la linea.
 * las lienas de codigo del ajaxscript que se movieron van de la 99-813 
 * Se realizo el commit el lunes 19 octubre a las 15.5
 */

function headersGridSlipAviacion(){
	return ",Nombre Piloto,Licencia Piloto,Tipo Licencia,";
}

function headersGridSlipAviacion2(){
	return ",Coberturas,Cuotas,Primas,Deducibles,Coaseguros,";
}

var slipAviacion;
var slipAviacionPath;
var slipAviacion2;
var slipAviacion2Path;
function mostrarAviacionGrids(){
	slipAviacion = new dhtmlXGridObject('slipAviacionGrid');
	slipAviacion.setHeader(headersGridSlipAviacion());
	//slipAviacion.setColumnIds(idsGridLineas());
	slipAviacion.setInitWidths("200,200,200");
	//slipAviacion.setColAlign(alignGridLineas());
	//slipAviacion.setColSorting(sortingGridLineas());
	slipAviacion.setColTypes("ro,ro,ro");

	slipAviacion.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	slipAviacion.setSkin("light");		
	slipAviacion.enableDragAndDrop(false);	
	slipAviacion.enableLightMouseNavigation(false);
	slipAviacion.init();	
	
	//slipAviacionPath=new Array("Saab","Volvo","BMW");
	//slipAviacion.load(slipAviacionPath, null, 'json');
	
	slipAviacion2 = new dhtmlXGridObject('slipAviacion2Grid');
	slipAviacion2.setHeader(headersGridSlipAviacion2());
	//slipAviacion2.setColumnIds(idsGridLineas());
	slipAviacion2.setInitWidths("200,200,200,200,200");
	//slipAviacion2.setColAlign(alignGridLineas());
	//slipAviacion2.setColSorting(sortingGridLineas());
	slipAviacion2.setColTypes("ro,ro,ro,ro,ro");

	slipAviacion2.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	slipAviacion2.setSkin("light");		
	slipAviacion2.enableDragAndDrop(false);	
	slipAviacion2.enableLightMouseNavigation(false);
	slipAviacion2.init();	
	
	//slipAviacion2Path=new Array("Saab","Volvo","BMW");
	//slipAviacion2.load(slipAviacion2Path, null, 'json');
	
}

function headersGridSlipGeneral(){
	return ",Coberturas,Cuotas,Primas,Deducibles,Coaseguros,";
}

var slipGeneral;
function mostrarGeneralGrids(){
	slipGeneral = new dhtmlXGridObject('slipGeneralGrid');
	slipGeneral.setHeader(headersGridSlipGeneral());
	slipGeneral.setInitWidths("200,200,200,200,200");
	slipGeneral.setColTypes("ro,ro,ro,ro,ro");
	slipGeneral.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	slipGeneral.setSkin("light");		
	slipGeneral.enableDragAndDrop(false);	
	slipGeneral.enableLightMouseNavigation(false);
	slipGeneral.init();	
}

function headersGridSlipRcConstructores(){
	return ",Subramo,Cobertura,Suma Asegurada,Primas,Cuotas,Coaseguros,Deducibles,";
}

var slipRcConstructores;
function mostrarRcConstructoresGrids(){
	slipRcConstructores = new dhtmlXGridObject('slipRcConstructoresGrid');
	slipRcConstructores.setHeader(headersGridSlipRcConstructores());
	slipRcConstructores.setInitWidths("200,200,200,200,200,200,200");
	slipRcConstructores.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	slipRcConstructores.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	slipRcConstructores.setSkin("light");		
	slipRcConstructores.enableDragAndDrop(false);	
	slipRcConstructores.enableLightMouseNavigation(false);
	slipRcConstructores.init();	
}

/*Se movio el codigo relacionado con los datos de la linea.
 * las lienas de codigo del ajaxscript que se movieron van de la 900-1520 
 * Se realizo el commit el lunes 19 octubre a las 15.5 hrs
 */


function sendRequestPagination(actionURL) {
	var url = actionURL;
	var divTarifa = document.getElementById("displayTagTarifa");
	if (divTarifa == null){
		var divResumenIncisos = document.getElementById("contenido_resumenIncisos");
		if (divResumenIncisos == null)
			sendRequest(document.forms[0], url, 'contenido', null);
		else
			sendRequest(null, url, 'contenido_resumenIncisos', null);
	}
	else{
		sendRequest(null, url, 'displayTagTarifa', null);
	}
}

function Confirma(mensaje, fobj, actionURL, targetId, pNextFunction) {
	var aceptas = confirm(mensaje)
	if (aceptas){
		sendRequest(fobj, actionURL, targetId, pNextFunction)
	}
}

function sendRequest(fobj, actionURL, targetId, pNextFunction) {
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
			
//Inicio. Al descomentar este bloque, el arbol de tarifa se carga doble.
//	    	var mensaje = jQuery("#mensaje").text();
//	    	var tipoMensaje = jQuery("#tipoMensaje").text();
//	    	
//	    	if(tipoMensaje !='' && mensaje != '' && tipoMensaje !="none") {
//				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
//			}
//			
//			if (tipoMensaje != MENSAJE_TIPO_ERROR) {
//				if (pNextFunction !== null && pNextFunction !== '') {
//					eval(pNextFunction);
//				}
//			}	
// Fin.
		}, // End of onComplte
		onSuccess : function(transport) {
			// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
			if (targetId != null)
				$(targetId).innerHTML = transport.responseText;
			if (pNextFunction !== null && pNextFunction !== '') {				
				eval(pNextFunction);
			} 
		} // End of onSuccess
		
	});
}

function sendRequestM(fobj, actionURL, targetId, pNextFunction, mensaje) {
	//alert(actionURL); 
	new Ajax.Request(actionURL, {
		method : "post",
		encoding : "UTF-8",
		parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onCreate : function(transport) {
			totalRequests = totalRequests + 1;
			showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			totalRequests = totalRequests - 1;
			hideIndicator();
			/*mostrarMensajeInformativo(mensaje, '20');
			jQuery("#nsOficina").focus();*/
		}, // End of onComplte
		onSuccess : function(transport) {
			// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
			//alert(transport.responseText);
			if (targetId != null)				
				$(targetId).innerHTML = transport.responseText;
			if (pNextFunction !== null && pNextFunction !== '') {				
				eval(pNextFunction);
			} 
		} // End of onSuccess
		
	});
}


function listarProductos(){
	procesarRespuesta("sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/listar.do','contenido',null);");
}

function listarEstilos(){
	procesarRespuestaEstilos("sendRequest(document.estiloVehiculoGrupoForm,'/MidasWeb/catalogos/estilovehiculogrupo/listar.do','contenido',null);");
}


function procesarRespuestaEstilos(afterClose) {
	var mensaje = $('mensaje');
	var tipoMensaje = $('tipoMensaje');
	if (tipoMensaje != null && mensaje !=null){
	mostrarVentanaMensaje(tipoMensaje.value, mensaje.value, afterClose);
	}else{
		mostrarVentanaMensaje("30", "Acci\u00F3n realizada correctamente", afterClose);	
	}
	
}

function agregarSolicitudApartirPoliza() {
	sendRequest(null,'/MidasWeb/solicitud/agregarApartirPoliza.do', 'contenido',null);
}

function agregarSolicitud(){
	sendRequest(null,'/MidasWeb/solicitud/agregar.do?accion=2', 'contenido',null);
}

function refrescaMensajeUsuario(){
	// new Ajax.Updater('mensaje_popup','/MidasWeb/sistema/inicio.jsp',{});
	sendRequest(null, "/MidasWeb/sistema/inicio.jsp", "mensaje_popup", null);
}

function refrescaMensajeUsuario(){
	// new Ajax.Updater('mensaje_popup','/MidasWeb/sistema/inicio.jsp',{});
	sendRequest(null, "/MidasWeb/sistema/inicio.jsp", "mensaje_popup", null);
}

function redirectWorkArea(formObj, actionObj, targetId, nextFunction) {
	sendRequest(formObj, actionObj, targetId, nextFunction);
}

function showIndicator() {
	if (totalRequests === 1 && $("blockWindow") !== null) {
		backgroundFilter("blockWindow");
		var centralIndicator = $("central_indicator");

		var pageW = (isIE) ? document.body.offsetWidth - 20 : (innerWidth - 50);
		var pageH = (isIE) ? document.body.offsetHeight : (innerHeight);
		imageX = 235;
		imageY = 135;
		pageX = (pageH / 2) - (imageY);
		pageY = (pageW / 2) - (imageX / 2);
		centralIndicator.style.top = pageX + 'px';
		centralIndicator.style.left = pageY + 'px';
		centralIndicator.style.display = 'block';
	} // End of if
}
function showIndicatorSimple() {
		backgroundFilter("blockWindow");
		var centralIndicator = $("central_indicator");

		var pageW = (isIE) ? document.body.offsetWidth - 20 : (innerWidth - 50);
		var pageH = (isIE) ? document.body.offsetHeight : (innerHeight);
		imageX = 235;
		imageY = 135;
		pageX = (pageH / 2) - (imageY);
		pageY = (pageW / 2) - (imageX / 2);
		centralIndicator.style.top = pageX + 'px';
		centralIndicator.style.left = pageY + 'px';
		centralIndicator.style.display = 'block';
}
function hideIndicator() {
	if (totalRequests === 0 && $("blockWindow") !== null) {
		backgroundFilter("blockWindow");
		var centralIndicator = $("central_indicator");
		centralIndicator.style.display = "none";
	} // End of if
}
function getHijos(padreId, divPopular, nombreHijo){
	if (nombreHijo === 'producto' || padreId !== '') {
		new Ajax.Request("/MidasWeb/configuracion/"+nombreHijo+"/listarPorPradre.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + padreId,
			onSuccess : function(transport) {
				poblarDiv(divPopular,nombreHijo,transport.responseXML);
			} // End of onSuccess
		});
	}// End of if
}
function poblarDiv (divPopular,nombreHijo,objXML){
	var items = objXML.getElementsByTagName("item");
	var target = $(divPopular);
	var html = "<table>"
	var expandir = "<img src=\"/MidasWeb/img/arrow_down.png\" border=\"0\" alt=\"Expandir\" />"
	var sigNivel;
	
	if(nombreHijo === 'producto'){
		sigNivel = 'tipopoliza';
	} else if (nombreHijo === 'tipopoliza'){
		sigNivel = 'seccion';
	}
	
	for ( var x = 0; x < items.length; x++) {
		html = html + "<tr style='font-size: 7pt'>";
		var item = items[x];
		var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		var url = "/MidasWeb/configuracion/"+nombreHijo+"/mostrarDetalle.do?id="+ value;
		var funcion = "onclick=\"sendRequest(null,'"+ url +"','contenido',null)\"";
		var funcionHijo = "onclick=\"getHijos("+value+",'"+ sigNivel+value +"','"+ sigNivel +"');\"";
		var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
		var childNumber = item.getElementsByTagName("childNumber")[0].firstChild.nodeValue;
		if (childNumber > 0){
			html = html  + "<td><a href='javascript: void(0);' "+funcionHijo+" >"+ expandir +"</a>&nbsp;&nbsp;<a  href='javascript: void(0);' "+ funcion +">" + text + "</a></td></tr>";
			html = html  + "<div id='"+sigNivel+value+"'> </div>";
		}else{
			html = html  + "<td>&nbsp;&nbsp;&nbsp;&nbsp;<a  href='javascript: void(0);' "+ funcion +">" + text + "</a></td> </tr>";
		}// End of If Else
	} // End of for
	html = html + "</table>";
	target.innerHTML = html;
}

function getEstados(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/estado.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			}
		});
	} else {
		removeAllOptionsExt($(child));
	}
}

function getCalificaciones(obj, child) {
	var band = (document.getElementById(child).value);
	
	if (obj.selectedIndex > 0) {
		
		new Ajax.Request("/MidasWeb/calificacionesReas.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				
				loadCombo(child, transport.responseXML);
				
				document.getElementById(child).value = band;
			} 
		});
	} else {
		removeAllOptionsExt($(child));
	} 
}

function getCiudades(obj, child) {
	
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/ciudad.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function getMunicipios(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/municipio.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function getColonias(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/colonia.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getMonedasTipoPoliza(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/monedaTipoPoliza.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getFormasPagoPorMoneda(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/formaPagoMoneda.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubGiros(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subGiro.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubGirosRC(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subGiroRC.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubTiposRecipientePresion(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subTipoRecipientePresion.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubTiposEquipoElectronico(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subTipoEquipoElectronico.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubTiposEquipoContratista(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subTipoEquipoContratista.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubTiposMaquinaria(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subTipoMaquinaria.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getSubRamos(obj, child) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/subRamo.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}
function getColoniasPorCP(codigoPostal, colonia, ciudad, estado) {
	if (codigoPostal != null || codigoPostal != "") {
		colonyCombo = colonia
		cityCombo = ciudad
		stateCombo = estado
		new Ajax.Request("/MidasWeb/codigoPostal.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + codigoPostal,
			onSuccess : function(transport) {
				loadCombos(transport.responseXML);
			} // End of onSuccess
		});
	}
}

function getMarcaVehiculos(obj, child){
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/marcaVehiculo.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function getTipoVehiculos(obj, child){
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/tipoVehiculo.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function getEstiloVehiculos(objVersionCarga,tipoVehiculo, marcaVehiculo ,child){
	objTipoVehiculo = $(tipoVehiculo);
	objMarcaVehiculo = $(marcaVehiculo);
//	alert("TipoVehiculo: "+objTipoVehiculo.selectedIndex+" MarcaVehiculo: "+objMarcaVehiculo[objMarcaVehiculo.selectedIndex].value+" VersionCarga: "+parseInt(objVersionCarga.value));
	if (objTipoVehiculo.selectedIndex > 0 && objMarcaVehiculo.selectedIndex > 0 && parseInt(objVersionCarga.value) > 0 ) {
		new Ajax.Request("/MidasWeb/estiloVehiculo.do", {
			method : "post",
			asynchronous : false,
			parameters : "idTipoVehiculo=" + objTipoVehiculo[objTipoVehiculo.selectedIndex].value + 
					"&idMarcaVehiculo=" + objMarcaVehiculo[objMarcaVehiculo.selectedIndex].value + 
					"&idVersionCarga=" + parseInt(objVersionCarga.value),
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function getVersionCarga(objMarcaVehiculo,claveTipoBien,tipoVehiculo,child){
	objTipoBien = $(claveTipoBien);
	objTipoVehiculo = $(tipoVehiculo);
	if (objTipoVehiculo.selectedIndex > 0 && objMarcaVehiculo.selectedIndex > 0 && objTipoBien.selectedIndex > 0 ) {
		new Ajax.Request("/MidasWeb/versionCarga.do", {
			method : "post",
			asynchronous : false,
			parameters : "tipoVehiculo=" + objTipoVehiculo[objTipoVehiculo.selectedIndex].value+"&marcaVehiculo="+objMarcaVehiculo[objMarcaVehiculo.selectedIndex].value+"&tipoBien="+objTipoBien[objTipoBien.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function loadCombos(doc) {
	var colonies = $(colonyCombo);
	var city = $(cityCombo);
	var state = $(stateCombo);
	if (colonies != null && city != null && state != null) {
		removeAllOptionsExt(colonies);
		removeAllOptionsExt(city);
		if (colonies.length == 0) {
			addOption(colonies, "Seleccione ...", "");
		}
		var items = doc.getElementsByTagName("colonies");
		for ( var x = 0; x < items.length; x++) {
			var item = items[x];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(colonies, text, value);
		}
		var cityElement = doc.getElementsByTagName("city");
		if (cityElement.length > 0) {
			var cityItem = cityElement[0];
			var value = cityItem.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = cityItem.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(city, text, value);
			for ( var i = 0; i < city.length; i++) {
				var option = city.options[i];
				if (option.value === value) {
					option.selected = true;
					break;
				}
			}
		}
		var stateElement = doc.getElementsByTagName("state");
		if (stateElement.length > 0) {
			var stateItem = stateElement[0];
			var value = stateItem.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = stateItem.getElementsByTagName("description")[0].firstChild.nodeValue;
			for ( var i = 0; i < state.length; i++) {
				var option = state.options[i];
				if (option.value === value) {
					option.selected = true;
					break;
				}
			}
		}
	}
	//Despues de cargar todos los combos entonces carga los ajustadores
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
}

function loadCombo(child, doc) {
	var selectedCombo = $(child);
	
	if (selectedCombo != null) {
		removeAllOptionsExt(selectedCombo);
		if (selectedCombo.length == 0) {
			addOption(selectedCombo, "Seleccione ...", "");
		} // End of if

		var items = doc.getElementsByTagName("item");
		for ( var x = 0; x < items.length; x++) {
			
			var item = items[x];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(selectedCombo, text, value);
		} // End of for
	} // End of if
}

function removeAllOptionsExt(selectbox) {
	for ( var i = selectbox.length - 1; i >= 1; i--) {
		selectbox.remove(i);
	}
}
function addOption(selectbox, text, value) {
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	try {
		selectbox.add(optn, null);
	} catch (ex) {
		selectbox.add(optn); // IE only
	} // End of try/catch
}
function limpiarObjetos(references) {
	var objects = references.split(",");
	for ( var i = objects.length - 1; i >= 0; i--) {
		var formObject = $(objects[i]);
		if (formObject !== null) {
			if (formObject.type === "select-one") {
				removeAllOptionsExt(formObject);
				formObject.options[0] = new Option("Seleccione ...", "", true,
						false);
			} else if (formObject.type === "text") {
				cleanText(formObject);
			} // End of if/else
		} // End of if/else
	} // End of for
}
function cleanText(obj) {
	obj.value = "";
}


/*funciones  de la linea cambiadas de lugar crearContrato, imprimeIdContrato (lineas 606-628)*/

function llenarCombo(obj, child, action) {
	if (obj.selectedIndex > 0) {
		new Ajax.Request(action, {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}

function llenarComboSubRamo(idRamo) {
	if (idRamo !== '' && idRamo !== null) {
		new Ajax.Request('/MidasWeb/subRamo.do', {
			method : "post",
			asynchronous : false,
			parameters : "id=" + idRamo,
			onSuccess : function(transport) {
				loadCombo('idTcSubRamo', transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($('idTcSubRamo'));
	} // End of if/else
}

/*funciones  de la linea cambiadas de lugar mostrar div ocultardiv y mostrardivparticipaciones*/

var accordionOT=null;
function inicializaObjetosEdicionOT(idToCotizacion){
	accordionOT = new dhtmlXAccordion("accordionOT");
	accordionOT.addItem("a1", "Datos del Asegurado");
	accordionOT.addItem("a2", "Datos del Contratante");
	accordionOT.addItem("a3", "Documentos digitales complementarios");
	accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
	accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
	accordionOT.cells("a3").attachURL("/MidasWeb/solicitud/listarDocumentos.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion");
	accordionOT.openItem("a1");
	accordionOT.setEffect(true);
}

var accordionCotizacion = null;
function inicializaObjetosEdicionCotizacion(idToCotizacion){
	accordionCotizacion = new dhtmlXAccordion("accordionCotizacion");
	accordionCotizacion.addItem("a1", "Datos del Asegurado");
	accordionCotizacion.addItem("a2", "Datos del Contratante");
	//accordionOT.addItem("a3", "Documentos digitales complementarios");
	accordionCotizacion.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3&origen=COT");
	accordionCotizacion.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1&origen=COT");
	//accordionOT.cells("a3").attachURL("/MidasWeb/solicitud/listarDocumentos.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion");
	accordionCotizacion.openItem("a1");
	accordionCotizacion.setEffect(true);
}

/** Inicio de funciones para Persona * */
var ventanaPersona=null;

function configurarFormularioPersonaVentana(obj){
	if (obj.value == 1){// Persona fisica
	document.getElementById('cajaApellidoPaterno').style.visibility = 'visible';
	document.getElementById('cajaApellidoMaterno').style.visibility = 'visible';
	document.getElementById('etiquetaPaterno').style.visibility = 'visible';
	document.getElementById('etiquetaMaterno').style.visibility = 'visible';
	}
	else if (obj.value == 2){// Persona moral
		document.getElementById('cajaApellidoMaterno').style.visibility = 'hidden';
		document.getElementById('cajaApellidoPaterno').style.visibility = 'hidden';
		document.getElementById('etiquetaPaterno').style.visibility = 'hidden';
		document.getElementById('etiquetaMaterno').style.visibility = 'hidden';
		}
}

function configurarFormularioPersona(obj,apellidoPaterno,apellidoMaterno){
	if (obj.value == 1){// Persona fisica
		document.getElementById(apellidoPaterno).style.display = '';
		document.getElementById(apellidoMaterno).style.display = '';
		document.getElementById('etiquetaPaterno').style.display = '';
		document.getElementById('etiquetaMaterno').style.display = '';
		document.getElementById('etiquetaRepresentante').style.display = 'none';
		document.getElementById('cajaRepresentante').style.display = 'none';
		document.getElementById('cajaNombreTD').colSpan = "1";
		document.getElementById('etiquetaFecha').firstChild.firstChild.firstChild.innerHTML = "Fecha de nacimiento *<div/>";
	} else if (obj.value == 2) {// Persona moral
		document.getElementById(apellidoPaterno).style.display = 'none';
		document.getElementById(apellidoMaterno).style.display = 'none';
		document.getElementById('etiquetaPaterno').style.display = 'none';
		document.getElementById('etiquetaMaterno').style.display = 'none';
		document.getElementById('etiquetaRepresentante').style.display = '';
		document.getElementById('cajaRepresentante').style.display = '';
		document.getElementById('cajaNombreTD').colSpan = "5";
		document.getElementById('etiquetaFecha').firstChild.firstChild.firstChild.innerHTML = "Fecha de constituci&oacute;n *<div/>";
	}
}

function mostrarRegistroPersona(idToPersona,descripcionPadre,idPadre,codigoPersona){
	if (parent.dhxWins==null){
		parent.dhxWins = new parent.dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaPersona = parent.dhxWins.createWindow("registroDireccion", 400, 250, 610, 350);
	parent.ventanaPersona.setText("Agregar/Actualizar Persona.");
	parent.ventanaPersona.center();
	parent.ventanaPersona.setModal(true);
	parent.ventanaPersona.attachURL("/MidasWeb/cotizacion/mostrarAgregarPersona.do?idToPersona="+idToPersona+"&idPadre="+idPadre+"&codigoPersona="+codigoPersona+"&descripcionPadre="+descripcionPadre);
	parent.ventanaPersona.button("minmax1").hide();
}

function mostrarPersonaInterfaz(){
	var idPadre = document.getElementById("idPadre").value;
	var codigo = document.getElementById("codigoPersona").value;
	if (parent.dhxWins==null){
		parent.dhxWins = new parent.dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaPersona = parent.dhxWins.createWindow("mostrarPersonaInterfaz", 400, 250, 910, 490);
	parent.ventanaPersona.setText("Busqueda de Clientes.");
	parent.ventanaPersona.center();
	parent.ventanaPersona.setModal(true);
	parent.ventanaPersona.attachURL("/MidasWeb/cotizacion/mostrarBuscarPersona.do?idPadre="+idPadre+"&codigoPersona="+codigo);
	parent.ventanaPersona.button("minmax1").hide();	
}

function guardarPersona(fobj){
	new Ajax.Request('/MidasWeb/cotizacion/agregarPersona.do', {
		method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {procesarRespuestaAgregarDireccion(transport.responseXML);}
	});
	if (parent.accordionOT != null){
		if (parent.accordionOT.cells("a1") != null)
			parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+fobj.idPadre.value+"&descripcionPadre=cotizacion&tipoDireccion=3");
		if (parent.accordionOT.cells("a2") != null)
			parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+fobj.idPadre.value+"&descripcionPadre=cotizacion&tipoDireccion=1");
	}
	//Esta condicion indica si se encuentra en el formulario de edici�n de Proveedor Inspeccion
	if (parent.document.getElementById("editaProveedorInspeccion") != null){
		if (parent.document.getElementById("accion") != null){
			if (parent.document.getElementById("accion").value != 'agrega'){
				var url = '/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?idPadre='+fobj.idPadre.value+'&accion=edita';
				new Ajax.Request(url, {method : "post",asynchronous : false,encoding : "UTF-8", parameters : null,
					onSuccess : function(transport) {
						parent.document.getElementById("contenido").innerHTML = transport.responseText;
					} });
			}
			else{
				var url = '/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?accion=agrega';
				new Ajax.Request(url, {method : "post",asynchronous : false,encoding : "UTF-8", parameters : null,
					onSuccess : function(transport) {
						parent.document.getElementById("contenido").innerHTML = transport.responseText;
					} });
			}
		}
	}
	if (parent.ventanaPersona != null){
		parent.ventanaPersona.close();
	}
}

/** Fin de funciones para Persona * */
/** Inicio de funcion para mostrar bitacora de seguimiento de renovaciones**/
var ventanaBitacora=null;
function mostrarBitacoraSeguimiento(idToPoliza){ 
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaCuotas = parent.dhxWins.createWindow("registroDireccion", 400, 320, 610, 310);
	parent.ventanaCuotas.setText("Bitacora de Seguimiento de Renovaciones de Poliza.");
	parent.ventanaCuotas.center();
	parent.ventanaCuotas.setModal(true);
	parent.ventanaCuotas.attachURL("/MidasWeb/renovacion/mostrarBitacoraSeguimiento.do?idToPoliza="+idToPoliza);
	parent.ventanaCuotas.button("minmax1").hide();

}

/** Inicio de funcion para modificar cuotas de endosos **/
var ventanaCuotas=null;
var cNextFunction = null;
function mostrarCuotasEndoso(idToCotizacion,idToSeccion,idToCobertura){ 
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaCuotas = parent.dhxWins.createWindow("registroDireccion", 400, 320, 610, 310);
	parent.ventanaCuotas.setText("Modificar Cuota del Endoso.");
	parent.ventanaCuotas.center();
	parent.ventanaCuotas.setModal(true);
	parent.ventanaCuotas.attachURL("/MidasWeb/cotizacion/endoso/mostrarModificarCuotas.do?idToCotizacion="+idToCotizacion+"&idToSeccion="+idToSeccion+"&idToCobertura="+idToCobertura);
	parent.ventanaCuotas.button("minmax1").hide();

}
function modificarCuota(idToCotizacion, numeroInciso, idToSeccion, idToCobertura, primaNeta, operacion){
	var url = '/MidasWeb/cotizacion/endoso/modificarCuotas.do?idToCotizacion='
			+ idToCotizacion + '&numeroInciso=' + numeroInciso
			+ '&idToSeccion=' + idToSeccion + '&idToCobertura=' + idToCobertura
			+ '&primaNeta=' + primaNeta + '&operacion='+operacion;
	new Ajax.Request(url, {method : "post",asynchronous : false,encoding : "UTF-8", parameters : null,
		onCreate : function(transport) {
			parent.dhxWins.window('registroDireccion').hide();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.dhxWins.window('registroDireccion').show();
		}, // End of onComplte
		onSuccess : function(transport) {
			procesarRespuestaModificarCuotas(transport.responseXML, idToCotizacion);
		} });
}

function procesarRespuestaModificarCuotas(xmlDoc,idToCotizacion){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if(value == "1") {
		//cerrar ventana y hacer nextfunction
		var nextFunction = "parent.sendRequest(null,'/MidasWeb/cotizacion/endoso/mostrarDatosEndoso.do?id="+idToCotizacion+"', 'contenido_endoso', null);";
		parent.mostrarVentanaMensaje('30', text,nextFunction);
		parent.dhxWins.window('registroDireccion').close();
	} else {
		parent.mostrarVentanaMensaje('10', text);
	}
}

/** Fin de funcion para modificar cuotas de endosos **/

/** Inicio de funciones para Direccion * */
var ventanaDireccion=null;
var dNextFunction = null;

function mostrarRegistroDireccion(idToDireccion,descripcionPadre,idPadre,codigoDireccion,nextFunction){ 
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaDireccion = parent.dhxWins.createWindow("registroDireccion", 400, 320, 610, 310);
	parent.ventanaDireccion.setText("Agregar/Actualizar Direccion.");
	parent.ventanaDireccion.center();
	parent.ventanaDireccion.setModal(true);
	parent.ventanaDireccion.attachURL("/MidasWeb/direccion/mostrarAgregarDireccion.do?idToDireccion="+idToDireccion+"&idPadre="+idPadre+"&codigoDireccion="+codigoDireccion+"&descripcionPadre="+descripcionPadre);
	parent.ventanaDireccion.button("minmax1").hide();
	if(nextFunction != null) {
		dNextFunction = nextFunction;
	} else {
		dNextFunction = null;
	}
}

function guardarDireccion(fobj){
	var clave;
	new Ajax.Request('/MidasWeb/direccion/agregarDireccion.do', {
		method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) { clave = procesarRespuestaAgregarDireccion(transport.responseXML);}
	});
	if(parent.accordionOT != null && parent.accordionOT.base.innerHTML != ""){
		if (parent.accordionOT.cells("a1") != null) {
			parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+fobj.idPadre.value+"&descripcionPadre=cotizacion&tipoDireccion=3");
		}
		if (parent.accordionOT.cells("a2") != null) {
			parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+fobj.idPadre.value+"&descripcionPadre=cotizacion&tipoDireccion=1");
		}
	}
	//Esta condicion indica si se encuentra en el formulario de edici�n de Proveedor Inspeccion
	if (parent.document.getElementById("editaProveedorInspeccion") != null){
		if (parent.document.getElementById("accion").value != 'agrega'){
			var url = '/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?idPadre='+fobj.idPadre.value+'&accion=edita';
			new Ajax.Request(url, {method : "post",asynchronous : false,encoding : "UTF-8", parameters : null,
				onSuccess : function(transport) {
					parent.document.getElementById("contenido").innerHTML = transport.responseText;
				} });
		}
		else{
			var url = '/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?accion=agrega';
			new Ajax.Request(url, {method : "post",asynchronous : false,encoding : "UTF-8", parameters : null,
				onSuccess : function(transport) {
					parent.document.getElementById("contenido").innerHTML = transport.responseText;
				} });
		}
	}
	var ubicacion = parent.document.getElementById('ubicacion');
	if (parent.ventanaDireccion != null){
		if(clave != null) {
			var fobj = null;
			
			new Ajax.Request("/MidasWeb/cotizacion/inciso/mostrarUbicacion.do?idDireccion=" + clave, {
				method : "post",
				encoding : "UTF-8",
				parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
						true) : null,
				onCreate : function(transport) {
					totalRequests = totalRequests + 1;
					showIndicator();
				}, // End of onCreate
				onComplete : function(transport) {
					totalRequests = totalRequests - 1;
					hideIndicator();
				}, // End of onComplte
				onSuccess : function(transport) {
					
					var ubicacionActual = parent.document.createElement("DIV");
					ubicacionActual.innerHTML = transport.responseText; 
					
					if (navigator.appName.indexOf("Microsoft") != -1) { //IE
						ubicacion.innerHTML = ubicacionActual.childNodes[0].innerHTML;
		            } else { //FireFox
		            	ubicacion.innerHTML = ubicacionActual.firstElementChild.innerHTML;
		            }
					
					eval("parent.ventanaDireccion.close()");
					
					
					
				} // End of onSuccess
				
			});
		}
	}
}

function procesarRespuestaAgregarDireccion(xmlDoc){
	var items = xmlDoc.getElementsByTagName("item");
	var item = items[0];
	var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
	var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
	if(value == "1") {
		eval(parent.dNextFunction);
		mostrarVentanaMensaje('30', text);
	} else {
		mostrarVentanaMensaje('10', text);
	}
	if(item.getElementsByTagName("clave")[0] != null && item.getElementsByTagName("clave")[0] != undefined) {
		var clave = item.getElementsByTagName("clave")[0].firstChild.nodeValue;
		return clave;
	}
}
/** Fin de funciones para Direccion * */
var calendarioDobleOT=null;
var calendarioOT=null;
var calendarioOT2=null;
var calendarioCERE=null;
function mostrarCalendarioDobleOT(){
	if (calendarioDobleOT==null && calendarioDobleOT==undefined){
		calendarioDobleOT= new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
		calendarioDobleOT.setDateFormat("%d/%m/%Y");
		calendarioDobleOT.leftCalendar.attachEvent("onClick",function(){
	 		var date = calendarioDobleOT.leftCalendar.getDate();
	 		$('fechaInicial').value=calendarioDobleOT.leftCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDobleOT.rightCalendar.attachEvent("onClick",function(){
			var date = calendarioDobleOT.rightCalendar.getDate();
			$('fechaFinal').value=calendarioDobleOT.rightCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDobleOT.show();
	}
	else
		if (calendarioDobleOT.isVisible())
			calendarioDobleOT.hide();
		else
			calendarioDobleOT.show();
}

var calendarioDobleBasesEmision = null;
function mostrarCalendarioDobleBasesEmision(){
	if (calendarioDobleBasesEmision == null && calendarioDobleBasesEmision == undefined){
		calendarioDobleBasesEmision= new dhtmlxDblCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
		calendarioDobleBasesEmision.setDateFormat("%d/%m/%Y");
		calendarioDobleBasesEmision.leftCalendar.attachEvent("onClick",function(){
	 		var date = calendarioDobleBasesEmision.leftCalendar.getDate();
	 		$('fechaInicial').value=calendarioDobleBasesEmision.leftCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDobleBasesEmision.rightCalendar.attachEvent("onClick",function(){
			var date = calendarioDobleBasesEmision.rightCalendar.getDate();
			$('fechaFinal').value=calendarioDobleBasesEmision.rightCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDobleBasesEmision.show();
	} else {
		if (calendarioDobleBasesEmision.isVisible()) {
			calendarioDobleBasesEmision.hide();
		} else {
			calendarioDobleBasesEmision.show();
		}
	}
}

var calendarioRCS = null;
function mostrarCalendarioRCS(){ 
	if (calendarioRCS == null && calendarioRCS == undefined){
		calendarioRCS= new dhtmlxCalendarObject('rangoDeFechas', true, {isMonthEditable: true, isYearEditable: true});
		calendarioRCS.setDateFormat("%d/%m/%Y");
		calendarioRCS.attachEvent("onClick",function(){
	 		var date = calendarioRCS.getDate();
	 		$('fechaInicial').value=calendarioRCS.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioRCS.show();
	} else {
		if (calendarioRCS.isVisible()) {
			calendarioRCS.hide();
		} else {
			calendarioRCS.show();
		}
	}
}

function initCalendarioRCS(){ 
	calendarioRCS = null;
}

var calendarioDoble = null;
function mostrarCalendarioDoble(){	
	if (calendarioDoble == null && calendarioDoble == undefined){
		calendarioDoble= new dhtmlxDblCalendarObject('calendarioDoble', true, {isMonthEditable: true, isYearEditable: true});
		calendarioDoble.setDateFormat("%d/%m/%Y");
		calendarioDoble.leftCalendar.attachEvent("onClick",function(){
	 		var date = calendarioDoble.leftCalendar.getDate();
	 		$('fechaInicial').value=calendarioDoble.leftCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDoble.rightCalendar.attachEvent("onClick",function(){
			var date = calendarioDoble.rightCalendar.getDate();
			$('fechaFinal').value=calendarioDoble.rightCalendar.getFormatedDate(formatoFechaCalendario(),date);
		});
		calendarioDoble.show();
	} else {
		if (calendarioDoble.isVisible()) {
			calendarioDoble.hide();			
		} else {
			calendarioDoble.show();			
		}
	}
}

function reiniciarCalendarioDoble(){
	calendarioDoble=null;
}

function mostrarCalendarioCancelacionRehabilitacionEndosos(idFecha){
	if (calendarioCERE==null || calendarioCERE==undefined){
		
		
		if(document.all){ //Para IE
		}else if (document.getElementById) {
			// Standard way to get element
			document.getElementById(idFecha.id).innerHTML="";
		}
		
		calendarioCERE = new dhtmlxCalendarObject(idFecha.id, true, {isMonthEditable: true, isYearEditable: true});
		calendarioCERE.setDateFormat("%d/%m/%Y");
		calendarioCERE.show();
	}
	else{
		if (calendarioCERE.isVisible()){
			calendarioCERE.hide();
			calendarioCERE=null;
		}
		else{
			calendarioCERE.show();
		}
	}
}


function mostrarCalendarioOT(){
	if (calendarioOT==null || calendarioOT==undefined){
		//document.getElementById("fecha").innerHTML="";
		calendarioOT = new dhtmlxCalendarObject('fecha', true, {isMonthEditable: true, isYearEditable: true});
		calendarioOT.setDateFormat("%d/%m/%Y");
		calendarioOT.show();
	}
	else{
		if (calendarioOT.isVisible()){
			calendarioOT.hide();
			calendarioOT=null;
		}
		else{
			calendarioOT=null;
			calendarioOT = new dhtmlxCalendarObject('fecha', true, {isMonthEditable: true, isYearEditable: true});
			calendarioOT.setDateFormat("%d/%m/%Y");
			calendarioOT.show();
		}
	}
}

function cerrarCalendario(){
	if (calendarioOT.isVisible()){
		calendarioOT.hide();
		calendarioOT=null;
	}
}

function mostrarCalendarioOT2(){
	if (calendarioOT2==null || calendarioOT2==undefined){
		document.getElementById("fecha2").innerHTML="";
		calendarioOT2 = new dhtmlxCalendarObject('fecha2', true, {isMonthEditable: true, isYearEditable: true});
		calendarioOT2.setDateFormat("%d/%m/%Y");
		calendarioOT2.show();
	}
	else{
		if (calendarioOT2.isVisible()){
			calendarioOT2.hide();
			calendarioOT2=null;
		}
		else{
			calendarioOT2.show();
		}
	}
}
function cerrarObjetosEdicionOT(){
	if (calendarioDobleOT != null && calendarioDobleOT != undefined )
		calendarioDobleOT.hide();
	calendarioDobleOT = null;
}

var calendarfechaAsignacion=null;
var calendarfechaContactoAjustador=null;
var calendarfechaLlegadaAjustador=null;
var calendarfechaInspeccionAjustador=null;
var calendarfechaPreliminar=null;
var calendarfechaDocumento=null;
var calendarfechaInformeFinal=null;
var calendarfechaTerminacionSiniestro=null;
var calendarfechaCerrado=null;

var calendarDatPolizafecPago=null;
var calendarDatPolizafecEmision=null;
var calendarDatPolizafecVigencia=null;
var calendarDatPolizaal=null;

var calendarPreEspFecNoti=null;
var calendarPreEspPlaRecl=null;

var calendarRepoSiniFechSiniestro=null;

var calendarListarFechaSiniestro=null;
function mostrarCalendarioSiniestro(calen){
	switch (calen){
	//------------------------------
	case '1':
		if (calendarfechaAsignacion==null && calendarfechaAsignacion==undefined){
			calendarfechaAsignacion = new dhtmlxCalendarObject('fechaAsignacion');
			calendarfechaAsignacion.setDateFormat(formatoFechaCalendario());
			calendarfechaAsignacion.show();
		}
		else
			if (calendarfechaAsignacion.isVisible())
				calendarfechaAsignacion.hide();
			else
				calendarfechaAsignacion.show();
		break;
	//------------------------------

	//------------------------------
		case '2':
			if (calendarfechaContactoAjustador==null && calendarfechaContactoAjustador==undefined){
				calendarfechaContactoAjustador = new dhtmlxCalendarObject('fechaContactoAjustador', true, {isMonthEditable: false, isYearEditable: false});
				calendarfechaContactoAjustador.setDateFormat(formatoFechaCalendario());
				calendarfechaContactoAjustador.show();
			}
			else
				if (calendarfechaContactoAjustador.isVisible())
					calendarfechaContactoAjustador.hide();
				else
					calendarfechaContactoAjustador.show();
		    break;
	//------------------------------
	
//------------------------------
	case '3':
	if (calendarfechaLlegadaAjustador==null && calendarfechaLlegadaAjustador==undefined){
		calendarfechaLlegadaAjustador = new dhtmlxCalendarObject('fechaLlegadaAjustador', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaLlegadaAjustador.setDateFormat(formatoFechaCalendario());
		calendarfechaLlegadaAjustador.show();
	}
	else
		if (calendarfechaLlegadaAjustador.isVisible())
			calendarfechaLlegadaAjustador.hide();
		else
			calendarfechaLlegadaAjustador.show();
	 break;
//------------------------------

//------------------------------
	case '4':
	if (calendarfechaInspeccionAjustador==null && calendarfechaInspeccionAjustador==undefined){
		calendarfechaInspeccionAjustador = new dhtmlxCalendarObject('fechaInspeccionAjustador', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaInspeccionAjustador.setDateFormat(formatoFechaCalendario());
		calendarfechaInspeccionAjustador.show();
	}
	else
		if (calendarfechaInspeccionAjustador.isVisible())
			calendarfechaInspeccionAjustador.hide();
		else
			calendarfechaInspeccionAjustador.show();
	 break;
//------------------------------

//------------------------------
	case '5':
	if (calendarfechaPreliminar==null && calendarfechaPreliminar==undefined){
		calendarfechaPreliminar = new dhtmlxCalendarObject('fechaPreliminar', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaPreliminar.setDateFormat(formatoFechaCalendario());
		calendarfechaPreliminar.show();
	}
	else
		if (calendarfechaPreliminar.isVisible())
			calendarfechaPreliminar.hide();
		else
			calendarfechaPreliminar.show();
	 break;
//------------------------------


//------------------------------
	case '6':
	if (calendarfechaDocumento==null && calendarfechaDocumento==undefined){
		calendarfechaDocumento = new dhtmlxCalendarObject('fechaDocumento', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaDocumento.setDateFormat(formatoFechaCalendario());
		calendarfechaDocumento.show();
	}
	else
		if (calendarfechaDocumento.isVisible())
			calendarfechaDocumento.hide();
		else
			calendarfechaDocumento.show();
	 break;
//------------------------------

//------------------------------
	case '7':
	if (calendarfechaInformeFinal==null && calendarfechaInformeFinal==undefined){
		calendarfechaInformeFinal = new dhtmlxCalendarObject('fechaInformeFinal', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaInformeFinal.setDateFormat(formatoFechaCalendario());
		calendarfechaInformeFinal.show();
	}
	else
		if (calendarfechaInformeFinal.isVisible())
			calendarfechaInformeFinal.hide();
		else
			calendarfechaInformeFinal.show();
	 break;
//------------------------------

//------------------------------
	case '8':
	if (calendarfechaTerminacionSiniestro==null && calendarfechaTerminacionSiniestro==undefined){
		calendarfechaTerminacionSiniestro = new dhtmlxCalendarObject('fechaTerminacionSiniestro', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaTerminacionSiniestro.setDateFormat(formatoFechaCalendario());
		calendarfechaTerminacionSiniestro.show();
	}
	else
		if (calendarfechaTerminacionSiniestro.isVisible())
			calendarfechaTerminacionSiniestro.hide();
		else
			calendarfechaTerminacionSiniestro.show();
	 break;
//------------------------------

//------------------------------
	case '9':
	if (calendarfechaCerrado==null && calendarfechaCerrado==undefined){
		calendarfechaCerrado = new dhtmlxCalendarObject('fechaCerrado', true, {isMonthEditable: false, isYearEditable: false});
		calendarfechaCerrado.setDateFormat(formatoFechaCalendario());
		calendarfechaCerrado.show();
	}
	else
		if (calendarfechaCerrado.isVisible())
			calendarfechaCerrado.hide();
		else
			calendarfechaCerrado.show();
	 break;
//------------------------------

//------------------------------
	case '10':
	if (calendarDatPolizafecPago==null && calendarDatPolizafecPago==undefined){
		calendarDatPolizafecPago = new dhtmlxCalendarObject('fecPago', true, {isMonthEditable: false, isYearEditable: false});
		calendarDatPolizafecPago.setDateFormat(formatoFechaCalendario());
		calendarDatPolizafecPago.show();
	}
	else
		if (calendarDatPolizafecPago.isVisible())
			calendarDatPolizafecPago.hide();
		else
			calendarDatPolizafecPago.show();
	 break;
//------------------------------

//------------------------------
	case '11':
	if (calendarDatPolizafecEmision==null && calendarDatPolizafecEmision==undefined){
		calendarDatPolizafecEmision = new dhtmlxCalendarObject('fecEmision', true, {isMonthEditable: false, isYearEditable: false});
		calendarDatPolizafecEmision.setDateFormat(formatoFechaCalendario());
		calendarDatPolizafecEmision.show();
	}
	else
		if (calendarDatPolizafecEmision.isVisible())
			calendarDatPolizafecEmision.hide();
		else
			calendarDatPolizafecEmision.show();
	 break;
//------------------------------

//------------------------------
	case '12':
	if (calendarDatPolizafecVigencia==null && calendarDatPolizafecVigencia==undefined){
		calendarDatPolizafecVigencia = new dhtmlxCalendarObject('fecVigencia', true, {isMonthEditable: false, isYearEditable: false});
		calendarDatPolizafecVigencia.setDateFormat(formatoFechaCalendario());
		calendarDatPolizafecVigencia.show();
	}
	else
		if (calendarDatPolizafecVigencia.isVisible())
			calendarDatPolizafecVigencia.hide();
		else
			calendarDatPolizafecVigencia.show();
	 break;
//------------------------------

//------------------------------
	case '13':
	if (calendarDatPolizaal==null && calendarDatPolizaal==undefined){
		calendarDatPolizaal = new dhtmlxCalendarObject('al', true, {isMonthEditable: false, isYearEditable: false});
		calendarDatPolizaal.setDateFormat(formatoFechaCalendario());
		calendarDatPolizaal.show();
	}
	else
		if (calendarDatPolizaal.isVisible())
			calendarDatPolizaal.hide();
		else
			calendarDatPolizaal.show();
	 break;
//------------------------------

//------------------------------
case '14':
	if (calendarPreEspFecNoti==null && calendarPreEspFecNoti==undefined){
		calendarPreEspFecNoti = new dhtmlxCalendarObject('fechaNotificacion', true, {isMonthEditable: false, isYearEditable: false});
		calendarPreEspFecNoti.setDateFormat(formatoFechaCalendario());
		calendarPreEspFecNoti.show();
	}
	else
		if (calendarPreEspFecNoti.isVisible())
			calendarPreEspFecNoti.hide();
		else
			calendarPreEspFecNoti.show();
	 break;
//------------------------------

//------------------------------
case '15':
	if (calendarPreEspPlaRecl==null && calendarPreEspPlaRecl==undefined){
		calendarPreEspPlaRecl = new dhtmlxCalendarObject('plazoReclamacion', true, {isMonthEditable: false, isYearEditable: false});
		calendarPreEspPlaRecl.setDateFormat(formatoFechaCalendario());
		calendarPreEspPlaRecl.show();
	}
	else
		if (calendarPreEspPlaRecl.isVisible())
			calendarPreEspPlaRecl.hide();
		else
			calendarPreEspPlaRecl.show();
	 break;
//------------------------------


//------------------------------
case '16':
	if (calendarRepoSiniFechSiniestro==null && calendarRepoSiniFechSiniestro==undefined){
		calendarRepoSiniFechSiniestro = new dhtmlxCalendarObject('fechaSiniestro', true, {isMonthEditable: false, isYearEditable: false});
		calendarRepoSiniFechSiniestro.setDateFormat(formatoFechaCalendario());
		calendarRepoSiniFechSiniestro.show();
	}
	else
		if (calendarRepoSiniFechSiniestro.isVisible())
			calendarRepoSiniFechSiniestro.hide();
		else
			calendarRepoSiniFechSiniestro.show();
	 break;
//------------------------------

//------------------------------
case '17':
	if (calendarListarFechaSiniestro==null && calendarListarFechaSiniestro==undefined){
		calendarListarFechaSiniestro = new dhtmlxCalendarObject('listarFechaSiniestro', true, {isMonthEditable: false, isYearEditable: false});
		calendarListarFechaSiniestro.setDateFormat(formatoFechaCalendario());
		calendarListarFechaSiniestro.show();
	}
	else
		if (calendarListarFechaSiniestro.isVisible())
			calendarListarFechaSiniestro.hide();
		else
			calendarListarFechaSiniestro.show();
	 break;
//------------------------------

	}//end switch



}


function cargarCombo(idComponente,resultadoConsulta,titulo,soloLectura,inhabilitar,filtro){
	// "[[1, 1111], [2, 2222], [3, 3333], [4, 4444], [5, 5555]]";
	
	var llenarOpciones = resultadoConsulta;
	var instanciaComponente = new dhtmlXCombo(idComponente,titulo, 200);
	instanciaComponente.addOption(llenarOpciones);

	instanciaComponente.readonly(soloLectura);
	instanciaComponente.enableFilteringMode(filtro);
	instanciaComponente.disable(inhabilitar);
}
function comboCoberturaSumaAsegurada(comboOrigen,idSeleccionadoComboActual){
	
	if(comboOrigen == 'tipoSumaAsegurada'){
		if (idSeleccionadoComboActual > 1){
			desplegarControl('celdaSumaAseguradaEtiqueta',true);
			desplegarControl('celdaSumaAseguradaSelect',true);
			if(idSeleccionadoComboActual == 3){
				desplegarControl('datosConfiguracionSublimite',true);
			}
			else{
				desplegarControl('datosConfiguracionSublimite',false);
			}
			
			comboCoberturaSumaAsegurada('tipoValorSumaAsegurada',document.getElementById('tipoValorSA').selectedIndex);
		}
		else{
			desplegarControl('celdaSumaAseguradaEtiqueta',false);
			desplegarControl('celdaSumaAseguradaSelect',false);
			desplegarControl('datosConfiguracionSublimite',false);
		}
	}
	else if (comboOrigen == 'tipoValorSumaAsegurada'){
		
		if(idSeleccionadoComboActual == 3){
			//ES derivada
			var indiceComboTipoSA = document.getElementsByName('claveTipoSumaAsegurada')[0].selectedIndex;
			if(indiceComboTipoSA == 0 || indiceComboTipoSA == 1 || indiceComboTipoSA == 3){
				//Es basica o sublimite, permite capturar campos
				desplegarControl('celdaSumaAseguradaEtiqueta',true);
				desplegarControl('celdaSumaAseguradaSelect',true);
				desplegarControl('datosConfiguracionSublimite',true);
			}
			else if (indiceComboTipoSA == 2){
				//ES amparada, permite capturar idcoberturadependencia
				desplegarControl('celdaSumaAseguradaEtiqueta',true);
				desplegarControl('celdaSumaAseguradaSelect',true);
				desplegarControl('datosConfiguracionSublimite',false);
			}
		}
	}
}

function desplegarControl(idControl,visible){
	var visibilidad = '';
	
	if(visible == true){
		visibilidad = 'visible';
	}
	else{
		visibilidad = 'hidden'
	}
	if(idControl != null && document.getElementById(idControl) != null){
		document.getElementById(idControl).style.visibility = visibilidad;
	}
}

function mostrarDatosInciso(id, modificar, numeroInciso, soloLectura){
	// ,'datosInciso'
	new Ajax.Request("/MidasWeb/cotizacion/inciso/mostrarDatosInciso.do", {
		method : "post",
		asynchronous : false,
		parameters : "cotizacionId=" + id + "&modificar=" + modificar + "&numeroInciso=" + numeroInciso + "&soloLectura=" + soloLectura,
		onSuccess : function(transport) {
			renderDatosInciso(transport.responseXML);
		} // End of onSuccess
	});	
}	
function renderDatosInciso(doc){
	var data = doc.getElementsByTagName("datosinciso")[0].firstChild.nodeValue;
	var datosInciso = $('datosInciso');
	datosInciso.innerHTML = data;
}

function mostrarDatosSubInciso(modificar, soloLectura, tipoEndoso){
	var idToCotizacion = document.getElementById("idToCotizacion").value;
	var numeroInciso = document.getElementById("numeroInciso").value;
	var idToSeccion = document.getElementById("idToSeccion").value;
	var numeroSubInciso = document.getElementById("numeroSubInciso").value;
	new Ajax.Request("/MidasWeb/cotizacion/subinciso/mostrarDatosSubInciso.do", {
		method : "post",
		asynchronous : false,
		parameters : "idToCotizacion=" + idToCotizacion + "&numeroInciso=" + numeroInciso + "&idToSeccion=" + idToSeccion + 
		"&modificar=" + modificar + "&soloLectura=" + soloLectura + "&numeroSubInciso=" + numeroSubInciso + "&tipoEndoso=" + tipoEndoso,
		onSuccess : function(transport) {
			renderDatosSubInciso(transport.responseXML);
		} // End of onSuccess
	});	
}
function renderDatosSubInciso(doc){
	var data = doc.getElementsByTagName("datossubinciso")[0].firstChild.nodeValue;
	var datosInciso = $('datosSubInciso');
	datosInciso.innerHTML = data;
}

function cargaTabDatosGeneralesCot(){
	sendRequest(null,'/MidasWeb/cotizacion/mostrarDatosGenerales.do?id='+$('idToCotizacion').value,'contenido_datosGenerales','inicializaObjetosEdicionCotizacion(' + $('idToCotizacion').value + ')');
}
function cargaTabDatosGeneralesCotEndoso(){
	sendRequest(null,'/MidasWeb/cotizacion/endoso/mostrarDatosGenerales.do?id='+$('idToCotizacion').value,'contenido_datosGenerales','inicializaObjetosEdicionCotizacion(' + $('idToCotizacion').value + ')');
}
function cargarComponentesModificarLinea(){
	initParticipacionesPEGrids();
	mostrarParticipacionesPEGrids();
	initParticipacionesCPGrids();
	mostrarParticipacionesCPGrids()
}

function editarPersona(){
	$('areaLectura').style.display = 'none';
	$('areaEdicion').style.display = 'block';
}
function ocultaBoton(valor){
	if(valor === '1'){
		$('b_buscar').style.display = 'none';
	}else{
		$('b_buscar').style.display = 'block';		
	}
}
function cambiaFiltros(valor){
	if(valor === '1'){
		$('filtrosPersonaFisica').style.display = 'block';
		$('filtrosPersonaMoral').style.display = 'none';
	}else{
		$('filtrosPersonaMoral').style.display = 'block';		
		$('filtrosPersonaFisica').style.display = 'none';		
	}	
}

function setTabIncisosODT(){	
	dhx_init_tabbars();
	ordenTrabajoTabBar.setTabActive('resumenIncisos');
}

function setTabIncisosCOT(){	
	dhx_init_tabbars();
	cotizaciontabbar.setTabActive('resumenIncisos');
}

function mostrarAgregarClienteCotizacion(idPadre,codigoPersona,idCliente,cambiaA){
	var url = "/MidasWeb/cliente/mostrarAsociarClienteCotizacion.do?idCotizacion="+idPadre+"&codigoPersona="+codigoPersona+"&idCliente="+idCliente+"&cambiaA="+cambiaA;
	if (codigoPersona == '1')
		parent.accordionOT.cells("a2").attachURL(url);
	else if (codigoPersona=='3')
		parent.accordionOT.cells("a1").attachURL(url);
}

function agregarPersonaCotizacion(clienteForm){
	
	new Ajax.Request('/MidasWeb/cliente/asociarClienteCotizacion.do',{
		method : "post", asynchronous : false,
		parameters : (clienteForm !== undefined && clienteForm !== null) ? $(clienteForm).serialize(true) : null, asynchronous : false,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onSuccess : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();

			var tipo = document.getElementById("codigoPersona").value;
			
			if (_isIE) {
				
				var idPadre = document.getElementById("idPadre").value;
				var formCliente = parent.document.createElement("DIV");
				formCliente.innerHTML = transport.responseText; 
				var tipoMensaje = formCliente.document.getElementById("tipoMensaje").value;
				var mensaje = formCliente.document.getElementById("mensaje").value;
				mostrarVentanaMensaje(tipoMensaje, mensaje, null);
				
				if (tipo == '3'){
					parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idPadre+"&descripcionPadre=cotizacion&tipoDireccion=3&mensaje=" + mensaje + "&tipoMensaje=" + tipoMensaje);
				} else if (tipo == '1') {
					parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idPadre+"&descripcionPadre=cotizacion&tipoDireccion=1&mensaje=" + mensaje + "&tipoMensaje=" + tipoMensaje);
				}
				
			} else {
				if (tipo == '3'){
					parent.accordionOT.cells("a1").win._frame.contentDocument.getElementById("formularioCliente").innerHTML = transport.responseText;
				} else if (tipo == '1') {
					parent.accordionOT.cells("a2").win._frame.contentDocument.getElementById("formularioCliente").innerHTML = transport.responseText;
				}
				
				procesarRespuesta(null);
			}

		}
	});
}

function copiarCotizacion(idToCotizacion){
	if (confirm("Desea duplicar la cotizacion seleccionada??")){
		sendRequest(null,"/MidasWeb/cotizacion/copiarCotizacion.do?idToCotizacion="+idToCotizacion,"contenido",null);
	}
}
/** Fin de Funciones de cotizacion **/

function sendRequestParticipacionFacultativo(action, idTmContratoFacultativo, idTdContratoFacultativo, idTdParticipacionFacultativo){
	url = "";
	funcionJS = "void(0)";
	switch(action){
		case 1 : url = "/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do?id="+id; 
	 			break;
		case 2 : url = "/MidasWeb/contratofacultativo/participacionfacultativa/borrar.do?idTdParticipacionFacultativo="+idTdParticipacionFacultativo;
		if (confirm('\u00BFDesea eliminar la participaci\u00F3n seleccionada?'))
				new Ajax.Request(url, {
					method : "post", asynchronous : false, 
					onSuccess : function(transport) {
						procesarRespuestaXml(transport.responseXML,null);
						recargaCoberturasFacultativaTree(idTmContratoFacultativo, idTdContratoFacultativo);
						mostrarGridParticipacionFacultativo(idTdContratoFacultativo);
					} // End of onSuccess
				});
				break;
	}
}

function sendRequestParticipacionCorredorFacultativo(action,idTmContratoFacultativo, idTdContratoFacultativo, idTdParticipacionFacultativo, idTdParticipacionCorredorFacultativo){
	url = "";
	funcionJS = "void(0)";
	switch(action){
		case 1 : url = "/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do?id="+id; 
	 			break;
		case 2 : url = "/MidasWeb/contratofacultativo/participacionfacultativa/borrarParticipacionCorredor.do?idTdParticipacionCorredorFacultativo="+idTdParticipacionCorredorFacultativo; 
		if (confirm('\u00BFDesea eliminar la participaci\u00F3n seleccionada?'))
				new Ajax.Request(url, {
					method : "post", asynchronous : false, 
					onSuccess : function(transport) {
						procesarRespuestaXml(transport.responseXML,null);
						recargaCoberturasFacultativaTree(idTmContratoFacultativo, idTdContratoFacultativo);
						mostrarGridParticipacionFacultativoCorredor(idTdParticipacionFacultativo);
					} // End of onSuccess
				});
				break;
	}
}

var accordionIgualaciones = null;
function inicializaAccordionIgualaciones(idToCotizacion, index) {
	accordionIgualaciones = new dhtmlXAccordion("accordionIgualaciones");
	accordionIgualaciones.addItem("a1", "Igualaci&#243;n de Primas");
	accordionIgualaciones.addItem("a2", "Igualaci&#243;n de Cuotas");
	accordionIgualaciones.cells("a1").attachURL("/MidasWeb/cotizacion/igualacion/mostrarIgualacionPrimas.do?id=" + idToCotizacion);
	//accordionIgualaciones.cells("a2").attachURL("/MidasWeb/cotizacion/igualacion/mostrarIgualacionCuotas.do?id=" + idToCotizacion);
	accordionIgualaciones.attachEvent("onActive", function(itemId, state){
		//if(itemId == "a2" && accordionIgualaciones.cells(itemId).win._frame == null) {
			accordionIgualaciones.cells("a2").attachURL("/MidasWeb/cotizacion/igualacion/mostrarIgualacionCuotas.do?id=" + idToCotizacion);
		//}
    });
	if(index != null && index == 2) {
		accordionIgualaciones.openItem("a2");
		accordionIgualaciones.cells("a2").attachURL("/MidasWeb/cotizacion/igualacion/mostrarIgualacionCuotas.do?id=" + idToCotizacion);
	} else {
		accordionIgualaciones.openItem("a1");
	}
	accordionIgualaciones.setEffect(true);
}

function inicializaAccordionIgualacionSoloLectura(idToCotizacion, index){
	
	var urlIgualacionPrimas = "/MidasWeb/cotizacion/igualacion/mostrarIgualacionPrimas.do?id=" + idToCotizacion+"&soloLectura=true";
	var urlIgualacionCuotas = "/MidasWeb/cotizacion/igualacion/mostrarIgualacionCuotas.do?id=" + idToCotizacion+"&soloLectura=true";
	
	accordionIgualaciones = new dhtmlXAccordion("accordionIgualaciones");
	accordionIgualaciones.addItem("a1", "Igualaci&#243;n de Primas");
	accordionIgualaciones.addItem("a2", "Igualaci&#243;n de Cuotas");
	accordionIgualaciones.cells("a1").attachURL(urlIgualacionPrimas);
	
	accordionIgualaciones.attachEvent("onActive", function(itemId, state){
			accordionIgualaciones.cells("a2").attachURL(urlIgualacionCuotas);
    });
	if(index != null && index == 2) {
		accordionIgualaciones.openItem("a2");
		accordionIgualaciones.cells("a2").attachURL(urlIgualacionCuotas);
	} else {
		accordionIgualaciones.openItem("a1");
	}
	accordionIgualaciones.setEffect(true);
}

function llenarComboCobertura(obj, child, action) {
	new Ajax.Request(action, {
		method : "post",
		asynchronous : false,
		parameters : "id=" + obj[obj.selectedIndex].value,
		onSuccess : function(transport) {
		loadComboCobertura(child, transport.responseXML);
		} // End of onSuccess
	});
}

function loadComboCobertura(child, doc) {
	var selectedCombo = $(child);
	if (selectedCombo != null) {
		removeAllOptionsExtCobertura(selectedCombo);
		var items = doc.getElementsByTagName("item");
		for ( var x = 0; x < items.length; x++) {
			var item = items[x];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(selectedCombo, text, value);
		} // End of for
	} // End of if
}

function removeAllOptionsExtCobertura(selectbox) {
	var size = selectbox.length;
	for ( var i = 0; i < size; i++) {
		selectbox.remove(0);
	}
}
function filtrarPorInciso(igualacionForm){
	if(document.getElementById('aplicoPrimerRiesgo').value == 'true') {
			if (!confirm("Existen coberturas en primer riesgo. \U00FDesea continuar?")) {
				return false;
			}
	}
	new Ajax.Request('/MidasWeb/cotizacion/igualacion/mostrarIgualacionPrimas.do', {
		method : "post",
		encoding : "UTF-8",
		parameters : (igualacionForm !== undefined && igualacionForm !== null) ? $(igualacionForm).serialize(true) : null,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
			// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
			parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;
			parent.eval('procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ')'); 
		} // End of onSuccess
		
	});
	return true;	
}

function igualacionPrimaRequest(igualacionForm){
	if (igualacionForm.primaNeta.value == '' || parseFloat(igualacionForm.primaNeta.value) <= 0) {
		alert('Debe introducir una Prima Neta positiva');
		return false;
	} else {
		if(document.getElementById('aplicoPrimerRiesgo').value == 'true') {
			if (!confirm("Existen coberturas en primer riesgo. \u00BFDesea continuar?")) {
				return false;
			}
		}
		//parent.sendRequest(igualacionForm, '/MidasWeb/cotizacion/igualacion/igualarPrimaNeta.do', 'contenido_igualacion', 'procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ')');
		new Ajax.Request('/MidasWeb/cotizacion/igualacion/igualarPrimaNeta.do', {
			method : "post",
			encoding : "UTF-8",
			parameters : (igualacionForm !== undefined && igualacionForm !== null) ? $(igualacionForm).serialize(true) : null,
			onCreate : function(transport) {
				parent.totalRequests = parent.totalRequests + 1;
				parent.showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				parent.totalRequests = parent.totalRequests - 1;
				parent.hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
				// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
				if (navigator.appName.indexOf("Microsoft") != -1) {
					var formIgualacion = parent.document.createElement("DIV");
					formIgualacion.innerHTML = transport.responseText; 
					var desplegarDetalle = formIgualacion.document.getElementById("desplegarDetalle").outerHTML;
					
					parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = desplegarDetalle;
				} else {
					parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;	
				}
				parent.eval('procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ')');
					
			} // End of onSuccess
			
		});
		return true;
	}
}
function eliminaIgualacionPrimaRequest(igualacionForm){
	//parent.sendRequest(igualacionForm, '/MidasWeb/cotizacion/igualacion/igualarPrimaNeta.do', 'contenido_igualacion', 'procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ')');
	new Ajax.Request('/MidasWeb/cotizacion/igualacion/eliminaIgualacionPrima.do', {
		method : "post",
		encoding : "UTF-8",
		parameters : (igualacionForm !== undefined && igualacionForm !== null) ? $(igualacionForm).serialize(true) : null,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
			// pNextFunction = "refrescaMensajeUsuario();"+ pNextFunction;
			parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("detalle").innerHTML = transport.responseText;
			parent.eval('procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ')'); 
		} // End of onSuccess
		
	});
	return true;	
}
function procesarRespuestaIgualacion(idToCotizacion, index) {
	var mensaje = parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("mensaje").value;
	var tipoMensaje = parent.accordionIgualaciones.cells("a1").win._frame.contentWindow.document.getElementById("tipoMensaje").value;
	if(tipoMensaje != '' && mensaje != '') {
		parent.mostrarVentanaMensaje(tipoMensaje, mensaje, null);
	}
}

function igualacionCuotaRequest(igualacionForm){
	if (igualacionForm.primaNeta.value == '') {
		alert('Debe introducir una Cuota positiva');
		return false;
	} else {
		if(document.getElementById('aplicoPrimerRiesgo').value == 'true') {
			if (!confirm("Existen coberturas en primer riesgo. \u00BFDesea continuar?")) {
				return false;
			}
		}
		parent.sendRequest(igualacionForm, '/MidasWeb/cotizacion/igualacion/igualarCuota.do', 'contenido_igualacion', 'procesarRespuestaIgualacion(' + igualacionForm.idToCotizacion.value + ', 2)');
		return true;
	}
}

function mostrarListaIncisos(idCotizacion, numeroInciso){
	dhx_init_tabbars();
	creaArbolOrdenesDeTrabajo(null);
	ordenTrabajoTabBar.setTabActive('resumenIncisos');
	alert("El inciso "+numeroInciso+" ha sido separado de la cotizaci\u00F3n "+idCotizacion);
}

function validarModificarOrdenTrabajo(){
	var errores = $("errores");
	if (errores === null || errores.innerHTML === ""){
		sendRequest(null,'/MidasWeb/cotizacion/mostrarODT.do?id='+document.getElementById("idToCotizacion").value,'contenido','dhx_init_tabbars();creaArbolOrdenesDeTrabajo(null);');
	}else{
		cerrarObjetosEdicionOT();
		inicializaObjetosEdicionOT(document.getElementById("idToCotizacion").value);
	}
}

function cargaCotizacionTextoAdicional(idCotizacion){
	creaArbolCotizacion(idCotizacion);
	dhx_init_tabbars();
	cotizaciontabbar.setTabActive('textosAdicionalesPorAutorizar');
}
function guardarCotizacionDatosGeneralesEndoso(){
	//sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/guardarOrdenTrabajo.do','contenido_datosGenerales','inicializaObjetosEdicionOT('+$("idToCotizacion").value)+')';
	if (document.getElementById('idTcMoneda').value == "")
		alert("Seleccione un tipo de moneda.");
	else{
		new Ajax.Request('/MidasWeb/endoso/guardarOrdenTrabajo.do', {
			method : "post", encoding : "UTF-8", asinchronous:false,
			parameters : (document.cotizacionForm !== undefined && document.cotizacionForm !== null) ? $(document.cotizacionForm).serialize(true) : null,
			onCreate : function(transport) {
				totalRequests = totalRequests + 1;
				showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				totalRequests = totalRequests - 1;
				hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
					$('contenido_datosGenerales').innerHTML = transport.responseText;
					var errores = $("errores");
					if (errores === null || errores.innerHTML === ""){
						//while ( document.getElementById('accordionCotizacion') == null) {delay(500);}
						inicializaObjetosEdicionCotizacion( $('idToCotizacion').value );
					}else{
						getMonedasTipoPoliza(document.getElementById('selectTipoPoliza'), 'idTcMoneda');
						inicializaObjetosEdicionOT(document.getElementById("idToCotizacion").value);
					}
					mostrarVentanaMensaje(document.getElementById('tipoMensaje').value,document.getElementById('mensaje').value,null);
			} // End of onSuccess
		});
	}
}
function guardarCotizacionDatosGenerales(){
	//sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/guardarOrdenTrabajo.do','contenido_datosGenerales','inicializaObjetosEdicionOT('+$("idToCotizacion").value)+')';
	if (document.getElementById('idTcMoneda').value == "")
		alert("Seleccione un tipo de moneda.");
	else{
		new Ajax.Request('/MidasWeb/cotizacion/guardarOrdenTrabajo.do', {
			method : "post", encoding : "UTF-8", asinchronous:false,
			parameters : (document.cotizacionForm !== undefined && document.cotizacionForm !== null) ? $(document.cotizacionForm).serialize(true) : null,
			onCreate : function(transport) {
				totalRequests = totalRequests + 1;
				showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				totalRequests = totalRequests - 1;
				hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
					$('contenido_datosGenerales').innerHTML = transport.responseText;
					var errores = $("errores");
					if (errores === null || errores.innerHTML === ""){
						//while ( document.getElementById('accordionCotizacion') == null) {delay(500);}
						inicializaObjetosEdicionCotizacion( $('idToCotizacion').value );
					}else{
						getMonedasTipoPoliza(document.getElementById('selectTipoPoliza'), 'idTcMoneda');
						inicializaObjetosEdicionOT(document.getElementById("idToCotizacion").value);
					}
					mostrarVentanaMensaje(document.getElementById('tipoMensaje').value,document.getElementById('mensaje').value,null);					
			} // End of onSuccess
		});
	}
}

function setCodigoPostal(idColonia){
	document.getElementById('codigoPostal').value = idColonia.substring(0,5);
}

function guardarComplementarCotizacion() {
	var errores = validarComplementarCotizacion();
	if (errores == "") {
		sendRequest(document.complementarCotizacionForm,'/MidasWeb/cotizacion/complementar/guardar.do', 
				'contenido','inicializarComplementarCotizacion(' + document.complementarCotizacionForm.idToCotizacion.value + ');existenErrores(\'procesarRespuesta(null)\')');
	}
	else {
		mostrarVentanaMensaje("10", errores);
	}
}

function autorizarOmitirDocumentosComplementar() {
	sendRequest(document.complementarCotizacionForm,'/MidasWeb/cotizacion/complementar/autorizarOmitirDocumentos.do',
			'contenido','inicializarComplementarCotizacion(' + document.complementarCotizacionForm.idToCotizacion.value + '); procesarRespuesta(null)');
}

function rechazarOmitirDocumentosComplementar() {
	sendRequest(document.complementarCotizacionForm,'/MidasWeb/cotizacion/complementar/rechazarOmitirDocumentos.do',
			'contenido','inicializarComplementarCotizacion(' + document.complementarCotizacionForm.idToCotizacion.value + '); procesarRespuesta(null)');
}

function emitirComplementarCotizacion() {
	sendRequest(document.complementarCotizacionForm,'/MidasWeb/cotizacion/complementar/emitir.do',
			'contenido','inicializarComplementarCotizacion(' + document.complementarCotizacionForm.idToCotizacion.value + '); procesarRespuestaEmision(null)');
}

function solicitarAutorizacion() {
	sendRequest(document.complementarCotizacionForm,'/MidasWeb/cotizacion/complementar/solicitarAutorizacion.do',
			'contenido','inicializarComplementarCotizacion(' + document.complementarCotizacionForm.idToCotizacion.value + '); procesarRespuesta(null);');	
}

function inicializarComplementarCotizacion(idToCotizacion) {
	if(idToCotizacion != null && idToCotizacion != 0) {
		inicializaAccordionComplementar(idToCotizacion);
	} else {
		inicializaAccordionComplementar($(complementarCotizacionForm).idToCotizacion.value);
	}
}

function inicializaAccordionComplementar(idToCotizacion){
	accordionOT = new dhtmlXAccordion("accordionOT");
	accordionOT.addItem("a1", "Datos del Asegurado");
	accordionOT.addItem("a2", "Datos del Contratante");
	accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
	accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idToCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
	accordionOT.openItem("a1");
	accordionOT.setEffect(true);
}

function validarComplementarCotizacion() {
	var errores = ""; //cambiarlo por array para no tener que poner br.
	var diasGracia = document.complementarCotizacionForm.diasGracia.value;
	var diasGraciaDefault = document.complementarCotizacionForm.diasGraciaDefault.value;

	if (diasGracia != null && diasGracia != "") {
		var diasGraciaNumber = parseInt(diasGracia);
		var diasGraciaDefaultNumber = parseInt(diasGraciaDefault);
		
		if (diasGraciaNumber <= 0) {
			errores += "Los dias de gracia deben ser mayor que 0<br\>";
		}
		//13/06/2011 Se permite registro de dias de gracia mayor al default
//		if (diasGraciaNumber > diasGraciaDefaultNumber) {
//			errores += "Los dias de gracia deben ser menor o igual que el valor de default: " + 
//			diasGraciaDefault + "<br\>";
//		}
	}
	else {
		errores += "Dias de gracia es requerido<br\>";
	}

	return errores;
}

function mostrarEmisorComplementar(idToCotizacion, origen) {
	 sendRequest(null,'/MidasWeb/cotizacion/complementar/mostrarEmisorComplementar.do?id=' + idToCotizacion + '&origen=' + origen,
			'contenido','inicializarComplementarCotizacion(' + idToCotizacion + ');');
}

function mostrarAutorizadorComplementar(idToCotizacion) {
	 sendRequest(null,'/MidasWeb/cotizacion/complementar/mostrarAutorizadorComplementar.do?id=' + idToCotizacion,
				'contenido','inicializarComplementarCotizacion(' + idToCotizacion + ');');
}

function regresarListarCotizaciones() {
	sendRequest(document.cotizacionForm, '/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do', 
			'contenido', 'null');
}

function verificarContratoRegistrarIngresosReaseguro(modificarAgregarIngresoReaseguro){
	var monto;
	var moneda;
	var notificarReaseguro = 0;
	var notificarSiniestro = 0;

	if ($('notificarReaseguro').checked == true)
		notificarReaseguro = 1;
	if ($('notificarSiniestro').checked == true)
		notificarSiniestro = 1;
	if(modificarAgregarIngresoReaseguro == 1){
		fromCurrencyToFloat($('montoIngreso'));
		monto = document.getElementById("montoIngreso").value;
		moneda = document.getElementById("moneda").value;
	}else {
		fromCurrencyToFloat($('montoIngresoM'));
		monto = document.getElementById("montoIngresoM").value;
		moneda = document.getElementById("monedaM").value;
	}
	var referencia = $('referencia').value;
	var numCuenta = $('numeroCuenta').value;
	var fechaIngreso = $('fechaIngreso').value;
	if(monto == "" || referencia == "" || numCuenta == "" || fechaIngreso == ""){
		if(modificarAgregarIngresoReaseguro == 1){
			$('montoIngreso').value = formatCurrency4Decs($('montoIngreso').value);
		}else {
			$('montoIngresoM').value = formatCurrency4Decs($('montoIngresoM').value);
		}
		alert("Favor de Llenar Todos los Campos");
	}else if(monto != parseFloat(monto)){
		document.getElementById("montoIngreso").setAttribute("style", "color:red");
	}else if(numCuenta != parseInt(numCuenta)){
		document.getElementById("numeroCuenta").setAttribute("style", "color:red");
	}else{
		document.getElementById("montoIngreso").setAttribute("style", "color:black");
		document.getElementById("numeroCuenta").setAttribute("style", "color:black");
		if(modificarAgregarIngresoReaseguro == 1){
			sendRequest(document.ingresosReaseguroForm, '/MidasWeb/contratos/ingresos/agregarIngresosReaseguro.do', null, "parent.dhxWins.window('Ingreso').close()");
		}else{
			sendRequest(document.ingresosReaseguroForm, '/MidasWeb/contratos/ingresos/modificarIngresosReaseguro.do', null, "parent.dhxWins.window('ModificarIngreso').close()");
		}
	}
}

function ocultarReporteMovimientos(opcionReporteMovimiento){
	if(opcionReporteMovimiento == '1' || opcionReporteMovimiento == '2' || opcionReporteMovimiento == '3' || opcionReporteMovimiento == '4'|| opcionReporteMovimiento == '5'){
		document.getElementById('seleccionarPeriodo').style.display = 'none';
		document.getElementById('monedaCambio').style.display = 'none';
		document.getElementById('movimientoMoneda').style.display = '';
	}else if(opcionReporteMovimiento == '6'){
		document.getElementById('seleccionarPeriodo').style.display = '';
		document.getElementById('monedaCambio').style.display = '';
		document.getElementById('movimientoMoneda').style.display = 'none';
	}
}

function desplegarErrorBorrarReasegurador(indicador){
	if(confirm("Est\u00e1 Seguro Que Desea Borrar El Reasegurador/Corredor")){
		var pNextFunction ;
		if(indicador == 2){
			pNextFunction = 'mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()';
		}else if (indicador == 1){
			pNextFunction = 'mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()';
		}
		targetId = 'contenido';
		fobj = document.participacionForm;
		new Ajax.Request("/MidasWeb/contratos/participacion/borrar.do", {
			method : "post",
			asynchronous : false,
			parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
					true) : null,
					onSuccess : function(transport) {
						var estatusLinea = obtenerEstatusLinea(transport.responseXML);
						if (estatusLinea == 'noBorrado')
							alert('El Reasegurado/Corredor No Pudo Ser Borrado');
						else
							alert('El Reasegurado/Corredor Ha Sido Borrado Correctamente');
						if(indicador == 2)
							sendRequest(document.participacionForm,'/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');
						else if (indicador == 1)
							sendRequest(document.participacionForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');
					} 
					// End of onSuccess
		});
	}
}

function creaArbolPolizaSoloLectura(parametros, numeroEndoso, onClickRequestParameters){
	cotizacionSoloLectura = true;
	// Modificando
	var cotizacionTree = dhtmlXTreeFromHTML("treeboxbox_tree");
	var idToCotizacion = -1;
	var contextoMenu = 'estructuraCotizacion';
	var funcion = 'manejadorArbolCotizacion';
	
	if ($('idToCotizacion') == null || $('idToCotizacion').value == '') {
		if (parametros != null && parametros > 0)
			idToCotizacion = parametros;
	} else {
		idToCotizacion = $('idToCotizacion').value;
	}

	cotizacionTree.setImagePath("/MidasWeb/img/csh_winstyle/");
	cotizacionTree.enableCheckBoxes(0);
	cotizacionTree.enableDragAndDrop(0);

	cotizacionTree.attachEvent("onClick",function(id) {
		if(cotizacionSoloLectura) {
			manejadorArbolCotizacionSoloLectura(splitIdArbol(id),cotizacionTree.getLevel(id), onClickRequestParameters);
		} else {
			manejadorArbolCotizacion(splitIdArbol(id),cotizacionTree.getLevel(id));
		}
	});	
	
	cotizacionTree.attachEvent("onXLS",function(id,m) {
		mostrarIndicadorCargaComps();
	});

	cotizacionTree.attachEvent("onXLE",function(id,m) {
    	ocultarIndicadorCargaComps();
    	cotizacionTree.openItem(idToCotizacion);
    });
	cotizacionTree.setXMLAutoLoading("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion + "&idToPoliza=" + parametros + "&numeroEndoso=" + numeroEndoso);
	cotizacionTree.loadXML("/MidasWeb/cotizacion/poblarTreeView.do?menu=" + contextoMenu + "&idToCotizacion=" + idToCotizacion + "&id=" + idToCotizacion + "&idToPoliza=" + parametros + "&numeroEndoso=" + numeroEndoso);
}


function modificarRPFDerechos(igualacionForm){				
		new Ajax.Request('/MidasWeb/cotizacion/modificarRPFDerechos.do', {
			method : "post",
			encoding : "UTF-8",
			parameters : (igualacionForm !== undefined && igualacionForm !== null) ? $(igualacionForm).serialize(true) : null,
			onCreate : function(transport) {
				parent.totalRequests = parent.totalRequests + 1;
				parent.showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				parent.totalRequests = parent.totalRequests - 1;
				parent.hideIndicator();
			}, // End of onComplete
			onSuccess : function(transport) {							
				actualizarRPFDerechos(transport.responseXML);
			} // End of onSuccess
			
		});
		return true;
}

function actualizarRPFDerechos(docXml){			
	var items = docXml.getElementsByTagName("item");		
	if(items!=null  && items.length>0){
		var item = items[0];														
		
		$("valorRPFEditable").value = item.getElementsByTagName("valorRPFEditable")[0].firstChild.nodeValue.trim();		
		$("porcentajeRPFSoloLectura").value = item.getElementsByTagName("porcentajeRPFSoloLectura")[0].firstChild.nodeValue.trim();		
		$("valorRPFSoloLectura").value = item.getElementsByTagName("valorRPFSoloLectura")[0].firstChild.nodeValue.trim();		
		$("porcentajeRPFEditable").value = item.getElementsByTagName("porcentajeRPFEditable")[0].firstChild.nodeValue.trim();		
		$("valorDerechos").value = item.getElementsByTagName("textoDerechos")[0].firstChild.nodeValue.trim();
		
		document.getElementById("rpf").innerHTML = item.getElementsByTagName("rpfResumen")[0].firstChild.nodeValue;
		document.getElementById("derechos").innerHTML = item.getElementsByTagName("derechos")[0].firstChild.nodeValue;		
		document.getElementById("iva").innerHTML = item.getElementsByTagName("iva")[0].firstChild.nodeValue;
		document.getElementById("primaTotal").innerHTML = item.getElementsByTagName("primaTotal")[0].firstChild.nodeValue;
		
		if(item.getElementsByTagName("claveAutRPF")[0].firstChild.nodeValue.trim()==="0"){
			document.getElementById("imgAutorizacionRPF").src = "/MidasWeb/img/blank.gif";
			document.getElementById("imgAutorizacionRPF").alt = "";
			document.getElementById("imgAutorizacionRPF").title = "";
		}else if(item.getElementsByTagName("claveAutRPF")[0].firstChild.nodeValue.trim()==="1"){
			document.getElementById("imgAutorizacionRPF").src = "/MidasWeb/img/ico_yel.gif";
			document.getElementById("imgAutorizacionRPF").alt = "Requiere autorizaci\u00F3n";
			document.getElementById("imgAutorizacionRPF").title = "Requiere autorizaci\u00F3n";
		}else if(item.getElementsByTagName("claveAutRPF")[0].firstChild.nodeValue.trim()==="7"){
			document.getElementById("imgAutorizacionRPF").src = "/MidasWeb/img/ico_green.gif";
			document.getElementById("imgAutorizacionRPF").alt = "Autorizado";
			document.getElementById("imgAutorizacionRPF").title = "Autorizado";
		}else if(item.getElementsByTagName("claveAutRPF")[0].firstChild.nodeValue.trim()==="8"){
			document.getElementById("imgAutorizacionRPF").src = "/MidasWeb/img/ico_red.gif";
			document.getElementById("imgAutorizacionRPF").alt = "Rechazado";
			document.getElementById("imgAutorizacionRPF").title = "Rechazado";
		}
		
		if(item.getElementsByTagName("claveAutDerechos")[0].firstChild.nodeValue.trim()==="0"){
			document.getElementById("imgAutorizacionDerechos").src = "/MidasWeb/img/blank.gif";
			document.getElementById("imgAutorizacionDerechos").alt = "";
			document.getElementById("imgAutorizacionDerechos").title = "";
		}else if(item.getElementsByTagName("claveAutDerechos")[0].firstChild.nodeValue.trim()==="1"){
			document.getElementById("imgAutorizacionDerechos").src = "/MidasWeb/img/ico_yel.gif";
			document.getElementById("imgAutorizacionDerechos").alt = "Requiere autorizaci\u00F3n";
			document.getElementById("imgAutorizacionDerechos").title = "Requiere autorizaci\u00F3n";
		}else if(item.getElementsByTagName("claveAutDerechos")[0].firstChild.nodeValue.trim()==="7"){
			document.getElementById("imgAutorizacionDerechos").src = "/MidasWeb/img/ico_green.gif";
			document.getElementById("imgAutorizacionDerechos").alt = "Autorizado";
			document.getElementById("imgAutorizacionDerechos").title = "Autorizado";
		}else if(item.getElementsByTagName("claveAutDerechos")[0].firstChild.nodeValue.trim()==="8"){
			document.getElementById("imgAutorizacionDerechos").src = "/MidasWeb/img/ico_red.gif";
			document.getElementById("imgAutorizacionDerechos").alt = "Rechazado";
			document.getElementById("imgAutorizacionDerechos").title = "Rechazado";
		}						 		
	}		
}

function mostrarMensajeDatosLicitacion(){	
	mostrarVentanaMensaje($('tipoMensaje').value, $('mensaje').value);	
}

function modificarRPFDerechosEndoso(cotizacionEndosoForm){				
	new Ajax.Request('/MidasWeb/cotizacion/endoso/modificarRPFDerechos.do', {
		method : "post",
		encoding : "UTF-8",
		parameters : (cotizacionEndosoForm !== undefined && cotizacionEndosoForm !== null) ? $(cotizacionEndosoForm).serialize(true) : null,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplete
		onSuccess : function(transport) {							
			actualizarRPFDerechos(transport.responseXML);
		} // End of onSuccess
		
	});
	return true;
}
function seleccionaTodos(val){
	var node_list = document.getElementsByTagName('input');
	for (var i = 0; i < node_list.length; i++) {
		var node = node_list[i];
		if (node.getAttribute('type') == 'checkbox') {
			if(node.id != 'adjuntarCotizacion')
				node.checked = val;
		}
	}
}
function validaSeleccion(){
	var node_list = document.getElementsByTagName('input');
	for (var i = 0; i < node_list.length; i++) {
		var node = node_list[i];
		if (node.getAttribute('type') == 'checkbox') {
			if(node.id != 'adjuntarCotizacion' && node.checked){
				return true;
			}
		}
	}
	return false;
}
function procesarAsignaciones(){	
	var node_list = document.getElementsByName('idToPoliza');
	var polizas = '';
	for(var i = 0; i < node_list.length; i++){
		var node = node_list[i];
		if(node !== null && node !== undefined  ){
			var checkboxSeleccion = $('checkboxSeleccion[' + i + ']');
			if(checkboxSeleccion !== null && checkboxSeleccion !== undefined  ){
				if(checkboxSeleccion.checked){
					polizas += node.value + ','; 
				}
			}
		}
	}
	sendRequest(document.renovacionPolizaForm, '/MidasWeb/renovacion/asignarRenovaciones.do?polizas='+polizas, 'contenido', 'procesarRespuesta(),manipulaCalendarioLineas()');
}
function validarAsigacionDeRenovaciones(){
	
	if(validaSeleccion()){
		if($('idSuscriptor').selectedIndex > 0){
			procesarAsignaciones();	
		}else{
			alert('Por favor, selccione alg\u00Fan suscriptor.');
		}
	}else{
		alert('Por favor, selccione alguna p\u00F3liza a renovar.');
	}
}

function validarEnvioCorreo(){
	if(validaSeleccion()){
		if($('idDestinatarioCorreo').selectedIndex > 0){
			var valCombo = $('idDestinatarioCorreo').options[$('idDestinatarioCorreo').selectedIndex].value ;
			if (valCombo == "40"){
				if($('destinatarioCorreo')!== undefined && $('destinatarioCorreo').value!==''){
					procesarEnvioCorreos();	
				}else{
					alert('Por favor, selccione alg\u00fan destinatario de correo electr\u00F3nico');
				}	
			}else{
				procesarEnvioCorreos();	
			}
		}else{
			alert('Por favor, selccione alg\u00fan destinatario de correo electr\u00F3nico');
		}
	}else{
		alert('Por favor, selccione alguna p\u00F3liza para enviar el correo electr\u00F3nico.');
	}
}
function procesarEnvioCorreos(){	
	var node_list = document.getElementsByName('polizaDTO.idToPoliza');
	var polizas = '';
	for(var i = 0; i < node_list.length; i++){
		var node = node_list[i];
		if(node !== null && node !== undefined  ){
			var checkboxSeleccion = $('checkboxSeleccion[' + i + ']');
			if(checkboxSeleccion !== null && checkboxSeleccion !== undefined  ){
				if(checkboxSeleccion.checked){
					polizas += node.value + ','; 
				}
			}
		}
	}
	sendRequest(document.renovacionPolizaForm, '/MidasWeb/renovacion/enviarCorreo.do?polizas='+polizas, 'contenido', 'procesarRespuesta();manipulaCalendarioLineas();$("idDestinatarioCorreo").value ="";');
	
}
function validaDestinatario(val){
	var divDestinatario = $("destinatarioAlterno");
	if(val !== null && val == "40"){
		divDestinatario.style.display = "block";
	}else{
		divDestinatario.style.display = "none";
	}	
}

function buscarClientesPorCURP(){
	var tipo;
	var curp = document.getElementById("codigoCURP").value;
	var cotizacion = document.getElementById("idPadre").value;
	var codigoCliente = document.getElementById("codigoPersona").value;
	
    for (i=0; i<document.clienteForm.claveTipoPersona.length; i++){
        if (document.clienteForm.claveTipoPersona[i].checked==true){
           tipo = document.clienteForm.claveTipoPersona[i].value;
           break;
        }
     }
	if (tipo != null && curp != null){
		new Ajax.Request('/MidasWeb/cliente/buscarCoincidenciasPorCURP.do?tipo='+tipo+"&curp="+curp,{
			method : "post",
			asynchronous : false,
			onCreate : function(transport) {
				parent.totalRequests = parent.totalRequests + 1;
				parent.showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				parent.totalRequests = parent.totalRequests - 1;
				parent.hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
				var xmlDoc = transport.responseXML;
				var items = xmlDoc.getElementsByTagName("item");
				var item = items[0];
				var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
				if(value == 1){
					//Inicializa Datos
					var cliente = item.getElementsByTagName("cliente")[0].firstChild.nodeValue;
					var domicilio = item.getElementsByTagName("domicilio")[0].firstChild.nodeValue;
					parent.asignarClienteCURPACotizacionDirecta(cotizacion,codigoCliente,cliente,domicilio);
					
				}else if (value > 1 ){
					parent.mostrarVentanaClientesPorCURP(tipo,curp,cotizacion,codigoCliente);
				}else if (value < 1){
					parent.mostrarVentanaMensaje("20", "No fue encontrado ning\u00fAn cliente con los datos proporcionados.");
				}
			}
		});
	}
}
function asignarClienteCURPACotizacion(idCotizacion,codigoCliente,idCliente,idDomicilio){
	new Ajax.Request('/MidasWeb/cotizacion/asignarCotizacionV2.do?idToCotizacion='+idCotizacion+"&idCliente="+idCliente+"&codigoCliente="+codigoCliente+"&idDomicilio="+idDomicilio,{
		method : "post",
		asynchronous : false,
		onCreate : function(transport) {
			parent.totalRequests = parent.totalRequests + 1;
			parent.showIndicator();
		}, // End of onCreate
		onComplete : function(transport) {
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}, // End of onComplte
		onSuccess : function(transport) {
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
			parent.mostrarVentanaMensaje("30", text);
			parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
			parent.ventanaClientesPorCURP.close();
			parent.accordionOT.openItem("a2");
			parent.accordionOT.openItem("a1");
		}
	});	
}

function asignarClienteCURPACotizacionDirecta(idCotizacion,codigoCliente,idCliente,idDomicilio){
	new Ajax.Request('/MidasWeb/cotizacion/asignarCotizacionV2.do?idToCotizacion='+idCotizacion+"&idCliente="+idCliente+"&codigoCliente="+codigoCliente+"&idDomicilio="+idDomicilio,{
		method : "post",
		asynchronous : false,
		onSuccess : function(transport) {
			var xmlDoc = transport.responseXML;
			var items = xmlDoc.getElementsByTagName("item");
			var item = items[0];
			var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			parent.accordionOT.cells("a1").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=3");
			parent.mostrarVentanaMensaje("30", text);
			parent.accordionOT.cells("a2").attachURL("/MidasWeb/cotizacion/mostrarPersona.do?idPadre="+idCotizacion+"&descripcionPadre=cotizacion&tipoDireccion=1");
			parent.accordionOT.openItem("a2");
			parent.accordionOT.openItem("a1");
			parent.totalRequests = parent.totalRequests - 1;
			parent.hideIndicator();
		}
	});	
}
function mostrarVentanaClientesPorCURP(tipo,curp,cotizacion,codigoCliente){
	if (parent.dhxWins==null){
		parent.dhxWins = new dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaClientesPorCURP = parent.dhxWins.createWindow("ventanaClientesPorCURP", 400, 250, 710, 290);
	parent.ventanaClientesPorCURP.setText("Clientes.");
	parent.ventanaClientesPorCURP.center();
	parent.ventanaClientesPorCURP.setModal(true);
	parent.ventanaClientesPorCURP.button("minmax1").hide();
	parent.ventanaClientesPorCURP.attachURL("/MidasWeb/cliente/listarClientePorCURP.do?tipo="+tipo+"&curp="+curp+"&cotizacion="+cotizacion+"&codigoCliente="+codigoCliente);
}


function popupValidarAgente() {
	var tipoMensaje = $("tipoMensaje").value;
	var mensaje = $("mensaje").value;
	if(tipoMensaje && mensaje) {
		mostrarVentanaMensaje(tipoMensaje, mensaje);
	}
}

function generarNuevaVersion(idToProducto){
	var redirect="existenErrores('refreshTree()')";
	
	var url = "sendRequestProcesaRespuestaXML(document.productoForm,'/MidasWeb/configuracion/producto/generarNuevaVersion.do?idToProducto="+idToProducto+"', 'contenido', "+redirect+");";
	ventanaConfirmacionSiniestros('\u00BFEsta seguro de Generar Nueva Versi\u00F3n?',url);
	

}

function generarNuevaVersionSeccion(idToSeccion){
	var redirect="existenErrores('refreshTree()')";
	
	var url = "sendRequestProcesaRespuestaXML(document.productoForm,'/MidasWeb/configuracion/seccion/generarNuevaVersion.do?idToSeccion="+idToSeccion+"', 'contenido', "+redirect+");";
	ventanaConfirmacionSiniestros('\u00BFEsta seguro de Generar Nueva Versi\u00F3n?',url);
	

}

function validaFiltroModeloVeh(){
	var filtro1 = dwr.util.getValue("id_claveTipoBien");
	var filtro2 = dwr.util.getValue("idTcTipoVehiculo");
	var filtro3 = dwr.util.getValue("idTcMarcaVehiculo");
	var filtro4 = dwr.util.getValue("id_idVersionCarga");
	var filtro5 = dwr.util.getValue("comboEstiloVehiculo");
	if(filtro1 != '' && filtro2 != ''){
		document.getElementById('b_descargar').style.display = 'block'; 
		document.getElementById('b_actualizar').style.display = 'block';
		if(filtro3 != '' && filtro4 != '' && filtro5 != ''){
			document.getElementById('busquedaModelos').style.display = 'block'; 
		}
	}else{
		document.getElementById('busquedaModelos').style.display = 'none';
	}
}

function validaFiltroEstilos(){
	var filtro1 = dwr.util.getValue("id_claveTipoBien");
	var filtro2 = dwr.util.getValue("tipoVehiculoForm.idTcTipoVehiculo");
	var filtro3 = dwr.util.getValue("marcaVehiculoForm.idTcMarcaVehiculo");
	var filtro4 = dwr.util.getValue("id_idVersionCarga");
	
	if(filtro1 != '' && filtro2 != '' && filtro3 != '' && filtro4 != '' ){
		document.getElementById('b_descargar').style.display = 'block'; 
		document.getElementById('b_actualizar').style.display = 'block';
		document.getElementById('busquedaEstilos').style.display = 'block'; 
	}
}


function cargaCombo(){
 jQuery("#idTcRamo").val(4);		
}


function desabilitaCampos(){
	
	var filtro1 = $('claveTipoCoaseguro').value;	
	var filtro2 = $('claveTipoLimiteCoaseguro').value;
	var filtro3 = $('claveTipoDeducible').value;
	var filtro4 = $('claveTipoLimiteDeducible').value;
	var campo1 = 'claveTipoCoaseguroValMin';
	var campo2 = 'valorMaximoCoaseguro';
	var campo3 = 'valorMinimoLimiteCoaseguro';
	var campo4 = 'valorMaximoLimiteCoaseguro';
	var campo5 = 'valorMinimoDeducible';
	var campo6 = 'valorMaximoDeducible';
	var campo7 = 'valorMinimoLimiteDeducible';
	var campo8 = 'valorMaximoLimiteDeducible';
	
	if(filtro1 == "0"){
		activaDesactiva(filtro1,campo1,campo2);
	}else{
		activaDesactiva(filtro1,campo1,campo2);
	}
	if(filtro2 == "0"){
		activaDesactiva(filtro2,campo3,campo4)
	}else{
		activaDesactiva(filtro2,campo3,campo4);
	}
	if(filtro3 == "0"){
		activaDesactiva(filtro3,campo5,campo6)
	}else{
		activaDesactiva(filtro3,campo5,campo6);
	}
	if(filtro4 == "0"){
		activaDesactiva(filtro4,campo7,campo8)
	}else{
		activaDesactiva(filtro4,campo7,campo8);
	}
}
function activaDesactiva(filtro,campo1,campo2){	
	if(filtro == 0){
		document.getElementById(campo1).value = 0.0;
		document.getElementById(campo2).value = 0.0;
		document.getElementById(campo1).readOnly = true;
		document.getElementById(campo2).readOnly = true;
		document.getElementById(campo1).style.background= "#f4f2f0";
		document.getElementById(campo2).style.background= "#f4f2f0";
		document.getElementById(campo1).style.color= "gray";
		document.getElementById(campo2).style.color= "gray";
	}else{		
		document.getElementById(campo1).readOnly = false;
		document.getElementById(campo2).readOnly = false;
		document.getElementById(campo1).style.background= "white";
		document.getElementById(campo2).style.background= "white";
		document.getElementById(campo1).style.color= "black";
		document.getElementById(campo2).style.color= "black";
	}		
}

/*****************************************************************************************
**funciones para obtener las colonias por codigo postal
***se carga la informacion de la colonia done el value y el texto son el nombre de la colonia
**** esta funciones se utilizan en Agentes
******************************************************************************************/
function getNameColoniasPorCP(codigoPostal, colonia, ciudad, estado) {
	if (codigoPostal != null || codigoPostal != "") {
		colonyCombo = colonia
		cityCombo = ciudad
		stateCombo = estado
		new Ajax.Request("/MidasWeb/codigoPostal.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + codigoPostal,
			onSuccess : function(transport) {
				loadCombosDomicilio(transport.responseXML);
			} // End of onSuccess
		});
	}
}

function loadCombosDomicilio(doc) {
	var colonies = $(colonyCombo);
	var city = $(cityCombo);
	var state = $(stateCombo);
	if (colonies != null && city != null && state != null) {
		removeAllOptionsExt(colonies);
		removeAllOptionsExt(city);
		if (colonies.length == 0) {
			addOption(colonies, "Seleccione ...", "");
		}
		var items = doc.getElementsByTagName("colonies");
		for ( var x = 0; x < items.length; x++) {
			var item = items[x];
			var value = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			var text = item.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(colonies, text, value);
		}
		var cityElement = doc.getElementsByTagName("city");
		if (cityElement.length > 0) {
			var cityItem = cityElement[0];
			var value = cityItem.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = cityItem.getElementsByTagName("description")[0].firstChild.nodeValue;
			addOption(city, text, value);
			for ( var i = 0; i < city.length; i++) {
				var option = city.options[i];
				if (option.value == value) {
					option.selected = true;
					break;
				}
			}
		}
		var stateElement = doc.getElementsByTagName("state");
		if (stateElement.length > 0) {
			var stateItem = stateElement[0];
			var value = stateItem.getElementsByTagName("id")[0].firstChild.nodeValue;
			var text = stateItem.getElementsByTagName("description")[0].firstChild.nodeValue;
			for ( var i = 0; i < state.length; i++) {
				var option = state.options[i];
				if (option.value == value) {
					option.selected = true;
					break;
				}
			}
		}
	}
	//Despues de cargar todos los combos entonces carga los ajustadores
//	llenarComboAjustadores($('idTipoNegocio').value,$('idCiudad').value);
}

function selectAllChecks(idUL)
{
	jQuery("#" + idUL + " input").each(function(index) {
		jQuery(this).attr('checked',true);
	});
	
}

function deselectAllChecks(idUL)
{
	jQuery("#" + idUL + " input").each(function(index) {
		jQuery(this).attr('checked',false);
	});
	
}

/** Actualización Catalogo Modelo Vehiculo **/

function downloadExcelModel(){
	
	var location ="/MidasWeb/catalogos/modelovehiculo/descargar.do?"+$(modeloVehiculoForm).serialize();
	window.open(location, "Descargando Excel de Modelos");
}

function loadExcelModel(){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("documentoActualizarDatosModelo", 34, 100, 440, 265);
	adjuntarDocumento.setText("Excel Actualizar Datos Modelo");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { 
    		method       : "post", 
    		asynchronous : false, 
    		parameters   : null, 
    		onSuccess    : function(transport) {
			    			var xmlDoc = transport.responseXML;
			    			var items = xmlDoc.getElementsByTagName("item");
			    			var item = items[0];
			    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			    			if (idRespuesta == 1){
			    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
			    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
			    				var filtro1 = $("id_claveTipoBien").value;
			    				var filtro2 = $("idTcTipoVehiculo").value;
			    				var filtro3 = $("idTcMarcaVehiculo").value;
			    				var filtro4 = $("id_idVersionCarga").value;
			    				var filtro5 = $("comboEstiloVehiculo").value;			    				
			    				actualizarDatosModeloVehiculo(idToControlArchivo, filtro1, filtro2, filtro3, filtro4, filtro5);			    				
			    			}
    		} // End of onSuccess    		
    	});
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }
    vault.create("vault");
    vault.setFormField("claveTipo", 41);
}

function actualizarDatosModeloVehiculo(idToControlArchivo, filtro1, filtro2, filtro3, filtro4, filtro5){
	sendRequest(null, '/MidasWeb/catalogos/modelovehiculo/actualizar.do?idToControlArchivo='+idToControlArchivo, 'contenido', 'actualizaFiltros('+filtro1+', '+filtro2+', '+filtro3+', '+filtro4+', '+filtro5+');');
}

function actualizaFiltros(filtro1, filtro2, filtro3, filtro4, filtro5){

	$("id_claveTipoBien").value = filtro1;
    $("idTcTipoVehiculo").value = filtro2;
	$("idTcMarcaVehiculo").value = filtro3;
	$("id_idVersionCarga").value = filtro4;
	$("comboEstiloVehiculo").value = filtro5;	
}





function getSubMarcaVehiculos(obj, child){
	if (obj.selectedIndex > 0) {
		new Ajax.Request("/MidasWeb/submarcaVehiculo.do", {
			method : "post",
			asynchronous : false,
			parameters : "id=" + obj[obj.selectedIndex].value,
			onSuccess : function(transport) {
				loadCombo(child, transport.responseXML);
			} // End of onSuccess
		});
	} else {
		removeAllOptionsExt($(child));
	} // End of if/else
}



var tablaGrid;
var tablaProcessor;
function iniciaTablaActualizaGrid(nombreGrid, cargaElementosPath, accionSobreElementosPath, editable, validarColumna,afterUpdateGrid) {	
	tablaGrid = new dhtmlXGridObject(nombreGrid);	
	accionSobreElementosPath=accionSobreElementosPath;
	tablaGrid.init();	
	//Creacion del DataProcessor
	if(editable=="true"){
		tablaGrid.load(cargaElementosPath);	
		tablaProcessor = new dataProcessor(accionSobreElementosPath);
		tablaProcessor.enableDataNames(true);
		tablaProcessor.setTransactionMode("POST");
		tablaProcessor.setUpdateMode("cell");
		tablaProcessor.setVerificator(validarColumna, function(value,row){
			var valido = true;
			tablaGrid.forEachRow(function(id){ 
	            // here id - id of the row
				var cellVal = tablaGrid.cellById(id, validarColumna).getValue().toUpperCase();
				//alert(cellVal + " - " + value + " - id - " + id + " - row - " + row);
				if(cellVal == value.toUpperCase() && id != row){
					tablaGrid.cellById(row, validarColumna).setValue(null);
					mostrarVentanaMensaje('20', 'El valor ingresado esta duplicado');
					valido =  false;
					return;
				}
	        });
			if(value <= 0){
				valido =  false;
			}
			return valido;
		});	
		//tablaProcessor.attachEvent("onAfterUpdate",obtenerGridSubMarcas);	
		tablaProcessor.attachEvent("onAfterUpdateFinish",
				function(){
			
			if(afterUpdateGrid=='obtenerGridSubMarca'){
				obtenerGridSubMarcas();
			}
			
		});
		tablaProcessor.init(tablaGrid);	
	}else{
		tablaGrid.load(cargaElementosPath,function(){
			tablaGrid.setColTypes("ro,ro");
			},null);
	}
}


function agregarColumnaTablaActualizaGrid() {
	if (tablaGrid.getRowsNum() ==0 ) {
		tablaGrid.addRow(1,",,,0");
	} else {
		var secuencia = parseInt(tablaGrid.getRowId(tablaGrid.getRowsNum() - 1)) + 1;
		var valido = true;
		tablaGrid.forEachRow(function(id){ 
			var cellVal = tablaGrid.cellById(id, 1).getValue();
			if(cellVal == ""){
				valido =  false;
				return;
			}
        });
		if(valido){
			tablaGrid.addRow(secuencia,",,,0");
		}
	}	
}

function eliminarColumnaTablaActualizaGrid() {
	var id = tablaGrid.getSelectedId();
	if (id != null){
		if(confirm('\u00BFDesea eliminar el elemento seleccionado?')){
			tablaGrid.deleteSelectedItem();
		}
	}else{
		alert('Debes seleccionar un registro para eliminar. ');
	}
}

function iniciaGridSubMarcas(isEditable, idTcMarcaVehiculo){
	var idMarca= idTcMarcaVehiculo;
	var nombreGrid='subMarcaGrid';
	var cargaElementosPath= '/MidasWeb/catalogos/subMarcaVehiculo/listadoSubmarcas.action?idTcMarcaVehiculo='+idMarca;
	var accionSobreElementosPath= '/MidasWeb/catalogos/subMarcaVehiculo/relacionarSubmarcas.action?idTcMarcaVehiculo='+idMarca;
	var editable=isEditable;
	validarColumna=1;
	afterUpdateGrid='obtenerGridSubMarca';
	iniciaTablaActualizaGrid(nombreGrid, cargaElementosPath, accionSobreElementosPath, editable, validarColumna,afterUpdateGrid);
}


function obtenerGridSubMarcas(){
	var idMarca= dwr.util.getValue("idTcMarcaVehiculo");
	var nombreGrid='subMarcaGrid';
	var cargaElementosPath= '/MidasWeb/catalogos/subMarcaVehiculo/listadoSubmarcas.action?idTcMarcaVehiculo='+idMarca;
	var accionSobreElementosPath= '/MidasWeb/catalogos/subMarcaVehiculo/relacionarSubmarcas.action?idTcMarcaVehiculo='+idMarca;
	var editable= dwr.util.getValue("isEditable");
	validarColumna=1;
	afterUpdateGrid='obtenerGridSubMarca';
	iniciaTablaActualizaGrid(nombreGrid, cargaElementosPath, accionSobreElementosPath, editable, validarColumna,afterUpdateGrid);
}



function eliminarSubmarca(){	
	var id = tablaGrid.getSelectedId();
	if (id != null){
		var idSubMarca = tablaGrid.cellById( tablaGrid.getSelectedId(),0).getValue();
		jQuery.ajax({
            url: '/MidasWeb/catalogos/subMarcaVehiculo/validaEliminarRegistro.action?id='+idSubMarca,
            dataType: 'json',
            async:false,
            type:"POST",
            data: $(marcaVehiculoForm).serialize(),
            success: function(json){
            	 var mensajeMostrar = json.mensajeMostrar;
            	 if(null!=mensajeMostrar && mensajeMostrar != ""){
             		mostrarMensajeInformativo('' +mensajeMostrar, '20');
            	 }else{
            		 eliminarColumnaTablaActualizaGrid();
            	 }
            }
      });
	
	}else{
		alert('Debes seleccionar un registro para eliminar. ');
	}
}
/** Actualización Catalogo Estilo Vehiculo **/

function downloadExcelModelSubmarcas(){
	var location ="/MidasWeb/catalogos/estilovehiculo/descargar.do?"+$(estiloVehiculoForm).serialize();
	window.open(location, "Descargando Excel de Modelos");
}

function loadExcelModelSubmarcas(){
	if(dhxWins != null) 
		dhxWins.unload();

	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("documentoActualizarDatosEstilos", 34, 100, 440, 265);
	adjuntarDocumento.setText("Excel Actualizar Datos Estilos");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { 
    		method       : "post", 
    		asynchronous : false, 
    		parameters   : null, 
    		onSuccess    : function(transport) {
			    			var xmlDoc = transport.responseXML;
			    			var items = xmlDoc.getElementsByTagName("item");
			    			var item = items[0];
			    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
			    			if (idRespuesta == 1){
			    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
			    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
			    				var filtro1 = $("id_claveTipoBien").value;
			    				var filtro2 = $("tipoVehiculoForm.idTcTipoVehiculo").value;
			    				var filtro3 = $("marcaVehiculoForm.idTcMarcaVehiculo").value;
			    				var filtro4 = $("id_idVersionCarga").value;
			    				var filtro5 = '';//$("id_claveEstilo").value;			    				
			    				actualizarDatosEstilosVehiculo(idToControlArchivo, filtro1, filtro2, filtro3, filtro4, filtro5);			    				
			    			}
    		} // End of onSuccess    		
    	});
    };
    vault.onAddFile = function(fileName) { 
        var ext = this.getFileExtension(fileName); 
        if (ext != "xls") { 
           alert("Solo puede importar archivos Excel (.xls). "); 
           return false; 
        } 
        else return true; 
     }; 
     
    vault.onBeforeUpload = function(fileName) {
    	var respuesta = confirm("\u00BFEsta seguro que desea agregar el archivo?");
    	if(respuesta){
    		return true;
    	}else{
    		return false;
    	}
    }
    vault.create("vault");
    vault.setFormField("claveTipo", 40);
}


function actualizarDatosEstilosVehiculo(idToControlArchivo, filtro1, filtro2, filtro3, filtro4, filtro5){
	sendRequest(null, '/MidasWeb/catalogos/estilovehiculo/actualizar.do?idToControlArchivo='+idToControlArchivo, 'contenido', 'actualizaFiltrosEstiloVeh('+filtro1+', '+filtro2+', '+filtro3+', '+filtro4+', '+filtro5+');');
}

function actualizaFiltrosEstiloVeh(filtro1, filtro2, filtro3, filtro4, filtro5){

	$("id_claveTipoBien").value = filtro1;
    $("tipoVehiculoForm.idTcTipoVehiculo").value = filtro2;
	$("marcaVehiculoForm.idTcMarcaVehiculo").value = filtro3;
	$("id_idVersionCarga").value = filtro4;
	//$("comboEstiloVehiculo").value = filtro5;	
}
