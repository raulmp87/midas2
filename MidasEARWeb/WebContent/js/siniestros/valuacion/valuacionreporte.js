var listaValuadoresNombres;

function mostrarCatalogoValuaciones(){ 	
	formParams = jQuery(document.informacionBusquedaValuacionesForm).serialize();
	var url = mostrarBusquedaPath;
	
	sendRequestJQ(null, url, targetWorkArea, null);
}

var valuacionesCruceroGrid;
function buscarValuacionesCrucero(mostrarTablaVacia){
	if(validarBusqueda(mostrarTablaVacia)){
		jQuery("#tituloListado").show();
		jQuery("#valuacionesCruceroGridContainer").show();	
		jQuery("#valuacionesCruceroGrid").empty();
		jQuery("#mostrarTablaVacia").val(mostrarTablaVacia);
		
		valuacionesCruceroGrid = new dhtmlXGridObject('valuacionesCruceroGrid');
		valuacionesCruceroGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		valuacionesCruceroGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		
		formParams = jQuery(document.busquedaValuacionReporte).serialize();
		var url = listarPath + '?' +  formParams;
		valuacionesCruceroGrid.load( url );
	}
}

function validarBusqueda(mostrarTablaVacia){
	var numValuacion = jQuery("#numValuacion_t").val();
	var numReporte = jQuery("#numReporte_t").val();
	var ajustador = jQuery("#ajustadores_s").val();
	var valuador = jQuery("#valuadores_s").val();
	var fechaInicial = jQuery("#txtFechaInicio").val();
	var fechaFinal = jQuery("#txtFechaFin").val();
	var estatus = jQuery("#estatus_s").val();
	var nsOficina = jQuery("#nsOficina").val();
	var nsConsecutivoR = jQuery("#nsConsecutivoR").val();
	var nsAnio = jQuery("#nsAnio").val();
	if(numValuacion || numReporte || ajustador || valuador || fechaInicial || fechaFinal 
			|| estatus || nsAnio || nsConsecutivoR || nsOficina || validarNumSerie() || mostrarTablaVacia){
			return true;
	}
	mostrarVentanaMensaje("20","Debe de seleccionar al menos un campo para buscar",null);
	return false;
}

function validarNumSerie(){
	var numSerie = jQuery("#numSerie_t").val();
	if(!numSerie){
		return false;
	}
	if(numSerie && !((numSerie.toString()).length > 6 )){
		mostrarVentanaMensaje("20","El numero de serie debe tener 7 caracteres o mas",null);
		return false;
	}
	return true;
}

var piezasValuacionGrid;
function buscarPiezasValuacion(){
	jQuery("#piezasValuacionGrid").empty(); 
	piezasValuacionGrid = new dhtmlXGridObject('piezasValuacionGrid'); 
	piezasValuacionGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
	piezasValuacionGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
	
	piezasValuacionGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
		var ORIGEN = 4;
		var REFACCIONES = 5;
		var PINTURA = 6;
		var HOJALATERIA = 7;
		if(cInd == ORIGEN || cInd == REFACCIONES || cInd == PINTURA || cInd == HOJALATERIA ){
			if( stage == 2 && !(nValue === oValue) ) {
				limpiarDivTotales();
				if(cInd != ORIGEN){
					nValue = nValue.toFixed(2);
					piezasValuacionGrid.cells(rId, cInd).setValue(nValue);
					if( validacionMoneda( nValue )){
						jQuery("#nuevoValorCelda").val(nValue);
						jQuery("#columna").val(cInd);
						jQuery("#idPiezaValuacion").val(rId);
						formParams = jQuery(document.valuacionDetalleForm).serialize();
						var url = actualizarTotalesPath + '?' +  formParams ;
						sendRequestJQ(null,url,'totales','buscarPiezasValuacion(); initCurrencyFormatOnTxtInput();');
					}else{
						mostrarVentanaMensaje("20","Debe ingresar una cantidad monetaria v\u00e1lida",null);
						piezasValuacionGrid.cellById(rId, cInd).setValue(oValue);
					}
				}else{
						jQuery("#origen").val(nValue);
						jQuery("#columna").val(cInd);
						jQuery("#idPiezaValuacion").val(rId);
						formParams = jQuery(document.valuacionDetalleForm).serialize();
						var url = actualizarTotalesPath + '?' +  formParams ;
						sendRequestJQ(null,url,'totales','buscarPiezasValuacion(); initCurrencyFormatOnTxtInput();');
				}
			}
		}
		return true;
	});		
	
	formParams = jQuery(document.valuacionDetalleForm).serialize();
	var url = actualizarPiezaPath + '?' +  formParams ;
	piezasValuacionGrid.load( url );
}

function limpiarDivTotales() {
	document.getElementById("totales").innerHTML = '';
}

function validacionMoneda( cantidadDinero ){
	var validation = true;
	expr=/^(-?(((\d{1,3},)+\d{3})|\d+)\.\d{2})?$/;

    if(!expr.exec( cantidadDinero )){
    	validation = false;
    }
	return validation;
}

function initConfiguration(){
	var esConsulta = jQuery("#esConsulta").val();
	if(esConsulta == 1){
//		jQuery(".deshabilitar").attr("readonly","true");
		jQuery(".deshabilitar").attr("disabled","disabled");
		jQuery("#btn_eliminarPiezas").remove();
		jQuery("#btn_guardar").remove();
		jQuery("#btn_terminar").remove();
		jQuery("#btn_agregarPieza").remove();
		jQuery("#btn_crearPieza").remove();
	}
}

function limpiarFiltros(){
	sendRequestJQ(null, mostrarBusquedaPath, targetWorkArea, null);
}

function regresarReporteBusqueda(){
	var idToReporte 		= jQuery("#idToReporte").val();
	var url = regresarReporteCabina + "?idToReporte="+idToReporte;
	
	sendRequestJQ(null, url,targetWorkArea, null);
}

function consultarValuacion(idValuacionReporte){
	jQuery("#idValuacionReporte").val(idValuacionReporte);
	var esConsulta = 1;
	jQuery("#esConsulta").val(esConsulta);
	formParams = jQuery(document.busquedaValuacionReporte).serialize();
	var url = obtenerPath + '?' +  formParams;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function editarValuacion(idValuacionReporte){
	jQuery("#idValuacionReporte").val(idValuacionReporte);
	var esConsulta = 0;
	jQuery("#esConsulta").val(esConsulta);
	formParams = jQuery(document.busquedaValuacionReporte).serialize();
	var url = obtenerPath + '?' +  formParams;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function imprimirValuacion(numeroValuacion){
	if(!numeroValuacion){
		numeroValuacion = jQuery("#idValuacionReporte").val();
	}
	var url = imprimirValuacionPath + "?idValuacionReporte=" + numeroValuacion;
	window.open(url, "ValuacionReporte");
}

function eliminarPiezas(){
	jQuery("#banderaEliminar").val(true);
	jQuery("#listaPiezasEliminar").val(piezasValuacionGrid.getCheckedRows(0));
	
	// # VALIDAR CHECKBOX CLICKEADOS SELECCIONADOS
	if( piezasValuacionGrid.getCheckedRows(0).length > 0 ){
		relacionarPieza();
	}else{
		mostrarMensajeInformativo('Seleccione la pieza a eliminar', '20');
	}
	
}

function agregarPieza(){
	jQuery("#banderaEliminar").val(false);
	removeCurrencyFormatOnTxtInput();
	relacionarPieza();
}

function relacionarPieza(){
	removeCurrencyFormatOnTxtInput();
	formParams = jQuery(document.valuacionDetalleForm).serialize();
	formBusqueda = jQuery(document.informacionBusquedaValuacionesForm).serialize();
	var url = relacionarPiezaPath+ '?' +  formParams + "&" + formBusqueda;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function mostrarVentanaImagenes(){
	ventanaFortimax('CRUC','' , 'FORTIMAX', jQuery("#entidadReporteId").val(), jQuery("#entidadReporteNumero").val());
}

function guardarReporteValuacion(){
	removeCurrencyFormatOnTxtInput();
	formParams = jQuery(document.valuacionDetalleForm).serialize();
	formBusqueda = jQuery(document.informacionBusquedaValuacionesForm).serialize();
	var url = guardarPath+ '?' +  formParams + "&" + formBusqueda;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function terminarReporteValuacion(){
//	var ESTATUS_TERMINADO = 2;
//	var esConsulta = 1;
//	jQuery("#estatusEntidad").val(ESTATUS_TERMINADO);
//	jQuery("#esConsulta").val(esConsulta);
	if(confirm("Al terminar la valuación no se podrá modificar, ¿Desea continuar?")){
		removeCurrencyFormatOnTxtInput();
		formParams = jQuery(document.valuacionDetalleForm).serialize();
		formBusqueda = jQuery(document.informacionBusquedaValuacionesForm).serialize();
		var url = guardarPath+ '?' +  formParams  + "&" + formBusqueda  + "&terminarReporte=true";
		sendRequestJQ(null, url, targetWorkArea, null);
	}
}

function regresarABusqueda(){
	if(confirm("Se perderán los cambios no guardados, ¿Desea continuar?")){
		mostrarCatalogoValuaciones();
	}
}

function mostrarAltaPieza(){
	var formParams = jQuery(document.informacionBusquedaValuacionesForm).serialize();
	var idValuacionReporte = jQuery("#idValuacionReporte").val();
	var esConsulta = jQuery("#esConsulta").val();
	var url = //"/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/mostrarAlta.action?accion=1" + '&' +  formParams + "&valuacionEsPantallaPrevia=true";
		"/MidasWeb/siniestros/catalogo/pieza/piezaValuacion/mostrarAlta.action?accion=1" + '&idValuacionReporte=' + 
		idValuacionReporte + "&valuacionEsPantallaPrevia=true" + "&esConsulta=" + esConsulta + "&" + formParams;
	sendRequestJQ(null, url ,"contenido", null);
}

function onChangeSeccionAuto(target){
	var idSeccionAuto = jQuery("#seccionAuto_s").val();
	if(idSeccionAuto != null  && idSeccionAuto != headerValue){		
		dwr.engine.beginBatch();
		listadoService.obtenerPiezasPorSeccionAutomovil(idSeccionAuto,
				function(data){
					addOptions(target,data);
				});
		dwr.engine.endBatch({async:false});
	}else{
		addOptions(target,"");
	}
}

function formatoCantidades(){
	var tpintura = jQuery("#totalPintura_t").val();
	var thojalateria = jQuery("#totalHojalateria_t").val();
	var trefacciones = jQuery("#totalRefacciones_t").val();
	var total = jQuery("#total_t").val();
	if(tpintura == ""){tpintura = 0};
	if(thojalateria == ""){thojalateria = 0};
	if(trefacciones == ""){trefacciones = 0};
	if(total == ""){total = 0};
	tpintura = parseFloat(tpintura).toFixed(2);
	trefacciones = parseFloat(trefacciones).toFixed(2);
	thojalateria = parseFloat(thojalateria).toFixed(2);
	total = parseFloat(total).toFixed(2);
	jQuery("#totalPintura_t").val(tpintura);
	jQuery("#totalHojalateria_t").val(thojalateria);
	jQuery("#totalRefacciones_t").val(trefacciones);
	jQuery("#total_t").val(total);
}

function validarNumericos(){
	jQuery(".numeric").keyup(function () {
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
}

function consultarValuacionHGS()
{	
	formParams = jQuery(document.estimacionForm).serialize();
	var url = "/MidasWeb/siniestros/valuacion/obtener.action" + '?'
		+  "&idEstimacionCoberturaReporteCabina=" + jQuery("#idEstimacionCoberturaReporteCabina").val() 
			+ "&tipoValuacion=2";
	//sendRequestJQ(null, url, targetWorkArea, null);
	
	mostrarVentanaModal("DetValuacionHGS", 'Consulta de valuaci\u00F3n HGS', 50, 200, 1200, 480, url);
}

function limpiarValuador(){
	jQuery("#nombreValuadorPs").val("");
	jQuery("#idPrestadorServicio").val("");
}

function guardarDatosValuacion(){
	removeCurrencyFormatOnTxtInput();
	formParams = jQuery(document.contenedorValuacionForm).serialize();
	var idValuacionHgs = jQuery("#idValuacionHgs").val();
	if( idValuacionHgs == null || idValuacionHgs == ""){
		mostrarMensajeInformativo("No se puede guardar la valuación sin número ID", '10');
	}else{
		var url="/MidasWeb/siniestros/valuacion/guardarDatosValuacionManual.action";
		jQuery.asyncPostJSON(url,formParams,responseGuardarValuacion);
	}
}

function validarNumero(numero){
    if (!/^([0-9])*$/.test(numero)){
    	alert("El ID Valuación HGS: " + numero.toUpperCase() + " no es un número");
    	jQuery("#idValuacionHgs").val("");
    }
}

function responseGuardarValuacion(json){
	var tipoMensaje = json.tipoMensaje;
	blockPage();
	initCurrencyFormatOnTxtInput();
	if (tipoMensaje == "30"){
		jQuery('#idValuacionHgs').attr('readonly', true);
	}
	unblockPage();
	parent.mostrarMensajeInformativo(json.mensaje,json.tipoMensaje);
	
}

//jquery1816(function(){
//	jquery1816("#nombreValuadorPs").autocomplete({
//        source: listaValuadoresNombres.size() > 0 ? listaValuadoresNombres : function(request, response){
//        	        	
//        },
//        minLength: 3,
//        delay: 500,
//        select: function(event, ui) {
//        	jquery1816("#idPrestadorServicio").val(ui.item.id);         	
//        	//buscarOrdenesCompra(); //se buscan las ordenes de compra del proveedor //Revisar invocacion segunda vez
//        	//actualizarOficina();        	
//        	
//        },
//        search: function(event, ui) {
//        	//Pre-busqueda para cuando no exista agente en listado mande mensaje de error
//            var busqueda = jQuery("#nombreValuadorPs").val();
//            if(findItem(busqueda.toString().toLowerCase(), listaValuadoresNombres).length == 0){
//            	//alert(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"));
//            	mostrarMensajeInformativo(("Proveedor ("+busqueda.toString().toUpperCase()+") no existe"), '20');
//            	jQuery("#nombreValuadorPs").val("");
//            	return false;
//            }
//        }
//    });
//});
//
//jquery1816(function(){
//	jquery1816("#nombreValuadorPs").bind("keypress", function(e) {
//	    if (e.keyCode == 13) {
//	        e.preventDefault();         
//	        return false;
//	    }
//	});
//});

//-------- termina funcionalidad Autocomplete

function onChangeValuador(componente){
	var componenteId = jQuery(componente).attr('id');
	var nombreId = jQuery("#" + componenteId + " option:selected").text();
	var nombreIdSplit = nombreId.split("-");
	var nombre = nombreIdSplit[0];
	jQuery("#nombreValuador").val(nombre);
}

var ventanaValuacionHgs;
function mostrarModalValuacionHgs(){

	console.log("En el metodo nuevo...");
	var url = "/MidasWeb/siniestros/valuacion/obtener.action" + '?'
	+  "&idEstimacionCoberturaReporteCabina=" + jQuery("#idEstimacionCoberturaReporteCabina").val() 
	+ "&tipoValuacion=2";
	
	sendRequestWindow(null, url, obtenerVentanaValuacionHgs);

}

function obtenerVentanaValuacionHgs(){
	
	console.log("Contruyendo la ventana...");
	var wins = obtenerContenedorVentanas();
	ventanaValuacionHgs = wins.createWindow("DetValuacionHGS", 50, 200, 1200, 440);
	ventanaValuacionHgs.center();
	ventanaValuacionHgs.setModal(true);
	ventanaValuacionHgs.setText("Consulta de valuaci\u00F3n HGS");

	return ventanaValuacionHgs;
}
