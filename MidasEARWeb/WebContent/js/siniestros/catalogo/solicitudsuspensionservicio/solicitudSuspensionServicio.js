function inicializarListadosuspensionServicios(){	
	mostrarListadoSuspensionServicios(false);
}


function mostrarListadoSuspensionServicios(filtro){
	//if(filtroValido(filtro)){
		document.getElementById("pagingArea").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		listadoGrid = new dhtmlXGridObject('listadoSuspensionServiciosGrid');
		//merge first 2 cells in the row with the id "row1"
		listadoGrid.attachEvent("onXLS", function(grid){	
			blockPage();
	    });
		listadoGrid.attachEvent("onXLE", function(grid){		
			unblockPage();
	    });	
		var formParams = null;
		
		formParams = jQuery(document.suspensionServicioForm).serialize();	 
		listadoGrid.load( buscarSuspensionServiciosPath + '?' + formParams );
//	}else{
//		mostrarMensajeInformativo("Debe capturar al menos un dato para realizar la búsqueda.", '10');
//	}
}

function mostrarAgregarSuspensionServicio(){
	mostrarSuspensionServicio( 0, 1 );
}

function mostrarEditarSuspensionServicio( id ){
	mostrarSuspensionServicio( id, 2 );
}

function mostrarConsultarSuspensionServicio( id ){
	mostrarSuspensionServicio( id, 3 );
}

function guardarSuspensionServicio(){	
	if(validateAll(true)){
		var tipoVentana = jQuery("#h_tipoVentana").val();
		formParams = jQuery(document.suspensionServicioForm).serialize(); 
		var url = guardarSuspensionServicioPath + '?' + formParams;// + "&tipoVentana=" + tipoVentana;
		
		if( this.validaDatosRequeridos() ){
			if( !fechaInicioMayorQueFechaFin( jQuery("#dp_fechaFin").val(), jQuery("#dp_fechaInicio").val()) ){
				mostrarVentanaMensaje("20","La Fecha de Inicio no puede ser mayor que la Fecha Fin",null);
			}else{		
				redirectVentanaModal('vm_suspensionServicio', guardarSuspensionServicioPath , document.suspensionServicioForm);
			}
		}else{
			isError();
		}
	}
}

function mostrarSuspensionServicio( id, tipoVentana ){
	if( tipoVentana == 1 ){
		mostrarVentanaModal("vm_suspensionServicio", "Alta de Solicitud de Suspensi\u00F3n de Servicios", null, null, 1020, 480, mostrarSuspensionServicioPath + '?idSuspensionServicio=' + id + 
																																							"&tipoVentana=" + tipoVentana, "");
	}else if( tipoVentana == 2 ){
		mostrarVentanaModal("vm_suspensionServicio", "Editar Solicitud de Suspensi\u00F3n de Servicios", null, null, 1020, 480, mostrarSuspensionServicioPath + '?idSuspensionServicio=' + id + 
																																						   "&tipoVentana=" + tipoVentana, "");
	}else if( tipoVentana == 3 ){
		mostrarVentanaModal("vm_suspensionServicio", "Consulta de datos de la Solicitud de Suspensi\u00F3n de Servicios", null, null, 1020, 480, mostrarSuspensionServicioPath + '?idSuspensionServicio=' + id + 
																																											"&tipoVentana=" + tipoVentana, "");
	}
}

function limpiarFiltros(){
	jQuery('#suspensionServicioForm').each (function(){
		  this.reset();
	});
}

function salirConfirmar(){
	var cerrar = true;
	if( jQuery('#h_tipoVentana').val() != 3 ){
		if(!confirm('Los cambios no guardados se perder\u00E1n. \u00BFDesea continuar?')){
			cerrar = false;
		}
	}
	if(cerrar){
		salir();
	}

}

function salir(){
	parent.cerrarVentanaModal("vm_suspensionServicio", "mostrarListadoSuspensionServicios(false);");
}

function isError(){
	mostrarVentanaMensaje("20","Favor de llenar los campos requeridos",null);
}

function onBlurRequeridos(){
	var fechaInicio = jQuery("#dp_fechaInicio");
	var fechaFin = jQuery("#dp_fechaFin");
	
	jQuery(".requerido").blur(
			function(){
				if(fechaInicio.val() != ""){
					fechaInicio.removeClass("error");
				}
				
				if(fechaFin.val() != ""){
					fechaFin.removeClass("error");
				}
				var these = jQuery(this);
				if( isEmpty( these.val() ) ){
					these.addClass("error");
				} else {
					these.removeClass("error");
				}
			}
		);
}

function validaDatosRequeridos(){
	var requeridos = jQuery(".requerido");
	var success = true;
	requeridos.each(
		function(){
			var these = jQuery(this);
			if( isEmpty(these.val()) ){
				these.addClass("error"); 
				success = false;
			} else {
				these.removeClass("error");
			}
		}
	);
	return success;
}

function fechaInicioMayorQueFechaFin(fec0, fec1){
    var bRes = false;
    var sDia0 = fec0.substr(0, 2);
    var sMes0 = fec0.substr(3, 2);
    var sAno0 = fec0.substr(6, 4);
    var sDia1 = fec1.substr(0, 2);
    var sMes1 = fec1.substr(3, 2);
    var sAno1 = fec1.substr(6, 4);
    if (sAno0 > sAno1) bRes = true;
    else {
     if (sAno0 == sAno1){
      if (sMes0 > sMes1) bRes = true;
      else {
       if (sMes0 == sMes1)
        if (sDia0 >= sDia1) bRes = true;
      }
     }
    }
    return bRes;
   } 

function exportarExcel(){
	var url="/MidasWeb/siniestros/catalogos/suspensionServicio/exportarResultados.action?" + jQuery(document.suspensionServicioForm).serialize();
		window.open(url, "Listado_Solicitud_Suspension_Servicio");
}

function eliminarConsultarSuspensionServicio( idSuspensionServicio ){
	if( confirm("\u00BF Realmente desea eliminar el registro ?") ){	
		sendRequest(null,'/MidasWeb/siniestros/catalogos/suspensionServicio/eliminar.action?idSuspensionServicio=' + idSuspensionServicio,null,"mostrarListadoSuspensionServicios(false);");
	}
}

function filtroValido(filtro){
	if (filtro){
		var txt_id       		= jQuery("#txt_id").val();
		var txt_numeroSerie		= jQuery("#txt_numeroSerie").val();
		var s_estatus			= jQuery("#s_estatus").val();
		var s_oficina			= jQuery("#s_oficina").val();
		var fechaInicio			= jQuery("#filtroCatalogo\\.fechaInicio").val();
		var fechaFin			= jQuery("#filtroCatalogo\\.fechaFin").val();
		
		var filtroValido = txt_id+txt_numeroSerie+s_estatus+s_oficina+fechaInicio+fechaFin;
	
		return filtroValido == "" || filtroValido == 'undefined' ? false : true;
	}else{
		return true;
	}
}