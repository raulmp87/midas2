function mostrarVentanaBajaPlacas(idIndemnizacion, tipoBajaPlacas){
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/mostrarBajaPlacas.action?cartaSiniestro.indemnizacion.id='
		+ idIndemnizacion + "&cartaSiniestro.tipo=" + tipoBajaPlacas;
	mostrarVentanaModal("vm_bajaPlaca", 'Llene la informaci\u00F3n para la Baja de Placas',  1, 1, 750, 350, url, null);
}

function guardarCartaBajaPlacas(){
	jQuery("#bajaPlacasForm").attr("action","guardarBajaPlacas.action");
	parent.submitVentanaModal("vm_bajaPlaca", document.bajaPlacasForm);
}


function imprimirCartaBajaPlacas(){
	if(validarImpresion()){
		formParams = jQuery(document.bajaPlacasForm).serialize();
		var url = "/MidasWeb/siniestros/indemnizacion/cartas/imprimirBajaPlacas.action?" + formParams ;
	    window.open(url, "CartaBajaPlacas");
	}
}

function imprimirNotificacionPerdidaTotal(idIndemnizacion, valuadorId){
	var url = "/MidasWeb/siniestros/indemnizacion/cartas/imprimirNotificacionPerdidaTotal.action?idIndemnizacion=" + 
	idIndemnizacion + "&valuadorId=" + valuadorId;
    window.open(url, "NotificacionPerdidaTotal");
}


function validarImpresion(){
	var descSol = jQuery("#descripcionSol_t").val();
	var descSolComp = jQuery("#descripcionSolComp_t").val();
	var entidadDescSol = jQuery("#entidadDescSol").val();
	var entidadDescSolComp = jQuery("#entidadDescSolComp").val();
	var informacionCompleta = jQuery("#informacionCompleta").val();
	if(descSol && descSolComp){
		if(descSol == entidadDescSol && descSolComp == entidadDescSolComp){
			if(informacionCompleta == "true"){
				return true;
			}else{
				mostrarVentanaMensaje("20","No se puede generar el reporte debido a que no esta completa la informacion del siniestro",null);
			}
		}else{
			mostrarVentanaMensaje("20","Debe guardar los cambios para poder imprimir",null);
			return false;
		}
	}
	mostrarVentanaMensaje("20","Debe capturar los campos y guardar para poder imprimir",null);
	return false;
}
