function configurarCancelarIndemnizacion(){
	if(!esIndemnizacionCancelada()){
		jQuery("#cancelarIndemnizacionBtn").show();
		jQuery("#motivoIndemnizacion").removeAttr('disabled');
		jQuery("#motivoIndemnizacion").val('');
	}else{
		jQuery("#cancelarIndemnizacionBtn").hide();
	}
}

function mostrarVentanaCancelarIndemnizacion(idIndemnizacion){
	var url = mostrarCancelarPath + "?idIndemnizacion=" + idIndemnizacion;
	mostrarVentanaModal("vm_cancelarIndemnizacion", 'Cancelar Indemnizaci\u00F3n',  1, 1, 900, 320, url, null);
}

function cerrarVentanaCancelarIndemnizacion(){
	parent.cerrarVentanaModal('vm_cancelarIndemnizacion',null);
}

function cancelarIndemnizacion(){
	if(validarIndemnizacionDeterminada()){
		var formParams = jQuery(document.cancelarIndemnizacionForm).serialize();
		var url = cancelarIndemnizacionPath + '?' + formParams;
		var idIndemnizacion = jQuery("#idIndemnizacion").val();
		url += ("&filtroPT.idIndemnizacion=" + idIndemnizacion);
		parent.document.getElementById('urlCancelarIndemnizacion').value = url;
		parent.cerrarVentanaModal("vm_cancelarIndemnizacion", "redireccionaCancelarIndemnizacion();");
	}
}

function redireccionaCancelarIndemnizacion(){
	var url = jQuery("#urlCancelarIndemnizacion").val();
	sendRequestJQ(null, url, targetWorkArea, 'buscarPerdidasTotales(false);');
}

function validarIndemnizacionDeterminada(){
	return true;
}

function esIndemnizacionCancelada(){
	var estatusAutIndemnizacion = jQuery("#estatusAutIndemnizacion").val();
	if(estatusAutIndemnizacion == 'C'){
		return true;
	}else{
		return false;
	}
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}
