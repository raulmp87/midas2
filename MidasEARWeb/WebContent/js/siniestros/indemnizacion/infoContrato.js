function mostrarVentanaInfoContrato(){
	var idLiqSiniestro = jQuery("#idLiqSiniestro").val();
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	var esConsulta = jQuery("#esConsulta").val();
	var url = mostrarVentanaInfoContratoPath;
	console.log(url);
	if(idLiqSiniestro){
		url += "?liquidacionSiniestro.id=" + idLiqSiniestro;
	}else{
		url += "?idIndemnizacion=" + idIndemnizacion + "&esConsulta=" + esConsulta;
	}
	console.log("url: " + url);
	mostrarVentanaModal("vm_infoContrato", "Informacion de Contrato",  1, 1, 950, 260, url, null);
}

function cerrarVentanaInfoContrato(){
		if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){
			parent.cerrarVentanaModal('vm_infoContrato',null);
		}
}

function imprimirInfoContrato(){
	var idLiquidacion = jQuery("#idLiquidacion").val();
	if(validarContratoGuardado()){
		formParams = jQuery(document.infoContratoForm).serialize();
		var url = "";
		if(idLiquidacion){
			url = imprimirInfoContratoLiquidacionPath + '?' +  formParams ;
		}else{
			url = imprimirInfoContratoPath + '?' +  formParams ;
		}
		console.log(url);
	    window.open(url, "InformacionContrato");
	}else{
		mostrarVentanaMensaje("20","Debe de guardar la informacion para poder imprimir",null);
	}
}

function validarContratoGuardado(){
	var idInfoContrato = jQuery("#idInfoContrato").val();
	if(idInfoContrato){
		return true;
	}else{
		return false;
	}
}

function guardarInfoContrato(){
	if(validateAll(true)){
		console.log("validando los campos obligatorios");
		var idLiquidacion = jQuery("#idLiquidacion").val();
		if(idLiquidacion){
			var url = jQuery("#infoContratoForm").attr("action") + " " + jQuery(document.infoContratoForm).serialize();
			console.log("liquidacion action ANTES de cambiar " + url);
			jQuery("#infoContratoForm").attr("action","/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/guardarInfoContrato.action");
			url = jQuery("#infoContratoForm").attr("action") + " " + jQuery(document.infoContratoForm).serialize();
			console.log("liquidacion DESPUES de cambiar " + url);
			parent.submitVentanaModal("vm_infoContrato", document.infoContratoForm);
		}else{
			var url = jQuery(document.infoContratoForm).serialize();
			console.log("guardando desde la pantalla de indemnizacion " + url);
			jQuery("#infoContratoForm").attr("action","/MidasWeb/siniestros/indemnizacion/cartas/guardarInfoContrato.action");
			parent.submitVentanaModal('vm_infoContrato', document.infoContratoForm);
		}
	}
}

