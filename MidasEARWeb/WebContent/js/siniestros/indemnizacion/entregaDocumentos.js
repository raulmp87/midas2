function configurarEntregaDocumentos(){
	jQuery("#avisoguardar").hide();
	jQuery("#avisoimprimir").hide();
	configurarTituloEntregaDocumentos();
	validarTipoCarta();
	bloquearFactura();
	buscarFacturas();
}


function configurarTituloEntregaDocumentos(){
	var tipoCarta = jQuery("#tipoCarta").val();
	if(tipoCarta == 'BPPT'){
		jQuery(".perdidatotal").show();
	}else{
		jQuery(".robototal").show();
	}
}

function validarTipoCarta(){
	var tipoCarta = jQuery("#tipoCarta").val();
	if(tipoCarta == "BPPT" || tipoCarta == "BPRT"){
		jQuery(".validatipocarta").show();
	}
}

function bloquearFactura(){
	var idFactura = jQuery("#idFactura").val();
	if(idFactura){
		jQuery('#numeroFactura').attr("disabled",true);
		jQuery('#emisorFactura').attr("disabled",true);
		jQuery('#receptorFactura').attr("disabled",true);
	}
}

function mostrarVentanaEntregaDocumentos(){
	var tipoCarta = jQuery("#tipoCarta").val();
	var siniestroId = jQuery("#siniestroId").val();
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	var height = 600;
	if(tipoCarta == 'BPPT'){
		height = 560;
	}
	var url = mostrarEntregaDocumentosPath + "?siniestroId=" + siniestroId + "&tipoCarta=" + tipoCarta + "&idIndemnizacion=" + idIndemnizacion;
	mostrarVentanaModal("vm_entregaDocumentos", "Entrega de Documentos",  1, 1, 930, height, url, null);
}

function cerrarVentanaEntregaDocumentos(){
	parent.cerrarVentanaModal('vm_entregaDocumentos',null);
}

function imprimirEntregaDocumentos(){
	if(validarCartaGuardada()){
		var informacionCompleta = jQuery("#informacionCompleta").val(); 
		if(informacionCompleta == 'true'){
			formParams = jQuery(document.entregaDocumentosForm).serialize();
			var url = imprimirEntregaDocumentosPath + '?' +  formParams ;
		    window.open(url, "Entrega Documentos");
		}else{
			jQuery("#entregaDocumentosForm").attr("action","imprimirEntregaDocumentos.action");
			parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
		}
	}else{
		jQuery("#avisoimprimir").show();
	}
}

function guardarEntregaDocumentos(){
	if(validarGuardadoCarta()){
		jQuery("#entregaDocumentosForm").attr("action","guardarEntregaDocumentos.action");
		parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
	}
}

function validarGuardadoCarta(){
	return true;
}

var endososEntregaDocumentosGrid;
function buscarEndosos(){
	if(validarBusquedaEndosos()){
		jQuery("#endososEntregaDocumentosGrid").empty();
		
		endososEntregaDocumentosGrid = new dhtmlXGridObject('endososEntregaDocumentosGrid');
		endososEntregaDocumentosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		endososEntregaDocumentosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		
		formParams = jQuery(document.entregaDocumentosForm).serialize();
		var url = listarEndososPath + '?' +  formParams;
		endososEntregaDocumentosGrid.load( url );
	}
}

function validarBusquedaEndosos(){
	return true;
}

function agregarEndoso(){
	if(validaFacturaSeleccionada()){
		if(validarNombreEndoso()){
			jQuery("#esEliminarEndoso").val(false);
			jQuery("#entregaDocumentosForm").attr("action","relacionarEndoso.action");
			parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
		}else{
			alert('Es obligatorio capturar el nombre del endoso');
		}
	}else{
		alert('Debe seleccionar una factura para poder agregar un endoso');
	}
}

function validarNombreEndoso(){
	var nombreEndoso = jQuery("#nombreEndoso").val();
	if(nombreEndoso){
		return true;
	}
	return false;
}

function validaFacturaSeleccionada(){
	var idFactura = jQuery("#idFactura").val();
	if(idFactura){
		return true;
	}
	return false;
}

function validarCartaGuardada(){
	var idCartaEntrega = jQuery("#idCartaEntrega").val();
	if(idCartaEntrega){
		return true;
	}
	return false;
}

function eliminarEndoso(){
	if(validarEndosoSeleccionado()){
		jQuery("#listarEndososEliminar").val(endososEntregaDocumentosGrid.getCheckedRows(0));
		jQuery("#esEliminarEndoso").val(true);
		jQuery("#entregaDocumentosForm").attr("action","relacionarEndoso.action");
		parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
	}else{
		console.log("Debe seleccionar al menos un endoso para eliminar");
	}
}

function eliminarEndoso(idEndoso){
		jQuery("#listarEndososEliminar").val(idEndoso);
		jQuery("#esEliminarEndoso").val(true);
		jQuery("#entregaDocumentosForm").attr("action","relacionarEndoso.action");
		parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
}

function validarEndosoSeleccionado(){
	var endosoSeleccionado = endososEntregaDocumentosGrid.getCheckedRows(0);
	if(endosoSeleccionado){
		return true;
	}
	return false;
}

function limpiarEndosoTxt(){
	jQuery("#nombreEndoso").val('');
}

var facturasEntregaDocumentosGrid;
function buscarFacturas(){
		document.getElementById("facturasEntregaDocumentosGrid").innerHTML = '';	
		
		facturasEntregaDocumentosGrid = new dhtmlXGridObject('facturasEntregaDocumentosGrid');
		facturasEntregaDocumentosGrid.attachEvent("onXLS", function(grid_obj){blockPage()});
		facturasEntregaDocumentosGrid.attachEvent("onXLE", function(grid_obj){unblockPage()});
		facturasEntregaDocumentosGrid.attachEvent("onSubGridCreated",function(sub,id,ind,value){			
			 sub.callEvent("onGridReconstructed",[])
			     sub.load("/MidasWeb/siniestros/indemnizacion/cartas/listarEndosos.action?factura.id=" + id,function(){
		    	 sub.callEvent("onGridReconstructed",[]);
		    	 sub.setSizes();
		    	 facturasEntregaDocumentosGrid.setSizes();
		    	 
			     });
		     return false;  
		});
		facturasEntregaDocumentosGrid.attachEvent("onRowSelect", function(id,ind){
			idFactura		= id;
			numeroFactura	= facturasEntregaDocumentosGrid.cells(id,1).getValue();
		    emisorFactura	= facturasEntregaDocumentosGrid.cells(id,2).getValue();
			receptorFactura	= facturasEntregaDocumentosGrid.cells(id,3).getValue();
			
			jQuery('#idFactura').val(idFactura);
			jQuery('#numeroFactura_h').val(numeroFactura);
			jQuery('#emisorFactura_h').val(emisorFactura);
			jQuery('#receptorFactura_h').val(receptorFactura);
			
			jQuery('#numeroFactura').val(numeroFactura);
			jQuery('#emisorFactura').val(emisorFactura);
			jQuery('#receptorFactura').val(receptorFactura);
			jQuery('#numeroFactura').attr("disabled",true);
			jQuery('#emisorFactura').attr("disabled",true);
			jQuery('#receptorFactura').attr("disabled",true);
		});
		
		formParams = jQuery(document.entregaDocumentosForm).serialize();
		var url = listarFacturasPath + '?' +  formParams;
		console.log(url);
		facturasEntregaDocumentosGrid.load( url );
}

function agregarFactura(){
	if(validarInformacionFactura()){
		if(validarCartaGuardada()){
			jQuery("#esEliminarEndoso").val(false);
			jQuery("#numeroFactura_h").val(jQuery("#numeroFactura").val());
			jQuery("#emisorFactura_h").val(jQuery("#emisorFactura").val());
			jQuery("#receptorFactura_h").val(jQuery("#receptorFactura").val());
			console.log(jQuery("#numeroFactura_h").val() + "," + jQuery("#emisorFactura").val() + "," + jQuery("#receptorFactura").val());
			jQuery("#entregaDocumentosForm").attr("action","relacionarFactura.action");
			parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
		}else{
			jQuery("#avisoguardar").show();
		}
	}else{
		alert("Debe capturar todos los campos de la factura");
	}
}

function validarInformacionFactura(){
	var numeroFactura = jQuery("#numeroFactura").val();
	var emisorFactura = jQuery("#emisorFactura").val();
	var receptorFactura = jQuery("#receptorFactura").val();
	if(numeroFactura
			&& emisorFactura
			&& receptorFactura){
		return true;
	}else{
		return false;
	}
}

function eliminarFactura(){
	if(validarFacturaSeleccionado()){
		jQuery("#listarEndososEliminar").val(facturasEntregaDocumentosGrid.getCheckedRows(1));
		jQuery("#esEliminarEndoso").val(true);
		jQuery("#entregaDocumentosForm").attr("action","relacionarFactura.action");
		parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
	}else{
		alert("Debe seleccionar al menos un endoso para eliminar");
	}
}

function eliminarFactura(idFactura){
		var idFacturaSeleccionada = jQuery("#idFactura").val();
		if(idFactura == idFacturaSeleccionada){
			limpiarFacturaTxt();
		}
		jQuery("#listarEndososEliminar").val(idFactura);
		jQuery("#esEliminarEndoso").val(true);
		jQuery("#entregaDocumentosForm").attr("action","relacionarFactura.action");
		parent.submitVentanaModal("vm_entregaDocumentos", document.entregaDocumentosForm);
}

function validarFacturaSeleccionado(){
	var facturaSeleccionado = facturasEntregaDocumentosGrid.getCheckedRows(1);
	if(facturaSeleccionado){
		return true;
	}
	return false;
}

function limpiarFacturaTxt(){
	jQuery("#idFactura").val('');
	jQuery("#numeroFactura").val('');
	jQuery("#emisorFactura").val('');
	jQuery("#receptorFactura").val('');
	jQuery("#numeroFactura").removeAttr("disabled");
	jQuery("#emisorFactura").removeAttr("disabled");
	jQuery("#receptorFactura").removeAttr("disabled");
	jQuery("#numeroFactura_h").val('');
	jQuery("#emisorFactura_h").val('');
	jQuery("#receptorFactura_h").val('');
	jQuery("#nombreEndoso").val('');
}