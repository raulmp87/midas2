function configurarAutorizacion(){
	var tipoAutorizacion = jQuery('#tipoAutorizacion').val();
	var TIPO_PERDIDA_TOTAL = "AT";
	var ACEPTADA = "A";
	var RECHAZADA = "R";
	var REGISTRADA = "G";
	var CANCELADA = "C";
	if(tipoAutorizacion == TIPO_PERDIDA_TOTAL){
		jQuery('#tituloPerdidaTotal').show();
		jQuery('.perdidatotal').show();
		var estatusAutPerdidaTotal = jQuery('#estatusAutPerdidaTotal').val();
		if(estatusAutPerdidaTotal != ACEPTADA && estatusAutPerdidaTotal != RECHAZADA ){
			jQuery('.habilitar').show();
			jQuery('.activar').removeAttr('disabled');
		}
	}else{
		jQuery('#tituloIndemnizacion').show()
		jQuery('.indemnizacion').show();
		var estatusAutIndemnizacion = jQuery('#estatusAutIndemnizacion').val();
		if(estatusAutIndemnizacion != ACEPTADA && estatusAutIndemnizacion != RECHAZADA  && estatusAutIndemnizacion != CANCELADA){
			jQuery('.habilitar').show();
			jQuery('.activar').removeAttr('disabled');
		}
	}
}

function mostrarAutorizar(idOrdenCompra, tipoAutorizacion,origen){
	var url = mostrarAutorizarPath + "?idOrdenCompra=" + idOrdenCompra + "&tipoAutorizacion=" + tipoAutorizacion+ "&pantallaOrigen=" + origen;
	var titulo = "";
	if(tipoAutorizacion == "AT"){
		titulo = 'Autorizaci\u00F3n de P\u00E9rdida Total';
	}else{
		titulo = 'Autorizaci\u00F3n de Indemnizaci\u00F3n';
	}
	mostrarVentanaModal("vm_autorizarPT", titulo,  1, 1, 900, 400, url, 'buscarPerdidasTotales(true);');
}

function autorizar(){
	removeCurrencyFormatOnTxtInput();
	var tipoAutorizacion = jQuery("#tipoAutorizacion").val();
	if(tipoAutorizacion == "AT" || validarMontoaIndemnizar()){
		jQuery("#autorizacionForm").attr("action","autorizar.action");
		parent.submitVentanaModal("vm_autorizarPT", document.autorizacionForm);
	}else{
		var msj = "";
		var esPagoDanios = jQuery("#esPagoDanios").val();
		if(esPagoDanios == 'true'){
			jQuery("#avisoimprimirpd").show();
		}else{
			jQuery("#avisoimprimirind").show();
		}
	}
	initCurrencyFormatOnTxtInput();
}

function validarMontoaIndemnizar(){
	var totalIndemnizacion = jQuery("#totalIndemnizacion").toNumber().val();
	if(totalIndemnizacion && totalIndemnizacion > 0){
		return true;
	}else{
		return false;
	}
}

function rechazar(){
	removeCurrencyFormatOnTxtInput();
	jQuery("#autorizacionForm").attr("action","rechazar.action");
	parent.submitVentanaModal("vm_autorizarPT", document.autorizacionForm); 
	initCurrencyFormatOnTxtInput();
}


function mostrarPaginaHGS(){
	var urlHGS = "https://efile.highgrade.com.mx/efile-web";
	window.open(urlHGS, "HGS");
}

function cerrarVentanaAutorizacion(){
	var pantallaOrigen = jQuery("#pantallaOrigen").val();
	if(pantallaOrigen != 'DT'){
		var idIndemnizacion = jQuery("#idIndemnizacion").val();
		parent.document.getElementById('idIndemnizacion').value = idIndemnizacion;
		parent.cerrarVentanaModal('vm_autorizarPT','buscarPerdidasTotales(false);');
	}else{
		parent.cerrarVentanaModal('vm_autorizarPT','buscarPerdidasTotales(true);');
	}
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}