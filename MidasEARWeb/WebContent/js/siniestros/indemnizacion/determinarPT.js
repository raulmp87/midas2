var TIPO_SINIESTRO_ROBO_TOTAL = "RB";
var TIPO_SINIESTRO_PERDIDA_TOTAL = "PT";
var ESTATUS_PERDIDA_TOTAL_PENDIENTE = 'P';

function mostrarDeterminacionPT(idIndemnizacion,tipoSiniestroDesc, tipoSiniestro, esConsulta,idOrdenCompra){
	var url = mostrarDeterminacionPath + "?idIndemnizacion=" + idIndemnizacion+"&tipoSiniestroDesc=" + tipoSiniestroDesc +"&idOrdenCompra=" + idOrdenCompra 
		+ "&tipoSiniestro=" + tipoSiniestro + "&esConsulta=" + esConsulta ;
	sendRequestJQ(null, url, targetWorkArea, null);
}

function configurarDeterminacionPT(){
	configurarBotonesDeterminacionRegistrada();
	formato2DecimalesOnLoad();
	habilitarCapturaTotalPD();
	configurarModoConsulta();
	calcularSubtotal1();
	calcularTotalaIndemnizar();
	calcularTotalDanosPiezas();
	 var tipoSiniestro = jQuery("#tipoSiniestro").val();
	 var estatusPT = jQuery("#estatusPT").val();

	 
	 var etapa = jQuery("#etapa").val();
	if (tipoSiniestro!=TIPO_SINIESTRO_ROBO_TOTAL){
		bloquearCriteriosRobos(true);
		if(etapa!='DT'  || estatusPT!='G'){ //validacion debe estar en etapa de determinacion y haber sido registrada

			jQuery("#btn_Autorizar").hide();
		}
		
	}else{
		jQuery("#btn_Autorizar").hide();
		//Solo puede autorizar indemnizaciones de PT, se omite ROBO
	}
	
	iniAutorizacionesGrid();
	
	
}



function iniAutorizacionesGrid(){
	removeCurrencyFormatOnTxtInput();
	var formParams  = jQuery(document.determinacionPerdidaTotalForm).serialize();
	document.getElementById("autorizacionGrid").innerHTML = '';
	grid = new dhtmlXGridObject("autorizacionGrid");
	grid.attachEvent("onXLS", function(grid_obj){blockPage()});
	grid.attachEvent("onXLE", function(grid_obj){
		unblockPage();
	
	});
	grid.attachEvent("onXLS", function(grid){
			mostrarIndicadorCarga("indicador");
	   });
	grid.attachEvent("onXLE", function(grid){
			ocultarIndicadorCarga('indicador');
	   });
	var url = mostrarGridAutorizarPath + '?' +  formParams;
	grid.load( url);
	initCurrencyFormatOnTxtInput();
}

function guardarDeterminacionPT(){
	if(validarGuardarDeterminacion()){
		removeCurrencyFormatOnTxtInput();
		
		var bnfEdit = jQuery("#bnfEdited").val();
		
		if(bnfEdit == "" || bnfEdit==null){
			alert("El nombre del beneficiario no puede ir vacío");
		}else{
			formParams = jQuery(document.determinacionPerdidaTotalForm).serialize();
			var url = guardarDeterminacionPath + '?' +  formParams  + '&nombreBeneficiarioEditable='+bnfEdit;
			sendRequestJQ(null, url, targetWorkArea, null);
		}
		
	}else{
		mostrarVentanaMensaje("10","El total a indemnizar debe ser mayor a 0",null);
	}
}

function guardarEImprimirDeterminacionPT(){
	if(validarImpresionDeterminacion()){
		removeCurrencyFormatOnTxtInput();
		formParams = jQuery(document.determinacionPerdidaTotalForm).serialize();
		var url = guardarDeterminacionPath + '?' +  formParams;
		sendRequestJQ(null, url, targetWorkArea, "imprimirDeterminiacionPT();");
	}
	initCurrencyFormatOnTxtInput();
}

function imprimirDeterminacionPT(){
	if(validarDeterminacionGuardada()){
		removeCurrencyFormatOnTxtInput();
		formParams = jQuery(document.determinacionPerdidaTotalForm).serialize();
		var url = imprimirDeterminacionPath + '?' +  formParams ;
	    window.open(url, "DeterminacionPerdidaTotal");
	}else{
		mostrarVentanaMensaje("10","Debe guardar el documento para poder imprimir",null);
	}
	initCurrencyFormatOnTxtInput();
}

function validarDeterminacionGuardada(){
	var registroGuardado = jQuery("#idIndemnizacion").val();
	if(registroGuardado){
		return true;
	}
	return false;
}

function validarGuardarDeterminacion(){
	if(jQuery("#totalIndemnizar").toNumber().val() > 0){
		return true;
	}else{
		return false;
	}
}

function validarImpresionDeterminacion(){
	return true;
}

function regresarACatalogoPerdidaTotal(){
	if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){
		var idIndemnizacion = jQuery("#idIndemnizacion").val();
		var url = mostrarBusquedaPath + "?filtroPT.idIndemnizacion=" + idIndemnizacion;
		sendRequestJQ(null, url, targetWorkArea, 'buscarPerdidasTotales(false);');
	}
}

function mostrarBajaPlacas(){
	var tipoBajaPlacas = jQuery("#tipoBajaPlacas").val();
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	mostrarVentanaBajaPlacas(idIndemnizacion, tipoBajaPlacas);
}

function imprimirNotificacionPT(){
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	var idValuador = jQuery("#idValuador").val();
	imprimirNotificacionPerdidaTotal(idIndemnizacion, idValuador);
}

function onChangeValorUnidad(){
	//calcularDeducibleValorUnidad();
	calcularSubtotal1();
	calcularTotalaIndemnizar();
}

function calcularDeducibleValorUnidad(){
	var valorUnidad = jQuery("#valorUnidad").toNumber().val();
	var porcentajeDeducible = 0.10;
	if(valorUnidad){
		valorUnidad = Number(jQuery("#valorUnidad").toNumber().val()).toFixed(2);
		jQuery("#valorUnidad").val(valorUnidad);
		var deducible = Number(valorUnidad) * porcentajeDeducible;
		deducible = Number(deducible).toFixed(2);
		jQuery("#deducible").val(deducible);
	}else{
		jQuery("#deducible").val("0.00");
	}
}

function calcularSubtotal1(){
	var valorUnidad = jQuery("#valorUnidad").toNumber().val();
	var deducible = jQuery("#deducible").toNumber().val();
	if(valorUnidad && deducible){
		var subtotal = Number(valorUnidad) - Number(deducible);
		jQuery("#subTotal").val(Number(subtotal).toFixed(2));
	}else{
		jQuery("#subTotal").val("0.00");
	}
	validarMaximoMontoDescuento();
}

function calcularDeducibleValorEquipo(){
	var valorEquipo = jQuery("#valorEqAdaptaciones").toNumber().val();
	var porcentajeDeducibleEquipo = 0.10;
	if(valorEquipo){
		jQuery("#valorEqAdaptaciones").val(Number(jQuery("#valorEqAdaptaciones").toNumber().val()).toFixed(2));
		var deducibleEquipo = Number(valorEquipo) * porcentajeDeducibleEquipo;
		deducibleEquipo = Number(deducibleEquipo).toFixed(2);
		jQuery("#deducibleEqAdaptaciones").val(deducibleEquipo);
	}else{
		jQuery("#deducibleEqAdaptaciones").val("0.00");
	}
}

function calcularTotalaIndemnizar(){
	var subtotal1 = jQuery("#subTotal").toNumber().val();
	var primasPendientes = jQuery("#primasPendientes").toNumber().val();
	var descuento = jQuery("#descuento").toNumber().val();
	var valorEquipo = jQuery("#valorEqAdaptaciones").toNumber().val();
	var valorEquipoDeducible = jQuery("#deducibleEqAdaptaciones").toNumber().val();
	if(!subtotal1){ 
		subtotal1 = 0.0; 
		jQuery("#subTotal").val(subtotal1.toFixed(2));
	}
	if(!descuento){
		descuento = 0.0;
		jQuery("#descuento").val(descuento.toFixed(2));
	}
	if(!valorEquipo){
		valorEquipo = 0.0;
		jQuery("#valorEqAdaptaciones").val(valorEquipo.toFixed(2));
	}
	if(!valorEquipoDeducible){
		valorEquipoDeducible = 0.0;
		jQuery("#deducibleEqAdaptaciones").val(valorEquipoDeducible.toFixed(2));
	}
	if(!primasPendientes){
		primasPendientes = 0.0;
	}
	primasPendientes = Number(primasPendientes).toFixed(2);
	jQuery("#primasPendientes").val(primasPendientes);
	var totalIndemnizar = Number(subtotal1) - Number(descuento) + ( Number(valorEquipo) - Number(valorEquipoDeducible) ) - Number(primasPendientes);
	jQuery("#totalIndemnizar").val(Number(totalIndemnizar).toFixed(2));
}


//AIGG INDEMNIZACION
function calcularTotalDanosPiezas(){
	var manoObraHP1 = jQuery("#manoObraHP").toNumber().val();
	var refaccionesCarroceria = jQuery("#refaccionesCarroceria").toNumber().val();
	if(!manoObraHP1){ 
		manoObraHP1 = 0.0; 
		jQuery("#manoObraHP").val(manoObraHP1.toFixed(2));
	}
	if(!refaccionesCarroceria){
		refaccionesCarroceria = 0.0;
	}
	refaccionesCarroceria = Number(refaccionesCarroceria).toFixed(2);
	jQuery("#refaccionesCarroceria").val(refaccionesCarroceria);
	var total = Number(manoObraHP1) + Number(refaccionesCarroceria);
	jQuery("#totalMoPiezas").val(Number(total).toFixed(2));
}


function formato2DecimalesOnLoad(){
	jQuery(".dosDecimales").each(function(i){
		formato2Decimales(this);
	});
}

function formato2Decimales(component){
	if(!isNaN(jQuery(component).val())){
		jQuery(component).val(Number(jQuery(component).val()).toFixed(2));
	}
}

function configurarModoConsulta(){
	var esConsulta = jQuery("#esConsulta").val();
	if(esConsulta == 1){
		jQuery(".esConsulta").attr("readonly","true");
		jQuery(".deshabilitar").attr("disabled","disabled");
		jQuery("#btn_guardar").remove();
		jQuery(":checkbox").click(function(){
		    return false;
		});
	}
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}

function habilitarCapturaTotalPD(){
	var esPagoDanios = jQuery("#esPagoDanios").attr('checked');
	if(esPagoDanios){
		jQuery("#totalPagoDanios").attr("readonly",false);
	}else{
		jQuery("#totalPagoDanios").attr("readonly",true);
	}
}

function validarTotalPagoDanios(){
	var totalIndemnizar = jQuery("#totalIndemnizar").toNumber().val();
	var totalPagoDanios = jQuery("#totalPagoDanios").toNumber().val();
	if(Number(totalPagoDanios) > Number(totalIndemnizar)){
		mostrarVentanaMensaje("20","El total de pago de da\u00F1os no puede ser mayor que el total a indemnizar",null);
		jQuery("#totalPagoDanios").val(totalIndemnizar);
	}else{
		jQuery("#totalPagoDanios").val(Number(totalPagoDanios).toFixed(2));
	}
}

function mostrarPaginaHGS(){
	var urlHGS = "https://efile.highgrade.com.mx/efile-web";
	window.open(urlHGS, "HGS");
}

function calculaGuias(){
	var guiaNada = jQuery("#guiaNada").toNumber().val();
	var guiaKbb = jQuery("#guiaKbb").toNumber().val();
	var guiaEbc = jQuery("#guiaEbc").toNumber().val();
	var guiaAutometrica = jQuery("#guiaAutometrica").toNumber().val();
	
	var contador = parseFloat(0);
	var suma = parseFloat(0);
	
	if(null!=guiaNada && guiaNada!="" ){
		if(guiaNada>0){
			contador = parseFloat(contador)+ parseFloat(1);
		}
		suma = parseFloat(suma)+ parseFloat(guiaNada);
	}
	
	if(null!=guiaKbb && guiaKbb!=""  ){
		if(guiaKbb>0){
			contador = parseFloat(contador)+ parseFloat(1);
		}
		suma = parseFloat(suma)+ parseFloat(guiaKbb);

	}
	
	if(null!=guiaEbc && guiaEbc!=""  ){
		if(guiaEbc>0){
			contador = parseFloat(contador)+ parseFloat(1);
		}
		suma = parseFloat(suma)+ parseFloat(guiaEbc);
	}
	
	if(null!=guiaAutometrica && guiaAutometrica!=""  ){
		if(guiaAutometrica>0){
			contador = parseFloat(contador)+ parseFloat(1);
		}
		suma = parseFloat(suma)+ parseFloat(guiaAutometrica);
	}
	
		jQuery("#sumaGuias").val(suma);
		jQuery("#numeroGuias").val(contador);
		
	if (suma!=0 && contador!=0){
		var prom =(suma/contador);
		jQuery("#promedioGuias").val(prom);
	}else{
		jQuery("#promedioGuias").val(0);

	}
}


function bloquearCriteriosRobos(bolean){
	if (bolean==true){
		jQuery("#derivadoAutoplazo").attr('disabled','disabled');
		jQuery("#personaMoralRenovacion").attr('disabled','disabled');
		jQuery("#personaFisicaRenovacion").attr('disabled','disabled');
		jQuery("#unidadInspecContratacion").attr('disabled','disabled');
		jQuery("#polizaAnteriorInmediata").attr('disabled','disabled');
		jQuery("#siniestrosAnteriores").attr('disabled','disabled');
		
	}else{
		jQuery("#derivadoAutoplazo").removeAttr("disabled");
		jQuery("#personaMoralRenovacion").removeAttr("disabled");
		jQuery("#personaFisicaRenovacion").removeAttr("disabled");
		jQuery("#unidadInspecContratacion").removeAttr("disabled");
		jQuery("#polizaAnteriorInmediata").removeAttr("disabled");
		jQuery("#siniestrosAnteriores").removeAttr("disabled");
	}
}


function mostrarAutorizacionDT(origen){
	var idIndemnizacion = jQuery('#idIndemnizacion').val();
	var tipoSiniestroDesc = jQuery('#tipoSiniestroDesc').val();
	var tipoSiniestro = jQuery('#tipoSiniestro').val();
	var idIndemnizacion = jQuery('#idIndemnizacion').val();
	var esConsulta = 0;
	var idOrdenCompra = jQuery('#idOrdenCompra').val();	
	var tipoAutorizacion="AT";
	var titulo = "";
	titulo = 'Autorizaci\u00F3n de P\u00E9rdida Total';
	
	var metodo;
	metodo="mostrarDeterminacionPT("+idIndemnizacion+",'"+tipoSiniestroDesc+"','"+tipoSiniestro+"',"+esConsulta+","+idOrdenCompra+");";
	var url = mostrarAutorizarPath + "?idOrdenCompra=" + idOrdenCompra + "&tipoAutorizacion=" + tipoAutorizacion+  "&metodoInvocar=" + metodo+ "&pantallaOrigen=" + origen ;
	mostrarVentanaModal("vm_autorizarPT", titulo,  1, 1, 900, 400, url,metodo );
}

function configurarBotonesDeterminacionRegistrada(){
	var estatusPT = jQuery("#estatusPT").val();
	if(estatusPT != ESTATUS_PERDIDA_TOTAL_PENDIENTE){
		jQuery('.determinacionRegistrada').show();
	}
}

function validarMaximoMontoDescuento(){
	var maximoMontoDescuento = calcularMaximoMontoDescuento();
	var descuento = jQuery("#descuento").val();
	if(descuento
			&&  jQuery("#descuento").toNumber().val() > maximoMontoDescuento ){
		jQuery("#descuento").val(maximoMontoDescuento);
	}
}

function calcularMaximoMontoDescuento(){
	var subtotal1 = jQuery("#subTotal").toNumber().val();
	var primasPendientes = jQuery("#primasPendientes").toNumber().val();
	if(!subtotal1){ 
		subtotal1 = 0.0; 
		jQuery("#subTotal").val(subtotal1.toFixed(2));
	}
	if(!primasPendientes){
		primasPendientes = 0.0;
	}
	primasPendientes = Number(primasPendientes).toFixed(2);
	jQuery("#primasPendientes").val(primasPendientes);
	var maximoMonto = Number(subtotal1) - Number(primasPendientes);
	if(maximoMonto < 0){
		maximoMonto = 0.0;
		maximoMonto = maximoMonto.toFixed(2);
	}
	return maximoMonto;
}

function onChangeDeducible(id){
	removeCurrencyFormatOnTxtInput();
	var montoTotal = 0;		
	jQuery("input[id='"+id+"']").each(function (i, el) {
		if(jQuery(this).val() == null || jQuery(this).val() == ''){
			jQuery(this).val(0);
		}		
		montoTotal = montoTotal + parseFloat(jQuery(this).val());		
    });	
	jQuery('#deducible').val(montoTotal);
	onChangeValorUnidad();
	initCurrencyFormatOnTxtInput();
}

function calcularPorcDaniosVsValorUnidad(){
	var valorUnidad = jQuery("#valorUnidad").toNumber().val();
	var danios = jQuery("#totalMoPiezas").toNumber().val();
	var porcentaje = 0.0;
	if(!valorUnidad){ 
		valorUnidad = 0.0; 
		jQuery("#valorUnidad").val(valorUnidad.toFixed(2));
	}
	if(!danios){ 
		danios = 0.0; 
		jQuery("#totalMoPiezas").val(danios.toFixed(2));
	}
	if(valorUnidad == 0){
		jQuery("#porcentajeDanioValorUnidad").val(porcentaje.toFixed(2) );
	}else{
		porcentaje = danios / valorUnidad * 100;
		jQuery("#porcentajeDanioValorUnidad").val(porcentaje.toFixed(2) );
	}
}