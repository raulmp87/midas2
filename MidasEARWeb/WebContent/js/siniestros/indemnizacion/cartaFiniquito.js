function configurarCartaFiniquito(){
	configurarTipoFiniquitoNotNull();
	configurarTituloCartaFiniquito();
}

function configurarTipoFiniquitoNotNull(){
	var esFiniquitoAsegurado = jQuery("#esFiniquitoAsegurado").val();
	if(esFiniquitoAsegurado != null){
		jQuery(".validarTipoFiniquitoNotNull").show();
	}
}

function configurarTituloCartaFiniquito(){
	var esFiniquitoAsegurado = jQuery("#esFiniquitoAsegurado").val();
	if(esFiniquitoAsegurado == 'true'){
		jQuery(".asegurado").show();
	}else{
		jQuery(".tercero").show();
	}
}

function mostrarVentanaCartaFiniquito(idIndemnizacion, tipoSiniestro){
	var esFiniquitoAsegurado = jQuery("#esFiniquitoAsegurado").val();
	var height = 230;
	if(esFiniquitoAsegurado == 'true'){
		height = 195;
	}
	var url = mostrarCartaFiniquitoPath + "?idIndemnizacion=" + idIndemnizacion + "&tipoSiniestro=" + tipoSiniestro;
	mostrarVentanaModal("vm_cartaFiniquito", "Carta de Finiquito",  1, 1, 900, height, url, null);
}

function cerrarVentanaFiniquito(){
	parent.cerrarVentanaModal('vm_cartaFiniquito',null);
}

function imprimirCartaFiniquito(){
	jQuery("#cartaFiniquitoForm").attr("action","imprimirCartaFiniquito.action");
	parent.submitVentanaModal("vm_cartaFiniquito", document.cartaFiniquitoForm); 
}

function imprimirCartaFiniquitoWO(){
		formParams = jQuery(document.cartaFiniquitoForm).serialize();
		var url = imprimirDeterminacionPath + '?' +  formParams ;
	    window.open(url, "CartaFiniquito");
}

