function mostrarPerdidasTotales(){
	var url = '/MidasWeb/siniestros/indemnizacion/perdidatotal/mostrarCatalogo.action'
	sendRequestJQ(null, url, targetWorkArea, 'buscarPerdidasTotales(true);');
}

var listadoPerdidaTotalGrid;
function buscarPerdidasTotales(mostrarListadoVacio){
	if(validarBusqueda(mostrarListadoVacio)){
		jQuery("#tituloListado").show();
		jQuery("#perdidaTotalGridContainer").show();	
		jQuery("#listadoPerdidaTotalGrid").empty(); 
		jQuery("#mostrarListadoVacio").val(mostrarListadoVacio);
		
		if(listadoPerdidaTotalGrid){listadoPerdidaTotalGrid.destructor();}
		listadoPerdidaTotalGrid = new dhtmlXGridObject('listadoPerdidaTotalGrid');
		listadoPerdidaTotalGrid.attachEvent("onXLS", function(grid_obj){blockPage();});
		listadoPerdidaTotalGrid.attachEvent("onXLE", function(grid_obj){unblockPage();});
		
		formParams = jQuery(document.busquedaPerdidasTotales).serialize();
		var url = listarPath + '?' +  formParams;
		listadoPerdidaTotalGrid.load( url );
		jQuery("#idIndemnizacion").val("");
	}
}

function validarBusqueda(mostrarListadoVacio){
	var contador = 0;
	var a = new Array();
	a[0] = 	 jQuery("#idIndemnizacion").val();
	a[1] = 	 jQuery("#oficina_s").val();
	a[2] =   jQuery("#tipoSiniestro_s").val();
	a[3] = 	 jQuery("#nsOficina").val();
	a[4] =   jQuery("#nsConsecutivoR").val();
	a[5] =   jQuery("#nsAnio").val();
	a[6] =   jQuery("#nrOficina").val();
	a[7] =   jQuery("#nrConsecutivoR").val();
	a[8] =   jQuery("#nrAnio").val();
	a[9] = 	 jQuery("#numPoliza_t").val();
	a[10] =  jQuery("#numSerie_t").val();
	a[11] =  jQuery("#numValuacion_t").val();
	a[12] =  jQuery("#txtFechaSiniestro").val();
	a[13] =  jQuery("#etapa_s").val();
	a[14] =  jQuery("#cobertura_s").val();
	a[15] =  jQuery("#estatusPT_s").val();
	a[16] =  jQuery("#estatusIndemnizacion_s").val();
	a[17] =  jQuery("#numPaseAtencion_t").val();
	
	for (x=0; x< a.length; x++){		 	
			if(a[x] != ""){				
				contador++;
			}	
		}
	 if(contador >2 || mostrarListadoVacio){
		 return true;
	 }
	 mostrarVentanaMensaje("20","Capture por lo menos tres datos para continuar con la búsqueda",null);
		return false;
}

function limpiarFiltros(){
	jQuery('#busquedaPerdidasTotales').each (function(){
		  this.reset();
	});
}

function exportarExcel(){
	if(validarBusqueda(false)){
		var url="/MidasWeb/siniestros/indemnizacion/perdidatotal/exportarResultados.action?" + jQuery(document.busquedaPerdidasTotales).serialize();
		window.open(url, "Perdidas Totales");
	}
}

/**
 * muestra pantalla de Formato de Fechas para Perdida Total
 * @param idOrdenCompra
 */
function mostrarFormatoFecha( idOrdenCompra, consulta ,tipoSiniestro, idIndemnizacion){
	var formParams = jQuery(document.busquedaPerdidasTotales).serialize();
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/mostrarFechasPerdidaTotal.action?idOrdenCompra=' + idOrdenCompra + 
																						'&consulta=' + consulta + "&" + '&tipoSiniestro=' + tipoSiniestro + "&idIndemnizacion=" + idIndemnizacion + "&" +formParams;
	if( consulta == 1 ){
		sendRequestJQ( null, url, targetWorkArea, "setConsultaFormatoFechasPT();" );
	}else{
		sendRequestJQ( null, url, targetWorkArea, null );
	}
}

function imprimirCartaPTS(siniestroId){
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/imprimirCartaPTS.action?siniestroId=' + siniestroId;
	window.open(url, "CartaPTSNoDocumentada");
}