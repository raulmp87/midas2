function mostrarRecepcionDoctos(idSiniestro, tipoBajaPlacas, esConsulta, idIndemnizacion, tipoSiniestro){
	jQuery("#esConsulta").val(esConsulta);
	var busquedaFormParams = jQuery(document.busquedaPerdidasTotales).serialize();
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/recepciondoctos/mostrarRecepcionDoctos.action?siniestroId='
		+ idSiniestro + "&tipoCarta=" + tipoBajaPlacas + "&idIndemnizacion=" + idIndemnizacion + "&tipoSiniestro=" + tipoSiniestro+ "&" + busquedaFormParams; 
	sendRequestJQ(null, url, targetWorkArea, null);
}

function guardarRecepcionDocumentos(){
	var esPagoDanios = jQuery("#esPagoDanios").attr('checked');
	if(validaRequeridos() && validaPension()){
		if(esPagoDanios || validarTenencias()){
			var formParams = jQuery(document.recepcionDoctosForm).serialize();
			var url = guardarPath+ '?' +  formParams;
			sendRequestJQ(null, url, targetWorkArea, "mostrarBotonEntregaDocumentos();");
		}else{
			mostrarVentanaMensaje("10","Debe entregar todas las tenencias y refrendos para poder guardar",null);
		}
	}else{
		mostrarVentanaMensaje("10","Los campos Ubicacion, Direccion, Disposicion y Pension son obligatorios",null);
	}
}

function guardarEImprimirDocumentos(){
	if(validarTenencias()){
		var formParams = jQuery(document.recepcionDoctosForm).serialize();
		var url = guardarPath+ '?' +  formParams;
		sendRequestJQ(null, url, targetWorkArea, "imprimirRecepcionDocumentos();");
	}else{
		mostrarVentanaMensaje("10","Debe entregar todas las tenencias y refrendos para poder guardar",null);
	}
}

function imprimirRecepcionDocumentos(){
	if(validarRegistroGuardado()){
		if(comprobarGuardado()){
			formParams = jQuery(document.recepcionDoctosForm).serialize();
			var url = imprimirPath + "?" + formParams ;
			window.open(url, "RecepcionDocumentos");
		    mostrarBotonEntregaDocumentos();
		}else{
			mostrarVentanaMensaje("10","No se pudo imprimir el documento",null);
		}
	}else{
		mostrarVentanaMensaje("10","Debe guardar los datos para poder imprimir",null);
	}
}

function initConfiguration(){
	var esConsulta = jQuery("#esConsulta").val();
	var esMostrarCartaPT = jQuery("#mostrarCartaPT").val();
	var tipoSiniestro = jQuery("#tipoSiniestro").val();
	if(esConsulta == 1){
		jQuery(".deshabilitar").attr("readonly","true");
		jQuery("#btn_guardar").remove();
		jQuery("#btn_entrega").remove();
		jQuery(":radio,:checkbox").click(function(){
		    return false;
		});
		jQuery("#bancosList").attr('disabled','disabled');
	}
	if(!validarRegistroGuardado()){
		jQuery("#btn_entrega").hide();
	}
	if(esMostrarCartaPT=='false'){
		jQuery("#btn_cartaPTS").hide();
	}
	if(tipoSiniestro!='RB'){
		jQuery("#denunciaRobo_rSI").attr('disabled','disabled');
		jQuery("#denunciaRobo_rNO").attr('disabled','disabled');
		jQuery("#denunciaRobo_rNA").attr('disabled','disabled');

	}
	deshabilitarPagoDaniosSiCheckedAlMostrar();
}

function deshabilitarPagoDaniosSiCheckedAlMostrar(){
	var esPagoDanios = jQuery("#esPagoDanios").attr('checked');
	if(esPagoDanios){
		jQuery("#esPagoDanios").click(function(){
			return false;
		});
	}
}

function mostrarBotonEntregaDocumentos(){
	if(validarRegistroGuardado()){
		jQuery("#btn_entrega").show();
		jQuery("#btn_cartaPTS").hide();
		
	}
}

function comprobarGuardado(){
	var guardadoCorrecto = jQuery("#guardadoCorrecto").val();
	if(guardadoCorrecto == 1){
		return true;
	}
	return false;
}

function validarTenencias(){
	var tenenciasSeleccionadas = true;
	jQuery('input:checkbox[name=recepcionDoctoSin.refrendoTenencia]').each(function(i){
		if(!jQuery(this).attr('checked')){
			tenenciasSeleccionadas = false;
		}
	});
	return tenenciasSeleccionadas;
}

function validarRegistroGuardado(){
	var registroGuardado = jQuery("#recepcionDoctosId").val();
	if(registroGuardado){
		return true;
	}
	return false;
}

function regresarACatalogoPerdidaTotal(){
	if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){
		var idIndemnizacion = jQuery("#idIndemnizacion").val();
		var url = mostrarBusquedaPath + "?filtroPT.idIndemnizacion=" + idIndemnizacion;
		sendRequestJQ(null, url, targetWorkArea, 'buscarPerdidasTotales(false);');
	}
}

/**
 * @deprecated Se deja de usar porque no existe caso de uso que diga que hay que bloquear los campos o limpiarlos, 
 * solo que hay que validar los campos que son obligatorios
 */
function onClickFormaPago(){
	var formaPago = jQuery('input:radio[name=recepcionDoctoSin.formaPago]:checked').val();
	if(formaPago == 'TB'){
		jQuery('#clabeBanco_t').attr('readonly',false);
		jQuery("#beneficiario_t").attr('readonly',false);
		jQuery('#correo_t').attr('readonly',false);
		jQuery('#telefonoLada_t').attr('readonly',false);
		jQuery('#telefonoNumero_t').attr('readonly',false);
		jQuery('#rfc_t').attr('readonly',false);
	}else if (formaPago == 'CH'){
		jQuery('#clabeBanco_t').attr('readonly',true);
		jQuery('#beneficiario_t').attr('readonly',true);
		jQuery('#correo_t').attr('readonly',true);
		jQuery('#telefonoLada_t').attr('readonly',true);
		jQuery('#telefonoNumero_t').attr('readonly',true);
		jQuery('#rfc_t').attr('readonly',true);
	}
}

function limitarTextArea(e) {
	alert(e.toSource());
  if(e.keyCode >= 48 && e.keyCode <= 90 ||
    e.keyCode >= 96 && e.keyCode <= 111 || 
    e.keyCode >= 186 && e.keyCode <= 222 ||
    e.keyCode == 13 || e.keyCode == 32){
	  var numeroLineas = countLines(jQuery(this).val(), jQuery(this).attr('cols'));
	  var numeroFilasMax = jQuery(this).attr('rows'); 
	  var esMayor = numeroLineas > numeroFilasMax;
	  if( esMayor ) {
		  return false;
	  }
  }
}

function countLines(text, maxCols){
  var lines = text.split("\n");
  var numberOfLines = lines.length;
  var MARGEN_CARACTERES = 5;
  lines.forEach(function(line){
     numberOfLines +=  Math.floor(line.length / (maxCols - MARGEN_CARACTERES));
  });
  return numberOfLines;
}

function configurarTextArea() {
  jQuery('textarea.limited').keydown(limitarTextArea);
}

function imprimirCartaPTS(){
	var idIndemnizacion = jQuery("#idIndemnizacion").val();
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/imprimirCartaPTS.action?idIndemnizacion=' + idIndemnizacion;
	window.open(url, "CartaPTSNoDocumentada");
}

function imprimirCartaPTSAccion(idIndemnizacion){
	var url = '/MidasWeb/siniestros/indemnizacion/cartas/imprimirCartaPTS.action?idIndemnizacion=' + idIndemnizacion;
	window.open(url, "CartaPTSNoDocumentada");
}

function truncarTexto(componente, maximoCaracteres){
	var textoComponente = jQuery(componente).val(); 
	var textoNuevo  = textoComponente.substring(0, Math.min(maximoCaracteres,textoComponente.length));
	jQuery(componente).val(textoNuevo);	
}

function validaRequeridos(){
    var requeridos = jQuery(".requerido");
    var esValido = true;
    requeridos.each(
          function(){
                var these = jQuery(this);
                if( isEmpty(these.val()) ){
                      these.addClass("errorField");
                      esValido = false;
                } else {
                      these.removeClass("errorField");
                }
          }
    );
    return esValido;
}

function validaPension(){
	var pension = jQuery('input:radio[name=recepcionDoctoSin.pension]:checked').val();
	if(pension){
		return true;
	}
	return false;
}

function imprimirCartaFiniquito(){
	formParams = jQuery(document.recepcionDoctosForm).serialize();
	var url = imprimirCartaFiniquitoPath + '?' +  formParams ;
    window.open(url, "CartaFiniquito");
}