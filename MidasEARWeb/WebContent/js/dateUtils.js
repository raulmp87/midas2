//Javascript de utilerias para el manejo de Fechas
var DATE_SEPARATOR = "/";

//Funcion que retorna una variable tipo date a partir del String
function getDateFromString(stringDate){
	var dateArray = stringDate.split(DATE_SEPARATOR);
	return new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);	
}

//Funcion que retorna una variable tipo date a partir del String
function getDateArray(stringDate){
	var dateArray = stringDate.split(DATE_SEPARATOR);
	return dateArray
}

//Funcion que valida el numero de dias en el mes
function daysInMonth(m, y) { // m is 0 indexed: 0-11
    switch (m) {
        case 1 :
            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
        case 8 : case 3 : case 5 : case 10 :
            return 30;
        default :
            return 31
    }
}

function isValid(d, m, y) {
    return m >= 0 && m < 12 && d > 0 && d <= daysInMonth(m, y);
}



//Funcion que retorno una variable String a partir de una variable Date
function getStringFromDate(date){
	var year = date.getFullYear().toString();
	var month = (date.getMonth()).toString();
	var day = date.getDate().toString();
	var monthNumber = parseInt(month) + 1;
	month = monthNumber.toString();
	return (day.length == 1 ? "0" + day : day) + DATE_SEPARATOR + (month.length == 1 ? "0" + month : month) + DATE_SEPARATOR + (year);
}
//Funcion que retorna el numero de dias de diferencia entre dos dias
function getDaysBetweenDates(date1, date2){
	var difMilis;
	if (date1 > date2){
		difMilis = date1 - date2;
	}else{
		difMilis = date2 - date1;
	}
	return ((((difMilis / 1000) /60) /60) /24);
}
//Funcion que retorna la fecha actual truncada por horas
function getTrunkCurrentDate(){
	var date = new Date();
	date.setHours(0,0,0,0);
	return date;
}
//Funcion que agregar a una fecha un numero de dias definido
function addDaysToDate(selectedDate, daysToAdd){
	//Truncamos la fecha
	selectedDate.setHours(0,0,0,0);
	var milis = selectedDate.getTime();
	var milisDays = ((((daysToAdd * 24) * 60) * 60 ) * 1000);
	milis += milisDays;
	return new Date(milis);
}

//Funcion que agregar a una fecha un numero de dias definido
function substractDaysToDate(selectedDate, daysToAdd){
	//Truncamos la fecha
	selectedDate.setHours(0,0,0,0);
	var milis = selectedDate.getTime();
	var milisDays = ((((daysToAdd * 24) * 60) * 60 ) * 1000);
	milis = milis - milisDays;
	return new Date(milis);
}

//Funcion que valida si una fecha es anterior en relacion a otra
function isDateBefore(date1, date2){
	var date1Milis = date1.getTime();
	var date2Milis = date2.getTime();
	if (date1Milis <= date2Milis){
		return true;
	}else{
		return false;
	}
}

function isDateAfter(date1, date2){
	var date1Milis = date1.getTime();
	var date2Milis = date2.getTime();
	if (date1Milis >= date2Milis){
		return true;
	}else{
		return false;
	}
}

function isDateAfterAllowSameDay(date1, date2){
	var date1Milis = date1.getTime();
	var date2Milis = date2.getTime();
	if (date1Milis > date2Milis){
		return true;
	}else{
		return false;
	}
}

//Funcion que valida si una cadena presenta formato de fecha
function isDate(str){
	var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
	if ((str.match(RegExPattern)) && (str!='')) {           
		return true;      
	}else{
	    return false;
	}
}
//Funcion que valida si una cadena presenta un valor numerico
function isNumber(strNum){
	var regex = /^\d*\.?\d{0,2}$/;
	var res = regex.test(strNum);
		
	return res;
	//var no = parseFloat(strNum);
	//if(isNaN(no)){
	//	return false;
	//}else{
	//	return true;
	//}
}
