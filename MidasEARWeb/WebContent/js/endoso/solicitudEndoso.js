var ventanaPoliza;
var polizaEncontrada=false;
var permiteCancelacion=false;
var errorAlValidarPermiso=0;
var aFinMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31); 
var ventanaPolizas=null;
var mensajeError=null;
var endosoCancelacion=1;
var endosoRehabilitacion=2;
var endosoModificacion=3;
var endosoDeclaracion=4;
var estatusVigente=1;//Obtiene solo las polizas vigentes
var estatusCancelada=0;//Obtiene solo las polizas canceladas
var estatusDefault=2;//Devuelve todo el listado de polizas
var activado=1;
var desactivado=0;


function activaDatosMotivo(){
	if($('presentaComboMotivo') != null){
		if(document.getElementById('presentaComboMotivo').value==activado) {
			document.getElementById('datosMotivo').style.display = '';
			
		} else {
			document.getElementById('datosMotivo').style.display = 'none';
			
		}
	}
}

function validaCambioTipoEndoso(valorSel){
	if(valorSel=="" || valorSel.length==0){
		$('estatusPoliza').value==estatusDefault;
		$('presentaComboMotivo').value=desactivado;
	}else if(valorSel==endosoCancelacion){
		if($('idPoliza').value.length>0){
			if($('cveEstatusPoliza').value==0){
				mostrarVentanaMensaje("20", "Esta p&oacute;liza que ya esta cancelada", null);
			}else{
				validaParaCancelar();
				if(!permiteCancelacion){
					mostrarVentanaMensaje("20", "Esta p&oacute;liza no puede ser cancelada,debido a que :"+mensajeError, null);
					$('idPoliza').focus();
					/*$('numPoliza').value="";
					$('idPoliza').value = "";
					$('idProducto').value="";
					$('fechaInicioCotizacion').value="";
					$('fechaFinCotizacion').value="";
					$('estatusPoliza').value=0;
					$('cveEstatusPoliza').value="";*/
					$('presentaComboMotivo').value=desactivado;
				}else{
					$('presentaComboMotivo').value=activado;
				}
			}
		}
		$('estatusPoliza').value=estatusVigente;
		
	}else if(valorSel==endosoRehabilitacion){
		if($('idPoliza').value.length>0){
			if($('cveEstatusPoliza').value!=0){
				mostrarVentanaMensaje("20", "La p&oacute;liza debe de estar cancelada.", null);
				$('numPoliza').value="";
				$('idPoliza').value = "";
				$('idProducto').value="";
				$('fechaInicioCotizacion').value="";
				$('fechaFinCotizacion').value="";
				$('estatusPoliza').value=0;
				$('cveEstatusPoliza').value="";
				}
		}
		
		if($('estatusPoliza').value==estatusVigente){
			$('numPoliza').value="";
			$('idPoliza').value = "";
			$('idProducto').value="";
			$('fechaInicioCotizacion').value="";
			$('fechaFinCotizacion').value="";
			$('estatusPoliza').value=0;
			$('cveEstatusPoliza').value="";
		}
		$('estatusPoliza').value==estatusCancelada;
		$('presentaComboMotivo').value=desactivado;
	}else {
		$('estatusPoliza').value=estatusVigente;
		$('presentaComboMotivo').value=desactivado;
	}
	activaDatosMotivo();
	
}
function seleccionaPolizaModal(idPoliza,numPoliza,producto,fecha1,fecha2,estatus,codAgente,idToProducto){
	//alert(fecha1);
	var listaValores = idPoliza+"|"+numPoliza+"|"+producto+"|"+fecha1+"|"+fecha2+"|"+estatus+"|"+codAgente;
	presentaDatosBusquedaPoliza(listaValores);
	parent.ventanaPolizas.close();
	
	if($('idToProducto') != null) {
		$('idToProducto').value = idToProducto;
	}
}

function seleccionaPolizaFromGrid(listaValores, idToProducto){
	presentaDatosBusquedaPoliza(listaValores);
	parent.ventanaPolizas.close();
	
	if($('idToProducto') != null) {
		$('idToProducto').value = idToProducto;
	}
}

function validaParaCancelar(){
	permiteCancelacion=false;
	mensajeError="";
	errorAlValidarPermiso=0;
	new Ajax.Request("/MidasWeb/endoso/solicitud/validaparacancelar.do?idPoliza="+$('idPoliza').value, {
		method : "post",
		asynchronous : false,
		onSuccess : function(resp) { 
			//alert("The response from the server is: " + resp.responseText);
			//muestraDatosPolizaAjax(resp.responseXML);
			}, 
		onFailure : function(resp) { 
			permiteCancelacion=false;
			return;
			},
		onComplete:function(resp){
			 puedeCancelar(resp.responseXML);
			}
	});
}

function puedeCancelar(docXml){
	var items = docXml.getElementsByTagName("item");
	
	if(items!=null  && items.length>0){
		var item = items[0];
		if(item.getElementsByTagName("permiteCancelacion")[0].firstChild.nodeValue==1){
			permiteCancelacion=true;
		}else{
			mensajeError=item.getElementsByTagName("descripcion")[0].firstChild.nodeValue;
			permiteCancelacion=false;
		}
	}else{
		errorAlValidarPermiso=1;
		
	}
	
}

function validarPolizaAjax(){
	var polizaPattern = /^\d{4}-\d{6}-\d{2}$/;
	if($('numPoliza').value.length < 14){
		mostrarVentanaMensaje("20","Favor de capturar correctamente el n&uacute;mero de la P&oacute;liza", null);
		polizaEncontrada = false;
		return;
	}
	if(!($('numPoliza').value.match(polizaPattern))){
		mostrarVentanaMensaje("20","Favor de seguir formato de P&oacute;liza (9999-999999-99)", null);
		polizaEncontrada = false;
		return;
	}
	
	var esRenovacion = 0;
	if(document.getElementsByName('esRenovacion')[1].checked)
		esRenovacion = 1;
	new Ajax.Request("/MidasWeb/endoso/solicitud/buscarPoliza.do?numPol="+$('numPoliza').value+"&cveEstatus="+$('estatusPoliza').value+"&esSolicitudEndoso=false"+"&esRenovacion="+esRenovacion, {
		method : "post",
		asynchronous : false,
		onSuccess : function(resp) { 
			// alert("The response from the server is: " + resp.responseText);
			// muestraDatosPolizaAjax(resp.responseXML);
		}, 
		onFailure : function(resp) { 
			mostrarVentanaMensaje("10","Ocurrio  un error al buscar la poliza.", null);
			polizaEncontrada = false;
			return;
		},
		onComplete:function(resp){
			var items = resp.responseXML.getElementsByTagName("item");
			if(items != null && items.length > 0) {
				var item = items[0]
				if(item.getElementsByTagName("mensaje")[0] != null) {
					var mensaje = item.getElementsByTagName("mensaje")[0].firstChild.nodeValue;
					mostrarVentanaMensaje("10", mensaje, null);
					polizaEncontrada = false;
					return false;
				}
			}
			muestraDatosPolizaAjax(resp.responseXML);
		}
	});
}

function buscaPolizaAjax(){
	var polizaPattern= /^\d{4}-\d{6}-\d{2}$/;
	if($('numPoliza').value.length<14){
		mostrarVentanaMensaje("20","Favor de capturar correctamente el numero de la Poliza", null);
		polizaEncontrada=false;
		return;
	}
	
	if(!($('numPoliza').value.match(polizaPattern))){
		mostrarVentanaMensaje("20","Favor de seguir formato de poliza (9999-999999-99)", null);
		polizaEncontrada=false;
		return;
	}

	new Ajax.Request("/MidasWeb/endoso/solicitud/buscarPoliza.do?numPol="+$('numPoliza').value+"&cveEstatus="+$('estatusPoliza').value, {
		method : "post",
		asynchronous : false,
		onSuccess : function(resp) { 
			//alert("The response from the server is: " + resp.responseText);
			//muestraDatosPolizaAjax(resp.responseXML);
			}, 
		onFailure : function(resp) { 
			mostrarVentanaMensaje("10","Ocurrio  un error al buscar la poliza.", null);
			polizaEncontrada=false;
			return;
			},
		onComplete:function(resp){
			var items = resp.responseXML.getElementsByTagName("item");
			if(items != null && items.length > 0) {
				var item = items[0]
				if(item.getElementsByTagName("mensaje")[0] != null) {
					var mensaje = item.getElementsByTagName("mensaje")[0].firstChild.nodeValue;
					mostrarVentanaMensaje("10", mensaje, null);
					polizaEncontrada = false;
					return false;
				}
			}
			 muestraDatosPolizaAjax(resp.responseXML);
			}
	});
	
	
}

function muestraDatosPolizaAjax(docXml){
	
	var items = docXml.getElementsByTagName("item");
	
	if(items!=null  && items.length>0){
		var item = items[0];
		$('idPoliza').value = item.getElementsByTagName("idPol")[0].firstChild.nodeValue;
		$('descProducto').value =item.getElementsByTagName("descProd")[0].firstChild.nodeValue;
		if($('idToProducto') != null) {
			$('idToProducto').value =item.getElementsByTagName("idToProducto")[0].firstChild.nodeValue;
		}
		$('fechaInicioCotizacion').value= item.getElementsByTagName("fecha1")[0].firstChild.nodeValue;
		
			if($('fechaInicioCotizacion').value==1){
				$('fechaInicioCotizacion').value="";
			}
		$('fechaFinCotizacion').value= item.getElementsByTagName("fecha2")[0].firstChild.nodeValue;
			
			if($('fechaFinCotizacion').value==1){
				$('fechaFinCotizacion').value="";
			}
		$('cveEstatusPoliza').value= item.getElementsByTagName("estatus")[0].firstChild.nodeValue;
		$('codigoAgente').value=item.getElementsByTagName("codAgente")[0].firstChild.nodeValue;
		$('idAgente').value=item.getElementsByTagName("codAgente")[0].firstChild.nodeValue;
		polizaEncontrada=true;
		
		if($('tipoEndoso') != null && $('tipoEndoso').value==endosoCancelacion){
			if($('idPoliza').value.length>0){
				validaParaCancelar();
				if(errorAlValidarPermiso==1){
					mostrarVentanaMensaje("30", "Ocurrio un error al obtener permiso para cancelar la Poliza.", null);
				}else{
					if(!permiteCancelacion){
						mostrarVentanaMensaje("20", "Esta p&oacute;liza no puede ser cancelada,debido a que: "+mensajeError, null);
						/*$('numPoliza').value="";
						$('idPoliza').value = "";
						$('idProducto').value="";
						$('fechaInicioCotizacion').value="";
						$('fechaFinCotizacion').value="";
						$('estatusPoliza').value=0;
						$('cveEstatusPoliza').value="";*/
						$('presentaComboMotivo').value=desactivado;
					}else{
						if($('presentaComboMotivo') != null) {
							$('presentaComboMotivo').value=activado;
						}
					}
				}
			}
			
		}else{
			if($('presentaComboMotivo') != null) {
				$('presentaComboMotivo').value=desactivado;
			}
		}
		
	}else{
		polizaEncontrada=false;
		if($('estatusPoliza').value==estatusDefault){
			mostrarVentanaMensaje("20","La poliza no se encuentra en el sistema,favor verifique", null);
		}else if($('estatusPoliza').value==estatusVigente){
			if($('tipoEndoso').value==1){
				mostrarVentanaMensaje("20","La poliza vigente no se encuentra para hacer una cancelacion "+'\n'+",favor verifique", null);
			}else if($('tipoEndoso').value==3){
				mostrarVentanaMensaje("20","La poliza vigente no se encuentra para hacer una modificacion"+'\n'+",favor verifique", null);
			}else if($('tipoEndoso').value==4){
				mostrarVentanaMensaje("20","La poliza vigente no se encuentra para hacer el otro"+'\n'+" ,favor verifique", null);
			}
			
		}else{
			mostrarVentanaMensaje("20","La poliza cancelada no se encuentra para hacer la rehabilitacion"+'\n'+",favor verifique", null);
		
		}
		
		$('numPoliza').focus();
		$('tipoEndoso').value=0;
		$('numPoliza').value="";
		$('idPoliza').value = "";
		$('idProducto').value="";
		$('fechaInicioCotizacion').value="";
		$('fechaFinCotizacion').value="";
		$('estatusPoliza').value=2;
		$('cveEstatusPoliza').value="";
		
	}
	activaDatosMotivo();
}




function presentaDatosBusquedaPoliza(param){
	//alert(param);
	
	if (param != null){
		
		var datos = param.split('|');
		$('idPoliza').value = datos[0];
		$('numPoliza').value= datos[1];
		$('descProducto').value=datos[2];
		$('fechaInicioCotizacion').value=datos[3];
	//	alert($('fechaInicioCotizacion').value);
		if($('fechaInicioCotizacion').value==1){
			$('fechaInicioCotizacion').value="";
		}
		$('fechaFinCotizacion').value=datos[4];
		if($('fechaFinCotizacion').value==1){
			$('fechaFinCotizacion').value="";
		}
		$('cveEstatusPoliza').value=datos[5];
		$('codigoAgente').value=datos[6];
		$('idAgente').value=datos[6];
		polizaEncontrada=true;
		
		if($('tipoEndoso') != null && $('tipoEndoso').value==endosoCancelacion){
			if($('idPoliza').value.length>0){
				validaParaCancelar();
				if(errorAlValidarPermiso==1){
					mostrarVentanaMensaje("30", "Ocurrio un error al obtener permiso para cancelar la Poliza.", null);
				}else{
					if(!permiteCancelacion){
						mostrarVentanaMensaje("20", "Esta p&oacute;liza no puede ser cancelada,debido a que: "+mensajeError, null);
						/*$('numPoliza').value="";
						$('idPoliza').value = "";
						$('idProducto').value="";
						$('fechaInicioCotizacion').value="";
						$('fechaFinCotizacion').value="";
						$('estatusPoliza').value=0;
						$('cveEstatusPoliza').value="";*/
						$('presentaComboMotivo').value=desactivado;
					}else{
						$('presentaComboMotivo').value=activado;
					}
				}
			}
			
		}else{
			if($('presentaComboMotivo') != null) {
				$('presentaComboMotivo').value=desactivado;
			}
		}
	}else{
		polizaEncontrada=false;
	}
	if($('presentaComboMotivo') != null) {
		activaDatosMotivo();
	}
}

function esValidaFechaVigencia(){
	
	var vDia;
	var vMes;
	var vAnio;
	var esValido=true;
	
	
	if($('tipoEndoso').value=="" || $('tipoEndoso').value.length==0){
		mostrarVentanaMensaje("20","Favor de especificar el tipo de endoso", null);
		return false;
	}
	
	if($('idPoliza').value== null || $('idPoliza').value.length==0){
		mostrarVentanaMensaje("20","Favor de especificar una poliza valida", null);
		return false;
	}
	
	if($('fechaInicioCotizacion').value== null || $('fechaInicioCotizacion').value.length==0){
		mostrarVentanaMensaje("20","La poliza especificada no presenta su ultimo endoso", null);
		return false;
	}
	
	
	if($('fecha').value== null || $('fecha').value.length==0){
		mostrarVentanaMensaje("20","La fecha de vigencia del endoso es requerida", null);
		return false;
	}
	
	
	
	
	/*****************/
	var fechaMayorActual=  addToDate($('fechaStrSolicitud').value, 15);
	var fechaMenorActual=  addToDate($('fechaStrSolicitud').value, -15);
	var fechaIncioVigencia= $('fecha').value;
    var fechaInicioCotizacion= $('fechaInicioCotizacion').value;
	var fechaFinCotizacion= $('fechaFinCotizacion').value;
	var fechaFinalEndoso=addToDate($('fechaInicioCotizacion').value, 60);
	
		if(	!esFechaMayorOIgualQue(fechaIncioVigencia, fechaInicioCotizacion)||
				!esFechaMayorOIgualQue(fechaFinCotizacion,fechaIncioVigencia)){
			mostrarVentanaMensaje("20","La fecha de inicio de vigencia del endoso  debe "+'\n'+" " +
					"estar dentro del rango de vigencia de la poliza", null);
			
			return false;
		}
/*	PENDIENTE VALIDAR PARA TIPO DE POLIZA (Transporte Especifico y Responsabilidad Civil Contratista Corto Plazo)
	if( !(compare_dates(fechaIncioVigencia, fechaMenorActual)) ||compare_dates(fechaIncioVigencia, fechaMayorActual)){
		
		if(!(confirm('La fecha de inicio de v&iacute;gencia del endoso requerir&aacute; '+ '\n'+
				' de una autorizaci&oacute;n. Desea continuar con el registro de la solicitud?'))){
			return false;
		}else {
			return true;
		}
	
	}
	
*/
		
	//Cuando se va a  habilitar una poliza cancelada se valida que esten dentro de 60 dias naturales
		if($('tipoEndoso').value==endosoRehabilitacion){
			if($('cveEstatusPoliza').value!=estatusCancelada){
				mostrarVentanaMensaje("20", "Para este tipo de endoso la poliza debe estar cancelada,"+'\n'+",favor verifique", null);
				return false;
			}else{
				if(compare_dates(fechaIncioVigencia, fechaFinalEndoso)){
					mostrarVentanaMensaje("20", "No se puede rehabilitar ," +
							"se han excedido los 60 dias naturales, del ultimo endoso", null);
				    
				   return false;
				}
			}
			    
		}else{
			
			if($('cveEstatusPoliza').value!=estatusVigente){
				mostrarVentanaMensaje
				("20", "Para este tipo de endoso la poliza debe estar vigente,"+'\n'+",favor verifique", null);
			   	return false;
			}
			if($('tipoEndoso').value==endosoCancelacion){
				if($('idPoliza').value.length>0){
					validaParaCancelar();
					if(errorAlValidarPermiso==1){
						mostrarVentanaMensaje("30", "Ocurrio un error al obtener permiso para cancelar la Poliza.", null);
						return false;
						
					}else{
						if(!permiteCancelacion){
							mmostrarVentanaMensaje("20", "Esta p&oacute;liza no puede ser cancelada,debido a que: "+mensajeError, null);
							return false;
							$('presentaComboMotivo').value=desactivado;
							/*$('numPoliza').value="";
							$('idPoliza').value = "";
							$('idProducto').value="";
							$('fechaInicioCotizacion').value="";
							$('fechaFinCotizacion').value="";
							$('estatusPoliza').value=0;
							$('cveEstatusPoliza').value="";*/
						}else{
							activaDatosMotivo();
							$('presentaComboMotivo').value=activado;
							if($('claveMotivoEndoso').value.length==0){
								mostrarVentanaMensaje("20", "Seleccione un Motivo para el Endoso de Cancelaci&oacute;n", null);
								return false;
							}
							
						}
					}
				}
			}else{
				$('presentaComboMotivo').value=desactivado;
			}
			
		}
	
	activaDatosMotivo();
	return esValido;
	
	
}


/*******************UTILERIAS DE FUNCIONALIDADES CON FECHAS ***************************/


function finMes(nMes, nAno){ 
 return aFinMes[nMes - 1] + (((nMes == 2) && (nAno % 4) == 0)? 1: 0); 
} 

 function padNmb(nStr, nLen, sChr){ 
  var sRes = String(nStr); 
  for (var i = 0; i < nLen - String(nStr).length; i++) 
   sRes = sChr + sRes; 
  return sRes; 
 } 

 function makeDateFormat(nDay, nMonth, nYear){ 
  var sRes; 
  sRes = padNmb(nDay, 2, "0") + "/" + padNmb(nMonth, 2, "0") + "/" + padNmb(nYear, 4, "0"); 
  return sRes; 
 } 
  
function incDate(sFec0){ 
 var nDia = parseInt(sFec0.substr(0, 2), 10); 
 var nMes = parseInt(sFec0.substr(3, 2), 10); 
 var nAno = parseInt(sFec0.substr(6, 4), 10); 
 nDia += 1; 
 if (nDia > finMes(nMes, nAno)){ 
  nDia = 1; 
  nMes += 1; 
  if (nMes == 13){ 
   nMes = 1; 
   nAno += 1; 
  } 
 } 
 return makeDateFormat(nDia, nMes, nAno); 
} 

function decDate(sFec0){ 
 var nDia = Number(sFec0.substr(0, 2)); 
 var nMes = Number(sFec0.substr(3, 2)); 
 var nAno = Number(sFec0.substr(6, 4)); 
 nDia -= 1; 
 if (nDia == 0){ 
  nMes -= 1; 
  if (nMes == 0){ 
   nMes = 12; 
   nAno -= 1; 
  } 
  nDia = finMes(nMes, nAno); 
 } 
 return makeDateFormat(nDia, nMes, nAno); 
} 

function addToDate(sFec0, sInc){ 
 var nInc = Math.abs(parseInt(sInc)); 
 var sRes = sFec0; 
 if (parseInt(sInc) >= 0) 
  for (var i = 0; i < nInc; i++) sRes = incDate(sRes); 
 else 
  for (var i = 0; i < nInc; i++) sRes = decDate(sRes); 
 return sRes; 
} 

function compare_dates(fecha, fecha2)   
{   
  var xDay=fecha.substring(0, 2); 
  var xMonth=fecha.substring(3, 5);   
  var xYear=fecha.substring(6,10);   
  
  var yDay=fecha2.substring(0, 2);   
  var yMonth=fecha2.substring(3, 5);   
  var yYear=fecha2.substring(6,10);   
  
  if (xYear> yYear)   
  {   
      return(true)   
  }   
  else  
  {   
    if (xYear == yYear)   
    {    
      if (xMonth> yMonth)   
      {   
          return(true)   
      }   
      else  
      {    
        if (xMonth == yMonth)   
        {   
          if (xDay> yDay)   
            return(true);   
          else  
            return(false);   
        }   
        else  
          return(false);   
      }   
    }   
    else  
      return(false);   
  }   
}  
    

function esFechaMayorOIgualQue(fec0, fec1){
    var bRes = false;
    var sDia0 = fec0.substr(0, 2);
    var sMes0 = fec0.substr(3, 2);
    var sAno0 = fec0.substr(6, 4);
    var sDia1 = fec1.substr(0, 2);
    var sMes1 = fec1.substr(3, 2);
    var sAno1 = fec1.substr(6, 4);
    if (sAno0 > sAno1) bRes = true;
    else {
     if (sAno0 == sAno1){
      if (sMes0 > sMes1) bRes = true;
      else {
       if (sMes0 == sMes1)
        if (sDia0 >= sDia1) bRes = true;
      }
     }
    }
    return bRes;
   } 



function equals_dates(fecha, fecha2)
{
  var xMonth=fecha.substring(3, 5);
  var xDay=fecha.substring(0, 2);
  var xYear=fecha.substring(6,10);
  var yMonth=fecha2.substring(3, 5);
  var yDay=fecha2.substring(0, 2);
  var yYear=fecha2.substring(6,10);
  if (xYear!= yYear)
  {
      return(false)
  }
  else
  {
    if (xYear == yYear)
    { 
      if (xMonth != yMonth)
      {
          return(false)
      }
      else
      { 
        if (xMonth == yMonth)
        {
          if (xDay== yDay)
            return(true);
          else
            return(false);
        }
        else
          return(false);
      }
    }
    else
      return(false);
  }
}




function validarTipoMovimiento(){

	new Ajax.Request("/MidasWeb/cotizacion/endoso/validaEndosoDeCancelacion.do?id="+$('idToCotizacion').value, {
		method : "post",
		asynchronous : false,
		onSuccess : function(transport) {
			validaMovimiento(transport.responseXML);
		}
	});	
}
function validaMovimiento(doc){
	var items = doc.getElementsByTagName("item");
	if(items!=null  && items.length>0){
		var item = items[0];
		var value = item.getElementsByTagName("id")[0].firstChild.nodeValue;
		if(value == 'true'){
			$('datosFinVigencia').style.display = "block";
		}else{
			$('datosFinVigencia').style.display = "none";
		}	
	}	
}
function distribuyeReaseguroEndoso(idCot){
	
	sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/validar/mostrarValidarReaseguroFacultativo.do?id='+idCot,null,null);
}


/*************  FUNCIONES PARA SOLICITUDES DE ENDOSO   ****************/

function mostrarAdjuntarArchivoSolicitudEndosoWindow() {
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var acercaDeWindow = dhxWins.createWindow("adjuntarArchivoSolicitudEndoso", 34, 100, 440, 265);
	acercaDeWindow.setText("Adjuntar archivos");
	acercaDeWindow.button("minmax1").hide();
	acercaDeWindow.button("park").hide();
	acercaDeWindow.setModal(true);
	acercaDeWindow.center();
	acercaDeWindow.denyResize();
	acercaDeWindow.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");

    vault.onUploadComplete = function(files) {
        var s="";
        for (var i=0; i<files.length; i++) {
            var file = files[i];
            s += ("id:" + file.id + ",name:" + file.name + ",uploaded:" + file.uploaded + ",error:" + file.error)+"\n";
        }
        sendRequest(null, '/MidasWeb/endoso/solicitud/listarDocumentos.do', 'resultadosDocumentosEndoso', null);
        parent.dhxWins.window("adjuntarArchivoSolicitudEndoso").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "0");
}


function mostrarAsignarSolicitudEndoso(idToSolicitud){
	instanciarContenedorVentanas();
	if (ventanaAsignaSolicitudEndoso==null || parent.dhxWins.window("AsignarSolicitudEndoso").isHidden()){
		ventanaAsignaSolicitudEndoso = dhxWins.createWindow("AsignarSolicitudEndoso", 400, 320, 610, 190);
		ventanaAsignaSolicitudEndoso.setText("Asignar Solicitud Endoso.");
		ventanaAsignaSolicitudEndoso.setModal(true);
		ventanaAsignaSolicitudEndoso.attachURL("/MidasWeb/endoso/solicitud/mostrarAsignar.do?idToSolicitud=" + idToSolicitud);
		ventanaAsignaSolicitudEndoso.attachEvent("onClose", cerrarVentanaSE);
	}
}

function asignarSolicitudEndoso(fobj){
	parent.showIndicatorSimple();
	new Ajax.Request('/MidasWeb/endoso/solicitud/asignar.do', {
		method : "post",asynchronous : false,parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(true) : null,
		onSuccess : function(transport) {
			parent.hideIndicator();
			procesarRespuestaXml(transport.responseXML, "cerrarVentanaSE();");
			}
			
	});
}


function cerrarVentanaSE(){
	parent.dhxWins.window("AsignarSolicitudEndoso").setModal(false);
	parent.dhxWins.window("AsignarSolicitudEndoso").hide();
	ventanaAsignaSolicitudEndoso=null;
	delay(500);
	parent.sendRequest(null,"/MidasWeb/solicitud/listar.do", "contenido",null);
}

function ocultarVentanaSE(){
	parent.dhxWins.window("AsignarSolicitudEndoso").setModal(false);
	parent.dhxWins.window("AsignarSolicitudEndoso").hide();
}

function mostrarResumenEndoso(idToSolicitud){
	if(dhxWins != null) {
		dhxWins.unload();
	}	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	
	var ventanaResumenEndoso = dhxWins.createWindow("Resumen", 400, 320, 610, 380);
	ventanaResumenEndoso.setText("Resumen General del Endoso");
	ventanaResumenEndoso.center();
	ventanaResumenEndoso.setModal(true);
	ventanaResumenEndoso.attachURL("/MidasWeb/endoso/solicitud/mostrarResumen.do?id="+ idToSolicitud);
	//ventanaResumen.attachEvent("onClose", cerrarVentanaOT);
}

function mostrarVentanaPolizas(param, esSolicitudPoliza){
	if (parent.dhxWins==null){
		parent.dhxWins = new parent.dhtmlXWindows();
		parent.dhxWins.enableAutoViewport(true);
		parent.dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	}
	parent.ventanaPolizas = parent.dhxWins.createWindow("mostrarPersonaInterfaz", 400, 250, 870, 328);
	parent.ventanaPolizas.setText("Busqueda de Polizas.");
	parent.ventanaPolizas.center();
	parent.ventanaPolizas.setModal(true);
	var url = "/MidasWeb/endoso/solicitud/mostrarPolizasWindow.do";
	if(esSolicitudPoliza) {
		url += "?esSolicitudPoliza=true";
	}
	parent.ventanaPolizas.attachURL(url);
	parent.ventanaPolizas.button("minmax1").hide();	
}

var polizasGrid;
function cargarPolizasGrid(claveEstatus, esSolicitudPoliza) {
	polizasGrid = new dhtmlXGridObject("polizasGrid");
	polizasGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
	polizasGrid.setHeader("N\u00famero de P\u00f3liza,Nombre del Asegurado,Fecha de Emisi\u00f3n,Estatus,listaValores,idToProducto,permiteCopia");
	polizasGrid.attachHeader("#text_filter,#text_filter,#text_filter,&nbsp;,#cspan,#cspan,#cspan");
	polizasGrid.setInitWidths("150,400,180,*,0,0,0");
	polizasGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	polizasGrid.setColAlign("center,left,center,left,left,left,left");
	polizasGrid.enableResizing("false,false,false,false,false,false,false");
	polizasGrid.setColumnHidden(4,true);
	polizasGrid.setColumnHidden(5,true);
	polizasGrid.setColumnHidden(6,true);
	polizasGrid.attachEvent("onRowDblClicked", function(rId,cInd){
		var listaValores = polizasGrid.cellById(rId, 4);
		var idToProducto = polizasGrid.cellById(rId, 5);
		var permiteCopia = polizasGrid.cellById(rId, 6);
		if(permiteCopia.getValue() == "true") {
			parent.seleccionaPolizaFromGrid(listaValores.getValue(), idToProducto.getValue());
		} else {
			parent.mostrarVentanaMensaje("10", "No se puede crear la solicitud a partir de esta p&oacute;liza, ya que los n&uacute;meros de sus incisos son discontinuos.", null);
		}
	});

	polizasGrid.enablePaging(true,10,5,"pagingArea",true,"infoArea");
	polizasGrid.setPagingSkin("bricks");
	//polizasGrid.enableSmartRendering(true);
	polizasGrid.setEditable(false);
	polizasGrid.enableLightMouseNavigation(true);
	polizasGrid.setSkin("light");
	polizasGrid.init();
	polizasGrid.loadXML("/MidasWeb/endoso/solicitud/listarPolizasJson.do?claveEstatus=" + claveEstatus + "&esSolicitudPoliza=" + esSolicitudPoliza);
}

function modificaSolicitudEndoso(){
	
}


function cambiarEstatusCotEndosoLiberada(idToCotizacion){
	parent.showIndicatorSimple();
	new Ajax.Request('/MidasWeb/cotizacion/cambiarEstatusEnProceso.do?idToCotizacion='+idToCotizacion, {
		method : "post",
		encoding : "UTF-8",
		onSuccess : function(transport) {
			parent.hideIndicator();
			document.cotizacionEndosoForm.idToCotizacion.value = idToCotizacion;
			procesarRespuestaXml(transport.responseXML, "listarCotizacionesEndoso();");
			}
			
	});

}

function listarCotizacionesEndoso(){
	parent.sendRequest(document.cotizacionEndosoForm,"/MidasWeb/cotizacion/endoso/listar.do","contenido",null);
}

function validaAgregarSolicitudEndoso(){
	var esValida=esValidaFechaVigencia();
	var tipoEndoso=$('tipoEndoso').value;
	if(esValida){
		if(tipoEndoso == 3 || tipoEndoso == 5){
			sendRequest(document.solicitudEndosoForm,'/MidasWeb/endoso/solicitud/validarDatosSolicitud.do', 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); existenErrores("mostrarAccionesSolicitudEndoso()")');
		}else{
			sendRequest(document.solicitudEndosoForm,'/MidasWeb/endoso/solicitud/validarDatosSolicitud.do', 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); existenErrores("agregarSolicitudEndoso()")');
		}
	}
}
function agregarSolicitudEndoso(){
	sendRequest(null,'/MidasWeb/endoso/solicitud/agregar.do', 'contenido','mensajeFinalSolicitudEndoso()');
}

function mensajeFinalSolicitudEndoso(){
	if($('tipoEndoso').value==endosoCancelacion){
		document.getElementById('datosMotivo').style.display = '';
	} else {
		document.getElementById('datosMotivo').style.display = 'none';
	}
	mostrarVentanaMensaje('30', 'La solicitud se registr&oacute; exitosamente.');
}



function mostrarAccionesSolicitudEndoso(){
	if(dhxWins != null) {
		dhxWins.unload();
	}
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var accionesSolicitud = dhxWins.createWindow("accionesSolicitudEndoso", 34, 100, 600, 150);
	accionesSolicitud.setText("Solicitud");
	accionesSolicitud.button("minmax1").hide();
	accionesSolicitud.button("park").hide();
	accionesSolicitud.button("close").hide();
	accionesSolicitud.setModal(true);
	accionesSolicitud.center();
	accionesSolicitud.denyResize();
	accionesSolicitud.attachURL("/MidasWeb/endoso/solicitud/mostrarAcciones.do");
}

function sendRequestSolicitudEndoso(fobj, url, target, nextFunction, accion) {
	var contenido = document.getElementById(target);
	var fobj = document.solicitudEndosoForm;
	if(accion == "3") {
		sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);
		eval(nextFunction);
	} else {
		new Ajax.Request(url, {
			method : "post",
			encoding : "UTF-8",
			parameters : (fobj !== undefined && fobj !== null) ? $(fobj).serialize(
					true) : null,
			onCreate : function(transport) {
				totalRequests = totalRequests + 1;
				showIndicator();
			}, // End of onCreate
			onComplete : function(transport) {
				totalRequests = totalRequests - 1;
				hideIndicator();
			}, // End of onComplte
			onSuccess : function(transport) {
				contenido.innerHTML = transport.responseText;
				dhxWins.window('accionesSolicitudEndoso').close();
				if(accion == "1") {
					dhx_init_tabbars();
					var idCotizacion = document.getElementById("idToCotizacion").value;
					creaArbolOrdenesDeTrabajo(idCotizacion);
					inicializaObjetosEdicionOT(idCotizacion);
				} else {
					mostrarVentanaMensaje('30', 'La solicitud se registr&oacute; exitosamente.');
				}
				//parent.dhxWins.window('accionesSolicitud').close();
				//eval(nextFunction);
			} // End of onSuccess
		});
	}
}

function liberaCotEndosoModo1(){
	javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/liberar.do','contenido_endoso', 'listarCotizacionesEndoso();validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value);');
}
function liberaCotEndosoModo2(){
	javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');
}





