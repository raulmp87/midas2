<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Afirme Seguros Registro</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/cobranza/pagos/payment.css">

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/form-elements.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/img/favicon.ico" rel="shortcut icon" type="image/png" />

<script type="text/javascript">
	var contextPath = '${pageContext.request.contextPath}';
</script>

<!-- Javascript -->
<script src="${pageContext.request.contextPath}/js/cobranza/cargoPortal/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/cobranza/pagos/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.backstretch.min.js"></script>
<script src="${pageContext.request.contextPath}/js/portalLogin.js"></script>

</head>
<body>
<!-- Top content -->
<div class="top-content">
	<div class="inner-bg">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text">
					<div class="logo">
						<img alt="Logo"
							src="${pageContext.request.contextPath}/img/Logo_seguros2.jpg">
					</div>
					<div class="description"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3 form-box">
					<div class="form-top">
						<div class="form-top-left">
							<h3>Registrate</h3>
							<p>Escribe tus datos</p>
						</div>
						<div class="form-top-right">
							<i class="fa fa-lock"></i>
						</div>
					</div>
					<div class="form-bottom">
						<form role="form" action="/MidasWeb/portal/createUser.action"
							method="post" class="login-form" id="createUser"
							name="createUser">
							<div class="form-group">
								<label class="sr-only" for="form-firstName">NOMBRE(S)</label> <input
									type="text" name="firstName" placeholder="Nombre...."
									class="form-firstname form-control" id="form-firstname">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-fatherLastName">APELLIDO
									PATERNO</label> <input type="text" name="fatherLastName"
									placeholder="Apellido Paterno...."
									class="form-fatherLastName form-control"
									id="form-fatherLastName">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-motherLastName">APELLIDO
									MATERNO</label> <input type="text" name="motherLastName"
									placeholder="Apellido Materno...."
									class="form-motherLastName form-control"
									id="form-motherLastName">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-cellPhone">CELULAR</label> <input
									type="text" name="cellPhone" placeholder="Telefono Celular...."
									class="form-cellPhone form-control" id="form-cellPhone">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-email">CORREO</label> <input
									type="text" name="email" placeholder="Correo...."
									class="form-email form-control" id="form-email">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-retypeEmail">CONFIRMAR
									CORREO</label> <input type="text" name="retypeEmail"
									placeholder="Confirmar Correo...."
									class="form-retypeEmail form-control" id="form-retypeEmail">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-password">CONTRASE&NTILDE;A</label>
								<input type="password" maxlength="8" name="password"
									placeholder="Contrase&ntilde;a...."
									class="form-password form-control" id="form-password">
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-retypePassword">CONFIRMAR
									CONTRASE&NTILDE;A</label> <input type="password" maxlength="8" name="retypePassword"
									placeholder="Confirmar Contrase&ntilde;a...."
									class="form-retypePassword form-control"
									id="form-retypePassword">
							</div>
							<button type="submit" class="btn" id="enviar">Enviar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mensajeCustom">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">
					<i class="fa fa-bell"></i> Mensaje
				</h4>
			</div>
			<div class="modal-bodyCustom">
				<br>
				<div style="margin-bottom: 0" class="alert alert-warning">
				<!--valor mensaje --><p id="textDinamico"></p>
				</div>
			</div><!-- /.modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div><!-- /.modal-footer -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="mensaje">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">
					<i class="fa fa-bell"></i> Mensaje
				</h4>
			</div>
			<div class="modal-body">
				<br>
				<div style="margin-bottom: 0"
					class="alert alert-<s:property value="portalMensaje.type" />">
					<s:property value="portalMensaje.message" /><br/>
				</div>
			</div><!-- /.modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div><!-- /.modal-footer -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<script>
$(document).ready(function(){
	<s:if test="%{ portalMensaje != null }">
	$('#mensaje').modal();
	</s:if>
});
</script>
</body>
</html>