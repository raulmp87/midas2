<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Login Midas</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Login page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/login.css"/>" rel="stylesheet" type="text/css">		
  	</head>    
  	
	<body id="loginBody">
		<div class="login">
			<div id="head_log"></div>
				<div id="cont_log">
					<div id="cont_img">
					<table width="100%">
						<tr>
				      		<td class="logo" colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="bienvenido" colspan="4">
								Inicio de Sesi&oacute;n
							</td>
						</tr>
						<tr>
							<td class="uc" colspan="4">Roles de usuario MIDAS</td>
						</tr>
						<tr height="80px" ><td colspan="4"></td></tr>
						<tr>
							<td style="text-align: center" colspan="4">
								<select id="roles">
								  
								  
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Supervisor_Suscriptor">Supervisor Suscriptor</option> 
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Suscriptor_Cot">Suscriptor de Cotizacion</option> 
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Emisor">Emisor</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Asignador_Ot">Asignador de Orden de Trabajo</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Suscriptor_Ot">Suscriptor de orden de trabajo</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Asignador_Sol">Asignador de Solicitudes</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Agente">Agente</option>
								  
								  
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Cabinero">Cabinero</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Ajustador">Ajustador</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Coordinador_Siniestros">Coordinador de Siniestros Daños</option> 
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Gerente_Siniestros">Gerente de Siniestros Daños</option> 
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Coordinador_Siniestro_Facultativo">Coordinador de Siniestros Facultativo</option> 
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Gerente_Siniestro_Facultativo">Gerente de Siniestros Facultativo</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Analista_Administrativo">Analista Administrativo</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Analista_Administrativo_Facultativo">Analista Administrativo Facultativo</option>
			 					 
								  
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Director_Reaseguro">Director Reaseguro</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Subdirector_Reaseguro">Subdirector de Reaseguro</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Op_Asistente_Subdirector_Reaseguro">Operador Asistente Subdirector Reaseguro</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Op_Reaseguro_Automatico">Operador Reaseguro Automatico</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Op_Reaseguro_Facultativo">Operador Reaseguro Facultativo</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Op_Pagos_Cobros_Reaseguradores">Operador de Pagos y Cobros a Reaseguradores</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Coordinador_Administrativo_Reaseguro">Coordinador Administrativo Reaseguro</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Actuario">Actuario</option>
								  
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Esp_Administrador_Colonias">Administrador Colonias (Especial)</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Reportes_Danios">Reportes Da&ntilde;os</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Reportes_Siniestros">Reportes Siniestros</option>
								  <option value="/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=Rol_Reportes_Reaseguro">Reportes Reaseguro</option>
								 
								  
								 
								</select>
							</td>
						</tr>
						<tr>
							<td width="70%" colspan="3"></td>
							<td>													
								<div id="b_seleccionar">
									<a href="#" onclick="javascript: window.location.href = document.getElementById('roles').options[document.getElementById('roles').selectedIndex].value;"><midas:mensaje clave="midas.accion.seleccionar"/></a>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			
		<div id="foot_log"></div>
	</div>
</body>
</html>
