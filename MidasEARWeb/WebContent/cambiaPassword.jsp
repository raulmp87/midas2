<%@ page isELIgnored="false"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>Login Midas</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Login page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/login.css"/>" rel="stylesheet" type="text/css">
		
	<script type="text/javascript">
		 
		function muestraMensaje() {
			var Request = {

						    QueryString : function(name)
						
						    {
						
						        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
						
						        var regexS = "[\\?&]"+name+"=([^&#]*)";
						
						        var regex = new RegExp( regexS );
						
						        var results = regex.exec( window.location.href );
						
						        if( results == null ){
						
						            return "";
						
						        }else{
						
						            return results[1];
						
						        }
						
						    }
		
			}
			var variable= Request.QueryString('mensaje');
			if(variable != '') {
				if (variable == 1) {//El usuario no es valido
					alert("El usuario no es valido");
				}
				if (variable == 2) {//Las nuevas contraseņas no son iguales
					alert("Las nuevas contraseņas no son iguales");
				}
				if (variable == 3) {//Hubo un error al actualizar
					alert("Hubo un error al actualizar");
				}
			}
		
		}
		 
      function cambiaPassword() {
      
      	var mensajeError = null;
      	
      	if (document.validarUsuarioForm.usuario.value.trim() == ''
      		|| document.validarUsuarioForm.password.value.trim() == ''
      		|| document.validarUsuarioForm.nuevopassword.value.trim() == ''
      		|| document.validarUsuarioForm.renuevopassword.value.trim() == ''
      	
      	) 
      	mensajeError = 'Favor de llenar todos los campos'
      
      	if (mensajeError == null) {
	      	var url = '/MidasWeb/usuario/cambiarPassword.do?nombreusuario=' +
	      	document.validarUsuarioForm.usuario.value +
	      	'&password=' +
	      	document.validarUsuarioForm.password.value +
	      	'&nuevopassword=' + 
	      	document.validarUsuarioForm.nuevopassword.value + 
	      	'&renuevopassword=' + 
	      	document.validarUsuarioForm.renuevopassword.value
	      	window.location.href = url; 
      	} else {
      		alert(mensajeError);
      	}
      }
    
	</script>
  </head>
  
  <body id="loginBody">
    
    <div class="login">
			<div id="head_log"></div>
				<div id="cont_log">
					<div id="cont_img">
					
					<midas:formulario accion="/usuario/cambiarPassword">
					
					<table width="100%">
						<tr>
				      		<td class="logo" colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="bienvenido" colspan="4">
								Cambio de contrase&ntilde;a
							</td>
						</tr>
						<tr>
							<td width="50%" style="text-align: right">
								<midas:mensaje clave="Login.usuario"/>
							</td>
	    					<td width="25%" align="left" style="text-align: left" >
	    						<div align="left">
	    							<input id="usuario" name="usuario" type="text" width="30px" maxlength="8px"/>
	    						</div>
	    					</td>
	    					<td width="25%" align="left" colspan="2" style="text-align: left" >
	    						
	    					</td>
						</tr>
						<tr>
							<td  width="50%" style="text-align: right">
	    						<midas:mensaje clave="Login.contrasena"/>
	    					</td>
	    					<td width="50%" style="text-align: left" colspan="3">
	    						<midas:password propiedadFormulario="password" nombreFormulario="validarUsuarioForm"/>
	    					</td>
						</tr>
						<tr height="80px" ><td colspan="4"></td></tr>
						<tr>
							<td  width="50%" style="text-align: right">
	    						<midas:mensaje clave="Login.nuevacontrasena"/>
	    					</td>
	    					<td width="50%" style="text-align: left" colspan="3">
	    						<midas:password propiedadFormulario="nuevopassword" nombreFormulario="validarUsuarioForm"/>
	    					</td>
						</tr>
						<tr>
							<td  width="50%" style="text-align: right">
	    						<midas:mensaje clave="Login.repitenuevacontrasena"/>
	    					</td>
	    					<td width="50%" style="text-align: left" colspan="3">
	    						<midas:password propiedadFormulario="renuevopassword" nombreFormulario="validarUsuarioForm"/>
	    					</td>
						</tr>
						<tr>
							<td width="40%" colspan="3"></td>
							<td>													
								<div id="b_seleccionar">
									<a href="#" onclick="javascript: cambiaPassword();"><midas:mensaje clave="midas.accion.guardar"/></a>
								</div>
							</td>
						</tr>
					</table>
					
					</midas:formulario>
				</div>
			</div>
			
		<div id="foot_log"></div>
	</div>
    
  </body>
  <script type="text/javascript">
  	muestraMensaje();
  </script>
  
</html>
