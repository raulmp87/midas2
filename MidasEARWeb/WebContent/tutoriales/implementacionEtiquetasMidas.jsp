<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
	<title>
    </title>
    <link href="../css/midas.css" rel="stylesheet" type="text/css" />
  </head>
  
  <body>
      <table border="0">
        <tr>
          <td><h2>Implementacion de Etiquetas Midas</h2></td>
        </tr>
        <tr>
          <td><h3>�Qu&eacute; son las etiquetas Midas?</h3></td>
        </tr>
        <tr>
        	<td>
        		<p align="justify">
	        		Son componentes de struts personalizados cuya finalidad es ser utilizados
	        		para implementar la interfaz grafica del sistema Midas, es decir, deben de ser utilizados
	        		en lugar de los componentes tradicionales de struts
        		</p>
        	</td>
        </tr>
        <tr>
        	<td>
        		<h2>�Como los utilizo?</h2>
        	</td>
        </tr>
        <tr>
        	<td>
	        		Primero que nada debemos de indicarle a nuestra pagina JSP que queremos utilizar las etiquetas midas,
	        		esto se realiza agregando la siguiente linea al inicio de la pagina:
	        		<br>
	        			<pre><B>&lt;%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %></B></pre>
	        		<br>
	        		Una vez que hayas hecho esto y si estas utilizando MyEclipse al presionar las teclas <b>Control + Barra Espaciadora</b>
	        		aparecen todas las opciones con las que cuentas en el JSP y si tu escribes 'midas', deben aparecer todos los componentes
	        		midas que tienes disponibles y a un costado la descripcion de cada uno, algo similar a la siguiente imagen:
	        		<img alt="ejemplo de implementacion de etiquetas midas" src="img/midasOpcionesEtiquetas.JPG">
	        		<br>
	        		Cuando hayas seleccionado una etiqueta midas, aparecera ya el codigo necesario para implementarla con las opciones
	        		requeridas por default, es decir, de las opciones que te ha escrito la etiqueta, aun puedes en gran parte de las etiquetas
	        		agregarle parametros opcionales pulsando de nuevo Control + Barra Espaciadora, algo similar a la siguiente imagen
	        		donde el atributo clave es requerido y lo pide la etiqueta al momento de seleccionarla y el atributo mensajeError
	        		es opcional:
	        		<img alt="ejemplo de implementacion de etiquetas midas con atributos opcionales" src="img/midasEtiquetasAtributosOpcionales.JPG">
	        		<br>
	        		Entonces tu codigo deber&aacute; lucir similar al siguiente:
	        		<br>
	        		&lt;midas:mensaje clave="Login.mensajeBienvenida"/>
	        		<br>
				  	&lt;br>
				  	<br>
				  	&lt;midas:tabla idTabla="data" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="usuariologueado" claseCss="dataTable">
				  	<br>
				  		&lt;midas:columna propiedad="usuarionombre" titulo="Nombre de usuario">&lt;/midas:columna>
				  	<br>
				  	&lt;/midas:tabla>
				  	<br>
	        		
	        		Mas adelante se describira detalladamente el funcionamiento de cada etiqueta midas y sus atributos. 
        	</td>
        </tr>
        <tr>
        	<td>
        		<h2>�Qu&eacute; componentes tengo disponibles?</h2>
        	</td>
        </tr>
    </table>
    <table border="3" bgColor="#ffffe6">
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3><b>mensaje</b></h3>
        	</td>
        	<td>
        		Escribe el valor asignado a una clave dentro del archivo de recursos
        		<h4>Parametros</h4>
        		<b>clave</b> (requerido): Identifica un mensaje dentro del archivo de recursos
        		<h4>Implementacion</h4>
        		He agregado un mensaje al archivo RecursosDeMensajes.properties identificado por la clave: <b>Tutorial.etiqueta.mensajeprueba</b> 
        		al implementar la etiqueta mensaje: <b>&lt;midas:mensaje clave="Tutorial.etiqueta.mensajeprueba"/></b> 
        		debemos obtener la siguiente salida:<br> 
        		<b><midas:mensaje clave="Tutorial.etiqueta.mensajeprueba"/></b>
        	</td>
        </tr>
        <%	
        	
        %>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3><b>tabla</b></h3>
        	</td>
        	<td>
        		Despliega odenadamente un conjunto de datos implementando paginacion y ordenamiento de ser requerido
        		<h4>Parametros</h4>
        		<b>idTabla</b> (requerido): Identifica la tabla dentro de un jsp<br>
        		<b>nombreLista</b> (requerido): Identificador de la lista de valores que se van a desplegar,
        									Esta lista debe de estar incluida en el request desde antes que se cargue
        									el jsp donde esta contenida esta etiqueta y debe ser de tipo List&lt;algunDTO>
        									o bien datos que conserven el mismo formato, es decir List&lt;algunPojo> donde algunPojo
        									defina sus atributos acompa�ados de su respectivo set y get<br>
        		<b>claseDecoradora</b> (requerido): recibe el nombre de la clase decoradora precedido por el paquete que lo contiene,
        										esta clase es la encargada de dar formato a la informacion que aparecer� en la tabla,
        										es decir, debemos especificar para cada campo el formato que requerimos(fecha, moneda,etc)
        										cuando sea necesario, por ejemplo, podemos revisar las clase decoradoras contenidas en:
        										mx.com.afirme.midas.decoradores y en ese mismo paquete definir las que necesitemos
        		<b>claseCss</b> (requerido): Es el identificador de la clase contenida en un archivo css que define la visualizacion
        									de la tabla (colores, grosor de linea,fuente, etc), este archivo debe ser referenciado en cada jsp donde 
        									se introduzca una tabla de la siguiente manera: &lt;link href="../css/style.css" rel="stylesheet" type="text/css" />
        									aunque la ruta en el parametro <b>href</b> depender&aacute; de la jerarqu&iacute; de directorio desde donde se invoque
        									al archivo css
        		<b>exportar</b> (opcional): Ind&iacute;ca si queremos habilitar en la tabla las opciones de exportacion en formatos XLS, CSV y XML.
        									Si se omite este parametro las opciones de exportacion no aparecer&aacute;n en la tabla
        		<h4>Implementacion</h4>
        		En su forma mas simple, solo necesitas llenar los parametros que te pide la etiqueta tabla y ella sola se encargar&aacute;,
        		de desplegar todos los atributos que contenga el pojo de la lista conservando el nombre de los atributos,esta implementacion deber&iacute;a ser similar a la siguiente:
        		<b>&lt;midas:tabla idTabla="data" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="listaUsuariosTutorial" claseCss="dataTable">
  				&lt;/midas:tabla></b> teniendo como resultado: <br>
  				<midas:tabla idTabla="data" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="listaUsuariosTutorial" claseCss="dataTable">
  				</midas:tabla>
  				<br>
  				El ancho de la tabla depende del ancho de su contenedor, como vemos en la siguiente implementacion asignamos el ancho manualmente, pero tambien podemos
  				hacer referencia a un identificador de una clase CSS:<br>
  				&lt;div style="width:400px"><br>
	  				&lt;midas:tabla idTabla="datosUsuariosMidas" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="listaUsuariosTutorial" claseCss="dataTable"><br>
	  				&lt;/midas:tabla><br>
  				&lt;/div>
  				<div style="width:400px"><br>
	  				<midas:tabla idTabla="datosUsuariosMidas" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" 
	  				nombreLista="listaUsuariosTutorial" claseCss="resultsTable sort-table" exportar="true"><br>
	  				</midas:tabla><br>
  				</div>
  				<h4>Consideraciones</h4>
  				Al invocar a tabla en su forma mas simple debemos tener en cuenta que el titulo de las columnas
  				ser&aacute; el mismo nombre de cada atributo del DTO o pojo que hemos agregado en la lista de 
  				valores y que se desplegar&aacute;n todos los atributos. Si queremos especificar el nombre de la
  				columna a mostrar asi como cuales deben ser visualizadas, debemos incluir en el cuerpo de la etiqueta
  				otra etiqueta llamada columna, la cual se explicar&aacute; a continuacion.
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>columna</h3>
        	</td>
        	<td>
        		Afina la etiqueta tabla permitiendo especificar cada columna que querramos mostrar. Solo puede utilizarse dentro del cuerpo de la etiqueta tabla
        		asi como su titulo
        		<h4>Parametros</h4>
        		<b>propiedad</b> (requerido): indica el nombre del atributo del DTO o pojo que queremos mostrar<br>
        		<b>titulo</b> (opcional): indica el titulo que tendr&aacute; la propiedad que hemos decidido
        									que aparezca en la tabla, si este parametro es omitido, el titulo tendr&aacute;
        									el mismo nombre que el atributo.
        		<h4>Implementacion</h4>
        		Continuaremos afinando la misma tabla ya hemos implementado, as&iacute; que ahora mostraremos
        		solamente la columna <b>usuarionombre</b> asignandole el titulo <b>Nombre de Usuario</b>,
        		por lo tanto tendremos el siguiente codigo con su respectiva salida, donde haremos un poco mas peque�o el contenedor
        		ya que mostraremos un solo campo:<br>
        		&lt;div style="width:150px"><br>
	  				&lt;midas:tabla idTabla="datosUsuariosMidasUnaColumna" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="listaUsuariosTutorial"
	  				claseCss="dataTable"><br>
	  				<b>&lt;midas:columna propiedad="usuarionombre" titulo="Nombre de Usuario"/></b><br>
	  				&lt;/midas:tabla><br>
  				&lt;/div>
  				<div style="width:150px">
	  				<midas:tabla idTabla="datosUsuariosMidasUnaColumna" claseDecoradora="mx.com.afirme.midas.decoradores.MidasDecoradorUsuario" nombreLista="listaUsuariosTutorial" 
	  				claseCss="dataTable" exportar="true"><
	  					<midas:columna propiedad="usuarionombre" titulo="Nombre de Usuario"  estilo="error"/>
	  				</midas:tabla>
  				</div>
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>Formulario</h3>
        	</td>
        	<td>
        		Crea un formulario Web ligado a una accion, todos las etiquetas de componentes midas que
        		se escriban dentro de este formulario, automaticamente estar&aacute;n mapeadas al dataform
        		relacionado con la accion
        		<h4>Parametros</h4>
        		<b>accion</b> (requerido): Identifica la accion que se va a ejecutar al enviar el formulario
        		<h4>Implementacion</h4>
        		en este caso estamos mapeando el campo de texto a la propiedad <b>usuario</b> del formulario asignado a la accion <b>/validarUsuario.do</b>
        		con el codigo:<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    	&lt;td>&lt;midas:texto propiedadFormulario="usuario"/>&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
        		<br>
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				    	<td><midas:mensaje clave="Login.usuario"/></td>
				    	<td><midas:texto propiedadFormulario="usuario"/></td>
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>texto</h3>
        	</td>
        	<td>
        		Una simple caja de texto
        		<h4>Parametros</h4>
        		<b>propiedadFormulario</b> (requerido): Es el nombre del atributo del dataForm al cual se va a mapear
        												el campo de texto si el campo de texto est&aacute; contenido dentro de la etiqueta
        												formulario, en caso contrario deber&aacute;
        												utilizarse el parametro opcional nombreformulario para hacer referencia al formulario
        												que se quiere mapear<br>
        		<b>nombreFormulario</b> (opcional): Debe contener el nombre del dataForm al que se va a mapear el campo de texto<br>
        		<b>caracteres</b> (opcional): Numero maximo de caracteres que queremos permitir que sean introducidos en el campo de texto<br>
        		<b>longitud</b> (opcional): Longitud del campo de texto expresada en numero de caracteres(aproximadamente ya que el html no interpreta el espacio exacto
        												sin embargo puedes definir la longitud de tus campos de texto con este parametro)<br>
        		<b>deshabilitado</b> (opcional): Especifica si el campo de texto responder&aacute; a la introduccion de texto, posibles valores:
        										"true" o "false"
        		<h4>Implementacion</h4>
        		Vamos a jugar un poco con la etiqueta texto agregando o quitando parametros:
    
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				  	<td><b>Etiqueta texto simple</b></td>
				    <td><midas:mensaje clave="Login.usuario"/></td>
				    <td><midas:texto propiedadFormulario="usuario"/></td>
				  </tr>
				  <tr> 
				  	<td><b>Etiqueta texto que solo permite 6 caracteres y logitud 15</b></td>
				    <td><midas:mensaje clave="Login.usuario"/></td>
				    <td><midas:texto propiedadFormulario="usuario" caracteres="6" longitud="15" /></td>
				  </tr>
				  <tr> 
				  	<td><b>Etiqueta texto deshabilitada</b></td>
				    <td><midas:mensaje clave="Login.usuario"/></td>
				    <td><midas:texto propiedadFormulario="usuario" deshabilitado="true"/></td>
				  </tr>
				  <tr> 
				  	<td><b>Etiqueta texto deshabilitada con longitud 40</b></td>
				    <td><midas:mensaje clave="Login.usuario"/></td>
				    <td><midas:texto propiedadFormulario="usuario" deshabilitado="true" longitud="40"/></td>
				  </tr>
				</table>
        		</midas:formulario>
        		<br>
        		Este es el codigo que genera las cajas de texto: <br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> <br>
				  	&lt;td>&lt;b>Etiqueta texto simple&lt;/b>&lt;/td><br>
				    &lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    &lt;td>&lt;midas:texto propiedadFormulario="usuario"/>&lt;/td><br>
				  &lt;/tr><br>
				  &lt;tr> <br>
				  	&lt;td>&lt;b>Etiqueta texto que solo permite 6 caracteres y logitud 15&lt;/b>&lt;/td><br>
				    &lt;td>&lt;&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    &lt;td>&lt;midas:texto propiedadFormulario="usuario" caracteres="6" longitud="15" />&lt;/td><br>
				  &lt;/tr><br>
				  &lt;tr> <br>
				  	&lt;td>&lt;b>Etiqueta texto deshabilitada&lt;/b>&lt;/td><br>
				    &lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    &lt;td>&lt;midas:texto propiedadFormulario="usuario" deshabilitado="true"/>&lt;/td><br>
				  &lt;/tr><br>
				  &lt;tr> <br>
				  	&lt;td>&lt;b>Etiqueta texto deshabilitada con longitud 40&lt;/b>&lt;/td><br>
				    &lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    &lt;td>&lt;midas:texto propiedadFormulario="usuario" deshabilitado="true" longitud="40"/>&lt;/td><br>
				  &lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario>
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>password</h3>
        	</td>
        	<td>
        		Una simple caja de texto donde todo caracter que se introduzca ser&aacute; mostrado al usuario como un <b>*</b>
        		<h4>Parametros</h4>
        		<b>propiedadFormulario</b> (requerido): Es el nombre del atributo del dataForm al cual se va a mapear
        												el campo de texto si el campo de texto est&aacute; contenido dentro de la etiqueta
        												formulario, en caso contrario deber&aacute;
        												utilizarse el parametro opcional nombreformulario para hacer referencia al formulario
        												que se quiere mapear<br>
        		<b>nombreFormulario</b> (opcional): Debe contener el nombre del dataForm al que se va a mapear el campo de texto<br>
        		<b>caracteres</b> (opcional): Numero maximo de caracteres que queremos permitir que sean introducidos en el campo de texto<br>
        		<b>longitud</b> (opcional): Longitud del campo de texto expresada en numero de caracteres(aproximadamente ya que el html no interpreta el espacio exacto
        												sin embargo puedes definir la longitud de tus campos de texto con este parametro)<br>
        		<b>deshabilitado</b> (opcional): Especifica si el campo de texto responder&aacute; a la introduccion de texto, posibles valores:
        										"true" o "false"
        		<h4>Implementacion</h4>
        		Debido a que la diferencia con una etiqueta de texto normal radica en como se presenta la informacion al usuario, vamos a seguir implementando
        		el mismo formulario, agregandole el campo para password con el fin de replicar la funcionalidad de un formulario de login
        		donde los campos de usuario y password estar&aacute;n limitados a 5 caracteres
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				    	<td><midas:mensaje clave="Login.usuario"/></td>
				    	<td><midas:texto propiedadFormulario="usuario" caracteres="5"/></td>
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/> </td>
				    	
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    	&lt;td>&lt;midas:texto propiedadFormulario="usuario" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.contrasena"/>&lt;/td><br>
				    	&lt;td>&lt;midas:password propiedadFormulario="contrase�a" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
    		</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>Oculto</h3>
        	</td>
        	<td>
        		Una simple caja de texto que no es mostrada al usuario donde podamos almacenar algun dato que nos interese
        		<h4>Parametros</h4>
        		<b>propiedadFormulario</b> (requerido): Es el nombre del atributo del dataForm al cual se va a mapear
        												el campo de texto si el campo de texto est&aacute; contenido dentro de la etiqueta
        												formulario, en caso contrario deber&aacute;
        												utilizarse el parametro opcional nombreformulario para hacer referencia al formulario
        												que se quiere mapear<br>
        		<b>nombreFormulario</b> (opcional): Debe contener el nombre del dataForm al que se va a mapear el campo de texto<br>
        		<h4>Implementacion</h4>
        		Debido a que la diferencia con una etiqueta de texto normal radica en que no es visible al usuario, vamos a agregar el codigo que la
        		genera solo como una referencia
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				    	<td><midas:mensaje clave="Login.usuario"/></td>
				    	<td><midas:texto propiedadFormulario="usuario" caracteres="5"/></td>
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/></td>
				  	</tr>
				  	<tr> 
				    	<td>Aqu&iacute; hay una caja de texto oculta</td>
				    	<td><midas:oculto propiedadFormulario="mensaje"/></td>
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    	&lt;td>&lt;midas:texto propiedadFormulario="usuario" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.contrasena"/>&lt;/td><br>
				    	&lt;td>&lt;midas:password propiedadFormulario="contrase�a" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>Aqu&iacute; hay una caja de texto oculta&lt;/td><br>
				    	&lt;td>&lt;midas:oculto propiedadFormulario="mensaje"/>&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
    		</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>limpiar</h3>
        	</td>
        	<td>
        		Regresa el formulario a su estado inicial(sin datos)
        		<h4>Parametros</h4>
        		Ninguno. Debe introducirse el texto que aparecer&aacute; sobre el boton, entre la etiqueta de apertura
        		y la etiqueta de cierre
        		<h4>Implementacion</h4>
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				    	<td><midas:mensaje clave="Login.usuario"/></td>
				    	<td><midas:texto propiedadFormulario="usuario" caracteres="5"/></td>
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/></td>
				  	</tr>
				  	<tr>
				  		<td>
				  		</td>
				  		<td>
				  			<midas:limpiar>Limpiar formulario</midas:limpiar>
				  		</td>
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    	&lt;td>&lt;midas:texto propiedadFormulario="usuario" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.contrasena"/>&lt;/td><br>
				    	&lt;td>&lt;midas:password propiedadFormulario="contrase�a" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr><br>
				  		&lt;td><br>
				  		&lt;/td><br>
				  		&lt;td><br>
				  			&lt;midas:limpiar>Limpiar formulario&lt;/midas:limpiar><br>
				  		&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>enviar</h3>
        	</td>
        	<td>
        		Envia la informacion introducida dentro del formulario a la accion definida en la etiqueta formulario
        		<h4>Parametros</h4>
        		Ninguno. Debe introducirse el texto que aparecer&aacute; sobre el boton, entre la etiqueta de apertura
        		y la etiqueta de cierre
        		<h4>Implementacion</h4>
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr> 
				    	<td><midas:mensaje clave="Login.usuario"/></td>
				    	<td><midas:texto propiedadFormulario="usuario" caracteres="5"/></td>
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/></td>
				  	</tr>
				  	<tr>
				  		<td>
				  		</td>
				  		<td>
				  			<midas:limpiar>Limpiar formulario</midas:limpiar>
				  		</td>
				  		<td>
				  			<midas:enviar>Entrar</midas:enviar>
				  		</td>
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr> 
				    	&lt;td>&lt;midas:mensaje clave="Login.usuario"/>&lt;/td><br>
				    	&lt;td>&lt;midas:texto propiedadFormulario="usuario" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.contrasena"/>&lt;/td><br>
				    	&lt;td>&lt;midas:password propiedadFormulario="contrase�a" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr><br>
				  		&lt;td><br>
				  		&lt;/td><br>
				  		&lt;td><br>
				  			&lt;midas:limpiar>Limpiar formulario&lt;/midas:limpiar><br>
				  		&lt;/td><br>
				  		&lt;td><br>
				  			&lt;midas:enviar>Entrar&lt;/midas:enviar><br>
				  		&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
        	</td>
        </tr>
         <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>radio</h3>
        	</td>
        	<td>
        		Elemento de seleccion simple, un boton de radio que establece un valor a una cierta propiedad del dataform,
        		se agrupa de acuerdo a la propiedad seleccionada del dataform y debe siempre estar contenido dentro de una
        		etiqueta formulario
        		<h4>Parametros</h4>
        		<b>propiedadFormulario</b> (requerido):  atributo del dataform, al cual modoficaremos el valor<br>
        		<b>valorEstablecido</b> (requerido):  El valor que estableceremos a propiedadFormulario
        		<h4>Implementacion</h4>
        		<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr>
				  		<td>
				  			<midas:mensaje clave="Login.usuario"/>
				  		</td>
						<td>
						<midas:radio propiedadFormulario="usuario" valorEstablecido="christian">christian</midas:radio>
						<midas:radio propiedadFormulario="usuario" valorEstablecido="jesus">jesus</midas:radio>			
				  		</td>
				  		
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/></td>
				  	</tr>
				  	<tr>
				  		<td>
				  		</td>
				  		<td>
				  			<midas:limpiar>Limpiar formulario</midas:limpiar>
				  		</td>
				  		<td>
				  			<midas:enviar>Entrar</midas:enviar>
				  		</td>
				  	</tr>
				</table>
        		</midas:formulario>
        		<br>
        		&lt;midas:formulario accion="/validarUsuario"><br>
        		&lt;table><br>
        			&lt;tr><br>
				  		&lt;td><br>
				  			&lt;midas:mensaje clave="Login.usuario"/><br>
				  		&lt;/td><br>
						&lt;td><br>
						&lt;midas:radio propiedadFormulario="usuario" valorEstablecido="christian">christian&lt;/midas:radio><br>
						&lt;midas:radio propiedadFormulario="usuario" valorEstablecido="jesus">jesus&lt;/midas:radio><br>			
				  		&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr> <br>
				    	&lt;td>&lt;midas:mensaje clave="Login.contrasena"/>&lt;/td><br>
				    	&lt;td>&lt;midas:password propiedadFormulario="contrase�a" caracteres="5"/>&lt;/td><br>
				  	&lt;/tr><br>
				  	&lt;tr><br>
				  		&lt;td><br>
				  		&lt;/td><br>
				  		&lt;td><br>
				  			&lt;midas:limpiar>Limpiar formulario&lt;/midas:limpiar><br>
				  		&lt;/td><br>
				  		&lt;td><br>
				  			&lt;midas:enviar>Entrar&lt;/midas:enviar><br>
				  		&lt;/td><br>
				  	&lt;/tr><br>
				&lt;/table><br>
        		&lt;/midas:formulario><br>
        	</td>
        </tr>
        <tr>
        	<td width="10%" align="center" valign="middle">
        		<h3>escribe</h3>
        	</td>
        	<td>
        		Escribe el valor de cierto atributo asociado a un bean (Como un formulario)
        		<h4>Parametros</h4>
	        		<b>nombre</b> (requerido): Nombre del bean<br>
	        		<b>propiedad</b> (requerido): Atributo del bean, del cual se quiere escribir el valor
        		<h4>Implementacion</h4>
        			Simplemente asignamos a la etiqueta el nombre del bean (en este caso un formulario) y el atributo que se va a imprimir
        			teniendo en cuenta que el valor que se va a escribir ya debe estar asignado, podemos utilizar el siguiente codigo para
        			escribir el valor de un formulario, est&eacute; o no est&eacute; dentro de una etiqueta formulario, la implementacion
        			es la misma: <br>
        		<b>&lt;midas:escribe propiedad="razonSocial" nombre="ajustadorForm"/></b>
        	</td>
        </tr>
        <tr>
        	<midas:formulario accion="/validarUsuario">
        		<table>
        			<tr>
				  		<td>
				  			<midas:mensaje clave="Login.usuario"/>
				  		</td>
				  		<td>
				  			<midas:combo propiedad="usuario">
				  				<midas:opcionCombo valor="">Seleccione una opcion...</midas:opcionCombo>
				  				<midas:opcionCombo valor="christian">Christian</midas:opcionCombo>
				  				<midas:opcionCombo valor="jesus">Jesus</midas:opcionCombo>
				  			</midas:combo>
				  		</td>
						<td>
						<midas:radio propiedadFormulario="usuario" valorEstablecido="christian">christian</midas:radio>
						<midas:radio propiedadFormulario="usuario" valorEstablecido="jesus">jesus</midas:radio>			
				  		</td>
				  	</tr>
				  	<tr> 
				    	<td><midas:mensaje clave="Login.contrasena"/></td>
				    	<td><midas:password propiedadFormulario="contrase�a" caracteres="5"/></td>
				  	</tr>
				  	<tr>
				  		<td>
				  		</td>
				  		<td>
				  			<midas:limpiar>Limpiar formulario</midas:limpiar>
				  		</td>
				  		<td>
				  			<midas:enviar>Entrar</midas:enviar>
				  		</td>
				  	</tr>
				</table>
        		</midas:formulario>
        </tr>
    </table>
  	<br>
  	
  </body>
</html:html>
