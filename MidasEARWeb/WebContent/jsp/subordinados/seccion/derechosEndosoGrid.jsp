<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
<!-- 			<call command="attachEvent"><param>onRowCreated</param><param>registrarCoberturaPaquete</param></call> -->
		</beforeInit>
		
		<column id="idDerecho" type="ro" width="0" sort="int" hidden="true">idDerechos</column>
		<column id="numeroSecuencia" type="ro" width="100" sort="int" hidden="true"># Secuencia</column>
		<column id="valor" type="ed" width="*" sort="int">Valor</column>
		<column id="claveDefault" type="ra" width="200" align ="center">Clave Default</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="derechosEndoso">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveDefault" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>