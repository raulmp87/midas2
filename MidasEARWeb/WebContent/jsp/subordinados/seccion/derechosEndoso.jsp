<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/subordinados/seccion/subordinadosSeccionHeader.jsp"/>

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarDerechosEndoso">
			
			<s:hidden name="idSeccion"/>
		
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="6"><midas:mensaje clave="configuracion.seccion.subordinados.derechos.endoso" />
					&nbsp;<midas:mensaje clave="configuracion.seccion.subordinados.sufijoseccion" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="derechosEndosoGrid" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
					</td>
					<td colspan="2">
						<div id="b_agregar">
							<a href="javascript: void(0);" onclick="javascript: agregarDerecho();">Agregar</a>
						</div>
						<br/><br/>
						<div id="b_borrar">
							<a href="javascript: void(0);" onclick="javascript: eliminarDerecho(); ">Eliminar</a>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="6"><midas:mensaje clave="configuracion.eliminar.seleccionado.mensaje"/></td>
				</tr>			
			</table>
		</s:form>
	</center>
</div>