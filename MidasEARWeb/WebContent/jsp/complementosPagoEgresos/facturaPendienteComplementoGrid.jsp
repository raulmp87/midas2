<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="numeroFactura" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.numeroFactura"/></column>
		<column id="fechaFactura"  type="ro" width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.fecha"/></column>
		<column id="importeSubTotal" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.subtotal"/></column>
		<column id="importeIva" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.iva"/></column>
		<column id="importeIvaRet"  type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.ivaRetenido"/></column>
		<column id="importeIsr" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.isr"/></column>
		<column id="importeTotal" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.total"/></column>
		<column id="estatusDesc" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.estatus"/></column>
		<column id="importeRestante" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.montoRestante"/></column>
		<column id="uuidCfdi" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.UUID"/></column>			
	</head>	  		
	<s:iterator value="listaFacturas" >
		<row id="<s:property value="uuidCfdi"/>">
		    <cell><s:property value="numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFactura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="importeSubTotal" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="importeIva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="importeIvaRet" escapeHtml="false"/></cell>
			<cell><s:property value="importeIsr" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="importeTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>	
    		<cell><s:property value="importeRestante" escapeHtml="false" escapeXml="true"/></cell>		
  			<cell><s:property value="uuidCfdi" escapeHtml="false" escapeXml="true"/></cell>	
		</row>
	</s:iterator>	
</rows>
