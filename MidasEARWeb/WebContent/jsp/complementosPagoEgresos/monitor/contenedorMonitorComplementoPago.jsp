<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript" src="<s:url value='/js/complementosPagoEgresos/monitor.js'/>"></script>
<script type="text/javascript">
	var listarPendientesMonitor = '<s:url action="listarPendientesMonitor" namespace="/complementosPago/egresos"/>';
	var enviarNotificacionMonitor = '<s:url action="enviarNotificacionMonitor" namespace="/complementosPago/egresos"/>';
</script>

<style type="text/css">
 div.ui-datapicker{
   font-size:10px;
 }
 table tr td div span label{
   color:black;
   font-weight: normal;
   text-align: left;
 }
 .td{
   max-width:150px;
 }
</style>
<div id="spacer1" style="height: 20px"></div>
<div align="center">
  <s:form id="monitorForm" >
  	<s:hidden name="origenEnvio" id="origenEnvio"/>
      <table id="filtrosM2" width="60%" border="0" cellpadding="5" cellspacing="2">
        <tr>
          <td class="titulo td" colspan="6">Monitor Recepcion de Complemento Pago</td>
        </tr>
        <tr class="pf" align="left">
          <th width="25%" align="left" style="text-align:left;">
            Fecha de Pago Inicio:&nbsp;
          </th>
          <td>
            <sj:datepicker name="fechaPagoInicio" id="fechaPagoInicio" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:12px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
       </sj:datepicker>
          </td>
          <th width="25%" align="right" style="text-align:right;">
            Fecha de Pago Fin:&nbsp;
          </th>
          <td>
            <sj:datepicker name="fechaPagoFin" id="fechaPagoFin" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:12px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
       </sj:datepicker>
          </td>
        </tr>
        <tr>
          <td colspan="6" align="right" class="td">
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <div id="searchBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
                    <div class="btn_back w140" style="display: inline; float: right;width: 170px;" >
                      <a href="javascript: void(0);" class="icon_buscar"
                        onclick="javascript: buscarFacturasPendientesComplemento();">
                        <s:text name="Buscar"/>
                      </a>
                    </div>
                  </div>
                  </td>
				<td>
                	<div id="execButon"  class="w150" style="float:right; padding-right: 10px;width: 200px;">
                      <div class="btn_back w140" style="display: inline; float: left;width: 200px;" >
                        <a href="javascript: void(0);" class="icon_ejecutar"
                          onclick="javascript: envioNotificacionComplementos();">
                          <s:text name="Enviar Notificacion"/>
                        </a>
                      </div>
                    </div>
                  </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>
      <div id="divGrid">    
    	<div id="indicador"></div>    		
		<div id="facturasPtecomplementosPagoGrid" class="h300" style="background-color:white;overflow:hidden"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
      </div>
  </s:form>
</div>
