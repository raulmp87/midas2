<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>

        <column id="liquidacion"       		type="ro" width="120" sort="int" ><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.noLiquidacion'}"/></column>
		 
		<column id="tipoLiquidacion"   		type="ro" width="140" sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.tipoLiquidacion'}"/> </column>
		
		<column id="claveAgente"       		type="ro" width="90"  sort="int"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.noPrestadorServicio'}"/> </column>
		 
		<column id="agente"         		type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.prestadorServicio'}"/> </column>
		
		<column id="factura"           		type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.noFactura'}"/> </column>
		
		<column id="noSolCheque"       		type="ro" width="135" sort="int"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.noSolicitudCheque'}"/> </column>
		<column id="noCheque" 		   		type="ro" width="135" sort="int"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.noChequeReferencia'}"/> </column>
		<column id="uuidFactura"         	type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.uuidFactura'}"/> </column>
		<column id="fechaPago"    	   		type="ro" width="120" sort="date"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.fechaPago'}"/> </column>
		<column id="uuidComplementoPago"    type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.uuidComplementoPago'}"/> </column>
		<column id="ImportePagado"        	type="ro" width="100" sort="int"><s:text name="%{'midas.siniestros.pagos.facturas.complementosPago.monitor.ImportePago'}"/> </column>
	  	</head>
	<s:iterator value="litadoFacturaPteRecepcionComplementos" status="row">
		<row> 
			<cell><s:property value="noLiquidacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoLiquidacion" escapeHtml="false" escapeXml="true" /></cell> 
			<cell><s:property value="noPrestadorServicio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="prestadorServicio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="factura" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noSolCheque" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noCheque" escapeHtml="false" escapeXml="true" /></cell> 
			<cell><s:property value="uuidFactura" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaPago" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="uuidComplementoPago" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="%{getText('struts.money.format',{ImportePagado})}" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>	
</rows>  