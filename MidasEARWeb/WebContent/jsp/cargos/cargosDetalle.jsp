<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/cargos/cargosHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript">
cargaGridCargos();
var fechaAlta = jQuery("#fecAltaMovimiento").val();
if (fechaAlta == null || fechaAlta == "") {
	var fechaHoy = fechaActual();
	jQuery("#fecAltaMovimiento").val(fechaHoy);
}

controlDeBotones();
</script>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.cargos.tituloAltaCargos')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.cargos.tituloConsultarCargo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
		muestraDocumentosADigitalizar();
		jQuery(".btn_digitalizarDoc").css("display","none");
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.cargos.tituloEliminarCargos')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
		jQIsRequired();
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.cargos.tituloEditarCargos')}"/>
	<script type="text/javascript">
	jQuery(document).ready(function(){
			generaEstructuraFortimax();	
			muestraDocumentosADigitalizar();		
	});		
	</script>
</s:if>

<div class="titulo w400"><s:text name="#titulo"/></div>
<s:form action="agregar" id="cargosForm" name="cargosForm">
<s:hidden name="tipoAccion"/>
<s:hidden name="cargos.id"  id="cargos_id"/>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th class="jQIsRequired">&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td>
				<s:textfield  name="cargos.agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
					<s:textfield id="txtId" name ="txtIdAgente" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" name="cargos.agente.id"/>
			</td>
			<td>
				<s:if test="tipoAccion != 2">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="mostrarListadoAgentes();">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>		
				</s:if>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.estatus" />
			</td>
			<td>
				<s:textfield id="estatusAgente" readonly="true" name="cargos.agente.tipoSituacion.valor" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.centroOperacion" />
			</td>
			<td>
				<s:textfield id="descripcionCentroOperacion" readonly="true" name="cargos.agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion" cssClass="cajaTextoM2 w160"/>
			</td>
		</tr>	
		<tr>			
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia" />
			</td>
			<td>
				<s:textfield id="descripcionGerencia" name="cargos.agente.promotoria.ejecutivo.gerencia.descripcion" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>	
				<s:text name="midas.prestamosAnticipos.promotoria" />
			</td>
			<td>
				<s:textfield id="descripcionPromotoria" name="cargos.agente.promotoria.descripcion" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td width="180px">
				<s:textfield id="nombreAgente" name="cargos.agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.rfc" />
			</td>
			<td width="150px">
				<s:textfield id="rfcAgente" name="cargos.agente.persona.rfc" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.ejecutivo" />
			</td>
			<td width="150px">
				<s:textfield id="ejecutivo" name="cargos.agente.promotoria.ejecutivo.personaResponsable.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.clasificacionAgente" />
			</td>
			<td>
				<s:textfield id="tipoAgente" name="cargos.agente.clasificacionAgentes.valor"  readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.numeroFianza" />
			</td>
			<td>
				<s:textfield id="numeroFianza" name="cargos.agente.numeroFianza" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.venceFianza" />
			</td>
			<td>
				<s:textfield id="venceFianza" name="cargos.agente.fechaVencimientoFianza" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
		</tr>	
		<tr>
			<!-- td>
				<s:text name="midas.prestamosAnticipos.domicilio" />
			</td>
			<td>
				<s:textfield name="cargos.agente.direccionAgente" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td-->
			<td>
				<s:text name="midas.prestamosAnticipos.numeroCedula" />
			</td>
			<td>
				<s:textfield id="numeroCedula" name="cargos.agente.numeroCedula" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.venceCedula" />
			</td>
			<td>
				<s:textfield id="venceCedula" name="cargos.agente.fechaVencimientoCedula" readonly="true" cssClass="cajaTextoM2 w160"/>
			</td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th class="jQIsRequired">
				<s:text name="midas.cargos.tipoCargo" />
			</th>	
			<td>
				<s:select  name="cargos.tipoCargo.id" id="tipoCargo" cssClass="cajaTextoM2 jQrequired" list="listaTiposCargos" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione.." disabled="#readOnly"/>
			</td>
			<th colspan="2" align="right" class="jQIsRequired">
				<s:text name="midas.cargos.estatusCargo" />
			</th>	
			<td>
				<s:select  name="cargos.estatusCargo.id" id="estatusCargo"  cssClass="cajaTextoM2" list="listaEstatusMovimiento" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione.." disabled="true"/>
			</td>
			<th colspan="2" align="right" class="jQIsRequired">
				<s:text name="midas.cargos.fechaAltaCargo" />
			</th>	
			<td>
				<s:textfield  name="cargos.fechaAltaCargo" id="fecAltaMovimiento" cssClass="cajaTextoM2" readonly="true"/>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired">
				<s:text name="midas.cargos.importe" />
			</th>	
			<td>
				<s:textfield name="cargos.importe" id="importe" readonly="readOnly" cssClass="cajaTextoM2 w70 jQrequired"/>
			</td>
			<th class="jQIsRequired">
				<s:text name="midas.cargos.plazo" /></th>	
			<td>
				<s:select  name="cargos.plazo.id" id="plazo" cssClass="cajaTextoM2 jQrequired" list="listaPlazo" listKey="id" listValue="valor" headerValue="Seleccione.." headerKey="" disabled="#readOnly"/>
			</td>
			<th class="jQIsRequired">
				<s:text name="midas.cargos.fechaInicioCobro"/>
			</th>
			<td>
				<s:if test="tipoAccion != 2">
					<sj:datepicker name="cargos.fechaInicioCoBro" id="fecIniCobro" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	  				   onblur="esFechaValida(this);" minDate="today"></sj:datepicker>
	  			</s:if>
	  			<s:else>
	  				<s:textfield name="cargos.fechaInicioCoBro" readonly="true" cssClass="cajaTextoM2"/>
	  			</s:else>
  			</td> 
			<th class="jQIsRequired">
				&nbsp;<s:text name="midas.cargos.numeroCargos" />
			</th>	 
			<td>
				<s:textfield name="cargos.numeroCargos" id="numeroCargos" readonly="readOnly" cssClass="cajaTextoM2 w70 jQnumeric jQrequired"/>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired">
				<s:text name="midas.cargos.importeCadaCargo" />
			</th>	
			<td>
				<s:textfield name="cargos.importeCargo" id="importeCargo" cssClass="cajaTextoM2 w70 jQrequired" readonly="true"/>
			</td> 
		</tr>
		<tr>
			<th class="jQIsRequired">
				<s:text name="midas.prestamosAnticipos.observaciones" />
			</th>	
			<td colspan="3">
				<s:textfield name="cargos.observaciones" id="observaciones" readonly="readOnly" cssClass="cajaTextoM2 w210 h80 jQrequired"/>
			</td>
		</tr>	
			
<!-- 			<td colspan="3"> -->
<%-- 			<script type="text/javascript">auditarDocumentos();</script> --%>
<!-- 			<div id='cargarCheck'></div> -->
<!-- 				<table class="contenedorFormas no-border"> -->
				
<!-- 					<tr> -->
<%-- 						<td colspan="2"><div class="titulo"><s:text name="midas.prestamosAnticipos.anexos"/></div></td> --%>
<!-- 					</tr> -->
<%-- 					<s:iterator value="listaDocumentosFortimax" status="stat" var="doc" id="doc"> --%>
<!-- 					<tr> -->
<!-- 						<td width="5%"> -->
<%-- 							<s:if test="#doc.existeDocumento==1"> --%>
<!-- 							 	<input type="checkbox" checked="checked" disabled="disabled"/> -->
<%-- 							</s:if>					 --%>
<%-- 							<s:else> --%>
<!-- 								<input type="checkbox" disabled="disabled"/> -->
<%-- 							</s:else> --%>
<%-- 							<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/> --%>
<%-- 							<s:hidden name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumento"/>				 --%>
<!-- 						</td>			 -->
<!-- 						<td width="95%"> -->
<%-- 							<s:text name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumentoFortimax"/> --%>
<!-- 						</td>				 -->
<!-- 					</tr> -->
<%-- 					</s:iterator> --%>
				
<!-- 					<tr> -->
<!-- 						<td colspan="2"> -->
<%-- 							<s:if test="tipoAccion != 2"> --%>
<!-- 								<div class="btn_back w150"> -->
<!-- 									<a href="javascript: void(0);" class="" -->
<!-- 										onclick="generarLigaIfimaxCargos();">imprimirComprobanteCargo -->
<%-- 										<s:text name="midas.prestamosAnticipos.btnDigitalizarDocumentos"/> --%>
<!-- 									</a> -->
<!-- 								</div>	 -->
<%-- 							</s:if> --%>
<!-- 								<div class="btn_back w180"> -->
<!-- 									<a href="javascript: auditarDocumentos();" class="icon_confirmAll" -->
<!-- 										onclick=""> -->
<%-- 									<s:text name="Auditar"/> --%>
<!-- 									</a> -->
<!-- 								</div>	 -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 				</table> -->
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td>
				<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div align="right" class="w80 inline " >
					<div class="btn_back w80">
						<a href="javascript: void(0);"
							onclick="auditarDocumentosCargosDigitalizados();">
							<s:text name="Auditar"/>
						</a>
					</div>							
	 			</div>	
	 			</s:if>
	 		</td>
	 	</tr>		
		<tr>
			<td colspan="9" height="50px"><div style="overflow:auto;height:50px;" id='muestraDocumentosADigitalizar'></div></td>
		</tr>
		<tr>		
			<td colspan="8" align="right">
				<s:if test="tipoAccion != 2">
					<div class="btn_back w110" style="display:none;" id="btnCalcularCargo">
						<a href="javascript: void(0);" class=""
							onclick="calcularCargos();">
							<s:text name="midas.boton.aceptar"/>
						</a>
					</div>	
				</s:if>
			</td>
		</tr>
	</table>
	
	<table width="98%" class="contenedorFormas w910" align="center">
		<tr>
			<td colspan="5">
				<div class="titulo"><s:text name="midas.cargos.gridDetalle.tituloDetallCargos"/>
				</div>
			</td>
		</tr>
		<tr>	
			<td colspan="5">
			<div id="divCarga" style="position:absolute;"></div>
			<div align="center" id="gridCargos" height="200px" width="750px" style="background-color:white;overflow:hidden"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
			</td>
		</tr>
		<tr>
			<td class="w120">
				<s:if test="tipoAccion != 2">
					<div class="btn_back w110" style="display:none;" id="btnAplicar">
						<a href="javascript: void(0);" class=""
							onclick="aplicarMovimiento();"><!--  imprimirComprobanteCargo(); -->
							<s:text name="midas.prestamosAnticipos.btnAplicar"/>
						</a>
					</div>
				</s:if>
			</td>
			<td class="w120">
				<s:if test="tipoAccion != 2">
					<div class="btn_back w110" style="display:none;" id="btnGuardar">
	<!-- 				 -->
						<a href="javascript: void(0);" class=""
							onclick="guardarConfigCargos();">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>	
				</s:if>
			</td>
			<td class="w120">
				<s:if test="tipoAccion != 2">
					<div class="btn_back w110" style="display:none;" id="btnCancelar">
						<a href="javascript: void(0);" class=""
							onclick="cancelarMovimiento();">
							<s:text name="midas.boton.cancelar"/>
						</a>
					</div>	
				</s:if>
			</td>
			<td class="w120">
<!-- 				<div align="right" class="w910 inline" > -->
					<div class="btn_back w100">
						<a href="javascript: void(0);" class="icon_regresar"
							onclick="javascript: salirCargos();">
							<s:text name="midas.boton.salir"/>
						</a>
					</div>		
<!-- 				</div>	 -->
			</td>
		</tr>
	</table>	
<table width="98%" class="contenedorFormas" align="center">	
			<tr>
				<s:if test="{cargos.id!=null}">
					<td>
						<s:text name="midas.fuerzaventa.negocio.ultimaModificacion"/>
					</td>
					<td>
						<s:textfield name="ultimaModificacion.fechaHoraActualizacion" id="txtFechaHora" readonly="true" cssClass="cajaTextoM2"></s:textfield>
					</td>
					<td>
						<s:text name="midas.fuerzaventa.negocio.usuario"/>
					</td>
					<td >
						<s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" cssClass="cajaTextoM2" readonly="true"></s:textfield>
					</td>
					<td colspan="2" align="right">							
							<div class="btn_back w110">
								<a href="javascript: void(0);" class="icon_guardar" onclick="javascript: mostrarHistoricoCargos(130,${cargos.id});">	
								<s:text name="midas.boton.historico"/></a>	
							</div>				
					</td>
				</s:if>		
			</tr>		
		</table>
</s:form>