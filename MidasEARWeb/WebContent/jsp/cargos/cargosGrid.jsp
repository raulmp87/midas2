<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="50" sort="int"><s:text name="midas.cargos.gridDetalle.no_Cargo"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.cargos.gridDetalle.fechaCargo"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.cargos.gridDetalle.importeInicial"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.cargos.gridDetalle.importeCargo"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.cargos.gridDetalle.importeRestante"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name ="midas.cargos.gridDetalle.estatus"/></column>	
		<column id="" type="ro" width="100" sort="str" hidden="true"></column>	
	</head>
	<s:iterator value="listaCargos" var="gridListaCargos" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
<%-- 			<cell><![CDATA[${gridListaCargos.id}]]></cell> --%>
			<cell><![CDATA[${gridListaCargos.fechaCargoStr}]]></cell>
			<cell><![CDATA[${gridListaCargos.capitalInicial}]]></cell>
			<cell><![CDATA[${gridListaCargos.importeCargo}]]></cell>	
			<cell><![CDATA[${gridListaCargos.importeRestante}]]></cell>	
			<cell><![CDATA[${gridListaCargos.estatus.valor}]]></cell>
			<s:if test="estatus.valor == \"PENDIENTE\" || estatus.valor == \"CANCELADO\"">
				<cell>0</cell>					
			</s:if>
			<s:if test="estatus.valor == \"ACTIVO\"">
				<cell>1</cell>
			</s:if>
			<cell><s:url value="/img/b_printer.gif"/>^<s:text name="midas.boton.imprimir"/>^javascript:imprimirPagare(${gridPagares.id})^_self</cell>
			<cell></cell>					
		</row>
	</s:iterator>
</rows>