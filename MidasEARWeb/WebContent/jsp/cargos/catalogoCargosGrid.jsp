<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id" type="ro" width="120" sort="str" hidden="true"></column>
		<column id="" type="ro" width="120" sort="str"><s:text name="midas.cargos.gridDetalle.tipoCargo"/></column>
		<column id="" type="ro" width="120" sort="str"><s:text name="midas.cargos.gridDetalle.fechaAltaCargo"/></column>
		<column id="" type="ro" width="120" sort="int"><s:text name="midas.cargos.gridDetalle.numeroAgente"/></column>
		<column id="" type="ro" width="120" sort="str"><s:text name="midas.cargos.gridDetalle.nombreAgente"/></column>
		<column id="" type="ro" width="100" sort="double"><s:text name="midas.cargos.gridDetalle.importe"/></column>
		<column id="" type="ro" width="100" sort="int"><s:text name="midas.cargos.gridDetalle.NumCargos"/></column>
		<column id="" type="ro" width="120" sort="str"><s:text name="midas.cargos.gridDetalle.estatus"/></column>
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
	</head>
	
	<s:iterator value="listaFiltrado" var="catalogoCargos" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${catalogoCargos.id}]]></cell>
			<cell><![CDATA[${catalogoCargos.tipoCargo}]]></cell>
			<cell><![CDATA[${catalogoCargos.fechaString}]]></cell>
			<cell><![CDATA[${catalogoCargos.idAgente}]]></cell>
			<cell><![CDATA[${catalogoCargos.nombreCompleto}]]></cell>
			<cell><![CDATA[${catalogoCargos.importe}]]></cell>	
			<cell><![CDATA[${catalogoCargos.numCargos}]]></cell>	
			<cell><![CDATA[${catalogoCargos.estatus}]]></cell>	
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(pathDetalle, 2,{"cargos.id":${catalogoCargos.id},"idRegistro":${catalogoCargos.id},"idTipoOperacion":130})^_self</cell>					
			<s:if test="estatus !=  \"CANCELADO\"">
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(pathDetalle, 4,{"cargos.id":${catalogoCargos.id},"idRegistro":${catalogoCargos.id},"idTipoOperacion":130})^_self</cell>
			</s:if>
		</row>
	</s:iterator>

</rows>