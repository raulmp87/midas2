<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/cargos/cargosHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/cargos/cargosAgentes/listarFiltrado.action?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"cargosGrid", null,idField,'prestamosModal');
 });
	
</script>
<s:form action="listarFiltrado" id="cargosForm" name="cargosForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table  id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.cargos.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield name="filtrar.agente.id" id="" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>
				&nbsp;<s:text name="midas.cargos.tipoCargo" />
			</th>	
			<td>
				<s:select  name="filtrar.tipoCargo.id" id="" cssClass="cajaTextoM2 w150" 
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaTiposCargos" listKey="id" listValue="valor" disabled="#readOnly"/>
			</td>
			
			<th>
				&nbsp;<s:text name="midas.cargos.fechaAltaCargo" />
			</th>
			<td>
				<sj:datepicker name="filtrar.fechaAltaCargo" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.prestamosAnticipos.nombreAgente"/>
			</th>	
			<td>
				<s:textfield name="filtrar.agente.persona.nombreCompleto" cssClass="cajaTextoM2 w170"/>
			</td>
 			<th>
 				&nbsp;<s:text name="midas.fuerzaventa.negocio.clasificacionAgente" /><!-- midas.cargos.tipoAgente -->
 			</th>	
			<td>
				<s:select  name="filtrar.tipoAgente.id" id="" cssClass="cajaTextoM2 w130" list="catalogoTipoAgente" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="id" listValue="valor" disabled="#readOnly"/>
			</td>
			<th>
				<s:text name="midas.cargos.centroOperacion"/>
			</th>
			<td>
				<s:select  name="filtrar.centroOperacion.id" id="" cssClass="cajaTextoM2 w130 wide" list="listCentroOperacion" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="id" listValue="descripcion" disabled="#readOnly"/>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.cargos.gerencia"/>
			</th>
			<td>
				<s:select  name="filtrar.gerencia.id" id="" cssClass="cajaTextoM2 w130 wide" list="listGerencia" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="idGerencia" listValue="descripcion" disabled="#readOnly"/>				
			</td>
			<th>
				<s:text name="midas.cargos.promotoria"/>
			</th>
			<td>
				<s:select  name="filtrar.promotoria.id" id="" cssClass="cajaTextoM2 w130 wide" list="listPromotoria" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="idPromotoria" listValue="descripcion" disabled="#readOnly"/>				
			</td>
			<th>
				<s:text name="midas.cargos.ejecutivo"/>
			</th>
			<td>
				<s:select  name="filtrar.ejecutivo.id" id="" cssClass="cajaTextoM2 w130 wide" list="listEjecutivo" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="idEjecutivo" listValue="nombreCompleto" disabled="#readOnly"/>				
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.cargos.fechaInicioCargo" />
			</th>	
			<td>
				<sj:datepicker name="filtrar.fechaInicioCoBro" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 			<th>
 				&nbsp;<s:text name="midas.cargos.fechaFinCargo" />
 			</th>		   
 			<td>
 				<sj:datepicker name="filtrar.fechaFinCobro" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
			<td colspan="3" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoConfigCargosPath, 'cargosGrid', document.cargosForm,'${idField}','afianzadoraModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="cargosGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica('/MidasWeb/cargos/cargosAgentes/verDetalle.action',1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>