<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/emision/conductoresAdicionales/configConductoresAdicionales.js'/>"></script>
<script type="text/javascript">
	var URL_BUSCAR_POLIZA = '<s:url action="buscarPoliza" namespace="/emision/conductoresAdicionales"/>';
	var URL_GUARDAR_CONDUCTORES = '<s:url action="guardarConductoresAdicionales" namespace="/emision/conductoresAdicionales"/>';
</script>
<div id="contenido_conductores">
	<div class="titulo">
		<s:text name='midas.emision.conductoresAdicionales.titulo' />
	</div>
		<s:form id="filtrosForm">
			<div id="contenedorFiltros" style="width: 98%;text-align: center;">
	
				<table id="agregar" border="0" class="fixTabla" >
					<tbody>
						<tr>
							<td>
								<s:textfield cssClass="txtfield jQalphaextra jQrequired" cssStyle="width: 100px;"
									key="midas.emision.conductoresAdicionales.filtro.numeroPoliza"
									labelposition="top" 
									size="16"
									maxlength="16"
									placeholder="0000-00000000-00"
									onkeyup="javascript:validateFormat();"
									id="numeroPoliza" name="numeroPoliza" />							
							</td>
							<td>
								<s:textfield cssClass="txtfield jQnumeric jQrestrict" cssStyle="width: 80px;"
									key="midas.suscripcion.cotizacion.auto.inciso.numero.secuencia"
									labelposition="top" 
									size="10"
									maxlength="2"
									id="polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso" name="polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso" />								
							</td>						
							<td>
								<div class="btn_back w140" style="display: inline; float: right;">                    
									<a id="submit" href="javascript: void(0);" onclick="buscarPoliza();" class="icon_buscar">
										<s:text name="midas.boton.buscar" />
									</a>
								</div>
							</td>							
						</tr>						
					</tbody>
				</table>
			</div>
		</s:form>		
		<s:form id="configConductoresAdicionalesForm" method="post" action="/emision/conductoresAdicionales/guardarConductoresAdicionales.action">		
			<s:hidden name="polizaAutoSeycos.idPolizaAutoSeycos.idCotizacion" id="polizaAutoSeycos.idPolizaAutoSeycos.idCotizacion" />
			<s:hidden name="polizaAutoSeycos.idPolizaAutoSeycos.idVersionPoliza" id="polizaAutoSeycos.idPolizaAutoSeycos.idVersionPoliza" />
			<s:hidden name="polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso" id="polizaAutoSeycos.idPolizaAutoSeycos.numeroInciso" />
			<s:hidden name="polizaAutoSeycos.idPolizaAutoSeycos.lineaNegocio" id="polizaAutoSeycos.idPolizaAutoSeycos.lineaNegocio" />			
			<s:hidden name="numeroPoliza" id="polizaAutoSeycos.numeroPoliza" />
			
			<div id="contenedorDatosGenerales" style="width: 98%;text-align: center;">		
				<table id="agregar" border="0" class="fixTabla" >
					<thead>
						<tr>
							<td class="subtitulo align-left" colspan="3" ><s:text name="midas.emision.conductoresAdicionales.datosGenerales.titulo" /></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>
								<s:text name="midas.emision.conductoresAdicionales.filtro.numeroPoliza" />
							</th>
							<th>
								<s:text name="midas.emision.conductoresAdicionales.datosGenerales.fechaInicioVigencia" />							
							</th>
							<th>
								<s:text name="midas.emision.conductoresAdicionales.datosGenerales.fechaFinVigencia" />							
							</th>
						</tr>
						<tr>
							<td>							
								<s:property value="polizaAutoSeycos.numeroPoliza"/>
							</td>
							<td>
								<s:property value="polizaAutoSeycos.fechaInicioVigencia"/>
							</td>
							<td>
								<s:property value="polizaAutoSeycos.fechaFinVigencia"/>
							</td>
						</tr>
						<tr>
							<th>
								<s:text name="midas.emision.consulta.poliza.estatus" />							
							</th>
							<th>
								<s:text name="midas.poliza.nombrecontratante" />
							</th>
							<th>
								<s:text name="midas.emision.conductoresAdicionales.datosGenerales.conductorHabitual" />							
							</th>
						</tr>
						<tr>
							<td>
								<s:property value="polizaAutoSeycos.claveEstatus"/>
							</td>
							
							<td>
								<s:property value="polizaAutoSeycos.nombreContratante"/>
							</td>
							<td>
								<s:property value="polizaAutoSeycos.nombreConductorHabitual"/>
							</td>
						</tr>
						<tr>
							<th>
								<s:text name="midas.emision.consulta.poliza.tipo" />							
							</th>
							<th>
								<s:text name="midas.emision.consulta.vehiculo.paquete" />							
							</th>						
						</tr>
						<tr>
							<td>
								<s:property value="polizaAutoSeycos.descripcionTipoPoliza"/>
							</td>						
							
							<td>
								<s:property value="polizaAutoSeycos.paquete"/>
							</td>						
						</tr>
						<tr>
							<th>
								<s:text name="midas.emision.conductoresAdicionales.descripcionVehiculo" />							
							</th>
							<th>
								<s:text name="midas.poliza.numeroSerie" />							
							</th>	
							<th>
								<s:text name="midas.emision.consulta.vehiculo.motor" />							
							</th>							
						</tr>
						<tr>	
							<td>
								<s:property value="polizaAutoSeycos.descripcionVehiculo"/>
							</td>	
							<td>
								<s:property value="polizaAutoSeycos.numeroSerie"/>
							</td>						
							<td>
								<s:property value="polizaAutoSeycos.numeroMotor"/>
							</td>	
						</tr>
					</tbody>
				</table>
			</div>		
			<div id="contenedorConductores" style="width: 98%;text-align: center;">
				<table id="agregar" border="0" class="fixTabla" >
					<tr>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 300px;"
								onkeyup="javascript:this.value=this.value.toUpperCase();"
								key="midas.emision.conductoresAdicionales.conductor1"
								labelposition="left" 
								size="30"
								maxlength="80"								
								id="polizaAutoSeycos.nombreConductorAdicional1" name="polizaAutoSeycos.nombreConductorAdicional1" />	
						</td>
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 300px;"
								onkeyup="javascript:this.value=this.value.toUpperCase();"
								key="midas.emision.conductoresAdicionales.conductor2"
								labelposition="left" 
								size="30"
								maxlength="80"								
								id="polizaAutoSeycos.nombreConductorAdicional2" name="polizaAutoSeycos.nombreConductorAdicional2" />
						</td>
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 300px;"
								onkeyup="javascript:this.value=this.value.toUpperCase();"
								key="midas.emision.conductoresAdicionales.conductor3"
								labelposition="left" 
								size="30"
								maxlength="80"								
								id="polizaAutoSeycos.nombreConductorAdicional3" name="polizaAutoSeycos.nombreConductorAdicional3" />
						</td>
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 300px;"
								onkeyup="javascript:this.value=this.value.toUpperCase();"
								key="midas.emision.conductoresAdicionales.conductor4"
								labelposition="left"
								size="30"
								maxlength="80"
								id="polizaAutoSeycos.nombreConductorAdicional4" name="polizaAutoSeycos.nombreConductorAdicional4" />
						</td>
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 300px;"
								onkeyup="javascript:this.value=this.value.toUpperCase();"
								key="midas.emision.conductoresAdicionales.conductor5"
								labelposition="left"
								size="30"
								maxlength="80"
								id="polizaAutoSeycos.nombreConductorAdicional5" name="polizaAutoSeycos.nombreConductorAdicional5" />
						</td>
					</tr>
					<tr>
						<td>
							<div align="right" class="inline" >
								<div class="btn_back w110">
									<a href="javascript: void(0);" class="icon_guardar"
										onclick="guardarConductoresAdicionales()">
										<s:text name="midas.boton.guardar"/>
									</a>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</s:form>
</div>
<style type="text/css">
table#agregar td {
   
    width: 75%;
}
</style>