	<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@taglib prefix="sj" uri="/struts-jquery-tags" %><%@taglib prefix="s" uri="/struts-tags" %>

	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/emision/ejecutivoColoca/ejecutivoColoca.js'/>"></script>
	

<div style="width: 98%;">
	<s:form action="listar" id="ejecutivoColocaForm" introButon="submit">
		<div class="titulo" style="width: 98%;">
			<s:text name="Mantenimiento de Ejecutivo coloca"/>
		</div>
	
	<!-- Filtros Inputs -->

	<div style=" width: 45%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/ padding-top:1%;">
		<div style="width: 100%;float: left;/*border: 1px solid rgba(0, 128, 0, 0.39);*//*background-color: rgba(115, 213, 74, 0.52);*/ ">
        	<b style="padding-left: 1%;">
				<s:text name="Introduzca los datos de la poliza o endoso a buscar" />
        	</b>
    	</div>

		<div style="width: 53%; padding-left: 2%;/* border: 1px solid rgba(0, 128, 0, 0.39); */float: left; padding-top:1%;">
      	  	<div style="/*width: 48%; float: left; */padding: 1%; margin-top:2%;">
      		   	<s:text name="Numero de poliza Midas" />        
			   	<sj:textfield name="npolmidas" id="npolmidas" cssStyle="width: 80%; border: 1px solid #a0e0a0;"></sj:textfield>
           
		  	</div>
        	<div style="/*width: 48%; float: left; */padding: 1%;">  
        		<s:text name="Numero de poliza Seycos" />      
				<sj:textfield name="npolseycos" id="npolseycos" cssStyle="width: 80%; border: 1px solid #a0e0a0;"></sj:textfield>    
			</div>
		 	<div style="/*width: 48%; float: left; */padding: 1%;">  
        		<s:text name="Numero de Endoso" />      
				<sj:textfield name="nendoso" id="nendoso" cssStyle="width: 80%; border: 1px solid #a0e0a0;"></sj:textfield>  
			</div>
			<div style="/* width: 95%; */margin: 1%;float: left;padding-left: 1%;     margin-top: 5%;">
			<div class="btn_back w140" style="display: inline; float: right;">                    
							<a id="submit" href="javascript: void(0);" onclick="limpiarCampos();" class="icon_aceptar" title="Limpiar campos">
								<s:text name="Limpiar"/>
							</a>
						</div>
				
			</div>
		</div>

	</div>
	
	
	<div style=" width: 45%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/">
        <div id="tipoarchi"  style="width: 96%; padding-top: 3%; float: left; padding-left: 3%;"> 
			<div style="width: 53%; padding-left: 2%;/* border: 1px solid rgba(0, 128, 0, 0.39); */float: left;">
				 <div style="/*width: 48%; float: left; */padding: 1%;">  
		 			<div style="/* width: 40%; */margin-left: 1%;">
						<sj:datepicker name="fechaInicial" cssStyle="width: 80%; border: 1px solid #a0e0a0;"
						key="midas.reporteSesas.fechaInicial"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaInicial"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" ></sj:datepicker>
					</div>
					<div style="/* width: 40%; */margin-left: 1%;">
						<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 80%; border: 1px solid #a0e0a0;"
						key="midas.reporteSesas.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
					</div> 
					<div style="/* width: 40%; */padding-top: 2%; margin-left: 1%;">
 						<s:text name="Sucursal (Agente)"  />      
						<sj:select id="agentes" name="agentes" cssStyle="width: 80%; border: 1px solid #a0e0a0;" ></sj:select>
					</div>
					
					<div style="/* width: 40%; */padding-top: 2%; margin-left: 1%;">
					<div style="/* width: 40%; */ margin-bottom: 2%;">
 						<s:text name="Ramo"  /> 
 						</div>     
						<select id="ramo" name="ramo" style="width: 77%; border: 1px solid #a0e0a0; margin-left:2%;" >
						<option value>Seleccione</option>
						<option value="A">Autos</option>
						<option value="V">Vida</option>
						<option value="D">Daños</option>
						
						</select>
					</div>
					<div style="/* width: 95%; */margin: 1%;float: left;padding-left: 1%;     margin-top: 5%;">
						<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="submit" href="javascript: void(0);" onclick="buscarPoliza();" class="icon_buscar" title="Buscar Poliza">
						<s:text name="Buscar"/>
					</a>
				</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			<s:hidden id="idRegAgente" value="" name="idRegAgente"></s:hidden>
	</s:form>

	<div id="contenedorFiltros"  style=" width: 98%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/">
			<div id="indicador" style=" width: 98%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/"></div>
			<div id="ejecutivoColocaGrid" style="width: 96%;  height: 130px; cursor: default; margin-left: 2%; padding-top: 2%; border-style: none;"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
</div>
<script type="text/javascript">
getAllAgentes(); 
	iniciarGrid();
	
</script>