<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>


<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			 <call command="enablePaging">
				<param>true</param>
				<param>70</param>
				<param>0</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="poliza" type="ro" width="*"><s:text name="#Poliza" /></column>
		<column id="endoso" type="ro" width="*"><s:text name="#Endoso" /></column>
		<column id="suc" type="ro" width="*"><s:text name="#Sucursal" /></column>
		<column id="nomSuc" type="ro" width="*"><s:text name="Nombre Sucursal" /></column> 
		<column id="producto" type="ro" width="*"><s:text name="Producto" /></column>
		<column id="ejecutivo" type="ro" width="*"><s:text name="Ejecutivo" /></column>
		<column id="acciones" type="link" width="*"><s:text name="Acciones" /></column>
</head>
	
 	<s:iterator value="listaPolizasEjecutivoColoca" status="row">
		<row>
		<cell><s:property value="(numeroPolizaSeycos!=null&&numeroPolizaSeycos!='')?numeroPolizaSeycos:numeroPolizaMidas" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="sucursal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreSucursal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="producto" escapeHtml="false" escapeXml="true"/></cell> 
			<cell><s:property value="ejecutivo" escapeHtml="false" escapeXml="true"/></cell> 
			<cell>Asignar^javascript: asignarEjecutivo("<s:property value="sucursal" escapeHtml="false" escapeXml="true"/>", 
			"<s:property value="numeroPolizaMidas" escapeHtml="false" escapeXml="true"/>","<s:property value="numeroPolizaSeycos" escapeHtml="false" escapeXml="true"/>",
			"<s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/>",
			"<s:property value="nombreSucursal" escapeHtml="false" escapeXml="true"/>", "<s:property value="producto" escapeHtml="false" escapeXml="true"/>",
			"<s:property value="ejecutivo" escapeHtml="false" escapeXml="true"/>","<s:property value="claveNeg" escapeHtml="false" escapeXml="true"/>",
			"<s:property value="esBancaSeguros" escapeHtml="false" escapeXml="true"/>");^_self</cell>
		</row>
	</s:iterator> 

</rows>

