	<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@taglib prefix="sj" uri="/struts-jquery-tags" %><%@taglib prefix="s" uri="/struts-tags" %>

<script type="text/javascript">
	(function() {
		var nomSuc = '<s:property value="polizaEditar.nombreSucursal"/>';
		console.log("polizaEditar.nombreSucursal=" + nomSuc);	
		var claveNeg = '<s:property value="polizaEditar.claveNeg"/>';
		console.log("polizaEditar.claveNeg=" + claveNeg);	
	})();
</script>


<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
		
		
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

 <style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	font-size: 11px;
	font-family: arial;
	}
</style>
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/emision/ejecutivoColoca/ejecutivoColoca.js'/>"></script>
	

<div style="width: 98%;">
	<s:form action="savePolizaEjecutivoColoca" id="ejecutivoColocaForm" name="ejecutivoColocaForm" introButon="submit" namespace="/emision/ejecutivoColoca">
		<div class="titulo" style="width: 98%;">
		<%-- 	<s:text name="Edicion de Ejecutivo coloca"/> --%>
		</div>
		<div style="/*width: 48%; float: left; */padding: 2%; /*margin-top:2%;*/"  class="labelBlack" >
      		   	<s:text name="Sucursal: "/><s:property value="polizaEditar.nombreSucursal"/>          
		</div>
		<div style="/*width: 48%; float: left; */padding: 2%; /*margin-top:2%;*/"  class="labelBlack" >
      		   	<s:text name="Producto: "/><s:property value="polizaEditar.producto"/>           
		</div>
		<div style="/*width: 48%; float: left; */padding: 2%;/*margin-top:2%;*/"  class="labelBlack" >
      		   	<s:text name="Poliza: " /><s:property value="(polizaEditar.numeroPolizaSeycos!=null&&polizaEditar.numeroPolizaSeycos!='')?polizaEditar.numeroPolizaSeycos:polizaEditar.numeroPolizaMidas" />           
		</div>
		<div style="/*width: 48%; float: left; */padding: 2%; /*margin-top:2%;*/"  class="labelBlack" >
      		   	<s:text name="Endoso: " /><s:property value="polizaEditar.numeroEndoso"/>           
		</div>
		<div style="/* width: 40%; */padding: 2%; /*margin-top:2%;*/"  class="labelBlack" >
 				<s:text name="Ejecutivo Coloca: " />     
 				<div style="/* width: 40%; */padding-top: 1%; /*margin-top:2%;*/"  class="labelBlack" > 
				<s:select id="ejecutivos" list="listaEjecutivos"  listKey="userFullName"  listValue="userFullName" name="polizaEditar.ejecutivo" cssStyle="width: 80%; border: 1px solid #a0e0a0; " ></s:select>					 
				</div> 
		</div> 
		 <s:if test="%{banderaRol}">
		<div id="ejecutivoColoca" style="/* width: 40%; */padding: 2%; /*margin-top:2%;*/"  class="labelBlack" > 
        		<s:text name="Ejecutivo Coloca Text" />      
				<sj:textfield name="ejecutivoAsig" id="ejecutivoAsig" cssStyle="width: 80%; border: 1px solid #a0e0a0;" value="%{polizaEditar.ejecutivo}">
				</sj:textfield>    
			</div>
			</s:if>	
		<div style="width: 90%;  margin-top:3%;">
		<div class="btn_back w140" style="display: inline; float: right; width: 40%; ">                    
		<%-- <a id="submit" href="javascript: void(0);" onclick="guardarPolizaEjecutivo('<s:property value="(polizaEditar.numeroPolizaSeycos!=null&&polizaEditar.numeroPolizaSeycos!='')?polizaEditar.numeroPolizaSeycos:polizaEditar.numeroPolizaMidas" />','<s:property value="polizaEditar.ejecutivo"/>'  );" class="icon_aceptar" title="Guardar">
			<s:text name="Guardar"/>
		</a> --%>
		<a id="submit" href="javascript: void(0);" onclick="guardarPolizaEjecutivo('<s:property value="polizaEditar.claveNeg" />','<s:property value="polizaEditar.idcotizacion"/>' ,'<s:property value="polizaEditar.idPoliza"/>' ,'<s:property value="polizaEditar.ejecutivo"/>',
		'<s:property value="(polizaEditar.numeroPolizaSeycos!=null&&polizaEditar.numeroPolizaSeycos!='')?polizaEditar.numeroPolizaSeycos:polizaEditar.numeroPolizaMidas" />' ,'<s:property value="polizaEditar.numeroEndoso"/>','<s:property value="polizaEditar.identificador"/>');" class="icon_aceptar" title="Guardar">
			<s:text name="Guardar"/>
		</a>
		</div>
		<div class="btn_back w140" style="display: inline; float: right; width: 40%; ">                    
		<a id="submit" href="javascript: void(0);" onclick="cerrarVentanaPolizaEjecutivo();" class="icon_cancelar" title="Cancelar">
			<s:text name="Cancelar"/>
		</a>
		</div>
		</div> 
		
	
	</s:form>

	
</div>