<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
		<column id="idBatch" type="ro" width="100" sort="int" align="center" ><s:text name="midas.vida.asistencia.listado.id"/></column>
		<column id="fRecepcion" type="ro" width="120" sort="date_custom" align="center"><s:text name="midas.vida.asistencia.listado.frecepcion"/></column>
		<column id="estatus" type="ro" width="120" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.estatus"/></column>
		<column id="usuario" type="ro" width="120" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.usuario"/></column>
		<column id="recibidos" type="ro" width="120" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.recibidos"/></column>
		<column id="aceptados" type="ro" width="120" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.aceptados"/></column>
		<column id="rechazados" type="ro" width="120" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.rechazados"/></column>
		<column id="archivo" type="ro" width="200" sort="str" align="center"><s:text name="midas.vida.asistencia.listado.archivo"/></column>
		<column id="acciones" type="img" width="40" sort="na" align="center"></column>
		<column id="acciones" type="img" width="40" sort="na" align="center"></column>
		<column id="acciones" type="img" width="*" sort="na" align="center"></column>
		
		
	</head>
	
	
	<s:iterator value="listaEnvios" var="c" status="index">
		<row id="${index.count}">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="getFechaRecepcion()" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="getEstatusDetalle()" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioRegistro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cantRecibidos" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cantAceptados" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cantRechazados" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="getNombreArchivo()" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:url value="/img/icons/ico_guardar.gif"/>^<s:text name="midas.boton.descarga"/>^javascript:exportarEnvioProveedorDetalle(${c.id})^_self</cell>
			<cell><s:url value="/img/b_enviar.jpg"/>^<s:text name="midas.boton.enviar"/>^javascript:enviarProveedor(${c.id})^_self</cell>

		</row>
	</s:iterator>
	
</rows>