<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.centro{
	margin-left: 5%;
}

</style>

<script src="<s:url value='/js/emision/vida/asistencia/asistenciaVida.js'/>"></script>

<div id="spacer1" style="height: 10px"></div>
<div align="center">

		<s:form  id="contenedorForm" name="contenedorForm">
			<div >
			<table id="filtros" cellpadding="2" cellspacing="1" >
				<tr>
					<td colspan="6">
						<div class="titulo" align="left">
						<s:text name="midas.vida.asistencia.titulo" />
						</div>
					</td>
				</tr>
				<tr>
					<th width="20px">				
						<s:text name="midas.vida.asistencia.filtro.id"/>:
					</th>	
					<td width="50px" class="centro">
						<s:textfield  id="idBatch" name="filtroEnvioProveedor.idBatch" cssClass="jQnumeric jQrestrict cajaTextoM2 w50"
						maxlength="5;"/>
					</td>
					
					<td  align="right"> 
					<sj:datepicker name="filtroEnvioProveedor.fechaRecepcionDesde"
					id="fechaRecepcionDesde" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2" 
					label="Desde"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
					</td>
							
					<td  align="right">
					<sj:datepicker name="filtroEnvioProveedor.fechaRecepcionHasta"
					id="fechaRecepcionHasta" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2" 
					label="Hasta"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
					
					<th width="50px">				
						<s:text  name="midas.vida.asistencia.filtro.estatus"/>:
					</th>	
					<td width="50px">
					    <s:select list="estatus" name="filtroEnvioProveedor.estatus" headerKey="" headerValue="%{getText('midas.general.seleccione)}"  id="estatus" cssClass="cajaTextoM2 w100" onchange=""/>
					</td>

				</tr>
				
				<tr>		
				<td colspan="6">		
					<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="buscarEnvioProveedor();" >
									 <s:text name="midas.boton.buscar" /> </a>
								</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
									<a href="javascript: void(0);" onclick="limpiarPantalla();"> 
									<s:text name="midas.boton.limpiar" /> </a>
								</div>	
								
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		
			</table>
			<br>
			</div>
		</s:form>
	
	<div class="titulo" align="left">
		<s:text name="midas.vida.asistencia.listado.titulo"/>
	</div>
	
	   
	<div id="envioProveedorGrid"  style="height:320px; width: 99%;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	
	<div id="spacer1" style="height: 10px"></div>
	
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w160" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarEnvioProveedorDetalleCons();">
					<s:text name="midas.boton.exportarDetalleExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Detalle a Excel' title='Exportar Detalle a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarEnvioProveedor();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
		</td>
	</tr>
</table>	
	
	
	<script type="text/javascript">
	    blockPage();
		iniContenedorEnvioProveedorGrid();
		unblockPage();
	</script>
	

	
</div>
