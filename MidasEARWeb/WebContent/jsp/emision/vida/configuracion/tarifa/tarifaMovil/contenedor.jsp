<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/emision/vida/configuracion/tarifa/tarifaMovil/tarifaMovilVidaHeader.jsp"></s:include>
<script type="text/javascript">
	jQuery(function() {
		dhx_init_tabbars();
		//incializarTabs();
	});
	function cargarExcelTarifaMovilVida(){
	 	var archivotxt = jQuery("#formularioTarifaMovilVida #fileUpload").val();
		if(archivotxt.indexOf(".csv")!=-1) {
			document.formularioTarifaMovilVida.submit();
		}
		else if(archivotxt.indexOf(".txt")!=-1) {
			document.formularioTarifaMovilVida.submit();
		}else{
			parent.mostrarMensajeInformativo("Seleccione un archivo .csv o .txt separado por pipes  | ","10");
		}
	 }
</script>
<div select="<s:property value='tabActiva'/>"  hrefmode="ajax-html"
	style="height: 380px; width: 910px; margin-left: 10px;"
	id="configuracionNegocioTabBar" class="dhtmlxTabBar"
	imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">
	
	<div width="100%" id="tarifa_movil_vida" name="Configurar"
		href="http://void" extraAction="javascript: mostrarContenedor();">
	</div>
	<div width="100%" id="info_general_carga_masiva" name="Cargar Masiva Tarifa Móvil"
		href="http://void">	
		<s:form  method="post" action="/tarifa/configuracion/vida/cargaMasiva.action" target="iframeProcessData" enctype="multipart/form-data" 
		id="formularioTarifaMovilVida" name="formularioTarifaMovilVida"> <!-- target="&quot;cargaMasivaAgente.jsp&quot;" -->
			<table width="90%" align="center"  class="contenedorFormas no-border"><!--  -->
				<tr>
					<td>
						<s:file id="fileUpload" name="fileUpload" cssClass="cajaTextoM2 w300 jQrequired" label="Carga Masiva Tarifa Movil Vida"/>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btn_back w120">
							<a href="javascript:cargarExcelTarifaMovilVida()" class="icon_guardar ." 
								onclick="">
								<s:text name="Cargar Archivo"/>
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="midas.agentes.cargaMasiva.resultadoCarga"/><br>
						<iframe name="iframeProcessData"  height="100px" width="400px" style="border: 1; border-color: #A0E0A0;"></iframe>
					</td>
				</tr>
			</table>
		</s:form>
	</div>
	<div width="100%" id="descuento_agente_movil" name="Descuentos Agentes"
		href="http://void" extraAction="javascript: verCatalogoDescuentoAgenteMovil();">
	</div>	
</div>