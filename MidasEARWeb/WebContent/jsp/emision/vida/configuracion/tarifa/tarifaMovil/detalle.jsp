<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
jQIsRequired();
	var ventanaCentroOperacion=null;
	var ventanaResponsable=null;
	function mostrarModalCentroOperacion(){
		var url="/MidasWeb/fuerzaventa/centrooperacion/mostrarContenedor.action?tipoAccion=consulta&idField=idCentroOperacion";
		sendRequestWindow(null, url, obtenerVentanaCentroOperacion);
	}
	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaCentroOperacion(){
		var wins = obtenerContenedorVentanas();
		ventanaCentroOperacion= wins.createWindow("centroOperacionModal", 400, 320, 930, 450);
		ventanaCentroOperacion.center();
		ventanaCentroOperacion.setModal(true);
		ventanaCentroOperacion.setText("Consulta de centros de operaci\u00F3n");
		ventanaCentroOperacion.button("park").hide();
		ventanaCentroOperacion.button("minmax1").hide();
		return ventanaCentroOperacion;
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdCentroOperacion(idCentroOperacion){
		var url="/MidasWeb/fuerzaventa/centrooperacion/findById.action";
		var data={"centroOperacion.id":idCentroOperacion};
		jQuery.asyncPostJSON(url,data,populateCentroOperacion);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populateCentroOperacion(json){
		if(json){
			var centroOperacion=json.centroOperacion;
			var idCentroOperacion=centroOperacion.id;
			jQuery("#idCentroOperacion").val(idCentroOperacion);
			var nombre=centroOperacion.descripcion;
			jQuery("#descripcionCentroOperacion").val(nombre);	
		}
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			jQuery("#idPersonaResponsable").val(idResponsable);
			jQuery("#nombreResponsable").val(nombre);
		}
	}
	function guardarTarifaMovilVida(){
		var path ="/MidasWeb/tarifa/configuracion/vida/guardar.action?"
			+ jQuery("#gerenciaForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	function eliminarTarifaMovilVida(){
		var path ="/MidasWeb/tarifa/configuracion/vida/eliminar.action?"
			+ jQuery("#gerenciaForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	function salirDeGerencia(){
		var url = "/MidasWeb/tarifa/configuracion/vida/mostrarContenedorPrincipal.action";
		if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}else{
			if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
				sendRequestJQAsync(null, url, targetWorkArea, null);
			}
		}
	}
</script>
<s:include value="/jsp/catalogos/fuerzaventa/gerencia/gerenciaHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarTarifaMovilVidaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.agregar.titulo')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.vida.tarifamovilvida.tarifaVida')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.vida.tarifamovilvida.eliminar.titulo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.vida.tarifamovilvida.editar.titulo')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.vida.tarifamovilvida.tarifaVida')}"/>
</s:else>
<s:form id="gerenciaForm">
<s:hidden name="tarifasMovilVida.fechaRegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="#titulo"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.catalogos.tarifa.descuentoagentemovil.datosDescuento"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.id" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.idTarifaMovilVida"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.edadMin" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.edadMinima"  readonly="#readOnly" id="txtEdadMinima" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.edadMax" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.edadMaxima"  readonly="#readOnly" id="txtEdadMaxima" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.sa" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.sumaAsegurada"  readonly="#readOnly" id="txtSumaAsegurada" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
	  <th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.tarifaBasica" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.tarifaBasica"  readonly="#readOnly" id="txttarifaBasico" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.tarifaPlatino" /></th>
			<td colspan="3"><s:textfield name="tarifasMovilVida.tarifaPlatino"  readonly="#readOnly" id="txttarifaPlatino" cssClass="cajaTextoM2 w250 jQrequired" maxlength="30"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.situacion" /></th>
			<td colspan="3">
				<s:select name="tarifasMovilVida.bajalogica" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'1':'ACTIVO', '0':'INACTIVO'}"/>
			</td>
		</tr>
	 </table>
	<s:if test="tarifasMovilVida.idTarifaMovilVida != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
		    <s:if test="tipoAccion != 5">
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>	    
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2">
			</td>
		</tr>
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeGerencia()"><!-- javascript: mostrarCatalogoGenerico(mostrarGerenciaPath);" class="icon_regresar -->
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarTarifaMovilVida();"><!-- if(validateAll(true)){realizarOperacionGenerica(guardarTarifaMovilVidaPath, document.gerenciaForm, null,false);} -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>	
					<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
							onclick="eliminarTarifaMovilVida();">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>	
					</s:if>
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
