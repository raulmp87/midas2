<%@ taglib prefix="s" uri="/struts-tags" %>
	
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script> 
<script type="text/javascript">
	var mostrarContenedorPath = '<s:url action="mostrarContenedor" namespace="/tarifa/configuracion/vida"/>';
	var mostrarBusquedaResponsablePath = '<s:url action="mostrarBusquedaResponsable" namespace="/tarifa/configuracion/vida"/>';	
	var verDetalleTarifaMovilVidaPath = '<s:url action="verDetalle" namespace="/tarifa/configuracion/vida"/>';
	var guardarTarifaMovilVidaPath = '<s:url action="guardar" namespace="/tarifa/configuracion/vida"/>';
	var eliminarTarifaMovilVidaPath = '<s:url action="eliminar" namespace="/tarifa/configuracion/vida"/>';
	var listarTarifaMovilVidaPath = '<s:url action="lista" namespace="/tarifa/configuracion/vida"/>';
	var listarFiltradoGerenciaPath = '<s:url action="listarFiltrado" namespace="/tarifa/configuracion/vida"/>';
	//var verCatalogoDescuentoAgenteMovilPath = '<s:url action="verCatalogoDescuentoAgenteMovil" namespace="/tarifa/descuentoAgenteMovil"/>';

	var agenteGrid, agenteCarteraClientesGrid;
	var ventanaAfianzadora=null;
	var tabInfo_general=null;
	var tabDomicilios=null;
	var tabDatosFiscales=null;
	var tabDatosContables=null;
	var tabDatosExtra=null;
	var tabDocumentos=null;
	var tabCarteraClientes=null;
	var contenedorTab=null;
	var grid=null;
	var gridEntretenimientosAgente;
	var gridHijosAgente;
	var TIPOACCION_CONSULTAHISTORICO = "5";
	/**
	 * Funcion para inicializar tabs, si es una alta, deshabilita el resto de los tabs hasta que se guarde la primer pestania
	 */
	function cargarExcelTarifasVida(){
		var archivotxt = jQuery("#fileUpload").val();
		if(archivotxt.indexOf(".xls")!=-1) {
			document.formularioCargaTarifasCotizacionMovilVida.submit();
		}
		else{
			parent.mostrarMensajeInformativo("Seleccione un archivo .xls ","10");
		}
	}
	function descargaExcelTarifasVida(){
		var estado = jQuery("#estadoTarifaMovil").val();
		if(estado!="") {
			document.tarifaDescargaMovilForm.submit();
		}
		else{
			parent.mostrarMensajeInformativo("Seleccione un Estado","10");
		}
	}
	function incializarTabs(){
		var idAgente=jQuery("#agente\\.id").val();
		contenedorTab=window["configuracionNegocioTabBar"];
		//Nombre de todos los tabs
		var tabs=["info_general","domicilios","datosFiscales","datosContables"];
		//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
		//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
		for(var i=1;i<tabs.length;i++){
			var tabName=tabs[i];
			contenedorTab.disableTab(tabName);
		}
		for(var i=0;i<tabs.length;i++){
			var tabName=tabs[i];
			var catalogoProveniente = jQuery("#moduloOrigen").val();
			var tab=contenedorTab[tabName];
			jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
				if(dwr.util.getValue("tipoAccion")==1||dwr.util.getValue("tipoAccion")==4){
					event.stopPropagation();
					parent.mostrarMensajeConfirm("Est� a punto de abandonar la secci�n, �Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}
				else{
					obtenerFuncionTab(event.data.value);
				}
			});
		}
	}
	
	function verInfoGeneralVida() {
			sendRequestJQ(null,verDetalleTarifaMovilVidaPath,
					'contenido_info_general','limpiarDivsGeneral();');			
	}
	
	function mostrarContenedor(){
		sendRequestJQ(null,mostrarContenedorPath,
				'tarifa_movil_vida','limpiarDivsGeneral();');	
	}
	
		function verCatalogoDescuentoAgenteMovil(){
			var path ="/MidasWeb/tarifa/descuentoAgenteMovil/verCatalogoDescuentoAgenteMovil.action?claveNegocio=V";
		sendRequestJQ(null,path,
				'contenido_descuento_agente_movil','limpiarDivsGeneral();');	
	}
	
	function limpiarDivsGeneral() {
		limpiarDiv('contenido_domicilios');
		limpiarDiv('contenido_datosContables');
		limpiarDiv('contenido_datosExtra');
		limpiarDiv('contenido_documentos');
		limpiarDiv('contenido_carteraClientes');
	}
	function obtenerFuncionTab(tab){
		var tipoOperac=50;
	//	if(jQuery("#moduloOrigen").val()==1){
	//		tipoOperac=120;
	//	}
	  if(tab==0){
	        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
	  }else if(tab==1){
	        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
	  }else if(tab==2){
	       sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
	  }else if(tab==3){
	        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
	  }else{
	        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
	  }
	}

</script>
