<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/emision/vida/configuracion/tarifa/tarifaMovil/tarifaMovilVidaHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"tarifaMovilVidaGrid", null,idField,'gerenciaModal');
	 });
</script>
		<s:form action="listarFiltrado" id="gerenciaCatalogoForm">
			<!--Parametro de la forma para que sea reutilizable -->
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.tarifaVida"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Tarifa Vida" />
					</th>
					<td width="275px">
						<s:textfield name="descuentosAgente.claveagente" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoGerenciaPath, 'tarifaMovilVidaGrid',document.gerenciaCatalogoForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<br>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="tarifaMovilVidaGrid" class="w880 h200" style="overflow:hidden"></div>	
				
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		
		<s:if test="tipoAccion!=\"consulta\"">
		
		<div class ="w880" align="right">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar ." 
					onclick="operacionGenerica(verDetalleTarifaMovilVidaPath,1);">
					<s:text name="midas.boton.agregar"/>
				</a>
			</div>	
		</div>
		</s:if>		

