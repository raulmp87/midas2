<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/emision/ppct/ordenPagoHeader.jsp" />
<s:include value="/jsp/catalogos/catalogosHeader.jsp" />
<head>
	<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>
<s:form id="listaOP" action="listar" introButon="submit" >
	<div class="titulo" style="width: 98%;">
		<s:text name="M�dulo de Pagos a Proveedores de Coberturas a Terceros" />
	</div>
	<div class="subtituloIzquierdaDiv" style="width: 98%;">
		<s:text name="Ordenes de Pago" />
	</div>
	<!-- Filtos -->
	<div id="divFiltrosOP"style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.emision.auto.ppct.folio"
					labelposition="top" 
					size="10"
					onkeypress="return soloNumerosM2(this, event, false)"
					onblur="this.value=jQuery.trim(this.value)"
					id="idOrdenPago" name="ordenPago.id" 
				/>
			</td>
			<td>
				<s:select
					key="midas.emision.auto.ppct.estatus"					
				    labelposition="top"
					id="claveEstatus" name="ordenPago.solicitudCheque.status" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					listKey="clave" listValue="descripcion" 
					list="listStatus"
					cssClass="txtfield" 
				/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select
					key="midas.emision.auto.ppct.proveedor"					
				    labelposition="top"
					id="idProveedor" name="ordenPago.proveedor.id" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					listKey="id" listValue="nombre" 
					list="listProveedores"
					cssClass="txtfield"
				/>
				<s:hidden id="claveNegocio" name="ordenPago.claveNegocio" />
			</td>
		</tr>
		<tr>
			<td><sj:datepicker required="false" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.fechaCorteDesde"
					labelposition="top" 					
					buttonImage="../img/b_calendario.gif"
				    id="fechainicial"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    yearRange="-2y:+1y"
					displayFormat="MM yy"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('mm/yyyy', 'date').attach(this)"
					onchange="jQuery('#fechaInicialH').val(jQuery.datepicker.formatDate('dd/mm/yy', jQuery.datepicker.parseDate( 'dd/MM yy', '01/'+this.value))); validaFechas('fechaInicialH','fechaFinalH',false);"
				/>
				<s:hidden id="fechaInicialH" name="ordenPago.rangoFechas.fechaInicio" />
			</td>
			<td>
				<sj:datepicker  required="false" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.hasta"
					labelposition="top" 					
					buttonImage="../img/b_calendario.gif"
				    id="fechaFinal"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    yearRange="-2y:+1y"
					displayFormat="MM yy"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('mm/yyyy', 'date').attach(this)"
					onchange="jQuery('#fechaFinalH').val(jQuery.datepicker.formatDate('dd/mm/yy', jQuery.datepicker.parseDate( 'dd/MM yy', '01/'+this.value))); validaFechas('fechaInicialH','fechaFinalH', false);"
				/>
				<s:hidden id="fechaFinalH" name="ordenPago.rangoFechas.fechaFin" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				
				<midas:boton onclick="javascript: loadGrid();"  tipo="buscar" key="midas.boton.buscar" style="display: inline; float: right; margin-left: 5px;"/>
					
				<div class="btn_back w70" style="display: inline; float: right;">
					<a id="limpiarOP" href="javascript: void(0);" class="icon_limpiar">
						<s:text name="midas.boton.limpiar" />
					</a>
				</div>
			</td>
		</tr>
	</table>
</div>
<!-- / Filtros -->
<br/>
<br/>
<div class="btn_back w100" style="display: inline; float: left;">                    
	<a id="filtrosOP" href="javascript: void(0);">
		<s:text name="midas.emision.auto.ppct.ocultarFiltros" />
	</a>
</div>
<div class="btn_back w140" style="display: inline; float: left;">
	<a id="agregarOP" href="javascript: void(0);">
		<s:text name="midas.emision.auto.ppct.agregarOP" />
	</a>
</div>
		
	<!-- <div id ="consultaPagoProvedorGrid" style="width:97%;height:247px"></div>  -->
	
	<s:action name="scrollableGrid" var="scrollableGrid" executeResult="true" namespace="/componente/scrollable" ignoreContextParams="true">
		<s:param name="name">ordenPagoGrid</s:param>
		<s:param name="form">listaOP</s:param>
		<s:param name="loadAction"><s:url action="buscar" namespace="/emision/ppct"/></s:param>
		<s:param name="gridColumnsPath">/jsp/emision/ppct/grid/columnasOrdenPagoGrid.jsp</s:param>
		<s:param name="gridRowPath">/jsp/emision/ppct/grid/registroOrdenPagoGrid.jsp</s:param>
	</s:action>
	
</s:form>
