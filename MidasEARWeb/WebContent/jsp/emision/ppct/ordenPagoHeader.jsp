<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript">
	var urlViewOP = '<s:url action="mostrar" namespace="/emision/ppct"/>';
	var urlSearchOP = '<s:url action="buscar" namespace="/emision/ppct"/>';
	var urlSearchRecibo = "";
	var urlSelectedOP = '<s:url action="seleccionar" namespace="/emision/ppct"/>';
	var urlAddOP = '<s:url action="agregar" namespace="/emision/ppct"/>';
	var urlViewDetail = '<s:url action="verDetalle" namespace="/emision/ppct/ordenpago"/>';
	var urlCancelOP = '<s:url action="cancelar" namespace="/emision/ppct/ordenpago"/>';
	var ventanaAgregar = 'divAddOP';
	var ventanaEditar = 'divEditOP';
	var datos="";
</script>
<script src="<s:url value="/js/midas2/emision/ppct/pagoproveedor.js"/>" type="text/javascript"></script>
