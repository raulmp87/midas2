<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/emision/ppct/detalle/detalleOrdenPagoHeader.jsp" />
<s:include value="/jsp/catalogos/catalogosHeader.jsp" />
<script src="<s:url value="/js/midas2/emision/ppct/pagoproveedor.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	var urlBackOP = '<s:url action="regresar" namespace="/emision/ppct/ordenpago"/>';
	var urlSaveOP = '<s:url action="guardar" namespace="/emision/ppct/ordenpago"/>';
	var urlSearchRecibo = '<s:url action="buscar" namespace="/emision/ppct/detalle"/>';
	var urlSearchOP = "";
	var urlDesAsignarRecibo = '<s:url action="seleccionar" namespace="/emision/ppct/detalle"/>';
	var ventanaAgregar = 'divAddOP';
	var ventanaEditar = 'divEditOP';
	datos = jQuery('#formDetalleOP').serialize();
	jQuery(document).ready(function(){
		<s:if test="ordenPago.id==null">
			redimensionarVentana(ventanaAgregar,980,550,false);
		</s:if>
		<s:else>
			<s:if test="historico">
				redimensionarVentana(ventanaEditar+'<s:property value="ordenPago.id"/>',980,550,false);
			</s:if>
			loadGrid();
		</s:else>		
	});
	
</script>
<s:form id="formDetalleOP" action="guardar">
	<s:if test ="ordenPago.id==null">
		<div class="subtituloIzquierdaDiv" style="width: 98%;">
			<s:text name="Paso 2 de 2" />
		</div>
		<br/>
		<br/>
	</s:if>
	<div id="divDatosOP">
		<table class="marco">
			<s:if test ="ordenPago.id!=null">
				<tr>
					<td>
						<s:text name="midas.emision.auto.ppct.folio"/>:
						<s:property value="ordenPago.id"/>
						<s:hidden name="ordenPago.id"/>
					</td>
					<td>
						<s:text name="midas.emision.auto.ppct.estatus"/>:
						<s:if test="ordenPago.solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@PENDIENTE">
		    				<s:text name="midas.emision.auto.ppct.ordenpago.status.pendiente"/>
						</s:if>
						<s:elseif test="ordenPago.solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@SOLICITADO">
							    <s:text name="midas.emision.auto.ppct.ordenpago.status.solicitado"/>
						</s:elseif>
						<s:elseif test="ordenPago.solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@TERMINADO">
							    <s:text name="midas.emision.auto.ppct.ordenpago.status.terminado"/>
						</s:elseif>
						<s:elseif test="ordenPago.solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@CANCELADO">
							    <s:text name="midas.emision.auto.ppct.ordenpago.status.cancelado"/>
						</s:elseif>
						<s:hidden name="ordenPago.solicitudCheque.status"/></td>
					<td></td>
				</tr>
			</s:if>
			<tr>
				<td>
					<s:text name="midas.emision.auto.ppct.proveedor"/>:
					<s:property value="ordenPago.proveedor.nombre"/>
					<s:hidden name="ordenPago.proveedor.id" id="proveedorId"/>
				</td>
				<td>
					<div id="totalOP">
					<s:text name="midas.emision.auto.ppct.total"/>: $
						<s:if test ="ordenPago.id==null"><s:property value="ordenPago.importePreeliminar"/></s:if>
						<s:else><s:property value="ordenPago.solicitudCheque.importeNeto"/></s:else>
						</div>
					<s:hidden id="claveNegocio" name="ordenPago.claveNegocio" />
					<s:hidden id="importePreeliminar" name="ordenPago.importePreeliminar" />
				<td></td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.emision.auto.ppct.fechaCorte"/>:
					<s:property value="ordenPago.fechaCorte"/>
					<s:hidden name="ordenPago.fechaCorte" id="fechaCorte"/>
					<s:hidden name="historico"/>
				</td>
				<td colspan="2">
					<s:if test ="ordenPago.id==null">
						<midas:boton onclick="confirmGuardaOP();" tipo="guardar" style="display: inline; float: right; margin-left: 5px;"/>
						
						<midas:boton onclick="javascript: regresarIniOP(false);" tipo="regresar" style="display: inline; float: right; margin-left: 5px;"/>

						<div class="btn_back w140" style="display: inline; float: right;">                    
							<a id="cancelAddOP" href="javascript: void(0);">
								<s:text name="midas.emision.auto.ppct.cancelar" />
							</a>
						</div>
					</s:if>
				</td>
			</tr>
		</table>
	</div>
	<br/>
		<s:include value="/jsp/emision/ppct/detalle/filtrosDetalle.jsp" />
	<br/>
	<br/>		
		
		<s:action name="scrollableGrid" var="scrollableGrid" executeResult="true" namespace="/componente/scrollable" ignoreContextParams="true">
			<s:param name="name">recibosGrid</s:param>
			<s:param name="form">formDetalleOP</s:param>
			<s:param name="loadAction"><s:url action="buscar" namespace="/emision/ppct/detalle"/></s:param>
			<s:param name="checkJs">seleccionarRecibo_chk</s:param>
			<s:param name="gridColumnsPath">/jsp/emision/ppct/grid/columnasReciboGrid.jsp</s:param>
			<s:param name="gridRowPath">/jsp/emision/ppct/grid/registroReciboGrid.jsp</s:param>
		</s:action>
		
</s:form>