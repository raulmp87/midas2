<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@  page contentType="text/xml" %>
<div id="divFiltrosRecibo" style="width: 98%;text-align: center;">
	<table class="marco" >
		<tr>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.emision.auto.ppct.numeroPoliza"
					labelposition="top" 
					size="10"
					onblur="this.value=jQuery.trim(this.value)"
					id="numeroPoliza" name="reciboProveedor.numeroPoliza" 
				/>
			</td>
			<td>
				<s:textfield
					cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.emision.auto.ppct.numeroEndoso"					
				    labelposition="top"
					size="10"
					onblur="this.value=jQuery.trim(this.value)"
					id="endoso" name="reciboProveedor.numeroEndoso" 
				/>		
			</td>
		</tr>
		<tr>
			<td colspan="2" align="left">
				<s:select
					key="midas.emision.auto.ppct.cobertura"
				    labelposition="top"
				    headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					list="listCoberturas"
				    listKey="id" listValue="descripcion"
					cssClass="txtfield"
					id="id" name="reciboProveedor.cobertura.id"
				/>
			</td>
		</tr>
		<tr>
			<td><sj:datepicker required="false" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.inicioVigencia"
					labelposition="top" 					
					buttonImage="../../../img/b_calendario.gif"
				    id="fechainicialRecibo" name="reciboProveedor.rangoFechas.fechaInicio"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    maxDate="+1y"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onchange="validaFechas(fechainicialRecibo, fechaFinalRecibo, true);"
				/>
			</td>
			<td>
				<sj:datepicker required="false" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.hasta"
					labelposition="top" 					
					buttonImage="../../../img/b_calendario.gif"
				    id="fechaFinalRecibo" name="reciboProveedor.rangoFechas.fechaFin"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    maxDate="+1y"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onchange="validaFechas(fechainicialRecibo, fechaFinalRecibo, true);"
				/>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
								
				<midas:boton onclick="javascript: loadGrid();"  tipo="buscar" key="midas.boton.buscar" style="display: inline; float: right; margin-left: 5px;"/>
				
				<div class="btn_back w90" style="display: inline; float: right;">  
					<a id="limpiarRecibo" href="javascript: void(0);" class="icon_limpiar">
						<s:text name="midas.boton.limpiar" />
					</a>
				</div>
			</td>
		</tr>
	</table>
</div>
<br/>
<div class="btn_back w140" style="display: inline; float: left">                    
	<a id="filtrosRecibo" href="javascript: void(0);">
		<s:text name="midas.emision.auto.ppct.ocultarFiltros" />
	</a>
</div>
<s:if test ="ordenPago.id!=null">
	<script type="text/javascript">
		displayFilters(jQuery("#filtrosRecibo"),'#divFiltrosRecibo');
	</script>
</s:if>