<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="folio" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="proveedor.nombre" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="fechaCorte" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="solicitudCheque.importeNeto" escapeHtml="false" escapeXml="true" /></cell>
<cell>
	<s:if test="solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@PENDIENTE">
		    <s:text name="midas.emision.auto.ppct.ordenpago.status.pendiente"/>
	</s:if>
	<s:elseif test="solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@SOLICITADO">
		    <s:text name="midas.emision.auto.ppct.ordenpago.status.solicitado"/>
	</s:elseif>
	<s:elseif test="solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@TERMINADO">
		    <s:text name="midas.emision.auto.ppct.ordenpago.status.terminado"/>
	</s:elseif>
	<s:elseif test="solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@CANCELADO">
		    <s:text name="midas.emision.auto.ppct.ordenpago.status.cancelado"/>
	</s:elseif>
</cell>
<cell>../img/icons/ico_editar.gif^Detalle^javascript: verDetalleOP(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
<s:if test="solicitudCheque.status == @mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO@PENDIENTE"><cell>../img/icons/ico_rechazar1.gif^Cancelar^javascript: cancelaOP(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell></s:if>