<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="cobertura.descripcion" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="reciboOriginal.serie" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="reciboOriginal.numeroFolio" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="monto" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="reciboOriginal.fechaInicioVigencia" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="reciboOriginal.fechaFinVigencia" escapeHtml="false" escapeXml="true" /></cell>
<s:if test="!historico"><cell><s:if test="seleccionado">1</s:if><s:else>0</s:else></cell></s:if>
