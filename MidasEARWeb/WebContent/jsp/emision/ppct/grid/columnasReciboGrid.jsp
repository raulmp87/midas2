<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<column id="id" type="ro" width="0" sort="na" hidden="true"></column>
<column id="numeroPoliza" type="ro" width="100" sort="server"><s:text name="midas.emision.auto.ppct.numeroPoliza"/></column>
<column id="numeroEndoso" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.numeroEndoso"/></column>
<column id="numeroInciso" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.inciso"/></column>
<column id="cobertura.descripcion" type="ro" width="200" sort="server"><s:text name="midas.emision.auto.ppct.cobertura"/></column>
<column id="reciboOriginal.serie" type="ro" width="60" sort="server"><s:text name="midas.emision.auto.ppct.serie"/></column>
<column id="reciboOriginal.numeroFolio" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.noFolio"/></column>
<column id="monto" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.monto"/></column>
<column id="reciboOriginal.fechaInicioVigencia" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.iniVigencia"/></column>
<column id="reciboOriginal.fechaFinVigencia" type="ro" width="80" sort="server"><s:text name="midas.emision.auto.ppct.finVigencia"/></column>
<s:if test="!historico"><column id="seleccionado" type="ch" width="80" align="center" sort="int"><s:text name="midas.emision.auto.ppct.excluir"/></column></s:if>