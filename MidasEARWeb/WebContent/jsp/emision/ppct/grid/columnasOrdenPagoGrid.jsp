<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<column id="id" type="ro" width="100" sort="na" hidden="true"></column>
<column id="folio" type="ro" width="100" sort="server"><s:text name="midas.emision.auto.ppct.folio"/></column>
<column id="proveedor.nombre" type="ro" width="380" sort="server"><s:text name="midas.emision.auto.ppct.proveedor"/></column>
<column id="fechaCorte" type="ro" width="200" sort="server"><s:text name="midas.emision.auto.ppct.fechaCorte"/></column>
<column id="solicitudCheque.importeNeto" type="ro" width="160" sort="na"><s:text name="midas.emision.auto.ppct.monto"/></column>
<column id="solicitudCheque.status" type="ro" width="200" sort="server"><s:text name="midas.emision.auto.ppct.estatus"/></column>
<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
<column id="accion1" type="img" width="30" sort="na"/>