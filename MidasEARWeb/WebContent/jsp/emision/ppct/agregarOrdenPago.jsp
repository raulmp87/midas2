<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>	
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/emision/ppct/detalle/detalleOrdenPagoHeader.jsp" />
<s:include value="/jsp/catalogos/catalogosHeader.jsp" />
<script src="<s:url value="/js/midas2/emision/ppct/pagoproveedor.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	var ventanaAgregar = 'divAddOP';
	var urlContinueOP = '<s:url action="continuar" namespace="/emision/ppct/ordenpago"/>';
	
	jQuery(document).ready(function(){
		redimensionarVentana(ventanaAgregar,980,240, true);
	});
</script>
<s:form id="formAddOP" action="continuar">
	<div class="subtituloIzquierdaDiv" style="width: 98%;">
		<s:text name="Paso 1 de 2" />
	</div>
	<br/>
	<br/>
	<div style="width: 98%;text-align: center;">
	<table id="desplegar" >
		<tr>
			<td>
				<s:select
					key="midas.emision.auto.ppct.proveedor"					
				    labelposition="top"
					id="ordenPago.proveedor.id" name="ordenPago.proveedor.id" 
					listKey="id" listValue="nombre" 
					list="listProveedores"
					cssClass="txtfield"
				/>
				<s:hidden id="claveNegocio" name="ordenPago.claveNegocio" />
			</td>
			<td>
			<s:if test="historico==null">
			<sj:datepicker required="true" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.fechaCorte"
					labelposition="top"
					buttonImage="../../img/b_calendario.gif"
				    id="fechaCorte" name = "fechaCortes"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    yearRange="-2y:+1y"
					displayFormat="MM yy"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onchange="jQuery('#fechaCorteH').val(jQuery.datepicker.formatDate('dd/mm/yy', jQuery.datepicker.parseDate( 'dd/MM yy', '01/'+this.value))); "
				/>
			</s:if>
			<s:if test="historico!=null">
				<sj:datepicker required="true" cssStyle="width: 170px;"
					key="midas.emision.auto.ppct.fechaCorte"
					labelposition="top"
					buttonImage="../../../img/b_calendario.gif"
				    id="fechaCorte" name="fechaCortes"
				    size="12"
				    changeMonth="true"
				    changeYear="true"
				    yearRange="-2y:+1y"
					displayFormat="MM yy"
					maxlength="10" cssClass="txtfield"
					onkeypress="return soloFecha(this, event, false);"
					onchange="jQuery('#fechaCorteH').val(jQuery.datepicker.formatDate('dd/mm/yy', jQuery.datepicker.parseDate( 'dd/MM yy', '01/'+this.value)));"
				/>
			</s:if>
				<s:hidden id="fechaCorteH" name="ordenPago.fechaCorte" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				
				<midas:boton onclick="javascript: agregarDetalle();" tipo="continuar" style="display: inline; float: right; margin-left: 5px;"/>
				
				<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="cancelOP" href="javascript: parent.cerrarVentanaModal(ventanaAgregar)">
						<s:text name="midas.emision.auto.ppct.cancelar" />
					</a>
				</div>
			</td>
		</tr>
	</table>
	</div>
	<br/>
	<br/>
</s:form>