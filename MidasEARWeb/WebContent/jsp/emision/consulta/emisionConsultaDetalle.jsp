<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

	<s:include value="detalle/emisionConsultaHidden.jsp"></s:include>
	<div id= "divDetalleEmisionConsulta">
		<s:include value="detalle/detalleConsulta.jsp"></s:include>
	</div>
	<s:if test="(seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@COBERTURA)
				&& (seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@COBRANZA)">
		<br>
		<div id="divBuscarBtn" style="float: right;" class="w150">
			<div class="btn_back w140">
				<a href="javascript: void(0);" onclick="buscarConsultaEmision();"> <s:text
						name="midas.boton.buscar" />
				</a>
			</div>
		</div>
		<div id="divLimpiarBtn" style="float: right;" class="w150">
			<div class="btn_back w140">
				<a href="javascript: void(0);" onclick="limpiarConsultaEmision();"> <s:text
						name="midas.boton.limpiar" />
				</a>
			</div>
		</div>
		<s:if test="
(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@AGENTE && consultaEmision.detalleAgente.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@CLIENTE && consultaEmision.detalleCliente.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@POLIZA && consultaEmision.detallePoliza.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@VEHICULO && consultaEmision.detalleVehiculo.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ENDOSO && consultaEmision.detalleEndoso.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ANEXO && consultaEmision.detalleAnexo.ultimaSeleccion) 
||(seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@SINIESTRO && consultaEmision.detalleSiniestro.ultimaSeleccion)">
			<b><s:text name="midas.emision.consulta.ultimaseleccion"/></b>
		</s:if>
		
		<br><br><br><br>
	</s:if>
	<div id="espacioIndicador" align="left" style="width:97%;height:50px">
		<div id="indicador" align="left">
			<br/><img src="/MidasWeb/img/loading-green-circles.gif"/><font style="font-size:9px;">Procesando la informaci&oacute;n, espere un momento por favor...</font>
		</div>
	</div>
	<div id ="consultaEmisionGrid" style="width:97%;height:247px"></div>