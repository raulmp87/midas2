<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	var mostrarDetallePath = '<s:url action="mostrarDetalle" namespace="/emision/consulta"/>';
	var navegarPath = '<s:url action="navegar" namespace="/emision/consulta"/>';
	var seleccionarPath = '<s:url action="seleccionar" namespace="/emision/consulta"/>';
	var mostrarResumenPath = '<s:url action="mostrarResumen" namespace="/emision/consulta"/>';
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/emision/consulta/emisionConsulta.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.emision.consulta.titulo"/>
</div>
<s:form action="navegar" id="consultaEmisionForm">
	
	<s:hidden name="seccion" />
	<table border="0">
		<tr>
			<td>
				<div id="divResumenBtn" class="w150" align="right" >
					<div class="btn_back w140" align="right">
						<a href="javascript: void(0);" id="alternarResumen" onclick="alternarResumenConsulta();"> 
							<s:text name="midas.emision.consulta.resumen.mostrar" />
						</a>
					</div>
				</div>
			</td>
			<td>
				<div id="divLimpiarBtn" align="right" class="w170">
					<div class="btn_back w170" align="right">
						<a href="javascript: void(0);" onclick="limpiarConsultaEmisionGlobal();"> <s:text
								name="midas.emision.consulta.limpiar" />
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
	<div id="resumenConsulta" style="position: relative; height: 100px; width: 100%;"></div>
	
	<div style= "height: 420px; width:100% overflow:auto" hrefmode="ajax-html"  id="cotizaciontabbar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >		
			<div width="150px" id="AGENTE" name="Agente" extraAction="mostrarDetalleConsultaEmision('AGENTE');" row="1"></div>
			<div width="150px" id="CLIENTE" name="Cliente" extraAction="mostrarDetalleConsultaEmision('CLIENTE');" row="1"></div>
			<div width="150px" id="POLIZA" name="Poliza" extraAction="mostrarDetalleConsultaEmision('POLIZA');" row="1"></div>
			<div width="150px" id="ENDOSO" name="Endoso" extraAction="mostrarDetalleConsultaEmision('ENDOSO');" row="1"></div>
			<div width="150px" id="ANEXO" name="Anexo" extraAction="mostrarDetalleConsultaEmision('ANEXO');" row="1"></div>
			<div width="150px" id="VEHICULO" name="Vehiculo" extraAction="mostrarDetalleConsultaEmision('VEHICULO');" row="2"></div>
			<div width="150px" id="COBERTURA" name="Cobertura" extraAction="mostrarDetalleConsultaEmision('COBERTURA');" row="2"></div>
			<div width="150px" id="SINIESTRO" name="Siniestro" extraAction="mostrarDetalleConsultaEmision('SINIESTRO');" row="2"></div>
			<div width="150px" id="COBRANZA" name="Cobranza" extraAction="mostrarDetalleConsultaEmision('COBRANZA');" row="2"></div>
	</div>

</s:form>
<script type="text/javascript">
	alternarResumenConsulta();
</script>
