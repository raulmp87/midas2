<%@ taglib prefix="s" uri="/struts-tags"%>

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.cargos.gridDetalle.nombreAgente" />
		</td>
		<td colspan="3" style="text-align: left;">
			<s:property value="consultaEmision.detalleAgente.nombre"/>
		</td>
	</tr>
	<tr>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.poliza.numero" />
		</td>
		<td colspan="3" style="text-align: left;">
			<s:property value="consultaEmision.detallePoliza.numeroPoliza"/>
		</td>
	</tr>
	<tr>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.vehiculo.inciso" />
		</td>
		<td style="text-align: left;">
			<s:property value="consultaEmision.detalleVehiculo.numeroInciso"/>
		</td>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.vehiculo.marca" />
		</td>
		<td style="text-align: left;">
			<s:property value="consultaEmision.detalleVehiculo.marca"/>
		</td>
	</tr>
	<tr>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.vehiculo.tipo" />
		</td>
		<td style="text-align: left;">
			<s:property value="consultaEmision.detalleVehiculo.tipo"/>
		</td>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.vehiculo.modelo" />
		</td>
		<td style="text-align: left;">
			<s:property value="consultaEmision.detalleVehiculo.modelo"/>
		</td>
	</tr>
	<tr>
		<td style="font-weight: bold;text-align: left;">
			<s:text name="midas.emision.consulta.endoso.numero" />
		</td>
		<td colspan="3" style="text-align: left;">
			<s:property value="consultaEmision.detalleEndoso.numeroEndoso"/>
		</td>
	</tr>
</table>