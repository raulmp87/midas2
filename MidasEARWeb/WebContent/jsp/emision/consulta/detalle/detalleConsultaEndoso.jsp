<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:hidden name="consultaEmision.detalleEndoso.polizaId" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.numero"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleEndoso.numeroEndoso" 
								    name="consultaEmision.detalleEndoso.numeroEndoso" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleEndoso.numeroEndoso" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.tipo"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleEndoso.tipoEndoso" 
								    name="consultaEmision.detalleEndoso.tipoEndoso" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.motivo"
									cssClass="txtfield"
									labelposition="left" 
									size="100"
									maxlength="100"
								    id="consultaEmision.detalleEndoso.motivo" 
								    name="consultaEmision.detalleEndoso.motivo" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.estatus"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detalleEndoso.estatus" 
								    name="consultaEmision.detalleEndoso.estatus" />
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.endoso.fechaemision" />:
			</div>
			<sj:datepicker     
								   name="consultaEmision.detalleEndoso.fechaEmision"
		    					   id="consultaEmision.detalleEndoso.fechaEmision"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.endoso.fechainivigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detalleEndoso.fechaInicioVigencia"
		    					   id="consultaEmision.detalleEndoso.fechaInicioVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.endoso.fechafinvigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detalleEndoso.fechaFinVigencia"
		    					   id="consultaEmision.detalleEndoso.fechaFinVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.primaneta"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleEndoso.primaNeta" 
								    name="consultaEmision.detalleEndoso.primaNeta" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleEndoso.primaNeta" />
    	</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.derechos"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleEndoso.derechos" 
								    name="consultaEmision.detalleEndoso.derechos" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleEndoso.derechos" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.iva"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleEndoso.iva" 
								    name="consultaEmision.detalleEndoso.iva" 
								    disabled="true"/>
		   <s:hidden name="consultaEmision.detalleEndoso.iva" />
    	</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.recargo"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleEndoso.recargo" 
								    name="consultaEmision.detalleEndoso.recargo" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleEndoso.recargo" />
    	</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.endoso.primatotal"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleEndoso.primaTotal" 
								    name="consultaEmision.detalleEndoso.primaTotal" 
								    disabled="true"/>
	    	<s:hidden name="consultaEmision.detalleEndoso.primaTotal" />
		</td>
	</tr>

</table>