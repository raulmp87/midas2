<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@AGENTE">
    <s:hidden name="consultaEmision.detalleAgente.id" />
    <s:hidden name="consultaEmision.detalleAgente.nombre" />
    <s:hidden name="consultaEmision.detalleAgente.oficina" />
    <s:hidden name="consultaEmision.detalleAgente.gerencia" />
    <s:hidden name="consultaEmision.detalleAgente.supervision" />
    <s:hidden name="consultaEmision.detalleAgente.estatus" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@CLIENTE">
    <s:hidden name="consultaEmision.detalleCliente.id" />
    <s:hidden name="consultaEmision.detalleCliente.nombre" />
    <s:hidden name="consultaEmision.detalleCliente.apellidoPaterno" />
    <s:hidden name="consultaEmision.detalleCliente.apellidoMaterno" />
    <s:hidden name="consultaEmision.detalleCliente.tipoPersona" />
    <s:hidden name="consultaEmision.detalleCliente.domicilioId" />
    <s:hidden name="consultaEmision.detalleCliente.domicilio" />
    <s:hidden name="consultaEmision.detalleCliente.rfc" />
    <s:hidden name="consultaEmision.detalleCliente.estado" />
    <s:hidden name="consultaEmision.detalleCliente.ciudad" />
    <s:hidden name="consultaEmision.detalleCliente.codigoPostal" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@POLIZA">
    <s:hidden name="consultaEmision.detallePoliza.id" />
    <s:hidden name="consultaEmision.detallePoliza.agenteId" />
    <s:hidden name="consultaEmision.detallePoliza.clienteId" />
    <s:hidden name="consultaEmision.detallePoliza.numeroPoliza" />
    <s:hidden name="consultaEmision.detallePoliza.claveSeycos" />
    <s:hidden name="consultaEmision.detallePoliza.fechaCreacion" />
    <s:hidden name="consultaEmision.detallePoliza.estatus" />
    <s:hidden name="consultaEmision.detallePoliza.fechaInicioVigencia" />
    <s:hidden name="consultaEmision.detallePoliza.fechaFinVigencia" />
    <s:hidden name="consultaEmision.detallePoliza.tipo" />
    <s:hidden name="consultaEmision.detallePoliza.numeroCotizacion" />
    <s:hidden name="consultaEmision.detallePoliza.contratante" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@VEHICULO">
    <s:hidden name="consultaEmision.detalleVehiculo.numeroInciso" />
    <s:hidden name="consultaEmision.detalleVehiculo.polizaId" />
    <s:hidden name="consultaEmision.detalleVehiculo.agenteId" />
    <s:hidden name="consultaEmision.detalleVehiculo.clienteId" />
    <s:hidden name="consultaEmision.detalleVehiculo.marca" />
    <s:hidden name="consultaEmision.detalleVehiculo.tipo" />
    <s:hidden name="consultaEmision.detalleVehiculo.modelo" />
    <s:hidden name="consultaEmision.detalleVehiculo.motor" />
    <s:hidden name="consultaEmision.detalleVehiculo.serie" />
    <s:hidden name="consultaEmision.detalleVehiculo.placa" />
    <s:hidden name="consultaEmision.detalleVehiculo.paquete" />
    <s:hidden name="consultaEmision.detalleVehiculo.estatus" />
    <s:hidden name="consultaEmision.detalleVehiculo.fechaInicioVigencia" />
    <s:hidden name="consultaEmision.detalleVehiculo.fechaFinVigencia" />
    <s:hidden name="consultaEmision.detalleVehiculo.primaNeta" />
    <s:hidden name="consultaEmision.detalleVehiculo.nombreAsegurado" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ENDOSO">
    <s:hidden name="consultaEmision.detalleEndoso.numeroEndoso" />
    <s:hidden name="consultaEmision.detalleEndoso.polizaId" />
    <s:hidden name="consultaEmision.detalleEndoso.tipoEndoso" />
    <s:hidden name="consultaEmision.detalleEndoso.motivo" />
    <s:hidden name="consultaEmision.detalleEndoso.estatus" />
    <s:hidden name="consultaEmision.detalleEndoso.fechaEmision" />
    <s:hidden name="consultaEmision.detalleEndoso.fechaInicioVigencia" />
    <s:hidden name="consultaEmision.detalleEndoso.fechaFinVigencia" />
    <s:hidden name="consultaEmision.detalleEndoso.primaNeta" />
    <s:hidden name="consultaEmision.detalleEndoso.derechos" />
    <s:hidden name="consultaEmision.detalleEndoso.iva" />
    <s:hidden name="consultaEmision.detalleEndoso.recargo" />
    <s:hidden name="consultaEmision.detalleEndoso.primaTotal" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ANEXO">
    <s:hidden name="consultaEmision.detalleAnexo.id" />
    <s:hidden name="consultaEmision.detalleAnexo.polizaId" />
    <s:hidden name="consultaEmision.detalleAnexo.tipo" />
    <s:hidden name="consultaEmision.detalleAnexo.descripcion" />
</s:if>

<s:if test="seccion != @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@SINIESTRO">
    <s:hidden name="consultaEmision.detalleSiniestro.numeroSiniestro" />
    <s:hidden name="consultaEmision.detalleSiniestro.causa" />
    <s:hidden name="consultaEmision.detalleSiniestro.tipo" />
    <s:hidden name="consultaEmision.detalleSiniestro.fecha" />
    <s:hidden name="consultaEmision.detalleSiniestro.montoEstimadoInicial" />
    <s:hidden name="consultaEmision.detalleSiniestro.pagos" />
    <s:hidden name="consultaEmision.detalleSiniestro.recuperacion" />
    <s:hidden name="consultaEmision.detalleSiniestro.terceroInvolucrado" />
    <s:hidden name="consultaEmision.detalleSiniestro.tipoResponsabilidad" />
    <s:hidden name="consultaEmision.detalleSiniestro.coberturasAfectadas" />
    <s:hidden name="consultaEmision.detalleSiniestro.deducible" />
    <s:hidden name="consultaEmision.detalleSiniestro.companiaInvolucrada" />
    <s:hidden name="consultaEmision.detalleSiniestro.numeroInciso" />
    <s:hidden name="consultaEmision.detalleSiniestro.polizaId" />
    <s:hidden name="consultaEmision.detalleSiniestro.agenteId" />
</s:if>

<s:hidden name="consultaEmision.detalleCobertura.coberturaId" />
<s:hidden name="consultaEmision.detalleCobertura.numeroInciso" />
<s:hidden name="consultaEmision.detalleCobertura.polizaId" />
<s:hidden name="consultaEmision.detalleCobertura.nombre" />
<s:hidden name="consultaEmision.detalleCobertura.deducible" />
<s:hidden name="consultaEmision.detalleCobertura.sumaAsegurada" />
<s:hidden name="consultaEmision.detalleCobertura.primaNeta" />

<s:hidden name="consultaEmision.detalleCobranza.polizaId" />
<s:hidden name="consultaEmision.detalleCobranza.numeroEndoso" />
<s:hidden name="consultaEmision.detalleCobranza.numeroInciso" />
<s:hidden name="consultaEmision.detalleCobranza.serie" />
<s:hidden name="consultaEmision.detalleCobranza.numeroRecibo" />
<s:hidden name="consultaEmision.detalleCobranza.monto" />
<s:hidden name="consultaEmision.detalleCobranza.fechaInicioVigencia" />
<s:hidden name="consultaEmision.detalleCobranza.fechaFinVigencia" />
<s:hidden name="consultaEmision.detalleCobranza.estatus" />

<s:hidden name="consultaEmision.detalleAgente.ultimaSeleccion" />
<s:hidden name="consultaEmision.detalleCliente.ultimaSeleccion" />
<s:hidden name="consultaEmision.detallePoliza.ultimaSeleccion" />
<s:hidden name="consultaEmision.detalleVehiculo.ultimaSeleccion" />
<s:hidden name="consultaEmision.detalleEndoso.ultimaSeleccion" />
<s:hidden name="consultaEmision.detalleAnexo.ultimaSeleccion" />
<s:hidden name="consultaEmision.detalleSiniestro.ultimaSeleccion" />
