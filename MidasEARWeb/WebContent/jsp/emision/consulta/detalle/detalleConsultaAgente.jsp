<%@ taglib prefix="s" uri="/struts-tags"%>

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.clave"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleAgente.id" 
								    name="consultaEmision.detalleAgente.id" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.nombre"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleAgente.nombre" 
								    name="consultaEmision.detalleAgente.nombre" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.oficina"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleAgente.oficina" 
								    name="consultaEmision.detalleAgente.oficina" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.gerencia"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleAgente.gerencia" 
								    name="consultaEmision.detalleAgente.gerencia" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.supervision"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleAgente.supervision" 
								    name="consultaEmision.detalleAgente.supervision" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.agente.estatus"
									cssClass="txtfield"
									labelposition="left" 
									size="1"
									maxlength="1"
								    id="consultaEmision.detalleAgente.estatus" 
								    name="consultaEmision.detalleAgente.estatus"
								    disabled="true"/>
								    
		    <s:hidden name="consultaEmision.detalleAgente.estatus" />
								    
		</td>
	</tr>

</table>