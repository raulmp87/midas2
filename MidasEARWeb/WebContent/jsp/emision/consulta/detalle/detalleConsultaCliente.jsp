<%@ taglib prefix="s" uri="/struts-tags"%>

<s:hidden name="consultaEmision.detalleCliente.domicilioId" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.id"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.id" 
								    name="consultaEmision.detalleCliente.id" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.nombre"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.nombre" 
								    name="consultaEmision.detalleCliente.nombre" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.apellido.paterno"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.apellidoPaterno" 
								    name="consultaEmision.detalleCliente.apellidoPaterno" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.apellido.materno"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.apellidoMaterno" 
								    name="consultaEmision.detalleCliente.apellidoMaterno" />
		</td>
	</tr>
	<tr>
		<td>
			<s:radio list="#{'1':'Fisica','2':'Moral'}" 
				id="consultaEmision.detalleCliente.tipoPersona"
				name="consultaEmision.detalleCliente.tipoPersona" 
				onclick="javascript: void(0);" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.domicilio"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.domicilio" 
								    name="consultaEmision.detalleCliente.domicilio" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.rfc"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.rfc" 
								    name="consultaEmision.detalleCliente.rfc" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.estado"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.estado" 
								    name="consultaEmision.detalleCliente.estado" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.ciudad"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.ciudad" 
								    name="consultaEmision.detalleCliente.ciudad" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.cliente.cp"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleCliente.codigoPostal" 
								    name="consultaEmision.detalleCliente.codigoPostal" />
		</td>
	</tr>
	

</table>