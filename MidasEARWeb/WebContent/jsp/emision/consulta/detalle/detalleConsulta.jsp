<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@AGENTE">
	<s:include value="detalleConsultaAgente.jsp"></s:include>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@CLIENTE">
    <s:include value="detalleConsultaCliente.jsp"></s:include>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@POLIZA">
    <s:include value="detalleConsultaPoliza.jsp"></s:include>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@VEHICULO">
    <s:include value="detalleConsultaVehiculo.jsp"></s:include>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ENDOSO">
    <s:include value="detalleConsultaEndoso.jsp"></s:include>
    <table style="border: #6FD33D; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;"><tr><td><b>
    	<s:text name="midas.emision.consulta.validacion.seleccion.poliza"/>
    </b></td></tr></table>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@ANEXO">
    <s:include value="detalleConsultaAnexo.jsp"></s:include>
    <table style="border: #6FD33D; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;"><tr><td><b>
    	<s:text name="midas.emision.consulta.validacion.seleccion.poliza"/>
    </b></td></tr></table>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@SINIESTRO">
    <s:include value="detalleConsultaSiniestro.jsp"></s:include>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@COBERTURA">
    <table style="border: #6FD33D; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;"><tr><td><b>
    	<s:text name="midas.emision.consulta.validacion.seleccion.vehiculo"/>
    </b></td></tr></table>
</s:if>

<s:if test="seccion == @mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta@COBRANZA">
    <table style="border: #6FD33D; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;"><tr><td><b>
    	<s:text name="midas.emision.consulta.validacion.seleccion.poliza"/>
    </b></td></tr></table>
</s:if>