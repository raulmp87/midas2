<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:hidden name="consultaEmision.detallePoliza.id" />
<s:hidden name="consultaEmision.detallePoliza.agenteId" />
<s:hidden name="consultaEmision.detallePoliza.clienteId" />
<s:hidden name="consultaEmision.detallePoliza.contratante" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.poliza.numero"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.numeroPoliza" 
								    name="consultaEmision.detallePoliza.numeroPoliza" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.poliza.claveseycos"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.claveSeycos" 
								    name="consultaEmision.detallePoliza.claveSeycos" />
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.poliza.fechacreacion" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detallePoliza.fechaCreacion"
		    					   id="consultaEmision.detallePoliza.fechaCreacion"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
								    
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.poliza.estatus"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.estatus" 
								    name="consultaEmision.detallePoliza.estatus" />
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.poliza.fechainivigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detallePoliza.fechaInicioVigencia"
		    					   id="consultaEmision.detallePoliza.fechaInicioVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/> 
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.poliza.fechafinvigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detallePoliza.fechaFinVigencia"
		    					   id="consultaEmision.detallePoliza.fechaFinVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.poliza.tipo"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.tipo" 
								    name="consultaEmision.detallePoliza.tipo"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.poliza.numerocotizacion"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.numeroCotizacion" 
								    name="consultaEmision.detallePoliza.numeroCotizacion"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.folio.reexpedible.column"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detallePoliza.folio" 
								    name="consultaEmision.detallePoliza.folio"/>
		</td>
	</tr>
</table>