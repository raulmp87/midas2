<%@ taglib prefix="s" uri="/struts-tags"%>

<s:hidden name="consultaEmision.detalleAnexo.id" />
<s:hidden name="consultaEmision.detalleAnexo.polizaId" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.anexo.tipo"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleAnexo.tipo" 
								    name="consultaEmision.detalleAnexo.tipo" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleAnexo.tipo" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textarea key="midas.emision.consulta.anexo.descripcion"
									cssClass="txtfield"
									labelposition="left" 
									cols="100" 
									rows="25"
									id="consultaEmision.detalleAnexo.descripcion"
									name="consultaEmision.detalleAnexo.descripcion" 
									disabled="true"/>
			<s:hidden name="consultaEmision.detalleAnexo.descripcion" />
		</td>
	</tr>

</table>