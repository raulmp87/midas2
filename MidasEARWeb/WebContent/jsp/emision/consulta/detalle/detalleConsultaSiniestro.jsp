<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:hidden name="consultaEmision.detalleSiniestro.numeroInciso" />
<s:hidden name="consultaEmision.detalleSiniestro.polizaId" />
<s:hidden name="consultaEmision.detalleSiniestro.agenteId" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.numero"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detalleSiniestro.numeroSiniestro" 
								    name="consultaEmision.detalleSiniestro.numeroSiniestro" />
		</td>
	</tr>
	<tr>	
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.causa"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleSiniestro.causa" 
								    name="consultaEmision.detalleSiniestro.causa" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.tipo"
									cssClass="txtfield"
									labelposition="left" 
									size="40"
									maxlength="40"
								    id="consultaEmision.detalleSiniestro.tipo" 
								    name="consultaEmision.detalleSiniestro.tipo" />
		</td>
    </tr>
	<tr>		
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.siniestro.fecha" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detalleSiniestro.fecha"
		    					   id="consultaEmision.detalleSiniestro.fecha"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.estimacioninicial"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleSiniestro.montoEstimadoInicial" 
								    name="consultaEmision.detalleSiniestro.montoEstimadoInicial" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleSiniestro.montoEstimadoInicial" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.pagos"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleSiniestro.pagos" 
								    name="consultaEmision.detalleSiniestro.pagos" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleSiniestro.pagos" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.recuperacion"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleSiniestro.recuperacion" 
								    name="consultaEmision.detalleSiniestro.recuperacion" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleSiniestro.recuperacion" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.terceroinvolucrado"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleSiniestro.terceroInvolucrado" 
								    name="consultaEmision.detalleSiniestro.terceroInvolucrado" />
		</td>
	</tr>
	<tr>
		<td>
			<s:radio list="#{'1':'Afectado', 
							 '2':'Responsable'}" 
				id="consultaEmision.detalleSiniestro.tipoResponsabilidad"
				name="consultaEmision.detalleSiniestro.tipoResponsabilidad" 
				onclick="javascript: void(0);" 
				disabled="true"/>
		    <s:hidden name="consultaEmision.detalleSiniestro.tipoResponsabilidad" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.coberturaafectada"
									cssClass="txtfield"
									labelposition="left" 
									size="40"
									maxlength="40"
								    id="consultaEmision.detalleSiniestro.coberturasAfectadas" 
								    name="consultaEmision.detalleSiniestro.coberturasAfectadas" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.deducible"
									cssClass="txtfield"
									labelposition="left" 
									size="25"
									maxlength="25"
								    id="consultaEmision.detalleSiniestro.deducible" 
								    name="consultaEmision.detalleSiniestro.deducible" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleSiniestro.deducible" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.siniestro.ciainvolucrada"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleSiniestro.companiaInvolucrada" 
								    name="consultaEmision.detalleSiniestro.companiaInvolucrada" />
		</td>
	</tr>
	
</table>