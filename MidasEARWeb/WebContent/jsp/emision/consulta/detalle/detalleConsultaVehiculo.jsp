<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:hidden name="consultaEmision.detalleVehiculo.polizaId" />
<s:hidden name="consultaEmision.detalleVehiculo.agenteId" />
<s:hidden name="consultaEmision.detalleVehiculo.clienteId" />
<s:hidden name="consultaEmision.detalleVehiculo.primaNeta" />

<table style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.inciso"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleVehiculo.numeroInciso" 
								    name="consultaEmision.detalleVehiculo.numeroInciso" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleVehiculo.numeroInciso" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.marca"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleVehiculo.marca" 
								    name="consultaEmision.detalleVehiculo.marca" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.tipo"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleVehiculo.tipo" 
								    name="consultaEmision.detalleVehiculo.tipo" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.modelo"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleVehiculo.modelo" 
								    name="consultaEmision.detalleVehiculo.modelo" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.motor"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detalleVehiculo.motor" 
								    name="consultaEmision.detalleVehiculo.motor" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.serie"
									cssClass="txtfield"
									labelposition="left" 
									size="20"
									maxlength="20"
								    id="consultaEmision.detalleVehiculo.serie" 
								    name="consultaEmision.detalleVehiculo.serie"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.placa"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleVehiculo.placa" 
								    name="consultaEmision.detalleVehiculo.placa" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.paquete"
									cssClass="txtfield"
									labelposition="left" 
									size="50"
									maxlength="50"
								    id="consultaEmision.detalleVehiculo.paquete" 
								    name="consultaEmision.detalleVehiculo.paquete" />
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.estatus"
									cssClass="txtfield"
									labelposition="left" 
									size="15"
									maxlength="15"
								    id="consultaEmision.detalleVehiculo.estatus" 
								    name="consultaEmision.detalleVehiculo.estatus" 
								    disabled="true"/>
		    <s:hidden name="consultaEmision.detalleVehiculo.estatus" />
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.vehiculo.fechainivigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detalleVehiculo.fechaInicioVigencia"
		    					   id="consultaEmision.detalleVehiculo.fechaInicioVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<div style="color: #666666;font-weight: bold;text-align: left;">
				<s:text name="midas.emision.consulta.vehiculo.fechafinvigencia" />:
			</div>
			<sj:datepicker     
		    					   name="consultaEmision.detalleVehiculo.fechaFinVigencia"
		    					   id="consultaEmision.detalleVehiculo.fechaFinVigencia"  
		    					   cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
			/>
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield key="midas.emision.consulta.vehiculo.asegurado"
									cssClass="txtfield"
									labelposition="left" 
									size="150"
									maxlength="150"
								    id="consultaEmision.detalleVehiculo.nombreAsegurado" 
								    name="consultaEmision.detalleVehiculo.nombreAsegurado"/>
		</td>
	</tr>

</table>