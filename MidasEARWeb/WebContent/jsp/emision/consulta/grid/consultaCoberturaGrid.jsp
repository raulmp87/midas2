<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleCobertura.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleCobertura.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleCobertura.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleCobertura.coberturaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleCobertura.numeroInciso" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleCobertura.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleCobertura.nombre" type="ro" width="300" sort="server"><s:text name="midas.emision.consulta.cobertura.nombre"/></column>
			<column id="consultaEmision.detalleCobertura.deducible" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobertura.deducible"/></column>
			<column id="consultaEmision.detalleCobertura.sumaAsegurada" type="ro" width="110" sort="server"><s:text name="midas.emision.consulta.cobertura.limite"/></column>
			<column id="consultaEmision.detalleCobertura.primaNeta" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobertura.primaneta"/></column>	
		
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleCobertura.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="coberturaId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="deducible" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="sumaAsegurada" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="primaNeta" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>