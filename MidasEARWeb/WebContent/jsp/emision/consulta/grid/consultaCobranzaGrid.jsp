<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleCobranza.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleCobranza.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleCobranza.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleCobranza.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleCobranza.numeroEndoso" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.numeroendoso"/></column>
			<column id="consultaEmision.detalleCobranza.numeroInciso" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.numeroinciso"/></column>
			<column id="recibos" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.recibos"/></column>
			<column id="consultaEmision.detalleCobranza.serie" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.serie"/></column>
			<column id="consultaEmision.detalleCobranza.numeroRecibo" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.numerorecibo"/></column>	
			<column id="consultaEmision.detalleCobranza.monto" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.monto"/></column>
			<column id="consultaEmision.detalleCobranza.fechaInicioVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.fechainivigencia"/></column>
			<column id="consultaEmision.detalleCobranza.fechaFinVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.fechafinvigencia"/></column>
			<column id="consultaEmision.detalleCobranza.estatus" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.cobranza.estatus"/></column>	
			
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleCobranza.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true" /></cell>
			<cell>
				<s:if test="serie.substring(0, 1) == 1">
					<s:text name="midas.emision.consulta.cobranza.reciboinicial"/>
				</s:if>
				<s:else>
					<s:text name="midas.emision.consulta.cobranza.recibosubsecuente"/>
				</s:else>
			</cell>
			<cell><s:property value="serie" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroRecibo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="monto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>