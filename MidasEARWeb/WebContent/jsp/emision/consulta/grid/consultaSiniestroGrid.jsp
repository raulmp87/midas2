<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleSiniestro.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleSiniestro.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleSiniestro.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
			
			<column id="consultaEmision.detalleSiniestro.agenteId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.numeroSiniestro" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.siniestro.numero"/></column>
			<column id="consultaEmision.detalleSiniestro.fecha" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.siniestro.fecha"/></column>
			<column id="consultaEmision.detalleSiniestro.causa" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.tipo" type="ro" width="130" sort="server"><s:text name="midas.emision.consulta.siniestro.tipo"/></column>
			<column id="consultaEmision.detalleSiniestro.numeroInciso" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.siniestro.numeroinciso"/></column>	
			<column id="consultaEmision.detalleSiniestro.montoEstimadoInicial" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.siniestro.estimacioninicial"/></column>
			<column id="consultaEmision.detalleSiniestro.pagos" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.recuperacion" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.terceroInvolucrado" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.tipoResponsabilidad" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.coberturasAfectadas" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.deducible" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.companiaInvolucrada" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleSiniestro.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			
	
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleSiniestro.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="agenteId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fecha" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="causa" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="montoEstimadoInicial" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="pagos" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="recuperacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="terceroInvolucrado" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoResponsabilidad" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="coberturasAfectadas" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="deducible" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="companiaInvolucrada" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>	
    