<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleAnexo.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleAnexo.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleAnexo.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleAnexo.id" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleAnexo.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleAnexo.descripcion" type="ro" width="750" sort="server"><s:text name="midas.emision.consulta.anexo.descripcion"/></column>
			<column id="consultaEmision.detalleAnexo.tipo" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.anexo.tipo"/></column>
			
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleAnexo.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipo" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>