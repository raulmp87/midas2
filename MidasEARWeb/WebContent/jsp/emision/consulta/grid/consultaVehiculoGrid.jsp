<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleVehiculo.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleVehiculo.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleVehiculo.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
			
			<column id="consultaEmision.detalleVehiculo.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.agenteId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.clienteId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.numeroInciso" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.inciso"/></column>
			<column id="consultaEmision.detalleVehiculo.marca" type="ro" width="200" sort="server"><s:text name="midas.emision.consulta.vehiculo.marca"/></column>	
			<column id="consultaEmision.detalleVehiculo.tipo" type="ro" width="200" sort="server"><s:text name="midas.emision.consulta.vehiculo.tipo"/></column>
			<column id="consultaEmision.detalleVehiculo.modelo" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.modelo"/></column>
			<column id="consultaEmision.detalleVehiculo.motor" type="ro" width="90" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.serie" type="ro" width="90" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.fechaInicioVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.fechainivigencia"/></column>
			<column id="consultaEmision.detalleVehiculo.fechaFinVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.fechafinvigencia"/></column>
			<column id="consultaEmision.detalleVehiculo.estatus" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.estatus"/></column>
			<column id="consultaEmision.detalleVehiculo.paquete" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.paquete"/></column>	
			<column id="consultaEmision.detalleVehiculo.primaNeta" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.importe"/></column>
			<column id="consultaEmision.detalleVehiculo.nombreAsegurado" type="ro" width="150" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleVehiculo.placa" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.vehiculo.placa"/></column>
		
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleVehiculo.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agenteId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clienteId" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="marca" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="modelo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="motor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="serie" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="paquete" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNeta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAsegurado" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="placa" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>