<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleAgente.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleAgente.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleAgente.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleAgente.id" type="ro" width="60" sort="server"><s:text name="midas.emision.consulta.agente.clave"/></column>
			<column id="consultaEmision.detalleAgente.nombre" type="ro" width="260" sort="server"><s:text name="midas.emision.consulta.agente.nombre"/></column>
			<column id="consultaEmision.detalleAgente.oficina" type="ro" width="240" sort="server"><s:text name="midas.emision.consulta.agente.oficina"/></column>
			<column id="consultaEmision.detalleAgente.gerencia" type="ro" width="160" sort="server"><s:text name="midas.emision.consulta.agente.gerencia"/></column>
			<column id="consultaEmision.detalleAgente.supervision" type="ro" width="130" sort="server"><s:text name="midas.emision.consulta.agente.supervision"/></column>	
			<column id="consultaEmision.detalleAgente.estatus" type="ro" width="0" sort="server" hidden="true"></column>
	
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleAgente.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="oficina" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="gerencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="supervision" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>