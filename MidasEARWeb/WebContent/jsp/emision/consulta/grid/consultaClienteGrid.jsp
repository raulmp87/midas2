<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleCliente.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleCliente.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleCliente.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleCliente.id" type="ro" width="30" sort="server"><s:text name="midas.emision.consulta.cliente.id"/></column>
			<column id="consultaEmision.detalleCliente.nombre" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.nombre"/></column>
			<column id="consultaEmision.detalleCliente.apellidoPaterno" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.apellido.paterno"/></column>
			<column id="consultaEmision.detalleCliente.apellidoMaterno" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.apellido.materno"/></column>
			<column id="consultaEmision.detalleCliente.tipoPersona" type="ro" width="150" sort="server" hidden="true"></column>	
			<column id="consultaEmision.detalleCliente.domicilioId" type="ro" width="30" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleCliente.domicilio" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.domicilio"/></column>
			<column id="consultaEmision.detalleCliente.rfc" type="ro" width="150" sort="server" hidden="true"><s:text name="midas.emision.consulta.cliente.rfc"/></column>
			<column id="consultaEmision.detalleCliente.estado" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.estado"/></column>
			<column id="consultaEmision.detalleCliente.ciudad" type="ro" width="150" sort="server"><s:text name="midas.emision.consulta.cliente.ciudad"/></column>
			<column id="consultaEmision.detalleCliente.codigoPostal" type="ro" width="90" sort="server" hidden="true"><s:text name="midas.emision.consulta.cliente.cp"/></column>
	
		</head>
    </s:if>
	
	 <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleCliente.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="apellidoPaterno" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="apellidoMaterno" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoPersona" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="domicilioId" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="domicilio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="rfc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estado" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="ciudad" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoPostal" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>