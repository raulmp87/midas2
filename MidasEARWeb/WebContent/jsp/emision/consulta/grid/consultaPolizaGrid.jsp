<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detallePoliza.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detallePoliza.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detallePoliza.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detallePoliza.id" type="ro" width="30" sort="server" hidden="true"></column>
			<column id="consultaEmision.detallePoliza.agenteId" type="ro" width="30" sort="server" hidden="true"></column>
			<column id="consultaEmision.detallePoliza.clienteId" type="ro" width="150" sort="server" hidden="true"></column>
			<column id="consultaEmision.detallePoliza.numeroPoliza" type="ro" width="110" sort="server"><s:text name="midas.emision.consulta.poliza.numero"/></column>
			<column id="consultaEmision.detallePoliza.claveSeycos" type="ro" width="135" sort="server"><s:text name="midas.emision.consulta.poliza.claveseycos"/></column>	
			<column id="consultaEmision.detallePoliza.contratante" type="ro" width="190" sort="server"><s:text name="midas.emision.consulta.poliza.contratante"/></column>
			<column id="consultaEmision.detallePoliza.fechaCreacion" type="ro" width="118" sort="server"><s:text name="midas.emision.consulta.poliza.fechacreacion"/></column>
			<column id="consultaEmision.detallePoliza.estatus" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.poliza.estatus"/></column>
			<column id="consultaEmision.detallePoliza.fechaInicioVigencia" type="ro" width="118" sort="server"><s:text name="midas.emision.consulta.poliza.fechainivigencia"/></column>
			<column id="consultaEmision.detallePoliza.fechaFinVigencia" type="ro" width="118" sort="server"><s:text name="midas.emision.consulta.poliza.fechafinvigencia"/></column>	
			<column id="consultaEmision.detallePoliza.tipo" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.poliza.tipo"/></column>
			<column id="midas.folio.reexpedible.column" type="ro" width="118" sort="server"><s:text name="midas.folio.reexpedible.column"/></column>
			<column id="consultaEmision.detallePoliza.numeroCotizacion" type="ro" width="30" sort="server" hidden="true"></column>1
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detallePoliza.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agenteId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clienteId" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="claveSeycos" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="contratante" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value	="estatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="folio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroCotizacion" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
	
	
</rows>