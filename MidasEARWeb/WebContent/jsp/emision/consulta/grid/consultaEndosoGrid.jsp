<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="consultaEmision.detalleEndoso.totalRegistros" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="consultaEmision.detalleEndoso.primerRegistroACargar" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="consultaEmision.detalleEndoso.primerRegistroACargar == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
	
			<column id="consultaEmision.detalleEndoso.polizaId" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleEndoso.numeroEndoso" type="ro" width="50" sort="server"><s:text name="midas.emision.consulta.endoso.numero"/></column>
			<column id="consultaEmision.detalleEndoso.tipoEndoso" type="ro" width="220" sort="server"><s:text name="midas.emision.consulta.endoso.tipo"/></column>
			<column id="consultaEmision.detalleEndoso.motivo" type="ro" width="220" sort="server"><s:text name="midas.emision.consulta.endoso.motivo"/></column>
			<column id="consultaEmision.detalleEndoso.fechaInicioVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.endoso.fechainivigencia"/></column>	
			<column id="consultaEmision.detalleEndoso.fechaFinVigencia" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.endoso.fechafinvigencia"/></column>
			<column id="consultaEmision.detalleEndoso.estatus" type="ro" width="90" sort="server"><s:text name="midas.emision.consulta.endoso.estatus"/></column>
			<column id="consultaEmision.detalleEndoso.fechaEmision" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleEndoso.primaNeta" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleEndoso.derechos" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleEndoso.iva" type="ro" width="20" sort="server" hidden="true"></column>	
			<column id="consultaEmision.detalleEndoso.recargo" type="ro" width="20" sort="server" hidden="true"></column>
			<column id="consultaEmision.detalleEndoso.primaTotal" type="ro" width="20" sort="server" hidden="true"></column>
	
		</head>
    </s:if>
    
    <s:iterator value="listado" status="status">
		<row id="<s:property value="consultaEmision.detalleEndoso.primerRegistroACargar + #status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="polizaId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoEndoso" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="motivo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaEmision" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNeta" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="derechos" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="iva" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="recargo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="primaTotal" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>