<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		<column id="documentoAnexoSeccion.id" type="ro"  width="0"  sort="int" hidden="true">id</column>
		<column id="documentoAnexoSeccion.nombre" type="ro" width="220" sort="str"><s:text name="midas.documentos.referencias.archivo"/></column>
		<column id="documentoAnexoSeccion.claveObligatoriedad" type="coro" width="80" sort="str">
			<s:text name="midas.documentos.referencias.obligatorio"/>
			<option value="0"> OPCIONAL </option>
         	<option value="3"> OBLIGATORIO </option>
		</column>
		<column id="documentoAnexoSeccion.numeroSecuencia" type="ed" width="80" sort="str"><s:text name="midas.documentos.referencias.secuencia"/></column>
		<column id="documentoAnexoSeccion.descripcion" type="ed" width="300" sort="str"><s:text name="midas.documentos.referencias.descripcion"/></column>
		<column id="descargar" type="img" width="70" align="center"><s:text name="midas.documentos.referencias.descargar"/></column>
	</head>
	
	<s:iterator value="docAnexoList" var="c" status="row">	
		<row id="<s:property value="#row.index"/>">	
			<cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveObligatoriedad" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript:descargarDocumentosReferencias("<s:property value="id" escapeHtml="false" escapeXml="true"/>");^_self</cell>
		</row>
	</s:iterator>
</rows>