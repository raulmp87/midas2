<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>


<script type="text/javascript"
	src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="cotizacion.idToCotizacion" />

<div class="btn_back w100" id="botonEmision" style="float: right;">
	<s:hidden name="linkFortimax" id="linkFortimax" />

	<a href="javascript: loadFortimax();" onclick=""
	class="icon_guardar2"
	
	>  <s:text name="midas.clientes.documentacion.digitalizar"/> </a>
</div>
<s:form id="DocumentacionForm" action="guardarEntrevista">
<s:hidden name="cotizacion.idToCotizacion" />
<s:hidden name="cotizacion.idToPersonaContratante" />

<s:hidden name="polizaDTO.cotizacionDTO.idToCotizacion" />
<s:hidden name="polizaDTO.cotizacionDTO.idToPersonaContratante" />

<s:hidden name="polizaId" />

<s:hidden name="tipoEndoso" />
<s:hidden name="fechaIniVigenciaEndoso" />



<table>

	<tr>
		<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo4"/></td>
	</tr>
	<s:iterator value="documentacion" status="index" var="documento">

		
		 <s:set name="requerido" value="%{documentacion[%{#index.index}].requerido}" />
		
		<s:if test="%{#index.index==7}">
			<tr>
				<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo1"/></td>
			</tr>
		</s:if>
		<s:if test="%{#index.index==8}">
			<tr>
				<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo2"/></td>
			</tr>
		</s:if>
		<s:if test="%{#index.index==6}">
			<tr>
				<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo3"/></td>
			</tr>
		</s:if>
			<tr  >
			<td>
			<s:if test="#documento.seleccionado" >
			<s:checkbox
					name="documentacion[%{#index.index}].seleccionado" 
					disabled="true" 
					/>
			<s:hidden
					name="documentacion[%{#index.index}].seleccionado" 
					
					/>
				
			</s:if>
			
			<s:else  >
			<s:checkbox
					name="documentacion[%{#index.index}].seleccionado" 
					id = "checkBox[%{#index.index}].seleccionado" 
					onclick="javascript: checkBoxSeleccioneDocumento(%{#index.index})"
					/>
			</s:else>
				</td>
			<s:if test="#documento.requerido" >
			<td style="background-color: #f5f500">
			</s:if>
			<s:else  >
			<td>
			<s:hidden
					name="documentacion[%{#index.index}].descripcion "/>
			</s:else >
			<label class="label"><s:property  value="nombre" escape="true" escapeHtml="true" /></label>
				<s:hidden name="documentacion[%{#index.index}].nombre"
					id="documentacion[%{#index.index}].nombre" />
				<s:hidden name="documentacion[%{#index.index}].documentoId"
					id="documentacion[%{#index.index}].documentoId" />
					<s:hidden name="documentacion[%{#index.index}].requerido"
					id="documentacion[%{#index.index}].nombre" />
					</td>
					
					
			<td><s:textfield
					name="documentacion[%{#index.index}].descripcion "
					id="documentacion[%{#index.index}].descripcion" label="especifique" labelposition="left"
					disabled="true"
					maxlength="20" cssClass="cajaTextoM2 " />
			</td>
			<s:if test="%{#index.index==5}">
			<td><label class="label"><s:text name="Vigencia"/></label>
					</td>
			<td>
			
				<div class="elementoInline">
					<sj:datepicker name="documentacion[%{#index.index}].fechaVigencia"
						buttonImageOnly="true" label="" cssClass="cajaTextoM2"
						labelposition="left" buttonImage="../img/b_calendario.gif"
						changeMonth="true" changeYear="true" yearRange="-80:-18"
						maxDate="today" showOn="%{#showOnConsulta}"
						id="documentacion[%{#index.index}].fechaVigencia" maxlength="10"
						cssClass="w100 cajaTextoM2 jQdate-mx  "
						onkeypress="return soloFecha(this, event, true);"
						disabled="true"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				</div>
			</td></s:if>

		</tr>
	</s:iterator>
</table>
<s:if test="polizaId == null " >
<table align="right">
	<tr>

		<td>
			<div class="btn_back w100" id="botonEmision" style="float: right;">
				<a
					href="javascript: verComplementarCotizacion(<s:property value="cotizacion.idToCotizacion"/>)">
					<s:text name="midas.boton.cancelar"/> </a>
			</div>
		<td>


			<div class="btn_back w100" id="botonEmision" style="float: right;">
				<a href="javascript: verComplementarCotizacionDeDocumentacion()">
					<s:text name="midas.boton.guardar"/> </a>
			</div></td>
	</tr>
</table>
</s:if>
<s:if test="polizaId != null " >
<table align="right">
	<tr>

		<td>
			<div class="btn_back w100" id="botonEmision" style="float: right;">
				<a
					href="javascript: consultarCotizacion(${polizaId},${tipoEndoso}  ,'2','<s:property value='fechaIniVigenciaEndoso'/>' )">
					<s:text name="midas.boton.cancelar"/> </a>
			</div>
		<td>


			<div class="btn_back w100" id="botonEmision" style="float: right;">
				<a href="javascript: verComplementarCotizacionDeDocumentacionEndosos()">
					<s:text name="midas.boton.guardar"/> </a>
			</div></td>
	</tr>
</table>
</s:if></s:form>

