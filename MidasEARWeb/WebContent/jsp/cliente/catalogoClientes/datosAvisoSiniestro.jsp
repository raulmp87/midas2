<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
		});
	</script>
<!--/head-->

<body>
		<!-- 
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="285"/>
		-->
		<div>Datos de Contacto en caso de un siniestro</div>
		<div id="gridAvisoSiniestro" style="width:680px;height:190px">
		</div>
		<br/>
		<table width="100%" align="center" id="agregar">
		<tbody>
			<tr>
				<td width="49%">
					<s:textfield name="cliente.parentescoAviso" id="cliente.parentescoAviso" label="Parentesco" labelposition="left"/>			
				</td>
				<td width="49%">
					<s:textfield name="cliente.nombreAviso" id="cliente.nombreAviso" label="Nombre" labelposition="left"/>
				</td>
			</tr>
			<tr>
				<td width="49%">
					<s:textfield name="cliente.telefonoCasaAviso" id="cliente.telefonoCasaAviso" label="Tel�fono Casa" labelposition="left"/>			
				</td>
				<td width="49%">
					<s:textfield name="cliente.celularAviso" id="cliente.celularAviso" label="Celular" labelposition="left"/>
				</td>
			</tr>
			<tr>
				<td width="49%">
					<s:textfield name="cliente.telefonoOficinaAviso" id="cliente.telefonoOficinaAviso" label="Tel�fono Oficina" labelposition="left"/>
				</td>
				<td width="49%">
					<s:textfield name="cliente.extensionAviso" id="cliente.extensionAviso" label="Extensi�n" labelposition="left"/>
				</td>
			</tr>
			<tr>
				<td width="49%">
					<s:textfield name="cliente.correoElectronicoAviso" id="cliente.correoElectronicoAviso" label="Correo electr�nico" labelposition="left"/>
				</td>
				<td width="49%">
					<div style="float:right;">
						<input type="button" id="btnGuardarContactoAviso" onclick="guardarContactoAviso();"/>
					</div>
				</td>
			</tr>
		</tbody>
		</table>
		<div>Lista de contactos en caso de un siniestro</div>
		<div id="gridContactosAviso">
		</div>
</body>
</html>