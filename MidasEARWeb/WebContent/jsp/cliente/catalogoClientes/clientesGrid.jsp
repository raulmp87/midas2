<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:set id="idDiv" value="%{divCarga}" />
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea_Clientes</param> 
				<param>true</param>
				<param>infoArea_Clientes</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
		<column id="idCliente" type="ro" width="100" sort="int" >Id Cliente</column>
		<!--
		<column id="idDomicilio" type="ro" width="0" sort="int" >idDomicilio</column>
		 -->
		<column id="nombreCompleto" type="ro" width="220" sort="str">Nombre</column>
		<column id="codigoRFC" type="ro" width="120" sort="str">RFC</column>
		<column id="email" type="ro" width="160" sort="str">Email</column>
		<column id="curp" type="ro" width="120" sort="str">CURP</column>
		<column id="telefono" type="ro" width="100" sort="str"><s:text name="midas.catalogos.telefono"/></column>
		<column id="direccion" type="ro" width="250" sort="str">Direccion</column>
		<column id="codigoPostal" type="ro" width="60" sort="str">Codigo Postal</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="80" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="40" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaClientes" var="c" status="index">
	<row id="${index.count}">
		<cell><![CDATA[<s:property value="idCliente" escapeHtml="false" escapeXml="true"/>]]></cell>
		<!-- 
		<cell><![CDATA[<s:property value="idDomicilioConsulta" escapeHtml="false" escapeXml="true"/>]]></cell>
		 -->
		<cell><![CDATA[<s:property value="nombreCompleto" escapeHtml="false" escapeXml="true"/>]]></cell>
		<cell><![CDATA[<s:property value="codigoRFC" escapeHtml="false" escapeXml="true"/>]]></cell>
		<cell><![CDATA[<s:property value="email" escapeHtml="false" escapeXml="true"/>]]></cell>
		<cell><![CDATA[<s:property value="codigoCURP" escapeHtml="false" escapeXml="true"/>]]></cell>
		<cell><![CDATA[<s:property value="telefono" escapeHtml="false" escapeXml="true"/>]]></cell>
		<s:if test="#c.nombreCalleFiscal !=null">
			<cell><![CDATA[<s:property value="nombreCalleFiscal" escapeHtml="false" escapeXml="true"/>,Col. <s:property value="nombreColoniaFiscal" escapeHtml="false" escapeXml="true"/>]]></cell>
		</s:if>
		<s:else>
			<cell></cell>
		</s:else>
		<cell><![CDATA[<s:property value="codigoPostalFiscal" escapeHtml="false" escapeXml="true"/>]]></cell>
		<s:if test="tipoAccion!=\"consulta\"">
			<s:if test="divCarga!=null && divCarga!=\"\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsRendererIntoDiv(verDetalleClientePath,{"tipoAccion":2,"divCarga":"${divCarga}","cliente.idCliente":${c.idCliente},"cliente.idNegocio":"${idNegocio}","${divCarga}")^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParamsRendererIntoDiv(verDetalleClientePath,{"tipoAccion":4,"divCarga":"${divCarga}","cliente.idCliente":${c.idCliente},"cliente.idNegocio":"${idNegocio}"},"${divCarga}")^_self</cell>
			</s:if>
			<s:else>
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleClientePath, 2,{"divCarga":"contenido","cliente.idCliente":${c.idCliente},"cliente.idNegocio":"${idNegocio}"})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleClientePath, 4,{"divCarga":"contenido","cliente.idCliente":${c.idCliente},"cliente.idNegocio":"${idNegocio}"})^_self</cell>
			</s:else>
		</s:if>
	</row>
	</s:iterator>
</rows>