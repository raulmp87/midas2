<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>
<s:include value="/jsp/cliente/catalogoClientes/clientesHeader.jsp"></s:include>
<s:hidden name="tabActiva" id="tabActiva"/>
<s:hidden name="divCarga"></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
</s:if>


<script type="text/javascript">
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		//jQuery("#btnGuardar").bind("click",guardarCliente);
		obtenerCamposObligatorios(1);
		obtenerCamposObligatorios(2);
		var idTipoPersona=jQuery("#cliente\\.claveTipoPersonaString").val();
		idTipoPersona=(idTipoPersona==null)?1:idTipoPersona;
		rescribirEtiquetas(idTipoPersona);
		var tipoMensaje="<s:property value='tipoMensaje'/>";
		if(jQuery.isValid(tipoMensaje) && 10==tipoMensaje){
			alert("Favor de verificar que haya completado todos los datos obligatorios, revise cada pesta�a para poder continuar.");
		}
	});
</script>
<body>
<s:if test="%{#cliente.idCliente} != ''">
	<s:textfield name="cliente.idCliente"/>
</s:if>
	<div select="<s:property value="tabActiva"/>" style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="clienteTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
		<div width="150px" id="datosGenerales" name="Datos Generales" href="http://void">
			 <jsp:include page="datosGenerales.jsp" flush="true"></jsp:include>
		</div>
		<div width="150px" id="datosContacto" name="Datos de Contacto" href="http://void">
			<jsp:include page="datosContacto.jsp" flush="true"></jsp:include>
		</div>
		<div width="150px" id="datosFiscales" name="Datos Fiscales" href="http://void">
			<jsp:include page="datosFiscales.jsp" flush="true"></jsp:include>
		</div>
		<s:if test="cliente.idCliente!=null">
		<div width="150px" id="datosCobranza" name="Datos de Cobranza" href="http://void">
			<jsp:include page="datosCobranza.jsp" flush="true"></jsp:include>
		</div>
		</s:if>
		 <!-- 
		<div width="150px" id="datosAvisoSiniestro" name="Aviso en Siniestros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarAvisoSiniestro.do?cliente.idCliente=', 'contenido', 'cargarComboAvisoSiniestro();"></div>
		<div width="150px" id="datosCanalVentas" name="Canales de Venta" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarCanalVentas.do?cliente.idCliente=', 'contenido', 'cargarComboCanalVentas();"></div>
		 -->
	</div>
	<!-- 
	<div>
		<input type="button" value="Guardar" id="btnGuardar"/>
	</div>
	-->
	<div align="right" class="w910 inline" >
		<div class="btn_back w110">
			<a href="javascript: guardarCliente('${divCarga}');" class="icon_guardar" onclick="">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>
		<div class="btn_back w110">
			<a href="javascript: regresarListarClientes('${divCarga}','${tipoAccion}','${cliente.idCliente}');" class="icon_regresar" onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
	</div>
	<s:if test="%{#idAgente==''}">
			<s:text name="si entra"/>
		</s:if>
</body>
<!--/html-->