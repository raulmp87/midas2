<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			//jQuery("#btnCopyGeneralData").bind("click",copiarDatosGenerales);
		});
	</script>
<!--/head-->

<body>
		<!-- 
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="285"/>
		-->
		<div class="toolBar">
			<br/>
<!-- 			<input type="button" id="btnCopyGeneralData" value="Copiar datos generales"/> -->
			<s:if test="display">
			<div class="btn_back w160" style="display:inline-block;">
				<a href="javascript: copiarDatosGenerales();" class="icon_guardar" onclick="">
					Copiar datos generales
				</a>
			</div>
			</s:if>
		</div>
		<table width="98%"  class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo" colspan="6">
					Secci�n de Datos Fiscales
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.nombreFiscal">
						<s:text name="midas.fuerzaventa.negocio.nombre"/>
					</label>
<!-- 					<label>Nombre</label> -->
				</td>
				<td>
						<s:textfield name="cliente.nombreFiscal" disabled="%{#readOnly}" id="cliente.nombreFiscal" maxlength="20" value="%{cliente.nombre}" cssClass="cajaTextoM2 jQrequired"/>
				</td>
				<td>
<!-- 					<label>Apellido Paterno</label> -->
					<label for="cliente.apellidoPaternoFiscal">
						<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/>
					</label>
				</td>
				<td>
					<s:textfield name="cliente.apellidoPaternoFiscal" disabled="%{#readOnly}" id="cliente.apellidoPaternoFiscal" value="%{cliente.apellidoPaterno}" maxlength="20" cssClass="cajaTextoM2 jQrequired"/>
				</td>
				<td>
<!-- 					<label>Apellido Materno</label> -->
					<label for="cliente.apellidoMaternoFiscal">
						<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/>
					</label>
				</td>
				<td>
					<s:textfield name="cliente.apellidoMaternoFiscal" disabled="%{#readOnly}" id="cliente.apellidoMaternoFiscal" maxlength="20"  value="%{cliente.apellidoMaterno}" cssClass="cajaTextoM2 jQrequired"/>
				</td>
			</tr>
		</tbody>
		</table>
		
		<table width="98%" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo">
					Domicilio Fiscal
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.idPaisFiscal</s:param>
						<s:param name="idEstadoName">cliente.idEstadoFiscal</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioFiscal</s:param>		
						<s:param name="idColoniaName">cliente.nombreColoniaFiscal</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalleFiscal</s:param>
						<s:param name="cpName">cliente.codigoPostalFiscal</s:param>
						<s:param name="numeroName">cliente.numeroDomFiscal</s:param>
						<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferenteFiscal</s:param>
						<s:param name="idColoniaCheckName">idColoniaCheckFiscal</s:param>				
						<s:param name="labelPais">Pa�s</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle</s:param>
						<s:param name="labelNumero">N�mero</s:param>
						<s:param name="labelCodigoPostal">C�digo Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">101</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
						<s:param name="idColoniaString">cliente.idColoniaString</s:param>
					</s:action>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>