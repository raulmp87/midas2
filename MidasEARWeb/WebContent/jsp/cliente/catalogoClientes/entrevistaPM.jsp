<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/cliente/catalogoClientes/clientesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
	<s:form id="entrevistaForm" action="guardarEntrevista">
<s:hidden name="cotizacion.idToCotizacion" id="cotizacion.idToCotizacion"/>
<s:hidden name="entrevista.idToCotizacion" id="entrevista.idToCotizacion"/>
<s:hidden name="entrevista.moneda" id="entrevista.moneda"/>
<s:hidden name="cotizacion.idToPersonaContratante" id="cotizacion.idToPersonaContratante" />
<s:hidden name="pdfDownload" />
<s:hidden name="entrevista.pep" />
<s:hidden name="entrevista.cuentaPropia"/>

		<s:hidden name="cotizacion.idToPersonaContratante" />
	<table width="98%" bgcolor="white" align="center"
		class="contenedorConFormato">
		<thead>
			<tr>
				<div id="personaMoral"></div>
				<td colspan="3" align="center"><h4><s:text name="midas.clientes.entrevista.prima.title"/></h4></td>
			</tr>
		</thead>
		<tbody>
		<tr>
				<td width="33%" style="display: none">
					<div><label><s:text name="midas.clientes.entrevista.prima.estimada"/> </label>
						<s:textfield name="entrevista.primaEstimada" disabled="%{#readOnly}"
							id="entrevista.primaEstimada" labelposition="left"
							onkeypress="return soloNumeros(this, event, true)"
							maxlength="40" cssClass="" />
					</div>
				</td>
				<td  style="display: none">
				<table class="contenedorFormas no-Border">
						
						<tr>
							<td></td>
						</tr>
					</table>
				
				</td>
				<td width="33%">
						
						
						
						<table class="contenedorFormas no-Border" style="border: none;">
							<tr>
								<td>
									<label><s:text name="midas.clientes.entrevista.prima.fiel"/> </label>
		 						</td>
	 							<td> 
	 								<s:textfield name="entrevista.numeroSerieFiel"
										disabled="%{#readOnly}" id="entrevista.numeroSerieFiel"
										onkeypress="return soloNumeros(this, event, false)"
										maxlength="40"
										cssClass="cajaTextoM2 "
										value="0" />
								</td>
								<td>
									<img src="/MidasWeb/img/question2.png"
									 title="<s:text name="midas.clientes.entrevista.prima.fiel.tooltip"/>"
									 style="width: 22px;" />
								</td>
								
							</tr>
						</table>
				</td>
				
			</tr>
			
			<tr>
			<td width="33%">
					<table class="contenedorFormas no-Border" style="border: none;">
						<tr>
							<td>
								<label>
									<s:text name="midas.clientes.entrevista.folio.mercantil"/>
								</label>
	 						</td>
 							<td> 
 								<s:textfield name="entrevista.folioMercantil"
									disabled="%{#readOnly}" id="entrevista.folioMercantil"
									maxlength="40"
									cssClass="cajaTextoM2 " />
							</td>
							<td>
								<img src="/MidasWeb/img/question2.png"
								 title="<s:text name="midas.clientes.entrevista.folio.mercantil.tooltip"/>"
								 style="width: 22px;" />
							</td>
							
						</tr>
					</table>
				</td>
			</tr>
		
		
		</tbody></table>
		
		<table align="right">
			<tr>

				<td>
					<div class="btn_back w100" id="botonEmision" style="float: right;">
						<a
							href="javascript: verComplementarCotizacion(<s:property value="cotizacion.idToCotizacion"/>)">
							<s:text name="midas.boton.cancelar"/> </a>
					</div>
				<td>
				
				
					<div class="btn_back w100" id="botonEmision" style="float: right;">
						<a href="javascript: verComplementarCotizacionDeEntrevista()"> <s:text name="midas.boton.guardar"/>  </a>
					</div></td>
			</tr>
		</table></s:form>