<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/complementarHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>



	<s:form id="entrevistaForm" action="guardarEntrevista">
		<s:hidden name="cotizacion.idToCotizacion" id="cotizacion.idToCotizacion"/>
		<s:hidden name="entrevista.idToCotizacion" id="entrevista.idToCotizacion"/>
		<s:hidden name="entrevista.moneda" id="entrevista.moneda"/>
		<s:hidden name="cotizacion.idToPersonaContratante" id="cotizacion.idToPersonaContratante" />
		<s:hidden name="pdfDownload" id="pdfDownload"/>
		<table width="98%" bgcolor="white" align="center"
		class="contenedorConFormato">
		<thead>
			<tr>
				<div id="personaFisica"></div>
				<td colspan="3" align="center"><h4><s:text name="midas.clientes.entrevista.prima.title"/></h4></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="100%">
					<table class="contenedorFormas no-Border" style="padding-left: 0; border: none">
							<tr>
								
								<td width="33%" style="display: none">
									<div>
									<label class="small"><s:text
									name="midas.clientes.entrevista.prima.estimada" /></label>
										<s:textfield name="entrevista.primaEstimada"
											disabled="%{#readOnly}" id="entrevista.primaEstimada"
											 labelposition="left" maxlength="40"
											onkeypress="return soloNumeros(this, event, true)"
											cssClass="" />
									</div>
								</td>
								<td width="33%" style="display: none">
									<table class="contenedorFormas no-Border" style="border: none">

										<tr>
											<td></td>
										</tr>
									</table>
								</td>

								<td width="33%">
									
									<table class="contenedorFormas no-Border" style="padding-left: 0; border: none;">
										<tr>
											<td>
												<label class="small" title="<s:text name="midas.clientes.entrevista.prima.fiel.tooltip"/>">
													<s:text name="midas.clientes.entrevista.prima.fiel" />
												</label>
											</td>
											<td>
												<s:textfield name="entrevista.numeroSerieFiel"
													disabled="%{#readOnly}" id="entrevista.numeroSerieFiel"
													onkeypress="return soloNumeros(this, event, false)"
													labelposition="left" maxlength="30" cssClass="" value="0" />
											</td>
											<td>
												<img src="/MidasWeb/img/question2.png"
												 title="<s:text name="midas.clientes.entrevista.prima.fiel.tooltip"/>"
												 style="width: 22px;" />
											</td>
										</tr>
									</table>									
							</td>

							</tr>
						</table></td>
				</tr>
				<tr>
					<td width="100%">
						<table class="contenedorFormas no-Border" style="border: none">
							<tr>
								<td width="85%"><label><s:text name="midas.clientes.entrevista.pep"/></label> 
								</td>
								<td><s:radio id="entrevista.pep" disabled="%{#readOnly}"
										name="entrevista.pep" list="#{'true':'Sí','false':'No'}"
										cssClass="cajaTextoM2 "
										onclick="return changePEPS();"
										></s:radio></td>
							</tr>

						</table>
					</td>

				</tr>
				
				<tr>
					<td width="100%">
						<table class="contenedorFormas no-Border" style="border: none">
							
							<tr id="pepDiV">
								<td width="25%">
									<div><label class="small"><s:text
										name="midas.clientes.entrevista.parentesco" /></label>
										<s:textfield name="entrevista.parentesco"
											disabled="%{#readOnly}" id="entrevista.parentesco"
											 labelposition="left"
											maxlength="40" cssClass="" />
									</div>
								</td>

								<td width="25%">
										<label class="small"><s:text
										name="midas.clientes.entrevista.puesto" /></label>
										<s:textfield name="entrevista.nombrePuesto"
										disabled="%{#readOnly}" id="entrevista.nombrePuesto"
										maxlength="50"
										cssClass="" /></td>
								<td width="25%">
										<label class="small"><s:text
										name="midas.clientes.entrevista.periodo" /></label>
										<s:textfield name="entrevista.periodoFunciones"
										disabled="%{#readOnly}" id="entrevista.periodoFunciones"
										 labelposition="left" maxlength="50"
										cssClass="" /></td>
								</tr>
								
								
								<tr>
								<td width="25%">
									<table class="contenedorFormas no-Border" style="border: none">
										<tr>
											<td><label><s:text name="midas.clientes.entrevista.cuenta.propia"/> </label>
											</td>
											<td><s:radio id="entrevista.cuentaPropia"
													disabled="%{#readOnly}" name="entrevista.cuentaPropia"
													list="#{'true':'Sí','false':'No'}" cssClass="cajaTextoM2 "
													onclick="return changeCuentaPropia();"
													></s:radio>
											</td>
										</tr>


									</table>
								</td></tr>
								
								<tr id="cuentaPropiaDiv">
								<td width="25%">
									<table class="contenedorFormas no-Border" style="border: none">
										<tr>
											<td><label>
													<s:text name="midas.clientes.entrevista.doc.rep"/>
												</label>
											</td>

											<td><s:radio id="entrevista.documentoRepresentacion"
													disabled="%{#readOnly}"
													name="entrevista.documentoRepresentacion"
													list="#{'true':'Sí','false':'No'}" cssClass="cajaTextoM2 "></s:radio>
											</td>
										</tr>
									</table>
								</td>
								<td width="25%">
										<label class="small"><s:text
										name="midas.clientes.entrevista.nombre.actua" /></label>
										<s:textfield name="entrevista.nombreRepresentado"
										disabled="%{#readOnly}" id="entrevista.nombreRepresentado"
										 maxlength="50"
										cssClass="" /></td>
							</tr>
						</table>
				</tr>

			</tbody>

		</table>
		<table align="right" style="border: none">
			<tr>

				<td>
					<div class="btn_back w100" id="botonEmision" style="float: right;">
						<a
							href="javascript: verComplementarCotizacion(<s:property value="cotizacion.idToCotizacion"/>)">
							<s:text name="midas.boton.cancelar"/>   </a>
					</div>
				<td>
				
				
					<div class="btn_back w100" id="botonEmision" style="float: right;">
						<a href="javascript: verComplementarCotizacionDeEntrevista()"> <s:text name="midas.boton.guardar"/>  </a>
					</div></td>
			</tr>
		</table>
	</s:form>
	
	<script type="text/javascript">
	
	jQuery(document).ready(function(){
		jQuery("input:radio").attr("checked", false);
		jQuery('#pepDiV').hide();
		jQuery("#cuentaPropiaDiv").hide();
	 });

	</script>
