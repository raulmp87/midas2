<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/cliente/catalogoClientes/clientesHeader.jsp"></s:include>
<s:hidden name="tabActiva" id="tabActiva"/>
<s:hidden name="divCarga"></s:hidden>
<s:hidden name="fechaActual" id="txtFechaActual"/>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set var="showOnConsulta">both</s:set>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set var="showOnConsulta">both</s:set>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set var="showOnConsulta">both</s:set>
</s:if>
<script type="text/javascript">
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		//jQuery("#btnGuardar").bind("click",guardarCliente);
		obtenerCamposObligatorios(1);
		obtenerCamposObligatorios(2);
		var booleanTipoPersonaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
		var booleanTipoPersonaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
		var idTipoPersona=null;
		idTipoPersona=(booleanTipoPersonaMoral)?2:1;
// 		var idTipoPersona=jQuery("#cliente\\.claveTipoPersonaString").val();
// 		idTipoPersona=(idTipoPersona==null)?1:idTipoPersona;
		rescribirEtiquetas(idTipoPersona);
		var tipoMensaje="<s:property value='tipoMensaje'/>";
		if(jQuery.isValid(tipoMensaje) && 10==tipoMensaje){
			alert("Favor de verificar que haya completado todos los datos obligatorios, revise cada pesta�a para poder continuar.");
		}
	});
</script>
<style type="text/css">
	.clientesForm select{
		width:150px;
	}
	.clientesForm label{
		padding-right:5px;
	}
</style>
<!--/head-->

<body>
	<s:form id="jsonForm">
	</s:form>
	<s:form id="clientForm" cssClass="clientesForm">
		<s:hidden name = "tipoOperac"/>
		<s:hidden name = "tipoAccionAgente"/>
		<s:hidden name="tipoAccion"/>		
		<s:hidden name="moduloOrigen"/>
		<s:hidden name="idAgente"/>
		<s:hidden name="cliente.idCliente"/>
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idToPersona" id="cliente.idToPersona"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="%{idLineaNegocio}"/>
		<s:hidden name="tipoRegreso" id="tipoRegreso"/>
		<s:hidden name="idNegocio" id="idNegocio"/>
		<s:hidden name="idToCotizacion" id="idToCotizacion"/>
		<s:hidden name="idToPoliza" id="idToPoliza" />
		<s:hidden name="validoEn" id="validoEn" />
		<s:hidden name="recordFrom" id="recordFrom" />
		<s:hidden name="validoEnMillis" id="validoEnMillis" />
		<s:hidden name="recordFromMillis" id="recordFromMillis" />
		<s:hidden name="claveTipoEndoso" id="claveTipoEndoso" />
		<s:actionerror/>
		<table width="97%">
		<tbody>
			<s:if test="tipoAccion == 2">
			<tr>
				<td colspan="6" class="titulo">Consulta de Cliente</td>
			</tr>
			</s:if>
			<s:elseif test="tipoAccion == 4">
				<td colspan="6" class="titulo">Editar Cliente</td>
			</s:elseif>
			<s:elseif test="tipoAccion == 3">
				<td colspan="6" class="titulo">Eliminar Cliente</td>
			</s:elseif>
			<s:else>
			<tr>
				<td colspan="6" class="titulo">Alta de Cliente</td>
			</tr>
			</s:else>
		</tbody>
		</table>
		<div id="workAreaClientes">
			<c:choose>
				<c:when test="${not empty cliente}">
					<!-- Si es persona moral -->
					<c:choose>
						<c:when test="${cliente.claveTipoPersona==2}">
							<div select="<s:property value="tabActiva"/>"  style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="clienteTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
								<div width="150px" id="datosGenerales" name="Datos Generales" href="http://void">
									<jsp:include page="datosGeneralesPM.jsp" flush="true"></jsp:include>
								</div>
								<div width="150px" id="datosContacto" name="Datos de Contacto" href="http://void">
									<jsp:include page="datosContactoPM.jsp" flush="true"></jsp:include>
								</div>
								<div width="150px" id="datosFiscales" name="Datos Fiscales" href="http://void">
									<jsp:include page="datosFiscalesPM.jsp" flush="true"></jsp:include>
								</div>
								<s:if test="cliente.idCliente!=null">
								<div width="150px" id="datosCobranza" name="Datos de Cobranza" href="http://void">
									<jsp:include page="datosCobranza.jsp" flush="true"></jsp:include>
								</div>
								</s:if>
								<!-- 
								<div width="150px" id="datosAvisoSiniestro" name="Aviso en Siniestros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarAvisoSiniestro.do?cliente.idCliente=', 'contenido', 'cargarComboAvisoSiniestro();"></div>
								<div width="150px" id="datosCanalVentas" name="Canales de Venta" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarCanalVentas.do?cliente.idCliente=', 'contenido', 'cargarComboCanalVentas();"></div>
								 -->
							</div>
							<div align="right" class="w910 inline" >
							<s:if test="display">
							<div class="btn_back w110">
								<a href="javascript: guardarCliente('${divCarga}');" class="icon_guardar" onclick="">
									<s:text name="midas.boton.guardar"/>
								</a>
							</div>
							</s:if>
							<s:if test="moduloOrigen==null">
							<div class="btn_back w110">
								<a href="javascript: regresarListarClientes('${divCarga}','${tipoAccion}','${cliente.idCliente}','${tipoRegreso}');" class="icon_regresar" onclick="">
									<s:text name="midas.boton.regresar"/>
								</a>
							</div>
							</s:if>
							<s:else>
								<div class="btn_back w110">
									<a href="javascript: regresarAgente();" class="icon_regresar" onclick="">
										<s:text name="midas.boton.regresar"/>
									</a>
								</div>
							</s:else>
						</div>
						</c:when>
						<c:otherwise>
							<div select="<s:property value="tabActiva"/>" style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="clienteTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
								<div width="150px" id="datosGenerales" name="Datos Generales" href="http://void" >
									<jsp:include page="datosGenerales.jsp" flush="true"></jsp:include>
								</div>
								<div width="150px" id="datosContacto" name="Datos de Contacto" href="http://void">
									<jsp:include page="datosContacto.jsp" flush="true"></jsp:include>
								</div>
								<div width="150px" id="datosFiscales" name="Datos Fiscales" href="http://void">
									<jsp:include page="datosFiscales.jsp" flush="true"></jsp:include>
								</div>
								<s:if test="cliente.idCliente!=null">
								<div width="150px" id="datosCobranza" name="Datos de Cobranza" href="http://void">
									<jsp:include page="datosCobranza.jsp" flush="true"></jsp:include>
								</div>
								</s:if>
								<!-- 
								<div width="150px" id="datosAvisoSiniestro" name="Aviso en Siniestros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarAvisoSiniestro.do?cliente.idCliente=', 'contenido', 'cargarComboAvisoSiniestro();"></div>
								<div width="150px" id="datosCanalVentas" name="Canales de Venta" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarCanalVentas.do?cliente.idCliente=', 'contenido', 'cargarComboCanalVentas();"></div>
								 -->
							</div>
							<div align="right" class="w910 inline" >
								<s:if test="display">
								<div class="btn_back w110">
									<a href="javascript: guardarCliente('${divCarga}');" class="icon_guardar" onclick="">
										<s:text name="midas.boton.guardar"/>
									</a>
								</div>
								</s:if>
								<s:if test="moduloOrigen==null">
								<div class="btn_back w110">
									<a href="javascript: regresarListarClientes('${divCarga}','${tipoAccion}','${cliente.idCliente}','${tipoRegreso}');" class="icon_regresar" onclick="">
										<s:text name="midas.boton.regresar"/>
									</a>
								</div>
								</s:if>
								<s:else>
									<div class="btn_back w110">
										<a href="javascript: regresarAgente();" class="icon_regresar" onclick="">
											<s:text name="midas.boton.regresar"/>
										</a>
									</div>
								</s:else>
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<div select="<s:property value="tabActiva"/>" style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="clienteTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
						<div width="150px" id="datosGenerales" name="Datos Generales" href="http://void" >
							<jsp:include page="datosGenerales.jsp" flush="true"></jsp:include>
						</div>
						<div width="150px" id="datosContacto" name="Datos de Contacto" href="http://void">
							<jsp:include page="datosContacto.jsp" flush="true"></jsp:include>
						</div>
						<div width="150px" id="datosFiscales" name="Datos Fiscales" href="http://void">
							<jsp:include page="datosFiscales.jsp" flush="true"></jsp:include>
						</div>
						<s:if test="cliente.idCliente!=null">
						<div width="150px" id="datosCobranza" name="Datos de Cobranza" href="http://void">
							<jsp:include page="datosCobranza.jsp" flush="true"></jsp:include>
						</div>
						</s:if>
						<!-- 
						<div width="150px" id="datosAvisoSiniestro" name="Aviso en Siniestros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarAvisoSiniestro.do?cliente.idCliente=', 'contenido', 'cargarComboAvisoSiniestro();"></div>
						<div width="150px" id="datosCanalVentas" name="Canales de Venta" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogoCliente/mostrarCanalVentas.do?cliente.idCliente=', 'contenido', 'cargarComboCanalVentas();"></div>
						 -->
					</div>
					<div align="right" class="w910 inline" >
						<s:if test="display">
						<div class="btn_back w110">
							<a href="javascript: guardarCliente('${divCarga}');" class="icon_guardar" onclick="">
								<s:text name="midas.boton.guardar"/>
							</a>
						</div>		
						</s:if>				
						<div class="btn_back w110">
							<a href="javascript: regresarListarClientes('${divCarga}','${tipoAccion}','${cliente.idCliente}','${tipoRegreso}');" class="icon_regresar" onclick="">
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>						
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</s:form>
</body>
<!--/html-->