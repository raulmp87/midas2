<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/cliente/catalogoClientes/clientesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<style>
	#filtrosM2 tr th,#filtrosM2 tr td{
		padding-left:5px;
	}
</style>
<script type="text/javascript">

jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var idField='<s:property value="idField"/>';
	 var idCliente='<s:property value="filtroCliente.idCliente"/>';
	 idCliente=(jQuery.isValid(idCliente))?idCliente:"";
	 var urlFiltro=listarFiltradoClientePath+"?tipoAccion="+tipoAccion+"&idField="+idField;
	 if(jQuery.isValid(idCliente)){
		 urlFiltro+="&filtroCliente.idCliente="+idCliente;
	 }
	 	 listarFiltradoGenerico(urlFiltro,"clienteGrid", null,idField,'clienteModal');
});	
</script>
<body>
<s:hidden name="filtroCliente.idCliente" id="filtroIdCliente"></s:hidden>
<s:form action="listarFiltrado" id="clienteForm" name="clienteForm">
	
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<s:hidden name="tipoRegreso"  id="tipoRegreso"/>
	<s:hidden name="idNegocio"  id="idNegocio"/>
	<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
	<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
	<s:hidden name="idField"></s:hidden>
	<s:hidden name="divCarga"></s:hidden>
	<table width="920px" id="filtrosM2" cellpadding="0" cellspacing="0" >
		<tr>
			<td class="titulo" colspan="4">Consulta de Clientes</td>
		</tr>
		<tr>
			<th width="200px">				
				Persona Jurídica
			</th>	
			<td width="315px">
				<s:radio id="filtroCliente.claveTipoPersonaString" onchange="ocultarCamposTipoPersona(this.value);" name="filtroCliente.claveTipoPersonaString" list="#{'1':'Persona Física','2':'Persona Moral'}"  value="1" cssClass="cajaTextoM2 "></s:radio>
			</td>
			<!-- 
			<th width="105px">				
				Nombre
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.nombre" id="nombre" cssClass="cajaTextoM2 w250"/>
			</td>
			-->
		</tr>
		<tr class="pf">
			<th width="105px">				
				Nombre
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.nombre" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200"
				maxlength="50;"/>
			</td>
			<th width="105px">				
				Apellido Paterno
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.apellidoPaterno" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200"
				maxlength="30"/>
			</td>			
			<th width="105px">				
				Apellido Materno
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.apellidoMaterno" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200"
				maxlength="30"/>
			</td>
		</tr>
		<tr class="pf">
			<th width="105px">				
				CURP
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.codigoCURP" id="curp" cssClass="jQCURP cajaTextoM2 w200"
				maxlength="18"/>
			</td>
		</tr>
		<tr class="pm" style="display:none;">
			<th width="105px">				
				Razón Social
			</th>	
			<td width="315px">
				<s:textfield name="filtroCliente.razonSocial" id="nombre" cssClass=" cajaTextoM2 w200"
				maxlength="500"/>
			</td>
			<th width="105px">				
				Fecha de Constitución 
			</th>	
			<td width="315px">
				<sj:datepicker name="filtroCliente.fechaConstitucion"  
								   buttonImage="../img/b_calendario.gif" 
								   id="fechaConstitucion" maxlength="10" cssClass="cajaTextoM2"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true"  
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onblur='esFechaValida(this);'></sj:datepicker>
			</td>
		</tr>
		<tr>
			<th width="105px">				
				RFC
			</th>	
			<td width="315px" colspan="3">
				<s:textfield name="filtroCliente.codigoRFC" id="rfc" cssClass="jQalphanumeric cajaTextoM2 w200" maxlength="13"/>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: (validateForId('clientForm', true,'search'))?listarFiltradoGenerico(listarFiltradoClientePath, 'clienteGrid', document.clienteForm, '<s:property value="idField"/>', 'clienteModal'):false">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
</s:form>
<div id="divCarga" style="left:214px; top: 40px; z-index: 1"></div>
<div align="center" id="clienteGrid" style="width:918px;height:220px;background-color:white;overflow:hidden"></div>
<div id="pagingArea_Clientes"></div><div id="infoArea_Clientes"></div>
<s:if test="ocultarAgregar == 0" >
	<div align="right" class="w920">
		<div class="btn_back w110">
			<!-- 
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="javascript:operacionGenerica(verDetalleClientePath,1);">
				<s:text name="midas.boton.agregar"/>
			</a>
			-->
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="javascript:operacionGenericaConParamsRendererIntoDiv(verDetalleClientePath,{'tipoAccion':1,'divCarga':'${divCarga}','tipoRegreso':'${tipoRegreso}','idNegocio':'${idNegocio}','idToCotizacion':'${idToCotizacion}'},'${divCarga}');cerrarContenedorVentanas('clienteModal');">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>	
	</div>
</s:if>
</body>
<!--/html-->