<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        <column id="numeroRegistro" type="ro" width="50"><s:text name="midas.cliente.datosCobranza.grid.column.numeroRegistro"/></column>
        <column id="medioPago" type="ro" width="200"><s:text name="midas.cliente.datosCobranza.grid.column.medioPago"/></column>
        <column id="nombreTarjetahabiente" type="ro" width="278"><s:text name="midas.cliente.datosCobranza.grid.column.nombreTarjetaHabiente"/></column>
		<column id="numeroTarjetaCobranza" type="ro" width="150"><s:text name="midas.cliente.datosCobranza.grid.column.numeroTarjetaCobranza"/></column>
		<column id="codigoRFCCobranza" type="ro" hidden="true" width="0"></column>
		<column id="codigoPostalCobranza" type="ro" hidden="true" width="0"></column>
		<column id="idEstadoCobranza" type="ro" hidden="true" width="0"></column>
		<column id="idMunicipioCobranza" type="ro" hidden="true" width="0"></column>
		<column id="nombreColoniaCobranza" type="ro" hidden="true" width="0"></column>
		<column id="nombreCalleCobranza" type="ro" hidden="true" width="0"></column>
		<column id="telefonoCobranza" type="ro" hidden="true" width="0"></column>
		<column id="emailCobranza" type="ro" hidden="true" width="0"></column>
		<column id="idBancoCobranza" type="ro" hidden="true" width="0"></column>
		<column id="idTipoTarjetaCobranza" type="ro" hidden="true" width="0"></column>
		<column id="numeroTarjetaCobranza" type="ro" hidden="true" width="0"></column>
		<column id="codigoSeguridadCobranza" type="ro" hidden="true" width="0"></column>
		<column id="mesFechaVencimiento" type="ro" hidden="true" width="0"></column>
		<column id="anioFechaVencimiento" type="ro" hidden="true" width="0"></column>
		<column id="diaPagoTarjetaCobranza" type="ro" hidden="true" width="0"></column>
		<column id="idTipoDomiciliacion" type="ro" hidden="true" width="0"></column>
	</head>
	<s:iterator value="mediosPagoCliente" var="medioPago" status="index">
		<row id="${index.count}">
			<cell>${index.count}</cell>
			<c:choose>
				<c:when test="${medioPago.idTipoConductoCobro==4}">	
					<cell>Tarjeta de Credito</cell>	
				</c:when>
				<c:when test="${medioPago.idTipoConductoCobro==1386}">	
					<cell>Cuenta Afirme</cell>	
				</c:when>
				<c:otherwise>
					<cell>Domiciliacion</cell>
				</c:otherwise>
			</c:choose>
			<cell>${medioPago.nombreTarjetaHabienteCobranza}</cell>
			<cell>${medioPago.numeroTarjetaCobranza}</cell>
			<cell>${medioPago.codigoRFCCobranza}</cell>
			<cell>${medioPago.codigoPostalCobranza}</cell>
			<cell>${medioPago.idEstadoCobranza}</cell>
			<cell>${medioPago.idMunicipioCobranza}</cell>
			<cell>${medioPago.nombreColoniaCobranza}</cell>
			<cell>${medioPago.nombreCalleCobranza}</cell>
			<cell>${medioPago.telefonoCobranza}</cell>
			<cell>${medioPago.emailCobranza}</cell>
			<cell>${medioPago.idBancoCobranza}</cell>
			<cell>${medioPago.idTipoTarjetaCobranza}</cell>
			<cell>${medioPago.numeroTarjetaCobranza}</cell>
			<cell>${medioPago.codigoSeguridadCobranza}</cell>
			<cell>${medioPago.mesFechaVencimiento}</cell>
			<cell>${medioPago.anioFechaVencimiento}</cell>
			<cell>${medioPago.diaPagoTarjetaCobranza}</cell>
			<cell>${medioPago.idTipoDomiciliacion}</cell>
		</row>
	</s:iterator>
</rows>
