<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<style>
	label{
		display:inline-block;
	}
</style>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
		});
	</script>
<!--/head-->

<body>
		<!-- 
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="285"/>
		-->
		<table width="100%" bgcolor="white" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td char="4" class="titulo">
					Datos de Contacto
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.puestoContacto">Puesto</label>
					<%-- <s:text name="Puesto"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.puestoContacto" id="cliente.puestoContacto" cssClass="cajaTextoM2" maxlength="60"/>			
				</td>
				<td>
					<label for="cliente.nombreContacto">Nombre</label>
					<%-- <s:text name="Nombre"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.nombreContacto" id="cliente.nombreContacto" cssClass="cajaTextoM2 jQrequired w180" maxlength="50"/>
				</td>
			</tr>
		</tbody>
		</table>
		
		<table width="100%" bgcolor="white" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td colspan="4" class="titulo">
					Informaci�n Adicional
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonoOficinaContacto">Tel�fono Oficina</label>
					<%-- <s:text name="Tel�fono Oficina"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.telefonoOficinaContacto" id="cliente.telefonoOficinaContacto" cssClass="cajaTextoM2" maxlength="12" onkeypress="return soloNumeros(this, event, false)"/>
				</td>
				<td>
					<label for="cliente.extensionContacto">Extensi�n</label>
					<%-- <s:text name="Extensi�n"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.extensionContacto" id="cliente.extensionContacto" cssClass="cajaTextoM2" maxlength="5" onkeypress="return soloNumeros(this, event, false)"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.celularContacto">Celular</label>
					<%-- <s:text name="Celular"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.celularContacto" id="cliente.celularContacto" cssClass="cajaTextoM2" maxlength="10" onkeypress="return soloNumeros(this, event, false)"/>
				</td>
				<td>
					<label for="cliente.faxContacto">Fax</label>
					<%-- <s:text name="Fax"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.faxContacto" id="cliente.faxContacto" cssClass="cajaTextoM2" maxlength="12" onkeypress="return soloNumeros(this, event, false)"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.email">Correo electr�nico</label>
					<%-- <s:text name="Correo electr�nico"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.email" id="cliente.email" cssClass="cajaTextoM2 jQemail" maxlength="50"/>
				</td>
				<td>
					<label for="cliente.facebookContacto">Facebook</label>
					<%-- <s:text name="Facebook"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.facebookContacto" id="cliente.facebookContacto" cssClass="cajaTextoM2" maxlength="50"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.twitterContacto">Twitter</label>
					<%-- <s:text name="Twitter"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.twitterContacto" id="cliente.twitterContacto" cssClass="cajaTextoM2" maxlength="50"/>
				</td>
				<td>
					<label for="cliente.paginaWebContacto">P�gina Web</label>
					<%-- <s:text name="P�gina Web"/> --%>
				</td>
				<td>
					<s:textfield disabled="%{#readOnly}" name="cliente.paginaWebContacto" id="cliente.paginaWebContacto" cssClass="cajaTextoM2" maxlength="50"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonosAdicionalesContacto">Tel�fonos Adicionales</label>
					<%-- <s:text name="Tel�fonos Adicionales"/> --%>
				</td>
				<td>
					<s:textarea disabled="%{#readOnly}" name="cliente.telefonosAdicionalesContacto" id="cliente.telefonosAdicionalesContacto" cssClass="areaTextoM2" cols="28" rows="3"></s:textarea>
				</td>
				<td>
					<label for="cliente.correosAdicionalesContacto">Correos Adicionales</label>
					<%-- <s:text name="Correos Adicionales"/> --%>
				</td>
				<td>
					<s:textarea disabled="%{#readOnly}" name="cliente.correosAdicionalesContacto" id="cliente.correosAdicionalesContacto" cssClass="areaTextoM2" cols="28" rows="3"></s:textarea>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>