<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp" />
<script type="text/javascript" src="<s:url value="/js/cliente/catalogoCliente.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/generadorCURPRFC.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/curpRFC.js'/>"></script>  
<script type="text/javascript" src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/jQuery/jquery-util.js"/>"></script>


<script type="text/javascript">
	var listarFiltradoClientePath="<s:url action='filtrarClientes' namespace='/catalogoCliente'/>";
	var verDetalleClientePath="<s:url action='loadById' namespace='/catalogoCliente'/>";
	var verDatosCobranzaPath="<s:url action='consultarMediosPago' namespace='/catalogoCliente'/>";
</script>