<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			/*
			var columnModel=[
				{id:"idCliente",header:"Clave Cliente"},
				{id:"nombre",header:"Nombre",colType:"ed"},
				{id:"apellidoPaterno",header:"Apellido Paterno"},
				{id:"apellidoMaterno",header:"Apellido Materno"}
			];
			var options={
				div:"grid",
				baseProperty:"mediosPagoCliente",
				imagePath:"<s:url value='/img/dhtmlxgrid/'/>",
				urlAction:"<s:url action='consultarMediosPagoPorCliente' namespace='/cliente/catalogoClienteJson'/>",
				rowIdProperty:"idCliente",
				onRowDblClicked:cargarMedioPagoPorIdMedioPago,
				columnModel:columnModel
			};
			cobranzaGrid=jQuery.getGrid("grid");
			*/
			cargarMediosPagoGrid();
			//jQuery("#btnGuardarMedioPago").bind("click",guardarDatosCobranza);
		});
		
		function cargarMediosPagoGrid(){
			cobranzaGrid=new dhtmlXGridObject("grid");
			cobranzaGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
			cobranzaGrid.loadXML(verDatosCobranzaPath+'?cliente.idCliente=${cliente.idCliente}');
		}
	</script>
<!--/head-->

<body>
		<!-- 
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="285"/>
		-->
		<table width="98%" bgcolor="white" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo">
					Lista de Medios de Pago
				</td>
			</tr>
			<tr>
				<td width="99%">
					<div id="grid" style="width:680px;height:190px"></div>
				</td>
			</tr>
		</tbody>
		</table>
		<br/>
		<!-- 
		<s:select list="mediosPagoDomiciliacion" name="cliente.idTipoDomiciliacion" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" onclick="habilitarPorMedioPagoDomiciliacion(this.value);" label="Medio de Pago" labelposition="left"></s:select>
		 -->
		 
		<table width="98%" bgcolor="white" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td colspan="4" class="titulo">Datos de Titular Tarjeta / Cuenta</td>
			</tr>
			<tr>
				<td width="35%">
					<s:select disabled="%{#readOnly}" list="#{'8':'COBRANZA DOMICILIADA','4':'TARJETA DE CREDITO','1386':'CUENTA AFIRME (CREDITO O DEBITO)'}" name="cliente.idTipoConductoCobro" headerKey="" cssClass="cajaTextoM2" headerValue="Seleccione" onclick="habilitarPorMedioPagoDomiciliacion(this.value);" label="Medio de Pago" labelposition="left"></s:select>
				</td>				
			</tr>
			<tr>
				<td>
					<s:if test="display">
					<div class="btn_back w180" id="btnLimpiar">
						<a href="javascript: void(0);" 
							onclick="javascript:limpiarDatosTarjetaHabiente();">
							<s:text name="Limpiar Datos Tarjetahabiente"/>
						</a>
					</div>
					</s:if>
				</td>	
			</tr>
			<tr>				
				<td>
					<s:textfield name="cliente.nombreTarjetaHabienteCobranza" disabled="%{#readOnly}" id="cliente.nombreTarjetaHabienteCobranza" label="Nombre del Tarjetahabiente" labelposition="left" maxlength="35"/>
				</td>				
			</tr>
			<tr>
				<td colspan="2">
					<s:textfield name="cliente.codigoRFCCobranza" disabled="%{#readOnly}" id="cliente.codigoRFCCobranza" label="RFC" labelposition="left" maxlength="13" cssClass="cajaTextoM2 jQalphanumeric"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					
						<s:param name="idEstadoName">cliente.idEstadoCobranza</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioCobranza</s:param>		
						<s:param name="idColoniaName">cliente.nombreColoniaCobranza</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalleCobranza</s:param>
						<s:param name="cpName">cliente.codigoPostalCobranza</s:param>				
						<s:param name="labelPais">Pa�s</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle y N�mero</s:param>
						<s:param name="labelCodigoPostal">C�digo Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">2</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="false"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield name="cliente.telefonoCobranza" disabled="%{#readOnly}" id="telefonoCobranza" label="Tel�fono" labelposition="left" onkeypress="return soloNumeros(this, event, false)" maxlength="15"/>
				</td>
				<td>
					<s:textfield name="cliente.emailCobranza" disabled="%{#readOnly}" id="emailCobranza" label="Correo electr�nico" labelposition="left" cssClass="cajaTextoM2 jQemail" maxlength="60"/>
				</td>
			</tr>
		</tbody>
		</table>
		
		<table width="98%" class="contenedorConFormato" align="center" >
		<tbody>
			<tr>
				<td class="titulo">Datos Tarjeta / Cuenta</td>
			</tr>
			<tr>
				<td width="49%">
					<s:select list="bancos" disabled="%{#readOnly}" cssClass="cajaTextoM2" name="cliente.idBancoCobranza" id="idBancoCobranza" label="Instituci�n Bancaria" labelposition="left" headerKey="" headerValue="Seleccione" listKey="idBanco" listValue="nombreBanco"></s:select>
				</td>
				<td width="49%">
					<s:select disabled="%{#readOnly}" list="#{'VS':'VISA','MC':'Master Card'}" headerKey="" cssClass="cajaTextoM2" headerValue="Seleccione"  name="cliente.idTipoTarjetaCobranza" id="idTipoTarjetaCobranza" label="Tipo de Tarjeta" labelposition="left"></s:select>
				</td>
			</tr>
			<tr>
				<td width="49%">
					<s:textfield id="numeroTarjetaCobranza" disabled="%{#readOnly}" name="cliente.numeroTarjetaCobranza" label="N�mero de Tarjeta" labelposition="left" onkeypress="return soloNumeros(this, event, false)" maxlength="17"/>
				</td>
				<td width="49%">
					<s:textfield id="codigoSeguridadCobranza" disabled="%{#readOnly}" name="cliente.codigoSeguridadCobranza" label="C�digo de Seguridad" labelposition="left" maxlength="3" onkeypress="return soloNumeros(this, event, false)" maxlength="3"/>
				</td>
			</tr>
			<tr>
				<td width="49%">
					<div class="elementoInline">
					<s:textfield name="cliente.mesFechaVencimiento" disabled="%{#readOnly}" maxlength="2" id="mesFechaVencimiento" label="Fecha de Vencimiento	Mes" labelposition="left" cssStyle="width:30px" onkeypress="return soloNumeros(this, event, false)"/>
					</div>
					<div class="elementoInline">
					<s:textfield name="cliente.anioFechaVencimiento" disabled="%{#readOnly}"  id="anioFechaVencimiento" label="A�o" labelposition="left" cssStyle="width:30px" onkeypress="return soloNumeros(this, event, false)" maxlength="2"/>
					</div>					
				</td>
				<td width="49%">
					<s:select name="cliente.diaPagoTarjetaCobranza" disabled="%{#readOnly}" id="diaPagoTarjetaCobranza" cssClass="cajaTextoM2" label="D�a de Pago" labelposition="left" list="#{'':'','1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9','10':'10','11':'11','12':'12','13':'13','14':'14','15':'15','16':'16','17':'17','18':'18','19':'19','20':'20','21':'21','22':'22','23':'23','24':'24','25':'25','26':'26','27':'27','28':'28','29':'29','30':'30','31':'31'}"></s:select>
				</td>
			</tr>
			<tr>
				<td width="49%">&nbsp;</td>
				<td width="49%">
					<!-- <input type="button" id="btnGuardarMedioPago" value="Guardar medio de Pago" style="float:right;">-->
					<br/>
					<s:if test="display">
					<div class="btn_back w170" style="display:inline-block;float:right;">
						<a href="javascript: guardarDatosCobranza();" class="icon_guardar" onclick="">
							Guardar medio de Pago
						</a>
					</div>
					</s:if>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>