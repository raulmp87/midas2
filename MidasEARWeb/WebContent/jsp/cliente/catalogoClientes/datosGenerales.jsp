<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			//Se comenta el jQIsRequired para que no marque los *
			//jQIsRequired();			
			/*
			var idPais=jQuery("#cliente\\.idPaisString").val();
			if(!jQuery.isValid(idPais)){
				jQuery("#cliente\\.idPaisString").val("PAMEXI");
			}
			*/
			var estatusActivo=jQuery("#cliente\\.tipoSituacionString01").attr("checked");
			var estatusInactivo=jQuery("#cliente\\.tipoSituacionString02").attr("checked");
			//Si no tienen un valor seleccionado, entonces lo pone por default
			if(estatusActivo==false && estatusInactivo==false){
				jQuery("#cliente\\.tipoSituacionString01").attr("checked",true);
			}
			var personaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
			var personaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
			if(personaFisica==false && personaMoral==false){
				jQuery("#cliente\\.claveTipoPersonaString1").attr("checked",true);
			}
			
			//Inicializar consulta prima en nulo.
			resultadoWSPrima = null;
			
			validacionesPrimaMayor();
			
		});
	</script>
<!--/head-->

<body>
<!-- 
<s:hidden name="divCarga"/>
-->
<s:hidden name="cliente.verificarRepetidos"   id="cliente.verificarRepetidos" value="true"/>
	<s:hidden name="cliente.idCliente" id="cliente.idCliente" />
	<s:hidden name="cliente.claveTipoPersonaString" id="cliente.claveTipoPersonaString" />
	
	<s:hidden name="cliente.numeroInterior" id="cliente.numeroInterior" />
	<s:hidden name="cliente.idClienteUnico" id="cliente.idClienteUnico" />

		<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		<tbody>
			<tr>
				<td class="titulo" colspan="3">
					Secci�n de Datos Generales del Cliente
				</td>
			</tr>
			<tr>
				<td width="33%">
					<s:textfield id="idClienteVista" label="Id Cliente" labelposition="left" disabled="true" cssClass="cajaTextoM2 " value="%{cliente.idCliente}"/>			
				</td>
				<td width="33%">
					<table class="contenedorFormas no-Border">
						<tr>
							<td>
								<label>Tipo de Situaci�n</label>
<%-- 								<s:text name="midas.cliente.catalogoCliente.tipoSituacion"/> --%>
							</td>
						</tr>
						<tr>
							<td>
								<s:radio id="cliente.tipoSituacionString" disabled="%{#readOnly}" name="cliente.tipoSituacionString" list="#{'A':'Activo','I':'Inactivo'}" cssClass="cajaTextoM2 "></s:radio>	
							</td>
						</tr>					
					</table>
				</td>
				<td width="33%">
					<table class="contenedorFormas no-Border">
						<tr>
							<td>
								<label>Persona Jur�dica</label>
<%-- 								<s:text name="midas.cliente.catalogoCliente.personaJuridica"/> --%>
							</td>
						</tr>
						<tr>
							<td>
								<s:radio id="cliente.claveTipoPersonaString" disabled="%{#readOnly}" name="cliente.claveTipoPersona" list="#{'1':'Persona F�sica','2':'Persona Moral'}" onclick="switchPorTipoPersona(this.value,'%{divCarga}');" cssClass="cajaTextoM2 "></s:radio>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="33%">
				
					<div>
					 <s:textfield name="cliente.nombre"  
					 disabled="%{#readOnly}" id="cliente.nombre" 
					 label="Nombre" labelposition="left" maxlength="40" cssClass="" />
					</div>
				</td>
				<td width="33%">
					<s:textfield name="cliente.apellidoPaterno"  disabled="%{#readOnly}" id="cliente.apellidoPaterno" label="Apellido Paterno"  labelposition="left" maxlength="20" cssClass="cajaTextoM2 " />
				</td>
				<td width="33%">
					<s:textfield name="cliente.apellidoMaterno"  disabled="%{#readOnly}" id="cliente.apellidoMaterno" label="Apellido Materno"  labelposition="left" maxlength="20" cssClass="cajaTextoM2 " />
				</td>
			</tr>
			<!-- 
			<tr>
				<td width="33%">
					<s:select id="cliente.claveNacionalidad" name="cliente.claveNacionalidad" list="paises" listValue="countryName" listKey="countryId" headerValue="Seleccione..." headerKey="" label="Nacionalidad" labelposition="left" cssClass="cajaTextoM2 " value="PAMEXI">
					</s:select>
				</td>
				<td width="33%">
					<s:select id="claveEstadoNacimiento" name="cliente.claveEstadoNacimiento" onchange="limpiarObjetos('claveCiudadNacimiento'); getCiudades(this,'claveCiudadNacimiento');" list="estados" listValue="stateName" listKey="stateId" headerValue="Seleccione..." headerKey="" label="Estado de Nacimiento" labelposition="left"  cssClass="cajaTextoM2 ">
					</s:select>
				</td>
				<td width="33%">
					<s:select id="claveCiudadNacimiento" name="cliente.claveCiudadNacimiento" list="ciudades" listValue="cityName" listKey="cityId" headerValue="Seleccione..." headerKey="" label="Ciudad de Nacimiento" labelposition="left" cssClass="cajaTextoM2 ">
					</s:select>
				</td>
			</tr>
			-->
			<tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.claveNacionalidad</s:param>
							<s:param name="idPaisNacimientoName">cliente.idPaisNacimiento</s:param>
							<s:param name="idEstadoName">cliente.claveEstadoNacimiento</s:param>	
						<s:param name="idCiudadName">cliente.claveCiudadNacimiento</s:param>		
						<s:param name="labelPais">Nacionalidad</s:param>	
						<s:param name="labelPaisNacimiento">Pa�s de Nacimiento</s:param>	
						
						<s:param name="labelEstado">Estado de Nacimiento</s:param>
						<s:param name="labelCiudad">Ciudad de Nacimiento</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">100</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
			</tr>
			<tr>
				<td width="33%">
					<!-- 
					<div style="display:inline-block;">
						<s:textfield name="cliente.fechaNacimiento" id="cliente.fechaNacimiento" label="Fecha de Nacimiento" readonly="true" labelposition="left" cssClass="cajaTextoM2 "/>
					</div>
					-->
					<!-- FALTA CHECAR QUE FUNCIONE -->
					<div class="elementoInline">
						<span class="wwlbl">
							<label class="label"><s:text name="Fecha de Nacimiento"/></label>
						</span>
					</div>
					<div class="elementoInline">
						<sj:datepicker name="cliente.fechaNacimiento" buttonImageOnly="true"
						   label="" cssClass="cajaTextoM2" labelposition="left"   
						   buttonImage="../img/b_calendario.gif" 
						   changeMonth="true"
						   changeYear="true"
						   yearRange="-80:-18" 
						   maxDate="today"
						   disabled="%{#readOnly}"
						   showOn="%{#showOnConsulta}"
						   id="cliente.fechaNacimiento" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx  " 
						   onkeypress="return soloFecha(this, event, false);"  
						   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" 
						   onblur='esFechaValida(this);midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>
						   </sj:datepicker>
					</div>
				</td>
				<td width="33%">
					<s:select name="cliente.estadoCivil" disabled="%{#readOnly}" id="cliente.estadoCivil" label="Estado Civil" list="estadosCiviles" labelposition="left" listKey="idEstadoCivil" listValue="nombreEstadoCivil" cssClass="cajaTextoM2 "/>
				</td>
<!-- 				<td width="33%"> -->
<%-- 					<s:textfield name="cliente.claveOcupacion" id="cliente.claveOcupacion" label="Ocupaci�n" labelposition="left" cssClass="cajaTextoM2 "/> --%>
<!-- 				</td> -->
			</tr>
			<tr>
				<td width="33%">
					<s:select name="cliente.sexo" id="cliente.sexo"  disabled="%{#readOnly}" list="#{'M':'Masculino', 'F':'Femenino'}" headerValue="Seleccione..." headerKey="" label="Sexo * " labelposition="left" cssClass="cajaTextoM2 "/>
				</td>
				<td width="33%">
					<div class="elementoInline">
						<s:textfield name="cliente.codigoRFC" id="cliente.codigoRFC" disabled="%{#readOnly}" label="RFC" labelposition="left" maxlength="13" cssClass="cajaTextoM2  jQalphanumeric jQrfc-length"/>
					</div>
					<s:if test="display">
					<div class="elementoInline">
		               	<a href="javascript: void(0);" onclick='javascript:midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>Generar RFC</a>
					</div>
					</s:if>
				</td>
				<td width="33%">
					<div class="elementoInline">
						<s:if test="tipoAccion == 4">
							<s:textfield name="cliente.codigoCURP" id="cliente.codigoCURP" disabled="%{#readOnly}" label="CURP" labelposition="left" maxlength="18" cssClass="cajaTextoM2 w120"/>
						</s:if>
						<s:else>
							<s:textfield name="cliente.codigoCURP" id="cliente.codigoCURP" disabled="%{#readOnly}" label="CURP" labelposition="left" maxlength="18" cssClass="cajaTextoM2  w120"/>
						</s:else>
					</div>
					<s:if test="display">
					<div class="elementoInline" >
						<a href="http://www.renapo.gob.mx/swb/swb/RENAPO/consultacurp" target="_blank">Consulta CURP</a>
					</div>				
					</s:if>	
				</td>
				<td>
					<s:if test="display">
					<div class="btn_back w50 elementoInline">
						<a href="javascript: void(0)" onclick='javascript: validaCURPPersonaClientesJS();'>
							<s:text name="Validar"/>
						</a>
					</div>				
					</s:if>		
				</td>
			</tr>
				<tr>
					
			<td width="32%">
			<label for="cliente.ocupacionCNSF" class="small"><s:text
				name="midas.clientes.giro.cnsf" /></label>
							<s:select name="cliente.ocupacionCNSF" id="cliente.ocupacionCNSF" headerKey="" headerValue="Seleccione"
								disabled="%{#readOnly}"  list="ocupacionesCNSF" 
							listValue="actividad" listKey="id"
								cssClass="cajaTextoM2 jQrequired" />
						
		
				</td>
			<td width="31%">
				<!--  <label class="small"><s:text
				name="midas.clientes.PLD" /></label>
					 <s:textfield name="cliente.gradoRiesgo"  
					 disabled="true" id="cliente.gradoRiesgo" 
					 labelposition="left" maxlength="40" cssClass="" />
					
				
				-->
					<s:hidden name="cliente.gradoRiesgo" id="cliente.gradoRiesgo" />
							
					
				</td>
				<td width="35%"></td>
			</tr>
		</tbody>
		</table>
		
		<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		<tbody>
			<tr>
			
			<s:hidden name="cliente.domicilioTemp.cvePais" id="cliente.domicilioTemp.cvePais"/>
			<s:hidden name="cliente.domicilioTemp.cveEstado" id="cliente.domicilioTemp.cveEstado"/>
			<s:hidden name="cliente.domicilioTemp.cveCiudad" id="cliente.domicilioTemp.cveCiudad"/>
			<s:hidden name="cliente.domicilioTemp.calle" id="cliente.domicilioTemp.calle"/>
			<s:hidden name="cliente.domicilioTemp.numero" id="cliente.domicilioTemp.numero"/>
			<s:hidden name="cliente.domicilioTemp.codigoPostal" id="cliente.domicilioTemp.codigoPostal"/>
			<s:hidden name="cliente.domicilioTemp.colonia" id="cliente.domicilioTemp.colonia"/>
			
				<td class="titulo">
					Domicilio
				</td>
				
				<td>
					
					<div class="btn_back w50 elementoInline" id="bttnDomAceptar">
						<a href="javascript: void(0)" onclick='javascript: agregarDomicilio();'>
							<s:text name="Agregar"/>
						</a>
					</div>				
					<div class="btn_back w50 elementoInline" id="bttnDomCancelar" style= "display:none">
						<a href="javascript: void(0)" onclick='javascript: cancelarDomicilio();'>
							<s:text name="Cancelar"/>
						</a>
					</div>	
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.idPaisString</s:param>
						<s:param name="idEstadoName">cliente.idEstadoDirPrin</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioDirPrin</s:param>		
						<s:param name="idColoniaName">cliente.nombreColonia</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalle</s:param>
						<s:param name="numeroName">cliente.numeroDom</s:param>
						
						<s:param name="cpName">cliente.codigoPostal</s:param>
						<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferente</s:param>
						<s:param name="idColoniaCheckName">idColoniaCheck</s:param>				
						<s:param name="labelPais">Pa�s</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle</s:param>
						<s:param name="labelNumero">N�mero</s:param>
						<s:param name="labelCodigoPostal">C�digo Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">101</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
						<s:param name="idColoniaString">cliente.idColoniaStringDom</s:param>
					</s:action>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>