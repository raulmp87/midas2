<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			//jQIsRequired();
			/*
			var idPais=jQuery("#cliente\\.idPaisString").val();
			if(!jQuery.isValid(idPais)){
				jQuery("#cliente\\.idPaisString").val("PAMEXI");
			}
			*/
			if(jQuery("#txtIdRepresentanteFiscal").val()!=null && jQuery("#txtIdRepresentanteFiscal").val()!=''){
				findByIdResponsable(jQuery("#txtIdRepresentanteFiscal").val());
			}
			var estatusActivo=jQuery("#cliente\\.tipoSituacionString01").attr("checked");
			var estatusInactivo=jQuery("#cliente\\.tipoSituacionString02").attr("checked");
			//Si no tienen un valor seleccionado, entonces lo pone por default
			if(estatusActivo==false && estatusInactivo==false){
				jQuery("#cliente\\.tipoSituacionString01").attr("checked",true);
			}
			var personaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
			var personaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
			if(personaFisica==false && personaMoral==false){
				jQuery("#cliente\\.claveTipoPersonaString1").attr("checked",true);
			}
			
			//Inicializar consulta prima en nulo.
			resultadoWSPrima = null;
			
			validacionesPrimaMayor();
		});
		
		
		function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=txtIdRepresentanteFiscal";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			jQuery("#txtIdRepresentanteFiscal").val(idResponsable);
			jQuery("#txtNombreRepresentanteFiscal").val(nombre);
		}
	}
	</script>
<!--/head-->

<body>
<!-- 
<s:hidden name="divCarga"/>
-->
<s:hidden name="cliente.verificarRepetidos"   id="cliente.verificarRepetidos" value="true"/>

<s:hidden name="cliente.idClienteUnico" id="cliente.idClienteUnico" />
<s:hidden name="cliente.idCliente" id="cliente.idCliente"/>
<s:hidden name="cliente.claveTipoPersonaString" id="cliente.claveTipoPersonaString" />
<s:if test ="%{#readOnly}=='true'" > 
	<s:hidden name="cliente.idGiro" id="cliente.idGiro" />
</s:if>
<s:if test ="%{#readOnly}=='true'" > 
	<s:hidden name="cliente.cveGiroCNSF" id="cliente.cveGiroCNSF" />
</s:if>


<s:hidden name="cliente.numeroInterior" id="cliente.numeroInterior" />




	
		<table width="99%" bgcolor="white" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo" colspan="3">
					Secci�n de Datos Generales del Cliente
				</td>
			</tr>
			<tr>
				<td width="36%">
					<s:textfield id="idClienteVista" label="Id Cliente" labelposition="left" disabled="true" cssClass="cajaTextoM2" value="%{cliente.idCliente}"/>			
				</td>
				<td width="30%">
					<s:radio id="cliente.tipoSituacionString" disabled="%{#readOnly}" name="cliente.tipoSituacionString" list="#{'A':'Activo','I':'Inactivo'}" label="Tipo de Situaci�n" cssClass="cajaTextoM2 jQrequired"></s:radio>
				</td>
				<td width="34%">
					<s:radio id="cliente.claveTipoPersonaString" disabled="%{#readOnly}" name="cliente.claveTipoPersona" list="#{'1':'Persona F�sica','2':'Persona Moral'}" label="Persona Jur�dica" onclick="switchPorTipoPersona(this.value,'%{divCarga}');" cssClass="cajaTextoM2 jQrequired"></s:radio>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield name="cliente.razonSocial" disabled="%{#readOnly}" id="cliente.razonSocial" label="Raz�n Social" labelposition="left" maxlength="200" cssClass="cajaTextoM2  jQalphaextra" />
				</td>
				<td>					
					<div class="elementoInline">
						<span class="wwlbl">
							<label class="label" for="cliente.fechaConstitucion"><s:text name="Fecha de Constituci�n"/></label>
						</span>
					</div>
					<div class="elementoInline">
						<sj:datepicker name="cliente.fechaConstitucion" buttonImageOnly="true"
						  labelposition="left"  buttonImage="../img/b_calendario.gif"						   
						   id="cliente.fechaNacimiento" maxlength="10" cssClass="w100  cajaTextoM2 jQdate-mx jQrequired" 
						   onkeypress="return soloFecha(this, event, false);"  
						   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" 
						   changeMonth="true"
						   changeYear="true"
						   disabled="%{#readOnly}"
						   showOn="%{#showOnConsulta}"
						   onblur='esFechaValida(this);'>
						   </sj:datepicker>
					</div>
				</td>
				<td>
					<s:select name="cliente.idGiro" disabled="%{#readOnly}" id="cliente.idGiro" list="giros" headerKey="" headerValue="Seleccione" listValue="nombreGiro" listKey="idGiro" label="Giro * " labelposition="left" cssClass="cajaTextoM2 "></s:select>
				</td>
			</tr>
			<tr>	
			<td>
				<div style="width: 100%">
				<div class="elementoInline" style="float: left; width: 70%;">
					<s:textfield name="cliente.idRepresentante" disabled="%{#readOnly}" label="Id Representante Legal"  labelposition="left" id="txtIdRepresentanteFiscal" cssClass="cajaTextoM2  w50 jQnumeric" readonly="true" onchange="javascript:findByIdResponsable(this.value);"/>
				</div>		
				<s:if test="display">
				<div class="btn_back" style="float: right; width:25%;" >
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalResponsable();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
				</s:if>
				</div>				
			</td>			
			<td>
					<s:textfield id="txtNombreRepresentanteFiscal" disabled="%{#readOnly}" cssClass="cajaTextoM2 w200" readonly="true"/>				
			</td>        	
			</tr>		
			<tr>
				<td>
					<s:select id="cliente.claveNacionalidad" disabled="%{#readOnly}" name="cliente.claveNacionalidad" list="paises" listValue="countryName" listKey="countryId" headerValue="Seleccione..." headerKey="" label="Nacionalidad" labelposition="left" cssClass="cajaTextoM2 ">
					</s:select>
				</td>
				<td>
					<s:select id="cliente.idPaisConstitucion" disabled="%{#readOnly}" name="cliente.idPaisConstitucion" list="paises" listValue="countryName" listKey="countryId" headerValue="Seleccione..." headerKey="" label="midas.clientes.pais.constitucion" labelposition="left" cssClass="cajaTextoM2 jQrequired">
					</s:select>
				</td>
				<td>
					<div class="elementoInline">
						<s:textfield name="cliente.codigoRFC" disabled="%{#readOnly}" id="cliente.codigoRFC" label="RFC" labelposition="left" maxlength="13" cssClass="cajaTextoM2 jQalphanumeric jQrfcM-length " />
					</div>
					<s:if test="display">
					<div class="elementoInline">					
		               	<a href="javascript: void(0);" onclick='javascript:generarYDividirRFCMoral(jQuery("#cliente\\.razonSocial").val(),jQuery("#cliente\\.fechaNacimiento").val(),null,null,"cliente.codigoRFC");'>Generar RFC</a>						
					</div>
					</s:if>
				</td>
				<td>
					<s:select name="cliente.claveSectorFinanciero" disabled="%{#readOnly}" id="cliente.claveSectorFinanciero" list="sectores" headerKey="" headerValue="Seleccione" listValue="nombreSector" listKey="idSector" label="Sector" labelposition="left" cssClass="cajaTextoM2 "></s:select>
				</td>
			</tr>
			<tr>
			
			
			
				<td width="33%">
					<label for="cliente.cveGiroCNSF" class="small"><s:text
				name="midas.clientes.giro.cnsf" /></label>
					<s:select name="cliente.cveGiroCNSF" 
					disabled="%{#readOnly}" 
					id="cliente.cveGiroCNSF" list="giros" 
					headerKey="" headerValue="Seleccione" 
					list="ocupacionesCNSF" 
							listValue="actividad" listKey="id"
					 labelposition="left" cssClass="cajaTextoM2 jQrequired"></s:select>
				</td>
			<td width="33%">
						<!--  <label class="small"><s:text
				name="midas.clientes.PLD" /></label>
						 <s:textfield name="cliente.gradoRiesgo"  
					 disabled="true" id="cliente.gradoRiesgo" 
					 labelposition="left" maxlength="40" cssClass="" />
					 -->
					<s:hidden name="cliente.gradoRiesgo" id="cliente.gradoRiesgo" />
					
				</td>
				<td width="33%"> </td>
				
			</tr>
		</tbody>
		</table>
		
		<table width="99%" bgcolor="white" align="center" class="contenedorConFormato">
		<tbody>
			<tr>
				<td class="titulo">
					Domicilio
				</td>
				
				<td>
				<s:hidden name="cliente.domicilioTemp.cvePais" id="cliente.domicilioTemp.cvePais"/>
			<s:hidden name="cliente.domicilioTemp.cveEstado" id="cliente.domicilioTemp.cveEstado"/>
			<s:hidden name="cliente.domicilioTemp.cveCiudad" id="cliente.domicilioTemp.cveCiudad"/>
			<s:hidden name="cliente.domicilioTemp.calle" id="cliente.domicilioTemp.calle"/>
			<s:hidden name="cliente.domicilioTemp.numero" id="cliente.domicilioTemp.numero"/>
			<s:hidden name="cliente.domicilioTemp.codigoPostal" id="cliente.domicilioTemp.codigoPostal"/>
			<s:hidden name="cliente.domicilioTemp.colonia" id="cliente.domicilioTemp.colonia"/>	
					<div class="btn_back w50 elementoInline" id="bttnDomAceptar">
						<a href="javascript: void(0)" onclick='javascript: agregarDomicilio();'>
							<s:text name="Agregar"/>
						</a>
					</div>				
					<div class="btn_back w50 elementoInline" id="bttnDomCancelar" style= "display:none">
						<a href="javascript: void(0)" onclick='javascript: cancelarDomicilio();'>
							<s:text name="Cancelar"/>
						</a>
					</div>	
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.idPaisString</s:param>
						<s:param name="idEstadoName">cliente.idEstadoDirPrin</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioDirPrin</s:param>		
						<s:param name="idColoniaName">cliente.nombreColonia</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalle</s:param>
						<s:param name="cpName">cliente.codigoPostal</s:param>
						<s:param name="numeroName">cliente.numeroDom</s:param>	
						<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferente</s:param>
						<s:param name="idColoniaCheckName">idColoniaCheck</s:param>			
						<s:param name="labelPais">Pa�s</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle</s:param>
						<s:param name="labelNumero">N�mero</s:param>
						<s:param name="labelCodigoPostal">C�digo Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">101</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>