<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<html>
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<style>
	label{
		display:inline-block;
	}
</style>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
		});
	</script>
<!--/head-->

<body>
		<!-- 
		<s:hidden name="cliente.idToPersonaString" id="cliente.idToPersonaString"/>
		<s:hidden name="cliente.idNegocio" id="cliente.idNegocio" value="285"/>
		-->
		<table width="98%" bgcolor="white" class="contenedorConFormato" align="center" > <!-- class="contenedorFormas" -->
		<tbody>
			<tr>
				<td class="titulo" colspan="4">
					Secci�n de Datos de Contacto
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefono">Tel�fono Casa</label> 
					<%--<label for="cliente.claveTelefono">Clave Telefono casa</label>
					<label for="cliente.numeroTelefono">Numero Telefono casa</label> --%>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoCasa"/> --%>
				</td>
				<td style="width: 40px;">
					<s:textfield name="cliente.claveTelefono" disabled="%{#readOnly}" id="cliente.claveTelefono" maxlength="2" cssStyle="width: 30px;" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
				</td>
				<td style="width: 40px;">
					<s:textfield name="cliente.clave2Telefono" disabled="%{#readOnly}" id="cliente.clave2Telefono" maxlength="2" cssStyle="width: 30px;" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
				</td>
				<td>
					<s:textfield name="cliente.numeroTelefono" disabled="%{#readOnly}" id="cliente.numeroTelefono" maxlength="8" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
					<%--<s:textfield name="cliente.telefono" disabled="%{#readOnly}" id="cliente.telefono" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/> --%>			
				</td>
				<td>
					<label for="cliente.celularContacto">Celular</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoCelular"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.celularContacto" disabled="%{#readOnly}" id="cliente.celularContacto" maxlength="10" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonoOficinaContacto">Tel�fono Oficina</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoOficina"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.telefonoOficinaContacto" disabled="%{#readOnly}" id="cliente.telefonoOficinaContacto" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.extensionContacto">Extensi�n</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.extension"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.extensionContacto" disabled="%{#readOnly}" id="cliente.extensionContacto" maxlength="5" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.faxContacto">Fax</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.fax"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.faxContacto"  disabled="%{#readOnly}" id="cliente.faxContacto" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.email">Correo Electr�nico</label>
<%-- 					<s:text name="midas.catalogos.centro.operacion.correoElectronico"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.email" disabled="%{#readOnly}" id="cliente.email" maxlength="50" cssClass="cajaTextoM2 jQemail"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.facebookContacto">Facebook</label>
<%-- 					<s:text name="midas.fuerzaventa.facebook"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.facebookContacto" disabled="%{#readOnly}" id="cliente.facebookContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.twitterContacto">Twitter</label>
<%-- 					<s:text name="midas.fuerzaventa.twitter"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.twitterContacto" disabled="%{#readOnly}" id="cliente.twitterContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.paginaWebContacto">P�gina WEB</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.paginaWeb"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.paginaWebContacto" disabled="%{#readOnly}" id="cliente.paginaWebContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonosAdicionalesContacto">Tel�fonos Adicionales</label>
<%-- 					<s:text name="midas.fuerzaventa.telefonosAdicionales"/> --%>
				</td>
				<td colspan="3">
					<s:textarea name="cliente.telefonosAdicionalesContacto" disabled="%{#readOnly}" id="cliente.telefonosAdicionalesContacto" cols="28" rows="3" cssClass="cajaTextoM2"></s:textarea>
				</td>				
			</tr>
			<tr>
				<td>
					<label for="cliente.correosAdicionalesContacto">Correos Adicionales</label>
<%-- 					<s:text name="midas.fuerzaventa.correosAdicionales"/> --%>
				</td>
				<td colspan="3">
					<s:textarea name="cliente.correosAdicionalesContacto" disabled="%{#readOnly}" id="cliente.correosAdicionalesContacto" cols="28" rows="3" cssClass="cajaTextoM2"></s:textarea>
				</td>
			</tr>
		</tbody>
		</table>
</body>
</html>