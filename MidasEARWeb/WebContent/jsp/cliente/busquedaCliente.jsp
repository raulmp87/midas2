<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>
	<script type="text/javascript">
		var clientesGrid;
		var urlConsulta = "<s:url action='filtrarClientes' namespace='/busquedaCliente'/>" ;
		
		function inicializarClientesGrid() {
			clientesGrid = new dhtmlXGridObject("clientesGrid");
			
			clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
			clientesGrid.setSkin("light");
			mostrarIndicadorCarga('indicador');	
			clientesGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
		    });			
			clientesGrid.init();
			clientesGrid.load(urlConsulta);
			
		}

		function validaRows(){
				
				if(clientesGrid.getRowsNum() == 0){
					alert("No se encontraron Registros Asociados " + clientesGrid.getRowsNum());
				}
		}
		
		function consultarClientes(){
			var nombre = document.getElementById('txtNombre').value;
			var apPat = document.getElementById('txtApPat').value;
			var apMat = document.getElementById('txtApMat').value;
			var url = urlConsulta+"?cliente.nombre="+nombre+"&cliente.apellidoPaterno="+apPat+"&cliente.apellidoMaterno="+apMat;
			clientesGrid.load(url,validaRows);
		}
		
		function regresarCliente(){
			var selectedId = clientesGrid.getSelectedId();
			var ventanaClientes = parent.obtenerVentanaClientes();
			ventanaClientes.setPersonaResultado(
				clientesGrid.cells(selectedId,0).getValue(),// 			idCliente,
				clientesGrid.cells(selectedId,1).getValue(),// 			idToPersona,
				clientesGrid.cells(selectedId,2).getValue(),// 			claveTipoPersona,
				clientesGrid.cells(selectedId,3).getValue(),// 			nombre,
				clientesGrid.cells(selectedId,4).getValue(),// 			apellidoPaterno,
				clientesGrid.cells(selectedId,5).getValue(),// 			apellidoMaterno,
				clientesGrid.cells(selectedId,6).getValue(),// 			fechaNacimiento,
				clientesGrid.cells(selectedId,7).getValue(),// 			codigoRFC,
				clientesGrid.cells(selectedId,8).getValue(),// 			telefono,
				clientesGrid.cells(selectedId,9).getValue(),// 			email,
				clientesGrid.cells(selectedId,10).getValue(),// 			codigoCURP,
				clientesGrid.cells(selectedId,11).getValue(),// 			sexo,
				clientesGrid.cells(selectedId,12).getValue(),// 			idToDireccion,
				clientesGrid.cells(selectedId,13).getValue(),// 			idDomicilio,
				clientesGrid.cells(selectedId,14).getValue(),// 			nombreCalle,
				clientesGrid.cells(selectedId,15).getValue(),// 			numeroExterior,
				clientesGrid.cells(selectedId,16).getValue(),// 			numeroInterior,
				clientesGrid.cells(selectedId,17).getValue(),// 			entreCalles,
				clientesGrid.cells(selectedId,18).getValue(),// 			idColonia,
				clientesGrid.cells(selectedId,19).getValue(),// 			idColoniaString,
				clientesGrid.cells(selectedId,20).getValue(),// 			nombreColonia,
				clientesGrid.cells(selectedId,21).getValue(),// 			nombreDelegacion,
				clientesGrid.cells(selectedId,22).getValue(),// 			idMunicipio,
				clientesGrid.cells(selectedId,23).getValue(),// 			idMunicipioString,
				clientesGrid.cells(selectedId,24).getValue(),// 			idEstado,
				clientesGrid.cells(selectedId,25).getValue(),// 			idEstadoString,
				clientesGrid.cells(selectedId,26).getValue(),// 			codigoPostal,
				clientesGrid.cells(selectedId,27).getValue(),// 			descripcionEstado,
				clientesGrid.cells(selectedId,28).getValue()// 			direccionCompleta
				);
			if(clientesGrid.cells(selectedId,0).getValue()==0){
				alert("No se encontraron Registros Asociados " + clientesGrid.getRowsNum());
			
			}
			ventanaClientes.finalizaConsulta();
		}
		
		function actualizaFormulario(objRadio){
			var valor = objRadio.value;
			var nombre = document.getElementById('txtNombre');
			var apPat = document.getElementById('divApPat');
			var apMat = document.getElementById('divApMat');
			if(valor == 1){
				apPat.style.display='block';
				apMat.style.display='block';
			}
			else if (valor == 2){
				apPat.style.display='none';
				apMat.style.display='none';
			}
		}
	</script>
	<style type="text/css">
.label {
	font-weight: bold;
	text-align: left;
	color: #666666;
	font-size:11px;
}
</style>
<!--/head-->
<body>
	<s:form onsubmit="javascript:consultarClientes();return false;"  id="form">
		<s:radio list="#{'1':'F�sica', '2':'Moral'}" name="cliente.claveTipoPersona" label="Tipo de persona" 
			labelposition="left" onclick="javascript:actualizaFormulario(this);" ></s:radio>
	
		<s:textfield name="cliente.nombre" id="txtNombre" label="Nombre" labelposition="left" cssStyle="margin-left: 49px;"></s:textfield>
		<div id="divApPat" >
			<s:textfield name="cliente.apellidoPaterno" id="txtApPat" label="Apellido paterno" labelposition="left" cssStyle="margin-left: 4px;" ></s:textfield>
		</div>
		
		<div id="divApMat" >
			<s:textfield name="cliente.apellidoMaterno" id="txtApMat" label="Apellido materno" labelposition="left" ></s:textfield>
		</div>
		
		
		<s:submit value="Buscar" onclick="mostrarIndicadorCarga('indicador');" cssStyle="float:right" theme="simple"></s:submit>
		<br/>
	</s:form>
	<div id="indicador"></div>
	<div id="clientesGrid" style="width:680px;height:190px"></div>	
	
	<script type="text/javascript">
				inicializarClientesGrid();
	</script>
</body>
<!--/html-->