<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<column id="idCliente" type="ro" width="0" sort="na" hidden="true">idCliente</column>
		<column id="idToPersona" type="ro" width="0" sort="na" hidden="true">idToPersona</column>
		<column id="claveTipoPersona" type="ro" width="0" sort="na" hidden="true">claveTipoPersona</column>
		
		<column id="nombre" type="ro" width="*" sort="str" hidden="false">Nombre</column>
		<column id="apellidoPaterno" type="ro" width="170" sort="str" hidden="true">apellidoPaterno</column>
		<column id="apellidoMaterno" type="ro" width="170" sort="str" hidden="true">apellidoMaterno</column>
		<column id="fechaNacimiento" type="ro" width="0" sort="na" hidden="true">fechaNacimiento</column>
		<column id="codigoRFC" type="ro" width="90" sort="str" hidden="false">RFC</column>
		<column id="telefono" type="ro" width="70" sort="str" hidden="false"><s:text name="midas.catalogos.telefono"/></column>
		<column id="email" type="ro" width="0" sort="na" hidden="true">email</column>
		<column id="codigoCURP" type="ro" width="0" sort="na" hidden="true">codigoCURP</column>
		<column id="sexo" type="ro" width="0" sort="na" hidden="true">sexo</column>
		<column id="idToDireccion" type="ro" width="0" sort="na" hidden="true">idToDireccion</column>
		<column id="idDomicilio" type="ro" width="0" sort="na" hidden="true">idDomicilio</column>
		
		<column id="nombreCalle" type="ro" width="0" sort="na" hidden="true">nombreCalle</column>
		<column id="numeroExterior" type="ro" width="0" sort="na" hidden="true">numeroExterior</column>
		<column id="numeroInterior" type="ro" width="0" sort="na" hidden="true">numeroInterior</column>
		<column id="entreCalles" type="ro" width="0" sort="na" hidden="true">entreCalles</column>
		<column id="idColonia" type="ro" width="0" sort="na" hidden="true">idColonia</column>
		<column id="idColoniaString" type="ro" width="0" sort="na" hidden="true">idColoniaString</column>
		<column id="nombreColonia" type="ro" width="0" sort="na" hidden="true">nombreColonia</column>
		<column id="nombreDelegacion" type="ro" width="0" sort="na" hidden="true">nombreDelegacion</column>
		<column id="idMunicipio" type="ro" width="0" sort="na" hidden="true">idMunicipio</column>
		<column id="idMunicipioString" type="ro" width="0" sort="na" hidden="true">idMunicipioString</column>
		<column id="idEstado" type="ro" width="0" sort="na" hidden="true">idEstado</column>
		<column id="idEstadoString" type="ro" width="0" sort="na" hidden="true">idEstadoString</column>
		<column id="codigoPostal" type="ro" width="0" sort="na" hidden="true">codigoPostal</column>
		<column id="descripcionEstado" type="ro" width="0" sort="na" hidden="true">descripcionEstado</column>
		<column id="direccionCompleta" type="ro" width="200" sort="na" hidden="false"><s:text name="midas.catalogos.descripcion"/></column>
		
		<column id="seleccion" type="img" width="30" sort="na"></column>
	</head>
	<s:iterator value="listaClientes">
		<row id="<s:property value="idCliente" escapeHtml="false" escapeXml="true" />">
			<cell><s:property value="idCliente" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idToPersona" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="claveTipoPersona" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="apellidoPaterno" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="apellidoMaterno" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaNacimiento" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoRFC" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="telefono" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="email" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoCURP" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="sexo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idToDireccion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idDomicilio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombreCalle" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroExterior" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="numeroInterior" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="entreCalles" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idColonia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idColoniaString" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombreColonia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombreDelegacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idMunicipio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idMunicipioString" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idEstado" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idEstadoString" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoPostal" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="descripcionEstado" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="direccionCompleta" escapeHtml="false" escapeXml="true" /></cell>
			
			<cell><s:url value='/img/dhtmlxgrid/true.gif'/>^Seleccionar^javascript:regresarCliente()^_self</cell>
		</row>
	</s:iterator>
</rows>