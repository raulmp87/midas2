<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
		<column id="id" type="ro" width="0" sort="str" hidden="true" >ID</column>
		<column id="descripcion" type="ro" width="*" sort="str">Grupo</column>
		<column id="seleccion" type="img" width="30" sort="na"></column>
	</head>
	<s:if test="listaGruposCliente.size() > 0">
		<s:iterator value="listaGruposCliente">
		<row id="<s:property value="id" escapeHtml="false" />">
			<cell><s:property value="id" escapeHtml="false" /></cell>
			<cell><s:property value="descripcion" escapeHtml="false" /></cell>
			
			<cell><s:url value='/img/dhtmlxgrid/true.gif'/>^Seleccionar^javascript:regresarGrupoCliente()^_self</cell>
		</row>
	</s:iterator>
	</s:if>
	<s:else>
		<row id="1">
			<cell></cell>
			<cell>No se encontraron resultados con la busqueda especificada</cell>
	</row>
	</s:else>
</rows>