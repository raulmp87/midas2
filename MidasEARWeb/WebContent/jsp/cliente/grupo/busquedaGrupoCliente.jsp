<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>
<script src="<s:url value='/js/validaciones.js'/>"></script>

	<script type="text/javascript">
		var clientesGrid;
		var urlConsulta = "<s:url action='filtrarGruposCliente' namespace='/busquedaCliente'/>" ;
		
		function inicializarGpoClientesGrid() {
			clientesGrid = new dhtmlXGridObject("clientesGrid");
			
			clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
			clientesGrid.setSkin("light");
			
			clientesGrid.init();
			//clientesGrid.load(urlConsulta);
			
		}
		
		
		function GpoClientesGrid() {
			clientesGrid = new dhtmlXGridObject("clientesGrid");
			
			clientesGrid.setImagePath("<s:url value='/img/dhtmlxgrid/'/>");
			clientesGrid.setSkin("light");
			
			clientesGrid.init();
			clientesGrid.load(urlConsulta);
		}
		
		function consultarGrupos(){
			if(validateAll(true,'search')){
				mostrarIndicadorCarga('indicador');
				var desc = document.getElementById('txtDescripcion').value;
				var clave = document.getElementById('IdCliente').value;
				var url = urlConsulta+"?grupoCliente.clave="+clave+"&grupoCliente.descripcion="+desc;
				if(desc == '' && clave == ''){
					ocultarIndicadorCarga('indicador');
					parent.mostrarMensajeInformativo("Ingresa por lo menos un criterio de busqueda", "10");
				}else{
					jQuery('#clientesGrid').hide();
					clientesGrid.load(url,function(){
					ocultarIndicadorCarga('indicador');
					jQuery('#clientesGrid').show();
					});
				}
				
			}
		}
		
		function regresarGrupoCliente(){
			var selectedId = clientesGrid.getSelectedId();
			var ventanaClientes = parent.obtenerVentanaClientes();
			ventanaClientes.setGrupoClienteResultado(
				clientesGrid.cells(selectedId,0).getValue(),// 			id,
				clientesGrid.cells(selectedId,1).getValue()// 			descripcion,
				);
			ventanaClientes.finalizaConsulta();
		}
	</script>
<!--/head-->
<style type="text/css">
label {
	font-weight: bold;
	text-align: left;
	color: #666666;
	font-size:11px;
}
</style>
<body>
<s:form theme="simple">
<table id="filtros" style="width: 670px;table-layout: fixed;">
	<tr>
		<td width="130px;"><label>Clave:</label></td>
		<td><s:textfield name="grupoCliente.clave"  maxlength="10"   id="IdCliente" label="Clave" labelposition="left"
				cssClass="jQnumeric"
				onkeypress="return soloNumeros(this, event, false)"
				/></td>
	</tr>
	<tr>
		<td><label>Nombre del Grupo:</label></td>
		<td><s:textfield name="midas.negocio.grupocliente.descripcion" id="txtDescripcion" label="Descripcion" labelposition="left"/></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>
			<div class="btn_back w110" style="float: right;">
				<a href="javascript: void(0);" class="icon_buscar"
					onclick="javascript:consultarGrupos();">
					<s:text name="midas.boton.buscar"/>
				</a>
			</div>
		</td>
	</tr>
</table>
</s:form>
	<div id="indicador"></div>
	<div id="clientesGrid" style="width:680px;height:190px"></div>	
	<script type="text/javascript">
		inicializarGpoClientesGrid();
	</script>
</body>
<!--/html-->