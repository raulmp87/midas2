<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>       
		<column id="nombre" type="ro" width="257" sort="str" >Archivo</column>
		<column id="descripcion" type="ro" width="257" sort="str">Descripcion</column>
		<column id="digitalizado" type="ro" width="135" sort="str">Digitalizado</column>
	</head>
	<s:iterator value="listaArchivos" var="c" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="digitalizado" escapeHtml="false" escapeXml="true"/></cell>
			  
				
		</row>
	</s:iterator>
	
</rows>




