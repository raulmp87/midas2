<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:form id="adminArchivosForm" name="adminArchivosForm"  class="floatLeft">
<div id="contenido_admin" style="width:99%;position: relative;">
<!-- HIDDEN -->
	<!-- <input id="idFortimax"  name="idFortimax" value="true" type="hidden"/> -->
	<s:hidden name="subCarpeta" id="subCarpeta"/>
	<s:hidden name="liga" id="liga"/>
	<s:hidden name="docFortimax" id="docFortimax"/>
	<s:hidden name="listaDocProceso" id="listaDocProceso"/>
	<s:hidden name="listaArchivos" id="listaArchivos"/>
	<s:hidden name="tituloVentana" id="tituloVentana"/>
	<s:hidden name="cveProceso" id="cveProceso"/>
	
	<s:hidden name="idFortimax" id="idFortimax"/>	
			<div class="titulo" align="left" >
			<s:text  name="tituloVentana" />
			</div>
	<div id="divInferior" style="width: 100% !important;" class="floatLeft">	
			
			<div id="divGenerales" style="width: 100%;  class="floatLeft">
				<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 99%; height: 50px;">
					
					<div class="divFormulario">
						<div class="floatLeft divInfDivInterno" style="width: 40%; " >
								<s:textfield id="folioExpediente" name="folioExpediente"  cssClass="txtfield" label="Expediente" labelposition="left" cssStyle="" readonly="true" ></s:textfield>
								
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 20%; padding-top: 10px" >
								<div class="btn_back w130"
								style="display: inline; float: left;">
								<a href="javascript: void(0);" onclick="verExpediente( jQuery('#cveProceso').val() ,  jQuery('#subCarpeta').val() , jQuery('#tituloVentana').val() , jQuery('#idFortimax').val() );"> <s:text
										name="Ver Expediente" />&nbsp;&nbsp;<img
									align="middle" border='0px' alt='Consultar'
									title='Ver Expediente' src='/MidasWeb/img/common/b_details.gif'
									style="vertical-align: left;" /> </a>
								</div>
						
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 25%; padding: 10px 0px 0px 10px" >
								<div class="btn_back w100"
								style="display: inline; float: left;">
								<a href="javascript: void(0);" onclick="iniContenedorGrid(jQuery('#cveProceso').val() ,jQuery('#subCarpeta').val(), jQuery('#tituloVentana').val() ,jQuery('#idFortimax').val() );"> <s:text
										name="Actualizar" />&nbsp;&nbsp;<img
									align="middle" border='0px' alt='Consultar'
									title='Actualizar' src='/MidasWeb/img/common/b_refrescar.gif'
									style="vertical-align: left;" /> </a>
								</div>
						
						</div>
						
						
					</div>
					
				</div>		
			</div>
			
	</div> 
	<div id="divInferior" style="width: 100% !important; padding-top: 10px" class="floatLeft">
				
			 	<div id="archivosGrid" class="dataGridConfigurationClass" style="width: 100%; height: 200px; "></div>
				<div id="pagingArea"></div><div id="infoArea"></div>			
	</div>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {
		iniContenedorGrid(  jQuery("#cveProceso").val(), jQuery("#subCarpeta").val(),  jQuery("#tituloVentana").val() .value,jQuery("#idFortimax").val());
	});
</script>		
</div>
</s:form>