<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:iterator value="promotoriasDTOs">
	<item>
		<id><s:property value="idPromotoria"/></id>
		<descripcion><s:property value="descripcion" escapeHtml="false" escapeXml="true" /></descripcion>
	</item>
	</s:iterator>
</resultados>
