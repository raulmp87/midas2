<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>	 	
        </beforeInit>
        <column id="cotizacion.idToCotizacion" type="ro" width="130" sort="int" hidden="false"><s:text name="midas.suscripcion.cotizacion.idtocotizacion"/></column>
        <column id="cotizacion.tipoPolizaDTO.claveAplicaFlotillas" type="ro" width="*" sort="int" hidden="true">cotizacion.tipoPolizaDTO.claveAplicaFlotillas</column>
		<column id="cotizacion.nombreContratante" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.nombreProspecto"/></column>
		<column id="cotizacion.codigoUsuarioCotizacion" type="ro" width="130" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.usuario"/></column>
		<column id="cotizacion.fechaCreacion" type="dhxCalendar" width="130" sort="date" hidden="false"><s:text name="midas.suscripcion.cotizacion.fecha"/></column>
		<column id="descripcionEstatus" type="ro" width="130" sort="str"><s:text name="midas.suscripcion.cotizacion.estatus"/></column>
		<column id="solicitud" type="ro" width="130" sort="str"><s:text name="midas.negocio.centroOperacion"/></column>
		<column id="descuento" type="ro" width="130" sort="int"><s:text name="midas.suscripcion.cotizacion.descuentoGrid"/></column>
		<column id="numeropoliza" type="ro" width="198" sort="int"><s:text name="midas.poliza.numeroPoliza"/></column>
		<column id="estatuspoliza" type="ro" width="130" sort="int"><s:text name="midas.suscripcion.cotizacion.estatusPoliza"/></column>
		<column id="valorPrimaTotal" type="edn" width="130" sort="int" format="$0,000.0" ><s:text name="midas.cotizacion.primatotal"/></column>
		<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
	    <column id="accion1" type="img" width="30" sort="na"/>
	    <column id="accion2" type="img" width="30" sort="na"/>
		<column id="accion3" type="img" width="30" sort="na"/>
		<column id="accion4" type="img" width="30" sort="na"/>
		<column id="accion5" type="img" width="30" sort="na"/>
		<column id="accion6" type="img" width="30" sort="na"/>
		<column id="accion7" type="img" width="30" sort="na"/>
		<column id="accion8" type="img" width="30" sort="na"/>
		<column id="accion9" type="img" width="30" sort="na"/>
		
	</head>
	<% int a=0;%>
	<s:iterator value="cotizacionList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell id="cotizacion.idToCotizacion"><s:property value="idToCotizacion" escapeHtml="false"/></cell>
			<cell><s:property value="tipoPolizaDTO.claveAplicaFlotillas" escapeHtml="false"/></cell>
			<cell><![CDATA[<s:property value="nombreContratante"/>]]></cell>
			<cell><s:property value="codigoUsuarioCotizacion" escapeHtml="false" /></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false"/></cell>
			<cell><![CDATA[<s:property value="centroEmisorDescripcion"/>]]></cell>
			<cell><s:property value="descuendoAplicadoCotizacion" escapeHtml="false"/></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false"/></cell>
			<cell><s:property value="estatusPoliza" escapeHtml="false" /></cell>
			<cell><s:property value="valorPrimaTotal" escapeHtml="false"/></cell>
	        <s:if test="(tipoPolizaDTO.claveAplicaFlotillas== \"0\") && (claveEstatus == 10 || claveEstatus == 12)">
	        	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizador_Agentes">
		        	<s:if test="(folio == \"C_N_COTIZADOR\") || (folio.split('_')[0] == \"TSSO\")">
						<cell>../img/icons/ico_editar_agente.gif^Editar Agente^javascript: cargarNuevoCotizador(<s:property value="idToCotizacion"/>,<s:property value="claveEstatus"/>)^_self</cell>
		        	</s:if>
		        	<s:else>
						<cell>../img/icons/ico_editar_agente.gif^Editar Agente^javascript: iniciarCotizadorAgentes(<s:property value="idToCotizacion"/>)^_self</cell>
					</s:else>
				</m:tienePermiso>
		    </s:if>
			<s:if test="claveEstatus == 10 || claveEstatus == 11" >
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Editar">
					<cell>../img/icons/ico_editar.gif^Editar^javascript: verDetalleCotizacionContenedor(<s:property value="idToCotizacion"/>,<s:property value="claveEstatus"/>)^_self</cell>		
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Asignar">
					<cell>../img/icons/ico_asignar.gif^Reasignar^javascript: mostrarVentanaReasignarCotizacion(<s:property value="idToCotizacion"/>)^</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus == 12">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Asignar">
				<cell>../img/icons/ico_asignar.gif^Reasignar^javascript: mostrarVentanaReasignarCotizacion(<s:property value="idToCotizacion"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Complementar">
				<cell>../img/complementar.png^Complementar Cotizacion^javascript: verComplementarCotizacion(<s:property value="idToCotizacion"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Copiar">
				<cell>../img/icons/ico_copiar1.gif^Copiar Cotizacion^javascript: TipoAccionDTO.getVer(copiarCotizacionAuto(<s:property value="idToCotizacion"/>));'^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir">
				<cell>../img/b_printer.gif^Imprimir Cotizacion^javascript: mostrarContenedorImpresion(1,<s:property value="idToCotizacion" escapeHtml="false"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus == 14">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Emitir">
				<cell>../img/confirmAll.gif^Emitir Cotizacion^javascript: verComplementarCotizacion(<s:property value="idToCotizacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
	        <s:if test="tipoPolizaDTO.claveAplicaFlotillas== \"1\" && (claveEstatus == 10 || claveEstatus == 11)">
	        	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cambios_Globales">
		    	<cell>../img/icons/cambioGlobal.png^Cambios Globales^javascript: mostrarVentanaCambiosGlobales(<s:property value="idToCotizacion"/>)^_self</cell>
		    	</m:tienePermiso>
		    </s:if>
			<s:if test="claveEstatus == 14 || claveEstatus == 15">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Enviar_Por_Email">
				<cell>../img/icons/email.png^Enviar por Correo^javascript: ventanaCorreo(<s:property value="idToCotizacion"/>)^_self</cell>	
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir">	
				<cell>../img/b_printer.gif^Imprimir Cotizacion^javascript: mostrarContenedorImpresion(1,<s:property value="idToCotizacion" escapeHtml="false"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
            <s:if test="claveEstatus == 12 || claveEstatus == 14">
            	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cambiar_A_Estado_Proceso">
				<cell>../img/icons/ico_regresar.gif^Cambiar Estado a En Proceso^javascript: cambiarEstatusEnProceso(<s:property value="idToCotizacion"/>)^_self</cell>		
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus != 19">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Formas_Pago">
				<cell>../img/esquemaPagos.jpg^Esquema de Pagos^javascript:abrirEsquemaPagos(<s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>

			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Comentarios">
			<cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript:abrirComentarios(<s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/>,1)^_self</cell>
			</m:tienePermiso>
			<s:if test="claveEstatus == 16">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir">	
				<cell>../img/b_printer.gif^Imprimir Cotizacion^javascript: mostrarContenedorImpresion(1,<s:property value="idToCotizacion" escapeHtml="false"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
            <s:if test="tipoPolizaDTO.claveAplicaFlotillas== \"1\" && (claveEstatus == 10 || claveEstatus == 12)">
            	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Carga_Masiva">
				<cell>/MidasWeb/img/cargaMasiva.png^Carga Masiva^javascript:abrirCargaMasiva(<s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/>,1)^_self</cell>		
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus != 16 && claveEstatus != 19">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cancelar">		
				<cell>/MidasWeb/img/cross16.png^Cancelar Cotizacion^javascript:abrirComentarios(<s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/>,2)^_self</cell>				
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus == 19">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Editar">
				<cell>../img/icons/ico_editar.gif^Ver detalle^javascript: verDetalleCotizacionContenedorSoloLectura(<s:property value="idToCotizacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
		</row>
	</s:iterator>
</rows>