<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>

<sj:head/>
<script type="text/javascript">jQuery.noConflict();</script>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>

<script type="text/javascript">
function $_mostrarIndicador(){
	jQuery('#destinatario').hide();
	jQuery('#correo').hide();
	jQuery('#btn-submit').hide();
	mostrarIndicadorCarga('indicador');
}
$(document).ready(function() {
	 
    $('#btn-submit').click(function() { 
        $(".error").hide();
        var hasError = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 
        var emailaddressVal = jQuery('#UserEmail').val().split(";");
        if(emailaddressVal == '') {	
            $("#btn-submit").before('<span class="error">Proporcione la direcci&oacute;n de correo</span>');
            hasError = true;
        }
 
         for(i = 0; i < emailaddressVal.length; i++){  
	         if(!emailReg.test(emailaddressVal[i])) { 	
	           jQuery("#btn-submit").before('<span class="error">Proporcione una direcci&oacute;n v&aacute;lida</span> ');
	           hasError = true;
	          break;
	         }
	        }
	       
	       /*for(i = 0; i < emailaddressVal.length; i++){
	         if(!emailReg.test(emailaddressVal[i])) {
	        	jQuery("#UserEmail").after('<span class="error">'+[emailaddressVal[i]]+',</span> \n' );
	            hasError = true;
	         }
	       }*/
 
        if(hasError == true) { 
        	jQuery('#destinatario').show();
        	jQuery('#correo').show();
        	jQuery('#btn-submit').show();
        	ocultarIndicadorCarga('indicador');
        	return false; }
 
    });
});
</script>
<s:form action="enviarCotizacionPorCorreo" namespace="/impresiones/poliza" id="imprimirPoliza" >
<s:hidden name="idToCotizacion" id="idToCotizacion" />
<center>
	<table id="agregar" width="100%">	
	<tr><td><div id="indicador"></div></td></tr>
		<tr>
			<td id="destinatario">
				<s:textfield labelposition="left" key="midas.correo.destinatario" name="nombreDestinatario" size="50" />			
			</td>
		</tr>	
		<tr>			
			<td id="correo">
				<s:textarea key="midas.suscripcion.solicitud.solicitudPoliza.enviarCotizacion"
							id="UserEmail" name="correo"  cssClass="cajaTextoM2 jQemail jQrequired" cssStyle="font-size:11px;"
							title="Correos electronicos separados por punto y coma"
							alt="Correos electronicos separados por punto y coma"
							rows="2" cols="45"  />				
			</td>
		</tr>
		<tr>
			<td colspan="2">				
				<s:submit id="btn-submit" value="Enviar"
				cssClass="b_submit" onclick="$_mostrarIndicador();"/>	
			</td>			
		</tr>	    
	</table>
</center>
</s:form>
