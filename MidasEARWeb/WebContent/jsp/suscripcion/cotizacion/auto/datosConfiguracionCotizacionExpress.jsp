<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/suscripcion/cotizacion/cotizacionHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>

<div id="cotizacionExpresDiv" style="width:95%;">
	<center>
		<jsp:include
			page="/jsp/suscripcion/cotizacion/auto/matrizDatosConfiguracionCotizacionExpress.jsp" />

		<s:if test="cotizacionExpressDTO.id != null">
			<div id="imprimeCotizacion" class="btn_back w170"
				style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="imprimirCotizacionExpress();"
					class="icon_imprimir "> <s:text
						name="midas.cotizacion.imprimircotizacion" /> </a>

			</div>
			<div id="enviaEmail" class="btn_back w140"
				style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="enviarCorreoCotizacionExpress();"
					class="icon_email"> <s:text name="midas.cotizacion.enviarmail" />
				</a>
			</div>
		</s:if>
	</center>
</div>