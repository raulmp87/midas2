<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"
          prefix="fmt" %>
<fmt:setLocale value="es_MX"/>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
		<column id="nombreCobertura" type="ro" width="*"><s:text name="midas.general.cobertura"/></column>
		<column id="sumaAsegurada" type="ro" width="150" align="right"><s:text name="midas.general.sumaasegurada"/></column>
		<column id="deducible" type="ro" width="100" align="right"><s:text name="midas.general.deducible"/></column>
		<column id="primaNeta" type="ro" width="100" align="right"><s:text name="midas.general.primaneta"/></column>
	</head>
	<s:iterator value="coberturaCotizacionExpressList">
		<row id="${coberturaPrimaCotExpress.id}">
			<cell>${coberturaPrimaCotExpress.cobertura.nombreComercial}</cell>
			<cell><c:catch var="e"><fmt:formatNumber value="${sumaAseguradaStr}" type="currency"/></c:catch><c:if test="${e!=null}">${sumaAseguradaStr}</c:if></cell>
			<cell><s:property value="%{getText('struts.percent.format',{coberturaPrimaCotExpress.pctDeducible})}" escapeHtml="false" escapeXml="true"/></cell>
			<cell><fmt:formatNumber value="${coberturaPrimaCotExpress.primaNetaARdt}" type="currency"/></cell>
		</row>
	</s:iterator>
</rows>