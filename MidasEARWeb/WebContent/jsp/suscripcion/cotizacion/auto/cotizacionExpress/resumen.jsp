<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"/>
<%-- <link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css"> --%>
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<style>
.fixIEGrid {
	height: 275px;
	position: relative;
	width: 785px;
}

#h1,#h2,#h3 {
	margin-bottom: 5px;
}

#h1 {
	float: left;
	width: 360px;
	position: relative;
}

#h2 {
	float: left;
	margin-left: 10px;
	margin-right: -3px;
	width: 400px;
	position: relative;
}

#h3 {
	width: 400px;
	height: 20px;
	float: right;
	position: relative;
}

#nav {
	float: left;
	width: 100%;
	height: 80px;
	margin-bottom: 5px;
	margin-top: -15px;
	clear: both;
	position: relative;
}

#content {
	width: 780%;
}

.footer {
	clear: both;
	height: 50px;
	width: 700px;
	margin-bottom: 10px;
}

<!--
.divTop {
	padding-bottom: 40px;
	padding-left: 5px;
}

.divIn {
	width: 40%;
}

.divLast {
	width: 50%;
	margin-top: -109px;
	margin-left: 400px;
}

.buttonAddAgente {
	margin-left: 50px;
	float:right;
	margin-right:200px;
	margin-top: -63px;
}
-->
</style>
<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
<s:url action="mostrarCoberturaCotizacionDhtmlx" var="mostrarCoberturaCotizacionDhtmlxUrl" escapeAmp="false" >
	<s:param name="id"><s:property value="id"/> </s:param>
	<s:param name="idPaquete" ><s:property value="idPaquete"/> </s:param>
	<s:param name="idFormaPago" ><s:property value="idFormaPago"/> </s:param>
</s:url>
<script type="text/javascript">
	var cotizarFormalmenteUrl = '<s:url action="cotizarFormalmente"/>';
	var mostrarCoberturaCotizacionDhtmlxUrl = '<s:property value="#mostrarCoberturaCotizacionDhtmlxUrl"/>';
	var opcionCapturaPath = '<s:url action="mostrar" namespace="/suscripcion/solicitud"/>';
</script>
</head>
<s:form id="resumenForm" action="cotizarFormalmente" theme="simple">
<s:hidden name="id" value="%{id}"/>
<s:hidden name="idPaquete" id="idPaquete"/>
<s:hidden name="idFormaPago" id="idFormaPago"/>
<s:hidden name="agente.id" id="idAgenteCot"/>
	<div id="container" class="grid">
		<div id="content" role="main">
			<div class="row">
				<div class="c1" style="width: 350px;">
					<div class="subtituloIzquierdaDiv" style="310px;">Información del Usuario</div>
					<table id="agregar" style="width: 300px;">
						<tr>
							<th><label>Id Usuario</label></th>
							<td><s:property value="usuario.id" /></td>
						</tr>
						<tr>
							<th><label>Nombre Usuario</label></th>
							<td><s:property value="usuario.nombre" /></td>
						</tr>
					</table>
				</div>
				<div class="c1" style="width: 350px;">
					<s:action name="getInfoAgente" var="agenteInfo"
						namespace="/componente/agente" ignoreContextParams="true"
						executeResult="true">
						<s:param name="idAgenteCompName">agente.id</s:param>
						<s:param name="idTcAgenteCompName">cotizacion.solicitudDTO.codigoAgente</s:param>
						<s:param name="nombreAgenteCompName">agente.persona.nombre</s:param>
						<s:param name="permiteBuscar">
							<s:property value="#search" />
						</s:param>
					</s:action>
				</div>
			</div>
			<div class="row">
				<div class="" style="margin-left: 600px">
					<s:if test="permiteBuscar">
						<div id="divBuscarBtn" class="w180" style="display: block;">
							<div class="btn_back w170">
								<a href="javascript: void(0);"
									onclick="parent.seleccionarAgenteCotizacion(2);"
									class="icon_persona"> <s:text
										name="midas.cotizacion.seleccionaragente" /> </a>
							</div>
						</div>
					</s:if>
				</div>
			</div>
				<div class="row">
				<div style="margin-left: 600px;">
					<div class="btn_back w170">
						<a href="javascript: void(0);" onclick="cotizarFormalmente()"
							class="icon_buscar"> <s:text
								name="midas.suscripcion.cotizacion.generarSolicitud" /> </a>
					</div>
				</div>
			</div>
			<!-- FIN Botones -->
			<div class="row" style="width: 780px;">
				<table id="agregar">
					<tr>
						<th><s:text name="midas.cotizacion.express.lineanegocio" />
						</th>
						<td><s:property
								value="cotizacionExpressDTO.negocioSeccion.seccionDTO.descripcion" />
						</td>
					</tr>
					<tr>
						<th><s:text name="midas.general.paquete" />
						</th>
						<td><s:property
								value="cotizacionExpressDTO.paquete.descripcion" />
						</td>
					</tr>
				</table>
			</div><!-- FIN INFO NEGOCIO -->
			<div class="row">
				<div id="coberturaCotizacionExpressGrid" class="fixIEGrid"></div>
			</div>
		</div>
		<!-- Fin Main -->
	</div>
	<!-- Fin Grid -->

<br/>
</s:form>
<script>inicializarCotizacionExpressResumen()</script>