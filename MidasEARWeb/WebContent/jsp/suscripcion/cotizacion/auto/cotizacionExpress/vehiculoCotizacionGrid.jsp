<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>		
		</beforeInit>
		
		<column id="cotizacionDTO.incisoCotizacionDTOs.id" type="ro" width="*"><s:text name="midas.cotizacion.numeroinciso" /></column>
		<column id="" type="ro" width="*" ><s:text name="midas.cotizacion.descripcionvehiculo" /></column>
		<column id="cotizacionDTO.primaNetaTotal" type="ro" width="*" ><s:text name="midas.cotizacion.primatotal" /></column>
		<column id="cotizacionDTO.claveEstatus" type="ro" width="*" ><s:text name="midas.cotizacion.estatus" /></column>
		<column id="definirInciso" type="ro" width="70" align="center">Acciones</column>
		<column id="multiplicarInciso" type="ro" width="30" />
		<column id="BorrarInciso" type="ro" width="30" />		
	</head>
		
	<% int a=0;%>
	<s:iterator value="vehiculo">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNetaTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveEstatus.descripcion" escapeHtml="false" escapeXml="true"/></cell>	
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: alert("Definir Inciso")' href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/autoEdit.jpg' title='Definir Inciso' alt='Definir Inciso'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: 
					 		mostrarMultiplicarInciso(<s:property value="id.numeroInciso" escapeHtml="false" escapeXml="true"/>,
					 			    				 <s:property value="id.idToCotizacion" escapeHtml="false" escapeXml="true"/>);' 
					 			    				 href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/autos.png' title='Multiplicar Inciso' alt='Multiplicar Inciso'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: alert("Borrar Inciso")' href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/autoBorrar.jpg' title='Borrar Inciso' alt='Borrar Incisor'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>			
		</row>
	</s:iterator>	
	
</rows>
