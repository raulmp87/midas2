<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
	var mostrarResumenCotizacionExpressUrl = '<s:url action="mostrarResumen"/>';
	var guardarCotizacionExpressUrl = '<s:url action="guardar"/>';
	var mostrarMatrizDatosCotizacionExpressUrl = '<s:url action="mostrarMatrizDatos"/>';
</script>
<%-- <script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script> --%>
<s:form id="cotizacionExpressForm" action="guardar">
	<div class="titulo" style="width: 98%;">
		<s:text name="Cotizador Express Autos"/>	
	</div>
<s:hidden name="id" id="id"/>
	
<s:action name="getDatosVehiculo" var="getDatosVehiculo" namespace="/componente/vehiculo" ignoreContextParams="true" executeResult="true" >
	<s:param name="cotizacionExpress">true</s:param>
	<s:param name="idEstadoName">cotizacionExpressDTO.estado.stateId</s:param>	
	<s:param name="idMunicipioName">cotizacionExpressDTO.municipio.municipalityId</s:param>
	<s:param name="idMonedaName">cotizacionExpressDTO.idMoneda</s:param>
	<s:param name="idNegocioSeccionName">cotizacionExpressDTO.negocioSeccion.idToNegSeccion</s:param>
	<s:param name="idMarcaVehiculoName">cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.marcaVehiculoDTO.idTcMarcaVehiculo</s:param>		
	<s:param name="idEstiloVehiculoName">cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.id.strId</s:param>
	<s:param name="idModeloVehiculoName">cotizacionExpressDTO.modeloVehiculo.id.modeloVehiculo</s:param>			
	<s:param name="checkEsFronterizoName">cotizacionExpressDTO.frontera</s:param>
	<s:param name="descripcionFinalName">cotizacionExpressDTO.modeloVehiculo.estiloVehiculoDTO.descripcionEstilo</s:param>
	<s:param name="modificadoresDescripcionName">cotizacionExpressDTO.variableModificadoraDescripcion</s:param>
	<s:param name="idExpressName">id</s:param>		
</s:action>	
<jsp:include page="/jsp/suscripcion/cotizacion/auto/datosConfiguracionCotizacionExpress.jsp"/>
</s:form>