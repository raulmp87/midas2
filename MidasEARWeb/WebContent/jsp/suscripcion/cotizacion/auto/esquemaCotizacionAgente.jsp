<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="detalle" style="overflow:auto;font-size: 10px; border: 1px solid #B2DBB2; background-color: #FFFFFF; height:auto">
	<table id="t_riesgo" style="width:100%;border: none" >
		<thead>
			<tr>
				<th style="width:30%"><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.formadePago" /></th>				
				<th style="width:20%">
				  <s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoInicial" /> 
				</th>				
				<th style="width:20%">
				  <s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.cantidad" />
				</th>				
				<th style="width:30%">
							<s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoSubsecuente" />
				</th>	
			</tr>
		</thead>
	</table>
	<div id="detalle_t_riesgo" style="overflow:auto;height:100px">
		<table id="t_riesgo" style="width:100%; border: none; border-collapse: collapse;">
			<s:iterator value="esquemaPagoCotizacionList" status="stats">
				<tr>
					<td style="width:30%"><s:property value="formaPagoDTO.descripcion" /></td>
					<td style="width:20%" align="right"><s:property value="pagoInicial.importeFormat" /></td>
					<td style="width:20%" align="center">
						<s:if test="pagoSubsecuente.numExhibicion != null">
							<s:property value="pagoSubsecuente.numExhibicion" />
						</s:if>
						<s:else>
							0
						</s:else>
					</td>
					<td style="width:30%" align="right">
						<s:if test="pagoSubsecuente.importeFormat != null">
							<s:property value="pagoSubsecuente.importeFormat" />
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</td>
				</tr>
			</s:iterator>  
		</table>
	</div>
	<div style="color: #FFFFFF; background-color: #66B766; background-image: url(../img/bg_tabla.jpg); background-repeat: repeat-x; background-position: bottom; padding: 4px; margin: 2px; text-align: left">* Estimaci&oacute;n para las distintas formas de pago sobre las condiciones originales.</div>
</div>