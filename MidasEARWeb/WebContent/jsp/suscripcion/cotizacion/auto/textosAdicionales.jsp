<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/suscripcion/cotizacion/cotizacionHeader.jsp"></s:include>
	<s:form id="cotizacionForm">
    <s:hidden name="fecha" id="fecha" />
    <s:hidden id="idToCotizacion" name="cotizacion.idToCotizacion"/>
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.title"/>
			</td>
		</tr>	
		<tr>
			<td colspan="8">
				<div id="contenido_textosAdicionalesGrid" class="dataGridQuotationClass" ></div>
			</td>
		</tr>
		<tr>
			<s:if test="soloConsulta == 0"> 
			<td colspan="6">
				<div id="b_agregar" style="display:none;">
					<a href="javascript: void(0);"
						onclick="javascript: agregarFilaGrid();"><s:text
							name="midas.boton.agregar" />
					</a>
				</div>
			</td>
			<td colspan="6">
				<div id="b_borrar">
					<a href="javascript: void(0);"
						onclick="javascript: eliminarRow();"><s:text name="midas.boton.borrar"/></a>
				</div>
			</td>
			</s:if>
		</tr>
		<tr>
			<td colspan="6">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.leyenda"/>
			</td>
		</tr>
	</table>	
</s:form>
<script  type="text/javascript">
	obtenertexAdicionalGrid();
</script>