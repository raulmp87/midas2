<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/autoexpedibles/autoExpediblesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="autoexpediblesdetalleForm">
	<s:hidden name="idToAutoExpediblesAutoCot" id="idToAutoExpediblesAutoCot" />
	<s:hidden name="idToNegocio" id="idToNegocio" />
	<s:hidden name="idToNegProducto" id="idToNegProducto" />
	<s:hidden name="idToNegTipoPoliza" id="idToNegTipoPoliza" />
	<s:hidden name="autoExpediblesAutoCot.claveTipo" id="claveTipo" />
	
	<div class="titulo"  style="width: 98%;">
		<s:if test="autoExpediblesAutoCot.claveTipo == 0">
		<s:text name="midas.cotizacion.cargamasivaindividual.resumenCotizacion" />
		</s:if>
		<s:else>
		<s:text name="midas.cotizacion.cargamasivaindividual.resumenPoliza" />
		</s:else>
	</div>
</s:form>
<div id="indicadorGrid"></div>
<div id="autoExpediblesDetalleGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div  style="width: 98%;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" class="icon_limpiar"
				onclick="regresarAAutoExpedibles();"> <s:text
					name="midas.boton.regresar" /> </a>
		</div>
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" class="icon_limpiar"
				onclick="descargarAutoExpedibles(<s:property value="autoExpediblesAutoCot.idToControlArchivo"/>);"> <s:text
					name="midas.cotizacion.cargamasivaindividual.descargarExcel" /> </a>
		</div>
</div>
<script type="text/javascript">
	iniciaListadoDetalleAutoExpedibles();
</script>
