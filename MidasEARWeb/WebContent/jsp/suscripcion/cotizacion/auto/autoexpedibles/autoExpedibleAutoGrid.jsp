<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idToAutoExpediblesAutoCot" type="ro" width="*" sort="int" hidden="true">id</column>
        <column id="archivo" type="ro" width="*" sort="str" hidden="false"><s:text name="midas.suscripcion.solicitud.comentarios.archivo" /></column>
      	<column id="fecha" type="ro" width="120" format="%d/%m/%Y %H:%i" sort="date" hidden="false"><s:text name="midas.cotizacion.cargamasiva.fechaCarga" /></column>
		<column id="usuario" type="ro" width="100" sort="str"><s:text name="midas.cotizacion.cargamasiva.usuario" /></column>
		<column id="estatus" type="ro" width="100" sort="str"><s:text name="midas.cotizacion.cargamasiva.estatus" /></column>
		<column id="resumen" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones" /></column>
	</head>
	<s:iterator value="autoExpediblesList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idToAutoExpediblesAutoCot" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="controlArchivo.nombreArchivoOriginal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaCreacion" format="dd/MM/yyyy HH:mm" /></cell>	
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>		
			<cell>../img/icons/ico_verdetalle.gif^Resumen^javascript:mostrarResumenAutoExpedibles(<s:property value="idToAutoExpediblesAutoCot" />);^_self</cell>
		</row>
	</s:iterator>
</rows>