<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/autoexpedibles/autoExpediblesHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<script type="text/javascript">
	mostrarIndicadorCarga("indicador");
</script>
<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
<div hrefmode="ajax-html" style="height: 450px; width: 920px" id="autoExpediblesTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Autoexpedibles_Cotizacion">
	<div width="200px" id="detalle" name="<s:text name="midas.cotizacion.autoexpedibles.autoExpediblesCotizacion" />" href="http://void" extraAction="javascript: verAutoExpediblesCotizacion();">
	</div>
	</m:tienePermiso>
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Autoexpedibles_Emision">
	<div width="200px" id="autoExpediblesEmision" name="<s:text name="midas.cotizacion.autoexpedibles.autoExpediblesEmision" />" href="http://void" extraAction="javascript: verAutoExpediblesEmision();"></div>
	</m:tienePermiso>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>