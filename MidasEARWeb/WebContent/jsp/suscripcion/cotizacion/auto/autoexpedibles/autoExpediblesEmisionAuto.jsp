<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/autoexpedibles/autoExpediblesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:form id="autoexpediblesForm">
	<s:hidden name="claveTipo" id="claveTipo" value="1"/>	
	<div class="titulo"  style="width: 98%;">
		<s:text name="midas.cotizacion.cargamasiva.seleccioneArchivo" />
	</div>
	<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<div class="btn_back w150" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(1);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importar" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w170" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(2);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importarTemporizado" /> </a>
				</div>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
			
					<br><s:text name="midas.carga.masiva.texto.1"/> </br>
					<br><s:text name="midas.carga.masiva.texto.2"/> </br>
					<br><s:text name="midas.carga.masiva.texto.3"/></br>
					<s:checkbox id="idAcurdoAfirmeMasiva" name="temp" />
				
			
			</td>
		</tr>
	</table>
</div>
</s:form>
<div id="indicadorGrid"></div>
<div id="autoExpediblesGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<script type="text/javascript">
	descargarLogAutoExpedibles();
	iniciaListadoAutoExpedibles(1);
</script>
