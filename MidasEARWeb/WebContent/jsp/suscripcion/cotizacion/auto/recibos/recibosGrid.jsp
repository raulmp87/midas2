<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
        </beforeInit>
        <!-- <column id="reciboSeycos.id.idRecibo" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>  -->
        <column id="reciboSeycos.cveTipoRecibo" type="ro" width="80" sort="str" hidden="false"><s:text name="midas.impresiones.recibos.tipo"/></column>
        <column id="reciboSeycos.numeroFolio" type="ro" width="80" sort="str" hidden="false"><s:text name="midas.impresiones.recibos.folio"/></column>
        <column id="reciboSeycos.fechaInicioVigencia" type="ro" width="80" sort="date_custom" hidden="false"><s:text name="midas.impresiones.recibos.inicio"/></column>          
        <column id="reciboSeycos.fechaFinVigencia" type="ro" width="80" sort="date_custom" hidden="false"><s:text name="midas.impresiones.recibos.fin"/></column>
        <column id="reciboSeycos.sitRecibo" type="ro" width="80" sort="int" hidden="false"><s:text name="midas.impresiones.recibos.sitRecibo"/></column>
        <column id="reciboSeycos.impPrimaTotal" type="edn" width="*" sort="int" format="$0,000.00" hidden="false"><s:text name="midas.impresiones.recibos.prima"/></column>
        <column id="accion" type="img" width="30" sort="na" align="center"></column>
        <column id="accion2" type="img" width="30" sort="na" align="center"></column>
        <column id="accion3" type="img" width="30" sort="na" align="center"></column>                    
	</head>

	<s:iterator value="reciboList">
		<row id="<s:property value="id.idRecibo" escapeHtml="false"/>">
			<!-- <cell>0</cell> -->
			<cell><s:property value="cveTipoRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroFolio" escapeHtml="false"/></cell>			
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="sitRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="impPrimaTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../../img/b_printer.gif^Imprimir^javascript: imprimirReciboLlaveFiscal(<s:property value="id.idRecibo"/>)^_self</cell>
			<cell>../../img/icons/ico_verdetalle.gif^Movimientos^javascript: verMovimientosRecibo(<s:property value="id.idRecibo"/>)^_self</cell>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Regenerar_Recibo">
			<cell>../../img/icons/ico_copiar2.gif^Regenerar^javascript: regenerarRecibo(<s:property value="id.idRecibo"/>)^_self</cell>
			</m:tienePermiso>
		</row>
	</s:iterator>
</rows>