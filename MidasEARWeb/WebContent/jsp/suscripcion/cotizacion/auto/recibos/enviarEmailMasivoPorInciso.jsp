<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<script src="${pageContext.request.contextPath}/js/midas2/jQuery/jquery-1.4.3.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxwindows_clear_green.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
 function submitForm(){
      crearData();
 }
 function crearData(){
 	var idPolizas = parent.cargaIdRecibos();
	creaIdRecibosEnVentana(idPolizas);
	var form = jQuery('#enviarEmailMasivoPorIncisoForm')[0];	
	var validarPolizaPath = "/MidasWeb/impresiones/recibos/enviarCorreoMasivo.action";
	parent.redirectVentanaModal("mostrarContenedorEmail", validarPolizaPath, form);
}
function creaIdRecibosEnVentana(idPolizas){
	jQuery('#polizasReciboDiv').empty();
	if(idPolizas != null && idPolizas != ""){
		var ids = idPolizas.split(",");
		var num = 0;		
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#enviarEmailMasivoPorIncisoForm').append('<input type="hidden" name="polizaList['+ num+ '].idToPoliza" value="' + ids[i] + '" />');
	   	 	num = num + 1;		
		}
	}
}
</script>
</head> 
<body>
<div id="detalle" >
	<s:if test="mensaje != null">
		<table id="t_riesgo" width="100%">
			<tr>
				<th width="10%">Información</th>
			</tr>
			<tr>
				<td><s:property value="mensaje"/></td>
			</tr>
		</table>
	</s:if>
	<s:form action="enviarCorreoMasivo" id="enviarEmailMasivoPorIncisoForm">	
		<div id="polizasReciboDiv"></div>
		<table id="agregar" width="100%">
			<tr>
				<th>
				  <s:fielderror name="enviarEmailPorIncisoParameters.from"/>
				  Correo de envió (De:)
				</th>
				<th>
				  <s:textfield name="enviarEmailPorIncisoParameters.from" cssClass="txtfield" cssStyle="text-transform:none" size="45"/>
				</th>
			</tr>
			<tr>
				<th>
					  <s:text name="midas.componente.impresiones.recibo" /> 
				</th>
				<th>
					<s:checkbox name="enviarEmailPorIncisoParameters.incluirRecibo"/> 
				</th>
			</tr>
			<tr>
				<th>
					  <s:text name="midas.componente.impresiones.inciso.mensaje" /> 
				</th>
				<th>
					<s:checkbox name="enviarEmailPorIncisoParameters.incluirNu"/> 
				</th>
			</tr>
			<tr>
				<th>
					  <s:text name="midas.componente.impresiones.anexo" /> 
				</th>
				<th>
					<s:checkbox name="enviarEmailPorIncisoParameters.incluirAnexos"/> 
				</th>
			</tr>
		</table>
		
	    <div class="btn_back w140"  style="display:inline; float: right;width: 80px;">
	      <a href="javascript: void(0);" onclick="submitForm(); return false;"> 
		  Enviar
		  </a>
	    </div>		    
	</s:form>	
</div>
</body>
</html>
