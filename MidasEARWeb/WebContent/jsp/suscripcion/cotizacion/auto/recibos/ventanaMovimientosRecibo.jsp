<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>

<sj:head/>
<s:form id="movRecibosForm" action="/" >
	<div id="contenedorMovimientos" >
					<table id=t_riesgo border="0" style="width: 100%;">
						<tr>
							<th><s:text name="midas.general.version" /></th>
							<th><s:text name="midas.catalogos.centro.operacion.situacion" /></th>
							<th><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.movimiento" /></th>
							<th><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" /></th>
						</tr>
						<s:iterator value="movimientoReciboList" status="stat">
							<tr>
								<td><s:property value="%{verRecibo}" /> </td>
								<td><s:property value="%{sitRecibo}" /></td>
								<td><s:property value="%{cveMovtoEnd}" /></td>
								<td><s:property value="%{numEndoso}" /></td>
							</tr>
						</s:iterator>
					</table>
	</div>
</s:form>