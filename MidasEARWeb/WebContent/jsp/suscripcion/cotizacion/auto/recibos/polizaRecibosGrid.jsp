<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
        </beforeInit>
        <column id="polizaDTO.idToPoliza" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
        <column id="polizaDTO.numeroPolizaFormateada" type="ro" width="110" sort="str" hidden="false"><s:text name="midas.emision.nopoliza"/></column>
        <column id="polizaDTO.cotizacionDTO.nombreContratante" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.nombreContratante"/></column>
        <column id="polizaDTO.numeroSerie" type="ro" width="150" sort="int" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numSerie"/></column>          
        <column id="polizaDTO.fechaCreacion" type="ro" width="150" sort="date_custom" hidden="false"><s:text name="midas.emision.consulta.poliza.fechacreacion"/></column>
        <column id="polizaDTO.claveEstatus" type="ro" width="100" sort="int" hidden="false"><s:text name="midas.poliza.renovacionmasiva.estatus"/></column>
        <column id="nombreAgente" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.nombreAgente"/></column>
        <column id="numAgente" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numAgente"/></column>                    
	</head>

	<s:iterator value="polizaList">
		<row id="<s:property value="idToPoliza" escapeHtml="false"/>">
			<cell>0</cell>
			<cell><s:property value="numeroPolizaFormateada" escapeHtml="false"/></cell>
			<cell><s:property value="cotizacionDTO.nombreContratante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="claveEstatus== \"1\"">
					VIGENTE
				</s:if>
				<s:else>
					CANCELADA
				</s:else>
			</cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.agente.idAgente" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>