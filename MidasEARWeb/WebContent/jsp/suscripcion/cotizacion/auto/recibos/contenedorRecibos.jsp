<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script src='<s:url value="/js/midas2/suscripcion/cotizacion/auto/recibos/impresionRecibos.js"></s:url>'></script>

<sj:head/>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="recibosPolizaForm" action="/" >
	<s:hidden name="polizaDTO.idToPoliza" id="idToPoliza" />
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.poliza.numeroPoliza"/>: 	
		<s:property value="polizaDTO.numeroPolizaFormateada"/>
	</div>	
	<div id="contenedorFiltros" >
		<table id="agregar" border="0">
			<tr><td>
				<s:select name="numeroEndoso" id="numeroEndoso" 
						      key="midas.boton.endosos" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="endososList" headerKey="" 
						      onchange="cargaProgramaPagosImpresion(this.value);"   >
				</s:select>								
			</td><td>
				<s:select name="idProgPago" id="progPago" 
						      key="midas.boton.programasPago" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="proPagoList" headerKey=""
						      onchange="cargarIncisosProgramaPago(this.value);" >
				</s:select>					
			</td><td>
				<s:select name="idInciso" id="incisos" 
						      key="midas.negocio.renovacion.numIncisos" cssClass="cajaTexto"
						      list="incisoList" headerKey=""
						      >
				</s:select>								
			</td></tr> 	
			</table>
	</div>
<div id="indicador"></div>
<div id="recibosGrid" style="width: 98%; height: 250px; margin: 10px;"></div>
<div style="width: 98%;">
	<div id="divSalirBtn" class="btn_back w100"
		style="display: inline, none; float: right;">
		<a href="javascript: void(0);"
			onclick="parent.cerrarVentanaModal('ventanaRecibosPoliza',null);"> <s:text
				name="midas.boton.salir" /> </a>
	</div>
</div>
</s:form>
<script type="text/javascript">
	initRecibosGrid();
</script>