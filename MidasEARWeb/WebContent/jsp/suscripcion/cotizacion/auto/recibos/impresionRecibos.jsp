<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/suscripcion/cotizacion/auto/recibos/impresionRecibosHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>

<script type="text/javascript">
	function cleanInput(id) {
		jQuery('#' + id).val('');
	}
	function cleanInputDiv(id) {
		jQuery('#' + id).text('');
	}
	
	function compareDates(){
		blockPage();
		fecha1=dwr.util.getValue("polizaDTO.fechaCreacion");
		fecha2=dwr.util.getValue("polizaDTO.fechaCreacionHasta");
		num_poliza_midas=dwr.util.getValue("polizaDTO.numeroPoliza");
		num_serie=dwr.util.getValue("polizaDTO.numeroSerie");
		orden_renovacion=dwr.util.getValue("polizaDTO.idToOrdenRenovacion");
	    if (compare_dates(fecha1, fecha2)){  
	    	mostrarMensajeInformativo("La fecha de creación desde no debe ser mayor a la fecha hasta. Verificar.", 10, null); 
	    } else if(validate_filters(fecha1, fecha2, num_poliza_midas, num_serie, orden_renovacion)){
    		mostrarMensajeInformativo("Se debe seleccionar al menos 1 filtro para la búsqueda.", 10, null);
    	} else if(num_poliza_midas.trim() == '' && num_serie.trim() == '' && orden_renovacion.trim() == '' && compare_dates_period(fecha1, fecha2)) {
	    	mostrarMensajeInformativo("El periódo entre la fecha de creación desde y la fecha hasta no debe ser mayor a 1 año. Verificar.", 10, null);
	    } else{
	    	// Definir fechas por default cuando no se capture filtro restrictivo
	    	if(num_poliza_midas.trim() == '' && num_serie.trim() == '' && orden_renovacion.trim() == '')
	    		definirFechasDefault("polizaDTO.fechaCreacion", "polizaDTO.fechaCreacionHasta");
	    		
	        listarRecibosPaginado(1,true);
	    }
	    unblockPage();
	}
	
	function validate_filters(fecha1, fecha2, num_poliza_midas, num_serie, orden_renovacion) {	
		nombre_contratante = dwr.util.getValue("polizaDTO.cotizacionDTO.nombreContratante");
		negocio = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.negocio.idToNegocio");
		producto = dwr.util.getValue("polizaDTO.idToNegProducto");
		tipo_poliza = dwr.util.getValue("polizaDTO.idToNegTipoPoliza");
		linea_negocio = dwr.util.getValue("polizaDTO.idToNegSeccion");
		centro_emisor = dwr.util.getValue("polizaDTO.idCentroEmisor");
		gerencia = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.ejecutivo.gerencia.id");
		ejecutivo = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.codigoEjecutivo");
		promotoria = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.id");
		nombre_agente = dwr.util.getValue("agenteNombre");
		codigo_agente = dwr.util.getValue("polizaDTO.cotizacionDTO.solicitudDTO.agente.idAgente");
		
		if(num_poliza_midas.trim() != '' || num_serie.trim() != '' || nombre_contratante.trim() != '' || fecha1.trim() != '' || fecha2.trim() != '' 
			|| orden_renovacion.trim() != '' || negocio.trim() != '' || producto.trim() != '' || tipo_poliza.trim() != '' || linea_negocio.trim() != '' 
			|| centro_emisor.trim() != '' || gerencia.trim() != '' || ejecutivo.trim() != '' || promotoria.trim() != '' || nombre_agente.trim() != '' 
			|| codigo_agente.trim() != '') 
		{
			return (false);
		} else {
			return (true);
		}
	}
	
	var agenteControlDeshabilitado = <s:property value="agenteControlDeshabilitado"/>;
	var promotoriaControlDeshabilitado = <s:property value="promotoriaControlDeshabilitado"/>;
</script>

<s:form id="impresionRecibosForm" action="/" >
	<s:hidden name="nombreArchivo" id="nombreArchivo" ></s:hidden>
	<div id = "polizasReciboDiv" ></div>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.poliza.recibos.title"/>	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tr>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQnumeric jQrestrict" 
						key="midas.poliza.numeroPoliza"
						labelposition="top" 
						size="10"
						maxlength="8"						
						id="numeroPoliza" name="polizaDTO.numeroPoliza" />
				</td>
				<td>
					<s:textfield key="midas.poliza.numeroSerie" cssClass="txtfield jQnumeric jQrestrict"
								 maxlength="17" 
								 labelposition="top" 
								 id="polizaDTO.numeroSerie" name="polizaDTO.numeroSerie" />						  			
				</td>	
				<td>
 					<s:textfield key="midas.poliza.nombrecontratante" cssClass="txtfield jQalphaextra jQrestrict"
								 maxlength="60"
								 labelposition="top" 
								 id="polizaDTO.cotizacionDTO.nombreContratante" name="polizaDTO.cotizacionDTO.nombreContratante"
								 onblur="validaLongitud('polizaDTO.cotizacionDTO.nombreContratante', this.value)" />		
				</td>
				</tr>
				<tr>
			    <td>
						<sj:datepicker name="polizaDTO.fechaCreacion"
							key="midas.suscripcion.cotizacion.fecha.desde"
							labelposition="top"
							changeMonth="true"
							changeYear="true"				
							buttonImage="../img/b_calendario.gif"
	                        id="polizaDTO.fechaCreacion"
							maxlength="10" cssClass="txtfield"
							size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
				</td>
				<td>
						<sj:datepicker name="polizaDTO.fechaCreacionHasta"
							key="midas.suscripcion.cotizacion.fecha.hasta"
							labelposition="top" 					
							buttonImage="../img/b_calendario.gif"
						    id="polizaDTO.fechaCreacionHasta"
						    size="12"
							changeMonth="true"
							changeYear="true"
							maxlength="10" cssClass="txtfield"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				 </td>		 	     			 
				<td>
					<s:textfield cssClass="txtfield jQnumeric jQrestrict" 
						key="midas.poliza.recibos.ordenRenovacion"
						labelposition="top" 
						size="10"
						maxlength="6"						
						id="numeroPoliza" name="polizaDTO.idToOrdenRenovacion" />												
				</td>	
				<td>
						<!--  -->
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<s:select id="polizaDTO.cotizacionDTO.solicitudDTO.negocio.idToNegocio" 
								key="midas.poliza.renovacionmasiva.negocio"
								labelposition="top" 
								cssStyle="width:300px;max-width: 300px;"
								name="polizaDTO.cotizacionDTO.solicitudDTO.negocio.idToNegocio"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'productos', 'getMapNegProductoPorNegocio')" 			
								listKey="idToNegocio" listValue="descripcionNegocio" 
								list="negocioList" cssClass="txtfield"  />
				</td>
				<td>
					<s:select id="productos" 
								key="midas.poliza.renovacionmasiva.producto"
								labelposition="top" 
								name="polizaDTO.idToNegProducto"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'tipopolizas', 'getMapNegTipoPolizaPorNegProducto')"
								list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion" 
								cssClass="txtfield"  />
				</td>
				<td>
					<s:select id="tipopolizas" 
								key="midas.poliza.renovacionmasiva.tipoPoliza"
								labelposition="top" 
								name="polizaDTO.idToNegTipoPoliza"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'secciones', 'getMapNegocioSeccionPorTipoPoliza')"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion" 
						  		cssClass="txtfield" /> 	
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<s:select id="secciones" key="midas.poliza.renovacionmasiva.lineaNegocio"
								labelposition="top" 
								name="polizaDTO.idToNegSeccion"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion"  
						  		cssClass="txtfield" /> 	
				</td>
				<td>
				<s:select  labelposition="top" key="midas.negocio.centroOperacion"
					list="centrosEmisores" name="polizaDTO.idCentroEmisor" id="centrosEmisores" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					cssClass="txtfield" />	
				</td>
				<td>
					<s:select id="gerencias" key="midas.poliza.renovacionmasiva.gerencia"
								labelposition="top" 
								name="polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.ejecutivo.gerencia.id"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'oficinas', 'getMapEjecutivosPorGerencia')"
						  		list="gerencias"   
						  		cssClass="txtfield" /> 	
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<s:select id="oficinas" key="midas.poliza.renovacionmasiva.oficina"
								labelposition="top" 
								name="polizaDTO.cotizacionDTO.solicitudDTO.codigoEjecutivo"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'promotorias', 'getMapPromotoriasPorEjecutivo')"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion"  
						  		cssClass="txtfield" /> 	
				</td>
				<td>
					<s:select id="promotorias" key="midas.poliza.renovacionmasiva.promotoria"
								labelposition="top" 
								name="polizaDTO.cotizacionDTO.solicitudDTO.agente.promotoria.id"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion"  
						  		cssClass="txtfield" /> 	
				</td>
			</tr>
			<tr>
				<td colspan="2">
						<div><label for="agenteNombre"><s:text name="midas.poliza.nombreAgente"></s:text></label></div>
						<br/>
						<div style="vertical-align: bottom;" >
						<s:if test="!agenteControlDeshabilitado">
	 						<s:set var="agenteControlOnclick" value="'seleccionarAgentePoliza(1);'"/>
				 		</s:if>
						<div id="agenteNombre" class="divInputText txtfield" style="width: 280px; float: left; max-width: 280px" onclick="${agenteControlOnclick}">
							<s:property value="polizaDTO.cotizacionDTO.solicitudDTO.agente.persona.nombreCompleto"/>
				 		</div>
				 		<div style="float: left; vertical-align: bottom;">
				 		<s:if test="!agenteControlDeshabilitado">
			 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripción" 
				 			style="margin-left: 5px;"
				 			onclick ="cleanInputDiv('agenteNombre');cleanInput('idAgentePol');cleanInput('idAgenteClavePol');"
				 		/>
				 		</s:if>	
				 		</div>
				 		</div>											
				</td>	
				<td>
					<s:hidden id="idAgentePol" name="polizaDTO.cotizacionDTO.solicitudDTO.codigoAgente" />
					<s:textfield key="midas.poliza.renovacionmasiva.numAgente" cssClass="txtfield jQnumeric jQrestrict"
								 labelposition="top" 
								 size="10"
								 maxlength="6"
								 id="idAgenteClavePol" 
								 name="polizaDTO.cotizacionDTO.solicitudDTO.agente.idAgente" />
				</td>
			</tr>
		</table>
	</div>
	<div  style="width: 98%;">
		<table id="agregar">
			<tr>		
				<td colspan="4">		
					<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);"
											onclick="limpiarFiltrosRecibos(1);"> <s:text
												name="midas.suscripcion.cotizacion.limpiar" /> </a>
									</div>
	
									<div class="btn_back w140" style="display: inline; float: right;">
										 <a id="submit" href="javascript: void(0);" onclick="compareDates();"> 
											<s:text name="midas.suscripcion.cotizacion.buscar" /> </a>
									</div>
									
									<div class="btn_back w130" style="display: inline; float: right;">
										<a href="javascript: void(0);" id="mostrarFiltros"
											onclick="displayFiltersRecibos();">Ocultar Filtros</a>
									</div>
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		</table>
	</div>	
</s:form>
<div id="indicador"></div>
<div id="gridPolizasPaginado" > 
<div id="polizaGrid" style="width: 98%; height: 130px"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
</div>  
<br>
<div  style="width: 98%;">
<table id="agregar" style="padding: 0px; width: 96%; margin: 0px; border: none;">
	<tr>
		<td>
			<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Enviar_Orden_Proveedor">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a  href="javascript: void(0);" onclick="mostrarSeleccionarNombre();"> 
					<s:text name="midas.poliza.recibos.enviarProveedor" /> 
				</a>
			</div>
			</m:tienePermiso>
			<div class="btn_back w140" style="display: inline; float: right;">
				<a  href="javascript: void(0);" onclick="mostrarContenedorImpresion();"> 
					<s:text name="midas.boton.imprimir" /> 
				</a>
			</div>
			<div class="btn_back w140" style="display: inline; float: right;">
				<a  href="javascript: void(0);" onclick="mostrarContenedorEmail();"> 
					<s:text name="midas.correo.enciarCorreo" /> 
				</a>
			</div>
		</td>
	</tr>
</table>
</div>