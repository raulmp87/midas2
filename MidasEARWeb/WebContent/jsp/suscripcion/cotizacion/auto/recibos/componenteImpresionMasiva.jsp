<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">
<!--
 function generarImpresion(){
	var path = '/MidasWeb/impresiones/componente/generarImpresion.action?"'+ jQuery(document.contenedorImpresionForm).serialize();
	var imprimir = parent.dhxWins.window('mostrarContenedorImpresion');
		
 }


 function setIncios(){
	if(jQuery("#chkinciso").is(":checked")){
     jQuery("#txtIncios").val("");		
  }
 }	
 function setInciosTxt(valor){
  if(valor.length > 0){
	 jQuery("input[name$='chkinciso']").removeAttr("checked");		
  }	
 }


 var validos = "-0123456789";
 function soloNumerosYGuion(campo,hasError) {
	 jQuery("#error").hide();
	    hasError = false;
    var letra;
    var bandera = true;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 bandera=false;
    	 hasError == true;
     }
    }
 
    if (!bandera) {
        jQuery('#txtIncios').css("background-color", "#FF0000");
        campo.value="";
        hasError = true;
    }
    if(bandera){
       jQuery('#txtIncios').css("background-color", "#ffffff");
    }
    if(hasError == true) {
    	jQuery("#txtIncioshidden").after('<span id="error">Ej: 1-10</span>');
    	campo.value=""; return false; 
    }
 }	

 function submitForm(){
   	  //mostrarIndicadorCarga('indicador');	
      //submitData();
      crearData();
 }
 function crearData(){
 	var idPolizas = parent.cargaIdRecibos();
	creaIdRecibosEnVentana(idPolizas);
	var form = jQuery('#contenedorImpresionForm')[0];	
	var validarPolizaPath = "/MidasWeb/impresiones/recibos/generarImpresion.action";
	parent.redirectVentanaModal("mostrarContenedorImpresion", validarPolizaPath, form);
}
function creaIdRecibosEnVentana(idPolizas){
	jQuery('#polizasReciboDiv').empty();
	if(idPolizas != null && idPolizas != ""){
		var ids = idPolizas.split(",");
		var num = 0;		
		for(i = 0; i < ids.length; i++){
	   	 	jQuery('#contenedorImpresionForm').append('<input type="hidden" name="polizaList['+ num+ '].idToPoliza" value="' + ids[i] + '" />');
	   	 	num = num + 1;		
		}
	}
}
 function submitData(){	
	document.contenedorImpresionForm.submit();	 			
 }
//-->
</script> 

<div id="detalle" >
<s:include value="/jsp/componente/impresiones/componenteImpresionHeader.jsp"></s:include>
 <div id="indicador" style="display:none;"></div>
	<center>
		<table id="agregar" width="100%">
		 <s:form action="generarImpresion" namespace="/impresiones/recibos" id="contenedorImpresionForm">
		  <s:hidden name="tipoImpresion"/>
		   <s:hidden name="totalIncisos"/>
		   <s:hidden name="id"/>	
		   <s:hidden name="idToPoliza"/>
		   <s:hidden name="validOnMillis" />
		   <s:hidden name="recordFromMillis" />
		   <s:hidden name="claveTipoEndoso" />
		   <s:hidden name="esSituacionActual" />
		   <tr>
		   <td>
		   <div id="polizasReciboDiv"></div>
		   </td>
		   <td></td>
		   </tr>		   
		  <tr>	
			<th width="30%">
				  <s:text name="midas.componente.impresiones.caratula" /> 
			</th>
			<th width="5%">
			  <s:checkbox name="chkcaratula"  id="chkcaratula" onclick=""/> 
			</th>	
		 </tr>
       	<s:if test="%{tipoImpresion != 1}">
		 <tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.recibo" /> 
			</th>
			<th width="5%">
				<s:checkbox name="chkrecibo" id="chkrecibo" alt="Seleccionar Todos" title="Seleccionar Todos" onclick=""/> 
			</th>	
		 </tr>
        </s:if>
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.inciso" /> 
			</th>
			<th width="5%">
			  <s:checkbox name="chkinciso" id="chkinciso" alt="Seleccionar Todos" title="Seleccionar Todos" onclick="setIncios();"/> 
			</th>	
		</tr>
		<tr>
			<th width="30%">
			</th>
			<th width="5%">
			  <s:hidden name="txtIncioshidden" id="txtIncioshidden"  />
			</th>	
		</tr>
		<s:if test="%{tipoImpresion !=4}">	
		 <tr>
			<th width="30%">
			  <s:text name="midas.componente.impresiones.incisoTexto" /> 
			</th>
			<th width="5%">
			  <s:textfield name="txtIncios" id="txtIncios" onmousemove="setInciosTxt(this.value);" onkeyup="return soloNumerosYGuion(this, false)" />
			</th>	
		</tr>
		</s:if>
	  <s:if test="%{tipoImpresion ==2 || tipoImpresion ==4}">	
	  
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.inciso.mensaje" /> 
			</th>
				
			<th width="5%">
			  <s:checkbox name="chkcertificado" id="chkinciso" alt="Seleccionar Todos" title="Seleccionar Todos" onclick=""/> 
			</th>	
		</tr>
		
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.anexo" /> 
			</th>
				
			<th width="5%">
			  <s:checkbox name="chkanexos" id="chkanexos" alt="Seleccionar Todos" title="Seleccionar Todos" 
			  onclick=""/> 
			</th>	
		</tr>
		
		<tr>
		  <th width="30%">
		    <s:text name="midas.componente.impresiones.fechaDeValidez"></s:text>
		  </th>
		  <th width="5%">
		    <s:textfield name="validOn" id="validOn" value="%{validOn}" readonly="true"></s:textfield>
		  </th>
		</tr>
	  </s:if>									
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript: void(0);"
				  onclick="submitForm(); return false;"> 
				  <s:text
				  name="midas.componente.impresiones.botonImprimir" /> </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;width: 80px; ">
			      <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){cerrarVentanaComponente(<s:property value="id"/>)}"> 
				  <s:text
				  name="midas.boton.cerrar" /> </a>
			    </div>	
		       </th>	         
			</tr>
			</s:form>
		</table>
	</center>  
</div>