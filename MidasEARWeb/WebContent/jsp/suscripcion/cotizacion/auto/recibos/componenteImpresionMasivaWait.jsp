<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
	<head>
		<meta http-equiv="refresh" content="2;url=<s:url includeParams="none" />"/>
		<title>Esperando impresion</title>	
		<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
		<script type="text/javascript">
			function regresar(){
				
			}
			
			function cerrar(){
				parent.cerrarVentanaModal('mostrarContenedorImpresion');
			}
		</script>
	</head>	
	<body>
		<center>
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="10%">Espere a que el sistema genere el PDF para ser guardado/impreso.</th>	
				</tr>
			</table>
			<table id="agregar" width="100%">
				<tr>			      
			      <th>
				     <div class="btn_back w140"  style="display:inline; float: left;width: 80px; ">
				      <a href="javascript: void(0);"
					  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){cerrar();}"> 
					  <s:text
					  name="midas.boton.cerrar" /> </a>
				    </div>	
			       </th>		         
				</tr>									
			</table>			
		</center>			
	</body>
</html>
