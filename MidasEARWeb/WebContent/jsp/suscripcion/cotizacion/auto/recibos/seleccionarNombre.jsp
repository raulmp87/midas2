<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">

function setNombreYEnviar(){
	nombreArchivo = jQuery("#nombreArchivoSel").val();
	parent.setNombreArchivo(nombreArchivo);
}

</script> 

<div id="detalle" >
 <div id="indicador" style="display:none;"></div>
	<center>
		<table id="agregar" width="100%">
		 <s:form action="enviarProveedor" namespace="/impresiones/recibos" id="enviarProveedorForm">
		 <tr>
			<th width="30%">
			  <s:text name="midas.poliza.recibos.nombreArchivo" /> 
			</th>
			<th width="5%">
			  <s:textfield name="nombreArchivoSel" id="nombreArchivoSel" />
			</th>	
		</tr>
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;">
			      <a href="javascript: void(0);" onclick="setNombreYEnviar(); return false;"> 
				  	<s:text name="midas.poliza.recibos.enviarProveedor" /> 
				  </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;">
			      <a href="javascript: void(0);" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){parent.cerrarVentanaModal('seleccionarNombre')}"> 
				  	<s:text name="midas.boton.cerrar" /> 
				  </a>
			    </div>	
		       </th>	         
			</tr>
			</s:form>
		</table>
	</center>  
</div>