<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idToCargaMasivaIndAutoCot" type="ro" width="*" sort="int" hidden="true">id</column>
        <column id="idToDetalleCargaMasivaIndAutoCot" type="ro" width="*" sort="int" hidden="true">id</column>
        <column id="idToCotizacion" type="ro" width="100" sort="str" hidden="false">Cotizacion</column>
        <s:if test="cargaMasivaIndividualAutoCot.claveTipo == 1" >
        <column id="idToPoliza" type="ro" width="100" sort="str" hidden="false">Poliza</column>
        </s:if>
      	<column id="fechaVigencia" type="ro" width="120" format="%d/%m/%Y %H:%i" sort="dateTime_custom" hidden="false">Vigencia</column>
		<column id="nombreCliente" type="ro" width="*" sort="str">Cliente</column>
		<column id="claveEstatus" type="ro" width="100" sort="str"><s:text name="midas.cotizacion.cargamasiva.estatus" /></column>
		<column id="resumen" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones" /></column>
	</head>
	<s:iterator value="cargaMasivaDetalleIndividualList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idToCargaMasivaIndAutoCot" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="idToDetalleCargaMasivaIndAutoCot" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="cargaMasivaIndividualAutoCot.claveTipo == 1" >
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<cell><s:date name="fechaVigencia" format="dd/MM/yyyy" /></cell>	
			<cell><s:property value="nombreCliente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>		
			<cell>../img/icons/ico_agregar.gif^Consultar Error^javascript:mostrarErrorCargaMasivaIndividual(<s:property value="claveEstatus" escapeHtml="false" escapeXml="true"/>, "<s:property value="mensajeError" escapeHtml="false" escapeXml="true"/>");^_self</cell>
		</row>
	</s:iterator>
</rows>