<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasivaindividual/cargaMasivaIndividualHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="cargamasivaindividualdetalleForm">
	<s:hidden name="idToCargaMasivaIndAutoCot" id="idToCargaMasivaIndAutoCot" />
	<s:hidden name="idToNegocio" id="idToNegocio" />
	<s:hidden name="idToNegProducto" id="idToNegProducto" />
	<s:hidden name="idToNegTipoPoliza" id="idToNegTipoPoliza" />
	<s:hidden name="cargaMasivaIndividualAutoCot.claveTipo" id="claveTipo" />
	
	<div class="titulo"  style="width: 98%;">
		<s:if test="cargaMasivaIndividualAutoCot.claveTipo == 0">
		<s:text name="midas.cotizacion.cargamasivaindividual.resumenCotizacion" />
		</s:if>
		<s:else>
		<s:text name="midas.cotizacion.cargamasivaindividual.resumenPoliza" />
		</s:else>
	</div>
</s:form>
<div id="indicadorGrid"></div>
<div id="cargasMasivasIndDetalleGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div  style="width: 98%;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" 
				onclick="regresarACargaMasivaIndividualAuto();"> <s:text
					name="midas.boton.regresar" /> </a>
		</div>
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" class="icon_excel"
				onclick="descargarCargaMasiva(<s:property value="cargaMasivaIndividualAutoCot.idToControlArchivo"/>);"> <s:text
					name="midas.cotizacion.cargamasivaindividual.descargarExcel" /> </a>
		</div>
		<s:if test="claveTipo == 1">
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Imprimir_Poliza">
				<div class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" class="icon_imprimir"
					onclick="verDetalleImpresionPolizaCargaMasiva(<s:property value="idToCargaMasivaIndAutoCot"/>);"> <s:text
					name="midas.cotizacion.cargamasivaindividual.descargarPolizas" /> </a>
				</div>	
			</m:tienePermiso>
		</s:if>
</div>
<script type="text/javascript">
	iniciaListadoDetalleIndividual();
</script>
