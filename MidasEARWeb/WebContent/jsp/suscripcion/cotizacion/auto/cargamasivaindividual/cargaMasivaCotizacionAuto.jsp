<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasivaindividual/cargaMasivaIndividualHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:form id="cargamasivaindividualForm">
	<s:hidden name="claveTipo" id="claveTipo" value="0"/>	
	<div class="titulo"  style="width: 98%;">
		<s:text name="midas.cotizacion.cargamasiva.seleccioneArchivo" />
	</div>
	<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<s:select labelposition="top" key="midas.suscripcion.solicitud.solicitudPoliza.negocio"
					list="negocioList" name="idToNegocio" id="negocios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 	
					onchange="cargarComboSimpleDWR(this.value, 'productos', 'getMapNegProductoPorNegocio')" 			
					listKey="idToNegocio" listValue="descripcionNegocio" 				
					cssClass="txtfield" />	
			</td>
			<td>
				<s:select key="midas.suscripcion.cotizacion.producto" 
						  name="idToNegProducto" id="productos"
						  labelposition="top" 
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  onchange="cargarComboSimpleDWR2(this.value, 0, 'polizas', 'getMapNegTipoPolizaPorNegProductoFlotillaOIndividual')"
						  list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion" 
						  cssClass="txtfield" /> 	
			</td>
			<td>
				<s:select key="midas.negocio.producto.tipoPoliza" 
						  name="idToNegTipoPoliza" id="polizas"
						  labelposition="top"
						  onchange="iniciaListadoIndividual(0)" 
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  list="tiposPoliza" listKey="idToNegTipoPoliza" listValue="tipoPolizaDTO.descripcion" 
						  cssClass="txtfield" /> 	
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w150" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(1);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importar" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w170" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(2);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importarTemporizado" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w220" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargarPlantillaCargaIndividual();"
						class="icon_guardar2"> <s:text
							name="midas.cotizacion.cargamasiva.descargarPlantilla" /> </a>
				</div>
			</td>
		</tr>
		<tr>
			<td >
					<br><s:text name="midas.carga.masiva.texto.1"/></br>
					<br><s:text name="midas.carga.masiva.texto.2"/> </br>
					<br><s:text name="midas.carga.masiva.texto.3"/></br>
					<s:checkbox id="idAcurdoAfirmeMasiva" name="temp" />
				</td>
		</tr>
	</table>
</div>
</s:form>
<div id="indicadorGrid"></div>
<div id="cargasMasivasIndividualesGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<script type="text/javascript">
	descargarLogCargaMasivaIndividual();
	ocultarIndicadorCarga('indicador');
	iniciaListadoIndividual(0);
</script>
