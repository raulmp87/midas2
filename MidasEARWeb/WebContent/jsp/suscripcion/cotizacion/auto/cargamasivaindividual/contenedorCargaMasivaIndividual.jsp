<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasivaindividual/cargaMasivaIndividualHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<script type="text/javascript">
	mostrarIndicadorCarga("indicador");
</script>
<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
<div hrefmode="ajax-html" style="height: 450px; width: 920px" id="cargaMasivaIndividualTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	
	<div width="200px" id="detalle" name="<s:text name="midas.cotizacion.cargamasivaindividual.cargaMasivaCotizacion" />" href="http://void" extraAction="javascript: verCargaMasivaCotizacion();">
	</div>
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cargas_Masivas_Individuales_Emision_Masiva_Individuales">
	<div width="200px" id="cargaMasivaEmision" name="<s:text name="midas.cotizacion.cargamasivaindividual.cargaMasivaEmision" />" href="http://void" extraAction="javascript: verCargaMasivaEmision();"></div>
	</m:tienePermiso>
	<div width="200px" id="generacionLayoutRenovacion" name="<s:text name="midas.poliza.auto.renovacionmasiva.autoplazo.generacionLayoutRenovacion" />" href="http://void" extraAction="javascript: verGeneracionLayoutRenovacion();"></div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>