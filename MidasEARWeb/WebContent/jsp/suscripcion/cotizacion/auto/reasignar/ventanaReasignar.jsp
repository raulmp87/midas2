<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/suscripcion/cotizacion/auto/reasignar/reasignarHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
<!--
function onChangeSeleccion(tipoAsignacion){
	var path = '/MidasWeb/suscripcion/cotizacion/reasignar/mostrarVentana.action';
	var asignar = parent.dhxWins.window('asignar');
	asignar.attachURL(path);	
}
//-->

function closeVentanaReasignar(){
	parent.dhxWins.window('asignar').close();
}

function confirmaReasignar(){
	var respuesta = confirm("\u00BFEsta seguro de reasignar la cotizaci\u00F3n, la cotizaci\u00F3n dejara de estar asignada al suscriptor " + jQuery("#nombreUsuarioAsignado").val() + "?");
	if(respuesta){
		jQuery("#reasignarBtn").click();
	}
}
</script> 
<s:if test="tipoMensaje == 30">
	<script>
		jQuery(document).ready(function() {
			setTimeout(function() {
				parent.dhxWins.window('asignar').close();
			}, 2000);
		});
	</script>
</s:if>
<div id="agregar">
	<s:form action="reasignar" namespace="/suscripcion/cotizacion/reasignar"
		id="asignarUsuarioForm">
		<s:hidden name="id" id="id" />
		<s:hidden name="nombreUsuarioAsignado" id="nombreUsuarioAsignado" />
		<div class="titulo">
			<s:text name="midas.suscripcion.cotizacion.reasignar.titulo" />:<s:property value="cotizacionDTO.numeroCotizacion" />
		</div>
		<div>
			<s:text name="midas.suscripcion.cotizacion.asignar.leyenda" />
		</div>
				<s:select id="solicitud.codigoUsuarioAsignacion"
					list="usuariosSuscriptores"
					name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					listKey="nombreUsuario" listValue="nombreCompleto"
					cssClass="cajaTexto" />


<div style="width: 98%;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);"
				onclick="javascript:closeVentanaReasignar();"> <s:text
					name="midas.boton.cancelar" /> </a>
		</div>
	<div style="display: inline; float: right; width:140px;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);"
				onclick="javascript:confirmaReasignar();"> <s:text
					name="midas.suscripcion.cotizacion.asignar.realizarCambio" /> </a>
		</div>
		<s:submit key="midas.suscripcion.cotizacion.asignar.realizarCambio"
				label="%{getText('midas.suscripcion.cotizacion.asignar.realizarCambio')}"
				cssStyle="width:140px; display:none;"  id="reasignarBtn"
			onclick="javascript: mostrarIndicadorCarga('indicador');"
			cssClass="b_submit" />
	</div>
</div>
		<div id="indicador"></div>
	</s:form>
</div>
<div id="central_indicator" class="sh2" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		src="/MidasWeb/img/loading-green-circles.gif" alt="Afirme" />
</div>