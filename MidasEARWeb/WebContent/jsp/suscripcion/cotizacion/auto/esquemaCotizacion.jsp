<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/cotizacion/cotizacionAuto.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>

<sj:head/>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="detalle" style="overflow:auto;height: 150px" >
	<center>
		<table id="t_riesgo" width="100%">
			<tr>
				<th width="10%"><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.formadePago" /> </th>
				
				<th width="30%">
				  <s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoInicial" /> 
				</th>
				
				<th width="30%">
				  <s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.cantidad" />
				</th>	
				
				<th width="30%">
							<s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoSubsecuente" />
				</th>	
			</tr>	  
			<s:iterator value="esquemaPagoCotizacionList" status="stats">
				<tr>
					<td ><s:property value="formaPagoDTO.descripcion" /></td>
					<td align="right"><s:property value="pagoInicial.importeFormat" /></td>
					<td align="center">
						<s:if test="pagoSubsecuente.numExhibicion != null">
							<s:property value="pagoSubsecuente.numExhibicion" />
						</s:if>
						<s:else>
							0
						</s:else>
					</td>
					<td align="right">
						<s:if test="pagoSubsecuente.importeFormat != null">
							<s:property value="pagoSubsecuente.importeFormat" />
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</td>
				</tr>
			</s:iterator>  
			<tr>
				<th colspan="4">* Estimaci&oacute;n para las distintas formas de pago sobre las condiciones originales.</th>
			</tr>
		</table>
	</center>
	
</div>