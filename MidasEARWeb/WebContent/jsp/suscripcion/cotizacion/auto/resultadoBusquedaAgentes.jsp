<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:iterator value="agentesList">
	<item>
		<idAgente><s:property value="idAgente" /></idAgente>
		<id><s:property value="id"/></id>
		<!-- 
		<descripcion><s:property value="persona.nombreCompleto" escapeHtml="false" escapeXml="true" /> - <s:property value="id" /></descripcion>
		-->
		<descripcion><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true" /> - <s:property value="idAgente" /></descripcion>
	</item>
	</s:iterator>
</resultados>
