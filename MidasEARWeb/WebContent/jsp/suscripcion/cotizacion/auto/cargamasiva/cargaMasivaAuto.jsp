<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasiva/detalleCargaHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:form id="cargamasivacotizacionForm">
	<s:hidden name="id" id="id"/>
	<s:hidden name="tipoRegreso" id="tipoRegreso"/>	
	<div class="titulo"  style="width: 98%;">
		<div style="float:left">
		<s:text name="midas.cotizacion.no" />
		<b><s:property value="cotizacion.numeroCotizacion"/></b>
		</div>
		<div style="text-align: right;">
		<s:text name="midas.cotizacion.estatus" />:
		<s:property value="cotizacion.descripcionEstatus" />
		</div>
	</div>
<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<s:text name="midas.cotizacion.cargamasiva.seleccioneArchivo" />
			</td>
			<td>
				<div class="btn_back w150" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(1);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importar" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w170" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(2);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importarTemporizado" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w220" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargarPlantillaCarga();"
						class="icon_guardar2"> <s:text
							name="midas.cotizacion.cargamasiva.descargarPlantilla" /> </a>
				</div>
			</td>
		</tr>
		<tr>
			<td >
					<br><s:text name="midas.carga.masiva.texto.1"/> </br>
					<br><s:text name="midas.carga.masiva.texto.2"/> </br>
					<br><s:text name="midas.carga.masiva.texto.3"/></br>
					<s:checkbox id="idAcurdoAfirmeMasiva" name="temp" />
				</td>
		</tr>
	</table>
</div>
<div id="leyenda">
	<s:text name="midas.cotizacion.cargamasiva.leyenda" />
</div>
</s:form>
<div id="indicador"></div>
<div id="cargasMasivasGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div  style="width: 98%;text-align: center;">
<div class="btn_back w220" style="display: inline; float: right; ">
	<a href="javascript: void(0);" onclick="regresarACotizacion(<s:property value='tipoRegreso' />);"> 
	<s:text name="midas.boton.regresar" /> </a>
</div>
</div>

<script type="text/javascript">
	descargarLogCargaMasiva();
	iniciaListado();
</script>