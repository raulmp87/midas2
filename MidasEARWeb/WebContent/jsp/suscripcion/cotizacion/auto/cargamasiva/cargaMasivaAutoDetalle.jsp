<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasiva/detalleCargaHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<s:hidden name="id" id="id"/>
	<s:hidden name="cargaMasivaAutoCot.idToControlArchivo" id="idToControlArchivo" />
	<s:hidden name="logErrors" id="logErrors" />
	<s:hidden name="cargaMasivaAutoCot.idToCargaMasivaAutoCot" id="idToCargaMasivaAutoCot"/>
	<s:hidden name="tipoRegreso" id="tipoRegreso"/>	
	<div class="titulo"  style="width: 98%;">
		<b><s:property value="cargaMasivaAutoCot.controlArchivo.nombreArchivoOriginal"/></b>
		<br>
		<b><s:text name="midas.cotizacion.cargamasiva.resumenCargaMasiva" /></b>
	</div>
	<div style="width: 98%;text-align: right;">
		<b><s:text name="midas.cotizacion.cargamasiva.fechaCarga" />:
		<s:property value="cargaMasivaAutoCot.fechaCreacion"/></b>
	</div>
	<br>
	<div id="indicador"></div>
	<div id="cargasMasivasDetalleGrid" style="width: 98%;height:200px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<br>
<s:form id="cargamasivadetallecotizacionForm">
<div  style="width: 98%;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" class="icon_limpiar"
				onclick="regresarACargaMasivaAuto();"> <s:text
					name="midas.boton.regresar" /> </a>
		</div>
		<div class="btn_back w200" style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="descargarCargaMasiva(<s:property value="cargaMasivaAutoCot.idToControlArchivo" />);">
				<s:text name="midas.cotizacion.cargamasiva.exportarDocumentoCarga" />
			</a>
		</div>
		<s:if test="logErrors == true" >
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="descargarLogCargaMasiva()"> <s:text
					name="midas.cotizacion.cargamasiva.logErroresGlobal" /> </a>
		</div>
		</s:if>
	</div>
</s:form>


<script type="text/javascript">
	iniciaListadoDetalle();
</script>