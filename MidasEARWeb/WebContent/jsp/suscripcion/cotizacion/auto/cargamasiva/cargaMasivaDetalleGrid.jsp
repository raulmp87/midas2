<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idToDetalleCargaMasivaAutoCot" type="ro" width="*" sort="int" hidden="true">id</column>
        <column id="numeroInciso" type="ro" width="100" sort="int" hidden="false"><s:text name="midas.cotizacion.cargamasiva.noinciso" /></column>
      	<column id="lineaNegocioDescripcion" type="ro" width="*" sort="str" hidden="false"><s:text name="midas.cotizacion.cargamasiva.lineanegocio" /></column>
		<column id="descripcion" type="ro" width="200" sort="str"><s:text name="midas.cotizacion.cargamasiva.descripcionvehiculo" /></column>
		<column id="estatus" type="ro" width="100" sort="str"><s:text name="midas.cotizacion.cargamasiva.estatus" /></column>
		<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones" /></column>
	</head>
	<s:iterator value="cargaMasivaDetalleList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idToDetalleCargaMasivaAutoCot" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="lineaNegocioDescripcion" escapeHtml="false" escapeXml="true" /></cell>	
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>	
			<s:if test="claveEstatus == 0">	
			<cell>../img/icons/ico_agregar.gif^Consultar Error^javascript:mostrarErrorCargaMasiva(<s:property value="claveEstatus" escapeHtml="false" escapeXml="true"/>, "<s:property value="idToDetalleCargaMasivaAutoCot" escapeHtml="false" escapeXml="true"/>");^_self</cell>
			</s:if>
		</row>
	</s:iterator>
</rows>