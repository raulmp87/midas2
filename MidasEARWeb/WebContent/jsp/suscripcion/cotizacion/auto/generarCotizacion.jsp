<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/cotizacion/cotizacionAuto.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/dwr/listado.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<sj:head/>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript">
	var seleccionarAgentePath = '<s:url action="ventanaAgentes" namespace="/suscripcion/cotizacion/auto"/>';
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="detalle" >
	<center>
	<s:form action="crearCotizacion" id="cotizacionAutoForm">
	<s:hidden name="cotizacion.solicitudDTO.idToSolicitud"/>
		<s:hidden name="complementarCreacion" value="%{complementarCreacion}"/>
		<s:hidden id="idToCotizacion" name="cotizacion.idToCotizacion"/>
		<s:set id="readOnly" value="false" />
		<s:set id="search" value="true" />
		<s:if test="complementarCreacion">
			<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
			<s:set id="readOnly" value="true" />
			<s:hidden id="idToCotizacion" name="cotizacion.idToCotizacion"/>
		</s:if>
		<s:else>
			<s:set id="claveTextoBoton" value="%{getText('midas.cotizacion.crear')}" />
		</s:else>
		<div class="subtituloIzquierdaDiv">Información del Usuario</div>
		<table id="agregar">
			<tr>
				<th><label>Id Usuario</label></th>
				<td><s:property value="usuario.id"/></td>
			</tr>
			<tr><th><label>Nombre Usuario</label></th>
				<td><s:property value="usuario.nombre"/></td>
			</tr>
		</table>
		<s:action name="getInfoAgente" var="agenteInfo" namespace="/componente/agente" ignoreContextParams="true" executeResult="true" >
			<s:param name="idTcAgenteCompName">cotizacion.solicitudDTO.codigoAgente</s:param>
			<s:param name="nombreAgenteCompName">agente.persona.nombre</s:param>
			<s:param name="permiteBuscar"><s:property value="#search"/></s:param>		
		</s:action>	
		<table id="agregar">
			<tr>
				<td class="titulo" colspan="2">
					<s:text name="midas.cotizacion.crear" /> 
				</td>
			</tr>
			<tr>
				<td>		
				<s:select name="cotizacion.solicitudDTO.negocio.idToNegocio" id="negocio" 
						      key="midas.cotizacion.negocio" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioAgenteList" headerKey="" required="true"
						      listKey="idToNegocio" listValue="descripcionNegocio"
						      disabled="#readOnly" 
						      onchange="cargarComboSimpleDWR(this.value, 'producto', 'getMapNegProductoPorNegocio');mostrarBotonCrearCotizacion();cleanCmbo('producto','tipoPoliza','idMoneda');"   >
				 </s:select>	
				</td>
				<td width="150px"><div class="w150" style="float:left;"> </div></td>				
			</tr>
			<tr>
				<td>
					<s:select name="cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto" id="producto" 
						      key="midas.cotizacion.producto" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioProductoList" headerKey="" required="true"
						      disabled="#readOnly"
						      onchange="cargarComboSimpleDWR(this.value, 'tipoPoliza', 'getMapNegTipoPolizaPorNegProducto');mostrarBotonCrearCotizacion();cleanCmbo('','tipoPoliza','idMoneda');">
				    </s:select>					
				</td>
				<td><div class="w150" style="float:left;"> </div></td>					
			</tr>
			<tr>
				<td>
					<s:select name="cotizacion.negocioTipoPoliza.idToNegTipoPoliza" id="tipoPoliza" 
						      key="midas.cotizacion.tipopoliza" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioTipoPolizaList" headerKey="" required="true"
						      onchange="cargarComboSimpleDWR(this.value, 'idMoneda', 'getMapMonedaPorNegTipoPoliza');mostrarBotonCrearCotizacion();cleanCmbo('','','idMoneda');">
				    </s:select>					
				</td>
				<td><div class="w150" style="float:left;"> </div></td>		
			</tr>
			<tr>
				<td>
					<s:select name="cotizacion.idMoneda" id="idMoneda" 
						      key="midas.general.moneda" cssClass="cajaTexto"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="monedasList" headerKey="" required="true"
						      onchange="mostrarBotonCrearCotizacion();">
				    </s:select>
				</td>
				<td><div class="w150" style="float:left;"> </div></td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="divCrearBtn" class="w170" style="display: none; float:right;">
						<div class="btn_back w170"  >
							<a href="javascript: void(0);" onclick="crearCotizacion();" class="icon_guardar">	
								<s:property value="%{#claveTextoBoton}"/>	
							</a>
                      	</div>
                     </div>
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);"
							onclick="javascript:cerrarVentanaCrearCotizacion();"> <s:text
							name="midas.boton.cancelar" /> </a>
					</div>
				</td>	
			</tr>
			<tr>
				<td colspan="2"> 
					<span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span>
				</td>
			</tr>
		</table>
	</s:form>
	</center>
</div>
<s:if test="negocioAgenteList.size() == 1">
	<script>
 jQuery(document).ready(function(){
	 excuteOnchange('negocio');
 });
</script>
</s:if>
<s:if test="cotizacion.solicitudDTO.idToSolicitud != null && cotizacion.solicitudDTO.negocio.claveDefault == 1">
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#negocio").attr('disabled','disabled'); 
		jQuery("#producto").attr('disabled','disabled'); 
		jQuery("#divBuscarBtn").find('a').attr('onclick','');
	});
	
	</script>
</s:if>
<s:elseif test="cotizacion.solicitudDTO.idToSolicitud != null">
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#negocio").attr('disabled','disabled'); 
		jQuery("#producto").attr('disabled','disabled'); 
		jQuery("#divBuscarBtn").hide();
		jQuery("#divBuscarBtn").find('a').attr('onclick','');
	});
	</script>
</s:elseif>
