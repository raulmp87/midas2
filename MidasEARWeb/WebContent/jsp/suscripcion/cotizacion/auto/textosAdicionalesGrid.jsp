<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		 <column id="texAdicionalCotDTO.idToTexAdicionalCot" type="ro" width="*" sort="int" hidden="true" >idToTexAdicionalCot</column>
		<column id="texAdicionalCotDTO.cotizacion.idToCotizacion" type="ro" width="0" sort="false" hidden="true">texAdicionalCotDTO.cotizacion.idToCotizacion</column>
		<column id="texAdicionalCotDTO.descripcionTexto" type="ed" width="*" sort="str" ><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional"/></column>
		<column id="texAdicionalCotDTO.nombreUsuarioCreacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.solicitadoPor"/></column>
		<column id="texAdicionalCotDTO.nombreUsuarioModificacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizadoPor"/></column>
		<column id="texAdicionalCotDTO.descripcionEstatusAut" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizacion"/></column>
		<column id="texAdicionalCotDTO.fechaCreacion" type="ro" width="*" sort="date_custom"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.fecha"/></column>
		
	</head>

	<% int a=0;%>
	<s:iterator value="textosAdicionales">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToTexAdicionalCot" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacion.idToCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTexto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuarioModificacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatusAut" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>