<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cobranza/cobranza.js'/>"></script>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<div id="mensajeCobInc" style="display:none"><s:property value="mensaje"/></div>
<div id="tipoMensajeCobInc" style="display:none"><s:property value="tipoMensaje"/></div>
<div id="nextFunctionCobInc" style="display:none"><s:property value="nextFunction"/></div>
<div id="wrapper">
	<div id="faux">
		<s:form id="cobranzaForm" action="guardarCobranzaInciso">
			<s:hidden id="idToCotizacion" name="cotizacionDTO.idToCotizacion" ></s:hidden>
			<div id="leftcolumn">
				<div id="accordion1" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Medios de pago</a>
					</h3>
					<div id="first" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">
						<br /> <br />
						<div id="block_0">
						<div id="clienteCobs">
						<s:select id="idToPersonaContratante"
								list="clientesCobro"
 								name="idToPersonaContratante"  
 								key="midas.cliente.nombreCliente"
 								cssStyle="width:100px;max-width: 200px;"
 								cssClass="txtfield jQrequired"								
								labelposition="left" 
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								 onchange="onchangeMediosInciso()"
								 />					
						</div>
						</div>
						<div id="block_1">
							<div id="medios">
							<s:select id="medioPagoDTOs" 
								list="#{'8':'COBRANZA DOMICILIADA','4':'TARJETA DE CREDITO','1386':'CUENTA AFIRME (CREDITO O DEBITO)'}"
								name="idMedioPago" headerKey=""
								key="midas.cliente.datosCobranza.grid.column.medioPago"
								labelposition="left"
								cssClass="txtfield jQrequired" headerValue="%{getText('midas.general.seleccione')}"
								cssStyle="width:200px;"
								onchange="onchangeMediosInciso()"
								disabled="%{#disabledConsulta}"
								labelposition="left">
							</s:select>
							</div>
							<div id="block_2">
								<div id="conductos_div">
								<s:select id="conductos" name="idConductoCobro" list="conductosDeCobro"
									key="midas.suscripcion.cotizacion.auto.cobranzaInciso.conductosCobro" labelposition="left"
									headerValue="%{getText('midas.general.seleccione')}"
									disabled="%{#disabledConsulta}"
									headerKey="" cssClass="txtfield" cssStyle="width:200px;"
									onchange="onchangeConductosInciso();"
					 			/>								
								</div>
							</div>
						</div>
						<div id="checkDatos">
							<s:select id="titularIgual" name="datosIguales"
								list="mapDatosIguales" headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								label="�El titular es la misma persona que el cliente de la p�liza?"
								labelposition="left"
								disabled="%{#disabledConsulta}"
								cssClass="txtfield jQrequired" cssStyle="width:100px;"
								onchange="onchangeTitularIncisoIgual();" />						
						</div>
						<div id="datosTitular_div"></div>
						<s:if test="soloConsulta != 1">
						<div class="guardarCobranza">
							<div class="btn_back w110">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="guardarCobranzaInciso();"> <s:text
										name="midas.boton.guardar" /> </a>
							</div>
						</div>
						</s:if>
					</div>
				</div>
				<div id="accordion2" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Datos titular tarjeta/cuenta</a>
					</h3>
					<div id="second" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">

					</div>
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Datos tarjeta / cuenta </a>
					</h3>
					<div id="third" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" style="height:238px;">
						<div id="infoCuentasCobranza">
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div id="rightcolumn">				
			</div>
		</s:form>
	</div>
</div>
<s:if  test="tipoMensaje != null && mensaje != null">
<script type="text/javascript"> 
	muestraMensajeInformativoCobranzaInciso();
</script>
</s:if>