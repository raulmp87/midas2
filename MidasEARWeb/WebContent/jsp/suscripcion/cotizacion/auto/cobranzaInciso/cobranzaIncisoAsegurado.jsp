<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:form id="cobBuscarIncisoForm">
	<s:hidden id="idToPersonaContratante" name="cotizacionDTO.idToPersonaContratante" ></s:hidden>
	<s:hidden id="idToCotizacion" name="cotizacionDTO.idToCotizacion" ></s:hidden>
	<table id="agregar" style="width: 95%;" class="fixTabla">
		<tr>
			<td width="150px">
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.numero.secuencia" />:</label>
			</td>
			<td><s:textfield
					name="incisoCotizacion.id.numeroInciso" cssClass="cajaTexto"
					cssStyle="width: 100px;"
					onkeypress="return soloNumeros(this, event, false);"></s:textfield>
			</td>
			<td width="150px">
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.linea.negocio" />:</label>
			</td>
			<td width=""><s:select list="negocioSeccionList"
					listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.negocioSeccionId"
					id="incisoCotizacion.incisoAutoCot.negocioSeccionId"
					onchange="cargarComboPaquetesCob('%{cotizacionDTO.idToCotizacion}',this.value);"
					cssClass="txtfield">
				</s:select>
			</td>
		</tr>
		<tr>
			<td>
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.descripcion" />:</label>
			</td>
			<td><s:select list="descripcionesIncisos" headerKey="null"
					headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.descripcionFinal"
					id="descripcionesIncisos" cssClass="txtfield">
				</s:select>
			</td>
			<td>
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.paquete" />:</label>
			</td>
			<td><s:select list="paquetes" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
					id="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
					cssClass="txtfield" />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div class="btn_back w140"
					style="display: inline; float: right; margin-right: .9em;">
					<a href="javascript: void(0);" onclick="buscarIncisosCobPaginadoAseg(1,true);">
						<s:text name="midas.suscripcion.cotizacion.buscar" /> </a>
				</div>
			</td>
		</tr>
	</table>
</s:form>
<div id="indicadorCobIncisos"></div>
<div id="gridListadoDeIncisos" style="margin: 10px;"></div>
<div class="row" style="margin-left: -31px;">
<div class="btn_back w140" style="display: inline; float: right;">
	<a href="javascript: void(0);" onclick="guardarMediosPago();"
		class="icon_guardar2"> <s:text name="midas.boton.guardar" /> </a>
</div>
</div>
<script type="text/javascript">
	buscarIncisosCobPaginadoAseg(1,true);
</script>