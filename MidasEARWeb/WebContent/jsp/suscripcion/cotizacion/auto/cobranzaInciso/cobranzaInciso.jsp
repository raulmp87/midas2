<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:form id="cobIncisoForm">
	<s:hidden id="idToPersonaContratante" name="cotizacionDTO.idToPersonaContratante" ></s:hidden>
	<s:hidden id="idToCotizacion" name="cotizacionDTO.idToCotizacion" ></s:hidden>
	<table id="agregar" style="width: 95%;" class="fixTabla">
		<tr>
			<td>
				<div id="wwgrp_monto" class="wwgrp">
					<span id="wwlbl_monto" class="wwlbl">
						<label  class="label">Monto:</label> <s:property value="esquemaPagoCotizacionDTO.pagoInicial.importeFormat"/>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="medioPagoDTOs" 
					list="#{'8':'COBRANZA DOMICILIADA','4':'TARJETA DE CREDITO','1386':'CUENTA AFIRME (CREDITO O DEBITO)','15':'EFECTIVO'}"
					name="idMedioPago" headerKey=""
					label="Medio de Pago" labelposition="left"
					cssClass="txtfield jQrequired" headerValue="%{getText('midas.general.seleccione')}"
					cssStyle="width:200px;"
					onchange="onchangeMediosInciso()"
					disabled="%{#disabledConsulta}"
					label="Medio de Pago" labelposition="left">
				</s:select>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="conductos" name="idConductoCobro" list="conductosDeCobro"
					label="Conductos de cobro" labelposition="left"
					headerValue="%{getText('midas.general.seleccione')}"
					disabled="%{#disabledConsulta}"
					headerKey="" cssClass="txtfield" cssStyle="width:200px;"
					 />
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w140"
					style="display: inline; float: right; margin-right: .9em;">
					<a href="javascript: void(0);" onclick="aplicarCob();">
						<s:text name="midas.suscripcion.cotizacion.auto.cobranzaInciso.aplicar" /> </a>
				</div>
				<div class="btn_back w140"
					style="display: inline; float: right; margin-right: .9em;">
					<a href="javascript: void(0);" onclick="aplicarCobTodos();">
						<s:text name="midas.suscripcion.cotizacion.auto.cobranzaInciso.aplicarTodos" /> </a>
				</div>				
			</td>
		</tr>
	</table>	
</s:form>
<s:form id="cobBuscarIncisoForm">
	<table id="agregar" style="width: 95%;" class="fixTabla">
		<tr>
			<td width="150px">
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.numero.secuencia" />:</label>
			</td>
			<td><s:textfield
					name="incisoCotizacion.id.numeroInciso" cssClass="cajaTexto"
					cssStyle="width: 100px;"
					onkeypress="return soloNumeros(this, event, false);"></s:textfield>
			</td>
			<td width="150px">
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.linea.negocio" />:</label>
			</td>
			<td width=""><s:select list="negocioSeccionList"
					listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.negocioSeccionId"
					id="incisoCotizacion.incisoAutoCot.negocioSeccionId"
					onchange="cargarComboPaquetesCob('%{cotizacionDTO.idToCotizacion}',this.value);"
					cssClass="txtfield">
				</s:select>
			</td>
		</tr>
		<tr>
			<td>
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.descripcion" />:</label>
			</td>
			<td><s:select list="descripcionesIncisos" headerKey="null"
					headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.descripcionFinal"
					id="descripcionesIncisos" cssClass="txtfield">
				</s:select>
			</td>
			<td>
				<label><s:text name="midas.suscripcion.cotizacion.auto.inciso.paquete" />:</label>
			</td>
			<td><s:select list="paquetes" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
					id="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
					cssClass="txtfield" />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div class="btn_back w140"
					style="display: inline; float: right; margin-right: .9em;">
					<a href="javascript: void(0);" onclick="buscarIncisosCobPaginado(1,true);">
						<s:text name="midas.suscripcion.cotizacion.buscar" /> </a>
				</div>
			</td>
		</tr>
	</table>
</s:form>
<div id="indicadorCobIncisos"></div>
<div id="gridListadoDeIncisos" style="margin: 10px;"></div>
<script type="text/javascript">
	buscarIncisosCobPaginado(1,true);
</script>
