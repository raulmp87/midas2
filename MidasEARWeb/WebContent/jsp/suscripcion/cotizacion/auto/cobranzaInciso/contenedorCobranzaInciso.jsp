<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include	value="/jsp/suscripcion/cotizacion/auto/cobranzaInciso/cobranzaIncisoHeader.jsp"></s:include>
<div id="indicadorContenedor"></div>
<script type="text/javascript">
	mostrarIndicadorCarga('indicadorContenedor');
</script>
<div hrefmode="ajax-html" style="height: 405px; width: 900px" id="cotizacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="150px" id="cobIncisoContratante" name="<s:text name="midas.suscripcion.cotizacion.auto.cobranzaInciso.porContrantante" />" href="http://void" extraAction="javascript: verCobIncisoContratante(<s:property value="cotizacionDTO.idToCotizacion" escapeHtml="false" default="null"/>);">
	</div>
	<div width="150px" id="cobIncisoAsegurado" name="<s:text name="midas.suscripcion.cotizacion.auto.cobranzaInciso.porAsegurado" />" href="http://void" extraAction="javascript: verCobIncisoAsegurador(<s:property value="cotizacionDTO.idToCotizacion" escapeHtml="false" default="null"/>);">
	</div>
	<div width="150px" id="cobIncisoAgregarNuevo" name="<s:text name="midas.suscripcion.cotizacion.auto.cobranzaInciso.AgregarCobranza" />" href="http://void" extraAction="javascript: verCobIncisoAgregarNuevo(<s:property value="cotizacionDTO.idToCotizacion" escapeHtml="false" default="null"/>);">
	</div>		
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>