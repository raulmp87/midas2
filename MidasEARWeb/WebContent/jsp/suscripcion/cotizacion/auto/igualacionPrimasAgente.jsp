<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>



<sj:head/>	
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	function ejecutarIgualacion(){
		var idControlArchivo = jQuery("#idControlArchivo").val();
		if(idControlArchivo == null || idControlArchivo == '' || idControlArchivo == undefined){
			parent.mostrarMensajeInformativo("Favor de Cargar el Documento para la Igualacion de la cotizaci\u00F3n", "20");
			return;
		}
		if(confirm('\u00BFEst\u00e1 seguro que desea igualar la prima de la cotizaci\u00F3n\u003F')){	
			var url = '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/igualarPrimasAgente.action';
			jQuery('#primaAIgualar').val(parent.removeCurrency(jQuery('#primaAIgualar').val()));	
			var form = jQuery('#igualarPrimasForm');			
			parent.redirectVentanaModal('igualarPrimasAgente', url, form);
		}
	}
	
	function eliminarIgualacion(){
		if(confirm('\u00BFEst\u00e1 seguro que desea restaurar la prima de la cotizaci\u00F3n\u003F')){			
			var form = jQuery('#igualarPrimasForm');	
			var url = '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/restaurarPrimaCotizacionAgente.action';
			parent.redirectVentanaModal('igualarPrimasAgente', url, form);
		}		
	}
</script>
<s:form action="igualarPrimasAgente" namespace="/suscripcion/cotizacion/auto/cotizadoragente" id="igualarPrimasForm">
	<s:hidden name="idToCotizacion" id="idToCotizacion"/>
	<div id="detallePrimaObjetivo" >
		<center>
			<table id="agregar" width="100%">
				<tr>
					<th colspan="2">
						<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.title" /> 
					</th>				
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td colspan="2">
						<s:hidden id="idControlArchivo" name="idControlArchivo" />
						<s:textfield name="nombreArchivoPrima" 
						   required="#requiredField"
			               key="midas.suscripcion.solicitud.comentarios.archivo"
				           labelposition="left" 				          				 							  
						   id="nombreArchivoPrima" cssClass="txtfield jQrestrict jQrequired"	cssStyle="width: 200px;" 						   								  
						   readonly="true" >
						</s:textfield>		
					</td>
				</tr>	 
				<tr>				
					<td colspan="2">
						<s:textfield name="primaAIgualar" 
						   required="#requiredField"
			               key="midas.suscripcion.cotizacion.igualacionPrimas.primaTotal"
				           labelposition="left" 				          				 							  
						   id="primaAIgualar" maxlength="10" cssClass="txtfield jQrestrict jQrequired"	cssStyle="width: 200px;" 						   								  
						   onkeypress="return parent.soloNumeros(this, event, true)">
						</s:textfield>		
					</td>
				</tr>   
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="right">												
						<div id="cerrar" class="btn_back w120" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('igualarPrimasAgente');"> <s:text
								name="midas.boton.cerrar" /> </a>
						</div>
						<div id="aceptar" class="btn_back w120" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="ejecutarIgualacion();"> <s:text
								name="midas.boton.aceptar" /> </a>
						</div>	
						<div id="eliminar" class="btn_back w120" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="eliminarIgualacion();"> <s:text
								name="midas.suscripcion.cotizacion.igualacionPrimas.eliminar" /> </a>
						</div>
						<div id="archivo" class="btn_back w120" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="parent.importarArchivoPrimaObjetivo();"> <s:text
								name="midas.agentes.cargaMasiva.cargarArchivo" /> </a>
						</div>														
					</td>
				</tr>
			</table>
		</center>	
	</div>
</s:form>
