<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/suscripcion/cotizacion/auto/asegurado/ComplementarAseguradoHeader.jsp"/>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>

<script type="text/javascript">
	var tipoPersona = '<s:property value="incisoCotizacion.cotizacionDTO.solicitudDTO.claveTipoPersona"/>';
	var guardarAseguradoPath = '<s:url action="asegurado.actualizarDatosAsegurados" namespace="/vehiculo/inciso"/>';
</script>

<script type="text/javascript">

	function onChangeDatosAsegurados(inputSelect) {
		switch (inputSelect) {
		case '2':	
			jQuery('#divInput').show();
			jQuery('#divAgregar').hide();
			jQuery('#divGuardar').show();
			break;
		case '3':
			jQuery('#divInput').hide();
			jQuery('#divAgregar').show();
			jQuery('#divGuardar').hide();
			break;
		default:
			jQuery('#divInput').hide();
			jQuery('#divAgregar').hide();
			jQuery('#divGuardar').show();
		}
	}
	function estaVisible(id){
		if(jQuery('#'+id).is(':visible')){
			jQuery('#'+id).hide();
		}else{
			jQuery('#'+id).show();
		}
	}
	
	function asignarIdCliente(idCliente, idDomCliente){
		muestraResultadoAgregarCliente(idCliente, idDomCliente);		
	}
	
	function guardarAsegurado(){	
		var form = jQuery('#complementarAseguradoForm')[0]; 
		var nextFunction = "&nextFunction=closeVentanaAsegurado("+jQuery('#idToCotizacion').val()+")";
		var url = guardarAseguradoPath + '?' + jQuery(form).serialize() + nextFunction;		
		parent.redirectVentanaModal('ventanaAsegurado', url, null);	
	}	
	
</script>
<div id="ventana" style="height:100%; overflow:hidden">
	<s:form id="complementarAseguradoForm" action="asegurado.actualizarDatosAsegurados" namespace="/vehiculo/inciso">
		<s:hidden id="numeroInciso" name="incisoCotizacion.id.numeroInciso"/>
		<s:hidden id="idToCotizacion" name="incisoCotizacion.id.idToCotizacion"/>
		<s:hidden id="personaContratante" name="incisoCotizacion.cotizacionDTO.idToPersonaContratante"/>
		<s:hidden id="personaAsegurada" name="incisoCotizacion.incisoAutoCot.personaAseguradoId"/>
		
	  	<div id="agregar" style="height:100px; width:380px;">
			<s:radio name="radioAsegurado"
				theme="simple"
				list="#{'1':' Los Datos del Asegurado son Iguales a los del Contratante <br/>','2':'Asignar Solo el Nombre <br/>','3':'Asignar Un Cliente Ya Existente <br/>'}"
				onclick="onChangeDatosAsegurados(this.value);"
				id="radioAsegurado" cssStyle="agregar" />

			<div id="divInput" style="display:none;">
				<s:textfield key="Capturar Cliente"
					name="incisoCotizacion.incisoAutoCot.nombreAsegurado"
					labelposition="%{getText('label.position')}"
					cssClass="txtfield"
					cssStyle="width:200px;"
					id="nombreAsegurado" />
			</div>
			<div id="divMensaje" style="padding-top: 14;text-align: center;">
				Asegurado: <s:property value="incisoCotizacion.incisoAutoCot.nombreAseguradoUpper" default="No disponible"/>
				<s:textfield id="idAsegurado" readonly="true"  cssStyle="display:none;" />
				<s:textfield id="domidAsegurado" readonly="true"  cssStyle="display:none;" />
			</div>
		</div>
		<div style="width:410px;" >
		<div id="divSalirBtn" class="btn_back w100" style="display:inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="parent.cerrarVentanaModal('ventanaAsegurado');"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
		<div id="divAgregar" style="display:none; float: right;" class="btn_back w100">
			<a href="javascript: void(0);" onclick="mostrarAgregarCliente();"> <s:text
					name="midas.suscripcion.cotizacion.buscar" /> </a>
		</div>
		
		<div id="divGuardar" style="display: block; float:right;">
			<div class="btn_back w100"  style="display:inline; float: left; ">
			   <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea guardar el asegurado?')){guardarAsegurado();}"> 
				  <s:text name="midas.boton.guardar" /> </a>
			</div>				
		</div>
		</div>
		<div id="indicador"></div>
		<div id="central_indicator" class="sh2" style="display: none;">
		<img id="img_indicator" name="img_indicator"
			src="/MidasWeb/img/as2.gif" alt="Afirme" />
	  </div>
  </s:form>
</div>
<script type="text/javascript">
	onChangeDatosAsegurados('<s:property value="radioAsegurado" />');
</script>