<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>

<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript">		
	function getPaquete(idToCotizacion, onCloseFunction) {
		var url = "/MidasWeb/suscripcion/cotizacion/auto/cambiosglobales/getPaquete.action?id="+ idToCotizacion+"&idToNegSeccion="+jQuery('#idToNegSeccion').val()+"&idToNegPaqueteSeccion="+jQuery('#idToNegPaqueteSeccion').val();
		parent.mostrarVentanaModal("Aplicar", "Aplicar a", 200,320, 600, 200, url,null);	
  	}	

	function reestablecerValoresDelNegocio(){
		if(confirm('\u00BFEst\u00e1 seguro que desea restablecer los valores del negocio?')){	
			var url = "/MidasWeb/suscripcion/cotizacion/auto/cambiosglobales/reestablecerValoresDelNegocio.action";
			var parameters = "?id=" + jQuery('#id').val() + "&idToNegSeccion=" + jQuery('#idToNegSeccion').val()  +
				"&idToNegPaqueteSeccion=" + jQuery('#idToNegPaqueteSeccion').val()
			url = url + parameters;		
			parent.redirectVentanaModal("CambiosGlobales", url, null);			
		}
	}
	
	
	function guardarPlantilla(){
			var mensaje = "\u00BFEst\u00e1 seguro que desea guardar la plantilla\u003F " +
			"\nRecuerde que solo se guardaran los cambios en aquellas coberturas que est\u00e9n seleccionadas en 'Cambiar en'";
			if(confirm(mensaje)){					
				parent.submitVentanaModal("CambiosGlobales", jQuery('#cambiosGlobalesForm'));		
// 				alert(jQuery('#cambiosGlobalesForm').serialize());
			}
	}
	
	jQuery(document).ready(function(){
			if(jQuery('#idToNegPaqueteSeccion').val() != null && jQuery('#idToNegPaqueteSeccion').val() != ''){
				obtenerCoberturaCotizacionesCambiosGlobales();
			}			
		});
	
	
	function cerrarVentana(idToCotizacion){
	        parent.cerrarVentanaModal("CambiosGlobales");
	}	
	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="guardarConfiguracion" namespace="/suscripcion/cotizacion/auto/cambiosglobales"  id="cambiosGlobalesForm">
	<s:hidden name="id" id="id" />
	<table class="contenedorMarco" width="100%">
		<tr>
			<td class="subtitulo align-left" colspan="4">
				<s:text name="midas.suscripcion.cotizacion.auto.cambiosglobales.titulo"/>
			</td>
		</tr>
		<tr id="trNormal2" style="display:inline;">
			<td>
				<s:text name="midas.suscripcion.cotizacion.auto.cambiosglobales.lineaNegocio"></s:text>
			</td>
			<td><s:select 
				id="idToNegSeccion" name="idToNegSeccion" cssClass="cajaTextoM2 w200"
				headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
				list="negocioSeccionList" id="idToNegSeccion" listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
				onchange="onChangeLineaNegocioporPaquete()"/>
			</td>				
			<td>
				<s:text name="midas.suscripcion.cotizacion.auto.cambiosglobales.paquete"></s:text>
			</td>
			<td>
				<s:select
				   id="idToNegPaqueteSeccion" name="idToNegPaqueteSeccion" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       list="negocioPaqueteSeccionList" id="idToNegPaqueteSeccion" onchange="obtenerCoberturaCotizacionesCambiosGlobales()"/>
			</td>	
		</tr>
		<tr>
			<td colspan="4">			
				<div id="coberturaCotizacionGrid" ></div>
			</td>
		</tr>	
	</table>
	<table width="100%">
		<tr>
		 <td>
		 <div id="indicador" style="display:inline;"></div>
		</td>
	   </tr>
	</table>
</s:form>