<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	var aplicarPlantillaPath = '<s:url action="aplicarPlantilla" namespace="/suscripcion/cotizacion/auto/cambiosglobales"/>';


	function terminarAplicar(){
			var id = jQuery("#id").val();
			var idToNegSeccion = jQuery("#idToNegSeccion").val();
			var idToNegPaqueteSeccion = jQuery("#idToNegPaqueteSeccion").val();	
			var paquete_id = jQuery("#paquete_id").val();
			var url = aplicarPlantillaPath + '?id='+id+'&idToNegSeccion='+idToNegSeccion+'&idToNegPaqueteSeccion='+idToNegPaqueteSeccion+'&paquete_id='+paquete_id+'&nextFunction=cerrarVentanaModal(\'Aplicar\')';
			//parent.mainDhxWindow.window("Aplicar").attachURL(url);
			parent.redirectVentanaModal('Aplicar', url);			
	}
	

</script>
<div id="agregar">
	<s:form  id="asignarIncisoForm">
		<s:hidden name="id" id="id" />	
		<s:hidden name="idToNegSeccion" id="idToNegSeccion" />		
		<s:hidden name="idToNegPaqueteSeccion" id="idToNegPaqueteSeccion" />			

		<s:select id="paquete_id" 
			list="paqueteList" key="midas.general.paquete"
			name="paquete_id" headerKey="0" labelposition="left"
			headerValue="TODOS" 
			listKey="id" listValue="descripcion" cssStyle="width: 200px;"
			cssClass="cajaTexto" />				
		<br/>
		<br/>
		<br/>
		<div  style="margin-left: 250px;" align="right">
			<div id="divinciso" class="btn_back w100"  style="display: inline; float: right; width:100px;">
		   		<a href="javascript: void(0);"
			  		onclick="terminarAplicar();"> <s:text
				  	name="midas.suscripcion.cotizacion.auto.complementar.aplicar" /> </a>
			</div>
			<div class="btn_back w100"  style="display:inline; float: left; ">
			   <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){parent.cerrarVentanaModal('Aplicar')}"> 
				  <s:text
				  name="midas.suscripcion.cotizacion.auto.complementar.cancelar" /> </a>
			</div>
		</div>		
		<br>
		<br/>		
	</s:form>
</div>