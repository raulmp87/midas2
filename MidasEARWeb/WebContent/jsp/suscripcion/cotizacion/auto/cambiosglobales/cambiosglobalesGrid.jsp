<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript">
function validaRangoSumaAsegurada(obj,min,max,valorDefault){
	var frame = parent.getWindowContainerFrame('CambiosGlobales');
	var valor = null;
	if(frame != null){	
		valor = frame.contentWindow.dwr.util.getValue(obj);
		if(valor > max){
			alert("El valor maximo permitido es: " + max);
			frame.contentWindow.dwr.util.setValue(obj,valorDefault);
		}else if(valor < min){
			alert("El valor minimo permitido es: " + min);
			frame.contentWindow.dwr.util.setValue(obj,valorDefault);
		}
	}else{
		valor = dwr.util.getValue(obj);
		if(valor > max){
			alert("El valor maximo permitido es: " + max);
			dwr.util.setValue(obj,valorDefault);
		}else if(valor < min){
			alert("El valor minimo permitido es: " + min);
			dwr.util.setValue(obj,valorDefault);
		}
	}
}

function habilitaRegistroCobertura(index){
	if(index!=null){
		if(jQuery("#claveContratoBoolean_"+index).is(":checked")){
			jQuery("#valorSumaAsegurada_"+index).removeAttr("readonly");
			jQuery("#valorDeducible_"+index).removeAttr("readonly");
		}else{
			jQuery("#valorSumaAsegurada_"+index).attr("readonly","readonly");
			jQuery("#valorDeducible_"+index).attr("readonly","readonly");
		}
	}
}

/*
function habilitaLineaCobertura(index){
	if(index != null){				
		if(jQuery("#claveCobertura_"+index).is(":checked")){	
			alert('modificable');				
			document.getElementById('tdContrato_'+index).style.display = '';	
			document.getElementById('tdSumaAsegurada_'+index).style.display = '';	
			document.getElementById('tdDeducible_'+index).style.display = '';
		}else{		
			alert('nie');
				
			document.getElementById('tdContrato_'+index).style.display = 'none';	
			document.getElementById('tdSumaAsegurada_'+index).style.display = 'none';	
			document.getElementById('tdDeducible_'+index).style.display = 'none';				
		}
	}
}*/


function validaDisable(combo, index){	
	if(!(jQuery("#claveContratoBoolean_"+index).is(":checked") || jQuery('#coberturaCotizacionList_'+index+'__claveObligatoriedad').val() == '0')){		
		alert('Se necesita contratar la cobertura antes de modificar el valor');
		combo.selectedIndex = 0;		
	}
	
}
</script>
<div style="text-align: center">
		<s:if test="idToNegPaqueteSeccion != null && configuracionPlantillaNeg == null">
		<label class="subtitulo">La línea de negocio y paquete cuenta con configuración de negocio</label>
	</s:if>
	<s:elseif test="idToNegPaqueteSeccion != null">
		<label class="subtitulo">La línea de negocio y paquete cuenta con configuración de cotización</label>
	</s:elseif>
</div>
<table id=t_riesgo border="0">
	<tr>
		<th>Cambiar en</th>			
		<th ><s:text name="midas.suscripcion.cotizacion.inciso.descripcion" /></th>
		<th><s:text name="midas.suscripcion.cotizacion.inciso.claveObligatoriedad" /></th>
		<th><s:text name="midas.suscripcion.cotizacion.inciso.sumaAsegurada" /></th>
		<th><s:text name="midas.suscripcion.cotizacion.inciso.porcDeducible" /></th>
	</tr>
	<s:iterator value="coberturaCotizacionList" status="stat">
	<tr>
		<td class ="txt_v2">
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.numeroInciso" value="%{id.numeroInciso}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToSeccion" value="%{id.idToSeccion}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCobertura" value="%{id.idToCobertura}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCotizacion" value="%{id.idToCotizacion}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveObligatoriedad" value="%{claveObligatoriedad}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].idTcSubramo" value="%{idTcSubramo}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCoaseguro" value="%{valorCoaseguro}" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCuota" value="0" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeCoaseguro" value="0" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutCoaseguro" value="0" />						
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutDeducible" value="0" />
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].numeroAgrupacion" value="0" />
			<s:if test="claveContrato==1">
				 <s:set name="readOnly" value="false"/>
			</s:if>
			<s:else>
				 <s:set name="readOnly" value="true"/>
			</s:else>
			<s:checkbox type="checkbox" id="claveCobertura_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].claveCobertura" />
		</td>		
		<td class ="txt_v2"><s:property value="coberturaSeccionDTO.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true" /></td>
		<td id="tdContrato_<s:property value="#stat.index" />" class="txt_v2">
			<s:if test="claveContrato==1 && claveObligatoriedad==0" >
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="true"/>
				<input type="checkbox" id="claveContratoBoolean_%{#stat.index}"  name="coberturaCotizacionList[%{#stat.index}].claveContrato" checked="checked" disabled/>
			</s:if>
			<s:else>
				<s:checkbox id="claveContratoBoolean_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="%{claveContrato}" fieldValue="true" onchange="habilitaRegistroCobertura(%{#stat.index});"/>			
			</s:else>								
		</td>
		<td id="tdSumaAsegurada_<s:property value="#stat.index" />" class ="txt_v2">
			<s:if test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 0">
				<s:if test="valorSumaAseguradaMax != 0.0 && valorSumaAseguradaMin!= 0.0">
					<div style="font-size:8px;">
						*Entre <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true" /> y <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />
					</div>
					<s:textfield id ="valorSumaAsegurada_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="" onblur="validaRangoSumaAsegurada(this,%{valorSumaAseguradaMin},%{valorSumaAseguradaMax},%{valorSumaAsegurada});" readonly="#readOnly"/>
				</s:if>
				<s:else>
					<s:textfield id="valorSumaAsegurada_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup=""  readonly="#readOnly"/>
				</s:else>
			</s:if>

			<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 1">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}"  />
				<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
			</s:elseif>
			<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 2">
				<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
				<s:textfield id ="valorSumaAsegurada_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="" readonly="#readOnly"/>
			</s:elseif>
			<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 3">
				<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}"  />
			</s:elseif>
			<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 9">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada" value="%{valorSumaAsegurada}"  />
				<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
			</s:elseif>
			<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 10">
				<s:text name="midas.suscripcion.cotizacion.inciso.valorCaratula"/>
				<s:textfield id ="valorSumaAsegurada_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="" readonly="readonly"/>
			</s:elseif>
		</td>
		<td id="tdDeducible_<s:property value="#stat.index" />" class ="txt_v2">
			<s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/>	
			<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveTipoDeducible" value="%{coberturaSeccionDTO.coberturaDTO.claveTipoDeducible}"></s:hidden>			
			<s:if test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "0"'>
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
			</s:if>
			<s:elseif test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "1"'>
				<s:select id="porcentajeDeducible_%{#stat.index}"
					list="%{deducibles}"
					disabled="%{#disabledConsulta}"
					name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					onchange="validaDisable(this, %{#stat.index});"
					listKey="valorDeducible" listValue="valorDeducible"
					cssClass="txtfield"
					 />	
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
			</s:elseif>
			<s:else>
				<s:select id="valorDeducible_%{#stat.index}"
					list="%{deducibles}"
					disabled="%{#disabledConsulta}"
					name="coberturaCotizacionList[%{#stat.index}].valorDeducible" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					onchange="validaDisable(this, %{#stat.index});"
					listKey="valorDeducible" listValue="valorDeducible"
					cssClass="txtfield"
					 />	
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0"  />
			</s:else>	
		</td>		
		</tr>
	</s:iterator>	
</table>

<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<s:textarea name="comentarios" cssClass="textfield"
				cols="120"
				rows="3"
				maxlength="500" 
				labelposition="top"
				key="midas.suscripcion.solicitud.rechazar.comentario"
				javascriptTooltip="%{getText('midas.suscripcion.solicitud.rechazar.comentario')}" 
				/>
		</td>
	</tr>
	<tr>
	    <td>
			<div id="divinciso" class="btn_back w140"  style="display: inline; float: left; width:150px;">
			   <a href="javascript: void(0);"
				  onclick="getPaquete(<s:property value="id"/>);"> <s:text
				  name="midas.suscripcion.cotizacion.auto.complementar.incisosActuales" /> </a>
			</div>		
			<div class="btn_back w140" style="display: inline; float: left; width:200px;">
			   <a href="javascript: void(0);"
				  onclick="reestablecerValoresDelNegocio();"> <s:text
				  name="midas.suscripcion.cotizacion.auto.complementar.restablecerValores" /> </a>
			</div>
			<div class="btn_back w140"  style="display:inline; float: left; ">
			   <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){cerrarVentana(<s:property value="id"/>)}"> 
				  <s:text
				  name="midas.suscripcion.cotizacion.auto.complementar.cancelar" /> </a>
			</div>		
			<div class="btn_back w140"  style="display:inline; float: left; ">
			   <a href="javascript: void(0);"
				  onclick="guardarPlantilla();"> <s:text
				  name="midas.suscripcion.cotizacion.auto.complementar.guardar" /> </a>
			</div>		
			<div class="btn_back w140" style="display: inline, none; float: left;">		  
				<s:submit key="midas.suscripcion.cotizacion.auto.complementar.guardar"
					label="%{getText('midas.boton.guardar')}" id="guardarCambiosGlobalesBtn"
					onclick="javascript: mostrarIndicadorCarga('indicador');"
					cssClass="b_submit" cssStyle="display: none" />		  
			</div>					
		</td>
	</tr>
</table>	

	

