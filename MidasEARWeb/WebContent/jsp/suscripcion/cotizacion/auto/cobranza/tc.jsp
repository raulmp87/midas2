<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<style>
<!--
.input_text {
	max-width: 200px;
	min-width: 200px;
}
-->
</style>
	<table id="agregar">
		<tr>
			<td for="idBancoCobranza">Instituci&oacute;n Bancaria</td>
			<td><s:select list="bancos" name="cuentaPagoDTO.idBanco"
					id="idBancoCobranza" headerKey="" headerValue="Seleccione"
					listKey="idBanco" cssClass="txtfield input_text jQrequired"
					listValue="nombreBanco"></s:select>
			</td>
		</tr>
		<tr>
			<td for="idTipoTarjetaCobranza">Tipo de Tarjeta</td>
			<td>
				<s:if test="cuentaPagoDTO.cuenta != null">
					<s:select list="#{'VS':'VISA','MC':'Master Card','TARJETA DE CRED':'TARJETA DE CREDITO'}"
						headerKey="" headerValue="Seleccione ..."
						cssClass="txtfield input_text" name="tipoTarjeta"
						id="idTipoTarjetaCobranza">
					</s:select>
				</s:if>
				<s:else>
					<s:select list="#{'VS':'VISA','MC':'Master Card'}"
						headerKey="" headerValue="Seleccione ..."
						cssClass="txtfield input_text jQrequired" name="tipoTarjeta"
						id="idTipoTarjetaCobranza">
					</s:select>
				</s:else>
			</td>
		</tr>
		<tr>
			<td for="cuenta">N&uacute;mero de Tarjeta</td>
			<td><s:textfield id="cuenta"
					name="cuentaPagoDTO.cuenta" cssClass="txtfield input_text jQrequired"
					onkeypress="return soloNumeros(this, event, false)" maxlength="16" />
			</td>
		</tr>
		<tr>
			<td for="codigoSeguridad">C&oacute;digo de Seguridad</td>
			<td>
				<s:if test="cuentaPagoDTO.cuenta != null">
					<s:textfield id="codigoSeguridad" name="cuentaPagoDTO.codigoSeguridad" maxlength="3" cssClass="txtfield input_text" onkeypress="return soloNumeros(this, event, false)" maxlength="4" />
				</s:if>
				<s:else>
					<s:textfield id="codigoSeguridad" name="cuentaPagoDTO.codigoSeguridad" maxlength="3" cssClass="txtfield input_text jQrequired" onkeypress="return soloNumeros(this, event, false)" maxlength="4" />
				</s:else>
			</td>
		</tr>
		<tr>
			<td for="fechaVencimiento">Fecha vencimiento (MMYY)</td>				
			<td>
				<s:if test="cuentaPagoDTO.cuenta != null">
					<s:textfield id="fechaVencimiento" name="cuentaPagoDTO.fechaVencimiento" cssClass="txtfield input_text" onkeypress="return soloNumeros(this, event, false)" maxlength="4" cssStyle="width: 60px;" />
				</s:if>
				<s:else>
					<s:textfield id="fechaVencimiento" name="cuentaPagoDTO.fechaVencimiento" cssClass="txtfield input_text jQrequired" onkeypress="return soloNumeros(this, event, false)" maxlength="4" cssStyle="width: 60px;" />
				</s:else>
			</td>
		</tr>
		<tr>
			<td for="promociones"><a href="javascript: getPromos();">Promoci&oacute;n</a></td>
			<td>
				<s:select list="promociones" id="promociones" name="cuentaPagoDTO.tipoPromocion"
					cssClass="txtfield input_text" headerKey="" headerValue="SELECCIONE..."/>
			</td>
		</tr>
		<tr>
<!-- 			<td for="diaPagoTarjetaCobranza">D&iacute;a de Pago</td> -->
			<td>
			<s:hidden name="cuentaPagoDTO.diaPago"></s:hidden> 
<%-- 			<s:select name="cuentaPagoDTO.diaPago" --%>
<!-- 					id="diaPagoTarjetaCobranza" headerKey="" -->
<!-- 					headerValue="Seleccione ..." cssClass="txtfield input_text jQrequired" -->
<%-- 					list="#{'1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9','10':'10','11':'11','12':'12','13':'13','14':'14','15':'15','16':'16','17':'17','18':'18','19':'19','20':'20','21':'21','22':'22','23':'23','24':'24','25':'25','26':'26','27':'27','28':'28','29':'29','30':'30','31':'31'}"></s:select> --%>
			</td>
		</tr>
	</table>
