<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include
	value="/jsp/suscripcion/cotizacion/auto/cobranza/cobranzaHeader.jsp"></s:include>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<div id="wrapper">
	<div id="faux">
		<s:form id="cobranzaForm" action="guardarCobranza">
			<s:hidden id="cotizadorAgente" name="cotizadorAgente" />
			<div id="leftcolumn">
				<div id="accordion1" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Medios de pago</a>
					</h3>
					<div id="first" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">
						<br /> <br />
						<div id="block_1">
							<div class="wwgrp">
							<label class="wwlbl">Monto:</label> <s:property value="esquemaPagoCotizacionDTO.pagoInicial.importeFormat"/>
							
							</div>
							<div id="medios">
								<s:select
									id="medioPagoDTOs" 
									list="medioPagoDTOs"
									listKey="idMedioPago" listValue="descripcion"
									name="idMedioPago" headerKey=""
									label="Medio de Pago" labelposition="left"
									cssClass="txtfield jQrequired" headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:200px;"
									onchange=" onchangeMedios()"
									disabled="%{#disabledConsulta}"
									label="Medio de Pago" labelposition="left"></s:select>
							</div>
							<div id="block_2">
								<div id="conductos_div" style="display:none">
									<s:select id="conductos" name="idConductoCobro" list="conductosDeCobro"
										label="Conductos de cobro" labelposition="left"
										headerValue="%{getText('midas.general.seleccione')}"
										disabled="%{#disabledConsulta}"
										headerKey="" cssClass="txtfield" cssStyle="width:200px;"
										onchange="onchangeConductos();" />
								</div>

							</div>
						</div>
						<div id="checkDatos" style="display:none">
							<s:select id="titularIgual" name="datosIguales"
								list="mapDatosIguales" headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								label="�El titular es la misma persona que el cliente de la p�liza?"
								labelposition="left"
								disabled="%{#disabledConsulta}"
								cssClass="txtfield jQrequired" cssStyle="width:100px;"
								onchange="onchangeTitularIgual();" />
						</div>
						<div id="datosTitular_div" style="display:none"></div>
						<s:if test="soloConsulta != 1">
						<div class="guardarCobranza">
							<div class="btn_back w110">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="guardarCobranza();"> <s:text
										name="midas.boton.guardar" /> </a>
							</div>
						</div>
						<div class="btn_back w200" style="display: inline; float: right;" id="datosTarjeta">
							<a href="javascript: void(0);" id="obtenerDatosTarjeta" onclick="obtenerDatosTarjeta();">
								Buscar/Obtener Tarjetas del Cliente
							</a>
						</div>
						</s:if>
					</div>
				</div>
				<div id="accordion2" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Datos titular tarjeta/cuenta</a>
					</h3>
					<div id="second" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">

					</div>
					<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all">
						<a href="#">Datos tarjeta / cuenta </a>
					</h3>
					<div id="third" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" style="height:280px;">
						<div id="infoCuentasCobranza">
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div id="rightcolumn">
				
			</div>
		</s:form>
	</div>
</div>
