<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
<!--
.input_text {
	max-width: 200px;
	min-width: 200px;
}
-->
</style>
		<table id="agregar">
			<tr>
				<td for="tarjetaHabiente">Nombre</td>
				<td><s:textfield name="cuentaPagoDTO.tarjetaHabiente"
						cssClass="txtfield input_text jQrequired" id="tarjetaHabiente"/>
				</td>
			</tr>
			<tr>
				<td for="rfc">RFC</td>
				<td><s:textfield id="rfc" name="cuentaPagoDTO.rfc" cssClass="txtfield input_text jQrequired" maxLength="13"/>
				</td>
			</tr>
			<tr>
				<td for="codigoPostal">Codigo Postal</td>
				<td><s:textfield id="codigoPostal" name="cuentaPagoDTO.domicilio.codigoPostal"
					onkeypress="return soloNumeros(this, event, false)"
					cssClass="txtfield jQrequired" maxlength="5"
					onblur="onchangeCp();" />
				</td>
			</tr>
			<tr>
				<td>Pa&iacute;s</td>
				<td>
<%-- 				<s:select id="idPais" list="paisesMap" --%>
<!-- 				name="cuentaPagoDTO.domicilio.clavePais" value="PAMEXI" -->
<!-- 						headerValue="%{getText('midas.general.seleccione')}" headerKey="" -->
<!-- 						onchange="onchangePaisCobranza();" cssClass="txtfield input_text jQrequired" disabled="true"/> -->
				<s:textfield value="M�XICO" cssStyle="txtfield" disabled="true"></s:textfield>
				<s:hidden name="cuentaPagoDTO.domicilio.clavePais" value="PAMEXI"/>
				</td>
				<td for="idEstadoCobranza">Estado</td>
				<td><s:select id="idEstadoCobranza" list="estadoMap"
						name="cuentaPagoDTO.domicilio.claveEstado" onchange="onchangeEstadoCobranza();"
						headerValue="%{getText('midas.general.seleccione')}" headerKey=""
						cssClass="txtfield input_text jQrequired"></s:select></td>
			</tr>
			<tr>
				<td for="idMunicipioCobranza">Municipio</td>
				<td><s:select id="idMunicipioCobranza" list="municipioMap"
						name="cuentaPagoDTO.domicilio.claveCiudad" onchange="onchangeMunicipioCobranza();"
						headerValue="%{getText('midas.general.seleccione')}" headerKey=""
						cssStyle="width:200px;max-width: 200px"
						cssClass="txtfield input_text jQrequired" /></td>
				<td for="idColoniaCobranza">Colonia</td>
				<td><s:select id="idColoniaCobranza" list="coloniasMap"
						name="cuentaPagoDTO.domicilio.idColonia" onchange="obtenerNombreColonia();"
						headerValue="%{getText('midas.general.seleccione')}" headerKey=""
						cssStyle="width:200px;max-width: 200px"
						cssClass="txtfield input_text jQrequired" />
						<script type="text/javascript">
							function obtenerNombreColonia() {
							jQuery("#nombreColonia").val(jQuery('#idColoniaCobranza option:selected')
										.text());
							}
						</script></td>
				<s:hidden name="cuentaPagoDTO.domicilio.nombreColonia" id="nombreColonia"></s:hidden>
			</tr>
			<tr>
				<td for="calleNumero">Calle y n�mero</td>
				<td><s:textfield id="calleNumero" name="cuentaPagoDTO.domicilio.calleNumero" cssClass="txtfield input_text jQrequired" />
				</td>
			</tr>
			<tr>
				<td for="telefono">Tel&eacute;fono</td>
		<td><s:textfield id="telefono"
				onkeypress="return soloNumeros(this, event, false)"
				name="cuentaPagoDTO.telefono"
				cssClass="txtfield input_text jQrequired jQnumeric" /></td>
		<td for="correo">Correo electr�nico</td>
				<td><s:textfield id="correo" name="cuentaPagoDTO.correo" cssClass="txtfield input_text jQemail" />
				</td>
			</tr>
		</table>
