<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cobranza/cobranza.js'/>" /></script>
<script type="text/javascript">
<!--
	// Cobranza vars
	//-->
	var guardarCobranzaUrl = '<s:url action="guardarCobranza" namespace="/suscripcion/cotizacion/auto/cabranza"/>';
	var idToPersonaContratante = '<s:property value="cotizacionDTO.idToPersonaContratante"/>';
	var datosIguales = '<s:property value="datosIguales"/>';
	var idConductoCobro = '<s:property value="idConductoCobro"/>';
</script>
<style>
<!-- /* Cobranza Style */
-->
#wrapper {
	margin: 0 auto;
	width: 922px;
}

#faux {
	margin: 10px 0px;
	overflow: auto;
	width: 100%
}

#rightcolumn {
	display: inline;
	color: #333;
	margin: 5px;
	padding: 0px;
	float: right;
}

#leftcolumn {
	float: left;
	color: #333;
	margin: 5px;
	padding: 0px;
	width: 750px;
	display: inline;
	position: relative;
}

.clear {
	clear: both;
	background: none;
}

#checkDatos {
	display: block; 
}

#conductos_div {
	display: block;
}

#first {
	height: 160px;
	overflow: hidden;
}

#second {
	height: 280px;
	overflow: hidden;
}

#third {
	height: 150px;
	overflow: hidden;
}

.guardarCobranza {
	float: right;
}
</style>