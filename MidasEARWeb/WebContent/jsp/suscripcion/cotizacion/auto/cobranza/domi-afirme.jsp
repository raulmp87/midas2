<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
<!--
.input_text {
	max-width: 200px;
	min-width: 200px;
}
-->
</style>
	<table id="agregar">
		<tr>
			<td for="idBancoCobranza">Instituci&oacute;n Bancaria</td>
			<td>
			<s:if test="cuentaPagoDTO.idBanco == 51">
			<s:hidden name="cuentaPagoDTO.idBanco"/>
			<span style="margin-left: 5px;">AFIRME</span>
			</s:if>
			<s:else>
				<s:select list="bancos" name="cuentaPagoDTO.idBanco"
					id="idBancoCobranza" headerKey="" headerValue="Seleccione"
					listKey="idBanco" cssClass="txtfield input_text jQrequired"
					listValue="nombreBanco"></s:select>
			</s:else>
			</td>
		</tr>
		<tr>
			<s:if test="idMedioPago == 8">
				<td for="numeroTarjetaCobranza">N&uacute;mero de CLABE</td>
			</s:if>
			<s:else>
				<s:if test="idMedioPago == 1386">
					<td for="numeroTarjetaCobranza">N&uacute;mero de Cuenta</td>
				</s:if><s:else>
					<td for="numeroTarjetaCobranza">N&uacute;mero de Tarjeta</td>
				</s:else>
		</s:else>
			<td>
				<s:if test="idMedioPago == 1386"><!-- N&uacute;mero de Cuenta -->
					<s:textfield id="numeroTarjetaCobranza"
						name="cuentaPagoDTO.cuenta" cssClass="txtfield input_text jQrequired"
						onkeypress="return soloNumeros(this, event, false)" maxlength="11" />
				
				</s:if><s:else><!-- N&uacute;mero de Tarjeta -->
					<s:textfield id="numeroTarjetaCobranza"
						name="cuentaPagoDTO.cuenta" cssClass="txtfield input_text jQrequired"
						onkeypress="return soloNumeros(this, event, false)" maxlength="18" />
				</s:else>
			</td>
		</tr>
	</table>