<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/xml" %>
<fieldset>	
	<div class="form-group col-sm-3" style="font-size:10px;" id="idPaqueteDiv">
		<label class="small"><s:text name="midas.excepcion.suscripcion.auto.paquete" />:</label>
		<s:select				
		    labelposition="top"
			id="idPaquete" name="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
			list="paqueteMap" onchange="habilitarActualizarDerecho();mostrarPorcentajeDescuentoEstadoDefault();"
			cssClass="form-control-datos-generales form-control  mandatory"/>
	</div>
	<div class="form-group col-sm-3" style="font-size:10px;" id="idFormaPagoDiv">
		<label class="small"><s:text name="midas.cotizacion.formapago" />:</label>
		<s:select				
		    labelposition="top"
			id="idFormaPago" name="cotizacion.idFormaPago"
			list="formasdePagoList" cssClass="form-control-datos-generales form-control mandatory"
			onchange="recalcularCotizador(true);"/>
	</div>
	<div  class="form-group col-sm-3" id="divPagoFraccionado" style="font-size:10px;" >
		<div class="has-feedback">
			<label class="small"><s:text name="midas.cotizacion.porcentajePagoFraccionado" />: </label>
			<s:textfield
			id="porcentajePagoFraccionado" name="cotizacion.porcentajePagoFraccionado" cssClass="form-control-datos-generales form-control"/>
		</div>
	</div>
	<div class="form-group col-sm-3" style="font-size:10px;" 	id="idDerechoDiv">
		<label class="small"><s:text name="midas.cotizacion.derechos" />:</label>
		<div class="input-group">
			<span class="input-group-addon">$</span> 
			<s:select				
			    labelposition="top"
				id="idDerecho" name="cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza"
				list="derechoMap" cssStyle="border-radius: 0px 3px 3px 0px;"
				onchange="recalcularCotizador(true)"
				cssClass="form-control-datos-generales form-control"/>
		</div>
	</div>
	<div class="form-group col-sm-3" style="font-size:10px;" id="porcentajeIvaDiv">
		<label class="small"><s:text name="midas.cotizacion.iva" />:</label>
		<div class="input-group">			
			<s:select name="cotizacion.porcentajeIva" id="porcentajeIva"
		      cssClass="form-control-datos-generales form-control form-control-datos-generales-override-size " cssStyle="border-radius: 3px 0px 0px 3px;"
		      list="resumenCostosDTO.ivaList"
		      value="cotizacion.porcentajeIva"
		      labelposition="%{getText('label.position')}" />
			<span class="input-group-addon">%</span>
		</div>
	</div>	
	<s:if test="configuracionId == null" >
	<div class="form-group col-sm-3" style="font-size:10px;" id="comisionDiv">
		<label class="small"><s:text name="midas.cotizacion.comisioncedida" />:</label>
		<div class="input-group">
			<s:textfield 
 				 id="comision" name="cotizacion.porcentajebonifcomision"
				cssClass="form-control-datos-generales form-control" cssStyle="border-radius: 3px 0px 0px 3px;" 
				onchange="validaComisionCedidaAgen(this.value,100,0,'');recalcularCotizador(true)"/> 
			<span class="input-group-addon">%</span>
		</div>
	</div>
	</s:if>	
	<div class="form-group col-sm-3" style="font-size:10px;" id="porcentajeDescuentoEstadoDiv">
	    <s:if test="cotizacion.tipoCotizacion=='TCSP'">
			<s:hidden name='incisoCotizacion.incisoAutoCot.pctDescuentoEstado'/>
	    </s:if>
	    <s:else>
			<div id="porcentajeDescuentoEstado" style="overflow: hidden; display: none;">
				<label class="small"><s:text name="midas.general.pctDescuentoEstado" />:</label>
				<div class="input-group">
					<s:if test="configuracionId == null" >
					<s:textfield 
		 				id="pctDescuentoEstado" name="incisoCotizacion.incisoAutoCot.pctDescuentoEstado"
						cssClass="form-control-datos-generales form-control form-control-datos-generales-override-size mandatory" cssStyle="border-radius: 3px 0px 0px 3px;"
						maxlength="5"
						onkeypress="return soloNumerosM2(this,event,true, true)"
						onchange="validaPorcentajeDescuentoEstado();" />
					</s:if>
					<s:else>
					<s:textfield 
		 				id="pctDescuentoEstado" name="incisoCotizacion.incisoAutoCot.pctDescuentoEstado"
						cssClass="form-control-datos-generales form-control mandatory" cssStyle="border-radius: 3px 0px 0px 3px;"
						maxlength="5"
						onkeypress="return soloNumerosM2(this,event,true, true)"
						onchange="validaPorcentajeDescuentoEstadoLigaAgente();" />
					</s:else>
					<span class="input-group-addon">%</span>
				</div>
			</div>
		</s:else>
	</div>
	<div class="form-group col-sm-3" style="font-size:10px;" id="btn_datosAdicionalesPaqDiv">
			<a href="#divDatosAdicionalesPaquete" class="btn btn-default" style="display:none" id="btn_datosAdicionalesPaq">
				<s:text name="midas.suscripcion.cotizacion.agentes.datosAdicionalesPaquete" />
			</a>
	</div>
</fieldset>