<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp"/>
<meta http-equiv="refresh" content="2;url=<s:url includeParams="all" />"/>
<div class="row" style="width: 600px; height:390px;">
	<div class="col-md-12">
		<div class="well">
			<fieldset>
				<legend></legend>
				<div class="alert alert-info" style="margin-bottom:0"><strong><s:text name="midas.suscripcion.cotizacion.agentes.msnWaitImpresion" /></strong></div>
			</fieldset>
		</div>
	</div>
</div>