<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<div id="altaCliente">
	<s:form id="addClienteAgente">
	<s:hidden name="cliente.idCliente" id="idClienteVista" />
	<s:hidden name="cliente.idClienteUnico" id="cliente.idClienteUnico" />
		<s:hidden name="cliente.verificarRepetidos"
			id="cliente.verificarRepetidos" value="true" />
		<s:hidden name="tipoRegreso" id="tipoRegreso" value="5" />
	<s:hidden name="cliente.tipoSituacionString" value="A" />
			<s:if test="mensaje!=null && mensaje!=''">
			<div id="mensajeExito"
				class="alert <s:if test="tipoMensaje=='30'"> alert-success</s:if><s:else>alert-danger</s:else>">
					<span class="pull-right" onclick="$('#mensajeExito').hide('slow')">x</span>
				<p id="mensajeExitoText">
					<s:property value="mensaje" />
				</p>
				</div>
			</s:if>
		<div id="msnInfo" class="alert alert alert-info" style="display: none">
				<span class="pull-right" onclick="$('#msnInfo').hide('slow')">x</span>
				<p  id="msnInfoText"></p>
			</div>
	<div class="row">
			<div class="col-md-12" style="font-size: 16px;">
				<h3 class="small">
					<s:if test="cliente.idCliente != null">
						<s:text name="midas.suscripcion.cotizacion.agentes.editarCliente" />
					</s:if>
					<s:else>
					<s:text name="midas.suscripcion.cotizacion.agentes.agregarCliente" />
					</s:else>
					
				</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="well">
						<fieldset>
								<legend style="font-size: 12px;">
									<s:text
										name="midas.suscripcion.cotizacion.agentes.datosGenerales" />
								</legend>
								<div class="form-group col-sm-12" style="font-size: 12px;">
									<div id="acTipoPersonaRB">
									<label class="small"><s:text
											name="midas.fuerzaventa.tipoPersona" /> </label>
								<div class="radio">
										<s:radio name="cliente.claveTipoPersona" id="claveTipoPersona"
										list="#{'1':'F\u00EDsica','2':'Moral'}" value="1" />
								</div>
								</div>
								<div id="addDivPersonaFisica">
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.negocio.nombres" />:</label>
											<s:textfield id="cliente.nombre" name="cliente.nombre"
												cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.suscripcion.cotizacion.agentes.apellidoPaterno" />:</label>
											<s:textfield id="cliente.apellidoPaterno"
												name="cliente.apellidoPaterno"
												cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.suscripcion.cotizacion.agentes.apellidoMaterno" />:</label>
											<s:textfield id="cliente.apellidoMaterno"
												name="cliente.apellidoMaterno"
												cssClass="form-control mandatory" />
										</div>
										<div class="col-sm-12" style="padding: 0px;">
											<div class="form-group col-sm-4 has-feedback" style="font-size: 10px;">
												<label class="small"><s:text
														name="midas.fuerzaventa.negocio.fechaNacimiento" />:</label>
												
												<s:textfield id="cliente.fechaNacimiento"
													name="cliente.fechaNacimiento"
													cssClass="form-control  datepicker mandatory" />
												<i class="glyphicon glyphicon-calendar form-control-feedback-prueba"></i>
											</div>
											<div class="form-group col-sm-4" style="font-size: 10px;">
												<label class="small"><s:text
														name="midas.fuerzaventa.sexo" />:</label>
												<s:select id="cliente.sexo" name="cliente.sexo"
													list="#{'F':'Femenino', 'M':'Masculino'}"
													headerValue="Seleccione..." headerKey=""
													labelposition="left" cssClass="form-control mandatory" />
											</div>
											
											<div class="form-group col-sm-4" style="font-size: 10px;">
												<label class="small"><s:text name="midas.clientes.pais.nacimiento"/></label>
												<s:select id="cliente.idPaisNacimiento"
													name="cliente.idPaisNacimiento" list="paises"
													listValue="countryName" listKey="countryId"
													headerValue="Seleccione..." headerKey=""
													labelposition="left" cssClass="form-control mandatory" />
											</div>
										</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small">Nacionalidad:</label>
											<s:select id="cliente.claveNacionalidad"
												name="cliente.claveNacionalidad" list="paises"
												listValue="countryName" listKey="countryId"
												headerValue="Seleccione..." headerKey=""
												onchange="onChangePais('cliente.claveEstadoNacimiento','cliente.claveCiudadNacimiento','idColonia','cp','calleNumero','cliente.claveNacionalidad')"
												labelposition="left" cssClass="form-control mandatory" />
										</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.suscripcion.cotizacion.agentes.estadoNacimiento" />:</label>
											<s:select id="cliente.claveEstadoNacimiento"
												name="cliente.claveEstadoNacimiento"
												list="estadosNacimiento" headerValue="Seleccione..."
												headerKey="" listKey="key" listValue="stateName"
												onchange="onChangeEstadoGeneral('cliente.claveCiudadNacimiento',null,null,null,'cliente.claveEstadoNacimiento')"
											labelposition="left" cssClass="form-control mandatory " />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text name="midas.clientes.ciudad.nacimiento"/></label>
											<s:select id="cliente.claveCiudadNacimiento"
												name="cliente.claveCiudadNacimiento"
												list="ciudadesNac" headerValue="Seleccione..."
												headerKey="" listKey="cityId" listValue="cityName"
												
												labelposition="left" cssClass="form-control mandatory " />
										</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small" for="estadoCivil"><s:text
													name="midas.fuerzaventa.estadoCivil" />:</label>
											<s:select id="cliente.estadoCivil" name="cliente.estadoCivil"
												list="estadosCiviles" listKey="idEstadoCivil"
												listValue="nombreEstadoCivil" labelposition="left"
												cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text name="midas.clientes.giro.cnsf"/></label>
										<s:select name="cliente.ocupacionCNSF"
											id="cliente.ocupacionCNSF" disabled="%{#readOnly}"
											list="ocupacionesCNSF" listValue="actividad" listKey="id"
											headerValue="Seleccione..." headerKey=""
											labelposition="left" cssClass="form-control mandatory" />
								</div>
									</div>
								<div style="display: none;" id="addDivPersonaMoral">
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.fuerzaventa.negocio.razonSocial" />:</label>
											<s:textfield id="cliente.razonSocial"
												name="cliente.razonSocial" cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4 has-feedback"
											style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.suscripcion.cotizacion.agentes.fechaConstitucion" />:</label>
											<s:textfield id="cliente.fechaConstitucion"
												name="cliente.fechaConstitucion"
												cssClass="form-control  datepicker mandatory"
												onfocus="$(this).datepicker('update');" />
											<i
												class="glyphicon glyphicon-calendar form-control-feedback-prueba"></i>
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
										<div>
												<label class="small"><s:text
														name="midas.fuerzaventa.representanteLegal" /> </label> <span
													class="pull-right"> <a
													onclick="mostrarModalResponsable()"> <s:text
															name="midas.boton.buscar" /> <s:text name="midas.clientes.representante"/> </a> </span>
										</div>
											<s:hidden name="cliente.idRepresentante"
												id="idRepresentanteFiscal" cssStyle="width:100px" />
											<s:textfield id="nombreRepresentanteFiscal"
												cssClass="form-control mandatory" readonly="true" />
									</div>

										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text name="midas.clientes.pais.constitucion"/></label>
											<s:select id="cliente.idPaisConstitucion"
												name="cliente.idPaisConstitucion" list="paises"
												listValue="countryName" listKey="countryId"
												headerValue="Seleccione..." headerKey=""
												labelposition="left" cssClass="form-control mandatory" />
										</div>
										
										</div>




									<div id="datosGenericosADDCLIENTE">
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small">Rfc: </label> <span class="pull-right">
											<a href="javascript: void(0);" onclick="generarRFCAgente()">Generar
												Rfc</a> </span>
										<s:textfield id="cliente.codigoRFC" name="cliente.codigoRFC"
											cssClass="form-control mandatory" maxlength="13" />
									<br>
								</div>
									<div id="curpDIV">
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small">Curp: </label> <span class="pull-right">
												<a href="javascript: void(0);"
												onclick="generarCURP2('cliente\\.nombre','cliente\\.apellidoPaterno','cliente\\.apellidoMaterno','cliente\\.fechaNacimiento', 'cliente\\.sexo', 'cliente\\.claveNacionalidad','cliente\\.claveEstadoNacimiento');">Generar
													Curp</a> </span>
											<s:textfield id="curpcliente" name="cliente.codigoCURP"
												cssClass="form-control mandatory" maxlength="20" />
									<br>
								</div>

							</div>
		
									<div id="giroDIV">
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small">Giro: </label> 
													<s:select name="cliente.idGiro"
											id="cliente.idGiro" disabled="%{#readOnly}"
											list="giros" listValue="nombreGiro" listKey="idGiro"
											labelposition="left" cssClass="form-control mandatory" />
							</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small">Ocupación, profesión, actividad o
											giro CNSF:</label>
										<s:select name="cliente.cveGiroCNSF"
											id="cliente.cveGiroCNSF" disabled="%{#readOnly}"
											list="ocupacionesCNSF" listValue="actividad" listKey="id"
											headerValue="Seleccione..." headerKey=""
											labelposition="left" cssClass="form-control mandatory" />
									</div>
									</div>
									

									<div class="form-group col-sm-4" style="font-size: 10px; display:none;">
										<label class="small">PLD/FT (Grado de riesgo del
											cliente):</label>
										<s:textfield id="cliente.razonSocialFiscal"
											name="cliente.gradoRiesgo" cssClass="form-control "
											disabled="true" />
									</div>
									</div>
									
								</div>



								<legend style="font-size: 12px;">Domicilio</legend>
							
							<div id="divDomicilio">
									
										<div class="btn_back w50 elementoInline" id="bttnDomAceptar">
						<a href="javascript: void(0)" onclick='javascript: agregarDomicilio();'>
							<s:text name="Agregar"/>
						</a>
					</div>				
					<div class="btn_back w50 elementoInline" id="bttnDomCancelar" style= "display:none">
						<a href="javascript: void(0)" onclick='javascript: cancelarDomicilio();'>
							<s:text name="Cancelar"/>
						</a>
					</div>	
									<s:hidden name="cliente.domicilioTemp.cvePais" id="cliente.domicilioTemp.cvePais"/>
			<s:hidden name="cliente.domicilioTemp.cveEstado" id="cliente.domicilioTemp.cveEstado"/>
			<s:hidden name="cliente.domicilioTemp.cveCiudad" id="cliente.domicilioTemp.cveCiudad"/>
			<s:hidden name="cliente.domicilioTemp.calle" id="cliente.domicilioTemp.calle"/>
			<s:hidden name="cliente.domicilioTemp.numero" id="cliente.domicilioTemp.numero"/>
			<s:hidden name="cliente.domicilioTemp.codigoPostal" id="cliente.domicilioTemp.codigoPostal"/>
			<s:hidden name="cliente.domicilioTemp.colonia" id="cliente.domicilioTemp.colonia"/>
									
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text name="Calle" />:</label>
										<s:textfield id="cliente.nombreCalle"
											name="cliente.nombreCalle" cssClass="form-control mandatory" />
									</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text name="Número" />:</label>
										<s:textfield id="cliente.numeroDom" name="cliente.numeroDom"
										cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.codigoPostal" />:</label>
										<s:textfield id="cliente.codigoPostal"
											name="cliente.codigoPostal"
										onchange="onChangeCodigoPostalAgentes(this.value, 'GENERALES')"
										cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.colonia" />:</label>
										<s:select id="cliente.nombreColonia"
											name="cliente.nombreColonia" 
											list="coloniasDir"
											listValue="colonyName" listKey="colonyName"
											onchange="onChangeColonia('cliente.codigoPostal',null, 'cliente.nombreColonia','cliente.idMunicipioDirPrin')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.pais" />:</label>
										<s:select id="cliente.idPaisString"
											name="cliente.idPaisString" list="paises"
											listValue="countryName" listKey="countryId"
											onchange="onChangePais('cliente.idEstadoDirPrin','cliente.idMunicipioDirPrin','cliente.nombreColonia',null,'cliente.nombreCalle','cliente.idPaisString')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.estado" />:</label>
										<s:select id="cliente.idEstadoDirPrin"
											name="cliente.idEstadoDirPrin" list="estadosDir" listKey="key"
											listValue="stateName"
											onchange="onChangeEstadoGeneral('cliente.idMunicipioDirPrin',null,null,null,'cliente.idEstadoDirPrin')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.municipio" />:</label>
										<s:select id="cliente.idMunicipioDirPrin"
											name="cliente.idMunicipioDirPrin" 
											list="ciudadesDir"  listKey="cityId"
											listValue="cityName"
											onchange="onChangeCiudad('cliente.nombreColonia',null,null,'cliente.idMunicipioDirPrin')"
										labelposition="left" cssClass="form-control mandatory" />
	 							</div>

							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-12">
					<div class="well">
						<fieldset>
								<legend style="font-size: 12px;">
									<s:text name="midas.fuerzaventa.negocio.datosFiscales" />
								<span class="pull-right">
										<button id="btnCopiarDatosGenerales" type="button"
											class="btn btn-default">
											<s:text
												name="midas.suscripcion.cotizacion.agentes.copiarDatosGenerales" />
										<span class="glyphicon glyphicon-new-window"></span>
										</button> </span>
							</legend>
								<div class="form-group col-sm-12" style="font-size: 10px;">
								<div id="addDivPersonaFisicaFiscal">
										<div class="form-group col-sm-4" style="font-size: 12px;">
											<label class="small"><s:text
													name="midas.fuerzaventa.negocio.nombre" />(s):</label>
											<s:textfield id="cliente.nombreFiscal"
												name="cliente.nombreFiscal"
											cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.fuerzaventa.negocio.apPaterno" />:</label>
											<s:textfield id="cliente.apellidoPaternoFiscal"
												name="cliente.apellidoPaternoFiscal"
											cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.fuerzaventa.negocio.apMaterno" />:</label>
											<s:textfield id="cliente.apellidoMaternoFiscal"
												name="cliente.apellidoMaternoFiscal"
											cssClass="form-control mandatory" />
											</br>
									</div>
										
										
									
									
								</div>
								<div style="display: none;" id="addDivPersonaMoralFiscal">
										<div class="form-group col-sm-4" style="font-size: 10px;">
											<label class="small"><s:text
													name="midas.fuerzaventa.negocio.razonSocial" />:</label>
											<s:textfield id="cliente.razonSocialFiscal"
												name="cliente.razonSocialFiscal"
											cssClass="form-control mandatory" />
									</div>
										<div class="form-group col-sm-4 has-feedback"
											style="font-size: 10px;">
											<label><s:text
													name="midas.suscripcion.cotizacion.agentes.fechaConstitucion" />:</label>
											<s:textfield id="cliente.fechaNacimientoFiscal"
												name="cliente.fechaNacimientoFiscal"
											cssClass="form-control  datepicker mandatory" />
											<i
												class="glyphicon glyphicon-calendar form-control-feedback-prueba"></i>

									</div>
										
									
								</div>
							</div>
								<legend style="font-size: 12px;">
									<s:text
										name="midas.suscripcion.cotizacion.agentes.domicilioFiscal" />
								</legend>
							<div id="divDomicilioFiscal">
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text name="Calle" />:</label>
										<s:textfield id="cliente.nombreCalleFiscal"
											name="cliente.nombreCalleFiscal"
										cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text name="Número" />:</label>
										<s:textfield id="cliente.numeroDomFiscal"
											name="cliente.numeroDomFiscal"
											cssClass="form-control mandatory" />
									</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.codigoPostal" />:</label>
										<s:textfield id="cliente.codigoPostalFiscal"
											name="cliente.codigoPostalFiscal"
										onchange="onChangeCodigoPostalAgentes(this.value, 'FISCALES')"
										cssClass="form-control mandatory mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.colonia" />:</label>
										<s:select id="cliente.nombreColoniaFiscal"
											name="cliente.nombreColoniaFiscal" list="coloniasFiscal" listValue="colonyName" listKey="colonyName"
										headerValue="Seleccione..." headerKey=""
										onchange="onChangeColonia('cliente.codigoPostalFiscal',null, 'cliente.nombreColoniaFiscal','cliente.idMunicipioFiscal')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.pais" />:</label>
										<s:select id="cliente.idPaisFiscal"
											name="cliente.idPaisFiscal" list="paises"
											headerValue="Seleccione..." headerKey=""
											listValue="countryName" listKey="countryId"
											onchange="onChangePais('cliente.idEstadoFiscal','cliente.idMunicipioFiscal','cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal',null,'cliente.idPaisFiscal')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.estado" />:</label>
										<s:select id="cliente.idEstadoFiscal"
											name="cliente.idEstadoFiscal" list="estadosDir" listKey="key"
											listValue="stateName" headerValue="Seleccione..."
											headerKey="" labelposition="left"
											cssClass="form-control mandatory" />
								</div>
									<div class="form-group col-sm-4" style="font-size: 10px;">
										<label class="small"><s:text
												name="midas.fuerzaventa.negocio.municipio" />:</label>
										<s:select id="cliente.idMunicipioFiscal"
											name="cliente.idMunicipioFiscal" 
											list="ciudadesFiscal" listKey="cityId"
											listValue="cityName"
											headerValue="Seleccione..." headerKey=""
										onchange="onChangeCiudad('cliente.nombreColoniaFiscal','cliente.codigoPostalFiscal',null,'cliente.idMunicipioFiscal')"
										labelposition="left" cssClass="form-control mandatory" />
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="col-md-12">
					<div class="well">
						<fieldset>
								<legend style="font-size: 12px;">
									<s:text name="midas.fuerzaventa.datosContacto" />
								</legend>
								<div class="form-group col-sm-4" style="font-size: 10px;">
									<label class="small"><s:text
											name="midas.fuerzaventa.telefonoCasa" />:</label>
									<s:textfield id="cliente.numeroTelefono"
										name="cliente.numeroTelefono" cssClass="form-control"
										maxlength="8" />
							</div>
								<div class="form-group col-sm-4" style="font-size: 10px;">
									<label class="small"><s:text
											name="midas.fuerzaventa.negocio.telefonoOficina" />:</label>
									<s:textfield id="cliente.telefonoOficinaContacto"
										name="cliente.telefonoOficinaContacto" cssClass="form-control"
										maxlength="8" />
							</div>
								<div class="form-group col-sm-4" style="font-size: 10px;">
									<label class="small"><s:text
											name="midas.fuerzaventa.twitter" />:</label>
									<s:textfield id="cliente.twitterContacto"
										name="cliente.twitterContacto" cssClass="form-control" />
							</div>
								<div class="form-group col-sm-4" style="font-size: 10px;">
									<label class="small"><s:text
											name="midas.fuerzaventa.facebook" />:</label>
									<s:textfield id="cliente.facebookContacto"
										name="cliente.facebookContacto" cssClass="form-control" />
							</div>

								<div class="form-group col-sm-4" style="font-size: 10px;">
									<label class="small"><s:text
											name="midas.negocio.agente.correo" /> <s:if
											test="correoObligatorio">
										*
									</s:if> : </label>
									<s:textfield id="correoElectronicoAviso"
										name="cliente.email" cssClass="form-control" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div align="right">
					<button id="addClient" type="button" class="btn btn-success"
						onclick="guardarClienteAjustador();">
						<s:text name="midas.boton.agregar" />
						<span class="glyphicon glyphicon-ok"></span>
				</button>
			</div>
		</div>
	</div>
	</s:form>
</div>
<div id="searchPersonaDiv" style="display: none"></div>

<script type="text/javascript">
	//Inicializar consulta prima en nulo.
	
	$(document).ready(function(){
	
		resultadoWSPrima = null;
		validacionesPrimaMayor();
		
		$('#wwctrl_claveTipoPersona input').click(function(){
			validacionesPrimaMayor();
	    });
		
	});
</script>
