<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:form id="serachPersona%{idElemento}">
	<s:hidden name="tipoAccion" value="consulta" />
	<div class="row">
		<div class="col-md-6">
			<div class="well">
				<fieldset>
					<legend><s:text name="midas.fuerzaventa.persona.tituloConsultar" /></legend>
					<div class="alert alert-info"><strong>Sugerencia:</strong><s:text name="midas.suscripcion.cotizacion.agentes.mnsBusqCliente" /></div>
					<div class="form-group">
						<label><s:text name="midas.fuerzaventa.tipoPersona" /></label>
						<div class="radio">
							<s:radio name="personaSeycos.claveTipoPersona" id="claveTipoPerson%{idElemento}" list="#{'1':'Física','2':'Moral'}" value="1" />
						</div>
					</div>
					<div class="form-group">
						<label id="busRazonSocialLabel"><s:text name="midas.fuerzaventa.negocio.nombrePerFisica" />:</label>
						<s:textfield name="personaSeycos.nombre" cssClass="form-control" id="busRazonSocial%{idElemento}"/>
					</div>
					<div class="form-group">
						<label><s:text name="midas.emision.consulta.cliente.rfc" />:</label>
						<s:textfield name="personaSeycos.codigoRFC" cssClass="form-control" id="busRfc%{idElemento}"/>
					</div>
					<div class="form-group">
						<label><s:text name="midas.fuerzaventa.negocio.telefonoOficina" />:</label>
						<s:textfield name="personaSeycos.telOficina" cssClass="form-control" id="busRazonSocial%{idElemento}"/>
					</div>
					<div class="form-group has-feedback">
						<label><s:text name="midas.fuerzaventa.fechaAlta" />:</label> 
						<s:textfield 
							id="personaSeycos.fechaAlta%{idElemento}" name="personaSeycos.fechaAlta"
							cssClass="form-control datepicker mandatory"/>
						<i class="glyphicon glyphicon-calendar form-control-feedback"></i>
					</div>
					<div class="form-group has-feedback">
						<label><s:text name="midas.fuerzaventa.fechaBaja" />:</label> 
						<s:textfield 
							id="personaSeycos.fechaBaja%{idElemento}" name="personaSeycos.fechaBaja"
							cssClass="form-control  datepicker mandatory" />
						<i class="glyphicon glyphicon-calendar form-control-feedback"></i>
					</div>
					<button type="button" id="btn_filterSearchPerson" class="btn btn-success" > 
						<s:text name="midas.boton.buscar" /><span class="glyphicon glyphicon-search"></span>
					</button>
				</fieldset>
			</div>
		</div>
		<div id="divLista${idElemento}" style="display:none">
			<div class="col-md-6">
				<div class="well">
					<div class="alert alert-info"><s:text name="Para seleccionar el cliente, de doble click sobre el renglon" /></div>
					<div id="divCarga${idElemento}" style="left: 214px; top: 40px; z-index: 1; display: none;"></div>
					<div style="width:520px;overflow:auto">
						<div id="PersonaGrid${idElemento}" style="width:918px;height:220px;background-color:white;overflow:hidden" class="table"></div>
						<div id="pagingArea"></div><div id="infoArea"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</s:form>

<script type="text/javascript">

 $("input[type=radio][name='personaSeycos.claveTipoPersona']").change(function(){
	if(this.value == 2){
		$("#busRazonSocialLabel").html('');
		$("#busRazonSocialLabel").html('<s:text name="midas.fuerzaventa.negocio.razonSocial" />');
	}else{
		$("#busRazonSocialLabel").html('');
		$("#busRazonSocialLabel").html('<s:text name="midas.fuerzaventa.negocio.nombrePerFisica" />');
	}
}); 

</script>