<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div class="row">
	<div class="col-md-12">
		<div class="well">
			<table style="width:100%">
				<tr>
					<th>No. Serie</th>
					<th>Sistema</th>
					<th>Alerta</th>
					<th>Acciones</th>
				</tr>
				<s:iterator value="sapAmisRespuestaAlertas" var="sapAmisRespuestaAlertas" status="userStatus">
					<tr id="${userStatus.count}">
						<td><s:property value="vin" escapeHtml="false" escapeXml="true"/></td>
						<td><s:property value="sistema" escapeHtml="false" escapeXml="true"/></td>
						<td><s:property value="alerta" escapeHtml="false" escapeXml="true"/></td>
						<td>
							<table id="tablaAcciones${userStatus.count}">
								<s:iterator value="acciones" var="acciones" status="subStatus">
									<tr id="${userStatus.count}|@|${subStatus.count}|@|<s:property value="#acciones.descAccion" escapeHtml="false" escapeXml="true"/>|@|<s:property value="#acciones.idAccion" escapeHtml="false" escapeXml="true"/>">
										<td><s:property value="#acciones.descAccion" escapeHtml="false" escapeXml="true"/></td>
										<td></td>
									</tr>
								</s:iterator>
								<tr id="nueva">
									<td><input placeholder="Nueva Accion" type="text" id="nuevaAccion<s:property value="idRelacion" escapeHtml="false" escapeXml="true"/>"></td>
									<td><div onclick="agregarAccionNuevoCotizador(<s:property value="idRelacion" escapeHtml="false" escapeXml="true"/>,${userStatus.count})" class="btn btn-success">Agregar</div></td>
								</tr>
							</table>
						</td>
				 	</tr>
				</s:iterator>
			</table>
		</div>
	</div>
</div>