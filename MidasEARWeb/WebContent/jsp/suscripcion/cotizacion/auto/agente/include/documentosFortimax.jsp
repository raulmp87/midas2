<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:form action="" id="clienteDocumentosForm">
	<!-- COTIZADOR DE AUTOS INDIVIDUAL -->
	<fieldset>
		<legend style="font-size:12px;">
			<s:text name="midas.suscripcion.cotizacion.agentes.documentosAnexos"/> 
		</legend>
		<s:if test="forzarArt140">
		<!-- Archivos obligatorios -->
		<s:iterator value="listaDocumentosFortimax" status="stat" var="documentos">	
			<div class="form-group col-sm-4" style="font-size:10px;">
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/>
				<label class="small" id="labelName_<s:property value='%{#stat.index}' />"><s:text name="listaDocumentosFortimax[%{#stat.index}].nombreDocumento" /></label>
				<s:if test="clavePF == 1">
					<!-- Persona Fisica -->
					<s:file id="file"                name="files" cssClass="mandatory"/>
				</s:if>
				<s:if test="clavePM == 1">
					<!-- Persona Moral -->
					<s:file id="file_%{#stat.index}" name="files"  cssClass="mandatory"/>
				</s:if>
				<s:hidden 
					name="listaDocumentosFortimax[%{#stat.index}].nombreDocumento" 
					id="listaDocumentosFortimax.%{#stat.index}.nombreDocumento"/>
				<label class="control-label error-label"><s:text name="midas.suscripcion.cotizacion.agentes.msnArchivoParaContinuar" /></label>
			</div>
		</s:iterator>
		</s:if>
		
		<s:else>
		<!-- Archivos opcionales -->
		<s:iterator value="listaDocumentosFortimax" status="stat" var="documentos">	
			<div class="form-group col-sm-4" style="font-size:10px;">
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/>
				<label class="small" id="labelName_<s:property value='%{#stat.index}' />"><s:text name="listaDocumentosFortimax[%{#stat.index}].nombreDocumento" /></label>
				<s:if test="clavePF == 1">
					<!-- Persona Fisica -->
					<s:file id="file"                name="files"/>
				</s:if>
				<s:if test="clavePM == 1">
					<!-- Persona Moral -->
					<s:file id="file_%{#stat.index}" name="files"/>
				</s:if>
				<s:hidden 
					name="listaDocumentosFortimax[%{#stat.index}].nombreDocumento" 
					id="listaDocumentosFortimax.%{#stat.index}.nombreDocumento"/>
				<label class="control-label error-label"><s:text name="midas.suscripcion.cotizacion.agentes.msnArchivoParaContinuar" /></label>
			</div>
		</s:iterator>
		</s:else>
		
	</fieldset>
</s:form>