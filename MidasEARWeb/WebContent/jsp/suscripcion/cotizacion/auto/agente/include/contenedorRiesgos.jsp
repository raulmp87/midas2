<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<html>
	<head>
	</head>
	<body>
		<s:form name="complementarIncisoForm" id="complementarIncisoForm">
			<s:hidden name ="cotizacionId" value="%{incisoCotizacion.id.idToCotizacion}" />
			<s:hidden name="incisoId" value="%{incisoCotizacion.id.numeroInciso}" />
			<div class="row" >
				<div class="col-md-12">
					<s:if test="mensaje!=null && mensaje!=''">
						<div id="mensajeRiesgo" class="alert <s:if test="tipoMensaje=='30'"> alert-success</s:if><s:else>alert-danger</s:else>">
							<span class="pull-right" onclick="$('#mensajeRiesgo').hide('slow')">x</span>
							<p><s:property value="mensaje"/></p>
						</div>
					</s:if>
					<div id="mensajeRiesgoError" class="alert alert-danger" style="display:none">
						<span class="pull-right" onclick="$('#mensajeRiesgoError').hide('slow')">x</span>
						<p id="mensajeRiesgoErrorText"></p>
					</div>
					<div id="msnRiesgo" class="alert alert-info" style="display:none">
						<span class="pull-right" onclick="$('#msnRiesgo').hide('slow')">x</span>
						<p id="msnRiesgoText"></p>
					</div>
					<div class="well">
						<fieldset>
							<legend style="font-size:16px;"><s:text name="midas.suscripcion.cotizacion.agentes.datosAdicionalesPaquete"/></legend>
							<table style="width:70%; border: 0px">
								<tr>
									<td>
										<div>
											<div id="loading" style="text-align: center;">
												<img src="/MidasWeb/img/loading-green-circles.gif">
												<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
											</div>
											<s:action name="cargaControlDinamicoRiesgo" executeResult="true"
												namespace="/suscripcion/cotizacion/auto/complementar/datosRiesgo" ignoreContextParams="true">
												<s:param name="idToCotizacion"
													value="%{incisoCotizacion.id.idToCotizacion}" />
												<s:param name="numeroInciso"
													value="%{incisoCotizacion.id.numeroInciso}" />
												<s:param name="name" value="'datosRiesgo'" />
											</s:action>
										</div>
									</td>
								</tr>
							</table>
							<br/>
							<br/>
							<div style="float: right;">
								<button id="b_guardarRiesgos" type="button" class="btn btn-success" onclick="validaGuardarRiesgos();">
									<s:text name="midas.boton.guardar" />
								</button>
								<button id="b_CerrarRiesgos" type="button" class="btn btn-default" onclick="$.fancybox.close();" style="display:none">
									<s:text name="midas.boton.cerrar" />
								</button>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</s:form>
	</body>
</html>