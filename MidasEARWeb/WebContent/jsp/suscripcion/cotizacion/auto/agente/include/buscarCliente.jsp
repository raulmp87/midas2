
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:form id="serachCliente">
	<div class="row">
		<div class="col-md-12">
			<div class="well">
				<fieldset>
					<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.busquedaClientes" /></legend>
					<div class="alert alert-info"><strong>Sugerencia:</strong><s:text name="midas.suscripcion.cotizacion.agentes.mnsBusqCliente" /></div>
					<div  class="form-group col-md-12" style="font-size:10px;">
						<div id="divTipoPersonaBuscarCliente">
							<label class="small"><s:text name="midas.fuerzaventa.tipoPersona" /></label>
							<div class="radio">
								<s:radio name="filtroCliente.claveTipoPersona" id="claveTipoPersona" list="#{'1':'Física','2':'Moral'}" value="1" />
							</div>
						</div>
						<div id="divPersonaFisica">
							<div  class="form-group col-md-4" style="font-size:10px;">
								<label class="small"><s:text name="midas.negocio.nombres" />:</label> 
								<s:textfield name="filtroCliente.nombre" cssClass="form-control" id="busNombre"/>
							</div>
							<div  class="form-group col-md-4" style="font-size:10px;">
								<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.apellidoPaterno" />:</label>
								<s:textfield name="filtroCliente.apellidoPaterno" cssClass="form-control" id="busNombreaPaterno"/>
							</div>
							<div class="form-group col-md-4" style="font-size:10px;">
								<label  class="small"><s:text name="midas.suscripcion.cotizacion.agentes.apellidoMaterno" />:</label>
								<s:textfield name="filtroCliente.apellidoMaterno" cssClass="form-control" id="busNombreaMaterno"/>
							</div>
							<div  class="form-group col-md-4" style="font-size:10px;">
								<label class="small"><s:text name="midas.fuerzaventa.curp" />:</label>
								<s:textfield name="filtroCliente.codigoCURP" cssClass="form-control" id="buscurp"/>
							</div>
						</div>
						<div style="display: none;" id="divPersonaMoral">
							<div  class="form-group col-md-4" style="font-size:10px;">
								<label class="small"><s:text name="midas.fuerzaventa.negocio.razonSocial" />:</label>
								<s:textfield name="filtroCliente.razonSocial" cssClass="form-control" id="busRazonSocial"/>
							</div>
							<div class="form-group col-sm-4 has-feedback">
								<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.fechaConstitucion" />:</label>
								<s:textfield name="filtroCliente.fechaConstitucion" cssClass="form-control datepicker" id="busFechaConstitucion"/>
								<i class="glyphicon glyphicon-calendar form-control-feedback-prueba-yes "></i>
							</div>
						</div>
						<div  class="form-group col-md-4" style="font-size:10px;">
							<label class="small"><s:text name="midas.emision.consulta.cliente.rfc" />:</label>
							<s:textfield name="filtroCliente.codigoRFC" cssClass="form-control" id="busRfc"/>
						</div>
						<button type="button" id="btn_filterSearchClient" class="btn btn-success" class="btn pull-right"> 
							<s:text name="midas.boton.buscar" /><span class="glyphicon glyphicon-search"></span>
						</button>
					</div>
				</fieldset>
			</div>
		</div>
		<div id="divLista" style="display:none">
			<div class="col-md-12">
				<div class="well">
					<div class="alert alert-info"><s:text name="Para seleccionar el cliente, de doble click sobre el renglon" /></div>
					<div id="divCarga" style="left: 214px; top: 40px; z-index: 1; display: none;"></div>
					<div id="clienteGrid" style="width:100%;height:220px;background-color:white;overflow:hidden" class="table"></div>
					<div id="pagingArea_Clientes"></div><div id="infoArea_Clientes"></div>
				</div>
			</div>
		</div>
	</div>
</s:form>