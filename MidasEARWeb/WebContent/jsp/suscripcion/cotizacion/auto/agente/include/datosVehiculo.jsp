<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="idTipoUsoBase" id="idTipoUsoBase"/>
<div id="numSerieVin" style="display:none">%{incisoCotizacion.incisoAutoCot.numeroSerie}</div>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none"></div>
<fieldset>
	<s:hidden name="esFlotilla" id="esFlotilla"/>
	<div class="form-group col-sm-4" style="font-size:10px;" id="idNegocioSeccionDiv">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.lineaNegocio" />:</label>
		<s:select				
		    labelposition="top" 
			id="idNegocioSeccion" name="vehiculo.idNegocioSeccion"
			list="listarNegocios"			
      		value="incisoCotizacion.incisoAutoCot.negocioSeccionId"
			cssClass="form-control-datos-generales mandatory"
			onchange="onChangeLineaNegocio('idTipoUso', 'idNegocioSeccion'); validaEstiloVehiculo(); limpiarPorLinea('numeroSerie');"/>
		<label class="control-label error-label">Selecciona la línea de negocio para continuar</label>
	</div>
	<div class="form-group has-feedback col-sm-4" style="font-size:10px;" id="numeroSerieDiv">
		<label class="small"><s:text name="midas.cotizador.fronterizos.serie" />:</label> 
		<s:textfield 
			id="numeroSerie" name="vehiculo.numeroSerie"
			cssClass="form-control-datos-generales"
			value="%{incisoCotizacion.incisoAutoCot.numeroSerie}"/>
			<s:hidden id="vinValidoId" name="vehiculo.vinValido" value="%{incisoCotizacion.incisoAutoCot.vinValido}"/>
			<s:hidden id="coincideEstiloId" name="vehiculo.coincideEstilo" value="%{incisoCotizacion.incisoAutoCot.coincideEstilo}"/>	
			<s:hidden id="observacionesSesa" name="vehiculo.observacionesSesa" value="%{incisoCotizacion.incisoAutoCot.observacionesSesa}"/>	
			<s:hidden id="claveAmis" name="vehiculo.claveAmis" value="%{incisoCotizacion.incisoAutoCot.claveAmis}"/>
			<s:hidden id="claveSesa" name="vehiculo.claveSesa" value="%{incisoCotizacion.incisoAutoCot.claveSesa}"/>
			<i id="icon_ValidaVin" class="glyphicon glyphicon-question-sign form-control-feedback" onclick="mostrarInfVehicularById('numeroSerie', 'idNegocioSeccion', 'idMonedaName', 'idEstado', 'idToCotizacion', 
			'idMarcaVehiculo', 'idEstiloVehiculo', 'idModeloVehiculo', 'incisoDescripcionFinal', 'idTipoUso', true);"></i>	
	</div>
	<div class="form-group col-sm-4" style="font-size:10px;" id="idTipoUsoDiv">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.tipoUso" />:</label>
		<s:select				
		    labelposition="top"
			id="idTipoUso" name="vehiculo.idTipoUso" headerKey=""
			headerValue="%{getText('midas.general.seleccione')}"
			list="tipoUsoMap"
      		value="incisoCotizacion.incisoAutoCot.tipoUsoId"
			cssClass="form-control-datos-generales mandatory"
			onchange="onChangeTipoUsoVehiculo('idMarcaVehiculo', 'idNegocioSeccion'); validaEstiloVehiculo();"/>
		<label class="control-label error-label">Selecciona el tipo de uso para continuar</label>
	</div>
	<div class="form-group col-sm-4" style="font-size:10px;" id="idMarcaVehiculoDiv">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.marca" />:</label>
		<s:select				
		    labelposition="top"
			id="idMarcaVehiculo" name="vehiculo.idMarcaVehiculo" headerKey=""
			headerValue="%{getText('midas.general.seleccione')}"
			list="marcaMap"
      		value="incisoCotizacion.incisoAutoCot.marcaId"
			cssClass="form-control-datos-generales mandatory"
			onchange="onChangeMarcaVehiculo('idModeloVehiculo', 'idMarcaVehiculo', 'idNegocioSeccion'); validaEstiloVehiculo();"/>
		<label class="control-label error-label">Selecciona la marca para continuar</label>
	</div>
	<div class="form-group col-sm-4" style="font-size:10px;" id="idModeloVehiculoDiv">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.modelo" />:</label>
		<s:select				
		    labelposition="top"
			id="idModeloVehiculo" name="vehiculo.idModeloVehiculo" headerKey=""
			headerValue="%{getText('midas.general.seleccione')}"
			list="modeloMap"
			value="incisoCotizacion.incisoAutoCot.modeloVehiculo"
			cssClass="form-control-datos-generales mandatory"
			onchange="validaEstiloVehiculo();limpiarTextBoxEstilo();"/>
			<label class="form-control-datos-generales error-label">Selecciona el modelo para continuar</label>
	</div>
	<div class="form-group col-sm-4" style="font-size:10px;" id="idEstiloVehiculoDiv">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.estilo" />:</label> 
		<s:textfield 
			name="idEstiloVehiculo" id="idEstiloVehiculo" 
			cssClass="form-control-datos-generales"  disabled="true"
			placeholder="Escribe solo una parte del estilo y selecciona el adecuado." 
			title="Debe ingresar los datos del vehiculo para ingresar el estilo"/>
		<s:hidden id="estiloSeleccionado" name="vehiculo.idEstiloVehiculo"  cssClass="mandatory"/>
		<s:hidden id="vehiculoEstiloName" name="vehiculoestiloName"  cssClass="mandatory"/>
		<label id="lblEstilo" class="mandatoryLbl"><s:property value="incisoCotizacion.incisoAutoCot.descripcionFinal" /></label>
		<label class="control-label error-label">Selecciona un estilo para continuar</label>
		<s:hidden id="incisoModificadoresDescripcion" name="incisoCotizacion.incisoAutoCot.modificadoresDescripcion"/>
		<s:hidden id="incisoDescripcionFinal" name="incisoCotizacion.incisoAutoCot.descripcionFinal"/>
	</div>
</fieldset>