<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="idBanco"/>
<fieldset>
	<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.datosTarjeta" /></legend>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.intitucionBancaria" /></label>
		<s:if test="cuentaPagoDTO.idBanco == 51">
			<span style="margin-left: 5px;">AFIRME</span>
		</s:if>
		<s:else>
			<s:select list="bancos" name="cuentaPagoDTO.idBanco"
				id="idBancoCobranza" headerKey="" headerValue="Seleccione"
				listKey="idBanco" cssClass="form-control mandatory"
				listValue="nombreBanco" />
			<label class="control-label error-label ">Selecciona Banco</label>
		</s:else>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label>
			<s:if test="idMedioPago == 8">
				<s:text name="midas.suscripcion.cotizacion.agentes.numeroclave" />
			</s:if><s:else>
				<s:if test="idMedioPago == 1386">
					<s:text name="midas.catalogos.agente.productosAgente.numeroCuenta" />
				</s:if><s:else>
					<s:text name="midas.suscripcion.cotizacion.agentes.numeroTarjeta" />
			</s:else>
		</label>
		
		<s:if test="idMedioPago == 1386"><!-- numeroCuenta -->
			<s:textfield id="numeroTarjetaCobranza"
				name="cuentaPagoDTO.cuenta" cssClass="form-control mandatory"
				onkeypress="return soloNumeros(this, event, false)" maxlength="11" />
		</s:if><s:else><!-- numeroTarjeta -->
			<s:textfield id="numeroTarjetaCobranza"
				name="cuentaPagoDTO.cuenta" cssClass="form-control mandatory"
				onkeypress="return soloNumeros(this, event, false)" maxlength="18" />
		</s:else>	
			<s:if test="idMedioPago == 8">
				<label class="control-label error-label ">Escribe el n&uacute;mero de CLABE</label>
			</s:if>
			<s:else>
				<label class="control-label error-label ">Escribe el n&uacute;mero de Cuenta Afirme</label>
			</s:else>
	</div>
</fieldset>
