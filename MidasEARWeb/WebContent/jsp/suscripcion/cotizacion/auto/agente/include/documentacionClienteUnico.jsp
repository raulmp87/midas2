<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>



<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<s:include
	value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp" />

<s:form id="DocumentacionForm" action="guardarEntrevista">
	<table>
		<tr>
			<td />
			<td />
			<td>

				<div class="form-group btn_back w100" id="botonEmision"
					style="float: right;">
					<s:hidden name="linkFortimax" id="linkFortimax" />

	<a href="javascript: loadFortimax();" onclick=""
	class="icon_guardar2"
	
	>  <s:text name="midas.clientes.documentacion.digitalizar"/></a>

				</div> <s:hidden name="cotizacion.idToCotizacion" /> <s:hidden
					name="cotizacion.idToPersonaContratante" /> <s:hidden
					name="cotizacion.claveEstatus" /> <s:hidden name="polizaId" /> <s:hidden
					name="tipoEndoso" /> <s:hidden name="fechaIniVigenciaEndoso" /> <s:hidden
					name="numeroInciso" />
			</td>

		</tr>

		<tr>
			<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo4"/></td>
		</tr>
		<s:iterator value="documentacion" status="index"  var="documento">

			<s:if test="%{#index.index==7}">
				<tr>
					<td class="titulo" colspan="3"><div id="inivigDiv"
							class="form-group has-feedback col-md-4">
							<s:text name="midas.clientes.documentacion.titulo1"/></div>
					</td>
				</tr>
			</s:if>
			<s:if test="%{#index.index==8}">
				<tr>
					<td class="titulo" colspan="3">
						<div id="inivigDiv" class="form-group has-feedback col-md-4">
							<s:text name="midas.clientes.documentacion.titulo2"/>
						</div></td>
				</tr>
			</s:if>
			<s:if test="%{#index.index==6}">
			<tr>
				<td class="titulo" colspan="3"><s:text name="midas.clientes.documentacion.titulo3"/></td>
			</tr>
			</s:if>
				
			<tr class="ev_light ">

			<td>
			<s:if test="#documento.seleccionado" >
			<s:checkbox
					name="documentacion[%{#index.index}].seleccionado" 
					disabled="true" 
					/>
				<s:hidden
					name="documentacion[%{#index.index}].seleccionado" 
					
					/>
			</s:if>
			
			<s:else  >
			<s:checkbox
					name="documentacion[%{#index.index}].seleccionado" 
					id = "checkBox[%{#index.index}].seleccionado" 
					onclick="javascript: checkBoxSeleccioneDocumento(%{#index.index})"
					/>
			</s:else>
				</td>
						
			<s:if test="#documento.requerido" >
			<td style="background-color: #f5f500">
			</s:if>
			<s:else  >
			<td>
			<s:hidden
					name="documentacion[%{#index.index}].descripcion "/>
			</s:else >
					<div id="inivigDiv_%{#index.index}" class="form-group has-feedback col-md-4"
						style="font-size: 10px;">

						<label class="small"><s:text
								name="documentacion[%{#index.index}].nombre" /> </label>
						<s:hidden name="documentacion[%{#index.index}].nombre"
							id="documentacion[%{#index.index}].nombre" />
						<s:hidden name="documentacion[%{#index.index}].documentoId"
							id="documentacion[%{#index.index}].documentoId" />
					</div></td>


				<td>
					<div id="inivigDiv" class="form-group has-feedback col-md-4"
						style="font-size: 10px;">


						<label class="small"><s:text name="midas.clientes.documentacion.especifique"/> </label>

						<s:textfield name="documentacion[%{#index.index}].descripcion "
							id="documentacion[%{#index.index}].descripcion"
							labelposition="left" maxlength="20" cssClass="cajaTextoM2 "
							disabled="true"
							 />
					</div></td>
			<s:if test="%{#index.index==5}">
				<td>

					<div id="inivigDiv" class="form-group has-feedback col-md-4"
						style="font-size: 10px;">
						<label class="small"><s:text name="Vigencia"/></label>
						<s:textfield id="documentacion[%{#index.index}].fechaVigencia"
							name="documentacion[%{#index.index}].fechaVigencia"
							cssClass="form-control-datos-generales datepicker" 
							disabled="true"
							/>

					</div></td></s:if>



			</tr>
		</s:iterator>
	<tr>
	
	<TD>
&nbsp;
</TD>
<TD>
&nbsp;
</TD><TD>
&nbsp;
</TD>
	</tr>

		<tr>
			<td />
			<td />
			<td><div class="form-group">
					<table align="right">
						<tr>

							<td>
								<div class="btn_back w100" id="botonEmision"
									style="float: right;">
									<a href="javascript: regresarCotizacionEmisionDocumentacion()">
									<s:text name="midas.boton.cancelar"/>  </a>
								</div>
							<td>


								<div class="btn_back w100" id="botonEmision"
									style="float: right;">
									<a
										href="javascript: verComplementarCotizacionDeDocumentacion()">
										<s:text name="midas.boton.guardar"/> </a>
								</div>
							</td>
						</tr>
					</table>
				</div></td>
		</tr>
	</table>
</s:form>