<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="cuentaPagoDTO.diaPago" /> 
<fieldset>
	<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.datosTarjeta" /></legend>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.intitucionBancaria" /></label>
		<s:select list="#{'6' : 'AMERICAN EXPRESS'}" name="cuentaPagoDTO.idBanco"
			id="idBancoCobranza" headerKey="" 
			cssClass="form-control mandatory" />
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.tipoTarjeta" /></label>
		<s:select list="#{'AMEX':'AMERICAN EXPRESS'}"
			headerKey="" 
			cssClass="form-control mandatory" name="tipoTarjeta"
			id="idTipoTarjetaCobranza" />
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.numeroTarjeta" /></label>
		<s:textfield id="cuenta"
			name="cuentaPagoDTO.cuenta" cssClass="form-control mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" maxlength="15" />
			<label class="control-label error-label ">Escribe el n&uacute;mero de tarjeta</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.codigoSeguridad" /></label>
		<s:textfield id="codigoSeguridad"
			name="cuentaPagoDTO.codigoSeguridad" maxlength="3"
			cssClass="form-control mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" maxlength="4" />
			<label class="control-label error-label ">Escribe el c&oacute;digo de seguridad </label>
	</div>
	<div class="form-group has-feedback col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.fechaVencimiento" /></label>
		<s:textfield id="fechaVencimiento"
			name="cuentaPagoDTO.fechaVencimiento" cssClass="form-control datepicker mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" maxlength="4" />
			<label class="control-label error-label ">Escribe la fecha de vencimiento</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><a href="javascript: getPromos();"><s:text name="midas.suscripcion.cotizacion.agentes.promocion" /></a></label>
		<s:select 
			id="promociones" name="cuentaPagoDTO.tipoPromocion"
			list="promociones"
			headerKey="" headerValue="SELECCIONE..."
			cssClass="form-control"/>
	</div>


	
</fieldset>