<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:form id="otrasCaracteristicasForm">
	<div class="row">
		<div class="col-md-12">
			<div class="well">
				<fieldset>
					<legend><s:text name="midas.suscripcion.cotizacion.agentes.caracteristicasOpcionales" /></legend>
					<s:iterator value="caracteristicasVehiculoList" status="stat">
						<div class="form-group">
							<label><s:text name="descripcion" />:</label>
							<s:select
								name="caracteristicasVehiculoList[%{#stat.index}].valorSeleccionado" 
								id="caracteristicasVehiculoList[%{#stat.index}].valorSeleccionado" 
								value="valorSeleccionado" 
								list="listadoVariablesModificadoras"									  	 
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"  
								required="false" cssClass="form-control" labelposition="top"/>
						</div>
					</s:iterator>
					<button type="button" class="btn btn-success" onClick="definirCaracteristicas()"> 
						<s:text name="midas.boton.aceptar" />
					</button>
				</fieldset>
			</div>
		</div>
	</div>
</s:form>