<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ taglib prefix="m" uri="/midas-tags"%>




<s:form id="addClienteAgente" cssClass="clientesForm">

<s:hidden name="cliente.nombreColonia" id="cliente.nombreColonia" />
<s:hidden name="cliente.codigoPostal" id="cliente.codigoPostal" />
<s:hidden name="cliente.idEstado" id="cliente.idEstado" />
<s:hidden name="cliente.idPaisString" id="cliente.idPaisString" />
<s:hidden name="cliente.idMunicipio" id="cliente.idMunicipio" />
<s:hidden name="cliente.nombreCalle" id="cliente.nombreCalle" />
<s:hidden name="cliente.nombreCalleFiscal"
	id="cliente.nombreCalleFiscal" />
<s:hidden name="cliente.idColoniaFiscal" id="cliente.idColoniaFiscal" />
<s:hidden name="cliente.codigoPostalFiscal"
	id="cliente.codigoPostalFiscal" />
<s:hidden name="cliente.idEstadoFiscal" id="cliente.idEstadoFiscal" />
<s:hidden name="cliente.idMunicipioFiscal"
	id="cliente.idMunicipioFiscal" />
<s:hidden name="cliente.rfcCobranza" id="cliente.rfcCobranza" />
<s:hidden name="cliente.nacionalidad" id="cliente.nacionalidad" />
<s:hidden name="cliente.nombreColoniaFiscal"
	id="cliente.nombreColoniaFiscal" />
<s:hidden name="cliente.idCliente" id="cliente.idCliente" />
<s:hidden name="cliente.idClienteUnico" id="cliente.idClienteUnico" />
<s:hidden name="cliente.codigoRFC" id="cliente.codigoRFC" />
<s:hidden name="cliente.email" id="cliente.email" />
<s:hidden name="cliente.faxContacto" id="cliente.faxContacto" />
<s:hidden name="cliente.gradoRiesgo" id="cliente.gradoRiesgo" />
<s:hidden name="cliente.twitterContacto" id="cliente.twitterContacto" />
<s:hidden name="cliente.facebookContacto" id="cliente.facebookContacto" />
<s:hidden name="cliente.paginaWebContacto"
	id="cliente.paginaWebContacto" />
<s:hidden name="cliente.telefonosAdicionalesContacto"
	id="cliente.telefonosAdicionalesContacto" />
<s:hidden name="cliente.correosAdicionalesContacto"
	id="cliente.correosAdicionalesContacto" />
<s:hidden name="cliente.extensionContacto"
	id="cliente.extensionContacto" />
<s:hidden name="cliente.telefonoOficinaContacto"
	id="cliente.telefonoOficinaContacto" />
<s:hidden name="cliente.celularContacto" id="cliente.celularContacto" />
<s:hidden name="cliente.numeroTelefono" id="cliente.numeroTelefono" />
<s:hidden name="cliente.codigoCURP" id="cliente.codigoCURP" />
<s:hidden name="cliente.idToPersona" id="cliente.idToPersona" />
<s:hidden name="cliente.claveTipoPersona" id="cliente.claveTipoPersona" />
<s:hidden name="cliente.claveTipoPersonaString"
	id="cliente.claveTipoPersonaString" />
<s:hidden name="cliente.idToPersonaString"
	id="cliente.idToPersonaString" />
<s:hidden name="cliente.apellidoMaterno" id="cliente.apellidoMaterno" />
<s:hidden name="cliente.apellidoPaterno" id="cliente.apellidoPaterno" />
<s:hidden name="cliente.nombre" id="cliente.nombre" />
<s:hidden name="cliente.sexo" id="cliente.sexo" />
<s:hidden name="cliente.claveCiudadNacimiento"
	id="cliente.claveCiudadNacimiento" />
<s:hidden name="cliente.claveEstadoNacimiento"
	id="cliente.claveEstadoNacimiento" />
<s:hidden name="cliente.claveOcupacion" id="cliente.claveOcupacion" />
<s:hidden name="cliente.ocupacionCNSF" id="cliente.ocupacionCNSF" />
<s:hidden name="cliente.idPaisNacimiento" id="cliente.idPaisNacimiento" />
<s:hidden name="cliente.claveNacionalidad"
	id="cliente.claveNacionalidad" />
<s:hidden name="cliente.estadoCivil" id="cliente.estadoCivil" />
<s:hidden name="cliente.fechaNacimiento" id="cliente.fechaNacimiento" />
<s:hidden name="cliente.idGiro" id="cliente.idGiro" />
<s:hidden name="cliente.idPaisConstitucion"
	id="cliente.idPaisConstitucion" />
<s:hidden name="cliente.fechaConstitucion"
	id="cliente.fechaConstitucion" />
<s:hidden name="cliente.claveSectorFinanciero"
	id="cliente.claveSectorFinanciero" />
<s:hidden name="cliente.razonSocial" id="cliente.razonSocial" />
<s:hidden name="cliente.idRepresentante" id="cliente.idRepresentante" />
<s:hidden name="cliente.numeroDomFiscal" id="cliente.numeroDomFiscal" />
<s:hidden name="cliente.idPaisFiscal" id="cliente.idPaisFiscal" />
<s:hidden name="tipoRegreso" id="tipoRegreso" value="5" />
<s:hidden name="cliente.numeroDom" id="cliente.numeroDom" />

<s:hidden name="cliente.nombreContacto" id="cliente.nombreContacto" />

<s:hidden name="cliente.puestoContacto" id="cliente.puestoContacto" />
<s:hidden name="cliente.verificarRepetidos"
	id="cliente.verificarRepetidos" value="false" />
<s:hidden name="cliente.tipoSituacionString"
	id="cliente.tipoSituacionString" />
<s:hidden name="fechaActual" id="txtFechaActual"/>


<s:hidden name="cliente.idEstadoDirPrin"/>
<s:hidden name="cliente.idMunicipioDirPrin"/>


<div id="clienteGrid"
	style="width: 938px; height: 480px; background-color: white; overflow: hidden; cursor: default;"
	class="gridbox gridbox_light" align="center">
	<table><tr><td>
			 		<s:text name="midas.clientes.texto.duplicados"/>			
				</td>
				</tr>
	</table>
	<div
		style="width: 100%; height: 40px; overflow: hidden; position: relative;"
		class="xhdr">
		<img
			style="display: inline; position: absolute; left: 307px; top: 5px;"
			src="/MidasWeb/img/dhtmlxgrid/sort_asc.gif">
		<table
			style="width: 938px; table-layout: fixed; margin-right: 20px; padding-right: 20px;"
			class="hdr" tdspacing="0" tdpadding="0">
			<tbody>
				<tr style="height: auto;">
					<th style="height: 0px; width: 260px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 260px;"></th>
					<th style="height: 0px; width: 40px;"></th>
					<th style="height: 0px; width: 40px;"></th>
				</tr>
				<tr>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.nombre"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.RFC"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.EMAIL"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.CURP"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.Direccion"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.cp"/></div></td>
					<td><div class="hdrcell"><s:text name="midas.clientes.duplicasos.Acciones"/></div></td>
					

				</tr>
			</tbody>
		</table>
	</div>
	<div style="width: 100%; overflow: auto; height: 460px;" class="objbox">
		<table style="width: 938px; table-layout: fixed;" class="obj tr20px"
			tdspacing="0" tdpadding="0">
			<tbody>
				<tr style="height: auto;">
					<th style="height: 0px; width: 260px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 100px;"></th>
					<th style="height: 0px; width: 260px;"></th>
					<th style="height: 0px; width: 40px;"></th>
					<th style="height: 0px; width: 40px;"></th>

				</tr>
				<s:iterator value="listaClientes" var="c" status="index">
					<tr class="ev_light " id="${index.count}">
						
	
						<td><s:property value="nombreCompleto" escapeHtml="false"
								escapeXml="true" />
						</td>
						<td><s:property value="codigoRFC" escapeHtml="false"
								escapeXml="true" />
						</td>
						<td><s:property value="email" escapeHtml="false"
								escapeXml="true" />
						</td><td><s:property value="codigoCURP" escapeHtml="false"
								escapeXml="true" />
						</td><td><s:property value="nombreCalleFiscal" escapeHtml="false"
								escapeXml="true" />
						</td><td><s:property value="codigoPostalFiscal" escapeHtml="false"
								escapeXml="true" />
						</td>
						<td><a
							href="javascript:complementarClienteDupplicados(${c.idCliente}, true)"
							target="_self"><img src="/MidasWeb/img/icons/ico_editar.gif"
								title="Editar" border="0"> </a></td>
					</tr>
				</s:iterator>
		</table>
	</div>
</div>
<table> <tr>
<td class="guardar">
				<div align="right">
					<button id="addClient" type="button" class="btn btn-success"
						onclick="guardarClienteDuplicadosAjustador();">
						<s:text name="midas.boton.continuar" />
						<span class="glyphicon glyphicon-ok"></span>
					</button>
				</div>
			</td> 
			
				<td>
				
				
				<div align="right">
					<a id="backClientBtn" class="btn-success"
						href="javascript:cancelarGuardarClienteDuplicadosAjustado();">
						<s:text name="midas.boton.regresar" />
						
					</a>
				</div>
							
				</td>		
</tr>

</table>
 </s:form>

