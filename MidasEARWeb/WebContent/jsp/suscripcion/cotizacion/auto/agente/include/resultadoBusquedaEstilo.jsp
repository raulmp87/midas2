<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:if test="mapGenerico.size>0">
		<s:iterator value="mapGenerico">
			<item>
				<id><s:property value="key"/></id>
				<descripcion><s:property value="value" escapeHtml="false" escapeXml="true" /></descripcion>
			</item>
		</s:iterator>
	</s:if>
	<s:else>
		<item>
			<id>0</id>
			<descripcion>No se encontraron Resultados ...</descripcion>
		</item>
	</s:else>
</resultados>