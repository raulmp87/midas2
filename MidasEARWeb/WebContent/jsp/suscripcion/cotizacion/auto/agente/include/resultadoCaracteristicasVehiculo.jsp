<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="row">
	<s:hidden id="modificadoresDescripcionName" name="incisoAutoCot.modificadoresDescripcion" value="%{incisoAutoCot.modificadoresDescripcion}"/>
	<div class="col-md-12">
		<div class="well">
			<fieldset>
				<legend><s:text name="midas.suscripcion.cotizacion.agentes.caracteristicasOpcionales" /></legend>
				<div class="form-group">
					<label><s:text name="midas.general.descripcionFinal" />:</label>
					 <s:textfield id="descripcionFinalName" 
				 		name="incisoAutoCot.descripcionFinal" value="%{incisoAutoCot.descripcionFinal}" 
				 		cssClass="form-control" readonly="true"/>
				 </div>
				 <div class="pull-right">
					<button id="btnOtrasCaract" class="btn btn-success" onclick="guardarVariablesModificadoras();">
						<s:text name="midas.boton.aceptar" />
					</button>
				</div>
			</fieldset>
		</div>
	</div>
</div>