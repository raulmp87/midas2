<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="cuentaPagoDTO.diaPago" /> 
<fieldset>
	<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.datosTarjeta" /></legend>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.intitucionBancaria" /></label>
		<s:select list="bancos" name="cuentaPagoDTO.idBanco"
			id="idBancoCobranza" headerKey="" headerValue="Seleccione"
			listKey="idBanco" cssClass="form-control-datos-generales mandatory"
			listValue="nombreBanco" />
			<label class="control-label error-label ">Selecciona el banco </label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.tipoTarjeta" /></label>
		<s:select list="#{'VS':'VISA','MC':'Master Card'}"
			headerKey="" headerValue="Seleccione ..."
			cssClass="form-control-datos-generales mandatory" name="tipoTarjeta"
			id="idTipoTarjetaCobranza" />
			<label class="control-label error-label ">Selecciona el tipo de tarjeta</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.numeroTarjeta" /></label>
		<s:textfield id="cuenta"
			name="cuentaPagoDTO.cuenta" cssClass="form-control-datos-generales mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" 
			onchange="javascript:void(0);getPromos();"
			maxlength="16" />
			<label class="control-label error-label ">Selecciona el número de tarjeta</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.codigoSeguridad" /></label>
		<s:textfield id="codigoSeguridad"
			name="cuentaPagoDTO.codigoSeguridad" maxlength="3"
			cssClass="form-control-datos-generales mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" 
			maxlength="4" />
			<label class="control-label error-label ">Selecciona el código de seguridad</label>
	</div>
	<div class="form-group has-feedback col-md-4" style="font-size:10px;">
		<label><s:text name="midas.suscripcion.cotizacion.agentes.fechaVencimiento" /></label>
		<s:textfield id="fechaVencimiento"
			name="cuentaPagoDTO.fechaVencimiento" cssClass="form-control-datos-generales datepicker mandatory"
			onkeypress="return soloNumerosYGuion(this, event, false)" maxlength="4" />
			<label class="control-label error-label ">Selecciona la fecha</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label><s:text name="midas.suscripcion.cotizacion.agentes.promocion" /></label>
		<s:select 
			id="promociones" name="cuentaPagoDTO.tipoPromocion"
			list="promociones"
			headerKey="" headerValue="SELECCIONE..."
			cssClass="form-control-datos-generales"/>
	</div>
</fieldset>