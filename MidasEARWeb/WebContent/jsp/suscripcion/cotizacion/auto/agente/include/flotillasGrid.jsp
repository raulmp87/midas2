<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <column id="lineaNegocio" type="ro" width="150" sort="int" hidden="false"><s:text name="midas.suscripcion.cotizacion.agentes.lineaNegocio" /></column>
      	<column id="tipoUso" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.agentes.tipoUso" /></column>
      	<column id="marca" type="ro" width="200" sort="str"><s:text name="midas.suscripcion.cotizacion.agentes.marca" /></column>
      	<column id="modelo" type="ro" width="100" sort="str"><s:text name="midas.suscripcion.cotizacion.agentes.modelo" /></column>
		<column id="estilo" type="ro" width="*" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.agentes.estilo" /></column>		
	</head>
	<s:iterator value="listaFlotilla" status="row">
		<row>
			<cell><s:property value="lineaNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoUso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="marca" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="modelo" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="estilo" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>