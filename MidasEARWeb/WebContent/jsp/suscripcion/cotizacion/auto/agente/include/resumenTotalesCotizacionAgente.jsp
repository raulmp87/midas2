<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %> 
<s:hidden name="configuracionIdResumen" id="configuracionIdResumen" />
<fieldset>
	<label style="font-size:12px;" class="text-left"><s:text name="midas.suscripcion.cotizacion.agentes.resumen" /></label>
	</br>
    <div class="col-md-12"> 
		<label style="font-size:11px;" class="pull-right" id="divBienAsegurado"></label>
	</div>
		</br>
	<div class="col-md-12"> 
	           <label style="font-size:11px;" class="pull-right"  id="div_resumenCostosDTO_resumen.primaTotal">
					<s:text name="struts.money.format">
					    <s:param name="resumenCostosDTO.primaTotal" value="resumenCostosDTO.primaTotal"/>
					</s:text>			
				</label>
			    <label  style="font-size:11px;"  class="pull-right"><s:text name="Prima Total de Periodo"/>:&nbsp</label>		
	</div>
	  <div class="form-group col-md-3" style="font-size:10px;" id="btn_IgualarDiv">
		<s:if test="configuracionIdResumen == null || configuracionIdResumen == \"\" " >
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Igualacion_Prima">
				<a href="#divIgualaPrimas" class="btn btn-default" id="btn_Igualar" style ="display:none">
					<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.boton.igualarPrima" /><span class="glyphicon glyphicon-usd"></span>
				</a>
			</m:tienePermiso>
		</s:if>
	  </div>  
	   <div class="form-group col-md-3" style="font-size:10px;" id="btn_EsquemasPagosDiv">
			<a href="#divEstimacionTC" id="btn_EsquemasPagos" class="btn btn-default" style ="display:none">
				<s:text name="midas.suscripcion.cotizacion.agentes.estimacionRecibos" /><span class="glyphicon glyphicon-usd"></span>
		   </a>
	  </div>
	  <div class="form-group col-md-3" style="font-size:10px;" id="btn_MostrarResumenDiv">
	 <button type="button"  class="btn btn-default" aria-hidden="true" id="btn_mostrarResumen" onclick="javascript:mostrarResumenCotizacion();">Mostrar Resumen</button>
	  </div>
      <div class="form-group col-md-3" style="font-size:10px;" id="btn_impresionDiv">
		<a href="#divImpresion" class="btn btn-default" id="btn_impresion">
			<s:text name="midas.componente.impresiones.botonImprimir" /> <span class="glyphicon glyphicon-print"></span>
		</a>
      </div>
         <div id="resumenCotizacion" class="modal fade">
                <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
               <!--   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
             </div>
                   <div class="modal-body">
                <div class="table">
			<table class="table table-hover">
		        <div class="form-group col-md-12" style="font-size:10px;">
					<div class="col-md-12"> 
					     <div class="" id="divBienAsegurado"></div>
				    </div>
				     <span class="pull-right" id="divButtonResumen">
				     <m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir,FN_M2_Liga_Agente">
				</div>
<!-- 		<div class="col-md-12" style="font-size:10px;"> -->
<!-- 		    <div class="form-group col-md-3" style="font-size:10px;"> -->
<!-- 					<a href="#divImpresion" class="btn btn-default" id="btn_impresion"> -->
<%-- 						<s:text name="midas.componente.impresiones.botonImprimir" /> <span class="glyphicon glyphicon-print"></span> --%>
<!-- 					</a> -->
<!-- 		    </div> -->
		</m:tienePermiso>
	     <div class="form-group col-md-3" style="font-size:10px;">
		 <s:if test="configuracionIdResumen == null || configuracionIdResumen == \"\" " >
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Igualacion_Prima">
				<a href="#divIgualaPrimas" class="btn btn-default" id="btn_Igualar" style ="display:none">
					<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.boton.igualarPrima" /><span class="glyphicon glyphicon-usd"></span>
				</a>
			</m:tienePermiso>
		 </s:if>
		 </div>
		    <div class="form-group col-md-3" style="font-size:8px;">
			    <a href="#divEstimacionTC" id="btn_EsquemasPagos" class="btn btn-default" style ="display:none">
				     <s:text name="midas.suscripcion.cotizacion.agentes.estimacionRecibos" /><span class="glyphicon glyphicon-usd"></span>
			    </a>
	       </div>	
	 <label style="font-size:12px;"  class="form-group col-md-12" class="text-left"> <s:text name="midas.suscripcion.cotizacion.agentes.resumen" /></label>
	<div class="col-md-12" style="font-size:10px;">
		<div class="table">
			<table class="table table-hover">
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Vigencia"/></td>
					<td width="40%" > 
						<div id="resumenFecha"></div>
					</td>
				</tr>
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Paquete"/></td>
					<td width="40%" > 
						<div id="resumenPaquete"></div>
					</td>
				</tr>
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Periodo de Pago"/></td>
					<td width="40%" > 
						<div id="resumenPago"></div>
					</td>
				</tr>
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Total de Primas"/></td>
					<td width="40%" > 
						<div id="div_resumenCostosDTO.primaNetaCoberturas">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.primaNetaCoberturas" value="resumenCostosDTO.totalPrimas"/>
							</s:text>			
						</div>
					</td>
				</tr>	
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Descuentos / Comis Ced."/></td>
					<td width="40%" >
						<div id="div_resumenCostosDTO.descuentoComisionCedida">

							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.descuentoComisionCedida" value="resumenCostosDTO.descuentoComisionCedida"/>
							</s:text>				
						</div>
					</td>
				</tr>	
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Prima Neta"/></td>
					<td width="40%" >
						<div id="div_resumenCostosDTO.totalPrimas">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.totalPrimas" value="resumenCostosDTO.primaNetaCoberturas"/>
							</s:text>				
						</div>
					</td>
				</tr>	
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Recargo"/></td>
					<td width="40%" >
						<div id="div_resumenCostosDTO.recargo">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.recargo" value="resumenCostosDTO.recargo"/>
							</s:text>				
						</div>
					</td>
				</tr>	
				<tr>
					<td width="60%" style="font-size:10px;"><s:text name="Derechos"/></td>
					<td width="40%" >
						<div id="div_resumenCostosDTO.derechos">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.derechos" value="resumenCostosDTO.derechos"/>
							</s:text>				
						</div>
					</td>
				</tr>	
				<tr height="24px">
					<td width="60%" style="font-size:10px;">
					<div id="porcentajeIvaContent" style="font-size:10px;">
							<s:text name="IVA"/><s:property value="valorIvaSeleccionado" />%
					</div>	
					</td>
					<td width="40%" style="font-size:10px;">
						<div id="div_resumenCostosDTO.iva">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.iva" value="resumenCostosDTO.iva"/>
							</s:text>				
						</div>
					</td>
				</tr>	
				<tr>
					<td width="60%" style="font-size:15px;"><s:text name="Prima Total de Periodo"/></td>
					<td width="40%" >
						<div style="font-size:15px;" id="div_resumenCostosDTO.primaTotal">
							<s:text name="struts.money.format">
							    <s:param name="resumenCostosDTO.primaTotal" value="resumenCostosDTO.primaTotal"/>
							</s:text>			
						</div>
					</td>
				</tr>		
			</table>
		</div>
		 <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>              
         </div>
	</div>
	</div>
	</div>
</fieldset>