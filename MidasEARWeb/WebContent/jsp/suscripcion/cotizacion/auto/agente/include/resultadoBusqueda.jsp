<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:if test="agentesList.size>0">
		<s:iterator value="agentesList">
		<item>
			<idAgente><s:property value="idAgente" /></idAgente>
			<id><s:property value="id"/></id>
			<descripcion><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true" /> - <s:property value="idAgente" /></descripcion>
		</item>
		</s:iterator>
	</s:if>
	<s:else>
		<item>
			<idAgente>0</idAgente>
			<id>0</id>
			<descripcion>No se encontraron Resultados ...</descripcion>
		</item>
	</s:else>
</resultados>
