<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<fieldset>
	<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.datosTitularTarjeta" /></legend>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.negocio.nombres" /></label>
		<s:textfield 
			name="cuentaPagoDTO.tarjetaHabiente" id="tarjetaHabiente"
				cssClass="form-control-datos-generales mandatory" />
				<label class="control-label error-label ">Selecciona el nombre</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.emision.consulta.cliente.rfc" /></label>
		<s:textfield 
			id="rfc" name="cuentaPagoDTO.rfc" 
			cssClass="form-control-datos-generales mandatory" maxLength="13" />
			<label class="control-label error-label ">Selecciona el el RFC</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.prestamosAnticipos.aval.cp" /></label>
		<s:textfield 
			id="codigoPostal" name="cuentaPagoDTO.domicilio.codigoPostal"
			onkeypress="return soloNumerosYGuion(this, event, false)"
			cssClass="form-control-datos-generales mandatory" maxlength="5"
			onblur="onchangeCp();" />
			<label class="control-label error-label ">Selecciona el código postal</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.pais" /></label>
		<s:textfield id="txbMexico" value="MÉXICO" cssClass="form-control-datos-generales" disabled="true" />
		<s:hidden name="cuentaPagoDTO.domicilio.clavePais" value="PAMEXI" />
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.estado" /></label>
		<s:select id="idEstadoCobranza" list="estadoMap"
			name="cuentaPagoDTO.domicilio.claveEstado" onchange="onchangeEstadoCobranza();"
			headerValue="%{getText('midas.general.seleccione')}" headerKey=""
			cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label ">Selecciona el estado</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.municipio" /></label>
		<s:select id="idMunicipioCobranza" list="municipioMap"
			name="cuentaPagoDTO.domicilio.claveCiudad" onchange="onchangeMunicipioCobranza();"
			headerValue="%{getText('midas.general.seleccione')}" headerKey=""
			cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label ">Selecciona el municipio</label>
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.colonia" /></label>
		<s:select id="idColoniaCobranza" list="coloniasMap"
			name="cuentaPagoDTO.domicilio.idColonia" onchange="obtenerNombreColonia();"
			headerValue="%{getText('midas.general.seleccione')}" headerKey=""
			cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label ">Selecciona la colonia</label>
			<script type="text/javascript">
				function obtenerNombreColonia() {
				$("#nombreColonia").val(jQuery('#idColoniaCobranza option:selected')
							.text());
				}
			</script>
			<s:hidden name="cuentaPagoDTO.domicilio.nombreColonia" id="nombreColonia" />
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.calleNumero" /></label>
		<s:textfield 
			id="calleNumero" name="cuentaPagoDTO.domicilio.calleNumero" 
			cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label ">Selecciona la calle y número</label>
			
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.siniestros.cabina.monitor.telefono" /></label>
		<s:textfield id="telefono"
			onkeypress="return soloNumerosYGuion(this, event, false)"
			name="cuentaPagoDTO.telefono"
			cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label ">Selecciona el número de telefono</label>
		
	</div>
	<div class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.configuracionNotificaciones.correo" /></label>
		<s:textfield 
			id="correoTitular" name="cuentaPagoDTO.correo" 
			cssClass="form-control-datos-generales mandatory email"/>			
		<label class="control-label error-label" >Correo inv&aacute;lido</label>
			
	</div>
</fieldset>