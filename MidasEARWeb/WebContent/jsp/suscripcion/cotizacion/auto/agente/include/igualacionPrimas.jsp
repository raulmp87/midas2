<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<html>
	<head>
		<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp"/>
		<script type="text/javascript">
			$(document).ready(function(){
				if($.isValid($('#controlArchivoId').val())){
					subirArchivo();	
				}
			});
		</script>
	</head>
	<body>
		<s:form action="igualarPrimasAgente" namespace="/suscripcion/cotizacion/auto/cotizadoragente" id="igualarPrimasForm" enctype="multipart/form-data">
			<s:hidden name="idToCotizacion" id="ip_idToCotizacion"/>
			<s:hidden id="controlArchivoId" name="controlArchivoId" />
			<s:hidden name="MAX_FILE_SIZE" value="943718400" />
			<s:hidden name="UPLOAD_IDENTIFIER" />
			<s:hidden name="claveTipo" value="4" />
			<div class="row" style="width: 600px; height:290px;">
				<div class="col-md-12">
					<div class="well">
						<fieldset>
							<legend><s:text name="midas.suscripcion.cotizacion.agentes.igualacionPrimas"/></legend>
							<div id="divMsnIgualarPrima" style="display:none" class="<s:if test="mensaje!=null">alert<s:if test="tipoMensaje==30">alert-success</s:if><s:else>alert-danger</s:else></s:if>">
								<span class="pull-right" onclick="$('#divMsnIgualarPrima').hide('slow')">X</span>
								<p id="divMsnIgualarPrimaText"><s:if test="mensaje!=null"><s:property value="mensaje"/></s:if></p>
							</div>
							<div id="detallePrimaObjetivo">
								<label><s:text name="midas.suscripcion.cotizacion.igualacionPrimas.title" /></label>
								<div class="form-group">
									<label><s:text name="midas.suscripcion.solicitud.comentarios.archivo" />:</label>
									   <s:file id="file1" name="file1" value="" />
								</div>
								<div class="form-group">
									<label><s:text name="midas.suscripcion.cotizacion.igualacionPrimas.primaTotal" />:</label>
									<s:textfield name="primaAIgualar" 
									   required="#requiredField"		          				 							  
									   id="primaAIgualar" maxlength="10" cssClass="form-control"
									   onkeypress="return soloNumerosYGuion(this, true)" />
								</div>
								<div class="form-group" align="right">
									<button id="btnAceptar" type="button" class="btn btn-success" onclick="ejecutarIgualacion();">
										<s:text name="midas.boton.aceptar" />
									</button>
									<button id="btnEliminar"  type="button" class="btn btn-success" onclick="eliminarIgualacion();">
										<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.eliminar" />
									</button>
								</div>
							</div>
					</fieldset>
					</div>
				</div>
			</div>
		</s:form>
	</body>
</html>
