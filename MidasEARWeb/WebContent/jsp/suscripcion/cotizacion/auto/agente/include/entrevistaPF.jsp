<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>



<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<s:include
	value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp" />
<title>Afirme - Cotizador Agentes</title>
</head>


<s:form id="entrevistaForm" action="guardarEntrevista">
	<div  id="idClienteUnico">
	<div  id="divMostEntrev"  >
	<s:hidden name="cotizacion.idToCotizacion" />
	<s:hidden name="entrevista.idToCotizacion" id="entrevista.idToCotizacion"/>
	<s:hidden name="entrevista.moneda" id="entrevista.moneda"/>
	<s:hidden name="cotizacion.idToPersonaContratante" />
	<s:hidden name="cotizacion.claveEstatus" />
	

	<s:hidden name="numeroInciso" />

	<table width="98%" bgcolor="white" align="center"
		class="contenedorConFormato">
		<thead>
			<tr>
				<div id="personaFisica"></div>
				<td colspan="3" align="center"><h4><s:text name="midas.clientes.entrevista.prima.title"/></h4></td>
			</tr>
		</thead>
		<tbody>
			 <tr>
				<td width="100%">
					<table class="contenedorFormas no-Border" style="padding-left: 0; border: none;">
						<tr>
							<td width="33%" style="display: none">
								<div>
									<table class="contenedorFormas no-Border">
										<tr>
											<td><label> <s:text name="midas.clientes.entrevista.prima.estimada"/> </label>
											</td>
											<td><s:textfield name="entrevista.primaEstimada"
													disabled="%{#readOnly}" id="entrevista.primaEstimada"
													
													labelposition="left" maxlength="40" cssClass="" />
													
													</td>
										</tr>
									</table>

								</div></td>
							<td width="33%" style="display: none">
								<table class="contenedorFormas no-Border" style="border: none">

									<tr>
										<td>
										</td>
									</tr>
								</table></td>

							<td width="33%">
								<table class="contenedorFormas no-Border"style="padding-left: 0; border: none;">
									<tr>
										<td>
											<label>
												<s:text	name="midas.clientes.entrevista.prima.fiel" />
											</label>
										</td>
										<td>
											<s:textfield name="entrevista.numeroSerieFiel"
												disabled="%{#readOnly}" id="entrevista.numeroSerieFiel"
												label="midas.clientes.entrevista.prima.fiel"
												onkeypress="return soloNumerosM2(this, event, false)"
												labelposition="left" maxlength="30" cssClass="" value="0" />
										</td>
										<td>
											<img src="/MidasWeb/img/question2.png"
											 title="<s:text name="midas.clientes.entrevista.prima.fiel.tooltip"/>"
											 style="width: 22px;" />
										</td>
									</tr>
								</table></td>

						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table class="contenedorFormas no-Border" style="border: none">
						<tr>
							<td width="85%"><label><s:text name="midas.clientes.entrevista.pep"/> </label> 
							</td>
							<td><s:radio id="entrevista.pep" disabled="%{#readOnly}"
									name="entrevista.pep" list="#{'true':'Sí','false':'No'}"
									cssClass="cajaTextoM2 "
									onclick="return changePEPS();"
									>
									
									
									</s:radio>
							</td>
						</tr>

					</table></td>

			</tr>
			
			<tr>
				<td width="100%">
					<table class=" no-Border">
						
						<tr id="pepDiV">
							<td width="25%">
								<div>
									<table class="contenedorFormas no-Border" style="border: none">
										<tr>
											<td><label><s:text name="midas.clientes.entrevista.parentesco"/></label>
											</td>
											<td><s:textfield name="entrevista.parentesco"
													disabled="%{#readOnly}" id="entrevista.parentesco"
													labelposition="left" maxlength="40" cssClass="" />
											</td>
										</tr>
									</table>
								</div></td>

							<td width="25%">
								<table class="contenedorFormas no-Border" style="border: none">
									
									<tr>
										<td><label><s:text name="midas.clientes.entrevista.puesto"/></label>
										</td>
										<td><s:textfield name="entrevista.nombrePuesto"
												disabled="%{#readOnly}" id="entrevista.nombrePuesto"
												labelposition="left" maxlength="50" cssClass="" />
										</td>
									</tr>
								</table>
							</td>
							<td width="25%">
								<table class="contenedorFormas no-Border">
									<tr>
										<td><label><s:text name="midas.clientes.entrevista.periodo"/></label>
										</td>
										<td><s:textfield name="entrevista.periodoFunciones"
												disabled="%{#readOnly}" id="entrevista.periodoFunciones"
												labelposition="left" maxlength="50" cssClass="" />
										</td>
									</tr>
								</table>
							</td>
							</tr>
							<tr>
							<td width="25%">
								<table class="contenedorFormas no-Border" style="border: none">
									<tr>
										<td><label><s:text name="midas.clientes.entrevista.cuenta.propia"/></label></td>
										<td><s:radio id="entrevista.cuentaPropia"
												disabled="%{#readOnly}" name="entrevista.cuentaPropia"
												list="#{'true':'Sí','false':'No'}" cssClass="cajaTextoM2 "
												onclick="return changeCuentaPropia();"
												></s:radio>
										</td>
									</tr>


								</table>
								</td></tr>
								
								<tr id="cuentaPropiaDiv">
								<td>
								<table class="contenedorFormas no-Border" style="border: none">
									<tr>
										<td><label> <s:text name="midas.clientes.entrevista.nombre.actua"/> </label></td>
										<td><s:textfield name="entrevista.nombreRepresentado"
												disabled="%{#readOnly}" id="entrevista.nombreRepresentado"
												label=" Nombre por quién actúa"
												labelposition="left" maxlength="30" cssClass="" /></td>
									</tr>
								</table>
								
							<td width="25%">
								<table class="contenedorFormas no-Border" style="border: none">
									<tr id="documentoRepresentacion">
										<td><label><s:text name="midas.clientes.entrevista.doc.rep"/>
											 </label>
										</td>

										<td><s:radio id="entrevista.documentoRepresentacion"
												disabled="%{#readOnly}"
												name="entrevista.documentoRepresentacion"
												list="#{'true':'Sí','false':'No'}" cssClass="cajaTextoM2 "></s:radio>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
			</tr>

		</tbody>

	</table>
	<table align="right">
		<tr>

			<td>
				<div class="btn_back w100" id="botonEmision" style="float: right;">
					<a href="javascript: regresarCotizacionEmision()"> 
						<s:text name="midas.boton.cancelar"/>  
					</a>
				</div>
			<td>


				<div class="btn_back w100" id="botonEmision" style="float: right;">
					<a href="javascript: verComplementarCotizacionDeEntrevista()">
						<s:text name="midas.boton.guardar"/> </a>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
</s:form>

	<script type="text/javascript">
	
		jQuery(document).ready(function(){
			jQuery("input:radio").attr("checked", false);
			jQuery('#pepDiV').hide();
			jQuery("#cuentaPropiaDiv").hide();
		 });

	</script>

