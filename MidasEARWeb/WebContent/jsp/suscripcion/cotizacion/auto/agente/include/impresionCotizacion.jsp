<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp"/>
<div id="divMsnImpresion" style="<s:if test="mensaje==null">display:none</s:if>" class="<s:if test="mensaje!=null">alert <s:if test="tipoMensaje==30"> alert-success</s:if><s:else>alert-danger</s:else></s:if>">
	<span class="pull-right" onclick="$('#divMsnImpresion').hide('slow')">X</span>
	<p id="divMsnImpresionText"><s:if test="mensaje!=null"><s:property value="mensaje"/></s:if></p>
</div>
<div class="row" style="width: 600px; height:390px;">
	<div class="col-md-12">
		<div class="well">
			<fieldset>
				<legend style="font-size:16px;">
				<s:if test="%{tipoImpresion == 1}">
					<s:text name="midas.cotizacion.imprimircotizacion"/>
				</s:if>				
				<s:if test="%{tipoImpresion == 2}">
					<s:text name="midas.suscripcion.cotizacion.agentes.imprimirPoliza"/>
				</s:if>
				</legend>
				<div class="alert alert-info" style="margin-bottom:0"><strong><s:text name="midas.componente.impresiones.titulo.modal.footer" /></strong></div>
				<br/>
		 		<s:form action="generarImpresionAgente" namespace="/impresiones/componente" id="contenedorImpresionForm">
				    <s:hidden name="nombreUsuario"/>
				    <s:hidden name="idSesionUsuario"/>
			 		<s:hidden name="tipoImpresion"/>
					<s:hidden name="totalIncisos"/>
					<s:hidden name="id"/>	
					<s:hidden name="idToPoliza"/>
					<s:hidden name="validOnMillis" />
					<s:hidden name="recordFromMillis" />
					<s:hidden name="claveTipoEndoso" />
					<s:hidden name="esSituacionActual" />
					<s:hidden name="correo"/>
					<s:hidden name="correoObligatorio"/>
					<s:hidden name="contratante"/>
					<s:hidden name="tienePermisosNoBrocker"/> 
		   			<s:hidden name="tienePermisosAviso"/> 
		  			<s:hidden name="tienePermisosDerechos"/> 
					<div class="table">
			 		<table width="100%" class="table">
			 			<% /*
			 				tipoImpresion == 1 : Cotizacion
			 				tipoImpresion == 2 : Poliza
			 				tipoImpresion == 3 : Endoso
			 			*/ %>
						<tr>
							<td><s:text name="midas.emision.totalincisos" /></td>
				     		<td><s:text name="totalIncisos" /></td>
				   		</tr>
						<s:if test="%{tipoImpresion != 3}">
					  	<tr>
							<td width="30%"><s:text name="midas.componente.impresiones.caratula" /></td>
							<td width="30%">
						  		<s:checkbox name="chkcaratula"  id="chkcaratula" value="1" cssClass="form-control"/> 
							</td>
		 				</tr>
						<tr>
							<td width="30%"><s:text name="midas.componente.impresiones.inciso" /></td>
							<td width="30%">
						  		<s:checkbox name="chkinciso" id="chkinciso" alt="Seleccionar Todos" title="Seleccionar Todos" value="1" cssClass="form-control"/> 
							</td>	
						</tr>
						<tr>
							<td width="30%">
							  <s:text name="midas.componente.impresiones.incisoTexto" /> 
							</td>
							<td width="30%">
							  <s:textfield 
							  	name="txtIncios" id="txtIncios" 
							  	nmousemove="setInciosTxt(this.value);"  
							  	cssClass="form-control" onkeyup="return soloNumerosYGuion(this, true)" />
							</td>	
						</tr>
							<s:if test="%{tipoImpresion != 1}">
		 						<tr>
									<td width="30%"><s:text name="midas.componente.impresiones.recibo" /></td>
									<td width="30%">
										<s:checkbox 
											name="chkrecibo" id="chkrecibo" alt="Seleccionar Todos" 
											title="Seleccionar Todos" onclick="" cssClass="form-control"/> 
									</td>	
								</tr>
						    </s:if>
	  						<s:if test="%{tipoImpresion ==2}">	
								<tr>
									<td width="30%"><s:text name="midas.componente.impresiones.inciso.mensaje" /></td>
									<td width="30%">
										<s:checkbox 
											name="chkcertificado" id="chkinciso" alt="Seleccionar Todos" 
											title="Seleccionar Todos" cssClass="form-control"/> 
									</td>	
								</tr>
								<tr>
									<td width="30%"><s:text name="midas.componente.impresiones.anexo" /> 
									</td>
									<td width="30%">
										<s:checkbox 
									  		name="chkanexos" id="chkanexos" alt="Seleccionar Todos" title="Seleccionar Todos" cssClass="form-control"/> 
									</td>	
								</tr>
								<tr>
								  	<td width="30%"><s:text name="midas.suscripcion.cotizacion.agentes.condicionesEspeciales" /></td>
								  	<td width="30%">
								    	<s:checkbox name="chkcondiciones" id="chkcondiciones" alt="Seleccionar Todos" 
								    		title="Imprimir condiciones especiales ligadas." cssClass="form-control"/>
								  	</td>
								</tr>
								<tr>
								  	<td width="30%">
								    	<s:text name="midas.componente.impresiones.fechaDeValidez"></s:text>
								  	</td>
									<td width="30%">
								  		<s:textfield 
								  			name="validOn" id="validOn" value="%{validOn}" 
								  			readonly="true" cssClass="form-control"/>
									</td>
								</tr>
							  </s:if>									
						  </s:if>
						  <s:if test="%{tipoImpresion == 2}">
						  		<s:if test="tienePermisosAviso">
						        <tr>
								  	<td width="30%"><s:text name="Aviso de privacidad" /></td>
								  	<td width="30%">
								    	<s:checkbox name="chkAviso" id="chkAviso" alt="Seleccionar Todos" 
								    		title="Aviso de privacidad." cssClass="form-control"/>
								  	</td>
								</tr>
								</s:if>
								<s:if test="tienePermisosDerechos">
								<tr>
								  	<td width="30%"><s:text name="Derechos Basicos del Asegurado" /></td>
								  	<td width="30%">
								    	<s:checkbox name="chkDerechos" id="chkDerechos" alt="Seleccionar Todos" 
								    		title="Derechos Basicos del Asegurado." cssClass="form-control"/>
								  	</td>
								</tr>
								</s:if>
						  </s:if>
						  <s:if test="%{tipoImpresion != 1}">
						  <s:if test="tienePermisosNoBrocker">
						      <tr>
							      <td width="30%">
							  <s:text name="Orden de Trabajo Brocker" /> 
							      </td>
								  <td width="30%">
							  <s:textfield 
							  	  name="txtNoOrder" id="txtNoOrder" 
							  	  cssClass="form-control" />
								</td>	
							</tr>
							</s:if>
						  </s:if>
	         		</table>
	         		<s:hidden name="txtIncioshidden" id="txtIncioshidden"  />
	         	</div>
	         	<div class="pull-right">
					<button type="submit" id="btnImprimirModal" class="btn btn-success">
						<s:text name="midas.componente.impresiones.botonImprimir" /> <span class="glyphicon glyphicon-print"></span>
					</button>
				</div>
				</s:form>
			</fieldset>
		</div>
	</div>
</div>