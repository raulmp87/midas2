<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div class="table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.formadePago" /></th>
				<th><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoInicial" /> </th>
				<th><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.cantidad" /></th>
				<th><s:text name="midas.suscripcion.cotizacion.esquemaCotizacion.pagoSubsecuente" /></th>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="esquemaPagoCotizacionList" status="stat">
			<tr>
				<td ><s:property value="formaPagoDTO.descripcion" /></td>
				<td align="right">
					<div id="pagoInicial.importeFormat_<s:property value='%{#stat.index}'/>">
						<s:property value="pagoInicial.importeFormat" />
					</div>
				</td>
				<td align="center">
					<div id="pagoSubsecuente.numExhibicion_<s:property value='%{#stat.index}'/>">
						<s:if test="pagoSubsecuente.numExhibicion != null">
							<s:property value="pagoSubsecuente.numExhibicion" />
						</s:if>
						<s:else>
							0
						</s:else>
					</div>
				</td>
				<td align="right">
					<div id="pagoSubsecuente.importeFormat_<s:property value='%{#stat.index}'/>">
						<s:if test="pagoSubsecuente.importeFormat != null">
							<s:property value="pagoSubsecuente.importeFormat" />
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</div>
				</td>
			</tr>
		</s:iterator>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4"><s:text name="midas.suscripcion.cotizacion.agentes.estimacionFormaPago" /></th>
			</tr>
		</tfoot>
	</table>
</div>