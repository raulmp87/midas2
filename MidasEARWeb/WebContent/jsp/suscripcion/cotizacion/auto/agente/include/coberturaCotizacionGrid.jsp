<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<table class="table table-hover table-striped">
	<thead class="form-group" width="20%"  style="font-size:10px;">
		<tr height="0px">
			<th><s:text name="midas.suscripcion.cotizacion.inciso.claveObligatoriedad" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.descripcion" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.sumaAsegurada" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.porcDeducible" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.primaNeta" /></th>
		</tr>
	</thead>
	<tbody><!-- AUTOS INDIVIDUAL -->
	<s:iterator value="coberturaCotizacionList" status="stat">
		<tr  height="0px" >
			<td style="width:8%">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.numeroInciso" value="%{id.numeroInciso}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToSeccion" value="%{id.idToSeccion}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCobertura" value="%{id.idToCobertura}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCotizacion" value="%{id.idToCotizacion}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveObligatoriedad" value="%{claveObligatoriedad}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].idTcSubramo" value="%{idTcSubramo}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCoaseguro" value="%{valorCoaseguro}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCuota" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeCoaseguro" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutCoaseguro" value="0" />			
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutDeducible" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].numeroAgrupacion" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].consultaAsegurada" value="%{consultaAsegurada}" />
				<s:if test="claveContrato==1">
					 <s:set name="readOnly" value="false"/>
				</s:if>
				<s:else>
					 <s:set name="readOnly" value="true"/>
				</s:else>
				<s:if test="claveContrato==1 && claveObligatoriedad==0" >
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="true"/>
					<s:checkbox id="claveContratoBoolean_%{#stat.index}"  name="coberturaCotizacionList[%{#stat.index}].claveContrato" checked="checked" 
					disabled="true" class="form-control" />
				</s:if>	
				<s:else>
					<s:checkbox id="claveContratoBoolean_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="%{claveContrato}" fieldValue="true" 
					onclick="habilitaRegistroCobertura(%{#stat.index});mostrarDaniosCargaInclude(%{#stat.index});"
					disabled="%{#disabledConsulta}" cssClass="form-control-datos-generales form-control"
					/>			
				</s:else>
			</td>
			<td style="width:20%;font-size:10px;"><s:property value="coberturaSeccionDTO.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true" /></td>
			<td style="width:30%">
				<s:if test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 0">
					<s:if test="valorSumaAseguradaMax != 0.0 && valorSumaAseguradaMin!= 0.0">
						<s:if test="coberturaSeccionDTO.coberturaDTO.idToCobertura==2650||coberturaSeccionDTO.coberturaDTO.idToCobertura==3020||coberturaSeccionDTO.coberturaDTO.idToCobertura==2860||coberturaSeccionDTO.coberturaDTO.idToCobertura==4821">
							<div class="form-group" style="font-size:10px;">FFGG
								<label class="small"><s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/></label>
								<div class="input-group">
									<s:if test="sumasAseguradas != null && !sumasAseguradas.isEmpty()">
										<s:select id="idValDiasSalario"
											name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
										  	headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
										  	list="sumasAseguradas" listKey="valorSumaAsegurada.intValue()" listValue="valorSumaAsegurada.intValue()"
										  	onchange="validaDisable(this, %{#stat.index});"
										  	cssClass="form-control-datos-generales form-control"
										/>
									</s:if>
								  	<s:else>
										<s:select id="idValDiasSalario"
											name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
										  	headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
										  	list="%{diasSalario}"
										  	onchange="validaDisable(this, %{#stat.index});"
										  	cssClass="form-control-datos-generales form-control"
										/>
									</s:else>
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<s:hidden id="numeroAcientos" value="%{numAcientos}"/>		
							<s:hidden id="valDiasSalario" value="%{getText(diasSalarioMinimo)}"  />
							<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" id="idSumaAsegurada" value="%{getText(valorSumaAsegurada)}"  />	
				 		</s:if>
				 		<s:else>
				 		<s:if test="sumasAseguradas != null && !sumasAseguradas.isEmpty()">
				 					<s:select id="valorSumaAseguradaStr_%{#stat.index}" 
										name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada"
										value="valorSumaAsegurada"
										list="sumasAseguradas" listKey="valorSumaAsegurada" listValue="%{getText('struts.money.format',{valorSumaAsegurada})}"  
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
										onchange="recalcularCotizador(false);"
										disabled="%{#disabledConsulta}"
								  		cssClass="form-control-datos-generales form-control" 
									/>
				 			</s:if>
				 			<s:else>				
							<div class="form-group">
								<label class="small">
									*Entre <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true"/> y <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />
								</label>
								<div class="input-group">
								<span class="input-group-addon">$</span>
									<s:textfield 
										disabled="%{#disabledConsulta}" id ="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="form-control-datos-generales form-control" cssStyle="border-radius: 3px 0px 0px 3px;" onkeypress="return soloNumerosM2(this, event, true,false);" onkeyup=" aplicaFormatoKeyUp(%{#stat.index});" onblur="validaRangoSumaAsegurada(this,%{valorSumaAseguradaMin},%{valorSumaAseguradaMax},%{valorSumaAsegurada},%{#stat.index});recalcularCotizador(false);" 
										onchange="recalcularCotizador(false);" readonly="#readOnly"/>
								</div>
							</div>
							</s:else>
							<s:hidden id="valorSumaAseguradaMax_%{#stat.index}" value = "%{getText('struts.money.format',{valorSumaAseguradaMax})}"/>
							<s:hidden id="valorSumaAseguradaMin_%{#stat.index}" value = "%{getText('struts.money.format',{valorSumaAseguradaMin})}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:if test="coberturaSeccionDTO.coberturaDTO.idToCobertura==2650||coberturaSeccionDTO.coberturaDTO.idToCobertura==3020||coberturaSeccionDTO.coberturaDTO.idToCobertura==2860||coberturaSeccionDTO.coberturaDTO.idToCobertura==4821">
							<div class="form-group">
								<label class="small"><s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/></label>
								<div class="input-group">
									<s:select id="idValDiasSalario"
										name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
									  	headerKey="-1"
									  	headerValue="%{getText('midas.general.seleccione')}"
									  	list="%{diasSalario}"
									  	onchange="validaDisable(this, %{#stat.index});recalcularCotizador(false);"
									  	cssClass="form-control-datos-generales form-control"/>
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<s:hidden id="numeroAcientos" value="%{numAcientos}"/>		
							<s:hidden id="valDiasSalario" value="%{getText(diasSalarioMinimo)}"  />
							<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" id="idSumaAsegurada" value="%{getText(valorSumaAsegurada)}"  />					
						</s:if>
						<s:else>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<s:textfield 
									disabled="%{#disabledConsulta}" id="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="form-control-datos-generales form-control" onkeypress="return soloNumerosM2(this, event, true,false);" onkeyup="aplicaFormatoKeyUp(%{#stat.index});" onchange="recalcularCotizador(false);" onblur="aplicaRedondeo(%{#stat.index});recalcularCotizador(false);" 
									readonly="#readOnly"/>	
							</div>
						</s:else>
					</s:else>
				</s:if>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 1">
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
					<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 2">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
					<s:textfield disabled="%{#disabledConsulta}" id ="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}" cssClass=" form-control-datos-generales form-control" onkeypress="return soloNumerosM2(this, event, true,false);" onkeyup="aplicaFormatoKeyUp(%{#stat.index});" onblur="aplicaRedondeo(%{#stat.index})" onchange="recalcularCotizador(false);" readonly="#readOnly"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 9">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 3">
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
					<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 10">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorCaratula"/>
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<s:textfield disabled="%{#disabledConsulta}" id ="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="form-control" cssStyle="border-radius: 0px 4px 4px 0px;" onkeyup="aplicaFormatoKeyUp(%{#stat.index});" onblur="aplicaRedondeo(%{#stat.index})" onchange="recalcularCotizador(false);" readonly="true"/>
					</div>
				</s:elseif>
			</td>
			<td style="width:30%; font-size:10px;">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveTipoDeducible" 
						 value="%{coberturaSeccionDTO.coberturaDTO.claveTipoDeducible}">
				</s:hidden>							
				<s:if test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "0"'>
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0" />
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
				</s:if>
				<div class="form-group">
					<label class="small"><s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/></label>
						<div class="input-group">
							<s:elseif test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "1"'>
									<s:if test="deducibles.size>0">
									<s:select id="porcentajeDeducible_%{#stat.index}"
										list="%{deducibles}"
										disabled="%{#disabledConsulta}"
										name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible"
										onchange="validaDisable(this, %{#stat.index});validaDeducible(this);"
										listKey="valorDeducible" listValue="valorDeducible"
										cssClass="form-control-datos-generales form-control mandatory" cssStyle="border-radius: 4px 0px 0px 4px;"/>
									<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
									<span class="input-group-addon">%</span>
									</s:if>
							</s:elseif>
							<s:else>
								<s:if test="deducibles.size>0">
									<s:select id="valorDeducible_%{#stat.index}"
										list="%{deducibles}"
										disabled="%{#disabledConsulta}"
										name="coberturaCotizacionList[%{#stat.index}].valorDeducible"
										onchange="validaDisable(this, %{#stat.index});validaDeducible(this)"
										listKey="valorDeducible" listValue="valorDeducible"
										cssClass="form-control-datos-generales form-control mandatory" cssStyle="border-radius: 4px 0px 0px 4px;"/>
									<s:hidden id="coberturaCotizacionList%{#stat.index}porcentajeDeducible" name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0"  />
									<span class="input-group-addon">%</span>
								</s:if>
							</s:else>
					</div>
				</div>		
			</td>
			<td style="width:10%; font-size:10px;">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorPrimaNeta"  value="%{valorPrimaNeta}"/>
				<div id="coberturaCotizacionList.valorPrimaNeta_<s:property value='%{#stat.index}' />">
					<s:property value="%{getText('struts.money.format',{valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/>	
				</div>
				
			</td>
			</tr>
	</s:iterator>
	</tbody>
</table>
<s:hidden name="configuracionIdEnCoberturas" id="configuracionIdEnCoberturas" />