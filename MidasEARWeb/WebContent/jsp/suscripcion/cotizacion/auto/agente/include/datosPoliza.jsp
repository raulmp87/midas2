<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<fieldset>             
  <s:if test="cotizacion.solicitudDTO.claveTipoPersona==2">
		<div class="col-sm-4 form-group"  id="nombreContratanteDiv">
			<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.nombreProspecto" />:</label>
			<s:textfield id="nombreContratante" name="cotizacion.nombreContratante" cssClass="form-control-datos-generales mandatory" />
			<label class="control-label error-label">Escribe el nombre del prospecto para continuar</label>
		</div>
	</s:if>
	<s:else>
		<s:hidden name="cotizacion.nombreContratante" id="nombreContratante"/>
		<div class="col-sm-4 form-group" id="nombreProspectoDiv" style="font-size:10px;">
			<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.nombreProspecto"/>:</label>
			<s:textfield id="nombreProspecto" cssClass="form-control-datos-generales mandatory" onchange="actualizaNombreContratante();"/>
			<label class="control-label error-label">Escribe el nombre del prospecto para continuar</label>
		</div>
		<div id="aPaternoDiv" class="col-sm-4 form-group" style="font-size:10px;" >
			<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.apellidoPaterno" />:</label>
			<s:textfield id="aPaterno" cssClass="form-control-datos-generales mandatory" onchange="actualizaNombreContratante()"/>
			<label class="control-label error-label">Escribe el apellido paterno del prospecto para continuar</label>
		</div>
		<div  id="aMaternoDiv"  class="col-sm-4 form-group" style="font-size:10px;" >
			<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.apellidoMaterno" />:</label>
			<s:textfield id="aMaterno"  cssClass="form-control-datos-generales mandatory"  onchange="actualizaNombreContratante()"/>
			<label class="control-label error-label">Escribe el apellido materno del prospecto para continuar</label>
		</div>
	</s:else>
	
	
	<div id="idNegocioDiv" class="form-group col-md-4" style="font-size:10px;">
		<s:hidden name="midas.suscripcion.cotizacion.agentes.negocio.tipoPersona" id="tipoPersona" />
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.negocio" />:</label>
		<s:if test="configuracionId == null">
			<s:select
			id="idNegocio" name="cotizacion.solicitudDTO.negocio.idToNegocio"
			headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			list="negocioAgenteList" required="#requiredField"
			listKey="idToNegocio" listValue="descripcionNegocio"
      		value="cotizacion.solicitudDTO.negocio.idToNegocio"
			cssClass="form-control-datos-generales mandatory" 
			onChange="refrescarConfiguracionVisibilidad();
			cargaTiposVigenciaNegocio(this.value);"/>
			
		<label class="control-label error-label ">Selecciona el negocio para continuar</label>
		</s:if>
		<s:else>
			<s:select
			id="idNegocio"
			headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			list="negocioAgenteList"
			listKey="idToNegocio" listValue="descripcionNegocio"
      		value="cotizacion.solicitudDTO.negocio.idToNegocio"
      		disabled="true"
			cssClass="form-control-datos-generales "/>
			<s:hidden name="cotizacion.solicitudDTO.negocio.idToNegocio" />
		</s:else>
	</div>
	<div id="idProductoDiv"  class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.producto" />: </label>
		<s:select
			id="idProducto" name="cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto" 
			list="negocioProductoList"
			cssClass="form-control-datos-generales mandatory"
			value="cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto"/>
		<label class="control-label error-label">Selecciona el producto para continuar</label>
	</div>
	<div id="idTipoPolizaDiv" class="form-group col-md-4" style="font-size:10px;">
		<label class="small "><s:text name="midas.suscripcion.cotizacion.agentes.tipoPoliza" />: </label>
		<s:select
			id="idTipoPoliza" name="cotizacion.negocioTipoPoliza.idToNegTipoPoliza" 
			list="negocioTipoPolizaList"
			value="cotizacion.negocioTipoPoliza.idToNegTipoPoliza"
			cssClass="form-control-datos-generales mandatory"/>
		<label class="control-label error-label">Selecciona el tipo de póliza para continuar</label>
	</div>
	<div id="tipoVigenciaDiv" class="form-group col-md-4" style="font-size:10px;">
		<label class="small "><s:text name="midas.cotizacion.tipovigencia" />: </label>
		<s:select
			id="idTipoVigencia" name="cotizacion.negocioTipoPoliza.tiposvigencia" 
			list="tiposVigenciaList"
			value="vigenciaDefault"
			headerKey=""
			headerValue="%{getText('midas.general.seleccione')}"
			cssClass="form-control-datos-generales"
			onchange="actualizarFechas(this.value);"/>
			<label class="control-label error-label">Selecciona el tipo de vigencia para continuar</label>
	</div>
	<div id="inivigDiv" class="form-group has-feedback col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.inicioVigencia" />: </label>
		<s:if test="configuracionId == null || configuracionId == \"\" " >
		<s:textfield
		id="inivig" name="cotizacion.fechaInicioVigencia" cssClass="form-control-datos-generales datepicker mandatory" onChange="validaFechasCotizacion();"/>
		</s:if>
		<s:else>
		<s:textfield
		id="inivig" name="cotizacion.fechaInicioVigencia" cssClass="form-control-datos-generales datepicker mandatory" onChange="validarInicioVigencia();actualizarFinVigencia();validaFechasCotizacion();"/>
		</s:else>
		<i class="glyphicon glyphicon-calendar form-control-feedback"></i>
		<label class="control-label error-label">Selecciona el inicio de vigencia para continuar</label>
	</div>
	<div id="finvigDiv" class="form-group has-feedback  col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.finVigencia" />: </label>
		<s:if test="configuracionId == null || configuracionId == \"\" " >
		<s:textfield 
		id="finvig" name="cotizacion.fechaFinVigencia" cssClass="form-control-datos-generales datepicker mandatory" onChange="validaFechasCotizacion()"/>
		</s:if>
		<s:else>
		<s:textfield 
		id="finvig" name="cotizacion.fechaFinVigencia" cssClass="form-control-datos-generales mandatory" readonly="true"/>
		</s:else>
		<i class="glyphicon glyphicon-calendar form-control-feedback"></i>
		<label class="control-label error-label">Selecciona el fin de vigencia para continuar</label>
	</div>
	
	<div id="telefonoContratanteDiv" class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.telefonoProspecto" />:</label>
		<s:textfield id="telefonoContratante" name="cotizacion.telefonoContratante" cssClass="form-control-datos-generales mandatory" maxlength="10"/>
		<label class="control-label error-label"><s:text name="midas.suscripcion.cotizacion.agentes.telefonoProspectoError" /></label>
	</div>
	<div id="correoContratanteDiv" class="form-group col-md-4" style="font-size:10px;">
		<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.correoProspecto" />:</label>
		<s:textfield id="correoContratante" name="cotizacion.correoContratante" cssClass="form-control-datos-generales mandatory" onblur="validarCorreo()" />
		<label class="control-label error-label">Escribe el correo del prospecto para continuar</label>
	</div>
	
</fieldset>
<script type="text/javascript">
jQuery(document).ready(function(){
	var dataDefault = '<s:property value="vigenciaDefault" />';
	if(dataDefault == -1){
		 dataDefault = "";
	}
	if(dataDefault == "" && '<s:property value="tiposVigenciaList.size" />' == 0){	
		deshabilitarVigencias(true);		 
	} else {
		deshabilitarVigencias(false);		
	}
	
	selectValorDefault('idTipoVigencia', dataDefault);
	var diasVigencia = document.getElementById("idTipoVigencia").value;
	actualizarFechas(diasVigencia == "" ? "365" : diasVigencia);
});
</script>