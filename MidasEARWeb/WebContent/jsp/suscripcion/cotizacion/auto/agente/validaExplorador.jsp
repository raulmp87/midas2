<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> Afirme - Cotizador Agentes</title>
		<link href="<html:rewrite page="/css/agente/bootstrap.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/agente/datepicker3.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/agente/jquery-ui.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/agente/style.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/agentes/fancybox/2.1.5/jquery.fancybox.css"/>"  rel="stylesheet" type="text/css">	
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<script type="text/javascript" >
			var internetExplorer = 'Microsoft Internet Explorer';
			var MSIE = "MSIE";
		function cargarPantalla(){
			if(compatibleIE()){
				document.getElementById("compatilbeExplorador").value='true';
				document.getElementById("fromValidaExplore").submit();
			}
		}
			/**
			 * Valida la compatibilidad del Internet Explorer 
			 *  y el modo vista compatibilidad
			 * @returns {Boolean}
			 */
			function compatibleIE() {
			
			    var browserName = navigator.appName;
			
			    if (browserName == internetExplorer) {
			
			        var IEVersion = getInternetExplorerVersion();
			        var IEDocMode = document.documentMode;
			        var IECompatibilityMode = document.compatMode;
			
			        if (IEDocMode != undefined) {
			
			            if (IEVersion >= 9 && IECompatibilityMode == "CSS1Compat") {
			                return true;
			            }
			
			            if (IEDocMode <= 9) {
							document.getElementById("errorCargaExplorerMode").style.display="inline";
							document.getElementById("errorCargaExplorer").style.display="none";
							return false;
			            }else{
							document.getElementById("errorCargaExplorerMode").style.display="inline";
							document.getElementById("errorCargaExplorer").style.display="none";
							return false;
						}
			        }else{
						document.getElementById("errorCargaExplorer").style.display="inline";
						document.getElementById("errorCargaExplorerMode").style.display="none";
						return false;
			        }
			    }else{
			    	return true;
			    }
			}
			
			/**
			 * Regresa la version del explorador("solo para explorer")
			 * @returns
			 */
			function getInternetExplorerVersion() {
			    var myNav = navigator.userAgent;
			    return (myNav.indexOf(MSIE) != -1) ? parseInt(myNav.split(MSIE)[1]) : false;
			}
		</script>
	</head>
	<body onload="cargarPantalla()">
	<div class="container">
	  <div style="float: left; width: 100%; position: relative; padding-top: 10px;">
			<span class="logo_cotizador_agentes" style=" position: relative; width: 600px; height: 120px; float: left;"></span>
			<div class="pull-right" style="height: 120px;">
				<div style=" top: 60%; float: right; position: relative;">
					<label><s:property value="nameUser" /></label>
					<br>
					<s:if test="idToCotizacion!=null">
						<strong class="title">
						<s:text 
							name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />: 
							<s:property value="idToCotizacion" /></strong>
					</s:if>
				</div>
			</div>
		</div>
	</div>
	<br/>
		<div id="errorCargaExplorer" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><strong>Explorador no Compatible:</strong><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorer" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="errorCargaExplorerMode" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorerModo" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<s:form id="fromValidaExplore">
			<s:hidden name="compatilbeExplorador" id="compatilbeExplorador"/>
			<s:hidden name="idToCotizacion" id="idToCotizacion"/>
			<s:hidden name="numeroInciso" id="numeroInciso"/>
			<s:hidden name="configuracionId" id="configuracionId"/>
			<s:hidden name="clati" id="clati"/>			
			<s:hidden name="forma" id="forma"/>
		</s:form>
	</body>
</html>