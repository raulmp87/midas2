<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none"></div>
<script type="text/javascript">
	(function() {
		var vin = '<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>';
		var cotizacionId = '<s:property value="idToCotizacion"/>';
		var numeroFolio =  '<s:property value="cotizacion.folio" />';
		var correoObligatorio =  '<s:property value="correoObligatorio" />';

		if(vin != null && vin != ""){
			if(obtenerAlertas(cotizacionId,vin) == "si"){
				$("#mensajeAlertas").css("display","block");
			}
		}	
	})();
</script>
<style type="text/css">
	
	.form-control-feedback-cp {
    width: 13px;
    margin-left: 220px;
    line-height: 30px;
    right: 300px;
   
}
</style>
<s:form id="fromDatosGenerales">
	<s:hidden name="forma" id="jspForm"/>
	<s:hidden name="nameUser" id="nombreUsuario"/>
	<s:hidden name="incisoCotizacion.id.idToCotizacion" id="idToCotizacion" />
	<s:hidden name ="incisoCotizacion.id.numeroInciso" id="numeroInciso" />
	<s:hidden name="idToCotizacion" id="0idToCotizacion" />
	<s:hidden name ="numeroInciso" id="0numeroInciso" />
	<s:hidden name="cotizacion.idMoneda" id="idMonedaName"/>
	<s:hidden name="idProductoBase" id="idProductoBase"/>
	<s:hidden name="idTipoPolizaBase" id="idTipoPolizaBase"/>
	<s:hidden name="cotizacion.solicitudDTO.claveTipoPersona" id="claveTipoPersona"/>
	<s:hidden name="incisoCotizacion.incisoAutoCot.tipoUsoId" id="0idTipoUsoBase"/>
	<s:hidden name="idNegocioSeccion" id="0idNegocioSeccion"/>
	<s:hidden name="cotizacion.idFormaPago" id="cotizacion.idFormaPago"/>
	<s:hidden name="cotizacion.porcentajebonifcomision" id="cotizacion.porcentajebonifcomision"/>
	<s:hidden name="cotizacion.porcentajeIva" id="cotizacion.porcentajeIva"/>
	<s:hidden name="cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza" id="cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza"/>
	<s:hidden name ="incisoCotizacion.cotizacionDTO.claveEstatus" id="claveEstatus"/>
	<s:hidden name="estiloSeleccionado" id="0estiloSeleccionado"/>
	<s:hidden name="incisoCotizacion.incisoAutoCot.estiloId" id="0idEstilo"/>
	<s:hidden name="cotizacion.primaTarifa" id="0primaTarifa"/>
	<s:hidden name="cotizacion.tipoCotizacion" id="0tipoCotizacion"/>
	<s:hidden name="incisoCotizacion.incisoAutoCot.idAgrupadorPasajeros" id="0idAgrupadorPasajeros"/>
	<s:hidden name='isJson' id="isJson" value="false"/>
	<s:hidden name="compatilbeExplorador" id="compatilbeExplorador"/>
	<s:hidden name="cuentaConAgente" id="cuentaConAgente"/>
	<s:hidden name="isAgente" id="isAgente"/>
	<s:hidden name="nextTap" id="nextTap" />	
	<s:hidden name="nombreContr" id="nombreContr"/>
	<s:hidden name="apellidoPatCont" id="apellidoPatCont"/>
	<s:hidden name="apellidoMatCont" id="apellidoMatCont"/>
	<s:hidden name="correoObligatorio" id="correoObligatorio" />
	<s:hidden name="configuracionId" id="configuracionId" />
	<s:hidden id="clati" name="clati"/>	
	<s:if test="idToCotizacion=!null">
		<s:hidden name='updateSolicitud' id="updateSolicitud" value="true"/>
	</s:if>
	<s:else>
		<s:hidden name='updateSolicitud' id="updateSolicitud" value="false"/>
	</s:else>
	<s:if test="cotizacion.tipoCotizacion=='TCSP'">
		<s:hidden name='cotizacion.fechaInicioVigencia'/>
		<s:hidden name='cotizacion.fechaFinVigencia'/>
		<s:hidden name='cotizacion.solicitudDTO.negocio.idToNegocio'/>
		<s:hidden name='cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto'/>
		<s:hidden name='cotizacion.negocioTipoPoliza.idToNegTipoPoliza' />
		<s:hidden name='incisoCotizacion.incisoAutoCot.estadoId'/>
		<s:hidden name='incisoCotizacion.incisoAutoCot.municipioId'/>
		<s:hidden name='vehiculo.idNegocioSeccion' value='%{incisoCotizacion.incisoAutoCot.negocioSeccionId}'/>
	</s:if>
	<div class="row">
		<s:if test="cuentaConAgente">
			<div class="col-md-12">
				<div class="container">
				  <p><span class="glyphicon glyphicon-info-sign"></span> <s:text name="midas.suscripcion.cotizacion.agentes.vehiculo.descripcion" /></p>
				</div>
			</div>
		</s:if>	
		<div <s:if test="!cuentaConAgente">class="col-md-6"</s:if><s:else>class="col-md-12"</s:else>>
			<div class="well">
				<fieldset>
					<legend  style="font-size:12px;">
						<s:if test="!cuentaConAgente">
							<s:text name="midas.suscripcion.cotizacion.agentes.seleccionarAgente"/>
						</s:if>
						<s:else>
							<s:text name="midas.suscripcion.cotizacion.agente"/>
							: <s:property value="agente.persona.nombreCompleto"/> - 
							<s:if test="isAgente">
								<s:property value="cotizacion.solicitudDTO.agente.idAgente"/>
							</s:if>
							<s:else>
								<s:property value="cotizacion.solicitudDTO.codigoAgente"/>
							</s:else>
							<s:if test="!folioReexpedible">
								<s:if test="configuracionId == null">
									<span class="pull-right">
										<s:if test="cotizacion.tipoCotizacion=='TCSP'">
											<a href="#" id="btnToggle">
												<s:text name="Cambiar Agente" />
											</a>
										</s:if>
										<s:elseif test="!isAgente">
											<a href="#" id="btnToggle">
												<s:text name="Cambiar Agente" />
											</a>
										</s:elseif>
									</span>
								</s:if>
							</s:if>							
						</s:else>
					</legend>
					<div id="seleccionarAgente" <s:if test="!cuentaConAgente">class="col-md-12"</s:if><s:else>class="col-md-6" style="display:none"</s:else>>
						<div class="form-group"  style="font-size:10px;">
							<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.seleccionarAgente"/>:</label>
							<s:textfield id="descripcionBusquedaAgente" cssClass="form-control-datos-generales" />
							<s:hidden name="cotizacion.solicitudDTO.codigoAgente" id="idAgente" cssClass="mandatory"/>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<s:if test="cuentaConAgente">
		<div class="col-md-12"  id="idDatosPoliza">
			<div class="well">
				<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/datosPoliza.jsp"/>
			</div>
		</div>
		<div class="col-md-12" id="divZonaCirculacion">
			<div class="well">
				<fieldset>
					<fieldset>
					 <div class="col-sm-4 has-feedback form-group" style="font-size:10px;" id="codigoPostalDiv">
						<label class="small"><s:text name="C&oacutedigo postal" />:</label>
						<s:textfield id="codigoPostal" maxlength="5" name="incisoCotizacion.incisoAutoCot.codigoPostal" cssClass="form-control-datos-generales mandatory" onblur="onChangeCodigoPostalAgenteCP(this.value);"/>
						<label class="control-label error-label">Escribe c&oacute;digo postal para continuar</label>
						<i class="glyphicon glyphicon-remove form-control-feedback" onclick="limpiarcodigoPostal('codigoPostal');" style="cursor: pointer;"></i>
						
						
		         </div>
					<div class="form-group col-sm-4" style="font-size:10px;" id="idEstadoDiv">
						<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.oestado"/>:</label>
						<s:select				
						    labelposition="top"
							id="idEstado" name="incisoCotizacion.incisoAutoCot.estadoId"
							value="incisoCotizacion.incisoAutoCot.estadoId"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							list="estadoMap"
							cssClass="form-control-datos-generales mandatory"
							onchange="onChangeEstado('idMunicipio', 'idEstado');"/>
							<label class="control-label error-label">Selecciona la zona de circulación para continuar</label>
					</div>
					<div class="form-group col-sm-4" style="font-size:10px;" id="idMunicipioDiv" >
							<label class="small"><s:text name="midas.suscripcion.cotizacion.agentes.municipio"/>:</label>
							<s:select				
						    labelposition="top"
							id="idMunicipio" name="incisoCotizacion.incisoAutoCot.municipioId"
							value="incisoCotizacion.incisoAutoCot.municipioId"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							list="municipioMap"
							cssClass="form-control-datos-generales mandatory"/>
							<label class="control-label error-label">Selecciona el municipio para continuar</label>
		          </div>
		         
		          </fieldset>
				  <div class="" id="divDV">			
						<div id="divDatosVehiculo">
							<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/datosVehiculo.jsp"/>
						</div>
				  </div>		
				</fieldset>
			</div>
		</div>
		<div class="col-md-12">
			<div style="display: none;">
				<div class="well">
					<a href="#divModalCaracteristicas" id="featuresLink">#</a>
				</div>
			</div>
		</div>
		</s:if>		
    	<s:if test="cuentaConAgente">
		<div class="row">
			<div class="col-md-12">
				<a href="javascript: void(0);"  onClick="cargarCaracterisitas(false)">
					<s:text name="midas.suscripcion.cotizacion.agentes.mostrarCOpcional" />
				</a>
			</div>
		
			<div class="col-md-12" align="right">
				<button id="nextBtnDG" type="button" class="btn btn-success">
					<s:text name="midas.suscripcion.cotizacion.agentes.siguientePaso"  /><span class="glyphicon glyphicon-chevron-right"></span>
				</button>
			</div>
		</div>
		</s:if>
	<div class="container" id="divModalCaracteristicas"  style="display: none;width: 600px;height: 500px;"></div>
</s:form>
<script type="text/javascript">
esconderCamposPaso(1, negocio);
</script>