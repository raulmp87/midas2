<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<html:rewrite page="/css/agente/bootstrap.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/agente/datepicker3.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/agente/jquery-ui.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/agente/style.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/agentes/fancybox/2.1.5/jquery.fancybox.css"/>"  rel="stylesheet" type="text/css">	
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<style type="text/css">
	body{
		background-image: none;
	}
	.input-group .form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
 
	  }
	.input-group-addon{
	padding: 6px 7px;
	font-size: 11px;
	
	
	
	}
	
	.ui-autocomplete-loading { 
		background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat;
	}

	.ui-autocomplete {
		max-height: 120px;
		overflow-y: auto;
		overflow-x: hidden;
		padding-right: 10px;
	}
	
	legend{
		padding-bottom: 5px;
	}
	
	.wwgrp {
		padding: 0;
	}
	.table.table {
		font-size: 11px;
	}

	.radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"]{
		margin-left:0;
	}

	input[type="radio"], input[type="checkbox"]{
		width:auto;
	}

	.form-control{
		text-transform: uppercase;
		height: 25px;
		font-size: 9px;
	}

	.title{
		color: #00a94f;
	}
	

	.logo_cotizador_agentes {
		
		padding: 0px 400px 88px 0px;
		background-size: 400px;
		border-radius: 5px;
		background-repeat: no-repeat;
	}
	
	.form-group {
	  margin-bottom: 1px;
	  top:18;
	  width:250;
	 
	}
	
	.btn {	
		margin:15px;
		padding: 5px;
		border:-1px;
		font-size: 13px;
	}
	
	.form-control-feedback{
		width: 250px;
		top:18px;
		right: 0px;
	}
	.form-control-feedback-prueba{
		width: 250px;
		top:-18px;
		right: -270px;
	}
	.form-control-feedback-prueba-yes {
    width: 250px;
    top: -18px;
    right: -310px;
    }
	
	
		.form-control-feedback-cliente{
		width: 250px;
		top:-23px;
		right: -317px;
	}
	
	
	
	.form-control-generales-override-size{
		width: 100%;
		height: 25px;
		padding: 6px 12px;
		font-size: 9px;
	}
	
	.form-control-datos-generales{
	  display: block;
	  width: 70%;
	  height: 25px;
	  padding: 6px 12px;
	  font-size: 9px;
	  line-height: 1.42857143;
	  color: #555;
	  text-transform: uppercase;
	  background-color: #fff;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
	       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
	          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
	}


</style>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/jquery-2.1.1.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/jquery-ui/jquery-ui.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/bootstrap.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/bootstrap-datepicker.js"/>"></script>

<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/date-es-MX.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/agente/main.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/agentes/fancybox/2.1.5/jquery.fancybox.js"/>"></script>

<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/agentes/cotizadoragentes.js"/>"></script>

<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/vehicular.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_srnd.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_nxml.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/generadorCURPRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/componente/componentedireccion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/suscripcion/cotizacion/auto/agentes/creacionCurpAgentes.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/midas2/jQuery/plugins/jquery.formatCurrency-1.4.0.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/datosRiesgo/datosRiesgo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/datosRiesgo/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/operacioensSapAmis/sapAmisAccionesAlertas.js'/>"></script>
<script type="text/javascript">
	var urlMostrarContenedor = '<s:url action="mostrarContenedor" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlMostrarCotizadorLigaAgente = '<s:url action="mostrarCotizadorLigaAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlBusquedaAgentes = '<s:url action="buscarAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlCargarlistas = '<s:url action="cargarlistas" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlCargarCaracteristicas = '<s:url action="mostrarOtrasCaract" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlDefinirOtrasCaract = '<s:url action="definirOtrasCaract" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlDatosVehiculo = '<s:url action="mostrarDatosVehiculo" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlCargaFlotilla = '<s:url action="consultaFlotilla" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlBusquedaEstilos = '<s:url action="buscarEstilo" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlSaveCotizacion = '<s:url action="saveCotizacion" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlValidaFechasCotizacion = '<s:url action="validarFechasCotizacion" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlValorDefaultCotizdor = '<s:url action="valorDefaultCotizdor" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlValidarInicioVigencia = '<s:url action="validarInicioVigencia" namespace="/suscripcion/cotizacion/auto/agentes"/>';

	var urlCargarResumenTotalCotizacion = '<s:url action="mostrarResumenTotalesCotizacionAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlCargarResumenTotalCotizacionAjax = '<s:url action="actualizarResumen" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlVerEsquemaPagoAgente = '<s:url action="verEsquemaPagoAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlVerEsquemaPagoAgenteAjax = '<s:url action="actualizarEsquema" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlVerDetalleIncisoAgente = '<s:url action="verDetalleIncisoAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlVerDetalleIncisoAgenteAjax = '<s:url action="actualizarCoberturas" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlImpresionModal = "<s:url action='mostrarContenedorImpresionAgente' namespace='/impresiones/componente' />";
	var urlImpresion = '<s:url action="generarImpresionAgente" namespace="/impresiones/componente" />';
	var urlVerIgualarPrimasAgente = '<s:url action="verIgualarPrimasAgentes" namespace="/suscripcion/cotizacion/auto/cotizadoragente"/>';
	var urlIgualarPrimasAgente =  '<s:url action="igualarPrimasAgentes" namespace="/suscripcion/cotizacion/auto/cotizadoragente" />';
	var urlRestaurarPrima = '<s:url action="restaurarPrimaCotizacionAgentes" namespace="/suscripcion/cotizacion/auto/cotizadoragente" />';
	var urlViewRiesgos = '<s:url action="cargaControlDinamicoRiesgoAgente" namespace="/suscripcion/cotizacion/auto/complementar/datosRiesgo" />';
	var urlSaveRiesgos = '<s:url action="guardarRiesgosCotizacionAgente" namespace="/suscripcion/cotizacion/auto/complementar/inciso" />';

	var urlViewSearchCliente = "<s:url action='mostrarPantallaConsulta' namespace='/catalogoCliente'/>";
	var urlSearchCliente = "<s:url action='filtrarClientes' namespace='/catalogoCliente'/>";
	var urlSearchClientebyId = "<s:url action='loadById' namespace='/catalogoCliente'/>";
	var urlSaveAsegurado = "<s:url action='actualizarDatosAseguradosAgente' namespace='/suscripcion/cotizacion/auto/agentes'/>"; 
	var urlLoadFormCliente = "<s:url action='mostrarDetalleClienteAgente' namespace='/catalogoCliente'/>";
	var urlSaveCliente = "<s:url action='guardarCliente' namespace='/catalogoCliente'/>";
	var urlListaDocumentos = '<s:url action="getListaDocumentos" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlGetLigaFortimax = '<s:url action="getURLFortimax" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlSaveDatosComplementarios = '<s:url action="saveDatosComplementarios" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlfindResponsable = '<s:url action="mostrarListadoPersona" namespace="/fuerzaVenta/persona"/>';
	var listarFiltradoResponsablePath = '<s:url action="cargarListadoPersona" namespace="/fuerzaVenta/persona"/>';

	var urlLoadFromCobranza = '<s:url action="mostrar" namespace="/suscripcion/cotizacion/auto/cabranza"/>';
	var urlComplementInfInciso = '<s:url action="complementarInfoCuenta" namespace="/suscripcion/cotizacion/auto/cabranza"/>';
	var urlComplementInfIncisoTitular = '<s:url action="complementarInfoTitular" namespace="/suscripcion/cotizacion/auto/cabranza"/>';
	var urlSaveCobranza = '<s:url action="guardarCobranza" namespace="/suscripcion/cotizacion/auto/cabranza"/>';
	var urlEmitirAgenteCotizacion = '<s:url action="emitirAgenteNew" namespace="/suscripcion/emision"/>';
	var urlValidarDatosAgente = '<s:url action="validarDatosComplementariosAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	var urlActualizaEstatusTerminado = '<s:url action="terminar" namespace="/suscripcion/cotizacion/auto/terminarcotizacion"/>';
	
	var urlGuardarSolicitudes = '<s:url action="enviarSolicitudes" namespace="/suscripcion/cotizacion/auto/terminarcotizacion"/>';
	var urlMostrarVehicular = '<s:url action="mostrarInfVehicular" namespace="/suscripcion/cotizacion/auto/agentes"/>';
	
	var urlFinalizarCotizacionLigaAgente = '<s:url action="finalizarCotizacionLigaAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';

	var urlCotizacionAgenteCobertura = "<s:url action='generaCotizacionAgenteCobertura' namespace='/suscripcion/cotizacion/auto/cotizadoragente'/>";
</script>