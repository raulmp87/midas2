<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp"/>
		<title> Afirme - Cotizador Agentes</title>
	</head>
	<input type="hidden" id="noSerieVin"/>	
	<s:form id="finalizaCotizacionForm">
	<s:hidden value="-3" id="jspForm"/>
	<s:hidden id="clati" name="clati"/>
	</s:form>
	<div id="backMSN" class="modal in fancybox-overlay fancybox-overlay-fixed no-remove ">
		<div id="mensajeExito" class="alert <s:if test="tipoMensaje=='30'"> alert-success</s:if><s:else>alert-danger</s:else>" style="width:300px;z-index: 1051;margin: 30px auto; display:none;">
			<span>Mensaje</span>
			<p id="mensajeExitoText" style="margin: 30px auto;text-align: center;"><s:property value="mensaje"/></p>
		</div>
		<div id="mensajeError" class="alert alert-danger" style="width:300px;z-index: 1051;margin: 30px auto;display:none;">
			<span>Advertencia</span>
			<p id="mensajeErrorText" style="margin: 30px auto;text-align: center;"></p>
		</div>
		<div id="mensajeInfo" class="alert alert-info" style="width:300px;z-index: 1051;margin: 30px auto;display:none;">
			<span>Mensaje</span>
			<p id="mensajeInfoText" style="margin: 30px auto;text-align: center;"></p>
		</div>
	</div>
	<div class="container">
	  <div style="float: left; width: 100%; position: relative; padding-top: 10px;">
			<span class="logo_cotizador_agentes" style=" position: relative; width: 600px; height: 120px; float: left;"></span>
			<div class="pull-right" style="height: 120px;">
				<div style=" top: 60%; float: right; position: relative;">
					<label><s:property value="nameUser" /></label>
					<br>
					<s:if test="cotizacion.idToCotizacion != null">
						<strong class="title">
							<s:text name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />: <s:property value="cotizacion.idToCotizacion" />
						</strong>
						
						<button id="newCot" type="button" class="btn btn-success">
								<s:text name="midas.cotizacion.nuevaCotizacion" />
								<span class="glyphicon glyphicon-repeat"></span>
						</button>
						<br>
					</s:if>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<div class="container">
		<div class="col-md-12">
			<div class="well text-center">
				<fieldset>
					<legend >
							<s:if test="tipoMensaje != \"10\" ">
								<s:text name="midas.suscripcion.cotizacion.agentes.cotLigaAgenteFin"/>
							</s:if>
							<s:else>
								<s:text name="mensaje"/>
							</s:else>
					</legend>
				</fieldset>
			</div>
			<div class="footer">
				<p>© Afirme Seguros <bean:write name="currentDate" format="yyyy" /></p>
			</div>
		</div>
	</div>
	<br/>
		<div id="errorCargaExplorer" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><strong>Explorador no Compatible:</strong><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorer" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="errorCargaExplorerMode" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorerModo" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<body></body>
</html>