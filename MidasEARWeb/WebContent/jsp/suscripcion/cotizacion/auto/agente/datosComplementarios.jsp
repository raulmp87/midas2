<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none"></div>
<script type="text/javascript">
	(function() {
	
		var vin = '<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>';
		var cotizacionId = <s:property value="idToCotizacion"/>;
		
		var nombreCont = '<s:property value="nombreContr" />';
		var apellidoPatCont = '<s:property value="apellidoPatCont" />';
		var apellidoMatCont = '<s:property value="apellidoMatCont" />';
		
		var numeroFolio =  '<s:property value="correoObligatorio" />';
		
		if(vin != null && vin != ""){			
			if(obtenerAlertas(cotizacionId,vin) == "si"){
				$("#mensajeAlertas").css("display","block");
			}
		}
	})();
</script>
<s:form id="datosComentarioForm" enctype="multipart/form-data">
	<s:hidden name="forma" id="jspForm" />
	<s:hidden name="nameUser" id="nombreUsuario" />
	<s:hidden id="idToCotizacion" name="incisoCotizacion.id.idToCotizacion" />
	<s:hidden id="numeroInciso" name="incisoCotizacion.id.numeroInciso" />
	<s:hidden id="0idToCotizacion" name="idToCotizacion" />
	<s:hidden id="0numeroInciso" name="numeroInciso" />
	<s:hidden id="saveAsegurado" name="saveAsegurado" />
	<s:hidden id="claveTipoPersona" name="cotizacion.solicitudDTO.claveTipoPersona"/>
	<s:hidden id="idDomicilioContratante" name ="incisoCotizacion.cotizacionDTO.idDomicilioContratante"/>
	<s:hidden id="claveEstatus" name ="incisoCotizacion.cotizacionDTO.claveEstatus"/>
	<s:hidden id="0tipoCotizacion" name="cotizacion.tipoCotizacion"/>
	<s:hidden name="compatilbeExplorador" id="compatilbeExplorador"/>
	<s:hidden name="nextTap" id="nextTap" />
	<s:hidden name="nombreContr" id="nombreContr"/>
	<s:hidden name="apellidoPatCont" id="apellidoPatCont"/>
	<s:hidden name="apellidoMatCont" id="apellidoMatCont"/>
	<s:hidden name="correoObligatorio" id="correoObligatorio" />
	<s:hidden id="clati" name="clati"/>	
	<s:hidden id="mostrarMensaje" name="mostrarMensaje"/>
	<s:hidden id="usuarioExterno" name="usuarioExterno" />
			
	<s:if test="usuarioExterno == 1">
		<s:set var="disabledConsultaDescripcion">false</s:set>	
	</s:if>
	<s:else>
		<s:set var="disabledConsultaDescripcion">true</s:set>
	</s:else>
	
	<div class="col-md-12">
		<div class="container">
		  <p><span class="glyphicon glyphicon-info-sign"></span> <s:text name="midas.suscripcion.cotizacion.agentes.cliente.descripcion" /></p>
		</div>
	</div>
	<div class="row"  class="form-group col-sm-4" style="font-size:10px;">
	<s:if test="%{incisoCotizacion.incisoAutoCot.vinValido && incisoCotizacion.incisoAutoCot.coincideEstilo}">
		<s:set var="controlesFronterizosDisabled">true</s:set>
	</s:if>
	<s:else>
		<s:set var="controlesFronterizosDisabled">false</s:set>
	</s:else>
	<div class="col-md-12" id="idDatosVehiculo">
			<div class="well">
				<fieldset>
					<div style="font-size:12px;">
						<legend class="form-group col-sm-12" style="font-size:12px;" >
							<s:text	name="midas.suscripcion.cotizacion.auto.complementar.inciso.title" />
						</legend>
					</div>
					 <br/>
					<div class="form-group col-sm-3" style="font-size:10px;" id="numMotorDiv">
						<label for="numMotor">
							<s:text	name="midas.suscripcion.cotizacion.auto.complementar.inciso.numMotor" />:
						</label>
						<s:textfield 
							id="numMotor" name="incisoCotizacion.incisoAutoCot.numeroMotor"
							cssClass="form-control mandatory"
							maxLength="19"/>
						<label class="control-label error-label">Escribe el número de motor para continuar</label>
					</div>
					<div class="form-group col-sm-3" style="font-size:10px;" id="numSerieDiv">
						<label for="numSerie">
							<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numSerie" />:
						</label>
						<s:textfield 
							id="numSerie" name="incisoCotizacion.incisoAutoCot.numeroSerie"
							cssClass="form-control mandatory"
							readonly="%{#controlesFronterizosDisabled}" maxLength="17"/>
						<label class="control-label error-label">Escribe el número de serie</label>
					</div>
					<div class="form-group col-sm-3" style="font-size:10px;" id="numPlacaDiv">
						<label for="numPlaca">
							<s:text	name="midas.suscripcion.cotizacion.auto.complementar.inciso.numPlaca" />:
						</label>
						<s:textfield 
							id="numPlaca" name="incisoCotizacion.incisoAutoCot.placa"
							cssClass="form-control mandatory"
							maxLength="12"/>
						<label class="control-label error-label">Escribe el número de placa para continuar</label>
					</div>
					<div class="form-group col-sm-3" style="font-size:10px;"  id="repuveDiv">
						<label for="repuve">
							<s:text	name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.repuve" /> (Opcional):
						</label>
						<s:textfield id="repuve"
							name="incisoCotizacion.incisoAutoCot.repuve"
							cssClass="form-control"
							maxLength="10"/>
					</div>
				</fieldset>
				<br/>
				<s:if test="mostrarMensaje">
				<div id="mensajeInfoInspeccionContainer">
					<leyend class="col-sm-12" style="font-size:12px; font-weight: bold;">
						<strong>
							<s:text name="midas.cotizacion.mensajeInspeccion"/>
						</strong>
					</leyend>
				</div>
				</s:if>
			</div>
			<!-- 
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Art_140">
				<div id="documentosFortimaxDiv" style="display:none">
					<div class="well">
						<div id="docFortimaxLoad" style="display:none"></div>
						<div id="documentosFortimax" style="display:none"></div>
					</div>
				</div>
			</m:tienePermiso>
			 -->
		</div>
		<div class="col-md-12" id="idClientes">
			<div class="well">
				<fieldset>
					<div class="form-group col-sm-4" style="font-size:12px;" id="nombreClienteDiv">
						<legend class="form-group col-sm-12" style="font-size:12px;" >
							<s:text name="midas.suscripcion.cotizacion.agentes.clientes" />
						</legend>
					    <br/>
						<label class="small"><s:text name="Complementar datos del cliente" />:</label>
						<!--  s:text name="midas.suscripcion.cotizacion.agentes.nombreCliente" />:</label-->
						<s:textfield cssClass="form-control-datos-generales form-control" id="nombreCliente"
							disabled="true" name="cotizacion.nombreContratante"
							onchange="cargarDocumentFortimax();" />
						<s:hidden id="idCliente" name="cotizacion.idToPersonaContratante" />
				   </div>
					 <br/>
					     </br>
						<span class="pull-left">
							<div  class="form-group col-sm-6"  id="searchClientBtnDiv">
							<a href="#searchClientDiv" id="searchClientBtn" class="btn btn-default">
								<s:text name="midas.cotizacion.buscarcliente" />
								<span class="glyphicon glyphicon-search"></span>
							</a>
							</div>
							<div class="form-group col-sm-6"  id="addClientBtnDiv">
							<a href="#addClientDiv" id="addClientBtn" class="btn btn-default">
								<s:text name="midas.boton.agregar" />
								<s:text name="midas.suscripcion.cotizacion.agentes.clientes" />
								<span class="glyphicon glyphicon-user"></span>
							</a>
							</div>
						</span>
				</fieldset>
			</div>
		</div>
		<div class="col-md-12" id="idAsegurados">
			<div class="well">
				<fieldset>
					
					<div class="form-group col-sm-12" style="font-size:12px;">
						<legend class="form-group col-sm-12" style="font-size:12px;" >
							<s:text name="midas.listadoSiniestrosAction.titulo.asegurado" />
						</legend>
					</div>
					<div class="form-group col-sm-12" style="font-size:12px;">
							<button id="btn_copiarCliente" type="button" class="btn btn-default" style="display:none">
								<s:text
									name="midas.suscripcion.cotizacion.agentes.copiarCliente" />
								<span class="glyphicon glyphicon-new-window"></span>
							</button> </span>
							</div>					
					<div class="form-group  col-sm-4" style="font-size:12px;" id="nombreAseguradoDiv">
						<label class="small" for="nombreAsegurado" style="font-size:10px;">
							<s:text 
								name="midas.endosos.cotizacionEndosoListado.nombreAsegurado" />:
						</label>
						<s:textfield cssClass="form-control mandatory"
							name="incisoCotizacion.incisoAutoCot.nombreAsegurado"
							id="nombreAsegurado"
							onChange="if(confirm('\u00BFSolo Ingresara el nombre del Asegurado ?')){cargarNombreAsegurado();}" />
						<label class="control-label error-label">Escribe el nombre del asegurado para continuar</label>
						<s:hidden id="idAsegurado" name="incisoCotizacion.incisoAutoCot.personaAseguradoId" />
					</div>					
					<div class="form-group col-sm-8" style="font-size:12px;" id="searchClientBtnAgenDiv">
						<span class="pull-right">
							<a href="#searchClientDiv" class="btn btn-default" id="searchClientBtnAgen">
								<s:text name="midas.cotizacion.buscarcliente" />
								<span class="glyphicon glyphicon-search"></span>
							</a>
							</div>
				</fieldset>
			</div>
		</div>
		<div class="col-md-12" id="idConductor">
			<s:if test="guardaDatoConductorInciso">
				<div class="well">
					<fieldset>
						<div class="form-group col-sm-12" style="font-size:12px;">
							<legend class="form-group col-sm-12" style="font-size:12px;" >
								<s:text
								name="midas.suscripcion.cotizacion.auto.complementar.inciso.datosConductor" />
							</legend>
								</br>
							<span class="pull-left">
								<button id="btn_copiarInciso" type="button" class="btn btn-default" style="display:none">
									<s:text
										name="midas.suscripcion.cotizacion.auto.complementar.inciso.copiarDatos" />
									<span class="glyphicon glyphicon-new-window"></span>
								</button>
							</span>
							</div>
						
						<div class="form-group col-sm-4" style="font-size:12px;" id="nombreCondutorDiv">
							<label class="small"><s:text
									name="midas.catalogos.centro.operacion.nombre" />:</label>
							<s:textfield
								id="nombreCondutor"	name="incisoCotizacion.incisoAutoCot.nombreConductor" 
								cssClass="form-control mandatory" maxLength="100"/>
							<label class="control-label error-label">Escribe el nombre para continuar</label>
						</div>
						<div class="form-group col-sm-4" style="font-size:12px;" id="aPaternoCondutorDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.auto.complementar.inciso.paterno" />:</label>
							<s:textfield
								id="aPaternoCondutor" name="incisoCotizacion.incisoAutoCot.maternoConductor"
								cssClass="form-control mandatory" maxLength="100"/>
							<label class="control-label error-label">Escribe el apellido paterno para continuar</label>
						</div>
						<div class="form-group col-sm-4" style="font-size:12px;" id="aMaternoCondutorDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.auto.complementar.inciso.materno" />:</label>
							<s:textfield
								id="aMaternoCondutor" name="incisoCotizacion.incisoAutoCot.paternoConductor"
								cssClass="form-control mandatory" maxLength="100"/>
							<label class="control-label error-label">Escribe el apellido materno para continuar</label>
						</div>
						<div class="form-group col-sm-4 has-feedback" style="font-size:12px;" id="fechaNacimientoCondutorDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.auto.complementar.inciso.fechaNacimiento" />:</label>
							<s:textfield 
								id="fechaNacimientoCondutor" name="incisoCotizacion.incisoAutoCot.fechaNacConductor"
								cssClass="form-control mandatory datepicker"
								placeholder="dd/MM/yyyy" />
							<i class="glyphicon glyphicon-calendar form-control-feedback-cliente"></i>
							<label class="control-label error-label">Selecciona la fecha de nacimiento del conductor para continuar</label>
						</div>
						<div  class="form-group col-sm-4" style="font-size:12px;" id="numLicenciaCondutorDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.auto.complementar.inciso.noLicencia" />:</label>
							<s:textfield cssClass="form-control mandatory"
								id="numLicenciaCondutor"
								name="incisoCotizacion.incisoAutoCot.numeroLicencia"
								maxLength="50"/>
							<label class="control-label error-label">Escribe número de licencia del conductor para continuar</label>
						</div>
						<div class="form-group col-sm-4" style="font-size:12px;" id="ocupacionCondutorDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.auto.complementar.inciso.ocupacion" />:</label>
							<s:textfield cssClass="form-control mandatory"
								id="ocupacionCondutor"
								name="incisoCotizacion.incisoAutoCot.ocupacionConductor"
								maxLength="100"/>
							<label class="control-label error-label">Escribe la ocupación del conductor para continuar</label>
						</div>
					</fieldset>
				</div>
			</s:if>
				<div class="well" id="idObserva">
				<fieldset>
					
						<div class="form-group col-sm-12" style="font-size:12px;">
							<s:text name="midas.suscripcion.solicitud.autorizacion.observaciones" />
							<span class="pull-right">
							<a href="javascript:void();" id="btnToggleObs"><s:text name="midas.suscripcion.cotizacion.agentes.mostrar" /></a>
						</span>
						</div>
					
					<div class="form-group" id="divObservaciones" style="display:none">
						<s:textarea id="observaciones"
							name="incisoCotizacion.incisoAutoCot.observacionesinciso"
							disabled="%{#disabledConsultaDescripcion}"
							cssClass="form-control-datos-generales form-control
							<s:if test="guardaObservacionesInciso">
								<s:if test="disabledConsultaDescripcion"></s:if>
								<s:else>
								mandatory
								</s:else>
							</s:if>"
						/>
					</div>
				</fieldset>
				</div>
		</div>
		
	</div>
	<br />
	<div class="row" align="right">
		<button id="returnBtnComplementario" type="button" class="btn btn-success">
			<span class="glyphicon glyphicon-chevron-left"></span> <s:text name="midas.suscripcion.cotizacion.agentes.regresar"  />
		</button>
		<button id="nextBtnCom" type="button" class="btn btn-success">
			<s:text name="midas.suscripcion.cotizacion.agentes.siguientePaso" />
			<span class="glyphicon glyphicon-chevron-right"></span>
		</button>
		<br/>
		<br/>
	</div>
</s:form>
<div id="searchClientDiv" class="container" style="display: none"></div>
<div id="addClientDiv" class="container" style="display: none; width: 1050px; height: 500px;"></div>

<script type="text/javascript">
var negocio = '<s:property value="cotizacion.solicitudDTO.negocio.idToNegocio"/>';
var negocioTipoPersona = '<s:property value="cotizacion.solicitudDTO.negocio.tipoPersona"/>';
esconderCamposPaso(3, negocio);

$('#searchClientBtn').tooltip({
	   'placement':'bottom', 
	   'width': '100%',
	   'title': 'Da clic para localizar tus datos',
	   delay: {show: 0, hide: 250}
	}).tooltip('show');
	
$('#addClientBtn').tooltip({
	   'placement':'bottom', 
	   'width': '100%',
	   'title': 'Da clic para dar de alta tus datos',
	   delay: {show: 0, hide: 250}
	}).tooltip('show');
</script>