<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none"></div>
<script type="text/javascript">
	(function() {
		try {
		var vin = '<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>';
		var cotizacionId = <s:property value="idToCotizacion"/>;
		
		if(vin != null && vin != ""){			
			if(obtenerAlertas(cotizacionId,vin) == "si"){
				$("#mensajeAlertas").css("display","block");
			}
		}
		} catch(err) {
			
		}

	
		
	})();
</script>
<s:form id="resultadoTerminarCotizacionForm">
	<s:hidden name="forma" id="jspForm"/>
	<s:hidden name="nameUser" id="nombreUsuario"/>
	
	<s:hidden id="id" name="id" value="%{incisoCotizacion.id.idToCotizacion}"/>
	<s:hidden id="idToCotizacion" name="incisoCotizacion.id.idToCotizacion" />
	<s:hidden id="numeroInciso" name ="incisoCotizacion.id.numeroInciso" />
	<s:hidden id="0idToCotizacion" name="idToCotizacion" />
	<s:hidden id="0numeroInciso" name ="numeroInciso" />
	<s:hidden id="0idToPersonaContratante" name ="incisoCotizacion.cotizacionDTO.idToPersonaContratante"/>
	<s:hidden id="idToPersonaContratante" name ="idToPersonaContratante"/>
	<s:hidden id="newCotizadorAgente" name ="newCotizadorAgente" value="true"/>
	<s:hidden id="claveEstatus" name ="incisoCotizacion.cotizacionDTO.claveEstatus"/>
	<s:hidden id="idConductoCobroCliente" name ="incisoCotizacion.idConductoCobroCliente" />
	<s:hidden id="0tipoCotizacion" name="cotizacion.tipoCotizacion"/>
	<s:hidden name="compatilbeExplorador" id="compatilbeExplorador"/>
	<s:hidden name="mensaje" id="mensaje"/>
	<s:hidden name="tipoMensaje" id="tipoMensaje"/>
	<s:hidden name="isAgente" id="isAgente" value="S"/>
	<s:hidden id="clati" name="clati"/>	
	<div class="row">
		<div class="col-md-12">
			<div class="container">
			  <p><span class="glyphicon glyphicon-info-sign"></span> <s:text name="midas.suscripcion.cotizacion.agentes.comprar.descripcion" /></p>
			</div>
		</div>
		<div class="col-md-12">
			<div class="well">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th><s:text name="midas.suscripcion.solicitud.autorizacion.seccion" /></th>
							<th><s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>
							<th>#<s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>
							<th><s:text name="midas.suscripcion.solicitud.autorizacion.cobertura" /></th>
							<th><s:text name="midas.suscripcion.solicitud.autorizacion.datosIncumplidos" /></th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="excepcionesList" status="stat">
							<tr>
								<s:hidden name="excepcionesList[%{#stat.index}].idExcepcion"  id="excepcionesList%{#stat.index}.idExcepcion" value="%{idExcepcion}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].numExcepcion"  id="excepcionesList%{#stat.index}.numExcepcion"  value="%{numExcepcion}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].descripcionExcepcion"  id="excepcionesList%{#stat.index}.descripcionExcepcion"  value="%{descripcionExcepcion}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].idToCotizacion"  id="excepcionesList%{#stat.index}.idToCotizacion"  value="%{idToCotizacion}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].idLineaNegocio"  id="excepcionesList%{#stat.index}.idLineaNegocio"  value="%{idLineaNegocio}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].lineaNegocio"  id="excepcionesList%{#stat.index}.lineaNegocio"  value="%{lineaNegocio}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].idInciso"  id="excepcionesList%{#stat.index}.idInciso"  value="%{idInciso}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].sequenciaInciso"  id="excepcionesList%{#stat.index}.sequenciaInciso"  value="%{sequenciaInciso}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].idCobertura"  id="excepcionesList%{#stat.index}."  value="%{idCobertura}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].cobertura"  id="excepcionesList%{#stat.index}.cobertura"  value="%{cobertura}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].id"  id="excepcionesList%{#stat.index}.id"  value="%{id}"/>
								<s:hidden name="excepcionesList[%{#stat.index}].descripcionRegistroConExcepcion"  id="excepcionesList%{#stat.index}.descripcionRegistroConExcepcion"  value="%{descripcionRegistroConExcepcion}"/>	
								<td><s:property value="descripcionSeccion"/></td>	
								<td><s:property value="descripcionInciso"/></td>				
								<td><s:property value="sequenciaInciso"/></td>
								<td><s:property value="descripcionCobertura"/></td>				
								<td>				
									<ul>
										<s:iterator value="listaProvocadorExcepcion" var="provocadorExcepcion" status="statusLista">
											<li>								
												<s:property value="provocadorExcepcion"/>
											</li>	
										</s:iterator>
									</ul>				
								</td>
							</tr>			
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12" align="right">
		<br />
		<button id="returnBtnCobertura" type="button" class="btn btn-success" onclick="cargarForma('3');">
		<span class="glyphicon glyphicon-chevron-left"></span> <s:text name="midas.suscripcion.cotizacion.agentes.regresar"  />
	</button>
		<button id="cancelarAutorizacion" type="button" class="btn btn-primary">
	        <s:text name="midas.suscripcion.solicitud.autorizacion.regresarCotizacion" />
		</button>
		<button id="enviarAutorizacion" type="button" class="btn btn-success">
	        <s:text name="midas.suscripcion.solicitud.autorizacion.solicitarAutorizacion" />
		</button>
	</div>
</s:form>

