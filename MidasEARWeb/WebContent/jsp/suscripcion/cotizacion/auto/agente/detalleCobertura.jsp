<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none"></div>
<script type="text/javascript">
	(function() {
	
		var vin = '<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>';
		var cotizacionId = <s:property value="idToCotizacion"/>;
		
		var nombreCont = '<s:property value="nombreContr" />';
		var apellidoPatCont = '<s:property value="apellidoPatCont" />';
		var apellidoMatCont = '<s:property value="apellidoMatCont" />';
		
		if(vin != null && vin != ""){			
			if(obtenerAlertas(cotizacionId,vin) == "si"){
				$("#mensajeAlertas").css("display","block");
			}
		}
	})();
	
	
	
</script>
<s:form id="detalleCoberturasForm">
	<s:hidden name="forma" id="jspForm"/>
	<s:hidden name="nameUser" id="nombreUsuario"/>
	<s:hidden name="incisoCotizacion.id.idToCotizacion" id="idToCotizacion" />
	<s:hidden name ="incisoCotizacion.id.numeroInciso" id="numeroInciso" />
	<s:hidden name ="incisoCotizacion.incisoAutoCot.estadoId" id="idEstado" />
	<s:hidden name="idToCotizacion" id="0idToCotizacion" />
	<s:hidden name ="numeroInciso" id="0numeroInciso" />
	<s:hidden name="cotizacion.fechaFinVigencia" id="fechaFin" />
	<s:hidden name="cotizacion.fechaInicioVigencia" id="fechaIni" />
	<s:hidden name="cotizacion.solicitudDTO.negocio.idToNegocio" id="idNegocio" />
	<s:hidden name='updateSolicitud' id="updateSolicitud" value="false"/>
	<s:hidden name="idNegocioSeccion" id="idNegocioSeccion" />
	<s:hidden name="vehiculoestiloName" id="vehiculoestiloName" />
	<s:hidden name="isJson" id="isJson"/>
	<s:hidden name ="requiereDatosAdicionales" id="requiereDatosAdicionales"/>
	<s:hidden id="claveEstatus" name ="incisoCotizacion.cotizacionDTO.claveEstatus"/>
	<s:hidden name="bienasegurado" id="0bienasegurado"/>
	<s:hidden id="0primaTarifa" name="cotizacion.primaTarifa"/>
	<s:hidden id="0tipoCotizacion" name="cotizacion.tipoCotizacion"/>
	<s:hidden name="compatilbeExplorador" id="compatilbeExplorador"/>
	<s:hidden name="nextTap" id="nextTap" />
	<s:hidden name="nombreContr" id="nombreContr"/>
	<s:hidden name="apellidoPatCont" id="apellidoPatCont"/>
	<s:hidden name="apellidoMatCont" id="apellidoMatCont"/>
	<s:hidden name="correoObligatorio"/>
	<s:hidden name="cotizacion.idMoneda" id="idMoneda"/>
	<s:hidden name="configuracionId" id="configuracionId" />
	<s:hidden name="configuracion.emitir" id="configuracion-emitir" />
	<s:hidden id="clati" name="clati"/>	
	<s:hidden name="finalizaCotizacion" id="finalizaCotizacion" />
	<s:if test="cotizacion.tipoCotizacion=='TCSP'">
		<s:hidden name='cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza'/>
		<s:hidden name='incisoCotizacion.incisoAutoCot.negocioPaqueteId'/>
		<s:hidden name='cotizacion.idFormaPago'/>
		<s:hidden name='cotizacion.porcentajebonifcomision'/>
		<s:hidden name='cotizacion.porcentajeIva'/>
		<s:hidden name='incisoCotizacion.incisoAutoCot.pctDescuentoEstado'/>
	</s:if>
	<div class="col-md-12">
		<div class="container">
		  <p><span class="glyphicon glyphicon-info-sign"></span> <s:text name="midas.suscripcion.cotizacion.agentes.cotizacion.descripcion" /></p>
		</div>
	</div>
	<div class="">
		<div class="col-md-12" id="divDatosPaquete">
			<div class="well">
				<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/datosPaquete.jsp" />
			</div>
		</div>
		<div class="col-md-12" id="divResumenDiv">
			<div class="row" width="40%" style="font-size:10px;">
				<div class="col-md-12" >
					<div class="well">
						<div id="divResumen">
							<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/resumenTotalesCotizacionAgente.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" style="font-size:10px;" id="idDeAsegurada">
			<div class="well">
				<fieldset>
					<legend style="font-size:12px;"><s:text name="midas.suscripcion.cotizacion.agentes.detalleCobSumaAsegDedu" />
						<span class="pull-right">
							<a href="#" id="btnToggleCob"><s:text name="midas.suscripcion.cotizacion.agentes.mostrar" /></a>
						</span>
					</legend>
					<div id="divCoberturas" style="display:none">
						<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/coberturaCotizacionGrid.jsp" />
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<br />
	<div class="row" align="right">
			<button id="returnBtnCobertura" type="button" class="btn btn-success" onclick="regresarDatosGen();">
		<span class="glyphicon glyphicon-chevron-left"></span> <s:text name="midas.suscripcion.cotizacion.agentes.regresar"  />
	</button>
		<s:if test="configuracionId != null">
			<button id="nextBtnDetalleFinalizaCot" type="button" class="btn btn-success" title="Finalizar la cotizacion" >
				<s:text name="midas.suscripcion.cotizacion.agentes.finalizar" />
				<span class="glyphicon glyphicon-stop"></span>
			</button>
		</s:if>	
		<s:if test="configuracionId == null || (configuracionId != null && configuracion.emitir)">	
			<button id="nextBtnDetalle"  type="button" class="btn btn-success" title="Continuar al siguiente paso de la cotizacion" >
					<s:text name="midas.suscripcion.cotizacion.agentes.siguientePaso" />
				<span class="glyphicon glyphicon-chevron-right"></span>
			</button>
		</s:if>
		<br/>
		<br/>
	</div>
	<div class="container" id="divEstimacionTC" style="display: none;overflow:hidden; width: 500px">
		<div class="well">
			<fieldset>
				<legend>
					<s:text name="midas.suscripcion.cotizacion.agentes.estimacionRecibos" />
				</legend>
					<div id="divEstimacion">
						<s:include value="/jsp/suscripcion/cotizacion/auto/agente/include/esquemaPagos.jsp" />
					</div>
			</fieldset>
		</div>
	</div>
</s:form>
<div class="container" id="divDatosAdicionalesPaquete"  style="width: 600px;display: none"></div>
<div class="container" id="divImpresion" style="width:auto; display: none;overflow:hidden">
	<iframe id="iframeImpresion" frameborder="0" src="" style="width: 590px; height:430px;overflow:hidden"></iframe>
</div>
<div class="container" id="divIgualaPrimas" style="width: auto; display: none;overflow:hidden">
	<iframe id="iframeIgualaPrimas" frameborder="0" src="" style="width: 590px; height:400px;overflow:hidden"></iframe>
</div>
<script type="text/javascript">
var negocio = '<s:property value="cotizacion.solicitudDTO.negocio.idToNegocio"/>';
esconderCamposPaso(2, negocio);
</script>