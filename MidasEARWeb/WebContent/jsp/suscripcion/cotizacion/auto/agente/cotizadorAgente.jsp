<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<html>
	
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cotizadorAgentesHeader.jsp"/>
		<title> Afirme - Cotizador Agentes</title>
	</head>
	<input type="hidden" id="noSerieVin"/>	
	 <div class="scroll-area">
	 	<!-- NOTA: se agrega la clase no-remove debido a que al cerrar la ventana modal de busqueda/agregar cliente
	 		,el plugin fancyBox (parece ser un bug) elimina todos los elementos con la clase fancybox-overlay, 
	 		por lo que se modificó el archivo fuente jquery.fancybox.js para excluir del remove() los elementos que tengan la clase no-remove
	 	-->	 	
			<div id="backMSN" class="modal fade in fancybox-overlay fancybox-overlay-fixed no-remove" role="dialog" style="z-index: 9999;"> 
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 id="titleModal" class="modal-title">Aviso</h4>
				        </div>
				        <div class="modal-body">
							<!--<div id="mensajeExito" class="alert <s:if test="tipoMensaje=='30'"> alert-success</s:if><s:else>alert-danger</s:else>" style="width:300px;z-index: 1051;margin: 30px auto; display:none;"></div>-->
							<!--<div id="mensajeError" class="alert alert-danger" style="width:300px;z-index: 1051;margin: 30px auto;display:none;"></div>-->	
							<!-- <div id="mensajeInfo" class="alert alert-info" style="width:300px;z-index: 1051;margin: 30px auto;display:none;"></div>-->	 
								<div id="mensajeError"><p id="mensajeErrorText"></p></div>
								<div id="mensajeInfo"><p id="mensajeInfoText"></p></div>
								<div id="mensajeExito"><p id="mensajeExitoText"><s:property value="mensaje"/></p></div>
						</div>
						<div class="modal-footer">
					        <button id="btnAceptarModal" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
					    </div>
				    </div>
			    </div>
		    </div>
	</div>
	
	<div class="container">
	  <div style="float: left; width: 100%; position: relative; padding-top: 10px;">
			<span class="logo_cotizador_agentes" style=" position: relative; width: 400px; height: 90px; float: left;"></span>
			<div class="pull-right" style="height: 90px;">
				<div style=" top: 10%; float: right; position: relative;">
					<label><s:property value="nameUser" /></label>
<!-- 					<br> -->
					<s:if test="cotizacion.idToCotizacion != null">
						<strong class="title">
							<s:text name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />: <s:property value="cotizacion.idToCotizacion" />
						</strong>
					</s:if>					
					<s:if test="configuracionId == null">
						<s:if test="folioReexpedible">
							<br/>
							<strong class="title">
								<s:text name="midas.folio.reexpedible.field.rango.folio" />: <s:property value="cotizacion.folio" />
							</strong>
						</s:if>
					</s:if>
					<s:elseif test="forma == 2">
						<br/>
						<strong class="title">
							<s:text name="midas.suscripcion.cotizacion.agentes.cotLigaAgenteMsgFinalizar" />
						</strong>
					</s:elseif>
						<br/>
						<s:if test="cotizacion.idToCotizacion != null">
							<button id="newCot" type="button" class="btn btn-success">
								<s:text name="midas.cotizacion.nuevaCotizacion" />
								<span class="glyphicon glyphicon-repeat"></span>
							</button>
						</s:if>
						<br/>
					
					
				</div>
			</div>
		</div>
	</div>
	<br/>
	
		<div id="formContainer">
			<div class="container"  style="font-size:15px; font-weight:bold;">
				<div id="mensajeInfoInspeccionContainer">
					<p><s:text name="midas.cotizacion.mensajeInspeccion"/></p>
				</div>
				<br/>
				<div class="header">
				  <ul class="nav nav-pills nav-justified">
				    <s:if test="cuentaConAgente">
				        <li <s:if test="forma==1">class="active"</s:if> >
				    		<a href="javascript: void(0);" onclick="cargarForma('1');">1. <s:text name="midas.suscripcion.cotizacion.agentes.vehiculo"/></a>
				    	</li>
					    <li <s:if test="forma==2">class="active"</s:if>>
					    	<a href="javascript: void(0);"onclick="cargarForma('2');">2. <s:text name="midas.suscripcion.cotizacion.agentes.cotizacion"/></a>
					    </li>
					    <s:if test="configuracionId == null || (configuracionId != null && configuracion.emitir)">
					    <li	<s:if test="forma==3">class="active"</s:if>>
					    	<a href="javascript: void(0);" onclick="cargarForma('3');">3. <s:text name="midas.suscripcion.cotizacion.agentes.cliente"/></a>
					    </li>
					    <li <s:if test="forma==4">class="active"</s:if>>
							<a href="javascript: void(0);" onclick="cargarForma('4');">4. <s:text name="midas.suscripcion.cotizacion.agentes.comprar"/></a>
					    </li>
					    </s:if>
				    </s:if>				  
				    <s:else>
				        <li class="active"yy>
							<s:if test="excepcionesList.size>0">
								<a href="javascript: void(0);" <s:if test="cotizacion.claveEstatus!=16">onclick="cargarForma('4');"</s:if>><s:text name="midas.cotizacion.excepciones.title"/></a>
							</s:if>
							<s:else>
				    			<a href="javascript: void(0);"><s:text name="midas.suscripcion.cotizacion.agentes.seleccionarAgente"/></a>
							</s:else>
				    	</li>
				    </s:else>
				  </ul>
				</div>
			</div>
			<s:if test="configuracion.descripcion != null">
				<br/>
				<div class="container" style="font-size:21px;">
				<b>
					<s:property value="configuracion.descripcion" />
				</b>
				</div>
			</s:if>
			<br/>
			<div class="container">
				<div id="mensajeAlertas" class="alert alert-danger" style="display:none">
					<span class="pull-right" onclick="mostrarAlertas(<s:property value="idToCotizacion"/>,'<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>')">Mostrar</span>
					<p id="mensajeAlertasText">Se han encontrado alertas para el numero de serie</p>
				</div>
				<s:if test="forma==1">
					<s:include value="/jsp/suscripcion/cotizacion/auto/agente/datosGenerales.jsp" />
				</s:if>
				<s:elseif test="forma==2">
					<s:include value="/jsp/suscripcion/cotizacion/auto/agente/detalleCobertura.jsp" />
				</s:elseif>
				<s:elseif test="forma==3">
					<s:include value="/jsp/suscripcion/cotizacion/auto/agente/datosComplementarios.jsp" />
				</s:elseif>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cobranza">
				<s:elseif test="forma==4">
					<s:if test="excepcionesList.size>0">
						<s:include value="/jsp/suscripcion/cotizacion/auto/agente/resultadoTerminarCotizacion.jsp" />
					</s:if>
					<s:else>
						<s:include value="/jsp/suscripcion/cotizacion/auto/agente/cobranzaEmision.jsp" />
					</s:else>
				</s:elseif>
				</m:tienePermiso>
				
				<s:if test="excepcionesList==null">
				<div class="col-md-12" >
					<div class="footer">
						<p>© Afirme Seguros <bean:write name="currentDate" format="yyyy" /></p>
					</div>
					</div>
				</s:if>
			</div>
		</div>
		<div id="errorCargaExplorer" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><strong>Explorador no Compatible:</strong><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorer" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="errorCargaExplorerMode" style="display:none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="well">
							<div class="alert alert-info"><s:text name="midas.suscripcion.cotizacion.agentes.errorInternetExplorerModo" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="divAlertasSapAmis" class="container" style="display: none"></div>
		<div style="display: none">
			<a href="#divAlertasSapAmis" id="divAlertasSapAmisBtn" class="btn btn-default">#</a>
		</div>
	<body></body>
</html>