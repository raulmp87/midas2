<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="m" uri="/midas-tags"%>
<div id="conenedorRespuestaAccioneSapAmis" style="display: none"></div>
<script type="text/javascript">
	(function() {
	
		var vin = '<s:property value="incisoCotizacion.incisoAutoCot.numeroSerie"/>';
		var cotizacionId = <s:property value="idToCotizacion"/>;
		
		if(vin != null && vin != ""){			
			if(obtenerAlertas(cotizacionId,vin) == "si"){
				$("#mensajeAlertas").css("display","block");
			}
		}	
	})();
</script>
<form id="cobranzaYEmisionForm">
	<s:hidden name="forma" id="jspForm" />
	<s:hidden name="nameUser" id="nombreUsuario" />
	<s:hidden id="id" name="id"
		value="%{incisoCotizacion.id.idToCotizacion}" />
	<s:hidden id="idToCotizacion" name="incisoCotizacion.id.idToCotizacion" />
	<s:hidden id="numeroInciso" name="incisoCotizacion.id.numeroInciso" />
	<s:hidden id="0idToCotizacion" name="idToCotizacion" />
	<s:hidden id="0numeroInciso" name="numeroInciso" />
	<s:hidden id="0idToPersonaContratante"
		name="incisoCotizacion.cotizacionDTO.idToPersonaContratante" />
	<s:hidden id="idToPersonaContratante" name="idToPersonaContratante" />
	<s:hidden id="newCotizadorAgente" name="newCotizadorAgente"
		value="true" />
	<s:hidden id="claveEstatus"
		name="incisoCotizacion.cotizacionDTO.claveEstatus" />
	<s:hidden id="idConductoCobroCliente"
		name="incisoCotizacion.idConductoCobroCliente" />
	<s:hidden id="0tipoCotizacion" name="cotizacion.tipoCotizacion" />
	<s:hidden name="compatilbeExplorador" id="compatilbeExplorador" />
	<s:hidden name="correo" id="correo" />
	<s:hidden name="correoObligatorio" id="correoObligatorio" />
	<s:hidden name="nombreContratante" id="nombreContratante" />
	<s:hidden name="nextTap" id="nextTap" />
	<s:hidden name="bienasegurado" id="0bienasegurado" />
	<s:hidden name="cotizacion.fechaFinVigencia" id="fechaFin" />
	<s:hidden name="cotizacion.fechaInicioVigencia" id="fechaIni" />
	<s:hidden name="nomPaquete" id="nomPaquete" />
	<s:hidden name="nomFormaPago" id="nomFormaPago" />
	<s:hidden name="tienePermisosAvisoPriv" id ="tienePermisosAvisoPriv" /> 
	<s:hidden id="clati" name="clati" />
	<s:hidden name="idDiv" id="idDivMostE" value="divMostEntrev"></s:hidden>
	<s:hidden name="idToNegocio" id="idToNegocio"></s:hidden>
	<div class="row">

		<div class="col-md-12">
			<div class="container">
				<p>
					<span class="glyphicon glyphicon-info-sign"></span>
					<s:text
						name="midas.suscripcion.cotizacion.agentes.comprar.descripcion" />
				</p>
			</div>
		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="well">
						<div id="divResumen">
							<s:include
								value="/jsp/suscripcion/cotizacion/auto/agente/include/resumenTotalesCotizacionAgente.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<s:if test="userTieneRolEjecutivo!='NINGUNO' ">
			<div class="col-md-12" id="listaEjecutivos">
				<div class="well">
					<fieldset>
						<legend style="font-size: 12px;">
							<s:text
								name="midas.suscripcion.cotizacion.agentes.ejecutivosVenta" />
						</legend>
						<div class="form-group col-sm-4" style="font-size: 10px;"
							id="listaEjecutivosDiv">
							<label class="small"><s:text
									name="midas.suscripcion.cotizacion.agentes.listaEjecutivos" />:</label>
							<s:select id="ejecutivosList" name="ejecutivoColoca"
								list="listaDeEjecutivosMap" value="cotizacion.ejecutivoColoca"
								headerKey=""
								headerValue="%{getText('midas.general.seleccione')}" onchange=""
								cssClass="form-control-datos-generales mandatory" />
							<label class="control-label error-label ">Elija un
								ejecutivo</label>
						</div>
					</fieldset>
				</div>
			</div>
		</s:if>

		<div class="col-md-12" id="idMediosP">
			<div class="well">
				<fieldset>
					<legend style="font-size: 12px;">
						<s:text name="midas.suscripcion.cotizacion.agentes.mediosPago" />
					</legend>
					<h4 style="font-size: 10px;">
						<s:text
							name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />
						<s:property value="idToCotizacion" />
					</h4>
					<div class="form-group col-sm-4" style="font-size: 10px;"
						id="medioPagoDTOsDiv">
						<label class="small"><s:text
								name="midas.suscripcion.cotizacion.agentes.medioPago" />:</label>
						<s:select id="medioPagoDTOs" name="idMedioPago"
							list="medioPagoDTOs" listKey="idMedioPago"
							listValue="descripcion" value="cotizacion.idMedioPago"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							onchange="onChangeMedios(); "
							cssClass="form-control-datos-generales mandatory" />
						<label class="control-label error-label ">Elija el medio
							de pago</label>
					</div>
					<div class="form-group col-sm-4" style="font-size: 10px;">
						<div id="conductos_div" style="display: inline">
							<label class="small" for="conductoCobro"><s:text
									name="midas.suscripcion.cotizacion.auto.cobranzaInciso.conductosCobro" />:</label>

							<s:select id="conductos" name="idConductoCobro"
								value="incisoCotizacion.idConductoCobroCliente"
								list="conductosDeCobro" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								onchange="onChangeConductos();"
								cssClass="form-control-datos-generales mandatoryInfoAdicional" />
							<label class="control-label error-label ">Elija el
								conducto de cobro</label>
						</div>

					</div>
					<div class="form-group col-sm-4" style="font-size: 10px;">
						<div id="checkDatos" style="display: none">
							<label class="small"><s:text
									name="¿El titular es la misma persona que el cliente de la póliza?" /></label>
							<s:select id="titularIgual" name="datosIguales"
								list="mapDatosIguales" value="datosIguales"
								onchange="onChangeTitularIgual(); eliminarAdvertencia();"
								cssClass="form-control-datos-generales mandatoryInfoAdicional" />
							<label class="control-label error-label ">Elija el
								titular de la tarjeta </label>

						</div>

					</div>

				</fieldset>
			</div>
		</div>
		<div class="col-md-12" style="font-size: 10px;">
			<div id="datosTitularYTarjeta" style="display: none">
				<div class="row">
					<div class="form-group col-md-12"
						style="font-size: 10px; display: none;" id="divSeccionTitular">
						<div class="well">
							<div id="divTitular"></div>
						</div>
					</div>
					<div class="col-md-12" id="divSeccionTarjeta"
						style="display: none;">
						<div class="well">
							<div id="divcuentaTarjeta"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" align="right">
		
		<div class="col-md-12" id="idClienteUnico">
				<div class="form-group col-md-12"  id="chkAvisPriv" align="left">
				<br /> <input type="checkbox" name="aviso" id="a" required checked
					value=checked>Acepto <a href="javascript:void(0);" onclick="window.open('https://www.afirme.com/Portal/VisualizadorContenido.do?menu=12&archivoId=10474')">Aviso
					de Privacidad</a> y Acepto uso de Medios Electrónicos Seguros Afirme <a
					href="javascript:void(0);" onclick="window.open('https://www.afirme.com/Portal/VisualizadorContenido.do?archivoId=1250&menu')">Ver
					Detalle...</a> <br /> 
			</div>
		</div>
			<button id="returnBtnCobertura" type="button" class="btn btn-success"
				onclick="cargarForma('3');">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<s:text name="midas.suscripcion.cotizacion.agentes.regresar" />
			</button>
			<button id="nextBtnEmt" type="button" class="btn btn-success">
				<s:text name="midas.boton.guardar" />
			</button>
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Emitir">
				<button id="btn_emitirCotizacion" type="button"
					class="btn btn-success" style="display: none">
					<s:text name="midas.suscripcion.cotizacion.complementar.comprar" />
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button id="printBtn" type="button"
					onclick="<s:property value="functionPrintPoliza"/>"
					class="btn btn-success" style="display: none">
					<s:text name="midas.componente.impresiones.botonImprimir" />
					<span class="glyphicon glyphicon-print"></span>
				</button>
			</m:tienePermiso>
			<button id="btn_newCotizacion" style="display: none" type="button"
				class="btn btn-primary">
				<s:text name="midas.cotizacion.nuevaCotizacion" />
				<span class="glyphicon glyphicon-repeat"></span>
			</button>
			<button id="btn_cerrar" type="button" class="btn btn-primary"
				onclick="window.close();">
				<s:text name="Cerrar" />
			</button>
		</div>
		<br /> <br />
	</div>
</form>
<div style="display: none">
	<a href="#divImpresion" id="btn_impresion">#</a>
</div>
<div class="container" id="divImpresion"
	style="width: auto; display: none; overflow: hidden">
	<iframe id="iframeImpresion" frameborder="0" src=""
		style="width: 600px; height: 700px; overflow: hidden;"></iframe>
</div>
<script type="text/javascript">
var negocio = '<s:property value="cotizacion.solicitudDTO.negocio.idToNegocio"/>';
jQuery('#idToNegocio').val(negocio);
esconderCamposPaso(4, negocio);
</script>

