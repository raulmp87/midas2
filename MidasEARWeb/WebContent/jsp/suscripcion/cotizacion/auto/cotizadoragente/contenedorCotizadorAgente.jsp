<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cotizadoragente/cotizadorAgenteHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/operacioensSapAmis/sapAmisAccionesAlertas.js'/>"></script>
<div id="indicadorContenedor"></div>
<script type="text/javascript">
	mostrarIndicadorCarga('indicadorContenedor');
</script>
	<div class="titulo">
		<s:text name="midas.cotizacion.no" />
		<b><s:property value="cotizacion.numeroCotizacion"/></b>
	</div>
<div id="conenedorRespuestaAccioneSapAmis" style="display:none" ></div>
<s:hidden id="idToCotizacion" name="idToCotizacion" />
<s:hidden id="cotizacionId" name="cotizacionId" value="%{idToCotizacion}" />
<s:hidden id="soloConsulta" name="soloConsulta" />
<s:hidden id="numInciso" name="numInciso" />
<s:hidden name="noSerieVin" id="noSerieVin" />
<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html" style="height: 430px; width: 920px" id="cotizacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="150px" id="detalle" name="<s:text name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />" href="http://void" extraAction="javascript: verDetalleCotizacionAgente(<s:property value="cotizacion.idToCotizacion" escapeHtml="false" default="null"/>,<s:property value="cotizacion.claveEstatus" escapeHtml="false" default="null"/>);">
	</div>
	<s:if test="idToCotizacion != null && cotizacion.claveEstatus==12">
		<div width="150px" id="complementar" name="<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.complemEmision" />" href="http://void" extraAction="javascript: verComplementarEmisionAgente();">
		</div>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cobranza">
			<div width="100%" id="cobranza" name="Cobranza" href="http://void" extraAction="javascript: desplegarCobranzaAgente()"></div>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Art_140">
			<div width="200px" id="documentos140" name="Documentos Art 140" href="http://void" extraAction="javascript: desplegarDocumentos140Agente()"></div>
		</m:tienePermiso>
		<!-- 2015/09/30 eduardo.chavez Se agrega la pestaña para las Acciones de las Alertas SAP-AMIS -->
			<div width="200px" id="accionesAlertasSAP" name="Acciones Alertas SAP-AMIS" href="http://void" extraAction="javascript: desplegarAccionesAlertasSAPAMIS()"></div>
	</s:if>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>