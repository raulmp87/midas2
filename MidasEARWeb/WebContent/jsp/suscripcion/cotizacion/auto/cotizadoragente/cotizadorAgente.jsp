<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<style type="text/css">
label {
	font-weight: bold;
}
</style>
<script type="text/javascript">
	var verDetalleIncisoPath = '<s:url action="verDetalleInciso" namespace="/vehiculo/inciso"/>';
	var verEsquemaPagoPath = '<s:url action="verEsquemaPagoAgente" namespace="/suscripcion/cotizacion/auto"/>';
	var datosVehiculoPath = '<s:url action="mostrar" namespace="/suscripcion/cotizacion/auto/complementar/inciso"/>';
	var verVehiculoPath = '<s:url action="verDetalleIncisoAgente" namespace="/vehiculo/inciso"/>';
	var idContratante = '<s:property value="cotizacion.idToPersonaContratante"/>';
	var verIgualarPrimasAgentePath = '<s:url action="verIgualarPrimasAgente" namespace="/suscripcion/cotizacion/auto/cotizadoragente"/>';
	jQuery(document).ready(function(){
		// Valida el estado de los campos
		function validaCampos(){
			var arrayCampos = new Array({id:"comisionCedida"},{id:"formaPago"},{id:"derechos"},{id:"descuentoGlobal"}
									   ,{id:"fecha"},{id:"fechados"},{id:"porcentajePagoFraccionado"},{id:"fechaSeguimiento"},{id:"nombreContratante"});
			for(x in arrayCampos){
				jQuery('#' + arrayCampos[x].id).cambio();
			}
		}
		validaCampos();
	});
</script>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<s:form id="cotizacionForm">	
	<s:hidden id="idMoneda" name="cotizacion.idMoneda" />
	<s:hidden id="idCotizacion" name="cotizacion.idToCotizacion" />	
	<s:hidden id="numeroInciso" value="1"  />
	<s:hidden id="validacionesPendientes" name="validacionesPendientes"  />
	<s:hidden id="cerrarVentanaIgualacion" name="cerrarVentanaIgualacion" />
<div class="clear"></div>
	<table id="agregar" style="padding: 0px; margin: 0px; border: none; width : 98%" class="fixTabla">
		<tr>
			<td valign="top" width="55%">
				<table id="agregar">
					<tr>
						<td colspan="2">
							<s:text name="midas.cotizacion.datosCotizacion"/>
						</td>
					</tr>
					<tr>
						<td colspan="2" >
							<s:textfield name="cotizacion.nombreContratante"
						               key="midas.suscripcion.cotizacion.nombreProspecto"
							           labelposition="top" 		
					           			disabled="%{#disabledConsulta}"				 							   
									   id="nombreContratante" maxlength="100" 
									   cssClass="txtfield" cssStyle="width: 400px;">
							</s:textfield>		
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<s:select name="cotizacion.negocio.tiposvigencia" id="tiposvigencia"						
							 key="midas.cotizacion.tipovigencia" cssClass="txtfield"
							 headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							 list="tiposVigenciaList" 
							 value="vigenciaDefault"
							 disabled="%{#disabledConsulta}"
							 onchange="actualizarFechas(this.value);"  >
							</s:select>
						</td>
					</tr>
					<tr>
						<td>
							<sj:datepicker name="cotizacion.fechaInicioVigencia" required="#requiredField" 
					           labelposition="top" 
					           key="midas.cotizacion.iniciovigencia"
					           size="10"
					           changeMonth="true"					           
					           changeYear="true"					           
					           readonly="readonly"	
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
					           cssClass="txtfield" cssStyle="width: 175px;"
							   buttonImage="../img/b_calendario.gif"
							   id="fecha" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onchange="ajustaFechaFinal();"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>						
						</td>
						<td>
							<sj:datepicker name="cotizacion.fechaFinVigencia" required="#requiredField"
					           labelposition="top"
							   buttonImage="../img/b_calendario.gif"
							   key="midas.cotizacion.finvigencia"
							   changeMonth="true"
					           changeYear="true"					          
							   cssClass="txtfield" cssStyle="width: 175px;"
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
							   size="10"
							   id="fechados" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>						
						</td>
					</tr>
					<s:if test="mostrarSeleccionarAgente">
						<tr>
							<td colspan="2">
								<div id="divSeleccionarAgenteIndicador"></div>
								<div id="divSeleccionarAgente">
								<s:action name="getInfoAgenteCotizador" var="agenteInfo" namespace="/componente/agente" ignoreContextParams="true" executeResult="true" >
									<s:param name="idTcAgenteCompName">cotizacion.solicitudDTO.codigoAgente</s:param>
									<s:param name="nombreAgenteCompName">agente.persona.nombre</s:param>
									<s:param name="permiteBuscar"><s:property value="mostrarSeleccionarAgente"/></s:param>		
								</s:action>
								</div>							
							</td>
						</tr>
					</s:if>
					<s:else>
						<s:hidden id="idAgente" name="cotizacion.solicitudDTO.codigoAgente" />
					</s:else>
					<tr>
						<td>		
							<s:select name="cotizacion.solicitudDTO.negocio.idToNegocio" id="negocio" 
						      key="midas.cotizacion.negocio" cssClass="txtfield"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioAgenteList" headerKey="" required="#requiredField"
						      listKey="idToNegocio" listValue="descripcionNegocio"
						      disabled="#readOnly" 
						      onchange="cargaProductosNegocioAgente(this.value);
						      cargaDerechosNegocio(this.value);
						      mostrarPagoFraccionado(this.value);
						      cargaTiposVigenciaNegocio(this.value);						      
						      "   >
				 			</s:select>	
						</td>
						<td>
							<s:select name="cotizacion.negocioTipoPoliza.negocioProducto.idToNegProducto" id="producto" 
						      key="midas.cotizacion.producto" cssClass="txtfield"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioProductoList" headerKey="" required="#requiredField"
						      disabled="#readOnly"
						      onchange="cargaTipoPolizaAgente(this.value);
						      ">
				    		</s:select>					
						</td>
					</tr>
					<tr>
						<td>
							<s:select name="cotizacion.negocioTipoPoliza.idToNegTipoPoliza" id="tipoPoliza" 
						      key="midas.cotizacion.tipopoliza" cssClass="txtfield"
						      headerValue="%{getText('midas.general.seleccione')}"
						      list="negocioTipoPolizaList" headerKey="" required="#requiredField"
						      onchange="cargaFormasPagoAgente(this.value);
						      mostrarDivVehiculo();
						      cargaDerechosNegocio();
						      ">
				    		</s:select>					
						</td>
						<td><div class="w150" style="float:left;"> </div></td>		
					</tr>
					<tr>
						<td>
							<s:select  id="formaPago"  cssClass="txtfield"
				         		key="midas.cotizacion.formapago"
								list="formasdePagoList"
								name="cotizacion.idFormaPago"
					    		labelposition="top" headerKey="-1"
					           	disabled="%{#disabledConsulta}"
					    		onchange="cargarRecargoPagoFraccionado(this.value, jQuery('#idMoneda').val());"
								headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
						</td>
						<td>
							<div id="divPagoFraccionado" style="display: none">
							<s:textfield name="cotizacion.porcentajePagoFraccionado" required="#requiredField"
						               key="midas.cotizacion.porcentajePagoFraccionado"
							           labelposition="top"
					           			disabled="%{#disabledConsulta}"
									   id="porcentajePagoFraccionado" maxlength="5" cssClass="txtfield" cssStyle="width: 200px;" 						   								  
									   onkeypress="return soloNumeros(this, event, true)">
							</s:textfield>
							</div>								
						</td>
					</tr>
					<tr>
						<td>
							<s:select  id="derechos"  cssClass="txtfield"
						     key="midas.cotizacion.derechos"
						        labelposition="top" 						    
								list="derechosPolizaList"
					           	disabled="%{#disabledConsulta}"
								name="cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza"
								headerKey=""
								headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>						
						</td>
													
					</tr>
					<tr>
						<td>
							<s:textfield name="cotizacion.porcentajebonifcomision"
								required="#requiredField" key="midas.cotizacion.comisioncedida"
								labelposition="top" id="comisionCedida" maxlength="5"
								cssClass="txtfield" cssStyle="width: 200px;"
					           	disabled="%{#disabledConsulta}"
								onkeypress="return soloNumeros(this, event, true);"
								onchange="validaComisionCedida(this.value,100,0,'')">
							</s:textfield></td>
					</tr>
					<tr>
						<td>
							<sj:datepicker name="cotizacion.fechaSeguimiento" required="#requiredField" 
					           labelposition="top" 
					           key="midas.cotizacion.fechaSeguimiento"
					           size="10"
					           changeMonth="true"
					           changeYear="true"
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
					           cssClass="txtfield" cssStyle="width: 175px;"
							   buttonImage="../img/b_calendario.gif"
							   id="fechaSeguimiento" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>		
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
							<div  style="font-weight:bold;">
								<s:text name="midas.cotizacion.descuentoporvolumen"/>: <s:property value="cotizacion.descuentoVolumen"/> %
							</div>	
						</td>						
					</tr>
				</table>          					   	  										
			</td>
			<td valign="top">
				<table style=" width:100%; padding: 5px; margin: 5px; border: none">
					<tr >
						<td colspan="2" valign="top" align="right">
							<div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid"></div>
						</td>
					</tr>
					<tr >
						<td colspan="2" valign="top" align="right">
							<div id="cargaEsquemaPago"></div>
							<div id="resumenEsquemaPago"></div>
						</td>
					</tr>
				</table>			
			</td>				
		</tr>
		<tr>
			<td colspan="2">
				<s:if test="cotizacion.idToCotizacion != null">
					<div class="btn_back w100" style="display: inline; float: right;">
						<a href="javascript: void(0);"
							onclick="volverAListadoCotizacion();"> <s:text
								name="midas.boton.salir" /> </a>
					</div>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir">
					<div id="imprimeCotizacion" class="btn_back w150"
						style="display: inline; float: right;">
						<a href="javascript: void(0);"
							onclick="mostrarContenedorImpresionAgente(1,<s:property value="cotizacion.idToCotizacion" escapeHtml="false"/>);"
							class="icon_imprimir "> <s:text
							name="midas.cotizacion.imprimircotizacion" /> </a>
					</div>
				</m:tienePermiso>
				</s:if>
				<div id="nuevaCotizacion" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="nuevaCotizacion();"> <s:text
						name="midas.cotizacion.nuevaCotizacion" /> </a>
				</div>
				<s:if test="cotizacion.idToCotizacion != null">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Igualacion_Prima">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="verIgualarPrimasAgente(<s:property value='cotizacion.idToCotizacion'/>);"
						class="icon_global"> <s:text name="midas.cotizacion.primaObjectivo" /> </a>
					</div>
				</m:tienePermiso>		
				<div id="btnRecalcular"  style="display: inline; float: right;" class="btn_back w100">
					<a href="javascript: void(0);" onclick="validaRecalcular();">
						<s:text name="ReCalcular"/>
					</a>
				</div>
				<s:if test="cotizacion.claveEstatus!=16 && cotizacion.claveEstatus!=12">
					<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Terminar">
						<div id="terminarCotizacion" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="terminarCotizacionAgente(<s:property value='cotizacion.idToCotizacion'/>);"> 
								<s:text name="midas.cotizacion.terminarCotizacion" /> </a>
						</div>
					</m:tienePermiso>
				</s:if>
				</s:if>
				
			</td>
		</tr>
	</table>
	
	<div id="divCargaVehiculo"></div>
	<div id="divVehiculo"></div>
	<div id="divResultado" style="display: none;"></div>
</s:form>


<script type="text/javascript">
		obtenerResumenTotalesCotizacionGridAgente();		
		mostrarDivEsquemaPago();
		jQuery(document).ready(function(){
			ocultarIndicadorCarga('indicador');
			
			var dataDefault = '<s:property value="vigenciaDefault" />';
			if(dataDefault == -1){
				 dataDefault = "";
			}
			if(dataDefault == "" && '<s:property value="tiposVigenciaList.size" />' == 0){	
				deshabilitarVigencias(true);		 
			} else {
				deshabilitarVigencias(false);		
			}
		});
</script>
<s:if test="cotizacion.idToCotizacion != null">
<script type="text/javascript">
		mostrarDivVehiculo();
		mostrarPagoFraccionado();
</script>
</s:if>