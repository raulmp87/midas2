<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cargamasivaserviciopublico/cargaMasivaServicioPublico.js'/>"></script>

<div class="titulo">
	<s:text name="midas.cargamasivaserviciopublico.adjuntos.titulo"/>
</div>

<s:hidden name="idToControlArchivo" id="idToControlArchivo" />

<s:form id="cargaMasivaServicioPublicoForm">
	<s:hidden name="claveTipo" id="claveTipo" value="1"/>
	<div  style="width: 98%;text-align: center;">
		<table id="agregar" >
			<tr>
				<td>
					<div class="btn_back w150" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="importarArchivo();" class="icon_enviar"> <s:text name="midas.cotizacion.cargamasiva.importar" /> </a>
					</div>
						<div class="btn_back w100" style="display: inline; float: right;">
							<a href="javascript: void(0);" id="cargaMasivaServicioPublico" onclick="iniciarbitacorasCargaMasivaSPublicoGrid();">
							Bitacora Negocio
							</a>
						</div>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
	</div>
</s:form>

<div class="titulo">
	<s:text name="midas.configuracionNotificaciones.adjuntos.listado"/>
</div>

<div id="indicadorGrid"></div>
<div id="archivosAdjuntosGrid" style="width: 98%;height:400px" align="center"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

<div class="btn_back w90" style="display: inline; float: right;">
	<a href="javascript: void(0);" id="cargaMasivaServicioPublico" onclick="regresaCotizador();">
		Regresar
	</a>
</div>

<script type="text/javascript">
	iniciaArchivosCargaMasivaGrid();
</script>