<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cargamasivaserviciopublico/CargaMasivaSPBitacora.js'/>"></script>

<style type="text/css">
	
	
	div.ui-datepicker {
		font-size: 10px;
	}
	
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
		height: 100px;
		width: 98%;
	}
	

	
	.floatLeft {
		float: left;
		padding-right: 10px;
	}
	
	.divContenedor {
		float:left; 
		clear: left; 
		padding: 10px 10px;
		width: 95%;		
	}	
	
</style>

<div id="bitacoraCargaMasivaSPFiltradoDTO" style="margin-left: 0% !important;">
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.cargamasivaserviciopublico.adjunto.bitacoracargamasiva.titulo"/>	
	</div>	
	<div class="divContenedorO">
		<form id="formBitacoraCargaMasivaSPFiltradoDTO">
				<div class="divContenedor">
					<div class="floatLeft" >
						<label>Póliza:</label><br/>
						<s:textfield  maxlength="10" name="bitacoraFiltradoDTO.poliza" 
								onBlur="validaFormatoSiniestro(this);"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield"></s:textfield>
					</div>
					<div class="floatLeft" >
						<label>No. Serie:</label><br/>
						<s:textfield maxlength="17"  name="bitacoraFiltradoDTO.numSerie"
								onBlur="validaFormatoBitacora(this);"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
					</div>
					<div class="floatLeft"  >
					    <label>Estatus:</label><br/>
						<div class="floatLeft" >
							<s:textfield maxlength="17"   name="bitacoraFiltradoDTO.estatus"
									cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
						</div>
					</div>
					<div class="floatLeft">
						<label>Descripcion:</label><br/>	
						<s:textfield maxlength="17"  name="bitacoraFiltradoDTO.codigoError" 
								cssClass="cleaneable txtfield"  cssStyle="float: left;width: 96%;"></s:textfield>
					</div>				 
					<div class="floatLeft" >
						<label>Fecha Inicial:</label><br/>						
						<sj:datepicker  name="bitacoraFiltradoDTO.fechaInicioCarga"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="bitacoraFiltradoDTO.reporteCabina.fechaHoraReporte"
								  maxlength="10" cssClass="txtfield hasDatapicker"									   
									   label="Fecha Inicio"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield">
						</sj:datepicker>
					</div>
					<div class="floatLeft" >
						<label>Fecha Final:</label><br/>
						<sj:datepicker  name="bitacoraFiltradoDTO.fechaFinCarga"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="bitacoraFiltradoDTO.reporteCabina.fechaHoraTerminacion"
								  maxlength="10" cssClass="txtfield hasDatapicker"									
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="txtfield cleaneable ">
						</sj:datepicker>
					</div>
				</div>				
				<div class="divContenedor" >
					<div id="comandos" style="float: left; padding-right: 5px; overflow: auto; text-align: left; width: 100%;">	
						<div class="btn_back w100" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="limpiar();"> 
								<s:text	name="midas.boton.limpiar"/> 
							</a>
						</div>
						<div class="btn_back w100" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="realizarBusqueda(true);"> 
								<s:text	name="midas.boton.buscar"/> 
							</a>
						</div>
						
						<div class="btn_back w100" style="display: inline; float: right;">
						<a href="javascript: void(0);" id="botonRegresar" onclick="regresaCargaMasiva();">
							Regresar
						</a>
						</div>
					</div>
				</div>
		</form>
	</div>	
</div>

<div class="titulo">
	Bitacora Carga Masiva
</div>

<div id="indicadorGrid"></div>
<div id="bitacorasArchivosAdjuntosGrid" style="width: 98%;height:400px" align="center"></div>
<div id="pagingArea"></div><div id="infoArea"></div>


<%-- <script type="text/javascript">
	/* iniciarbitacorasCargaMasivaSPublicoGrid(); */
	realizarBusqueda(true);
</script> --%>