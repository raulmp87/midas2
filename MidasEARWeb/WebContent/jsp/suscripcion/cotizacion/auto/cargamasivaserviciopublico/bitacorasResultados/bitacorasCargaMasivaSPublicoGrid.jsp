<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">

	<s:iterator value="resultado">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="poliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numSerie" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="usuarioCarga" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:date name="fechaCarga" format="dd/MM/yyyy HH:mm" /></cell>	
		    <cell><s:property value="descripcionError" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>