<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cargamasivaserviciopublico/cargaMasivaServicioPublicoDetalle.js'/>"></script>


<s:hidden name="idToControlArchivo" id="idToControlArchivo"/>
<div class="titulo">
	<s:text name="midas.cargamasivaserviciopublico.adjunto.detalledelarchivo"/>
</div>




<div id="indicadorGrid"></div>
<div id="detalleArchivosAdjuntosGrid" style="width: 98%;height:450px" align="center"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

	<div class="btn_back w90" style="display: inline; float: right;">
		<a href="javascript: void(0);" id="cargaMasivaServicioPublico" onclick="iniciarCargaMasivaServicioPublico();">
			Regresar
		</a>
	</div>
						

<script type="text/javascript">
	iniciarCargaMasivaDetalleGrid();
</script>