<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
	<%-- <head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="*"  align="center" sort="int" hidden="true"> <s:text name="midas.cargamasivaserviciopublico.adjunto.id" />   </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.poliza" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.cliente" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.numserie" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.estilo" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.modelo" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.primatotal" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.cargamasivaserviciopublico.adjunto.estatus" />  </column>
          
          

	</head> --%>
	<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="detalle">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="idDetalle" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="poliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cliente" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numSerie" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estilo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="modelo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property  value="%{getText('struts.money.format',{primaTotal})}" escapeHtml="false" escapeXml="true"  /></cell>
		    <cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
		    <s:if test="estatus=='ERROR' || estatus=='ERROR_COTIZACION' || estatus=='ERROR_POLIZA' || estatus=='POR_AUTORIZAR' || estatus == 'ERROR_EMISION' ">
		    	<cell>../img/icons/ico_verdetalle.gif^Resumen^javascript:consultarErrorDetalle(<s:property value="idDetalle" />);^_self</cell>
		    </s:if>
		    <s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>	
	</rows>