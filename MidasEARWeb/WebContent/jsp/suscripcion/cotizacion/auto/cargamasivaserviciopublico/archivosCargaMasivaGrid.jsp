<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="listArchivosAdjuntos" status="index">
		<row>
			<cell><s:property value="idArchivoCargaMasiva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreArchivoCargaMasiva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaCreacion" format="dd/MM/yyyy HH:mm" /></cell>	
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_verdetalle.gif^Resumen^javascript:mostrarResumenCargaMasivaServicioPublico(<s:property value="idArchivoCargaMasiva" />);^_self</cell>
		</row>
	</s:iterator>
</rows>