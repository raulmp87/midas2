<%@ taglib prefix="s" uri="/struts-tags"%>
<s:if test="datosCotizacionExpressDTO.totalEncabezados > 0 && datosCotizacionExpressDTO.totalFilas > 0">
<table id="t_riesgo">
	<tr>
		<th>&nbsp;</th>
	<s:iterator value="datosCotizacionExpressDTO.encabezados" status="statsE">
		<th style="font-size:small;">							
			<input type="checkbox" id="encabezado_<s:property value="%{#statsE.count}" />"  checked="checked"  name="formaPagoMatriz"
			value="<s:property value="formaPagoDTO.idFormaPago" />" 
			onclick="clickColumnaExpress(<s:property value="%{#statsE.count}" />, <s:property value="%{datosCotizacionExpressDTO.totalFilas}" />)"/>
			<s:property value="formaPagoDTO.descripcion" />
		</th>
	</s:iterator>
	</tr>
	<s:iterator value="datosCotizacionExpressDTO.fila" status="statsF">
		<tr>
			<th align="left" style="font-size:small;">
			<input type="checkbox" id="fila_<s:property value="%{#statsF.count}" />"  name="paquetesMatriz"
			value="<s:property value="paquete.id" />"  checked="checked"
			onclick="clickFilaExpress(<s:property value="%{#statsF.count}" />, <s:property value="%{datosCotizacionExpressDTO.totalEncabezados}" />)"/>	
			<s:property value="paquete.descripcion" />							
			</th>
			<s:iterator value="datosCotizacionExpressDTO.encabezados" status="stats">
			<td class="txt_v" style="font-size: xx-small;">
				<s:div id="cell_%{#statsF.count}_%{#stats.count}">
					<a href="javascript: void(0);" onclick="mostrarResumenCotizacionExpress(<s:property value="paquete.id"/>, <s:property value="formaPagoDTO.idFormaPago"/>)">	
						<s:text name="midas.cotizacion.primatotal"/><br/>
							<s:property value="getText('struts.money.format', {cotizacionExpressDTO.getCotizacionExpress(paquete, formaPagoDTO).primaTotal})" default="default" />	
					</a>
				</s:div>								
			</td>
			</s:iterator>
		</tr>
	</s:iterator>
</table>
</s:if>