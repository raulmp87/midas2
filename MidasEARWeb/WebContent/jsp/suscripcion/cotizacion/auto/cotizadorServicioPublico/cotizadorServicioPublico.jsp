<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%> 
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/negocio/copiarNegocioHeader.jsp"></s:include>

<s:include value="/jsp/suscripcion/cotizacion/auto/cotizadorServicioPublico/cotizadorServicioPublicoHeader.jsp"></s:include>

<s:form action="guardar" id="altaFoliosForm" name="altaFoliosReexpedibleForm">
	
	<s:hidden name="idToCotizacion" id="idToCotizacion"/>
	<s:hidden name="codigoAgente" id="codigoAgente"/>
	<s:hidden name="esLineaAutobuses" id="esLineaAutobuses"/>
	<s:if test="banderaFolio">
		<table style="width: 100%;" border="0">
			<tr>
				<td class="titulo" colspan="4">
					Cotizador Tarjeta Pre Configurada
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="mensajes" style="display: none;">
						<div id="textMessage" >  </div>
					</div>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"> </th> <th style="width: 33%;"> </th> <th style="width: 33%;"> </th>
			</tr>
		</table>
	</s:if>
	<s:else>
		<table border="0">
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.cotizador.servicioPublico" /> 
				</td>
			</tr>
		</table>
	</s:else>
	<table id="desplegarDetalle" border="0">
		<s:if test="banderaFolio">
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.folio.reexpedible.field.rango.folio"/>
				</th>
				<th style="width: 33%;"> </th> <th style="width: 33%;"> </th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:textfield id="numeroFolio" name="filtroFolioRe.folioInicio" cssClass="cajaTexto"  
							onfocus="javascript: new Mask('****_##########', 'string').attach(this)" />
				</td>
				<td style="width: 33%;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a id="submit" onclick="buscarFolioVenta()" href="javascript: void(0);"> 
							<s:text name="midas.boton.buscar" />
						</a>
					</div>
				</td>
				<td style="width: 33%;"></td>
			</tr>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.general.negocio" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.suscripcion.cotizacion.producto" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.seccion.tiposPoliza" />
				</th>
			</tr>
			<tr>
				<td style="width: 34%;">  
	 				<s:select cssClass="cajaTexto" disabled="true"
	 					name="cotizacion.SolicitudDTO.negocio.idToNegocio" id="idToNegocio" 
	 					list="negocioList" listKey="idToNegocio" 
	 					listValue="descripcionNegocio"
	 					value="%{idToNegocio}" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}"
	 					onchange="obtenerProductos(this.value);"/> 
	 			</td>
				<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" disabled="true"
	 					name="cotizacion.NegocioTipoPoliza.NegocioProducto.IdToNegProducto" id="idToNegProducto" 
	 					list="negocioProductoList" listKey="idToNegProducto" 
	 					listValue="productoDTO.descripcion" 
	 					headerKey="-1"  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 					onchange="obtenerTiposPoliza(this.value);obtenerEstados(%{idToNegocio});"/> 
	 			</td> 
				<td style="width: 33%;">
					<s:select id="idToTipoPoliza" cssClass="cajaTexto" disabled="true"
						list="{}"  
						name="cotizacion.NegocioTipoPoliza.IdToNegTipoPoliza"
						listKey="idToNegTipoPoliza" headerKey="-1"
						listValue="tipoPolizaDTO.descripcion"					 
						headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
						OnChange="obtenerLineasDeNegocio(this.value); obtenerMonedas(this.value); obtenerVigencias();" />
				</td>					
			</tr>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.negocio.seccion.disponibles" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.paquete" /> 
				</th>
				<th style="width: 33%;">
					<s:text name="midas.general.moneda" /> 
				</th>
			</tr>
			<tr>
				<td style="width: 34%;"> 
					<s:select id="idToNegSeccion" cssClass="cajaTexto"  
						name="incisoCotizacion.IncisoAutoCot.NegocioSeccionId"  
						list="{}" listKey="idToNegSeccion" listValue=""  
						OnChange="obtenerPaquetes(this.value);" 
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td>
				<td style="width: 33%;">
					<s:select name="incisoCotizacion.IncisoAutoCot.negocioPaqueteId" id="idToNegPaqueteSeccion"  
							  list="{}" listKey="idToNegPaqueteSeccion" listValue=""  cssClass="cajaTexto" 
							  OnChange="onChangeComboPaquete(this.value);" headerKey="-1" 
							  headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td> 
				<td style="width: 33%;">
					<s:select name="cotizacion.IdMoneda" id="idTcMoneda"  cssClass="cajaTexto" 
							  list="{}" listKey="idTcMoneda" listValue=""  disabled="true"
							  OnChange="onChangeComboMoneda(this.value);" headerKey="-1"  
							  headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td> 
	 		</tr>
			<tr> 
				<th style="width: 34%;"> 
					 Vigencia 
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.estado" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.municipio" />
				</th>
			</tr>
	 		<tr>
	 			<td style="width: 34%;">  
	 				<s:select cssClass="cajaTexto" name="idTcVigencia" id="idTcVigencia" 
	 					list="{}" listKey="idTcVigencia" 
	 					listValue="descripcion" disabled="true"
	 					headerKey="-1"  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 					onchange="onChangeComboVigencia(this.value);"/> 
	 			</td> 
	 			<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" 
	 					id="stateId" name="incisoCotizacion.IncisoAutoCot.EstadoId" 
	 					disabled="true" list="{}" listKey="stateId" 
	 					listValue="stateName" 
	 					OnChange="obtieneMunicipio(this.value);" headerKey=""  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 				/> 
	 			</td> 
	 			<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" 
	 					id="cityId" name="incisoCotizacion.incisoAutoCot.municipioId"  
	 					disabled="true" list="{}" listKey="cityId" 
	 					listValue=""  headerKey="" 
	 					OnChange="onChangeComboMunicipio(this.value);initGridsTarifaServicioPublico();"  
	 					headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td>
			</tr>
			<tr>
				<td colspan="4">
					<div style="display:none">
						<label><s:text name="midas.suscripcion.cotizacion.agentes.seleccionarAgente"/>:</label>
						<s:textfield id="descripcionBusquedaAgente" cssClass="form-control" />
						<s:hidden name="cotizacion.solicitudDTO.codigoAgente" id="idAgente" cssClass="mandatory"/>
					</div>
				</td>
			</tr>
			
			
		</s:if>
		<s:else>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.general.negocio" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.suscripcion.cotizacion.producto" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.seccion.tiposPoliza" />
				</th>
			</tr>
			<tr>
				<td style="width: 34%;">  
	 				<s:select cssClass="cajaTexto" 
	 					name="cotizacion.SolicitudDTO.negocio.idToNegocio" id="idToNegocio" 
	 					list="negocioList" listKey="idToNegocio" 
	 					listValue="descripcionNegocio"
	 					value="%{idToNegocio}"
	 					onchange="obtenerProductos(this.value);"/> 
	 			</td> 
				<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" 
	 					name="cotizacion.NegocioTipoPoliza.NegocioProducto.IdToNegProducto" id="idToNegProducto" 
	 					list="negocioProductoList" listKey="idToNegProducto" 
	 					listValue="productoDTO.descripcion" 
	 					headerKey="-1"  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 					onchange="obtenerTiposPoliza(this.value);obtenerEstados(%{idToNegocio});"/> 
	 			</td> 
				<td style="width: 33%;">
					<s:select id="idToTipoPoliza" cssClass="cajaTexto" 
						list="{}"  
						name="cotizacion.NegocioTipoPoliza.IdToNegTipoPoliza"
						listKey="idToNegTipoPoliza" headerKey="-1"
						listValue="tipoPolizaDTO.descripcion"					 
						headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
						OnChange="obtenerLineasDeNegocio(this.value); obtenerMonedas(this.value); obtenerVigencias();" />
				</td>					
			</tr>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.negocio.seccion.disponibles" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.paquete" /> 
				</th>
				<th style="width: 33%;">
					<s:text name="midas.general.moneda" /> 
				</th>
			</tr>
			<tr>
				<td style="width: 34%;"> 
					<s:select id="idToNegSeccion" cssClass="cajaTexto"  
						name="incisoCotizacion.IncisoAutoCot.NegocioSeccionId"  
						list="{}" listKey="idToNegSeccion" listValue=""  
						OnChange="obtenerPaquetes(this.value);" 
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td>
				<td style="width: 33%;">
					<s:select name="incisoCotizacion.IncisoAutoCot.negocioPaqueteId" id="idToNegPaqueteSeccion"  
							  list="{}" listKey="idToNegPaqueteSeccion" listValue=""  cssClass="cajaTexto" 
							  OnChange="onChangeComboPaquete(this.value);" headerKey="-1" 
							  headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td> 
				<td style="width: 33%;">
					<s:select name="cotizacion.IdMoneda" id="idTcMoneda"  cssClass="cajaTexto" 
							  list="{}" listKey="idTcMoneda" listValue=""  
							  OnChange="onChangeComboMoneda(this.value);" headerKey="-1"  
							  headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td> 
	 		</tr>
			<tr> 
				<th style="width: 34%;"> 
					 Vigencia 
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.estado" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.municipio" />
				</th>
			</tr>
	 		<tr>
	 			<td style="width: 34%;">  
	 				<s:select cssClass="cajaTexto" name="idTcVigencia" id="idTcVigencia" 
	 					list="{}" listKey="idTcVigencia" 
	 					listValue="descripcion" 
	 					headerKey="-1"  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 					onchange="onChangeComboVigencia(this.value);"/> 
	 			</td> 
	 			<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" 
	 					id="stateId" name="incisoCotizacion.IncisoAutoCot.EstadoId" 
	 					disabled="true" list="{}" listKey="stateId" 
	 					listValue="stateName" 
	 					OnChange="obtieneMunicipio(this.value);" headerKey=""  
	 					headerValue="%{getText('midas.general.seleccione')}"
	 				/> 
	 			</td> 
	 			<td style="width: 33%;">
	 				<s:select cssClass="cajaTexto" 
	 					id="cityId" name="incisoCotizacion.incisoAutoCot.municipioId"  
	 					disabled="true" list="{}" listKey="cityId" 
	 					listValue=""  headerKey="" 
	 					OnChange="onChangeComboMunicipio(this.value);initGridsTarifaServicioPublico();"  
	 					headerValue="%{getText('midas.general.seleccione')}"/> 
	 			</td>
			</tr>
			<tr>
				<td colspan="4">
					<div style="display:none">
						<label><s:text name="midas.suscripcion.cotizacion.agentes.seleccionarAgente"/>:</label>
						<s:textfield id="descripcionBusquedaAgente" cssClass="form-control" />
						<s:hidden name="cotizacion.solicitudDTO.codigoAgente" id="idAgente" cssClass="mandatory"/>
					</div>
				</td>
			</tr>
		</s:else>
	</table>
	<div>
		<div id="coberturasObligatoriasGrid" style="width: 800px; height: 100px;"></div>
		<br/>
		<div id="coberturasAdicionalesGrid" style="width: 800px; height: 100px"></div>
		<br/>
		<div id="sumasAseguradasAdicionalesGrid" style="width: 800px; height: 100px"></div>
		<br/>
		<div id="deduciblesAdicionalesGrid" style="width:800px; height: 100px"></div>
	</div>
</s:form>				
<div style="display:none">
	<a id="nuevoCotizadorAgentes" target="_blank"></a>
</div>