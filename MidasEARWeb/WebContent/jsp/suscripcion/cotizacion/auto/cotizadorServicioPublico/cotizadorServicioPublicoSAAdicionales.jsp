<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="enableAutoHeight"><param>true</param><param>200</param></call>
			<call command="enableAutoWidth"><param>true</param><param>800</param><param>800</param></call>
		</beforeInit>
		<column id="tarifaServicioPublicoSumaAseguradaAd.id" type="ro" width="0" sort="na" hidden="true">id</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.coberturaDTO.idToCobertura" type="ro" width="0" sort="na" hidden="true">idToCobertura</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.idSumaAseguradaAd" type="ro" width="0" sort="na" hidden="true">numeroSecuencia</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.negocioPaqueteSeccion.idToNegPaqueteSeccion" type="ro" width="0" sort="na" hidden="true">idToNegPaqueteSeccion</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.monedaDTO.idTcMoneda" type="ro" width="0" sort="na" hidden="true">idTcMoneda</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.estadoDTO.stateId" type="ro" width="0" sort="na" hidden="true">stateId</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.ciudadDTO.cityId"  type="ro" width="0" sort="na" hidden="true">cityId</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.coberturaDTO.descripcion" type="ro" width="400" sort="str" >Sumas Aseguradas Adicionales</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.valorSumaAsegurada" format="$0,000.00" type="edn" width="200" align="right" sort="int" ></column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.prima" format="$0,000.00" type="ron" width="100" align="right" sort="int" >Prima</column>
		<column id="tarifaServicioPublicoSumaAseguradaAd.vigenciaDTO.idTcVigencia" type="ro" width="0" align="right" sort="na" hidden="true">idTcVigencia</column>
		<column id="seleccionado" type="ch"  width="70" align="center" sort="int">Contratada</column>
	</head>
	<s:iterator value="relacionesTarifaServicioPublicoDTO.sumasAseguradasAdicionales" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.idToCobertura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="idSumaAseguradaAd" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioPaqueteSeccion.idToNegPaqueteSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.idTcMoneda" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciudadDTO.cityId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="valorSumaAsegurada" escapeHtml="false" /></cell>
			<cell><s:property value="prima" escapeHtml="false" /></cell>
			<cell><s:property value="vigenciaDTO.idTcVigencia" escapeHtml="false" /></cell>
			<cell><s:if test="seleccionado">1</s:if><s:else>0</s:else></cell>
		</row>
	</s:iterator>
</rows>