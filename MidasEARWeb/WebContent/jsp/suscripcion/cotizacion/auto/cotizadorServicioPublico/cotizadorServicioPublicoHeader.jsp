<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/cotizadorServicioPublico/cotizadorServicioPublico.js'/>"></script>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>


<script type="text/javascript">
 	var obtenerCoberturasObligatoriasGridPath = '<s:url action="obtenerCoberturasObligatorias" namespace="/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico"/>';
    var obtenerCoberturasAdicionalesGridPath = '<s:url action="obtenerCoberturasAdicionales" namespace="/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico"/>';
    var obtenerSumasAseguradasAdicionalesGridPath = '<s:url action="obtenerSumasAseguradasAdicionales" namespace="/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico"/>';
    var obtenerDeduciblesAdicionalesGridPath = '<s:url action="obtenerDeduciblesAdicionales" namespace="/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico"/>';

    var crearCotizacionServicioPublicoPath = '<s:url action="generarCotizacionServicioPublico" namespace="/suscripcion/cotizacion/auto/agentes/cotizadorServicioPublico"/>';
    var urlBusquedaAgentes = '<s:url action="buscarAgente" namespace="/suscripcion/cotizacion/auto/agentes"/>';
</script>


