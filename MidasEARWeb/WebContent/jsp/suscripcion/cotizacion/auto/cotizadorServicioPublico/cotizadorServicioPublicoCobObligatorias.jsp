<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="enableAutoHeight"><param>true</param><param>250</param></call>
			<call command="enableAutoWidth"><param>true</param><param>800</param><param>800</param></call>
		</beforeInit>
		<column id="tarifaServicioPublico.id" type="ro" width="0" sort="na" hidden="true">idToTarifaServicioPublico</column>
		<column id="tarifaServicioPublico.coberturaDTO.idToCobertura" type="ro" width="0" sort="na" hidden="true">idToCobertura</column>
		<column id="tarifaServicioPublico.negocioPaqueteSeccion.idToNegPaqueteSeccion" type="ro" width="0" sort="na" hidden="true">idToNegPaqueteSeccion</column>
		<column id="tarifaServicioPublico.monedaDTO.idTcMoneda" type="ro" width="0" sort="na" hidden="true">idTcMoneda</column>
		<column id="tarifaServicioPublico.estadoDTO.stateId" type="ro" width="0" sort="na" hidden="true">stateId</column>
		<column id="tarifaServicioPublico.ciudadDTO.cityId"  type="ro" width="0" sort="na" hidden="true">cityId</column>
		<column id="tarifaServicioPublico.coberturaDTO.descripcion" type="ro" width="400" sort="str" >Coberturas Obligatorias</column>
		<column id="tarifaServicioPublico.prima" type="ro" width="0" sort="na" hidden="true">prima</column>
		<column id="tarifaServicioPublico.valorSumaAsegurada" type="ro" width="300" align="right" sort="str" >Suma Asegurada</column>
		<column id="seleccionado" type="ch"  width="80" align="center" sort="int">Contratada</column>
	</head>
	<s:iterator value="relacionesTarifaServicioPublicoDTO.obligatorias" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.idToCobertura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="negocioPaqueteSeccion.idToNegPaqueteSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.idTcMoneda" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciudadDTO.cityId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="prima" escapeHtml="false" /></cell>
			<cell><s:property value="valorSumaAsegurada" escapeHtml="false" /></cell>
			<cell>1</cell>
		</row>
	</s:iterator>
</rows>