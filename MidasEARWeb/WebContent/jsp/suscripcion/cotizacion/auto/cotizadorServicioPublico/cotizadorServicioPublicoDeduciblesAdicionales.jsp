<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="enableAutoHeight"><param>true</param><param>400</param></call>
			<s:if test="esLineaAutobuses==1">
				<call command="enableAutoWidth"><param>true</param><param>1020</param><param>1020</param></call>
			</s:if>
			<s:else>
				<call command="enableAutoWidth"><param>true</param><param>800</param><param>800</param></call>
			</s:else>
		</beforeInit>
		<column id="tarifaServicioPublicoDeduciblesAd.id" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="tarifaServicioPublicoDeduciblesAd.coberturaDTO.idToCobertura" type="ro" width="0" sort="na" hidden="true">idToCobertura</column>
		<column id="tarifaServicioPublicoDeduciblesAd.negocioPaqueteSeccion.idToNegPaqueteSeccion" type="ro" width="0" sort="na" hidden="true">idToNegPaqueteSeccion</column>
		<column id="tarifaServicioPublicoDeduciblesAd.monedaDTO.idTcMoneda" type="ro" width="0" sort="na" hidden="true">idTcMoneda</column>
		<column id="tarifaServicioPublicoDeduciblesAd.estadoDTO.stateId" type="ro" width="0" sort="na" hidden="true">stateId</column>
		<column id="tarifaServicioPublicoDeduciblesAd.ciudadDTO.cityId"  type="ro" width="0" sort="na" hidden="true">cityId</column>
		<column id="tarifaServicioPublicoDeduciblesAd.negocioDeducibleCob.id" type="ro" width="0" sort="na" hidden="true">deducibleId</column>
		<column id="tarifaServicioPublicoDeduciblesAd.negocioDeducibleCob.valorDeducible" type="ro" width="400" sort="int" >Deducible</column>
		<column id="tarifaServicioPublicoDeduciblesAd.claveSumaAsegurada" type="ro" width="0" align="right" sort="na" hidden="true">claveSumaAsegurada</column>
		<column id="tarifaServicioPublicoDeduciblesAd.valorSumaAsegurada" format="$0,000.00" type="ron" width="0" align="right" sort="int" hidden="true">valorSumaAsegurada</column>
		<column id="tarifaServicioPublicoDeduciblesAd.valorSumaAseguradaStr" type="ro" width="0" align="right" sort="na" hidden="true">valorSumaAseguradaStr</column>
		<s:if test="esLineaAutobuses==1">
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible1" format="$0,000.00" type="link" width="100" align="right" sort="int" >25 Ocupantes</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible2" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 26 a 29</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible3" format="$0,000.00" type="link" width="100" align="right" sort="int" >30 Ocupantes</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible4" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 31 a 35</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible5" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 36 a 40</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible6" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 41 a 45</column>
		</s:if>
		<s:else>
		    <column id="tarifaServicioPublicoDeduciblesAd.valorDeducible1" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 03 a 05</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible2" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 06 a 15</column>
			<column id="tarifaServicioPublicoDeduciblesAd.valorDeducible3" format="$0,000.00" type="link" width="100" align="right" sort="int" >De 16 a 23</column>
		</s:else>
		<column id="tarifaServicioPublicoDeduciblesAd.vigenciaDTO.idTcVigencia" type="ro" width="0" align="right" sort="na" hidden="true">idTcVigencia</column>
	</head>
	<s:iterator value="relacionesTarifaServicioPublicoDTO.deduciblesAdicionales" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.idToCobertura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="negocioPaqueteSeccion.idToNegPaqueteSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.idTcMoneda" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciudadDTO.cityId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioDeducibleCob.id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocioDeducibleCob.valorDeducible" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="claveSumaAsegurada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAsegurada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaStr" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="esLineaAutobuses==1">
				<cell><s:property value="valorDeducible1" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible1" escapeHtml="false" />", 4, <s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible2" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible2" escapeHtml="false" />", 5,<s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible3" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible3" escapeHtml="false" />", 6,<s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible4" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible4" escapeHtml="false" />", 7, <s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible5" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible5" escapeHtml="false" />", 8,<s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible6" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible6" escapeHtml="false" />", 8,<s:property value="#row.index"/>);^_top</cell>
			</s:if>
			<s:else>
				<cell><s:property value="valorDeducible1" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible1" escapeHtml="false" />", 1, <s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible2" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible2" escapeHtml="false" />", 2,<s:property value="#row.index"/>);^_top</cell>
				<cell><s:property value="valorDeducible3" escapeHtml="false" escapeXml="true"/>^javascript:crearCotizacionServicioPublico("<s:property value="valorDeducible3" escapeHtml="false" />", 3,<s:property value="#row.index"/>);^_top</cell>
			</s:else>
			<cell><s:property value="vigenciaDTO.idTcVigencia" escapeHtml="false" /></cell>
		</row>
	</s:iterator>
</rows>