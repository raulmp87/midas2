<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.numeric.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>		
<script type="text/javascript">		
		function igualarMasivoEndoso(){
			//parent.submitVentanaModal("igualarMasivo", document.formIgualarMasivo);
			var form = jQuery('#formIgualarMasivo')[0];
			var igualarMasivoPath = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/igualarMasivo.action";
			form.setAttribute("action", "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/igualarMasivo.action");
			parent.redirectVentanaModal("igualarMasivo", igualarMasivoPath, form);	
		}
		
</script>
<s:form theme="simple" id="formIgualarMasivo" action="/">	
	<s:hidden name="validoEn" id="validoEn"/>
	<s:hidden name="tipoEndoso" id="tipoEndoso"/>
	
	<div id="loading" style="display: none;">
                <img id="img_indicator" name="img_indicator"
                               src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
	</div>
	
	<div id="detalle">
		<center>
			<table id="agregar" width="100%">
				<tr>
					<td align="center"><s:text name="midas.cotizacion.numeroinciso"/>:</td>
					<td><s:text name="midas.suscripcion.cotizacion.igualacionPrimas.valorTotalIgualar"/></td>
				</tr>
				<s:iterator value="incisosInProcessList" status="stat">
							<tr>
								<td align="center">
									<s:hidden name="incisosInProcessList[%{#stat.index}].continuity.id" value="%{continuity.id}"/>
									<s:hidden name="incisosInProcessList[%{#stat.index}].id" value="%{id}" />
									<s:hidden name="incisosInProcessList[%{#stat.index}].continuity.numero" value="%{continuity.numero}" /> 
									<s:property value="%{continuity.numero}" />: 
								</td>
								<td><s:textfield cssClass="numeric" id="bitemporalInciso.value.valorPrimaIgualacion_%{#stat.index}" name="incisosInProcessList[%{#stat.index}].value.valorPrimaIgualacion"></s:textfield></td>
							</tr>
				</s:iterator>
				<tr>
					<td colspan="2">
						<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.igualacionMasiva.texto"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="btnIgualar"  style="display: inline; float: right;" class="btn_back w150">
							<a href="javascript: void(0);" onclick="igualarMasivoEndoso();">
								<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.igualar"/>
							</a>
						</div>
					</td>
				</tr>
			</table>
		</center>
	</div>
</s:form>
