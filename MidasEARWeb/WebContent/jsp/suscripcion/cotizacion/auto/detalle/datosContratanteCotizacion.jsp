<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="row">
	<div class="c6 titulo">
		<s:text name="midas.cotizacion.datoscontratante" />
	</div>
</div>
<br/>
<div class="row">	
	<div class="c3">
		<s:hidden name="cotizacionDTO.idToPersonaContratante"></s:hidden>
		<s:textfield name="cliente.nombreCliente" readonly="true"
			key="midas.cotizacion.nombrecontratante" size="45"
			cssStyle="max-width:242;left:-10px;"
			labelposition="top" cssClass="txtfield" theme="simple" />
	</div>
</div>
<br/>
<div class="row">
	<div class="c3 s2">
		<s:if test="soloConsulta == 0">
		<div class="btn_back w170">
			<a href="javascript: void(0);"
				onclick="buscarCliente(seleccionarCliente);" class="icon_cliente">
				<s:text name="midas.cotizacion.buscarcliente" /> </a>
		</div>
		</s:if>
	</div>
</div>
