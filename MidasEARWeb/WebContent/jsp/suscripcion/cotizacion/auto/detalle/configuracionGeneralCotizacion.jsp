<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/validarCambios.js'/>"></script>

<script type="text/javascript">
	var verDetalleIncisoPath = '<s:url action="verDetalleInciso" namespace="/vehiculo/inciso"/>';
	var verEsquemaPagoPath = '<s:url action="verEsquemaPago" namespace="/suscripcion/cotizacion/auto"/>';
	var datosVehiculoPath = '<s:url action="mostrar" namespace="/suscripcion/cotizacion/auto/complementar/inciso"/>';
	var idContratante = '<s:property value="cotizacion.idToPersonaContratante"/>';
	jQuery(document).ready(function(){
		// Valida el estado de los campos
		function validaCampos(){
			var arrayCampos = new Array({id:"comisionCedida"},{id:"formaPago"},{id:"derechos"},{id:"descuentoGlobal"}
									   ,{id:"fecha"},{id:"fechados"},{id:"porcentajePagoFraccionado"},{id:"fechaSeguimiento"},{id:"nombreContratante"});
			for(x in arrayCampos){
				jQuery('#' + arrayCampos[x].id).cambio();
			}
		}
		validaCampos();
	});
</script>
<s:if test="cotizacion.porcentajeDescuentoGlobal > 0">
		<script type="text/javascript">
			// Asigna el descuento global
			jQuery('#descuentoGlobal').val(<s:property value="cotizacion.porcentajeDescuentoGlobal"/>);
		</script>
</s:if>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<s:form id="cotizacionForm">	
<s:hidden name="fechaOperacionCompareTo" id="mensajeFechaOperacion" />
<s:hidden type="hidden" name="cotizacion.idMoneda" id="idMoneda" />
	<table width="98%">
		<tr>
			<td width="50%">
				<s:include value="/jsp/suscripcion/cotizacion/auto/detalle/datosNegocioCotizacion.jsp"></s:include>
			</td>
			<td width="50%">
				<s:action name="getInfoAgente" var="agenteInfo" namespace="/componente/agente" ignoreContextParams="true" executeResult="true" >
					<s:param name="idTcAgenteCompName">cotizacion.solicitudDTO.codigoAgente</s:param>
					<s:param name="nombreAgenteCompName">agente.nombre</s:param>
					<s:param name="permiteBuscar">false</s:param>		
				</s:action>	
			</td>
		</tr>
	</table>
	<div  style="width: 98%; margin-right: .3em;">
		<s:hidden name="cotizacion.idToCotizacion" id="idToCotizacion"/>
		<s:if test="mensajeDTO.mensaje!=null">
		  <b><s:property value="mensajeDTO.mensaje"/></b>
		</s:if>
			
		<div id="esquemadePago" class="alinearBotonALaDerecha" style="display: none">
			<s:if test="cotizacion.tipoPolizaDTO.claveAplicaFlotillas == 1 && soloConsulta == 0">
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cambios_Globales">
			<div id="cambiosGlobales" class="btn_back w140" style="display: block; float: right;">
				<a href="javascript: void(0);" onclick="mostrarVentanaCambiosGlobales(<s:property value='cotizacion.idToCotizacion' />,'verDetalleCotizacionFormal(<s:property value='cotizacion.idToCotizacion' />)');"
					class="icon_global"> <s:text
						name="midas.cotizacion.cambiosglobales" /> </a>
	   		</div>
	   		</m:tienePermiso>
			</s:if>
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Formas_Pago">
			<div class="btn_back w170" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="verEsquemaPago();"
					class="icon_esquemaPagos"> <s:text
						name="midas.cotizacion.veresquemapago" /> </a>
			</div>
			</m:tienePermiso>
	
			<s:if test="soloConsulta == 0">
			<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Igualacion_Prima">
			<div class="btn_back w170" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="verIgualarPrimas(jQuery('#idToCotizacion').val());"
					class="icon_global"> <s:text name="midas.cotizacion.verigualarprimas" /> </a>
			</div>
			</m:tienePermiso>
			</s:if>
		</div>
	</div>
<div class="clear"></div>
	<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" class="fixTabla">
		<tr>
			<td valign="top" width="60%">
				<table id="agregar">
					<tr>
						<td colspan="2">
							<s:select name="cotizacion.negocio.tiposvigencia" id="idTipoVigencia"						
							 key="midas.cotizacion.tipovigencia" cssClass="txtfield"
							 headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							 list="tiposVigenciaList" 
							 disabled="%{#disabledConsulta}"
							 value="vigenciaDefault"
							 onchange="actualizarFechas(this.value);"  >
							</s:select>
						</td>
					</tr>
					<tr>
						<td>
							<sj:datepicker name="cotizacion.fechaInicioVigencia" required="#requiredField" 
					           labelposition="top" 
					           key="midas.cotizacion.iniciovigencia"
					           size="10"
					           changeMonth="true"					           
					           changeYear="true"					           
					           readonly="readonly"	
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
					           cssClass="txtfield" cssStyle="width: 175px;"
							   buttonImage="../img/b_calendario.gif"
							   id="fecha" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onchange="ajustaFechaFinal();"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>						
						</td>
						<td>
							<sj:datepicker name="cotizacion.fechaFinVigencia" required="#requiredField"
					           labelposition="top"
							   buttonImage="../img/b_calendario.gif"
							   key="midas.cotizacion.finvigencia"
							   changeMonth="true"
					           changeYear="true"					          
							   cssClass="txtfield" cssStyle="width: 175px;"
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
							   size="10"
							   id="fechados" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>						
						</td>
					</tr>
					<tr>
						<td>
							<s:select  id="formaPago"  cssClass="txtfield"
				         		key="midas.cotizacion.formapago"
								list="formasdePagoList"
								name="cotizacion.idFormaPago"
					    		labelposition="top" headerKey="-1"
					           	disabled="%{#disabledConsulta}"
					    		onchange="cargarRecargoPagoFraccionado(this.value, jQuery('#idMoneda').val());"
								headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
						</td>
						<td>
						<s:if test="cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado()">
								<s:textfield name="cotizacion.porcentajePagoFraccionado" required="#requiredField"
						               key="midas.cotizacion.porcentajePagoFraccionado"
							           labelposition="top"
					           			disabled="%{#disabledConsulta}"
									   id="porcentajePagoFraccionado" maxlength="5" cssClass="txtfield" cssStyle="width: 200px;" 						   								  
									   onkeypress="return soloNumeros(this, event, true)">
							</s:textfield>
						</s:if>
						<s:else>
							<s:hidden id="porcentajePagoFraccionado" name="cotizacion.porcentajePagoFraccionado"></s:hidden>
						</s:else>
								
						</td>
					</tr>
					<tr>
						<td>
							<s:select  id="derechos"  cssClass="txtfield"
						     key="midas.cotizacion.derechos"
						        labelposition="top" 						    
								list="derechosPolizaList"
								listValue="%{(new java.text.DecimalFormat('0.00')).format(importeDerecho)}"
								listKey="idToNegDerechoPoliza"
					           	disabled="%{#disabledConsulta}"
								name="cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza"
								headerKey=""
								headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>						
						</td>
													
					</tr>
					<tr>
						<td>
							<s:textfield name="cotizacion.porcentajebonifcomision"
								required="#requiredField" key="midas.cotizacion.comisioncedida"
								labelposition="top" id="comisionCedida" maxlength="5"
								cssClass="txtfield" cssStyle="width: 200px;"
					           	disabled="%{#disabledConsulta}"
								onkeypress="return soloNumeros(this, event, true);"
								onchange="validaComisionCedida(this.value,100,0,'')">
							</s:textfield></td>
					</tr>
					<tr>
						<td>
							<sj:datepicker name="cotizacion.fechaSeguimiento" required="#requiredField" 
					           labelposition="top" 
					           key="midas.cotizacion.fechaSeguimiento"
					           size="10"
					           changeMonth="true"
					           changeYear="true"
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
					           cssClass="txtfield" cssStyle="width: 175px;"
							   buttonImage="../img/b_calendario.gif"
							   id="fechaSeguimiento" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>		
						</td>
						<td>
							<s:textfield name="cotizacion.nombreContratante"
						               key="midas.suscripcion.cotizacion.nombreProspecto"
							           labelposition="top" 		
					           			disabled="%{#disabledConsulta}"				 							   
									   id="nombreContratante" maxlength="100" 
									   cssClass="txtfield" cssStyle="width: 200px;">
							</s:textfield>		
						</td>
					</tr>
					<tr>
						<td>
							<div  style="font-weight:bold;">
								<s:text name="midas.cotizacion.descuentoporvolumen"/>: <s:property value="cotizacion.descuentoVolumen"/> %
							</div>	
						</td>						
					</tr>
				</table>          					   	  										
			</td>
			<td valign="top">
				<table style=" width:100%; padding: 5px; margin: 5px;" border="0">
					<tr >
						<td colspan="2" valign="top" align="right">
							<div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid"></div>
						</td>
					</tr>
				</table>			
			</td>				
		</tr>
		<tr>
			<td colspan="2">
				<s:if test="soloConsulta == 0">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Terminar">
				<div id="terminarCotizacion" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="terminarCotizacion(<s:property value='cotizacion.idToCotizacion'/>);"> <s:text
						name="midas.cotizacion.terminarCotizacion" /> </a>
				</div>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Guardar">
				<div id="guardarInciso" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="guardarCotizacion();"
						class="icon_guardar"> <s:text name="midas.cotizacion.guardar" />
					</a>
				</div>
				</m:tienePermiso>
		      	<s:if test="cotizacion.idFormaPago > 0 && cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza > 0">
		      	<!-- Boton Carga Masiva  -->
		      	<s:if test="cotizacion.tipoPolizaDTO.claveAplicaFlotillas == 1">
		      	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Carga_Masiva">
				<div id="cargaMasiva" class="btn_back w140" style="display: block; float: right;">
						<a href="javascript: void(0);" onclick="abrirCargaMasiva(<s:property value='cotizacion.idToCotizacion'/>,2);" class="icon_cargaMasiva">	
							<s:text name="midas.cotizacion.cargamasiva"/>	
						</a>
		      	</div>
		      	</m:tienePermiso>
		      	</s:if>
		      	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Agregar">
			 	<div id="agregarInciso" class="btn_back w140" style="display: none; float: right;">
					<a href="javascript: void(0);" onclick="agregarInciso();" class="icon_masAgregar">	
						<s:text name="midas.cotizacion.agregarinciso"/>	
					</a>
		      	</div>
		      	</m:tienePermiso>		
		      	</s:if>
		      	</s:if>
				<s:if test="soloConsulta == 1 && (cotizacion.claveEstatus == 12 || cotizacion.claveEstatus == 16)">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Complementar">
				<div id="complementarConsulta" class="btn_back w160" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="verComplementarCotizacionConsulta(<s:property value='cotizacion.idToCotizacion'/>);"> <s:text
						name="midas.suscripcion.cotizacion.auto.complementar.complementarCotizacion" /> </a>
				</div>
				</m:tienePermiso>
				</s:if>
			</td>
		</tr>
	</table>
</s:form>
<!-- Valida el boton de temrinar cotizacion -->
<s:if test="cotizacion.incisoCotizacionDTOs.size() == 0">
	 	<script type="text/javascript">
	 		jQuery('div [id=terminarCotizacion]').css('display','none');
	 	</script>
</s:if>

<!-- Cotizacion flotilla -->
 <s:if test="cotizacion.tipoPolizaDTO.claveAplicaFlotillas == 1">
	 <div id ="listadoDeIncisos" style="height: 400px; float:both;" class="agregar">
		<s:action name="mostrarContenedor" namespace="/vehiculo/inciso" ignoreContextParams="true" executeResult="true" >
			<s:param name="incisoCotizacion.id.idToCotizacion"><s:property value="cotizacion.idToCotizacion"/></s:param>
			<s:param name="soloConsulta"><s:property value="soloConsulta"/></s:param>
		</s:action>	
	 </div>
 </s:if>
 <!-- Cotizacion individual -->
 <s:else>
   <script type="text/javascript">
						jQuery('div [id=esquemadePago]').css('display','block');
						jQuery('div [id=cargaMasiva]').css('display','none');
	</script>
 	<div  style="width: 98%;">
	 	 <br/><br/>
	 	 <s:if test="incisoCotizacionDTO.id.idToCotizacion > 0">
		 	 <script type="text/javascript">
						jQuery('div [id=esquemadePago]').css('display','block');
						jQuery('div [id=cargaMasiva]').css('display','none');
			</script>
	 	 	<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.informacionInciso" /></div>
		 <div id ="contenedorInciso" style="height: 400px; float:both; width:900px" class="agregar">
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery('div [id=buscador]').css('display','none');
					jQuery('div [id=ajustarCaracteristicas]').css('display','none');
					jQuery('div [id=contenedorInciso] :input').attr('disabled','disabled');
				});
			</script>
				<s:push value="incisoCotizacionDTO">
					<s:action name="getDatosVehiculo" var="getDatosVehiculo" namespace="/componente/vehiculo" ignoreContextParams="true" executeResult="true" >
						<s:param name="idCodigoPostalName">incisoAutoCot.codigoPostal</s:param>
						<s:param name="idEstadoName">incisoAutoCot.estadoId</s:param>	
						<s:param name="idMunicipioName">incisoAutoCot.municipioId</s:param>
						<s:param name="idCotizacionName">id.idToCotizacion</s:param>
						<s:param name="idMonedaName">cotizacionDTO.idMoneda</s:param>
						<s:param name="idNegocioSeccionName">incisoAutoCot.negocioSeccionId</s:param>
						<s:param name="idMarcaVehiculoName">incisoAutoCot.marcaId</s:param>		
						<s:param name="idEstiloVehiculoName">incisoAutoCot.estiloId</s:param>
						<s:param name="idModeloVehiculoName">incisoAutoCot.modeloVehiculo</s:param>
						<s:param name="modificadoresDescripcionName">incisoAutoCot.modificadoresDescripcion</s:param>	
						<s:param name="descripcionFinalName">incisoAutoCot.descripcionFinal</s:param>			
						<s:param name="idTipoUsoVehiculoName">incisoAutoCot.tipoUsoId</s:param>
						<s:param name="idNegocioPaqueteName">incisoAutoCot.negocioPaqueteId</s:param>	
						<s:param name="pctDescuentoEstadoName">incisoAutoCot.pctDescuentoEstado</s:param>
						<s:param name="onChangePaquete">obtenerCoberturaCotizaciones()</s:param>
						<s:param name="aplicaFlotilla">cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas</s:param>
						<s:param name="soloConsulta"><s:property value="soloConsulta"/></s:param>
						<s:param name="serieName">incisoAutoCot.numeroSerie</s:param>
						<s:param name="vinValidoName">incisoAutoCot.vinValido</s:param>
						<s:param name="coincideEstiloName">incisoAutoCot.coincideEstilo</s:param>
						<s:param name="observacionesSesaName">incisoAutoCot.observacionesSesa</s:param>
						<s:param name="claveAmisName">incisoAutoCot.claveAmis</s:param>
						<s:param name="claveSesaName">incisoAutoCot.claveSesa</s:param>
					</s:action>	
					<s:if test="soloConsulta == 0">
					<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Eliminar">
				      	<div id="btnDelet" class="btn_back w140" style="display: inline; float: left;">
							<a href="javascript: void(0);" onclick="javascript:eliminarIncisoIndividual(<s:property value="id.idToCotizacion" escapeHtml="false" escapeXml="true"/>,<s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/>,<s:property value="incisoAutoCot.negocioSeccionId" escapeHtml="false" escapeXml="true"/>);" class="icon_carDelet">	
								<s:text name="midas.boton.borrar"/>	
							</a>
			      		</div>
		      		</m:tienePermiso>
		      		</s:if>
		      		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Editar">
				      	<div id="btnEdit" class="btn_back w140" style="display: inline; float: left;">
							<a href="javascript: void(0);" onclick="javascript:mostrarVentanaInciso(<s:property value="id.numeroInciso" />, 'parent.closeInciso();', <s:property value="soloConsulta" />,1);" class="icon_carEdit">	
								<s:text name="midas.cotizacion.mostrarDetalle"/>	
							</a>
			      		</div>	
		      		</m:tienePermiso>
				</s:push>
			 </div>
	 	 </s:if>
	 	  <s:else>
	 	 	<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery('div [id=agregarInciso]').css('display','block');
				});
			</script>
	 	 </s:else>
 	 </div>
 </s:else>
<div id="recargaVehiculo" style="display:none;"></div>
<script type="text/javascript">
		obtenerResumenTotalesCotizacionGrid();
		jQuery(document).ready(function(){
			ocultarIndicadorCarga('indicador');	
			if('<s:property value="tiposVigenciaList.size" />' > 0){
				document.getElementById("fecha").parentNode.childNodes[1].setAttribute("readonly", true);			
				document.getElementById("fechados").parentNode.childNodes[1].setAttribute("readonly", true);				
				actualizarFechas('<s:property value="vigenciaDefault" />');
				
				jQuery("#fechados").unbind("click").unbind("focus");
				jQuery("#fecha").unbind("click").unbind("focus");
			}
			
			if('<s:property value="vigenciaDefault" />' == "-1" && '<s:property value="tiposVigenciaList.size" />' == 0){
				jQuery("#idTipoVigencia").attr("disabled", true);		
			} else {
				jQuery("#idTipoVigencia").attr("disabled", false);
			}
		});	
	
	function actualizarFechas(dias){
		var fechaInicioVigencia = new Date();
		var diasVigencia = parseInt(dias);
		
		var fechaFinVigencia = new Date();
		fechaFinVigencia.setDate(fechaInicioVigencia.getDate() + diasVigencia);	
		
		dwr.util.setValue('fechados', fechaFinVigencia.getDate() + '/' + (fechaFinVigencia.getMonth() + 1) + '/' + fechaFinVigencia.getFullYear());
	}
</script>
<style>
	input[readonly] + button {
		display: none;
		visibility: hidden;
		width: 0;
		overflow: hidden;
		height: 0;
		z-index: -99999;
		opacity: 0;
	}
</style>