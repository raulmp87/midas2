<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.numeric.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>		
<script type="text/javascript">
		jQuery(function(){			
			jQuery(".numeric").numeric();
			jQuery("#btnIgualar").click(function(){
				if(jQuery("#primaTotalAIgualar").val()) {
					jQuery("#formIgualar").submit();
				} else {
					parent.mostrarMensajeInformativo("Es necesario escribir el valor total a igualar","20",null, null);
				}
			});
		});
		
		function igualarIncisoEndoso(){
			parent.submitVentanaModal("igualarInciso", document.formIgualar);	
		}
		
		function restaurarPrimaInciso(){
			if (confirm('\u00BFEst\u00e1 seguro que desea restaurar la prima del inciso\u003F')) {
				var form = jQuery('#formIgualar');
				var url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/restaurarPrimaInciso.action';
				if(jQuery("#tipoEndoso").val() === undefined || jQuery("#tipoEndoso").val() === ''){
					url = '/MidasWeb/vehiculo/inciso/restaurarPrimaInciso.action';
				}
				parent.redirectVentanaModal('igualarInciso', url, form);
			}
		}
</script>
<s:form theme="simple" id="formIgualar" action="igualarInciso.action">
	<s:hidden name="incisoCotizacion.id.idToCotizacion" id="incisoCotizacion.id.idToCotizacion"/>
	<s:hidden name="incisoCotizacion.id.numeroInciso" id="incisoCotizacion.id.numeroInciso"/>
	<s:hidden name="bitemporalInciso.value.valorPrimaTotal" id="bitemporalInciso.value.valorPrimaTotal"/>
	<s:hidden name="bitemporalInciso.continuity.id" id="bitemporalInciso.continuity.id"/>
	<s:hidden name="validoEn" id="validoEn"/>
	<s:hidden name="tipoEndoso" id="tipoEndoso"/>
	
	<div id="loading" style="display: none;">
                <img id="img_indicator" name="img_indicator"
                               src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
	</div>
	
	<div id="detalle">
		<center>
			<table id="agregar" width="100%">
				<tr>
					<td>Valor total del inciso:</td>
					<s:if test="bitemporalInciso == null">
						<td><s:property value="%{getText('struts.money.format',{incisoCotizacion.valorPrimaTotal})}" escapeHtml="false" escapeXml="true"/></td>
					</s:if>
					<s:else>
						<td><s:property value="%{getText('struts.money.format',{bitemporalInciso.value.valorPrimaTotal})}" escapeHtml="false" escapeXml="true"/></td>										
					</s:else>
				</tr>
				<tr>
					<td>Valor total a igualar:</td>
					<td><s:textfield cssClass="numeric" id="primaTotalAIgualar" name="primaTotalAIgualar"></s:textfield></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div id="btnIgualar"  style="display: inline; float: right;" class="btn_back w150">
							<a href="javascript: void(0);" onclick="igualarIncisoEndoso();">
								<s:text name="Igualar"/>
							</a>
						</div>
						<div id="aceptar" class="btn_back w150" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="restaurarPrimaInciso();"> <s:text
								name="midas.suscripcion.cotizacion.igualacionPrimas.eliminar" /> </a>
						</div>
					</td>
				</tr>
			</table>
		</center>
	</div>
</s:form>
