<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>			
		</beforeInit>		
		
		<column>#cspan</column>
		<column>#cspan</column>
		<column>#cspan</column>		
	</head>
		
	<row>
		<cell><s:text name="midas.cotizacion.totalprimas"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.descuentoComisionCedida"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.primaneta"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.recargo"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.derechos"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.iva"/></cell>
		<cell type="co"><s:text name="midas.cotizacion.pctiva"/>
			<option value="11">11</option>
			<option value="16">16</option>
		</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
	<row>
		<cell><s:text name="midas.cotizacion.primatotalperiodo"/></cell>
		<cell>#cspan</cell>
		<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
	</row>
</rows>
