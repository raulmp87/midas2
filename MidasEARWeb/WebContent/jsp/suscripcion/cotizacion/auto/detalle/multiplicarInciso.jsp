<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
#required{
	display:none;
	font-size: 10px;
	color:red;
}
</style>

<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/cotizacion/cotizacionAuto.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<sj:head/>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<script type="text/javascript">

var multiplicarIncisoBitemporalPath = '<s:url action="multiplicarInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	
	function submitMultiplicar(){
		var numeroCopias = jQuery('#numeroCopias').val();
		if (numeroCopias <= 0) {
			parent.mostrarMensajeInformativo("El m\u00ednimo de copias permitido es 1","20",null, null);
			return false
		}
		if(numeroCopias > 1000){
			parent.mostrarMensajeInformativo("Solo se permiten 1000 copias por transacción","20",null, null);
			return false;
		}

	if (validaForm(jQuery('#numeroCopias').val())) {
	
	       if(dwr.util.getValue("bitemporalInciso.continuity.id") != null && dwr.util.getValue("bitemporalInciso.continuity.id") != ''
	       && dwr.util.getValue("validoEn") != null && dwr.util.getValue("validoEn") != '')
	       {
	           var incisoContinuityId = dwr.util.getValue("bitemporalInciso.continuity.id");
			   var validoEn = dwr.util.getValue("validoEn");
			   var path = multiplicarIncisoBitemporalPath +'?bitemporalInciso.continuity.id=' + incisoContinuityId + '&validoEn=' + validoEn + '&numeroCopias=' + numeroCopias;
			   path = path + '&nextFunction=funcionRefrescarAltaInciso()';
			   parent.redirectVentanaModal('multiplicarInciso', path, null)	           
	       }
	       else
	       {
	        var numeroInciso = dwr.util.getValue("incisoCotizacion.id.numeroInciso");
			var idToCotizacion = dwr.util.getValue("incisoCotizacion.id.idToCotizacion");
			var path = "/MidasWeb/vehiculo/inciso/multiplicarInciso.action?incisoCotizacion.id.numeroInciso="
					+ numeroInciso
					+ "&incisoCotizacion.id.idToCotizacion="
					+ idToCotizacion + '&numeroCopias=' + numeroCopias;
			path = path + '&nextFunction=funcionCierreMultiplicarInciso('
					+ idToCotizacion + ')';
			parent.redirectVentanaModal('multiplicarInciso', path, null)       
	       }    
			
		}
	}

	function validaForm(valor) {
		if (parseInt(valor) > 0) {
			muestraError(false);
			return true;
		} else {
			muestraError(true);
			return false
		}
	}

	function muestraError(bol) {
		if (bol) {
			jQuery('#required').show();
		} else {
			jQuery('#required').hide();
		}
	}
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form theme="simple" action="multiplicarInciso.action">
<s:hidden name="incisoCotizacion.id.idToCotizacion" id="incisoCotizacion.id.idToCotizacion"/>
<s:hidden name="incisoCotizacion.id.numeroInciso" id="incisoCotizacion.id.numeroInciso"/>
<s:hidden name="mensaje"/>
<s:hidden name="bitemporalInciso.continuity.id" id="bitemporalInciso.continuity.id"/>
<s:hidden name="validoEn" id="validoEn"/>
<div id="detalle" >
	<center>
		<table id="agregar" width="100%">
			<tr>
				<th>
					<s:text name="midas.cotizacion.multiplicarinciso" /> 
				</th>
			</tr>
			<tr>
				<th>
					<s:text name="midas.cotizacion.incisomultiplicador" /> 
				</th>
				<td>
					<s:textfield id="numeroCopias" name="numeroCopias" value="1"/>
					<div id="required">Ingresa un n&uacute;mero mayor que cero</div>
				</td>				
			</tr>
			<tr>
				<td>
					<div id="divEnviarBtn" class="w150" style="display: block; float:left;">
						<div class="btn_back w140"  >
								<a href="javascript: void(0);" onclick="submitMultiplicar();" class="icon_guardar">
								<s:text name="midas.cotizacion.clonar"/>	
							</a>
                      	</div>
                     </div>
				</td>
				<td>
					<div id="divCancelarBtn" class="w150" style="display: block; float:left;">
						<div class="btn_back w140"  >
							<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('multiplicarInciso')" class="icon_cancelar">	
								<s:text name="midas.cotizacion.cancelar"/>	
							</a>
                      	</div>
                     </div>
				</td>
			</tr>	    
		</table>
	</center>
</div>
</s:form>