<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript">
	var verDetalleIncisoPath = '<s:url action="verDetalleInciso" namespace="/vehiculo/inciso"/>';
	var verEsquemaPagoPath = '<s:url action="verEsquemaPago" namespace="/suscripcion/cotizacion/auto"/>';
	var urlBusquedaPromotoria = '<s:url action="buscar.promotoria" namespace="/suscripcion/cotizacion/auto"/>';
	var urlBusquedaAgente  = '<s:url action="buscar.agente" namespace="/suscripcion/cotizacion/auto"/>';
	var seleccionarAgentePath = '<s:url action="ventanaAgentes" namespace="/suscripcion/cotizacion/auto"/>';
	var seleccionarPromotoriaPath = "<s:url action="ventanaPromotorias" namespace="/suscripcion/cotizacion/auto"/>"
</script>
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
.divInputText{
    width: 180px;
    max-width:180px;
    height:14px;
    background-color:#FFF;
    text-align:left;
    cursor:text;
}
</style>
