<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<table class="contenedorMarco">
	<tr>
		<td class="subtitulo align-left" colspan="2" >
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.infVehiculo"/>
		</td>
	</tr>
	<tr>
		<td colspan="2"><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.lineaNegocio"  
			name="configAuto.negocioSeccionId" cssClass="cajaTextoM2 w200"
			headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			list="negocioSeccionList" id="idToNegSeccion" 
			listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
			onchange="onChangeLineaNegocio()"
			labelposition="left"/>
		</td>		
	</tr>
			
	<tr id="trNormal2" style="display:none;">
		<td><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.marca"  
				name="configAuto.marcaId" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       id="idTcMarcaVehiculo"
			       list="#{}" onchange="onChangeMarcaVehiculo()"
			       value="" labelposition="left"/>
		</td>	
	</tr>
	<tr id="trNormal3" style="display:none;">
		<td><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.estilo"  
				name="configAuto.claveEstilo" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       id="estiloId"
			       list="#{}" onchange="onChangeEstiloVehiculo()"
			       value="" labelposition="left"/>
		</td>	
	</tr>
	<tr>
		<td><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.modelo"  
				name="configAuto.modeloVehiculo" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       list="#{}" id="modeloVehiculoId" onchange="onChangeModeloVehiculo()"
			       value="" labelposition="left"/>
		</td>
		<td class="w500">
			<div class="btn_back w150">
				<a href="javascript: void(0);"
					onclick="">
					<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.ajustarCaract"/>
				</a>
			</div>	
		</td>		
	</tr>	
	<tr id="trNormal1" style="display:none;">
		<td><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.tipoUso"  
				name="configAuto.tipoUsoId" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       id="idTcTipoUsoVehiculo"
			       list="#{}"
			       value="" labelposition="left"/>
		</td>	
	</tr>	
	<tr>
		<td colspan="2"><s:select key="midas.suscripcion.cotizacion.datosConfVehiculo.paquete"  
					name="configAuto.negocioPaqueteId" cssClass="cajaTextoM2 w200"
			       headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			       list="#{}" id="idToNegPaqueteSeccion" onchange='obtenerCoberturaCotizaciones()'
			       value="" labelposition="left"/>
		</td>		
	</tr>
</table>
