<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/plugins/jquery.formatCurrency-1.4.0.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>

<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<table id="t_riesgo">
	<thead><!-- COTIZADOR AGENTES Y INDIVIDUAL FLOTILLA -->
		<tr>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.claveObligatoriedad" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.descripcion" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.sumaAsegurada" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.porcDeducible" /></th>
			<th><s:text name="midas.suscripcion.cotizacion.inciso.primaNeta" /></th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="coberturaCotizacionList" status="stat">
		<tr>
			<td class ="txt_v2">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.numeroInciso" value="%{id.numeroInciso}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToSeccion" value="%{id.idToSeccion}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCobertura" value="%{id.idToCobertura}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].id.idToCotizacion" value="%{id.idToCotizacion}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveObligatoriedad" value="%{claveObligatoriedad}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].idTcSubramo" value="%{idTcSubramo}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCoaseguro" value="%{valorCoaseguro}" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorCuota" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeCoaseguro" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutCoaseguro" value="0" />			
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveAutDeducible" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].numeroAgrupacion" value="0" />
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].consultaAsegurada" value="%{consultaAsegurada}" />
				<s:if test="claveContrato==1">
					 <s:set name="readOnly" value="false"/>
				</s:if>
				<s:else>
					 <s:set name="readOnly" value="true"/>
				</s:else>
				<s:if test="claveContrato==1 && claveObligatoriedad==0" >
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="true"/>
					<input type="checkbox" id="claveContratoBoolean_%{#stat.index}"  name="coberturaCotizacionList[%{#stat.index}].claveContrato" checked="checked" disabled/>
				</s:if>	
				<s:else><!---->
					<s:checkbox id="claveContratoBoolean_%{#stat.index}" 
					name="coberturaCotizacionList[%{#stat.index}].claveContratoBoolean" value="%{claveContrato}" 
					fieldValue="true" 
					onchange="ocultaGuardar();habilitaRegistroCobertura(%{#stat.index});mostrarDaniosOcasionadosPorLaCarga(this, %{#stat.index});"
					disabled="%{#disabledConsulta}"
					/>			
				</s:else>								
			</td>

			<td class ="txt_v2"><s:property value="coberturaSeccionDTO.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true" /></td>
			<td class ="txt_v2">
				<s:if test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 0">
					<s:if test="valorSumaAseguradaMax != 0.0 && valorSumaAseguradaMin!= 0.0">
						<s:if test="coberturaSeccionDTO.coberturaDTO.idToCobertura==2650||coberturaSeccionDTO.coberturaDTO.idToCobertura==3020||coberturaSeccionDTO.coberturaDTO.idToCobertura==2860||coberturaSeccionDTO.coberturaDTO.idToCobertura==4821">
							<s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
							<s:if test="sumasAseguradas != null && !sumasAseguradas.isEmpty()">
								<s:select id="idValDiasSalario"
									name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
								  	headerKey="-1"
								  	headerValue="%{getText('midas.general.seleccione')}"
								  	list="sumasAseguradas" listKey="valorSumaAsegurada.intValue()" listValue="valorSumaAsegurada.intValue()"
								  	onchange="validaDisable(this, %{#stat.index});"
								  	cssClass="txtfield"
								/>
							</s:if>
						  	<s:else>
								<s:select id="idValDiasSalario"
									name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
								  	headerKey="-1"
								  	headerValue="%{getText('midas.general.seleccione')}"
								  	list="%{diasSalario}"
								  	onchange="validaDisable(this, %{#stat.index});"
								  	cssClass="txtfield"
								/>
							</s:else>
							<s:hidden id="numeroAcientos" value="%{numAcientos}"/>		
							<s:hidden id="valDiasSalario" value="%{getText(diasSalarioMinimo)}"  />
							<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" id="idSumaAsegurada" value="%{getText(valorSumaAsegurada)}"  />	
				 		</s:if>
				 		<s:else>
				 				<s:if test="sumasAseguradas != null && !sumasAseguradas.isEmpty()">
				 					<s:select id="valorSumaAseguradaStr_%{#stat.index}" 
										name="coberturaCotizacionList[%{#stat.index}].valorSumaAsegurada"
										value="valorSumaAsegurada"
										list="sumasAseguradas" listKey="valorSumaAsegurada" listValue="%{getText('struts.money.format',{valorSumaAsegurada})}"
										onchange="validaSumaSeleccionada(this, %{#stat.index});"
										disabled="%{#disabledConsulta}"
								  		cssClass="txtfield" 
									/>
				 			</s:if>
				 			<s:else>				
							<div style="font-size:8px;">
								*Entre <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true" /> y <s:property value="%{getText('struts.money.format',{valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />
							</div> 
							<s:textfield disabled="%{#disabledConsulta}" id ="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});" onblur="validaRangoSumaAsegurada(this,%{valorSumaAseguradaMin},%{valorSumaAseguradaMax},%{valorSumaAsegurada},%{#stat.index});" readonly="#readOnly"/>
							</s:else>
							<s:hidden id="valorSumaAseguradaMax_%{#stat.index}" value = "%{getText('struts.money.format',{valorSumaAseguradaMax})}"/>
							<s:hidden id="valorSumaAseguradaMin_%{#stat.index}" value = "%{getText('struts.money.format',{valorSumaAseguradaMin})}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:if test="coberturaSeccionDTO.coberturaDTO.idToCobertura==2650||coberturaSeccionDTO.coberturaDTO.idToCobertura==3020||coberturaSeccionDTO.coberturaDTO.idToCobertura==2860||coberturaSeccionDTO.coberturaDTO.idToCobertura==4821">
							<s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
							<s:select id="idValDiasSalario"
								name="coberturaCotizacionList[%{#stat.index}].diasSalarioMinimo"
							  	headerKey="-1"
							  	headerValue="%{getText('midas.general.seleccione')}"
							  	list="%{diasSalario}"
							  	onchange="validaDisable(this, %{#stat.index});"
							  	cssClass="txtfield"
							/>		
							<s:hidden id="numeroAcientos" value="%{numAcientos}"/>		
							<s:hidden id="valDiasSalario" value="%{getText(diasSalarioMinimo)}"  />
							<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" id="idSumaAsegurada" value="%{getText(valorSumaAsegurada)}"  />					
						</s:if>
						<s:else>
							<s:textfield disabled="%{#disabledConsulta}" id="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});"  onblur="aplicaRedondeo(%{#stat.index})"  readonly="#readOnly"/>	

						</s:else>
					</s:else>
				</s:if>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 1">
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
					<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 2">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
					<s:textfield disabled="%{#disabledConsulta}" id ="valorSumaAseguradaStr_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});" onblur="aplicaRedondeo(%{#stat.index})" readonly="#readOnly"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 9">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 3">
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{valorSumaAsegurada}"  />
					<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
				</s:elseif>
				<s:elseif test="coberturaSeccionDTO.coberturaDTO.claveFuenteSumaAsegurada == 10">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorCaratula"/>
					<s:textfield id ="valorSumaAsegurada_%{#stat.index}" name="coberturaCotizacionList[%{#stat.index}].valorSumaAseguradaStr" value="%{getText('struts.money.format',{valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="" readonly="true"/>
				</s:elseif>
			</td>
			<td class ="txt_v2">				
				<s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/>	
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].claveTipoDeducible" 
						 value="%{coberturaSeccionDTO.coberturaDTO.claveTipoDeducible}">
				</s:hidden>							
				<s:if test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "0"'>
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0" />
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
				</s:if>
				<s:elseif test='coberturaSeccionDTO.coberturaDTO.claveTipoDeducible == "1"'>
					<s:select id="porcentajeDeducible_%{#stat.index}"
						list="%{deducibles}"
						disabled="%{#disabledConsulta}"
						name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="validaDisable(this, %{#stat.index});"
						listKey="valorDeducible" listValue="valorDeducible"
						cssClass="txtfield"
						 />	
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorDeducible" value="0"  />
				</s:elseif>
				<s:else>
					<s:select id="valorDeducible_%{#stat.index}"
						list="%{deducibles}"
						disabled="%{#disabledConsulta}"
						name="coberturaCotizacionList[%{#stat.index}].valorDeducible" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="validaDisable(this, %{#stat.index});"
						listKey="valorDeducible" listValue="valorDeducible"
						cssClass="txtfield"
						 />	
					<s:hidden name="coberturaCotizacionList[%{#stat.index}].porcentajeDeducible" value="0"  />
				</s:else>			
			</td>
			<td class ="txt_v2">
				<s:hidden name="coberturaCotizacionList[%{#stat.index}].valorPrimaNeta"  value="%{valorPrimaNeta}"/>
				<s:property value="%{getText('struts.money.format',{valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/>
			</td>
		</tr>
		</s:iterator>
	</tbody>	
</table>
<script type="text/javascript">
	var $_numeroIncisoTemp = '<s:property value="incisoCotizacion.id.numeroInciso"/>';
	procesoCmbDiasSalario();
</script>
