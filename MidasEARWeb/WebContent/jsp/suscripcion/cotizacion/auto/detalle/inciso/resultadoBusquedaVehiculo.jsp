<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:iterator value="resultadosBusquedaVehiculo">
	<item>
		<estiloId><s:property value="id"/></estiloId>
		<descripcion><s:property value="descripcionEstilo"></s:property></descripcion>
		<marcaId><s:property value="marcaVehiculoDTO.idTcMarcaVehiculo"  escapeHtml="false" escapeXml="true" /></marcaId>
	</item>
	</s:iterator>
</resultados>
