<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<table  class="contenedorMarco">
	<tr>
		<td class="subtitulo align-left" colspan="2" >
			<s:text name="Ubicación cliente"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:select cssClass="cajaTextoM2 w200" list="estados" key="midas.fuerzaventa.negocio.estado"
			          headerValue="%{getText('midas.general.seleccione')}" 
			          headerKey=""
			          name="estadoId" id="estados" 
			          onchange="cargarComboSimpleDWR(this.value, 'municipios', ' getMapMunicipiosPorEstado')" labelposition="left">
			</s:select>		
		</td>
		<td>
			<s:select key="midas.fuerzaventa.negocio.municipio"  cssClass="cajaTextoM2 w200"
			       headerValue="%{getText('midas.general.seleccione')}" headerKey="" 
			       name="municipioId" id="municipios"
			       list="#{}"
			       value="selectedMonth" labelposition="left"/>
		</td>
	</tr>
</table>