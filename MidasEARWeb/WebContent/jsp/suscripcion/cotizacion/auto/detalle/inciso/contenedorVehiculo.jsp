<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/validaciones.js"></s:url>'></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/vehiculo/componenteVehiculo.js'/>"></script> 
<s:include value="/jsp/suscripcion/cotizacion/cotizacionHeader.jsp"></s:include>


<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>

<style type="text/css">
.icon_guardar {
	display: none;
}
#infoMsg, #error{
display:none;
margin: 10px;
padding: 10px;
font-size: 12px;
text-aling: center;
width: 93%;
}
#error{
	color:red;
	border: 1px solid red;
}
#infoMsg{
	color:green;
	border: 1px solid #73D54A;	
}
</style>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript">
var numeroInciso = '<s:property value="incisoCotizacion.id.numeroInciso"/>';
var idToCotizacion = '<s:property value="incisoCotizacion.id.idToCotizacion"/>';
	function validaGuardar() {
		listadoService
				.validaDatosRiesgosCompletosCotizacion(
						idToCotizacion,
						numeroInciso,
						function(data) {
							if (!data) {
								parent.mostrarMensajeInformativo(
										"Faltan guardar los datos adicionales del paquete.",
										"30", "mostrarDatosRiesgo("
												+ idToCotizacion + ","
												+ numeroInciso + ");", null);
							} else if (validateAll(true, 'save')) {
							if (document.getElementById("idCodigo").value !=''){	
								guardarInciso(<s:property value="incisoCotizacion.id.idToCotizacion"/>);
								}else{
									parent.mostrarMensajeInformativo(
											"El C\u00F3digo postal es obligatorio", "20", null, null);
									} 
							}
						});
	}
	function ocultaGuardar(id, valor) {
		jQuery('#infoMsg').show();
		jQuery('#infoMsg').text('Calcula el inciso para guardar');
		jQuery('#btnRecalcular').css('display', 'block');
		jQuery('#btnGuardar').css('display', 'none');
		jQuery('#btnDatosAdicionalesPaquete').css('display', 'none');
	}

	function validaRecalcular() {
	    
		if (document.getElementById("coberturaCotizacionGrid").innerHTML != '') {
			if(numeroInciso == '' || numeroInciso == null) {
				numeroInciso = $_numeroIncisoTemp;
			}
			if (document.getElementById("idCodigo").value !=''){
				recalcular();
			}else{
						parent.mostrarMensajeInformativo(
					"El C\u00F3digo postal es obligatorio", "20", null, null);
			} 
			
		} else {
			parent.mostrarMensajeInformativo(
					"No tienes coberturas para Recalcular", "20", null, null);
		}
	}
</script>
<s:form id="incisoForm"  action="/vehiculo/inciso/calcularInciso.action" name="incisoForm">
<s:hidden name="soloConsulta" />
<s:hidden id="incisoCotizacion.id.numeroInciso" name="incisoCotizacion.id.numeroInciso" />
<s:hidden id="incisoCotizacion.id.idToCotizacion" name="incisoCotizacion.id.idToCotizacion" />
<s:hidden id="incisoCotizacion.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas" name="incisoCotizacion.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas" />
<s:hidden id="incisoCotizacion.cotizacionDTO.porcentajeIva" name="incisoCotizacion.cotizacionDTO.porcentajeIva" />

<s:action name="getDatosVehiculo" var="getDatosVehiculo" namespace="/componente/vehiculo" ignoreContextParams="true" executeResult="true" >
	<s:param name="idCodigoPostalName">incisoCotizacion.incisoAutoCot.codigoPostal</s:param>
	<s:param name="idEstadoName">incisoCotizacion.incisoAutoCot.estadoId</s:param>	
	<s:param name="idMunicipioName">incisoCotizacion.incisoAutoCot.municipioId</s:param>
	<s:param name="idCotizacionName">incisoCotizacion.id.idToCotizacion</s:param>
	<s:param name="idMonedaName">incisoCotizacion.cotizacionDTO.idMoneda</s:param>
	<s:param name="idNegocioSeccionName">incisoCotizacion.incisoAutoCot.negocioSeccionId</s:param>
	<s:param name="idMarcaVehiculoName">incisoCotizacion.incisoAutoCot.marcaId</s:param>		
	<s:param name="idEstiloVehiculoName">incisoCotizacion.incisoAutoCot.estiloId</s:param>
	<s:param name="idModeloVehiculoName">incisoCotizacion.incisoAutoCot.modeloVehiculo</s:param>
	<s:param name="modificadoresDescripcionName">incisoCotizacion.incisoAutoCot.modificadoresDescripcion</s:param>	
	<s:param name="descripcionFinalName">incisoCotizacion.incisoAutoCot.descripcionFinal</s:param>			
	<s:param name="idTipoUsoVehiculoName">incisoCotizacion.incisoAutoCot.tipoUsoId</s:param>
	<s:param name="idNegocioPaqueteName">incisoCotizacion.incisoAutoCot.negocioPaqueteId</s:param>
	<s:param name="onChangePaquete">obtenerCoberturaCotizaciones()</s:param>
	<s:param name="soloConsulta"><s:property value="soloConsulta" /></s:param>
	<s:param name="pctDescuentoEstadoName">incisoCotizacion.incisoAutoCot.pctDescuentoEstado</s:param>
	<s:param name="serieName">incisoCotizacion.incisoAutoCot.numeroSerie</s:param>
	<s:param name="vinValidoName">incisoCotizacion.incisoAutoCot.vinValido</s:param>
	<s:param name="coincideEstiloName">incisoCotizacion.incisoAutoCot.coincideEstilo</s:param>
	<s:param name="observacionesSesaName">incisoCotizacion.incisoAutoCot.observacionesSesa</s:param>
	<s:param name="claveAmisName">incisoCotizacion.incisoAutoCot.claveAmis</s:param>
	<s:param name="claveSesaName">incisoCotizacion.incisoAutoCot.claveSesa</s:param>
</s:action>	
<div id="loading" style="display: none;">
                <img id="img_indicator" name="img_indicator"
                               src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="error"></div>
<div id="infoMsg"></div>
<div class="subtituloIzquierdaDiv"><s:text name="Detalle de Coberturas"/></div>
<div id="cargando" style="display: none;text-align: center;">
	<img src="/MidasWeb/img/loading-green-circles.gif">
	<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
</div>
<div id="indicador"></div>
<div id="coberturaCotizacionGrid" align="center"></div>
<br/>
<div class="clear"></div>
<div class="clear"></div>
<div id="resumenTotalesIncisoGrid"></div>
<div class="clear"></div>
<div id="divBtn" class="divContenedorBotones">	
	<s:if test="soloConsulta == 0">		
	<div style="display: inline; float: right;" class="btn_back w100">
		<a href="javascript: void(0);" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir? Los cambios no guardados se perder\u00E1n')){parent.cerrarVentanaModal('inciso')};">
			<s:text name="Cancelar"/>
		</a>
	</div>	
	<div id="btnGuardar" style="display: none; float: right;" class="btn_back w100 guardar">
		<a href="javascript: void(0);" class="icon_guardar" onclick="validaGuardar();">
			<s:text name="Guardar"/>
		</a>
	</div>
	<div id="btnRecalcular"  style="display: inline; float: right;" class="btn_back w100">
		<a href="javascript: void(0);" onclick="validaRecalcular();">
			<s:text name="Calcular"/>
		</a>
	</div>
	<div id="btnDatosAdicionalesPaquete" style="float: right; display:none"
	class="btn_back w250">
	<a href="javascript: void(0);"
		onclick="mostrarDatosRiesgo(dwr.util.getValue('incisoCotizacion.id.idToCotizacion'),<s:property value="incisoCotizacion.id.numeroInciso"/>);">
		<s:text name="Datos Adicionales Paquete" /> </a>
</div>
	</s:if>
	<s:if test="soloConsulta == 1">
	<div style="display: inline; float: right;" class="btn_back w100">
		<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('inciso');">
			<s:text name="salir"/>
		</a>
	</div>	
	</s:if>
</div>
</s:form>
<div class="clear"></div>
<div id="contenidoVehiculo"></div>
<s:if test="soloConsulta == 1">
	<script type="text/javascript">
		cargaValoresInciso();
	</script>
</s:if>
<s:if test="incisoCotizacion.id.numeroInciso != null && incisoCotizacion.id.numeroInciso > 0 && soloConsulta == 0">
	<script type="text/javascript">
		muestraDatosAdicionalesVehiculo();
		cargaValoresInciso();
	</script>
</s:if>
<s:else>
	<s:if test="soloConsulta == 0">
	<script type="text/javascript">
		jQuery('#btnDatosAdicionalesPaquete').css('display', 'none');
		jQuery('#btnRecalcular').css('display', 'none');
		jQuery('#btnGuardar').css('display', 'none');
	</script>
	</s:if>
</s:else>
<div id="loading" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>