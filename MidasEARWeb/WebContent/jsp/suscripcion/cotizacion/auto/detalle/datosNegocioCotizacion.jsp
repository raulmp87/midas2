<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.informacionnegocio" /></div>
<table id="agregar">
	<tr>
		<th>
			<s:text name="midas.cotizacion.negocio" /> 
		</th>
		<td>    
			<s:property value="cotizacion.SolicitudDTO.negocio.descripcionNegocio"/>
		</td>
		<th>
			<s:text name="midas.cotizacion.tipopoliza" /> 
		</th>
		<td>
			<s:property value="cotizacion.tipoPolizaDTO.descripcion"/>
		</td>
	</tr>
	<tr>
		<th>
			<s:text name="midas.cotizacion.producto" /> 
		</th>
		<td>
			<s:property value="cotizacion.tipoPolizaDTO.productoDTO.descripcion"/>
		</td>
		<th>
			<s:text name="midas.general.moneda" /> 
		</th>
		<td>
			<s:property value="cotizacion.descripcionMoneda"/>
		</td>
	</tr>	    
</table>
