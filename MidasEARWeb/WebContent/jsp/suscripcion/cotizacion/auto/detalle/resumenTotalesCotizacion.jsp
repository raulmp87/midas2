<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="incisoCotizacion!=null">
	<s:set id="readOnly" value="true"/>
</s:if>
<s:else>
	<s:set id="readOnly" value="false" />
</s:else>
<s:set id="valorIvaSeleccionado" value="%{resumenCostosDTO.valorIvaSeleccionado}" />
<table id="t_riesgo" border="0"  width="98%" style="padding: 0px; margin: 0px;">
	<tr>
		<th width="60%"><s:text name="Total de Primas"/></th>
		<td width="40%" class="txt_v" align="right"> 
			<div id="primaNetaCoberturas">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.primaNetaCoberturas" value="resumenCostosDTO.totalPrimas"/>
				</s:text>			
			</div>
		</td>
	</tr>	
	<tr>
		<th width="60%"><s:text name="Descuentos / Comis Ced."/></th>
		<td width="40%" class="txt_v" align="right">
			<div id="descuentoComisionCedida">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.descuentoComisionCedida" value="resumenCostosDTO.descuentoComisionCedida"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="60%"><s:text name="Prima Neta"/></th>
		<td width="40%" class="txt_v" align="right">
			<div id="totalPrima">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.totalPrimas" value="resumenCostosDTO.primaNetaCoberturas"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="60%"><s:text name="Recargo"/></th>
		<td width="40%" class="txt_v" align="right">
			<div id="recargo">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.recargo" value="resumenCostosDTO.recargo"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="60%"><s:text name="Derechos"/></th>
		<td width="40%" class="txt_v" align="right">
			<div id="derechos">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.derechos" value="resumenCostosDTO.derechos"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr height="24px">
		<th width="60%">
		<div id="porcentajeIvaContent">
				<s:text name="IVA %"/>
				<s:select name="cotizacion.porcentajeIva" id="porcentajeIva"
			      cssClass="txtfield fixIvaFieldIe" cssStyle="width: 40px;" theme="simple"
			      list="resumenCostosDTO.ivaList"
			      value="#valorIvaSeleccionado"
			      labelposition="%{getText('label.position')}" disabled="#readOnly" onchange="modificaIVA()"/>
		</div>	
		</th>
		<td width="40%" class="txt_v" align="right">
			<div id="iva">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.iva" value="resumenCostosDTO.iva"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="60%"><s:text name="Prima Total de Periodo"/></th>
		<td width="40%" class="txt_v" align="right">
			<div id="primaTotal">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosDTO.primaTotal" value="resumenCostosDTO.primaTotal"/>
				</s:text>			
			</div>
		</td>
	</tr>		
</table>