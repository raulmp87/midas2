<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		
		<column id="docAnexoCotDTO.id.idToCotizacion" type="ro" width="0" sort="int" hidden="true" align="center">idToCotizacion</column>
		<column id="docAnexoCotDTO.id.idToControlArchivo" type="ro" width="0" sort="int" hidden="true" align="center">idToCotizacion</column>
		<column id="docAnexoCotDTO.claveObligatoriedad" type="ro" width="*" sort="int" hidden="true" align="center">obligatoriedad</column>
		<column id="docAnexoCotDTO.claveSeleccion" type="ch" width="100" sort="int" align="center" >Seleccione</column>
		<column id="claveTipo" type="ro" width="80" sort="str" align="center" >Nivel</column>
		<column id="docAnexoCotDTO.descripcionDocumentoAnexo" type="ro"  width="*" sort="str"  >Anexo</column>
		<column id="download" type="img" width="90" sort="na" align="center" >Acciones</column>		
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="anexosList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idToCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idToControlArchivo" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="claveObligatoriedad == 3">
				<cell><%=a%>-<s:property value="claveObligatoriedad" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:else>
				<cell><s:property value="claveObligatoriedad" escapeHtml="false" escapeXml="true"/></cell>
			</s:else>
			
			<cell><s:property value="claveSeleccion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>
				<s:if test="claveTipo==0" >P</s:if>
				<s:elseif test="claveTipo==1" >TP</s:elseif>
				<s:elseif test="claveTipo==2" >C</s:elseif>
				<s:else>N/A</s:else>
			</cell>			
			<cell><s:property value="descripcionDocumentoAnexo" escapeHtml="false" escapeXml="true"/></cell> 		
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar Documento^javascript:descargarAnexo(<s:property value="id.idToControlArchivo" escapeHtml="false" escapeXml="true"/>);^_self</cell>
		</row>
	</s:iterator>	
	
</rows>
