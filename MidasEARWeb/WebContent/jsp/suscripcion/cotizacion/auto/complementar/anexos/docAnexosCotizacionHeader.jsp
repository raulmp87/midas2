<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/anexos/docAnexosCotizacion.js'/>"></script>

<script type="text/javascript">
    var obtenerAnexosPath = '<s:url action="obtenerAnexos" namespace="/suscripcion/cotizacion/auto/complementar/anexos"/>';
    var modificarAnexosPath = '<s:url action="modificarAnexos" namespace="/suscripcion/cotizacion/auto/complementar/anexos"/>';
</script>