<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:form action="mostrarDocumentos" id="clienteDocumentosForm">
<s:hidden name="tipoAccion"/>	
<s:hidden name="cliente.idCliente"/>
<s:hidden name="cliente.claveTipoPersona"/>
	<table width="97%" class="contenedorFormas">		
		<tr>
			<s:if test="cliente.claveTipoPersona== 1">
				<td colspan="4">
					<div class="titulo">
						<s:text name="midas.catalogos.agente.documentosAgente.documentosPersonaFisica"/>
					</div>
				</td>
			</s:if>
			<s:else>
				<td colspan="4">
					<div class="titulo">
						<s:text name="midas.catalogos.agente.documentosAgente.documentosPersonaMoral"/>
					</div>
				</td>
			</s:else>
		</tr>
<s:iterator value="listaDocumentosFortimax" status="stat" var="doc" id="doc">		
		<tr>
			<td width="5%">
				<s:if test="#doc.existeDocumento==1">
				 	<input type="checkbox" checked="checked" disabled="disabled"/>
				</s:if>					
				<s:else>
					<input type="checkbox" disabled="disabled"/>
				</s:else>
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/>
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumento"/>				
			</td>			
			<td width="95%">
				<s:text name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumentoFortimax"/>
			</td>				
		</tr>		
		</s:iterator>		
		<tr>
		<td colspan="3">
		<div align="right" class="w890 inline" >
			<div class="btn_back w180">
				<a href="javascript: generarLigaIfimaxCliente();" class="icon_imprimir"
					onclick="">
					<s:text name="midas.boton.digitalizarDoc"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: auditarDocumentosCliente();" class="icon_confirmAll"
					onclick="">
					<s:text name="Auditar"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: guardarDocumentosFortimaxCliente();" class="icon_guardar2"
					onclick="">
					<s:text name="Guardar"/>
				</a>
			</div>	
		</div>
		</td>
		</tr>
		</table>
</s:form>

