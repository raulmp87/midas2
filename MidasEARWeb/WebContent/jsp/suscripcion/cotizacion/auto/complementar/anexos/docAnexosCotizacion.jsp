<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/anexos/docAnexosCotizacionHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<center>
		<s:form id="anexosForm">
			<s:hidden name="idToCotizacion"  id="idToCotizacion"/> 
	
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" >
							<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.anexo.title" /> 
					</td>
				</tr>
				
				<tr>
					<td >
						<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.anexo.listado" />
					</td>
				</tr>
			</table>
		</s:form>
		<div id="indicador"></div>
		<div id="anexosGrid" style="width:97%;height:284px"></div>
		<s:if test="soloConsulta == 0" >
		<div class="btn_back w140" >
			<a href="javascript: void(0);" onclick="guardarAnexoCotizacion();" class="icon_enviar">	
				<s:text name="midas.boton.guardar"/>	
			</a>
	    </div>
	    </s:if>
	</center>
</div>

<script type="text/javascript">
	obtenerAnexos();
</script>