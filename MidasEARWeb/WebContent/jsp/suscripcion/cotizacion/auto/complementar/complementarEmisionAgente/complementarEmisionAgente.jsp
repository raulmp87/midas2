<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/complementarEmisionAgente/complementarEmisionAgenteHeader.jsp"/>
<style type="text/css">
.contenido {
	height: 200px;
}
</style>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<div id="detalle" style="width: 100%;">
	<!--<s:hidden id="idToCotizacion" name="idToCotizacion" />-->
	<s:hidden id="tipoRegreso" name="tipoRegreso" value="4"/>
	<s:hidden id="idToNegocio" name="idToNegocio"/>
	<s:hidden id="claveEstatus" name="cotizacionDTO.claveEstatus" />
	<table id="agregar" style="padding: 0px; margin: 0px; border: none; width : 98%">
		<tr>
			<td valign="top" width="55%">
				<table id="agregar">
					<tr>
						<td colspan="3" class="subtitulo  align-left">
							<s:text name="midas.cotizacion.datoscontratante" />
						</td>
					</tr>
					<tr>
						<td>
							<s:hidden name="cotizacionDTO.idToPersonaContratante"></s:hidden>
							<s:textfield name="cliente.nombreCliente" readonly="true"
									key="midas.cotizacion.nombrecontratante" size="45"
									cssStyle="max-width:242;left:-10px;"
									labelposition="top" cssClass="txtfield" theme="simple" />
						</td>
						<td>
							<s:if test="negocioAsociado">							
								<s:if test="soloConsulta == 0">
									<div class="btn_back w110">							
										<a href="javascript: void(0);"
											onclick="mostrarClientesAsociados();" class="icon_cliente">
											<s:text name="midas.cotizacion.buscarcliente" /> </a>	
									</div>							
								</s:if>									
							</s:if>
							<s:else>
								<s:if test="soloConsulta == 0">		
									<div class="btn_back w110">				
										<a href="javascript: void(0);"
											onclick="buscarCliente(seleccionarCliente);" class="icon_cliente">
											<s:text name="midas.cotizacion.buscarcliente" /> </a>
									</div>
								</s:if>								
							</s:else>
						</td>
						<td>
							<s:if test="soloConsulta == 0">
								<div class="btn_back w150">							
									<a href="javascript: void(0);"
										onclick="mostrarVentanaAseguradoAgente(1,<s:property value="idToCotizacion" />,1);"
										 class="icon_persona">										
										<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.datosAsegurado" /> </a>	
								</div>							
							</s:if>								
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div id="incisosCompletos" class="row titulo" style="float: both;">
								<s:if test="mensajeDTO.mensaje != null">
									<s:property value="mensajeDTO.mensaje" />
								</s:if>
							</div>
							<s:if test="soloConsulta == 0">
								<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cambiar la cotizaci\u00F3n a EN PROCESO?')){cambiarEstatusEnProcesoAgente(<s:property value="idToCotizacion"/>);}">
										<s:text name="midas.boton.regresar" /> </a>
								</div>
								<s:if test="!mensajeDTO.visible">
									<s:set id="displayBoton" value="'display: none;'" />
								</s:if>
								<s:if test="cotizacionDTO.claveEstatus==12">
									<s:set id="claveTextoBoton" value="%{getText('midas.suscripcion.cotizacion.complementar.emitir')}" />
									<s:set id="accionJsBoton" value="'emitirCotizacion();'" />
									<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Emitir">
										<div class="btn_back w100" id="botonEmision" style="<s:property value='#displayBoton'/> float: right;">
											<a href="javascript: void(0);"
												onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){<s:property value="#accionJsBoton"/>;}"> 
												<s:text name="#claveTextoBoton"/> 
											</a>
										</div>
									</m:tienePermiso>
								</s:if>
							</s:if>
						</td>
					</tr>
				</table>
				<s:textfield name="idClienteResponsable" id="idClienteResponsable"
					readonly="true" cssStyle="display:none;"
					onchange="seleccionarCliente(this.value)" />
				<s:textfield name="domidClienteResponsable"
					id="domidClienteResponsable" readonly="true" cssStyle="display:none;" />	
			</td>
			<td valign="top">
				<table style=" width:100%; margin: 5px; border: none">
					<tr >
						<td colspan="2" valign="top" align="right">
							<div id="cargaResumenTotalesConsulta"></div>
							<div id="resumenTotalesCotizacionGridConsulta"></div>
						</td>
					</tr>
				</table>			
			</td>		
		</tr>
	</table>
	<div id="divCargaComplementarInciso"></div>
	<div id="divComplementarInciso"></div>	
</div>
<div id="clienteModal2"></div>
<div id="clienteModal1"></div>
<div id="central_indicator" class="sh2" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		src="/MidasWeb/img/as2.gif" alt="Afirme" />
</div>
<script type="text/javascript">
		obtenerResumenTotalesCotizacionGridAgenteConsulta();
		jQuery(document).ready(function(){
			ocultarIndicadorCarga('cargaResumenTotalesConsulta');
		});
</script>
<script type="text/javascript">		
		mostrarDivComplementarInciso();
		jQuery(document).ready(function(){
			ocultarIndicadorCarga('divCargaComplementarInciso');
		});		
</script>