<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		
		<column id="docDigitalCotDTO.idDocumentoDigitalCotizacion" type="ro" width="0" sort="int" hidden="true" >idToCotizacion</column>
		<column id="docDigitalCotDTO.cotizacionDTO.idToCotizacion" type="ro" width="0" sort="int" hidden="true" >idToCotizacion</column>
		<column id="docDigitalCotDTO.controlArchivo.idToControlArchivo" type="ro" width="0" sort="int" hidden="true" >idToControl</column>
		<column id="docDigitalCotDTO.controlArchivo.nombreArchivoOriginal" type="ro" width="*" sort="str" >Archivo</column>
		<column id="docDigitalCotDTO.nombreUsuarioCreacion" type="ro" width="*" sort="str"  >Anexado Por</column>
		<column id="docDigitalCotDTO.fechaCreacion" type="ro" width="*" sort="str"  >Fecha</column>
		<column id="accionEliminar" type="img" width="80px" sort="na" align="center">Acciones</column>
		<column id="accionDescargar" type="img" width="30px" sort="na"/>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="docDigitalCotizacionList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idDocumentoDigitalCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.idToCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="controlArchivo.idToControlArchivo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="controlArchivo.nombreArchivoOriginal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuarioCreacion" escapeHtml="false" escapeXml="true" /></cell>		
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell> 
			<cell>/MidasWeb/img/delete14.gif^Borrar^javascript:eliminarDocCotizacion(<s:property
				value="idDocumentoDigitalCotizacion" escapeHtml="false"
				escapeXml="true" />);^_self</cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar Documento^javascript:descargarDocCotizacion(<s:property value="controlArchivo.idToControlArchivo" escapeHtml="false" escapeXml="true"/>);^_self</cell>
		</row>
	</s:iterator>	
	
</rows>
