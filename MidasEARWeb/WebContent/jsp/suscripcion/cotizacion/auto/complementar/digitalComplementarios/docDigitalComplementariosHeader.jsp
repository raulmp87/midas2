<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/digitalComplementarios/docDigitalComplementarios.js'/>"></script>

<script type="text/javascript">
	var obtenerDocCotizacionPath = '<s:url action="mostrarDocDigitalCotizacion" namespace="/suscripcion/cotizacion/auto/complementar/digitalComplementarios"/>';
	var eliminarDocDigitalCotizacionPath = '<s:url action="eliminarDocDigitalCotizacion" namespace="/suscripcion/cotizacion/auto/complementar/digitalComplementarios"/>';
	var guardarAclaracionesPath = '<s:url action="guardarAclaraciones" namespace="/suscripcion/cotizacion/auto/complementar/digitalComplementarios"/>';
	var obtenerComentariosPath = '<s:url action="obtenerComentariosDisponibles" namespace="/suscripcion/solicitud/comentarios"/>';
</script>