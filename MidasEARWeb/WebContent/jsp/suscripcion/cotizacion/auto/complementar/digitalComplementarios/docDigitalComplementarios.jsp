<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
	ocultarIndicadorCarga('indicador');
</script>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/digitalComplementarios/docDigitalComplementariosHeader.jsp"></s:include>
<s:hidden name="tabActiva"></s:hidden>
<s:form id="docDigitalComplementariosForm">	
<div id="detalle" name="Detalle">
			<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
			<s:hidden name="idSolicitud" id="idSolicitud" />
			<table id="desplegarDetalle1" border="0">
				<tr>
					<td class="titulo" >
							<s:text name="midas.suscripcion.cotizacion.auto.complementar.docDigitales.deSolicitud" /> 
					</td>
				</tr>
				<tr><td>
		
		<div id="docSolicitudGrid" style="width:97%;height:150px"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		
		</td></tr>
				<tr>
					<td class="titulo" >
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.docDigitales.deCotizacion" /> 
					</td>
				</tr>
			<tr><td>
			
			<s:if test="soloConsulta == 0">
		<div class="btn_back w140" >
			<a href="javascript: void(0);" onclick="agregarDocCotizacion();" class="icon_enviar">	
				<s:text name="midas.boton.agregar"/>	
			</a>
	    </div>
	    </s:if>
		<div id="docCotizacionGrid" style="width:97%;height:150px"></div>
	    
	    	</td></tr>
				<tr>
					<td class="titulo" >
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.docDigitales.aclaracaciones" /> 
					</td>
				</tr>
				<tr>
					<td>
						<s:textarea id="aclaraciones" name="aclaraciones" cols="100" rows="10" cssStyle="font-size:12px;" />
					</td>
				</tr>
				<tr>
					<td>
					<s:if test="soloConsulta == 0">
					<div class="btn_back w140">
						<a href="javascript: void(0);" onclick="guardarAclaraciones();"
							class="icon_enviar"> <s:text name="midas.boton.guardar" /> </a>
					</div>
					</s:if>
					</td>
				</tr>
			</table>
</div>
</s:form>
<script type="text/javascript">
	mostrarDocCotizacion();
	obtenerComentarios();
</script>