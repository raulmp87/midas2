<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden id="usuarioExterno" name="usuarioExterno" />
	<s:if test="%{inciso.incisoAutoCot.vinValido && inciso.incisoAutoCot.coincideEstilo}">
		<s:set var="controlesFronterizosDisabled">true</s:set>
	</s:if>
	<s:else>
		<s:if test="#disabledConsulta">
			<s:set var="controlesFronterizosDisabled">true</s:set>
			<s:set var="disabledConsultaDescripcion">true</s:set>
			
		</s:if>
		<s:else>
			<s:set var="controlesFronterizosDisabled">false</s:set>
			
			<s:if test="usuarioExterno == 1">
				<s:set var="disabledConsultaDescripcion">false</s:set>	
			</s:if>
			<s:else>
				<s:set var="disabledConsultaDescripcion">true</s:set>
			</s:else>
		</s:else>
	</s:else>
	
	<table id="agregar" border="0" width="98%">
		<tr>
			<td class="titulo"colspan="2">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.title"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="numMotor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numMotor"/></label>
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  
					 disabled="%{#disabledConsulta}"
					 maxlength="19" size="25" id="numMotor" name="inciso.incisoAutoCot.numeroMotor" />							
			</td>
			<td>
				<label for="numSerie"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numSerie"/></label>
				
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  
					 disabled="%{#controlesFronterizosDisabled}"
					 maxlength="17" size="25" id="numSerie" name="inciso.incisoAutoCot.numeroSerie" 
					 onblur="remplazarCaracteresNumSerie(this)"/>
			</td>
			
		</tr>
		<tr>
			<td>
				<label for="numPlaca"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numPlaca"/></label>
				
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  
					 disabled="%{#disabledConsulta}"
					 maxlength="10" size="25" id="numPlaca" name="inciso.incisoAutoCot.placa" maxlength="12" />
				
			</td>
			<td><label for=""><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.descripcion"/></label>
				
			</td>
			<td colspan="3">
				<s:textfield cssClass="txtfield jQrequired w400"  
					 disabled="%{#disabledConsultaDescripcion}"	
					 maxlength="80" size="50" id="descripcion" name="inciso.incisoAutoCot.descripcionFinal" maxlength="500" />
			</td>
		</tr>
		<tr>
			<td><label for="repuve"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.repuve" /></label>
				 
			</td>		
			<td>
				<s:textfield cssClass="txtfield"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="10" size="25" id="repuve" name="inciso.incisoAutoCot.repuve" maxlength="10"/>
			</td>
			<td><label for="emailContacto"><s:text name="midas.cotizacion.emails" /></label>				 
			</td>		
			<td colspan="3">
				<s:textfield cssClass="txtfield"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="500" size="50" id="emailContacto" name="inciso.emailContacto"/>
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.rutacirculacion" />			 
			</td>		
			<td colspan="5">
				<s:textfield cssClass="txtfield"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="200" size="50" id="rutaCirculacion" name="inciso.incisoAutoCot.rutaCirculacion" />
			</td>
		</tr>				
	</table>
<div id="contenedorObservaciones" style="width:98%">
		<table id="agregar" style="width:98%">
			<tr>
				<td class="titulo" colspan="2"><s:text
						name="midas.suscripcion.cotizacion.auto.complementar.inciso.observaciones" />
				</td>
			</tr>
			<tr>
			<td>
				<s:if test="guardaObservacionesInciso">
					<s:if test="#disabledConsultaDescripcion">
						<s:textarea cssClass="txtfield" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4"  readonly="%{#disabledConsulta}" disabled="%{#disabledConsultaDescripcion}"></s:textarea>
					</s:if>
					<s:else>
						<s:textarea cssClass="txtfield jQrequired" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4" readonly="%{#disabledConsulta}"></s:textarea>
					</s:else>
				</s:if>
				<s:else>
					<s:textarea cssClass="txtfield" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4" readonly="%{#disabledConsulta}" disabled="%{#disabledConsultaDescripcion}"></s:textarea>
				</s:else>
			</td>
			</tr>
		</table>
</div>