<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<style type="text/css">
/*
input {
	min-width: 100px;
	max-width: 230px;
}*/

select {
	min-width: 100px;
	max-width: 230px;
}

textarea {
	min-width: 230px;
	max-width: 330px;
}

div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}

div#contenedorObservaciones {
	width:100%;
}
</style>
<script type="text/javascript">

	var claveTipoPersona = '<s:property value="cliente.claveTipoPersona"/>';
	
	function validaGuardar() {
		if (validateForId('complementarIncisoForm', true)
				&& validarFecha(jQuery('#fechaNacimiento').val())) {
			guardarComplementarDatosInciso();
		}
	}

	function getAge(dateString) {
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}
	
	function validarFecha(fecha) {
		if (fecha != null && fecha != undefined) {
			var dia = fecha.substring(0, 2);
			var mes = fecha.substring(3, 5);
			var anio = fecha.substring(6, 10);

			if (getAge(mes+"/"+dia+"/"+anio) < 18) {
				parent.mostrarMensajeInformativo(
						"El conductor tiene que ser mayor de edad", "10", null,
						null);
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	function guardarComplementarDatosInciso() {
		var form = jQuery('#complementarIncisoForm')[0];
		sendRequestJQ(null,'/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardar.action' + '?' + jQuery(form).serialize(), 
		null,"parent.loadDivComplementar();parent.mostrarMensajeInformativo('<s:text name="midas.negocio.exito"/>', '30',null, null);");
	}

	jQuery(document).ready(function() {
	
		jQuery('#fechaNacimiento').attr('buttonImage', '../img/b_calendario.gif');
				
		var url = '<s:url action="cargaControlDinamicoRiesgo" namespace="/suscripcion/cotizacion/auto/complementar/datosRiesgo"/>';
		url += '?name=datosRiesgo&idToCotizacion=' + jQuery("#cotizacionId").val() + '&numeroInciso=' + jQuery("#numeroInciso").val();
		
		jQuery('#divCargaControlDinamicoRiesgo').empty();
		
		sendRequestJQ(null, url,
				"divCargaControlDinamicoRiesgo", jQuery('#loadingCargaControlDinamicoRiesgo').css('display', 'none'));		
	});
	
	function validaCiente(){
		if(jQuery("#idCliente").val() > 0){
			jQuery("input[name$='copiarDatos']").removeAttr("disabled");
		}else{
			jQuery("input[name$='copiarDatos']").attr("disabled",true);		
		}
		if(jQuery("input[name$='copiarDatos']:checked").val() == 2){
			jQuery("#nombre").attr("disabled",true);
			jQuery("#paterno").attr("disabled",true);
			jQuery("#materno").attr("disabled",true);
		}
	}
	
	function copiarDatosUsuario(sel){
		var numeroSecuencia = jQuery("#numeroSecuencia").val();
		if (claveTipoPersona == 2 && sel == 2) {
			jQuery("#copiarDatos1").attr('checked', true);
			parent.mostrarMensajeInformativo("No se puede copiar una persona moral", "10",null, null);
			return false;
		}
		if(sel == 1){
			jQuery("#nombre").removeAttr("disabled");
			jQuery("#paterno").removeAttr("disabled");
			jQuery("#materno").removeAttr("disabled");
		}else {
			parent.recargarVehiculoAgente(jQuery("#cotizacionId").val(), jQuery("#incisoId").val(),sel,numeroSecuencia, jQuery("#soloConsulta").val());
		}
	}
	
	function remplazarCaracteresNumSerie(obj){
		var pattN = /[�]/gi;
		var valN = 'N';
		var pattOQU = /[oq]/gi;
		var valOQU = '0';
		var pattI = /[i]/gi;
		var valI = '1';	
		var space = /[ ]/gi;
		var empty = '';
		var str = obj.value;
		str = str.replace(pattN, valN);
		str = str.replace(pattOQU, valOQU);
		str = str.replace(pattI, valI);
		str = str.replace(space, empty);
		obj.value = str.toUpperCase();
	}
</script>
<s:form action="guardar" id="complementarIncisoForm">
	<s:hidden id="cotizacionId" name="cotizacionId" />
	<s:hidden id="incisoId" name="incisoId" />
	<s:hidden id="numeroInciso" name="inciso.id.numeroInciso"/>
	<s:hidden id="numeroSecuencia" name="numeroSecuencia" />
	<s:hidden id="guardadoExitoso" name="guardadoExitoso" />
	<s:hidden id="usuarioExterno" name="usuarioExterno" /> 
	<s:hidden name="soloConsulta" />
	<s:if test="soloConsulta == 1">
		<s:set var="disabledConsulta">true</s:set>
		<s:set var="disabledConsultaDescripcion">true</s:set>	
		<s:set var="showOnConsulta">focus</s:set>
	</s:if>
	<s:else>
		<s:set var="disabledConsulta">false</s:set>	
		<s:set var="showOnConsulta">both</s:set>
		
		<s:if test="usuarioExterno == 1">
			<s:set var="disabledConsultaDescripcion">false</s:set>	
		</s:if>
		<s:else>
			<s:set var="disabledConsultaDescripcion">true</s:set>
		</s:else>
	</s:else>
	
	
	
	
	<s:if test="%{inciso.incisoAutoCot.vinValido && inciso.incisoAutoCot.coincideEstilo}">
		<s:set var="controlesFronterizosDisabled">true</s:set>
	</s:if>
	<s:else>
		<s:if test="#disabledConsulta">
			<s:set var="controlesFronterizosDisabled">true</s:set>
		</s:if>
		<s:else>
			<s:set var="controlesFronterizosDisabled">false</s:set>
		</s:else>
	</s:else>
	<s:if test="guardaDatoConductorInciso">
		<div id="contenedorConductor" style="width:98%;overflow:auto;">
			<table id="agregar">
				<s:hidden id="idCliente" name="cliente.idCliente" />
				<s:if test="copiarDatos == 2">
					<s:hidden name="inciso.incisoAutoCot.nombreConductor" />
					<s:hidden id="inciso.incisoAutoCot.paternoConductor" name="inciso.incisoAutoCot.paternoConductor" />
					<s:hidden name="inciso.incisoAutoCot.maternoConductor" />
				</s:if>
				<tr>
					<td class="titulo"colspan="2">
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.datosConductor"/>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<s:radio name="copiarDatos"  list="#{'1':'Nuevo Conductor','2':'Copiar Datos del Asegurado'}"   id ="copiarDatos" disabled="true"
						onclick="copiarDatosUsuario(this.value);" />	
					</td>
				</tr>
				<tr>
					<td>
						<label for="nombre"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/></label> 
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrequired"		 
							  size="25" id="nombre" name="inciso.incisoAutoCot.nombreConductor"
							   disabled="%{#disabledConsulta}"
							   maxlength="100"/>							
					</td>
					<td>
						<label for="paterno"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.paterno"/></label>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrequired" 	  			
							   disabled="%{#disabledConsulta}"		 
							  size="25" id="paterno" name="inciso.incisoAutoCot.paternoConductor" 
							  maxlength="100"/>
					</td>
					<td>
						<label for="materno"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.materno"/></label>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrequired" 
							  disabled="%{#disabledConsulta}"
							  size="25" id="materno" name="inciso.incisoAutoCot.maternoConductor"
							  maxlength="100" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="licencia"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.noLicencia"/></label>
					</td>		
					<td>
						<s:textfield  cssClass="txtfield numeric restrict jQrequired" 
							  size="25" id="licencia" name="inciso.incisoAutoCot.numeroLicencia" 
							  disabled="%{#disabledConsulta}"
							  onkeypress="return soloNumerosM2(this, event, false)"  required="true" maxlength="50"/>
					</td>
					<td>
						<label for="fechaNacimiento"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.fechaNacimiento"/></label>
					</td>
					<td >
						<sj:datepicker name="inciso.incisoAutoCot.fechaNacConductor" required="#requiredField" 
						 			   buttonImage="../img/b_calendario.gif" 
						               id="fechaNacimiento" maxlength="10" cssClass="txtfield jQrequired"	
						               labelposition="%{getText('label.position')}" 
						               size="12"
						               yearRange="-80:-18" maxDate="-18y"
						               changeYear="true"
						               changeMonth="true"
							           showOn="%{#showOnConsulta}"
							           disabled="%{#disabledConsulta}"
						               onkeypress="return soloFecha(this, event, false);"
						               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						               onblur="esFechaValida(this);"></sj:datepicker>
					</td>
					<td>
						<label for="ocupacionConductor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.ocupacion"/></label>
					</td>
					<td >
						<s:textfield cssClass="txtfield jQrequired"   
							 disabled="%{#disabledConsulta}"
							 size="25" id="ocupacionConductor" name="inciso.incisoAutoCot.ocupacionConductor" maxlength="100" />
					</td>
				</tr>		
			</table>
		</div>
	</s:if>
	<div id="contenedorControles" style="width:98%;">
		<table id="agregar" border="0" width="98%">
			<tr>
				<td class="titulo"colspan="2">
					<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.title"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="numMotor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numMotor"/></label>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrequired"
						 disabled="%{#disabledConsulta}"
						 maxlength="19" size="25" id="numMotor" name="inciso.incisoAutoCot.numeroMotor" />							
				</td>
				<td>
					<label for="numSerie"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numSerie"/></label>
					
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrequired w300"
						 disabled="%{#controlesFronterizosDisabled}"
						 maxlength="17" size="25" id="numSerie" name="inciso.incisoAutoCot.numeroSerie" 
						 onblur="remplazarCaracteresNumSerie(this)"/>
				</td>
				
			</tr>
			<tr>
				<td>
					<label for="numPlaca"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numPlaca"/></label>
					
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrequired"
						 disabled="%{#disabledConsulta}"
						 maxlength="10" size="25" id="numPlaca" name="inciso.incisoAutoCot.placa" maxlength="12" />
					
				</td>
				<td><label for=""><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.descripcion"/></label>
					
				</td>
				<td colspan="3">
						 <s:textfield cssClass="txtfield jQrequired w300"
						 disabled="%{#disabledConsultaDescripcion}"	
						 maxlength="80"  id="descripcion" name="inciso.incisoAutoCot.descripcionFinal" maxlength="500" />
				</td>
			</tr>
		<tr>
			<td><label for="repuve"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.repuve" /></label>
				 
			</td>		
			<td>
				<s:textfield cssClass="txtfield"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="10" size="25" id="repuve" name="inciso.incisoAutoCot.repuve" maxlength="10"/>
			</td>
			<td><label for="emailContacto"><s:text name="midas.cotizacion.emails" /></label>				 
			</td>		
			<td colspan="3">
				<s:textfield cssClass="txtfield w300"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="500" size="50" id="emailContacto" name="inciso.emailContacto" />
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.rutacirculacion" />			 
			</td>		
			<td colspan="5">
				<s:textfield cssClass="txtfield w600"  	
					 disabled="%{#disabledConsulta}"				 
					 maxlength="200"  id="rutaCirculacion" name="inciso.incisoAutoCot.rutaCirculacion" />
			</td>		
		</tr>	
		</table>
	<div id="contenedorObservaciones">
			<table id="agregar" style="width:98%">
				<tr>
					<td class="titulo" colspan="2"><s:text
							name="midas.suscripcion.cotizacion.auto.complementar.inciso.observaciones" />
					</td>
				</tr>
				<tr>
				<td>
					<s:if test="guardaObservacionesInciso">
						<s:if test="#disabledConsultaDescripcion">
							<s:textarea cssClass="txtfield" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4" readonly="%{#disabledConsulta}" disabled="%{#disabledConsultaDescripcion}"></s:textarea>
						</s:if>
						<s:else>
							<s:textarea cssClass="txtfield jQrequired" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4" readonly="%{#disabledConsulta}"></s:textarea>
						</s:else>
					</s:if>
					<s:else>
						<s:textarea cssClass="txtfield" name="inciso.incisoAutoCot.observacionesinciso" cols="70" rows="4" readonly="%{#disabledConsulta}" disabled="%{#disabledConsultaDescripcion}"	></s:textarea>
					</s:else>
				</td>
				</tr>
			</table>
	</div>
	</div>
	<br/>
	<div id="contenedorPaquete" style="width:98%;">
		<table id="agregar" border="0" width="98%">
			<tr>
				<td class="titulo"colspan="2">
					<s:label  value="Datos Adicionales del Paquete"/>	
				</td>
			</tr>
			<tr>
				<td>	
					<div>
						<div id="loadingCargaControlDinamicoRiesgo" style="text-align: center;">
							<img src="/MidasWeb/img/loading-green-circles.gif"> <font
								style="font-size: 9px;">Procesando la informaci�n, espere
								un momento por favor...</font>
						</div>
						<div id="divCargaControlDinamicoRiesgo"></div>						
					</div>
				</td>
			</tr>		
		</table>
				
		<div class="inline align-right">
			<s:if test="soloConsulta == 0">
				<div class="btn_back w100 guardar">
					<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="javascript: validaGuardar();">
						<s:text name="Guardar"/>
					</a>
				</div>	
			</s:if>			
		</div>
	</div>
</s:form>

<s:if test="guardaDatoConductorInciso && soloConsulta == 0">
	<script type="text/javascript">
		validaCiente();
	</script>
</s:if>