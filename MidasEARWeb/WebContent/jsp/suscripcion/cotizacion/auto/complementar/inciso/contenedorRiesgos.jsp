<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<s:include
	value="/jsp/suscripcion/cotizacion/auto/complementar/inciso/contenedorHeaderRiesgos.jsp"></s:include>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(':input').css('width', '250px');
		jQuery('#loading').css('display', 'none');
	});
	// valida que los inputs del formulario esten llenos
	function validaGuardarRiesgos(){
		var input= '';
		if(jQuery('select').size() > 0 ){
			jQuery('select').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text().trim();
				}
			});
		}
		if(jQuery('input[type!=checkbox]').size() > 0 ){
			jQuery('input').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text().trim();
				}
			});
		}
		if(jQuery('textarea').size() > 0 ){
			jQuery('textarea').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text().trim();
				}
			});
		}
		if(input != ''){
			alert("Por favor verifica los siguientes campos \n" + input);
		}else{
			guardarRiesgosCotizacion();
		}
	}
	
</script>
</head>
<body>
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<s:form action="/suscripcion/cotizacion/auto/complementar/inciso/guardarRiesgosCotizacion.action" name="complementarIncisoForm" id="complementarIncisoForm">
		<s:hidden name ="cotizacionId" value="%{incisoCotizacion.id.idToCotizacion}" />
		<s:hidden name="incisoId" value="%{incisoCotizacion.id.numeroInciso}" />
		<div id="contenedorPaquete" style="width: 98%; overflow-y: auto;">
			<s:label value="Datos Adicionales del Paquete" cssClass="titulo" />
			<table id="agregar" border="0" width="98%">
				<tr>
					<td>
						<div>
							<div id="loading" style="text-align: center;">
								<img src="/MidasWeb/img/loading-green-circles.gif">
								<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
							</div>
							<s:action name="cargaControlDinamicoRiesgo" executeResult="true"
								namespace="/vehiculo/inciso" ignoreContextParams="true">
								<s:param name="incisoCotizacion.id.idToCotizacion"
									value="%{incisoCotizacion.id.idToCotizacion}" />
								<s:param name="incisoCotizacion.id.numeroInciso"
									value="%{incisoCotizacion.id.numeroInciso}" />
								<s:param name="name" value="'datosRiesgo'" />
							</s:action>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
		<div id="controles" style="width: 98%;margin-right: .4em;">
			<div id="b_guardar" style="float: right;">
				<a href="javascript: void(0);"
					onclick="javascript: validaGuardarRiesgos();"> <s:text
						name="midas.boton.guardar" /> </a>
			</div>
		</div>
	</s:form>
</body>
</html>