<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
<!--
var claveTipoPersona = '<s:property value="cliente.claveTipoPersona"/>';
//-->
</script>
<div id="datosConductor" style="width:100%;">
	<table id="desplegarDetalle" style="border: 1px solid #73D54A;">
		<s:hidden id="idCliente" name="cliente.idCliente" />
		<s:if test="copiarDatos == 2">
			<s:hidden name="inciso.incisoAutoCot.nombreConductor" />
			<s:hidden id="inciso.incisoAutoCot.paternoConductor" name="inciso.incisoAutoCot.paternoConductor" />
			<s:hidden name="inciso.incisoAutoCot.maternoConductor" />
		</s:if>
		<tr>
			<td class="titulo"colspan="2">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.datosConductor"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:radio name="copiarDatos"  list="#{'1':'Nuevo Conductor','2':'Copiar Datos del Asegurado'}"   id ="copiarDatos" disabled="true"
				onclick="copiarDatosUsuario(this.value);" />	
			</td>
		</tr>
		<tr>
			<td>
				<label for="nombre"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/></label> 
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  					 
					  size="25" id="nombre" name="inciso.incisoAutoCot.nombreConductor"
					   disabled="%{#disabledConsulta}"
					   maxlength="100"/>							
			</td>
			<td>
				<label for="paterno"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.paterno"/></label>
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  			
					   disabled="%{#disabledConsulta}"		 
					  size="25" id="paterno" name="inciso.incisoAutoCot.paternoConductor" 
					  maxlength="100"/>
			</td>
			<td>
				<label for="materno"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.materno"/></label>
			</td>
			<td>
				<s:textfield cssClass="txtfield jQrequired"  	
					  disabled="%{#disabledConsulta}"
					  size="25" id="materno" name="inciso.incisoAutoCot.maternoConductor"
					  maxlength="100" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="licencia"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.noLicencia"/></label>
			</td>		
			<td>
				<s:textfield  cssClass="txtfield numeric restrict jQrequired" 				 
					  size="25" id="licencia" name="inciso.incisoAutoCot.numeroLicencia" 
					  disabled="%{#disabledConsulta}"
					  onkeypress="return soloNumerosM2(this, event, false)"  required="true" maxlength="50"/>
			</td>
			<td>
				<label for="fechaNacimiento"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.fechaNacimiento"/></label>
			</td>
			<td >
				<sj:datepicker name="inciso.incisoAutoCot.fechaNacConductor" required="#requiredField" 
				 			   buttonImage="../../../../../img/b_calendario.gif" 
				               id="fechaNacimiento" maxlength="10" cssClass="txtfield jQrequired"	
				               labelposition="%{getText('label.position')}" 
				               size="12"
				               yearRange="-80:-18" maxDate="-18y"
				               changeYear="true"
				               changeMonth="true"
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<label for="ocupacionConductor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.ocupacion"/></label>
			</td>
			<td >
				<s:textfield cssClass="txtfield jQrequired"  
					 disabled="%{#disabledConsulta}"
					 size="25" id="ocupacionConductor" name="inciso.incisoAutoCot.ocupacionConductor" maxlength="100" />
			</td>
		</tr>		
	</table>
</div>