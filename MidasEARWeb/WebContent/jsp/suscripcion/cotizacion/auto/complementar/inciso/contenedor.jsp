<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/inciso/contenedorHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
input {
	min-width: 100px;
	max-width: 230px;
}

select {
	min-width: 100px;
	max-width: 230px;
}

textarea {
	min-width: 230px;
	max-width: 330px;
}

div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}
</style>
<script type="text/javascript">
	function validaGuardar() {
		if (validateAll(true, 'save')
				&& validarFecha(jQuery('#fechaNacimiento').val())) {
			guardarComplementarDatosInciso();
		}
	}

	function getAge(dateString) {
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}
	
	function validarFecha(fecha) {
		if (fecha != null && fecha != undefined) {
			var dia = fecha.substring(0, 2);
			var mes = fecha.substring(3, 5);
			var anio = fecha.substring(6, 10);

			if (getAge(mes+"/"+dia+"/"+anio) < 18) {
				parent.mostrarMensajeInformativo(
						"El conductor tiene que ser mayor de edad", "10", null,
						null);
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	function guardarComplementarDatosInciso() {
		var form = jQuery('#complementarIncisoForm')[0];
		//var nextFunction = "&nextFunction=cerrarComplementarDatosInciso";
		var url = '/MidasWeb/suscripcion/cotizacion/auto/complementar/inciso/guardar.action'
				+ '?' + jQuery(form).serialize();
		parent.redirectVentanaModal('ventanaVehiculo', url, null);
	}

	function cerrarComplementarDatosInciso() {
		var idCotizacion = jQuery('#cotizacionId').val();
		sendRequestJQ(
				null,
				'/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='
						+ idCotizacion, 'contenido', null);
	}

	jQuery(document).ready(function() {
		jQuery('#loading').css('display', 'none');
	});
</script>

<s:form action="guardar" id="complementarIncisoForm">
<s:hidden id="cotizacionId" name="cotizacionId" />
<s:hidden id="incisoId" name="incisoId" />
<s:hidden id="numeroSecuencia" name="numeroSecuencia" />
<s:hidden id="guardadoExitoso" name="guardadoExitoso" />
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<s:if test="guardaDatoConductorInciso">
	<div id="contenedorConductor" style="width:100%;overflow:auto;">
		<jsp:include page="datosConductor.jsp"/>
	</div>
</s:if>
<br/>
<div id="contenedorControles" style="width:98%;">
	<jsp:include page="datosVehiculo.jsp"/>
</div>
<br/>
<div id="contenedorPaquete" style="width:98%;">
		<table id="agregar" border="0" width="98%">
		<tr>
			<td class="titulo"colspan="2">
				<s:label  value="Datos Adicionales del Paquete"/>	
			</td>
		</tr>
		<tr><td>	
			<div>
						<div id="loading" style="text-align: center;">
							<img src="/MidasWeb/img/loading-green-circles.gif"> <font
								style="font-size: 9px;">Procesando la información, espere
								un momento por favor...</font>
						</div>
						<s:action name="cargaControlDinamicoRiesgo" executeResult="true" 
		namespace="/suscripcion/cotizacion/auto/complementar/datosRiesgo" 
		ignoreContextParams="true">
			<s:param name="idToCotizacion" value="%{cotizacionId}"/>
			<s:param name="numeroInciso" value="%{inciso.id.numeroInciso}"/>
			<s:param name="name" value="'datosRiesgo'"/>
		</s:action>
		</div>
		</td></tr>
		
		</table>
			
		<div class="inline align-right">
		<s:if test="soloConsulta == 0">
			<div class="btn_back w100 guardar">
				<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="javascript: validaGuardar();">
					<s:text name="Guardar"/>
				</a>
			</div>		
			<div class="btn_back w100">
				<a href="javascript: void(0);" id="salirBtn" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cerrar la ventana?')){parent.cerrarVentanaModal('ventanaVehiculo');} ">
					<s:text name="Salir"/>
				</a>
			</div>
		</s:if>	
		<s:if test="soloConsulta == 1">
			<div class="btn_back w100">
				<a href="javascript: void(0);" id="salirBtn" onclick="parent.cerrarVentanaModal('ventanaVehiculo');">
					<s:text name="Salir"/>
				</a>
			</div>
		</s:if>
		</div>
		
</div>
	

</s:form>
<s:if test="guardaDatoConductorInciso && soloConsulta == 0">
<script type="text/javascript">
	validaCiente();
</script>
</s:if>
<s:if test="soloConsulta == 0">
<script type="text/javascript">
	if(jQuery("#guardadoExitoso").val() == "true"){
		var idCotizacion = jQuery('#cotizacionId').val();		
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+idCotizacion,'contenido', "parent.cerrarVentanaModal('ventanaVehiculo')");		
	}	
</script>
</s:if>