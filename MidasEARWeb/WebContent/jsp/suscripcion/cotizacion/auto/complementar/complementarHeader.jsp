<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/complementar.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script >

 jQuery( document ).ready(function() {
	
	var pdfDownload = dwr.util.getValue("pdfDownload");
	
	
    if(pdfDownload!=null&&pdfDownload!=''){
		var idToCotizacion = dwr.util.getValue("cotizacionId");
		
	    var urlPDF='/MidasWeb/suscripcion/emision/getFormatoEntrevista.action?&idToCotizacion='+idToCotizacion;
		window.open(urlPDF, 'download');
	}

});
</script>