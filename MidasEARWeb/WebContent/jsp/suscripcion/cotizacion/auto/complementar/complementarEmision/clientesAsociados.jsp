<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet"
	type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript"
	src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript"
	src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript"
	src="<html:rewrite page="/js/midas2/util.js"/>"></script>
<style>
<!--
body {
	overflow: hidden;
	background-color: #F4F3EE;
}
-->
</style>
<script type="text/javascript">
	var urlConsulta = '<s:url action="getClientesAsociados" namespace="/negocio" />';
	var idToNegocio = '<s:property value="idToNegocio"/>';
	var clientesGrid;
	function inicializarClientesGrid() {
		document.getElementById("clientesGrid").innerHTML = '';
		clientesGrid = new dhtmlXGridObject("clientesGrid");
		clientesGrid.attachHeader("&nbsp,&nbsp,&nbsp,#select_filter,#text_filter,&nbsp");
		clientesGrid.attachEvent("onRowDblClicked", function(rId, cInd) {
			var id = clientesGrid.getSelectedId();
			var idValue = clientesGrid.cellById(id, 0).getValue();
			var selectedId = clientesGrid.getSelectedRowId();
			var idCliente = clientesGrid.cellById(selectedId, 2).getValue();
			var nombre = clientesGrid.cellById(selectedId, 4).getValue();
			if (idCliente != null && nombre != null) {
				parent.cargaClienteAsociado(idCliente, nombre);
			}
		});
		clientesGrid.setImagePath("/MidasWeb/img/dhtmlxgrid/");
		mostrarIndicadorCarga('indicador');
		clientesGrid.attachEvent("onXLE", function(grid) {
			ocultarIndicadorCarga('indicador');
		});
		clientesGrid.init();
		clientesGrid.load(urlConsulta + "?idToNegocio=" + idToNegocio);
	}
</script>
<div class="grid">
	<div class="row">
		<div class="c12 titulo">
			<s:text
				name="midas.suscripcion.cotizacion.complementar.clientesAsociados" />
			:
			<s:property value="negocio.descripcionNegocio" />
		</div>
	</div>
	<div class="row">
		<div id="indicador" style="left: 214px; top: 40px; z-index: 1;"></div>
		<div id="clientesGrid" style="width: 680px; height: 300px"></div>
	</div>
</div>

<script type="text/javascript">
	inicializarClientesGrid();
</script>