<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/complementarEmision/complementarEmisionHeader.jsp"/>

<style type="text/css">
.contenido {
	height: 200px;s
}
/* paginaDestino == >/MidasWeb/catalogoCliente/mostrarPantallaConsulta.action */
label {
	font-weight: bold;
}

table {
	font-size: 10px;
}

.w150 {
	margin-right: 50px;
	margin-bottom: 10px;
}

.inciso {
	width: 10px;
	margin-left: 5px;
	margin-right: 5px;
}
	
.multiplicar {
	float: left;
}

.dividir {
	left: 5px;
}

.eliminar {
	margin-top: -18px;
	margin-right:5px;
	float: right;
}

.vehiculo {
	float: left;
}

.asegurado {
	left: 5px;
}
</style>
<script type="text/javascript">
<!--
var idContratante = '<s:property value="cotizacionDTO.claveEstatus"/>';
//-->
</script>
<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>
<div id="detalle" style="width: 100%;">
	<s:hidden id="idToCotizacion" name="idToCotizacion" />
	
	
	
	<s:hidden id="idToNegocio" name="idToNegocio"/>
	<s:hidden id="claveEstatus" name="cotizacionDTO.claveEstatus" />
	<s:hidden id="tipoRegreso" name="tipoRegreso" value="2" />
	<s:textfield name="idClienteResponsable" id="idClienteResponsable"
		readonly="true" cssStyle="display:none;"
		onchange="seleccionarCliente(this.value)" />
	<s:textfield name="domidClienteResponsable"
		id="domidClienteResponsable" readonly="true" cssStyle="display:none;" />
	<div id="container" class="grid">
		<div id="content" role="main">
			<div class="row">
				<div class="c5">
					<s:if test="negocioAsociado">
					<s:include
						value="/jsp/suscripcion/cotizacion/auto/detalle/datosContratanteCotizacionAgentesAsociados.jsp"></s:include>
					</s:if>
					<s:else>
						<s:include
						value="/jsp/suscripcion/cotizacion/auto/detalle/datosContratanteCotizacion.jsp"></s:include>
						<br>
						<div style="font-size:10px;font-weight: bold;">Tipo Impresion Cliente</div>
						
						<s:select id="tipoImpresionCliente"
							name="tipoImpresionCliente"
						  	list="%{listCatalogoTipoImpresionCliente}"
						  	headerValue="Seleccione..." headerKey=""
						  	cssClass="txtfield"
						  	onchange="seleccionarTipoImpresionCliente();"
						/>
					</s:else>
					
					<!-- Agrupacion 20131216 Combo de seleccion-->
					<s:if test="cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas == 1 && habilitarTipoRecibos">
						<br/> 
						<div class="row">
							<div class="c6 titulo">
								<s:text name="midas.cotizacion.agrupacionRecibos" />
							</div>
						</div>
						<br/>
						<div class="row">	
							<div class="c3">						
								<s:select id="cotizacionDTO.tipoAgrupacionRecibos"
									name="cotizacionDTO.tipoAgrupacionRecibos"
									list="%{listTipoAgrupacionRecibos}" 
									listKey="value"
									listValue="label"									
								  	cssClass="txtfield"
								  	onclick="setVariableValue('comboAgrupacionesValue',  this.value);"
								  	onchange="seleccionarTipoAgrupacionRecibos(this.value);"
								/>													
							</div>
						</div>
					</s:if>
										
				</div>
				<div id="resumenCotizacion" class="c5 end">
					<s:action name="mostrarResumenTotalesCotizacion"
						executeResult="true" namespace="/suscripcion/cotizacion/auto"
						ignoreContextParams="true">
						<s:param name="cotizacion.idToCotizacion" value="%{idToCotizacion}" />
						<s:param name="esComplementar" value="true" />
					</s:action>
				</div>
			</div>
			<div id="botonesDetalleCotizacion" class="row">
				<s:if test="soloConsulta == 0">
						<s:if test="!mensajeDTO.visible">
							<s:set id="displayBoton" value="'display: none;'" />
						</s:if>
						<s:set id="claveTextoBoton"
							value="%{getText('midas.suscripcion.cotizacion.complementar.emitir')}" />
						
						<s:if test="cotizacionDTO.idMedioPago == 6">
							<s:set id="accionJsBoton" value="'mostrarVentanModalDatosTarjeta();'" />
						</s:if>
						<s:else>
							<s:set id="accionJsBoton" value="'emitirCotizacion();'" />
						</s:else>
						
						<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Emitir">
							<div class="btn_back w100" id="botonEmision"
								style="<s:property value='#displayBoton'/> float: right;">
								<a href="javascript: void(0);"
									onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){<s:property value="#accionJsBoton"/>;}"> <s:text
										name="#claveTextoBoton" /> </a>
							</div>
						</m:tienePermiso>
						<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Imprimir">
							<div id="imprimeCotizacion" class="btn_back w170"
								style="display: inline; float: right;">
								<a href="javascript: void(0);"
									onclick="mostrarContenedorImpresion(1,<s:property value="idToCotizacion" escapeHtml="false"/>);"
									class="icon_imprimir "> <s:text
										name="midas.cotizacion.imprimircotizacion" /> </a>

							</div>
						</m:tienePermiso>
						<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Enviar_Por_Email">
							<div id="enviaEmail" class="btn_back w140"
								style="display: inline; float: right;">
								<a href="javascript: void(0);"
									onclick="ventanaCorreo(<s:property value="idToCotizacion"/>);"
									class="icon_email"> <s:text
										name="midas.cotizacion.enviarmail" /> </a>
							</div>
						</m:tienePermiso>
					
				      	<!-- Boton Carga Masiva  -->
				      	<s:if test="cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas == 1">
							<div id="cargaMasiva" class="btn_back w140" style="display: block; float: right;">
									<a href="javascript: void(0);" onclick="abrirCargaMasiva(<s:property value='cotizacionDTO.idToCotizacion'/>,3);" class="icon_cargaMasiva">	
										<s:text name="midas.cotizacion.cargamasiva"/>	
									</a>
					      	</div>
		    			</s:if>
				</s:if>
							
				<s:if test="%{banderaModuloRecuotificacionActivo}">
					<div id="previoRecibos" class="btn_back w140" style="float: right;">
					  	<a href="javascript: void(0);"
					       onclick="openProgramaPago(<s:property value="idToCotizacion"/>, 'P');" 
					       class="icon_email"> <s:text name="Previo Recibos" /> 
				      	</a>
			    	</div>	
				</s:if>			    		
				<s:if test="soloConsulta == 1 ">
						<div id="cotizacionConsulta" class="btn_back w160" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="verDetalleCotizacionContenedor(<s:property value='idToCotizacion'/>);"> <s:text
							name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizacion" /> </a>
						</div>
				</s:if>
				
			</div>
			<!-- Termina botones accion -->
			<div class="row titulo">
				<div id="incisosCompletos" style="float: both;">
					<s:if test="mensajeDTO.mensaje != null">
						<s:property value="mensajeDTO.mensaje" />
					</s:if>
				</div>
			</div>
			<!-- Termina # incisos complementados -->
			<s:if test="cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas == 1">
				<div class="row">
			<s:form id="paginadoGridForm">
				<s:hidden id="incisoCotizacionIdToCotizacion" name="incisoCotizacion.id.idToCotizacion" value="" />
				<s:hidden name="cleanCache" value="true"/>
				<div  id="filtros" class="c12">
					<table id="agregar" style="max-width: 840px;" class="hdr">
						<tr>
							<td><label class="label"><s:text name="midas.suscripcion.cotizacion.auto.inciso.numero.secuencia"></s:text></label></td>
							<td><s:textfield id="numeroSecuencia" name="incisoCotizacion.numeroSecuencia" cssClass="cajaTexto" cssStyle="width:150px"/></td>
							<td><label class="label"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.descripcion"></s:text></label></td>
							<td><s:textfield id="descripcionFinal" name="incisoCotizacion.incisoAutoCot.descripcionFinal" cssClass="cajaTexto" cssStyle="width:150px"/></td>
						</tr>
						<tr>
							<td><label class="label"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numSerie"/></label></td>
							<td><s:textfield id="numeroSerie" name="incisoCotizacion.incisoAutoCot.numeroSerie" cssClass="cajaTexto" cssStyle="width:150px"/></td>
							<td><label class="label"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numPlaca"></s:text></label></td>
							<td><s:textfield id="placa" name="incisoCotizacion.incisoAutoCot.placa" cssClass="cajaTexto" cssStyle="width:150px"/></td>
						</tr>
						<tr>
							<td><label class="label">Nombre asegurado</label>
							</td>
							<td><s:textfield id="nombreAsegurado" name="incisoCotizacion.incisoAutoCot.nombreAsegurado"
									cssClass="cajaTexto" cssStyle="width:150px" />
							</td>
							<td></td>
							<td><div class="btn_back w140"
									style="display: inline; float: right;">
									<a id="submit" href="javascript: void(0);"
										onclick="pageGridPaginadoIncisos_complementar(1,true);" class="icon_buscar"> <s:text
											name="midas.suscripcion.cotizacion.buscar" /> </a>
								</div>
							</td>
						</tr>
					</table>
				</div>
				</s:form>
			</div>
			</s:if>
			<div class="row">
				<div id="indicador"></div>
				<div class="c12">
					<div id="gridListadoDeIncisos" style="max-width: 860px; margin-left: 8px;">
						<div id="listadoIncisos" style="width: 700px; height: 300px; max-width: 860px;"></div>
						<div id="pagingArea"></div>
						<div id="infoArea"></div>
					</div>
				</div>

			</div>
			<div id="botonesDetalleCotizacion" class="row">
				<s:if test="soloConsulta == 0">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);"
								onclick="volverAListadoCotizacion();"> <s:text
									name="midas.boton.salir" /> </a>
						</div>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);"
								onclick="verComplementarCotizacion(<s:property value="idToCotizacion"/>)">
								<s:text name="midas.boton.actualizar" /> </a>
						</div>
						<div class="btn_back w140" style="display: inline ;float: right;">
							<a href="javascript: void(0);"
								onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cambiar la cotizaci\u00F3n a EN PROCESO?')){cambiarEstatusEnProceso(<s:property value="idToCotizacion"/>);}">
								<s:text name="midas.boton.regresar" /> </a>
						</div>
					</s:if>
				</div>
				
				
				
		</div>
	</div>
</div>
<div id="clienteModal2"></div>
	<div id="clienteModal1"></div>
<script type="text/javascript">
	validaIva();
	pageGridPaginadoIncisos(1,true);
	jQuery('#incisoCotizacionIdToCotizacion').val(jQuery("#idToCotizacion").val());
	procesoCmbTipoImpresionCliente();
	validaEstatusRecuotificacion();
</script>