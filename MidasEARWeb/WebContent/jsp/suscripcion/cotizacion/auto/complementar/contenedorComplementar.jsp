<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/complementarHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<s:hidden name="tipoAccion"/>
<s:hidden name="cotizacionId" />
<s:hidden name="incisoId" />
<s:hidden name="claveNegocio"/>
<s:hidden name="soloConsulta"/>
<s:hidden name="pdfDownload" />
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	mostrarIndicadorCarga("indicador");
</script>
<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
<div class="titulo">
	<s:text name="midas.suscripcion.cotizacion.complementar.emision" />
	<s:property value="cotizacionDTO.numeroCotizacion" />
</div>
<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html" style="height: 430px; width: 920px" id="complementarEmisionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="150px" id="detalle" name="<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.complemEmision" />" href="http://void" extraAction="javascript: verComplementarEmision();">
	</div>
	<s:if test="cotizacionId != null">
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Documentos_Digitales_Complementarios">
			<div width="200px" id="documentosDigitales" name="<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.digitalesCompl" />" href="http://void" extraAction="javascript: desplegarDigitalComplementarios();"></div>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Anexos">
			<div width="100%" id="documentosAnexos" name="Anexos" href="http://void" extraAction="javascript: desplegarAnexos();"></div>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Textos_Adicionales">
		<div width="100%" id="textosAdicionales" name="Textos Adicionales" href="http://void" extraAction="javascript: desplegarTextosAdicionales()"></div>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cobranza">
			<s:if test="habilitaCobranzaInciso == 0" >
				<div width="100%" id="cobranza" name="Cobranza" href="http://void" extraAction="javascript: desplegarCobranza()"></div>
			</s:if>
			<s:if test="habilitaCobranzaInciso == 1" >
				<div width="100%" id="cobranzaInciso" name="Cobranza X Inciso" href="http://void" extraAction="javascript: desplegarCobranzaInciso()"></div>
			</s:if>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Art_140">
			<div width="200px" id="documentos140" name="Documentos Art 140" href="http://void" extraAction="javascript: desplegarDocumentos140()"></div>
		</m:tienePermiso>
		<div width="200px" id="condicionesEspeciales" name="Condiciones Especiales" href="http://void" extraAction="javascript: verCondicionesEspeciales('normal')"></div>
	</s:if>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>