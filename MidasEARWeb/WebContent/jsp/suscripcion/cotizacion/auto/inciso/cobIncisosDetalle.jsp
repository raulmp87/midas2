<%@ taglib prefix="s" uri="/struts-tags" %>
	<center>
		<s:form action="relacionarIncisoCob"  id="cobIncisoAsegForm">
			<table id="desplegar" border="0" style="background-color: white; padding: 0px; margin: 0px; width: 860px">
				<tr>
					<th>N� Inciso</th>
					<th>Descripcion</th>
					<th>Numero Serie</th>
					<th>Asegurado</th>
					<th>Prima Total</th>
					<th>Cliente Cob</th>
					<th>Medio Pago</th>
					<th>Conducto Cobro</th>
				</tr>
					<s:iterator value="incisosCobranza" status="stats"  >					
					<tr  class="bg_t2">
						<s:hidden name="incisos[%{#stats.index}].id.idToCotizacion" value="%{inciso.id.idToCotizacion}"></s:hidden>
						<s:hidden id="numeroInciso_%{#stats.count}" name="incisos[%{#stats.index}].id.numeroInciso" value="%{inciso.id.numeroInciso}"></s:hidden>
						<td><s:property value="inciso.numeroSecuencia" /></td>
						<td><s:property value="inciso.incisoAutoCot.descripcionFinal" /></td>
						<td><s:property value="inciso.incisoAutoCot.numeroSerie" /></td>
						<td><s:property value="inciso.incisoAutoCot.nombreAseguradoUpper" /></td>
						<td><s:property value="%{getText('struts.money.format',{inciso.valorPrimaTotal})}" /></td>
						<td>
						<s:select id="idClienteCob_%{#stats.count}"
								list="clientesCobro"
 								name="incisos[%{#stats.index}].idClienteCob"  
 								cssStyle="width:100px;;max-width: 200px;"
 								cssClass="cajaTexto jQrequired"								
								labelposition="left" value="inciso.idClienteCob"
								headerKey="" headerValue="Seleccione ..."
								 required="#requiredField"
								 onchange="actualizaConductosCobro(%{#stats.count})"
								 />
						</td>
						<td>						
						<s:select id="idMedioPago_%{#stats.count}"
							list="mediosPago"
							name="incisos[%{#stats.index}].idMedioPago" 
							cssStyle="width:100px;max-width: 200px;"
							cssClass="cajaTexto jQrequired validaCobInciso"
							listKey="idMedioPago" listValue="descripcion"
							labelposition="left" value="inciso.idMedioPago"
							headerKey="" headerValue="Seleccione ..."
							 required="#requiredField"
							 onchange="actualizaConductosCobro(%{#stats.count})"
							 />
						</td>
						<td>
						<s:select id="idConductoCobro_%{#stats.count}"
								list="conductosCobro"
 								name="incisos[%{#stats.index}].idConductoCobroCliente"  
 								cssStyle="width:100px;;max-width: 200px;"
 								cssClass="cajaTexto jQrequired"								
								labelposition="left" value="inciso.idConductoCobroCliente"
								 required="#requiredField"
								 />
						</td>
				</tr>
				</s:iterator> 	
			</table>
		</s:form>
</center>
	