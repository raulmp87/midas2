<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">	
	var listarIncisosPath = '<s:url action="listarIncisos" namespace="/suscripcion/autos/inciso"/>';
	var incisosPaginadosRapidaPath = '<s:url action="busquedaRapidaPaginada" namespace="/vehiculo/inciso"/>';
	var incisosPaginadosPath = '<s:url action="busquedaPaginada" namespace="/vehiculo/inciso"/>';
	var busquedaRapidaPath = '<s:url action="busquedaRapida" namespace="/vehiculo/inciso"/>';
</script>
<!--  imports Js files -->
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script language="JavaScript"
	src='<s:url value="/js/validaciones.js"></s:url>'>
</script>
