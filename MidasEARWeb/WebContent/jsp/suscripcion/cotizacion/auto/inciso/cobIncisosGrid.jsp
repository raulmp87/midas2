<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<s:set var="estatus" value="%{incisoCotizacion.cotizacionDTO.claveEstatus}"/>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <column id="id.numeroInciso" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
        <column id="numeroSecuencia" type="ro" width="30" sort="int" hidden="false">#</column>
      	<column id="descripcionGiroAsegurado" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.inciso.descripcion.vehiculo" /></column>
      	<column id="numeroSerie" type="ro" width="137" sort="str">No. Serie</column>
      	<column id="nombreAsegurado" type="ro" width="200" sort="str">Asegurado</column>
		<column id="valorPrimaTotal" type="ro" width="*" sort="str" hidden="false">Prima Total</column>
		<column id="descripcionMedioPago" type="ro" width="*" sort="str" hidden="false">Medio de Pago</column>
		<column id="numeroTarjetaCobranza" type="ro" width="*" sort="str" hidden="false">Conducto de Cobro</column>		
	</head>
	<s:iterator value="incisoCotizacionList" status="row">
		<row id="<s:property value="id.numeroInciso"/>">
			<cell>0</cell>
			<cell><s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="incisoAutoCot.descripcionFinal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="incisoAutoCot.numeroSerie" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="incisoAutoCot.nombreAseguradoUpper" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="%{getText('struts.money.format',{valorPrimaTotal})}" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionMedioPago" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="numeroTarjetaCobranza" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>