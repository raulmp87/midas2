<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include
	value="/jsp/suscripcion/cotizacion/auto/inciso/contenedorHeader.jsp"></s:include>
<%-- <s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>--%>
<style type="text/css">
.contenido {
	height: 200px;
}

label {
	font-weight: bold;
}

table {
	font-size: 10px;
}

.w150 {
	margin-right: 50px;
	margin-bottom: 10px;
}

.inciso {
	width: 10px;
	margin-left: 5px;
	margin-right: 5px;
}

.multiplicar {
	float: left;
}

.dividir {
	left: 5px;
}

.eliminar {
	margin-top: -18px;
	margin-right:5px;
	float: right;
}

.vehiculo {
	float: left;
}

.asegurado {
	left: 5px;
}
</style>
<script type="text/javascript">
var lineaNegocioId;
	function cargarComboPaquetes(idToCotizacion,idNegocioSeccion) {
		parent.removeAllOptionsAndSetHeader(
				document.getElementById('incisoCotizacion.incisoAutoCot.negocioPaqueteId'), parent.headerValue,
				"Cargando...");
		if (lineaNegocioId > 0 || lineaNegocioId != "null") {
			listadoService.getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(idToCotizacion,idNegocioSeccion, function(
					data) {
				parent.addOptions(document.getElementById('incisoCotizacion.incisoAutoCot.negocioPaqueteId'), data);
			});
		}
	}
	function habilitaPaquetes(lineaNegocioId) {
		if (lineaNegocioId > 0) {
			jQuery("#incisoCotizacion.incisoAutoCot.negocioPaqueteId").removeAttr('disabled');
		} else {
			jQuery("#incisoCotizacion.incisoAutoCot.negocioPaqueteId").attr("disabled", 'disabled');			
		}
	}
	// Obtiene los filtros seleccionados
	// para pasarlos como parametros a multiplicar incisos
	function obtieneFiltros(){
		 filtros = new Object();
		 filtros.descripcion = jQuery('#descripcionesIncisos').val();
		 if(jQuery('#incisoCotizacion.incisoAutoCot.negocioPaqueteId').size() > 0){
		 	filtros.paqueteId = jQuery('#incisoCotizacion.incisoAutoCot.negocioPaqueteId').val();
		 }else{
				filtros.paqueteId = "";	 
		 }
		 filtros.idToSeccion = jQuery('#incisoCotizacion.incisoAutoCot.negocioSeccionId').val();
		 return filtros;
	}
</script>
<div class="subtituloIzquierdaDiv"><s:text name="Listado de Incisos" /></div>

<div id="content_form"  style="width: 98%;">
	<s:form action="mostrarContenedor" id="IncisoForm">
	<s:hidden id="idToCotizacion" name="incisoCotizacion.id.idToCotizacion" value="%{id}"/>
		<div id="form">
			<table id="agregar" style="width:98%;" class="fixTablaInciso">
				<tr>
					<td width="110px;"><label> <s:text name="midas.suscripcion.cotizacion.auto.inciso.numero.secuencia"/>	</label>
					</td>
					<td width=""><s:textfield name="incisoCotizacion.numeroSecuencia" cssClass="cajaTexto" cssStyle="width: 294px;"
							onkeypress="return soloNumeros(this, event, false);"></s:textfield>
					</td>
					<td width="100px;"><label> <s:text name="midas.suscripcion.cotizacion.auto.inciso.linea.negocio"/></label>
					</td>
					<td width=""><s:select list="negocioSeccionList"
							listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
							headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							name="incisoCotizacion.incisoAutoCot.negocioSeccionId" id="incisoCotizacion.incisoAutoCot.negocioSeccionId"
							onchange="cargarComboPaquetes('%{id}',this.value);"
							cssClass="txtfield">
						</s:select></td>
				</tr>
				<tr>
					<td><label><s:text name="midas.suscripcion.cotizacion.auto.inciso.descripcion"/></label></td>
					<td>
						<s:select list="descripcionesIncisos" headerKey="null"
								headerValue="%{getText('midas.general.seleccione')}"
								name="incisoCotizacion.incisoAutoCot.descripcionFinal" id="descripcionesIncisos"
								cssClass="txtfield">
						</s:select>
					</td>
					<td><label><s:text name="midas.suscripcion.cotizacion.auto.inciso.paquete"/></label></td>
					<td><s:select list="paquetes" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							name="incisoCotizacion.incisoAutoCot.negocioPaqueteId" id="incisoCotizacion.incisoAutoCot.negocioPaqueteId"
							cssClass="txtfield" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="btn_back w140" style="display: inline; float: right; margin-right: .9em;">
							<a href="javascript: void(0);" onclick="pageGridPaginado(1,true);"> <s:text
									name="midas.suscripcion.cotizacion.buscar" /> </a>
						</div>
					</td>
				</tr>
			</table>
			
		</div>
	</s:form>
</div>
<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
<div id="gridListadoDeIncisos">
	<div id="listadoIncisos" style="width: 98%; height: 135px"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</div>

<br />
<div style="width: 98%;">
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Actualizar_Listado_Inciso">
	<div class="alinearBotonALaDerecha">
		<div align="right" id="b_regresar">
			<a href="javascript: void(0);"
				onclick="javascript:pageGridPaginadoIncisos(1,true)"> <s:text
					name="midas.boton.actualizar" /> </a>
		</div>
	</div>
	</m:tienePermiso>
	<s:if test="incisoCotizacion.cotizacionDTO.claveEstatus == 10 && soloConsulta == 0" >
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Eliminar_Inciso">
	<div class="alinearBotonALaDerecha">
		<div align="right" id="b_regresar">
			<a href="javascript: void(0);"
				onclick="javascript:eliminarIncisosSeleccionados()"> <s:text
					name="midas.boton.borrar" /> </a>
		</div>
	</div>
	</m:tienePermiso>
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Eliminar_Inciso_Todos">
	<div class="alinearBotonALaDerecha">
		<div align="right" id="b_regresar">
			<a href="javascript: void(0);"
				onclick="javascript:eliminarTodosLosIncisos()"> <s:text
					name="midas.boton.borrarTodo" /> </a>
		</div>
	</div>
	</m:tienePermiso>
	</s:if>
</div>

<script type="text/javascript">
<!-- Ejecuta el metodo que llena el Grid -->
// 	mostrarListadoIncisos(<s:property value="incisoCotizacion.id.idToCotizacion"/>);
pageGridPaginadoIncisos(1,true);
</script>
