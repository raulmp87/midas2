<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<s:set var="estatus" value="%{incisoCotizacion.cotizacionDTO.claveEstatus}"/>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numeroSerieHidden" type="ro" width="0" hidden="hidden" sort="str">No. Serie</column>
        <s:if test="#estatus==10 && soloConsulta == 0" >
        <column id="numeroSecuencia" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
        </s:if>
        <column id="numeroSecuencia" type="ro" width="30" sort="int" hidden="false">#</column>
      	<column id="descripcionGiroAsegurado" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.inciso.descripcion.vehiculo" /></column>
		<s:if test="#estatus==10 && soloConsulta == 0">
			<column id="paquete" type="ro" width="100" sort="str" hidden="false">Paquete</column>
			<column id="valorPrimaNeta" type="ro" width="*" sort="str" hidden="false">Total de Primas</column>
			<column id="valorPrimaTotal" type="ro" width="*" sort="str" hidden="false">Prima Total</column>
		</s:if>
		<s:if test="#estatus==12 || soloConsulta == 1">
			
			<column id="numeroMotor" type="ro" width="137" sort="str">No. Motor</column>
			<column id="placa" type="ro" width="82" sort="str">Placas</column>
			<column id="nombreConductor" type="ro" width="200" sort="str">Conductor</column>
			<column id="nombreAsegurado" type="ro" width="200" sort="str">Asegurado</column>
		</s:if>
		<column id="accionDefinir" type="img" width="81" sort="na" align="center">Acciones</column>
		<s:if test="#estatus==10 && soloConsulta == 0">
			<column id="borrarInciso" type="img" width="30" sort="na" align="center"></column>
			<column id="verDetalle" type="img" width="30" sort="na" align="center"></column>
			<column id="colIgualarInciso" type="img" width="30" sort="na" align="center"></column>
		</s:if>
		<s:elseif test="#estatus==12 || soloConsulta == 1">
			<column id="verDetalle" type="img" width="30" sort="na" align="center"></column>
			<column id="verCondicionesEspeciales" type="img" width="30" sort="na" align="center"></column>
		</s:elseif>
	</head>
	<s:iterator value="incisoCotizacionList" status="row">
		<row id="<s:property value="id.numeroInciso"/>">
			<cell><s:property value="incisoAutoCot.numeroSerie" escapeHtml="true" escapeXml="true"/></cell>
			<s:if test="#estatus== 10 && soloConsulta == 0" >
			<cell>0</cell>
			</s:if>
			<cell><s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="incisoAutoCot.descripcionFinal" escapeHtml="false" escapeXml="true"/></cell>	
			<s:if test="#estatus==10 && soloConsulta == 0">
				<cell><s:property value="incisoAutoCot.paquete.descripcion" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="%{getText('struts.money.format',{valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="%{getText('struts.money.format',{valorPrimaTotal})}" escapeHtml="false" escapeXml="true"/></cell>		
			</s:if>
			<s:if test="#estatus==12 || soloConsulta == 1">
				<cell><s:property value="incisoAutoCot.numeroMotor" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="incisoAutoCot.placa" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="incisoAutoCot.nombreCompletoConductor" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="incisoAutoCot.nombreAseguradoUpper" escapeHtml="true" escapeXml="true"/></cell>
			</s:if>	
			<s:if test="#estatus==10 && soloConsulta == 0">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Multiplicar">
					<cell>../img/carAdd.png^Multiplicar Inciso^javascript:mostrarMultiplicarInciso(<s:property value="id.numeroInciso" escapeHtml="false" escapeXml="true"/>, <s:property value="id.idToCotizacion" escapeHtml="false" escapeXml="true"/>,<s:property value="incisoAutoCot.negocioSeccionId" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Eliminar">
					<cell>../img/carDelet.png^Eliminar Inciso^javascript:eliminarInciso(<s:property value="id.idToCotizacion" escapeHtml="false" escapeXml="true"/>,<s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/>,<s:property value="incisoAutoCot.negocioSeccionId" escapeHtml="false" escapeXml="true"/>,obtieneFiltros());^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Editar">
					<cell>../img/carEdit.png^Ver detalle Inciso^javascript:mostrarInciso(<s:property value="id.idToCotizacion" />,<s:property value="id.numeroInciso" />,0,<s:property value="numeroSecuencia"/>);^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Igualar">
					<cell>../img/calcIconAmount.jpg^Igualar Inciso^javascript: mostrarIgualarInciso("<s:property value="id.idToCotizacion" />","<s:property value="id.numeroInciso" />","<s:property value="incisoCotizacion.cotizacionDTO.claveEstatus" />")^_self</cell>
				</m:tienePermiso>	
			</s:if>	
			<s:elseif test="#estatus==12 && soloConsulta == 0">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Datos_Adicionales">
				<cell>../img/icons/ico_agregar.gif^Agregar Datos Inciso^javascript:mostrarDatosVehiculo(<s:property value="id.idToCotizacion" />,<s:property value="id.numeroInciso" />,0,<s:property value="numeroSecuencia"/>);^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Datos_Asegurado">
				<cell>../img/persona.jpg^Ver detalle Asegurado^javascript:mostrarVentanaAsegurado(<s:property value="id.numeroInciso" escapeHtml="false" escapeXml="true"/>,<s:property value="id.idToCotizacion" />,<s:property value="numeroSecuencia"/>);^_self</cell>
				</m:tienePermiso>
				<cell>../img/listsicon.gif^Condiciones Especiales^javascript:mostrarVentanaCondicionesEspeciales(<s:property value="id.numeroInciso" escapeHtml="false" escapeXml="true"/>,<s:property value="id.idToCotizacion" />,<s:property value="numeroSecuencia"/>, "window");^_self</cell>
			</s:elseif>
			<s:elseif test="soloConsulta == 1">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Editar">
					<cell>../img/carEdit.png^Ver detalle Inciso^javascript:mostrarInciso(<s:property value="id.idToCotizacion" />,<s:property value="id.numeroInciso" />,<s:property value="soloConsulta" />);^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Inciso_Datos_Adicionales">
					<cell>../img/icons/ico_agregar.gif^Ver Datos Inciso^javascript:mostrarDatosVehiculo(<s:property value="id.idToCotizacion" />,<s:property value="id.numeroInciso" />,<s:property value="soloConsulta" />,<s:property value="numeroSecuencia"/>);^_self</cell>
				</m:tienePermiso>
			</s:elseif>
		</row>
	</s:iterator>
</rows>