<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/terminar/terminarCotizacion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	var verExcepcionDetallePath = '<s:url action="verExcepcionDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
</script>
<s:form id="resultadoTerminarCotizacionForm">	
	<s:hidden name="id" id="id" />
	<s:hidden name="aplicaEndoso" id="aplicaEndoso"/>
	<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
	<s:if test="aplicaEndoso">
		<s:hidden name="polizaId" id="polizaId"/>
		<s:hidden name="tipoEndoso" id="tipoEndoso"/>
		<s:hidden name="accionEndoso" id="accionEndoso"/>
		<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
	</s:if>
	<div class="titulo" style="width:98%;" >
		<s:text name="midas.cotizacion.excepciones.title" />
	</div>
	<div style="overflow: auto; height: 400px; width: 98%">
		<table id="desplegar" border="0">
			<tr>
				<th><s:text name="midas.suscripcion.solicitud.autorizacion.seccion" /></th>		
				<th><s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>		
				<th>#<s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>				
				<th><s:text name="midas.suscripcion.solicitud.autorizacion.cobertura" /></th>
				<th><s:text name="midas.suscripcion.solicitud.autorizacion.datosIncumplidos" /></th>												
				<th><s:text name="midas.suscripcion.solicitud.autorizacion.nombreExcepcion" /></th>
			</tr>
		<s:iterator value="excepcionesList" status="stat">
			<tr class="bg_t2">
				<s:hidden name="excepcionesList[%{#stat.index}].idExcepcion"  value="%{idExcepcion}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].numExcepcion"  value="%{numExcepcion}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].descripcionExcepcion"  value="%{descripcionExcepcion}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].idToCotizacion"  value="%{idToCotizacion}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].idLineaNegocio"  value="%{idLineaNegocio}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].lineaNegocio"  value="%{lineaNegocio}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].idInciso"  value="%{idInciso}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].sequenciaInciso"  value="%{sequenciaInciso}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].idCobertura"  value="%{idCobertura}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].cobertura"  value="%{cobertura}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].id"  value="%{id}"/>
				<s:hidden name="excepcionesList[%{#stat.index}].descripcionRegistroConExcepcion"  value="%{descripcionRegistroConExcepcion}"/>	
				<td><s:property value="descripcionSeccion"/></td>	
				<td><s:property value="descripcionInciso"/></td>				
				<td><s:property value="sequenciaInciso"/></td>
				<td><s:property value="descripcionCobertura"/></td>				
				<td>				
					<ul>
						<s:iterator value="listaProvocadorExcepcion" var="provocadorExcepcion" status="statusLista">
							<li>								
								<s:property value="provocadorExcepcion"/>
							</li>	
						</s:iterator>
					</ul>				
				</td>									
				<td>
				<div align ='center'>
					<s:if test="idExcepcion >= 100">
						<a  onclick="javascript: mostrarExcepcionDetalle(<s:property value='idExcepcion' />);"
							href='javascript: void(0);'> <img border='0'
							src='/MidasWeb/img/icons/ico_verdetalle.gif'
							title='Ver Excepcion' /> </a>
					</s:if>
					<s:else>
						NA
					</s:else>
				</div>
				</td>
			</tr>			
		</s:iterator>
		</table>
	</div>
	<div style="width:98%;">
	<div id="cancelarAutorizacion" class="alinearBotonALaDerecha">
		<div class="btn_back w140" style="display: inline; float: left;">
			<a href="javascript: void(0);" onclick="cancelarAutorizacion();"> <s:text
					name="midas.suscripcion.solicitud.autorizacion.regresarCotizacion" /> </a>
		</div>
	</div>	
	<div id="enviarAutorizacion" class="alinearBotonALaDerecha">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="enviarAutorizacion();"> <s:text
					name="midas.suscripcion.solicitud.autorizacion.solicitarAutorizacion" /> </a>
		</div>
	</div>
	</div>
</s:form>

