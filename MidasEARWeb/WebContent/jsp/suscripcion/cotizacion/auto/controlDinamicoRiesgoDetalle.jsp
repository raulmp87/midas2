<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/datosRiesgo/datosRiesgo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/datosRiesgo/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<s:set var="sq" >'</s:set>
<s:set var="bs" ><s:text name="midas.general.backslash"  /></s:set>
<s:set var="idActual" value=""/>
<!-- Rendereo de Controles -->

<s:if test="controles.size == 0">
	<s:label  value="No se encontraron datos de riesgo" cssClass="titulo" />
</s:if>

<s:iterator value="controles">
	<s:set var="controlName" value="%{name + '[' + #sq + id + #sq + ']'}" />
	<s:if test="visible">
		<s:set var="idGenerado" value="name + '_' + #bs + #sq + id + #bs + #sq + '_'" />
		<!--	Este no tiene Backslash	-->
		<s:set var="idGenerado2" value="name + '_' + #sq + id + #sq + '_'" />
		<s:set var="idDepGenerado" value="name + '_' + #bs + #sq + dependencia + #bs + #sq + '_'" />
		<s:set var="validadorJs"
			value="validadorJs.generarValidacionJs(#idGenerado, #idDepGenerado, tipoValidador, tipoDependencia)" />
			
		<s:if test="#idActual != idNivel" >		
			<s:label  value="%{descripcionNivel}" cssClass="titulo" />
			<s:set var="idActual" value="%{idNivel}"/> 
		</s:if>
		<s:if test="tipoControl == 1">
			<s:select id="%{#idGenerado2}" name="%{#controlName}" 
			 value="%{valor}"   list="lista" label="%{etiqueta}"
				onchange="%{#validadorJs}" required="%{requerido}" labelposition="%{labelPosition}"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssStyle="width:200px;"
				disabled="%{deshabilitado}"
				cssClass="txtfield jQrequired"/>
		</s:if>
		<s:elseif test="tipoControl == 2">
			<s:select id="%{#idGenerado2}" name="%{#controlName}" 
			 value="%{valor}"   list="lista" label="%{etiqueta}"
				onchange="%{#validadorJs}" required="%{requerido}" labelposition="%{labelPosition}"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssStyle="width:200px;"
				disabled="%{deshabilitado}"
				cssClass="txtfield jQrequired"/>
		</s:elseif>
		<s:elseif test="tipoControl == 3">
			<s:textfield id="%{#idGenerado2}" name="%{#controlName}" 
			value="%{valor}" label="%{etiqueta}" cssClass="txtfield jQrequired"
			onblur="%{#validadorJs}"  required="%{requerido}"  size="50"
			disabled="%{deshabilitado}"
			labelposition="%{labelPosition}"/>
		</s:elseif>
		<s:elseif test="tipoControl == 4">
			<s:textarea id="%{#idGenerado2}" name="%{#controlName}" 
				value="%{valor}" label="%{etiqueta}" cssClass="txtfield jQrequired"
				disabled="%{deshabilitado}" cols="70" rows="3" 
				onblur="%{#validadorJs}" required="%{requerido}" labelposition="%{labelPosition}" />
		</s:elseif>		
		<s:elseif test="tipoControl == 5">
			<s:checkbox id="%{#idGenerado2}" name="%{#controlName}" value="%{valor}" label="%{etiqueta}" onchange="%{#validadorJs}" 
				disabled="%{deshabilitado}" required="%{requerido}" labelposition="%{labelPosition}" cssClass="cajaTexto"/>
		</s:elseif>	
		<!-- Es la cadena javascript a ejecutar segun el tipo de validacion al finalizar el rendereo. -->
		<s:set var="inicializacionJsValidacion"
			value="validadorJs.generaTipoDependenciaJs(#idGenerado, #idDepGenerado, tipoDependencia, true)" />
		
		<s:if test="#inicializacionJsValidacion != null && #inicializacionJsValidacion != '' ">
			<s:if test="#initializeJs == null">
				<s:set var="initializeJs" value="#inicializacionJsValidacion"/>
			</s:if>
			<s:else>
				<s:set var="initializeJs" value="#initializeJs + #inicializacionJsValidacion"/>	
			</s:else>
		</s:if>	
	</s:if>
	<s:else>
		<s:hidden name="%{#controlName}" value="%{valor}"/>
	</s:else>
</s:iterator>

<!-- Una vez rendereado los controles con validaciones que queremos que se ejecuten seran llamadas. -->
<script type="text/javascript">
	eval(<s:property value="#initializeJs"/>);
</script>	