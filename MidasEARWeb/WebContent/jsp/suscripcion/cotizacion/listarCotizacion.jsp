<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/cotizacionHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript">
	function cleanInput(id) {
		jQuery('#' + id).val('');
	}
	function cleanInputDiv(id) {
		jQuery('#' + id).text('');
	}

function validate_filters(fecha1, fecha2, idCotizacion) {	
	fechaCreacion = dwr.util.getValue("cotizacion.fechaCreacion");
	fechaFinal = dwr.util.getValue("fechaFinal");
	clave_estatus = dwr.util.getValue("cotizacion.claveEstatus");
	nombre_contratante = dwr.util.getValue("cotizacion.nombreContratante");
	codigo_usuario = dwr.util.getValue("cotizacion.codigoUsuarioCotizacion");
	descripcion_vehiculo = dwr.util.getValue("incisoCotizacionDTO.incisoAutoCot.descripcionFinal");
	centro_emisor = dwr.util.getValue("centroEmisorId");
	promotoria = dwr.util.getValue("promotoria.id");
	agente = dwr.util.getValue("agente.id");	
	negocio = dwr.util.getValue("cotizacion.solicitudDTO.negocio.idToNegocio");	
	producto = dwr.util.getValue("cotizacion.solicitudDTO.productoDTO.idToProducto");	
	descuento = dwr.util.getValue("descuento");	
	descuentoFin = dwr.util.getValue("descuentoFin");	
	primaDesde = dwr.util.getValue("cotizacion.valorPrimaTotal");	
	primaHasta = dwr.util.getValue("valorPrimaTotalFin");
	fechaSeguimiento = dwr.util.getValue("cotizacion.fechaSeguimiento");
	folio = dwr.util.getValue("cotizacion.folio");
	
	if(idCotizacion.trim() != '' || fechaCreacion.trim() != '' || fechaFinal.trim() != '' || clave_estatus.trim() != '' || nombre_contratante.trim() != '' 
		|| codigo_usuario.trim() != '' || descripcion_vehiculo.trim() != '' || centro_emisor.trim() != '' || promotoria.trim() != '' || agente.trim() != '' 
		|| negocio.trim() != '' || producto.trim() != '' || descuento.trim() != '' || descuentoFin.trim() != '' || primaDesde.trim() != '' || primaHasta.trim() != ''
		|| fechaSeguimiento.trim() != '' || folio.trim() != '') 
	{
		return (false);
	} else {
		return (true);
	}
}

function compareDates(){
	blockPage();
	fecha1=dwr.util.getValue("fechaCreacion");
	fecha2=dwr.util.getValue("fechaFinal");
	idCotizacion = dwr.util.getValue("cotizacion.idToCotizacion");
	folio = dwr.util.getValue("cotizacion.folio");
    if (compare_dates(fecha1, fecha2)){  
    	mostrarMensajeInformativo("La fecha Creaci�n no debe ser mayor a la fecha Hasta. Verificar.", 10, null); 
    } else if(validate_filters(fecha1, fecha2, idCotizacion)){
    	mostrarMensajeInformativo("Se debe seleccionar al menos 1 filtro para la b�squeda.", 10, null);
    } else if(idCotizacion.trim() == '' && compare_dates_period(fecha1, fecha2)) {
    	mostrarMensajeInformativo("El peri�do entre la fecha de Creaci�n desde y la fecha hasta no debe ser mayor a 1 a�o. Verificar.", 10, null);
    } else{
    	// Definir fechas por default cuando no se capture filtro restrictivo
    	if(idCotizacion.trim() == '' && folio == '')
    		definirFechasDefault("fechaCreacion", "fechaFinal");
    		
        cotizacionesGridPaginadoBtnBuscar(1,true);
    }
    unblockPage();
}

</script>
<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>
<s:form action="listar" id="cotizacionForm" introButon="submit">
	<s:hidden name="claveNegocio" id="claveNegocio" />
	<div class="titulo" style="width: 98%;">
		<s:if test="claveNegocio== \"A\"">
			<s:text name="Listado de Cotizaciones Autos"/>
		</s:if>
		<s:else>
			<s:text name="Listado de Cotizaciones Da�os"/>	
		</s:else>
	</div>
<!-- Filtros Inputs -->
<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>		
			<td>		
				<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;margin-left:0px">
					<tr>
						<td>
						
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizacion_Formal">
								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="iniciarCotizacionFormal()"> <s:text
											name="midas.suscripcion.cotizacion.cotizacionFormal" /> </a>
								</div>
								</m:tienePermiso>
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizacion_Express">
								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="iniciarCotizacionExpress();"> <s:text
											name="midas.suscripcion.cotizacion.cotizacionExpress" /> </a>
								</div>
								</m:tienePermiso>
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizador_Agentes">
								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);" id="cotizadorAgentes"
										onclick="iniciarCotizadorAgentes();">
										<s:text name="midas.boton.cotizacion.cotizadorAgente" /> 
									</a>
								</div>
								<div class="btn_back w150" style="display: inline; float: right;">
									<a href="<s:url  action="mostrarContenedor" namespace="/suscripcion/cotizacion/auto/agentes"/>" id="nuevoCotizadorAgentes" target="_blank">
									<s:text name="midas.suscripcion.cotizacion.autosIndividual" /> 
										</a>
								</div>
								</m:tienePermiso>
								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);" class="icon_limpiar"
										onclick="limpiarFormCotizacion();"> <s:text
											name="midas.suscripcion.cotizacion.limpiar" /> </a>
								</div>

								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);" id="mostrarFiltros"
										onclick="displayFilters();">Ocultar Filtros</a>
								</div>
						</td>
					</tr>
					<tr>
						<td>
							<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizador_Agentes">
								<div class="btn_back w200" style="display: inline; float: right;">
									<a href="javascript: void(0);" id="cargaMasivaServicioPublico"
										onclick="iniciarCargaMasivaServicioPublico();">
										<s:text name="midas.cotizador.cargaMasiva" />
									</a>
								</div>
							</m:tienePermiso>
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizador_Agentes">
								<div class="btn_back w200" style="display: inline; float: right;">
									<a href="javascript: void(0);" id="cotizadorServicioPublico"
										onclick="iniciarCotizadorServicioPublico();">
										<s:text name="midas.cotizador.servicioPublico" />
									</a>
								</div>
								</m:tienePermiso>
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Autoexpedibles">
								<div class="btn_back w130" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="iniciarAutoExpedibles()"> <s:text
											name="midas.cotizacion.autoexpedibles.autoExpedibles" /> </a>
								</div>
								</m:tienePermiso>
								<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cargas_Masivas_Individuales">
								<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="iniciarCargaMasivaIndividual()"> <s:text
											name="midas.cotizacion.cargamasivaindividual.cargaMasivaIndividual" /> </a>
								</div>
								</m:tienePermiso>
						</td>
					</tr>
				</table>				
			</td>		
		</tr>
	</table>
</div>
<!-- / Filtros Inputs -->	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0" class="fixTabla" >
			<tr>
			<td><s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.suscripcion.cotizacion.idtocotizacion"
					labelposition="top" 
					size="10"
					onkeypress="return soloNumerosM2(this, event, false)"
					onblur="this.value=jQuery.trim(this.value)"
					id="idToCotizacion" name="cotizacion.idToCotizacion" />
			</td>
		    <td>
					<sj:datepicker name="cotizacion.fechaCreacion" cssStyle="width: 170px;"
						key="midas.suscripcion.cotizacion.fecha.desde"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaCreacion"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td> 
		    <td>
					<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 170px;"
						key="midas.suscripcion.cotizacion.fecha.hasta"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			 </td>	
			<td >			
				<s:select
					key="midas.suscripcion.cotizacion.estatus"					
				    labelposition="top"
					id="claveEstatus" name="cotizacion.claveEstatus" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					list="listaEstatus"
					cssClass="txtfield" />						
			</td>		 	     			 
	    </tr>
        <tr>	
			<td>	
			 	<s:textfield cssClass="txtfield" cssStyle="width: 200px;"
						key="midas.suscripcion.cotizacion.nombreProspecto"					
					    labelposition="top"
					    size="25"
					id="nombreAsegurado" name="cotizacion.nombreContratante" onblur="validaLongitud('nombreAsegurado', this.value)" />
			 </td>								 	
			 <td><s:textfield  cssClass="txtfield" cssStyle="width: 200px;"
			 		key="midas.suscripcion.cotizacion.usuario"	
			        labelposition="top"
					id="codigoUsuarioCotizacion"
					name="cotizacion.codigoUsuarioCotizacion" onblur="validaLongitud('codigoUsuarioCotizacion', this.value)" /></td>	
			<td>
				<s:textfield  cssClass="txtfield" cssStyle="width: 200px;"
					key="midas.suscripcion.cotizacion.descvehiculo"
			        labelposition="top"
					id="agente" name="incisoCotizacionDTO.incisoAutoCot.descripcionFinal"
					onblur="validaLongitud('agente', this.value)" />
			</td>
			<td >							
				<s:select  labelposition="top" key="midas.negocio.centroOperacion"
					list="centrosEmisores" name="centroEmisorId" id="centrosEmisores" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					cssClass="txtfield" />						 				
			</td>			
		</tr>
		<tr>								
			<td>	
				<s:hidden id="idPromotoria" name="promotoria.id"/>
	 		<div>
	 			<label for="descripcionBusquedaPromotoria"><s:text name="midas.negocio.promotoria"></s:text></label>
	 		</div>
	 		<br/>
	 		<div style="vertical-align: bottom;" >
	 		<s:if test="!promotoriaControlDeshabilitado">
	 			<s:set var="promotoriaControlOnclick" value="'seleccionarPromotoria();'"/>
	 		</s:if>
	 		<div id="descripcionBusquedaPromotoria" class="divInputText txtfield" style="width: 180px; float: left;" onclick="${promotoriaControlOnclick}">
	 			<s:property  value="promotoria.descripcion"/>
	 		</div>
	 		<s:if test="!promotoriaControlDeshabilitado">
	 		<div style="float: left; vertical-align: bottom;">
	 		<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripci�n" 
	 			style="margin-left: 5px; "
	 			onclick ="cleanInputDiv('descripcionBusquedaPromotoria');cleanInput('idPromotoria');"/>
	 		</div>
	 		</s:if>
	 		</div>	
			</td>
			<td>		
				<s:hidden id="idAgenteCot" name="agente.id"/>
			<div><label for="agenteNombre"><s:text name="midas.negocio.agente"></s:text></label></div>
			<br/>
			<div style="vertical-align: bottom;" >
			<s:if test="!agenteControlDeshabilitado">
	 			<s:set var="agenteControlOnclick" value="'seleccionarAgenteCotizacionBuscar(1);'"/>
	 		</s:if>
			<div id="agenteNombre" class="divInputText txtfield" style="width: 180px; float: left;" onclick="${agenteControlOnclick}">
				<s:property value="agente.persona.nombreCompleto"/>
	 		</div>
	 		<div style="float: left; vertical-align: bottom;">
	 		<s:if test="!agenteControlDeshabilitado">
 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripci�n" 
	 			style="margin-left: 5px; "
	 			onclick ="cleanInputDiv('agenteNombre');cleanInput('idAgenteCot');"
	 		/>	
	 		</s:if>
	 		</div>
	 		</div>
			</td>
			<td>								
				<s:select labelposition="top" key="midas.suscripcion.solicitud.solicitudPoliza.negocio"
					list="negocioAgenteList" name="cotizacion.solicitudDTO.negocio.idToNegocio" id="negocios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 	
					onchange="cargarComboSimpleDWR(this.value, 'productos', 'getMapProductos')" 			
					listKey="idToNegocio" listValue="descripcionNegocio" 				
					cssClass="txtfield" />		
			</td>		
			<td>
				<s:select key="midas.suscripcion.cotizacion.producto" 
						  name="cotizacion.solicitudDTO.productoDTO.idToProducto" id="productos"
						  labelposition="top" 
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  list="productoList" listKey="idToProducto" listValue="descripcion" 
						  cssClass="txtfield" /> 							
			</td>								
		</tr>
		
	 	<tr>	
	 	<td>
								<s:textfield  cssClass="txtfield" cssStyle="width: 50px;"
						 		key="midas.suscripcion.cotizacion.descuento"
						 		size="4"
						        labelposition="top"
								id="descuento" name="descuento" 
								onkeypress="return soloNumerosM2(this, event, false)" /> <!-- Fin de campo por definir -->
						</td>
						 <td><s:textfield  cssClass="txtfield" cssStyle="width: 50px;"
						 		key="midas.suscripcion.cotizacion.descuento_a"
						 		size="4"
						        labelposition="top"
								id="descuentoFin" name="descuentoFin"
								onkeypress="return soloNumerosM2(this, event, false)" /> <!-- Fin de campo por definir -->
						 </td>	
						<td>
			 				<s:textfield cssClass="txtfield" cssStyle="width: 50px;"
								key="midas.suscripcion.cotizacion.primaNetaAnual"
								size="10"
						        labelposition="top"
								id="valorPrimaTotal" name="cotizacion.valorPrimaTotal"
								onkeypress="return soloNumerosM2(this, event, false)" />
						</td>
						<td>
							<s:textfield cssClass="txtfield" cssStyle="width: 50px;"
								key="midas.suscripcion.cotizacion.primaNetaAnualFin"
								size="10"
						        labelposition="top"
								id="valorPrimaTotalFin" name="valorPrimaTotalFin"
								onkeypress="return soloNumerosM2(this, event, false)" />
						</td>	
			</tr>
			<tr>
			 	<td>
			 		<s:radio  key="midas.suscripcion.cotizacion.aplicaFlotilla"
						name="cotizacion.tipoPolizaDTO.claveAplicaFlotillas"
 						list="#{'1':'Si','0':'No','':'Todos'}" value="" labelposition="top" />
				</td>										 
				<td>
					<sj:datepicker name="cotizacion.fechaSeguimiento" cssStyle="width: 170px;"
						key="midas.cotizacion.fechaSeguimiento"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaSeguimiento"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td>
				<td>
					<s:text name="midas.suscripcion.solicitud.solicitudPoliza.folio" />:
					<s:textfield id="numeroFolio" name="cotizacion.folio" cssClass="txtfield" />
					<%-- <s:text name="midas.cotizacion.conflictoNumeroSerie" />:
					 <s:checkbox name="cotizacion.conflictoNumeroSerie" id="conflictoNumeroSerie"  /> --%>
				</td>
		</tr>
		<tr>
			<td colspan="4">
				<div class="btn_back w140" style="display: inline; float: right;">                    
										<a id="submit" href="javascript: void(0);" onclick="compareDates();" class="icon_buscar">
										
											<s:text name="midas.suscripcion.cotizacion.buscar" /> </a>
				</div>
			</td>
		</tr>
		</table>
		<br/>
</div>
</s:form>
<div id="indicador"></div>
<div id="gridCotizacionPaginado" 
	style="padding-left:.5em; padding-right:.5em;  margin-left:.5em; margin-right:.4em;">
	<div id="cotizacionGrid" style="width: 98%;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
</div>

<script type="text/javascript">
	//listarCotizacionPaginado(1, true);
</script>
