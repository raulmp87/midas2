<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/suscripcion/solicitud/asignar/asignarHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
//<!--
function onChangeSeleccion(tipoAsignacion){

	var path = '/MidasWeb/suscripcion/solicitud/asignar/mostrarVentana.action?tipoAsignacion='+tipoAsignacion+'&id='+jQuery('#id').val();
	var asignar = parent.dhxWins.window('asignar');
	asignar.attachURL(path);
}

function disabledSuscriptor(){
 if(jQuery('#claveEstatus').val()=='2'){
  document.asignarUsuarioForm.tipoAsignacion[1].disabled = true;
 }
}


	function guardarComentarios() {
	if(jQuery("#codigoUsuarioAsignacion").val() == '' || jQuery("#codigoUsuarioAsignacion").val() == -1){
		if(jQuery("#tipoAsignacionVal").val() == '1'){
			alert("Debe seleccionar a un Coordinador.")			
		}else{
			alert("Debe seleccionar a un Suscriptor.")
		}
		return;
	}
    var answer = confirm("\u00BFEst\u00e1 seguro que desea asignar solicitud?");
      if(answer){	
         if( (
              document.asignarUsuarioForm.tipoAsignacionVal.value=="2" ) &&
             (document.asignarUsuarioForm.valor.value == ""
					|| document.asignarUsuarioForm.valor.value.length == 0)) {
				alert("Debe de capturar el campo Comentarios.");
				return false;
			} else {
				mostrarIndicadorCarga('indicador');
				document.forms["asignarUsuarioForm"].submit();
			}
			
	   }else{
	   	parent.dhxWins.window('asignar').setModal(false);
		parent.dhxWins.window('asignar').hide();
		parent.ventanaAsignacion=null;
	   }
	}
//-->
</script> 
<div id="agregar">
	<s:form action="asignar" namespace="/suscripcion/solicitud/asignar"
		id="asignarUsuarioForm">
		<s:hidden name="id" id="id" />
		<s:hidden name="solicitud.claveEstatus" id="claveEstatus"/>
		<s:hidden name="claveTipoSolicitud" value="%{solicitud.claveTipoSolicitud}" />
		<s:hidden id="tipoAsignacionVal" name="tipoAsignacionVal" value="%{tipoAsignacion}" />
		<div class="titulo">
			<s:text name="midas.suscripcion.solicitud.asignar.titulo" />:<s:property value="solicitud.numeroSolicitud" />
		</div>
		 <s:if test="esMesaControl==true">		
			<s:radio name="tipoAsignacion" id='tipoAsignacion'
				list="tipoAsignaciones"
				onclick="onChangeSeleccion(this.value);" />
		</s:if>
		<s:else>
			<s:hidden name="tipoAsignacion" value="C" disabled="true" />
		</s:else>		
		<s:if test="esMesaControl==true">
			<s:if test="tipoAsignacion==1">
				<s:select id="codigoUsuarioAsignacion"
					list="usuariosCoordinadores"
					name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="nombreUsuario" listValue="nombreCompleto"
					cssClass="cajaTexto" />				

			</s:if>
			<s:else>
				<s:select id="codigoUsuarioAsignacion"
					list="usuariosSuscriptores"
					name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					listKey="nombreUsuario" listValue="nombreCompleto"
					cssClass="cajaTexto" />
			</s:else>
		</s:if>
		<s:else>
			<s:select id="codigoUsuarioAsignacion"
				list="usuariosCoordinadores"
				name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
				headerValue="%{getText('midas.general.seleccione')}"
				listKey="nombreUsuario" listValue="nombreCompleto"
				cssClass="cajaTexto" />
		</s:else>
		
<%-- 	 <s:if test="solicitud.claveTipoSolicitud==2">	 --%>
	    <s:if test="tipoAsignacion==2">	
         <s:textarea key="midas.suscripcion.solicitud.comentarios.comentario" 
                     id="valor" name="valor" 
                     cssClass="textarea jQalphaextra jQrestrict jQrequired" 
                     title="count[400]" labelposition="top" 
                     cssStyle="width:400px;height:50px;"/>
<%--       </s:if>                --%>
     </s:if>                
		

	<s:if test="%{mensaje == null}">
	 <div class="btn_back w140"  style="display:inline; float: left; ">
		<a href="javascript: void(0);"
			onclick="guardarComentarios();"> <s:text
			name="midas.boton.guardar" /> </a>
	   </div>		
	</s:if>
		<div id="indicador"></div>
	</s:form>
</div>
<div id="central_indicator" class="sh2" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		src="/MidasWeb/img/as2.gif" alt="Afirme" />
</div>

<script type="text/javascript">
 disabledSuscriptor();
</script>