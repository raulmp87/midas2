<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script src="<s:url value='/js/midas2/suscripcion/solicitud/solicitudPoliza.js'/>"></script>
<div id="mensajeImg">
	<img src='/MidasWeb/img/b_ok.png'>
</div>
<div id="mensaje_texto" class="mensaje_texto">
	<font color="gray"><s:text name="midas.solicitud.rechazo.success.message"/></font>
</div>
 <div class="btn_back w140"  style="display:inline; float: right; top:40%;">
			   <a href="javascript: void(0);"
				 onclick="javascript: cerrarVentanaSolicitud();">
				  <s:text
				  name="midas.boton.aceptar" /> </a>
 </div>