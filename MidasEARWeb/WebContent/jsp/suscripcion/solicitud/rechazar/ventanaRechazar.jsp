<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/suscripcion/solicitud/rechazar/rechazarHeader.jsp"></s:include>
<style>
<!--
body{
	overflow: hidden;
}
-->
</style>
<script type="text/javascript">
<!--
function guardarRechazar() {
	mostrarIndicadorCarga('indicador');
	document.forms["rechazarForm"].submit();
}
//-->
</script>
<div id="agregar">
<s:form action="rechazar" namespace="/suscripcion/solicitud/rechazar" id="rechazarForm" >
<div class="titulo">
	<s:text name="midas.suscripcion.solicitud.rechazar.titulo"/>:<s:property value="solicitud.numeroSolicitud"/> 
</div>
  <s:hidden name="rechazar"  id="agregar"/>
	<s:hidden name="solicitud.numeroSolicitud" />
	<s:hidden name="id" />
	<s:textarea name="comentario" cssClass="textfield jQrequired"
		cols="35"
		cssStyle="font-size:12px;"
		rows="1"
		maxlength="500"
		labelposition="%{getText('label.position')}"
		key="midas.suscripcion.solicitud.rechazar.comentario"
		javascriptTooltip="%{getText('midas.suscripcion.solicitud.rechazar.comentario')}" 
		/>
    <div id="divRechazarBtn" class="w100" style="display: block; float:right;">
					<div class="btn_back w100"  >
						<a href="javascript: void(0);" onclick="(validateAll(true,'save')) ? guardarRechazar() : false" class="b_submit icon_rechazar w100">	
							<s:text name="midas.boton.rechazar"/>	
						</a>
                    </div>
                </div>
<br/>	
</s:form>	
<div id="indicador"></div>
</div>