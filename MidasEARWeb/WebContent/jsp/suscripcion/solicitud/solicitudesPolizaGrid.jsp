<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="solicitud.numeroSolicitud" type="ro" width="100" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.folio" /></column>
		<column id="solicitud.nombrePersona"  type="ro" width="200" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.nombreCliente" /></column>
		<column id="solicitud.nombreAgente" type="ro" width="200" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.nombreAgente" /></column>
		<column id="solicitud.productoDTO.descripcion" type="ro" width="150" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.producto" /></column>
		<column id="solicitud.descripcionTipoSolicitud" type="ro" width="150" sort="str" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.tipoMovimiento" />
		</column>
		<column id="solicitud.fechaCreacion" type="ro" width="170" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.fecha" /></column>
		<column id="solicitud.descripcionEstatus" type="ro" width="100" sort="str" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.estatus" />
		</column>
		<column id="solicitud.codigoUsuarioCreacion" type="ro" width="200" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.solicitante" /></column>
		<column id="solicitud.codigoUsuarioAsignacion" type="ro" width="200" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.asignacion" /></column>
		<column id="solicitud.codigoAgente" type="ro" width="120" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.claveAgente" /></column>
		<column id="solicitud.nombreOficina" type="ro" width="200" sort="int" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.oficina" /></column>
		<column id="editar" type="img" width="30" align="center"></column>
		<column id="asignar" type="img" width="30" align="center"></column>	
		<column id="rechazar" type="img" width="30" align="center"></column>	
		<column id="comentarios" type="img" width="30" align="center"></column>
		<column id="cancelar" type="img" width="30" align="center"></column>
		<column id="descargar" type="img" width="30" sort="na"/>
		<column id="reasignar" type="img" width="30" sort="na"/>
		<column id="impresiones" type="img" width="30" sort="na"/>
		<column id="cotizarFormalmente" type="img" width="30" sort="na"/>
		<column id="extra" type="img" width="30" sort="na"/>
		<column id="descargarCC" type="img" width="30" sort="na"/>
</head>
		
	<s:iterator value="solicitudes" status="stats">
		<row id="<s:property value='%{stats.index}' />">
			<cell><s:property value="numeroSolicitud" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombrePersona" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoPaterno" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoMaterno" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="productoDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTipoSolicitud" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:date name="fechaCreacion"  format="dd/MM/yyyy HH:mm" /></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoUsuarioAsignacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agente.idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreEjecutivo" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>			
			<s:if test="claveEstatus!= \"2\" && claveEstatus!= \"8\" && claveEstatus!= \"9\"">
				<s:if test="codigoUsuarioAsignacion == null, tipoCabina">
					<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Asignar_Coordinador,FN_M2_Emision_Solicitudes_Asignar_Suscriptor">
					<cell>/MidasWeb/img/icons/ico_asignar.gif^Asignar^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>
					</m:tienePermiso>
				</s:if>
				<s:else>
					<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Reasignar_Coordinador,FN_M2_Emision_Solicitudes_Reasignar_Suscriptor">
					<cell>../img/icons/ico_asignar.gif^Reasignar^javascript:mostrarVentanaReasignar(<s:property value="idToSolicitud" />, parent.cerrarVentanaReasignar)^_self</cell>
					</m:tienePermiso>
				</s:else>
			</s:if>	
			<s:if test="claveEstatus!= \"2\" && claveEstatus!= \"8\" && claveEstatus!= \"9\"">
				<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Rechazar">
				<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Rechazar^javascript: mostrarVentanaRechazar(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>,"parent.obtenerSolicitudesEmision()")^_self</cell>
				</m:tienePermiso>
				<s:if test="!tipoCabina">
				<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Eliminar">
				<cell>/MidasWeb/img/common/b_delete.gif^Cancelar^javascript: mostrarCancelarSolicitud(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</m:tienePermiso>
				</s:if>
				<s:if test="!tipoCabina">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Cotizacion_Formal">
				<cell>../img/complementar.png^Cotizar formalmente^javascript: $_iniciarCotizacionFormal_bandeja(<s:property value="codigoAgente"/>,<s:property value="negocio.idToNegocio"/>,<s:property value="productoDTO.idToProducto"/>,<s:property value="idToSolicitud"/>)^_self</cell>
				</m:tienePermiso>
				</s:if>
			</s:if>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Comentarios">
			<s:if test="esBusquedaRapida">
			<cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>,0)^_self</cell>
			</s:if>
			<s:else>
			<cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>,3)^_self</cell>
			</s:else>
			</m:tienePermiso>
			<s:if test="!tipoCabina">
			<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Adjuntar_Documentos">
			<cell>../img/b_enviar.jpg^Adjuntar Documentos^javascript: mostrarVentanaAdjuntarDocumentos(<s:property value='idToSolicitud'/>);^_self</cell>
			</m:tienePermiso>
			</s:if>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Imprimir">
	  		<cell>../img/b_printer.gif^Imprimir Solicitud^javascript: mostrarSolicitudImpresion(<s:property value="idToSolicitud"/>)^_self</cell>
	  		</m:tienePermiso>
	  		<cell>../img/common/b_excel.gif^Carta Cobertura^/MidasWeb/solicitud/entramite/descargar.action?idToSolicitud=<s:property value="idToSolicitud"/></cell>
		</row>
	</s:iterator>	
	
</rows>
