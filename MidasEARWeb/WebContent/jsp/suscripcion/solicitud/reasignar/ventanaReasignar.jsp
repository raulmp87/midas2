<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/suscripcion/solicitud/asignar/asignarHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
<!--
function onChangeSeleccion(tipoAsignacion){

	var path = '/MidasWeb/suscripcion/solicitud/reasignar/mostrarVentana.action?tipoAsignacion='+tipoAsignacion+'&id='+jQuery('#id').val();
	var reasignar = parent.dhxWins.window('reasignar');
	reasignar.attachURL(path);
}


function guardar() {
	if(jQuery("#codigoUsuarioAsignacion").val() == '' || jQuery("#codigoUsuarioAsignacion").val() == -1){
		if(jQuery("#tipoAsignacionVal").val() == '1'){
			alert("Debe seleccionar a un Coordinador.")			
		}else{
			alert("Debe seleccionar a un Suscriptor.")
		}
		return;
	}
var answer = confirm("\u00BFEst\u00e1 seguro que desea re-asignar solicitud?");
 
 if(answer){
  mostrarIndicadorCarga('indicador');
  document.forms["asignarUsuarioForm"].submit();
 }else{
   	parent.dhxWins.window('reasignar').setModal(false);
	parent.dhxWins.window('reasignar').hide();
	parent.ventanaReasignacion=null;
 }
}
//-->
</script> 
<div id="agregar">
	<s:form action="reasignar" namespace="/suscripcion/solicitud/reasignar"
		id="asignarUsuarioForm">
		<s:hidden name="id" id="id" />
		<s:hidden name="solicitud.claveEstatus" id="claveEstatus"/>
		<s:hidden id="tipoAsignacionVal" name="tipoAsignacionVal" value="%{tipoAsignacion}" />
		<div class="titulo">
			<s:text name="Reasignar Solicitud" />:<s:property value="solicitud.numeroSolicitud" />
		</div>
		 <s:if test="esMesaControl==true">		
			<s:radio name="tipoAsignacion" id='tipoAsignacion'
				list="tipoAsignaciones"
				onclick="onChangeSeleccion(this.value);" />
		</s:if>
		<s:else>
			<s:hidden name="tipoAsignacion" value="C" disabled="true" />
		</s:else>		
		<s:if test="esMesaControl==true">
			<s:if test="tipoAsignacion==1">
				<s:select id="codigoUsuarioAsignacion"
					list="usuariosCoordinadores"
					name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="nombreUsuario" listValue="nombreCompleto"
					cssClass="cajaTexto" />				

			</s:if>
			<s:else>
				<s:select id="codigoUsuarioAsignacion"
					list="usuariosSuscriptores"
					name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					listKey="nombreUsuario" listValue="nombreCompleto"
					cssClass="cajaTexto" />
			</s:else>
		</s:if>
		<s:else>
			<s:select id="codigoUsuarioAsignacion"
				list="usuariosCoordinadores"
				name="solicitud.codigoUsuarioAsignacion" headerKey="-1"
				headerValue="%{getText('midas.general.seleccione')}"
				listKey="nombreUsuario" listValue="nombreCompleto"
				cssClass="cajaTexto" />
		</s:else>         
		

	
	 <div class="btn_back w140"  style="display:inline; float: left; ">
		<a href="javascript: void(0);"
			onclick="guardar()"> <s:text
			name="midas.boton.guardar" /> </a>
	   </div>		
	
		<div id="indicador"></div>
	</s:form>
</div>
<div id="central_indicator" class="sh2" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		src="/MidasWeb/img/as2.gif" alt="Afirme" />
</div>

<script type="text/javascript">
</script>