<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/suscripcion/solicitud/solicitudPolizaHeader.jsp"></s:include>




<script type="text/javascript">

	function ocultaTipoliza(){
		jQuery('#tipoPersona').hide('slow');
	}
	jQuery(document).ready(function(){
		if(!jQuery('#tipoPersona').is(':visible')){
			jQuery('#tipoPersona').show('slow');
		}else{
			jQuery('#tipoPersona').hide('slow');
		}
		showUpExcel(jQuery('#tieneCartaCoberturaDiv input[checked=checked]').val());
	});
	
	function changeTipoPersona(valor){
		if(!jQuery('#contenedorFiltros').is(':visible')){
			jQuery('#contenedorFiltros').show();
			reSizeGridSolicitud();
		}
		jQuery('#claveTipoPersona').val(valor);
		if(valor == 2){
			jQuery('#nombreCliente').hide();
			jQuery('#razonSocial').show('slow');
			jQuery('#razonSocialText').attr('disabled','');
			jQuery('#nombrePersona').attr('disabled','disabled');
			jQuery('#apellidoPaterno').attr('disabled','disabled');
			jQuery('#apellidoMaterno').attr('disabled','disabled');
		}else{
			jQuery('#razonSocial').hide();
			jQuery('#razonSocialText').attr('disabled','disabled');
			jQuery('#nombreCliente').show('slow');
			jQuery('#apellidoPaterno').attr('disabled','');
			jQuery('#nombrePersona').attr('disabled','');
			jQuery('#apellidoMaterno').attr('disabled','');
		}
	}
	
 function compare_dates(fecha, fecha2)  
{  
  var xMonth=fecha.substring(3, 5);  
  var xDay=fecha.substring(0, 2);  
  var xYear=fecha.substring(6,10);  
  var yMonth=fecha2.substring(3, 5);  
  var yDay=fecha2.substring(0, 2);  
  var yYear=fecha2.substring(6,10);  
  if (xYear> yYear)  
  {  
      return(true)  
  }  
  else  
  {  
    if (xYear == yYear)  
    {   
      if (xMonth> yMonth)  
      {  
          return(true)  
      }  
      else  
      {   
        if (xMonth == yMonth)  
        {  
          if (xDay> yDay)  
            return(true);  
          else  
            return(false);  
        }  
        else  
          return(false);  
      }  
    }  
    else  
      return(false);  
  }  
}  

function compareDates(){
	fecha1=dwr.util.getValue("fechaCreacion");
	fecha2=dwr.util.getValue("fechaFinal");
    if (compare_dates(fecha1, fecha2)){  
    	mostrarMensajeInformativo("La fecha Creación no debe ser mayor a la fecha Hasta. Verificar.", 10, null); 
      }else{
        pageGridPaginado(1, true);
      }	
}

function showUpExcel(value){
	if(value == 0){
		jQuery('#upExcel').hide();
	}else if (value == 1){
		jQuery('#upExcel').show();
	}
}

function showWindowsByUpExcel(idSolicitud) {
	if(dhxWins != null) 
		dhxWins.unload();
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(true);
	dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
	var adjuntarDocumento = dhxWins.createWindow("SubeExcelWindow", 34, 100, 440, 265);
	adjuntarDocumento.setText("Excel");
	adjuntarDocumento.button("minmax1").hide();
	adjuntarDocumento.button("park").hide();
	adjuntarDocumento.setModal(true);
	adjuntarDocumento.center();
	adjuntarDocumento.denyResize();
	adjuntarDocumento.attachHTMLString("<div id='vault'></div>");

	var vault = new dhtmlXVaultObject();
    vault.setImagePath("/MidasWeb/img/dhtmlxvault/spn/");
    vault.setServerHandlers("/MidasWeb/sistema/vault/uploadHandler.do",
            "/MidasWeb/sistema/vault/getInfoHandler.do",
            "/MidasWeb/sistema/vault/getIdHandler.do");
    //vault.strings.remove = "Eliminar"; // Remove 
    //vault.strings.done = "Hecho"; // Done 
    //vault.strings.error = "Error"; // Error 
    vault.setFilesLimit(1);
    vault.onUploadComplete = function(files) {
    	new Ajax.Request('/MidasWeb/sistema/vault/getFileInformation.do', { method : "post", asynchronous : false, parameters : null, 
    		onSuccess : function(transport) {
    			var xmlDoc = transport.responseXML;
    			var items = xmlDoc.getElementsByTagName("item");
    			var item = items[0];
    			var idRespuesta = item.getElementsByTagName("id")[0].firstChild.nodeValue;
    			if (idRespuesta == '1'){
    				var nombreArchivo = item.getElementsByTagName("nombreArchivo")[0].firstChild.nodeValue;
    				var idToControlArchivo = item.getElementsByTagName("idToControlArchivo")[0].firstChild.nodeValue;
    		    	
   					var pathId = "?idToSolicitud="+idSolicitud + "&idToControlArchivo="+idToControlArchivo;
   					var url = "/MidasWeb/solicitud/entramite/carga.action" + pathId;
   					sendRequestJQ(null, url, 'contenido',null);
    				
    			}else if(idRespuesta == '2'){
    				mostrarVentanaMensaje("10", "Error al cargar el archivo, Intente nuevamente", null);
    			}
    		} // End of onSuccess    		
    	});
        parent.dhxWins.window("SubeExcelWindow").close();
    };
    vault.create("vault");
    vault.setFormField("claveTipo", "16");
    
}
</script>
<s:if test="solicitud.claveTipoPersona == 2">
	<script type="text/javaScript">
		jQuery('#nombreCliente').hide();
		jQuery('#razonSocial').show('slow');
	</script>
</s:if>
<s:else>
<script type="text/javaScript">
		jQuery('#razonSocial').hide();
		jQuery('#nombreCliente').show('slow');
	</script>
</s:else>
<s:if test="(id != null && id != '')">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:else>
	<s:set var="disabledConsulta">false</s:set>	
	<s:set var="showOnConsulta">both</s:set>
</s:else>

<s:if test="usuarioService.tienePermisoUsuarioActual('FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual') || #disabledConsulta">
	<s:set var="controlesAgenteDisabled">true</s:set>
</s:if>
<s:else>
	<s:set var="controlesAgenteDisabled">false</s:set>
</s:else>

<s:if test="usuarioService.tienePermisoUsuarioActual('FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual') || usuarioService.tienePermisoUsuarioActual('FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual') || #disabledConsulta">
	<s:set var="controlEjecutivoDisabled">true</s:set>
</s:if>
<s:else>
	<s:set var="controlEjecutivoDisabled">false</s:set>
</s:else>
<div class="titulo"><s:text name="midas.suscripcion.solicitud.solicitudPoliza" /></div>
<div id="detalle" >
	<center>	
		<table id="filtros" width="98%" >
			<tr>
				<td>
					<s:radio name="seleccion"  list="tipoOperaciones" 
					         onclick="opcionCaptura(this.value,null);ocultaTipoliza();"  id ="seleccion"/>	
				</td>
				<td>
					<div id="tipoPersona" style="display:none;">
							<s:radio name="solicitud.claveTipoPersona" list="#{'1':'Física','2':'Moral'}"  
								disabled="%{#disabledConsulta}"
							 onclick="changeTipoPersona(this.value);"/>					
							
					</div>
				</td>
				<td>
				<s:if test="tipoCabina">
					<div id="divBuscarEmisionBtn" class="w180" style="display: block;  float: right;">
						<div class="btn_back w170" >
							<a id="submit" href="javascript: void(0);" alt="Solicitudes del d&iacute;a" 
								title="Solicitudes del d&iacute;a"  
								onclick="pageGridPaginadoEmision(1, true);" class="icon_buscar">	
								<s:text name="midas.suscripcion.solicitud.solicitudPoliza.busquedaRapida"/>	
							</a>
                     	</div>
                     </div>
                     </s:if>
				</td>
			</tr>
		</table>
		
		<s:form action="mostrar" id="solicitudPolizaForm">
			<s:hidden name="usuario.nombreUsuario"  id="nombreUsuario"/>		
			<s:hidden name="id"  id="solicitud.idToSolicitud"/>			
			<s:hidden name="idToNegocio"  id="idToNegocio"/>
			<s:hidden name="idToCACartaCobertura" id="idToCACartaCobertura" value="0"/>
			<s:hidden name="claveNegocio"  id="claveNegocio"/>
			<s:hidden name="solicitud.idToPolizaAnterior"  id="idToPolizaAnterior" value="0"/>
			<s:hidden name="solicitud.claveOpcionEmision"  id="claveOpcionEmision" value="0"/>
			<s:hidden name="solicitud.claveTipoPersona"  id="claveTipoPersona" value="1"/>
			<s:hidden name="tipoBusqueda"  id="tipoBusqueda"/> 
			<div id="controlArchivoTemp"></div> 	
			<div id="contenedorFiltros" >		
				<table id="agregar" border="0" class="fixTablaPoliza">
					<tr>
						<td>
							<s:if test="seleccion == 1">
								<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.folio" cssStyle="width: 100px;"
											 cssClass="txtfield jQnumeric jQrestrict" labelposition="top"
											 size="12"
											 maxlength="10"
											 id="idToSolicitud" name="solicitud.idToSolicitud" />							
							</s:if>
							<s:else>
								<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.folio" cssStyle="width: 100px;"
											 cssClass="txtfield jQnumeric jQrestrict" labelposition="top" 
											 size="12"
											 maxlength="10"
											 id="idToSolicitud" name="solicitud.idToSolicitud" disabled="true"/>
								<s:hidden name="solicitud.idToSolicitud" id="idToSolicitud"/>																										 											
							</s:else>
						</td>
						<td>
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.solicitante" cssStyle="width: 245px;"
									cssClass="txtfield" labelposition="top" size="30"
										 id="usuario.nombreCompleto" name="usuario.nombreCompleto" disabled="true" />
						</td>	
						<td>
						<s:if test="seleccion == 1">
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.asignacion" cssStyle="width: 245px;"
									cssClass="txtfield" labelposition="top" size="30"
										 id="solicitud.codigoUsuarioAsignacion" name="solicitud.codigoUsuarioAsignacion" disabled="%{#disabledConsulta}" />
						</s:if><s:else>
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.asignacion" cssStyle="width: 245px;"
									cssClass="txtfield" labelposition="top" size="30"
										 id="solicitud.codigoUsuarioAsignacion" name="solicitud.codigoUsuarioAsignacion" disabled="true" />
						</s:else>
						</td>				
					</tr>
					<tr id="razonSocial" style="display: none">
						<td colspan="2">
							<s:textfield key="midas.catalogos.centro.operacion.razonSocial" cssStyle="width: 245px;"
								cssClass="txtfield jQalphaextra jQrestrict"
								labelposition="top" 
								size="20" required="%{seleccion==2}"
								maxlength="100"
								disabled="%{#disabledConsulta}"
							    id="razonSocialText" name="nombreRazonSocial"/>
						</td>
					</tr>
					<tr id="nombreCliente">
						<td>
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.nombreCliente" cssStyle="width: 245px;"
								cssClass="txtfield jQalphaextra jQrestrict"
								labelposition="top" 
								size="20" required="%{seleccion==2}"
								maxlength="50"
					            disabled="%{#disabledConsulta}"
							    id="nombrePersona" name="solicitud.nombrePersona"  />	
	
						</td>
						<td>
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno" cssStyle="width: 245px;"
								cssClass="txtfield  jQalphaextra jQrestrict"
								labelposition="top" 
								size="18" required="%{seleccion==2}"
								maxlength="50"
					            disabled="%{#disabledConsulta}"
							    id="apellidoPaterno" name="solicitud.apellidoPaterno"/>							             					
						
						</td>	
						<td>
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno" cssStyle="width: 245px;"
								cssClass="txtfield  jQalphaextra jQrestrict"
								labelposition="top"
								size="18" required="%{seleccion==2}"
								maxlength="50"
					            disabled="%{#disabledConsulta}"
							    id="apellidoMaterno" name="solicitud.apellidoMaterno" />						
						</td>					
					</tr>
					<tr>
					
						<td>
							<s:hidden id="idTcAgenteHdn" name="solicitud.codigoAgente" />
							<s:textfield key="midas.suscripcion.solicitud.solicitudPoliza.claveAgente" cssStyle="width: 245px;"
								cssClass="txtfield  jQnumeric jQrestrict"
								maxlength="10" required="%{seleccion==2}"
								labelposition="top" 
					            disabled="%{#controlesAgenteDisabled}"
								onblur="obtenerDatosAgente(this.value, %{seleccion});"
								id="idTcAgente" name="solicitud.agente.idAgente" />																				
						</td>
						<td>
		 					<s:select key="midas.suscripcion.solicitud.solicitudPoliza.oficina" 
		 							  name="solicitud.codigoEjecutivo" id="codigoEjecutivo"
		 							  labelposition="top" 
		 							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 							  list="oficinasList"
					                  disabled="%{#controlEjecutivoDisabled}"
									  onchange="obtieneAgentes(this.value,%{seleccion});" 
									  required="%{seleccion==2}"
									  cssClass=" txtfield"/>		
						</td>
						<td>
		 					<s:select key="midas.suscripcion.solicitud.solicitudPoliza.nombreAgente" 
		 							  name="solicitud.codigoAgente" id="codigoAgente" 
		 							  labelposition="top"  
		 							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 							  list="agenteList"
					                  disabled="%{#controlesAgenteDisabled}"
		 							  onchange="onChangeCodigoAgente(this.value);"
		 							  required="%{seleccion==2}"
		 							  cssClass="txtfield"/>					
						</td>																										
					</tr>
					<tr>
						
						<td>
		 					<s:select key="midas.suscripcion.solicitud.solicitudPoliza.negocio" 
		 							  name="solicitud.negocio.idToNegocio" id="negocios"  
		 							  labelposition="top" 
		 							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 							  onchange="cargarComboSimpleDWR(this.value, 'idToProducto', 'getMapNegProductoPorNegocio')" 
		 							  list="negocioList" listKey="idToNegocio" listValue="descripcionNegocio"
					                  disabled="%{#disabledConsulta}" 
		 							  required="%{seleccion==2}"
		 							  cssClass=" txtfield"/> 						
						
						</td>
						<td>
		 					<s:select key="midas.suscripcion.solicitud.solicitudPoliza.producto" 
		 							  name="negocioProducto.idToNegProducto" id="idToProducto"
		 							  labelposition="top" 
		 							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 							  list="negocioProductoList"
		 							  required="%{seleccion==2}"
					           		  disabled="%{#disabledConsulta}"
		 							  cssClass="txtfield" /> 								
						</td>
						<td>
		 					<s:select key="midas.suscripcion.solicitud.solicitudPoliza.tipoMovimiento" 
		 							  name="solicitud.claveTipoSolicitud" id="tiposSolicitud"
		 							  cssClass="txtfield jQalphaextra jQrestrict"
		 							  labelposition="top"   
		 							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 							  list="tiposSolicitud"  
		 							  required="%{seleccion==2}"
					                  disabled="%{#disabledConsulta}"
		 							  cssClass="txtfield"/>
						</td>									
					</tr>
					<tr>
						<td valign="top">
							<s:if test="%{seleccion==1}">
							  	<s:select key="midas.suscripcion.solicitud.solicitudPoliza.estatus" 
		 							id="claveEstatus" name="solicitud.claveEstatus" 
	 							    labelposition="top" 
	 							  	headerKey="" headerValue="%{getText('midas.general.seleccione')}"
	 							  	list="listaEstatus" 
	 							  	cssClass="txtfield"/> 						
							</s:if>
							<s:else>
								<s:textarea key="midas.suscripcion.solicitud.solicitudPoliza.enviarCotizacion"
									id="emailContactos" name="solicitud.emailContactos"
									cssClass="jQEmailList"
									cssErrorStyle="font-size:12px;"
									title="Correos electronicos separados por punto y coma"
									alt="Correos electronicos separados por punto y coma"
					           		disabled="%{#disabledConsulta}"
					           		required="%{seleccion==2}"
									rows="1" cols="50" cssStyle="width:245px;height:40px;" />
							</s:else>	
						</td>	
						<s:if test="%{seleccion==2 && solicitud.claveEstatus != null}">
						<td>
							  	<s:select key="midas.suscripcion.solicitud.solicitudPoliza.estatus" 
		 							id="claveEstatus" name="solicitud.claveEstatus" 
	 							    labelposition="top" 
	 							  	headerKey="" headerValue="%{getText('midas.general.seleccione')}"
	 							  	list="listaEstatus"  disabled="true"
	 							  	cssClass="txtfield"/> 
	 							 <s:hidden name="solicitud.claveEstatus" />	
							
							<s:else>
								&nbsp;				
							</s:else>
						</td>	
						</s:if>		
						<td>
							<s:if test="%{seleccion==1}">
							<sj:datepicker name="solicitud.fechaCreacion" required="#requiredField" cssStyle="width: 225px;"
							   			   buttonImage="../img/b_calendario.gif" key="midas.suscripcion.solicitud.solicitudPoliza.fecha"
							               id="fechaCreacion" maxlength="10" cssClass="txtfield"	
							               labelposition="top" 
							               size="12"
							               maxDate="today"
							               changeMonth="true"
							               changeYear="true"							   								  
							               onkeypress="return soloFecha(this, event, false);"
							               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							               onblur="esFechaValida(this);"></sj:datepicker>
							</s:if>	
						</td>
						<td>    
							<s:if test="%{seleccion==1}">           
							<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 225px;"
							    		   buttonImage="../img/b_calendario.gif"
							    		   labelposition="top" 
							    		   size="12"
							    		   maxDate="today"
							               changeMonth="true"
							               changeYear="true"
							    		   key="midas.suscripcion.solicitud.solicitudPoliza.fechaA"
							    		   id="fechaFinal" maxlength="10" cssClass="txtfield"								   								  
							    		   onkeypress="return soloFecha(this, event, false);"
							    		   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							    		   onblur="esFechaValida(this);"></sj:datepicker>	
							 </s:if>
						</td>													
					</tr>
					<s:if test="%{id != null}">
						<tr>
							<td>
								<div>
									<s:text name="midas.suscripcion.solicitud.solicitudPoliza.cartacobertura" />
								</div>
								<div id="tieneCartaCoberturaDiv">
									<s:radio name="solicitud.tieneCartaCobertura" list="#{'0':'No','1':'Si'}" onclick="showUpExcel(this.value);"/>
								</div>
							</td>
							<td>
							</td>
							<td>
							</td>
						</tr>
					</s:if>
				</table>
			</div>
			<table>	
				<tr>
					<td align="right">
						<div id="upExcel" class="btn_back w140" style="display: none; float: right;">
									<a href="javascript: void(0);"
										onclick="showWindowsByUpExcel(<s:property value='id'/>)"> <s:text
											name="midas.suscripcion.cotizacion.subeExcelDetalleCartaCobertura" /> </a>
						</div>	
					<s:if test="solicitud.codigoUsuarioModificacion.length() > 0  && solicitud.claveEstatus != 8 && solicitud.claveEstatus != 9 && solicitud.claveEstatus != 2">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);"
										onclick="$_iniciarCotizacionFormal()"> <s:text
											name="midas.suscripcion.cotizacion.cotizacionFormal" /> </a>
						</div>					
					</s:if>
						<div id="divFiltrosBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" id="mostrarFiltros"
									onclick="displayFilterSolicitud()">Ocultar Filtros</a>
							</div>
						</div>
                     	<div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="limpiarFormSolicitud();" class="icon_limpiar" >	
									<s:text name="midas.suscripcion.solicitud.solicitudPoliza.limpiar"/>	
								</a>
		                    </div>
                      	</div>			
						<s:if test="%{seleccion==1}">
	                      	<div id="divBuscarBtn" class="w150" style="float:left;">
								<div class="btn_back w140"  >
									<a href="javascript: void(0);" onclick="compareDates();" class="icon_buscar">	
										<s:text name="midas.suscripcion.solicitud.solicitudPoliza.buscar"/>	
									</a>
	                      		</div>
	                     	</div>																			
						</s:if>
						<s:elseif test="seleccion==2 && id == null">
							<div id="divEnviarBtn" class="w150" style="float:left;">
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="(validateAll(true))?enviar():false" class="icon_enviar">	
										<s:text name="midas.suscripcion.solicitud.solicitudPoliza.enviar"/>	
									</a>
	                      		</div>
	                     	</div>							
						</s:elseif>
						<s:if test="%{id!=null}">
							<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Comentarios">
							<div id="divComentariosBtn" style="float:left;" class="w150">
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick='abrirComentarios(<s:property value="id"/>);' class="icon_comentario">	
										<s:text name="midas.suscripcion.solicitud.solicitudPoliza.comentarios"/>	
									</a>
									
	                      		</div>
	                      	</div>
	                      	</m:tienePermiso>
						</s:if>
							<s:if test="%{seleccion==2}">
							 <div id="divEnviarBtn" class="w150" style="float:left;">
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="mostrarVentanaAdjuntarDocumentos(<s:property value='id'/>);" class="icon_enviar">	
										<s:text name="midas.suscripcion.solicitud.solicitudPoliza.adjuntar"/>	
									</a>
	                      		</div>
	                      		
	                     	 </div>	
	                     	</s:if>						
					</td>
				</tr>
			</table>
		</s:form>
			<div id="indicador"></div>
			<div id="gridSolicitudesPaginado" >
				<div id="solicitudesPolizaGrid" style="width:98%;height:130px"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</div>
		<br>
		<s:if test="!tipoCabina">
			<m:tienePermiso nombre="FN_M2_Emision_Solicitudes_Exportar">
			<div  style="width: 98%;text-align: center;">
				<div class="btn_back w220" style="display: inline; float: left; ">
					<a href="javascript: void(0);" onclick="descargaExcelSolicitudPoliza();"> 
					<s:text name="midas.boton.exportarExcel" /> </a>
				</div>
			</div>
			</m:tienePermiso>
		</s:if>
	</center>
</div>
<s:if test="%{esRetorno==0}">
<script type="text/javascript">
	iniciaSolicitudPoliza();
</script>
</s:if>