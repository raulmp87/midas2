<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/xml"%>
<resultados> 
	<s:iterator value="listResultUsuarios">
		<item>
		    <usuario><s:property value="nombreUsuario" /></usuario>
			<id><s:property value="id" /></id> 			
			<descripcion><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true" /></descripcion> 
		</item>
	</s:iterator>
</resultados>
