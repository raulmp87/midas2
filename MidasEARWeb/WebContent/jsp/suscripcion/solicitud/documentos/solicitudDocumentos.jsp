<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxvault.css'/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxvault.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/solicitud/solicitudPoliza.js'/>"></script>

<sj:head/>
<div id="contenedorFiltros" >
			<div id="indicador"></div>
			<div id="solicitudDocumentosGrid" style="width:98%;height:130px"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
</div>
<s:form id="solicitudDocumentosForm" >
	<s:hidden name="solicitud.idToSolicitud" id="idSolicitud" />
	<s:hidden name="tipoConsulta" id="tipoConsulta" />
	<div style="width: 410px;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="cierraVaultAdjuntarDocumentos();parent.cerrarVentanaModal('adjuntarDocumentos');"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
		<s:if test="tipoConsulta == 0">
			<div id="divGuardar" style="display: block; float: right;">
				<div class="btn_back w140" style="display: inline; float: left;">
				<a href="javascript: void(0);"
					onclick="parent.adjuntarDocumentosbySolicitud(<s:property value='id' />);">
					<s:text name="midas.suscripcion.solicitud.solicitudPoliza.adjuntar" /> </a>
				</div>
			</div>
		</s:if>
	</div>
</s:form>
<script type="text/javascript">
	iniciaSolicitudDocumentos();
</script>