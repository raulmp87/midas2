<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="solicitudDTO.idToSolicitud" type="ro" width="100" sort="int" hidden="true" ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.folio" /></column>
		<column id="controlArchivo.nombreArchivoOriginal" type="ro" width="*" sort="int" ><s:text name="midas.suscripcion.solicitud.comentarios.archivo" /></column>
		<column id="descargar" type="img" width="30" sort="na"/>
		<column id="esCartaCobertura" type="img2" width="180" sort="na"><s:text name="midas.suscripcion.solicitud.solicitudPoliza.escartacobertura" /></column>

</head>
		
	<s:iterator value="documentoDigitalSolicitudList" status="stats">
		<row id="<s:property value='%{stats.index}' />">
			<cell><s:property value="solicitudDTO.idToSolicitud" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="controlArchivo.nombreArchivoOriginal" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript: descargarDocumento(<s:property value="idToControlArchivo" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell><s:if test="esCartaCobertura == 1">/MidasWeb/img/dhtmlxgrid/radio_chk1.gif</s:if><s:elseif test="esCartaCobertura == 0">/MidasWeb/img/dhtmlxgrid/radio_chk0.gif</s:elseif>^Carta Cobertura^seleccionaCartaCobertura(this, <s:property value='idToControlArchivo' escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>	
	
</rows>

