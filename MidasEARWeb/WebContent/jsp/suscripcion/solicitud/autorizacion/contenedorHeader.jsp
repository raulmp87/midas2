<%@ taglib prefix="s" uri="/struts-tags"%>
<!--  imports Js files -->
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/solicitud/autorizacion/autorizacionSolicitud.js'/>"></script>
<script language="JavaScript"
	src='<s:url value="/js/validaciones.js"></s:url>'>
</script>
<script type="text/javascript">
	var obtenerSolicitudesAutPaginadoPath = '<s:url action="listarSolicitudesPaginado" namespace="/suscripcion/solicitud/autorizar"/>';
	var obtenerSolicitudesAutPath = '<s:url action="listarSolicitudes" namespace="/suscripcion/solicitud/autorizar"/>';
	var obtenerSolicitudesDetalleAutPath = '<s:url action="listarSolicitudesDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
	var actualizaSolicitudDetalleAutPath = '<s:url action="actualizaSolicitudesDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
	var selecionarUsuarioPath = '<s:url value="../jsp/suscripcion/solicitud/autorizacion/solicitudBusquedaUsuario.jsp"></s:url>';
	var terminarSolicitudesPath = '<s:url action="terminarSolicitudAutorizacionMasiva" namespace="/suscripcion/solicitud/autorizar"/>';
</script>
