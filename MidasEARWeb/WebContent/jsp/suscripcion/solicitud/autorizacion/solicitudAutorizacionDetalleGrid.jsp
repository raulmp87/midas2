<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	var actualizaSolicitudDetalleAutPath = '<s:url action="actualizaSolicitudesDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
	var terminarSolicitudAutPath = '<s:url action="terminarSolicitudAutorizacion" namespace="/suscripcion/solicitud/autorizar"/>';
	var verExcepcionDetallePath = '<s:url action="verExcepcionDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
	var verExcepcionSerieDetallePath = '<s:url action="verExcepcionSerieDetalle" namespace="/suscripcion/solicitud/autorizar"/>';
	var cancelarSolicitudAutPath = '<s:url action="cancelarSolicitudDesdeDetalle" namespace="/suscripcion/solicitud/autorizar"/>';

	function terminarSolicitud(idSolicitud){	
		if(confirm('\u00BFEst\u00e1 seguro que desea terminar la solicitud '+ idSolicitud +'\u003F \nLas observaciones de detalles que no tengan estatus seleccionado se perder\u00E1n')){
			//var url = terminarSolicitudAutPath + '?idSolicitud='+idSolicitud+'&nextFunction=mostrarListadoSolicitudes()';
			//parent.mainDhxWindow.window("solicitudDetalle").attachURL(url);
			document.forms[0].submit();
		}
		
	}
	
	function cancelarSolicitudDesdeDetalle(idSolicitud){
		if(confirm('\u00BFEst\u00e1 seguro que desea cancelar la solicitud '+ idSolicitud +'\u003F')){
			var url = cancelarSolicitudAutPath + '?idSolicitud='+idSolicitud+'&nextFunction=mostrarListadoSolicitudes()';
			parent.mainDhxWindow.window("solicitudDetalle").attachURL(url);			
		}		
		
	}
		
	function mostrarExcepcionDetalle(id){
		var url = verExcepcionDetallePath + "?idExcepcion=" + id;
		parent.mostrarVentanaModal("excepcionDetalle", "Excepci\u00F3n Ocurrida", 50, 50, 600, 500, url);	
	}
	function mostrarExcepcionSerieDetalle(id,idToCotizacion,numeroInciso){
		var url = verExcepcionSerieDetallePath + "?idExcepcion=" + id
				+ "&solicitudExcepcionDetalle.idInciso=" + numeroInciso
				+ "&solicitudExcepcionCot.toIdCotizacion=" + idToCotizacion; 
		parent.mostrarVentanaModal("excepcionSerieDetalle", "Excepci\u00F3n Ocurrida", 50, 50, 700, 500, url);	
	}
	function actualizaEstatus(estatus, nombre, idSolicitud, idDetalle, observacion){		
		if(confirm('\u00BFEst\u00e1 seguro que desea actualizar el estatus de este detalle\u003F \nSi desea agregar una observaci\u00F3n, llene el campo antes de intentar cambiar el estatus')){
			var url = actualizaSolicitudDetalleAutPath + "?idSolicitud="+idSolicitud+"&idDetalle="+idDetalle+"&estatus="+estatus+"&observacion="+observacion;
			parent.mainDhxWindow.window("solicitudDetalle").attachURL(url);		
		}else{
			document.forms[0].reset();			
		}		
	}
</script>
<s:form name="terminarSolicitudAutorizacion" id="terminarSolicitudAutorizacion" action="/suscripcion/solicitud/autorizar/terminarSolicitudAutorizacion.action" namespace="/suscripcion/solicitud/autorizar">
<table width="98%">
	<s:hidden name="solicitudExcepcionCot.id" id="idSolicitud" />
	<s:hidden name="idSolicitud" />
	<s:hidden name="solicitudExcepcionCot.toIdCotizacion" id="idCotizacion" />
	<s:hidden name="solicitudExcepcionCot.estatus" id="estatus" />	
	<tr>
		<td><s:text name="midas.suscripcion.solicitud.autorizacion.numSolicitud" />:</td>
		<td><s:property value="solicitudExcepcionCot.id" /></td>
		<td>&nbsp;</td>
		<td><s:text name="midas.suscripcion.solicitud.autorizacion.numCotizacion" />:</td>
		<td><s:property value="solicitudExcepcionCot.toIdCotizacion" /></td>
	</tr>
</table>
<div style="overflow: auto; width: 98%; height: 480px;">
<table id="desplegar" border="0" style="vertical-align: top;">
	<tr>
		<s:if test="solicitudExcepcionCot.estatus == 0">
		<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Autorizar_Excepcion">
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.autorizar" /></th>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Rechazar_Excepcion">
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.rechazar" /></th>
		</m:tienePermiso>
		<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Cancelar_Excepcion">
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.cancelar" /></th>
		</m:tienePermiso>
		</s:if>
		<s:else>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.estatus" /></th>
		</s:else>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.seccion" /></th>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>	
		<th>#<s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.cobertura" /></th>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.datosIncumplidos" /></th>
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.observaciones" /></th>										
		<th><s:text name="midas.suscripcion.solicitud.autorizacion.nombreExcepcion" /></th>					
	</tr>
	<s:iterator value="solicitudesDetalle" status="stat">	
	<tr class="bg_t2">
		<s:if test="solicitudExcepcionCot.estatus == 0">
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Autorizar_Excepcion">
			<td>
				<s:radio list="#{'1':''}" name="solicitudesDetalle[%{#stat.index}].estatus" cssClass="cajaTexto"></s:radio>						
			</td>
			</m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Rechazar_Excepcion">
			<td><s:radio list="#{'2':''}" name="solicitudesDetalle[%{#stat.index}].estatus" cssClass="cajaTexto"></s:radio>			
			</td>
			</m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Cancelar_Excepcion">
			<td><s:radio list="#{'3':''}" name="solicitudesDetalle[%{#stat.index}].estatus" cssClass="cajaTexto"></s:radio>
			</td>
			</m:tienePermiso>
		</s:if>
		<s:else>
			<td>
				<s:if test="estatus==1">AUTORIZADA</s:if>
				<s:elseif test="estatus==2">RECHAZADA</s:elseif>
				<s:else>CANCELADA</s:else>
			</td>
		</s:else>
		
		<td>
			<s:hidden name="solicitudesDetalle[%{#stat.index}].id"  value="%{id}"/>		
			<s:if test="descripcionSeccion != null">	
				<s:property value="descripcionSeccion"/>
			</s:if>
			<s:else>
				NA
			</s:else>
		</td>
		
		<td>
			<s:if test="descripcionInciso != null">	
				<s:property value="descripcionInciso"/>
			</s:if>
			<s:else>
				NA
			</s:else>
		</td>
		
		
		<td>
			<s:if test="numeroSecuencia != null">	
				<s:property value="numeroSecuencia"/>
			</s:if>
			<s:else>
				NA
			</s:else>
		</td>	
		
		<td>
			<s:if test="descripcionCobertura != null">	
				<s:property value="descripcionCobertura"/>
			</s:if>
			<s:else>
				NA
			</s:else>
		</td>
		<td>				
			<ul>
				<s:iterator value="listaProvocadorExcepcion" var="renglon" status="statusLista">
					<li>								
						<s:property value="renglon"/>
					</li>	
				</s:iterator>
			</ul>				
		</td>
		<td>
			<s:if test="solicitudExcepcionCot.estatus == 0">
				<s:textfield name="solicitudesDetalle[%{#stat.index}].observacion" id="observacion_<s:property value='id'/>" class="cajaTexto"  />
			</s:if>
			<s:else>
				<s:property value="observacion"/>
			</s:else>
		</td>		
		<td>
		 <s:if test="idExcepcion >= 100">
					<div align='center'>
						<a
							onclick="javascript: mostrarExcepcionDetalle(<s:property value='idExcepcion' />);"
							href='javascript: void(0);'> <img border='0'
							src='/MidasWeb/img/icons/ico_verdetalle.gif'
							title='Ver Excepcion' /> </a>
					</div>
		</s:if> 
		<s:elseif test="idExcepcion == 4">
					<div align='center'>
						<a
							onclick="javascript: mostrarExcepcionSerieDetalle(<s:property value='idExcepcion' />,<s:property value='solicitudExcepcionCot.toIdCotizacion'/>,<s:property value='idInciso'/>);"
							href='javascript: void(0);'> <img border='0'
							src='/MidasWeb/img/icons/ico_verdetalle.gif'
							title='Ver Excepcion' /> </a>
					</div>
		</s:elseif>
		 <s:else>
				NA
		</s:else>
		</td>
	</tr>
	</s:iterator>	
</table>
</div>
<br/>
<div class="alinearBotonALaDerecha">

			<div id="divSalirBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="parent.mostrarListadoSolicitudes();parent.cerrarVentanaModal('solicitudDetalle');"> <s:text
							name="midas.boton.salir" /> </a>
				</div>
			</div>		
		
		<s:if test="solicitudExcepcionCot.estatus == 0">
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Cancelar_Solicitud">
			<div id="divCancelarBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="cancelarSolicitudDesdeDetalle(<s:property value='solicitudExcepcionCot.id' />);"> <s:text
							name="midas.cotizacion.cancelar" /> </a>
				</div>
			</div>
			</m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Terminar_Solicitud">
			<div id="divTerminarBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="terminarSolicitud(<s:property value='solicitudExcepcionCot.id' />);"> <s:text
							name="midas.boton.terminar" /> </a>
				</div>
			</div>
			</m:tienePermiso>
		</s:if>					
</div>
</s:form>