<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css" >
#columns {
	width: 100%;
}

#columns .column {
	position: relative;
	padding: 1%;
}

#columns .left {
	float: left;
	width: 60%;
}

.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
}

#columns .right {
	float: right;
}
</style>
<s:form name="verExcepcion" >
<s:hidden name="excepcionId" id="excepcionId"/>
	<div id="columns" style="width: 95%">			
			<table id="agregar">
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.numero"/></th>
					<td><s:property value="excepcion.numExcepcion"/></td>
					<td></td>
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.descripcion"/></th>
					<td colspan="2"><s:property value="excepcion.descripcion" /></td>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.tipoUsuario"/></th>
					<td><s:property value="excepcion.usuario"/></td>	
					<td>
					</td>	
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.agente"/></th>
					<td><s:property value="excepcion.agente"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.negocio"/></th>
					<td><s:property value="excepcion.negocio"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.producto"/></th>
					<td><s:property value="excepcion.producto"/></td>
					<td>
						
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.tipoPoliza"/></th>
					<td><s:property value="excepcion.tipoPoliza"/></td>
					<td>
						
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.lineaNegocio"/></th>
					<td><s:property value="excepcion.lineaNegocio"/></td>
					<td>
						
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.paquete"/></th>
					<td><s:property value="excepcion.paquete"/></td>
					<td>
						
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.cobertura"/></th>
					<td><s:property value="excepcion.cobertura"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.deducible"/></th>
					<td><s:property value="excepcion.deducible"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.sumaasegurada"/></th>
					<td><s:property value="excepcion.sumaAsegurada"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.zona"/></th>
					<td><s:property value="excepcion.zona"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.vehiculo"/></th>
					<td><s:property value="excepcion.amisDescripcionVehiculo"/></td>
					<td>
					</td>		
				</tr>
				<tr>
					<th><s:text name="midas.excepcion.suscripcion.auto.rangoModelo"/></th>
					<td><s:property value="excepcion.rangoModelos"/></td>
					<td>
					</td>		
				</tr>
			</table>
		</div>		
	<br/>
	<div class="alinearBotonALaDerecha">
		<div id="divSalirBtn" class="w150"
			style="display: inline; float: right;">
			<div class="btn_back w140">
				<a href="javascript: void(0);"
					onclick="parent.cerrarVentanaModal('excepcionDetalle')"> <s:text
						name="midas.boton.salir" /> </a>
			</div>
		</div>	
	</div>
</s:form>