<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>			
		</beforeInit>
		<column id="solicitudExcepcionCot.id" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
		<column id="solicitudExcepcionCot.id" type="ro" width="120" sort="int" ><s:text name="midas.suscripcion.solicitud.autorizacion.numSolicitud" /></column>
		<column id="solicitudExcepcionCot.toIdCotizacion"  type="ro" width="*" sort="int" ><s:text name="midas.suscripcion.solicitud.autorizacion.numCotizacion" /></column>
		<column id="solicitudExcepcionCot.usuarioSolicitante" type="ro" width="0" sort="int" hidden="true"  ><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioSolicitante" /></column>
		<column id="solicitudExcepcionCot.nombreSolicitante" type="ro" width="*" sort="str" ><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioSolicitante" /></column>
		<column id="solicitudExcepcionCot.usuarioAutorizador" type="ro" width="0" sort="int" hidden="true" ><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioAutorizador" /></column>
		<column id="solicitudExcepcionCot.nombreAutorizador" type="ro" width="*" sort="str" ><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioAutorizador" /></column>
		<column id="solicitudExcepcionCot.fechaSolicitud" type="ro" width="120" sort="date_custom" ><s:text name="midas.suscripcion.solicitud.autorizacion.fechaSolicitud" /></column>
		<column id="solicitudExcepcionCot.estatus" type="ro" width="*" sort="str" ><s:text name="midas.suscripcion.solicitud.autorizacion.estatus" /></column>
		<column id="acciones" type="img" width="70" align="center"><s:text name="midas.general.acciones" /></column>	
		<column id="acciones" type="img" width="30" align="center"></column>	
	</head>
		
	<s:iterator value="solicitudes">
		<row id="<s:property value="id" escapeHtml="false"/>">
			<cell>0</cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="toIdCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioSolicitante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreSolicitante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioAutorizador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAutorizador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaSolicitud" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus==0">
				<cell>EN PROCESO</cell>
			</s:if>
			<s:elseif test="estatus==1">
				<cell>TERMINADA</cell> 
			</s:elseif>
			<s:elseif test="estatus==3">
				<cell>CANCELADA</cell> 
			</s:elseif>	
			<s:else>
				<cell></cell>
			</s:else>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Editar">
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: mostrarSolicitudAutDetalle(<s:property value="id"/>)^_self</cell>	
			</m:tienePermiso>
			<s:if test="estatus!=1">
				<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Cancelar">
				<cell>/MidasWeb/img/icons/ico_eliminar.gif^Cancelar^javascript: cancelarSolicitud(<s:property value="id"/>)^_self</cell> 
				</m:tienePermiso>
			</s:if>	
		</row>
	</s:iterator>	
	
</rows>
