<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<table id="desplegar" border="0" style="vertical-align: top;">
	<tr>
		<th><s:text
				name="midas.suscripcion.solicitud.autorizacion.numCotizacion" />
		</th>
		<th><s:text
				name="midas.suscripcion.solicitud.autorizacion.seccion" /></th>
		<th><s:text name="midas.poliza.numeroSerie"></s:text></th>
		<th>#<s:text
				name="midas.suscripcion.solicitud.autorizacion.inciso" /></th>
		<th><s:text
				name="midas.suscripcion.solicitud.autorizacion.fechaInicioVigencia"></s:text>
		</th>
		<th><s:text
				name="midas.suscripcion.solicitud.autorizacion.fechaFinVigencia"></s:text>
		</th>
	</tr>
	<s:iterator value="incisoNumeroSerieDTOs">
		<tr class="bg_t2">
			<td><s:property value="idCotizacion" />
			</td>
			<td><s:property value="idSeccion" />
			</td>
			<td><s:property value="numeroSerie" />
			</td>
			<td><s:property value="numeroInciso" />
			</td>
			<td><s:property value="fechaIniVigencia" />
			</td>
			<td><s:property value="fechaFinVigencia" />
			</td>
		</tr>
	</s:iterator>
</table>


