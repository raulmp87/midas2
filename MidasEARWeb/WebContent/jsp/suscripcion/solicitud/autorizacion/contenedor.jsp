<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include
	value="/jsp/suscripcion/solicitud/autorizacion/contenedorHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<style type="text/css">
.contenido {
	height: 200px;
}

label {
	font-weight: bold;
}

table {
	font-size: 10px;
}

.w150 {
	margin-right: 50px;
	margin-bottom: 10px;
}
</style>
<script type="text/javascript">
function compareDates(){
	blockPage();
	fecha1=dwr.util.getValue("fechaSolicitud");
	fecha2=dwr.util.getValue("fechaSolicitudHasta");
	numSolicitud=dwr.util.getValue("solicitudExcepcionCot.id");
	numCotizacion=dwr.util.getValue("solicitudExcepcionCot.toIdCotizacion");
    if (compare_dates(fecha1, fecha2)){  
    	mostrarMensajeInformativo("La fecha de Solicitud no debe ser mayor a la fecha hasta. Verificar.", 10, null); 
    } else if(numSolicitud.trim() == '' && numCotizacion.trim() == '' && compare_dates_period(fecha1, fecha2)) {
    	mostrarMensajeInformativo("El peri�do entre la fecha de Solicitud inicial y la fecha de solicitud final no debe ser mayor a 1 a�o. Verificar.", 10, null);
    } else{
    	// Definir fechas por default cuando no se capture filtro restrictivo
    	if(numSolicitud.trim() == '' && numCotizacion.trim() == '')
    		definirFechasDefault("fechaSolicitud", "fechaSolicitudHasta");
    		
        mostrarListadoSolicitudesPaginado(1, true);
    }
    unblockPage();
}

var usuarioControlDeshabilitado = <s:property value="usuarioControlDeshabilitado"/>;
</script>
<div class="titulo" style="width: 98%;">
		Solicitudes de Autorizaci&oacute;n
</div>
<div id="content_form"  style="width: 98%;text-align: center;">
	<s:form action="mostrarContenedor" id="solicitudForm">
		<s:hidden name="solicitudExcepcionCot.cveNegocio" value="A" />
		<div id="form">
			<table id="agregar" >
				<tr>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.numSolicitud" />:</td>
					<td><s:textfield name="solicitudExcepcionCot.id" id="solicitudExcepcionCot.id" cssClass="txtfield"
							onkeypress="return soloNumeros(this, event, false);"></s:textfield>
					</td>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.fechaSolicitud" />:</td>
					<td>
						<sj:datepicker name="solicitudExcepcionCot.fechaSolicitud" required="#requiredField" 
				 			   buttonImage="../img/b_calendario.gif" 
				               id="fechaSolicitud" maxlength="10"
				               cssClass="txtfield" cssStyle="height:16px"
				               labelposition="%{getText('label.position')}" 
				               size="12"
					           changeMonth="true" changeYear="true"
					           maxDate="today"
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur="esFechaValida(this);"></sj:datepicker>
				       </td><td>Fecha de Solicitud a:</td><td>
				         <sj:datepicker name="solicitudExcepcionCot.fechaHasta" required="#requiredField" 
				 			   buttonImage="../img/b_calendario.gif" 
				               id="fechaSolicitudHasta" maxlength="10" 
				               cssClass="txtfield" cssStyle="height:16px"	
				               labelposition="%{getText('label.position')}" 
				               size="12" 
				               changeMonth="true" changeYear="true"
				               maxDate="today"							   								  
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur="esFechaValida(this);"></sj:datepicker>
					</td>
				</tr>
				<tr>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.numCotizacion" /></td>
					<td>
						<s:textfield name="solicitudExcepcionCot.toIdCotizacion" id="toIdCotizacion" 
						cssClass="txtfield" onkeypress="return soloNumeros(this, event, false);"/>
					</td>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioSolicitante" /></td>
					<td colspan="3">
						<s:hidden id="usuarioSolicitante" name="solicitudExcepcionCot.usuarioSolicitante"></s:hidden>
						<s:if test="!usuarioControlDeshabilitado">
	 						<s:textfield name="solicitudExcepcionCot.nombreSolicitante" id="nombreSolicitante" 
								onclick="seleccionarUsuario(1)" cssClass="txtfield w150"/>
				 		</s:if>
				 		<s:else>
							<s:textfield name="solicitudExcepcionCot.nombreSolicitante" id="nombreSolicitante" 
								readonly="true" cssClass="txtfield w150"/>
				 		</s:else>
					</td>
				</tr>
				<tr>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.estatus" /></td>
					<td>
						  	<s:select id="estatus" name="solicitudExcepcionCot.estatus" 
	 							      labelposition="%{getText('label.position')}" 
	 							      cssStyle="max-width: 200px;"	 							  	 
	 							  	  list="#{'0':'EN PROCESO','1':'TERMINADA','3':'CANCELADA','-1':'TODOS'}" 
	 							  	  cssClass="txtfield"/> 
					</td>
					<td><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioAutorizador" /></td>
					<td colspan="3">
						<s:hidden id="usuarioAutorizador" name="solicitudExcepcionCot.usuarioAutorizador"></s:hidden>
						<s:textfield name="solicitudExcepcionCot.nombreAutorizador" id="nombreAutorizador"
						onclick="seleccionarUsuario(2)" 
						cssClass="txtfield w150"/>
					</td>
				</tr>
			</table>
			<div class="row">
				<div class="c2 s7">
					<div id="divEnviarBtn1" class="w130"
						style="display: inline; float: none;">
					<div class="btn_back w130">
						<a href="javascript: void(0);" class="icon_limpiar"
							onclick="limpiarFiltrosAutorizacion();"> <s:text
								name="midas.suscripcion.cotizacion.limpiar" /> </a>
					</div>
					</div>
				</div>
				<div class="c1">
						<div id="divEnviarBtn1" class="w100"
						style="display: inline; float: none;">
						<div class="btn_back w100">
							<a href="javascript: void(0);"
								onclick="compareDates();"> <s:text
									name="midas.suscripcion.cotizacion.buscar" /> </a>
						</div>
					</div>
				</div>
			</div>


		</div>
	</s:form>
</div>
<div class="clear"></div>
<div id="indicador"></div>
<div id="gridListadoSolicitudesPaginado" > 
<div id="listadoSolicitudes" style="width: 97%; height: 200px;"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
</div>

<div class="alinearBotonALaDerecha">

			<div id="divSalirBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="terminarSolicitudesMasiva(3);"> <s:text
							name="midas.suscripcion.solicitud.autorizacion.cancelar" /> </a>
				</div>
			</div>		
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Cancelar_Solicitud">
			<div id="divCancelarBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="terminarSolicitudesMasiva(2);"> <s:text
							name="midas.suscripcion.solicitud.autorizacion.rechazar" /> </a>
				</div>
			</div>
			</m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Solicitud_Autorizacion_Terminar_Solicitud">
			<div id="divTerminarBtn" class="w150"
				style="display: inline; float: right;">
				<div class="btn_back w140">
					<a href="javascript: void(0);"
						onclick="terminarSolicitudesMasiva(1);"> <s:text
							name="midas.suscripcion.solicitud.autorizacion.autorizar" /> </a>
				</div>
			</div>
			</m:tienePermiso>					
</div>
			
<script type="text/javascript">
<!-- Ejecuta el metodo que llena el Grid -->
	//mostrarListadoSolicitudes();
	mostrarListadoSolicitudesPaginado(1, true);
</script>
