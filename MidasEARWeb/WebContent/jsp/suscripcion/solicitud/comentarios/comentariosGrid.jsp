<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>				
		</beforeInit>		
		
		<column id="comentario.id" type="ro" width="*" sort="str" hidden="true">Comentario</column>
		<column id="comentario.solicitudDTO.idToSolicitud" type="ro" width="*" sort="str" hidden="true">Comentario</column>
		<column id="comentario.documentoDigitalSolicitudDTO.idToControlArchivo" type="ro" width="*" sort="str" hidden="true">idToControlArchivo</column>
		<column id="comentario.valor" type="ro" width="500" sort="str" >Comentario</column> 		
		<column id="comentario.codigoUsuarioCreacion" type="ro" width="165" sort="str">Usuario</column>
		<column id="comentario.fechaCreacion" type="ro" width="120" sort="date"><s:text name="midas.suscripcion.solicitud.comentarios.fechacreacion" /></column>				
 		<column id="accionAsociarUso" type="img" width="70" sort="na" align="center">Acciones</column>
		<s:if test="documentoDigitalSolicitudDTO.idToControlArchivo != null">
		<column id="descargar" type="img" width="30" sort="na"/>
		</s:if>
		<s:if test="documentoDigitalSolicitudDTO.idToControlArchivo == null">
		<column id="borrar" type="img" width="60" sort="na"/>
		</s:if>
		<column id="borrar" type="img" width="60" sort="na"/>
	</head>

	<% int a=0;%>
	<s:iterator value="comentariosList">
		<% a+=1; %>
		<row id="<%=a%>">			
			<cell><s:property value="id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="solicitudDTO.idToSolicitud" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="documentoDigitalSolicitudDTO.idToControlArchivo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="valor" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:date name="fechaCreacion" format="dd/MM/yyyy hh:mm" /></cell>						
			<s:if test="documentoDigitalSolicitudDTO.idToControlArchivo != null">			
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript: descargarDocumentoComentarios(<s:property value="documentoDigitalSolicitudDTO.idToControlArchivo" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			</s:if>
			<s:if test="documentoDigitalSolicitudDTO == null">
             <cell>/MidasWeb/img/icons/ico_agregar.gif^Subir^javascript: adjuntarDocumentos(<s:property value="id" escapeHtml="false" escapeXml="true"/>,<s:property value="solicitudDTO.idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
		  	</s:if>
           <cell>/MidasWeb/img/delete14.gif^Eliminar^javascript: TipoAccionDTO.getVer(eliminarComentarios)^_self</cell>						
		</row>		
	</s:iterator>	
</rows>

