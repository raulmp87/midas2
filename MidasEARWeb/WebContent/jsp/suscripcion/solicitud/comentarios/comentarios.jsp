<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<style type="text/css">
	.errors {
			color: red;
		}
</style>
<script type="text/javascript" >
	
	function validaGuardar(){
		var valor = jQuery("#valor").val();
		if(valor != null && valor != undefined){
			valor =  valor.replace(/(\r\n|\n|\r)/gm," ");
			valor = valor.replace(/\s+/g," ");
			jQuery("#valor").val(valor);
		}
		if(jQuery("#valor").val().length >= 449){
			mostrarMensajeInformativo(jQuery('#valorcC').text(), "10", null,null);
			return false;
		} else {
			return validateAll(true,'save');
		}
	}
</script>
<s:form action="guardarComentarios" id="comentariosForm">
<s:hidden name="claveNegocio" id="claveNegocio" />
<s:hidden name="tipoComentario"  id="tipoComentario"/>
<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
<s:hidden name="solicitudDTO.idToSolicitud" id="idToSolicitud"/>
<s:hidden name="seleccion"/>
<s:include value="/jsp/suscripcion/solicitud/comentarios/comentariosHeader.jsp"></s:include>

<div id="detalle">
	<table id="filtros" width="98%">
		<tr>
			<td class="titulo" >
				 <s:if test="tipoComentario==1  || tipoComentario==2">
				 	<s:text name="midas.suscripcion.solicitud.comentarios.comentariosCot" /> <s:property value="cotizacion.numeroCotizacion"/>
				 </s:if>
				 <s:else>
			     	<s:text name="midas.suscripcion.solicitud.comentarios.comentarios" /> <s:property value="solicitudDTO.numeroSolicitud"/>
			     </s:else>
			</td>
		</tr>
	</table>
	<table id="agregar" width="98%" style="padding: 10 10 10 0px; margin: 10 10 10 0px;"  width="98%">		
		<tr>
			<td>
				<s:textarea key="midas.suscripcion.solicitud.comentarios.comentario" id="valor" name="comentario.valor" cssClass="textarea jQrequired" title="count[449]" labelposition="top" cssStyle="width:800px;height:50px;"/>
			</td>
		</tr>		
		<tr>					
		 	<td align="right">
				<div class="w150" style="display: block; float:right;">
					<div class="btn_back w140"  >
						<a href="javascript: void(0);" onclick="(validaGuardar()) ? guardarComentario() : false" class="icon_guardar">	
							<s:text name="midas.boton.guardar"/>	
						</a>
                    </div>
                </div>
                <div class="w150" style="display: block; float:right;">
					<div class="btn_back w140"  >
						<a href="javascript: void(0);" onclick="cancelarComentario();" class="">	
							<s:text name="midas.boton.cancelar"/>	
						</a>
                    </div>
                </div>	
                <div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" class="icon_limpiar"
										onclick="$_cleanForm('comentariosForm');"> <s:text
											name="midas.suscripcion.cotizacion.limpiar" /> </a>
				</div>				
			</td>			
		</tr>	
	</table>
	<div id="comentariosGrid" class="dataGridConfigurationClass" style="width:98%;height:200px"></div>
	<label class="label">* Para ver un comentario completo, posiciona el cursor sobre el comentario requerido.</label>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<div class="w150" style="display: block; float:right;">
		<div class="btn_back w140"  >
			<a href="javascript: void(0);" onclick="salirComentarios();" class="icon_anterior">	
				<s:text name="midas.boton.regresar"/>	
			</a>
        </div>
    </div>	
</div>
</s:form>
<script type="text/javascript">
	initGridsComentarios();
</script>