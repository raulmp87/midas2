<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/jQuery/plugins/jqueryCharCounterPlugin.js"/>"></script>
<script type="text/javascript">
    var obtenerComentariosPath = '<s:url action="obtenerComentariosDisponibles" namespace="/suscripcion/solicitud/comentarios"/>';
    var guardarComentariosPath = '<s:url action="guardarComentarios" namespace="/suscripcion/solicitud/comentarios"/>';
</script>