<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/solicitud/solicitudPoliza.js'/>"></script>
<script src="<s:url value='/js/midas2/dwr/agenteUtil.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript">
	var buscarSolicitudPath = '<s:url action="buscarSolicitud" namespace="/suscripcion/solicitud"/>';
	var busquedaRapidaPath = '<s:url action="busquedaRapida" namespace="/suscripcion/solicitud"/>';
	var opcionCapturaPath = '<s:url action="mostrar" namespace="/suscripcion/solicitud"/>';
	var solicitudesPaginadasPath = '<s:url action="busquedaPaginada" namespace="/suscripcion/solicitud"/>';
	var solicitudesPaginadasRapidaPath = '<s:url action="busquedaRapidaPaginada" namespace="/suscripcion/solicitud"/>';
</script>