<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script src="${pageContext.request.contextPath}/struts/utils.js"
	type="text/javascript"></script>
<s:form  id="paginadoGridForm">
<s:hidden name="totalCount" />
<s:hidden name="posStart" />
<s:hidden name="posFinish" />
<s:hidden name="funcionPaginar" />
<s:hidden name="divGridPaginar" />
<s:hidden name="paginaAnt" />
<s:hidden name="paginaSig" />
</s:form>
<s:hidden name="posActual" />
<s:hidden name="elementosSeleccionados" />
<div id="<s:property value="divGridPaginar" />" style="width: 98%; height: 130px"></div>
<div id="pagingArea">
	<s:if test="totalCount > 0">
		<div style="width: 98%; clear: both;">			
			<div class="dhx_pbox_light"><span>&nbsp;</span></div>
			<div class="dhx_pline_light" style="width: 170px;">
			
			<div style="clear: both;">
			<s:if test="paginaAnt > 0">
			<div class="dhx_page_light">
				<div>
					<a title="Ir a la página <s:property  value="paginaAnt"/>"
						onclick="<s:property value="funcionPaginar" />(<s:property value="paginaAnt"/>)"
						href="javascript: void(0);"> ← </a>
				</div>
			</div>
			</s:if>
			<s:iterator value="pages" status="stats">
				<s:set var="pagina">
					<s:property />
				</s:set>

				<s:if test="%{#pagina == posActual}">
						<div class="dhx_page_light dhx_page_active_light">
							<div class="dhx_page_active_light" style="width: 15px;"><s:property /></div>
						</div>					
				</s:if>
				<s:else>
					<div class="dhx_page_light">
						<div style="width: 15px;">
							<a title="Ir a la página <s:property />"
								onclick="<s:property value="funcionPaginar" />(<s:property />)"
								href="javascript: void(0);"> <s:property /> </a>
						</div>
					</div>
				</s:else>
			</s:iterator>
			</div>
			<s:if test="paginaSig > 0">
			<div class="dhx_page_light">
				<div>
					<a title="Ir a la página <s:property  value="paginaSig"/>"
						onclick="<s:property value="funcionPaginar" />(<s:property value="paginaSig"/>)"
						href="javascript: void(0);"> → </a>
				</div>
			</div>
			</s:if>
			<span>&nbsp;</span>
			</div>
			<div class="dhx_pager_info_light">
				Registros del <s:property value="%{posStart + 1}" /> al <s:property value="posFinish" /> de <s:property value="totalCount" />
			</div>
			<span>&nbsp;</span>
		</div>
	</s:if>
	<s:else>
		<div style="width: 98%; clear: both;">			
			<div class="dhx_pbox_light"><span>&nbsp;</span></div>
			<div class="dhx_pager_info_light">
				No se encontraron registros
			</div>
			<span>&nbsp;</span>
		</div>
	</s:else>
</div>


