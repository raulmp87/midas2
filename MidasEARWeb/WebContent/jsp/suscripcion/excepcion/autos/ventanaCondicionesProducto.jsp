<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
	margin-top: -12;
}
#footer {
	float: right;
}
</style>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<script type="text/javascript">
function cargarComboNegProductos(negocioId){
	parent.removeAllOptionsAndSetHeader(document.getElementById('productos'), parent.headerValue, parent.headerDescription);
	parent.listadoService.getMapNegProductoPorNegocio(negocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('productos'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
}

function cargarComboNegProductosAll(){	
	parent.removeAllOptionsAndSetHeader(document.getElementById('productos'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('tipoPolizas'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('lineasNegocio'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('paquetes'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('coberturas'), parent.headerValue, parent.headerDescription);
	reiniciarCobertura();
	var negocioId = jQuery('#negocios').attr('value');
	parent.listadoService.getMapNegProductoPorNegocio(negocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('productos'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
	var productoId = jQuery('#productos').attr('value');
	parent.listadoService.getMapNegTipoPolizaPorNegProducto(productoId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('tipoPolizas'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
	var tipoPolizaId = jQuery('#tipoPolizas').attr('value');
	parent.listadoService.getMapNegSeccionPorNegTipoPoliza(tipoPolizaId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('lineasNegocio'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
	var lineaNegocioId = jQuery('#lineasNegocio').attr('value');
	parent.listadoService.getMapNegPaqueteSeccionPorNegSeccion(lineaNegocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('paquetes'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
	parent.listadoService.getMapCoberturaPorNegocioSeccion(lineaNegocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('coberturas'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
}

function cargarComboTipoPolizas(productoId){
	parent.removeAllOptionsAndSetHeader(document.getElementById('tipoPolizas'), parent.headerValue, parent.headerDescription);
	reiniciarCobertura();
		parent.listadoService.getMapNegTipoPolizaPorNegProducto(productoId, function(data){
			parent.addOptionsHeaderAndSelect(document.getElementById('tipoPolizas'), data, '', '', 'ESTE VALOR NO IMPORTA');
		});
}
function cargarComboLineasNegocio(tipoPolizaId){
	parent.removeAllOptionsAndSetHeader(document.getElementById('lineasNegocio'), parent.headerValue, parent.headerDescription);
	reiniciarCobertura();
	if(!tipoPolizaId || tipoPolizaId <= 0 || tipoPolizaId == undefined){
		tipoPolizaId = jQuery('#tipoPolizas').attr('value');
	}
	parent.listadoService.getMapNegSeccionPorNegTipoPoliza(tipoPolizaId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('lineasNegocio'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
}

function cargarComboNegPaquete(lineaNegocioId){
	parent.removeAllOptionsAndSetHeader(document.getElementById('paquetes'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('coberturas'), parent.headerValue, parent.headerDescription);
	reiniciarCobertura();
	
	lineaNegocioId = document.getElementById('lineasNegocio').value;
	parent.listadoService.getMapNegPaqueteSeccionPorNegSeccion(lineaNegocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('paquetes'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
	parent.listadoService.getMapCoberturaPorNegocioSeccion(lineaNegocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('coberturas'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});
}
function cargarComboNegCobertura(paqueteId){
	parent.removeAllOptionsAndSetHeader(document.getElementById('paquetes'), parent.headerValue, parent.headerDescription);
	parent.removeAllOptionsAndSetHeader(document.getElementById('coberturas'), parent.headerValue, parent.headerDescription);
	reiniciarCobertura();
	lineaNegocioId = document.getElementById('lineasNegocio').value;

	parent.listadoService.getMapCoberturaPorNegPaqueteSeccion(paqueteId, function(data){
	parent.addOptionsHeaderAndSelect(document.getElementById('coberturas'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});				
	parent.listadoService.getMapCoberturaPorNegocioSeccion(lineaNegocioId, function(data){
		parent.addOptionsHeaderAndSelect(document.getElementById('coberturas'), data, '', '', 'ESTE VALOR NO IMPORTA');
	});		
}
// Rango Deducibles
maxSuma = 0;
minSuma = 0;

function guardar(){
	if(validateGuardar()){
		if(confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci�n\u003F')){
			document.forms[0].submit();
		}	
	}
}

function validateGuardar(){
	var min = jQuery('#minDeducible').val();
	var max = jQuery('#maxDeducible').val();

	if(!validaCheckBox('checkRango')){
		jQuery('#required').hide();
		return true;
	}
	if(!jQuery('#detalleCobertura').is(':visible')){
		return true;
	}
	if(jQuery('#minDeducible').val() == '' || jQuery('#maxDeducible').val() == ''){
		jQuery('#required').text('Ambos rangos son requedidos');
		jQuery('#required').show();
		return false;
	}
	if(parseInt(minSuma) != 0){
	if(parseInt(min) < parseInt(minSuma) || parseInt(min) > parseInt(maxSuma) || parseInt(min) > parseInt(max) ){
			jQuery('#required').text('Por favor verifica que los valores a detectar esten dentro del rango permitido');
			jQuery('#required').show();
		return false;
		}
	}else if(parseInt(min) < 0){
		jQuery('#required').text('Por favor verifica que los valores a detectar esten dentro del rango permitido');
		jQuery('#required').show();
		return false;
	}
	if(parseInt(max) > parseInt(maxSuma) || parseInt(max) < parseInt(minSuma) || parseInt(max) < parseInt(min)){
		jQuery('#required').text('Por favor verifica que los valores a detectar esten dentro del rango permitido');
		jQuery('#required').show();
		return false;
	}
	else{
		jQuery('#required').hide();
		return true;
	}
}

function setMaxSuma(suma){
	//maxSuma = suma;
	maxSuma  = 1000;
}
function setMinSuma(suma){
	//minSuma = suma;
	minSuma = 0;
}

function setValue(id, value){
	document.getElementById(id).innerHTML = value;
}

function desplegarCobertura(coberturaDTO){
	setValue("cobertura.descripcion", coberturaDTO.descripcion);			
	if(coberturaDTO.claveTipoSumaAsegurada == 1){
		jQuery('#sumaAseguradaDescripcion').show();
		jQuery('#sumaAseguradaValor').show();
		if(coberturaDTO.minSumaAsegurada){
			setValue("cobertura.minsumaasegurada", coberturaDTO.minSumaAsegurada);
			
		}else{
			setValue("cobertura.minsumaasegurada", 0);
		}
		if(coberturaDTO.maxSumaAsegurada){
			setValue("cobertura.maxsumaasegurada", coberturaDTO.minSumaAsegurada);
			
		}else{
			setValue("cobertura.maxsumaasegurada", 0);
		}				
	}else{
		jQuery('#sumaAseguradaDescripcion').hide();
		jQuery('#sumaAseguradaValor').hide();
	}
	setValue("cobertura.mindeducible", coberturaDTO.valorMinimoDeducible);
	setValue("cobertura.maxdeducible", coberturaDTO.valorMaximoDeducible);
	setMaxSuma(coberturaDTO.valorMaximoDeducible);	
	setMinSuma(coberturaDTO.valorMinimoDeducible);
}

function reiniciarCobertura(){
	limpiarElementos();
	obtenerCobertura(jQuery('#coberturas').val());

}

function limpiarElementos(){
	jQuery('#tipoValorSumaAsegurada').val('0');
	jQuery('#minDeducible').val('');
	jQuery('#maxDeducible').val('');
	jQuery('#sumaAsegurada').val('');
}

function obtenerCobertura(id){	
	if(id != ''){
		jQuery('#detalleCobertura').show();
		parent.transporteService.obtenerCobertura(id, function(coberturaDTO){
			desplegarCobertura(coberturaDTO);
		});    
	}else{
		jQuery('#detalleCobertura').hide();	
	}
}

jQuery(document).ready(function(){
	obtenerCobertura(jQuery('#coberturas').val());
	// da formato a la suma asegurada
	jQuery('#sumaAsegurada').attr('value',number_format(jQuery('#sumaAsegurada').val(),2,'.',',',' ')); 
	cargarComboNegProductosAll();
});


function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    if(number == null || number == ''){
    	return '';
    }
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function checkRangoBox(id){
	if(!validaCheckBox(id)){
		jQuery('#maxDeducible').val('');
		jQuery('#minDeducible').val('');
		jQuery('#maxDeducible').attr('disabled','disabled');
		jQuery('#minDeducible').attr('disabled','disabled');
	}else{
		jQuery('#maxDeducible').attr('disabled','');
		jQuery('#minDeducible').attr('disabled','');
	}
}

function checkValorBox(id){
	if (!validaCheckBox(id)) {
			jQuery('#sumaAsegurada').val('');
			jQuery('#sumaAsegurada').attr('disabled', 'disabled');
			jQuery('#tipoValorSumaAsegurada').attr('disabled', 'disabled');
		} else {
			jQuery('#sumaAsegurada').attr('disabled', '');
			jQuery('#tipoValorSumaAsegurada').attr('disabled', '');
		}
}
	// Revisa el estado del check box

	function validaCheckBox(idCheck) {
		if (!jQuery("#" + idCheck).is(':checked')) {
			return true;
		} else {
			return false;
		}
	}
</script>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			parent.recargarExcepcion(jQuery('#excepcionId').val(),jQuery('#mensaje').text());
			parent.cerrarVentanaModal('producto');
		});
	</script>
</s:if>
<s:form name="guardarProducto" id="guardarProducto" 
action="/suscripcion/excepcion/autos/guardarProducto.action" namespace="/suscripcion/excepcion/autos" theme="simple">
<s:hidden name="excepcionId" id="excepcionId"/>
<s:hidden name="tipoValorSumaAsegurada" value="2"></s:hidden>
<table id="desplegarDetalle">
	<tr>
		<th>Negocio:</th>
		<td>
			<s:select list="negocios" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
					  name="negocioId" id="negocios" cssClass="cajaTexto w300" labelposition="left" label="Negocio"
					  onchange="cargarComboNegProductos(this.value)">
			</s:select>
		</td>
	</tr>
	<tr>
		<th>Producto:</th>
		<td>
			<s:select list="productos" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
					  name="productoId" id="productos" cssClass="cajaTexto w300"
					  onchange="cargarComboTipoPolizas(this.value)">
			</s:select>
		</td>
	</tr>
	<tr>
		<th>Tipo de P&oacute;liza:</th>
		<td>
			<s:select list="tipoPolizas" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
					  name="tipoPolizaId" id="tipoPolizas" cssClass="cajaTexto w300"
					  onchange="cargarComboLineasNegocio(this.value)">
			</s:select>
		</td>
	</tr>
	<tr>
		<th>L&iacute;nea de Negocios:</th>
		<td>
			<s:select list="lineasNegocio" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
					  name="lineaNegocioId" id="lineasNegocio" cssClass="cajaTexto w300" 
					  onchange="cargarComboNegPaquete(this.value)">
			</s:select>
		</td>
	</tr>
	<tr>
		<th>Paquete:</th>
		<td>
			<s:select list="paquetes" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
					  name="paqueteId" id="paquetes" cssClass="cajaTexto w300" 
					  onchange="cargarComboNegCobertura(this.value)">
			</s:select>
		</td>
	</tr>
	<tr>
		<th>Cobertura:</th>
		<td>
			<s:select list="coberturas" headerKey="" headerValue="%{getText('midas.general.noimporta')}" 
			          name="coberturaId" id="coberturas" cssClass="cajaTexto w300"
         			  onchange="reiniciarCobertura()">
			</s:select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<hr />
		</td>
	</tr>
</table>
<div id="detalleCobertura" style="display:none;">
<table  id="desplegarDetalle">
	<tr>
		<th width="30%">Cobertura</th>
		<td width="50%"><span id="cobertura.descripcion"></span></td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr id="sumaAseguradaDescripcion">
<!-- 		<th>Suma Asegurada: Entre</th> -->
		<td style="display: none"><span id="cobertura.minsumaasegurada"></span> y <span id="cobertura.maxsumaasegurada"></span></td>
	</tr>
	<tr id="sumaAseguradaValor">
		<th>Valor a detectar:  Mayor o igual a</th>
		<td><s:textfield name="sumaAsegurada" id="sumaAsegurada" cssClass="cajaTexto w200" onchange="this.value = number_format(this.value,2,'.',',',' ');"
		cssStyle=""/>
		<span style=" width: 140px;"><input type="checkbox" id="checkValor" style="" onchange="checkValorBox(this.id)"/>Este valor no importa</span></td>
	</tr>
	<tr>
		<td><span id="cobertura.sumaAsegurada"></span></td>
	</tr>
	<tr>
				<th>Deducibles</th>
				<td><div style="display: none;">
						Rango entre <span id="cobertura.mindeducible"></span> y <span
							id="cobertura.maxdeducible"></span>
					</div>
				</td>
			</tr>	
	<tr>
		<th>Valor a detectar: Entre</th>
		<td>
			<table>
				<tr>
					<td><s:textfield name="minDeducible" cssClass="cajaTexto w100" id="minDeducible" onchange="this.value = number_format(this.value,2,'.',',',' ');"/></td>
					<td><s:textfield name="maxDeducible" cssClass="cajaTexto w100" id="maxDeducible" onchange="this.value = number_format(this.value,2,'.',',',' ');"/></td>
					<td style="font-size: 9px;"><span style=" width: 140px;"><input type="checkbox" id="checkRango" style="" onclick="checkRangoBox(this.id);"/>Este valor no importa</span></td>
				</tr>
			</table>
		</td>
		<td></td>	
	</tr>	
</table>
<div id="required" class="required">Verif&iacute;que que el rango este dentro del l&iacute;mite</div>
</div>
<!-- Botones Footer -->
<div id="footer">
	<div id="b_guardar">
		<a href="javascript: void(0);"
			onclick="javascript:guardar();">
			<s:text name="midas.boton.guardar"/>
		</a>
	</div>
	<div class="btn_back w80"  style="display:inline; float: left; ">
		<a href="javascript: void(0);"
			onclick="javascript:parent.cerrarVentanaModal('producto');">
			<s:text name="midas.boton.cerrar"/>
		</a>
	</div>
</div>	
</s:form>