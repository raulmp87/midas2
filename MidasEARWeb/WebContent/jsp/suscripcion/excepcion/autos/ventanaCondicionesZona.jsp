<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<style type="text/css">

#footer{
	margin-left: 30%;
}
</style>
<script type="text/javascript">
function cargarCombo(value){
	parent.listadoService.getMapMunicipiosPorEstado(value,
		function(data){
			parent.addOptionsHeaderAndSelect(document.getElementById('municipios'), data, '', '', 'ESTA VALOR NO IMPORTA');
		}
	);
}

function guardar(){
	if(confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci�n\u003F')){
		document.forms[0].submit();
	}
}
</script>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			parent.recargarExcepcion(jQuery('#excepcionId').val(),jQuery('#mensaje').text());
			parent.cerrarVentanaModal('zona');
		});
	</script>
</s:if>
<s:if test="%{mensaje != null}">
	<script type="text/javascript">
		jQuery('#mensaje').css('font-size', '12px');
		jQuery('#mensaje').css('color', 'green');
		jQuery('#mensaje').css('text-align', 'center');
		jQuery('#mensaje').show();
	</script>
</s:if>
<s:form name="guardarZona" id="guardarZona" action="/suscripcion/excepcion/autos/guardarZona.action" namespace="/suscripcion/excepcion/autos">
<s:hidden name="excepcionId" id="excepcionId"/>
<table id="desplegarDetalle">
	<tr>
		<td>Estado:</td>
		<td>
			<s:select cssClass="txtfield" list="estados" headerKey="" headerValue="%{getText('midas.general.noimporta')}" name="estadoId" id="estados" onchange="cargarCombo(this.value);">
			</s:select>
		</td>
	</tr>
	<tr>
		<td>Municipio:</td>
		<td>
			<s:select cssClass="txtfield"  list="municipios" headerKey="" headerValue="%{getText('midas.general.noimporta')}" name="municipioId" id="municipios">
			</s:select>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
		<div id="footer">			
			<div id="b_guardar">
				<a href="javascript: void(0);"
					onclick="javascript:guardar();">
					<s:text name="midas.boton.guardar"/>
				</a>
			</div>
			<div class="btn_back w80"  style="display:inline; float: left; ">
				<a href="javascript: void(0);"
					onclick="javascript:parent.cerrarVentanaModal('zona');">
					<s:text name="midas.boton.cerrar"/>
				</a>
			</div>						
		</div>
</s:form>