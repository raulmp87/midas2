<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/suscripcion/excepcion/autos/contenedorHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div class="titulo" style="width:900px;">
	<s:text name="midas.excepcion.suscripcion.auto.title" />
</div>
<div id="indicador"></div>
<div id="listadoExcepciones" class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<div id=""  style="margin-left: 550px;" align="right">
	<div align="right" class="btn_back w140" style="float: left;">
		<a href="javascript: void(0);" class="icon_limpiar" 
		   onclick="javascript:limpiaFiltros();">
			<s:text name="midas.suscripcion.solicitud.solicitudPoliza.limpiar"/>
		</a>
	</div>		
	<div align="right" id="b_imprimir"  style="margin-left: 20px;">
		<a href="javascript: void(0);" style="width: 70px;"
		   onclick="javascript:imprimirListadoExcepciones();">
			<s:text name="midas.boton.imprimir"/>
		</a>
	</div>		
	<div align="right" id="b_agregar" style="margin-left: 30px;">
		<a href="javascript: void(0);"
		   onclick="javascript:agregarExcepcion();">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
		
</div>