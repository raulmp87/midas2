<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:iterator value="resultadosBusquedaVehiculo">
	<item>
		<id><s:property value="id.claveEstilo" /></id>
		<estiloId><s:property value="id" /></estiloId>
		<descripcion><s:property value="descripcionEstilo" escapeHtml="false" escapeXml="true" /></descripcion>
		<marcaId><s:property value="marcaVehiculoDTO.idTcMarcaVehiculo"  escapeHtml="false" escapeXml="true" /></marcaId>
		<claveTipoBien><s:property value="id.claveTipoBien"/></claveTipoBien>
	</item>
	</s:iterator>
</resultados>
