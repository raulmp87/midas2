<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<script type="text/javascript">
	// Revisa el estado del check box
	function validaCheckBox() {
		if (jQuery("#noImportaCheck").is(':checked')) {
			CheckBox(true);
		} else {
			CheckBox('');
		}
	}
	//Deshabilita/Habilita los inputs para que se manden nulos
	function CheckBox(val) {
		jQuery('input[id != "noImportaCheck"]').attr('disabled', val);
	}
	// Limpia los valores de los imputs
	function cleanInput() {
		jQuery("input").attr('value', '');
	}
	// Manda el Formulario
	function guardar() {
		if(validaGuardar()){
			if (confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci�n\u003F')) {
					document.forms[0].submit();
				}
		}
	}
	// Valida que ambos campos tengan algo
	function validaGuardar(){
		if(!jQuery("#noImportaCheck").is(':checked')){
			if(	jQuery("#modeloFinal").attr("value") == '' || 	jQuery("#modeloFinal").attr("value") == null || 
				jQuery("#modeloInicial").attr("value") == '' || jQuery("#modeloInicial").attr("value") == null){
				// Muestra el mensaje de error
					jQuery("#required").text('Ambos a�os son requedidos');
					jQuery("#required").show();
					return false;
			}
			// Comprueba la longitud de las fechas
			else if(jQuery("#modeloFinal").attr("value").length < 4 || jQuery("#modeloInicial").attr("value").length < 4){
				// Muestra el mensaje de error
					jQuery("#required").text('Ingresa por lo menos 4 digitos');
					jQuery("#required").show();
					return false;
			}
			// Valida que la fechaInicio < fechaFin
			else if(parseInt(jQuery("#modeloFinal").attr("value")) < parseInt(jQuery("#modeloInicial").attr("value"))){
				// Muestra el mensaje de error
					jQuery("#required").text('El valor inicial es mayor al final, por favor de corrgir el orden.');
					jQuery("#required").show();
					return false;
			}
			else{
				jQuery("#required").text('');
				jQuery("#required").hide();
				return true;
			}
		}else{
			return true;
		}
	}
	// Evalua si la fecha inicial es una letra
	function isNumero(){
		var n = jQuery("#modeloInicial").attr("value");
		var year = new Date();
			if(n == null || n == ''){
				return year.getFullYear();
			}else{
				return parseInt(n);
			}
	}
</script>
<style>
.inputModel {
	width: 40px;
}

.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
}

body {
	font-size: 10px;
}

#footer {
	margin-top: 30px;
	margin-left: 35%;
}

#noImporta {
	margin-top: 19px;
	text-align: center;
}
#fechasInput{
	margin-top: 26px;
	text-align: center;
}
</style>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			parent.recargarExcepcion(jQuery('#excepcionId').val(),jQuery('#mensaje').text());
			parent.cerrarVentanaModal('modelo');
		});
	</script>
</s:if>
<s:if test="%{mensaje != null}">
	<script type="text/javascript">
		jQuery('#mensaje').css('font-size','12px');
		jQuery('#mensaje').css('color','green');
		jQuery('#mensaje').css('text-align','center');
		jQuery('#mensaje').show();
	</script>
</s:if>
<s:form id="form" theme="simple"
	action="/suscripcion/excepcion/autos/guardarModelo.action"
	namespace="/suscripcion/excepcion/autos">
	<s:hidden name="excepcionId" id="excepcionId"/>
	<div id="fechasInput">
		Modelos a detectar: Entre
		<s:textfield name="modeloInicial" id="modeloInicial"
			onkeypress="return parent.soloNumeros(this, event, false);"
			cssClass="inputModel" maxlength="4"></s:textfield>
		* y
		<s:textfield name="modeloFinal" id="modeloFinal"
			onkeypress="return parent.soloNumeros(this, event, false);"
			cssClass="inputModel" maxlength="4"></s:textfield>
		*
	</div>
	<div id="required" class="required">Ambos campos son requeridos</div>
	<div id="noImporta">
		<s:checkbox name="checkBox" id="noImportaCheck"
		onchange="validaCheckBox();cleanInput();"/>
	 <label for="noImportaCheck">Este campo no importa</label>
	</div>
	<br />
	<!-- Botones Footer -->
	<div id="footer">
		<div id="b_guardar">
			<a href="javascript: void(0);" onclick="javascript:guardar();"> <s:text
					name="midas.boton.guardar" /> </a>
		</div>
		<div class="btn_back w80"  style="display:inline; float: left; ">
			<a href="javascript: void(0);"
				onclick="javascript:parent.cerrarVentanaModal('modelo');"> <s:text
					name="midas.boton.cerrar" /> </a>
		</div>
	</div>
</s:form>