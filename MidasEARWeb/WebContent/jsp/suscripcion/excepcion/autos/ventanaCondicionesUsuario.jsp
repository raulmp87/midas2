<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<script type="text/javascript">
	// Revisa el estado del check box
	function validaCheckBox() {
		if (jQuery("#noImportaCheck").is(':checked')) {
			CheckBox(true);
		} else {
			CheckBox('');
		}
	}
	//Deshabilita/Habilita los inputs para que se manden nulos
	function CheckBox(val) {
		jQuery('input[id != "noImportaCheck"]').attr('disabled', val);
	}
	// Limpia los valores de los imputs
	function cleanInput() {
		jQuery("input").attr('value', '');
	}
	// Manda el Formulario
	function guardar() {
		if (confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci�n\u003F')) {
			document.forms[0].submit();
		}
	}
</script>
<style>
.inputModel {
	width: 40px;
}

.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
}

body {
	margin-top: 26px;
	font-size: 10px;
}

#form {
	text-align: center;
	margin-top: 20px;
}

#footer {
	text-align: center;
	margin-top: 43px;
	margin-left: 89px;
}
</style>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			parent.recargarExcepcion(jQuery('#excepcionId').val(),jQuery('#mensaje').text());
			parent.cerrarVentanaModal('usuario');
		});
	</script>
</s:if>
<s:if test="%{mensaje != null}">
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#mensaje').css('font-size', '12px');
			jQuery('#mensaje').css('color', 'green');
			jQuery('#mensaje').css('text-align', 'center');
			jQuery('#mensaje').show();
		});
	</script>
</s:if>
<div id="content">
	<div id="form">
		<s:form action="/suscripcion/excepcion/autos/guardarUsuario.action"
			namespace="/suscripcion/excepcion/autos">
			<s:hidden name="excepcionId" id="excepcionId"/>
			<div>
				<s:select list="tipoUsuario" headerKey=""
					headerValue="%{getText('midas.general.noimporta')}"
					name="usuarioId" id="tipoUsuario" label="Usuario" />
			</div>
			<!-- Botones Footer -->
			<div id="footer">
				<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript:guardar();">
						<s:text name="midas.boton.guardar" /> </a>
				</div>
				<div class="btn_back w80"  style="display:inline; float: left; ">
					<a href="javascript: void(0);"
						onclick="javascript:parent.cerrarVentanaModal('usuario');">
						<s:text name="midas.boton.cerrar" /> </a>
				</div>
			</div>
		</s:form>
	</div>
</div>