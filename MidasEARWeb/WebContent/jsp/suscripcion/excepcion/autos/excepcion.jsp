<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<style type="text/css" >
#columns {
	width: 100%;
}

#columns .column {
	position: relative;
	padding: 1%;
}

#columns .left {
	float: left;
	width: 60%;
}

.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
}

#columns .right {
	float: right;
}
</style>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<script type="text/javascript">
	// Abre la ventana para seleccionar un agente
	function seleccionarAgente() {
		parent.mostrarVentanaModal("agente", "Selecci\u00F3n Agente", 200, 400,
				510, 150, seleccionarAgentePath);
	}
	function mostrarModalExcepcion(id, texto, url, width, height, x, y) {
		if (document.getElementById("excepcionId").value != null
				&& document.getElementById("excepcionId").value != '') {
			if (width == null) {
				width = 350;
			}
			if (height == null) {
				height = 200;
			}
			if (x == null) {
				x = 200;
			}
			if (y == null) {
				y = 320;
			}
			parent.mostrarVentanaModal(id, texto, 200, 320, width, height, url);
		} else {
			parent.mostrarVentanaMensaje('10',
					'Antes de agregar condiciones, guarde la excepci&oacute;n');
		}
	}
	
	function submitFormExcepcion() {
		parent.submitVentanaModal('mostrar', document.forms[0]);
	}
	
	function guardar() {
		var form = document.forms[0];
		if (validaGuardar()) {
			if (confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci\u00f3n\u003F')) {
				parent.submitVentanaModal('mostrar', form);
			}
		} else {
			
	if (validaCheckBox('notificacion')
					|| validaCheckBox('autorizacion')) {

				parent
						.mostrarMensajeInformativo(
								"para continuar por favor seleccione Nofitificar o Requiere Autorizaci\u00f3n",
								"20", null, null);
			}
		}

	}
	// valida que almenos notificacion o autorizacion este seleccionado
	function validaGuardar() {
		try {
			var guardar = new Boolean();
			guardar = false;
			if (!validaCheckBox('notificacion')
					|| !validaCheckBox('autorizacion')) {
				if (!validaCheckBox('notificacion')) {
					if (validateAll(true,'save')) {
						guardar = true;
					} else {
						return false;
					}
				}
				guardar = true;
				return guardar;
			} else {
				guardar = false;
				return guardar;
			}
		} catch (error) {
			alert("validaGuardar error description \n" + error.message);
		}
	}
	// valida el estado del check box de autoriacion
	function validaAutorizacion() {
		if (jQuery('#autorizacion').is(':checked')) {
			return true;
		} else {
			return false;
		}
	}

	// Revisa el estado del check box
	function validaCheckBox(idCheck) {

		if (!jQuery("#" + idCheck).is(':checked')) {
			return true;
		} else {
			return false;
		}

	}
	// Limpia el input correo
	function cleanInput() {
		jQuery("#correo").attr('value', '');
	}
	//Deshabilita/Habilita los inputs para que se manden nulos
	function checkBoxEnlable(val) {
		jQuery('#correo').attr('disabled', val);
		jQuery('#correo').addClass('jQrequired');
	}

	function quitaComa(e) {
		tecla = (document.all) ? e.keyCode : e.which;
		if (tecla == 44)
			return false;
	}
</script>
<s:if test="%{excepcion.correo != null}">
	<script type="text/javascript">
		// Habilita el campo de correo en la accion de mostrar excepcion
		jQuery(document).ready(function(){
				jQuery('#correo').attr('disabled','');
		});
	</script>
</s:if>
<s:form name="guardarExcepcion" action="/suscripcion/excepcion/autos/guardarExcepcion.action" id="guardarExcepcion">
<s:hidden name="excepcionId" id="excepcionId"/>
	<div class="grid">
	<div class="row">
		<div id="contenedorGrids" class="c6">		
				<table id="agregar" class="">
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.numero"/></th>
						<td><s:property value="excepcion.numExcepcion"/></td>
						<td></td>
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.descripcion"/></th>
						<td colspan="2"><s:textfield name="excepcion.descripcion" required="true" maxlength="50" cssClass=" jQrequired txtfield" /></td>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.tipoUsuario"/></th>
						<td><s:property value="excepcion.usuario"/></td>	
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('usuario', 'Seleccionar tipo de usuario para la excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarUsuario.action',400);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>	
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.agente"/></th>
						<td><s:property value="excepcion.agente"/></td>
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('agente', 'Seleccionar agente para la excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarAgente.action', 650, 200);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.negocio"/></th>
						<td><s:property value="excepcion.negocio"/></td>
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('producto', 'Seleccionar condiciones de producto como excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarProducto.action', 630 , 490, 200, 150);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.producto"/></th>
						<td><s:property value="excepcion.producto"/></td>
						<td>
							
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.tipoPoliza"/></th>
						<td><s:property value="excepcion.tipoPoliza"/></td>
						<td>
							
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.lineaNegocio"/></th>
						<td><s:property value="excepcion.lineaNegocio"/></td>
						<td>
							
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.paquete"/></th>
						<td><s:property value="excepcion.paquete"/></td>
						<td>
							
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.cobertura"/></th>
						<td><s:property value="excepcion.cobertura"/></td>
						<td>
							
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.zona"/></th>
						<td><s:property value="excepcion.zona"/></td>
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('zona', 'Selección de Zona para la excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarZona.action', 600);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.vehiculo"/></th>
						<td><s:property value="excepcion.amisDescripcionVehiculo"/></td>
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('vehiculo', 'Seleccionar condiciones del vehículo como excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarVehiculo.action',600,420);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>		
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.rangoModelo"/></th>
						<td><s:property value="excepcion.rangoModelos"/></td>
						<td>
							<div align ='center'>
								<a onclick="mostrarModalExcepcion('modelo', 'Seleccionar condiciones de modelo como excepci\u00F3n', '/MidasWeb/suscripcion/excepcion/autos/mostrarModelo.action', 450);" href="javascript: void(0);">
									   <img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Ver'/>
								</a>
							</div>
						</td>		
					</tr>
				</table>
			</div><!-- End column left -->
				<div id="contenerdorAgente" class="c4">			
				<table id="agregar">
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.correo"/></th>
						<td><s:textarea id="correo" name="excepcion.correo" disabled="true"
										rows="4" cols="20"
						 				cssClass="txtfield w150 jQEmailList" onkeypress="return quitaComa(event);"/>
						 				</td>
						 				
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.notificar"/></th>
						<td><s:checkbox id="notificacion" name="excepcion.notificacion" cssClass="txtfield" onchange="checkBoxEnlable(validaCheckBox(this.id));cleanInput();"/></td>
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.autorizacion"/></th>
						<td><s:checkbox id="autorizacion" name="autorizacion" cssClass="txtfield"/></td>
					</tr>
					<tr>
						<th><s:text name="midas.excepcion.suscripcion.auto.habilitado"/></th>
						<td><s:checkbox name="habilitado" cssClass="txtfield"/></td>
					</tr>
				</table>
		</div>	<!-- Right column -->
		</div><!-- End row -->
	</div><!-- End grid-->
	<br/>
	<div id="" style="float:left; margin-left: 600px;">
		<div align="right" id="b_guardar">
			<a href="javascript: void(0);"
			   onclick="javascript:guardar();">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>		
		<div class="btn_back w80" style="display:inline; float: left; " >
			<a href="javascript: void(0);"
				onclick="parent.mostrarListadoExcepciones();parent.cerrarVentanaModal('mostrar');">
				<s:text name="midas.boton.cerrar"/>
			</a>
		</div>	
	</div>
</s:form>