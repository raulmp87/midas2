<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css"/>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet"
	type="text/css"/>
<style type="text/css">
#content {
	font-size: 10px;
}

#form {
	text-align: center;
	margin-top: 20px;
}

#footer {
	margin-top: 30px;
	margin-left: 30%;
}
#buscador{
		display: none;
}
table.tr {
	text-alight: left;
}
table {
	font-size: 10px;
}

.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>
<script type="text/javascript">
	var urlBusquedaVehiculos = '<s:url action="buscarVehiculo" namespace="/suscripcion/excepcion/autos"/>';
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>	
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'>
</script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script>
function cargarEstilosPorMarca(marca, cveTipoBien,idEstilo){
		
		dwr.util.setValue('estilos','');
		if(marca != '' && cveTipoBien != ''){
			removeAllOptionsAndSetHeader(document.getElementById('estilos'), parent.headerValue, 'CARGANDO ...');
			listadoService.listarEstilosPorMarcaCveTipoBien(marca, cveTipoBien, function(data){
				if(idEstilo){
					addOptionsHeaderAndSelect(document.getElementById('estilos'), data, idEstilo, '', 'SELECCIONE ...');
				}else{
					addOptions(document.getElementById('estilos'), data);
				}
		});
	}
}

	function cargarMarcasPorCveTipoBien(id,idMarca){
		
		if(jQuery('#cvesTipoBien').val() == ''){
			removeAllOptionsAndSetHeader(document.getElementById('marcas'), parent.headerValue, 'SELECCIONE ...');
		}else{
			removeAllOptionsAndSetHeader(document.getElementById('marcas'), parent.headerValue, 'CARGANDO ...');
		}
		removeAllOptionsAndSetHeader(document.getElementById('estilos'), parent.headerValue, 'SELECCIONE ...');
		if(id != ''){
			listadoService.listarMarcasPorCveTipoBien(id, function(data){
					if(idMarca){
						addOptionsHeaderAndSelect(document.getElementById('marcas'), data, idMarca, '', 'SELECCIONE ...');
					}else{
						addOptions(document.getElementById('marcas'),data);
					}
		});
		}
	
	}

	function validaGuardar(){
			if(validaCheckBox('checkAmis') && validaCheckBox('checkDesc')){
				alert("Solo un tipo de excepci�n puede estar habilitada");
				return false;
			}
			return true;
	}

	// Manda el Formulario
	function guardar() {
		if (validaGuardar()) {
			if (confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci�n\u003F')) {
				document.forms[0].submit();
			}
		}
	}

	$(function() {
		$('#descripcionBusqueda')
				.autocomplete(
						{
							source : function(request, response) {
								$
										.ajax({
											type : "POST",
											url : urlBusquedaVehiculos,
											data : {
												descripcionBusqueda : request.term,
												idToSeccion : jQuery(
														'#lineaNegocioInciso')
														.val()
											},
											dataType : "xml",
											success : function(xmlResponse) {
												response($("item", xmlResponse)
														.map(
																function() {
																	return {
																		value : $(
																				"descripcion",
																				this)
																				.text(),
																		estiloId : $(
																				"estiloId",
																				this)
																				.text(),
																		marcaId : $(
																				"marcaId",
																				this)
																				.text(),
																		claveTipoBien : $(
																				"claveTipoBien",
																				this)
																				.text(),
																		id : $(
																				"id",
																				this)
																				.text()
																	}
																}));
											}
										})
							},
							minLength : 3,
							delay: 1000,
							select : function(event, ui) {
								dwr.util.setValue('cvesTipoBien',ui.item.claveTipoBien);
								cargarMarcasPorCveTipoBien(ui.item.claveTipoBien,ui.item.marcaId);
								cargarEstilosPorMarca(ui.item.marcaId,ui.item.claveTipoBien,ui.item.id);
								setTimeout(function() {
									dwr.util.setValue('estilos', ui.item.id);
									jQuery('#estilos').change();
								}, 1000)

							}
						});
	});
	// muestra - oculta el buscador dinamico
	function mostrarBuscador(valor) {
		if (!jQuery('#buscador').is(":visible")) {
			jQuery('#buscador').show("slow");
		}else{
			if(valor == ''){
				jQuery('#buscador').hide("slow");
		}
		}
	}
	function onChangeLineaNegocio(target, negocioSeccionSelect) {
		cargarMarcasPorCveTipoBien(jQuery('#cvesTipoBien').val());		
		/*
		var idNegocioSeccion = dwr.util
				.getValue(jQuery(negocioSeccionSelect).selector);
		targetNegocioSeccion = target;
		removeAllOptionsAndSetHeader(document.getElementById(target),parent.headerValue, "CARGANDO...");
		removeAllOptionsAndSetHeader(document.getElementById('estilos'), parent.headerValue, 'SELECCIONE ...');
		if (idNegocioSeccion != null && idNegocioSeccion != headerValue) {
			
		} else {
			addOptions(targetNegocioSeccion, null);
			// 		onChangeMarcaVehiculo(targetMarcaVehiculo,null,null);
		}*/
	}
	// Revisa el estado del check box
	function validaCheckBox(idCheck) {
		if (!jQuery("#" + idCheck).is(':checked')) {
			return true;
		} else {
			return false;
		}
	}
	
	// Copia el tipo cuando la busqueda es manual, al campo de descripcion de busqueda dinamica
	function cambiaDescripcion() {
		if (jQuery('#estilos').val() != '') {
			jQuery('#descripcionBusqueda').val(
					jQuery('#estilos option:selected').text());
		}
	}
</script>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(
				function() {
					parent.recargarExcepcion(jQuery('#excepcionId').val(),
							jQuery('#mensaje').text());
					parent.cerrarVentanaModal('vehiculo');
				});
	</script>
</s:if>
<s:if test="%{mensaje != null}">
	<script type="text/javascript">
		jQuery('#mensaje').css('font-size', '12px');
		jQuery('#mensaje').css('color', 'green');
		jQuery('#mensaje').css('text-align', 'center');
		jQuery('#mensaje').show();
	</script>
</s:if>
<s:if test="%{descripcionEstilo.length()!= 0}">
</s:if>
<s:else>
<s:property value="descripcionEstilo"/>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#checkDesc').attr({checked:'checked'});			
			jQuery('#descripcionEstilo').attr({disabled:'disabled'});
		});
	</script>
</s:else>
<s:if test="%{amis.length() != 0}">
</s:if>
<s:else>
	<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#checkAmis').attr({checked:'checked'});			
				jQuery('select').attr({disabled:'disabled'});
				jQuery('#amis').attr({disabled:'disabled'});
				jQuery('#descripcionBusqueda').attr({disabled:'disabled'});
			});
	</script>
</s:else>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div id="content">
	<div id="form">
		<s:form theme="simple" action="/suscripcion/excepcion/autos/guardarVehiculo.action"
	namespace="/suscripcion/excepcion/autos">
	<s:hidden name="excepcionId" id="excepcionId" />
				<div id="infoVehiculo">
					<table id="desplegarDetalle" width="100%">
						<tr>
							<th colspan="3"><span>Excepci&oacute;n por descripci&oacute;n de estilo en veh&iacute;culo</span></th>
						</tr>
						<tr>
							<td>Descripci&oacute;n</td>
							<td>
								<div class="ui-widget">
									<s:textfield name="descripcionEstilo" id="descripcionEstilo" cssClass="cajaTexto w250" cssStyle="font-size: 10px;" maxlength="20" />
								</div>								
							</td>		
							<td><input type="checkbox" id="checkDesc" onclick="checkDescBox(this.id);"/> Este valor no importa
								<script type="text/javascript">
								
									function checkDescBox(id) {
										jQuery('#descripcionEstilo').val('');
										if (validaCheckBox(id)) {
											jQuery('#checkAmis').attr('checked',true);
											checkAmisBox('checkAmis');
											jQuery('#descripcionEstilo').attr({
												disabled : ''
											});
										} else {
											jQuery('#descripcionEstilo').attr({
												disabled : 'disabled'
											});
										}
									}
								</script>
							</td>		
						</tr>
						<tr>
							<td colspan="3"><hr/></td>
						</tr>
						<tr>
							<th colspan="3"><span>Excepci&oacute;n por AMIS</span></th> 
						</tr>
						<tr>
							<td colspan="1" width="50px"><span>Buscar por Linea de negocio</span></td> 
							<td>	<s:select list="seccionList" id="lineaNegocioInciso" name="idToSeccion" listValue="descripcion" listKey="idToSeccion" headerKey="" headerValue="%{getText('midas.general.seleccione')}"
									onchange="onChangeLineaNegocio('marcas', 'lineaNegocioInciso');mostrarBuscador(this.value);"
									cssClass="cajaTexto"/>
							</td>
						</tr>
						<tr>
						<td></td>
						<td colspan="1">
						<div id="buscador">
								<div style="width: 50px; margin-left: -93px">
													<span style="width:50px;margin-left:24px">Descripci�n:</span>
												</div>	
												<input type="text" name="descripcionBusqueda" id="descripcionBusqueda" class="cajaTexto"
												onkeypress="mostrarLimpiar();"
												style="width: 321px; margin-left: 0px;"/>
												<div>
												<script type="text/javascript">
													function limpiarDesc(){
														jQuery('#descripcionBusqueda').val('');
														jQuery('#descripcionBusqueda').focus();
														jQuery('#limpiar').hide();
													}
													
													function mostrarLimpiar(){
														jQuery('#limpiar').show();
													}
												</script>
												<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripci�n" 
												style="display:none;margin-left: 324px; border-bottom-width: 0px; margin-top: -15px;"
												onclick ="limpiarDesc();"/></div>
								</div>
						</td></tr>
						<tr>
							<td>
							�
							</td>
						</tr>
						<tr>
							<td><span>Tipo Bien:</span>
							</td>							
							<td>
								<s:select list="cvesTipoBien" name="cveTipoBien" headerKey="" headerValue="%{getText('midas.general.seleccione')}" id="cvesTipoBien" cssClass="cajaTexto" onchange="cargarMarcasPorCveTipoBien(this.value)"/>
							</td>
						</tr>
						<tr>
							<td><span>Marca:</span>
							</td>
							
							<td>
								<s:select list="marcas" name="marcaId" id="marcas" headerKey="" headerValue="%{getText('midas.general.seleccione')}" cssClass="cajaTexto" onchange="cargarEstilosPorMarca(this.value, jQuery('#cvesTipoBien').val() )"/>
							</td>
						</tr>
						<tr>
							<td><span>Tipo:</span>
							</td>
							<td>
								<s:select list="estilos" name="estiloId" id="estilos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" cssClass="cajaTexto" onchange="jQuery('#amis').val(this.value);cambiaDescripcion();"/>
							</td>
						</tr>
						<tr>
							<td>Clave AMIS:</td>
							<td><s:textfield name="amis" id="amis" cssClass="cajaTexto" readonly="true" /></td>
							<td><input type="checkbox" id="checkAmis" onchange="checkAmisBox(this.id);" /> Este valor no importa
							<script type="text/javascript">
										function checkAmisBox(id) {
										jQuery('#amis').val('');
										if (validaCheckBox(id)) {
											jQuery('#checkDesc').attr('checked',true);
											checkDescBox('checkDesc');
											jQuery('select').attr({
												disabled : ''
											});
											jQuery('#amis').attr({
												disabled : ''
											});
											jQuery('#descripcionBusqueda').attr({disabled:''});
										} else {
											jQuery('select').attr({
												disabled : 'disabled'
											});
											jQuery('select').val('');
											jQuery('#descripcionBusqueda').val('');
											jQuery('#amis').attr({
												disabled : 'disabled'
											});
											jQuery('#descripcionBusqueda').attr({disabled:'disabled'});
										}
									}
								</script>
							</td>
						</tr>
					</table>
				</div>
				<div id="descripcion" style="text-align: center"><span>Descripci�n: <s:property value="descripcionEstiloId" default="NI"/></span></div>
				<!-- Botones Footer -->
				<div id="footer">
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript:guardar();">
							<s:text name="midas.boton.guardar" /> </a>
					</div>
					<div class="btn_back w80"  style="display:inline; float: left;">
						<a href="javascript: void(0);"
							onclick="javascript:parent.cerrarVentanaModal('vehiculo');">
							<s:text name="midas.boton.cerrar" /> </a>
					</div>
				</div>
		</s:form>
	</div>
</div>