<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="idExcepcion" type="ro" width="0" sort="int" hidden="true">idToNegAgente</column>
		<column id="acciones" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="accionVer" type="img" width="30" sort="na"/>
		<column id="numExcepcion" type="ro" width="60" sort="int" escapeXml="true" >N�mero</column>
		<column id="descripcion" type="ro" width="150" sort="int">Descripci�n</column>
		<column id="usuario" type="ro" width="60" sort="str">Usuario</column>
		<column id="agente" type="ro" width="60" sort="str">Agente</column>
		<column id="negocio" type="ro" width="150"  sort="str">Negocio</column>
		<column id="producto" type="ro" width="150" sort="str">Producto</column>
		<column id="tipoPoliza" type="ro" width="150" sort="str">Tipo P�liza</column>
		<column id="linea" type="ro" width="150" sort="str">L�nea</column>
		<column id="paquete" type="ro" width="150" sort="str">Paquete</column>		
		<column id="cobertura" type="ro" width="150" sort="str">Cobertura</column>
		<column id="cobertura" type="ro" width="120" sort="str">Suma Asegurada</column>
		<column id="cobertura" type="ro" width="80" sort="str">Deducible</column>
		<column id="zona" type="ro" width="150" sort="str">Zona</column>
		<column id="amisDescripcionVehiculo" type="ro" width="150" sort="str">AMIS o Descripci�n</column>
		<column id="modelos" type="ro" width="100" sort="str">Modelos</column>
		<column id="notifica" type="ro" width="80" sort="str">Se Notifica</column>
		<column id="autoriza" type="ro" width="100" sort="str">Autorizaci�n</column>
		<column id="habilitado" type="ro" width="80" sort="str">Habilitado</column>
	</head>			
	<s:iterator value="excepciones" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idExcepcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_editar.gif^Ver/Editar^javascript: mostrarExcepcion(<s:property value="idExcepcion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarExcepcion(<s:property value="idExcepcion" escapeHtml="false" escapeXml="true"/>, <s:property value="numExcepcion" escapeHtml="false" escapeXml="true"/>)^_self</cell>						
			<cell><s:property value="numExcepcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuario" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="agente" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="negocio" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="producto" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="tipoPoliza" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="lineaNegocio" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="paquete" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="cobertura" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="sumaAsegurada" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="deducible" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="zona" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="amisDescripcionVehiculo" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="rangoModelos" escapeHtml="false" escapeXml="true"/></cell>		
			<cell>	
				<s:if test="notificacion">
					SI
				</s:if>		
				<s:else>
					NO
				</s:else>			
			</cell>		
			<cell>
				<s:if test="autorizacion">
					SI
				</s:if>		
				<s:else>
					NO
				</s:else>		
			</cell>		
			<cell>
				<s:if test="habilitado">
					SI
				</s:if>	
				<s:else>
					NO
				</s:else>
			</cell>									
		</row>
	</s:iterator>	
</rows>
