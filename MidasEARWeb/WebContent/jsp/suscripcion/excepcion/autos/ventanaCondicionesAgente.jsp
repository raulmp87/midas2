<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet"
	type="text/css"/>
<script type="text/javascript">
	var urlBusquedaAgentes = '<s:url action="buscarAgente" namespace="/suscripcion/cotizacion/auto"/>';
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>	
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'>
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	// Manda el Formulario
	function guardar() {
		if (confirm('\u00BFEst\u00e1 seguro que desea guardar los datos de la excepci\u00f3n\u003F')) {
			document.forms[0].submit();
		}
	}
</script>
<style type="text/css">
#content {
	font-size: 10px;
}

body{
	overflow: hidden;
}
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}

#form {
	text-align: center;
	margin-top: 20px;
}

#footer {
	float: right;
	padding-top:10px;
}
</style>
<script>
function enlableInput() {
	jQuery("#agenteId").val('');
	if (jQuery("#descripcionBusquedaAgente").attr("disabled")) {
		jQuery("#descripcionBusquedaAgente").attr("disabled", false);
	}else{
		jQuery("#descripcionBusquedaAgente").attr("disabled", true);
	}
	
}
$(function(){
	$( '#descripcionBusquedaAgente' ).autocomplete({
               source: function(request, response){
               		$.ajax({
			            type: "POST",
			            url: urlBusquedaAgentes,
			            data: {descripcionBusquedaAgente:request.term},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( $( "item", xmlResponse ).map( function() {
								return {
									value: $( "descripcion", this ).text(),
				                    id: $( "id", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 3,
               delay: 1000,
               select: function( event, ui ) {
            	   jQuery('#agenteId').val(ui.item.id);
               }		          
         });
 });	
</script>
<s:if test="agregada">
	<script type="text/javascript">
		jQuery(document).ready(function(){
			parent.recargarExcepcion(jQuery('#excepcionId').val(),jQuery('#mensaje').text());
		parent.dhxWins.window('agente').close();
		});
	</script>
</s:if>
<div id="content">
	<div id="form">
		<s:form id="guardarAgente"
			action="/suscripcion/excepcion/autos/guardarAgente.action"
			namespace="/suscripcion/excepcion/autos" name="guardarAgente">
			<s:hidden name="excepcionId" id="excepcionId" />
			<s:hidden name="agenteId" id="agenteId"/>
			<table id="agregar" width="100%">
				<tr>
					<td colspan="3">B&uacute;squeda por</td>
				</tr>
				<tr>
					<td width="15%"><span>Descripci&oacute;n:</span>
					</td>
					<td width="80%"><input type="text"
						name="descripcionBusquedaAgente" id="descripcionBusquedaAgente"
						class="cajaTexto" />
					</td>
				</tr>
			</table>
			<div style="text-align: right;">
				<s:checkbox  id="checkImporta" name="checkImporta" label="Este valor no importa"
					labelposition="left"
					onclick="enlableInput()"/>
			</div>
			<!-- Botones Footer -->
			<div id="footer">
				<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript:guardar();">
						<s:text name="midas.boton.guardar" /> </a>
				</div>
				<div class="btn_back w80"  style="display:inline; float: left; ">
					<a href="javascript: void(0);"
						onclick="javascript:parent.cerrarVentanaModal('agente');">
						<s:text name="midas.boton.cerrar" /> </a>
				</div>
			</div>
		</s:form>
	</div>
</div>