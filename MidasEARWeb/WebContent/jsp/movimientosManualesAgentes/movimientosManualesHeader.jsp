<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/movimientosManualesAgentes/movimientosManuales.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_splt.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScript.js'/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar_start.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<script type="text/javascript">
	var pathDetalle='/MidasWeb/agtSaldoMovto/verDetalle.action';
	var listarFiltradoPath = '<s:url action="listarFiltrado" namespace="/agtSaldoMovto"/>';
	var verDetalleMovtoSaldoPath = '<s:url action="verDetalleSaldoMovimientos" namespace="/agtSaldoMovto"/>';
	var verDetalleMovimientoPath = '<s:url action="verDetalleMovimiento" namespace="/agtSaldoMovto"/>';
	var seleccionarUsuarioMovManualesPath = '<s:url value="../jsp/suscripcion/solicitud/autorizacion/solicitudBusquedaUsuario.jsp"></s:url>';
	var buscarUsuarioMovManualesPath = '<s:url action="buscarUsuarios" namespace="/fuerzaventa/MovimientosManuales"/>';
	var buscarMovimientosManualesPath = '<s:url action="buscarMovimientosManuales" namespace="/fuerzaventa/MovimientosManuales"/>';
	var aplicarMovimientosManualesPath = '<s:url action="aplicarMovimientosManuales" namespace="/fuerzaventa/MovimientosManuales"/>';
	var eliminarMovimientosManualesPath = '<s:url action="eliminarMovimientosManuales" namespace="/fuerzaventa/MovimientosManuales"/>';
</script>