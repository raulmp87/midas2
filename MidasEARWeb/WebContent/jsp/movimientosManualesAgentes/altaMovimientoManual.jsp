<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/movimientosManualesAgentes/movimientosManualesHeader.jsp"></s:include>

<script type="text/javascript">
	function agregarMovimiento(){
		var msg = null;
		var idAgente= jQuery("#idAgente").val();
		var conceptoId= jQuery("#conceptoId").val();
		var ramoId= jQuery("#ramoId").val();
		var subramoId= jQuery("#subramoId").val();
		var tipoMovimientoId= jQuery("#tipoMovimientoId").val();
		var importe= jQuery("#importe").val();
				
		if(idAgente==""){
			msg = 'Debe seleccionar un Agente.\n';
		}
		
		if(conceptoId==""){
			msg += 'Debe ingresar un Concepto.\n';
		}
		
		if(ramoId==""){
			msg += "Debe seleccionar un Ramo.\n";
		}
		
		if(subramoId==""){
			msg += "Debe seleccionar un Subramo.\n";
		}
		
		if(tipoMovimientoId==""){
			msg += "Debe seleccionar un Tipo de Movimiento.\n";
		}
		
		if(importe==""){
			msg += "Debe ingresar un Importe.\n";
		}		
		
		if(msg!=null){
			alert(msg);
		}else{
			alert('Formulario Completo');//listarFiltradoGenerico(listarFiltradoPath, 'agtSaldoGrid', document.agtSaldoForm,null,null);
		}
	}
</script>


<s:form action="listarFiltrado" id="agtSaldoForm" name="agtSaldoForm">
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="5">
				<s:text name="Alta  de Movimientos Manuales"/>
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.agente"></s:text>
			</th>
			<td>
				<s:textfield  name="agtSaldo.idAgente.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgt_MovManual();"></s:textfield>
					<s:textfield id="txtId"  cssStyle="display:none" onchange="onChangeAgente_MovManual();"/>
					<s:textfield id="id" cssStyle="display:none"/>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes_MovManual();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 w200" disabled="true" id="nombreCompleto"/>
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.estatus"/>
			</th>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="tipoSituacion" disabled="true" />
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="motivoEstatusAgente" disabled="true" />
			</td>
			<th>
				<s:text name="midas.fuerzaventa.movimientomanual.alta.fechaEstatus"/>
			</th>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="fechaEstatus" disabled="true" />
			</td>
		</tr>
	</table>
	<br>
	<table width="880px" id="filtrosM2">		
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.cargo"/>
			</th>
			<td width="15%">
				<s:textfield cssClass="cajaTextoM2" id="cargo" disabled="true" />
			</td>
			<th width="15%">
				<s:text name="midas.fuerzaventa.movimientomanual.alta.abono"/>
			</th>
			<td width="15%">
				<s:textfield cssClass="cajaTextoM2" id="abono" disabled="true" />
			</td>
			<th width="15%">
				<s:text name="midas.fuerzaventa.movimientomanual.alta.saldoFinal"/>
			</th>
			<td width="15%">
				<s:textfield cssClass="cajaTextoM2" id="saldoFinal" disabled="true" />
			</td>	
		</tr>		
	</table>
	<br>
	<table width="880px" id="filtrosM2">		
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.concepto"/>
			</th>
			<td width="10%">
				<s:textfield cssClass="cajaTextoM2 jQrequired" id="conceptoId" onchange="onChangeIdConcepto();"/>
				<s:textfield id="txtIdConcepto"  cssStyle="display:none" onchange="onChangeConcepto();"/>
				<s:textfield id="idC" cssStyle="display:none"/>
			</td>
			<td width="5%">
				<div class="btn_back w20">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoConceptos();">						
					</a>
				</div>
			</td>
			<td width="70%" colspan="3">
				<s:textfield cssClass="cajaTextoM2 w550" id="conceptoDescripcion" disabled="true"/>
			</td>				
		</tr>	
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.descripcion"/>
			</th>
			<td width="85%" colspan="5">
				<s:textfield cssClass="cajaTextoM2 w700" id="descripcion"   maxlength="80" />
			</td>			
		</tr>
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.ramo"/>
			</th>
			<td width="10%">
				<s:textfield cssClass="cajaTextoM2 jQrequired" id="ramoId" onchange="onChangeIdRamo();"/>
				<s:textfield id="txtIdRamo"  cssStyle="display:none" onchange="onChangeRamo();"/>
				<s:textfield id="idR" cssStyle="display:none"/>
			</td>
			<td width="5%">
				<div class="btn_back w20">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoRamos();">						
					</a>
				</div>
			</td>
			<td width="70%" colspan="3">
				<s:textfield cssClass="cajaTextoM2 w550" id="ramoDescripcion" disabled="true" />
			</td>				
		</tr>
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.subramo"/>
			</th>
			<td width="10%">
				<s:textfield cssClass="cajaTextoM2 jQrequired" id="subramoId" onchange="onChangeIdSubRamo();" />
				<s:textfield id="txtIdSubramo"  cssStyle="display:none" onchange="onChangeSubRamo();"/>
				<s:textfield id="idS" cssStyle="display:none"/>
			</td>
			<td width="5%">
				<div class="btn_back w20">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoSubRamos();">						
					</a>
				</div>
			</td>
			<td width="70%" colspan="3">
				<s:textfield cssClass="cajaTextoM2 w550" id="subramoDescripcion" disabled="true" />
			</td>
				
		</tr>
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.tipoMovto"/>
			</th>
			<td width="10%">				
				<s:select list="#{'A':'ABONO', 'C':'CARGO'}" id="tipoMovimientoId" cssClass="cajaTextoM2" disabled="true" headerKey="" headerValue="Seleccione.."/>
			</td>			
			<td width="75%" colspan="4">
				<br>
			</td>				
		</tr>
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.importe"/>
			</th>
			<td width="10%">
				<s:textfield cssClass="cajaTextoM2 jQrequired" id="importe" />
			</td>			
			<td width="75%" colspan="4">
				<br>
			</td>
				
		</tr>
		<tr>			
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.alta.fechaMovto"/>
			</th>
			<td width="10%">
				<s:textfield cssClass="cajaTextoM2" id="fechaMovimiento" disabled="true" name="fechaActual" />
			</td>
			<td width="5%">
				<br>
			</td>
			<td width="40%">
				<br>
			</td>
			<th width="15%">
				<s:text name="midas.fuerzaventa.movimientomanual.alta.usuario"/>
			</th>
			<td width="15%">
				<s:textfield cssClass="cajaTextoM2" id="usuario" disabled="true" name="usuario.nombreUsuario" />
			</td>	
		</tr>			
	</table>
	<br>
	<table width="880px">
		<tr align="right">
			<td width="50%">	
			
			</td>	
			<td>				
				<div class="btn_back w110" id="nuevoBoton">
					<a href="javascript: void(0);" class=".icon_limpiar"
						onclick="guardarMovimientoManual();">
						<s:text name="midas.boton.guardar"/>
					</a>			
				</div>										
			</td>	
			<td>
				<div class="btn_back w110" id="modificarBoton" style="display: none">
					<a href="javascript: void(0);" class="icon_masAgregar"
						onclick="modificarMovimiento();">
						<s:text name="midas.boton.editar"/>
					</a>
				</div>		
			</td>			
			<td>
				<div class="btn_back w110" id="borrarBoton" style="display: none">
					<a href="javascript: void(0);" class="icon_eliminar"
						onclick="emiminarMovimiento();">
						<s:text name="midas.boton.borrar"/>
					</a>
				</div>	
			</td>			
			<td>
				<div class="btn_back w110" id="guardarBoton" style="display: none">
					<a href="javascript: void(0);" class="icon_guardar2"
						onclick="agregarMovimiento();">
						<s:text name="midas.boton.guardar"/>
					</a>
				</div>
			</td>			
		</tr>			
	</table>	
	<div id="divCarga" style="position:absolute;"></div>		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>