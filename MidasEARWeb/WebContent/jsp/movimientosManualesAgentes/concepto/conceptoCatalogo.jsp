<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<s:include value="/jsp/movimientosManualesAgentes/concepto/conceptoHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>
<script type="text/javascript">	
	
	
	var tipoAccionConcepto='<s:property value="tipoAccion"/>';
	var noCerrarModal = '<s:property value="closeModal"/>';
	var varModal='conceptoModal';
	var idFieldConcepto = '<s:property value ="idField"/>';
	if(noCerrarModal=='No'){
		 varModal='';
	}
	
	jQuery(function(){
		listarConceptos(true);
	 });
	
	function listarConceptos(cargaInicial){				
		var urlFiltro=listarFiltradoConceptoPath;
		var urlFil="";
		if(cargaInicial==true){
			urlFil = urlFiltro+"?tipoAccion="+tipoAccionConcepto;
		}else{
			urlFil = urlFiltro;
		}				
		listarFiltradoGenerico(urlFil,"conceptosGrid", jQuery("#conceptoForm"),idFieldConcepto,varModal);
	}	
</script>
<s:form action="listarFiltradoConcepto" id="conceptoForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<!--<s:text name="midas.fuerzaventa.negocio.titulo"/>-->
				Listar Conceptos
			</td>
		</tr>
		<tr>	
			<th>
				<!--<s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text>-->
				Concepto
			</th>	 
			<td>
				<s:textfield  name="filtroConcepto.idRegistro" id="txtConceptoId" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>
				Descripción
			</th>	 
			<td>
				<s:textfield name="filtroConcepto.valor" id="txtConceptoDescripcion" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>			
		</tr>
		<tr>			
			<td colspan="6">
				<div class="btn_back w110">	
					<a href="javascript: void(0);" class="icon_buscar" id="conceptoBuscar"
						onclick="listarConceptos(false);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br>	
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="conceptosGrid" width="880px" height="300px" style="background-color:white;overflow:hidden"></div>	
</s:form>
