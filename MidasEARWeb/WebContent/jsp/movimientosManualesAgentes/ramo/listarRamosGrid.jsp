<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>        
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call> 		
        </beforeInit>
        <afterInit>
        	  <call command="splitAt"><param>1</param></call>
        </afterInit>
						
		<column id="id" type="ro" width="1" sort="int" >Id</column>
		<column id="codigo" type="ro" width="240" sort="int">Codigo</column>
 		<column id="ramo" type="ro" width="640" sort="str">Ramo</column>		

	</head>
	<s:iterator value="listaRamos" var="rowRamos" status="index">
	<row id="${index.count}">			
			<cell><![CDATA[${rowRamos.idTcRamo}]]></cell>
			<cell><![CDATA[${rowRamos.codigo}]]></cell>
			<cell><![CDATA[${rowRamos.descripcion}]]></cell>						
 		</row>
	</s:iterator>
</rows>