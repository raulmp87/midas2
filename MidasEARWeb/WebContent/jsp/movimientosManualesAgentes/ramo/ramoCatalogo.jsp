<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<s:include value="/jsp/movimientosManualesAgentes/ramo/ramoHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>
<script type="text/javascript">	
	
	
	var tipoAccionRamo='<s:property value="tipoAccion"/>';
	var noCerrarModal = '<s:property value="closeModal"/>';
	var varModal='ramoModal';
	var idFieldRamo = '<s:property value ="idField"/>';
	if(noCerrarModal=='No'){
		 varModal='';
	}
	
	jQuery(function(){
		listarRamos(true);
	 });
	
	function listarRamos(cargaInicial){				
		var urlFiltro=listarFiltradoRamoPath;
		var urlFil="";
		if(cargaInicial==true){
			urlFil = urlFiltro+"?tipoAccion="+tipoAccionRamo;
		}else{
			urlFil = urlFiltro;
		}				
		listarFiltradoGenerico(urlFil,"ramoGrid", jQuery("#ramoForm"),idFieldRamo,varModal);
	}	
</script>
<s:form action="listarFiltradoRamo" id="ramoForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<!--<s:text name="midas.fuerzaventa.negocio.titulo"/>-->
				Listar Ramos
			</td>
		</tr>
		<tr>	
			<th>
				<!--<s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text>-->
				Id Ramo
			</th>	 
			<td>
				<s:textfield  name="filtroRamo.codigo" id="txtRamoId" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>
				Ramo
			</th>	 
			<td>
				<s:textfield name="filtroRamo.descripcion" id="txtConceptoDescripcion" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>			
		</tr>
		<tr>			
			<td colspan="6">
				<div class="btn_back w110">	
					<a href="javascript: void(0);" class="icon_buscar" id="ramoBuscar"
						onclick="listarRamos(false);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br>	
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="ramoGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>	
</s:form>
