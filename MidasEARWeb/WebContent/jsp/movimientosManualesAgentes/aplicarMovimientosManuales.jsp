<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<s:include value="/jsp/movimientosManualesAgentes/movimientosManualesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>


<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var idAgente='<s:property value="filtroMovimientosDelSaldo.agente.idAgente"/>';
	 var anioMes='<s:property value="filtroMovimientosDelSaldo.anioMes"/>';
	 var tipoAccion='<s:property value="tipoAccion"/>';
	/* var urlFiltro="/MidasWeb/fuerzaventa/ListarMovimientosManuales/listarFiltradoDetalle.action?tipoAccion="+tipoAccion+"&filtroMovimientosDelSaldo.agente.idAgente="+idAgente+"&filtroMovimientosDelSaldo.anioMes="+anioMes;*/
	 var idField='<s:property value="idField"/>';
	buscarMovimientosManuales();	
 });
	
</script>
<s:form action="buscarMovimientosManuales" id="movimientosManualesForm" name="movimientosManualesForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2" >
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="Aplicar Movimientos Manuales"/>
			</td>
		</tr>
		<tr>
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.aplicar.usuario"/>
			</th>	
			<td width="10%">
				<s:textfield id="usuarioId" name="usuario.nombreUsuario" onclick="javascript: seleccionarUsuario(3)" cssClass="cajaTextoM2 w90" ></s:textfield>
			</td>
			<td width="5%">
				<div class="btn_back w20">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: seleccionarUsuario(3)">						
					</a>
				</div>
			</td>
			<td width="70%" colspan="3">
				<s:textfield id="usuarioNombre" name="usuario.NombreCompleto" cssClass="cajaTextoM2 w500" disabled="true" />
			</td> 
		</tr>
		<tr>
			<th width="15%"> 
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.aplicar.inicioPeriodo"/>
			</th>
			<td width="15%" colspan="2">
				<sj:datepicker name="movimientoManual.fechaInicioPeriodo"
					cssStyle="width: 100px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaInicio"
					changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
				</sj:datepicker>
			</td>			
			<th width="15%">
				<s:text name="midas.fuerzaventa.movimientomanual.aplicar.finPeriodo"/>
			</th>
			<td width="20%">
				<sj:datepicker name="movimientoManual.fechaFinPeriodo"
					cssStyle="width: 100px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaFin"
					changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
				</sj:datepicker>
			</td>
			<td width="35%">
				
			</td>
		</tr>
		<tr>
			<th width="15%">
				&nbsp;<s:text name="midas.fuerzaventa.movimientomanual.aplicar.abonosCargos"/>
			</th>	
			<td width="10%">
				<s:textfield id="saldo" name="saldoFinal" value="%{getText('struts.money.format',{saldoFinal})}"  cssClass="cajaTextoM2 w90" disabled="true"></s:textfield>
			</td>
			<td width="5%">
				
			</td>
			<th width="15%">
				<s:text name="midas.fuerzaventa.movimientomanual.aplicar.cifraControl"/>
			</th>
			<td width="10%">
				<s:textfield id="cifraControl" cssClass="cajaTextoM2 w90 jQfullFloat jQrestrict"></s:textfield>				
			</td>
			<td width="45%" align="right" >
				<div class="btn_back w110" id="botonBuscar">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="buscarMovimientosManuales();">
						<s:text name="midas.boton.buscar"/>
					</a>			
				</div>	
			</td>
		</tr>
	</table>	
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="movimientosManualesGrid" width="880px" height="200px" style="background-color:white;overflow:hidden" >
	</div>
</s:form>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<table width="880px">
	<tr>
		<td>
			<div align="left">
				<div class="btn_back w160">
					<a href="javascript: void(0);" 
						onclick="javascript:eliminarMovimientos();">
						Eliminar Movimientos
					</a>
				</div>	
			</div>
		</td>
		<td>
			<div align="right">
				<div class="btn_back w160">
					<a href="javascript: void(0);" class="icon_enviar"
						onclick="javascript:aplicarMovimientos();">
						Aplicar Movimientos
					</a>
				</div>	
			</div>
		</td>
	</tr>
</table>

