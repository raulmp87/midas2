<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<s:include value="/jsp/movimientosManualesAgentes/subramo/subRamoHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>
<script type="text/javascript">	
	
	
	var tipoAccionRamo='<s:property value="tipoAccion"/>';
	var noCerrarModal = '<s:property value="closeModal"/>';
	var varModal='subramoModal';
	var idFieldSubRamo = '<s:property value ="idField"/>';
	if(noCerrarModal=='No'){
		 varModal='';
	}
	
	jQuery(function(){
		listarSubRamos(true);
	 });
	
	function listarSubRamos(cargaInicial){				
		var urlFiltro=listarFiltradoSubRamoPath;
		var urlFil="";
		if(cargaInicial==true){
			urlFil = urlFiltro+"?tipoAccion="+tipoAccionRamo;
		}else{
			urlFil = urlFiltro;
		}				
		
		jQuery("#IdsubRamo").val(jQuery("#txtSubRamoId").val()); // se copia el valor para realizar el filtrado
		listarFiltradoGenerico(urlFil,"subRamoGrid", jQuery("#subRamoForm"),idFieldSubRamo,varModal);
	}	
</script>
<s:form action="listarFiltradoSubRamo" id="subRamoForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<s:hidden name="ramoMovimiento.codigo" id="ramoId"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<!--<s:text name="midas.fuerzaventa.negocio.titulo"/>-->
				Listar SubRamos
			</td>
		</tr>
		<tr>	
			<th>
				<!--<s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text>-->
				Concepto
			</th>	 
			<td>
				<s:textfield  name="filtroSubRamo.codigoSubRamoMovsManuales" id="txtSubRamoId" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield>
				<s:hidden name="filtroSubRamo.codigoSubRamo" id="IdsubRamo"></s:hidden>
			</td>
			<th>
				Descripción
			</th>	 
			<td>
				<s:textfield name="filtroSubRamo.descripcionSubRamo" id="txtSubRamoDescripcion" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>			
		</tr>
		<tr>			
			<td colspan="6">
				<div class="btn_back w110">	
					<a href="javascript: void(0);" class="icon_buscar" id="subRamoBuscar"
						onclick="listarSubRamos(false);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br>	
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="subRamoGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>	
</s:form>
