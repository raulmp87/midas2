<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDFUrl" namespace="/fuerzaventa/ReporteMovimientosManuales"></s:url>';
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte  de Movimientos Manuales</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/ReporteMovimientosManuales/"
	id="exportarToPDF">
<table class="contenedorFormas">
	<tr>
		<th> 
			<s:text name="midas.fuerzaventa.movimientomanual.reporte.fechaInicio"/>
		</th>
		<td>
			<sj:datepicker name="fechaInicio"
				cssStyle="width: 170px;" required="#requiredField"
				buttonImage="../img/b_calendario.gif" id="fechaInicio"
				maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
				cssClass="txtfield jQrequired" size="12"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>
		</td>
		<th>
			<s:text name="midas.fuerzaventa.movimientomanual.reporte.fechaFin"/>
		</th>
		<td>
			<sj:datepicker name="fechaFin"
				cssStyle="width: 170px;" required="#requiredField"
				buttonImage="../img/b_calendario.gif" id="fechaFin"
				maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
				cssClass="txtfield jQrequired" size="12"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>
		</td>
	</tr>
</table>
</s:form>
<div class="row">
	<div class="c2" style="float: right;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class=""
				onclick="exportTo('pdf');"> <s:text
					name="Generar" /> </a>
		</div>
	</div>
</div>