<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- structura -->
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar_start.js'/>"></script>
<!-- utils -->
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>

<!-- main script -->	
<script src="<s:url value='/js/midas2/endoso/previo/previoEmisionPendiente.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div hrefmode="ajax-html" style="height: 450px; width: 920px" id="emisionPendienteTabbar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">
	<div width="100%" name="Autos" id="emisionPendienteAutos" href="<s:url action="mostrarPrevioCancelacion" namespace="/endoso/previo"/>"></div>
	<div width="100%" name="Da&ntilde;os" id="emisionPendienteDanio" href="<s:url action="mostrarPrevioCancelacionDanios" namespace="/endoso/previo"/>"></div>	   
</div>

<script type="text/javascript">
	dhx_init_tabbars();
</script>