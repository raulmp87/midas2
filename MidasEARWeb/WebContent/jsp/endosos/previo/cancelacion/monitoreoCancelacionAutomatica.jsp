<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<head>
    <meta http-equiv="refresh" content="60,url=http:/MidasWeb/endoso/previo/monitorearProcesoCancelacionAutomatica.action"/>
</head>
<div class="titulo" style="width: 98%;">
</div>
<div id="contenedorFiltros">	
	<table id="agregar" class="tablaConResultados" width="100%">			
        <tr>
            <th>Total Registros</th>
            <th>Procesados exitosamente</th>
            <th>Procesados fallidos</th>
            <th>Por Procesar</th>
        </tr>
        <tr>
            <td><s:text name="cifras.get('TOTAL_REGISTROS')"></s:text></td>
            <td><s:text name="cifras.get('PROCESADO_EXITO')"></s:text></td>
            <td><s:text name="cifras.get('PROCESADO_FALLO')"></s:text></td>
            <td><s:text name="cifras.get('SIN_PROCESAR')"></s:text></td>
        </tr>						
	</table>
</div>
