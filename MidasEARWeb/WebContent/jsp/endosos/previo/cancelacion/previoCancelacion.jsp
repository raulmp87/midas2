<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<style type="text/css">
table#agregar {
    margin-top: 10px;
    margin-left: 0px;  
    margin-right: 0px;
}
</style>

<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
.divInputText{
    width: 180px;
    max-width:180px;
    height:14px;
    max-height:14px;
    background-color:#FFF;
    text-align:left;
    cursor:text;
	-moz-user-select: -moz-none;
   	-khtml-user-select: none;
	-webkit-user-select: none;
   /*
     Introduced in IE 10.
     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
   */
   	-ms-user-select: none;
   	user-select: none;
}
</style>


<script type="text/javascript">
	var buscarCancelacionesPendientesPath = '<s:url action="listarCancelaciones" namespace="/endoso/previo"/>';
	var buscarCancelacionesPendientesPaginadoPath = '<s:url action="listarCancelacionesPaginado" namespace="/endoso/previo"/>';
	var actualizarBloqueoPath = '<s:url action="actualizarBloqueo" namespace="/endoso/previo"/>';	
	var emitirPendientePath = '<s:url action="emitirPendiente" namespace="/endoso/previo"/>';   
	var excelCancelacionesPendientesPath = '<s:url action="getExcelCancelacionesPendientes" namespace="/endoso/previo"/>';
	var aplicarAccionSeleccionadasPath = '<s:url action="aplicarAccionSeleccionadas" namespace="/endoso/previo"/>';
	var iniciarCancelacionAutomaticaPath = '<s:url action="iniciarCancelacionAutomatica" namespace="/endoso/previo"/>';
	var monitorearCancelacionAutomaticaPath = '<s:url action="monitorearProcesoCancelacionAutomatica" namespace="/endoso/previo"/>';
	var buscarAgentePath = '<s:url action="buscarAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var polizasAutPath = '<s:url action="iniciarPolizasAutomaticos" namespace="/endoso/previo"/>';
	var endososAutPath = '<s:url action="iniciarEndososAutomaticos" namespace="/endoso/previo"/>';
</script>

<div class="titulo" style="width: 98%;"><s:text name="midas.endoso.previo.emisionpendiente.cancelacion.autos.titulo"  /></div>
<div align="left" class="contenedorFiltros">
	<s:form id="previoEmisionPendienteForm" >
	<table id="agregar" border="0" cellspacing="1%" cellpadding="1%">
        <tr>                      
            <td>
                <font color="#FF6600">*</font>
                <s:text name="midas.endoso.previo.emisionpendiente.cancelacion.cancelablesaldia"></s:text>:
            </td>
            <td style="padding-left:0px;">                
                <sj:datepicker     name="fechaValidacion"
	                                   required="true" cssStyle="w150" 
						   			   buttonImage="../img/b_calendario.gif"
						               id="fechaValidacion" maxlength="10" 	
						               labelposition="left"					               
						               cssClass="txtfield"
						               onkeypress="return soloFecha(this, event, false);"
						               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						               onblur="esFechaValida(this);"					              
						               changeMonth="true" 
						               changeYear="true"							   								  
						               >
					</sj:datepicker>   
            </td>           
            <td>
                <s:text name="midas.poliza.numeroPoliza"></s:text>:
            </td>
            <td><s:textfield cssClass="txtfield w150 jQnumeric jQrestrict" 															
						maxlength="12"						
						id="filtroNumeroPoliza" name="filtros.numeroPoliza" />
			</td> 
		</tr>
		<tr>   
		    <td>
                <s:text name="midas.poliza.numeroPolizaSeycos"></s:text>:
            </td> 
            <td>
                <s:textfield cssClass="txtfield w150 jQnumeric jQrestrict" 						
				labelposition="top" 											
				maxlength="10"						
				id="filtroNumeroPolizaSeycos" name="filtros.numeroPolizaSeycos" />
			</td>
			<td>
                <s:text name="midas.poliza.nombreAgente"></s:text>:
            </td>    
            <td>
	            <s:hidden id="idAgentePol" name="polizaDTO.cotizacionDTO.solicitudDTO.codigoAgente"/>
						<div><label for="agenteNombre"><s:text name="midas.poliza.nombreAgente"></s:text></label></div>
						<br/>
						<div style="vertical-align: bottom;" >
						<div id="agenteNombre" class="divInputText txtfield" style="width: 180px; float: left; " onclick="seleccionarAgentePoliza(1);">
				 		</div>
				 		<div style="float: left; vertical-align: bottom;">
			 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripción" 
				 			style="margin-left: 5px;"
				 			onclick ="cleanInputDiv('agenteNombre');cleanInput('idAgentePol');"
				 		/>	
				 		</div>
				 		</div>                    		           			       
		   </td>                      
        </tr>       
        <tr>                    
           <td>
               <s:text name="midas.poliza.nombrecontratante"></s:text>:
           </td>
           <td style="padding-left:0px;">
               <s:textfield  cssClass="txtfield w150 jQalphaextra jQrestrict"
								 maxlength="60"
								 labelposition="top" 
								 id="filtroNombreContratante" name="filtros.nombreContratante" />		
           </td>           
           <td>
                <s:text name="Tipo Emision"></s:text>:
           </td>
           <td style="padding-left:0px;">
               <s:select list="tiposEmisiones" listValue="value" listKey="key" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="filtros.tipoEmision" id="tiposEmisiones"
					cssClass="txtfield" />		
           </td>             	                
        </tr>                              
        <tr>
                                                           
            <td align="left" colspan="4" >
	                <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: left;" title="Regenar documentos cancelables">
									<a href="javascript: void(0);" onclick="iniciaPrevioEmisionPendienteCancelacionValidacion();">
										Regenerar y Buscar </a>
					    </div>
		             </div>	
		             <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: left;">
									<a href="javascript: void(0);" onclick="iniciaPrevioEnEmsionPendiente();">
										Buscar</a>
					    </div>
		             </div>	
		             <s:if test="!jobRunning">
		             	<div id="divBuscarBtn" class="w200" style="float:left;">
							<div class="btn_back w200" style="display: inline; float: left;">
									<a href="javascript: void(0);" onclick="arrancarCancelacionAutomatica();">
										Iniciar Cancelación Automática</a>
					    	</div>
		             	</div>						   
					</s:if>	
					<s:else>
						<div id="divBuscarBtn" class="w210" style="float:left;">
							<div class="btn_back w210" style="display: inline; float: left;">
									<a href="javascript: void(0);" onclick="mostrarVentanaMonitoreo();">
										Monitorear Cancelación Automática</a>
					    	</div>
		             	</div>						  
					</s:else>			             
		        </td> 
        </tr>
    </table>
	</s:form>
</div>

<div id="spacer1" style="height: 10px"></div>

<div id="tablaEmisionesPendientes" class="detalle">
    <!-- Tabla   -->
    <div id="indicador"></div>
	<div id="gridEmisionesPendientesPaginado">
		<div id="emisionesPendientesListadoGrid" style="width:98%;height:300px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>

<div id="spacer1" style="height: 15px"></div>
<div id="botones" class="detalle"  style="width: 98%;" align="left">
    <table align="left">
        <tr>            
            <td>
                <div id="divBloquearBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" title="Bloquear Seleccionadas" class="icon_rechazar" onclick="aplicarAccionSeleccionadas(2, 1);">	
									<s:text name="Bloquear"/>	
								</a>
		                    </div>
                </div>
            </td>
            <td>
                <div id="divDesbloquearBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" title="Desbloquear Seleccionadas" class="icon_desbloquear" onclick="aplicarAccionSeleccionadas(2, 0);">	
									<s:text name="Desbloquear"/>	
								</a>
		                    </div>
                </div>
            </td>                                            
            <td>
                <div id="divCancelarBtn" style="float:right;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" title="Cancelar Seleccionadas" class="icon_agregar" onclick="aplicarAccionSeleccionadas(1, -1);">	
									<s:text name="Emitir"/>	
								</a>
		                    </div>
                </div>
            </td>
            <td width="32%"></td>
            <td>                    
               <div id="divExcelBtn"  class="w150" style="float:left;">
	               <div class="btn_back w140" style="display: inline; float: right;">
		               <a href="javascript: void(0);" onclick="exportarExcelCancelaciones();">
		                   <s:text name="midas.boton.exportarExcel" /> </a>
	               </div>
               </div>                   
            </td>                               
        </tr>    
    </table>    
</div>	
<div id="spacer1" style="height: 35px"></div>
	                   		                  

<script type="text/javascript">
	parent.iniciaPrevioEmisionPendienteCancelacion();
</script>
