<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
	var generarCancelacionesPendientesDaniosPath = '<s:url action="generarPrevioCancelacionDanios" namespace="/endoso/previo"/>';
</script>
<div class="titulo" style="width: 98%;"><s:text name="midas.endoso.previo.emisionpendiente.cancelacion.danios.titulo"  /></div>
<div align="left" class="contenedorFiltros">
	<s:form id="previoEmisionPendienteDaniosForm" >
		<table id="agregar" border="0" cellspacing="1%" cellpadding="1%">
	        <tr>                      
	            <td width="20%">
	                <font color="#FF6600">*</font>
	                <s:text name="midas.calculos.fechaCorte"></s:text>:
	            </td>
	            <td style="padding-left:0px;" width="35%">                
	                <sj:datepicker name="fechaCorteDanios"
		                           required="true" cssStyle="w150" 
							   	   buttonImage="../img/b_calendario.gif"
					               id="fechaCorteDanios" maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true">
					</sj:datepicker>   
	            </td>     
	            <td width="45%">&nbsp;</td>      
			</tr>
			<tr>	
				<td colspan="3">				        	
					<div class="btn_back w140" style="display: inline; float: left;" title="Regenar documentos cancelables">
						<a href="javascript: void(0);" onclick="generarReporteEndososCancelables();">
						Generar Reporte</a>
				 	</div>
				 </td>				 
			</tr>
		</table>
	</s:form>
</div>