<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="selected" type="ch" width="30" sort="int" >#master_checkbox</column>
		<column id="id" type="ro" width="0px" sort="int" hidden="true">id</column>		
		<column id="numeroPoliza" type="ro" width="100" sort="int" align="center"><s:text name="midas.poliza.renovacionmasiva.numPoliza" /></column>
		<column id="numeroPoliza" type="ro" width="100" sort="int" align="center"><s:text name="midas.emision.nopolizaseycos" /></column>
		<column id="numeroEndoso"  type="ro" width="60" sort="int" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" /></column>
		<column id="numeroInciso"  type="ro" width="50" sort="int" align="center"><s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></column>		
		<column id="contratante" type="ro" width="200" sort="str"><s:text name="midas.poliza.nombrecontratante"/></column>
		<column id="agente" type="ro" width="*" sort="str"><s:text name="midas.negocio.agente"/></column>
		<column id="tipoEmision" type="ro" width="60" sort="str"><s:text name="Tipo Emision"/></column>
		<column id="fechaVencimiento" type="ro" width="100" sort="int" align="center"><s:text name="midas.negocio.fechaVencimiento" /></column>
		<column id="primaTotal" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.endoso.previo.emisionpendiente.cancelacion.totalcancelar" /></column>
		<column id="bloqueo" type="img" width="30" align="center"></column>
		<column id="emitir" type="img" width="30" align="center"></column>				
	</head>	  		
	<s:iterator value="emisionesPendientes" status="stats">
		<row id="<s:property value="id"/>">
		    <cell><s:property value="checked" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clavePolizaSeycos" escapeHtml="false"/></cell>
			<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				 <s:if test="tipoEmision == 7">
				 NA
				 </s:if>
				 <s:else>
				 	<s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/>
				 </s:else>
			</cell>
			<cell><s:property value="nombreContratante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
			    <s:if test="tipoEmision == 7">
					    <s:property value="'Poliza'"/>
				</s:if>
				<s:if test="tipoEmision == 9">
					    <s:property value="'Endoso'"/>
				</s:if>
			</cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorPrimaTotal" escapeHtml="false" escapeXml="true" /></cell>			
			<s:if test="bloqueoEmision== 1">
				<cell>/MidasWeb/img/icons/ico_terminar.gif^Desbloquear Emision^javascript: desbloquearEmision(<s:property value="id"/>)^_self</cell>
			</s:if>	
			<s:else>
			    <cell>/MidasWeb/img/icons/ico_rechazar2.gif^Bloquear Emision^javascript: bloquearEmision(<s:property value="id"/>)^_self</cell>
			</s:else>
			<s:if test="estatusRegistro == 3">
				<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Error al Emitir^javascript: emitirPendiente(<s:property value="id"/>)^_self</cell>			
			</s:if>		
			<s:else>
				<cell>/MidasWeb/img/icons/ico_agregar.gif^Cancelar^javascript: emitirPendiente(<s:property value="id"/>)^_self</cell>
			</s:else>
		</row>
	</s:iterator>	

</rows>
