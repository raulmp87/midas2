<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/complementar.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/poliza/auto/poliza.js"/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/cotizacionEndoso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript">
	var buscarSolicitudPath = '<s:url action="buscarSolicitud"/>';
	var busquedaRapidaPath = '<s:url action="busquedaRapida"/>';
	var opcionCapturaPath = '<s:url action="mostrar" namespace="/suscripcion/solicitud"/>';
	var solicitudesPaginadasPath = '<s:url action="busquedaPaginada"/>';
	var solicitudesPaginadasRapidaPath = '<s:url action="busquedaRapidaPaginada"/>';
    var cambiarAgenteEndosoPath = '<s:url action="mostrarCambioAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var cambiarFormaPagoEndosoPath = '<s:url action="mostrarCambioFormaPago" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var cancelacionPolizaEndosoPath = '<s:url action="mostrarCancelacionPoliza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var rehabilitacionPolizaEndosoPath = '<s:url action="mostrarRehabilitacionPoliza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var definirTipoModificacionPath =  '<s:url action="mostrarDefTipoModificacion" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
    var cancelarCotizacionPath = '<s:url action="cancelarCotizacion" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
	var emitirBajaIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
	var emitirMovimientosPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var emitirCambioDatosPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos"/>';
	var emitirCambioAgentePath =  '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var emitirCambioFormaPagoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"/>';
	var emitirEndosoCancelacionEndosoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';
	var emitirEndosoCancelacionPolizaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
	var emitirExtensionVigenciaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';
	var emitirInclusionAnexoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo"/>';
	var emitirInclusionTextoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto"/>';
	var emitirRehabilitacionIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionInciso"/>';
	var emitirRehabilitacionEndosoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';
	var emitirRehabilitacionPolizaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';
	var emitirAltaIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
</script>