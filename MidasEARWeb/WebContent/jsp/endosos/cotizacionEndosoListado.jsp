<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/endosos/cotizacionEndosoListadoHeader.jsp"></s:include>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.cotizacionEndosoListado.titulo"/></div>
<div id="detalle" >
	<center>	
		<s:form action="busquedaPaginada" id="cotizacionEndosoForm">		
			<div id="contenedorFiltros" >		
				<table id="agregar" border="0" width="100%" cellspacing="0px" cellpadding="0px">
				<tr>
				    <td>
				        <table cellspacing="0px" cellpadding="0px"  style="border: #000000;
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt;"> 
				        <tr>
						    <td>					
								<s:textfield key="midas.endosos.cotizacionEndosoListado.numeroPoliza" cssStyle="width: 220px;"
											 cssClass="txtfield jQnumeric jQrestrict" labelposition="top" 
											 size="10" 
						                     maxlength="8"
											 id="numeroPoliza" name="filtrosBusqueda.numeroPoliza"/>					
						    </td>
						    <td>
							    <s:textfield key="midas.endosos.cotizacionEndosoListado.nombreAsegurado" cssStyle="width: 220px;"
									         cssClass="txtfield jQalphaextra jQrestrict" labelposition="top" size="12"
										     id="nombreAsegurado" name="filtrosBusqueda.nombreAsegurado" />
						    </td>																							
					    </tr>			   
					    <tr>
						    <td>
							<s:textfield key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud" cssStyle="width: 220px;"
								cssClass="txtfield jQnumeric jQrestrict"
								labelposition="top" 
								size="12"
								maxlength="12"
							    id="numeroCotizacion" name="filtrosBusqueda.solicitudId" />	
	
						    </td>
						    <td>
							<s:textfield key="midas.endosos.cotizacionEndosoListado.numeroSerieVehiculo" cssStyle="width: 220px;"
								cssClass="txtfield  jQalphaextra jQrestrict"
								labelposition="top" 
								size="12"
								maxlength="17"
							    id="numeroSerieVehiculo" name="filtrosBusqueda.numeroSerieVehiculo" />							             					
						
						    </td>										
					    </tr>
					    <tr id="tipoEndoso">
					        <td>
					            <s:select key="midas.endosos.cotizacionEndosoListado.tipoEndoso" 
		 							      name="filtrosBusqueda.idTipoEndoso" id="tipoEndoso"
		 							      labelposition="top" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="tiposEndoso" listKey="id.idDato" listValue="descripcion" 		   
									      cssClass=" txtfield"/>
					        </td>
					        <td>
					            <s:text name="midas.endosos.cotizacionEndosoListado.conflictoNoSerie" />:
					            <s:checkbox id="conflictoNoSerie" name="filtrosBusqueda.conflictoNumeroSerie" />
					        </td>
					    </tr>
					    <tr id="fechasCotizacion">
					        <td>
					           <sj:datepicker name="filtrosBusqueda.fechaCotizacionDesde" required="#requiredField" cssStyle="width: 200px;"
							   			   buttonImage="../img/b_calendario.gif" key="midas.endosos.cotizacionEndosoListado.fechaCotizacionDesde"
							               id="fechaCotizacionInicio" maxlength="10" cssClass="txtfield"	
							               labelposition="top" 
							               size="12"
							               maxDate="today"
							               changeMonth="true"
							               changeYear="true"							   								  
							               onkeypress="return soloFecha(this, event, false);"
							               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							               onblur="esFechaValida(this);"></sj:datepicker>					    
					        </td>
					        <td>
					       
						              <sj:datepicker name="filtrosBusqueda.fechaCotizacionHasta" required="#requiredField" cssStyle="width: 200px;"
							    		   buttonImage="../img/b_calendario.gif" key="midas.endosos.cotizacionEndosoListado.fechaCotizacionHasta"
							    		   labelposition="top" 
							    		   size="12"
							    		   maxDate="today"
							               changeMonth="true"
							               changeYear="true"
							    		   id="fechaCotizacionFin" maxlength="10" cssClass="txtfield"								   								  
							    		   onkeypress="return soloFecha(this, event, false);"
							    		   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							    		   onblur="esFechaValida(this);"></sj:datepicker>
						           					    	        
					        </td>					
					     </tr>
					     </table>  
				  </td>				
				<td align="left">
				        <table id="definir" align="left" class="contenedorFormas" style="
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt;" border="0" >
				            <tr>			            
				                <td style="color: black" colspan="2">
				                <s:text name="midas.endosos.cotizacionEndosoListado.tituloTablaCheckBoxes" />
				                </td>				                				             
				            </tr>
				            <tr><td><s:text name="midas.endosos.cotizacionEndosoListado.enProceso" />:</td><td><s:checkbox  name="filtrosBusqueda.busquedaEstatusEnProceso" id="checkEnProceso"/></td></tr>
				            <tr><td><s:text name="midas.endosos.cotizacionEndosoListado.emitidas" />:</td><td><s:checkbox name="filtrosBusqueda.busquedaEstatusEmitidas" id="checkEmitidas"/></td></tr>				            
				        </table>				        
				    </td>
				</tr>						
				</table>
			</div>	
			<table>	
				<tr>
					<td align="right">
						<div id="divFiltrosBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" id="mostrarFiltros"
									onclick="displayFilterSolicitud()">Ocultar Filtros</a>
							</div>
						</div>
                     	<div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="limpiar();" class="icon_limpiar" >	
									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar"/>	
								</a>
		                    </div>
                      	</div>			
	                      	<div id="divBuscarBtn" class="w150" style="float:left;">
								<div class="btn_back w140"  >
									<a href="javascript: void(0);" onclick="pageGridPaginadoBtnBuscar(1, true);" class="icon_buscar">	
										<s:text name="midas.boton.buscar"/>	
									</a>
	                      		</div>
	                     	</div>																			
					</td>
				</tr>
			</table>
		</s:form>
	 	<div id="indicador"></div>
			<div id="gridSolicitudesPaginado" >
				<div id="cotizacionEndosoListadoGrid" style="width:98%;height:130px"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</div>	         
	</center>
	
	<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Nueva_Solicitud, FN_M2_Emision_Cotizacion_Listar_Nueva_Solicitud">
    <div id="divLimpiarBtn" style="float:right;" class="w200" >
							<div class="btn_back w200" >
								<a href="javascript: void(0);" onclick="verNuevaSolicitudEndoso();">	
									<s:text name="midas.endosos.cotizacionEndosoListado.boton.nuevaSolicitudEndoso"/>	
								</a>
		                    </div>
                      	</div>	
    </m:tienePermiso>
</div>
<s:if test="esRetorno==1">
<script type="text/javascript">
	pageGridPaginado(1, true);
</script>
</s:if>
<s:else>
<script type="text/javascript">
	iniciaSolicitudPoliza();
</script>
</s:else>

  