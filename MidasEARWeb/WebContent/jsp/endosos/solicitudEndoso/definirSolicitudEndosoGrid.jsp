<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>


<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="movimiento" type="ro" width="100" sort="int" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.movimiento" /></column>
		<column id="numeroMovimiento"  type="ro" width="120" sort="int" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroMovimiento" /></column>
		<column id="tipoEndoso" type="ro" width="140" sort="int" align="center" ><s:text name="midas.endosos.cotizacionEndosoListado.tipoEndoso" /></column>				 		
		<column id="numeroInciso" type="ro" width="100" sort="int" align="center" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso" /></column>
		<column id="fechaInicioEndoso" type="ro" width="165" sort="int" align="center" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.fechaInicioEndoso" /></column> <!-- Cambiar a Fecha Inicio Vigencia -->
		<column id="fechaEmision" type="ro" width="140" sort="int" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.fechaEmision" /></column>
		<column id="codigoUsuarioCreacion" type="ro" width="140" sort="str" align="center"><s:text name="midas.suscripcion.endoso.auto.usuarioCreacion" /></column>
		<column id="primaEndoso" type="ron" width="140" sort="int" format="$0,000.00" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.primaEndoso" /></column>
		<column id="estatus" type="ro" width="100" sort="int" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.estatus" /></column>
		<column id="consultar" type="img" width="30" align="center"></column>
		<column id="editar" type="img" width="30" align="center"></column>
		<column id="emitir" type="img" width="30" align="center"></column>
		<column id="cancelar" type="img" width="30" align="center"></column>				
	</head>	  		
	<s:iterator value="listaCotizacionesEndoso" status="stats">
		<row id="<s:property value='%{stats.index}'/>">
			<cell>
			    <s:if test="estatusCotizacionEndoso == 10 || estatusCotizacionEndoso == 12 ">
					COTIZACION
				</s:if>
				<s:if test="estatusCotizacionEndoso == 16">
					ENDOSO
				</s:if>	
			</cell>		
			<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
			    <s:if test="idTipoEndoso== 5">
					ALTA DE INCISO
				</s:if>
				<s:if test="idTipoEndoso== 6">
					BAJA DE INCISO
				</s:if>
				<s:if test="idTipoEndoso== 7">
					CAMBIO DE DATOS
				</s:if>
				<s:if test="idTipoEndoso== 8">
					CAMBIO DE AGENTE
				</s:if>
				<s:if test="idTipoEndoso== 9">
					CAMBIO DE FORMA PAGO
				</s:if>
				<s:if test="idTipoEndoso== 10">
					CANCELACION DE ENDOSO
				</s:if>
				<s:if test="idTipoEndoso== 11">
					CANCELACION DE POLIZA
				</s:if>
				<s:if test="idTipoEndoso== 12">
					EXTENSION DE VIGENCIA
				</s:if>
				<s:if test="idTipoEndoso== 13">
					INCLUSION DE ANEXO
				</s:if>
				<s:if test="idTipoEndoso== 14">
					INCLUSION DE TEXTO
				</s:if>
				<s:if test="idTipoEndoso== 15">
					DE MOVIMIENTOS
				</s:if>
				<s:if test="idTipoEndoso== 16">
					REHABILITACION DE INCISOS
				</s:if>
				<s:if test="idTipoEndoso== 17">
					REHABILITACION DE ENDOSO DE CANCELACION DE ENDOSO
				</s:if>
				<s:if test="idTipoEndoso== 18">
					REHABILITACION DE POLIZA
				</s:if>
				<s:if test="idTipoEndoso== 19">
					AJUSTE DE PRIMA
				</s:if>
				<s:if test="idTipoEndoso== 20">
					EXCLUSION DE TEXTO
				</s:if>
				<s:if test="idTipoEndoso== 21">
					CAMBIO DE IVA
				</s:if>		
				<s:if test="idTipoEndoso== 22">
					INCLUSION CONDICIONES ESPECIALES
				</s:if>					
				<s:if test="idTipoEndoso== 23">
					BAJA CONDICIONES ESPECIALES
				</s:if>		
				<s:if test="idTipoEndoso== 24">
					BAJA DE INCISO PERDIDA TOTAL
				</s:if>	
				<s:if test="idTipoEndoso== 25">
					CANCELACION DE POLIZA PERDIDA TOTAL
				</s:if>	
				<s:if test="idTipoEndoso== 26">
					DESAGRUPACION DE RECIBOS
				</s:if>
				<s:if test="idTipoEndoso== 27">
					CAMBIO DE CONDUCTO DE COBRO
				</s:if>		
			</cell>
			<cell>
			    <s:if test="not empty numeroInciso">
					<s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/>
				</s:if>
				<s:else>
					-
				</s:else>
			</cell>
			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaEmisionEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="primaEndoso" escapeHtml="false" escapeXml="true" /></cell>
			<cell>			   
				<s:if test="estatusCotizacionEndoso== 10">
					COT EN PROCESO
				</s:if>	
				<s:if test="estatusCotizacionEndoso== 12">
					COT TERMINADA
				</s:if>			
				<s:if test="estatusCotizacionEndoso== 16">
					COT EMITIDA
				</s:if>								
			</cell>			
			<s:if test="estatusCotizacionEndoso== 12">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Consultar">
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^consultarCotizacion^javascript: complementarEmision(<s:property value="idPoliza"/>,<s:property value="idTipoEndoso"/>,<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/>,"<s:text name="fechaInicioVigencia"/>",<s:property value="continuityId"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Editar">
				<cell>/MidasWeb/img/complementar.png^Complementar Emision^javascript: complementarEmision(<s:property value="idPoliza"/>,<s:property value="idTipoEndoso"/>,<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()"/>,"<s:text name="fechaInicioVigencia"/>",<s:property value="continuityId"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Cancelar">
				<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Cancelar^javascript: cancelarCotizacion(<s:property value="idPolizaBusqueda"/>,"<s:property value="numeroPolizaFormateado" escapeHtml="false"/>",<s:property value="continuityId"/>,<s:property value="numeroCotizacionEndoso"/>)^_self</cell>		
				</m:tienePermiso>
			</s:if>	
			<s:elseif test="estatusCotizacionEndoso== 16">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Consultar">
			    <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Poliza^javascript: verDetallePoliza(<s:property value="idPoliza"/>,"<s:text name="fechaCotizacionEndoso"/>", <s:text name="%{fechaCotizacionEndoso.getTime()}"/>, "<s:text name="fechaEmisionEndosoMidas"/>", <s:text name="%{fechaEmisionEndosoMidas.getTime()}"/>, 2, <s:property value="claveTipoEndoso"/>)^_self</cell>
			    </m:tienePermiso>
			    <m:tienePermiso nombre="FN_M2_Emision_Emision_Imprimir_Poliza">
			    <cell>/MidasWeb/img/b_printer.gif^Imprimir Poliza^javascript: mostrarContenedorImpresion(2, <s:property value="idPoliza"/>, -1, -1, "<s:text name="fechaCotizacionEndoso"/>", <s:text name="%{fechaCotizacionEndoso.getTime()}"/>, <s:text name="%{fechaEmisionEndosoMidas.getTime()}"/>, "<s:text name="fechaEmisionEndosoMidas" />", <s:property value="claveTipoEndoso"/>)^_self</cell>
			    </m:tienePermiso>
			    <m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Imprimir">
			    <cell>/MidasWeb/img/b_printer.gif^Imprimir Endoso^javascript: imprimirEndoso(<s:property value="numeroCotizacionEndoso"/>, "<s:text name="fechaEmisionEndosoMidas"/>","<s:text name="%{fechaEmisionEndosoMidas.getTime()}"/>", <s:property value="claveTipoEndoso"/>)^_self</cell>			    
			    </m:tienePermiso>
			</s:elseif>
			<s:elseif test="estatusCotizacionEndoso== 10">
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Consultar">
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Endoso^javascript: consultarCotizacion(<s:property value="idPoliza"/>,<s:property value="idTipoEndoso"/>,<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/>,"<s:text name="fechaInicioVigencia"/>")^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Editar">
                <cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: consultarCotizacion(<s:property value="idPoliza"/>,<s:property value="idTipoEndoso"/>,<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()" />,"<s:text name="fechaInicioVigencia"/>")^_self</cell>
                </m:tienePermiso>
				<s:if test="idTipoEndoso!= 5 && idTipoEndoso!= 15 && idTipoEndoso != 24 && idTipoEndoso != 25 && idTipoEndoso != 26">
					<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Emitir">
					<cell>../img/confirmAll.gif^Emitir Endoso^javascript: emitirEndoso(<s:property value="continuityId"/>,<s:property value="idPoliza"/>, "<s:text name="fechaInicioVigencia"/>", <s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()" />,<s:property value="idTipoEndoso"/>)^_self</cell>
					</m:tienePermiso>
				</s:if>
				<m:tienePermiso nombre="FN_M2_Emision_Cotizacion_Endoso_Cancelar">
                <cell>/MidasWeb/img/icons/ico_rechazar1.gif^Cancelar^javascript: cancelarCotizacion(<s:property value="idPolizaBusqueda"/>,"<s:property value="numeroPolizaFormateado" escapeHtml="false"/>",<s:property value="continuityId"/>,<s:property value="numeroCotizacionEndoso"/>)^_self</cell>						
                </m:tienePermiso>
            </s:elseif>			 
		</row>
	</s:iterator>	

</rows>
