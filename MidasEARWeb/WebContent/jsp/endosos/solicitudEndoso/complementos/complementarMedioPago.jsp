<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/cobranzaInciso/cobranzaInciso.js'/>"></script>
<script type="text/javascript">
	var guardarMedioPagoPath = '<s:url action="actualizarDatosMedioPago" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado"/>';
</script>

<script type="text/javascript">

$(document).ready(function() {
    $("#idNumeroTarjeta").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $("#idCodigoSeguridad").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
	var idMedioPago = jQuery("#idMedioPago_0").val();
 	mostrarMedioPago(idMedioPago);
 	llenadoDatos();
});

	function onChangeDatosAsegurados(inputSelect) {
		switch (inputSelect) {
		case '2':	
			jQuery('#divInput').show();
			jQuery('#divAgregar').hide();
			jQuery('#divGuardar').show();
			break;
		case '3':
			jQuery('#divInput').hide();
			jQuery('#divAgregar').show();
			jQuery('#divGuardar').hide();
			break;
		default:
			jQuery('#divInput').hide();
			jQuery('#divAgregar').hide();
			jQuery('#divGuardar').show();
		}
	}
	function estaVisible(id){
		if(jQuery('#'+id).is(':visible')){
			jQuery('#'+id).hide();
		}else{
			jQuery('#'+id).show();
		}
	}
	
	function datosPago() {
		var idMedioPago = jQuery("#idMedioPago_0").val();
		var conductoTxt = dwr.util.getText('idConductoCobro_0');
				
		if (conductoTxt == 'Agregar nuevo...') {
			mostrarMedioPago(idMedioPago);
			llenadoDatos();
		}else{
			$('.datosPago1').hide();
			$('.datosPago2').hide();
			$('.fieldsDatos1').hide();
			$('.fieldsDatos2').hide();
		}
	}
	
	function mostrarMedioPago(idMedioPago){
		var conductoTxt = dwr.util.getText('idConductoCobro_0');
		if (conductoTxt == 'Agregar nuevo...') {
			if (idMedioPago == 4) {
				$('.datosPago1').show();
				$('.datosPago2').show();
				$('.fieldsDatos1').show();
				$('.fieldsDatos2').show();
				$("#idInstitucionBancaria").val("");
				$("#numeroCuentaClabe").text('Numero de Tarjeta');
				$("#idInstitucionBancaria").attr("disabled", false);
			    $("#idNumeroTarjeta").attr('maxlength','16');
			    
			} else if(idMedioPago == 8) {
				$('.datosPago1').show();
				$('.fieldsDatos1').show();
				$('.datosPago2').hide();
				$('.fieldsDatos2').hide();
				$("#idInstitucionBancaria").val("");
				$("#numeroCuentaClabe").text('Número de CLABE');
				$("#idInstitucionBancaria").attr("disabled", false);
			    $("#idNumeroTarjeta").attr('maxlength','18');
	
			} else if(idMedioPago == 1386){
				$('.datosPago1').show();
				$('.fieldsDatos1').show();
				$('.datosPago2').hide();
				$('.fieldsDatos2').hide();
				$("#idInstitucionBancaria").val("AFIRME");
				$("#numeroCuentaClabe").text('Numero de Cuenta');
				$("#idInstitucionBancaria").attr("disabled", true); 
			    $("#idNumeroTarjeta").attr('maxlength','18');
	
			}else{
				$('.datosPago1').hide();
				$('.datosPago2').hide();
				$('.fieldsDatos1').hide();
				$('.fieldsDatos2').hide();
			}	
		}
	}

	function llenadoDatos(){
		var idMedioPago = jQuery("#idMedioPago").val();
		var idMedioPagoSelect = jQuery("#idMedioPago_0").val();

		if(idMedioPagoSelect != 1386){
			$("#idInstitucionBancaria").val('');
		}
		$("#idNumeroTarjeta").val('');
		$("#idTipoTarjeta").val('');
		$("#idCodigoSeguridad").val('');
		$("#idFechaVencimiento").val('');
				
		if(idMedioPago==idMedioPagoSelect){
			if (idMedioPago == 4) {
				jQuery('#idInstitucionBancaria').val($("#institucionBancaria").val());
				jQuery('#idTipoTarjeta').val($("#tipoTarjeta").val());
				jQuery('#idNumeroTarjeta').val($("#numeroTarjetaClave").val());
				jQuery('#idCodigoSeguridad').val($("#codigoSeguridad").val());
				jQuery('#idFechaVencimiento').val($("#fechaVencimiento").val());
			} else if(idMedioPago == 8||idMedioPago == 1386) {
				jQuery('#idInstitucionBancaria').val($("#institucionBancaria").val());
				jQuery('#idNumeroTarjeta').val($("#numeroTarjetaClave").val());
			} 
		}
	}
	
	function asignarIdCliente(idCliente, idDomCliente) {
		muestraResultadoAgregarCliente(idCliente, idDomCliente);
	}

	function guardarMedioPago() {
		var idMedioPago = jQuery("#idMedioPago_0").val();
		var idConducto = jQuery('#idConductoCobro_0').val();
		var conductoTxt = dwr.util.getText('idConductoCobro_0');
		
		if (idMedioPago === "" || idMedioPago === undefined) {
			parent.mostrarVentanaMensaje('10',
					"Seleccione el Medio de Pago a aplicar.");
		} else if (conductoTxt === "" || conductoTxt === undefined
				|| idConducto === "") {
			parent.mostrarVentanaMensaje('10',
					"Seleccione el Conducto de Cobro a aplicar.");
		} else {
			if(validarInformacionPago(idMedioPago,conductoTxt)){
				var form = 
				"incisoContinuityId="+jQuery('#incisoContinuityId').val()+
				"&validoEn="+jQuery('#validoEn').val()+
				"&origen="+jQuery('#origen').val()+
				"&biCotizacion.value.personaContratanteId="+jQuery('#personaContratante').val()+
				"&biAutoInciso.value.personaAseguradoId="+jQuery('#personaAsegurada').val()+
				"&idClienteCob="+jQuery('#idClienteCob_0').val()+
				"&idMedioPago="+jQuery('#idMedioPago_0').val()+
				"&idConductoCobroCliente="+jQuery('#idConductoCobro_0').val()+
				"&idBancoCobro="+jQuery('#idInstitucionBancaria').val()+
				"&numeroTarjeta="+jQuery('#idNumeroTarjeta').val()+
				"&tipoTarjeta="+jQuery('#idTipoTarjeta').val()+
				"&codigoSeguridad="+jQuery('#idCodigoSeguridad').val()+
				"&fechaVencimiento="+jQuery('#idFechaVencimiento').val()+
				"&conductoCobroNuevo="+conductoTxt;
				var nextFunction = "&nextFunction=closeVentanaMedioPago()";
				var url = guardarMedioPagoPath + '?' + form
						+ nextFunction;
				parent.redirectVentanaModal('ventanaMedioPago', url, null);	
			}
		}
	}
	
	function validarInformacionPago(idMedioPago,conductoTxt) {
		
		if (conductoTxt == 'Agregar nuevo...') {
			var idInstitucionBancaria = jQuery('#idInstitucionBancaria').val();
			var idNumeroTarjeta = jQuery('#idNumeroTarjeta').val();
			var idTipoTarjeta = jQuery('#idTipoTarjeta').val();
			var idCodigoSeguridad = jQuery('#idCodigoSeguridad').val();
			var idFechaVencimiento = jQuery('#idFechaVencimiento').val();
			
			if (idMedioPago == 4) {
				if(idInstitucionBancaria==""||idNumeroTarjeta==""||idTipoTarjeta==""||idCodigoSeguridad==""||idFechaVencimiento==""){
					parent.mostrarVentanaMensaje('10',"No se pueden guardar campos vacios.");
					return false;
				}
			} else if(idMedioPago == 8||idMedioPago == 1386) {
				if(idInstitucionBancaria==""||idNumeroTarjeta==""){
					parent.mostrarVentanaMensaje('10',"No se pueden guardar campos vacios.");
					return false;
				}
			} 	
		}
		
		return true;
	}
</script>
<div id="ventana" style="height:100%; overflow:hidden">
	<s:form id="complementarMedioPagoForm">
	
		<s:hidden id="incisoContinuityId" name="incisoContinuityId"/>
		<s:hidden id="validoEn" name="validoEn"/>	
		<s:hidden id="origen" name="origen"/>
		<s:hidden id="personaContratante" name="biCotizacion.value.personaContratanteId"/>
		<s:hidden id="personaAsegurada" name="biAutoInciso.value.personaAseguradoId"/>
		<s:hidden id="idMedioPago" name="idMedioPago"/>
		<s:hidden id="institucionBancaria" name="biInciso.value.institucionBancaria"/>
		<s:hidden id="tipoTarjeta" name="biInciso.value.tipoTarjeta"/>
		<s:hidden id="numeroTarjetaClave" name="numeroTarjeta"/>
		<s:hidden id="codigoSeguridad" name="codigoSeguridad"/>
		<s:hidden id="fechaVencimiento" name="fechaVencimiento"/>
				
	  	<div id="agregar" style="height:160px; width:395px;">
			<table id="desplegar" border="0" style="background-color: white; padding: 0px; margin: 0px; width: 380px">
				<tr>
					<th>Cliente Cob</th>
					<th>Medio Pago</th>
					<th>Conducto Cobro</th>
				</tr>	
					<tr  class="bg_t2">
						<td>						
						<s:select id="idClienteCob_0"
								list="clientesCobro"
 								name="idClienteCob"  
 								cssStyle="width:100px; max-width: 200px;"
 								cssClass="cajaTexto jQrequired"								
								labelposition="left" 
								headerKey="" headerValue="Seleccione ..."
								 onchange="actualizaConductosCobro(0)"
								 />
						</td>
						<td>						
						<s:select id="idMedioPago_0"
							list="medioPagoDTOs"
							name="idMedioPago" 
							cssStyle="width:100px;max-width: 200px;"
							cssClass="cajaTexto jQrequired"
							listKey="idMedioPago" listValue="descripcion"
							labelposition="left" 
							headerKey="" headerValue="Seleccione ..."
							 onchange="actualizaConductosCobro(0); datosPago()"
							 />
						</td>
						<td>
						<s:select id="idConductoCobro_0"
								list="conductosDeCobro"
 								name="idConductoCobroCliente"  
 								cssStyle="width:100px;max-width: 200px;"
 								cssClass="cajaTexto jQrequired"								
								labelposition="left" 
								headerKey="" headerValue="Seleccione ..."
								onchange="datosPago()"
								 />
						</td>
				</tr>
				<tr class="datosPago1" style="display: none">
					<th>Institucion Bancaria</th>
					<th><label id="numeroCuentaClabe">Numero de Tarjeta</label></th>
				</tr>
				<tr  class="bg_t2 fieldsDatos1" style="display: none">
					<td>
						<s:select id="idInstitucionBancaria"
							list="bancosCobro"
							name="idBancoCobro" 
							cssStyle="width:100px;max-width: 200px;"
							cssClass="cajaTexto jQrequired"
							listKey="nombreBanco" listValue="nombreBanco"
							labelposition="left" 
							headerKey="" headerValue="Seleccione ..."
							 />
					</td>
					<td>
						<s:textfield id="idNumeroTarjeta" 
							name="numeroTarjeta" 
							labelposition="left" 
							maxlength="18"/>
					</td>
				</tr>
				<tr class="datosPago2" style="display: none">
					<th>Tipo de Tarjeta</th>
					<th>Codigo de Seguridad</th>
					<th>Fecha de Vencimiento</th>
				</tr>
				<tr  class="bg_t2 fieldsDatos2" style="display: none">
					<td>
							
						<s:select  id="idTipoTarjeta"
							name="tipoTarjeta"
							list="#{'VISA':'VISA','MASTERCARD':'Master Card'}" 
							headerKey="" cssClass="cajaTexto jQrequired" 
							cssStyle="width:100px;max-width: 200px;"
							headerValue="Seleccione" 
							labelposition="left"></s:select>	
					</td>
					<td>
						<s:textfield id="idCodigoSeguridad" 
							name="codigoSeguridad"
							labelposition="left" 
							maxlength="3"
							cssStyle="width:50px"
							/>
					</td>
					<td>
						<s:textfield id="idFechaVencimiento" 
							name="fechaVencimiento"
							labelposition="left" 
							maxlength="5"
							placeholder="mm/yy"
							cssStyle="width:50px"
							/>
					</td>
				</tr>
			</table>
		</div>
		<div style="width:410px;" >
		<div id="divSalirBtn" class="btn_back w100" style="display:inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="parent.cerrarVentanaModal('ventanaMedioPago');"> <s:text
					name="midas.boton.salir" /> </a>
		</div>		
		<div id="divGuardar" style="display: block; float:right;">
			<div class="btn_back w140"  style="display:inline; float: left; ">
			   <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea guardar el medio de pago?')){guardarMedioPago();}"> 
				  <s:text name="midas.boton.guardar" /> </a>
			</div>				
		</div>
		</div>
		<div id="indicador"></div>
		<div id="central_indicator" class="sh2" style="display: none;">
		<img id="img_indicator" name="img_indicator"
			src="/MidasWeb/img/as2.gif" alt="Afirme" />
	  </div>
  </s:form>
</div>