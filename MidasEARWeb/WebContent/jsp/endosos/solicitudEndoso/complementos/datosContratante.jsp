<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/datosContratante.js'/>"></script>
<script src="<s:url value='/js/midas2/cliente/busquedaCliente.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>

<script type="text/javascript">
	var mostrarDatosContratanteURL = '<s:url action="verDatosContratante" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos"/>';
	var getClientesAsociadosURL = '<s:url action="mostrarClientesAsociados" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos"/>';
	var seleccionarContratanteURL = '<s:url action="seleccionarContratante" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos"/>';
</script>

<s:hidden id="cotizacionContinuityId" name="cotizacionContinuityId"/>
<s:hidden id="idToNegocio" name="idToNegocio"/>
<s:hidden id="idCliente" name="idCliente"/>
<s:hidden id="fechaIniVigenciaEndoso" name="fechaIniVigenciaEndoso"/>
<s:hidden name="accionEndoso" id="accionEndoso"/>
<s:hidden name="negocioAsociado" id="negocioAsociado"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

<div class="subtituloIzquierdaDiv"><s:text name="midas.poliza.datosContratante" /></div>
<s:textfield name="idClienteResponsable" id="idClienteResponsable"
		readonly="true" cssStyle="display:none;"
		onchange="seleccionarCliente(this.value)" />
	<s:textfield name="domidClienteResponsable"
		id="domidClienteResponsable" readonly="true" cssStyle="display:none;" />
<table id="agregar">
	<tr>
		<th width="10%">
			<s:text name="midas.negocio.nombre" /> 
		</th>
		<td width="70%">    
			<s:textfield name="cliente.nombreCliente" readonly="true"
				key="midas.cotizacion.nombrecontratante" size="45"
				cssStyle="max-width:242;left:-10px;"
				labelposition="top" cssClass="txtfield" theme="simple" />
		</td>
		<td>
		    <s:if test="!#soloConsulta">
				<div class="row">
					<s:if test="negocioAsociado">
						<div class="c3 s2">
							<div class="btn_back w170">
								<a href="javascript: void(0);"
									onclick="mostrarClientesAsociados();" class="icon_cliente">
									<s:text name="midas.cotizacion.buscarcliente" /> </a>
							</div>
						</div>
					</s:if>
					<s:else>
						<div class="btn_back w170">
							<a href="javascript: void(0);"
								onclick="buscarClienteNoAgregar(seleccionarCliente);" class="icon_cliente">
								<s:text name="midas.cotizacion.buscarcliente" /> </a>
						</div>
					</s:else>
				</div>
			</s:if>
		</td>  
	</tr>   
</table>
