<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript">
	var guardarCobranzaPath = '<s:url action="guardarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCobranza"/>';
</script>

<script type="text/javascript">
	
	function guardarCobranza() {	
		//var nextFunction = "&nextFunction=closeVentanaAsegurado()";
		var url = guardarCobranzaPath + '?' + jQuery(document.previsualizarCobranzaForm).serialize() 
		//+ nextFunction
		;		
		parent.redirectVentanaModal('Cobranza', url, null);	
	}	
	
	
	function onChangeAfectacionPrimas(inputSelect) {
		switch (inputSelect) {
			case '2':
				jQuery('#divInput').show();
				break;
			default:
				jQuery('#divInput').hide();
	}
	}
	
</script>
<div id="ventana" style="height:100%; overflow:hidden">
	<s:form id="previsualizarCobranzaForm">
	
		<s:hidden id="cotizacionContinuityId" name="cotizacionContinuityId"/>
		<s:hidden id="validoEn" name="validoEn"/>
		<s:hidden id="accionEndoso" name="accionEndoso"/>
		<s:hidden id="tipoEndoso" name="tipoEndoso"/>
		
		
	  	<div id="agregar" style="height:100px; width:380px;">
			<s:radio name="radioCobranza"
				theme="simple"
				list="#{'1':' Generar Notas de Credito <br/>','2':'Abonar a Recibos de Poliza <br/>','3':'Solicitud de Cheque <br/>'}"
				id="radioCobranza" cssStyle="agregar"
				onclick="onChangeAfectacionPrimas(this.value);" />

		
		</div>		
		
		<div id="divInput" style="display:none;">
			<s:textfield cssClass="txtfield" cssStyle="width: 120px;" 
						key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza"
						labelposition="left"
						required="true"  
						size="20"
						onkeypress="return soloNumeros(this, event, false);"
					    onfocus="javascript: new Mask('####-########-##', 'string').attach(this)"
					    maxlength="16"																			
						id="numeroPolizaFormateadoReferencia" name="numeroPolizaFormateadoReferencia" />
		</div>
		
		<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
			<div style="width:410px;" >
			<div id="divGuardar" style="display: block; float:right;">
				<div class="btn_back w140"  style="display:inline; float: left; ">
				   <a href="javascript: void(0);"
					  onclick="guardarCobranza();"> 
					  <s:text name="midas.boton.guardar" /> </a>
				</div>				
			</div>
			</div>
		</s:if>

		
		<div id="indicador"></div>
		<div id="central_indicator" class="sh2" style="display: none;">
		<img id="img_indicator" name="img_indicator"
			src="/MidasWeb/img/as2.gif" alt="Afirme" />
	  </div>
  </s:form>
</div>
<script type="text/javascript">
	onChangeAfectacionPrimas(dwr.util.getValue('radioCobranza'));
</script>