<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
			    <call command="enableDragAndDrop"><param>true</param></call>
			</s:if>
		</beforeInit>		
		<column id="condicionEspecialBitemporalDTO.idCondicionBitemporal" type="ro" width="*" sort="int" hidden="true">idCondicionBitemporal</column>
		<column id="condicionEspecialBitemporalDTO.idContinuity" type="ro" width="*" sort="int" hidden="true">idContinuity</column>
		<column id="condicionEspecialBitemporalDTO.condicionEspecial.id" type="ro" width="*" sort="int" hidden="true">idCondicionEspecial</column>
		<column id="condicionEspecialBitemporalDTO.condicionEspecial.codigo" type="ro" width="80" sort="int"><s:text name="midas.general.codigo"/></column>
		<column id="condicionEspecialBitemporalDTO.condicionEspecial.nombre" type="ro" width="*" sort="str"><s:text name="midas.general.nombre"/></column>

	</head>

	<% int a=0;%>
	<s:iterator value="condicionesAsociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idCondicionBitemporal" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idContinuity" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="condicionEspecial.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="condicionEspecial.codigo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="condicionEspecial.nombre" escapeHtml="false" escapeXml="true" /></cell>
			<userdata name="drag"><s:property value="obligatoria" escapeHtml="false" escapeXml="true" /></userdata>
		</row>
	</s:iterator>
	
</rows>