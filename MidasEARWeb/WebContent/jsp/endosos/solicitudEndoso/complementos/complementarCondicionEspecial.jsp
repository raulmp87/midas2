<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
 
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCondicionEspecial.js'/>"></script>
<script type="text/javascript">
	var buscarCondicionAutPath = '<s:url action="buscarCondicionAutocompletado" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';	
	var buscarCondicionPath = '<s:url action="buscarCondicion" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
	var condicionesAsociadasPath = '<s:url action="obtenerCondicionesAsociadas" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
	var condicionesDisponiblesPath = '<s:url action="obtenerCondicionesDisponibles" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
	var relacionarCondicionesPath = '<s:url action="relacionarCondiciones" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
	var asociarTodasPath = '<s:url action="asociarTodasCondiciones" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
	var eliminarTodasPath = '<s:url action="eliminarTodasCondiciones" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial"/>';
</script>

<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}
.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

</style>
<div id="contenido_condicionesEspeciales">
<s:form id="condicionEspecialForm">
	<div class="titulo"><s:text name="midas.condicionespecial.title"/>	</div>
	<s:hidden id="incisoContinuityId" name="incisoContinuityId"/>
	<s:hidden id="validoEn" name="validoEn"/>
	<s:hidden id="condicionId" name="condicionId"/>
	<s:hidden id="accionEndoso" name="accionEndoso"/>	
	
	
	<table id="agregar" border="0">		
		<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">			
			<tr>			
				<td><label class="label"><s:text name="midas.condicionespecial.condicionEspecial"/></label></td>
			
				<td><s:textfield id="nombreCondicion" name="nombreCondicion" cssClass="cajaTexto" cssStyle="width:250px"/></td>
				<td><div class="btn_back w100">
						<a id="submit" href="javascript: void(0);" onclick="buscarCondiciones();" class="icon_buscar"> <s:text name="midas.boton.buscar" /></a>
					</div>
				</td>
			</tr>
		</s:if>
		<tr>
			<td colspan="2" style="font-size: 10px;font-weight: bold;">
				<s:text name="midas.condicionespecial.condicionDisponible"/>
			</td>
			
			<td/>
			<td  style="font-size: 10px;font-weight: bold;">
				<s:text name="midas.condicionespecial.condicionAsociada"/>
			</td>
		</tr>	
		<tr>
			<td colspan="2" >
				<div id="condicionesDisponiblesGrid" style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
			</td>
			
			<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">				
				<td ><div style="margin-left:5px; width: 4%;   position: relative;" id="divID">
					<button class="btnActionForAll" onclick="eliminarTodas();" type="button"><<</button>
					
					</div>
					<div style="margin-left:5px; width: 4%;   position: relative;" id="divID">
					<button class="btnActionForAll" onclick="asociarTodas();" type="button">>></button>
					</div>
				
				</td>

			</s:if>
			<s:else>
				<td width="20%"/>
			</s:else>
			
			<td >
				<div id="condicionesAsociadasGrid" style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
			</td>
		</tr>
	</table>
</s:form>
</div>

<script type="text/javascript">
	initGrids();

</script>