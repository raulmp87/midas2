<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- ##########################   *******************   ESTILOS -->
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<!-- ##########################   *******************   FIN ESTILOS -->

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp.js'/>"></script>


<s:hidden name="id" id="id"/>
<s:hidden name="tipoRegreso" id="tipoRegreso" />
<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"  />
<s:hidden name="nivelAplicacion" id="nivelAplicacion"  />
<s:hidden name="accionEndoso" id="accionEndoso"  />	
<s:hidden name="cotizacionContinuityId" id="cotizacionContinuityId"/>
<s:hidden name="numeroPolizaFormateado" id="numeroPolizaFormateado"  />
<s:hidden name="listaIncisos" id="listaIncisos" />
<s:hidden name="polizaId" id="polizaId" />
	
<s:form id="cargamasivacotizacionForm">
	
	<div class="titulo"  style="width: 98%;">
		<div style="float:left">
		<s:text name="Poliza:" />
		<b><s:property value="numeroPolizaFormateado"/></b>
		</div>
		<div style="text-align: right;">
		<s:text name="Estatus:" />
		<b><s:property value="estatusCargaMasiva"/></b>
		</div>
	</div>
<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<s:text name="midas.cotizacion.cargamasiva.seleccioneArchivo" />
			</td>
			<td>
				<div class="btn_back w150" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivo(1);"
						claass="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importar" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w220" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargarPlantillaCarga();"
						class="icon_guardar2"> <s:text
							name="midas.cotizacion.cargamasiva.descargarPlantilla" /> </a>
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="leyenda">
	<s:text name="midas.cotizacion.cargamasiva.leyenda" />
</div>
</s:form>
<div id="indicador"></div>
<div id="cargasMasivasGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div  style="width: 98%;text-align: center;">
<div class="btn_back w220" style="display: inline; float: right; ">
	<a href="javascript: void(0);" onclick="regresarAPoliza();"> 
	<s:text name="midas.boton.regresar" /> </a>
</div>
</div>

<script type="text/javascript">
	descargarLogCargaMasiva();
	iniciaListadoMasiva();
</script>