<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspeciales.js'/>"></script>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div>  

<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 7pt;" >
	    <tr>
	        <td><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud"/>:&nbsp;<s:text name="biCotizacion.value.solicitud.numeroSolicitud"/></td>
	        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="biCotizacion.value.numeroEndoso"/></td>	        
	    </tr>
	    <tr height="10px"></tr>	       	
	</table>
</div>

<s:form id="endosoInclusionCondicionEspecialForm" >

<s:hidden id="polizaId" name="polizaId"/>
<s:hidden id="numeroPolizaFormateado" name="polizaDTO.numeroPolizaFormateada"/>
<s:hidden id="fechaIniVigenciaEndoso" name="fechaIniVigenciaEndoso"/>
<s:hidden id="accionEndoso" name="accionEndoso" />
<s:hidden id="idToSolicitud" name="biCotizacion.value.solicitud.idToSolicitud"/>
<s:hidden id="cotizacionContinuityId" name="biCotizacion.continuity.id"/>
<s:hidden id="biCotizacion.continuity.id" name="biCotizacion.continuity.id"/>
<s:hidden id="biCotizacion.value.solicitud.idToSolicitud"  name="biCotizacion.value.solicitud.idToSolicitud"/>
<s:hidden id="actionNameOrigen" name="actionNameOrigen"/>
<s:hidden id="namespaceOrigen" name="namespaceOrigen"/>
<s:hidden id="nivelAplicacion" name="nivelAplicacion"/>
<s:hidden id="tipoRegreso" name="tipoRegreso" value="0"/>
<s:hidden id="tipoEndoso" name="tipoEndoso" />

<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set id="soloConsulta" var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set id="soloConsulta" var="soloConsulta" value="false"/>
</s:else>

   <table style="border: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 7pt;">
      <tr>
          <td valign="top">
            
		        	<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
					labelposition="top"  
					size="10" readonly="true"
					name="fechaIniVigenciaEndoso" disabled="true" />  
			    </td>	
			    
			    <td>
			    	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.leyendaInclusionNivel"/>
			    	<div id="nivelApp">
							<s:radio name="nivelAplicacion" value="0" list="#{'0':'Poliza','1':'Inciso'}"  
							 onclick="deleteInfoPrevia(this.value);"/>							
					</div>
					
					<div class="btn_back w140 btnActionForAll">
						<a class="" onclick="limpiarDatosAsociados();" alt="Asociar todas" href="javascript: void(0);"> 
							<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.leyendaLimpiar"/>
						</a>
				    </div>
					
			    </td>
			    
			    <td colspan="2">
				    <div id="codigosAfectar">
				      <s:textarea onblur="validateIncisosInclusion();" disabled="disabled" id="codigosInciso" label="No. de Incisos a Afectar" name="codigosInciso" cols="32" rows="3"/>
				      
				      
				      <div class="btn_back w140 btnActionForAll">
						<a class="" onclick="loadInfoIncisoInclusion();" alt="Asociar todas" href="javascript: void(0);"> 
							<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.leyendaCargarInformacion"/>
						</a>
				    </div>
				    </div>
				    
				    
				    
			    </td>	
			    
			    <td>
			    	<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onClick="cargaMasiva();" >
						<s:text name="Carga Masiva" /> </a>
					</div>
			    	
			    </td>	    
			    

		  
     
      </tr>
      
		<tr>
			<td class="titulo" colspan="3">
				 <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.titulo"/>
			</td>
		</tr>
			
		<tr>
			<td colspan="4" style="font-size: 11px;">
				<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.leyendaDisponibles"/>
			</td>
			<td width="20%"/>
			<td style="font-size: 11px;">
				<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionCondicionEspecial.leyendaAsociadas"/>
			</td>
		</tr>
		
		<tr>
		
			<td><s:textfield id="nombreCondicion" name="nombreCondicion" cssClass="cajaTexto" cssStyle="width:150px"/></td>
			<td><div class="btn_back w100">
					<a id="submit" href="javascript: void(0);" onclick="buscarCondicionesEspeciales();" class="icon_buscar"> <s:text name="midas.boton.buscar" /></a>
				</div>
			</td>
			<td></td>
		</tr>
		
		<tr>
			<td colspan="4" >
				<div id="indicadorCondicionesDisponibles"></div>
				<div id="condicionesEspecialesDisponiblesGrid" style="width: 420px; height: 240px;"></div>
			</td>
			
	
			<td align="center" valign="middle" width="8%">
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="limpiarDatosAsociados();" alt="Desligar todas" href="javascript: void(0);"> << </a>
							</div>
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="asociarTodas();" alt="Asociar todas" href="javascript: void(0);"> >> </a>
							</div>
		</td >
	
			<td colspan="2" >
				<div id="indicadorCondicionesAsociadas"></div>
				<div id="condicionesEspecialesAsociadasGrid" style="width: 420px; height: 240px;float:right;"></div>
			</td>
		</tr>

			<tr>
				<td colspan="4">
				   <table style="font-size: 9px;">
				   <tr><td>--<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionAnexo.arrastrar.mensaje" />--</td></tr>
				   </table>				
				</td>
			</tr>
      
      <tr>
		    <td align="left" colspan="10">
		    <s:if test="accionEndoso!=@mx.com.afirme.midas2.dto.TipoAccionDTO@getAltaIncisoEndosoCot()">
			    <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>
	              <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
		             <div id="divEmitirBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
					    </div>
		             </div>	
	              </s:if>
	              <s:if test="!#soloConsulta">
		              <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="cotizar();">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" /> </a>
					    </div>
		              </div>	 
	             </s:if> 
    		</s:if>														
		    </td>
	  </tr>	  
   </table>		
</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">
	initGridsInclusionCondicionEspecial();
	validateNivelSeleccted( jQuery('#nivelAplicacion').val());
</script>
