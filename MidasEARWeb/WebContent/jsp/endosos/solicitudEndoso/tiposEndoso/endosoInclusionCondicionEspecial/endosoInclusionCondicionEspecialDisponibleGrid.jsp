<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="condicionEspecialBitemporalDTO.idCondicionBitemporal" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="condicionEspecialBitemporalDTO.condicionEspecial.id" type="ro" width="0" sort="int" hidden="true">Id</column>
		<column id="condicionEspecialBitemporalDTO.idContinuity" type="ro" width="0" sort="int" hidden="true">id</column>
		
		<column id="condicionEspecialBitemporalDTO.tipoCondicionStr" type="ro"  width="80">Tipo Condicion</column>
		<column id="condicionEspecialBitemporalDTO.condicionEspecial.codigo" type="ro"  width="60"  sort="str" >Codigo</column>
	    <column id="condicionEspecialBitemporalDTO.condicionEspecial.nombre" type="ro"  width="*"  sort="str">Nombre</column>
		
		
	</head>
	<% int a=0;%>
	<s:iterator value="condicionesDisponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idCondicionBitemporal" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="condicionEspecial.id" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idContinuity" escapeHtml="false" escapeXml="true"/></cell>
		    
		    <cell><s:property value="tipoCondicionStr" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="condicionEspecial.codigo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="condicionEspecial.nombre" escapeHtml="false" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>
</rows>