<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCambioFormaPago.js'/>"></script>

<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"/>';	
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>

<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioFormaPago.titulo"  />&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000;
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt;" >
	    <tr>
	        <td><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud"/>:&nbsp;<s:text name="cotizacion.value.solicitud.numeroSolicitud" /></td>
	        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></td>	        
	    </tr>
	    <tr height="15px"></tr>	       	
	</table>
</div>
<s:form action="cotizar" id="endosoCambioFormaPagoForm">
	<s:hidden name="polizaId"/>
	<s:hidden name="cotizacion.value.fechaInicioVigencia" value="%{fechaIniVigenciaEndoso}"/>
	<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
	<s:hidden name="cotizacion.value.negocioDerechoEndosoId"/>
	<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso"></s:hidden>
	<s:hidden name="cotizacion.continuity.id"/>
	<s:hidden name="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen"/>
	<s:hidden name="accionEndoso" id="accionEndoso"/>
	<s:hidden name="cotizacion.value.moneda.idTcMoneda" id="idMoneda"/>

	
   <table>
      <tr>
          <td>
              <table id="agregar"  style="border: #000000;" width="98%">
		    <tr>
		        <td>
		        	<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
					labelposition="top"  
					size="10" readonly="true"
					name="fechaIniVigenciaEndoso" disabled="true"/>  
			    </td>			
		    </tr>
		    <tr id="tipoEndoso">
			    <td>
			        <s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.endosos.solicitudEndoso.tiposEndoso.cambioFormaPago.formaPagoVigente"
					labelposition="top"  
					size="10" readonly="true"
					name="formaPagoVigente" disabled="true"/>    
			    </td>					        
			</tr>
			<tr id="tipoEndoso">
			    <td>
			        <s:select key="midas.endosos.solicitudEndoso.tiposEndoso.cambioFormaPago.cambioFormaPagoA" 
							  name="cotizacion.value.formaPago.idFormaPago" id="cotizacion.value.formaPago.idFormaPago"
							  labelposition="top" 
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  list="formasdePago" 		   
						      cssClass=" txtfield"
						      onchange="cargarRecargoPagoFraccionadoECFP(this.value, jQuery('#idMoneda').val());"
						      disabled="#soloConsulta"/>   
						       				        
			    </td>		
				<td>
					<s:if test="mostrarPctRecargoPagoFraccionado" >
					<s:textfield name="cotizacion.value.porcentajePagoFraccionado" required="#requiredField"
				               key="midas.cotizacion.porcentajePagoFraccionado"
					           labelposition="top" disabled="#soloConsulta"
							   id="porcentajePagoFraccionado" maxlength="5" cssClass="txtfield" cssStyle="width: 200px;" 						   								  
							   onkeypress="return soloNumeros(this, event, true)">
					</s:textfield>
					</s:if>	
					<s:else>
					<s:hidden id="porcentajePagoFraccionado" name="cotizacion.value.porcentajePagoFraccionado" />
					</s:else>				
				</td>		        
			</tr>							
									   
	    </table>
          </td>
		<td align="right">
			<div>
				<div id="cargaResumenTotales"></div>
				<div id="resumenTotalesCotizacionGrid">
					<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
				</div>
			</div>
		</td>

		</tr>
      <tr height="50px">
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td>
          </td>
		    <td align="left">
			    <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>	
	             <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
		             <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
					    </div>
		             </div>	
	             </s:if>
	             <s:if test="!#soloConsulta">
		             <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="TipoAccionDTO.getEditarEndosoCot(cotizarEndosoCFP);" >	
							    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
						    </a>
		                </div>
		             </div>	
	             </s:if>																		
		    </td>
	  </tr>	  
   </table>		
</s:form>
