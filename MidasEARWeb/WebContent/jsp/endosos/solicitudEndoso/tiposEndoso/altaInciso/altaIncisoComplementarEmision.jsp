<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/programapago/componenteProgramaPago.js'/>"></script>
<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/endosoAltaIncisoHeader.jsp"></s:include>

<s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_ALTA_INCISO">
	<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado.js'/>"></script>
</s:if>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>
 
<div class="titulo" style="width: 98%;">
	<s:if test="tipoEndoso == 5" >
		<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.altaIncisoComplementarEmision"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)
	</s:if>
	<s:elseif test="tipoEndoso == 15">
		<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.movimientos.movimientosComplementarEmision"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)
	</s:elseif>
	<s:else>
		<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.default.defaultComplementarEmision"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)
	</s:else>
</div> 

<div id="spacer1" style="height: 10px"></div>
<div align="center">
<s:form action="mostrarAltaInciso" id="altaIncisoForm" cssClass="" >


<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
<s:hidden name="actionNameOrigen"/>
<s:hidden name="namespaceOrigen"/>
<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>

<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

 <table width="98%" style="padding: 0px; margin: 0px; border: none;" class="fixTabla">
		<tr>
			<td>
				<div id ="datosContratante" style="height: 150px; float:both;" class="agregar">
					<s:action name="verDatosContratante" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos" ignoreContextParams="true" executeResult="true" >
						<s:param name="cotizacionContinuityId"><s:property value="cotizacion.continuity.id"/></s:param>
						<s:param name="fechaIniVigenciaEndoso"><s:property value="fechaIniVigenciaEndoso"/></s:param>
						<s:param name="accionEndoso"><s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/></s:param>
					</s:action>	
				</div>
			</td> 	
		</tr>
	</table> 
   <table width="98%">
      <tr align="left">
          <td valign="bottom">
           <div class="row titulo">
				<div id="incisosCompletos" style="float: both;">
					<s:if test="mensajeDTO.mensaje != null">
						<s:property value="mensajeDTO.mensaje" />
					</s:if>
				</div>
			</div>
          </td>
          <td align="right">               
              <div>                              
                  <div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div> 
              </div>              
          </td>
      </tr>        
   </table>		
</s:form>	
</div>
<s:form id="cotizacionForm">
    <s:hidden name="cotizacion.idToCotizacion" value="%{cotizacion.continuity.numero}"/>
    <s:hidden name="cotizacion.continuity.id"/>
</s:form>
<div align="center" style="width: 100%;">                                                                   
	<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
		<s:param name="idToPolizaName">polizaId</s:param>		
		<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
		<s:param name="idAccionEndosoName">accionEndoso</s:param>	
		<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO}"></s:param>
		<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
	</s:action>
</div> 
<div id="spacer2" style="height: 40px"></div>

<s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_ALTA_INCISO">
	
	<s:hidden name="idToControlArchivo" />
	
	<div class="filtros"style="width: 90%;text-align: left; height: 60px">
		
		<div class="subtituloIzquierdaDiv" style="width: 98%;">
			<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.complemento.masivo.titulo"/>
		</div>
		
		<div id="b_reporteXLS" style="display: inline; float: left; width:27%; margin: 0px 5px;">
			<a href="javascript: void(0);" onclick="javascript: descargarPlantillaComplementarios();"> <s:text
				name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.complemento.masivo.descargar" /> </a>
		</div>
		
		<midas:boton onclick="javascript: procesarPlantillaComplementarios();"  tipo="agregar" key="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.complemento.masivo.procesar" 
		style="width:30%"/>
	
	</div>
</s:if>

<div align="right">
    <table>
        <tr>             
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="cancelar();">	
									<s:text name="midas.boton.salir"/>	
								</a>
		                    </div>
                </div>
            </td>
            
            
 			<!-- Muestra Movimientos del endoso -->
            <s:if test="mensajeDTO.visible">   
                <s:if test="!#soloConsulta"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarMovimientosEndoso(<s:property value="cotizacion.continuity.id"/>);">	
											<s:text name="midas.suscripcion.endoso.auto.mostrarMovimientos"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          	</s:if>
             </s:if>
            <td>
	            <div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onClick="cargaMasiva();" >
						<s:text name="Carga Masiva" /> </a>
				</div>
            </td>
            <s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_ALTA_INCISO">
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="regresarAProceso();">	
									<s:text name="midas.boton.regresar"/>	
								</a>
		                    </div>
                </div>
            </td> 
            
            </s:if>
            
            <s:if test="mensajeDTO.visible">   
                <s:if test="!#soloConsulta">
	                <td>		
	                	<div id="previoRecivos">	
		             	   <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarProgramaPagoEndoso(<s:property value="cotizacion.value.solicitud.idToSolicitud"/>,<s:property value="cotizacion.continuity.id"/>,<s:property value="polizaId"/>, 'E');">	
											Previo de Recibos	
										</a>
				                    </div>
		                	</div>
		                </div>
		            </td> 
                </s:if>	            
            </s:if>
            
            <s:if test="mensajeDTO.visible">   
                <s:if test="!#soloConsulta">
	                <td>			
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="emitir();">	
											<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir"/>	
										</a>
				                    </div>
		                </div>
		            </td> 
                </s:if>	            
            </s:if>                 
        </tr>    
    </table>
</div>
<script type="text/javascript">
console.log("Validando estatus de recuotificacionn");
validaEstatusRecuotificacion();
console.log("Fin de validacion");
</script>