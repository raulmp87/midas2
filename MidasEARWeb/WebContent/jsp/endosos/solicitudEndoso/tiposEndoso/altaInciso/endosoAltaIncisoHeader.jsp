<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/definirSolicitudEndoso.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/cotizacionEndoso.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>


<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/componente/programapago/programaPago.js'/>"></script>


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoAltaInciso.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoAltaIncisoCargaMsva.js'/>"></script>
<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';	
    var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
    var terminarActionPath = '<s:url action="terminar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
    var regresarProcesoActionPath = '<s:url action="regresarAProceso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
    var verDetalleTipoEndosoOrigenPath = '<s:url action="verDetalleTipoEndoso" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
    var descargarPlantillaComplementariosPath = '<s:url action="descargarPlantillaComplementarios" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
    var procesarPlantillaComplementariosPath = '<s:url action="procesarPlantillaComplementarios" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
    
    var mostrarAltaIncisoCargaMsvaPath = '<s:url action="mostrarCargaMasivaAltaInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
//	var previsualizarCobranzaActionPath = '<s:url action="previsualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
//	var busquedaRapidaPath = '<s:url action="busquedaRapida" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
//	var solicitudesPaginadasRapidaPath = '<s:url action="busquedaRapidaPaginada" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
//	var definirTipoModificacionPath =  '<s:url action="mostrarAltaInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>'; 
</script>