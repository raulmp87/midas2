<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/endosoAltaIncisoHeader.jsp"></s:include>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.b_submit:disabled{
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;	
}
</style>
 
<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: right;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></div>
<div id="spacer1" style="height: 10px"></div>
<div align="center">
<s:form action="mostrarAltaInciso" id="altaIncisoForm" cssClass="" >
<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="accionEndoso" id="accionEndoso"/>
<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>

<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

   <table width="98%">
      <tr align="left">
          <td valign="top">
              <table id="agregar"  style="border: #000000;" width="98%">
				    <tr>		     
					        <td>
					            <s:textfield cssClass="txtfield" 
								    cssStyle="width: 80px;"
								    key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
								    labelposition="top"  
								    size="10" 
								    readonly="true"
								    name="fechaIniVigenciaEndoso" 
								    disabled="true"/>    
					        </td>     			
				    </tr>
				    <tr>
						<td>
							 <s:select key="midas.cotizacion.derechos"  
							 		labelposition="top"
							 		id="derechos"  cssClass="txtfield" 
									style="width: 100px"
							     	list="derechosEndosoList"
									name="cotizacion.value.negocioDerechoEndosoId"
									headerKey=""
									disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
									headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
													
						</td>
					</tr>								   
			  </table>
          </td>
          <td align="right">               
              <div>                              
                  <div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div> 
              </div>              
          </td>
      </tr>        
   </table>		
</s:form>	
</div>
<s:form id="cotizacionForm">
    <s:hidden name="cotizacion.idToCotizacion" value="%{cotizacion.continuity.numero}"/>
</s:form>
<div align="center" style="width: 100%;">                                                                   
	<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
		<s:param name="idToPolizaName">polizaId</s:param>		
		<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
		<s:param name="idAccionEndosoName">accionEndoso</s:param>	
		<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO}"></s:param>
		<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
	</s:action>
</div> 
<div id="spacer2" style="height: 40px"></div>
<div align="right">
    <table>
        <tr>             
            <td>                               
                <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();">	
						    <s:text name="midas.boton.cancelar"/>	
						</a>
		            </div>
                </div>
                
            </td>
              <td>                               
                <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="mostrarAltaIncisoCargaMsva();">	
						    <s:text name="midas.cotizacion.cargamasiva"/>	
						</a>
		            </div>
                </div>
                
            </td>          
		          <!-- Muestra Movimientos del endoso -->
	             	<s:if test="!#soloConsulta"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarMovimientosEndoso(<s:property value="cotizacion.continuity.id"/>);">	
											<s:text name="midas.suscripcion.endoso.auto.mostrarMovimientos"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          	</s:if>
		                      
            <td>
                <s:if test="!#soloConsulta">
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
						<div class="btn_back w140" >
							<a href="javascript: void(0);" onclick="cotizar();">	
								<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
							</a>
			            </div>
	                </div>
                </s:if>
            </td>
            <td>
               <!--  <div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="terminar();">	
									<s:text name="midas.boton.terminar"/>	
								</a>
		                    </div>
                </div> -->
                <s:if test="!#soloConsulta">
	                <div class="btn_back w140" >
				        <s:submit key="midas.boton.terminar" 
				                  id="botonTerminar" 			                 
				                  name="botonTerminar"
				                  onclick="terminar(); return false;"
						          cssClass="b_submit w140"/>						    
				   </div>
			   </s:if>
            </td>                  
        </tr>    
    </table>
</div>