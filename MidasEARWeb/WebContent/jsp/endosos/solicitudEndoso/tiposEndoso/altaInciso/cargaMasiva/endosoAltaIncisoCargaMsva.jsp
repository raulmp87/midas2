<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/endosoAltaIncisoHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:form id="endosoAltaIncisoCargaMsvaForm">
	<s:hidden name="polizaId" id ="polizaId"/>
	<s:hidden name="accionEndoso" id="accionEndoso"/>
	<s:hidden name="cotizacion.value.solicitud.idToSolicitud" id="cotizacion.value.solicitud.idToSolicitud"/>
	<s:hidden name="cotizacion.value.negocioDerechoEndosoId" id="cotizacion.value.negocioDerechoEndosoId"/>
	<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
	<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
	<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
	<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
	<s:hidden name="tipoEndoso" id="tipoEndoso"/>
	<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
	<div class="titulo"  style="width: 98%;">
		<div style="float:left">
		<s:text name="midas.cotizacion.no" />
		<b><s:property value="cotizacion.continuity.id"/></b>
		</div>
		<div style="text-align: right;">
		<s:text name="midas.cotizacion.estatus" />:
		<s:property value="fechaIniVigenciaEndoso" />
		</div>
	</div>
<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<s:text name="midas.cotizacion.cargamasiva.seleccioneArchivo" />
			</td>
			<td>
				<div class="btn_back w150" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="importarArchivoEndosoAI(1);"
						class="icon_enviar"> <s:text
							name="midas.cotizacion.cargamasiva.importar" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w220" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargarPlantillaCargaEndosoAI();"
						class="icon_guardar2"> <s:text
							name="midas.cotizacion.cargamasiva.descargarPlantilla" /> </a>
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="leyenda">
	<s:text name="midas.cotizacion.cargamasiva.leyenda" />
</div>
</s:form>
<div id="indicador"></div>
<div id="cargasMasivaAltaIncisoGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div  style="width: 98%;text-align: center;">
<div class="btn_back w220" style="display: inline; float: right; ">
	<a href="javascript: void(0);" onclick="regresarACotizacionEndosoAI(<s:property value='tipoRegreso' />);"> 
	<s:text name="midas.boton.regresar" /> </a>
</div>
</div>

<script type="text/javascript">
	descargarLogCargaMasivaEndosoAI();
	iniciaListadoEndosoCargaMsva();
</script>