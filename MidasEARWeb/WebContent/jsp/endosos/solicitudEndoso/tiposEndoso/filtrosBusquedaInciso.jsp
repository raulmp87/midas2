<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>


<s:form id="filtrosBusquedaInciso" cssClass="">
<div>
        <table id="agregar" class="" width="96%">
            <tr>
                <td>
                   <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso"></s:text>:
                </td>
                <td>
                    <s:textfield cssClass="txtfield  jQnumeric jQrestrict" cssStyle="width: 50px;"							
							labelposition="left"  
							size="10"
							maxlength="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="numSolicitudBusqueda" />
                </td>
                <td>
                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.descVehiculo"></s:text>:
                </td>
                <td>
                    <s:textfield cssClass="txtfield jQrestrict" cssStyle="width: 130px;"							
							labelposition="left"  
							size="10"
							maxlength="10"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="numSolicitudBusqueda" />
                </td>                
                <s:if test="tipoOperacion == 1">
                    <td>
                        <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.lineaNegocio"></s:text>:
                    </td>
                    <td>
                        <s:textfield cssClass="txtfield jQrestrict" cssStyle="width: 100px;"									
									 labelposition="left"  
									 size="10"
									 maxlength="10"
									 onblur="this.value=jQuery.trim(this.value)"
									 id="idToCotizacion" name="numSolicitudBusqueda" />
                    </td>                        
                </s:if>
                <s:else>
                    <td>
                        <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie"></s:text>:
                    </td>
                    <td>
                        <s:textfield cssClass="txtfield jQalphanumeric jQrestrict" cssStyle="width: 100px;"							
									 labelposition="left"  
									 size="10"
									 maxlength="10"
									 onblur="this.value=jQuery.trim(this.value)"
									 id="idToCotizacion" name="numeroSerie" />
                    </td>                        
                </s:else>               
                <s:if test="tipoOperacion == 1">
                        <td>
                            <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.paquete"></s:text>:
                        </td>
                        <td>
                            <s:textfield cssClass="txtfield  jQrestrict" cssStyle="width: 100px;"							
										 labelposition="left"  
										 size="10"
										 maxlength="10"
										 onblur="this.value=jQuery.trim(this.value)"
										 id="idToCotizacion" name="numSolicitudBusqueda" />
                        </td>                        
               </s:if>
               <s:else>
                        <td>
                            <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor"></s:text>:
                        </td>
                        <td>
                            <s:textfield cssClass="txtfield jQalphanumeric jQrestrict" cssStyle="width: 100px;"							
										 labelposition="left"  
										 size="10"
										 maxlength="10"
										 onblur="this.value=jQuery.trim(this.value)"
										 id="idToCotizacion" name="numMotor" />
                        </td>                        
                </s:else>               
            </tr>
            <s:if test="tipoOperacion == 2">
                <tr>
                    <td>
                        <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.placas"></s:text>:
                    </td>
	                <td>
	                    <s:textfield cssClass="txtfield jQalphanumeric jQrestrict" cssStyle="width: 50px;"								
									 labelposition="left"  
									 size="10"
									 maxlength="10"
									 onblur="this.value=jQuery.trim(this.value)"
									 id="idToCotizacion" name="placas" />
	                </td>
	                <td>
	                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.conductor"></s:text>:
	                </td>
	                <td>
	                    <s:textfield cssClass="txtfield jQrestrict" cssStyle="width: 130px;"								
									 labelposition="left"  
									 size="10"
									 maxlength="10"
									 onblur="this.value=jQuery.trim(this.value)"
									 id="idToCotizacion" name="nombreConductor" />
	                </td>
	                <td>
	                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado"></s:text>:
	                </td>
	                <td>	                                    
                        <s:textfield cssClass="txtfield  jQrestrict" cssStyle="width: 130px;"								
									 labelposition="left"  
									 size="10"
									 maxlength="10"
									 onblur="this.value=jQuery.trim(this.value)"
									 id="idToCotizacion" name="nombreAsegurado" />                                      
	                </td>
	                <td>                                        
	                </td>
                </tr>
            </s:if>                        
            <tr>
                <td colspan="5"></td>                                               
                <td colspan="3" align="right">
                    <div id="divBuscarBtn" class="w150" style="float:right;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="emitirEndoso();">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar" /> </a>
					    </div>
                  </div>                
                    <div id="divBuscarBtn" class="w150" style="float:right;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="emitirEndoso();">
										<s:text name="midas.boton.buscar" /> </a>
					    </div>
                  </div>
                </td>
            </tr>
        </table>
      </div>
    </s:form>


