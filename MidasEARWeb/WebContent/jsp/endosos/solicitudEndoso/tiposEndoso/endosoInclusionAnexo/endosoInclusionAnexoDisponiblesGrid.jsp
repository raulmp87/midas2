<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
			    <call command="enableDragAndDrop"><param>true</param></call>
			</s:if>
		</beforeInit>		
		<column id="biDocAnexoCot.continuity.id" type="ro" width="*" sort="int" hidden="true">continuity.id</column>
		<column id="biDocAnexoCot.value.nivel" type="ro" width="*" sort="str"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionAnexo.nivel"/></column>
		<column id="biDocAnexoCot.value.descripcionDocumentoAnexo" type="ro" width="*" sort="str"><s:text name="midas.negocio.producto.tipoPoliza.descripcion"/></column>
		<column id="biDocAnexoCot.value.claveObligatoriedad" type="ro" width="*" sort="int" hidden="true">value.claveObligatoriedad</column>
		<column id="biDocAnexoCot.value.orden" type="ro" width="*" sort="int" hidden="true">value.orden</column>
		<column id="biDocAnexoCot.value.claveTipo" type="ro" width="*" sort="int" hidden="true">value.claveTipo</column>
		<column id="biDocAnexoCot.value.coberturaId" type="ro" width="*" sort="int" hidden="true">value.coberturaId</column>
		<column id="biDocAnexoCot.continuity.controlArchivoId" type="ro" width="*"  hidden="true">continuity.controlArchivoId</column>
	</head>

	<% int a=0;%>
	<s:iterator value="biDocAnexosDisponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="continuity.id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="value.nivel" escapeHtml="false"  escapeXml="true"/></cell>
			<cell><s:property value="value.descripcionDocumentoAnexo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.claveObligatoriedad" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="value.orden" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="value.claveTipo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="value.coberturaId" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="continuity.controlArchivoId" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>