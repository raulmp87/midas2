<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="biTexAdicionalCot.continuity.id" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
		<column id="biTexAdicionalCot.value.descripcionTexto" type="ro" width="300" sort="str" ><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional"/></column>
		<column id="biTexAdicionalCot.value.nombreUsuarioCreacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.solicitadoPor"/></column>
		<column id="biTexAdicionalCot.value.codigoUsuarioAutorizacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizadoPor"/></column>
		<column id="biTexAdicionalCot.value.descripcionEstatusAut" type="ro" width="*" sort="str"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionTexto.estatusSolicitud"/></column>
		<column id="biTexAdicionalCot.value.fechaCreacion" type="ro" width="*" sort="date_custom"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.fecha"/></column>
		
	</head>

	<s:iterator value="listaTextosAdicionales">
		<row id="<s:property value="continuity.id" escapeHtml="false" escapeXml="true" />">
			<cell>0</cell>
			<cell><![CDATA[<s:property value="value.descripcionTexto"  escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><s:property value="value.nombreUsuarioCreacion"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.codigoUsuarioAutorizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.descripcionEstatusAut" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="value.fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>