<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/plugins/jquery.formatCurrency-1.4.0.js'/>"></script>	

<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">		
    <s:set id="soloConsulta" value="1" />		
</s:if>
<s:elseif test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
	 <s:set id="soloConsulta" value="2" />	
</s:elseif>
<s:else>
	<s:set id="soloConsulta" value="0" />
</s:else>

<s:if test="#soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="disabledLNConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:elseif test="#soloConsulta == 2">
	<s:set var="disabledConsulta">false</s:set>
	<s:set var="disabledLNConsulta">true</s:set>		
	<s:set var="showOnConsulta">both</s:set>
</s:elseif>
<s:else>
	<s:set var="disabledConsulta">false</s:set>
	<s:set var="disabledLNConsulta">false</s:set>		
	<s:set var="showOnConsulta">both</s:set>
</s:else>

<table id=t_riesgo border="0">
<tr>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.claveObligatoriedad" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.descripcion" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.sumaAsegurada" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.porcDeducible" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.primaNeta" /></th>
</tr>
	<s:iterator value="bitemporalCoberturaSeccionList" status="stat">	
	<s:if test="((value.claveContrato == 1 && #soloConsulta == 1) || #soloConsulta == 2 || #soloConsulta == 0)">	
	<tr>
		<td class ="txt_v2">
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].continuity.coberturaDTO.idToCobertura" value="%{continuity.coberturaDTO.idToCobertura}" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].continuity.id" value="%{continuity.id}" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveObligatoriedad" value="%{value.claveObligatoriedad}" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.subRamoId" value="%{value.subRamoId}" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorCoaseguro" value="%{value.valorCoaseguro}" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorCuota" value="0" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.porcentajeCoaseguro" value="0" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveAutCoaseguro" value="0" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveAutDeducible" value="0" />
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.numeroAgrupacion" value="0" />
			<s:if test="value.claveContrato==1">
				 <s:set name="readOnly" value="false"/>
			</s:if>
			<s:else>
				 <s:set name="readOnly" value="true"/>
			</s:else>
			<s:if test="value.claveContrato==1 && value.claveObligatoriedad==0" >
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveContratoBoolean" value="true"/>
				<input type="checkbox" id="claveContratoBoolean_%{#stat.index}"  name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveContrato" checked="checked" disabled/>
			</s:if>			
			<s:else>
				<s:checkbox id="claveContratoBoolean_%{#stat.index}" name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveContratoBoolean" value="%{value.claveContrato}" fieldValue="true" onchange="ocultaGuardar();habilitaRegistroCobertura(%{#stat.index});"/>			
			</s:else>								
		</td>
		<td class ="txt_v2"><s:property value="continuity.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true" /></td>
		<td class ="txt_v2">
			<s:if test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 0">
				<s:if test="value.valorSumaAseguradaMax != 0.0 && value.valorSumaAseguradaMin!= 0.0">
					<s:if test="continuity.coberturaDTO.idToCobertura==2650||continuity.coberturaDTO.idToCobertura==3020||
					continuity.coberturaDTO.idToCobertura==2860||continuity.coberturaDTO.idToCobertura==4821">
						<s:property  value="%{descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
						<s:select id="idValDiasSalario"
							name="bitemporalCoberturaSeccionList[%{#stat.index}].value.diasSalarioMinimo"
						  	headerKey="-1"
						  	headerValue="%{getText('midas.general.seleccione')}"
						  	list="%{diasSalario}"
						  	onchange="validaDisable(this, %{#stat.index});"
						  	cssClass="txtfield"
						/>		
						<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" id="valorSumaAseguradaStr_%{#stat.index}" value="%{getText(value.valorSumaAsegurada)}"  />	
			 		</s:if>
			 		<s:else>
						<div style="font-size:8px;">
							*Entre <s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true" /> y <s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />
						</div>
						<s:textfield id ="valorSumaAseguradaStr_%{#stat.index}" name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{getText('struts.money.format',{value.valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});" onblur="validaRangoSumaAsegurada(this,%{value.valorSumaAseguradaMin},%{value.valorSumaAseguradaMax},%{value.valorSumaAsegurada},%{#stat.index});" readonly="#readOnly" disabled="%{#disabledConsulta}"/>
						<s:hidden id="valorSumaAseguradaMax_%{#stat.index}" value = "%{getText('struts.money.format',{value.valorSumaAseguradaMax})}"/>
						<s:hidden id="valorSumaAseguradaMin_%{#stat.index}" value = "%{getText('struts.money.format',{value.valorSumaAseguradaMin})}"/>
					</s:else>
				</s:if>
				<s:else>
					<s:textfield id="valorSumaAseguradaStr_%{#stat.index}" name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{getText('struts.money.format',{value.valorSumaAsegurada})}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});"  onblur="aplicaRedondeo(%{#stat.index})" readonly="#readOnly" disabled="%{#disabledConsulta}"/>
				</s:else>
			</s:if>

			<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 1">
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{value.valorSumaAsegurada}"  />
				<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
			</s:elseif>
			<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 2">
				<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
				<s:textfield id ="valorSumaAseguradaStr_%{#stat.index}" name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{value.valorSumaAsegurada}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false);" onkeyup="ocultaGuardar(); aplicaFormatoKeyUp(%{#stat.index});" onblur="aplicaRedondeo(%{#stat.index})" readonly="#readOnly" disabled="%{#disabledConsulta}"/>
			</s:elseif>
			<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 9">
				<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{value.valorSumaAsegurada}"  />
			</s:elseif>
			<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 3">
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorSumaAseguradaStr" value="%{value.valorSumaAsegurada}"  />
				<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
			</s:elseif>
		</td>
		<td class ="txt_v2">				
			<s:property  value="%{value.descripcionDeducible}" escapeHtml="false" escapeXml="true"/>	
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.claveTipoDeducible" 
					 value="%{continuity.coberturaDTO.claveTipoDeducible}">					 
			</s:hidden>						
			<s:if test='continuity.coberturaDTO.claveTipoDeducible == "0"'>
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.porcentajeDeducible" value="0" />
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorDeducible" value="0"  />
			</s:if>
			<s:elseif test='continuity.coberturaDTO.claveTipoDeducible == "1"'>
				<s:select id="porcentajeDeducible_%{#stat.index}"
					list="%{value.deducibles}"
					disabled="%{#disabledConsulta}"
					name="bitemporalCoberturaSeccionList[%{#stat.index}].value.porcentajeDeducible" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					onchange="validaDisable(this, %{#stat.index});"
					listKey="valorDeducible" listValue="valorDeducible"
					cssClass="txtfield"
					 />	
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorDeducible" value="0"  />
			</s:elseif>
			<s:else>
				<s:select id="valorDeducible_%{#stat.index}"
					list="%{value.deducibles}"
					disabled="%{#disabledConsulta}"
					name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorDeducible" headerKey="-1"
					headerValue="%{getText('midas.general.seleccione')}"
					onchange="validaDisable(this, %{#stat.index});"
					listKey="valorDeducible" listValue="valorDeducible"
					cssClass="txtfield"
					 />	
				<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.porcentajeDeducible" value="0"  />
			</s:else>		
		</td>
		<td class ="txt_v2">
			<s:hidden name="bitemporalCoberturaSeccionList[%{#stat.index}].value.valorPrimaNeta"  value="%{value.valorPrimaNeta}"/>
			<s:property value="%{getText('struts.money.format',{value.valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/>
		</td>
		</tr>   
		</s:if>	
	</s:iterator>	
</table>