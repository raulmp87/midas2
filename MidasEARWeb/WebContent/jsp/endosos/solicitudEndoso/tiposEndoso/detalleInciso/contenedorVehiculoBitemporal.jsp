<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/contenedorVehiculoBitemporalHeader.jsp"></s:include>
<s:div id="dhtmlxWindowMainContent">

<s:form id="incisoForm"  action="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/calcularInciso.action" name="incisoForm" acceptcharset="ISO-8859-1">
<s:hidden id="bitemporalCotizacion.continuity.id" name="bitemporalCotizacion.continuity.id" />
<s:hidden id="bitemporalCotizacion.continuity.numero" name="bitemporalCotizacion.continuity.numero" />
<s:hidden id="bitemporalInciso.continuity.id" name="bitemporalInciso.continuity.id" />
<s:hidden id="bitemporalAutoInciso.continuity.id" name="bitemporalAutoInciso.continuity.id" />
<s:hidden id="bitemporalAutoInciso.value.asociadaCotizacion" name="bitemporalAutoInciso.value.asociadaCotizacion" />
<s:hidden id="bitemporalAutoInciso.value.nombreAsegurado" name="bitemporalAutoInciso.value.nombreAsegurado" />
<s:hidden id="bitemporalAutoInciso.value.numeroMotor" name="bitemporalAutoInciso.value.numeroMotor" />
<%-- <s:hidden id="bitemporalAutoInciso.value.numeroSerie" name="bitemporalAutoInciso.value.numeroSerie" /> --%>
<s:hidden id="bitemporalAutoInciso.value.paquete.id" name="bitemporalAutoInciso.value.paquete.id" />
<s:hidden id="bitemporalAutoInciso.value.paquete.descripcion" name="bitemporalAutoInciso.value.paquete.descripcion" />
<s:hidden id="bitemporalAutoInciso.value.personaAseguradoId" name="bitemporalAutoInciso.value.personaAseguradoId" />
<s:hidden id="bitemporalAutoInciso.value.placa" name="bitemporalAutoInciso.value.placa" />
<s:hidden id="bitemporalAutoInciso.value.repuve" name="bitemporalAutoInciso.value.repuve" />
<s:hidden id="bitemporalAutoInciso.value.fechaNacConductor" name="bitemporalAutoInciso.value.fechaNacConductor" />
<s:hidden id="bitemporalAutoInciso.value.maternoConductor" name="bitemporalAutoInciso.value.maternoConductor" />
<s:hidden id="bitemporalAutoInciso.value.nombreConductor" name="bitemporalAutoInciso.value.nombreConductor" />
<s:hidden id="bitemporalAutoInciso.value.numeroLicencia" name="bitemporalAutoInciso.value.numeroLicencia" />
<s:hidden id="bitemporalAutoInciso.value.ocupacionConductor" name="bitemporalAutoInciso.value.ocupacionConductor" />
<s:hidden id="bitemporalAutoInciso.value.paternoConductor" name="bitemporalAutoInciso.value.paternoConductor" />
<s:hidden id="bitemporalAutoInciso.value.rutaCirculacion" name="bitemporalAutoInciso.value.rutaCirculacion" />

<s:hidden id="validoEn" name="validoEn"/>
<s:hidden id="tipoEndoso" name="tipoEndoso"/>
<s:hidden id="accionEndoso" name="accionEndoso"/>
<s:hidden id="polizaId" name="polizaId"/>
<s:hidden id="bitemporalCotizacion.value.tipoPoliza.claveAplicaFlotillas" name="bitemporalCotizacion.value.tipoPoliza.claveAplicaFlotillas" />
<s:hidden id="bitemporalCotizacion.value.porcentajeIva" name="bitemporalCotizacion.value.porcentajeIva" />
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">		
    <s:set id="soloConsulta" value="1" />		
</s:if>
<s:elseif test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
	 <s:set id="soloConsulta" value="2" />	
</s:elseif>
<s:else>
	<s:set id="soloConsulta" value="0" />
</s:else>

<s:action name="getDatosVehiculo" var="getDatosVehiculo" namespace="/componente/vehiculo/bitemporal" ignoreContextParams="true" executeResult="true" >
    <s:param name="idCodigoPostalName">bitemporalAutoInciso.value.codigoPostal</s:param>
	<s:param name="idEstadoName">bitemporalAutoInciso.value.estadoId</s:param>	
	<s:param name="idMunicipioName">bitemporalAutoInciso.value.municipioId</s:param>
	<s:param name="validoEnName">validoEn</s:param>
	<s:param name="idCotizacionName">bitemporalCotizacion.continuity.numero</s:param>
	<s:param name="idMonedaName">bitemporalCotizacion.value.moneda.idTcMoneda</s:param>
	<s:param name="idNegocioSeccionName">bitemporalAutoInciso.value.negocioSeccionId</s:param>
	<s:param name="idMarcaVehiculoName">bitemporalAutoInciso.value.marcaId</s:param>		
	<s:param name="idEstiloVehiculoName">bitemporalAutoInciso.value.estiloId</s:param>
	<s:param name="idModeloVehiculoName">bitemporalAutoInciso.value.modeloVehiculo</s:param>
	<s:param name="modificadoresDescripcionName">bitemporalAutoInciso.value.modificadoresDescripcion</s:param>	
	<s:param name="descripcionFinalName">bitemporalAutoInciso.value.descripcionFinal</s:param>			
	<s:param name="idTipoUsoVehiculoName">bitemporalAutoInciso.value.tipoUsoId</s:param>
	<s:param name="idNegocioPaqueteName">bitemporalAutoInciso.value.negocioPaqueteId</s:param>	
	<s:param name="idAutoIncisoContinuityName">bitemporalAutoInciso.continuity.id</s:param>
	<s:param name="onChangePaquete">mostrarCoberturasInciso()</s:param>
	<s:param name="soloConsulta" value="%{#soloConsulta}"></s:param>
	<s:param name="pctDescuentoEstadoName">bitemporalAutoInciso.value.pctDescuentoEstado</s:param>
	<s:param name="serieName">bitemporalAutoInciso.value.numeroSerie</s:param>
	<s:param name="vinValidoName">bitemporalAutoInciso.value.vinValido</s:param>
	<s:param name="coincideEstiloName">bitemporalAutoInciso.value.coincideEstilo</s:param>
	<s:param name="observacionesSesaName">bitemporalAutoInciso.value.observacionesSesa</s:param>
</s:action>	
<div id="loading" style="display: none;">
                <img id="img_indicator" name="img_indicator"
                               src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="error"></div>
<div id="infoMsg"></div>
<div class="subtituloIzquierdaDiv"><s:text name="Detalle de Coberturas"/></div>
<div id="cargando" style="display: none;text-align: center;">
	<img src="/MidasWeb/img/loading-green-circles.gif">
	<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
</div>
<div id="indicador"></div>
<div id="coberturasIncisoGrid" align="center"></div>
<br/>
<div class="clear"></div>
<div class="clear"></div>
<div class="clear"></div>
<div id="divBtn" class="divContenedorBotones">			
	<div style="display: inline; float: right;" class="btn_back w100">
	    <s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_ALTA_INCISO">
	        <a href="javascript: void(0);" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir? Los cambios no guardados se perder\u00E1n')){parent.closeInciso()};">
				<s:text name="Cancelar"/>
			</a>
	    </s:if>
		<s:elseif test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">	
			<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('inciso');">
				<s:text name="Cancelar"/>
			</a>
		</s:elseif>
		<s:else>
				<a href="javascript: void(0);" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir? Los cambios no guardados se perder\u00E1n')){parent.cerrarVentanaModal('inciso')};">
				<s:text name="Cancelar"/>
			</a>
		</s:else>
	</div>	
	<s:if test="accionEndoso != 3">	
		<div id="btnGuardar" style="display: none; float: right;" class="btn_back w100 guardar">
			<a href="javascript: void(0);" class="icon_guardar" onclick="validaGuardar();">
				<s:text name="Guardar"/>
			</a>
		</div>
		<div id="btnRecalcular"  style="display: inline; float: right;" class="btn_back w150">
			<a href="javascript: void(0);" onclick="validaRecalcular();">
				<s:text name="Calcular"/>
			</a>
		</div>
	</s:if>
	<div id="btnDatosAdicionalesPaquete"  style="display: none; float: right;" class="btn_back w250">
		<a href="javascript: void(0);" onclick="mostrarDatosRiesgo(dwr.util.getValue('bitemporalInciso.continuity.id'),dwr.util.getValue('validoEn'),dwr.util.getValue('tipoEndoso'), dwr.util.getValue('accionEndoso'));">
			<s:text name="Datos Adicionales Paquete"/>
		</a>
	</div>
	<div id="btnDatosConductor"  style="display: none; float: right;" class="btn_back w150">
		<a href="javascript: void(0);" onclick="mostrarDatosConductor(dwr.util.getValue('bitemporalInciso.continuity.id'),dwr.util.getValue('validoEn'),dwr.util.getValue('accionEndoso'),dwr.util.getValue('tipoEndoso'));">
			<s:text name="Datos Conductor"/>
		</a>
	</div>
</div>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div class="clear"></div>
<div id="contenidoVehiculo"></div>
<s:if test="bitemporalInciso.continuity.id != null && bitemporalInciso.continuity.id > 0 ">
	<script type="text/javascript">		
 		muestraDatosAdicionalesVehiculo();
 		cargaValoresInciso();
	</script>
</s:if>
<s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_DE_MOVIMIENTOS">
	<script type="text/javascript">
			muestraBotonDatosConductor();
	</script>
</s:if>
<s:if test="accionRecalcular==1">
<script type="text/javascript">		
	ocultarBotonGuardar();
</script>
</s:if>
<div id="loading" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
</s:div>