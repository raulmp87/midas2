<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/validaciones.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>

<sj:head/>



<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/incisovehiculobitemporal.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/vehiculo/componenteVehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/vehicular.js'/>"></script> 

<script type="text/javascript">
	/* var verDetalleIncisoPath = '<s:url action="verDetalleInciso" namespace="endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>'; */
	var verDetalleTipoEndosoOrigenPath = '<s:url action="verDetalleTipoEndoso" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
	var guardarIncisoPath = '<s:url action="asociarIncisoCotizacion" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var mostrarCoberturasIncisoPath = '<s:url action="mostrarCoberturasInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var verDatosRiesgoIncisoPath = '<s:url action="mostrarControlDinamicoRiesgo" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var urlMostrarVehicular = '<s:url action="mostrarInfVehicular" namespace="/vehiculo/inciso"/>';
</script>


<style type="text/css">
	.icon_guardar {
		display: none;
	}
	#infoMsg, #error{
	display:none;
	margin: 10px;
	padding: 10px;
	font-size: 12px;
	text-aling: center;
	width: 93%;
	}
	#error{
		color:red;
		border: 1px solid red;
	}
	#infoMsg{
		color:green;
		border: 1px solid #73D54A;	
	}
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
	
	.ui-autocomplete {
		max-height: 120px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 10px;
	}
	
	/* IE 6 doesn't support max-height
	                  * we use height instead, but this forces the menu to always be this tall
	                  */
	* html .ui-autocomplete {
		height: 120px;
	}
	.divInputText{
	    width: 180px;
	    max-width:180px;
	    height:14px;
	    max-height:14px;
	    background-color:#FFF;
	    text-align:left;
	    cursor:text;
		-moz-user-select: -moz-none;
	   	-khtml-user-select: none;
		-webkit-user-select: none;
	   /*
	     Introduced in IE 10.
	     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
	   */
	   	-ms-user-select: none;
	   	user-select: none;
	}
</style>

<script type="text/javascript">
		
	function validaGuardar(){
		if(validateAll(true, 'save')){
			validaGuardarDatosComplementarios();			
		}
	}
	function ocultaGuardar(id,valor) {		
		jQuery('#infoMsg').show();
		jQuery('#infoMsg').text('Calcula el inciso para guardar');
		jQuery('#btnRecalcular').css('display', 'block');
		jQuery('#btnGuardar').css('display', 'none');
		jQuery('#btnDatosAdicionalesPaquete').css('display', 'none');
		jQuery('#btnDatosConductor').css('display', 'none');
	}
	
	
	function validaGuardarDatosComplementarios() {
	
		var validoEn = dwr.util.getValue("validoEn");
		var dateParts = validoEn.split("/");  
    	var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
    	 
		listadoService.validaDatosComplementariosIncisoBorrador(
						jQuery("#polizaId").val(), dwr.util.getValue("bitemporalInciso.continuity.id"),jQuery("#tipoEndoso").val(),date,
						function(data) {
							if (!data) {
							parent.mostrarMensajeInformativo(
										"Faltan guardar los datos adicionales.",
										"30", null, null);
							} else {
								guardarInciso();
							}
						});
	}

	function validaRecalcular() {
	
		if (document.getElementById("coberturasIncisoGrid").innerHTML != '') {		
		
			if(!validarCamposSA())
			{
				parent.mostrarMensajeInformativo("Existen coberturas contratadas sin valor de suma asegurada capturado, debe ingresar un valor", "20", null, null);
						
			}else if(!validarCombosDeducibles())
			{
				parent.mostrarMensajeInformativo("Existen coberturas contratadas sin valor de deducible seleccionado, debe elegir un valor", "20", null, null);
			}else
			{
				// Valida el numero de riesgos
				recalcular();		
			}			
						
		} else {
			parent.mostrarMensajeInformativo("No tienes coberturas para Recalcular", "20", null, null);
		}
	}
	
	function refrescarVentanaInciso(){
	var url;
	var verDetalleIncisoPath = "/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/verDetalleInciso.action";
	var incisoContinuityId = dwr.util.getValue("bitemporalInciso.continuity.id");
	if(incisoContinuityId == null || incisoContinuityId == undefined){
		url = verDetalleIncisoPath + "?polizaId="+jQuery('#polizaId').val()+"&validoEn="+jQuery('#validoEn').val()+"&accionEndoso="+jQuery('#accionEndoso').val()+"&tipoEndoso="+jQuery('#tipoEndoso').val();
	}else{
		url = verDetalleIncisoPath + "?polizaId="+jQuery('#polizaId').val()+"&validoEn="+jQuery('#validoEn').val()+"&accionEndoso="+jQuery('#accionEndoso').val()+"&tipoEndoso="+jQuery('#tipoEndoso').val()+"&bitemporalInciso.continuity.id="+incisoContinuityId;
	}
	url += "&accionRecalcular=1";
	parent.redirectVentanaModal("inciso", url, null);
}

	function validarCombosDeducibles()
	{
		var validacionExitosa = true;	

	 	jQuery('select[id*=Deducible]').each(function(){
	 	
	 	
    
	        var indice = jQuery(this).attr('id').split('_')[1];
	        var nombreCheck = 'claveContratoBoolean_';
	        var nombreHidden = '__value_claveContratoBoolean'
	        	        
	   	    if (jQuery('#'+nombreCheck + indice).length > 0){
	    	   
	    	    if(jQuery("input[id='"+nombreCheck.concat(indice)+"']:checked").val() == 'true')
	    	    {     	    		    	    	
	    	    	if(jQuery('#'+jQuery(this).attr('id') + " option:selected").val() == '-1')
	    	    	{	    	    	    
	    	    		validacionExitosa = false;
	    	    	}    	    	    	    		    	    
	    	    }
	        }        
	        else
	        if(jQuery("input[id$="+indice + nombreHidden + "]").length > 0)
	       	{	        	
	        	if(jQuery("input[id$="+indice + nombreHidden + "]").val()== 'true')
	        	{	        	    
	        		if(jQuery('#'+jQuery(this).attr('id') + " option:selected").val() == '-1')
	    	    	{
	    	    		validacionExitosa = false;
	    	    	}	 	        		
	        	}
	        }    
     
  		});  		
  		
  		return validacionExitosa;  	
	}
	
	function validarCamposSA()
	{
		var validacionExitosa = true;	

	 	jQuery('input[id*=SumaAseguradaStr]').each(function(){
	 	
	 	
    
	        var indice = jQuery(this).attr('id').split('_')[1];
	        var nombreCheck = 'claveContratoBoolean_';
	        var nombreHidden = '__value_claveContratoBoolean'
	        	        
	   	    if (jQuery('#'+nombreCheck + indice).length > 0){
	    	   
	    	    if(jQuery("input[id='"+nombreCheck.concat(indice)+"']:checked").val() == 'true')
	    	    {	    	
	    	    	 	    	    	
	    	    	if(jQuery('#'+jQuery(this).attr('id')).val() == '')
	    	    	{	    	    	    
	    	    		validacionExitosa = false;
	    	    	}    	    	    	    		    	    
	    	    }
	        }        
	        else
	        if(jQuery("input[id$="+indice + nombreHidden + "]").length > 0)
	       	{	        	
	        	if(jQuery("input[id$="+indice + nombreHidden + "]").val()== 'true')
	        	{	      
	        	      	    
	        		if(jQuery('#'+jQuery(this).attr('id')).val() == '')
	    	    	{
	    	    		validacionExitosa = false;
	    	    	}	 	        		
	        	}
	        }    
     
  		});  		
  		
  		return validacionExitosa;  	
	}


</script>
