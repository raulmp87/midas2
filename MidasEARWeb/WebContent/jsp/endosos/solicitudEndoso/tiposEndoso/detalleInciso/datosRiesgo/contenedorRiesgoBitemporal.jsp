<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/contenedorVehiculoBitemporalHeader.jsp"></s:include>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(':input').css('width', '250px');
		jQuery('#loading').css('display', 'none');
	});
	// valida que los inputs del formulario esten llenos
	function validaGuardarDatosRiesgo(){
		var input= '';
		if(jQuery('select').size() > 0 ){
			jQuery('select').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text() + "\n";
				}
			});
		}
		if(jQuery('input[type!=checkbox]').size() > 0 ){
			jQuery('input').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text() + "\n";
				}
			});
		}
		if(jQuery('textarea').size() > 0 ){
			jQuery('textarea').each(function(){
				if(jQuery(this).val() == '' || jQuery(this).val() == null){
					 input += jQuery("label[for="+jQuery(this).attr('id')+"]").text() + "\n";
				}
			});
		}
		if(input != ''){
			alert("Por favor verifica los siguientes campos \n" + input)
		}else{
			guardarDatosRiesgo();
		}
	}
	
</script>
</head>
<body>
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<%-- 	<s:if test="%{mensaje != null}">
		<script type="text/javascript">
			jQuery('#mensaje').css('font-size', '12px');
			jQuery('#mensaje').css('color', 'green');
			jQuery('#mensaje').css('text-align', 'center');
			jQuery('#mensaje').show();
		</script>
	</s:if> --%>
	<s:form action="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/guardarDatosRiesgo.action" name="complementarIncisoForm" id="complementarIncisoForm">
		<s:hidden name ="bitemporalInciso.continuity.id" value="%{bitemporalInciso.continuity.id}" />
		<s:hidden name="validoEn" id="validoEn"/>
		<s:hidden name="tipoEndoso" id="tipoEndoso" />
		<div id="contenedorPaquete" style="width: 98%; overflow-y: auto;">
			<s:label value="Datos Adicionales del Paquete" cssClass="titulo" />
			<table id="agregar" border="0" width="98%">
				<tr>
					<td>
						<div>
							<div id="loading" style="text-align: center;">
								<img src="/MidasWeb/img/loading-green-circles.gif">
								<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
							</div>
							<s:action name="cargaDatosRiesgoInciso" executeResult="true"
								namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso" ignoreContextParams="true">
								<s:param name="bitemporalInciso.continuity.id"
									value="%{bitemporalInciso.continuity.id}" />
								<s:param name="validoEn"><s:property value="validoEn"/></s:param>
								<s:param name="tipoEndoso"><s:property value="tipoEndoso"/></s:param>
								<s:param name="name" value="'datosRiesgo'" />
							</s:action>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"></div>
		<div id="controles" style="width: 98%;margin-right: .4em;">
			 <s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
				<div id="b_guardar" style="float: right;">
					<a href="javascript: void(0);"
						onclick="javascript: validaGuardarDatosRiesgo();"> <s:text
							name="midas.boton.guardar" /> </a>
				</div>
			</s:if>
		</div>
	</s:form>
</body>
</html>