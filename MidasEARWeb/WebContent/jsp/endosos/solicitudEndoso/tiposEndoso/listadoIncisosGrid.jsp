<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<!-- La variable tipoOperacion determina el tipo de operacion para el cual va a ser usado el Grid: 1 = Alta de Inciso,  2 = Baja de Inciso  -->
<!-- La variable complementarEmisionFlag determina si las acciones que se mostraran corresponderan a las acciones para la funcionalidad Complementar Emision, esto aplica
     unicamente cuando se trata de una operacion de Alta de Inciso
-->

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<s:if test="tipoOperacion == 2">
		    <column id="selected" type="ch" width="30" sort="int" >#master_checkbox</column>
		</s:if>
		<column id="midas.suscripcion.cotizacion.idtocotizacion" type="ro" width="110" sort="int" ><s:text name="No. de Inciso" /></column>
		<column id="midas.suscripcion.solicitud.solicitudPoliza.folio"  type="ro" width="160" sort="int" ><s:text name="Descripcion Vehiculo" /></column>
		<s:if test="tipoOperacion == 1">
		    <column id="lineaNegocio" type="ro" width="150" sort="int" ><s:text name="Linea de Negocio" /></column>
		    <column id="paquete" type="ro" width="150" sort="int" ><s:text name="Paquete" /></column>
		</s:if>
		<s:else>
		    <column id="numeroSerieVehiculo" type="ro" width="100" sort="int" ><s:text name="No.Serie" /></column>
		    <column id="numeroMotor" type="ro" width="100" sort="int" ><s:text name="No. Motor" /></column>
		    <column id="placas" type="ro" width="100" sort="int" ><s:text name="Placas" /></column>
		    <column id="conductor" type="ro" width="140" sort="int" ><s:text name="Conductor" /></column>
		    <column id="asegurado" type="ro" width="140" sort="int" ><s:text name="Asegurado" /></column>
		</s:else>		
		<column id="solicitud.claveTipoEndoso" type="ro" width="122" sort="int" ><s:text name="Prima Total" /></column>
		<column id="solicitud.claveEstatus" type="co" width="120" sort="int" ><s:text name="Estatus" />
			 <option value="0">EN PROCESO</option>
			 <option value="1">ORDEN DE TRABAJO</option>
			 <option value="2">TERMINADA</option>
			 <option value="3">PENDIENTE</option>
			 <option value="5">EN PROCESO INCOMPLETA</option>
			 <option value="10">COT EN PROCESO</option>
			 <option value="11">COT ASIGNADA</option>
			 <option value="13">COT LIBERADA</option>
			 <option value="14">COT LISTA PARA EMITIR</option>
			 <option value="15">COT ASIGNADA PARA EMISION</option>
			 <option value="16">COT EMITIDA</option>
			 <option value="17">COT PENDIENTE AUTORIZACION</option>
		</column>
		<s:if test="tipoOperacion == 1">		    
		    <s:if test="complementarEmisionFlag == 1">
		        <column id="action1" type="img" width="30" align="center"></column>
		        <column id="action2" type="img" width="30" align="center"></column>
		        <column id="action3" type="img" width="30" align="center"></column>
		    </s:if>
		    <s:else>
		        <column id="action1" type="img" width="45" align="center"></column>
		        <column id="action2" type="img" width="45" align="center"></column>
		    </s:else>	    
		</s:if>
		<s:else>
		    <column id="action1" type="img" width="30" align="center"></column>
		    <column id="action2" type="img" width="30" align="center"></column>
		</s:else>
				
	</head>
	<row id="row1">
	        <s:if test="tipoOperacion == 2">
			    <cell>false</cell>
			</s:if>
			<cell>00001</cell>
			<cell>ATOS 2006 STD 4 PTS</cell>
			<s:if test="tipoOperacion == 1">
			    <cell>Autos Flotilla</cell>
			    <cell>Amplio</cell>    
			</s:if>
			<s:else>
			    <cell>78547587</cell>
			    <cell>1AH47884F34</cell>
			    <cell>SRR2511</cell>
			    <cell>Pedro Moreno</cell>
			    <cell>Gloria Hernan</cell>
			</s:else>
			<cell>$ 1,526.00</cell>
			<cell>EN PROCESO</cell>
			<s:if test="tipoOperacion == 1">
			    <s:if test="complementarEmisionFlag == 1">
				    <cell>/MidasWeb/img/icons/ico_agregar.gif^Definir Inciso^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
					<cell>/MidasWeb/img/cargaMasiva.png^Multiplicar Inciso^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>	
		            <cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar Inciso^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell> 
                </s:if> 
                <s:else>
	                <cell>/MidasWeb/img/icons/ico_editar.gif^Complementar Datos Asegurado^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
					<cell>/MidasWeb/img/icons/ico_editar.gif^Complementar Datos Adicionales Vehiculo^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>	
                </s:else>
			</s:if>
			<s:else>
			    <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Inciso^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
			    <cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar Inciso^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
			</s:else>
			          
    </row>
<!--  		
	<s:iterator value="solicitudes" status="stats">
		<row id="<s:property value='%{stats.index}' />">
			<cell><s:property value="numeroSolicitud" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombrePersona" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoPaterno" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoMaterno" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="productoDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveEstatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_asignar.gif^Asignar^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>	
			<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Rechazar^javascript: mostrarVentanaRechazar(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>,"parent.obtenerSolicitudesEmision()")^_self</cell>
			<cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
		    <cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>		 
		</row>
	</s:iterator>	
-->	
</rows>
