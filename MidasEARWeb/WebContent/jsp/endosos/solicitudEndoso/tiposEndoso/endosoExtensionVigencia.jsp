<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<script
	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoExtensionVigencia.js'/>"></script>
<script
	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas.js'/>"></script>


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';
	var previsualizarCobranzaActionPath = '<s:url action="previsualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';	
	var igualarPrimaActionPath = '<s:url action="igualarPrima" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';		
	
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else> 

<div class="titulo" style="width: 98%;">
	<s:text
		name="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.titulo" />
	&nbsp;-&nbsp;P&oacuteliza(
	<s:text   name="numeroPolizaFormateado"  />
	)
</div>
<div style="width: 98%; text-align: right;">
	<s:text
		name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" />
	:&nbsp;
	<s:text name="numeroEndosoFormateado" />
</div>

<s:form action="mostrar" id="endosoExtensionVigPolizaForm" cssClass="">
	<table>
		<tr>
			<td>
				<table id="agregar" style="border: #000000;" width="98%">
					<tr>
						<td>
						 <s:textfield cssClass="txtfield" cssStyle="width: 100px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
								labelposition="top" size="10"
								id="fechaIniVigenciaEndosoxx"
								name="fechaIniVigenciaEndoso" disabled="true"/>
					</tr>
					<tr id="tipoEndoso">
						<td>
						<s:if test="#soloConsulta">
						    <s:textfield cssClass="txtfield" cssStyle="width: 100px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.fechaFinPolizaExtVigencia"
								labelposition="top" size="10"
								id="fechaExtensionVigencia"
								name="fechaExtensionVigencia" disabled="true"/>   
						</s:if>
						<s:else>
						    <sj:datepicker name="fechaExtensionVigencia" required="true"
							cssStyle="width: 100px;" buttonImage="../img/b_calendario.gif"
							key="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.fechaFinPolizaExtVigencia"
							id="fechaExtensionVigencia" maxlength="10" labelposition="top"
							readonly="false" cssClass="txtfield"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);" changeMonth="true"
							disabled="#soloConsulta"
							changeYear="true" minDate="fechaIniExtensionVigenciaValida"  >
					   </sj:datepicker>
						</s:else>						
						</td>
					</tr>
					<tr>
						<td>
							<s:if	test="accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
								<div id="divLimpiarBtn" style="float: left;" class="w150">
									<div class="btn_back w140">
										<a href="javascript: void(0);" onclick="mostrarIgualarMovimientoEndosoEV();">
											<s:text
												name="midas.endosos.solicitudEndoso.tiposEndoso.extensionVigencia.boton.igualarPrima" />
										</a>
									</div>
								</div>
							</s:if>
						</td>
						<td>
						</td>
					</tr>

				</table></td>
			<td>
				<div>
                  <div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div> 
              </div></td>
		</tr>
		<tr height="160px">
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td align="left">
				<div id="divLimpiarBtn" style="float: left;" class="w150">
					<div class="btn_back w140">
						<a href="javascript: void(0);" onclick="cancelar();"> <s:text
								name="midas.boton.cancelar" /> </a>
					</div>
				</div> 
				<s:if	test="accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
					<div id="divBuscarBtn" class="w150" style="float: left;">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" />
							</a>
						</div>
					</div>
				</s:if>
				<s:if test="!#soloConsulta"> 
					<div id="divLimpiarBtn" style="float: left;" class="w150">
						<div class="btn_back w140">
							<a href="javascript: void(0);" onclick="cotizar();"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" />
							</a>
						</div>
					</div>
				</s:if>
			</td>
		</tr>
	</table>
	<s:hidden name="accionEndoso" />
	<s:hidden name="polizaId"  id ="numeroPoliza"/>
	<s:hidden name="cotizacion.continuity.id"  />
	<s:hidden name="cotizacion.continuity.numero"  />
	<s:hidden name="cotizacion.value.solicitud.idToSolicitud"  />
	<s:hidden name="cotizacion.value.fechaFinVigencia"  />
	<s:hidden name="fechaIniVigenciaEndoso" />

	<s:hidden name="actionNameOrigen"  id="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen"  id="namespaceOrigen"/>
	<s:hidden name="numeroPolizaFormateado"  id="numeroPolizaFormateado"/>
</s:form>
