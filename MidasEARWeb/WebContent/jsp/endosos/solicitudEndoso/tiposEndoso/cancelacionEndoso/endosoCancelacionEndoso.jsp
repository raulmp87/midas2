<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head />
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso.js'/>"></script>

<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';
	var obtenerEndososPath = '<s:url action="obtenerEndosos" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';
	var obtenerEndososPaginadasPath = '<s:url action="obtenerEndososPaginado" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';	
	var previsualizarCobranzaActionPath = '<s:url action="mostrarPrevisualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCobranza"/>';
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>

<div class="titulo" style="width: 98%;">
	<s:text	name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionEndoso.titulo" />
	&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada" />)
</div>
<div style="width: 98%; text-align: center;">
	<table width="98%"
		style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
		<tr>
			<td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" />:&nbsp;
				<s:text	name="siguienteEndoso" />
			</td>
		</tr>
		<tr height="15px"></tr>
	</table>
</div>
<s:form id="solicitudPolizaForm">
	<s:hidden name="polizaId" />
	<s:hidden name="accionEndoso" />
	<s:hidden name="cotizacionContinuityId" />
	<s:hidden name="siguienteEndoso" />
	<s:hidden name="tipoEndoso" />
	<s:hidden name="biCotizacion.value.importeNotaCredito"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>	
	<table width="98%">
		<tr>
			<td>
				<table id="agregar" style="border: #000000;" width="98%">
					<tr>
						<td> 
						<s:if test="#soloConsulta">
						    <s:textfield cssClass="txtfield" cssStyle="width: 100px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
								labelposition="top" size="10"
								id="fechaIniVigenciaEndoso"
								name="fechaIniVigenciaEndoso" disabled="true"/>						   
						</s:if>
						<s:else>
						    <sj:datepicker name="fechaIniVigenciaEndoso" key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso" 
                           required="true" cssStyle="width: 170px;" 
			   			   buttonImage="../img/b_calendario.gif"
			               id="fechaIniVigenciaEndoso" maxlength="10" 	
			               labelposition="left"					               
			               cssClass="txtfield"
			               onkeypress="return soloFecha(this, event, false);"
			               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
			               onblur="esFechaValida(this);"					              
			               changeMonth="true" 
			               changeYear="true"	
			               disabled="#soloConsulta"						   								  
			               >
					    </sj:datepicker>
						</s:else>
						 
						</td>  
					</tr>
				</table></td>
			<td align="right" width="30%">
				<div>
					<div id="cargaResumenTotales"></div>
					<div id="resumenTotalesCotizacionGrid">
						<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
					</div>
				</div>
			</td>

		</tr>
		<tr height="50px">
			<td colspan="2">
				<div class="titulo" style="width: 98%;">
					<s:text
						name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.listaEndosos" />
				</div>

				<div id="tablaCentral" class=detalle>
					<!-- Tabla   -->
					<div id="indicador"></div>
					<div id="gridEndososListadoPaginado">
						<div id="cotizacionEndososListadoGrid"
							style="width: 98%; height: 130px"></div>
						<div id="pagingArea"></div>
						<div id="infoArea"></div>
					</div>
				</div></td>
		</tr>
		<tr>
			
			<td align="right">
				<div id="divLimpiarBtn" style="float: left;" class="w150">
					<div class="btn_back w140">
						<a href="javascript: void(0);" onclick="cancelar();"> <s:text
								name="midas.boton.cancelar" /> </a>
					</div>
				</div>
				
			 <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && biCotizacion.value.importeNotaCredito > 0">
	             <div id="divCobranzaBtn" class="w150" style="float:left;">
					<div class="btn_back w140">
					  <a href="javascript: void(0);" 
						onclick="previsualizarCobranza(dwr.util.getValue('cotizacionContinuityId'),dwr.util.getValue('fechaIniVigenciaEndoso'),dwr.util.getValue('accionEndoso'),dwr.util.getValue('tipoEndoso'));">
						<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.previsualizarCobranza" />
					  </a>
				    </div>
	             </div>	
             </s:if>
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
					<div id="divBuscarBtn" class="w150" style="float: left;">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" />
							</a>
						</div>
					</div>
				</s:if>
				<s:if test="!#soloConsulta"> 
					<div id="divLimpiarBtn" style="float: left;" class="w150">
						<div class="btn_back w140">
							<a href="javascript: void(0);" onclick="cotizar();"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" />
							</a>
						</div>
					</div>
				</s:if>
		</tr>
	</table>
</s:form>

<script type="text/javascript">
	iniciaLlenadoEndosos();
</script>

