<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>


<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="id" type="ro" width="30" sort="int"  hidden="true"></column>
		<column id="selected" type="ra" width="150" sort="int"  ></column>
		<column id="noEndoso" type="ro" width="150" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" /></column>
		<column id="validFrom" type="ro" width="150" sort="int" ><s:text name="midas.componente.impresiones.fechaDeValidez" /></column>
		<column id="tipoMovimiento" type="ro" width="200" sort="int" ><s:text name="midas.endosos.cotizacionEndosoListado.tipoEndoso" /></column>	
		<column id="primaEndoso" type="ro" width="200" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.primaEndoso" /></column>
	
	</head>
     
	<s:iterator value="lstEndosoDto" status="stats">
		<row id="<s:property value='idComposicion'/>"   style="<s:property value='styleRow'/>">
			<cell><s:property value="id.idToPoliza" escapeHtml="false" escapeXml="true" /></cell>
			<cell disabled="true"><s:property value="checked" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="id.numeroEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="validFrom" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTipoEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="%{getText('struts.money.format',{valorPrimaTotal})}" escapeHtml="false" escapeXml="true"/></cell>			
			
		</row>
	</s:iterator>	
	
</rows>