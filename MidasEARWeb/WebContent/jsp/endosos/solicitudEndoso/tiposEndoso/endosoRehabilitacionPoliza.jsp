<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionPoliza.js'/>"></script>
<script type="text/javascript">
    var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';
	var previsualizarCobranzaActionPath = '<s:url action="previsualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>


<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionPoliza.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div>  
<div style="width: 98%; text-align: right;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"  />:&nbsp;<s:text name="cotizacionEndoso.numeroEndoso" /></div>

<s:form action="mostrar"  id="solicitudPolizaForm" cssClass="" >
   <table>
      <tr>
          <td>
              <table id="agregar"  style="border: #000000;" width="98%">
		    <tr>
		        <td>
			       <s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.general.fechaInicioVigenciaEndoso"
					labelposition="top"  
					size="10" readonly="true"
					name="fechaIniVigenciaEndoso" disabled="true"/>  				    
			    </td>			
		    </tr>
		    
		     <s:if test="%{accionEndoso !=@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}">
			    <s:if test="cotizacionEndoso.origenCancelacionAutomatica && cotizacionEndoso.aplicaCorrimientoVigencias">
				    <tr id="tipoEndoso">
					    <td>
					        <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionPoliza.corrimientoVigencias"/>:
					        <s:radio name="cotizacionEndoso.requiereCorrimientoVigencias" 
					        	list="#{'0':'No', '1':'Si'}" onchange="javascript: muestraCorrimiento();"
				        	 	id="requiereCorrimientoVigencias" cssStyle="agregar" />
					    </td>					        
					</tr>
					<tr id="sinCorrimiento">
					    <td> 
							<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionPoliza.fechaFinPoliza"
								labelposition="top"  
								size="10" readonly="true"
								name="cotizacionEndoso.fechaFinPoliza" disabled="true"/> 	 					        
					    </td>					        
					</tr>
					<tr id="aplicaCorrimiento">
					    <td> 
							<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionPoliza.fechaFinPoliza"
								labelposition="top"  
								size="10" readonly="true"
								name="cotizacionEndoso.fechaFinPolizaRecorrida" disabled="true"/> 					        
					    </td>					        
					</tr>	
					<script type="text/javascript">
						muestraCorrimiento();
					</script>
				</s:if>	
				<s:else>
					<tr id="sinCorrimiento">
					    <td> 
							<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
								key="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionPoliza.fechaFinPoliza"
								labelposition="top"  
								size="10" readonly="true"
								name="cotizacionEndoso.fechaFinPoliza" disabled="true"/> 	 					        
					    </td>					        
					</tr>
				</s:else>
			 </s:if>	
								   
	    </table>
          </td>
          <td>               
              <div align="right">              
                 <s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>              
              </div>            
          </td>
      </tr>
      <tr height="160px">
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td>
          </td>
		    <td align="left">
			    <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>	
	              <s:if test="%{accionEndoso !=@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}">
		             <s:if test="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()}">
		             <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
					    </div>
		             </div>	
		             </s:if>
		             <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="cotizar();" >	
							    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
						    </a>
		                </div>
		             </div>
		           </s:if>  																		
		    </td>
	  </tr>	  
   </table>		
   <s:hidden name="accionEndoso"/>
   <s:hidden name="polizaId"/>
   <s:hidden name="fechaIniVigenciaEndoso"/>
   <s:hidden name="cotizacionEndoso.fechaFinPoliza"/>
   <s:hidden name="cotizacionEndoso.fechaFinPolizaRecorrida"/>
   <s:hidden name="actionNameOrigen"/>
   <s:hidden name="namespaceOrigen"/>
</s:form>