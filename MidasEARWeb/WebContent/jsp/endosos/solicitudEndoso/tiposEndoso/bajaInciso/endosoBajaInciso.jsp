<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/programaPago.js"/>"></script>
<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/bajaInciso/endosoBajaIncisoHeader.jsp"></s:include>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>
 
<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
    <div style="width: 98%; text-align: right;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></div>
<div id="spacer1" style="height: 10px"></div>
<div align="center">
<s:form action="mostrarAltaInciso" id="bajaIncisoForm" cssClass="" >
<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="accionEndoso" id="accionEndoso"/>
<!--<s:hidden name="cotizacion.value.fechaInicioVigencia" value="%{fechaIniVigenciaEndoso}"/> -->
<s:hidden name="fechaIniVigenciaEndoso"/>
<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
<s:hidden name="cotizacion.value.importeNotaCredito"/>
<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden name="motivoEndoso" id="motivoEndoso"/>
<s:hidden name="actionNameOrigen" tipo="actionNameOrigen" />
<s:hidden name="namespaceOrigen" tipo="namespaceOrigen" />
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

   <table width="98%">
      <tr align="left">
          <td valign="top">
              <table id="agregar"  style="border: #000000;" width="98%">
		        <tr>		        
			        <td>			      
						<s:textfield cssClass="txtfield" 
						    cssStyle="width: 80px;"
						    key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
						    labelposition="top"  
						    size="10" 
						    readonly="true"
						    name="fechaIniVigenciaEndoso" 
						    disabled="true"/>    
			        </td>                           	    			
		        </tr>								   
	         </table>
          </td>
          <td align="right">               
              <div>
                  <div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div> 
              </div>            
          </td>
      </tr>        
   </table>		
</s:form>	
</div>
<s:form id="cotizacionForm">
    <s:hidden name="cotizacion.idToCotizacion" value="%{cotizacion.continuity.numero}"/>
</s:form>
<div align="center" style="width: 100%;">                                                                         
<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
	<s:param name="idToPolizaName">polizaId</s:param>		
	<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
	<s:param name="idAccionEndosoName">accionEndoso</s:param>	
	<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO}"></s:param>
	<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
</s:action>

</div>    
<!--  <div>
  <center>
    <table width="98%">
        <tr>
            <td style="font-size: 12px;font-weight: bold;">
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.listaIncisos"/>
            </td>                   
        </tr>    
    </table>
    <div id="indicador"></div>
			<div id="gridIncisosPaginado" >
				<div id="incisosListadoGrid" style="width:98%;height:130px"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</div>
  </center>
</div>

-->
<div id="spacer2" style="height: 40px"></div>
<div align="right">
    <table>
        <tr>           
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150" >                			
					<div class="btn_back w140" >
						<s:if test="#soloConsulta">
							<a href="javascript: void(0);" onclick="cancelar(false);">	
									<s:text name="midas.boton.cancelar"/>	
							</a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" onclick="cancelar(true);">	
									<s:text name="midas.boton.cancelar"/>	
							</a>
						</s:else>
		             </div>
                </div>
            </td>
            
		          <!-- Igualar endoso -->
				  <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() &&
				  	!esExterno"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarIgualarBajaEndoso();">	
											<s:text name="midas.cotizacion.verigualarprimas"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          </s:if>
		                      
             <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && cotizacion.value.importeNotaCredito > 0">
              <td>
	             <div id="divCobranzaBtn" class="w150" style="float:left;">
					<div class="btn_back w140">
					  <a href="javascript: void(0);" 
						onclick="previsualizarCobranza(dwr.util.getValue('cotizacion.continuity.id'),dwr.util.getValue('fechaIniVigenciaEndoso'),dwr.util.getValue('accionEndoso'),dwr.util.getValue('cotizacion.value.solicitud.claveTipoEndoso'));">
						<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.previsualizarCobranza" />
					  </a>
				    </div>
	             </div>
	           </td>
             </s:if>
          <!--<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
              <td>
	             <div id="previoRecivos" class="w150" style="float:left;">
					<div class="btn_back w140">
					  <a href="javascript: void(0);" 
						<a href="javascript: void(0);" onclick="mostrarProgramaPagoEndoso(<s:property value="cotizacion.value.solicitud.idToSolicitud"/>,<s:property value="cotizacion.continuity.id"/>,<s:property value="polizaId"/>,'E');">	
											Previo de Recibos	
					  </a>
				    </div>
	             </div>
	           </td>
             </s:if>-->
            <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">	                      
	            <td>
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">	
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir"/>	
									</a>
			                    </div>
	                </div>
	            </td>
            </s:if>
            <td>
              <s:if test="!#soloConsulta">      
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
						<div class="btn_back w140" >
							<a href="javascript: void(0);" onclick="cotizar();">	
								<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
							</a>
			            </div>
	                </div>
              </s:if>
            </td>
            
        </tr>    
    </table>       
</div>