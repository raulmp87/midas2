<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/definirSolicitudEndoso.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoBajaInciso.js'/>"></script>
<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
	var previsualizarCobranzaActionPath = '<s:url action="mostrarPrevisualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCobranza"/>';
	var mostrarIgualarActionPath = '<s:url action="mostrarIgualacionPrimas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas"/>';	
//	var previsualizarCobranzaActionPath = '<s:url action="previsualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
//	var busquedaRapidaPath = '<s:url action="busquedaRapida" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
//	var solicitudesPaginadasRapidaPath = '<s:url action="busquedaRapidaPaginada" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
//	var definirTipoModificacionPath =  '<s:url action="mostrarAltaInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
</script>