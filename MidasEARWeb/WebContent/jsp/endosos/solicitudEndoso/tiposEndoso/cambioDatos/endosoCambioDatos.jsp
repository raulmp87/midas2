<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/cambioDatos/endosoCambioDatosHeader.jsp"></s:include>

<style type="text/css">

.b_submit:disabled{
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;
}

.b_submit[disabled]:hover{
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;
	!important;	
}

div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000;
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt;" >
	    <tr>
	        <td><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud"/>:&nbsp;<s:text name="cotizacion.value.solicitud.numeroSolicitud" /></td>
	        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></td>	        
	    </tr>
	    <tr height="15px"></tr>	       	
	</table>
</div>
<div id="spacer1" style="height: 10px"></div>
<div align="center">
<s:form action="mostrarCambioDatos" id="cambioDatosForm" cssClass="">
<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="accionEndoso" id="accionEndoso"/>
<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
<s:hidden name="fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
</s:form>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

	<table width="100%" cellspacing="0">
		<tr>
			<td>
				<table id="agregar" cellspacing="0"
					style="border: #000000;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;"
					width="100%">
					<tr>
						<td>
				            <s:textfield cssClass="txtfield" 
							    cssStyle="width: 80px;"
							    key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
							    labelposition="top"  
							    size="10" 
							    readonly="true"
							    name="fechaIniVigenciaEndoso" 
							    disabled="true"/>    
			            </td>
					</tr>
				</table>
		   </td>
		</tr>
		<tr>
			<td>
				   <table width="98%" style="padding: 0px; margin: 0px; border: none;" class="fixTabla">
						<tr>
							<td>
							<div id ="datosContratante" style="height: 150px; float:both;" class="agregar">
								<s:action name="verDatosContratante" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos" ignoreContextParams="true" executeResult="true" >
									<s:param name="cotizacionContinuityId"><s:property value="cotizacion.continuity.id"/></s:param>
									<s:param name="fechaIniVigenciaEndoso"><s:property value="fechaIniVigenciaEndoso"/></s:param>
									<s:param name="accionEndoso"><s:property value="accionEndoso"/></s:param>
								</s:action>	
							</div>
							</td> 	
						</tr>
					</table>
		</tr>
	</table>

</div>
<div align="center" style="width: 100%;">                                                                   
	<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
		<s:param name="idToPolizaName">polizaId</s:param>		
		<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
		<s:param name="idAccionEndosoName">accionEndoso</s:param>	
		<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS}"></s:param>
		<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
	</s:action>
</div> 
<div id="spacer2" style="height: 40px"></div>
<div align="right">
    <table>
        <tr>                                
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="cancelar();">	
									<s:text name="midas.boton.cancelar"/>	
								</a>
		                    </div>
                </div>
            </td>
            
 				<!-- Muestra Movimientos del endoso -->
				  <s:if test="!#soloConsulta"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarMovimientosEndoso(<s:property value="cotizacion.continuity.id"/>);">	
											<s:text name="midas.suscripcion.endoso.auto.mostrarMovimientos"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          </s:if>	
		                      
            <td>
                <s:if test="!#soloConsulta">                
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
						<div class="btn_back w140" >
							<a href="javascript: void(0);" onclick="cotizar();">	
								<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
							</a>
			            </div>
	                </div>
                </s:if>
            </td>
            <td>
               <!--   <div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="emitir();">	
									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir"/>	
								</a>
		                    </div>
                </div>  -->
                <s:if test="!#soloConsulta">
	                <div class="btn_back w140" >
				        <s:submit key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" 
				                  id="botonEmision" 			                 
				                  name="botonEmision"
				                  onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir(); return false;}"
						          cssClass="b_submit w140"/>						    
				   </div>
			   </s:if>
            </td>                               
        </tr>    
    </table>
</div>			
	
