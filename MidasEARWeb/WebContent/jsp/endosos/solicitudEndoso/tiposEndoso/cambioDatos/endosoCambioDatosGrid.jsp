<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="numeroInciso" type="ro" width="110" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso" /></column>
		<column id="midas.suscripcion.solicitud.solicitudPoliza.folio"  type="ro" width="160" sort="int" ><s:text name="Descripcion Vehiculo" /></column>
		<column id="nombreAsegurado" type="ro" width="100" sort="int" ><s:text name="No. de Serie" /></column>
		<column id="numeroSerieVehiculo" type="ro" width="100" sort="int" ><s:text name="No. de Motor" /></column>
		<column id="placas" type="ro" width="75" sort="int" ><s:text name="Placas" /></column>
		<column id="conductor" type="ro" width="155" sort="int" ><s:text name="Conductor" /></column>
		<column id="asegurado" type="ro" width="155" sort="int" ><s:text name="Asegurado" /></column>
		<column id="modificarDatosAsegurado" type="img" width="30" align="center"></column>
		<column id="modificarDatosVehiculo" type="img" width="30" align="center"></column>			
	</head>
	<row id="row1">
			<cell>00001</cell>
			<cell>ATOS 2006 STD 4 PTS</cell>
			<cell>7845141003</cell>
			<cell>14A78SDF89</cell>
			<cell>SRR1411</cell>
			<cell>Pedro Moreno</cell>		
			<cell>Gloria Hernan</cell>	
            <cell>/MidasWeb/img/icons/ico_editar.gif^Modificar Datos Asegurado^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
		    <cell>/MidasWeb/img/icons/ico_editar.gif^Modificar Datos Adicionales Vehiculo^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>	
                   
    </row>
<!--  		
	<s:iterator value="solicitudes" status="stats">
		<row id="<s:property value='%{stats.index}' />">
			<cell><s:property value="numeroSolicitud" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombrePersona" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoPaterno" escapeHtml="false" escapeXml="true"/> <s:property value="apellidoMaterno" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="productoDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveEstatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_asignar.gif^Asignar^javascript: mostrarVentanaAsignarSolicitud(<s:property value="idToSolicitud"/>,parent.closeAsignarSolicitud)^_self</cell>	
			<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Rechazar^javascript: mostrarVentanaRechazar(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>,"parent.obtenerSolicitudesEmision()")^_self</cell>
			<cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
		    <cell>/MidasWeb/img/b_comentariog.gif^Comentarios^javascript: abrirComentarios(<s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>		 
		</row>
	</s:iterator>	
-->	
</rows>
