<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCambioDatos.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript">	
    var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos"/>';	
    var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos"/>';
</script>