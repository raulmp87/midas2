<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script>
<s:include value="/jsp/endosos/solicitudEndoso/tiposEndoso/endososPerdidaTotal/endososPerdidaTotalHeader.jsp"></s:include>

<div class="titulo" style="width: 98%;">
<s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL">
	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaIncisoPerdidaTotal.titulo"/>
</s:if>
	<s:elseif test ="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL">
<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPolizaPerdidaTotal.titulo"/>
</s:elseif>
&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)

</div> 
    <div style="width: 98%; text-align: right;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></div>
<div id="spacer1" style="height: 10px"></div>
<div align="center">
<s:form id="endososPerdidaTotalForm" cssClass="" >
<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="accionEndoso" id="accionEndoso"/>
<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
<s:hidden name="cotizacion.value.importeNotaCredito"/>
<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndosoSolicitud"/>
<s:hidden name="cotizacion.continuity.id" id="cotizacion.continuity.id"/>
<s:hidden name="idsSeleccionados" id="idsSeleccionados"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden name="motivoEndoso" id="motivoEndoso"/>
<s:hidden name="tipoIndemnizacion" id="tipoIndemnizacion"/>
<s:hidden name="idSiniestroPT" id="idSiniestroPT"/>
<s:hidden name="numeroSiniestro" id="numeroSiniestro"/>
<s:hidden name="actionNameOrigen" tipo="actionNameOrigen" />
<s:hidden name="namespaceOrigen" tipo="namespaceOrigen" />
<s:hidden name="resumenCostosDTO.totalPrimas" id="resumenCostosDTO.totalPrimas" />
<s:hidden name="resumenCostosDTO.descuentoComisionCedida" id="resumenCostosDTO.descuentoComisionCedida" />
<s:hidden name="resumenCostosDTO.primaNetaCoberturas" id="resumenCostosDTO.primaNetaCoberturas" />
<s:hidden name="resumenCostosDTO.recargo" id="resumenCostosDTO.recargo" />
<s:hidden name="resumenCostosDTO.derechos" id="resumenCostosDTO.derechos" />
<s:hidden name="resumenCostosDTO.iva" id="resumenCostosDTO.iva" />
<s:hidden name="resumenCostosDTO.primaTotal" id="resumenCostosDTO.primaTotal" />
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

<table width="98%">
      <tr align="left">
          <td valign="top">
              <table id="agregar"  style="border: #000000;" width="98%">
		        <tr>		        
			        <td>			      
						<s:textfield cssClass="txtfield" 
						    cssStyle="width: 80px;"
						    key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
						    labelposition="top"  
						    id="fechaIniVigenciaEndoso"
						    size="10" 
						    readonly="true"
						    name="fechaIniVigenciaEndoso" 
						    disabled="true"/>    
			        </td>                           	    			
		        </tr>								   
	         </table>
          </td>
          <td align="right">               
              <div>
                  <div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div> 
              </div>            
          </td>
      </tr>        
   </table>		

</s:form>	
</div>
<s:form id="cotizacionForm">
    <s:hidden name="cotizacion.idToCotizacion" value="%{cotizacion.continuity.numero}"/>
</s:form>
<div align="center" style="width: 100%;">  
<s:if test="tipoEndoso == @mx.com.afirme.midas.solicitud.SolicitudDTO@CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL">
<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
	<s:param name="idToPolizaName">polizaId</s:param>		
	<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
	<s:param name="idAccionEndosoName">accionEndoso</s:param>
	<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL}"></s:param>
	<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
</s:action>
</s:if>
<s:else>
<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
	<s:param name="idToPolizaName">polizaId</s:param>		
	<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
	<s:param name="idAccionEndosoName">accionEndoso</s:param>
	<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL}"></s:param>
	<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
</s:action>
</s:else>                                                                       


</div>    
<div id="spacer2" style="height: 40px"></div>
<div align="right">
    <table>
        <tr>           
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150" >                			
					<div class="btn_back w140" >
						<s:if test="#soloConsulta">
							<a href="javascript: void(0);" onclick="cancelar(false,tipoEndoso.value);">	
									<s:text name="midas.boton.cancelar"/>	
							</a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" onclick="cancelar(true,tipoEndoso.value);">	
									<s:text name="midas.boton.cancelar"/>	
							</a>
						</s:else>
		             </div>
                </div>
            </td>
            
             <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
              <td>
	             <div id="divCobranzaBtn" class="w250" style="float:left;">
					<div class="btn_back w240">
					  <a href="javascript: void(0);" 
						onclick="imprimirDesgloseCoberturas(tipoEndoso.value);">
						<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.imprimirDesgloseCoberturas" />
					  </a>
				    </div>
	             </div>
	           </td>
             </s:if>
            <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">	                      
	            <td>
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir(tipoEndoso.value);}">	
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir"/>	
									</a>
			                    </div>
	                </div>
	            </td>
            </s:if>
            <td>
              <s:if test="!#soloConsulta">      
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
						<div class="btn_back w140" >
							<a href="javascript: void(0);" onclick="cotizar(tipoEndoso.value);">	
								<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
							</a>
			            </div>
	                </div>
              </s:if>
            </td>
            
        </tr>    
    </table>       
</div>