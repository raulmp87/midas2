<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/definirSolicitudEndoso.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endososPerdidaTotal.js'/>"></script>
<script type="text/javascript">
	var cotizarEndBajaIncisoPTActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaIncisoPerdidaTotal"/>';
	var cancelarEndBajaIncisoPTActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaIncisoPerdidaTotal"/>';
	var cotizarEndCancPolizaPTActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal"/>';
	var emitirEndBajaIncisoPTActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaIncisoPerdidaTotal"/>';
	var emitirEndCancPolizaPTActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal"/>';
	var cancelarEndCancPolizaPTActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal"/>';
	var imprimirDesgCoberturasEndBajaIncisoPTActionPath = '<s:url action="imprimirDesgloseCoberturas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaIncisoPerdidaTotal"/>';
    var imprimirDesgCoberturasEndCancIncisoPTActionPath = '<s:url action="imprimirDesgloseCoberturas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal"/>';
</script>