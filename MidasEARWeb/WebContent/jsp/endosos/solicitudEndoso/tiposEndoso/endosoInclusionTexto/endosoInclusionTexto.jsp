<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoInclusionTexto.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
.b_submit:disabled {
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;	
}
</style>


 
<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionTexto.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 

<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 7pt;" >
	    <tr>
	        <td><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud"/>:&nbsp;<s:text name="biCotizacion.value.solicitud.numeroSolicitud"/></td>
	        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="biCotizacion.value.numeroEndoso"/></td>	        
	    </tr>
	    <tr height="15px"></tr>	       	
	</table>
</div>

<s:form id="endosoInclusionTextoForm" >
<s:hidden name="polizaId"/>
<s:hidden name="fechaIniVigenciaEndoso"/>
<s:hidden name="accionEndoso"/>
<s:hidden name="biCotizacion.value.solicitud.idToSolicitud"/>
<s:hidden name="biCotizacion.continuity.id"/>
<s:hidden name="actionNameOrigen"/>
<s:hidden name="namespaceOrigen"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>

   <table width="98%">
      <tr>
          <td valign="top">
              <table id="agregar"  style="border: #000000;" width="98%">
		    <tr>
		        <td>
		        	<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"
					labelposition="top"  
					size="10" readonly="true"
					name="fechaIniVigenciaEndoso" disabled="true" />  
			    </td>			    
			    

		    </tr>								   
	    </table>
          </td>
     
      </tr>
      
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.title"/>
			</td>
		</tr>	
		<tr>
			<td colspan="8" width="98%">
				<div id="contenido_textosAdicionalesGrid" style="width: 98%; height: 250px; background-color: white; overflow: hidden;" ></div>
			</td>
		</tr>
		 <s:if test="!#soloConsulta">
			<tr>		   
				<td colspan="5">				
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: agregarTexto();"><s:text
								name="midas.boton.agregar" />
						</a>
					</div>				
				</td>
				<td colspan="6">				
					<div id="b_borrar">
						<a href="javascript: void(0);"
							onclick="javascript: eliminarTexto();"><s:text name="midas.boton.borrar"/></a>
					</div>				
				</td>
			</tr>		
		<tr>
			<td colspan="6">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.leyenda"/>
			</td>
		</tr>
      </s:if>
      <tr>
          <td>
          </td>
		    <td align="left">
		    <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getAltaIncisoEndosoCot()">
			     <div id="divfguardBtn" class="w150" style="float:left;">
					<div class="btn_back w140" style="display: inline; float: right;">
								<a href="javascript: void(0);" onclick="cotizar(1);">
									<s:text name="midas.boton.guardar" /> </a>
				    </div>
	             </div>	
	           </s:if>  
	           <s:else>
	           		<div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>
	              <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
		             <div id="divEmitirBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
					    </div>
		             </div>	
	              </s:if>
	              <s:if test="!#soloConsulta">
		              <div id="divBuscarBtn" class="w150" style="float:left;">
					      <div class="btn_back w140" style="display: inline; float: right;">
						      <a href="javascript: void(0);" onclick="cotizar(2);">
							      <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" /> </a>
					      </div>
		              </div>	
	             </s:if>
	           </s:else>          																	
		    </td>
	  </tr>	  
   </table>		
</s:form>

<script  type="text/javascript">
	iniciaGridTextos();
</script>

<script type="text/javascript">
	$(document).ready(function () {
		setTimeout("habilitaEliminar()",1000);
	});
</script>
