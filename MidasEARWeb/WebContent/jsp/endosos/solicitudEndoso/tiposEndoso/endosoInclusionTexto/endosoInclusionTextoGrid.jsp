<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
			    <call command="enableDragAndDrop"><param>true</param></call>
			</s:if>
		</beforeInit>
		<column id="biTexAdicionalCot.continuity.id" type="ro" width="*" sort="int" hidden="true" >continuity.id</column>
		<s:if test="accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
				<column id="biTexAdicionalCot.value.descripcionTexto" type="ed" width="300" sort="str" ><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional"/></column>
		</s:if>
		<s:else>
				<column id="biTexAdicionalCot.value.descripcionTexto" type="ro" width="300" sort="str" ><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional"/></column>
		</s:else>
		<column id="biTexAdicionalCot.value.nombreUsuarioCreacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.solicitadoPor"/></column>
		<column id="biTexAdicionalCot.value.codigoUsuarioAutorizacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizadoPor"/></column>
		<column id="biTexAdicionalCot.value.descripcionEstatusAut" type="ro" width="*" sort="str"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionTexto.estatusSolicitud"/></column>
		<column id="biTexAdicionalCot.value.fechaCreacion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.fecha"/></column>
		
	</head>

	<% int a=0;%>
	<s:iterator value="textosAdicionales">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="continuity.id" escapeHtml="false" escapeXml="true" /></cell>
			<s:if test="value.claveAutorizacion == @mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot@CLAVEAUTORIZACION_AUTORIZADA">
				<cell type="ro"><s:property value="value.descripcionTexto" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:else>
				<cell><s:property value="value.descripcionTexto" escapeHtml="false" escapeXml="true"/></cell>
			</s:else>
			<cell><s:property value="value.nombreUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.codigoUsuarioAutorizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.descripcionEstatusAut" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="value.fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>