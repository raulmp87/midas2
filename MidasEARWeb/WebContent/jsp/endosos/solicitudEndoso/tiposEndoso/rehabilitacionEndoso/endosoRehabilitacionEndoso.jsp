<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso.js'/>"></script>

<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';
	var obtenerEndososPath = '<s:url action="obtenerEndosos" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';
	var obtenerEndososPaginadasPath = '<s:url action="obtenerEndososPaginado" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';	
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.rehabilitacionEndoso.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 7pt;" >
	    <tr>
	        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;
	        <s:text name="siguienteEndoso"/></td>	        
	    </tr>
	    <tr height="15px"></tr>	       	
	</table>
</div>
<s:form id="solicitudPolizaForm">
<s:hidden name="polizaId" />
	<s:hidden name="accionEndoso" />
	<s:hidden name="cotizacionContinuityId" />
	<s:hidden name="siguienteEndoso" />
   <table>
      <tr>
          <td>
              <table id="agregar" style="border: #000000;" width="98%">
					<tr>
						<td> 
						<s:if test="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}">
							<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
								key="midas.general.fechaInicioVigenciaEndoso"
								labelposition="top"  
								size="10" readonly="true"
							name="fechaIniVigenciaEndoso" disabled="true"/>  
						</s:if>
						<s:else>
							<sj:datepicker  name="fechaIniVigenciaEndoso" key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso" 
                                   required="true" cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               id="fechaIniVigenciaEndoso" maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
					               >
							</sj:datepicker> 
						</s:else>
						
						
						</td>  
					</tr>
				</table>
          </td>
		<td align="right">
			<div>
				<div id="cargaResumenTotales"></div>
				<div id="resumenTotalesCotizacionGrid">
					<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
				</div>
			</div>
		</td>

		</tr>
      <tr height="50px">
          <td colspan="2">
          	<div class="titulo" style="width: 98%;">
				<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.listaEndosos" />			
			</div>
			
			<div id="tablaCentral" class=detalle>
				<!-- Tabla   -->
				<div id="indicador"></div>
				<div id="gridEndososListadoPaginado">
					<div id="cotizacionEndososListadoGrid"
						style="width: 98%; height: 130px"></div>
					<div id="pagingArea"></div>
					<div id="infoArea"></div>
				</div>
			</div>
		</td>
      </tr>
      <tr>
          <td>
          </td>
		    <td align="left">
			    <div id="divLimpiarBtn" style="float:left;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>	
	              <s:if test="%{accionEndoso !=@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}">
		             <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
			             <div id="divBuscarBtn" class="w150" style="float:left;">
							<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}">
											<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
						    </div>
			             </div>	
		             </s:if>
		             <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="cotizar();" >	
							    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
						    </a>
		                </div>
		             </div>
	              </s:if>																	
		    </td>
	  </tr>	  
   </table>		
</s:form>

<script type="text/javascript">
	iniciaLlenadoEndosos();
</script>

