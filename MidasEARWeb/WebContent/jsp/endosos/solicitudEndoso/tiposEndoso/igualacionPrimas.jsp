<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script
	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas.js'/>"></script>

		
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	function ejecutarIgualacion(soloNegativos) {
		var primaNetaIgualar = jQuery('#primaAIgualar').val().trim();

		if(soloNegativos && primaNetaIgualar != '' && primaNetaIgualar >= 0){
			alert("La prima total a igualar debe ser menor que 0.", "20", null);
			return;
		}
		if (primaNetaIgualar != '') {
			if (confirm('\u00BFEst\u00e1 seguro que desea igualar la prima de la cotizaci\u00F3n\u003F')) {
				var url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas/igualarPrima.action';
				jQuery('#primaAIgualar').val(
						parent.removeCurrency(jQuery('#primaAIgualar').val()));
				var form = jQuery('#igualarPrimasForm');
				parent.redirectVentanaModal('igualarPrimas', url, form);
			}
		} else {
			alert(
					"Favor de capturar la prime neta a igualar.", "20", null);
		}
	}

	function eliminarIgualacion() {
		if (confirm('\u00BFEst\u00e1 seguro que desea restaurar la prima de la cotizaci\u00F3n\u003F')) {
			var form = jQuery('#igualarPrimasForm');
			var url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas/revertirIgualacionPrima.action';
			parent.redirectVentanaModal('igualarPrimas', url, form);
		}
	}
</script>
<s:form  id="igualarPrimasForm">
	<s:hidden name="polizaId"  id ="polizaId"/>
	
	<div id="detalle" style="overflow:auto;height: 150px" >
		<center>
			<table id="t_riesgo" width="100%">
				<tr>
					<th colspan="2">
						<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.title" /> 
					</th>				
				</tr>
				<tr><td>&nbsp;</td></tr>	 
				<tr>				
					<td colspan="2">
						<s:textfield name="primaAIgualar" 
						   required="#requiredField"
			               key="midas.suscripcion.cotizacion.igualacionPrimas.primaTotal"
				           labelposition="left" 				          				 							  
						   id="primaAIgualar" maxlength="10" cssClass="txtfield jQrestrict jQrequired"	cssStyle="width: 200px;" 						   								  
						   onkeypress="return soloNumeros(this, event, true,false,6,4)">
						</s:textfield>		
					</td>
				</tr>   
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="right">			
						<div id="aceptar" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="ejecutarIgualacion(<s:property value="soloNegativos" />);"> <s:text
								name="midas.boton.aceptar" /> </a>
						</div>	
						<div id="aceptar" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="eliminarIgualacion();"> <s:text
								name="midas.suscripcion.cotizacion.igualacionPrimas.eliminar" /> </a>
						</div>								
					</td>
				</tr>
			</table>
		</center>	
	</div>
</s:form>