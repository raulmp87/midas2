<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>	
<script type="text/javascript" src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'></script>	

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCambioAgente.js'/>"></script>


<script type="text/javascript">
	var emitirActionPath =  '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';	
	var cancelarActionPath =  '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var seleccionarAgentePath = '<s:url action="ventanaAgentes" namespace="/suscripcion/cotizacion/auto"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var buscarAgentePath = '<s:url action="buscarAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var listaAgentes = new Array();
	<s:if test="#{listaAgentes != null && listaAgentes.size == 0}">
	<s:iterator value="listaAgentes" var="agenteMap" status="index" >
		listaAgentes[${index.count - 1}] = { "label": "${agenteMap.label}", "id": "${agenteMap.id}" } ;
	</s:iterator>
	</s:if>
	
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
</style>

<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.titulo"/>&nbsp;-&nbsp;P&oacuteliza(<s:text name="poliza.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: right;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>&nbsp;:<s:text name="cotizacion.value.numeroEndoso" /></div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="emitir" id="endosoCambioAgenteForm" >
    <s:hidden name="polizaId"/>
	<s:hidden name="cotizacion.value.numeroEndoso"/>
	<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
	<s:hidden name="cotizacion.continuity.id"/>
	<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso"/>
	<s:hidden name="fechaIniVigenciaEndoso" value="%{fechaIniVigenciaEndoso}"/>
	<s:hidden name="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen"/>
    <s:hidden name="accionEndoso" id="accionEndoso"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>   
    
		<table id="agregar" style="border: #000000;" width="98%">
		    <tr>
		        <td>
		        	<s:label><s:text name="midas.general.fechaInicioVigenciaEndoso"></s:text>:</s:label>
		        	<s:date name="fechaIniVigenciaEndoso" format="dd/MM/yyyy"/>							    
			    </td>			
		    </tr>
		    <tr>
			    <td>
			    	<s:property value="cotizacion.value.solicitud.nombreAgente"/>-<s:property value="codigoAgenteOriginal"/>
			    </td>				        
			</tr>
			<tr>
			    <td>
	            	<s:hidden id="idAgenteCot" name="cotizacion.value.solicitud.codigoAgente"/>
	                <s:if test="!#soloConsulta"> <font color="#FF6600">*</font></s:if> <label for="agenteNombre" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.agenteACambiar" ></s:text>:</label>
	                <br/>
	                <s:if test="!#soloConsulta">
	                    <input value="${nombreAgente}" class="txtfield" style="width: 300px" type="text" id="nombreAgente" name="nombreAgente"/>
	                </s:if>
	                <s:else>
	                    <input value="${nombreAgente}" class="txtfield" style="width: 300px" type="text" id="nombreAgente" name="nombreAgente"  disabled="disabled"/>
	                </s:else> 
	                <s:if test="!#soloConsulta">
	                    <img src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" 
			             onclick ="$('#nombreAgente').val(''); $('#idAgenteCot').val('');"/>   
	                </s:if>            	
		            		           			       
			    </td>					        
			</tr>
			<tr>
			    <td>
			    <s:hidden name="cotizacion.value.solicitud.comentarioList[0].solicitudDTO.idToSolicitud"/>
			    <s:textarea key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.motivoCambio" cols="60" 
			    			maxlength="500"
			    			name="cotizacion.value.solicitud.comentarioList[0].valor"
							labelposition="top"
							required="true"
							rows="8"
							cssClass="textarea" 									
						    id="motivoCambio"
						    disabled="#soloConsulta"
						   /> 
			    </td>					        
			</tr>					
			<tr>
			    <td align="left">
				    <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="cancelar();" >	
							    <s:text name="midas.boton.cancelar"/>	
						    </a>
	                       </div>
	                  </div>
	                  <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">	
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitirEndoso();}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitirEndoso" /> </a>
					    </div>
	                 </div>	
	                  </s:if>
	                   <s:if test="!#soloConsulta">	                   
		                   <div id="divLimpiarBtn" style="float:left;" class="w150" >
						      <div class="btn_back w140" >
							     <a href="javascript: void(0);" onclick="TipoAccionDTO.getEditarEndosoCot(cotizarEndosoCambioAgente);" >	
								    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
							     </a>
			                  </div>
			               </div>	
		              </s:if>																	
			    </td> 
		    </tr>								   
	    </table>
</s:form>	

