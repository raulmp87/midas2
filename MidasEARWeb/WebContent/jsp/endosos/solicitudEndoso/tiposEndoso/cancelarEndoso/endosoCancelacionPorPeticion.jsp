<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head />
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript"
	src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"
	charset="ISO-8859-1"></script>

<script
	src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionPorPeticion.js'/>"></script>


<s:form id="variablesEndosos"> 
<s:hidden name="polizaId" id ="polizaId"/>
<s:hidden name="actionNameOrigen" id="actionNameOrigen" />
<s:hidden name="namespaceOrigen" id="namespaceOrigen" />
<s:hidden name="isCancelacionWindow" id="isCancelacionWindow" />
<s:hidden name="redirectPageEndoso" id="redirectPageEndoso" />
</s:form>
<script type="text/javascript">
	initEndosoCancelacionPage();
</script>

<div class="titulo" style="width: 98%;">
	<s:text name="tituloPage" />
	<s:text name="-" />
	<s:text
		name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza" />
	<s:text name=":" />
	<s:text name="noPolizaFormateado" />
</div>


<div align="right" style="width: 98%; font-weight: bold">
	<s:text
		name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" />
	<s:text name=":" />
	<s:text name="noEndosoFormateado" />
</div>

<hr style="color: #a0e0a0; width: 98%;" />


<div align="right" style="height: 210px; width: 98%">

	<jsp:include page="../../totalesEndoso.jsp" />
</div>


<div class="titulo" style="width: 98%;">
	<s:text
		name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.listaEndosos" />

</div>

<div id="tablaCentral" class=detalle>
	<!-- Tabla   -->
	<div id="indicador"></div>
	<div id="gridEndososListadoPaginado">
		<div id="cotizacionEndososListadoGrid"
			style="width: 98%; height: 130px"></div>
		<div id="pagingArea"></div>
		<div id="infoArea"></div>
	</div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<s:form>
		<table
			style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
			<tr>
				<td>
					<div id="divBuscarBtn" align="left" class="w150"
						style="float: left;">
						<div class="btn_back w140" style="display: inline; float: left;">
							<a href="javascript: void(0);" onclick="cancelarEndosos();"> <s:text
									name="midas.boton.cancelar" /> </a>
						</div>
					</div>
				</td>
				<td>
					<div id="divBuscarBtn" align="left" class="w150"
						style="float: left;">
						<div class="btn_back w140" style="display: inline; float: left;">
							<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitirEndosos();}"> <s:text
									name="midas.suscripcion.cotizacion.complementar.emitir" /> </a>
						</div>
					</div>
				</td>
				<td>
					<div id="divBuscarBtn" align="left" class="w150"
						style="float: left;">
						<div class="btn_back w140" style="display: inline; float: left;">
							<a href="javascript: void(0);" onclick="cotizarEndosos();"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" />
							</a>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</s:form>
</div>



<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	iniciaLlenadoEndosos();
</script>

