<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>


<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="noEndoso" type="ro" width="0" sort="int"  ></column>
	<column id="selected" type="ch" width="50" sort="int"  >#master_checkbox</column>
		<column id="noEndoso" type="ro" width="150" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" /></column>
		<column id="fechaEmision" type="ro" width="150" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.fechaEmision" /></column>
		<column id="tipoMovimiento" type="co" width="170" sort="int" ><s:text name="midas.endosos.cotizacionEndosoListado.tipoEndoso" />
			<option value="0">EN PROCESO</option>
			 <option value="1">ORDEN DE TRABAJO</option>     
			 <option value="2">TERMINADA</option>
			 <option value="3">PENDIENTE</option>
		
		</column>
		<column id="primaEndoso" type="ro" width="150" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.primaEndoso" /></column>
		<column id="comentarios" type="ro" width="221" sort="int" ><s:text name="midas.negocio.bonocomision.sobrecomision.comentarios" /></column>
		
		<column id="consultar" type="img" width="30" align="center"></column>	
	</head>
     
	<s:iterator value="model.endosos" status="stats">
			<row id="<s:property value='idComposicion'/>"   style="<s:property value='styleRow'/>">
			
			<cell><s:property value="noEndoso" escapeHtml="false" escapeXml="true" /></cell>
			
			<cell><s:property value="selected" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noEndosoFormateado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaEmision" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoMovimiento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaEndosoFormateado" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="comentarios" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: mostrarMsg(<s:property value="noEndosoFormateado"/>)^_self</cell>
	
		</row>
	</s:iterator>	
	
</rows>