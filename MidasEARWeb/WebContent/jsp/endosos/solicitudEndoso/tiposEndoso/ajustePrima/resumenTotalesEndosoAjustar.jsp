<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!-- <table id="t_riesgo" border="0"  width="98%" style="padding: 0px; margin: 0px;"> -->
<table id="t_riesgo" style="padding: 0px; margin: 0px; border: none;"  width="280">
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Total de Primas"/></th>
		<td width="112" class="txt_v" align="right"> 
			<div id="primaNetaCoberturas">
				<s:text name="struts.money.format">
					<s:param name="resumenCostosEndosoAjustar.totalPrimas" value="resumenCostosEndosoAjustar.totalPrimas"/>
				</s:text>			
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Descuentos / Comis Ced."/></th>
		<td width="112" class="txt_v" align="right">
			<div id="descuentoComisionCedida">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosEndosoAjustar.descuentoComisionCedida" value="resumenCostosEndosoAjustar.descuentoComisionCedida"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Prima Neta"/></th>
		<td width="112" class="txt_v" align="right">
			<div id="totalPrima">
				<s:text name="struts.money.format">
					<s:param name="resumenCostosEndosoAjustar.primaNetaCoberturas" value="resumenCostosEndosoAjustar.primaNetaCoberturas"/>    
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Recargo"/></th>
		<td width="112" class="txt_v" align="right">
			<div id="recargo">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosEndosoAjustar.recargo" value="resumenCostosEndosoAjustar.recargo"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Derechos"/></th>
		<td width="112" class="txt_v" align="right">
			<div id="derechos">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosEndosoAjustar.derechos" value="resumenCostosEndosoAjustar.derechos"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left">
				<s:text name="IVA %"/>
				<s:if test="resumenCostosEndosoAjustar.porcentajeIva != null">
					(<s:text name="resumenCostosEndosoAjustar.porcentajeIva"/>)
				</s:if>
		</th>
		<td width="112" class="txt_v" align="right">
			<div id="iva">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosEndosoAjustar.iva" value="resumenCostosEndosoAjustar.iva"/>
				</s:text>				
			</div>
		</td>
	</tr>	
	<tr>
		<th width="168" valign="top" align="left"><s:text name="Prima Total de Periodo"/></th>
		<td width="112" class="txt_v" align="right">
			<div id="primaTotal">
				<s:text name="struts.money.format">
				    <s:param name="resumenCostosEndosoAjustar.primaTotal" value="resumenCostosEndosoAjustar.primaTotal"/>
				</s:text>			
			</div>
		</td>
	</tr>		
</table>