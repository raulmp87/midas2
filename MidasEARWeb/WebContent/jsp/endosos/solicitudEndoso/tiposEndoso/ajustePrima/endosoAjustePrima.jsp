<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoAjustePrima.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/programapago/programaPago.js'/>"></script>

<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima"/>';
	var obtenerEndososPath = '<s:url action="obtenerEndosos" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima"/>';
	var obtenerEndososPaginadasPath = '<s:url action="obtenerEndososPaginado" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima"/>';	
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>

<div class="titulo" style="width: 98%;">
	<s:text	name="midas.endosos.solicitudEndoso.tiposEndoso.ajustePrima.titulo" />
	&nbsp;-&nbsp;P&oacuteliza(<s:text name="polizaDTO.numeroPolizaFormateada" />)
</div>
<div style="width: 98%; text-align: center;">
	<table width="98%"
		style="border: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 7pt;">
		<tr>
			<td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso" />:&nbsp;
				<s:text	name="siguienteEndoso" />
			</td>
		</tr>
		<tr height="15px"></tr>
	</table>
</div>
<s:form id="solicitudPolizaForm">
	<s:hidden name="polizaId" />
	<s:hidden name="accionEndoso" />
	<s:hidden name="cotizacionContinuityId" />
	<s:hidden name="siguienteEndoso" />
	<s:hidden name="tipoEndoso" />
	<s:hidden name="numeroEndoso" />
	<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>	
	<table width="98%">
		<tr>
			<td>
				<table id="agregar" style="border: #000000;" width="98%">
					<tr>
						<td align="right" width="35%">
							<div id="cargaResumenTotales"></div>
							<div id="resumenTotalesCotizacionGrid">
								<s:include value="resumenTotalesEndosoAjustar.jsp"></s:include>
							</div>
						</td>
						<td align="left" width="30%">
							<s:textfield key="midas.endosos.solicitudEndoso.tiposEndoso.ajustePrima.primaTotalIgualar"
									cssClass="txtfield jQrestrict jQfloat jQrequired"
									labelposition="left" 
									size="12"
									maxlength="12"
								    id="controlEndosoCotEAP.primaTotalIgualar" name="controlEndosoCotEAP.primaTotalIgualar" />
							
							<s:if test="!#soloConsulta">
								<s:select key="midas.suscripcion.cotizacion.esquemaCotizacion.formadePago" 
		 							      name="tipoFormaPago" id="tipoFormaPago"
		 							      labelposition="left" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="#{'1':'IGUAL POLIZA','2':'ANUAL'}"		   
									      cssClass=" txtfield"/>
							</s:if>
							<s:else>
								<s:hidden name="tipoFormaPago" />
							</s:else>
						</td>
						<td align="right" width="35%">
							<div id="resumenTotalesCotizacionAjustarGrid">
								<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr height="50px">
			<td>
				<div class="titulo" style="width: 98%;">
					<s:text
						name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.listaEndosos" />
				</div>

				<div id="tablaCentral" class=detalle>
					<!-- Tabla   -->
					<div id="indicador"></div>
					<div id="gridEndososListadoPaginado">
						<div id="cotizacionEndososListadoGrid"
							style="width: 98%; height: 130px"></div>
						<div id="pagingArea"></div>
						<div id="infoArea"></div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			
			<td align="right">
				<div id="divLimpiarBtn" style="float: left;" class="w150">
					<div class="btn_back w140">
						<a href="javascript: void(0);" onclick="cancelar();"> <s:text
								name="midas.boton.cancelar" /> </a>
					</div>
				</div>
				
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
					<div id="previoRecivos">
					<div id="divBuscarBtn" class="w150" style="float: left;">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="mostrarProgramaPagoAjusteDePrima(<s:property value="cotizacionContinuityId"/>,<s:property value="tipoFormaPago"/>,<s:property value="polizaId"/>,<s:property value="numeroEndoso"/>);"> 
								Previo de Recibos
							</a>
						</div>
					</div>
					</div>
				</s:if>
			
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
					<div id="divBuscarBtn" class="w150" style="float: left;">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){emitir();}"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" />
							</a>
						</div>
					</div>
				</s:if>
				<s:if test="!#soloConsulta"> 
					<div id="divLimpiarBtn" style="float: left;" class="w150">
						<div class="btn_back w140">
							<a href="javascript: void(0);" onclick="cotizar();"> <s:text
									name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar" />
							</a>
						</div>
					</div>
				</s:if>
			</td>
		</tr>
	</table>
</s:form>

<script type="text/javascript">
	iniciaLlenadoEndosos();
	validaEstatusRecuotificacion();
</script>