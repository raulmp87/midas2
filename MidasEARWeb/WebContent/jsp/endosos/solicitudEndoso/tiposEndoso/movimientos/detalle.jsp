<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionAuto.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/validarCambios.js'/>"></script> --%>
<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoMovimientos.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/programaPago.js"/>"></script>

<script type="text/javascript">
	var mostrarDetalleActionPath = '<s:url action="mostrarDetalle" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var terminarActionPath = '<s:url action="terminar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var mostrarIgualarActionPath = '<s:url action="mostrarIgualacionPrimas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas"/>';
		

	var verEsquemaPagoPath = '<s:url action="verEsquemaPago" namespace="/suscripcion/cotizacion/auto"/>';
	var datosVehiculoPath = '<s:url action="mostrar" namespace="/suscripcion/cotizacion/auto/complementar/inciso"/>';
	var idContratante = '<s:property value="cotizacion.value.personaContratanteId"/>';
	jQuery(document).ready(function(){
		// Valida el estado de los campos
		function validaCampos(){
			var estatusCotizacion = dwr.util.getValue('estatusCotizacion');
			if(estatusCotizacion != 10){
				$('#botonTerminar').attr("disabled", true);
			} else { 
				$('#botonTerminar').removeAttr("disabled"); 	
			}
			
			var arrayCampos = new Array({id:"comisionCedida"},{id:"formaPago"},{id:"derechos"},{id:"descuentoGlobal"},{id:"fecha"},{id:"fechados"});
			for(x in arrayCampos){
				jQuery('#' + arrayCampos[x].id).cambio(arrayCampos[x].val);
			}
			
		}
		validaCampos();	

	});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:if test="cotizacion.value.porcentajeDescuentoGlobal > 0">
		<script type="text/javascript">
			// Asigna el descuento global
			jQuery('#descuentoGlobal').val(<s:property value="cotizacion.value.porcentajeDescuentoGlobal"/>);
		</script>
</s:if>
<s:form id="cotizacionForm">	
	<div class="titulo">
		<s:text name="midas.cotizacion.no" />
		<b><s:property value="cotizacion.continuity.numero"/></b>	
	</div>
	
	<div style="width: 98%; text-align: center;">
		<table width="98%" style="border: #000000; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 7pt;" >
		    <tr>		        
		        <td align="right" colspan="2"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso"/></td>	        
		    </tr>
		    <tr height="15px"></tr>	       	
		</table>
	</div>
	<table width="98%">
		<tr>
			<td width="50%">
				<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.informacionnegocio" /></div>
				<table id="agregar">
					<tr>
						<th>
							<s:text name="midas.cotizacion.negocio" /> 
						</th>
						<td>    
							<s:property value="cotizacion.value.solicitud.negocio.descripcionNegocio"/>
						</td>
						<th>
							<s:text name="midas.cotizacion.tipopoliza" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.tipoPoliza.descripcion"/>
						</td>
					</tr>
					<tr>
						<th>
							<s:text name="midas.cotizacion.producto" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.tipoPoliza.productoDTO.descripcion"/>
						</td>
						<th>
							<s:text name="midas.general.moneda" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.moneda.descripcion"/>
						</td>
					</tr>	    
				</table>
			</td>
			<td width="50%">
				<s:action name="getInfoAgente" var="agenteInfo" namespace="/componente/agente" ignoreContextParams="true" executeResult="true" >
					<s:param name="idTcAgenteCompName">cotizacion.value.solicitud.codigoAgente</s:param>
					<s:param name="nombreAgenteCompName">agente.nombre</s:param>
					<s:param name="permiteBuscar">false</s:param>		
				</s:action>	
			</td>
		</tr>
	</table>
	<div  style="width: 98%; margin-right: .3em;">
		<s:hidden name="cotizacion.continuity.id" id="idToCotizacion"/>
		<s:hidden name= "fechaIniVigenciaEndoso" id="fechaIniVigenciaEndoso"/>
		<s:hidden name="polizaId" id="polizaId"/>
		<s:hidden name="accionEndoso" id="accionEndoso"/>
		<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso" id="tipoEndoso"/>
		<s:hidden name="cotizacion.value.solicitud.idToSolicitud"/>
		<s:hidden name="actionNameOrigen" id="actionNameOrigen"/>
		<s:hidden name="namespaceOrigen" id="namespaceOrigen"/>
		<s:hidden name="motivoEndoso" id="motivoEndoso"/>
		<s:if test="mensajeDTO.mensaje!=null">
		  <b><s:property value="mensajeDTO.mensaje"/></b>
		</s:if>
			
		<div id="esquemadePago" class="alinearBotonALaDerecha" style="display: none">
			<s:if test="cotizacion.value.tipoPoliza.claveAplicaFlotillas == 1">
			<div id="cambiosGlobales" class="btn_back w140" style="display: block; float: right;">
				<a href="javascript: void(0);" onclick="mostrarVentanaCambiosGlobales(<s:property value='cotizacion.continuity.id' />,'verDetalleCotizacionFormal(<s:property value='cotizacion.continuity.id' />)');"
					class="icon_global"> <s:text
						name="midas.cotizacion.cambiosglobales" /> </a>
	   		</div>
			</s:if>
			<div class="btn_back w170" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="verEsquemaPago();"
					class="icon_esquemaPagos"> <s:text
						name="midas.cotizacion.veresquemapago" /> </a>
			</div>
	
			<div id="calculaCotizacion" class="btn_back w140" style="display: none; float: right;">
				<a href="javascript: void(0);"
					onclick="guardarCotizacion();"
					class="icon_calcularCotizacion "> <s:text
						name="midas.cotizacion.calcularcotizacion" /> </a>
			</div>		
		</div>
	</div>
<div class="clear"></div>   
	<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" class="fixTabla">
		<tr>
			<td valign="top" width="65%">
				<table id="agregar" style="white-space: normal;">
					<tr>
						<th>
							<s:text name="midas.cotizacion.iniciovigencia" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.fechaInicioVigencia"/>
						</td>
						<th>
							<s:text name="midas.cotizacion.finvigencia" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.fechaFinVigencia"/>
						</td>
					</tr>
					<tr>
						<th>
							<s:text name="midas.cotizacion.formapago" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.formaPago.descripcion"/>
						</td>
						<th>
							<s:text name="midas.cotizacion.porcentajePagoFraccionado" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.porcentajePagoFraccionado"/>
						</td>		
					</tr>
					<tr>
						<th>
							<s:text name="midas.cotizacion.derechos" /> 
						</th>
						<td>
							 <s:select  id="derechos"  cssClass="txtfield" 
									style="width: 100px"
							     	list="derechosEndosoList"
									name="cotizacion.value.negocioDerechoEndosoId"
									headerKey=""
									disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
									headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
													
						</td>
					</tr>
					<tr>
						<th>
							<s:text name="midas.cotizacion.comisioncedida" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.porcentajebonifcomision"/>
						</td>
					</tr>
					<tr>
						<th>
							<s:text name="midas.cotizacion.fechaSeguimiento" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.fechaSeguimiento"/>
						</td>
						<th>
							<s:text name="midas.suscripcion.cotizacion.nombreProspecto" /> 
						</th>
						<td>
							<s:property value="cotizacion.value.nombreContratante"/>
						</td>
					</tr>
					<tr>
						<td>
							<div  style="font-weight:bold;">
								<s:text name="midas.cotizacion.descuentoporvolumen"/>: <s:property value="cotizacion.descuentoVolumen"/> %
							</div>	
						</td>						
					</tr>
				</table>          					   	  										
			</td>
			<td valign="top" width="35%">
				
				<div id="cargaResumenTotales"></div>
				<div id="resumenTotalesCotizacionGrid">
					<s:include value="/jsp/poliza/auto/resumenTotales.jsp"/>
				</div>
								
			</td>				
		</tr>
<!--		<tr>
			<td colspan="2">
				<div id="terminarCotizacion" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="terminarCotizacion(<s:property value='cotizacion.continuity.id'/>);"> <s:text
						name="midas.cotizacion.terminarCotizacion" /> </a>
				</div>
				<div id="guardarInciso" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="guardarCotizacion();"
						class="icon_guardar"> <s:text name="midas.cotizacion.guardar" />
					</a>
				</div>
				<div id="cargaMasiva" class="btn_back w140" style="display: none; float: right;">
						<a href="javascript: void(0);" onclick="cargaMasiva();" class="icon_cargaMasiva">	
							<s:text name="midas.cotizacion.cargamasiva"/>	
						</a>
		      	</div>
		      	<s:if test="cotizacion.value.formaPago.idFormaPago > 0 && cotizacion.value.negocioDerechoPoliza.idToNegDerechoPoliza > 0">
				 	<div id="agregarInciso" class="btn_back w140" style="display: none; float: right;">
						<a href="javascript: void(0);" onclick="agregarInciso();" class="icon_masAgregar">	
							<s:text name="midas.cotizacion.agregarinciso"/>	
						</a>
			      	</div>		
		      	</s:if>
			</td>
		</tr>    -->
	</table>
</s:form>


<div id="recargaVehiculo" style="display:none;"></div>

<div align="center" style="width: 98%;">                                                                              
<s:action name="mostrarListadoCotizacionesDinamico" var="mostrarListadoCotizacionesDinamico" namespace="/componente/incisos" ignoreContextParams="true" executeResult="true" >
	<s:param name="idToPolizaName">polizaId</s:param>		
	<s:param name="idValidoEnName">fechaIniVigenciaEndoso</s:param>	
	<s:param name="idAccionEndosoName">accionEndoso</s:param>	
	<s:param name="idTipoVista" value="%{@mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS}"></s:param>
	<s:param name="idClaveTipoEndosoName">tipoEndoso</s:param>	
</s:action>
</div>
<div id="spacer1" style="height: 40px"></div>
<div align="right">
    <table>
        <tr>
            <td>
                <div id="divLimpiarBtn" style="float:left;" class="w150">
                    <div class="btn_back w140">
                        <a href="javascript: void(0);" onclick="cancelar(); return false;">
                            <s:text name="midas.boton.cancelar"/>
                        </a>
                    </div>
                </div>
            </td>
            
		          <!-- Muestra Movimientos del endoso -->
				  <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
	             	<s:if test="mensajeDTO.visible"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarMovimientosEndoso(<s:property value="cotizacion.continuity.id"/>);">	
											<s:text name="midas.suscripcion.endoso.auto.mostrarMovimientos"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          	</s:if>
		          </s:if>	
		          
		          <!-- Igualar endoso -->
				  <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() &&
				  	!esExterno">
	             	<s:if test="mensajeDTO.visible"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarIgualarMovimientoEndoso();">	
											<s:text name="midas.cotizacion.verigualarprimas"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          	</s:if>
		          </s:if>	
            
            
            <s:if test="%{accionEndoso !=@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}">
	             <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
	             	<s:if test="mensajeDTO.visible"> 
	             	 <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="emitir();">	
											<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir"/>	
										</a>
				                    </div>
		                </div>
		            </td>
		          	</s:if>
		          </s:if>
		          
		          <!-- BOTON DE PREVIO DE RECIBOS -->
		          <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()">
	             	<s:if test="mensajeDTO.visible"> 
	             	 <td>
	             	   <div id="previoRecivos">
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="mostrarProgramaPagoEndoso(<s:property value="cotizacion.value.solicitud.idToSolicitud"/>,<s:property value="cotizacion.continuity.id"/>,<s:property value="polizaId"/>,'E');">	
											Previo de Recibos	
										</a>
				                    </div>
		                </div>
		              </div>
		            </td>
		          	</s:if>
		          </s:if>
		         	          
		          
		            <td>
		                <div id="divLimpiarBtn" style="float:left;" class="w150" >
									<div class="btn_back w140" >
										<a href="javascript: void(0);" onclick="cotizar();">	
											<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
										</a>
				                    </div>
		                </div>
		            </td>   
			 </s:if>                          
                   
        </tr>    
    </table>
</div>
<script type="text/javascript">
validaEstatusRecuotificacion();
</script>
