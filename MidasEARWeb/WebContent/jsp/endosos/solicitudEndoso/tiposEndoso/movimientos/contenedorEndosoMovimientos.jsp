<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoMovimientos.js'/>"></script>

<script type="text/javascript">
	var mostrarDetalleActionPath = '<s:url action="mostrarDetalle" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';	
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<s:form id="contenedorEndosoMovimientosForm" >
<s:hidden name="polizaId"/>
<s:hidden name="fechaIniVigenciaEndoso"/>
<s:hidden name="accionEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
</s:form>

<div hrefmode="ajax-html" style="height: 450px; width: 920px" id="cotizacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="150px" id="detalle" name="<s:text name="midas.suscripcion.cotizacion.auto.cotizar.generaCotizacion" />" href="http://void" extraAction="javascript: verDetalleEndosoMovimientos();">
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</div>
</div>

<script type="text/javascript">
	dhx_init_tabbars();
</script>

