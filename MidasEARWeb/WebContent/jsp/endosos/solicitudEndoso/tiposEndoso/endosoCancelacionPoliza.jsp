<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionPoliza.js'/>"></script>
<script type="text/javascript">
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
	var previsualizarCobranzaActionPath = '<s:url action="mostrarPrevisualizarCobranza" namespace="/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCobranza"/>';	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
 <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
    <s:set var="soloConsulta" value="true"/>
</s:if>
<s:else>
    <s:set var="soloConsulta" value="false"/>
</s:else>
<s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
	<s:set var="habilitar" value="true"/>
</s:if>
<s:else>
    <s:set var="habilitar" value="false"/>
</s:else>
<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPoliza.titulo"/>&nbsp;-&nbsp;P&oacuteliza
(<s:text name="polizaDTO.numeroPolizaFormateada"/>)</div> 
<div style="width: 98%; text-align: center;">
	<table width="98%" style="border: #000000;
					                       font-family: Verdana,Arial,Helvetica,sans-serif;
	                                       font-size: 7pt;" >
		    <tr>
		        <td><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud"/>:&nbsp;<s:text name="cotizacion.value.solicitud.numeroSolicitud" /></td>
		        <td align="right"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroEndoso"/>:&nbsp;<s:text name="cotizacion.value.numeroEndoso" /></td>	        
		    </tr>
		    <tr height="15px"></tr>	       	
	</table>
</div>
<s:form action="mostrar"  id="solicitudPolizaForm" cssClass="" >
   <table>
      <tr>
          <td valign="top">
              <table id="agregar"  style="border: #000000;" width="98%">
		    	<tr>
			        <td>
			        	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso"/>:&nbsp;
						<s:date name="fechaIniVigenciaEndoso" format="dd/MM/yyyy"/>
				    </td>			
			    </tr>
				<tr>
					<td>
						<s:if test="!esExterno">
							<s:text name="midas.suscripcion.cotizacion.agentes.igualacionPrimas"/>/&nbsp;
							<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.primaTotal"/>:&nbsp;
							<s:textfield id="primaTotalIgualar"  name="primaTotalIgualar" disabled="#habilitar"  />
							<s:hidden id="primaTotalPoliza"  name="primaTotalPoliza"/>
						</s:if>
					<td>
				</tr>
		   	 </table>
          </td>
          <td>               
              <div align="right">              
              	<s:include value="/jsp/poliza/auto/resumenTotales.jsp"></s:include>
              </div>            
          </td>
      </tr>
      <tr height="160px">
          <td></td>
          <td></td>
      </tr>
    </table>
   <table id="agregar"  style="border: #000000;" width="98%">
      <tr>
		    <td colspan="2">
			    <div id="divLimpiarBtn" style="float:right;" class="w150" >
				    <div class="btn_back w140" >
					    <a href="javascript: void(0);" onclick="cancelar();" >	
						    <s:text name="midas.boton.cancelar"/>	
					    </a>
	                      </div>
	             </div>	
	              <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && cotizacion.value.importeNotaCredito > 0">
		             <div id="divCobranzaBtn" class="w150" style="float:right;">
						<div class="btn_back w140" style="display: inline; float: right;">
						  <a href="javascript: void(0);" 
							onclick="previsualizarCobranza(dwr.util.getValue('cotizacion.continuity.id'),dwr.util.getValue('fechaIniVigenciaEndoso'),dwr.util.getValue('accionEndoso'),dwr.util.getValue('cotizacion.value.solicitud.claveTipoEndoso'));">
							<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.previsualizarCobranza" />
						  </a>
					    </div>
		             </div>	
	             </s:if>
	               
	             <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && !#soloConsulta">
		             <div id="divBuscarBtn" class="w150" style="float:right;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="if(confirm('\u00BFSolicitar Emisi\u00F3n?')){TipoAccionDTO.getEditarEndosoCot(emitir);}">
										<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.emitir" /> </a>
					    </div>
		             </div>	
	             </s:if>
	             <s:elseif test="!#soloConsulta">
		             <div id="divLimpiarBtn" style="float:right;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="TipoAccionDTO.getEditarEndosoCot(cotizar);" >	
							    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.cotizar"/>	
						    </a>
		                      </div>
		             </div>	
	             </s:elseif>																		
		    </td>
	  </tr>	  
   </table>		
   <s:hidden name="accionEndoso" id="accionEndoso"/>
	<s:hidden name="polizaId"/>
	<s:hidden name="fechaIniVigenciaEndoso" />
	<s:hidden name="idToSolicitud" />
	<s:hidden name="cotizacion.value.solicitud.idToSolicitud"></s:hidden>
	<s:hidden name="cotizacion.value.solicitud.claveTipoEndoso"></s:hidden>
	<s:hidden name="cotizacion.continuity.id"/>
	<s:hidden name="actionNameOrigen"/>
	<s:hidden name="namespaceOrigen"/>
	<s:hidden name="tipoEndoso" id="tipoEndoso"/>
	<s:hidden name="motivoEndoso" id="motivoEndoso"/>
	<s:hidden name="cotizacion.value.importeNotaCredito"/>
</s:form>	
