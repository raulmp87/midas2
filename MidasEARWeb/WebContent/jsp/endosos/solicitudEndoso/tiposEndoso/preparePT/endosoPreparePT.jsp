<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript">
	var bloquearMigracionSeycosPath =  '<s:url action="bloquearMigracionSeycos" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var igualarEndosoMidasPath =  '<s:url action="igualarEndosoMidas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var migrarEndosoMidasPath =  '<s:url action="migrarEndosoMidas" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var regeneraPrimaReciboPath =  '<s:url action="regeneraPrimaRecibo" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var regeneraPrimaReciboListaPath =  '<s:url action="regeneraPrimaReciboLista" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var regeneraReciboPath =  '<s:url action="regeneraRecibo" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var setLlaveFiscalPath =  '<s:url action="setLlaveFiscal" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var reprocesaEndosoSeycosPath =  '<s:url action="reprocesaEndosoSeycos" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var migrarEndosoCFPPath =  '<s:url action="migrarEndosoCFP" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	var generaReciboEndosoPath =  '<s:url action="generaReciboEndoso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT"/>';
	
	function bloquearMigracionSeycos(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea bloquear la emision de este endoso?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, bloquearMigracionSeycosPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}		
	}	
	
	function igualarEndosoMidas(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea igualar la emision de este endoso?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, igualarEndosoMidasPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function migrarEndosoMidas(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea igualar y migrar la emision de este endoso?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, migrarEndosoMidasPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function regeneraPrimaRecibo(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea regenerar la prima del recibo?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, regeneraPrimaReciboPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function regeneraPrimaReciboLista(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea regenerar la prima de la lista de recibos?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, regeneraPrimaReciboListaPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}

	function regeneraRecibo(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea regenerar el recibo?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, regeneraReciboPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function setLlaveFiscal(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea agregar la Llave Fiscal?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, setLlaveFiscalPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function reprocesaEndosoSeycos(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea reprocesar endoso seycos?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, reprocesaEndosoSeycosPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function migrarEndosoCFP(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea migrar solo Endoso?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, migrarEndosoCFPPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
	
	function generaReciboEndoso(){
		var mensaje = "\u00BFEst\u00E1 seguro que desea generar recibos endoso?";
		if(confirm(mensaje))
		{
			sendRequestJQ(null, generaReciboEndosoPath+"?"+jQuery(document.endosoPreparePTForm).serialize(), targetWorkArea,null);				
		}	
	}
</script>



<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.titulo"/></div> 
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="mostrarPreparePT" id="endosoPreparePTForm" >    
		<table id="agregar" style="border: #000000;" width="98%">
			<tr>
				<td>IdMidas</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="idMidas" name="idMidas"/>            	     			       
			    </td>
			    <td>pllaveFiscal</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pllaveFiscal" name="pllaveFiscal"/>            	     			       
			    </td>
			</tr>
			<tr>
				<td>pId_Cotizacion</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pId_Cotizacion" name="pId_Cotizacion"/>            	     			       
			    </td>
			    <td>pId_Solicitud_Canc</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pId_Solicitud_Canc" name="pId_Solicitud_Canc"/>            	     			       
			    </td>
			</tr>
			<tr>
				<td>pid_version_pol</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pIdVersionPol" name="pIdVersionPol"/>            	     			       
			    </td>
			    <td>pid_version_polCanc</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pIdVersionPolCanc" name="pIdVersionPolCanc"/>            	     			       
			    </td>
			    <td>pcve_t_endoso</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pCveTEndoso" name="pCveTEndoso"/>            	     			       
			    </td>
			    <td>pCveL_T_Folio_CE</td>
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pCveLTFolioCE" name="pCveLTFolioCE"/>            	     			       
			    </td>
			</tr>
			<tr>
			    <td align="left" colspan="6">	
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="bloquearMigracionSeycos();">
										<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.bloquearEndoso" /> </a>
					    </div>
	                 </div>
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="migrarEndosoMidas();">
										<s:text name="midas.suscripcion.cotizacion.complementar.enviaraemision" /> </a>
					    </div>
	                 </div>
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="setLlaveFiscal();">
										<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.setLlaveFiscal" /> </a>
					    </div>
	                 </div>
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="reprocesaEndosoSeycos();">
										<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.reprocesaEndosoSeycos" /> </a>
					    </div>
	                 </div>
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="migrarEndosoCFP();">
										<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.migrarEndosoCFP" /> </a>
					    </div>
	                 </div>
	                  <div id="divBuscarBtn" class="w150" style="float:left;">
						<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="generaReciboEndoso();">
										<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.generaReciboEndoso" /> </a>
					    </div>
	                 </div>
			    </td> 
		    </tr>								   
	    </table>
		<table id="agregar" style="border: #000000;" width="98%">
		    <tr>
			    <td>pnum_poliza:</td>				        
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pnum_poliza" name="pnum_poliza"/>            	     			       
			    </td>
			    <td>pnum_renov:</td>				        
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pnum_renov" name="pnum_renov"/>            	     			       
			    </td>
			    <td>pnum_endoso</td>				        
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pnum_endoso" name="pnum_endoso"/>            	     			       
			    </td>
			</tr>
		    <tr>
			    <td>pId_Recibo</td>				        
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pId_Recibo" name="pId_Recibo"/>            	     			       
			    </td>
			    <td>pId_RecibosAjusta</td>				        
			    <td>
					<input value="" class="txtfield" style="width: 100px" type="text" id="pId_RecibosAjusta" name="pId_RecibosAjusta"/>            	     			       
			    </td>
			</tr>
		    <tr>
		    	<td colspan="6">
					<div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" >
						    <a href="javascript: void(0);" onclick="regeneraRecibo();" >	
							    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.regeneraRec"/>	
						    </a>
	                       </div>
	                 </div>
		    	</td>
			</tr>
		    <tr>
			    <td>varPrimaNeta</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varPrimaNeta" name="varPrimaNeta"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varBonifComis</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varBonifComis" name="varBonifComis"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varRecargos</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varRecargos" name="varRecargos"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varBonifComisRPF</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varBonifComisRPF" name="varBonifComisRPF"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varDerechos</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varDerechos" name="varDerechos"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varIva</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varIva" name="varIva"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varPrimaTotal</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varPrimaTotal" name="varPrimaTotal"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varComisPN</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varComisPN" name="varComisPN"/>            	     			       
			    </td>					        
			</tr>
		    <tr>
			    <td>varComisRPF</td>				        
			    <td colspan="5">
					<input value="" class="txtfield" style="width: 300px" type="text" id="varComisRPF" name="varComisRPF"/>            	     			       
			    </td>					        
			</tr>
			<tr>
			    <td align="left" colspan="6">
				    <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" style="display: inline; float: right;">
						    <a href="javascript: void(0);" onclick="igualarEndosoMidas();" >	
							    <s:text name="midas.suscripcion.cotizacion.igualacionPrimas.igualar"/>	
						    </a>
	                       </div>
	                  </div>																	
				    <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" style="display: inline; float: right;">
						    <a href="javascript: void(0);" onclick="regeneraPrimaRecibo();" >	
							    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.regeneraPrima"/>	
						    </a>
	                       </div>
	                  </div>
				    <div id="divLimpiarBtn" style="float:left;" class="w150" >
					    <div class="btn_back w140" style="display: inline; float: right;">
						    <a href="javascript: void(0);" onclick="regeneraPrimaReciboLista();" >	
							    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.preparePT.regeneraPrimaLista"/>	
						    </a>
	                       </div>
	                  </div>
			    </td> 
		    </tr>								   
	    </table>	    
</s:form>	

