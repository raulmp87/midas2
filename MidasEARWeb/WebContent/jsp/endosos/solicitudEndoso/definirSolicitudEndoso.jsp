<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/endosos/solicitudEndoso/definirSolicitudEndosoHeader.jsp"></s:include>


<style type="text/css">
.b_submit:disabled{
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;	
}
</style>

<s:hidden name="actionNameOrigen"  id="actionNameOrigen"/>
<s:hidden name="namespaceOrigen"  id="namespaceOrigen"/>
<s:hidden name="poliza.idToPoliza" id="numeroPoliza"/>
<div class="titulo" style="width: 98%;"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.titulo"  /></div>
<div class="detalle">    
        <s:form>
            <div class="contenedorFiltros">
                <table>
	    <tr>
	        <td>
	            <table style="border: #000000;
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt;">
	                <tr>
	                    <td>
		                    <s:textfield cssClass="txtfield" cssStyle="width: 120px;" 
							key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza"
							labelposition="left"
							required="true"  
							size="20"
							onkeypress="return soloNumeros(this, event, false);"
						    onfocus="javascript: new Mask('####-########-##', 'string').attach(this)"
						    maxlength="16"																			
							id="idPolizaBusqueda" name="numeroPolizaFormateado" />
						</td>
	                    <td>
		                    
						    <div class="btn_back w140" >
						        <s:submit key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.validarNumero" 
						                  id="botonValidar"
						                  name="botonValidar"
						                  onclick="validar(); return false;"
								          cssClass="b_submit w140"/>						    
			               </div>			               
		                </td>
	                </tr>           
	            </table>			
	        </td>	        
	    </tr>
	</table>
            </div>
        </s:form>    
</div> 
<hr style="color:#a0e0a0;width: 98%;"/>
<div class=detalle>
    <table style="border: #000000;
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt; width:98%;">
        <tr>
            <td width="85%">
                <table style="border: #000000; 
				                       font-family: Verdana,Arial,Helvetica,sans-serif;
                                       font-size: 7pt; width:100%">
        <tr>
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza"/>:	                
            </td>
            <td>
                <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"							
							labelposition="left" 
							readonly="true" 
							size="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="poliza.numeroPolizaFormateada" />                
            </td>
            <td> 
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.contratante"/>:               
            </td>
            <td>
               <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"							
							labelposition="left" 
							readonly="true" 
							size="70"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="cotizacion.value.nombreContratante" />                
            </td>
        </tr>
        <tr>
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.vigenciaDesde"/>:	                
            </td>
            <td>
                <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"
							
							labelposition="left" 
							readonly="true" 
							size="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="cotizacion.value.fechaInicioVigencia" />                
            </td>
            <td> 
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.vigenciaHasta"/>:               
            </td>
            <td>
               <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"
							
							labelposition="left" 
							readonly="true" 
							size="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="cotizacion.value.fechaFinVigencia" />                
            </td>
        </tr>
        <tr>
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.negocio"/>:	                
            </td>
            <td>
                <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"
							
							labelposition="left" 
							readonly="true" 
							size="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="cotizacion.value.solicitud.negocio.descripcionNegocio" />                
            </td> 
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.producto"/>:	                
            </td>
            <td>
                <s:textfield cssClass="txtfield" cssStyle="width: 300px; border: none;"
							
							labelposition="left" 
							readonly="true" 
							size="10"
							onkeypress="return soloNumerosM2(this, event, false)"
							onblur="this.value=jQuery.trim(this.value)"
							id="idToCotizacion" name="cotizacion.value.solicitud.productoDTO.descripcion" />                
            </td>           
        </tr>        
    </table>
            </td>
            <td valign="top" width="15%">
            	<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza">
                <div id="divBuscarBtn" class="w150" style="float:left;">
					<div class="btn_back w140" style="display: inline; float: right;">
								<!-- function verTipoEndoso(accionEndoso) -->
								<a href="javascript: void(0);" onclick="verDetallePoliza(<s:property value="poliza.idToPoliza"/>, null, <s:property value="ultimoEndosoValidFromDateTimeStr"/>, null, <s:property value="ultimoEndosoRecordFromDateTimeStr"/>, 2, <s:property value="ultimoEndosoClaveTipoEndoso"/>, true, true);">
									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.verPoliza" /> </a>
				    </div>
	             </div>
	             </m:tienePermiso>	
            </td>
        </tr>    
        
    </table>    
</div>
<div id="tablaCentral" class=detalle>
    <!-- Tabla   -->
    <div id="indicador"></div>
			<div id="gridEndososListadoPaginado" >
				<div id="cotizacionEndososListadoGrid" style="width:98%;height:130px">
			</div>
			<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div align="left">
   <s:form>
    <table style="border: #000000;
		   font-family: Verdana,Arial,Helvetica,sans-serif;
           font-size: 7pt;">          
        <tr>
            <td>
                <s:select key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.combo.tipo" 
					      name="tipoEndoso" id="tipoEndoso"
					      required="true"
					      labelposition="left"  
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="tiposEndoso" listKey="id.idDato" listValue="descripcion" 					      	   
				          cssClass=" txtfield"/>
            </td>
        </tr>
        <tr>
            <td id="dtRow" align="left">
                <sj:datepicker     name="fechaIniVigenciaEndoso" key="midas.endosos.solicitudEndoso.tiposEndoso.cambioAgente.fechaIniVigenciaEndoso" 
                                   required="true" cssStyle="width: 170px;" 
					   			   buttonImage="../img/b_calendario.gif"
					               id="fechaIniVigenciaEndoso" maxlength="10" 	
					               labelposition="left"					               
					               cssClass="txtfield"
					               onkeypress="return soloFecha(this, event, false);"
					               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					               onblur="esFechaValida(this);"					              
					               changeMonth="true" 
					               changeYear="true"							   								  
					               >
					</sj:datepicker>   
            </td>
            
            
        </tr>
        
        <tr>
        	<td>
        		<div id="tdMotivo" style="display:none">
                <s:select key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.motivoEndoso" 
					      name="motivoEndoso" id="motivoEndoso"
					      required="true"
					      labelposition="left"  
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="motivosEndoso" listKey="id.idDato" listValue="descripcion" 					      	   
				          cssClass="txtfield"/>
				</div>
            </td>       
        </tr>
        <tr>
            <td>
                <div id="divBuscarBtn" class="w150" style="float:left;">
					<div class="btn_back w140" style="display: inline; float: left;">
								<a href="javascript: void(0);" onclick="TipoAccionDTO.getNuevoEndosoCot(verTipoEndoso);">
									<s:text name="midas.boton.continuar" /> </a>
				    </div>
	             </div>	
	             <div id="divBuscarBtn" class="w150" style="float:left;">
					<div class="btn_back w140" style="display: inline; float: left;">
								<a href="javascript: void(0);" onclick="cancelar();">
									<s:text name="midas.boton.cancelar" /> </a>
				    </div>
	             </div>	
            </td>
            
        </tr>
        
    </table>
  </s:form>
</div>
<script type="text/javascript">
   	iniciaDefinirSolicitudEndoso();
</script>