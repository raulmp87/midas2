<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script type="text/javascript">
	function cerrarVentanaMovimientosEndoso(){
		parent.cerrarVentanaModal("movimientosEndoso", null);
	}
</script>
<sj:head/>
<s:form id="accionMovimientosForm" action="/endoso/cotizacion/auto/solicitudEndoso.action" >
	<div id="contenedorFiltros" >
					<table id=t_riesgo border="0" style="width: 100%;">
						<tr>
							<th><s:text name="midas.suscripcion.endoso.auto.movimientos" /></th>
						</tr>
						<s:iterator value="listaMovimientoEndoso" status="stat">
							<tr>
								<td> 
									<s:property value="%{descripcion}" /> 
								</td>
							</tr>
						</s:iterator>
					</table>
	</div>
	<BR>
	<div style="width: 100%;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="cerrarVentanaMovimientosEndoso();"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
	</div>
</s:form>