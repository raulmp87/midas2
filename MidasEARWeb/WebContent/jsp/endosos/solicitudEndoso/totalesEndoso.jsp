<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<table>
    <tr>
        <td style="font-size: 12px;
	font-weight: bold;"><s:text name="Totales de Endoso">
</s:text></td>
        <td></td>
    </tr>
    <tr>
		<td>
			<table align="right" class="contenedorFormas">
				<tr>
					<td><s:text name="Total de Primas:" /></td>					
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="totalPrimas" value="totalPrimas" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="Descuento Comis/Ced:" /></td>
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="descuentoComis" value="descuentoComis" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="Prima Neta:" /></td>
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="primaNeta" value="primaNeta" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="Recargo:" /></td>
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="recargo" value="recargo" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="Derechos:" /></td>
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="derechos" value="derechos" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="IVA:" /></td>
					<td><s:select name="idAgenteRelacionadoPoliza" id="tipoEndoso"
							labelposition="top" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							list="endososList" listKey="id" listValue="description"
							cssClass=" txtfield" />
					</td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="iva" value="iva" />
						</s:text>
					</td>
				</tr>
				<tr>
					<td><s:text name="Prima Total de Periodo:" /></td>
					<td></td>
					<td align="right"><s:text name="struts.money.format">
							<s:param name="primaTotalPeriodo" value="primaTotalPeriodo" />
						</s:text>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<div id="divLimpiarBtn" style="float: right" class="w150">
				<div class="btn_back w140">
					<a href="javascript: void(0);" onclick="cotizar();"> <s:text
							name="Previsualizar Cobranza" /> </a>
				</div>
			</div>
		</td>
	</tr>
</table>


