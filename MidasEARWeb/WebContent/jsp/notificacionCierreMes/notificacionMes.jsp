<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/notificacionCierreMes/notificacionCierreMesHeader.jsp"></s:include>

<s:form action="mostrarAdminNotificacion" id="notificacionM"
	name="notificacionM">


	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name="midas.notificacioncm.config.tituloNotificacion" /></td>
		</tr>
		<tr>
			<th>&nbsp;<s:text
					name="midas.notificacioncm.config.fechaEjecucion" /></th>

			<td><s:textfield name="fechaEjecucion" id="txtfechaEjecucion"
					readonly="true" disabled="true" cssClass="cajaTextoM2 w100"></s:textfield></td>
			<th>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.notificacioncm.config.fecha" /></th>
			<td><sj:datepicker name="fechaNueva" id="fechaNueva"
					buttonImage="../img/b_calendario.gif" changeYear="true"
					changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" minDate="today" ><!--maxDate="today"-->
					</sj:datepicker></td>
		<tr>
			<th>&nbsp;<s:text name="midas.notificacioncm.config.proceso" /></th>
			<td><s:checkbox name="active" id="active"
					onclick="valueCheck();"
					cssClass="cajaTextoM2 w90 jQnumeric jQrestrict"></s:checkbox></td>

			<td>
		</tr>
		<td>
		</tr>

		<tr>

			<td colspan="6" align="right"></td>

			<td>
				<div class="btn_back w110">
					<a class=".icon_limpiar" onclick="infoConfigNotifica();"
						href="javascript: void(0);"> Guardar </a>
				</div>
			</td>

		</tr>

	</table>
	<br>
	<div id="divCarga" style="position: absolute;"></div>
	<div align="center" id="provisionesGrid" width="880px" height="200px"
		style="background-color: white; overflow: hidden"></div>

</s:form>
<div id="pagingArea"></div>
<div id="infoArea"></div>

