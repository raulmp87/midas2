<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript"
	src="<s:url value='/js/midas2/notificacionCierreMes/notificacionMes.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/ajaxScript.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
