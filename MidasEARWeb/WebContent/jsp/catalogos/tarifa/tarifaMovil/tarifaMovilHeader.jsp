<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script type="text/javascript">
	var cargarExcelTarifasAutosPath = '<s:url action="guardarTarifasExcel" namespace="/tarifa/tarifaMovil"/>';
	var mostrarCargaExcelTarifasAutosPath = '<s:url action="mostrarDetalleTarifaMovil" namespace="/tarifa/tarifaMovil"/>';
	var verCatalogoDescuentoAgenteMovilPath = '<s:url action="verCatalogoDescuentoAgenteMovil" namespace="/tarifa/descuentoAgenteMovil"/>';
	var verCatalogoAdministracionCotizacionMovilPath  = '<s:url action="mostrar" namespace="/tarifa/administracionCotizacionMovil"/>';
	var verCatalogoAdministracionUsuarioMovilPath = '<s:url action="mostrar" namespace="/tarifa/administracionUsuarioMovil"/>';
	var verCatalogoAdministracionCotizacionPortalPath  = '<s:url action="mostrar" namespace="/tarifa/administracionCotizacionPortal"/>';
	var verCatalogoAdministracionConsultaMovilPath  = '<s:url action="mostrar" namespace="/tarifa/administracionConsultaMovil"/>';
	//var verCatalogoAdministracionPolizasMovilPath  = '<s:url action="mostrar" namespace="/tarifa/administracionPolizasMovil"/>';
	
		var tabInfo_general=null;
		var contenedorTab=null;
		var grid=null;
		var gridEntretenimientosAgente;
		var gridHijosAgente;
		/**
		 * Funcion para inicializar tabs, si es una alta, deshabilita el resto de los tabs hasta que se guarde la primer pestania
		 */
		function cargarExcelTarifasAutos(){
			var archivotxt = jQuery("#fileUpload").val();
			if(archivotxt.indexOf(".xls")!=-1) {
				document.formularioCargaTarifasCotizacionMovil.submit();
			}
			else{
				parent.mostrarMensajeInformativo("Seleccione un archivo .xls ","10");
			}
		}
		function descargaExcelTarifasAutos(){
			var estado = jQuery("#estadoTarifaMovil").val();
			if(estado!="") {
				document.tarifaDescargaMovilForm.submit();
			}
			else{
				parent.mostrarMensajeInformativo("Seleccione un Estado","10");
			}
		}
		function incializarTabs(){
			var idAgente=jQuery("#agente\\.id").val();
			contenedorTab=window["configuracionNegocioTabBar"];
			//Nombre de todos los tabs
			var tabs=["info_general","domicilios","datosFiscales","datosContables"];
			//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
			//inicia de posicion 1 para deshabilitar desde el segundo en adelante.
			for(var i=1;i<tabs.length;i++){
				var tabName=tabs[i];
				contenedorTab.disableTab(tabName);
			}
			for(var i=0;i<tabs.length;i++){
				var tabName=tabs[i];
				var catalogoProveniente = jQuery("#moduloOrigen").val();
				var tab=contenedorTab[tabName];
				jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
					if(dwr.util.getValue("tipoAccion")==1||dwr.util.getValue("tipoAccion")==4){
						event.stopPropagation();
						parent.mostrarMensajeConfirm("Est� a punto de abandonar la secci�n, �Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
					}
					else{
						obtenerFuncionTab(event.data.value);
					}
				});
			}
		}
		function verInfoGeneral() {
				sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,
						'contenido_info_general','limpiarDivsIfo();');			
		}
		function verCatalogoAdministracionCotizacionMovil(){
			sendRequestJQ(null,verCatalogoAdministracionCotizacionMovilPath,
					'contenido_administracion_cotizacion','limpiarDivAdmiCot();');	
		}
		function verCatalogoAdministracionUsuarioMovil(){
			sendRequestJQ(null,verCatalogoAdministracionUsuarioMovilPath,
					'contenido_administracion_usuarios','limpiarDivAdmiUsu();');	
		}
		function verCatalogoDescuentoAgenteMovil(){
			var path ="/MidasWeb/tarifa/descuentoAgenteMovil/verCatalogoDescuentoAgenteMovil.action?claveNegocio=A";
			sendRequestJQ(null,path,
					'contenido_descuento_agente_movil','limpiarDivsGeneral();');	
		}
		function verCatalogoAdministracionCotizacionPortal(){
			sendRequestJQ(null,verCatalogoAdministracionCotizacionPortalPath,
					'contenido_administracion_cotizacion_portal','limpiarDivAdmiCotPor();');	
		}
		function limpiarDivAdmiCot() {
			limpiarDiv('contenido_administracion_usuarios');
			limpiarDiv('contenido_info_general');
			limpiarDiv('contenido_descuento_agente_movil');
			limpiarDiv('contenido_administracion_cotizacion_portal');
		}
		function limpiarDivAdmiUsu() {
			limpiarDiv('contenido_administracion_cotizacion');
			limpiarDiv('contenido_info_general');
			limpiarDiv('contenido_descuento_agente_movil');
			limpiarDiv('contenido_administracion_cotizacion_portal');
		}
		function limpiarDivsIfo() {
			limpiarDiv('contenido_administracion_cotizacion');
			limpiarDiv('contenido_administracion_usuarios');
			limpiarDiv('contenido_descuento_agente_movil');
			limpiarDiv('contenido_administracion_cotizacion_portal');
			jQuery("#formularioDescuentoAgenteMovil #boxCargaMasiva").hide();
		}
		function limpiarDivAdmiCotPor() {
			limpiarDiv('contenido_administracion_usuarios');
			limpiarDiv('contenido_info_general');
			limpiarDiv('contenido_descuento_agente_movil');
			limpiarDiv('contenido_administracion_cotizacion');
		}
		function obtenerFuncionTab(tab){
			var tipoOperac=50;
		  if(tab==0){
		        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
		  }else if(tab==1){
		        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
		  }else if(tab==2){
		       sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
		  }else if(tab==3){
		        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
		  }else{
		        sendRequestJQ(null,mostrarCargaExcelTarifasAutosPath,targetWorkArea,'dhx_init_tabbars();');
		  }
		}
</script>