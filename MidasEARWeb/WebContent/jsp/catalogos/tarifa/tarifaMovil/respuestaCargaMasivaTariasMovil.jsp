<%@page language="java"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<%-- <s:if test="%{#varCentroOperacion.id == configComisiones.listaCentroOperaciones[#status.index]}"> --%>
	<s:if test="{mensaje!=null}">
		<label>Mensaje:</label>
		<s:text name="mensaje"/>
		<br>
		<label>Registros Insertados:</label>
		<s:text name="registrosInsertados"/>
		<br>
		<label>Registros no Insertados:</label>
		<s:text name="registrosNoInsertados"/>
		<br>
		<label>Registros Actualizados:</label>
		<s:text name="registrosActualizados"/>
	</s:if>
	<br>
	<s:if test="{logErrores!=null}">
	<label>Log de errores</label>
		<s:text name="logErrores"/>
	</s:if>
