<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:hidden name="idNegocio" id="idNegocio" />
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltroEstados=config.contextPath + "/tarifa/tarifaMovil/mostrarEstados.action"+"?tipoAccion="+tipoAccion;
		var urlFiltroMarcas=config.contextPath + "/tarifa/tarifaMovil/mostrarMarcas.action"+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltroEstados,"configCatalogoTarifaEstadosMovilGrid", null,idField,'gerenciaModal');
	 	listarFiltradoGenerico(urlFiltroMarcas,"configCatalogoTarifaMarcasMovilGrid", null,idField,'gerenciaModal');
	 });

	 function activarEstado(desc){
		sendRequestJQ(null, config.contextPath + "/tarifa/tarifaMovil/activarEstado.action?param.descripcion=" + desc, targetWorkArea, null, "html", defaultContentType);
	 }
		
	 function desactivarEstado(desc){
		sendRequestJQ(null, config.contextPath+ "/tarifa/tarifaMovil/desactivarEstado.action?param.descripcion=" + desc, targetWorkArea, null, "html", defaultContentType);
	 }
	 function activarMarca(desc){
		sendRequestJQ(null, config.contextPath + "/tarifa/tarifaMovil/activarMarca.action?param.descripcion=" + desc, targetWorkArea, null, "html", defaultContentType);
	 }
		
	 function desactivarMarca(desc){
		sendRequestJQ(null, config.contextPath+ "/tarifa/tarifaMovil/desactivarMarca.action?param.descripcion=" + desc, targetWorkArea, null, "html", defaultContentType);
	 }
</script>
<div select="<s:property value='subTabActiva'/>" style= "height: 100%; width:100%; overflow:auto" hrefmode="ajax-html"  id="contenidoTarifasMovil" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="150%" id="cargaTarifasXLS" name="<s:text name='Cargar Tarifas'/>" href="http://void">
		<s:form  method="post" action="/tarifa/tarifaMovil/cargarTarifas.action" target="iframeProcessCargaMasivaData" enctype="multipart/form-data" 
		id="formularioCargaTarifasCotizacionMovil" name="formularioCargaTarifasCotizacionMovil"> <!-- target="&quot;cargaMasivaAgente.jsp&quot;" -->
			<table width="90%" align="center"  class="contenedorFormas no-border"><!--  -->
				<tr>
					<td>
						<s:file id="fileUpload" name="fileUpload" cssClass="cajaTextoM2 w300 jQrequired" label="Tarifas Movil"/>
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td>
						<div class="btn_back w120">
							<a href="javascript:cargarExcelTarifasAutos()" class="icon_guardar ." 
								onclick="">
								<s:text name="Cargar Archivo"/>
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="midas.agentes.cargaMasiva.resultadoCarga"/><br>
						<iframe name="iframeProcessCargaMasivaData"  height="100px" width="400px" style="border: 1; border-color: #A0E0A0;"></iframe>
					</td>
				</tr>
			</table>
		</s:form>
	</div>
	<div width="100%" id="descargraTarifasMovil" name="<s:text name='Descargar Tarifas'/>" href="http://void" >
		<s:form action="generarExcelTarifaMovil" id="tarifaDescargaMovilForm">
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="800px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="midas.movil.catalogos.tarifa.titulo"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="midas.movil.catalogos.tarifa.estado" />
					</th>
					<td width="275px">
					<s:select  name="tarifasDTO.estado" id="estadoTarifaMovil" cssClass="cajaTextoM2 w150"  
								headerKey="%{getText('midas.general.defaultHeaderKey')}" 
								headerValue="%{getText('midas.general.seleccione')}" 
								list="estados" listKey="nombreEstado" listValue="nombreEstado" />
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="descargaExcelTarifasAutos();">
								<s:text name="midas.boton.descarga"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
		</s:form>
	</div>
	<div width="100%" id="configTarifasMovilEstados" name="<s:text name='Configurar Estados'/>" href="http://void" >
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Configuracion Estados"/>
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="configCatalogoTarifaEstadosMovilGrid" class="w880 h200" style="overflow:hidden"></div>	
	</div>
	<div width="100%" id="configTarifasMovilMarcas" name="<s:text name='Configurar Marcas'/>" href="http://void" >
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Configuracion Marcas"/>
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="configCatalogoTarifaMarcasMovilGrid" class="w880 h200" style="overflow:hidden"></div>	
	</div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>




	