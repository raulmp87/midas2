<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/tarifaMovil/tarifaMovilHeader.jsp"></s:include>
<script type="text/javascript">
	jQuery(function() {
		dhx_init_tabbars();
	});
</script>
<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html"
	style="height: 380px; width: 910px; margin-left: 10px;"
	id="configuracionNegocioTabBar" class="dhtmlxTabBar"
	imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">
	<div width="100%" id="info_general" name="Tarifas Movil"
		href="http://void" extraAction="javascript: verInfoGeneral();">	
	</div>
	<div width="100%" id="descuento_agente_movil" name="Descuentos Agentes"
		href="http://void" extraAction="javascript: verCatalogoDescuentoAgenteMovil();">
	</div>
	<div width="100%" id="info_general_carga_masiva" name="Cargar Masiva Des. Agentes"
		href="http://void">	
	</div>
	<div width="100%" id="administracion_cotizacion" name="Cotizaciones"
		href="http://void">	
	</div>
	<div width="100%" id="administracion_usuarios" name="Usuarios"
		href="http://void">	
	</div>	
	<div width="100%" id="administracion_cotizacion_portal" name="Cotizaciones Portal"
		href="http://void">	
	</div>
	<div width="100%" id="administracion_consulta_movil" name="Consulta Móvil"
		href="http://void">	
	</div>	
</div>