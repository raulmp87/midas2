<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="id" type="ro" width="50" sort="int" ><s:text name="Id"/></column>
		<column id="Descripcion" type="ro" width="120" sort="str"><s:text name="Descripcion"/></column>
		<column id="accionDesactivar" type="img" width="30" sort="na" align="center"/>
       	<column id="accionActivar" type="img" width="30" sort="na" align="center"/>
	</head>
	<s:iterator value="marcaList" var="desc" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${descripcion}]]></cell>
			<cell><s:url value="/img/icons/ico_bloquear.gif"/>^<s:text 
			name="Desactivar"/>^javascript:desactivarMarca("${descripcion}")^_self</cell>
			<cell><s:url value="/img/icons/ico_aceptar.gif"/>^<s:text 
			name="Activar"/>^javascript:activarMarca("${descripcion}")^_self</cell>							
		</row>
	</s:iterator>
</rows>