<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/tarifaMovil/tarifaMovilHeader.jsp"></s:include>
<script type="text/javascript">
	jQuery(function() {
		dhx_init_tabbars();
		//jQuery("#formularioDescuentoAgenteMovil #boxCargaMasiva").hide();
	});
	function cargarExcelDescuentoAgenteMovil(){
	 	var archivotxt = jQuery("#formularioDescuentoAgenteMovil #fileUpload").val();
		if(archivotxt.indexOf(".csv")!=-1) {
			document.formularioDescuentoAgenteMovil.submit();
		}
		else if(archivotxt.indexOf(".txt")!=-1) {
			document.formularioDescuentoAgenteMovil.submit();
		}else{
			parent.mostrarMensajeInformativo("Seleccione un archivo .csv o .txt separado por pipes  | ","10");
		}
	 }
	function verBoxCargaMasiva(){
		limpiarDiv('contenido_info_general');
		limpiarDiv('contenido_administracion_cotizacion');
		limpiarDiv('contenido_administracion_usuarios');
		limpiarDiv('contenido_descuento_agente_movil');
		limpiarDiv('contenido_administracion_cotizacion_portal');
		limpiarDiv('contenido_administracion_consulta_movil');
		jQuery("#formularioDescuentoAgenteMovil #boxCargaMasiva").show();
	 }
	 
	 function verCatalogoAdministracionConsultaMovil(){
		sendRequestJQ(null,verCatalogoAdministracionConsultaMovilPath,
			'contenido_administracion_consulta_movil','limpiarDivAdmiConsultaMovil();');	
	 }
	 function limpiarDivAdmiConsultaMovil() {
		limpiarDiv('contenido_administracion_usuarios');
		limpiarDiv('contenido_info_general');
		limpiarDiv('contenido_descuento_agente_movil');
		limpiarDiv('contenido_administracion_cotizacion');
		limpiarDiv('contenido_administracion_cotizacion_portal');
	 }
</script>
<div select="<s:property value='tabActiva'/>"  hrefmode="ajax-html"
	style="height: 380px; width: 910px; margin-left: 10px;"
	id="configuracionNegocioTabBar" class="dhtmlxTabBar"
	imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">
	<div width="100%" id="info_general" name="Tarifas Movil"
		href="http://void" extraAction="javascript: verInfoGeneral();">	
	</div>
	<div width="100%" id="descuento_agente_movil" name="Descuentos Agentes"
		href="http://void" extraAction="javascript: verCatalogoDescuentoAgenteMovil();">
	</div>
	<div width="100%" id="info_general_carga_masiva" name="Cargar Masiva"
		href="http://void" extraAction="javascript: verBoxCargaMasiva();">	
		<s:form  method="post" action="/tarifa/descuentoAgenteMovil/cargaMasiva.action" target="iframeProcessData" enctype="multipart/form-data" 
		id="formularioDescuentoAgenteMovil" name="formularioDescuentoAgenteMovil"> <!-- target="&quot;cargaMasivaAgente.jsp&quot;" -->
			<table width="90%" align="center"  class="contenedorFormas no-border"><!--  -->
				<tr>
					<td>
						<s:file id="fileUpload" name="fileUpload" cssClass="cajaTextoM2 w300 jQrequired" label="Carga Masiva Descuentos Agente"/>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btn_back w120">
							<a href="javascript:cargarExcelDescuentoAgenteMovil()" class="icon_guardar ." 
								onclick="">
								<s:text name="Cargar Archivo"/>
							</a>
						</div>
					</td>
				</tr>
				<tr id="boxCargaMasiva">
					<td>
						<s:text name="midas.agentes.cargaMasiva.resultadoCarga"/><br>
						<iframe name="iframeProcessData"  height="100px" width="400px" style="border: 1; border-color: #A0E0A0;"></iframe>
					</td>
				</tr>
			</table>
		</s:form>
	</div>
	<div width="100%" id="administracion_cotizacion" name="Cotizaciones"
		href="http://void" extraAction="javascript: verCatalogoAdministracionCotizacionMovil();">
	</div>
		<div width="100%" id="administracion_usuarios" name="Usuarios"
		href="http://void" extraAction="javascript: verCatalogoAdministracionUsuarioMovil();">
	</div>
	<div width="100%" id="administracion_cotizacion_portal" name="Cotizaciones Portal"
		href="http://void" extraAction="javascript: verCatalogoAdministracionCotizacionPortal();">
	</div>
	<div width="100%" id="administracion_consulta_movil" name="Consulta Móvil"
		href="http://void" extraAction="javascript: verCatalogoAdministracionConsultaMovil();">
	</div>
</div>