<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idusuario" type="ro" width="50" sort="int" ><s:text name="ID"/></column>
		<column id="email" type="ro" width="*" sort="str"><s:text name="Email"/></column>
		<column id="nombreCompleto" type="ro" width="*" sort="str"><s:text name="Nombre Completo"/></column>
		<column id="numeroCotizaciones" type="ro" width="95" sort="str"><s:text name="Num. Cotizaciones"/></column>
		<column id="numeroTelefono" type="ro" width="95" sort="str"><s:text name="Telefono"/></column>
		<column id="claveEstatus" type="ro" width="80" sort="str"><s:text name="midas.catalogos.fuerzaventa.grid.estatus.titulo"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaUsuarios" var="rowGerencia" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${idusuario}]]></cell>
			<cell><![CDATA[${email}]]></cell>
			<cell><![CDATA[${nombreCompleto}]]></cell>
			<cell><![CDATA[${numeroCotizaciones}]]></cell>
			<cell><![CDATA[${numeroTelefono}]]></cell>
			<s:if test="bajalogica == 1">
				<cell><s:text name="Activa"></s:text></cell>
			</s:if>
			<s:else>
				<cell><s:text name="Inactiva"></s:text></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleAdministracionUsuarioMovilPath, 2,{"usuario.idusuario":${rowGerencia.idusuario},"idRegistro":${rowGerencia.idusuario},"idTipoOperacion":20})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleAdministracionUsuarioMovilPath, 4,{"usuario.idusuario":${rowGerencia.idusuario},"idRegistro":${rowGerencia.idusuario},"idTipoOperacion":20})^_self</cell>
				<s:if test="bajalogica==1">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleAdministracionUsuarioMovilPath, 3,{"usuario.idusuario":${rowGerencia.idusuario},"idRegistro":${rowGerencia.idusuario},"idTipoOperacion":20})^_self</cell>
				</s:if>
			</s:if>
		</row>
	</s:iterator>
</rows>