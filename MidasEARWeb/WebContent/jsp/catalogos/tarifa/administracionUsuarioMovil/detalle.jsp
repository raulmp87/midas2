<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
jQIsRequired();
	var ventanaCentroOperacion=null;
	var ventanaResponsable=null;
	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function salirDeAdministracionUsuariosMovil(){
		var url = "/MidasWeb/tarifa/tarifaMovil/mostrarCatalogoTarifaMovil.action?tabActiva=administracion_usuarios";
		if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}else{
			if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
				sendRequestJQAsync(null, url, targetWorkArea, null);
			}
		}
	}

	function eliminarUsuarioMovil() {
		var path = "/MidasWeb/tarifa/administracionUsuarioMovil/eliminar.action?"
				+ jQuery("#administracionUsuarioMovilForm").serialize() + "&tipoAccion="
				+ parent.dwr.util.getValue("tipoAccion");
		if (validateAll(true)) {
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
    function guardarUsuarioMovil(){
		var path ="/MidasWeb/tarifa/administracionUsuarioMovil/guardar.action?"
			+ jQuery("#administracionUsuarioMovilForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
</script>
<s:include value="/jsp/catalogos/tarifa/administracionCotizacionMovil/administracionCotizacionMovilHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.agregar.titulo')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="Usuario Movil"/>

</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<div class="titulo w400"><s:text name="Eliminar Usuario Movil"/></div>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<div class="titulo w400"><s:text name="Editar Usuario Movil"/></div>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="Administracion  de Usuarios"/>
</s:else>
<s:form id="administracionUsuarioMovilForm">
<s:hidden name="usuario.fecharegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th width="130px"><s:text name="Id" /></th>
			<td colspan="3"><s:textfield name="usuario.idusuario"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th  width="130px"><s:text name="Email" /></th>
			<td colspan="3"><s:textfield name="usuario.email"  readonly="#readOnly" id="txtIdTocotizacionMidas" cssClass="w250 cajaTextoM2 " ></s:textfield></td>
		</tr>
		<tr>
			<th  width="130px"><s:text name="Nombre Completo" /></th>
			<td colspan="3"><s:textfield name="usuario.nombreCompleto"  readonly="#readOnly" id="txtNombreCompleto" cssClass="w250 cajaTextoM2 " ></s:textfield></td>
		</tr>
		<tr>
			<th  width="130px"><s:text name="Num. Cotizaciones" /></th>
			<td colspan="3"><s:textfield name="usuario.numeroCotizaciones"  readonly="#readOnly" id="txtNumeroCotizaciones" cssClass="w250 cajaTextoM2 " ></s:textfield></td>
		</tr>
		<tr>
	 		<th  width="130px"><s:text name="Numero Telefono" /></th>
			<td colspan="3"><s:textfield name="usuario.numeroTelefono"  readonly="#readOnly" id="txtValorPrimaTotal" cssClass="w250 cajaTextoM2 " ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Baja Logica" /></th>
			<td colspan="3">
				<s:select name="usuario.bajalogica" cssClass="cajaTextoM2 w250 jQrequired"
				       list="#{'0':'DESACTIVADO', '1':'ACTIVO'}"/>
			</td>
		</tr>
	 </table>
	<s:if test="usuario.idusuario != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
		    <s:if test="tipoAccion != 5">
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>	    
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2">
			</td>
		</tr>
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeAdministracionUsuariosMovil();"><!-- javascript: mostrarCatalogoGenerico(mostrarGerenciaPath);" class="icon_regresar -->
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarUsuarioMovil();"><!-- if(validateAll(true)){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm, null,false);} -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>
					<s:if test="tipoAccion == 3">
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_borrar"
								onclick="javascript:eliminarUsuarioMovil();">
								<s:text name="midas.boton.borrar"/>
							</a>
						</div>	
					</s:if>
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
