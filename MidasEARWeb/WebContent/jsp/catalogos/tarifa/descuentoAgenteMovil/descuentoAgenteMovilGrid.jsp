<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idtcdescuentosagente" type="ro" width="50" sort="int" ><s:text name="midas.negocio.producto.id"/></column>
        <column id="claveNegocio" type="ro" width="80" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.Ramo"/></column>
		<column id="claveagente" type="ro" width="100" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.claveAgente"/></column>
		<column id="nombre" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.nombreAgente"/></column>
		<column id="clavepromo" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.clavePromo"/></column>
		<column id="numerotelefono" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.numeroTelefono"/></column>
		<column id="email" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.Mail"/></column>
		<column id="descuento" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.descuento"/></column>
		<column id="claveEstatus" type="ro" width="80" sort="str"><s:text name="midas.catalogos.fuerzaventa.grid.estatus.titulo"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaDescuentoAgente" var="rowGerencia" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${idtcdescuentosagente}]]></cell>
			<s:if test="claveNegocio== \"A\"">
					<cell><s:text name="AUTOS"/></cell>	
			</s:if>
			<s:elseif test="claveNegocio== \"V\"">
					<cell><s:text name="VIDA"/></cell>	
			</s:elseif>
			<s:else>
					<cell><s:text name="DAÑOS"/></cell>	
			</s:else>
			<cell><![CDATA[${claveagente}]]></cell>
			<cell><![CDATA[${nombre}]]></cell>
			<cell><![CDATA[${clavepromo}]]></cell>
			<cell><![CDATA[${numerotelefono}]]></cell>
			<cell><![CDATA[${email}]]></cell>
			<cell><![CDATA[${porcentaje}]]></cell>
			<s:if test="bajalogica == 1">
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.activa"></s:text></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.inactiva"></s:text></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleGerenciaPath, 2,{"descuentosAgente.idtcdescuentosagente":${rowGerencia.idtcdescuentosagente},"idRegistro":${rowGerencia.idtcdescuentosagente},"idTipoOperacion":20})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleGerenciaPath, 4,{"descuentosAgente.idtcdescuentosagente":${rowGerencia.idtcdescuentosagente},"idRegistro":${rowGerencia.idtcdescuentosagente},"idTipoOperacion":20})^_self</cell>
				<s:if test="bajalogica==1">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleGerenciaPath, 3,{"descuentosAgente.idtcdescuentosagente":${rowGerencia.idtcdescuentosagente},"idRegistro":${rowGerencia.idtcdescuentosagente},"idTipoOperacion":20})^_self</cell>
				</s:if>
			</s:if>
		</row>
	</s:iterator>
</rows>