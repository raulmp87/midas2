<%@ taglib prefix="s" uri="/struts-tags" %>
	
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
 
<script type="text/javascript">
	var mostrarGerenciaPath    = '<s:url action="mostrarContenedor" namespace="/tarifa/descuentoAgenteMovil"/>';
	
	var verDetalleGerenciaPath = '<s:url action="verDetalle" namespace="/tarifa/descuentoAgenteMovil"/>';
	var guardarGerenciaPath    = '<s:url action="guardar" namespace="/tarifa/descuentoAgenteMovil"/>';
	var eliminarGerenciaPath   = '<s:url action="eliminar" namespace="/tarifa/descuentoAgenteMovil"/>';
	
	var listarGerenciaPath         = '<s:url action="lista" namespace="/tarifa/descuentoAgenteMovil"/>';
	var listarFiltradoGerenciaPath = '<s:url action="listarFiltrado" namespace="/tarifa/descuentoAgenteMovil"/>';
	
</script>