<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/descuentoAgenteMovil/descuentoAgenteMovilHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	var claveNegocio='<s:property value="claveNegocio"/>';
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var claveNegocio='<s:property value="claveNegocio"/>';
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion+"&claveNegocio="+claveNegocio;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"descuentoAgenteMovilGrid", null,idField,'gerenciaModal');
	 });
	 function buscarDescuentoAgenteMovil(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var claveNegocio='<s:property value="claveNegocio"/>';
		var claveAgente= jQuery("#txtClaveAgente").val();		
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion+"&claveNegocio="+claveNegocio+"&claveAgente="+claveAgente;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"descuentoAgenteMovilGrid", null,idField,'gerenciaModal');
	}
</script>
		<s:form action="listarFiltrado" id="gerenciaCatalogoForm">
			<!-- Parametro de la forma para que sea reutilizable -->
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="midas.catalogos.tarifa.descuentoagentemovil.porcentajeDescuento"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="midas.catalogos.tarifa.descuentoagentemovil.claveAgente" />
					</th>
					<td width="275px">
						<s:textfield name="descuentosAgente.claveagente" id="txtClaveAgente" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar" onclick="buscarDescuentoAgenteMovil();">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="descuentoAgenteMovilGrid" class="w880 h200" style="overflow:hidden"></div>	
				
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		
		<s:if test="tipoAccion!=\"consulta\"">
		
		<div class ="w880" align="right">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar" 
					onclick="operacionGenerica(verDetalleGerenciaPath,1);">
					<s:text name="midas.boton.agregar"/>
				</a>
			</div>	
		</div>
		</s:if>		

