<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
	jQIsRequired();
		function guardarDescuentoAgenteMovil(){
			var path ="/MidasWeb/tarifa/descuentoAgenteMovil/guardar.action?"
				+ jQuery("#gerenciaForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
			if(validateAll(true)){
				sendRequestJQ(null, path, targetWorkArea, null);
			}
		}
		function eliminarDescuentoAgenteMovil(){
			var path ="/MidasWeb/tarifa/descuentoAgenteMovil/eliminar.action?";
				+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
			if(validateAll(true)){
				sendRequestJQ(document.gerenciaForm, path, targetWorkArea, null);
			}
		}
		function salirDeDescuentoAgenteMovil(){
			if(claveNegocio == 'A'){
				var url = "/MidasWeb/tarifa/tarifaMovil/mostrarCatalogoTarifaMovil.action?tabActiva=descuento_agente_movil";
			}
			else if(claveNegocio == 'V'){
				var url = "/MidasWeb/tarifa/configuracion/vida/mostrarContenedorPrincipal.action?tabActiva=descuento_agente_movil";
			}
			   if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
				sendRequestJQAsync(null, url, targetWorkArea, null);
			   }else{
					if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
						sendRequestJQAsync(null, url, targetWorkArea, null);
					}
				}
		}
</script>
<s:include value="/jsp/catalogos/fuerzaventa/gerencia/gerenciaHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.agregar.titulo')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>

<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.porcentajeDescuento')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />	
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.eliminar.titulo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.editar.titulo')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.porcentajeDescuento')}"/>
</s:else>
<s:form id="gerenciaForm">
<s:hidden name="descuentosAgente.fecharegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="#titulo"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.catalogos.tarifa.descuentoagentemovil.datosDescuento"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.id" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.idtcdescuentosagente"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Ramo" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.claveNegocio" readonly="true" id="txtClaveNegocio" cssClass="cajaTextoM2 jQrequired" disabled="#readOnly"></s:textfield></td>
		</tr>
		
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.claveAgente" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.claveagente"  readonly="#readOnly" id="txtClaveAgente" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.clavePromo" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.clavepromo"  readonly="#readOnly" id="txtClavePromo" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.numeroTelefono" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.numerotelefono"  readonly="#readOnly" id="txtNumeroTelefono" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
	  <th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.Mail" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.email"  readonly="#readOnly" id="txtMail" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.nombreAgente" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.nombre"  readonly="#readOnly" id="txtDescripcion" cssClass="cajaTextoM2 w250 jQrequired" maxlength="30"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.tarifa.descuentoagentemovil.descuento" /></th>
			<td colspan="3"><s:textfield name="descuentosAgente.porcentaje"  readonly="#readOnly" id="txtPorcentajeDescuento" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.situacion" /></th>
			<td colspan="3">
				<s:select name="descuentosAgente.bajalogica" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'1':'ACTIVO', '0':'INACTIVO'}"/>
			</td>
		</tr>
	 </table>
	<s:if test="descuentosAgente.claveagente != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
		    <s:if test="tipoAccion != 5">
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>	    
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2">
			</td>
		</tr>
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeDescuentoAgenteMovil();"><!-- javascript: mostrarCatalogoGenerico(mostrarGerenciaPath);" class="icon_regresar -->
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarDescuentoAgenteMovil();"><!-- if(validateAll(true)){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm, null,false);} -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>	
					<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
							onclick="eliminarDescuentoAgenteMovil();">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>	
					</s:if>
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
jQuery('#txtClaveNegocio').val(claveNegocio);
</script>
