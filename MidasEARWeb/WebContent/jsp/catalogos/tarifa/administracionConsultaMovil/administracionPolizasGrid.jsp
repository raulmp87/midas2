<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="Id"/></column>
		<column id="usuario" type="ro" width="100" sort="str"><s:text name="Usuario"/></column>
		<column id="poliza" type="ro" width="120" sort="str"><s:text name="Poliza"/></column>
		<column id="inciso" type="ro" width="60" sort="str"><s:text name="Inciso"/></column>
		<column id="polizasPermitidos" type="ro" width="145" sort="str"><s:text name="Polizas Permitido"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listarPolizasCliente" var="usuario" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${usuario}]]></cell>
			<cell><![CDATA[${poliza}]]></cell>
			<cell><![CDATA[${inciso}]]></cell>
			<cell><![CDATA[${polizasPermitidos}]]></cell>					
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^
				<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleConsultaPolizasPath, 2,{"polizasClienteMovil.poliza":${rowGerencia.poliza},"idRegistro":${rowGerencia.poliza},"idTipoOperacion":20})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleGerenciaPath, 4,{"descuentosAgente.idtcdescuentosagente":${rowGerencia.idtcdescuentosagente},"idRegistro":${rowGerencia.idtcdescuentosagente},"idTipoOperacion":20})^_self</cell>
			</s:if>		
		</row>
	</s:iterator>
</rows>