<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/administracionConsultaMovil/administracionConsultaMovilHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarFiltradoAdministracionConsultaMovilPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"administracionConsultaMovilGrid", null,idField,'gerenciaModal');
	 });
	/* jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=verConsultaPolizasPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"consultaPolizasGrid", null,idField,'gerenciaModal');
	 });
	 
	 jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=verConsultaPolizasPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"administracionPolizasGrid", null,idField,'gerenciaModal');
	 });*/
	 
	 function bloquearUsuario(usuario){
		sendRequestJQ(null, config.contextPath + "/tarifa/administracionConsultaMovil/bloquearUsuario.action?usuario=" + usuario, targetWorkArea, null, "html", defaultContentType);
	 }
		
	 function desbloquearUsuario(usuario){
		sendRequestJQ(null, config.contextPath + "/tarifa/administracionConsultaMovil/desbloqueaUsuario.action?usuario=" + usuario, targetWorkArea, null, "html", defaultContentType);
	 }
</script>
<div style= "height: 100%; width:100%; overflow:auto" hrefmode="ajax-html"  id="contenidoConsultaMovil" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="150%" id="adminDescargas" name="<s:text name='Admin. Descargas'/>" href="http://void">
		<s:form action="listarFiltrado" id="administracionDescargasForm">
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Admin. Descargas"/>
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="administracionConsultaMovilGrid" class="w880 h200" style="overflow:hidden"></div>	
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<s:if test="tipoAccion!=\"consulta\"">
		</s:if>
   </div>
   <!-- div width="150%" id="consultaPolizas" name="<s:text name='Consulta Polizas'/>" href="http://void">
   		<s:form action="listarFiltrado" id="consultaPolizasForm">
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Consulta Polizas"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Poliza" />
					</th>
					<td width="275px">
						<s:textfield name="polizasClienteMovil.poliza" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Nombre" />
					</th>
					<td width="275px">
						<s:textfield name="polizasClienteMovil.nombreUsuario" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoConsultaPolizasPath, 'consultaPolizasGrid',document.consultaPolizasForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="consultaPolizasGrid" class="w880 h200" style="overflow:hidden"></div>	
		</s:form>
   </div>
   <div width="150%" id="adminPolizas" name="<s:text name='Administración Polizas'/>" href="http://void">
   		<s:form action="---" id="administracionPolizasForm">
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Admin. Polizas"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Poliza" />
					</th>
					<td width="275px">
						<s:textfield name="polizasClienteMovil.poliza" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Nombre" />
					</th>
					<td width="275px">
						<s:textfield name="polizasClienteMovil.nombreUsuario" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoConsultaPolizasPath, 'administracionPolizasGrid',document.consultaPolizasForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="administracionPolizasGrid" class="w880 h200" style="overflow:hidden"></div>	
		</s:form>
   </div-->
</div>	
<!-- script type="text/javascript">
	dhx_init_tabbars();
</script-->	

