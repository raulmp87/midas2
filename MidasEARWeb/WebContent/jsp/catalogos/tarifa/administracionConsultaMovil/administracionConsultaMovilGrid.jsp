<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="id" type="ro" width="50" sort="int" ><s:text name="Id"/></column>
		<column id="usuario" type="ro" width="120" sort="str"><s:text name="Usuario"/></column>
		<column id="descargas" type="ro" width="80" sort="str"><s:text name="Descargas"/></column>
		<column id="Estatus" type="ro" width="120" sort="str"><s:text name="Estatus"/></column>
		<column id="accionBloquear" type="img" width="30" sort="na" align="center"/>
       	<column id="accionDesbloquear" type="img" width="30" sort="na" align="center"/>
        <!-- column id="accionVerDetalles" type="img" width="30" sort="na" align="center"/-->

	</head>
	<s:iterator value="listarFiltrado" var="usuario" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${usuario}]]></cell>
			<cell><![CDATA[${descargas}]]></cell>
			<cell><![CDATA[${estatus}]]></cell>
			<cell><s:url value="/img/icons/ico_bloquear.gif"/>^<s:text 
			name="Bloquear"/>^javascript:bloquearUsuario("${usuario}")^_self</cell>
			<cell><s:url value="/img/icons/ico_aceptar.gif"/>^<s:text 
			name="Desbloquear"/>^javascript:desbloquearUsuario("${usuario}")^_self</cell>					
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text 
			name="Detalles"/>^javascript:mostrarDetalle("${usuario}")^_self</cell>		
		</row>
	</s:iterator>
</rows>