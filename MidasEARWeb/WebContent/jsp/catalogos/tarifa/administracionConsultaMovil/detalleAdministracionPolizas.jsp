<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
jQIsRequired();
	var ventanaCentroOperacion=null;
	var ventanaResponsable=null;
	
	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	function guardarPolizasPermitidos(){
		var path ="/MidasWeb/tarifa/administracionConsultaMovil/guardarAdminPolizas.action?"
			+ jQuery("#gerenciaForm").serialize()+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	
	function salirDeAdministracionPolizas(){
		var url = "/MidasWeb/tarifa/tarifaMovil/mostrarCatalogoTarifaMovil.action?tabActiva=administracion_consulta_movil";
		if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}else{
			if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
				sendRequestJQAsync(null, url, targetWorkArea, null);
			}
		}
	}
</script>
<s:include value="/jsp/catalogos/tarifa/administracionConsultaMovil/administracionConsultaMovilHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.agregar.titulo')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="Cotizacion Portal"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.eliminar.titulo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.editar.titulo')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="Administracion  de Polizas"/>
</s:else>
<s:form id="administracionConsultaMovilForm">

<s:hidden name="cotizacionMovil.fecharegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="Datos Cotización Portal"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="Detalles Consulta Movil"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="Id" /></th>
			<td colspan="3"><s:textfield name="id"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Usuario" /></th>
			<td colspan="3"><s:textfield name="usuario"  readonly="true" id="txtUsuario" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Poliza" /></th>
			<td colspan="3"><s:textfield name="poliza"  readonly="true" id="txtPoliza" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Inciso" /></th>
			<td colspan="3"><s:textfield name="inciso"  readonly="true" id="txtInciso" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Polizas Permitidos" /></th>
			<td colspan="3"><s:textfield name="polizasPermitidos"  readonly="#readOnly" id="txtPolizasPermitidos" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
	 </table>
	<s:if test="polizasClienteMovil.id != null">
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarPolizasPermitidos();"><!-- if(validateAll(true)){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm, null,false);} -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeAdministracionPolizas()">
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
