<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
jQIsRequired();
	var ventanaCentroOperacion=null;
	var ventanaResponsable=null;
	
	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function salirDeCotizacionMovil(){
		var url = "/MidasWeb/tarifa/tarifaMovil/mostrarCatalogoTarifaMovil.action?tabActiva=administracion_cotizacion";
		if(parent.dwr.util.getValue("tipoAccion") == 2 || parent.dwr.util.getValue("tipoAccion") == 3){
			sendRequestJQAsync(null, url, targetWorkArea, null);
		}else{
			if(confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")){
				sendRequestJQAsync(null, url, targetWorkArea, null);
			}
		}
	}
</script>
<s:include value="/jsp/catalogos/tarifa/administracionCotizacionMovil/administracionCotizacionMovilHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.agregar.titulo')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="Cotizacion Movil"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.eliminar.titulo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.editar.titulo')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="Administracion  de Cotizaciones"/>
</s:else>
<s:form id="administracionCotizacionMovilForm">
<s:hidden name="cotizacionMovil.fecharegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="Datos Cotización Móvil"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="Datos Cotizacion"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="Id" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.idtocotizacionmovil"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Id Cotizacion" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.idtocotizacionmidas"  readonly="#readOnly" id="txtIdTocotizacionMidas" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Email" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.emailProspecto"  readonly="#readOnly" id="txtEmailProspecto" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Telefono" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.telefonoProspecto"  readonly="#readOnly" id="txtNumeroTelefono" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
	  <th class="jQIsRequired" width="130px"><s:text name="Valor Prima Total" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.valorPrimaTotal"  readonly="#readOnly" id="txtValorPrimaTotal" cssClass="w250 cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="Tarifa Amplia" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.tarifaamplia"  readonly="#readOnly" id="txtTarifaAmplia" cssClass="w250 cajaTextoM2 jQrequired" maxlength="30"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Tarifa Limitada" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.tarifalimitada"  readonly="#readOnly" id="txtTarifaLimitada" cssClass="w250 cajaTextoM2 jQrequired"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Clave Promo" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.clavepromo"  readonly="#readOnly" id="txtClavePromo" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Clave Agente" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.claveagente"  readonly="#readOnly" id="txtClaveAgente" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Tipo Descuento" /></th>
			<td colspan="3">
				<s:select name="cotizacionMovil.tipoDescuento" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'A':'AGENTE', 'E':'ESTADO', '0':'NINGUNO'}"/>
			</td>
		</tr>
				<tr>
			<th class="jQIsRequired"><s:text name="porcentaje Descuento" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.porcentajeDescuento"  readonly="#readOnly" id="txtPorcentajeDescuento" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Ramo" /></th>
			<td colspan="3">
				<s:select name="cotizacionMovil.claveNegocio" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'A':'AUTOS', 'V':'VIDA', 'D':'DAÑOS'}"/>
			</td>
		</tr>
	    <tr>
			<th class="jQIsRequired"><s:text name="Fecha Registro" /></th>
			<td colspan="3"><s:textfield name="cotizacionMovil.fecharegistro"  readonly="#readOnly" id="txtFechaRegistro" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
	 </table>
	<s:if test="cotizacionMovil.idtocotizacionmovil != null">
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeCotizacionMovil()"><!-- javascript: mostrarCatalogoGenerico(mostrarGerenciaPath);" class="icon_regresar -->
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
