<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="idtocotizacionmovil" type="ro" width="50" sort="int" ><s:text name="Id"/></column>
		<column id="idtocotizacionmidas" type="ro" width="77" sort="str"><s:text name="Cotizacion"/></column>
		<column id="email" type="ro" width="100" sort="str"><s:text name="Email"/></column>
		<column id="telefono" type="ro" width="*" sort="str"><s:text name="Telefono"/></column>
		<column id="valorPrimaTotal" type="ro" width="75" sort="str"><s:text name="Prima Total"/></column>
		<column id="tarifaAmplia" width="*" sort="str" type="ro"><s:text name="Tarifa Amplia"/></column>
		<column id="tarifaLimitada" type="ro" width="*" sort="str"><s:text name="Tarifa Limitada"/></column>
		<column id="porcentajeDescuento" type="ro" width="80" sort="str"><s:text name="Porcentaje Descuento"/></column>
		<column id="tipoDescuento" type="ro" width="80" sort="str"><s:text name="Tipo Descuento"/></column>
		<column id="claveNegocio" type="ro" width="65" sort="str"><s:text name="Ramo"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		</s:if>
	</head>
	<s:iterator value="listaCotizacionMovil" var="rowGerencia" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${idtocotizacionmovil}]]></cell>
			<cell><![CDATA[${idtocotizacionmidas}]]></cell>
			<cell><![CDATA[${emailProspecto}]]></cell>
			<cell><![CDATA[${telefonoProspecto}]]></cell>
			<cell><fmt:formatNumber value="${valorPrimaTotal}" type="currency" currencySymbol="$"/></cell>
			<cell><fmt:formatNumber value="${tarifaamplia}" type="currency" currencySymbol="$"/></cell>
			<cell><fmt:formatNumber value="${tarifalimitada}" type="currency" currencySymbol="$"/></cell>
			<cell><![CDATA[${porcentajeDescuento}]]></cell>
			<s:if test="tipoDescuento== \"A\"">
				<cell><s:text name="AGENTE"></s:text></cell>
			</s:if>
			<s:elseif test="tipoDescuento== \"E\"">
				<cell><s:text name="ESTADO"></s:text></cell>
			</s:elseif>
			<s:else>
				<cell><s:text name="NINGUNO"></s:text></cell>
			</s:else>
			<s:if test="claveNegocio== \"A\"">
					<cell><s:text name="AUTOS"/></cell>	
			</s:if>
			<s:elseif test="claveNegocio== \"V\"">
					<cell><s:text name="VIDA"/></cell>	
			</s:elseif>
			<s:else>
					<cell><s:text name="DAÑOS"/></cell>	
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleAdministracionCotizacionMovilPath, 2,{"cotizacionMovil.idtocotizacionmovil":${rowGerencia.idtocotizacionmovil},"idRegistro":${rowGerencia.idtocotizacionmovil},"idTipoOperacion":20})^_self</cell>
			</s:if>
		</row>
	</s:iterator>
</rows>