<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/administracionCotizacionMovil/administracionCotizacionMovilHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarFiltradoAdministracionCotizacionMovilPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"administracionCotizacionMovilGrid", null,idField,'gerenciaModal');
	 });
</script>


		<s:form action="listarFiltrado" id="administracionCotizacionMovilForm">
			<!-- Parametro de la forma para que sea reutilizable -->
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Administracion de Cotizaciones Movil"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Id Cotizacion" />
					</th>
					<td width="275px">
						<s:textfield name="cotizacionMovil.idtocotizacionmidas" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Email" />
					</th>
					<td width="275px">
						<s:textfield name="cotizacionMovil.emailProspecto" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoAdministracionCotizacionMovilPath, 'administracionCotizacionMovilGrid',document.administracionCotizacionMovilForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="administracionCotizacionMovilGrid" class="w880 h200" style="overflow:hidden"></div>	
				
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		
		<s:if test="tipoAccion!=\"consulta\"">
		
		<!--  div class ="w880" align="right">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar ." 
					onclick="operacionGenerica(verDetalleCotizacionMovilPath,1);">
					<s:text name="midas.boton.agregar"/>
				</a>
			</div>	
		</div>-->
		</s:if>		

