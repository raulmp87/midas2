<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/tarifa/administracionCotizacionPortal/administracionCotizacionPortalHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarFiltradoAdministracionCotizacionPortalPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"administracionCotizacionPortalGrid", null,idField,'gerenciaModal');
	 });
</script>


		<s:form action="listarFiltrado" id="administracionCotizacionPortalForm">
			<!-- Parametro de la forma para que sea reutilizable -->
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Administracion de Cotizaciones Portal"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Id Cotizacion" />
					</th>
					<td width="275px">
						<s:textfield name="cotizacionMovil.idtocotizacionmidas" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Email" />
					</th>
					<td width="275px">
						<s:textfield name="cotizacionMovil.emailProspecto" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoAdministracionCotizacionPortalPath, 'administracionCotizacionPortalGrid',document.administracionCotizacionPortalForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>			
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="administracionCotizacionPortalGrid" class="w880 h200" style="overflow:hidden"></div>	
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<s:if test="tipoAccion!=\"consulta\"">
		</s:if>		

