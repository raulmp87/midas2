<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/negocio/copiarNegocioHeader.jsp"></s:include>

<s:include value="/jsp/catalogos/tarifa/tarifaServicioPublico/tarifaServicioPublicoHeader.jsp"></s:include>

<div id="content" style="width: 98%;">
	<s:form action="mostrar" >
		<s:hidden name="idToNegocio" /> 
		<s:hidden name="esLineaAutobuses" id="esLineaAutobuses"/>
		<table style="width: 100%;" border="0">
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.tarifa.servicioPublico" /> 
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"></th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
		</table>
		<table style="width: 100%;" id="desplegarDetalle" border="0">
		<s:if test="folio">
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.folio.reexpedible.field.rango.folio"></s:text>
				</th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:textfield id="folio" name="filtroFolioRe.folioInicio" cssClass="cajaTexto"  
						onfocus="javascript: new Mask('****_##########', 'string').attach(this)" />
				</td>
				<td style="width: 33%;"></td>
				<td style="width: 33%;"></td>
			</tr>
		</s:if>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.general.negocio" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.suscripcion.cotizacion.producto" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.seccion.tiposPoliza" />
				</th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:select cssClass="cajaTexto" name="idToNegocio" id="idToNegocio" 
						list="negocioList" listKey="idToNegocio" 
						listValue="descripcionNegocio" 
						headerKey="-1"  
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="obtenerProductos(this.value);"/> 
				</td> 
				<td style="width: 33%;">
					<s:select cssClass="cajaTexto" name="idToNegProducto" id="idToNegProducto" 
						list="negocioProductoList" listKey="idToNegProducto" 
						listValue="productoDTO.descripcion" 
						headerKey="-1"  
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="obtenerTiposPoliza(this.value);"/> 
				</td> 
				<td style="width: 33%;">
					<s:select id="idToTipoPoliza" cssClass="cajaTexto" 
						list="negocioTipoPolizaList"  
						name="idToTipoPoliza"
						listKey="idToNegTipoPoliza" headerKey="-1"
						listValue="tipoPolizaDTO.descripcion"					 
						headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
						OnChange="obtenerLineasDeNegocio(this.value); obtenerMonedas(this.value); obtenerVigencias();" />
				</td>					
			</tr>
			<tr>
				<th style="width: 34%;">
					<s:text name="midas.negocio.seccion.disponibles" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.paquete" /> 
				</th>
				<th style="width: 33%;">
					<s:text name="midas.general.moneda" /> 
				</th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:select id="idToNegSeccion" cssClass="cajaTexto"  
						name="idToNegSeccion"  
						list="secciones" listKey="idToNegSeccion" listValue=""  
						OnChange="obtenerPaquetes(this.value);" 
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"/> 
				</td>
				<td style="width: 33%;">
					<s:select name="idToNegPaqueteSeccion" id="idToNegPaqueteSeccion"  
							  list="paquetes" listKey="idToNegPaqueteSeccion" listValue=""  cssClass="cajaTexto" 
							  OnChange="onChangeComboPaquete(this.value);" headerKey="-1" 
							  headerValue="%{getText('midas.general.seleccione')}"/> 
				</td> 
				<td style="width: 33%;">
					<s:select name="idTcMoneda" id="idTcMoneda"  cssClass="cajaTexto" 
							  list="monedaList" listKey="idTcMoneda" listValue=""  
							  OnChange="onChangeComboMoneda(this.value);" headerKey="-1"  
							  headerValue="%{getText('midas.general.seleccione')}"/> 
				</td> 
			</tr>
			<tr> 
				<th style="width: 34%;">
					Vigencia
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.estado" />
				</th>
				<th style="width: 33%;">
					<s:text name="midas.negocio.cobertura.paquete.seccion.municipio" />
				</th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:select cssClass="cajaTexto" name="idTcVigencia" id="idTcVigencia" 
						list="vigenciaList" listKey="idTcVigencia" 
						listValue="descripcion" 
						headerKey="-1"  
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="onChangeComboVigencia(this.value);"/> 
				</td> 
				<td style="width: 33%;">
					<s:select cssClass="cajaTexto" name="stateId" id="stateId" 
						disabled="true" list="estadoList" listKey="stateId" 
						listValue="stateName" 
						OnChange="obtieneMunicipio(this.value);initGridsTarifaServicioPublico();" headerKey=""  
						headerValue="%{getText('midas.general.seleccione')}"
					/> 
				</td> 
				<td style="width: 33%;">
					<s:select cssClass="cajaTexto" name="cityId" id="cityId"  
						disabled="true" list="municipioList" listKey="cityId" 
						listValue=""  headerKey="" 
						OnChange="onChangeComboMunicipio(this.value);initGridsTarifaServicioPublico();"  
						headerValue="%{getText('midas.general.seleccione')}"
					/> 
				</td>
			</tr>
		</table>
		<div>
			<div id="coberturasObligatoriasGrid" style="width: 765px; height: 100px;"></div>
			<br/>
			<div id="coberturasAdicionalesGrid" style="width: 765px; height: 100px"></div>
			<br/>
			<div id="sumasAseguradasAdicionalesGrid" style="width: 765px; height: 100px"></div>
			<br/>
			<div id="deduciblesAdicionalesGrid" style="width:765px; height: 100px"></div>
		</div>
	</s:form>
</div>