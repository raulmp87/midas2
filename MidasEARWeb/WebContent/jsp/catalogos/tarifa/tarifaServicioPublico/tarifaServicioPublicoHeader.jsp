<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/tarifa/tarifaServicioPublico/tarifaServicioPublico.js'/>"></script>

<script type="text/javascript">
 	var obtenerCoberturasObligatoriasGridPath = '<s:url action="obtenerCoberturasObligatorias" namespace="/tarifa/tarifaServicioPublico"/>';
    var obtenerCoberturasAdicionalesGridPath = '<s:url action="obtenerCoberturasAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
    var obtenerSumasAseguradasAdicionalesGridPath = '<s:url action="obtenerSumasAseguradasAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
    var obtenerDeduciblesAdicionalesGridPath = '<s:url action="obtenerDeduciblesAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
    
    var guardarMontosCoberturasAdicionalesPath = '<s:url action="guardarMontosCoberturasAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
    var guardarMontosSumasAseguradasAdicionalesPath = '<s:url action="guardarMontosSumasAseguradasAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
    var guardarMontosDeduciblesAdicionalesPath = '<s:url action="guardarMontosDeduciblesAdicionales" namespace="/tarifa/tarifaServicioPublico"/>';
</script>




