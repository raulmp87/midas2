<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>            
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>			 			
        </beforeInit>
        <column id="id" type="ro" width="*" sort="str" hidden="true">id</column>
		<column id="descripcionAgrupador" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.agrupador.descripcion"/></column>
		<column id="idVerAgrupadorTarifa" type="ro" width="*" sort="int"><s:text name="midas.catalogos.tarifa.agrupador.version"/></column>
		<column id="fechaInicioVigencia" type="ro" width="*" sort="date">Inicio Vigencia</column>
		<column id="fechaFinVigencia" type="ro" width="*" sort="date">Fin Vigencia</column>
		<column id="claveEstatus" type="ro" width="*" sort="str">Estatus</column>
		<column id="accionNuevaVersion" type="img" width="30px" sort="na"/>
		<column id="accionVer" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>
<%-- 		<column id="accionBorrar" type="img" width="30px" sort="na"/> --%>
	</head>
	<% int a=0;%>
	<s:iterator value="agrupadorTarifaList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="businessKey" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionAgrupador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idVerAgrupadorTarifa" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaInicioVigencia" format="dd/MM/yyyy" /></cell>
<%-- 			<cell><s:property value="fechaInicioVigencia" escapeHtml="false" escapeXml="true"/> </cell> --%>
			<cell><s:date name="fechaFinVigencia" format="dd/MM/yyyy" /></cell>
<%-- 			<cell><s:property value="fechaFinVigencia" escapeHtml="false" escapeXml="true"/></cell> --%>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_copiar1.gif^Nueva Version^javascript: nuevaVersionAgrupador("<s:property value="businessKey" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleAgrupador)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleAgrupador)^_self</cell>
<%-- 			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleAgrupador)^_self</cell> --%>	
		</row>
	</s:iterator>	
</rows>