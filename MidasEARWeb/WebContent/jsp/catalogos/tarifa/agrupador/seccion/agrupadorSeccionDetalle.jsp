<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/tarifa/agrupador/seccion/agrupadorSeccionHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardarAsociacion')}" />
	<s:set id="accionJsBoton" value="'guardarAgrupadorSeccion();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="agrupadorTarifaSeccion.id.idToSeccion != null && agrupadorTarifaSeccion.id.idToAgrupadorTarifa != null">
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="readOnly" value="true" />
		<s:set id="tituloAccionA" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.autos.editar.titulo')}" />
		<s:set id="tituloAccionD" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.danos.editar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="tituloAccionA" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.autos.agregar.titulo')}" />
		<s:set id="tituloAccionD" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.danos.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccionA" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.autos.detalle.titulo')}" />
	<s:set id="tituloAccionD" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.danos.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.eliminarAsociacion')}"/>
	<s:set id="accionJsBoton" value="'eliminarAgrupadorSeccion();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccionA" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.autos.eliminar.titulo')}" />
	<s:set id="tituloAccionD" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.danos.eliminar.titulo')}" />
</s:else>

<s:form action="guardar" id="agrupadorSeccionForm">
	<s:hidden name="tipoAccion"/>
	<s:hidden name="negocio"/>
	<s:if test="id.idToSeccion != null">
		<s:hidden name="id.idToSeccion"/>
		<s:hidden name="id.idMoneda"/>
		<s:hidden name="id.idToAgrupadorTarifa"/>
	</s:if>
	<table  id="agregar" >
		<tr>
			<td class="titulo" colspan="6">
				<s:if test="negocio != \"D\"">
					<s:text name="%{#tituloAccionA}"/>	
					<s:set id="nombreBienSeccion" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.autos.bien')}"/>
				</s:if>
				<s:else>
					<s:text name="%{#tituloAccionD}"/>
					<s:set id="nombreBienSeccion" value="%{getText('midas.catalogos.tarifa.agrupadorseccion.danos.bien')}"/>	
				</s:else>						
			</td>
		</tr>
		<tr>
			<th width="18px">
				<s:text name="midas.general.bienseccion"></s:text>
			</th>
			<td width="140px">				
				<s:select list="seccionesList"
					id="seccion" 
					name="agrupadorTarifaSeccion.id.idToSeccion" 
					listValue="descripcion" listKey="idToSeccion" 
					labelposition="left" cssClass="cajaTexto2"
					key=""
					headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}"
					disabled="#readOnly"
					required="#requiredEditField"
				/>
				<s:if test="#readOnly == true">
					<s:hidden name="agrupadorTarifaSeccion.id.idToSeccion"></s:hidden>
				</s:if>
			</td>
			<td width="741px"></td>
		</tr>
		<s:if test="negocio != \"D\"">
			<tr>
				<th width="18px">
					<s:text name="midas.general.moneda"></s:text>
				</th>
				<td width="140px">					
					<s:select list="monedasList"
						id="agrupadorTarifaSeccion.id.idMoneda" 
						name="agrupadorTarifaSeccion.id.idMoneda" 
						listValue="descripcion" listKey="idTcMoneda" 
						labelposition="left" cssClass="cajaTexto2"
						onchange="getAgrupadoresPorMoneda($('agrupadorTarifaSeccion.id.idMoneda'), $('agrupadorTarifaSeccion.id.idToAgrupadorTarifa'));"
						key=""
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}"
						disabled="#readOnly"
						required="#requiredEditField"
					/>
				</td>
				<s:if test="#readOnly == true">
					<s:hidden name="agrupadorTarifaSeccion.id.idMoneda"></s:hidden>
				</s:if>	
				<td width="741px"></td>
			</tr>
		</s:if>		
		<tr>
			<th width="18px">
				<s:text name="midas.catalogos.tarifa.agrupador.descripcion"></s:text>
			</th>
			<td width="140px">				
				<s:select list="agrupadoresMap" 
					id="agrupadorTarifaSeccion.id.idToAgrupadorTarifa"
					name="agrupadorTarifaSeccion.id.idToAgrupadorTarifa" 
					labelposition="left" cssClass="cajaTexto2"
					key=""
					headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}"
					disabled = "#readOnly"
					required="#requiredEditField"
				/>
				<s:if test="#readOnly == true">
					<s:hidden name="agrupadorTarifaSeccion.id.idToAgrupadorTarifa"></s:hidden>
				</s:if>
			</td>
			<td width="741px"></td>
		</tr>
		<tr>
			<th width="18px">
				<s:text name="midas.catalogos.tarifa.agrupadorseccion.default"></s:text>
			</th>
			<td width="140px">			
				<s:checkbox name="agrupadorTarifaSeccion.claveDefaultBoolean" 
						key="" 
						labelposition="left" disabled="#readEditOnly"></s:checkbox>
			</td>
			<td width="741px"></td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="6"> 
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"
								cssClass="b_submit icon_guardar w150"/> 
						</div>
   	 				</div>
				</td>
			</tr>
		</s:if>
		<tr>
			<td colspan="6"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" 
							onclick="mostrarCatalogoAgrupadorSeccion(); return false;"
							cssClass="b_submit icon_regresar w100"/> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="6"> 
					<span style="color:red">
						<s:text name="midas.catalogos.mensaje.requerido"/>
					</span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>
