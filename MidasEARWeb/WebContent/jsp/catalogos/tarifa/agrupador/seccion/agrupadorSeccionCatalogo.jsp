<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/tarifa/agrupador/seccion/agrupadorSeccionHeader.jsp"></s:include>

<s:form action="listar" id="agrupadorSeccionForm">
	<s:hidden name="negocio"/>
	<table width="97%" id="filtros" >
		<tr>
			<td class="titulo" colspan="6">
				<s:if test="negocio != \"D\"">
					<s:text name="midas.catalogos.tarifa.agrupadorseccion.autos.titulo"/>	
				</s:if>
				<s:else>
					<s:text name="midas.catalogos.tarifa.agrupadorseccion.danos.titulo"/>	
				</s:else>
			</td>
		</tr>		
		<tr>
			<th>
				<s:text name="midas.general.bienseccion"></s:text>
			</th>
			<s:if test="negocio != \"A\"">	<%-- Daños --%>
				<td colspan="3">
					<s:select list="seccionesList"
						id="seccion" 
						name="agrupadorTarifaSeccion.id.idToSeccion" 
						listValue="descripcion" listKey="idToSeccion"
						labelposition="left" cssClass="cajaTexto2"
						key="" 
						headerKey="-1" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="getAgrupadoresPorMonedaPorSeccion($('agrupadorTarifaSeccion.id.idToSeccion'),null,$('agrupadorTarifaSeccion.id.idToAgrupadorTarifa')); validaFiltro();"
					/>					
				</td>
			</s:if>
 			<s:if test="negocio != \"D\"">	<%-- Autos --%>
				<td colspan="3">
					<s:select list="seccionesList" 
						id="seccion"
						name="agrupadorTarifaSeccion.id.idToSeccion"
						listValue="descripcion" listKey="idToSeccion"
						labelposition="left" cssClass="cajaTexto2"
						key="" 
						headerKey="-1" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="getMoneda($('agrupadorTarifaSeccion.id.idMoneda'));validaFiltro();"
					/>
				</td>
			</s:if>
			<td colspan="2">&nbsp;</td>
		</tr>
		<s:if test="negocio != \"D\"">	<%-- Autos --%>		
			<tr>
				<th>
					<s:text name="midas.general.moneda"></s:text>
				</th>
				<td colspan="3">					
					<s:select key="" labelposition="left" cssClass="cajaTexto2"
						list="monedasList" id="agrupadorTarifaSeccion.id.idMoneda" 
						name="agrupadorTarifaSeccion.id.idMoneda" 
						listValue="descripcion" listKey="idTcMoneda" 					
						headerKey="-1" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="getAgrupadoresPorMonedaPorSeccion($('seccion'),$('agrupadorTarifaSeccion.id.idMoneda'), $('agrupadorTarifaSeccion.id.idToAgrupadorTarifa'));"
					/>
				</td>	
				<td colspan="2">&nbsp;</td>
			</tr>
		</s:if>
		<tr>
			<th>
				<s:text name="midas.catalogos.tarifa.agrupador.descripcion"></s:text>
			</th>
			<td colspan="3">				
				<s:select list="agrupadoresMap" 
					id="agrupadorTarifaSeccion.id.idToAgrupadorTarifa"
					name="agrupadorTarifaSeccion.id.idToAgrupadorTarifa" 
					labelposition="left" cssClass="cajaTexto2"
					key=""
					headerKey="-1" 
					headerValue="%{getText('midas.general.seleccione')}"
					disabled = "#readOnly"
					required="#requiredEditField"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
			
		</tr>
		<tr>
			<td class="buscar" colspan="6">
				<div id="busquedaAgrupadorSeccion" class="alinearBotonALaDerecha" style="display: none">				
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript: listarFiltradoAgrupadorSeccion();return false;">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<br></br>
	<div id ="agrupadorSeccionGrid" style="width:97%;height:210px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>

	<div style="float: right; margin-right: 40px">
		<div class="btn_back w150 " >
			<a href="javascript: void(0);"
				onclick="javascript:TipoAccionDTO.getAgregarModificar(verDetalleAgrupadorSeccion);return false;"
				class="icon_masAgregar ">
				<s:text name="midas.boton.agregarAsociacion"/>
			</a>
		</div>
	</div>
</s:form>