<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script> 
<script src="<s:url value='/js/midas2/catalogos/tarifa/seccion/agrupadorSeccion.js'/>"></script>

<script type="text/javascript">
	var listarAgrupadorSeccionPath = '<s:url action="listar" namespace="/tarifa/agrupador/seccion"/>';
	var listarFiltradoAgrupadorSeccionPath = '<s:url action="listarFiltrado" namespace="/tarifa/agrupador/seccion"/>';
	var verDetalleAgrupadorSeccionPath = '<s:url action="verDetalle" namespace="/tarifa/agrupador/seccion"/>';
	var guardarAgrupadorSeccionPath = '<s:url action="guardar" namespace="/tarifa/agrupador/seccion"/>';
	var eliminarAgrupadorSeccionPath = '<s:url action="eliminar" namespace="/tarifa/agrupador/seccion"/>';
	var mostrarCatalogoAgrupadorSeccionPath = '<s:url action="mostrarCatalogo" namespace="/tarifa/agrupador/seccion"/>';
</script>