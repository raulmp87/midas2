<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>            
			<call command="enablePaging">
				<param>true</param>
				<param>11</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>			 			
        </beforeInit>
        <afterInit>        	
        </afterInit>
        <column id="id" type="ro" width="50" sort="str" hidden="true">id</column>
		<column id="seccion" type="ro" width="250" sort="str">
			<s:if test="negocio != \"A\"">
				<s:text name="midas.general.bienseccion"/>
			</s:if>
			<s:else>
				<s:text name="midas.general.seccion"/>			
			</s:else>
		</column>
		<column id="agrupador" type="ro" width="200" sort="str">Agrupador</column>
		<column id="moneda" type="ro" width="*" sort="str"  
				hidden=<s:if test="negocio != \"A\"">
							"true"
						</s:if>
						<s:else>
							"false"			
						</s:else>>Moneda</column>
		<column id="default" type="ro" width="*" sort="na">Default</column>
		<column id="accionVer" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>
		<column id="accionBorrar" type="img" width="30px" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="agrupadorTarifaSeccionList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="businessKey" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agrupadorTarifa.descripcionAgrupador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.descripcion" escapeHtml="false" escapeXml="true"/> </cell>
			<cell><s:property value="claveDefault" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleAgrupadorSeccion)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleAgrupadorSeccion)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleAgrupadorSeccion)^_self</cell>			
		</row>
	</s:iterator>	
</rows>