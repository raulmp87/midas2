<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/tarifa/agrupador/agrupadorHeader.jsp"></s:include>
<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarAgrupador();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="agrupadorTarifa.id != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.tarifa.agrupador.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.tarifa.agrupador.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.tarifa.agrupador.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarAgrupador();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.tarifa.agrupador.borrar.titulo')}" />
</s:else>

<s:form action="guardar" id="agrupadorForm">
	<s:hidden name="tipoAccion"/>
	<s:hidden name="negocio"/>
	<s:hidden name="id"/>
	<table  id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="%{#tituloAccion}"/>			
			</td>
		</tr>
		<tr>
			<th width="150px">
				<s:text name="midas.catalogos.tarifa.agrupador.descripcion"></s:text>
			</th>
			<td>
				<s:textfield name="agrupadorTarifa.descripcionAgrupador" 
					id="txtClave" maxlength="18"
					key="" 
					labelposition="left" 
					readonly="#readOnly" required="#requiredField" 
					onkeypress="return upperCaseAlfaNumerico(event);"/>
			</td>						
		</tr>
		
		<s:if test="agrupadorTarifa.id != null">
			<tr>
				<th width="150px">
					<s:text name="midas.catalogos.tarifa.agrupador.version.descripcion"></s:text>
				</th>
				<td>
					<s:textfield name="agrupadorTarifa.descripcionVersion" id="txtClave" 
						key="" 
						labelposition="left" readonly="#readEditOnly" 
						required="#requiredEditField" />
				<td>	
			</tr>
			<s:if test="tipoAccion == 1">		
			<tr>
				<th width="150px">
					<s:text name="midas.catalogos.tarifa.agrupador.estatus"></s:text>
				</th>
				<td>
					<s:select list="agrupadorTarifa.estatusValidos" 
						name="agrupadorTarifa.claveEstatus" 
						value="agrupadorTarifa.claveEstatus" labelposition="left" 
						key=""
						required="#requiredField"/>
				<td>
			</tr>
			</s:if>
			<s:else>		
			<tr>
				<th width="150px">
					<s:text name="midas.catalogos.tarifa.agrupador.estatus"></s:text>
				</th>
				<td>
					<s:textfield
						name = "agrupadorTarifa.descripcionEstatus"
						key="" 
						labelposition="left" 
						readonly="#readEditOnly" 
						required="#requiredEditField"/>
				<td>
			</tr>
			</s:else>			
		</s:if>
		<s:else>
			<tr>
				<td>
					<s:hidden name="agrupadorTarifa.claveEstatus"  value="0" />
				</td>
			</tr>
		</s:else>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="2"> 
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"
								cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
				</td>
			</tr>
		</s:if>
		<tr>
			<td colspan="2"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoAgrupador(); return false;"
							cssClass="b_submit icon_regresar w100"/> 
					</div>
   	 			</div>
			</td>
		</tr>
		<tr>
			<s:if test="tipoAccion == 1">
				<td colspan="2"> <span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span></td>
			</s:if>
		</tr>
	</table>
</s:form>



