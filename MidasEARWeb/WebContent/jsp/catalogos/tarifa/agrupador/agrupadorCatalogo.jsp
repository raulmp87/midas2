<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<s:include value="/jsp/catalogos/tarifa/agrupador/agrupadorHeader.jsp"></s:include>

<s:form  id="agrupadorForm" onKeyPress="return disableEnterKey(event)">
	<s:hidden name="negocio"/>
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.tarifa.agrupador.titulo"/>
			</td>
		</tr>
	</table>
	<br></br>
	<div id ="agrupadorGrid" style="width:97%;height:318px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>

	<div class="alinearBotonALaDerecha" style="margin-right: 30px">
		<div id="b_agregar">
			<a href="javascript: void(0);" title="Agregar"
				onclick="javascript:TipoAccionDTO.getAgregarModificar(verDetalleAgrupador);return false;">
				<s:text name="midas.boton.agregar"/>
			</a>
			
		</div>
	</div>


</s:form>

