<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/catalogos/tarifa/agrupador/tarifa/tarifaAgrupadorTarifaHeader.jsp"></s:include>
 
<s:form action="mostrarCatalogo">
<table width="97%" id="filtros">
	<tr>
		<td class="titulo" colspan="6">
			<s:text name="midas.catalogos.tarifa.agrupador.tarifa.titulo"/>
			<s:hidden name="claveNegocio"/>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<s:select key="midas.general.agrupador.tarifa" name="idAgrupadorTarifaName" 
				id="idAgrupadorTarifaName" 
				list="mapAgrupadorTarifa" onchange="getAccionTarifaAgrupadorTarifa();" 
				headerKey="" headerValue="%{getText('midas.general.seleccione')}"
				labelposition="left"/>
		</td>
	</tr>
		<tr>
			<td colspan="6">
				<s:select key="midas.general.moneda" name="idMonedaName" id="idMonedaName" 
					list="mapMoneda" labelposition="left"
					onchange="iniciaGridsTarifaAgrupadorTarifa();" headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}"
					labelposition="left"/>
			</td>
		</tr>
</table>


		<table id="desplegarDetalle" >
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.catalogos.asociar.tarifa.agrupador.tarifa"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.catalogos.tarifa.agrupador.tarifa.asociadas"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="tarifaAgrupadorTarifaAsociadasGrid"  style="width:635px;height:110px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.catalogos.tarifa.agrupador.tarifa.disponible"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="tarifaAgrupadorTarifaDisponiblesGrid"  style="width:635px;height:110px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>


</s:form>
	