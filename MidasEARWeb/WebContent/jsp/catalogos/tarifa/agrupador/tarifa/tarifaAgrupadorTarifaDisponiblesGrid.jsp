<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="tarifaAgrupadorTarifa.id.idLineanegocio" type="ro" width="0" sort="str" hidden="true">idToCobertura</column>
		<column id="tarifaAgrupadorTarifa.id.idMoneda" type="ro" width="0" sort="str" hidden="true">idMoneda</column>
		<column id="tarifaAgrupadorTarifa.id.idToRiesgo" type="ro" width="0" sort="str" hidden="true">idToRiesgo</column>
		<column id="tarifaAgrupadorTarifa.id.idConcepto" type="ro" width="0" sort="str" hidden="true">idConcepto</column>		
		<column id="tarifaConcepto.descripcion" type="ro" width="400" sort="str">Concepto</column>
		<column id="descripcion" type="ro" width="200" sort="str" ><s:text name="midas.catalogos.tarifa.agrupador.version.descripcion"/></column>
		<column id="tarifaAgrupadorTarifa.id.idVertarifa" type="ro" width="70" align="right" sort="int" ><s:text name="midas.general.version"/></column>
		<column id="descripcionEstatus" type="ro" width="100" sort="str">Estatus</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesTarifaAgrupadorTarifaDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idLineaNegocio" escapeHtml="false" /></cell>
			<cell><s:property value="id.idMoneda" escapeHtml="false" /></cell>
			<cell><s:property value="id.idRiesgo" escapeHtml="false" /></cell>
			<cell><s:property value="id.idConcepto" escapeHtml="false" /></cell>			
			<cell><s:property value="tarifaConcepto.descripcion" escapeHtml="false" /></cell>
			<cell><s:property value="descripcion" escapeHtml="false" /></cell>
			<cell><s:property value="id.version" escapeHtml="false" /></cell>			
			<cell><s:property value="descripcionEstatus" escapeHtml="false" /></cell>
		</row>
	</s:iterator>
	
</rows>