<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/tarifa/agrupador.js'/>"></script>
<script src="<s:url value='/js/validaciones.js'/>"></script>

<script type="text/javascript">
	var listarAgrupadorTarPath = '<s:url action="listar" namespace="/tarifa/agrupador"/>';
	var listarFiltradoAgrupadorTarPath = '<s:url action="listarFiltrado" namespace="/tarifa/agrupador"/>';
	var verDetalleAgrupadorTarPath = '<s:url action="verDetalle" namespace="/tarifa/agrupador"/>';
	var guardarAgrupadorTarPath = '<s:url action="guardar" namespace="/tarifa/agrupador"/>';
	var eliminarAgrupadorTarPath = '<s:url action="eliminar" namespace="/tarifa/agrupador"/>';
	var mostrarCatalogoAgrupadorTarPath = '<s:url action="mostrarCatalogo" namespace="/tarifa/agrupador"/>';
	var nuevaVersionAgrpadorTarPath = '<s:url action="nuevaVersion" namespace="/tarifa/agrupador"/>';
</script>
