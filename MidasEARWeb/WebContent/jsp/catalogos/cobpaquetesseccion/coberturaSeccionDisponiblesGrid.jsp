<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="cobPaquetesSeccion.id.idToCobertura" type="ro" width="0" sort="int" hidden="true">idToCobertura</column>
		<column id="cobPaquetesSeccion.id.idToSeccion" type="ro" width="0" sort="int" hidden="true">idToSeccion</column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.catologos.cobpaquetessecciones.descripcion"/></column>
		<column id="cobPaquetesSeccion.claveObligatoriedad" type="co" width="*" sort="int">Clave Obligatoriedad
			<option value="0">Opcional</option>
			<option value="1">Opcional Default</option>
			<option value="2">Obligatoria</option>
		</column>
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesCobPaquetesSeccionDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idtocobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idtoseccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveObligatoriedad" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>