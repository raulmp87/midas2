<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

	<s:include value="/jsp/catalogos/cobpaquetesseccion/cobPaquetesSeccionesHeader.jsp"></s:include>

 
<s:form action="mostrarCatalogo">
<table width="97%" id="filtros">
	<tr>
		<td class="titulo" colspan="6">
			<s:text name="midas.catologos.cobpaquetessecciones.titulo"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<s:select key="midas.general.seccion" name="idToSeccionName" id="idToSeccionName" 
					  list="secciones" listKey="idToSeccion" listValue="%{descripcion + ' - Versi\u00F3n: ' + version}" 
					  labelposition="left"
					  onchange="getPaquetes($('idToSeccionName'), $('idPaqueteName'));" 
					  headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
		</td>

		<td colspan="2">
			<s:select key="midas.general.paquete" name="idPaqueteName" id="idPaqueteName" 
					  labelposition="left"
					  list="paquetes" listKey="id" listValue="descripcion" onchange="iniciaGridsCoberturaSeccion();" 
					  headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
		</td>
	</tr>
</table>
<div id="detalle" style="height:320px">
	<center>
		<table id="desplegarDetalle" border="0">
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.catalogos.asociar.seccion.paquete.cobertura"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.catalogos.lista.cobertura.asociada"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="coberturaSeccionAsociadasGrid" class="dataGridConfigurationClass" style="height:130px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.catalogos.lista.cobertura.disponible"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="coberturaSeccionDisponiblesGrid" class="dataGridConfigurationClass" style="height:130px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>
</div>	
</s:form>
	