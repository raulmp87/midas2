<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/colorvehiculo/colorVehiculoHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="colorVehiculoForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.colorvehiculo.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="colorVehiculo.clave" id="txtClave" 
					key="midas.catalogos.clave" cssStyle="cajaTexto"
					labelposition="left" maxlength="5"
					onkeypress="return soloNumeros(this, event, false)"
					onfocus="javascript: new Mask('######', 'number').attach(this)"
				>
				</s:textfield>
			</td>

			<td colspan="2">
				<s:textfield name="colorVehiculo.descripcion" id="txtDescripcion" 
					key="midas.catalogos.descripcion" cssStyle="cajaTexto"
					labelposition="left" 
					styleClass="jQalphaextra jQrestrict jQtoUpper cajaTexto"
							 >
				</s:textfield> 
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript:listarFiltradoColorVehiculo();return false;">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</s:form>

<div id ="colorVehiculoGrid" style="width:97%;height:247px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div class="alinearBotonALaDerecha" style="margin-right: 30px">
	<div id="b_agregar">
		<a href="javascript: void(0);"
			onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoColorVehiculo);return false;">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>


