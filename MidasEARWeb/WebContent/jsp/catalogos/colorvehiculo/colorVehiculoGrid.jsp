<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>11</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>  
        </beforeInit>
        <afterInit>        	
        </afterInit>
		
		<column id="id" type="ro" width="0px" sort="int" hidden="true">id</column>
		<column id="clave" type="ro" width="200px" sort="int"><s:text name="midas.catalogos.colorvehiculo.codigocolor"/></column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.catalogos.colorvehiculo.descripcion"/></column>
		<column id="accionVer" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>
		<column id="accionBorrar" type="img" width="30px" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="colorVehiculoList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clave" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleColorVehiculo)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleColorVehiculo)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleColorVehiculo)^_self</cell>
		</row>
	</s:iterator>
</rows>