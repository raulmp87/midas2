<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script src="<s:url value='/js/midas2/catalogos/colorvehiculo.js'/>"></script>
  
    <script type="text/javascript">
    var listarFiltradoColorVehiculoPath = '<s:url action="listarFiltrado" namespace="/colorvehiculo"/>';
	var verDetalleColorVehiculoPath = '<s:url action="verDetalle" namespace="/colorvehiculo"/>';
	var guardarColorVehiculoPath = '<s:url action="guardar" namespace="/colorvehiculo"/>';
	var eliminarColorVehiculoPath = '<s:url action="eliminar" namespace="/colorvehiculo"/>';
	var mostrarCatalogoColorVehiculoPath = '<s:url action="mostrarCatalogo" namespace="/colorvehiculo"/>';
	</script>