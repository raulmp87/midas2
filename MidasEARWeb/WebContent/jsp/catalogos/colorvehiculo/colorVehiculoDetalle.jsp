<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/colorvehiculo/colorVehiculoHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarColorVehiculo();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="colorVehiculo.id != null">
		<s:set id="readEditOnly" value="false" />
		<s:set id="readOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.colorvehiculo.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.colorvehiculo.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.colorvehiculo.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarColorVehiculo();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.colorvehiculo.borrar.titulo')}" />
</s:else>
<s:if test="mensaje!=null">
 <s:set id="readOnly" value="false" />
</s:if>

<s:form action="guardar" id="colorVehiculoForm">
	<table  id="agregar" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="colorVehiculo.id"/>
				<s:hidden name="tipoAccion"/>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.clave"></s:text>
			</th>
			<td> 
				<s:textfield name="colorVehiculo.clave" id="txtClave" 
					key="" maxlength="5" cssClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200 h16"
					labelposition="left" disabled="#readOnly"
					onkeypress="return soloNumerosM2(this, event, false)" 
					readonly="#readOnly" required="#requiredField"
				/>
				<s:if test="#readOnly == true">
					<s:hidden name="colorVehiculo.clave"></s:hidden>
				</s:if>
			</td>
			<td></td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.descripcion"></s:text>
			</th>
			<td> 
				<s:textfield name="colorVehiculo.descripcion" 
				id="txtDescripcion" key="" maxlength="100"
				cssClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200 h16"
				labelposition="left"  readonly="#readEditOnly" 
				required="#requiredField" disabled="#readEditOnly"
				/> 
			</td>
			<td></td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="4">
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" 
								value="%{#claveTextoBoton}" 
								cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
   	 			</td>
			</tr>
		</s:if>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoColorVehiculo(); return false;" 
						          cssClass="b_submit icon_regresar w100" /> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="4"> 
					<span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>



