    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/parametrogeneral.js'/>"></script>

<s:form id="parametroGeneralForm" name="parametroGeneralForm"
	cssStyle="background:white;">
	<table width="98%" border="0" id="filtros">
		<tbody>
			<tr>
				<td colspan="6" class="titulo"><s:label value="Parametros Generales" />
				</td>
			</tr>
		</tbody>
	</table>
	<div>
		<div id="parametroGeneralGrid" style="width: 98%; height:535px;"></div>
	</div>
</s:form>
<div>&nbsp;</div>
<script type="text/javascript">
	obtenerParametrosGenerales();
</script>