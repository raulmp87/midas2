<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="parametroGeneral.id.idToGrupoParametroGeneral" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="parametroGeneral.id.codigoParametroGeneral" type="ro" width="0" sort="int" hidden="true">codigo</column>
		<column id="parametroGeneral.grupoParametroGeneral.descripcionGrupoParametroGral" type="ro" width="*" sort="str" >Grupo</column>
		<column id="parametroGeneral.descripcionParametroGeneral" type="ro" width="*" sort="str" >Descripcion</column>
		<column id="parametroGeneral.claveTipoValor" type="ro" width="50" sort="int" hidden="true">Tipo</column>
		<column id="parametroGeneral.valor" type="ed" width="*" sort="str" >Valor</column>
	</head>
		
	<% int a=0;%>
	<s:iterator value="listaParametroGeneral">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idToGrupoParametroGeneral" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.codigoParametroGeneral" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="grupoParametroGeneral.descripcionGrupoParametroGral" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="descripcionParametroGeneral" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoValor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valor" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>
</rows>