<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<table width="99%" class="contenedorConFormato" align="center">
<tr>
    <s:if test="tipoAccion != 5">
        <td><s:text name="midas.fuerzaventa.negocio.ultimaModificacion"/></td>
    </s:if>
    <s:else>
        <td><s:text name="midas.fuerzaventa.negocio.fechaModificacion"/></td>
    </s:else>	
	<td><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" readonly="true"/></td>
	<td><s:text name="midas.suscripcion.solicitud.asignar.usuario"/></td>
	<td><s:textfield name="ultimaModificacion.usuarioActualizacion" readonly="true"/></td>
	<td>
	    <s:if test="tipoAccion != 5">
	        <div class="w150" style="display: block; float:right;">
				<div class="btn_back w140"  >
					<a href="javascript: mostrarHistorico();" class="icon_guardar">	
						<s:text name="midas.boton.historico"/>	
					</a>
	            </div>
            </div>
        </s:if>						
	</td>
</tr>
</table>