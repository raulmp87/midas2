<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="tipoAccion"/>
<s:hidden name="personaSeycos.idPersona" />
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="1" />
	<s:set id="display" value="true" />
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1" />
</s:if>
<s:form action="guardarDatosFiscales" id="personaDatosOficinaForm">
<s:hidden id="persoPais" name="personaSeycos.domicilios[0].clavePais" />
<s:hidden id="persoEstado" name="personaSeycos.domicilios[0].claveEstado" />
<s:hidden id="persoCiudad" name="personaSeycos.domicilios[0].claveCiudad" />
<s:hidden id="persoColonia" name="personaSeycos.domicilios[0].nombreColonia" />
<s:hidden id="persoCalleNumero" name="personaSeycos.domicilios[0].calleNumero" />
<s:hidden id="persoCP" name="personaSeycos.domicilios[0].codigoPostal" />

<s:hidden id="fiscPais" name="personaSeycos.domicilios[1].clavePais" />
<s:hidden id="fiscEstado" name="personaSeycos.domicilios[1].claveEstado" />
<s:hidden id="fiscCiudad" name="personaSeycos.domicilios[1].claveCiudad" />
<s:hidden id="fiscColonia" name="personaSeycos.domicilios[1].nombreColonia" />
<s:hidden id="fiscCalleNumero" name="personaSeycos.domicilios[1].calleNumero" />
<s:hidden id="fiscCP" name="personaSeycos.domicilios[1].codigoPostal" />
<s:hidden name="idTipoOperacion" value="90"/>
	<table width="99%" class="contenedorConFormato" align="center">				
		<tr>
			<td class="titulo" colspan="6" >
				<s:text name="midas.fuerzaventa.datosOficina"/>
			</td>
		</tr>		
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.telefonoOficina"/>*
			</td>
			<td>
				<s:textfield name="personaSeycos.telOficina" id="txtTelefonoOficina" cssClass="jQtelefono jQrequired" readonly="#readOnly" maxlength="12"/>				
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.extension"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.extension" id="txtExtension" cssClass="jQalphanumeric" readonly="#readOnly"/>				
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.fax"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.telFax" id="txtFax" cssClass="jQalphanumeric" readonly="#readOnly" maxlength="12"/>				
			</td>
		</tr>
</table>	
<table width="99%" class="contenedorConFormato" align="center">		
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.boton.domicilio"/>
			</td>
		</tr>
		<s:if test="tipoAccion == 1 || tipoAccion == 4">		
			<tr>
				<td>
				<div align="left" class="inline" >
				Copiar Domicilio Personal				
					<div class="btn_back w20">				
						<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioPersona(2,jQuery('#persoPais').val(),jQuery('#persoEstado').val(),jQuery('#persoCiudad').val(),jQuery('#persoColonia').val(),jQuery('#persoCalleNumero').val(),jQuery('#persoCP').val());">
							
						</a>
					</div>
					Fiscal
					<div class="btn_back w20">				
						<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioPersona(2,jQuery('#fiscPais').val(),jQuery('#fiscEstado').val(),jQuery('#fiscCiudad').val(),jQuery('#fiscColonia').val(),jQuery('#fiscCalleNumero').val(),jQuery('#fiscCP').val());">
							
						</a>
					</div>
					</div>
				</td>
			</tr>
		</s:if>
		<tr>
		<td colspan="6">
			<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
			<s:param name="idPaisName">personaSeycos.domicilios[2].clavePais</s:param>
			<s:param name="idEstadoName">personaSeycos.domicilios[2].claveEstado</s:param>	
			<s:param name="idCiudadName">personaSeycos.domicilios[2].claveCiudad</s:param>		
			<s:param name="idColoniaName">personaSeycos.domicilios[2].nombreColonia</s:param>
			<s:param name="calleNumeroName">personaSeycos.domicilios[2].calleNumero</s:param>
			<s:param name="cpName">personaSeycos.domicilios[2].codigoPostal</s:param>
			<s:param name="nuevaColoniaName">personaSeycos.domicilios[2].nuevaColonia</s:param>				
			<s:param name="idColoniaCheckName">idColoniaCheck</s:param>		
			<s:param name="labelPais">País*</s:param>	
			<s:param name="labelEstado">Estado*</s:param>
			<s:param name="labelCiudad">Municipio*</s:param>
			<s:param name="labelCalleNumero"><s:text name="midas.fuerzaventa.negocio.calleNumero"/>*</s:param>	
			<s:param name="labelColonia">Colonia*</s:param>
			<s:param name="labelCodigoPostal"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/>*</s:param>
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">2</s:param>
			<s:param name="readOnly" value="%{#readOnly}"></s:param>
			<s:param name="requerido" value="%{#required}"></s:param>	
			<s:param name="enableSearchButton" value="%{#display}"></s:param>
			<s:param name="funcionResult" >populateDomicilioOficina</s:param>							
		</s:action>
		</td>
		</tr>		
		</table>
		<s:include value="/jsp/catalogos/fuerzaventa/persona/personaFooter.jsp"></s:include>
		<s:if test="tipoAccion != 5">
		    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
		</s:if>		
		<div align="right" class="w910 inline" style="width: 99%">
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:guardarDatosOficinaPersona();">
					<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
				</a>
			</div>
			</s:if>
			<s:if test="tipoAccion != 5">
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosFiscales');">
						<s:text name="midas.boton.atras"/>
					</a>
			    </div>			
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosContacto');">
						<s:text name="midas.boton.siguiente"/>
					</a>
			    </div>
			</s:if>										
	</div>
</s:form>

