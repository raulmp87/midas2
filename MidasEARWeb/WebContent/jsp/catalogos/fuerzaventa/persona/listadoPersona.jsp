<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/fuerzaventa/persona/personaHeader.jsp"/>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
jQuery(function(){
	var tipoAccion='<s:property value="tipoAccion"/>';
	var urlFiltro=listarFiltradoPersonaPath+"?tipoAccion="+tipoAccion;
	var idField='<s:property value="idField"/>';
	urlFiltro=urlFiltro+"&cargaInicial=1";
	listarFiltradoGenerico(urlFiltro,"personaGrid", null,idField,"responsableModal");
});
</script>
<s:form action="cargarListadoPersona" id="personaForm" name="personaForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="920px" id="filtrosM2" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.catalogos.agente.persona.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th width="150px">
				<s:text name="midas.catalogos.centro.operacion.nombre"/> / <s:text name="midas.catalogos.centro.operacion.razonSocial"/>
			</th>	
			<td width="325px">
				<s:textfield name="personaSeycos.nombre" id="txtNombreLPers" cssClass="w250 cajaTextoM2 " maxlength="40"/>				
			</td>
			<th><s:text name="midas.agentes.afianzadora.personalidadJuridica"></s:text></th>			
			<td><s:select list="#{'1':'Fisica', '2':'Moral'}" name="personaSeycos.claveTipoPersona" id="txtClaveTipoPersona"
						headerKey="" headerValue="Seleccione" cssClass="cajaTextoM2 "></s:select></td>
<!-- 			<th width="95px"> -->
<%-- 				<s:text name="midas.fuerzaventa.negocio.razonSocial"/> --%>
<!-- 			</th>			 -->
<!-- 			<td> -->
<%-- 				<s:textfield name="personaSeycos.razonSocial" id="txtRazonSocial" cssClass="w250 cajaTextoM2 " maxlength="200"/> --%>
<!-- 			</td> -->
		</tr>
		<tr>			
			<th>
				<s:text name="midas.negocio.agente.rfc"/>
			</th>			
			<td>
				<s:textfield name="personaSeycos.codigoRFC" id="txtRFC" cssClass="w250 cajaTextoM2 " maxlength="200"/>
			</td>
			<th>
				<s:text name="midas.catalogos.centro.operacion.telefono"/>
			</th>
			<td>
				<s:textfield name="personaSeycos.telOficina" id="txtTelefonoCasa" cssClass="w250 cajaTextoM2 jQnumeric jQrestrict" maxlength="10"/>				
			</td>		
		</tr>
		<tr>			
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaInicio"/>
			</th>			
			<td><sj:datepicker name="personaSeycos.fechaAlta" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaFin"/>
			</th>
			<td><sj:datepicker name="personaSeycos.fechaBaja" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>				
		</tr>		
		<tr>
			<td class="JS_hide" colspan="4">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">personaSeycos.domicilios[0].clavePais</s:param>
					<s:param name="idEstadoName">personaSeycos.domicilios[0].claveEstado</s:param>	
					<s:param name="idCiudadName">personaSeycos.domicilios[0].claveCiudad</s:param>		
					<s:param name="idColoniaName">personaSeycos.domicilios[0].nombreColonia</s:param>
					<s:param name="calleNumeroName">personaSeycos.domicilios[0].calleNumero</s:param>
					<s:param name="cpName">personaSeycos.domicilios[0].codigoPostal</s:param>	
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="componente">6</s:param>						
				</s:action>
			</td>
		</tr>
		<tr>
			<td>
				<div style="display: block" id="masFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('masFiltros');">
						<s:text name="midas.boton.masFiltros"/>
					</a>
				</div>
				<div style="display:none" id="menosFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('menosFiltros');">
						<s:text name="midas.boton.menosFiltros"/>
					</a>
				</div>
			</td>
			<td colspan="4" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
				
						onclick="javascript: obtenerCondiciones('${idField}');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>	
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="personaGrid" width="918px" height="200px" style="background-color:white;overflow:hidden"></div>		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w920">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:cargarDatosPersona(1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>