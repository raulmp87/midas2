<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		
		<column id="personaSeycosList.idPersona" type="ro" width="90" sort="int" >Id</column>
		<column id="personaSeycosList.nombre" type="ro" width="*" sort="str" ><s:text name="midas.catalogos.fuerzaventa.nombreORazonSocial"/></column>
		<column id="personaSeycosList.fechaNacimiento" type="ro" width="170" sort="str"><s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/></column>
		<column id="personaSeycosList.codigoCURP" type="ro" width="200" sort="str"><s:text name="midas.fuerzaventa.curp"/></column>
		<column id="personaSeycosList.tipoSituacion" type="ro" width="80" sort="str"><s:text name="midas.catalogos.centro.operacion.situacion"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>		 								
	</head>
	<s:iterator value="personaSeycosList" var="persona" status="index">
		<row id="${index.count}">			
			<cell><![CDATA[${persona.idPersona}]]></cell>
			<cell><![CDATA[${persona.nombreCompleto}]]></cell>
			<cell><![CDATA[${persona.fechaNacimiento}]]></cell>
			<cell><![CDATA[${persona.codigoCURP}]]></cell>
			<s:if test="situacionPersona==\"AC\"">	
			<cell>ACTIVO</cell>			
			</s:if>				
			<s:else>
			<cell>INACTIVO</cell>
			</s:else>	
			<s:if test="tipoAccion!=\"consulta\"">	
	            <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetallePersonaPath, 2,{"personaSeycos.idPersona":${persona.idPersona},"idRegistro":${persona.idPersona},"idTipoOperacion":90})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetallePersonaPath, 4,{"personaSeycos.idPersona":${persona.idPersona},"idRegistro":${persona.idPersona},"idTipoOperacion":90})^_self</cell>					
				<s:if test="situacionPersona==\"AC\"">
				<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetallePersonaPath, 3,{"personaSeycos.idPersona":${persona.idPersona},"idRegistro":${persona.idPersona},"idTipoOperacion":90})^_self</cell>
				</s:if>								
            </s:if>				
		</row>		
	</s:iterator>	
</rows>
