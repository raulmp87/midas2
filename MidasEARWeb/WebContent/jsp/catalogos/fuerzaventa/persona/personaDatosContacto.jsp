<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="tipoAccion"/>
<s:hidden name="personaSeycos.idPersona"/>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="1" />
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="0" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="1" />
</s:if>
<s:form action="guardarDatosContacto" id="personaDatosContactoForm">
<s:hidden name="idTipoOperacion" value="90"/>

	<table width="880px" class="contenedorFormas" bgcolor="white" align="center">				
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.fuerzaventa.datosContacto"/>
			</td>
		</tr>		
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.telefonoCasa"/>*
			</td>
			<td>
				<s:textfield name="personaSeycos.telCasa" id="txtTelefonoCasa" cssClass="cajaTextoM2 jQtelefono jQrequired w180" maxlength="12" readonly="#readOnly"/>				
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.telefonoCelular"/>*
			</td>
			<td>
				<s:textfield name="personaSeycos.telCelular" id="txtCelular" cssClass="cajaTextoM2 jQtelefono jQrequired w180" maxlength="12" readonly="#readOnly"/>				
			</td>			
		</tr>
		<tr>
			<td>
				<s:text name="midas.catalogos.centro.operacion.correoElectronico"/>*
			</td>
			<td>
				<s:textfield name="personaSeycos.email" id="txtCorreo" cssClass="cajaTextoM2 jQemail jQrequired w180" maxlength="60" readonly="#readOnly"/>				
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.facebook"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.faceBook" id="txtFacebook" cssClass="cajaTextoM2 w180" maxlength="50" readonly="#readOnly"/>				
			</td>
			<td>
				<s:text name="midas.fuerzaventa.twitter"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.twitter" id="txtTwitter"  cssClass="cajaTextoM2 w180" maxlength="50" readonly="#readOnly"/>				
			</td>			
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.paginaWeb"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.paginaWeb" id="txtPaginaWeb" cssClass="cajaTextoM2 w180" maxlength="50" readonly="#readOnly"/>				
			</td>
		</tr>
		<tr>
			<td valign="top">
				<s:text name="midas.fuerzaventa.telefonosAdicionales"/>
			</td>
			<td>
				<s:textarea rows="15" name="personaSeycos.telefonosAdicionales"  id="txtTelsAdicionales" cssClass="areaTextoM2 jQalphanumeric w180" cols="30" readonly="#readOnly"/>				
			</td>
			<td valign="top">
				<s:text name="midas.fuerzaventa.correosAdicionales" />*
			</td>
			<td>
				<s:textarea rows="15"  name="personaSeycos.correosAdicionales" id="txtCorreosAdicionales" cssClass="areaTextoM2 jQEmailList jQrequired w180" cols="30" readonly="#readOnly" onkeypress="return quitaComa(event);"/>	<!-- jQrequired -->		
			</td>			
		</tr>
	</table>
	<s:include value="/jsp/catalogos/fuerzaventa/persona/personaFooter.jsp"></s:include>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>		
	<div align="right" class="w910 inline" style="width: 99%">
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:guardarDatosContactoPersona();">
					<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
				</a>
			</div>
			</s:if>	
			<s:if test="tipoAccion != 5">
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosOficina');"">
						<s:text name="midas.boton.atras"/>
					</a>
			    </div>	
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:salirTabsPersona();">
						<s:text name="midas.boton.salir"/>
					</a>
			    </div>
			</s:if>									
	</div>
</s:form>