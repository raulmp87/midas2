<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script>
<s:hidden name="tipoAccion" id ="tipoAccion"/>
<s:hidden name="personaSeycos.idPersona" />
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="1" />
	<s:set id="display" value="true" />	
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 3 && tipoAccion == 5">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1" />
</s:if>
<s:form action="guardarDatosFiscales" id="personaDatosFiscalesForm">
<s:hidden id="persoPais" name="personaSeycos.domicilios[0].clavePais" />
<s:hidden id="persoEstado" name="personaSeycos.domicilios[0].claveEstado" />
<s:hidden id="persoCiudad" name="personaSeycos.domicilios[0].claveCiudad" />
<s:hidden id="persoColonia" name="personaSeycos.domicilios[0].nombreColonia" />
<s:hidden id="persoCalleNumero" name="personaSeycos.domicilios[0].calleNumero" />
<s:hidden id="persoCP" name="personaSeycos.domicilios[0].codigoPostal" />

<s:hidden name="idTipoOperacion" value="90"/>
<s:hidden name="personaSeycos.claveTipoPersona" id="hiddenTipoPersona"/>
	<table width="99%" class="contenedorConFormato" align="center">				
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.fuerzaventa.datosFiscales"/>
			</td>
		</tr>
		<tr>
			<td class="w100">
				<s:text name="midas.fuerzaventa.personalidadJuridica"/>*
			</td>
			<td colspan="2">				
				<s:select name="personaSeycos.claveTipoPersona" cssClass="jQrequired w100" id="tipoPers"
				       disabled="true" onclick="javascript:tipoPersonaEnableDisable(jQuery('#tipoPers').val());"
				       onchange="show_hide_BtnGenerarRFC();"
				      list="#{'1':'FISICA', '2':'MORAL'}" value="personaSeycos.claveTipoPersona"/>				
			</td>
			<td>
				<div id="fechaConstit">
					<div class="elementoInline">
							<s:text name="Fecha Constitución"/>*
					</div>
					<div class="elementoInline">
							<s:textfield name="personaSeycos.fechaConstitucion" readonly="true"/>
					</div>
				</div>
			</td>	
		</tr>			
		<tr id="datosFisica">
			<td id="lNombre">
				<s:text name="midas.negocio.nombres"/>*	
			</td>
			<td id="tNombre">
				<s:textfield name="personaSeycos.nombre" id="txtNombreFiscal" cssClass="w200 " maxlength="40" readonly="true"/>				
			</td>
			<td id="lAPaterno">
				<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/>*	
			</td>
			<td id="tAPaterno">
				<s:textfield name="personaSeycos.apellidoPaterno" id="txtApellidoPaternoFiscal" cssClass="w140 " maxlength="20" readonly="true"/>				
			</td>
			<td id="lAMaterno">
				<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/>*	
			</td>
			<td id="tAMaterno">
				<s:textfield name="personaSeycos.apellidoMaterno" id="txtApellidoMaternoFiscal" cssClass="w140 " maxlength="20" readonly="true"/>				
			</td>
		</tr>
		<tr>
			<td id="lRSocial">
				<s:text name="midas.fuerzaventa.negocio.razonSocial"/>*	
			</td>			
			<td id="tRSocial" class="w100">
				<s:textfield name="personaSeycos.razonSocial" id="txtRazonSocialFiscal" cssClass="w200 " maxlength="200" readonly="true"/>
			</td>
			<td>
				<s:text name="RFC"/>*
			</td>
			<td>			
                 <div id="rfc">
                       <div class="elementoInline">                       	
                       		<s:textfield name="rfcSiglas" id="txtRfcSiglas" cssClass="w40" maxlength="4" readonly="true"/>
                       </div>
                       <div class="elementoInline">                       
                       		<s:textfield name="rfcFecha" id="txtRfcFecha" cssClass="w50" maxlength="6" readonly="true"/>
                       </div>
                       <div class="elementoInline">                       
                       		<s:textfield name="rfcHomoclave" id="txtRfcHomoclave" cssClass="w40" maxlength="3" readonly="true"/>
                       </div>                     
                 </div>                
            </td>		
		</tr>
		<tr>
			<td id="lRepLegal">
				<s:text name="midas.fuerzaventa.altaPorInternet.contacOrepresentanteLeg"/>*
			</td>			
			<td id="tRepLegal">
				<s:if test="tipoAccion==1 || tipoAccion==4">
					<s:textfield name="personaSeycos.idRepresentante" id="txtIdRepresentanteFiscal" cssClass="w200 jQnumeric" readonly="true" onchange="javascript:findByIdResponsable(this.value);"/>
				</s:if>
				<s:else>
					<s:textfield name="personaSeycos.idRepresentante" id="txtIdRepresentanteFiscal" cssClass="w200" readonly="true" onchange="javascript:findByIdResponsable(this.value);"/>
				</s:else>			
			</td>
			<s:if test="tipoAccion==1 || tipoAccion==4">
				<td>
					<div id="botonRep" class="btn_back w80">
						<a href="javascript: void(0)" class="icon_buscar" 
							onclick="mostrarModalResponsable();">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</td>
			</s:if>
			<td id="tRepLegalNombre">
				<s:textfield id="txtNombreRepresentanteFiscal" cssClass="w200" readonly="true" name="personaSeycos.nombreRepresentanteLegal"/>				
			</td>			
		</tr>		
		<tr>
		<s:if test="tipoAccion==1 || tipoAccion==4">
			<td>Copiar Domicilio Personal</td>
			
				<td>
					<div class="btn_back w70">
						<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioPersona(1,jQuery('#persoPais').val(),jQuery('#persoEstado').val(),jQuery('#persoCiudad').val(),jQuery('#persoColonia').val(),jQuery('#persoCalleNumero').val(),jQuery('#persoCP').val());">
							copiar
						</a>
					</div>
				</td>
			</s:if>
		</tr>
		<tr>
		<td colspan="6">
			<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
			<s:param name="idPaisName">personaSeycos.domicilios[1].clavePais</s:param>
			<s:param name="idEstadoName">personaSeycos.domicilios[1].claveEstado</s:param>	
			<s:param name="idCiudadName">personaSeycos.domicilios[1].claveCiudad</s:param>		
			<s:param name="idColoniaName">personaSeycos.domicilios[1].nombreColonia</s:param>
			<s:param name="calleNumeroName">personaSeycos.domicilios[1].calleNumero</s:param>
			<s:param name="cpName">personaSeycos.domicilios[1].codigoPostal</s:param>		
			<s:param name="nuevaColoniaName">personaSeycos.domicilios[1].nuevaColonia</s:param>
			<s:param name="idColoniaCheckName">idColoniaCheck</s:param>		
			<s:param name="labelPais">País*</s:param>	
			<s:param name="labelEstado">Estado*</s:param>
			<s:param name="labelCiudad">Municipio*</s:param>
			<s:param name="labelCalleNumero"><s:text name="midas.fuerzaventa.negocio.calleNumero"/>*</s:param>	
			<s:param name="labelColonia">Colonia*</s:param>
			<s:param name="labelCodigoPostal"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/>*</s:param>
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">2</s:param>
			<s:param name="readOnly" value="%{#readOnly}"></s:param>
			<s:param name="requerido" value="%{#required}"></s:param>	
			<s:param name="enableSearchButton" value="%{#display}"></s:param>
				<s:param name="funcionResult" >populateDomicilioFiscal</s:param>  						
			</s:action>
		</td>
		</tr>
		</table>
		<s:include value="/jsp/catalogos/fuerzaventa/persona/personaFooter.jsp"></s:include>
		<s:if test="tipoAccion != 5">
		    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
		</s:if>		
		<div align="right" class="99% inline" style="width: 99%">
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:guardarDatosFiscalesPersona();">
					<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
				</a>
			</div>
			</s:if>
			<s:if test="tipoAccion != 5">
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosGenerales');">
						<s:text name="midas.boton.atras"/>
					</a>
			    </div>			
			    <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosOficina');">
						<s:text name="midas.boton.siguiente"/>
					</a>
			    </div>
			</s:if>
								
	</div>
</s:form>
<script type="text/javascript">	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=txtIdRepresentanteFiscal";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			jQuery("#txtIdRepresentanteFiscal").val(idResponsable);
			jQuery("#txtNombreRepresentanteFiscal").val(nombre);
		}
	}
	
	mostrarOcultarCamposPersonaFiscal();
	
	if  (jQuery("#tipoAccion").val() == 5){
		jQRemoveClass('jQrequired');
// 		alert('ok');jQuery("#tipoAccion").removeClass('jQrequired');
	}
</script>
