<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="tipoAccion"/>
<s:hidden name="personaSeycos" />
<s:hidden name="personaSeycos.idPersona"/>
<s:hidden name="tabActiva"/>
<s:include value="/jsp/catalogos/fuerzaventa/persona/personaHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script>
<script type="text/javascript">
jQuery(function(){
	dhx_init_tabbars();
	incializarTabsPersona();
});
</script>

<s:hidden name="ultimaModificacion.fechaHoraActualizacionString"/>
<s:if test="tipoAccion == 1">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.persona.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.persona.tituloConsultar')}"/>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.persona.tituloEliminar')}"/>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.persona.tituloEditar')}"/>
</s:if>

<s:if test="tipoAccion != 5">
    <div class="titulo w400"><s:text name="#titulo"/></div>
</s:if>
<div select="<s:property value="tabActiva"/>" hrefmode="ajax-html" style="height: 430px; width: 900px;" id="configuracionPersonaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="datosGenerales" name="Datos Generales" href="http://void" extraAction="javascript: cargarDatosGralesPersona();"></div>	
	<div width="100%" id="datosFiscales" name="Datos Fiscales" href="http://void" extraAction="javascript: cargarDatosFiscalesPersona();"></div>
	<div width="100%" id="datosOficina" name="Datos de Oficina" href="http://void" extraAction="javascript: cargarDatosOficinaPersona();"></div>
	<div width="100%" id="datosContacto" name="Datos de Contacto" href="http://void" extraAction="cargarDatosContactoPersona();"></div>
		
</div>

