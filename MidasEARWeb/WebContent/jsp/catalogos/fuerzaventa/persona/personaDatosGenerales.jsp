<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="tipoAccion"/>
<s:hidden name="rangoFechas" id="txtFechaActual"/>
<s:hidden name="personaSeycos.idPersona" id="personaSeycos.idPersona"/>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="requiredNacimiento"  />	
	<s:set id="requiredDomActual" value="1" />
	<s:set id="display" value="true" />	
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="requiredDomActual" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="requiredDomActual" value="0" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="requiredDomActual" value="1" />
</s:if>
<s:if test="personaSeycos.idPersona!=null">
<s:set id="enabledisable" value="true" />
</s:if>
<script type="text/javascript">
show_hide_BtnGenerarRFC();
</script>
<s:form action="guardarDatosGenerales" id="personaDatosGeneralesForm">
<s:hidden name="idTipoOperacion" value="90"/>
<s:hidden name="isAdministradorAgente" id="isAdministradorAgente"/>
	<table width="99%" class="contenedorConFormato" align="center">	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.idPersona"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.idPersona" readonly="true"/>
			</td>
			<td>
				<s:text name="midas.catalogos.centro.operacion.situacion"/>*
			</td>
			<td>			
			<s:hidden name="personaSeycos.situacionPersona"></s:hidden>		
			<s:select name="personaSeycos.situacionPersona" cssClass="jQrequired w100" id="txtStatusPersona" disabled="true"
				      onclick="javascript:void(0);"
				      list="#{'IN':'INACTIVO','AC':'ACTIVO'}" value="personaSeycos.situacionPersona"/>				
			</td>					
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.fechaAlta"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.fechaAlta" readonly="true"/>
			</td>
			<td>
				<s:text name="Fecha Inactivo"/>
			</td>
			<td>
				<s:textfield name="personaSeycos.fechaBaja" id="txtFechaBAja" readonly="true"/>
			</td>
			<td>
				<div id="txtSectorFinanciero">
					<s:text name="midas.agentes.sector"/>
				</div>
			</td>
			<td colspan="1">
				<div id="selSectorFinanciero">
					<s:select name="personaSeycos.claveSectorFinanciero" cssClass="w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly" id="txtSectorFinanciero"
				        list="sectorList" listKey="idSector" listValue="nombreSector"/>
				</div>
			</td>			
		</tr>		
		<tr>
			<td class="titulo" colspan="6" >
				<s:text name="midas.catalogos.centro.operacion.datosGenerales"/>
			</td>
		</tr>	
		<tr>
			<td class="w100">
				<s:text name="midas.fuerzaventa.tipoPersona"/>*
			</td>
			<td colspan="2">
			<s:if test="(tipoAccion !=5)">				
				<s:select name="personaSeycos.claveTipoPersona" cssClass="jQrequired w100" id="tipoPers"
				       disabled="#enabledisable" onclick="javascript:tipoPersonaEnableDisable(jQuery('#tipoPers').val());"
				       onchange="show_hide_BtnGenerarRFC();javascript:clearRfc();" 
				      list="#{'1':'FISICA', '2':'MORAL'}" value="personaSeycos.claveTipoPersona"/>
			</s:if>
			<s:else>
				<s:select name="personaSeycos.claveTipoPersona" cssClass="w100"
				       disabled="true"
				      list="#{'1':'FISICA', '2':'MORAL'}" value="personaSeycos.claveTipoPersona"/>
			</s:else>
				      <s:if test="personaSeycos.idPersona!=null">
				      	<s:hidden name="personaSeycos.claveTipoPersona" id="hiddenTipoPersona"/>
				      </s:if>				
			</td>			
		</tr>
		<tr>	
			<td>
				<div  id="trRazonSocial"> 
					<s:text name="midas.fuerzaventa.negocio.razonSocial"/>*
				</div>
			</td>			
			<td colspan="2">
				<div id="trRazonSocial1">
					<s:if test="(tipoAccion ==5)">
						<s:textfield name="personaSeycos.razonSocial"  cssClass="w250" maxlength="200" readonly="#readOnly"/>
					</s:if>
					<s:else>
						<s:textfield name="personaSeycos.razonSocial" id="txtRazonSocial" cssClass="w250" maxlength="200" readonly="#readOnly"/>
					</s:else>
				</div>
			</td>	
		</tr>
		<tr>
			<td>
				<div id="lNombre">
					<s:text name="midas.negocio.nombres"/>*
				</div>
			</td>	
			<td>
				<div id="tNombre">
					<s:if test="(tipoAccion ==5)">
						<s:textfield name="personaSeycos.nombre" readonly="#readOnly" cssClass="w160" maxlength="40"/>
					 </s:if>
					 <s:else>
						<s:textfield name="personaSeycos.nombre" readonly="#readOnly" id="txtNombre" cssClass="w160" maxlength="40" onchange='javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#fecha").val(),"rfcSiglas","rfcFecha","rfcHomoclave");'/>
					</s:else>				
				</div>
			</td>
			<td>
				<div  id="lAPaterno">
					<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/>*
				</div>
			</td>	
			<td>
				<div id="tAPaterno"> 
				<s:if test="(tipoAccion ==5)">
					<s:textfield name="personaSeycos.apellidoPaterno" readonly="#readOnly"/>
				</s:if>
				<s:else>
					<s:textfield name="personaSeycos.apellidoPaterno" readonly="#readOnly" id="txtApellidoPaterno" cssClass="w140" maxlength="20" onblur='javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#fecha").val(),"rfcSiglas","rfcFecha","rfcHomoclave");'/>
				</s:else>				
				</div>
			</td>
			<td class="w100">
				<div id="lAMaterno">
					<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/>*
				</div>
			</td>	
			<td>
				<div id="tAMaterno"> 
				<s:if test="(tipoAccion ==5)">
					<s:textfield name="personaSeycos.apellidoMaterno" readonly="#readOnly" cssClass="w150" maxlength="20" />
				</s:if>
				<s:else>
					<s:textfield name="personaSeycos.apellidoMaterno" readonly="#readOnly" id="txtApellidoMaterno" cssClass="w150" maxlength="20" onblur='javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#fecha").val(),"rfcSiglas","rfcFecha","rfcHomoclave");'/>
				</s:else>
				</div>
			</td>			
		</tr>	
		<tr>
		<td>
			<div id="lSexo">
            	<s:text name="midas.fuerzaventa.sexo"/>*
            </div>
        </td>
		<td>
			<div id="tSexo">
				<s:select name="personaSeycos.sexo" id="sexo" disabled="#readOnly"
				list="#{'M':'Masculino', 'F':'Femenino'}" cssClass="w150 jQrequired"
				headerValue="Seleccione..." headerKey="" labelposition="left"/>	
			</div>	
		</td>	
		<td>
			<div id="lEdoCivil">
				<s:text name="midas.fuerzaventa.estadoCivil"/>*
			</div>
		</td>	
		<td>
			<div id="tEdoCivil">				       
				<s:select name="personaSeycos.estadoCivil" id="estadoCivil" disabled="#readOnly"
				list="estadosCiviles" labelposition="left" headerKey="" headerValue="Seleccione" 
				listKey="idEstadoCivil" listValue="nombreEstadoCivil" cssClass="jQrequired"/>	
			</div>
		</td>		
		</tr>
	</table>	
	<table width="99%" class="contenedorConFormato"  align="center" id="tablaNacimiento">	
		<tr>
		<td colspan="4">
		<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
			<s:param name="idPaisName">personaSeycos.clavePaisNacimiento</s:param>
			<s:param name="idEstadoName">personaSeycos.claveEstadoNacimiento</s:param>	
			<s:param name="idCiudadName">personaSeycos.claveCiudadNacimiento</s:param>			
			<s:param name="labelPais">Nacionalidad*</s:param>	
			<s:param name="labelEstado">Estado de Nacimiento*</s:param>
			<s:param name="labelCiudad">Ciudad de Nacimiento*</s:param>			
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">5</s:param>
			<s:param name="readOnly" value="%{#readOnly}"></s:param>
			<s:param name="requerido" value="%{#requiredDomActual}"></s:param>		
			<s:param name="idDomicilioName">idDomicilio</s:param>								
		</s:action>
		</td>
		</tr>		
		
	</table>	
	<table width="99%" class="contenedorConFormato"  align="center">		
		<tr>
		<td id="fNacimiento" class="w120">
			<s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/>*
		</td>
		<td id="fConstitucion" class="w120">
			<s:text name="Fecha Constitución"/>*
		</td>
		<s:if test="(tipoAccion ==2)||(tipoAccion ==3)||(tipoAccion ==5)">
		<s:if test="personaSeycos.claveTipoPersona==1">
			<td>
				<s:textfield name="personaSeycos.fechaNacimiento"  readonly="#readOnly"></s:textfield>
			</td>
		</s:if>
		<s:else>
			<td>
				<s:textfield name="personaSeycos.fechaConstitucion"  readonly="#readOnly"></s:textfield>
			</td>
		</s:else>		
		</s:if>
		<s:else>		
			<td><div id="fechaPersonaFisica"><sj:datepicker name="personaSeycos.fechaNacimiento"  
								   buttonImage="../img/b_calendario.gif" 
								   id="fecha" maxlength="10" cssClass="jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true" yearRange="-80:-18"
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onchange='esFechaValida(this);javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#fecha").val(),"rfcSiglas","rfcFecha","rfcHomoclave");'></sj:datepicker>
			</div>
			<div id="fechaPersonaMoral">
			<sj:datepicker name="personaSeycos.fechaConstitucion"  
								   buttonImage="../img/b_calendario.gif" 
								   id="fecha2" maxlength="10" cssClass="jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true"
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onchange='esFechaValida(this);javascript:generarYDividirRFCMoral(jQuery("#txtRazonSocial").val(),jQuery("#fecha2").val(),"rfcSiglas","rfcFecha");'></sj:datepicker>
			</div></td>
		</s:else>
		<td>
			<s:text name="RFC"/>*
		</td>		        	                    	
        <td>
           	<div class="elementoInline">
           		<s:if test="(tipoAccion !=5)">     
	           		<s:textfield name="rfcSiglas" id="txtRfcSiglas" cssClass="w40 jQalphabetic jQrestrict" maxlength="4" readonly="#readOnly"/>
	           	</s:if>
	           	<s:else>
	           		<s:textfield name="rfcSiglas" cssClass="w40" maxlength="4" readonly="#readOnly"/>
	           	</s:else>
	         </div>
	         <div class="elementoInline">
	         	<s:if test="(tipoAccion !=5)">                      
	           		<s:textfield name="rfcFecha" id="txtRfcFecha"  cssClass="w50 jQfechaRFC jQnumeric jQrestrict" maxlength="6" readonly="#readOnly"/>
	         	</s:if>
	         	<s:else>
	         		<s:textfield name="rfcFecha" cssClass="w50" maxlength="6" readonly="#readOnly"/>
	         	</s:else>
	         </div>
	         <div class="elementoInline">
	         	<s:if test="(tipoAccion !=5)">                         
	           		<s:textfield name="rfcHomoclave" id="txtRfcHomoclave" cssClass="w40 jQclaveRFC jQalphanumeric jQrestrict" maxlength="4" readonly="#readOnly"/>
	           	</s:if>
	           	<s:else>
	           		<s:textfield name="rfcHomoclave" cssClass="w40" maxlength="4" readonly="#readOnly"/>
	           	</s:else>
           	</div>
                  
        <s:if test="(tipoAccion !=2) && (tipoAccion !=3)&&(tipoAccion !=5)">        
	    <div id="btnGenerarRFC" class="elementoInline"><a href="javascript: void(0);" 
	       onclick='javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#fecha").val(),"rfcSiglas","rfcFecha","rfcHomoclave");'>Generar RFC</a>
	    </div>
	    <div id="btnGenerarRFCMoral" class="elementoInline"><a href="javascript: void(0);" 
	       onclick='javascript:generarYDividirRFCMoral(jQuery("#txtRazonSocial").val(),jQuery("#fecha2").val(),"rfcSiglas","rfcFecha");'>Generar RFC</a>
	    </div>                       
        </s:if>  
        </td>          
		<tr>
		<td class="w120">
			<div id="lCURP">
				<s:text name="midas.fuerzaventa.curp"/>*
			</div>
		</td>	
		<td>
			<div id="tCURP">				
			<div class="elementoInline">
			<s:if test="(tipoAccion !=2) && (tipoAccion !=3)&&(tipoAccion !=5)">  
				<s:textfield name="personaSeycos.codigoCURP" readonly="#readOnly" id="txtCURP" cssClass="jQalphanumeric jQrequired w150" maxlength="18"/>
			</s:if>
			<s:else>
				<s:textfield name="personaSeycos.codigoCURP" readonly="#readOnly" cssClass="w150" maxlength="18"/>
			</s:else>
			</div>				
			<s:if test="(tipoAccion!=2)&&(tipoAccion !=3) &&(tipoAccion !=5)">
			<div class="elementoInline">  
				<a href="http://www.renapo.gob.mx/swb/swb/RENAPO/consultacurp" target="_blank">Consulta CURP</a>	
			</div>					
			<div class="btn_derecha w50">
				<a href="javascript: void(0)"						
				onclick='javascript: validaCURPPersonaAgentesJS();'>
					<s:text name="Validar"/>
				</a>
			</div>
			</s:if>		
			</div>
		</td>
		</tr>		
	</table>			
	<table width="99%" class="contenedorConFormato" align="center" >		
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.boton.domicilio"/>
			</td>
		</tr>
		<tr>
		<td>
		<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
			<s:param name="idPaisName">personaSeycos.domicilios[0].clavePais</s:param>
			<s:param name="idEstadoName">personaSeycos.domicilios[0].claveEstado</s:param>	
			<s:param name="idCiudadName">personaSeycos.domicilios[0].claveCiudad</s:param>		
			<s:param name="idColoniaName">personaSeycos.domicilios[0].nombreColonia</s:param>
			<s:param name="calleNumeroName">personaSeycos.domicilios[0].calleNumero</s:param>
			<s:param name="cpName">personaSeycos.domicilios[0].codigoPostal</s:param>		
			<s:param name="nuevaColoniaName">personaSeycos.domicilios[0].nuevaColonia</s:param>
			<s:param name="idColoniaCheckName">idColoniaCheck</s:param>		
			<s:param name="labelPais">País*</s:param>	
			<s:param name="labelEstado">Estado*</s:param>
			<s:param name="labelCiudad">Municipio*</s:param>
			<s:param name="labelCalleNumero"><s:text name="midas.fuerzaventa.negocio.calleNumero"/>*</s:param>	
			<s:param name="labelColonia">Colonia*</s:param>
			<s:param name="labelCodigoPostal"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/>*</s:param>
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">2</s:param>
			<s:param name="readOnly" value="%{#readOnly}"></s:param>
			<s:param name="requerido" value="%{#requiredDomActual}"></s:param>	
			<s:param name="enableSearchButton" value="%{#display}"></s:param>
			<s:param name="idDomicilioName">idDomicilio1</s:param>
		</s:action>
		</td>
		</tr>
	</table>	
	<s:if test="personaSeycos.idPersona != null">
		<s:include value="/jsp/catalogos/fuerzaventa/persona/personaFooter.jsp"></s:include>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>	
	<div align="right" class="w890 inline" style="width: 99%">
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick='javascript: guardarDatosGeneralesPersona();'>
					<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
				</a>
			</div>	
			</s:if>
			<s:if test="tipoAccion == 3">
				<div id="btnEliminar" class="btn_back w110">
					<a href="javascript: void(0);" class="icon_borrar"
					onclick="eliminarPersona();">
					<s:text name="midas.boton.borrar"/>
					</a>
				</div>	
			</s:if>
				<s:if test="tipoAccion != 5">
		            <div class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:salirTabsPersona();">
						<s:text name="midas.boton.atras"/>
					</a>
				</div>
				<div id="btnSiguiente" class="btn_back w80">
					<a href="javascript: void(0);"
						onclick="javascript:atrasOSiguiente('datosFiscales');">
						<s:text name="midas.boton.siguiente"/>
					</a>
				</div>
            </s:if>					
	</div>
		
</s:form>
<script type="text/javascript">
	tipoPersonaEnableDisable(); 	
	ocultarIndicadorCarga('indicador');
</script>