<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%-- <script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script> --%>
<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
		<link href="<html:rewrite page="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
		
<%-- 		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script> --%>
		
		<sj:head/>
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree_sb.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_srnd.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_nxml.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_dhxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
		<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<s:hidden name="idTipoOperacion"/>
<s:hidden name="idRegistro"/>		
<div id="mainDiv" style="background-color: white;">
<div id="detalle" style="height: 250px;">	
	<div id="historicoGrid" class="dataGridConfigurationClass" style="width:100%;height:200px">&nbsp;</div>
	<div id="pagingAreaHist"></div><div id="infoAreaHist"></div>	
</div>
<script type="text/javascript">	
	var verDetalleHistoricoCentroOperacionPath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/centrooperacion"/>';
	var verDetalleHistoricoGerenciaPath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/gerencia"/>';
	var verDetalleHistoricoEjecutivoPath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/ejecutivo"/>'; 
	var verDetalleHistoricoAfianzadoraPath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/afianzadora"/>';
	var verDetalleHistoricoPromotoriaPath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/promotoria"/>';
	var verDetalleHistoricoAgentePath = '<s:url action="verDetalleHistorico" namespace="/fuerzaventa/agente"/>';
	var verDetalleHistoricoPersonaPath = '<s:url action="mostrarCapturaPersona" namespace="/fuerzaVenta/persona"/>';
	var detalleDiv = 'detalleHistorico';

	obtenerHistorico();	
</script>
<div id="indicador"></div>
<div id="detalleHistorico" style="width: 100%;height: 500px;" >

</div>
</div>