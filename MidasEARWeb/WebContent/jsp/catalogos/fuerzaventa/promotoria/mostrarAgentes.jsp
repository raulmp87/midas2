<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
function addTooltTipJavascript(idElemento){
	var elementoOption = document.getElementById(idElemento).options;
	var numberOption = elementoOption.length;
	for(x=0; x<numberOption; x++){
		var option = elementoOption[x].text;
		elementoOption[x].setAttribute('title',option);
	}
}
//-->
</script>

<s:form action="listarFiltrado" id="agenteForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo">
				Lista de Agentes 
<%-- 				<s:text name="midas.catalogos.centro.operacion.gerencias.titulo"/> --%>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="agMostrar" multiple="true"  name="listAgentes" listValue="persona.nombreCompleto" listKey="id" cssClass="cajaTextoM w350 h200" list="listAgentes" labelposition="left"/>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
<!--
 addTooltTipJavascript('agMostrar');
//-->
</script>
</s:form>