<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/promotoria.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
	var mostrarContenedorPromotoriaPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/promotoria"/>';	
	var verDetallePromotoriaPath = '<s:url action="verDetalle" namespace="/fuerzaventa/promotoria"/>';	
	var urlBusquedaDomicilio = '<s:url action="buscarDomicilio" namespace="/fuerzaventa/promotoria"/>';
	
	var listarPromotoriaPath = '<s:url action="lista" namespace="/fuerzaventa/promotoria"/>';
	
	var listarFiltradoPromotoriaPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/promotoria"/>';

// 	var mostrarContenedorEjecutivoPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/ejecutivo"/>';
	var mostrarAgentesPath = '<s:url action="mostrarAgentes" namespace="/fuerzaventa/promotoria"/>';
	var eliminarProductoBancarioPath='<s:url action="eliminarProductoBancario" namespace="/fuerzaventa/promotoria"/>';
	
</script>