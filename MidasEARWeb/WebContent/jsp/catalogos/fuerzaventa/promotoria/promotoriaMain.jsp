<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/catalogos/fuerzaventa/promotoria/promotoriaHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:hidden name="divCarga"></s:hidden>
<script type="text/javascript">
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		incializarTabs();
	});
</script>
<style type="text/css">
	.clientesForm select{
		width:150px;
	}
	.clientesForm label{
		padding-right:5px;
	}
</style>
<!--/head-->

<body>
	<s:form id="jsonForm">
	</s:form>
	<s:form id="clientForm" cssClass="clientesForm">
		<s:hidden name="promotoria.id" id="promotoria.id"/>
		<s:actionerror/>
		<div id="workAreaClientes">
			<div style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="promotoriaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
				<div width="150px" id="datosPrincipalesPromotoria" name="Datos Generales" href="http://void" >
					<jsp:include page="promotoriaDetalle.jsp" flush="true"></jsp:include>
				</div>
				<div width="150px" id="datosContables" name="Datos de Contacto" href="http://void">
					<jsp:include page="promotoriaDatosContables.jsp" flush="true"></jsp:include>
				</div>
			</div>
		</div>
	</s:form>
</body>
<!--/html-->