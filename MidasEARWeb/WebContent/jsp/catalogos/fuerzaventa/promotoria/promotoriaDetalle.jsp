<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript">
jQIsRequired();
jQuery(document).ready(function(){
	dhx_init_tabbars();
	var tipoAccion='<s:property value="tipoAccion" />';
	incializarTabs(tipoAccion);
	var idResponsable=jQuery("#idPersonaResponsable").val();
	if(jQuery.isValid(idResponsable)){
		findByIdResponsable(idResponsable);
	}
	jQuery("#nombreResponsable").val(jQuery("#descripcionPromotoria").val());
});
var ventanaEjecutivo=null;
var ventanaAgentes=null;
function mostrarModalEjecutivo(){
	var url="/MidasWeb/fuerzaventa/ejecutivo/mostrarContenedor.action?tipoAccion=consulta&idField=idEjecutivo";
	sendRequestWindow(null, url, obtenerVentanaEjecutivo);
}

function obtenerVentanaEjecutivo(){
	var wins = obtenerContenedorVentanas();
	ventanaEjecutivo= wins.createWindow("ejecutivoModal", 400, 320, 900, 450);
	ventanaEjecutivo.center();
	ventanaEjecutivo.setModal(true);
	ventanaEjecutivo.setText("Consulta de Ejecutivos");
	ventanaEjecutivo.button("park").hide();
	ventanaEjecutivo.button("minmax1").hide();
	return ventanaEjecutivo;
}

function findByIdEjecutivo(idejecutivo){
	var url="/MidasWeb/fuerzaventa/ejecutivo/findById.action";
	var data={"ejecutivo.id":idejecutivo};
	jQuery.asyncPostJSON(url,data,populateEjecutivo);
}

function populateEjecutivo(json){
	if(json){
		var ejecutivo=json.ejecutivo;
		var idEjecutivo=ejecutivo.id;
		jQuery("#idEjecutivo").val(idEjecutivo);
		var nombre=ejecutivo.personaResponsable.nombreCompleto;
		jQuery("#descripcionEjecutivo").val(nombre);	
	}
}

function mostrarModalAgentes(){
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtIdAgentePromotor";
	sendRequestWindow(null, url, obtenerVentanaAgentes);
}

function obtenerVentanaAgentes(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 200, 200, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agente");
	ventanaAgentes.button("park").hide();
	ventanaAgentes.button("minmax1").hide();
	return ventanaAgentes;
}

function findByIdAgente(idagente){
	var url="/MidasWeb/fuerzaventa/agente/findById.action";
	var data={"agente.id":idagente};
	jQuery.asyncPostJSON(url,data,populateAgente);
}

function populateAgente(json){
	if(json){		
		var nombre = json.agente.persona.nombreCompleto;
		jQuery("#descripcionAgente").val(nombre);
	}
}	
</script>
<s:include value="/jsp/catalogos/fuerzaventa/promotoria/promotoriaHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:if test="tipoAccion == 5">
	<s:hidden name ="ultimaModificacion.fechaHoraActualizacionString" id ="fechaHoraActualizacString"/>
</s:if>
<s:hidden name="tabActiva"/>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloEditar')}"/>
</s:if>
<s:if test="tipoAccion != 5">
    <div class="titulo w400"><s:text name="#titulo"/></div>
</s:if>
<div select="<s:property value="tabActiva"/>" style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="promotoriaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="150px" id="datosPrincipalesPromotoria" name="Datos Generales" href="http://void" >
		<s:form action="listarFiltrado" id="promotoriaForm">
		<s:hidden id="idPromotoria" name="promotoria.id" value="%{promotoria.id}" />
		<table width="98%" class="contenedorFormas" align="center">
			<tr>
				<td class="titulo" colspan="4" >
					Datos de <s:text name="midas.detalle.promotoria.titulo"/>
				</td>
			</tr>
			<tr>
				<th><s:text name="midas.negocio.promotoria.id" /></th>
				<td><s:textfield value="%{promotoria.id}"  cssClass="cajaTextoM2 w200 jQnumeric" readonly="true" disabled="true"></s:textfield></td>
				<s:hidden name="promotoria.descripcion" id="descripcionPromotoria"/>
				<s:textfield cssStyle="display:none;visibility:none;" id="idPersonaResponsable" name="promotoria.personaResponsable.idPersona" cssClass="cajaTextoM2 w100" onchange="javascript:findByIdResponsable(this.value);"  onchange="javascript:findByIdResponsable(this.value);"/>
			</tr>
			<tr>
				<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.descripcion" /></th>
				<td><s:textfield id="nombreResponsable" name="promotoria.personaResponsable.nombreCompleto" cssClass="cajaTextoM2 w200 jQrequired" readonly="true"></s:textfield></td>
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<td>
					<div class="btn_back w80">
						<a href="javascript: void(0)" class="icon_buscar" 
							onclick="mostrarModalResponsable();">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</td>
				</s:if>
			</tr>
			
			<tr>
				<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.correoElectronico" /></th>
				<td colspan="3"><s:textfield name="promotoria.personaResponsable.email" id="txtCorreo" cssClass="cajaTextoM2 w250 jQemail" readonly="true" disabled="true"></s:textfield></td>
			</tr>		
			<tr>
				<th class="jQIsRequired"><s:text name="midas.agentes.tipoPromotoria" /></th>
				<td colspan="3">
					<s:select name="promotoria.tipoPromotoria.id" cssClass="cajaTextoM2 w250 jQrequired jQnonZero" disabled="#readOnly"
					       headerKey="0" headerValue="Seleccione.."
					       list="listaTipoPromotoria" listKey="id" listValue="valor"/>
				</td>
			</tr>	
			<tr>
				<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.situacion" /></th>
				<td colspan="3"><s:select name="promotoria.claveEstatus" cssClass="cajaTextoM2 jQrequired" disabled="#readOnly"
	 				       list="#{'1':'ACTIVO','0':'INACTIVO' }"/></td>
			</tr>	
			<tr>
				<th class="jQIsRequired"><s:text name="midas.agentes.agrupador" /></th>
				<td colspan="3">
					<s:select name="promotoria.agrupadorPromotoria.id" cssClass="cajaTextoM2 w250 jQnonZero jQrequired" disabled="#readOnly"
					       headerKey="0" headerValue="Seleccione.."
					       list="listaAgrupadores" listKey="id" listValue="valor"/>
				</td>
			</tr>		
			<tr>
				<th class="jQIsRequired"><s:text name="midas.agentes.idEjecutivo" /></th>
				<td><s:textfield name="promotoria.ejecutivo.id" id="idEjecutivo" cssClass="cajaTextoM2 w50 jQnumeric jQrequired" onchange="javascript:findByIdEjecutivo(this.value);" readonly="true"></s:textfield></td>
				<td>
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w80">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="mostrarModalEjecutivo();">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
					</s:if>	
				</td>
				<td>
					<s:textfield id="descripcionEjecutivo" name="promotoria.ejecutivo.personaResponsable.nombreCompleto" cssClass="cajaTextoM2 w200" readonly="#readOnly"></s:textfield>
				</td>
			</tr>
			
			<s:if test="tipoAccion != @mx.com.afirme.midas2.dto.TipoAccionDTO@getAgregarModificar()">
				<tr>
					<td colspan="6">
						<div  class="inline">
							<div class="btn_back w110">
								<a href="javascript: void(0);" onclick="mostrarVentanaGenerica(mostrarAgentesPath,'Agentes', 450, 400 ,document.promotoriaForm);">
									<s:text name="midas.boton.agente"/>
								</a>
							</div>								
						</div>
					</td>
				</tr>
				<tr>
					<th  width="120px"><s:text name="midas.agentes.idAgentePromotor" /></th>
					<td width="80px">
						<s:textfield  name="promotoria.agentePromotor.idAgente" id="idAgente" cssClass="cajaTextoM2 w50" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
							<s:textfield id="txtId" name ="txtIdAgente" cssStyle="display:none" onchange="onChangeAgente();"/>
							<s:textfield id="id" cssStyle="display:none" name="promotoria.agentePromotor.id"/>
					</td>
					<td>
						<s:if test="tipoAccion != 2">
							<div class="btn_back w80">
								<a href="javascript: void(0);" class="icon_buscar"
									onclick="mostrarListadoAgentes();">
									<s:text name="midas.boton.buscar"/>
								</a>
							</div>		
						</s:if>
					</td>
					<td>
						<s:textfield name="promotoria.agentePromotor.persona.nombreCompleto" id="nombreAgente" cssClass="cajaTextoM2 w200" />
					</td>
				</tr>
			</s:if>
		</table>
		<s:if test="tipoAccion == @mx.com.afirme.midas2.dto.TipoAccionDTO@getAgregarModificar()">
			<table  width="98%" class="contenedorFormas" align="center">
				<tr>
					<td colspan="2" class="titulo" >
						<s:text name="midas.promotoria.sp.titulo"/>
					</td>
				</tr>
				<tr>
					<td width= "30%">
						<s:checkbox id="capturaManualClave" name="promotoria.capturaManualClave"  key="midas.promotoria.sp.check" labelposition="left" onchange="onChangeTipoCapturaClave()"/>
					</td>
					<td width= "70%">
						<s:textfield 
							cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
							key="midas.catalogos.clave"
							labelposition="left" 
							size="20"
							onblur="this.value=jQuery.trim(this.value)"
							id="clave" 
							name="promotoria.idPromotoria" 
							disabled="%{promotoria.idPromotoria == null}"
						/>
					</td>
				</tr>
			</table>
		</s:if>
		<s:else>
			<s:hidden name="promotoria.idPromotoria" id="txtIdPromotoria" value="%{promotoria.idPromotoria}" />
		</s:else>
		<table  width="98%" class="contenedorFormas" align="center">
			<tr>
				<td class="titulo" >
					<s:text name="midas.boton.domicilio"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
			 			<s:param name="idDomicilioName">promotoria.personaResponsable.domicilios[0].idDomicilio.idDomicilio</s:param> 
						<s:param name="idPaisName">promotoria.personaResponsable.domicilios[0].clavePais</s:param>
						<s:param name="idEstadoName">promotoria.personaResponsable.domicilios[0].claveEstado</s:param>	
						<s:param name="idCiudadName">promotoria.personaResponsable.domicilios[0].claveCiudad</s:param>		
						<s:param name="idColoniaName">promotoria.personaResponsable.domicilios[0].nombreColonia</s:param>
						<s:param name="calleNumeroName">promotoria.personaResponsable.domicilios[0].calleNumero</s:param>
						<s:param name="cpName">promotoria.personaResponsable.domicilios[0].codigoPostal</s:param>
						<s:param name="nuevaColoniaName">promotoria.personaResponsable.domicilios[0].nuevaColonia</s:param>
						<s:param name="idColoniaCheckName">promotoria.personaResponsable.domicilios[0].idColoniaCheck</s:param>				
						<s:param name="labelPais">Pais</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle y Número</s:param>
						<s:param name="labelCodigoPostal">Código Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">2</s:param>		
						<s:param name="readOnly" value="true"></s:param>
						<s:param name="requerido" value="1"></s:param>	
						<s:param name="enableSearchButton" value="false"></s:param>		
					</s:action>
				</td>
			</tr>		
		</table>
		
		<table width="98%" class="contenedorFormas" align="center">
		    <s:if test="tipoAccion != 5">
		        <tr>
				<td colspan="6"><span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span></td>
			</tr>
		    </s:if>				
			<tr>
			<td colspan="5">&nbsp;</td>
			<td align="right">
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">
				        <div class="btn_back w110">
						<a href="javascript: salirDePromotoria();" class="icon_regresar"
							onclick="">
							<s:text name="midas.boton.regresar"/>
						</a>
					</div>
				    </s:if>					
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarPromotoria();">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>	
					<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
							onclick="eliminarPromotoria();">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>
					</s:if>
					<div id="btnSiguiente" class="btn_back w80">
						<a href="javascript: void(0);" onclick="">
							<s:text name="midas.boton.siguiente"/>
						</a>
					</div>	
				</div>
			</td>
			</tr>
		</table>
		</s:form>
	</div>
	<!-- Tab 2 -->
	<div width="150px" id="datosContables" name="Datos Contables" href="http://void">
		<jsp:include page="promotoriaDatosContables.jsp" flush="true"></jsp:include>
	</div>
</div>
		<s:if test="tipoAccion != 1">
		<table width="98%" class="contenedorFormas" align="center" id="historico">	
			<tr>
			    <s:if test="tipoAccion != 5">
			        <td>
					    <s:text name="midas.fuerzaventa.negocio.ultimaModificacion"/>
				    </td>
			    </s:if>
			    <s:else>
			        <td>
					    <s:text name="midas.fuerzaventa.negocio.fechaModificacion"/>
				    </td>
			    </s:else>				
				<td>
					<s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" cssClass="cajaTextoM2" id="txtFechaHora" readonly="true"/> <!-- key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" -->
				</td>
				<td>
					<s:text name="midas.fuerzaventa.negocio.usuario"/>
				</td>
				<td >
					<s:textfield name="ultimaModificacion.usuarioActualizacion" cssClass="cajaTextoM2" id="txtUsuario" readonly="true"/> <!-- key="midas.fuerzaventa.negocio.usuario" labelposition="left"  -->
				</td>
				<td>
					&nbsp;
				</td>
				<td align="right">
				    <s:if test="tipoAccion != 5">
				        <div class="btn_back w110">
						<a href="javascript: mostrarHistoricoPromotoria(40,${promotoria.id});" class="icon_guardar">
							<s:text name="midas.boton.historico"/>
						</a>
					</div>
				    </s:if>												
				</td>		
			</tr>		
		</table>	
		</s:if>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>