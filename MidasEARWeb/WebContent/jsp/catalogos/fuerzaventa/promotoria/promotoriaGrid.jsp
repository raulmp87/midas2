<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="90" sort="int" >Id Promotoría</column>
		<column id="descripcion" type="ro" width="160" sort="str">Descripción</column>
		<column id="tipoPromotoria" type="ro" width="150" sort="str">Tipo de Promotoría</column>
		<column id="agrupador" type="ro" width="100" sort="str">Agrupador</column>
		<column id="oficina" type="ro" width="100" sort="str">Ejecutivo</column>
		<column id="fechaAlta" type="ro" width="*" sort="str">Fecha de Alta</column>
		<column id="claveEstatus" type="ro" width="80" sort="str"><s:text name="midas.catalogos.fuerzaventa.grid.estatus.titulo"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaPromotorias" var="rowPromotoria" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowPromotoria.id}]]></cell>
			<cell><![CDATA[${rowPromotoria.descripcion}]]></cell>
			<cell><![CDATA[${rowPromotoria.tipoPromotoria}]]></cell>
			<cell><![CDATA[${rowPromotoria.agrupadorPromotoria}]]></cell>
			<cell><![CDATA[${rowPromotoria.nombreEjecutivo}]]></cell>
			<cell><![CDATA[${rowPromotoria.fechaAltaString}]]></cell>
			<!-- 
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenerica(verDetallePromotoriaPath, 2,"promotoria", ${rowPromotoria.id})^_self</cell>
            <cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenerica(verDetallePromotoriaPath, 4,"promotoria", ${rowPromotoria.id})^_self</cell>
            <cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenerica(verDetallePromotoriaPath, 3,"promotoria", ${rowPromotoria.id})^_self</cell>
			-->
			<s:if test="claveEstatus == 1">
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.activa"></s:text></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.inactiva"></s:text></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetallePromotoriaPath, 2,{"promotoria.id":${rowPromotoria.id},"idRegistro":${rowPromotoria.id},"idTipoOperacion":40})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetallePromotoriaPath, 4,{"promotoria.id":${rowPromotoria.id},"idRegistro":${rowPromotoria.id},"idTipoOperacion":40})^_self</cell>
				<s:if test="claveEstatus == 1">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetallePromotoriaPath, 3,{"promotoria.id":${rowPromotoria.id},"idRegistro":${rowPromotoria.id},"idTipoOperacion":40})^_self</cell>
				</s:if>			
			</s:if>
		</row>
	</s:iterator>
</rows>