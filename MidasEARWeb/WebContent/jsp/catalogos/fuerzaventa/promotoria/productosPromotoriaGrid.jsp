<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingAreaProducto</param>
				<param>true</param>
				<param>infoAreaProducto</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		 <column id="id" type="ro" width="0" sort="int" hidden="true"><s:text name="midas.negocio.producto.id"/></column>
		 <column id="tipoProducto" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.tipoProducto"/></column>
        <column id="nombreId" type="ro" width="*" hidden="true" sort="str"><s:text name="midas.catalogos.agente.productosAgente.banco"/></column>
		<column id="producto" type="ro" width="130" sort="str"><s:text name="midas.catalogos.agente.productosAgente.producto"/></column>
		<column id="bancoNombre" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.banco"/></column>
		<column id="numeroCuenta" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.numeroCuenta"/></column>
		<column id="numeroClabe" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.numeroClabe"/></column>
		<column id="claveDefault" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.claveDefault"/></column>
		<column id="montoCredito" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.montoCredito"/></column>
		<column id="numeroPlazos" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.numeroPlazos"/></column>
		<column id="montoPago" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.montoPago"/></column>
		<column id="sucursal" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.sucursal"/></column>
		<column id="plaza" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.plaza"/></column>
		<s:if test="tipoAccion!=2">
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
		<!-- 
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
		 -->
	</head>
	<s:iterator value="productos" var="p" status="index">
		<row id="${index.count}">			
			<cell><![CDATA[${index.count-1}]]></cell>
			<s:if test="productoBancario.claveCredito==1">
				<cell><![CDATA[Credito]]></cell>
			</s:if>
			<s:else>
				<cell><![CDATA[Transferencia Electronica]]></cell>
			</s:else>
			<cell><![CDATA[${p.productoBancario.id}]]></cell>
			<cell><![CDATA[${p.productoBancario.descripcion}]]></cell>
			<cell><![CDATA[${p.productoBancario.banco.nombreBanco}]]></cell>
			<s:if test="#p.numeroCuenta!=null || p.numeroCuenta!=0"> <%--&& !p.numeroCuenta.isEmpty()--%>
				<cell><![CDATA[${p.numeroCuenta}]]></cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#p.numeroClabe!=null || p.numeroClabe!=0">
				<cell><![CDATA[${p.numeroClabe}]]></cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#p.claveDefault== 1">
				<cell>Si</cell>
			</s:if>
			<s:else>
				<cell>No</cell>
			</s:else>
			<s:if test="#p.montoCredito!=null || p.montoCredito!= 0">
				<cell><![CDATA[${p.montoCredito}]]></cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#p.numeroPlazos!=null || p.numeroPlazos!= 0">
				<cell><![CDATA[${p.numeroPlazos}]]></cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#p.montoPago!=null || p.montoPago!= 0">
				<cell><![CDATA[${p.montoPago}]]></cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<cell><![CDATA[${p.sucursal}]]></cell>
			<cell><![CDATA[${p.plaza}]]></cell>
			<s:if test="tipoAccion!=2">
			<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:delete_row(${index.count})^_self</cell>
			</s:if>
		</row>
	</s:iterator>
</rows>