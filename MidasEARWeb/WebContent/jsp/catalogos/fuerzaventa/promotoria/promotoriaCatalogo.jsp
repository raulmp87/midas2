<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/promotoria/promotoriaHeader.jsp"></s:include>
<script type="text/javascript">
jQuery(function(){
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarFiltradoPromotoriaPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"promotoriaGrid", null,idField,'promotoriaModal');
});
</script>
<s:form action="listarFiltrado" id="promotoriaForm" name="promotoriaForm">
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.agente.promotoria.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th width="70px"><s:text name="midas.negocio.promotoria.id" /></th>	
			<td><s:textfield name="filtroPromotoria.id" cssClass="cajaTextoM2 w150 jQnumeric jQrestrict"></s:textfield></td>
			
			<th  width="50px">Tipo de <s:text name="midas.fuerzaventa.negocio.promotoria" /></th>
			<td><s:select id="txtPromotoria" list="listaTipoPromotoria" listValue="valor" listKey="id" name="filtroPromotoria.tipoPromotoria.id" headerKey="" headerValue="Seleccione..." cssClass="cajaTextoM2 w150" ></s:select></td>
			
			<th><s:text name="midas.fuerzaventa.fechaInicio"/></th>	
			<td>
				<sj:datepicker name="filtroPromotoria.fechaInicio" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
		<tr>
			<th width="70px"><s:text name="midas.catalogos.descripcion" /></th>	
			<td><s:textfield name="filtroPromotoria.descripcion" id="txtDescripcion" cssClass="cajaTextoM2 w150"></s:textfield></td>
			
			<th><s:text name="midas.catalogos.centro.operacion.situacion" /></th>
			<td><s:select list="#{'1':'ACTIVO', '0':'INACTIVO'}" name="filtroPromotoria.claveEstatus" headerKey="" headerValue="Seleccione..."  cssClass="cajaTextoM2 w150" ></s:select></td>
			
			<th><s:text name="midas.fuerzaventa.fechaFin"/></th>
			<td><sj:datepicker name="filtroPromotoria.fechaFin" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
 		<tr>
 			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo" /></th>
			<td><s:select list="listaEjecutivos" listValue="nombreCompleto" listKey="id" name="filtroPromotoria.ejecutivo.id" headerKey="" headerValue="Seleccione..."  cssClass="cajaTextoM2 w150" ></s:select></td> 
			
			<th  width="50px"><s:text name="midas.agentes.agrupador" /></th>
			<td><s:select id="txtPromotoria" list="listaAgrupadores" listValue="valor" listKey="id" name="filtroPromotoria.agrupadorPromotoria.id" headerKey="" headerValue="Seleccione..." cssClass="cajaTextoM2 w150" ></s:select></td>
			
			<td colspan="2">&nbsp;</td>	 
 		</tr> 
			
<!-- 		<tr> -->
<!-- 			<td colspan="6"> -->
<%-- 				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" > --%>

<%-- 					<s:param name="idEstadoName">filtroPromotoria.domicilio.claveEstado</s:param>	 --%>
<%-- 					<s:param name="idCiudadName">filtroPromotoria.domicilio.claveCiudad</s:param>		 --%>
<%-- 					<s:param name="idColoniaName">filtroPromotoria.domicilio.nombreColonia</s:param> --%>
<%-- 					<s:param name="calleNumeroName">filtroPromotoria.domicilio.calleNumero</s:param> --%>
<%-- 					<s:param name="cpName">filtroPromotoria.domicilio.codigoPostal</s:param>	 --%>
<%-- 					<s:param name="labelEstado">Estado</s:param> --%>
<%-- 					<s:param name="labelCiudad">Municipio</s:param> --%>
<%-- 					<s:param name="labelPosicion">left</s:param> --%>
<%-- 					<s:param name="componente">3</s:param>						 --%>
<%-- 				</s:action> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td colspan="6"  align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoPromotoriaPath, 'promotoriaGrid', document.promotoriaForm,'<s:property value="idField"/>', 'promotoriaModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>			
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div id="promotoriaGrid" width="880px" height="220px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica(verDetallePromotoriaPath,1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>

