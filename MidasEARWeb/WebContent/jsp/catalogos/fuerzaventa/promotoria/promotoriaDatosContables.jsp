<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/promotoria/promotoriaHeader.jsp"></s:include>
<script type="text/javascript">
	cargarGridDatosBancarios();
</script>
<s:form action="listarFiltrado" id="productosPromotoriaForm">
	<s:hidden name="tipoAccion"/>
	<s:if test="tipoAccion == 1">
		<s:set id="readOnly" value="false" />
		<s:set id="idReadOnly" value="true" />
		<s:set id="display" value="true" />
		<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloAlta')}"/>
	</s:if>
	<s:if test="tipoAccion == 2">
		<s:set id="readOnly" value="true" />	
		<s:set id="idReadOnly" value="true" />
		<s:set id="display" value="false" />
		<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloConsultar')}"/>
	</s:if>
	<s:if test="tipoAccion == 3">
		<s:set id="readOnly" value="true" />
		<s:set id="idReadOnly" value="true" />
		<s:set id="display" value="false" />
		<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloEliminar')}"/>
	</s:if>
	<s:if test="tipoAccion == 4">
		<s:set id="readOnly" value="false" />
		<s:set id="idReadOnly" value="true" />
		<s:set id="display" value="true" />
		<s:set id="titulo" value="%{getText('midas.fuerzaventa.promotoria.tituloEditar')}"/>
	</s:if>
	<!--<s:hidden name="promotoria.id" id="promotoria.id"/>-->
	<table  width="98%" class="contenedorFormas" align="center">		
		<tr>
			<td class="titulo" colspan="10">			
				<s:text name="midas.fuerzaventa.negocio.productosBancarios"/>
			</td>
		</tr>
		<tr>
			<td width="200" class="jQIsRequired">
				<s:text name="midas.catalogos.agente.productosAgente.banco"/>
			</td>
			<td>
				<s:select id="idBanco" list="listaBancos" listKey="idBanco" listValue="nombreBanco" headerKey="" 
 				headerValue="Seleccione..."  cssClass="cajaTextoM2 w150 jQrequired" onchange="onChangeBanco();" disabled="#readOnly"></s:select>
			</td>
			<td>
				<s:text name="midas.catalogos.agente.productosAgente.producto"/>
			</td>
			<td>
				<s:select id="idProductoBancario" name="producto.productoBancario.id" list="productosBancarios" listKey="id" 
				listValue="descripcion" headerKey="" headerValue="Seleccione..." cssClass="cajaTextoM2 w150" 
 				onchange="onChangeProductoBancario();" disabled="#readOnly"></s:select> 
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.catalogos.agente.productosAgente.tipoProducto"/>
			</td>
			<td>
				<s:textfield name="tipoProducto" id="tipoProducto" cssClass="cajaTextoM2" readonly="true"/> 
			</td>
			<td>
				<s:text name = "midas.catalogos.agente.productosAgente.claveDefault"/>
			</td>
			<td>
				<s:checkbox id="claveDefault" name="producto.claveDefaultBoolean" disabled="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td id="numeroCuentaLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroCuenta"/>
			</td>
			<td>
				<s:textfield name="producto.numeroCuenta" id="numeroCuenta" cssClass = "cajaTextoM2 jQnumeric jQrestrict jQrequired" maxLength="20" readonly="#readOnly"/>
			</td>
			<td id="numeroClabeLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroClabe"/>
			</td>
			<td>
				<s:textfield name="producto.numeroClabe" id="numeroClabe" cssClass = "cajaTextoM2 jQnumeric jQrestrict" maxLength="20" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td id="montoCreditoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoCredito"/>
			</td>
			<td>
				<s:textfield name="producto.montoCredito" id="montoCredito" cssClass = "cajaTextoM2 jQfloat jQrestrict" maxLength="10" readonly="#readOnly"/>
			</td>
			<td id="numeroPlazosLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroPlazos"/>
			</td>
			<td>
				<s:textfield name="producto.numeroPlazos" id="numeroPlazos" cssClass = "cajaTextoM2 jQnumeric jQrestrict" maxLength="4" readonly="#readOnly"/>
			</td>
			<td id="montoPagoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoPago"/>
			</td>
			<td>
				<s:textfield name="producto.montoPago" id="montoPago" cssClass = "cajaTextoM2 jQfloat jQrestrict" maxLength="10" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name = "midas.catalogos.agente.productosAgente.sucursal"/>
			</td>
			<td>
				<s:textfield name="producto.sucursal" id="sucursal" cssClass = "cajaTextoM2 jQrequired" maxLength="60" readonly="#readOnly"/>
			</td>
			<td class="jQIsRequired">
				<s:text name = "midas.catalogos.agente.productosAgente.plaza"/>
			</td>
			<td>
				<s:textfield name="producto.plaza" id="plaza" cssClass = "cajaTextoM2 jQrequired" maxLength="60" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.productosSeleccionados"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="productoBancario" class="w880 h150">				
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="pagingAreaProducto"></div><div id="infoAreaProducto"></div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div align="right" class="inline" >
					<div class="btn_back w110">
						<a href="javascript: salirDePromotoria();" class="icon_regresar"
							onclick="">
							<s:text name="midas.boton.regresar"/>
						</a>
					</div>		
			
				
					<div class="btn_back w80" id="btnAtras" align="right" >
						<a href="javascript: void(0);">
							<s:text name="midas.boton.atras"/>
						</a>
					</div>
								
					<div align="right">
						<s:if test="tipoAccion == 1 || tipoAccion == 4">
							<div class="btn_back w130">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="agregarProductoInFront();">
									<s:text name="midas.boton.agregarProducto"/>
								</a>
							</div>	
						</s:if>
					</div>
					<div align="right">
						<s:if test="tipoAccion == 1 || tipoAccion == 4">
							<div class="btn_back w130">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="guardarProdBancario();">
									<s:text name="Guardar"/>
								</a>
							</div>	
						</s:if>
					</div>									
				</div>
			</td>	
		</tr>
	</table>	
	<!-- 
	<div align="right" >
		<div class="btn_back w130">
			<a href="javascript: guardarDatosContables();" class="icon_guardar"
				onclick="">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
			</a>
		</div>
	</div>
	-->
</s:form>

