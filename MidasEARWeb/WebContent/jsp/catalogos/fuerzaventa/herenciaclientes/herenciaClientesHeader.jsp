<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/herenciaclientes.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/docFortimax.js'/>"></script>
 <script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
	var mostrarContenedorHerenciaClientesPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/herenciaclientes"/>';	
	var verDetalleHerenciaClientesPath = '<s:url action="verDetalle" namespace="/fuerzaventa/herenciaclientes"/>';		
	var listarHerenciaClientesPath = '<s:url action="listar" namespace="/fuerzaventa/herenciaclientes"/>';	
	var listarFiltradoHerenciaClientesPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/herenciaclientes"/>';
	
	var mostrarAgentesPath = '<s:url action="mostrarAgentes" namespace="/fuerzaventa/agente"/>';
	
	var  guardarHerenciaPath = '<s:url action="guardarHerencia" namespace="/fuerzaventa/herenciaclientes"/>';
	
</script>
<script src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>