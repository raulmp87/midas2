<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea2</param>
				<param>true</param>
				<param>infoArea2</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="attachHeader">
			 		<param>,#text_filter,#text_filter</param>
			 </call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="idcheckbox" type="ch" width="30" align="center">#master_checkbox</column>
		<column id="id" type="ro" width="*" sort="int"><s:text name="midas.negocio.producto.id"/></column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/></column>
	</head>
	<s:iterator value="listaClientesCedidos" var="row" status="index">
	<row id="${row.id}">
		<cell>0</cell>
		<cell><![CDATA[${row.idCliente}]]></cell>		
		<cell><![CDATA[${row.nombreCompleto}]]></cell>			
	</row>
	</s:iterator>
</rows>