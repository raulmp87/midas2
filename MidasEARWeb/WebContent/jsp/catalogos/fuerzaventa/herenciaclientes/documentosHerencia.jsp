<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="herenciaDocumentosForm">
<s:hidden name="tipoAccion"/>	
<s:hidden name="agenteDestino.id"/>
<s:hidden name="agenteDestino.persona.claveTipoPersona"/>
<s:hidden name="tabActiva" value="documentos"/>
<s:hidden name="moduloOrigen"/>
	<table width="97%" class="contenedorFormas">		
		<s:iterator value="listaDocumentosFortimax" status="stat" var="doc" id="doc">
		<tr>
			<td width="5%">
				<s:if test="#doc.existeDocumento==1">
				 	<input type="checkbox" checked="checked" disabled="disabled"/>
				</s:if>					
				<s:else>
					<input type="checkbox" disabled="disabled"/>
				</s:else>
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/>
				<s:hidden name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumento"/>				
			</td>			
			<td width="95%">
				<s:text name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumentoFortimax"/>
			</td>				
		</tr>
		</s:iterator>
		<tr>
		<td colspan="4">
		<div align="right" class="w890 inline" >			
			<div class="btn_back w180">
				<a href="javascript: generarLigaIfimaxHerencia();" class="icon_imprimir"
					onclick="">
					<s:text name="midas.boton.digitalizarDoc"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: auditarDocumentosHerencia();" class="icon_confirmAll"
					onclick="">
					<s:text name="Auditar"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: guardarDocumentosFortimaxHerencia();" class="icon_guardar2"
					onclick="">
					<s:text name="Activar"/>
				</a>
			</div>			
		</div>
		</td>
		</tr>
		</table>
</s:form>

