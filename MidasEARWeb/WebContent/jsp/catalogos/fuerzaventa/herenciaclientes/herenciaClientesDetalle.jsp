<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.herenciaCltes.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.herenciaCltes.tituloConsultar')}"/>
</s:if>

<s:include value="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:textfield name="herenciaClientes.id" id="idHerencia" cssStyle="display:none"/>  <!--  onchange="getHistorico()"--> 
<s:hidden name="rangoFechas" id="txtFechaActual"/>
<s:hidden name="herenciaClientes.status.valor"/>
<s:form action="agregar" id="herenciaClientesForm" name="herenciaClientesForm">
<s:textfield name="agenteDestino.id" id="txtIdHiddenDestino" onchange="onChangeAgente();" cssStyle="display:none;"/>
<s:textfield name="agenteOrigen.id" id="txtIdHiddenOrigen" onchange="onChangeAgente();mostrarOcultarDocumentos();" cssStyle="display:none;"/>
<s:hidden name="agenteDestino.idAgente" id="txtIdAgenteHiddenDestino"/>
<s:hidden name="agenteOrigen.idAgente" id="txtIdAgenteHiddenOrigen"/>	
<s:hidden name="agenteOrigen.tipoCedula.id" id="txtIdCedulaOrigen" cssClass="cajaTextoM2 w200" />
<s:hidden name="tipoDocumento" value="Herencia de Clientes"/>
<s:hidden name="agenteDestino.tipoCedula.id" id="txtIdCedulaDestino" cssClass="cajaTextoM2 w200" readonly="true"/>
<div class="titulo w400"><s:text name="#titulo"/></div>
	<table width="98%" class="contenedorFormas" align="center">		
		<tr>
			<th><s:text name="midas.boton.agente"></s:text></th>
			<th>
				Origen<input  type="Radio" name="consultarAgente" value="1" class="js_isCheck"  checked readOnly="#readOnly">				 
				Fin<input  type="Radio" name="consultarAgente" value="2" class="js_isCheck" readOnly="#readOnly">
			</th>		
			<th><s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text></th>	 
			<td><s:textfield name="clienteAgente.agente.idAgente" id="txtIdAgente" cssClass="jQnumeric jQrestrict cajaTextoM2 w200" disabled="#readOnly"></s:textfield></td>								
			<td>	
				<s:if test="tipoAccion == 1">			
				<div class="btn_back w110">
					<a href="javascript: void(0);"
						onclick="mostrarModalAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>			
				</s:if>
			</td>				 
		</tr>		
		<tr>
			<th><s:text name="midas.fuerzaventa.herencia.motivoCesion"></s:text></th>
			<td colspan="2"><s:select id="listMotivoCesion"  name="herenciaClientes.motivoCesion.id" headerKey="" headerValue="Seleccione..." listValue="valor" listKey="id" cssClass="cajaTextoM2 w250 jQrequired" list="listaMotivosCesion" disabled="#readOnly"/></td>			
		</tr>		
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td width="45%" valign="top">
				<table width="98%" class="contenedorFormas" align="center">
					<tr>
						<td colspan="2"><s:text name="Datos Generales del Agente Origen" /></td>						
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.herencia.claveAgente" /></td>
						<td><s:textfield name="agenteOrigen.idAgente" id="txtIdAgenteOrigen" cssClass="cajaTextoM2 w40" readonly="true" ></s:textfield></td>									
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.nombreAgente" /></td>
						<td><s:textfield name="agenteOrigen.persona.nombreCompleto" id="txtNombreCompletoOrigen" cssClass="cajaTextoM2 w240" readonly="true" ></s:textfield></td>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.tipoCedula" /></td>
						<td><s:textfield name="agenteOrigen.tipoCedula.valor" id="txtTipoCedulaOrigen" cssClass="cajaTextoM2 w240" readonly="true" ></s:textfield></td>
					</tr>
					<tr>
						<td><s:text name="midas.agentes.afianzadora.personalidadJuridica" /></td>
						<td><s:select name="agenteOrigen.persona.claveTipoPersona" list="#{'1':'FISICA', '2':'MORAL'}" id="txtPersonalidadOrigen" headerKey="" headerValue="Seleccione..." disabled="true" cssClass="cajaTextoM2 w250" /></td>
<%-- 						<td><s:textfield name="agenteOrigen.persona.claveTipoPersona" id="txtPersonalidadOrigen" cssClass="cajaTextoM2 w240" readonly="true" ></s:textfield></td> --%>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.prioridad" /></td>
						<td colspan="2"><s:select name="agenteOrigen.idPrioridadAgente" list="listPrioridadesAgente" id="txtIdPrioridadAgenteOrigen" headerKey="" headerValue="Seleccione..." disabled="true" listValue="valor" listKey="id" cssClass="cajaTextoM2 w250" /></td>
<%-- 						<td><s:textfield id="txtIdPrioridadAgenteOrigen" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td> --%>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.curp" /></td>
						<td><s:textfield name="agenteOrigen.persona.curp" id="txtCurpOrigen" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td>
					</tr>
<!-- 					<tr> -->
<%-- 						<td><s:text name="midas.fuerzaventa.herencia.totalClientes" /></td> --%>
<%-- 						<td><s:textfield id="txtTotalClientes"  cssClass="cajaTextoM2 w40" readonly="true"></s:textfield></td> --%>
<!-- 					</tr> -->
					<s:if test="tipoAccion == 1">	
					<tr>
						<td><s:text name="midas.fuerzaventa.herencia.agentesACeder" /></td>
						<td><s:textfield id="txtClientesACeder" cssClass="cajaTextoM2 w60" readonly="true"></s:textfield></td>
					</tr>
					</s:if>
					<tr>
						<td colspan="2">
							<div id="divCarga" style="position:absolute;"></div>
							<div align="center" id="clientesAcederGrid" width="380px" height="200px" style="background-color:white;overflow:hidden"></div>
							<div id="pagingArea"></div><div id="infoArea"></div>
						</td>
<%-- 						<td colspan="2"><s:select multiple="true" id="listaClientes" name="listaClientes"  listValue="nombreCompleto" listKey="id" cssClass="cajaTextoM w380 h200" list="listaClientes" /></td> --%>
					</tr>
				</table>	
			</td>
			<td width="5%" >
				<s:if test="tipoAccion == 1">	
				<div class="h180">&nbsp;</div>
				<div class="btn_back w30">
					<a href="javascript: void(0);" onclick="agregaOptionCliente();">
					<s:text name=">>"/></a>
				</div>			
				<br>
				<div class="btn_back w30">
					<a href="javascript: void(0);" onclick="regresaOptionCliente();">
					<s:text name=""/></a>
				</div>
				<br>				
				<br>
				</s:if>
			</td>
			<td width="45%" valign="top">
				<table width="98%" class="contenedorFormas" align="center">
					<tr>
						<td colspan="2"><s:text name="Datos Generales del Agente Fin" /></td>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.herencia.claveAgente" /></td>
						<td><s:textfield name="agenteDestino.idAgente" id="txtIdAgenteDestino" cssClass="cajaTextoM2 w40" readonly="true"></s:textfield></td>
						
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.nombreAgente" /></td>
						<td><s:textfield name="agenteDestino.persona.nombreCompleto" id="txtNombreCompletoDestino" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.tipoCedula" /></td>
						<td><s:textfield name="agenteDestino.tipoCedula.valor" id="txtTipoCedulaDestino"  cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td>
					</tr>
					<tr>
						<td><s:text name="midas.agentes.afianzadora.personalidadJuridica" /></td>
						<td><s:select name="agenteDestino.persona.claveTipoPersona" list="#{'1':'FISICA', '2':'MORAL'}" id="txtPersonalidadDestino" headerKey="" headerValue="Seleccione..." disabled="true" cssClass="cajaTextoM2 w250" /></td>
<%-- 						<td><s:textfield id="txtPersonalidadDestino" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td> --%>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.prioridad" /></td>						
						<td colspan="2"><s:select name="agenteDestino.idPrioridadAgente" list="listPrioridadesAgente"  id="txtIdPrioridadAgenteDestino" headerKey="" headerValue="Seleccione..." disabled="true" listValue="valor" listKey="id" cssClass="cajaTextoM2 w250"/></td>
<%-- 					<td><s:textfield id="txtIdPrioridadAgenteDestino" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td> --%>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.curp" /></td>
						<td><s:textfield name="agenteDestino.persona.curp" id="txtCurpDestino" cssClass="cajaTextoM2 w240" readonly="true"></s:textfield></td>
					</tr>
					<s:if test="tipoAccion == 1">	
					<tr>
						<td><s:text name="midas.fuerzaventa.herencia.agentesCedidos" /></td>
						<td><s:textfield cssClass="cajaTextoM2 w60" readonly="true" id="clientesCedidos"></s:textfield></td>
					</tr>
					</s:if>
					<tr>
						<td colspan="2">
							<div id="divCarga" style="position:absolute;"></div>
							<div align="center" id="clientesCedidosGrid" width="380px" height="200px" style="background-color:white;overflow:hidden"></div>
							<div id="pagingArea2"></div><div id="infoArea2"></div>
						</td>
<%-- 						<td colspan="2"><s:select multiple="true" id="listaClientesCedidos"  name="listaClientesCedidos" listValue="nombreCompleto" listKey="id" cssClass="cajaTextoM w380 h200" list="listaClientesCedidos" /></td> --%>
					</tr>
				</table>	
			</td>
		</tr>		
	</table>
<!-- 	<div id="documentosHerencia"></div> ***************************-->
	<div id='cargarCheck'></div>
<table id="botonesFortimax" style="display: none;">
	<tr>
		<td colspan="4">
		<div align="right" class="w890 inline" >			
			<div class="btn_back w180">
				<a href="javascript: generarLigaIfimaxHerencia();" class="icon_imprimir"
					onclick="">
					<s:text name="midas.boton.digitalizarDoc"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: auditarDocumentosHerencia();" class="icon_confirmAll"
					onclick="">
					<s:text name="Auditar"/>
				</a>
			</div>			
			<div class="btn_back w180" style="display:none" id="btnActivar">
				<a href="javascript: guardarDocumentosFortimaxHerencia();" class="icon_guardar2"
					onclick="">
					<s:text name="Activar"/>
				</a>
			</div>			
		</div>
		</td>
	</tr>
</table>
<br>
	<s:if test="herenciaClientes.id != null">
		<script type="text/javascript">
  			getHistorico(null); 
		</script>
	</s:if>
		<table width="98%" class="contenedorFormas" align="center" id="mostrar_historico" style="display: none;">	
			<tr>				
				<td>
					<s:textfield name="ultimaModificacion.fechaHoraActualizacionString" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"/>
				</td>
				<td>
					<s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"/>
				</td>
				<td colspan="2" align="right">							
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar" onclick="javascript: mostrarHistorico();">	
						<s:text name="midas.boton.historico"/>	
						</a><!-- mostrarHistorico(110,${herenciaClientes.id}) -->
					</div>				
				</td>		
			</tr>		
		</table>
	<div align="right" class="w900 inline" >
		<div class="btn_back w110">
			<a href="javascript: mostrarCatalogoGenerico(mostrarContenedorHerenciaClientesPath);" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
		<s:if test="tipoAccion!=2">
		<div id="btnGuardar">
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="guardarHerencia();">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>
		</div>
		</s:if>
	</div>	
	<script type="text/javascript">
	crearGrid();
	</script>
</s:form>

