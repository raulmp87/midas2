<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarHerenciaClientesPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"herenciaClientesGrid", null,idField);

 });
</script>
<s:form action="listarFiltrado" id="herenciaClientesForm" name="herenciaClientesForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="920px" id="filtrosM2" cellpadding="0" cellspacing="0" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.fuerzaventa.herenciaCltes.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.ejecutivo.gerencia" />
			</th>
			<td>
				<s:select id="idGerencia" name="filtroHerenciasClienteAgente.agenteOrigen.promotoria.ejecutivo.gerencia.id" value="idGerencia" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivo','idPromotoria',this.value);" cssClass="cajaTextoM2 w200"/>
			</td>		
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text></th>
			<td>
				<s:select id="idEjecutivo" name="filtroHerenciasClienteAgente.agenteOrigen.promotoria.ejecutivo.id" value="idEjecutivo" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoria',this.value);" cssClass="cajaTextoM2 w200 "/>
			</td>
			<th><s:text name="midas.fuerzaventa.negocio.promotoria"></s:text></th>
			<td>
				<s:select id="idPromotoria" name="filtroHerenciasClienteAgente.agenteOrigen.promotoria.id" value="filtroAgenteAutorizacion.promotoria.id" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 w200 "/>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.nombre"></s:text></th>	 
			<td><s:textfield name="filtroHerenciasClienteAgente.agenteOrigen.persona.nombreCompleto" id="txtNombreCompleto" cssClass="cajaTextoM2 w200" ></s:textfield></td>
			<th><s:text name="midas.fuerzaventa.herencia.motivoCesion"></s:text></th>      
			<td>
				<s:select id="listaMotivosCesion" name="filtroHerenciasClienteAgente.motivoCesion.id" listKey="id" 
				listValue="valor" list="listaMotivosCesion" cssClass="cajaTextoM2 w200 "
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" />
			</td>
			<th><s:text name="midas.boton.agente"></s:text></th>
			<th>
				Origen<input   type="Radio" readonly="#readOnly" name="isAgente" value="ORIGEN" checked>				 
				Fin<input     type="Radio" readonly="#readOnly" name="isAgente" value="DESTINO" >
			</th>			
		</tr>
		<tr>
<%-- 			<th><s:text name="midas.fuerzaventa.fechaAlta"></s:text></th>	  --%>
<!-- 			<td>	  -->
<%-- 				<sj:datepicker name="filtroHerenciasClienteAgente.fechaAlta" disabled="#readOnly" --%>
<!-- 							   buttonImage="../img/b_calendario.gif" -->
<!-- 							   id="txtFechaAlta" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								   -->
<!-- 							   onkeypress="return soloFecha(this, event, false);" -->
<!-- 							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" -->
<%-- 							   onblur="esFechaValida(this);"></sj:datepicker>	 --%>
<!-- 			</td> -->
			<th><s:text name="midas.fuerzaventa.negocio.tipoCedula"></s:text></th>      
			<td>
				<s:select id="listaTiposCedula" name="filtroHerenciasClienteAgente.agenteOrigen.tipoCedula.id" listKey="id"
				listValue="valor"  cssClass="cajaTextoM2 w200 "
				list="listaTiposCedula" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"/>
			</td>
			<th><s:text name="midas.fuerzaventa.curp"></s:text></th>
			<td><s:textfield name="filtroHerenciasClienteAgente.agenteOrigen.persona.curp"  cssClass="cajaTextoM2 w200" ></s:textfield></td>		
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.fuerzaventa.fechaInicio"/></th>	
			<td><sj:datepicker name="filtroHerenciasClienteAgente.fechaInicio" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 			<th><s:text name="midas.fuerzaventa.fechaFin"/></th>
			<td><sj:datepicker name="filtroHerenciasClienteAgente.fechaFin" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
		<tr>			
			<td colspan="6"  align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="listarFiltradoGenerico(listarFiltradoHerenciaClientesPath,'herenciaClientesGrid',document.herenciaClientesForm);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>			
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="herenciaClientesGrid" width="918px" height="220px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<div align="right" class="w920">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica(verDetalleHerenciaClientesPath,1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>

