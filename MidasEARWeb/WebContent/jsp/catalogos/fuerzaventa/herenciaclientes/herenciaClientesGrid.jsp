<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="40" sort="int"><s:text name="midas.negocio.producto.id"/></column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/></column>
		<column id="responsable" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.altaPorInternet.tipoCedula"/></column>
		<column id="situacion" type="ro" width="*" sort="str"><s:text name="midas.boton.promotoria"/></column>
		<column id="situacion" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.herencia.motivoCesion"/></column>
		<column id="situacion" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.herencia.fechaSecion"/></column>
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>

	</head>
	<s:iterator value="listaHerencias" var="rowHerencias" status="index">
	<row id="${idHerencia}">
				<cell><![CDATA[${idHerencia}]]></cell>		
				<cell><![CDATA[${nombreCompletoAgente}]]></cell>			
				<cell><![CDATA[${descripcionTipoCedula}]]></cell>
				<cell><![CDATA[${nombrePromotoria}]]></cell>			
				<cell><![CDATA[${descripcionMotivoCesion}]]></cell>
				<cell><![CDATA[${fechaString}]]></cell>	
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verDetalleHerenciaCliente(${idHerencia},${idHerencia},110)^_self</cell>
		</row>
	</s:iterator>
</rows>
