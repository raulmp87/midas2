<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/suspensiones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/docFortimax.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.suspensiones.tituloAlta')}"/>
	<script type="text/javascript">
		estatusMovDefault();
		jQuery("#fechaSolicitud").val(fechaActual());
	</script>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.suspensiones.tituloConsultar')}"/>
	<script type="text/javascript">
	
		generaEstructuraFortimax();		
		ocultaBtnAutorizarYRechazar();
// 		muestraDocumentosADigitalizarSusp();
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.suspensiones.tituloEliminar')}"/>
	<script type="text/javascript">
// 		jQRemoveClass('jQrequired');
		muestraDocumentosADigitalizarSusp();
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.suspensiones.tituloEditar')}"/>
	<script type="text/javascript">
		muestraDocumentosADigitalizarSusp();
	</script>
</s:if>
<div class="titulo w400"><s:text name="#titulo"/></div>
<s:form action="agregar" id="suspensionesForm" name="suspensionesForm">
<s:hidden name="tipoAccion"/>
<s:hidden name="suspension.id"  id="suspension_id"/>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" />
				
			</th>	
			
			<td>
				<s:textfield  name="suspension.agente.idAgente" id="txtIdAgente" onchange="buscaAgentePorIdAgente();"  cssClass="cajaTextoM2 w100 jQnumeric jQrestrict jQrequired"  readonly="#readOnly"></s:textfield>
				<s:textfield id="id" name="suspension.agente.id" readonly="true" onchange="buscaAgentePorId();" style="display:none;"></s:textfield>
				<s:textfield id="tipoPersona" name="suspension.agente.persona.claveTipoPersona"  readonly="true"  style="display:none;"></s:textfield>					
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="buscaAgentePorIdAgente();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
				</s:if>		
			</td>
		</tr>
		<tr>			
			
			<td><s:text name="midas.prestamosAnticipos.estatus" /></td>
			<td><s:textfield id="estatusAgente" name="suspension.agente.tipoSituacion.valor" cssClass="cajaTextoM2 w160" readonly="true"></s:textfield></td>
			<td><s:text name="midas.prestamosAnticipos.centroOperacion" /></td>
			<td><s:textfield id="descripcionCentroOperacion" name="suspension.agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>			
			<td><s:text name="midas.prestamosAnticipos.gerencia" /></td>
			<td><s:textfield id="descripcionGerencia" name="suspension.agente.promotoria.ejecutivo.gerencia.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.promotoria" /></td>
			<td><s:textfield id="descripcionPromotoria" name="suspension.agente.promotoria.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
			<td><s:text name="midas.prestamosAnticipos.nombreAgente" /></td>
			<td width="180px"><s:textfield id="nombreAgente" name="suspension.agente.persona.nombreCompleto"  cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.rfc" /></td>
			<td width="150px"><s:textfield id="rfcAgente" name="suspension.agente.persona.rfc" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.ejecutivo" /></td>
			<td width="150px"><s:textfield id="ejecutivo" name="suspension.agente.promotoria.ejecutivo.personaResponsable.nombreCompleto" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.clasificacionAgente" /></td>
			<td><s:textfield id="tipoAgente" name="suspension.agente.clasificacionAgentes.valor"  cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.numeroFianza" /></td>
			<td><s:textfield id="numeroFianza" name="suspension.agente.numeroFianza" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.venceFianza" /></td>
			<td><s:textfield id="venceFianza" name="suspension.agente.fechaVencimientoFianza" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
			<td><s:text name="midas.prestamosAnticipos.domicilio" /></td>
			<td><s:text name=""  /></td>
			<td><s:text name="midas.prestamosAnticipos.numeroCedula" /></td>
			<td><s:textfield id="numeroCedula" name="suspension.agente.numeroCedula" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.venceCedula" /></td>
			<td><s:textfield id="venceCedula" name="suspension.agente.fechaVencimientoCedula" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td><s:text name="midas.suspensiones.fechaSolicitud" /></td>
			<td><s:textfield id="fechaSolicitud" name="suspension.fechaSolicitud" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.suspensiones.estatusMovimiento" /></td>
			<td><s:select  name="suspension.estatusMovimiento.id" id="estatusMovimiento" cssClass="cajaTextoM2 w130"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaEstatusMovimiento" listKey="id" listValue="valor" disabled="true"/></td>			
		</tr>
		<tr>
			<td><s:text name="midas.suspensiones.estatus" /></td>
			<td><s:select  name="suspension.estatusAgtSolicitado.id" id="estatusAgenteSituacion" cssClass="cajaTextoM2 w130 jQrequired"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaEstatusAgente" listKey="id" listValue="valor" disabled="#readOnly" onchange="muestraSoloOpcionPermitida()"/></td>	
			<td><s:text name="midas.suspensiones.solicitante" /></td>
			<td><s:textfield id="solicitante" name="suspension.nombreSolicitante" cssClass="cajaTextoM2 w160" readonly="#readOnly"/></td>
				<s:hidden name="nombreUsuarioActual" id="nombreUsuarioActual" readonly="true"></s:hidden>
		</tr>
		<tr>
			<td><s:text name="midas.suspensiones.motivoEstatus" /></td>
			<td><s:select  name="suspension.motivoEstatus.id" id="motivoEstatus" cssClass="cajaTextoM2 w130 jQrequired"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaMotivoEstatus" listKey="id" listValue="valor" disabled="#readOnly"/></td>	
			<td><s:text name="midas.suspensiones.estatus" /> Anterior</td>
<%-- 			<s:if test="tipoAccion != 1" > --%>
				<td><s:select  name="suspension.agente.tipoSituacion.id" id="estatusAgenteAnterior" cssClass="cajaTextoM2 w130 jQrequired"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaEstatusAgente" listKey="id" listValue="valor" disabled="true"/></td>
<%-- 			</s:if>		 --%>
		</tr>		
		<tr>
			<td><s:text name="midas.suspensiones.observaciones" /></td>
			<td><s:textarea  id="observaciones" name="suspension.observaciones" cssClass="cajaTextoM2 w160"  readonly="#readOnly"></s:textarea></td>
			<td><s:text name="midas.suspensiones.motivoEstatus" /> Anterior</td>
<%-- 			<s:if test="tipoAccion != 1" > --%>
				<td><s:select  name="suspension.agente.idMotivoEstatusAgente" id="motivoEstatusAnterior" cssClass="cajaTextoM2 w130 jQrequired"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaMotivoEstatus" listKey="id" listValue="valor" disabled="true"/></td>
<%-- 			</s:if>	 --%>
		</tr>
		<tr>
			<td colspan="4" height="100px"><div style="overflow:auto;height:100px;" id='muestraDocumentosADigitalizar'></div></td>
		</tr>
<!-- 		<tr>************************************************* -->
<!-- 				<td> -->
<%-- 					<s:if test="suspension.id!=null"> --%>
<!-- 							<table class="contenedorFormas no-border"> -->
<!-- 								<tr> -->
<%-- 									<td colspan="2"><div class="titulo"><s:text name="midas.prestamosAnticipos.anexos"/></div></td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td colspan="2"> -->
<!-- 										<div id='cargarCheck'></div> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<%-- 									<s:iterator value="listaDocumentosFortimax" status="stat" var="doc" id="doc"> --%>
<!-- 										<tr> -->
<!-- 											<td width="5%"> -->
<%-- 												<s:if test="#doc.existeDocumento==1"> --%>
<!-- 												 	<input type="checkbox" checked="checked" disabled="disabled"/> -->
<%-- 												</s:if>					 --%>
<%-- 												<s:else> --%>
<!-- 													<input type="checkbox" disabled="disabled"/> -->
<%-- 												</s:else> --%>
<%-- 												<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/> --%>
<%-- 												<s:hidden name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumento"/>				 --%>
<!-- 											</td>			 -->
<!-- 											<td width="95%"> -->
<%-- 												<s:text name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumentoFortimax"/> --%>
<!-- 											</td>				 -->
<!-- 										</tr> -->
<%-- 									</s:iterator> --%>
<!-- 							<tr> -->
<!-- 								<td colspan="2"> -->
<%-- 									<s:if test="tipoAccion != 2"> --%>
<!-- 										<div class="btn_back w150"> -->
<!-- 										<a href="javascript: void(0);" class="" -->
<!-- 											onclick="generarLigaIfimax();"> -->
<%-- 											<s:text name="midas.prestamosAnticipos.btnDigitalizarDocumentos"/> --%>
<!-- 										</a> -->
<!-- 									</div><br>		 -->
<%-- 									</s:if> --%>
<!-- 										<div class="btn_back w180"> -->
<!-- 											<a href="javascript: auditarDocumentos();" class="icon_confirmAll" -->
<!-- 												onclick=""> -->
<%-- 											<s:text name="Auditar"/> --%>
<!-- 											</a> -->
<!-- 										</div>	 -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 						</table> -->
<%-- 					</s:if> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
	</table>	
	<table width="98%" class="contenedorFormas no-Border" align="center">	
			<tr>
				<td >
					<div align="right" class="inline" >
						<s:if test="tipoAccion == 2">
						<div>
						<div class="btn_back w110" id="btn_autorizar">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="autorizarSuspension();">
								<s:text name="midas.boton.autorizar"/>
							</a>
						</div>
						</div>
						</s:if>
						<s:if test="tipoAccion == 2">
						<div>
						<div class="btn_back w110" id="btn_rechazar">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="rechazarSuspension();">
								<s:text name="midas.boton.rechazar"/>
							</a>
						</div>
						</div>
						</s:if>					
<!-- 					</div> -->
<!-- 				</td> -->
<!-- 				<td> -->
					<div align="right" class="inline" >										
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_regresar"
								onclick="regresarSuspensiones();">
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
						<s:if test="tipoAccion == 1 || tipoAccion == 4">
						<div>
						<div class="btn_back w110" id="btn_guardar">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="guardarSolicitudSuspension();">
								<s:text name="midas.boton.guardar"/>
							</a>
						</div>
						</div>
						</s:if>				
					</div>
					</div>
				</td>	
			</tr>
		</table>		
<!-- 	<br> -->
<!-- 	<br>							 -->
<!-- 	<table width="98%" class="contenedorFormas no-Border" align="center">	 -->
<!-- 			<tr> -->
<!-- 				<td> -->
<!-- 					<div align="right" class="inline" >										 -->
<!-- 						<div class="btn_back w110"> -->
<!-- 							<a href="javascript: void(0);" class="icon_regresar" -->
<!-- 								onclick="regresarSuspensiones();"> -->
<%-- 								<s:text name="midas.boton.regresar"/> --%>
<!-- 							</a> -->
<!-- 						</div> -->
<%-- 						<s:if test="tipoAccion == 1 || tipoAccion == 4"> --%>
<!-- 						<div> -->
<!-- 						<div class="btn_back w110" id="btn_guardar"> -->
<!-- 							<a href="javascript: void(0);" class="icon_guardar" -->
<!-- 								onclick="guardarSolicitudSuspension();"> -->
<%-- 								<s:text name="midas.boton.guardar"/> --%>
<!-- 							</a> -->
<!-- 						</div> -->
<!-- 						</div> -->
<%-- 						</s:if>	 --%>
<!-- 						<div> -->
<%-- 						<s:if test="suspension.id!=null"> --%>
<%-- 						</s:if> --%>
<!-- 						</div>				 -->
<!-- 					</div> -->
<!-- 				</td>	 -->
<!-- 			</tr> -->
<!-- 		</table> -->
			
</s:form>	