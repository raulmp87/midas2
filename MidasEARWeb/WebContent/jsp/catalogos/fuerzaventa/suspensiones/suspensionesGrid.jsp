<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="50" sort="int">Id Solicitud</column>
		<column id="claveAgente" type="ro" width="100" sort="str"><s:text name="midas.prestamosAnticipos.numeroAgente"/></column>
		<column id="nombreAgente" type="ro" width="*" sort="str"><s:text name="midas.prestamosAnticipos.nombreAgente"/></column>
		<column id="estatusSolicitado" type="ro" width="*" sort="str">Estatus Solicitado</column>
		<column id="fechaSolicitud" type="ro" width="80" sort="str">Fecha de Solicitud</column>
		<column id="solicitante" type="ro" width="80" sort="str">Solicitante</column>
		<column id="estatusMovimiento" type="ro" width="*" sort="str"><s:text name="midas.prestamosAnticipos.estatusMovimiento"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
<%-- 			<column id="accionEditar" type="img" width="30" sort="na"/> --%>
		</s:if>
	</head>
	<s:iterator value="listaSolicitudesDeSuspension" var="rowSuspension" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowSuspension.id}]]></cell>
			<cell><![CDATA[${rowSuspension.idAgente}]]></cell>
			<cell><![CDATA[${rowSuspension.nombreAgente}]]></cell>
			<cell><![CDATA[${rowSuspension.estatusAgente}]]></cell>
			<cell><![CDATA[${rowSuspension.fechaSolicitudString}]]></cell>
			<cell><![CDATA[${rowSuspension.nombreSolicitante}]]></cell>
			<cell><![CDATA[${rowSuspension.estatusMovimiento}]]></cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/suspensiones/verDetalle.action", 2,{"suspension.id":${rowSuspension.id},"idRegistro":${rowSuspension.id},"idTipoOperacion":80})^_self</cell>
<%-- 				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/suspensiones/verDetalle.action", 4,{"suspension.id":${rowSuspension.id},"idRegistro":${rowSuspension.id},"idTipoOperacion":80})^_self</cell> --%>
			</s:if>			
		</row>
	</s:iterator>
</rows>