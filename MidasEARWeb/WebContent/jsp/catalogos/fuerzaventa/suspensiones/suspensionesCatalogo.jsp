<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
		 
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/fuerzaventa/suspensiones/listar.action?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"suspensionesGrid", null,idField,'suspensionModal');
 });
	
</script>
<s:form action="listarFiltrado" id="suspensionForm" name="suspensionForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.suspensiones.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="Fecha de Alta Inicio" /></th>	
				<td><sj:datepicker name="filtrar.fechaInicio" id="" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);"></sj:datepicker></td>
	 			<th>&nbsp;<s:text name="Fecha de Alta Fin" /></th>		   
	 			<td><sj:datepicker name="filtrar.fechaFin" id="" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);"></sj:datepicker></td>
	 		    <th>&nbsp;<s:text name="midas.prestamosAnticipos.estatusMovimiento" /></th>
	 		    <td><s:select name="filtrar.estatusMovimiento.id" id="estatusMovimiento" cssClass="cajaTextoM2 w130"
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					list="listaEstatusMovimiento" listKey="id" listValue="valor" disabled="#readOnly"/></td>
				
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td><s:textfield name="filtrar.agente.idAgente" id="" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.nombreAgente"/></th>	
			<td><s:textfield name="filtrar.agente.persona.nombreCompleto" cssClass="cajaTextoM2 w170"/></td> 
			<th>&nbsp;<s:text name="midas.suspensiones.estatus" /></th>	
	 		<td><s:select  name="filtrar.estatusAgtSolicitado.id" id="estatusAgente" cssClass="cajaTextoM2 w130"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaEstatusAgente" listKey="id" listValue="valor" disabled="#readOnly"/></td>			
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.suspensiones.solicitante"/></th>	
			<td><s:textfield name="filtrar.nombreSolicitante" cssClass="cajaTextoM2 w170"/></td> 
		</tr>
		<tr>	
			
			<td colspan="6" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico('/MidasWeb/fuerzaventa/suspensiones/listarFiltrado.action', 'suspensionesGrid', document.suspensionForm,'${idField}','suspensionModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="suspensionesGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica('/MidasWeb/fuerzaventa/suspensiones/verDetalle.action',1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>