<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
jQIsRequired();
</script>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/autorizacionagentes/autorizacionAgentesHeader.jsp"></s:include>
<s:form action="rechazarAutorizacion" id="rechazoAgentesForm" name="rechazoAgentesForm">
<table width="97%" class="ContenedorFormas">
		<tr><!--<s:hidden name="agenteAutorizacion.id"></s:hidden>-->
			<s:hidden name="agenteAutorizacion.id"/>
			<th><s:text name="midas.fuerzaventa.negocio.nombreAgente"></s:text></th>
			<td><s:textfield name="agenteAutorizacion.persona.nombreCompleto" id="txtnombreCompleto" cssClass="cajaTextoM2 w200" readonly="true"></s:textfield></td>      
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.motivoRechazo"></s:text></th>
			<td><s:select name="agenteAutorizacion.idMotivoRechazo" cssClass="cajaTextoM2 jQrequired w200"
				       list="#{'1':'Motivo 1','2':'Motivo 2' }" labelposition="left"  /></td>	

		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.agente.tipoCedula"></s:text></th>
			<td>
				<s:hidden name="agenteAutorizacion.tipoCedula.id" ></s:hidden>
				<s:select name="agenteAutorizacion.tipoCedula.id"  headerKey="" headerValue="Seleccione.." disabled="true"
				 listValue="valor" listKey="id" cssClass="cajaTextoM2 w200" list="listaCedulas" />				
			</td>			      
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.vigenciaEmitir"></s:text> (Días)</th>
			<td><s:textfield name="agenteAutorizacion.diasParaEmitirEnRechazo" cssClass="cajaTextoM2 w40 jQrequired jQnumeric jQrestrict"></s:textfield></td>
		</tr>
		<tr>			
			<th><s:text name="midas.agentes.afianzadora.personalidadJuridica"></s:text></th>
			<s:hidden name="agenteAutorizacion.persona.claveTipoPersona"> </s:hidden>
			<td><s:select name="agenteAutorizacion.persona.claveTipoPersona" cssClass="cajaTextoM2 w200" disabled="true"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'1':'Personalidad Fisica', '2':'Personalidad Moral'}"/>	
			</td>   
<!-- 			<th>				 -->
<%-- 				<s:text name="midas.fuerzaventa.fechaAlta" /> --%>
<!-- 			</th>	 -->
<!-- 			<td>	  -->
<%-- 				<sj:datepicker name="agenteAutorizacion.fechaAutorizacionRechazo"  --%>
<!-- 							   buttonImage="../img/b_calendario.gif" -->
<!-- 							   id="fechaAutorizacionRechazo" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx jQrequired" 								   								   -->
<!-- 							   onkeypress="return soloFecha(this, event, false);" -->
<!-- 							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" -->
<%-- 							   onblur="esFechaValida(this);"></sj:datepicker>	 --%>
<!-- 			</td>		 -->
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.prioridad"></s:text></th>			
			<td>
				<s:hidden name="agenteAutorizacion.idPrioridadAgente" ></s:hidden>
				<s:select name="agenteAutorizacion.idPrioridadAgente"  headerKey="" headerValue="Seleccione.." disabled="true"
				 listValue="valor" listKey="id" cssClass="cajaTextoM2 w200" list="listaPrioridades" />				
			</td>			
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.observaciones"></s:text></th>
			<td rowspan="3"><s:textarea  name="agenteAutorizacion.observacionesRechazo" cols="40" rows="5" cssClass="cajaTextoM2 w200 jQrequired"/></td>			
		</tr>
		<tr>		     
			<th><s:text name="midas.fuerzaventa.curp"></s:text></th>
			<td><s:textfield name="agenteAutorizacion.persona.curp" cssClass="cajaTextoM2 w200" readonly="true"></s:textfield></td>
		</tr>
		
	</table>
</s:form>
<div class ="w500 inline" align="right">	
	<div class="btn_back w110">
		<a href="javascript: void(0);" 
			onclick="rechazarAutorizacion();">
			<s:text name="midas.boton.rechazar"/>
		</a>
	</div>		
</div>

