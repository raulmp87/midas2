<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>50</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="0" sort="int" >id</column>
		<column id="idcheck" type="ch" width="35" sort="int"></column>
 		<column id="descripcion" type="ro" width="100" sort="int"><s:text name="midas.fuerzaventa.autorizacionAgentes.diasTrans"/></column> 
 		<column id="responsable" type="ro" width="100" sort="str">Conducto Alta</column> 
 		<column id="situacion" type="ro" width="100" sort="str">Perfil</column> 
		<column id="ejecutivo" type="ro" width="250" sort="str">Ejecutivo</column>
		<column id="promotoria" type="ro" width="200" sort="str"><s:text name="midas.fuerzaventa.configBono.promotoria"/></column>
		<column id="nombre" type="ro" width="250" sort="str">Nombre</column>
		<column id="tipoCedula" type="ro" width="150" sort="str"><s:text name="midas.fuerzaventa.agente.tipoCedula"/></column>
		<column id="tipoCedula" type="ro" width="100" sort="str">RFC</column>
		<column id="tipoCedula" type="ro" width="100" sort="str">Gerencia</column>
		<column id="tipoCedula" type="ro" width="100" sort="str">Prioridad</column>
		<column id="tipoCedula" type="ro" width="170" sort="str"><s:text name="midas.catalogos.centro.operacion.situacion"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<!-- el id debe de ir en la primer celda ya que se busca desde javascript para ejecutar funciones de rechazar y aceptar -->
	<s:iterator value="listaAgentesPorAutorizar" var="agentesPorAutorizar" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${agentesPorAutorizar.id}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.id}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.diasTranscurridos}]]></cell> 
 			<cell><![CDATA[${agentesPorAutorizar.conductoAlta}]]></cell> 
 			<cell><![CDATA[${agentesPorAutorizar.perfil}]]></cell> 
			<cell><![CDATA[${agentesPorAutorizar.ejecutivo}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.promotoria}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.nombreCompleto}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.tipoCedulaAgente}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.codigoRfc}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.gerencia}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.prioridad}]]></cell>
			<cell><![CDATA[${agentesPorAutorizar.tipoSituacion}]]></cell>		
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleAutorizacionAgentesPath, 2,{"agente.id":${agentesPorAutorizar.id},"idRegistro":${agentesPorAutorizar.id},"idTipoOperacion":50,"moduloOrigen":1})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleAutorizacionAgentesPath, 4,{"agente.id":${agentesPorAutorizar.id},"idRegistro":${agentesPorAutorizar.id},"idTipoOperacion":50,"moduloOrigen":1})^_self</cell>
				<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleAutorizacionAgentesPath, 3,{"agente.id":${agentesPorAutorizar.id},"idRegistro":${agentesPorAutorizar.id},"idTipoOperacion":50,"moduloOrigen":1})^_self</cell>
			</s:if>			
		</row>
	</s:iterator>
</rows>