<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/autorizacionagentes/autorizacionAgentesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarFiltradoAutorizacionAgentesPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"autorizacionAgentesGrid", null,idField);


 });

</script>
<s:form action="listarFiltrado" id="autorizacionAgentesForm" name="autorizacionAgentesForm">
<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.agente.autorizacionagente.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.ejecutivo.gerencia" />
			</th>
			<td>
				<s:select id="idGerencia" name="filtroAgenteAutorizacion.promotoria.ejecutivo.gerencia.id" value="idGerencia" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivo', 'idPromotoria',this.value)" cssClass="cajaTextoM2 w200"/> <!-- jQrequired -->
			</td>		
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text></th>
			<td>
				<s:select id="idEjecutivo" name="filtroAgenteAutorizacion.promotoria.ejecutivo.id" value="idEjecutivo" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoria',this.value)" cssClass="cajaTextoM2  w200 "/> <!-- jQrequired -->
<%-- 				<s:select name="filtroAgenteAutorizacion.ejecutivo" headerKey="" headerValue="Seleccione.." --%>
<!-- 					 	listValue="personaResponsable.nombreCompleto" listKey="id" cssClass="cajaTextoM2 w200" -->
<!-- 				       list="listEjecutivo" />			 -->
			</td>
			<th><s:text name="midas.fuerzaventa.negocio.promotoria"></s:text></th>
			<td>
				<s:select id="idPromotoria" name="filtroAgenteAutorizacion.promotoria.id" value="filtroAgenteAutorizacion.promotoria.id" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 w200 "/> <!-- jQrequired -->
<%-- 				<s:select name="filtroAgenteAutorizacion.promotoria"  headerKey="" headerValue="Seleccione.." --%>
<!-- 				 listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200" list="listPromotoria" />				 -->
			</td>
		</tr>
		<tr>	
			<th><s:text name="midas.fuerzaventa.negocio.rfc"></s:text></th>	 
			<td><s:textfield name="filtroAgenteAutorizacion.persona.rfc" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td>
			<th><s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text></th>	 
			<td><s:textfield name="filtroAgenteAutorizacion.idAgente" id="txtIdAgente" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield></td>
		</tr>		
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.nombre"></s:text></th>	 
			<td><s:textfield name="filtroAgenteAutorizacion.persona.nombreCompleto" id="txtClave" cssClass="cajaTextoM2 w200" ></s:textfield></td>
<%-- 			<th><s:text name="midas.fuerzaventa.negocio.razonSocial"></s:text></th>       --%>
<%-- 			<td><s:textfield name="filtroAgenteAutorizacion.persona.razonSocial" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td> --%>
			<th><s:text name="midas.fuerzaventa.negocio.telefono"></s:text></th>
			<td><s:textfield name="filtroAgenteAutorizacion.persona.telefonoCasa" id="txtClave" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield></td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaInicio"></s:text>
			</th>	 
			<td>
				<sj:datepicker name="filtroAgenteAutorizacion.fechaAlta" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroAgente.fechaAlta" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAlta" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaFin"></s:text>
			</th>	 
			<td>
				<sj:datepicker name="filtroAgenteAutorizacion.fechaAltaFin" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroAgente.fechaAltaFin" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAltaFin" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>
		</tr>
		<tr>
		<td colspan="6">
		<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >			
			<s:param name="idEstadoName">filtroAgenteAutorizacion.persona.domicilios[0].claveEstado</s:param>	
			<s:param name="idCiudadName">filtroAgenteAutorizacion.persona.domicilios[0].claveCiudad</s:param>
			<s:param name="idColoniaName">filtroAgenteAutorizacion.persona.domicilios[0].nombreColonia</s:param>			
			<s:param name="cpName">filtroAgenteAutorizacion.persona.domicilios[0].codigoPostal</s:param>
							
			<s:param name="labelEstado"><s:text name="midas.catalogos.centro.operacion.estado"/></s:param>
			<s:param name="labelColonia"><s:text name="midas.catalogos.centro.operacion.colonia"/></s:param>
			<s:param name="labelCodigoPostal"><s:text name="midas.catalogos.centro.operacion.codigoPostal"/></s:param>			
			<s:param name="labelCiudad"><s:text name="midas.catalogos.centro.operacion.municipio"/></s:param>
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">6</s:param>						
		</s:action>
		</td>
		</tr>
		<tr>			
			<td colspan="6"  align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="listarFiltradoGenerico(listarFiltradoAutorizacionAgentesPath,'autorizacionAgentesGrid',document.autorizacionAgentesForm);activaCheck();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>			
	</table>
	<div class ="w900 inline">
		<div class="w150"><s:checkbox name="checkAll" id="checkAll" value="ok"  label="Seleccionar Todos" onclick="checkUncheckAll();" labelposition="left"/></div>
		<div class="btn_back w150">
			<a href="javascript: void(0);" 
				onclick="allChecksSelected();">
				<s:text name="Filtrar los Marcados"/>
			</a>
		</div>	
		<div class="btn_back w110">
			<a href="javascript: void(0);" 
				onclick="reloadGrid();">
				<s:text name="Cancelar Filtro"/>
			</a>
		</div>	
	</div>
	<div id="divCarga" style="position:absolute;"></div>
	<div id="autorizacionAgentesGrid" class="w880 h180" style="overflow:hidden"></div>

</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<div class ="w900 inline" align="right">	
	<div class="btn_back w110" id="jQBtnRechazar" >
		<a href="javascript: void(0);" style="display:none;"
			onclick="mostrarModalRechazarAutorizacion();">
			<s:text name="midas.boton.rechazar"/>
		</a>
	</div>	
	<div class="btn_back w110" id="btnAutorizar">
		<a href="javascript: void(0);" 
			onclick="aceptarAutorizacion();">
			<s:text name="midas.boton.autorizar"/>
		</a>
	</div>	
	
</div>

