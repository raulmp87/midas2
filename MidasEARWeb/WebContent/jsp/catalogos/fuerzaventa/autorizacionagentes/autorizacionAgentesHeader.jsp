<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/autorizacionagentes.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
 
<script type="text/javascript">
	var listarAautorizacionAgentesPath = '<s:url action="listar" namespace="/fuerzaventa/autorizacionagentes"/>';
	var listarFiltradoAutorizacionAgentesPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/autorizacionagentes"/>';		
	var verDetalleAutorizacionAgentesPath = '<s:url action="verDetalle" namespace="/fuerzaventa/agente"/>';	
	//var urlBusquedaDomicilio = '<s:url action="buscarDomicilio" namespace="/fuerzaventa/afianzadora"/>';
	
</script>