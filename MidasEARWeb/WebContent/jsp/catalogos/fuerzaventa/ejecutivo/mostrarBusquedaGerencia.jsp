<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<sj:head/>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<script type="text/javascript">
	jQuery.noConflict();
	jQuery(function(){
		listarFiltradoGenerico(listarFiltradoGerenciaPath, 'busquedaGerenciaGrid',null);
	});
</script>
<s:include value="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoHeader.jsp"></s:include>


<s:form action="listarFiltrado" id="busquedaGerenciaForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.centro.operacion.busqueda"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="ejecutivo.gerencia.descripcion" key="midas.catalogos.centro.operacion.descripcion" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.centroDeOperacion"  name="ejecutivo.gerencia.centroOperacion" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:textfield name="ejecutivo.gerencia.responsable" id="txtClave" key="midas.catalogos.centro.operacion.responsable" labelposition="left"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.estado"  name="ejecutivo.gerencia.estado" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.municipio" name="ejecutivo.gerencia.municipio" cssClass="cajaTextoM2"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>	 
			<td colspan="2">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="ejecutivo.gerencia.calleYNumero" key="midas.catalogos.centro.operacion.calleYNumero" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="ejecutivo.gerencia.codigoPostal" key="midas.catalogos.centro.operacion.codigoPostal" labelposition="left"></s:textfield>
			</td>	 
			<td colspan="2">
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<div align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="listarFiltradoGenerico(listarFiltradoGerenciaPath, 'busquedaGerenciaGrid',document.busquedaGerenciaForm);">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>	
				</div>
			</td>
			<td></td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="6">
				<div id="busquedaGerenciaGrid" width="400px" height="250px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
	</table>
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>
<div align="center">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_masAgregar"
			onclick="">
			<s:text name="midas.boton.seleccionar"/>
		</a>
	</div>
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_cancelar"
			onclick="">
			<s:text name="midas.boton.cancelar"/>
		</a>
	</div>
</div>

