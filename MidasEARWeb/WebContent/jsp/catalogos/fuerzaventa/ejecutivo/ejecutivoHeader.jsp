<%@ taglib prefix="s" uri="/struts-tags" %>
	
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script> 
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
	var mostrarEjecutivoPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/ejecutivo"/>';
	var mostrarBusquedaGerenciaPath = '<s:url action="mostrarBusquedaGerencia" namespace="/fuerzaventa/ejecutivo"/>';
	var mostrarBusquedaPersonaPath = '<s:url action="mostrarBusquedaPersona" namespace="/fuerzaventa/ejecutivo"/>';
	var mostrarPromotoriaPath = '<s:url action="mostrarPromotoria" namespace="/fuerzaventa/ejecutivo"/>';
	
	var verDetalleEjecutivoPath = '<s:url action="verDetalle" namespace="/fuerzaventa/ejecutivo"/>';
	var guardarEjecutivoPath = '<s:url action="guardar" namespace="/fuerzaventa/ejecutivo"/>';
	var eliminarEjecutivoPath = '<s:url action="eliminar" namespace="/fuerzaventa/ejecutivo"/>';
	
	var listarEjecutivoPath = '<s:url action="lista" namespace="/fuerzaventa/ejecutivo"/>';
	var listarFiltradoEjecutivoPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/ejecutivo"/>';
	
	var listarPersonaPath = '<s:url action="listarPersona" namespace="/fuerzaventa/ejecutivo"/>';
	var listarFiltradoPersonaPath = '<s:url action="listarFiltradoPersona" namespace="/fuerzaventa/ejecutivo"/>';
	
	var listarGerenciasPath = '<s:url action="listarGerencias" namespace="/fuerzaventa/ejecutivo"/>';
	var listarFiltradoGerenciasPath = '<s:url action="listarFiltradoGerencias" namespace="/fuerzaventa/ejecutivo"/>';
</script>