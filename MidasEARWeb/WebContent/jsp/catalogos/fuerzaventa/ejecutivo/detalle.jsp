<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/ejecutivo.js'/>"></script>
<script type="text/javascript">
	jQIsRequired();
	var ventanaGerencia=null;
	function mostrarModalGerencia(){
		var url="/MidasWeb/fuerzaventa/gerencia/mostrarContenedor.action?tipoAccion=consulta&idField=idGerencia";
		sendRequestWindow(null, url, obtenerVentanaGerencia);
	}
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaGerencia(){
		var wins = obtenerContenedorVentanas();
		ventanaGerencia= wins.createWindow("gerenciaModal", 400, 320, 900, 450);
		ventanaGerencia.center();
		ventanaGerencia.setModal(true);
		ventanaGerencia.setText("Consulta de Gerencias");
		ventanaGerencia.button("park").hide();
		ventanaGerencia.button("minmax1").hide();
		return ventanaGerencia;
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdGerencia(idGerencia){
		var url="/MidasWeb/fuerzaventa/gerencia/findById.action";
		var data={"gerencia.id":idGerencia};
		jQuery.asyncPostJSON(url,data,populateGerencia);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populateGerencia(json){
		if(json){
			var gerencia=json.gerencia;
			var idGerencia=gerencia.id;
			jQuery("#idGerencia").val(idGerencia);
			var nombre=gerencia.descripcion;
			jQuery("#nombreGerencia").val(nombre);	
		}
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			var email=json.personaSeycos.email;
			jQuery("#idPersonaResponsable").val(idResponsable);
			jQuery("#nombreResponsable").val(nombre);
			jQuery("#txtCorreo").val(email);
			var domicilios=json.personaSeycos.domicilios;
			var domicilioOficina=null;
		if(!jQuery.isNull(domicilios)){
			for(var i=0;i<domicilios.length;i++){
				var domicilioPers=domicilios[0];
				var domicilio=domicilios[i];
				var tipoDomicilio=domicilio.tipoDomicilio;
				if(tipoDomicilio!=null && "OFIC"==tipoDomicilio){
					domicilioOficina=domicilio;
					break;
				}
			}
		}
		if(!jQuery.isNull(domicilioOficina)){
			populateDomicilio(domicilioOficina);
		}else if(!jQuery.isNull(domicilioPers)){
			populateDomicilio(domicilioPers);
		}else{
			dwr.util.setValue("ejecutivo.domicilio.clavePais","");
			dwr.util.setValue("ejecutivo.domicilio.claveEstado","");
			dwr.util.setValue("ejecutivo.domicilio.claveCiudad","");
			dwr.util.setValue("ejecutivo.domicilio.nombreColonia","");
			dwr.util.setValue("ejecutivo.domicilio.calleNumero","");
			dwr.util.setValue("ejecutivo.domicilio.codigoPostal","");
		}
		}
	}
	
	function populateDomicilio(json){
	if(json){
		/*var estadoName="promotoria.domicilio.claveEstado";
		var paisName="promotoria.domicilio.clavePais";
		var ciudadName="promotoria.domicilio.claveCiudad";
		var coloniaName="promotoria.domicilio.nombreColonia";
		var calleNumeroName="promotoria.domicilio.calleNumero";
		var codigoPostalName="promotoria.domicilio.codigoPostal";*/
		var estadoName="ejecutivo.domicilio.claveEstado";
		var paisName="ejecutivo.domicilio.clavePais";
		var ciudadName="ejecutivo.domicilio.claveCiudad";
		var coloniaName="ejecutivo.domicilio.nombreColonia";
		var calleNumeroName="ejecutivo.domicilio.calleNumero";
		var codigoPostalName="ejecutivo.domicilio.codigoPostal";
		var idDomicilioName="ejecutivo.domicilio.idDomicilio.idDomicilio";
		var idEstado=json.claveEstado;
		var idPais=json.clavePais;
		var idCiudad=json.claveCiudad;
		var calleNumero=json.calleNumero;
		var colonia=json.nombreColonia;
		var codigoPostal=json.codigoPostal;
		var idDomicilio=json.idDomicilio.idDomicilio;
		jQuery("#pais").val(idPais);
		jQuery("#estado").val(idEstado);
		jQuery("#ciudad").val(idCiudad);		
		jQuery("#colonia").val(colonia);
		jQuery("#codigoPostal").val(codigoPostal);
		jQuery("#calleNumero").val(calleNumero);
		dwr.util.setValue(paisName, idPais);
		onChangePais(estadoName,ciudadName,coloniaName,codigoPostalName,calleNumeroName,paisName);
		dwr.util.setValue(estadoName, idEstado);
		onChangeEstadoGeneral(ciudadName,coloniaName,codigoPostalName,calleNumeroName, estadoName);
		dwr.util.setValue(ciudadName, idCiudad);
		onChangeCiudad(coloniaName,codigoPostalName,calleNumeroName,ciudadName);
		dwr.util.setValue(coloniaName, colonia);
		onChangeColonia(codigoPostalName,calleNumeroName,coloniaName ,ciudadName);
		dwr.util.setValue(calleNumeroName, calleNumero);
		dwr.util.setValue(codigoPostalName, codigoPostal);
		dwr.util.setValue(idDomicilioName, idDomicilio);
		//Cambio para poner hidden los campos de domicilio
	}
}
</script>
<s:include value="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoHeader.jsp"></s:include>

<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save'))realizarOperacionGenerica(guardarEjecutivoPath, document.ejecutivoForm , null);'" />
	<s:set id="readOnly" value="false"/>
	<s:set id="required" value="1"/>
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.ejecutivo.tituloAlta')}"/>	
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true"/>
	<s:set id="required" value="0"/>
	<s:set id="display" value="false" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.ejecutivo.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'realizarOperacionGenerica(eliminarEjecutivoPath, document.ejecutivoForm , null);'" />
	<s:set id="readOnly" value="true" />
	<s:set id="required" value="0"/>
	<s:set id="display" value="false" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.ejecutivo.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true))realizarOperacionGenerica(guardarEjecutivoPath, document.ejecutivoForm , null);'" />
 	<s:set id="readOnly" value="false" />
 	<s:set id="required" value="1"/>
 	<s:set id="display" value="false" />	
 	<s:set id="titulo" value="%{getText('midas.fuerzaventa.ejecutivo.tituloEditar')}"/>
</s:elseif>

<s:form action="listarFiltrado" id="ejecutivoForm">
    <s:if test="tipoAccion != 5">
        <div class="titulo w400"><s:text name="#titulo"/></div>
    </s:if>	
	<s:hidden id ="idEjecutivo" name="ejecutivo.id" ></s:hidden>
	<table width="97%" class="contenedorFormas" align="center" >
		<tr>
			<th class="titulo" colspan="4" >
				<s:text name="midas.fuerzaventa.ejecutivo.datosEjecutivo"/>
			</th>
		</tr>
		<tr>
			<th ><s:text name="midas.fuerzaventa.negocio.ejecutivo.idEjecutivo" /></th>			
			<td colspan="3"><s:textfield readonly="true" name="ejecutivo.idEjecutivo" cssClass="cajaTextoM2 w100"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.descripcion" /></th>
			<td><s:textfield id="idPersonaResponsable" readonly="true" name="ejecutivo.personaResponsable.idPersona" cssClass="cajaTextoM2 jQrequired w100" onchange="javascript:findByIdResponsable(this.value);"></s:textfield></td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<div class="btn_back w80">
					<a href="javascript: void(0)" class="icon_buscar"
						onclick="mostrarModalResponsable();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</s:if>	
			</td>
			<td><s:textfield id="nombreResponsable" name="ejecutivo.personaResponsable.nombreCompleto" cssClass="cajaTextoM2 w200" readonly="true"></s:textfield></td>
		</tr>
		<tr>
			<th ><s:text name="midas.fuerzaventa.negocio.correoElectronico" /></th>		
			<td colspan="3"><s:textfield name="ejecutivo.personaResponsable.email" id="txtCorreo" readonly="true"  cssClass="cajaTextoM2 w200"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.tipoEjecutivo" /></th>
			<td colspan="3"><s:select disabled="#readOnly"  headerKey="" headerValue="Seleccione..." list="listaTipoEjecutivo" listValue="valor" listKey="id" name="ejecutivo.tipoEjecutivo.id" cssClass="cajaTextoM2 w150 jQrequired" ></s:select></td>
		</tr>
		<tr>	
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.situacion" /></th>		
			<td colspan="3">
				<s:select disabled="#readOnly"  name="ejecutivo.claveEstatus" cssClass="cajaTextoM2 w250 jQrequired"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'1':'Activo', '0':'Inactivo'}"/>
			</td>
		</tr>
		<tr>
			<th width="110px" class="jQIsRequired"><s:text name="midas.fuerzaventa.ejecutivo.IdGerencia" /></th>
			<td width="110px"><s:textfield name="ejecutivo.gerencia.id" id="idGerencia" readonly="true" cssClass="cajaTextoM2 jQrequired w100"  onchange="javascript:findByIdGerencia(this.value);"></s:textfield></td>
			<td width="110px">
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<div class="btn_back w80">
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalGerencia();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
				</s:if>
			</td>
			<td><s:textfield name="ejecutivo.gerencia.descripcion" id="nombreGerencia" readonly="true" cssClass="cajaTextoM2 w200"></s:textfield></td>
		</tr>
		<tr>
			<td>
				<s:if test="tipoAccion!=1">
					<div class="btn_back w110">
						<a href="javascript: void(0);"
							onclick="mostrarVentanaGenerica(mostrarPromotoriaPath ,'<s:text name="midas.fuerzaventa.ejecutivo.tituloPromotoria"/>',350,200, document.ejecutivoForm);">
							<s:text name="midas.fuerzaventa.ejecutivo.promotoria"/>
						</a>
					</div>
				</s:if>	
			</td>
		</tr>
	</table>
	<table width="97%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="midas.boton.domicilio"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idDomicilioName">ejecutivo.domicilio.idDomicilio.idDomicilio</s:param>
					<s:param name="idPaisName">ejecutivo.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">ejecutivo.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">ejecutivo.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">ejecutivo.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">ejecutivo.domicilio.calleNumero</s:param>
					<s:param name="cpName">ejecutivo.domicilio.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">ejecutivo.domicilio.nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck</s:param>				
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado"><s:text name="midas.catalogos.centro.operacion.estado"/></s:param>
					<s:param name="labelColonia"><s:text name="midas.catalogos.centro.operacion.colonia"/></s:param>
					<s:param name="labelCodigoPostal"><s:text name="midas.catalogos.centro.operacion.codigoPostal"/></s:param>
					<s:param name="labelCalleNumero"><s:text name="midas.catalogos.centro.operacion.calleYNumero"/></s:param>
					<s:param name="labelCiudad"><s:text name="midas.catalogos.centro.operacion.municipio"/></s:param>
					<s:param name="labelPosicion">top</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="true"></s:param>
					<s:param name="requerido" value="%{#required}"></s:param>
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
				</s:action>
			</td>
		</tr>
		<!--  
		<tr>
			<td>
				<s:hidden name="ejecutivo.domicilio.clavePais" id="pais"/>
				<s:hidden name="ejecutivo.domicilio.claveEstado" id="estado"/>
				<s:hidden name="ejecutivo.domicilio.claveCiudad" id="ciudad"/>
				<s:hidden name="ejecutivo.domicilio.nombreColonia" id="colonia"/>
				<s:hidden name="ejecutivo.domicilio.codigoPostal" id="codigoPostal"/>
				<s:hidden name="ejecutivo.domicilio.calleNumero" id="calleNumero"/>				
			</td>
		</tr>
		-->
	</table>
	<s:if test="ejecutivo.id != null">
	<table width="97%" class="contenedorFormas" align="center">	
		<tr>
		    <s:if test="tipoAccion != 5">
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>      
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2">
				<s:if test="tipoAccion != 5">
				    <div class="btn_back w110">
						<a href="javascript: mostrarHistorico(30,${ejecutivo.id});" class="icon_guardar">
							<s:text name="midas.boton.historico"/>
						</a>
					</div>
				</s:if>				
			</td>
		</tr>
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>	
	<table width="97%" class="contenedorFormas" align="center" style="border-style: none; ">	
	<tr>
		<td colspan="5">&nbsp;</td>
		<td align="right" class="inline">
		<s:if test="tipoAccion != 5">
		    <div class="btn_back w100">
			<a href="javascript: salirEjecutivo();" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
		</s:if>
				
		<s:if test="tipoAccion == 1 || tipoAccion == 4">		
		<div class="btn_back w100">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="guardarEjecutivo();">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>		
		</s:if>	
		<s:if test="tipoAccion == 3">		
		<div class="btn_back w100">
			<a href="javascript: void(0);" class="icon_borrar"
				onclick="realizarOperacionGenerica(eliminarEjecutivoPath, document.ejecutivoForm , null);">
				<s:text name="midas.boton.borrar"/>
			</a>
		</div>		
		</s:if>
		</td>	
	</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
   var idPersonaResp=jQuery("#idPersonaResponsable").val();
   if(idPersonaResp!=null){
		findByIdResponsable(idPersonaResp);
	}
</script>