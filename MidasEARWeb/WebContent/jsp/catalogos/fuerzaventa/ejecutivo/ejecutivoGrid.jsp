<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idEjecutivo" type="ro" width="50" sort="int"><s:text name="midas.negocio.producto.id"/></column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.negocio.producto.descripcion"/></column>
		<column id="gerencia" type="ro" width="200" sort="str"><s:text name="midas.negocio.gerencia"/></column>
		<column id="tipoEjecutivo" type="ro" width="140" sort="str"><s:text name="midas.fuerzaventa.ejecutivo.tipoEjecutivo"/></column>
		<column id="claveEstatus" type="ro" width="80" sort="str"><s:text name="midas.catalogos.fuerzaventa.grid.estatus.titulo"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaEjecutivoView" var="rowEjecutivo" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${nombreCompleto}]]></cell>
			<cell><![CDATA[${descripcion}]]></cell>
			<cell><![CDATA[${tipoEjecutivo}]]></cell>
			<s:if test="claveEstatus == 1">
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.activo"></s:text></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.inactivo"></s:text></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleEjecutivoPath, 2,{"ejecutivo.id":${rowEjecutivo.id},"idRegistro":${rowEjecutivo.id},"idTipoOperacion":30})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleEjecutivoPath, 4,{"ejecutivo.id":${rowEjecutivo.id},"idRegistro":${rowEjecutivo.id},"idTipoOperacion":30})^_self</cell>
				<s:if test="claveEstatus == 1">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleEjecutivoPath, 3,{"ejecutivo.id":${rowEjecutivo.id},"idRegistro":${rowEjecutivo.id},"idTipoOperacion":30})^_self</cell>
				</s:if>
			</s:if>
		</row>
	</s:iterator>
</rows>