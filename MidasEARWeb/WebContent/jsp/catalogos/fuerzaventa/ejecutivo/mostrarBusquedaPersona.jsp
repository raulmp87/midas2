<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<sj:head/>		
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<script type="text/javascript">
	jQuery.noConflict();
	jQuery(function(){
		listarFiltradoGenerico(listarFiltradoPersonaPath, 'busquedaPersonaGrid',null);
	});
</script>
<s:include value="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="busquedaPersonaForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.centro.operacion.busqueda"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.nombre" key="midas.catalogos.centro.operacion.nombre" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.razonSocial" id="txtClave" key="midas.catalogos.centro.operacion.razonSocial" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.rfc" id="txtClave" key="midas.catalogos.centro.operacion.rfc" labelposition="left"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.estado"  name="ejecutivo.persona.estado" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.municipio" name="ejecutivo.persona.municipio" cssClass="cajaTextoM2"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>	 
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.colonia"  name="ejecutivo.persona.colonia" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.calleYNumero" key="midas.catalogos.centro.operacion.calleYNumero" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.codigoPostal" key="midas.catalogos.centro.operacion.codigoPostal" labelposition="left"></s:textfield>
			</td>	 
			<td colspan="2">
				<s:textfield name="ejecutivo.persona.telefono" key="midas.catalogos.centro.operacion.telefono" labelposition="left"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<div align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="listarFiltradoGenerico(listarFiltradoPersonaPath, 'busquedaPersonaGrid',document.busquedaPersonaForm);">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>	
				</div>
			</td>
			<td></td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="6">
				<div id="busquedaPersonaGrid" width="400px" height="250px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
	</table>
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>
<div align="center">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_masAgregar"
			onclick="">
			<s:text name="midas.boton.seleccionar"/>
		</a>
	</div>
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_cancelar"
			onclick="">
			<s:text name="midas.boton.cancelar"/>
		</a>
	</div>
</div>

