<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
<!--
function addTooltTipJavascript(idElemento){
	var elementoOption = document.getElementById(idElemento).options;
	var numberOption = elementoOption.length;
	for(x=0; x<numberOption; x++){
		var option = elementoOption[x].text;
		elementoOption[x].setAttribute('title',option);
	}
}
//-->
</script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<s:form action="listarFiltrado" id="promotoriaForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo">
				<s:text name="midas.fuerzaventa.ejecutivo.listaPromotoria"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="proMostrar" multiple="true"   name="listaPromotorias" listValue="descripcion" listKey="id" cssClass="cajaTextoM w200" list="listPromotoria" labelposition="left"/>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
<!--
 addTooltTipJavascript('proMostrar');
//-->
</script>
</s:form>