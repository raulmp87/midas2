<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>   
            <call command="enableDragAndDrop"><param>true</param></call> 
        </beforeInit>		
		<column id="id" type="ro" width="40" sort="int" >id</column>		
		<column id="situacion" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.cedulas.descripcionTipoCed"/></column>
		
	</head>
	<s:iterator value="listaSubRamosAsociados" var="rowlistaSubRamosAsociados" status="index">
	<row id="${rowlistaSubRamosAsociados.subramoDTO.idTcSubRamo}">
			<cell>${rowlistaSubRamosAsociados.subramoDTO.idTcSubRamo}</cell>
			<cell>${rowlistaSubRamosAsociados.subramoDTO.descripcionSubRamo}</cell>
		</row>
	</s:iterator>
</rows>