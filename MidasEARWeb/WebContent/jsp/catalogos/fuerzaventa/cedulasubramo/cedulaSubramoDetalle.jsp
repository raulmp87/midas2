<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.tipoCedula.tituloAlta')}"/>	
</s:if>
<s:elseif test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.tipoCedula.tituloConsultar')}"/>	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.tipoCedula.tituloEditar')}"/>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.tipoCedula.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<script type="text/javascript">
	jQIsRequired();
</script>

<s:include value="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:hidden name="cedula.id"/>
<s:form action="agregar" id="cedulaSubramoForm" name="cedulaSubramoForm">
	<div class="titulo w400"><s:text name="#titulo"/></div>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th width="160px">				
				<s:text name="midas.fuerzaventa.cedulas.idTipoCed" />
			</th>
			<td><s:textfield name="cedula.id" id="txtIdCedula"  cssClass="w50 cajaTextoM2 jQnumeric" readonly="true" ></s:textfield></td>				
		</tr>
		<tr>
			<th class="jQIsRequired">				
				<s:text name="midas.catalogos.clave" />
			</th>
			<td><s:textfield name="cedula.clave" id="txtClave"  cssClass="w50 cajaTextoM2 jQrequired" maxlength="2" readonly="#readOnly" ></s:textfield></td>			
		</tr>
		<tr>
			<th class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.cedulas.descripcionTipoCed" />
			</th>
			<td><s:textfield name="cedula.valor" id="txtCedulaDescripcion"  cssClass="w450 cajaTextoM2 jQrequired" readonly="#readOnly" ></s:textfield></td>			
		</tr>
		<tr>
			<td colspan="2">
				<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
			</td>
		</tr>			
	</table>

	<table width="98%" class="contenedorFormas no-Border" align="center">
		<tr>
			<td class="inline" align="right">
				<div class="btn_back w110">
					<a href="javascript: mostrarCatalogoGenerico(mostrarContenedorCedulaSubramoPath);" class="icon_regresar"
						onclick="">
						<s:text name="midas.boton.regresar"/>
					</a>
				</div>
				<s:if test="tipoAccion==1 || tipoAccion==4">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="guardarCedula();">
						<s:text name="midas.boton.guardar"/>
					</a>
				</div>
				</s:if>
				<s:if test="tipoAccion==3">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="eliminarCedula();">
						<s:text name="Eliminar"/>
					</a>
				</div>
				</s:if>
			</td>
		</tr>
	</table>	
	
</s:form>

