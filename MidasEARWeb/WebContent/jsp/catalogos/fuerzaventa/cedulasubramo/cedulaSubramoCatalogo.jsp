<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarCedulaSubramoPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"cedulaSubramoGrid", null,idField);
 });
	
function disableEnterKey(e){
	var key;
	if(window.event){
		key = window.event.keyCode; //IE
	}else{
		key = e.which; //firefox
	}
	if(key==13){
		return false;
	}else{
		return true;
	}
}
</script>
<s:form action="listarFiltrado" id="cedulaSubramoForm" name="cedulaSubramoForm" onKeyPress="return disableEnterKey(event)">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="890px" id="filtrosM2" cellpadding="0" cellspacing="0" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.fuerzaventa.tipoCedula.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.cedulas.descripcionTipoCed" />
			</th>	
			<s:hidden name="filtroCedulas.grupoCatalogoAgente.descripcion" value="Tipos de Cedula de Agente"/>
			<td ><s:textfield name="filtroCedulas.valor" id="txtValorCedula" cssClass="cajaTextoM2 w250"></s:textfield></td>		
		</tr>		
		<tr>
			<td colspan="2" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoCedulaSubramoPath, 'cedulaSubramoGrid', document.cedulaSubramoForm);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center"  id="cedulaSubramoGrid" class="w890 h260" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<div align="right" class="w890 inline">
		<div class="btn_back w180">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="javascript:asociaRamosSubramos();">
				<s:text name="midas.boton.asociarRamosSubramos"/>
			</a>
		</div>	
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="javascript:operacionGenerica(verDetalleCedulaSubramoPath,1);">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>	
</div>

