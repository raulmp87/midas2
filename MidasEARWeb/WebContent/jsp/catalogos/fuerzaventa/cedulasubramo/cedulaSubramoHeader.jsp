<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/cedulaSubramo.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
 
<script type="text/javascript">
	var mostrarContenedorCedulaSubramoPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/cedulasubramo"/>';	
	var verDetalleCedulaSubramoPath = '<s:url action="verDetalle" namespace="/fuerzaventa/cedulasubramo"/>';	
	
	
	var asociacionRamoSubramoPath = '<s:url action="cedulaAsociacionRamo" namespace="/fuerzaventa/cedulasubramo"/>';
	var obtenerSubramosPorRamoPath = '<s:url action="findSubramosByRamos" namespace="/fuerzaventa/cedulasubramo"/>';
	
	var listarFiltradoCedulaSubramoPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/cedulasubramo"/>';
	var listarCedulaSubramoPath = '<s:url action="listar" namespace="/fuerzaventa/cedulasubramo"/>';
	var guardarCedulaPath = '<s:url action="guardar" namespace="/fuerzaventa/cedulasubramo"/>';
	
	
	var obtenerRamosSubramosAsociadosPath = '<s:url action="obtenerSubRamosAsociadosPorCedula" namespace="/fuerzaventa/cedulasubramo"/>';
	var guardarAsociacionCedulaSubRamosPath = '<s:url action="guardarAsociacionCedulaSubRamos" namespace="/fuerzaventa/cedulasubramo"/>';
	
	var eliminarCedulaSubRamosPath = '<s:url action="eliminarTipoCedula" namespace="/fuerzaventa/cedulasubramo"/>';
	
</script>