<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="40" sort="int" >id</column>		
		<column id="situacion" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.cedulas.descripcionTipoCed"/></column>
		
	</head>
	<s:iterator value="listaCedulas" var="cedulas" status="index">
	<row id="${cedulas.id}">
			<cell>${cedulas.id}</cell>
			<cell>${cedulas.id}</cell>					
		</row>
	</s:iterator>
</rows>