<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoHeader.jsp"></s:include>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/catalogos/cedulaSubramo.js'/>"></script> --%>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
// 	 var urlFiltro=listarFiltradoCedulaSubramoPath+"?tipoAccion="+tipoAccion;
// 	 var idField='<s:property value="idField"/>';
// 	 loadGridsCedulasSubramoDragNDrop(urlFiltro,"ramosAsociadosGrid", null,idField);
// 	 loadGridsCedulasSubramoDragNDrop(urlFiltro,"ramosDisponiblesGrid", null,idField);
	 iniciaGrids();
 });
	
</script>
<s:hidden name="tipoAccion"/>
<s:hidden name="cedula.id"/>
<s:hidden name="cedulaSubramo.tipoCedula.id"/>
<s:form action="agregar" id="cedulaSubramoForm" name="cedulaSubramoForm">
	<table width="97%" class="contenedorFormas" align="center" >
		<tr>
			<th class="titulo" colspan="2" >
				<s:text name="midas.fuerzaventa.cedulas.tituloAsociacionRamos"/>
			</th>
		</tr>
		<tr>
			<th width="150px">				
				<s:text name="midas.fuerzaventa.cedulas.idTipoCed" />
			</th>
			<td><s:textfield name="cedula.id" id="txtIdCedula"  cssClass="w50 cajaTextoM2 jQnumeric" readonly="true" ></s:textfield></td>				
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.cedulas.descripcionTipoCed" />
			</th>
			<td><s:textfield name="cedula.valor" id="txtCedulaDescripcion"  cssClass="w450 cajaTextoM2 jQrequired" readonly="true" ></s:textfield></td>			
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.cedulas.ramo" />
			</th>
			<td><s:select name="cedula.tipoCedula.valor" id="listaRamos" list="listaRamos" listKey="idTcRamo" listValue="descripcion" cssClass="w250 cajaTextoM2" headerKey="" headerValue="Seleccione.." onchange="loadSubramosPorRamos(this.value);"></s:select></td>
		</tr>
		<tr>
			<td class="titulo"  colspan="2"><s:text name="midas.boton.asociarRamosSubramos"/></td>
		</tr>
		<tr>
			<td colspan="2"><s:text name="midas.fuerzaventa.cedulas.ramosSubramosAsociados"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="center" id="ramoSubramoAsociadosGrid" width="700px" height="150px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
		<tr>
			<td colspan="2"><s:text name="midas.fuerzaventa.cedulas.ramosSubramosDisponibles"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="center" id="ramoSubramoDisponiblesGrid" width="700px" height="150px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
	</table>

	<div align="right" class="w900 inline" >
		<div class="btn_back w110">
			<a href="javascript: mostrarCatalogoGenerico(mostrarContenedorCedulaSubramoPath);" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
		
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="guardarAsociacionSubRamos();">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>		
	</div>	
	
</s:form>

