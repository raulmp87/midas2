<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<script type="text/javascript">
	
	var urlDescargarReporte = '<s:url action="descargar" namespace="/fuerzaventa/mediciones/reporte"/>';
	
</script>

<script src="<s:url value="/js/midas2/agentes/mediciones/reporte.js"/>" type="text/javascript"></script>

<s:form id="reporteMedicionesForm">
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.mediciones.agentes.reporte.titulo" />
	</div>
	<div style="display: inline;float: left; margin-top: 30px;">	
		<s:select
			key="midas.mediciones.agentes.reporte.formato"					
		    labelposition="top"
			id="tipo" name="tipo" 
			headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
			list="periodosMap"
			cssClass="txtfield"
		/>
	</div>
	<div id="botonDescargar">
		<midas:boton onclick="javascript: descargarReporte();" tipo="seleccionar" key="midas.mediciones.agentes.reporte.boton" style="width: 200px; display: inline; float: left; margin-left: 130px; margin-top: 60px;"/>
	</div>
	
</s:form>


