<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript">
	
	//<s:url action="buscarAgentes" namespace="/fuerzaventa/mediciones/conf"/>
	//<s:url action="buscarPromotorias" namespace="/fuerzaventa/mediciones/conf"/>
	//<s:url action="mostrar" namespace="/fuerzaventa/mediciones/conf"/>
		
	var urlAgregarAgente = '<s:url action="agregarAgente" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlAgregarPromotoria = '<s:url action="agregarPromotoria" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlAgregarCorreosAdicionales = '<s:url action="agregarCorreosAdicionales" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlEliminarAgente = '<s:url action="eliminarAgente" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlEliminarPromotoria = '<s:url action="eliminarPromotoria" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlHabilitarAgente = '<s:url action="habilitarAgente" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlHabilitarPromotoria = '<s:url action="habilitarPromotoria" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlActualizarAgente = '<s:url action="actualizarAgente" namespace="/fuerzaventa/mediciones/conf"/>';
	var urlActualizarPromotoria = '<s:url action="actualizarPromotoria" namespace="/fuerzaventa/mediciones/conf"/>';
	
	var urlContenedorAyudaAgentes = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/agente"/>';
	var urlSeleccionarAgente=  '<s:url action="seleccionarAgente" namespace="/fuerzaventa/configuracionBono"/>';
	var urlContenedorAyudaPromotorias=  '<s:url action="mostrarContenedor" namespace="/fuerzaventa/promotoria"/>';
	var urlSeleccionarPromotoria = '<s:url action="seleccionarPromotoria" namespace="/fuerzaventa/configuracionBono"/>';
	
	var urlEnviarCorreosReporteSemanal = '<s:url action="enviarCorreosReporteSemanal" namespace="/fuerzaventa/mediciones/conf"/>';
	
</script>
<script src="<s:url value="/js/midas2/agentes/mediciones/configuracion.js"/>" type="text/javascript"></script>
