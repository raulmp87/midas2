<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/catalogos/fuerzaventa/mediciones/conf/confHeader.jsp" />
<s:include value="/jsp/catalogos/catalogosHeader.jsp" />

<s:form id="configMedicionesForm">
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.mediciones.agentes.conf.titulo" />
	</div>
		
	<table class="marco">
	
		<!-- Subtitulo -->
		<tr>
			<td width="50%">
				<div class="subtituloIzquierdaDiv" style="width: 98%;">
					<s:text name="midas.mediciones.agentes.conf.agentes" />
				</div>
			</td>
		    <td>
		    	<div class="subtituloIzquierdaDiv" style="width: 98%;">
					<s:text name="midas.mediciones.agentes.conf.promotores" />
				</div>
			</td>
		</tr>
	
		<!-- Clave agente -->
		<tr>
			<td>
				<!-- Este es el que se usa en los otros ejemplos -->
				<s:textfield id="txtIdAgenteBusqueda" onchange="addAgenteProduccion();" style="display:none;"/>
				<div style="display: inline;float: left;">
					<s:textfield 
						cssClass="txtfield" cssStyle="width: 280px;"
						key="midas.mediciones.agentes.conf.clave"
						labelposition="left" 
						size="280"
						onblur="this.value=jQuery.trim(this.value)"
						id="idAgente" name="configAgente.agente.idAgente" disabled="false"
					/>
				</div>
				<midas:boton onclick="javascript: pantallaModalBusquedaAgente();"  tipo="buscar" style="display: inline; float: left; margin-left: 5px;padding-top: 3px;"/>
				
			</td>
			<td>
				<!-- Este es el que se usa en los otros ejemplos -->
				<s:textfield id="txtIdBeneficiarioPromotoria" onchange="addPromotoriaBeneficiaria();" style="display:none;"/>
				<div style="display: inline;float: left;">
					<s:textfield 
						cssClass="txtfield" cssStyle="width: 280px;"
						key="midas.mediciones.agentes.conf.clave"
						labelposition="left" 
						size="280"
						onblur="this.value=jQuery.trim(this.value)"
						id="idPromotoria" name="configPromotoria.promotoria.idPromotoria" disabled="false"
					/>
				</div>
				<midas:boton onclick="javascript: pantallaModalBusquedaPromotoria();"  tipo="buscar" style="display: inline; float: left; margin-left: 5px;padding-top: 3px;"/>
				
			</td>
		</tr>
	
		<!-- Nombre -->
		<tr>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 280px;"
					key="midas.mediciones.agentes.conf.nombre"
					labelposition="left" 
					size="280"
					onblur="this.value=jQuery.trim(this.value)"
					id="nombreAgente" name="configAgente.agente.persona.nombreCompleto" disabled="true"
				/>
			</td>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 280px;"
					key="midas.mediciones.agentes.conf.nombre"
					labelposition="left" 
					size="280"
					onblur="this.value=jQuery.trim(this.value)"
					id="nombrePromotoria" name="configPromotoria.promotoria.descripcion" disabled="true"
				/>
			</td>
		</tr>
		
		<!-- Correo -->
		<tr>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 280px;"
					key="midas.mediciones.agentes.conf.correo"
					labelposition="left" 
					size="100"
					onblur="this.value=jQuery.trim(this.value)"
					id="emailAgente" name="configAgente.email" 
				/>
			</td>
			<td>
				<s:textfield 
					cssClass="txtfield" cssStyle="width: 280px;"
					key="midas.mediciones.agentes.conf.correo"
					labelposition="left" 
					size="100"
					onblur="this.value=jQuery.trim(this.value)"
					id="emailPromotoria" name="configPromotoria.email" 
				/>
			</td>
		</tr>
		
		<!-- Boton Agregar -->
		<tr>
			<td>
				<div id="botonAgregarAgente">
					<midas:boton onclick="javascript: agregarConfAgente();"  tipo="agregar" style="display: inline; float: left; margin-left: 5px;"/>
				</div>
			</td>
		    <td>
		    	<div id="botonAgregarPromotoria">
					<midas:boton onclick="javascript: agregarConfPromotoria();"  tipo="agregar" style="display: inline; float: left; margin-left: 5px;"/>
				</div>
			</td>
		</tr>
	
		<!-- Grid -->
		<tr>
			<td>
				<s:action name="scrollableGrid" var="confAgenteGrid" executeResult="true" namespace="/componente/scrollable" ignoreContextParams="true">
					<s:param name="name">confAgenteGrid</s:param>
					<s:param name="form">configMedicionesForm</s:param>
					<s:param name="loadAction"><s:url action="buscarAgentes" namespace="/fuerzaventa/mediciones/conf"/></s:param>
					<s:param name="checkJs">habilitarConfAgente_chk</s:param>
					<s:param name="cellChangedJs">actualizarConfAgente</s:param>
					<s:param name="gridColumnsPath">/jsp/catalogos/fuerzaventa/mediciones/conf/columnasAgenteGrid.jsp</s:param>
					<s:param name="gridRowPath">/jsp/catalogos/fuerzaventa/mediciones/conf/registroAgenteGrid.jsp</s:param>
				</s:action>
			</td>
		    <td>
		   		<s:action name="scrollableGrid" var="confPromotoriaGrid" executeResult="true" namespace="/componente/scrollable" ignoreContextParams="true">
					<s:param name="name">confPromotoriaGrid</s:param>
					<s:param name="form">configMedicionesForm</s:param>
					<s:param name="loadAction"><s:url action="buscarPromotorias" namespace="/fuerzaventa/mediciones/conf"/></s:param>
					<s:param name="checkJs">habilitarConfPromotoria_chk</s:param>
					<s:param name="cellChangedJs">actualizarConfPromotoria</s:param>
					<s:param name="gridColumnsPath">/jsp/catalogos/fuerzaventa/mediciones/conf/columnasPromotoriaGrid.jsp</s:param>
					<s:param name="gridRowPath">/jsp/catalogos/fuerzaventa/mediciones/conf/registroPromotoriaGrid.jsp</s:param>
				</s:action>
			</td>
		</tr>
	
	</table>
	
	<div class="subtituloIzquierdaDiv" style="width: 98%;">
		<s:text name="midas.mediciones.agentes.conf.correos.adicionales" />
	</div>
	<div style="display: inline;float: left;">
		<s:textfield 
			cssClass="txtfield" cssStyle="width: 320px;"
			key="midas.mediciones.agentes.conf.correos.adicionales.descripcion"
			labelposition="left" 
			size="100"
			onblur="this.value=jQuery.trim(this.value)"
			id="correosAdicionales" name="correosAdicionales" 
		/>
	</div>
	<midas:boton onclick="javascript: agregarCorreosAdicionales();"  tipo="agregar" key="midas.mediciones.agentes.conf.correos.adicionales.agregar" style="width: 200px; display: inline; float: left; margin-left: 5px;"/>
	
	<div style="display: inline;float: right;">
		<midas:boton onclick="javascript: enviarCorreosReporteSemanal();"  tipo="seleccionar" key="midas.mediciones.agentes.conf.envio.correos.manual" style="width: 300px; display: inline; float: right; margin-left: 5px;"/>
	</div>
	
</s:form>


