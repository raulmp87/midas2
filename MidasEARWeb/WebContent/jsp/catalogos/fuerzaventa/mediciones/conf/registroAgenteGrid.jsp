<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<cell><s:property value="agente.idAgente" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="agente.persona.nombreCompleto" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:property value="email" escapeHtml="false" escapeXml="true" /></cell>
<cell><s:if test="habilitado">1</s:if><s:else>0</s:else></cell>
<cell>../img/icons/check_out.png^Quitar^javascript: eliminarConfAgente(<s:property value="agente.idAgente" escapeHtml="false" escapeXml="true"/>,<s:property value="#action.posStart + #status.index" escapeHtml="false" escapeXml="true"/>)^_self</cell>