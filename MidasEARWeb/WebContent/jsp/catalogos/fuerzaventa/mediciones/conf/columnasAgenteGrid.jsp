<%@  page contentType="text/xml" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<column id="agente.idAgente" type="ro" width="120" sort="server"><s:text name="midas.mediciones.agentes.conf.clave"/></column>
<column id="agente.persona.nombreCompleto" type="ro" width="300" sort="server"><s:text name="midas.mediciones.agentes.conf.nombre"/></column>
<column id="email" type="ed" width="300" sort="server"><s:text name="midas.mediciones.agentes.conf.correo"/></column>
<column id="habilitado" type="ch" width="80" align="center" sort="na"><s:text name="midas.mediciones.agentes.conf.habilitado"/></column>
<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
