<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/agentes/agenteHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="agenteDomicilioForm">
<table width="97%" id="agregar">	
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.negocio.tituloDomicilioPersonal"/>
			</td>
		</tr>	
		<tr>
			<td  width="300px"><s:textfield name="agente.idDomicilioPersonal" id="txtidDomicilio" key="midas.fuerzaventa.negocio.idDomicilio" labelposition="left" ></s:textfield></td>
			<td class= "buscar" >
				<div>
					<div id="b_historico">
						<a href="javascript: void(0);"
							onclick="">
							<s:text name="midas.boton.buscarDomicilio"/>
						</a>
					</div>
				</div>
			</td>	
		</tr>
		<tr>
			<td>
				<s:select key="midas.fuerzaventa.negocio.pais"  name="agente.paisPersonal"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td><s:textfield name="agente.fechaHoraPersonal" id="txtCalleNumeroPersonal" key="midas.fuerzaventa.negocio.calleNumero" labelposition="left" ></s:textfield></td>
		</tr>
		<tr>
			<td>
				<s:select key="midas.fuerzaventa.negocio.estado"  name="agente.estadoPersonal"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td>
				<s:select key="midas.fuerzaventa.negocio.municipio"  name="agente.municipioPersonal"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:select key="midas.fuerzaventa.negocio.colonia"  name="agente.coloniaPersonal"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
		</tr>
	</table>
</s:form>
	