<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/componentes/domicilioHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
 	listarFiltradoDomicilio();
 });
	
</script>
<s:form action="listarFiltrado" id="domicilioForm">
	<table width="890px" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.afianzadora.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="4"><s:textfield name="afianzadora.afianzadora" id="txtAfianzadora" key="midas.agentes.afianzadora" labelposition="left" cssClass="w350"></s:textfield></td>			
			<td colspan="2"><s:textfield name="afianzadora.rfc" id="txtClave" key="midas.fuerzaventa.negocio.rfc" labelposition="left" cssClass="w250"></s:textfield></td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.estado"  name="afianzadora.estado" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>			
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.municipio"  name="afianzadora.municipio" cssClass="cajaTextoM2 w220"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.colonia"  name="afianzadora.colonia" cssClass="cajaTextoM2 w220"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>			 
		</tr>		
		<tr>
			<td colspan="2"><s:textfield name="afianzadora.calleNumero" id="txtCalleNumero" key="midas.fuerzaventa.negocio.calleNumero" labelposition="left" cssClass="w190"></s:textfield></td>
		
			<td colspan="2"><s:textfield name="afianzadora.codigoPostal" id="txtCodigoPostal" key="midas.fuerzaventa.negocio.codigoPostal" labelposition="left" cssClass="w200"></s:textfield></td>
				
			<td colspan="2"  align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>			
	</table>
	
	<div id="afianzadoraGrid" width="890px" height="250px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div align="right" class="w890">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica(verDetalleAfianzadoraPath);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>

