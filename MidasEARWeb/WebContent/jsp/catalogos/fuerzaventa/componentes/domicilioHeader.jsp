<%@ taglib prefix="s" uri="/struts-tags" %>
	
<script src="<s:url value='/js/midas2/catalogos/domicilio.js'/>"></script>
 
<script type="text/javascript">
	var mostrarContenedorDomicilioPath = '<s:url action="mostrarContenedor" namespace="/catalogos/fuerzaventa/componentes"/>';	
	var verDetalleDomicilioPath = '<s:url action="detalle" namespace="/catalogos/fuerzaventa/componentes"/>';
	var listarFiltradoDomicilioPath = '<s:url action="listarFiltrado" namespace="/catalogos/fuerzaventa/componentes"/>';
</script>