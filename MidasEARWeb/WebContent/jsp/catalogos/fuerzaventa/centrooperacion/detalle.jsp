<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script src="<s:url value='/js/midas2/catalogos/centroOperacion.js'/>"></script>
<!-- script type="text/javascript">
	var mostrarDetalleActionPath = '<s:url action="mostrar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var cancelarActionPath = '<s:url action="cancelar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var emitirActionPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';	
	var cotizarActionPath = '<s:url action="cotizar" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';	
</script-->
<script type="text/javascript">
	jQIsRequired();
	var ventanaResponsable=null;
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 900, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			jQuery("#idPersonaResponsable").val(idResponsable);
			jQuery("#nombreResponsable").val(nombre);
		}
	}
</script>
<s:include value="/jsp/catalogos/fuerzaventa/centrooperacion/centroOperacionHeader.jsp"></s:include>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'realizarOperacionGenerica(guardarCentroOperacionPath, document.centroOperacionForm , null);'" />
	<s:set id="readOnly" value="false"/>
	<s:set id="required" value="1"/>
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.centroOperacion.tituloAlta')}"/>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true"/>
	<s:set id="required" value="0"/>
	<s:set id="display" value="false" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.centroOperacion.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'realizarOperacionGenerica(eliminarCentroOperacionPath, document.centroOperacionForm , null);'" />
	<s:set id="readOnly" value="true" />
	<s:set id="required" value="0"/>
	<s:set id="display" value="false" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.centroOperacion.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="tipoAccion" value="4" />
 	<s:set id="readOnly" value="false" />
 	<s:set id="required" value="1"/>
 	<s:set id="display" value="true" />	
 	<s:set id="titulo" value="%{getText('midas.fuerzaventa.centroOperacion.tituloEditar')}"/>
</s:else>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="listarFiltrado" id="centroOperacionForm">
<s:hidden name="polizaId"/>
<s:hidden name="fechaIniVigenciaEndoso"/>
<s:hidden name="accionEndoso"/>
<s:hidden name="tipoEndoso" id="tipoEndoso"/>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="#titulo"/></div>
    </s:if>
	<!--<s:hidden id ="id" name="centroOperacion.id" ></s:hidden>-->
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.catalogos.centro.operacion.datosGenerales"/>
			</td>
		</tr>	
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.idCentroOperacion" /></th>
			<td colspan="3">
				<s:textfield id="idCentroOperacion" readonly="true" name="centroOperacion.id" cssClass="cajaTextoM2 w150"></s:textfield>
				<s:hidden name ="centroOperacion.idCentroOperacion"></s:hidden>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.descripcion" /></th>
			<td colspan="3">
				<s:textfield name="centroOperacion.descripcion" readonly="#readOnly" cssClass="cajaTextoM2 w250 jQrequired"></s:textfield>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.correoElectronico" /></th>
			<td colspan="3">
				<s:textfield name="centroOperacion.correoElectronico" readonly="#readOnly" cssClass="cajaTextoM2 w250 jQrequired jQemail"></s:textfield>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.situacion" /></th>
			<td colspan="3">
				<s:select disabled="#readOnly" name="centroOperacion.claveEstatus" cssClass="cajaTextoM2 jQrequired"
				       list="#{'1':'Activo','0':'Inactivo' }" labelposition="left"  />
			</td>
		</tr>
		<tr>
			<th width="110px" class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.idResponsable" /></th>
			<td width="100px">
				<s:textfield id="idPersonaResponsable" name="centroOperacion.personaResponsable.idPersona" readonly="true" cssClass="cajaTextoM2 w80 jQrequired" onchange="javascript:findByIdResponsable(this.value);"></s:textfield>
			</td>
			<s:if test="tipoAccion==1 || tipoAccion==4">
			<td width="110px">
				<div class="btn_back w110">
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalResponsable();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			</s:if>
			<td>
				<s:textfield id="nombreResponsable"  name="centroOperacion.personaResponsable.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:if test="tipoAccion != 1">
					<div class="btn_back w110">
						<a href="javascript: void(0);" onclick="mostrarVentanaGenerica(mostrarGerenciaPath,'Gerencias', 450, 400 ,document.centroOperacionForm);">
							<s:text name="midas.catalogos.centro.operacion.gerencias"/>
						</a>
					</div>
				</s:if>
			</td>
		</tr>
	</table>
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td colspan="4" class="titulo">
				<s:text name="midas.fuerzaventa.negocio.tituloDomicilio"/>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">centroOperacion.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">centroOperacion.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">centroOperacion.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">centroOperacion.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">centroOperacion.domicilio.calleNumero</s:param>
					<s:param name="cpName">centroOperacion.domicilio.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">centroOperacion.domicilio.nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">centroOperacion.idColoniaCheck</s:param>				
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado"><s:text name="midas.catalogos.centro.operacion.estado"/></s:param>
					<s:param name="labelColonia"><s:text name="midas.catalogos.centro.operacion.colonia"/></s:param>
					<s:param name="labelCodigoPostal"><s:text name="midas.catalogos.centro.operacion.codigoPostal"/></s:param>
					<s:param name="labelCalleNumero"><s:text name="midas.catalogos.centro.operacion.calleYNumero"/></s:param>
					<s:param name="labelCiudad"><s:text name="midas.catalogos.centro.operacion.municipio"/></s:param>
					<s:param name="labelPosicion">top</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="%{#required}"></s:param>
					<s:param name="componente">2</s:param>
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
				</s:action>
			</td>
		</tr>
		<!-- 
		<tr align="right">
			<td colspan="2"></td>
			<td>
				<s:if test="tipoAccion != 2">
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}" cssClass="b_submit icon_guardar w100"/> 
						</div>
					</div>
				</s:if>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_cancelar" onclick="mostrarCatalogoGenerico(mostrarCentroOperacionPath);">
						<s:text name="midas.boton.cancelar"/>
					</a>
				</div>
			</td>
			
		</tr>
		-->
	</table>
	<s:if test="centroOperacion.id != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
		<s:if test="tipoAccion != 5">
		    <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		</s:if>
		<s:else>
		    <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		</s:else>
			
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2" align="right">	
			<s:if test="tipoAccion != 5">								
				<div class="btn_back w110">
					
						<a href="javascript: mostrarHistorico(10,${centroOperacion.id});" class="icon_guardar">	
						<s:text name="midas.boton.historico"/>
						</a>	
					
				</div>	
			</s:if>			
			</td>		
		</tr>		
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	 </s:if>
	<table width="98%" align="center" >
		<tr>
			<td class="inline" align="right">
			<s:if test="tipoAccion != 5">
			
				<div class="btn_back w110">
					<a href="javascript: salirCentroOperacion();" class="icon_regresar"
						onclick="">
						<s:text name="midas.boton.regresar"/>
					</a>
				</div>	
			</s:if>
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="guardarCentroOperacion();">
						<s:text name="midas.boton.guardar"/>
					</a>
				</div>
				</s:if>	
				<s:if test="tipoAccion == 3">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_borrar"
						onclick="realizarOperacionGenerica(eliminarCentroOperacionPath, document.centroOperacionForm , null);">
						<s:text name="midas.boton.borrar"/>
					</a>
				</div>	
				</s:if>
			</td>
		</tr>
	</table>		
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>