<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<sj:head/>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<script type="text/javascript">
	jQuery.noConflict();
	jQuery(function(){
		listarFiltradoGenerico(listarFiltradoBusquedaResponsablePath, 'busquedaResponsableGrid');
	});	
</script>

<s:include value="/jsp/catalogos/fuerzaventa/centrooperacion/centroOperacionHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="busquedaResponsable">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.centro.operacion.titulo"/>
			</td>
		</tr> 
		<tr>
			<td colspan="2">
				<s:textfield name="nombreResponsable" key="midas.catalogos.centro.operacion.nombre" cssClass="" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="razonSocial" id="txtClave" key="midas.catalogos.centro.operacion.razonSocial" labelposition="left"></s:textfield>
			</td>
			<td colspan="2"> 
				<s:textfield name="RFC" id="txtClave" key="midas.catalogos.centro.operacion.rfc" labelposition="left"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.estado"  name="estado" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:select key="midas.catalogos.centro.operacion.municipio" name="municipio" cssClass="cajaTextoM2"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>	 
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.colonia"  name="colonia" cssClass="cajaTextoM"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="calleYNumero" id="txtClave" key="midas.catalogos.centro.operacion.calleYNumero" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="codigoPostal" id="txtClave" key="midas.catalogos.centro.operacion.codigoPostal" labelposition="left"></s:textfield>
			</td>	 
			<td colspan="2">
				<s:textfield name="telefono" id="txtClave" key="midas.fuerzaventa.negocio.telefono" labelposition="left"></s:textfield>
			</td>
		</tr>
	</table>
	
	<div class="dataGridGenericStyle">
		<div id="busquedaResponsableGrid" class="w550 h100"></div>
	</div>
	
	
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>

<div align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="parent.mostrarCatalogoGenerico(mostrarCentroOperacionPath);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator" src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>