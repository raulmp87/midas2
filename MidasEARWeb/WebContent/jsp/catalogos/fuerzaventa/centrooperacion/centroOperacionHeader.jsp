<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/centroOperacion.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript">
	var mostrarCentroOperacionPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/centrooperacion"/>';
	var mostrarBusquedaResponsablePath = '<s:url action="mostrarBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
	var mostrarGerenciaPath = '<s:url action="mostrarGerencias" namespace="/fuerzaventa/centrooperacion"/>';
	
	var verDetalleCentroOperacionPath = '<s:url action="verDetalle" namespace="/fuerzaventa/centrooperacion"/>';
	var guardarCentroOperacionPath = '<s:url action="guardar" namespace="/fuerzaventa/centrooperacion"/>';
	var eliminarCentroOperacionPath = '<s:url action="eliminar" namespace="/fuerzaventa/centrooperacion"/>';
	
	var listarCentroOperacionPath = '<s:url action="lista" namespace="/fuerzaventa/centrooperacion"/>';
	var listarFiltradoCentroOperacionPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/centrooperacion"/>';
	
	var listarBusquedaResponsablePath = '<s:url action="listarBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
	var listarFiltradoBusquedaResponsablePath = '<s:url action="listarFiltradoBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
</script>