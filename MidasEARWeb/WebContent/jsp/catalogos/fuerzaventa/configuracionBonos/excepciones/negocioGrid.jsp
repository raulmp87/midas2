<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
            <call command="enableRowspan"><param>true</param></call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
<%--         	<call command="attachHeader"><param>#rspan,% Bono,Monto Bono,% Bono,Monto Bono,#rspan,#rspan</param></call> --%>
        </afterInit>
		<column id="" type="ro" width="80" sort="int"> id Negocio</column>
		<column id="" type="ro" width="80" sort="int">Negocio</column>
		
		<c:choose>
		  <c:when test="${configuracionBono.tipoBeneficiario == 838}">
		    <column id="" type="ro" width="*" sort="str" align="center">Porcentaje bono Agente</column>
			<column id="" type="ed" width="*" sort="str" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		  <c:when test="${configuracionBono.tipoBeneficiario == 839}">
		    <column id="" type="ed" width="*" sort="str" align="center">Porcentaje bono Agente</column>
			<column id="" type="ro" width="*" sort="str" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		  <c:when test="${configuracionBono.tipoBeneficiario == 840}">
		    <column id="" type="ed" width="*" sort="str" align="center">Porcentaje bono Agente</column>
			<column id="" type="ed" width="*" sort="str" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		    <c:otherwise>
		    	<column id="" type="ro" width="*" sort="str" align="center">Porcentaje bono Agente</column>
				<column id="" type="ro" width="*" sort="str" align="center">Porcentaje bono Promotoría</column>
		  </c:otherwise>
		</c:choose>
		
		<column id="" type="ro" width="80" sort="str">% Desc. Máximo</column>
		<column id="accionBorrar" type="img" width="69" sort="na" align="center">Acciones</column>
		<column id="id" type="ro" width="0" sort="int" hidden="true"></column>
	</head>
	<s:iterator value="listaExcepxionesNegocios" var="rowExcepxionesNegocios" status="index">
	<row id="${index.count}">			
			<cell><![CDATA[${rowExcepxionesNegocios.negocio.idToNegocio}]]></cell>
			<cell><![CDATA[${rowExcepxionesNegocios.negocio.descripcionNegocio} ]]></cell>
			<cell><![CDATA[${rowExcepxionesNegocios.valorMontoPcteAgente}]]></cell>
			<cell><![CDATA[${rowExcepxionesNegocios.valorMontoPctePromotoria}]]></cell>
			<cell><![CDATA[${rowExcepxionesNegocios.descuentoMaximo}]]></cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepNegocio(${index.count})^_self</cell>
			<cell><![CDATA[${rowExcepxionesNegocios.id}]]></cell>			
		</row>
	</s:iterator>
</rows>