<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
            <call command="enableRowspan"><param>true</param></call>  
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
        	<column id="" type="ro" width="0" sort="int"></column>
			<column id="cveAmisIni" type="ed" width="100" sort="str">Clave Amis Inicial</column>
			<column id="cveAmisFin" type="ed" width="100" sort="str" align="center">Clave Amis Final</column>
			<column id="accionBorrar" type="img" width="*" sort="na" align="center"></column>
			
	</head>
	<s:iterator value="listaRangosClaveAmis" var="rowRangoCveAmis" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>			
			<cell><![CDATA[${rowRangoCveAmis.cve_amis_ini}]]></cell>
			<cell><![CDATA[${rowRangoCveAmis.cve_amis_fin}]]></cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowCveAmis(${index.count})^_self</cell>			
		</row>
	</s:iterator>
</rows>