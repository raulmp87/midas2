<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
            <call command="enableRowspan"><param>true</param></call>  
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
			<column id="idcheck" type="ch" width="*" sort="int" align="center">N/A Exepc. % Desc</column>			
			<column id="idAgente" type="ro" width="0" sort="int" hidden="true"></column>
			<column id="nombreAgente" type="ro" width="250" sort="str">Agente</column>
			<column id="moto" type="ed" width="*" sort="str" align="center">Monto/Porcentaje Bono</column>
			<column id="accionBorrar" type="img" width="*" sort="na" align="center">Acciones</column>
			<column id="id" type="ro" width="0" sort="int" hidden="true"></column>
			
	</head>
	<s:iterator value="listaExcepxionesAgentes" var="rowExcepxionesAgentes" status="index">
	<row id="${index.count}">			
			<cell><![CDATA[${rowExcepxionesAgentes.noAplicaDescuentoExcepcion}]]></cell>
			<cell><![CDATA[${rowExcepxionesAgentes.agente.id}]]></cell>
			<cell><![CDATA[${rowExcepxionesAgentes.agente.persona.nombreCompleto} ]]></cell>
			<cell><![CDATA[${rowExcepxionesAgentes.valorMontoPcteAgente}]]></cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepAgente(${index.count})^_self</cell>
			<cell><![CDATA[${rowExcepxionesAgentes.id}]]></cell>			
		</row>
	</s:iterator>
</rows>