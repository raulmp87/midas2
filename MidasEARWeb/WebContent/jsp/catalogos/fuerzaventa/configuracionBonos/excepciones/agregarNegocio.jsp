<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.css'/>"></script> --%>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<style type="text/css">
   ul { height: 100px; overflow: auto; width: 400px; border: 1px solid; border-color : #B2DBB2;
		 list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }
   label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; font-size: 10px;}
   label:hover { background-color: Highlight; color: HighlightText; }
</style>
<script type="text/javascript">
<!--
jQuery(document).ready(function() {
    jQuery("#NegocioAsociado").sortable({
       connectWith : ['#ajax_listaNegDesasoc'],
       update : function(event, ui) {
           if (ui.sender){
        	   var idsRecuperados=ui.item.parent().sortable('toArray');
// 				recuperaIds();				
              }/* else {
				alert(this.id+"->"+ui.item.parent().attr('id'));
           }*/
        }});
    jQuery("#ajax_listaNegDesasoc").sortable({connectWith : ['#NegocioAsociado']});
 });   
 
 function recuperaIds(){
	 var idsRecuperados = "";
	 jQuery("#NegocioAsociado li").each(function(index){
		 idsRecuperados+=jQuery(this).attr("id")+","; 		
	 });
	 alert(idsRecuperados);
 }
//-->
</script>

<table id="filtrosM2" width="97%">
	<tr>
		<td class="titulo" colspan="4">
			<s:text name="midas.fuerzaventa.configBono.buscarNegocio"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.negocio"/>
		</td>
		<td>			
			<s:select name="filtroNegocio.idToNegocio" id ="idNegocio" cssClass="cajaTextoM2 w150" 
			  headerValue="Seleccione.." headerKey="" list="listaNegociosActivos"/>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configBono.estatusNegocio"/>
		</td>
		<td>
			<s:select name="filtroNegocio.claveEstatus" id="claveEstatus" headerValue="Seleccione.." headerKey=""
			list="#{0:'CREADO',1:'ACTIVO',2:'INACTIVO',3:'BORRADO'}" cssClass="cajaTextoM2 w150"/>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="right">
			<div id="botonRep" class="btn_back w80">
				<a href="javascript: void(0)" class="icon_seleccionar" 
					onclick="javascript:llenarListaNegDes();">
					<s:text name="midas.boton.buscar"/>
				</a>
			</div>
		</td>
	</tr>
</table>

<table class="contenedorFormas" width="97%">
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.negAsociadoExcepcion"/>	
		</td>
	</tr>
	<tr>
		<td>
			<ul id="NegocioAsociado">
<%-- 				<s:iterator value="catalogoTipoBono" var="varCatalogoTipoBono" status="stat"> --%>
<!-- 				   	<li> -->
<!-- 				   		 <label for="varCatalogoTipoBono[%{#stat.index}].id"> -->
<%-- 				   		 <input name="varCatalogoTipoBono[${stat.index}]"  --%>
<%-- 				   		 		id="varCatalogoTipoBono${stat.index}"  --%>
<%-- 				   		 		value="${varGerenciaList.id}" validar = "true" --%>
<!-- 								class="js_checked_TipoBono" type="checkbox"> -->
<%-- 				   				${varCatalogoTipoBono.valor} --%>
<!-- 				   		 </label> -->
<!-- 			     	</li> -->
<%-- 				</s:iterator> --%>
			</ul>
		</td>
	</tr>
	<tr>
		<td><br>
			<s:text name="midas.fuerzaventa.configBono.negDesasociadoExcepcion"/>
		</td>
	</tr>
	<tr>
		<td>
			<ul id="ajax_listaNegDesasoc">
<%-- 				<s:iterator value="catalogoTipoBono" var="varCatalogoTipoBono" status="stat"> --%>
<!-- 				   	<li> -->
<!-- 				   		 <label for="varCatalogoTipoBono[%{#stat.index}].id"> -->
<%-- 				   		 <input name="varCatalogoTipoBono[${stat.index}]"  --%>
<%-- 				   		 		id="varCatalogoTipoBono${stat.index}"  --%>
<%-- 				   		 		value="${varGerenciaList.id}" validar = "true" --%>
<!-- 								class="js_checked_TipoBono" type="checkbox"> -->
<%-- 				   				${varCatalogoTipoBono.valor} --%>
<!-- 				   		 </label> -->
<!-- 			     	</li> -->
<%-- 				</s:iterator> --%>
			</ul>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.arrastreRegistroAgregar"/>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<div id="botonRep" class="btn_back w80" >
				<a href="javascript: void(0)" class="icon_seleccionar" 
					onclick="javascript:llenarGridNegocio();">
					<s:text name="midas.boton.aceptar"/>
				</a>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div id="botonRep" class="btn_back w80" >
				<a href="javascript: void(0)" class="icon_seleccionar" 
					onclick="javascript:closeDhtmlxWindows('closeModalNegocioAsoc');">
					<s:text name="midas.boton.atras"/>
				</a>
			</div>
		</td>
	</tr>
</table>