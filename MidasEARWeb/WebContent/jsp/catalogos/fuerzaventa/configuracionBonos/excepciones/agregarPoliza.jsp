<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/configuracionBono.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/cargarListado.js'/>"></script>
<script type="text/javascript">
jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarPolizasExcepcionesPath;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro + "?onlyHeaders=true","listarPolizaGrid", null,idField,null);
	 
	 jQuery("#buscarPolizasExcepcionesBtn").click(function() {
	 	var numeroPoliza = jQuery.trim(jQuery("#filtrarPoliza_obtenerIdPoliza").val());
	 	var numeroPolizaSeycos = jQuery.trim(jQuery("#filtrarPoliza_numeroPolizaSeycos").val());
		if (numeroPoliza || numeroPolizaSeycos) {
			var idField = '<s:property value="idField"/>';
			listarFiltradoGenerico(listarPolizasExcepcionesPath,'listarPolizaGrid',document.filtrarPoliza, idField,null);
		} else {
			alert("Llene el campo para poder buscar.");
		}
	});
});
</script>
<s:form id="filtrarPoliza" name="filtrarPoliza">
<s:hidden name="configuracionBono.id" id="configuracionBono_id"/>
<table id="filtrosM2" width="97%">
	<tr>
		<td>
			<s:text name="midas.poliza.numeroPoliza"/>
		</td>
		<td>
			<s:textfield name="obtenerIdPoliza" cssClass="cajaTextoM2 jQrestrict jQnumeric"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.poliza.numeroPolizaSeycos"/>
		</td>
		<td>
			<s:textfield name="numeroPolizaSeycos" cssClass="cajaTextoM2 jQrestrict jQnumeric"/>
		</td>
	</tr>
	<tr>
		<td>
			<div id="botonRep" class="btn_back w80">
				<a id="buscarPolizasExcepcionesBtn" href="javascript: void(0)" class="icon_seleccionar">
					<s:text name="midas.boton.buscar"/>
				</a>
			</div>
		</td>
	</tr>
</table>

<table class="contenedorFormas" width="97%">
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.listaPoliza"/>
		</td>
	</tr>
	<tr>
		<td>
			<div id="listarPolizaGrid" class="w670 h200" style="overflow:hidden">
			</div>
		</td>
	</tr>
</table>
<table>
<!-- 	<tr> -->
<!-- 		<td> -->
<!-- 			<div id="botonRep" class="btn_back w80" > -->
<!-- 				<a href="javascript: void(0)" class="icon_seleccionar"  -->
<!-- 					onclick="javascript:llenarGridPoliza();"> -->
<%-- 					<s:text name="midas.boton.aceptar"/> --%>
<!-- 				</a> -->
<!-- 			</div> -->
<!-- 		</td> -->
<!-- 	</tr> -->
	<tr>
		<td>
			<div id="botonRep" class="btn_back w80" >
				<a href="javascript: void(0)" class="icon_seleccionar" 
					onclick="javascript:closeDhtmlxWindows('modalPoliza');">
					<s:text name="midas.boton.atras"/>
				</a>
			</div>
		</td>
	</tr>
</table>
</s:form>