<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
            <call command="enableRowspan"><param>true</param></call>      
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
	
		<column id="" type="ro" hidden="true" width="80" sort="int"></column>
		<column id="" type="ro" width="240" sort="str">Folio Póliza</column>
<%-- 		<column id="" type="ro" width="150" sort="str">Folio Póliza Seycos</column> --%>
		<column id="" type="ed" width="220" sort="str" align="left">Agente</column>
		<column id="" type="ed" width="220" sort="str" align="center">Promotoría</column>
	</head>
	<s:iterator value="listaPolizas" var="polizaX" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${polizaX.idPoliza}]]></cell>
<%-- 			<cell><![CDATA[${polizaX.numeroPolizaMidasString}]]></cell> --%>
			<cell><![CDATA[${polizaX.numeroPolizaSeycosString}]]></cell>						
			<cell><![CDATA[${polizaX.nombreCompleto}]]></cell>
			<cell><![CDATA[${polizaX.descripcionPromotoria}]]></cell>
		</row>
	</s:iterator>
</rows>