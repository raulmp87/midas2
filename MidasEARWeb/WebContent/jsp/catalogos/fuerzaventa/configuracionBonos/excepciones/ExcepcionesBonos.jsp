<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<script type="text/javascript">
<!--
cargarGridPoliza();
cargarGridNegocio();
cargarGridAgente();
cargarGridCveAmis();
//desavilitarEdisionSegunBenef();
//-->
</script>
<style type="text/css">
   ul { height: 100px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
		 list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }
   label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; font-size: 10px;}
   label:hover { background-color: Highlight; color: HighlightText; }
</style>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		enableCheckBox();
	</script>
</s:if>

<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloEditar')}"/>
</s:if>
<s:form id="excepcionesForm" name="excepcionesForm">
	<s:hidden name="configuracionBono.id" id="configuracionBono_id"/>
	<s:hidden name = "tabActiva"  value="excepciones"/>
	<s:hidden name="configuracionBono.tipoBeneficiario" id="configuracionBono_benef"/> 
	<table class="contenedorFormas" width="97%">
		<tr>
			<td colspan="3">
				<s:text name="midas.fuerzaventa.configBono.DescripcionBono"/>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th colspan="4" class="titulo">
				<s:text name="midas.fuerzaventa.configBono.exclusiones"/>
			</th>
		</tr>
		<tr>
			<td class="w200">
				<s:text name ="midas.fuerzaventa.configBono.poliza"/>:
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.descMayorIgualA"/>
			</td>
			<td class="w200">
				<s:textfield name="configuracionBono.conDescuentoMayorIgual" id="descuentoPoliza" cssClass="cajaTextoM2 w50" disabled="#readOnly" onchange="checarTodasPolizas()"/>
			</td>
			<td class="w30" align="right">
			<s:checkbox name="configuracionBono.TodasPolizas" id="todasPolizas" cssClass="cajaTextoM2"  disabled="true"/>
			</td>
			<td class="w300">
				<s:text name="Todas las Pólizas"/>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.configBono.negocios"/>:
			</th>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.contratadoSobreComision"/>
			 </td>
			 <td>
			 	<s:checkbox name="configuracionBono.ContratadoConSobreComision" id="contratadoConSobreComision" cssClass="cajaTextoM2" disabled="#readOnly"/>
			 </td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.configBono.agentes"/>:
			</th>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.tipoPersona"/>
			</td>
			<td>
				<s:select name="configuracionBono.tipoPersona" cssClass="cajaTextoM2 w100" id="tipoPers"
				disabled="#readOnly" onclick="javascript:tipoPersonaEnableDisable(jQuery('#tipoPers').val());"
				onchange="show_hide_BtnGenerarRFC();javascript:clearRfc();" 
				list="#{'':'seleccione...','1':'FISICA', '2':'MORAL'}"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.tipoCedula"/>
			</td>
			<td>
				<ul id ="tipoCedula">
					<s:iterator value="catalogoTipocedula" var="varcatalogoTipocedula" status="stat">
					   	<li>
					   		 <label for="varcatalogoTipocedula[%{#stat.index}]">
					   		 <input name="varcatalogoTipocedula[${stat.index}]" 
					   		 		id="varcatalogoTipocedula${stat.index}" 
					   		 		value="${varcatalogoTipocedula.id}" validar = "true"
					   		 		class="js_checked_TipoCedula js_checkEnable" type="checkbox">
					   				${varcatalogoTipocedula.valor}
					   		 </label>
				     	</li>
					</s:iterator>
				</ul>
				<s:iterator value="listaBusquedaCedula" status="status">
					<s:iterator value="catalogoTipocedula" var="varcatalogoTipocedula" status="stat">
						<s:if test="%{#varcatalogoTipocedula.id == listaBusquedaCedula[#status.index].idTipoBono.id}">
							<script type="text/javascript">
								checarChec('varcatalogoTipocedula${stat.index}');
							</script>
				    	</s:if>
				    </s:iterator>		
				</s:iterator>	      
			</td>
			<s:if test="tipoAccion != 2">
				<td colspan="2">
					<div>
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="javascript:limpiarChec('js_checked_TipoCedula');">
								<s:text name="midas.suscripcion.solicitud.solicitudPoliza.limpiar"/>
							</a>
						</div>	
					</div>
					<br>
					<br>
					<!-- Boton de prueba -->
<!-- 					<div> -->
<!-- 						<div class="btn_back w110"> -->
<!-- 							<a href="javascript: void(0);" class="icon_guardar" -->
<!-- 								onclick="javascript:saveConfigBonoExcluTipoCedula();"> -->
<%-- 								<s:text name="Guardar"/> --%>
<!-- 							</a> -->
<!-- 						</div>	 -->
<!-- 					</div>		 -->
				</td>
			</s:if>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.frchaVencimientoCedula"/>
			</td>
			<td>
				<sj:datepicker name="configuracionBono.fechaVencimientoCedula" id="txtfechaVencimientoCedula" buttonImage="../img/b_calendario.gif" 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 		       onblur="esFechaValida(this);" disabled="#readOnly"></sj:datepicker> 
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.diasAntesVencimiento"/>
			</td>
			<td>
				<s:textfield name="configuracionBono.diasAntesVencimiento" cssClass="w50 cajaTextoM2" disabled="#readOnly"/> 
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.tipoBono"/>
			</td>
			<td>
				<ul id="tipoBono">
					<s:iterator value="listaBonoEclusion" var="varCatalogoTipoBono" status="stat">
						<s:if test="%{#varCatalogoTipoBono.id!=configuracionBono.id}">
						   	<li>
						   		 <label for="varCatalogoTipoBono[%{#stat.index}].id">
						   		 <input name="CatalogoTipoBono[${stat.index}]" 
						   		 		id="varCatalogoTipoBono${stat.index}" 
						   		 		value="${varCatalogoTipoBono.id}" validar = "true"
										class="js_checked_TipoBono js_guardarBono js_checkEnable" type="checkbox">
						   				${varCatalogoTipoBono.descripcion}
						   		 </label>
					     	</li>
				     	</s:if>
					</s:iterator>
				</ul>
				<s:iterator value="listaBusquedaBono" status="status">
					<s:iterator value="listaBonoEclusion" var="varCatalogoTipoBono" status="stat">
						<s:if test="%{#varCatalogoTipoBono.id == listaBusquedaBono[#status.index].idBono.id}">
							<script type="text/javascript">
								checarChec('varCatalogoTipoBono${stat.index}');
							</script>
				    	</s:if>
				    </s:iterator>		
				</s:iterator>	  
			</td>
			<s:if test="tipoAccion != 2">
				<td colspan="2">
					<div>
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="javascript:limpiarChec('js_checked_TipoBono');">
								<s:text name="midas.suscripcion.solicitud.solicitudPoliza.limpiar"/>
							</a>
						</div>	
					</div>
					<br><br>
					<!-- Boton de prueba -->
<!-- 					<div> -->
<!-- 						<div class="btn_back w110"> -->
<!-- 							<a href="javascript: void(0);" class="icon_guardar" -->
<!-- 								onclick="javascript:saveConfigBonoExcluTipoBono();"> -->
<%-- 								<s:text name="midas.suscripcion.solicitud.solicitudPoliza.limpiar"/> --%>
<!-- 							</a> -->
<!-- 						</div>	 -->
<!-- 					</div>		 -->
				</td>
			</s:if>
		</tr>
		<tr>
			<td>
				<s:text name="Clave AMIS (Pólizas Autos)"/>
			</td>
			<td><div id="gridCveAmis" class="w240 h150" style="overflow:hidden"></div></td>
			<td>
					<div>
						<div class="btn_back w110">
							<a href="javascript: void(0);" class=""
								onclick="javascript:add_RowCveAmis();">
								<s:text name="Agregar"/>
							</a>
						</div>	
					</div>
				</td>	
		</tr>
		<tr>
			<td>
				<s:text name="Polizas (Autos) con incisos mayor igual a "/>
			</td>
			<td class="w200">
				<s:textfield name="configuracionBono.conIncisosMayorIgual" id="conIncisosMayorIgual" cssClass="cajaTextoM2 w50" disabled="#readOnly" onchange=""/>				
			</td>
		</tr>		
	</table>
	
	<!--Inicia seccion Excepsiones -->
	<table class="contenedorFormas" width="97%">
		<tr>
			<td colspan="3" class="titulo">
				<s:text name="midas.fuerzaventa.configBono.excepciones"/>
			</td>
			
		</tr>
		<tr>
			<td valign="top">
				<s:text name="midas.fuerzaventa.configBono.poliza"/>
			</td>
			<s:if test="isCheckedImporte != 1">
				<s:if test="tipoAccion != 2 ">
				<td valign="top">
				<s:textfield name="txt_IdPoliza" id="txt_IdPoliza" onchange="addPolizaExcepciones();" style="display:none"></s:textfield>
					<div id="botonRep" class="btn_back w80">
						<a href="javascript: void(0)" class="icon_seleccionar" 
							onclick="mostrarModalPoliza();">
							<s:text name="midas.boton.seleccionar"/>
						</a>
					</div>
	<!-- 				<div  class="btn_back w80"> -->
	<!-- 					<a href="javascript: void(0)" class="icon_guardar"  -->
	<!-- 						onclick="guardarExcepcionPoliza();"> -->
	<%-- 						<s:text name="midas.boton.guardar"/> --%>
	<!-- 					</a> -->
	<!-- 				</div> -->
				</td>
				</s:if>
			</s:if>
			<s:else>
				<td>&nbsp;</td>
			</s:else>
			<td>
				<div id="gridPoliza" class="w630 h150" style="overflow:hidden">
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.negocio"/>
			</td>
			<s:if test="isCheckedImporte != 1">
				<s:if test="tipoAccion != 2">
				<td valign="top">
					<div id="botonRep" class="btn_back w80">
						<a href="javascript: void(0)" class="icon_seleccionar" 
							onclick="mostrarModalNegocio();">
							<s:text name="midas.boton.seleccionar"/>
						</a>
					</div>
	<!-- 				<div  class="btn_back w80"> -->
	<!-- 					<a href="javascript: void(0)" class="icon_guardar"  -->
	<!-- 						onclick="guardarNegocioExcepcion();"> -->
	<%-- 						<s:text name="midas.boton.guardar"/> --%>
	<!-- 					</a> -->
	<!-- 				</div> -->
				</td>
				</s:if>
			</s:if>
			<s:else>
				<td>&nbsp;</td>
			</s:else>
			<td>
				<div id="negocioGrid" class="w550 h150" style="overflow:hidden">
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<s:text name="midas.fuerzaventa.configBono.agente"/>
				<s:textfield  id="txtIdAgente" onchange="javascript:llenarGridAgengte(this.value);"  style="display: none"/>
			</td>
			<s:if test="tipoAccion != 2">
			<td valign="top">
				<div id="botonRep" class="btn_back w80">
					<a href="javascript: void(0)" class="icon_seleccionar" 
						onclick="mostrarModalAgente();">
						<s:text name="midas.boton.seleccionar"/>
					</a>
				</div>
<!-- 				<div id="botonRep" class="btn_back w80"> -->
<!-- 					<a href="javascript: void(0)" class="icon_guardar"  -->
<!-- 						onclick="guardarAgenteExcepcion();"> -->
<%-- 						<s:text name="midas.boton.guardar"/> --%>
<!-- 					</a> -->
<!-- 				</div> -->
			</td>
			</s:if>
			<td>
				<div id="gridAgente" class="w700 h150" style="overflow:hidden">
				</div>
			</td>
		</tr>
	</table>
			<!--Termina seccion Excepsiones -->
			
	<table class="contenedorFormas" width="97%">
		<tr>
			<s:if test="tipoAccion != 2">
				<td align="right">
					<div id="botonRep" class="btn_back w90">
						<a href="javascript: void(0)" class="icon_seleccionar" 
							onclick="javascript:grabarYcopiar();">
							<s:text name="midas.boton.grabarCopiar"/>
						</a>
					</div>
				</td>
				<td class="w150" align="right">
					<div id="botonRep" class="btn_back w80">
						<a href="javascript: void(0)" class="icon_seleccionar" 
							onclick="validaGridAmisAndSaveExcepcion();"><!-- guardarExcepcion() -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
				</td>
			</s:if>
			<td class="w150" align="right"> 
				<div id="botonRep" class="btn_back w80" >
					<a href="javascript: void(0)" class="icon_seleccionar" 
						onclick="javascript:atrasOSiguiente('configuracionBonos');">
						<s:text name="midas.boton.atras"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
</s:form>
<script type="text/javascript">
<!--
//desavilitarEdisionSegunBenef();
//-->
</script>