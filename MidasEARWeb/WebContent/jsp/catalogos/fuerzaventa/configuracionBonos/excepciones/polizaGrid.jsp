<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set name="configuracionBono.tipoBeneficiario" value="benef"/>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
            <call command="enableRowspan"><param>true</param></call>      
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
<%--         	<call command="attachHeader"><param>#rspan,#rspan,% Bono,Monto Bono,% Bono,Monto Bono,#rspan,#rspan</param></call> --%>
        </afterInit>
	
		<column id="polizaExce" type="ch" width="*">N/A Exepc. % Desc</column>
		<column id="idPoliza" type="ro" width="0" sort="int" hidden="true">idPoliza</column>
		<column id="poliza" type="ro" width="*" sort="int"> Póliza</column>
		<c:choose>
		  <c:when test="${configuracionBono.tipoBeneficiario == 838}">
		    <column id="porBonoAgt" type="ro" width="*" sort="int">Porcentaje bono Agente</column>
			<column id="porBonoProm" type="ed" width="*" sort="int" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		  <c:when test="${configuracionBono.tipoBeneficiario == 839}">
		    <column id="porBonoAgt" type="ed" width="*" sort="int">Porcentaje bono Agente</column>
			<column id="porBonoProm" type="ro" width="*" sort="int" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		  <c:when test="${configuracionBono.tipoBeneficiario == 840}">
		    <column id="porBonoAgt" type="ed" width="*" sort="int">Porcentaje bono Agente</column>
			<column id="porBonoProm" type="ed" width="*" sort="int" align="center">Porcentaje bono Promotoría</column>
		  </c:when>
		    <c:otherwise>
		    	<column id="porBonoAgt" type="ro" width="*" sort="int">Porcentaje bono Agente</column>
				<column id="porBonoProm" type="ro" width="*" sort="int" align="center">Porcentaje bono Promotoría</column>
		  </c:otherwise>
		</c:choose>
		<column id="descPol" type="ro" width="*" sort="int"> % Desc. Póliza</column>
		<column id="accionBorrar" type="img" width="69" sort="na" align="center">Acciones</column>
		<column id="id" type="ro" width="0" sort="int" hidden="true"></column>
	</head>
	
	
	<s:iterator value="listabonoExcepcionPoliza" var="rowExcepcionPoliza" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowExcepcionPoliza.noAplicaDescuentoExcepcion}]]></cell>
			<cell><![CDATA[${rowExcepcionPoliza.idCotizacion}]]></cell>
			<cell><![CDATA[${rowExcepcionPoliza.polizaFormateada} ]]></cell>
			<cell><![CDATA[${rowExcepcionPoliza.valorMontoPcteAgente}]]></cell>
			<cell><![CDATA[${rowExcepcionPoliza.valorMontoPctePromotoria} ]]></cell>
			<cell><![CDATA[${rowExcepcionPoliza.descuentoPoliza}]]></cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowExcepPoliza(${index.count})^_self</cell>
			<cell><![CDATA[${rowExcepcionPoliza.idExcepcionPoliza}]]></cell>			
		</row>
	</s:iterator>
</rows>