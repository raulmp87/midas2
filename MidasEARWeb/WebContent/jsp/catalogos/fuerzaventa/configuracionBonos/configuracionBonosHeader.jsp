<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/configuracionBono.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/cargarListado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
 <script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
 
<script type="text/javascript">
	var mostrarContenedorConfiguracionBonosPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/configuracionBono"/>';	
	var listarConfiguracionBonosPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/configuracionBono"/>';
	var verDetalleConfiguracionBonosPath = '<s:url action="verDetalle" namespace="/fuerzaventa/configuracionBono"/>';	
	
	var verExcepcionesPath = '<s:url action="verExcepcion" namespace="/fuerzaventa/configuracionBono"/>';
	var verDetallePath = '<s:url action="verDetalleBono" namespace="/fuerzaventa/configuracionBono"/>';
	var listarPolizasExcepcionesPath = '<s:url action="listarPolizas" namespace="/fuerzaventa/configuracionBono"/>';
	
	var verPreviewBonosPath = '<s:url action="verPreviewBonos" namespace="/fuerzaventa/calculos/bonos"/>';
	var verPreviewBonosGridPath = '<s:url action="previewBonosGrid" namespace="/fuerzaventa/calculos/bonos"/>';
	
	var listarAgentes = '<s:url action="obtenerAgentesXConfiguracion" namespace="/fuerzaventa/configuracionBono"/>';
	
</script>
