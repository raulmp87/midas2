<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>false</param>
				<param>100</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>false</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        	<call command="attachHeader"><param>Num.,De,Hasta,% Inicial,%Final,% Inicial,%Final,% Inicial,%Final,De,Hasta,#rspan</param></call>
        </afterInit>
        <column id="" type="ro" width="100" sort="int"></column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">PRIMA</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">#cspan</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">% SINIESTRALIDAD</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">#cspan</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">% CRECIMIENTO</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">#cspan</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">%SUPERACION DE META</column>
		<column format="0,000.00" id="" type="edn" width="100" sort="int">#cspan</column>
		<column format="0,000" id="" type="edn" width="200" sort="int">No. POLIZAS EMITIDAS PAGADAS</column>
		<column format="0,000" id="" type="edn" width="100" sort="int">#cspan</column>
		<column format="0,000.00" id="" type="edn" width="120" sort="int">BONIFICACION</column>		
		<column id="accionEliminar" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="idRecord" type="ro" hidden="true" width="100" sort="int"></column>

	</head>
	<s:iterator value="listRangoFacotres" var="rowRangoFacotres" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.importePrimaInicial}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.importePrimaFinal}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctSiniestralidadInicial}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctSiniestralidadFinal}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctCrecimientoInicial}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctCrecimientoFinal}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctSupMetaInicial}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.pctSupMetaFinal}]]></cell>
			<cell><![CDATA[${rowRangoFacotres.numPolizasEmitidasInicial}]]></cell>	
			<cell><![CDATA[${rowRangoFacotres.numPolizasEmitidasFinal}]]></cell>	
			<cell><![CDATA[${rowRangoFacotres.valorAplicacion}]]></cell>		
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: delete_rowRangoFactor(${index.count})^_self</cell>
			<cell><![CDATA[${rowRangoFacotres.id}]]></cell>
 		</row> 
	</s:iterator>
</rows>