<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarConfiguracionBonosPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"configuracionBonosGrid", null,idField,'configuracionBonosModal');
 });
	
</script>
<s:form action="listarFiltrado" id="configuracionBonosForm" name="configuracionBonosForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.agentes.configBono.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th ><s:text name="midas.agentes.configBono.descripcionBono" /></th>
			<td>
				<s:textfield id="idDescripcionBono" name="filtroConfigBono.descripcion" cssClass="cajaTextoM2 w200 "/>
			</td>
			<th ><s:text name="midas.fuerzaventa.negocio.promotoria" /></th>
			<td>
				<s:select id="idPromotoria" name="filtroConfigBono.idPromotoria" 
				list="listaPromotorias" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200 "/>
			</td>	
			<th ><s:text name="midas.negocio.agente" /></th>
			<td>
				<s:textfield name="filtroConfigBono.nombreAgente" cssClass="cajaTextoM2 w200 "></s:textfield>
			</td>						
		</tr>			
		<tr>
			<th ><s:text name="midas.catalogos.centro.operacion.centroDeOperacion" /></th>
			<td>
				<s:select id="idcentroOperacion" name="filtroConfigBono.idCentroOperacion" 
				list="listaCentroOperacion" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200 "/>	
			</td>
			<th ><s:text name="midas.fuerzaventa.configBono.tipoPromotoria" /></th>
			<td>
				<s:select id="tiposPromotoria" name="filtroConfigBono.idTipoPromotoria" 
				list="listaTiposPromotoria" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200"/>
			</td>
			<th ><s:text name="midas.fuerzaventa.negocio.clasificacionAgente" /></th>
			<td>
				<s:select id="idTipoAgente" name="filtroConfigBono.idTipoAgente" 
				list="listaTiposAgente" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/>	
			</td>
		</tr>
		<tr>
			<th ><s:text name="midas.fuerzaventa.configBono.gerencia" /></th>
			<td>
				<s:select id="idGerencia" name="filtroConfigBono.idGerencia" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				 cssClass="cajaTextoM2 w200"/>
			</td>
			<th ><s:text name="midas.fuerzaventa.configBono.producto" /></th>
			<td>
				<s:select id="idProducto" name="filtroConfigBono.idProducto"
				list="listaProductos" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/>
			</td>
			<th ><s:text name="midas.fuerzaventa.negocio.prioridad" /></th>
			<td>
				<s:select id="idPrioridad" name="filtroConfigBono.idProridad" 
				list="listaPrioridad" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/>
			</td>
		</tr>
		<tr>
			<th ><s:text name="midas.agentes.configBono.ejecutivo" /></th>
			<td>
				<s:select id="idEjecutivo" name="filtroConfigBono.idEjecutivo"
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				 cssClass="cajaTextoM2 w200 "/>
			</td>
			<th ><s:text name="midas.fuerzaventa.configBono.ramo" /></th>
			<td>
				<s:select id="idRamo" name="filtroConfigBono.idRamo"
				list="listaRamos" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/>
			</td>
<%-- 			<th ><s:text name="midas.fuerzaventa.configBono.periodoEjecucion" /></th> --%>
<!-- 			<td> -->
<%-- 				<s:select id="idPeriodoEjecucion" name="filtroConfigBono.periodoAjuste.id" --%>
<!-- 				list="listaPeriodoEjecucion" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"  -->
<!-- 				listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/> -->
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
			<th>&nbsp;<s:text name="midas.fuerzaventa.fechaInicio"/></th>	
			<td><sj:datepicker name="filtroConfigBono.fechaAltaInicio" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 		</tr>
		<tr>
 			<th><s:text name="midas.fuerzaventa.fechaFin"/></th>
			<td><sj:datepicker name="filtroConfigBono.fechaAltaFin" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 			<th ><s:text name="midas.fuerzaventa.configBono.poliza" /></th>
			<td>
				<s:textfield name="filtroConfigBono.idPoliza" cssClass="cajaTextoM2 w200 "></s:textfield>
			</td>
		</tr>
		<tr>
			<td>&nbsp;
<!-- 				<div style="display: block" id="masFiltros"> -->
<!-- 					<a href="javascript: void(0);" -->
<!-- 						onclick="toggle_Hidden();ocultarMostrarBoton('masFiltros');"> -->
<%-- 						<s:text name="midas.boton.masFiltros"/> --%>
<!-- 					</a> -->
<!-- 				</div> -->
<!-- 				<div style="display:none" id="menosFiltros"> -->
<!-- 					<a href="javascript: void(0);" -->
<!-- 						onclick="toggle_Hidden();ocultarMostrarBoton('menosFiltros');"> -->
<%-- 						<s:text name="midas.boton.menosFiltros"/> --%>
<!-- 					</a> -->
<!-- 				</div> -->
			</td>
			<td colspan="6" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarConfiguracionBonosPath, 'configuracionBonosGrid', document.configuracionBonosForm,'${idField}','configuracionBonosModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="configuracionBonosGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:irDetalleBonos(1);"> <!--//irDetalleBonos(1); ***   javascript:operacionGenerica(verDetalleConfiguracionBonosPath,1);   -->
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>