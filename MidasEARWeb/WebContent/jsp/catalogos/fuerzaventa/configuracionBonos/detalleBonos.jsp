<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<s:hidden name="isCheckedImporte" id ="isCheckImporte"/>
<s:hidden name="tabActiva"/>
<s:hidden name="tipoAccion"/>
<s:hidden name = "configuracionBono.id" id="configuracionBono.id"/>
<script type="text/javascript">
	jQuery(function(){
		dhx_init_tabbars();
		incializarTabs(); 
	});
</script>

<div select="<s:property value="tabActiva"/>" hrefmode="ajax-html" style="height: 380px;width:910px;margin-left:10px;" id="configuracionBonoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="configuracionBonos" name="Config. Bono" href="http://void" extraAction="javascript: verConfiguracionBono();"></div>
	<div width="100%" id="excepciones" name="Excepciones" href="http://void" extraAction="javascript: verExcepciones();"></div>
</div>

<table width="910px" class="contenedorFormas" align="center">	
		<tr>
		<s:if test="configuracionBono.id != null">				
			<td ><s:textfield name="ultimaModificacion.fechaHoraActualizacionString" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2" align="right">							
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="mostrarHistoricoConfigBono();">
						<s:text name="midas.boton.historico"/>
					</a>
				</div>				
			</td>		
		</s:if>	
			<td align="right">
				<div class="btn_back w110">
					<a href="javascript: salirConfigBono(); " class="icon_regresar"
					onclick="">
					<s:text name="midas.boton.regresar"/>
					</a>
				</div>
			</td>
		</tr>		
</table>	