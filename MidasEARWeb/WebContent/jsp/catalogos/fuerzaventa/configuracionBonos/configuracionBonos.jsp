<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>

<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
</style>        
<script src="<s:url value='/js/midas2/catalogos/configuracionBono/configuracionBono.js'/>"></script>
<script type="text/javascript">
activaFechasSiEsActivo();
activaCamposSiEsAjusteOficina();
activaSiEsGlobal();
checkSegunElTipoModoAplicacion();
jQIsRequired();
// existeCrecimiento();
activaBtnSeleccionarPromoBenef();
activaBtnSeleccionarAgtBenef();
if(jQuery("#todosAgentesBoolean").is(':checked')==true || jQuery("#listaBeneficiariosAgentes").find('li').length!=0){
	jQuery("#benefAgente").attr("checked","checked");	
}
if(jQuery("#todasPromotoriasBoolean").is(':checked')==true || jQuery("#listaPromotoriasBeneficiarias").find('li').length!=0){
	jQuery("#benefPromotoria").attr("checked","checked");
}


</script>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQuery(document).ready(function(){
			enableCheckBox();
		});
		
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloEditar')}"/>
	<script type="text/javascript">
		
	</script>
</s:if>

<s:form action="agregar" id="configuracionBonoForm" name="configuracionBonoForm">
<s:hidden name="tabActiva" id="tabActiva" value="configuracionBonos"/> 
<s:hidden name="configuracionBono.id" id="configuracionBono_id"/>
<table class="contenedorFormas w910">
	<tr>
		<td colspan="6" class="titulo">
			<s:text name="midas.fuerzaventa.configBono.caracteristicasBono"></s:text>
		</td>
	</tr>		
	<tr>
		<th width="160px" class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.descripcionBono"/></th>
		<td colspan="3"><s:textfield name="configuracionBono.descripcion" id="txtDescripcion" cssClass="w260 cajaTextoM2 jQrequired" disabled="#readOnly" /></td>
	
		<th width="110px" class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.tipoBono" /></th>
		<td ><s:select name="configuracionBono.tipoBono.id" headerKey="" headerValue="Seleccione.."  list="listaBonos"
			listKey="id" listValue="valor" lilabelposition="left" cssClass="cajaTextoM2 w150 jQrequired" disabled="#readOnly"/></td>
	</tr>
	<!--  -->
	<tr>
		<th width="160px"><s:text name="midas.fuerzaventa.configBono.nivelSiniestralidadBono"/></th>
		<td colspan="3">
			<s:select id="idNivelSiniestralidad" name="configuracionBono.idNivelSiniestralidad" headerKey="" headerValue="Seleccione.."  list="listaNivelSiniestralidad"
			listKey="id" listValue="valor" lilabelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly"/>
		</td>
		<th width="110px" class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.subtipoBono" /></th>
		<td>
			<s:select id="idSubtipoBono" name="configuracionBono.idSubtipoBono.id" headerKey="" headerValue="Seleccione.."  list="listaSubtpoBono"
			listKey="id" listValue="valor" lilabelposition="left" cssClass="cajaTextoM2 w150 jQrequired" disabled="#readOnly"/>
		</td>
	</tr>
	<tr>
		<th width="160px"><s:text name="Nivel de Productividad"/></th>
		<td colspan="3">
			<s:select id="idNivelProductividad" name="configuracionBono.idNivelProductividad" headerKey="" headerValue="Seleccione.."  list="listaNivelProductividad"
			listKey="id" listValue="valor" lilabelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly"/>
		</td>
		<th width="110px"><s:text name="Nivel de Crecimiento" /></th>
		<td>
			<s:select id="idNivelCrecimiento" name="configuracionBono.idNivelCrecimiento" headerKey="" headerValue="Seleccione.."  list="listaNivelCrecimiento"
			listKey="id" listValue="valor" lilabelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly"/>
		</td>
	</tr>
	<!--  -->
	<tr>	
		<td><s:checkbox name="configuracionBono.pagoSinFacturaBoolean"  key="midas.fuerzaventa.configBono.pagarSinFactura" labelposition="right" disabled="#readOnly"/></td>
		<td><s:checkbox name="configuracionBono.activoBoolean" id="chkActivo" key="midas.fuerzaventa.configBono.activo" labelposition="right" disabled="#readOnly"/></td>
		
		<th><s:text name="midas.fuerzaventa.configBono.inicioVigencia" /></th>
		<td id="js_inicioVigencia1">
			<s:textfield name="configuracionBono.fechaInicioVigencia" id="txtFechaInicioVigencia1" readOnly="true" cssClass="cajaTextoM2 w150" disabled="#readOnly"/>
		</td>
		<td style="display:none"  id="js_inicioVigencia2">
			<s:if test="tipoAccion != 2">
				<sj:datepicker name="configuracionBono.fechaInicioVigencia" id="txtInicioVigencia2" buttonImage="../img/b_calendario.gif" 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 		       onblur="esFechaValida(this);" disabled="#readOnly"></sj:datepicker>
	 		 </s:if>
	 		 <s:else>
	 		 	<s:textfield name="configuracionBono.fechaInicioVigencia" disabled="#readOnly" cssClass="w100 cajaTextoM2"/>
	 		 </s:else>  
		</td>		
		<th><s:text name="midas.fuerzaventa.configBono.finVigencia" /></th>
		<td id="js_finVigencia1">
			<s:textfield name="configuracionBono.fechaFinVigencia" id="txtFechaFinVigencia1" readOnly="true" cssClass="cajaTextoM2 w150" disabled="#readOnly"/></td>
			<td style="display:none" id="js_finVigencia2">
				<s:if test="tipoAccion != 2">
					<sj:datepicker name="configuracionBono.fechaFinVigencia" id="txtFechaFinVigencia2" buttonImage="../img/b_calendario.gif" 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
		 			   onblur="esFechaValida(this);" disabled="#readOnly"></sj:datepicker>
		 		</s:if>
		 		<s:else>
		 			<s:textfield name="configuracionBono.fechaFinVigencia" disabled="#readOnly" cssClass="w100 cajaTextoM2"/>
		 		</s:else> 
		</td>		
	</tr>	
 	<tr> 
 		<td colspan="6"> 
 			<table class="contenedorFormas w880 no-border"> 
 				<tr> 
 					<th><s:text name="midas.fuerzaventa.configBono.lineaVenta" /></th>						 
 					<td> 
 						<ul class="w300" id="ajax_listaLineaVenta"> 
 						    <s:iterator value="listaLineaVenta" var ="varLineaVenta" status="stat" > 
 						           <li> 
 						           		<s:if test="#varLineaVenta.checado==1"> 						               		
 						                 		 <input type="checkbox" 
 						                  		 name="listaLineaVentaSeleccionadas[${stat.index}].clave"  
 						                  		 id="listaLineaVentaSeleccionadas${stat.index}"  
 						                  		 value="${varLineaVenta.clave}"  
 						                  		 onclick='javascript:onChangeLineaVenta();' 
 						                  		 checked="checked"
 						                  		 class="js_checkEnable"/>      
<!-- 		 						               <label for="varLineaVenta[%{#stat.index}].id">    		                                                -->
		 						                  ${varLineaVenta.valor}
<!-- 		 						               </label>  -->
 						               </s:if>
 						               <s:else>
 						                     <input type="checkbox"
	 						                  		 name="listaLineaVentaSeleccionadas[${stat.index}].clave"  
	 						                  		 id="listaLineaVentaSeleccionadas${stat.index}"  
	 						                  		 value="${varLineaVenta.clave}"  
	 						                  		 onclick='javascript:onChangeLineaVenta();' 
	 						                  		 class="js_checkEnable"/>
<!-- 	 						                <label for="varLineaVenta[%{#stat.index}].id"> 		                                                      -->
	 						                  ${varLineaVenta.valor}
<!-- 	 						               </label> -->
 						               </s:else> 
 						          </li>
 						    </s:iterator>
 						</ul>                      
 					</td> 					
 					<th><s:text name="midas.fuerzaventa.configBono.producto" /></th> 
 					<td> 
						<ul class="w300" id="ajax_listaProductos">
								 <s:iterator value="listaProductos" var ="varProductos" status="stat" > 
							 		<li> 
 						           		<s:if test="#varProductos.checado==1"> 						               		
 						                 		 <input type="checkbox" 
 						                  		 name="listaProductosSeleccionados[${stat.index}].idProducto"  
 						                  		 id="listaProductosSeleccionados${stat.index}"  
 						                  		 value="${varProductos.id}"  
 						                  		 onclick="onChangeProducto();" 
 						                  		 checked="checked"
 						                  		 class="js_checkEnable"/> 						                  		 
<!-- 		 						                <label for="varProductos[%{#stat.index}].id" > -->
		 						                ${varProductos.valor}
<!-- 		 						                </label>  -->
 						               </s:if>
 						               <s:else> 						               
	 						                  <input type="checkbox"
	 						                  		 name="listaProductosSeleccionados[${stat.index}].idProducto"  
	 						                  		 id="listaProductosSeleccionados${stat.index}"  
	 						                  		 value="${varProductos.id}"  
	 						                  		 onclick="onChangeProducto();" 
	 						                  		 class="js_checkEnable"/>  
<!-- 	 						                <label for="varProductos[%{#stat.index}].id" > -->
	 						                ${varProductos.valor}
<!-- 	 						                </label> -->
 						               </s:else> 
 						          </li>
 						    </s:iterator>
						</ul> 
 					</td>					 
 					
 				</tr> 
 				<tr>
 					<th><s:text name="midas.fuerzaventa.configBono.ramo" /></th> 
 					<td> 
 						<ul class="w300"  id="ajax_listaRamos">
 								<s:iterator value="listaRamos" var ="varObjRamos" status="stat" > 
	 									<li> 
	 						           		<s:if test="#varObjRamos.checado==1">	 						               		
	 						                 		 <input type="checkbox" 
	 						                  		 name="listaRamosSeleccionados[${stat.index}].idRamo"  
	 						                  		 id="listaRamosSeleccionados${stat.index}"  
	 						                  		 value="${varObjRamos.id}"  
	 						                  		 onclick="onChangeRamo();" 
	 						                  		 checked="checked"
	 						                  		 class="js_checkEnable"/>
<!-- 			 						                 <label for="varObjRamos[%{#stat.index}].id">  		                                                      -->
			 						                  ${varObjRamos.valor}
<!-- 			 						                </label>  -->
	 						               </s:if>
	 						               <s:else>	 						               
		 						             <input type="checkbox"
		 						           		 name="listaRamosSeleccionados[${stat.index}].idRamo"  
		 						           		 id="listaRamosSeleccionados${stat.index}"  
		 						           		 value="${varObjRamos.id}"  
		 						           		 onclick="onChangeRamo();" 
		 						           		 class="js_checkEnable"/> 
<!-- 			 						           	<label for="varObjRamos[%{#stat.index}].id"> 	                                                     -->
			 						              ${varObjRamos.valor}
<!-- 			 						            </label> -->
	 						         </s:else> 
	 						     </li>
 						     </s:iterator>
 						</ul> 
 					</td> 
 					<th><s:text name="midas.fuerzaventa.configBono.subramo" /></th> 
 					<td> 
 						<ul class="w300"  id="ajax_listaSubRamos">
 							<s:iterator value="listaSubRamos" var ="varObjSubRamos" status="stat" > 
	 									<li> 
	 						           		<s:if test="#varObjSubRamos.checado==1">	 						               		 
	 						                 		 <input type="checkbox" 
	 						                  		 name="listaSubRamosSeleccionados[${stat.index}].idSubramo"  
	 						                  		 id="listaSubRamosSeleccionados${stat.index}"  
	 						                  		 value="${varObjSubRamos.id}"  
	 						                  		 checked="checked"
	 						                  		 class="js_checkEnable"/>
<!-- 		 						                  	<label for="varObjSubRamos[%{#stat.index}].id">	                                                      -->
			 						                  ${varObjSubRamos.valor}
<!-- 			 						                </label>  -->
	 						               </s:if>
	 						               <s:else>	 						                
		 						            	<input type="checkbox"
		 						           		 name="listaSubRamosSeleccionados[${stat.index}].idSubramo"  
		 						           		 id="listaSubRamosSeleccionados${stat.index}"  
		 						           		 value="${varObjSubRamos.id}"  
		 						           		 class="js_checkEnable"/>
<!-- 		 						           	    <label for="varObjSubRamos[%{#stat.index}].id">	                                                      -->
			 						              ${varObjSubRamos.valor}
<!-- 			 						            </label> -->
	 						         </s:else> 
	 						     </li>
 						     </s:iterator>
 						</ul> 
 					</td>
 				</tr> 
 				<tr>  										 
 					<th><s:text name="midas.fuerzaventa.configBono.lineaNegocio" /></th> 
 					<td> 
 						<ul class="w300" id="ajax_lineaNegocio">
 							<s:iterator value="listaLineaNegocio" var ="varObjLineaNegocio" status="stat" > 
	 									<li> 
	 						           		<s:if test="#varObjLineaNegocio.checado==1">	 						               		
	 						                 		 <input type="checkbox" 
	 						                  		 name="listaLineaNegocioSeleccionados[${stat.index}].idSeccion"  
	 						                  		 id="listaLineaNegocioSeleccionados${stat.index}"  
	 						                  		 value="${varObjLineaNegocio.id}"  
	 						                  		 onclick="onChangeLineaNegocio();" 
	 						                  		 checked="checked"
	 						                  		 class="js_checkEnable lineaNegocio"/>
<!-- 	 						                  		 <label for="varObjLineaNegocio[%{#stat.index}].id">                                                      -->
			 						                  ${varObjLineaNegocio.valor}
<!-- 			 						                </label>  -->
	 						               </s:if>
	 						               <s:else>	 						               
			 						             <input type="checkbox"
			 						           		 name="listaLineaNegocioSeleccionados[${stat.index}].idSeccion"  
			 						           		 id="listaLineaNegocioSeleccionados${stat.index}"  
			 						           		 value="${varObjLineaNegocio.id}"  
			 						           		 onclick="onChangeLineaNegocio();" 
			 						           		 class="js_checkEnable lineaNegocio"/>
<!-- 			 						           	<label for="varObjLineaNegocio[%{#stat.index}].id"> 	                                                      -->
			 						              ${varObjLineaNegocio.valor}
<!-- 			 						            </label> -->
	 						         </s:else> 
	 						     </li>
 						     </s:iterator>
 						</ul> 
 					</td>					 
 					<th><s:text name="midas.fuerzaventa.configBono.coberturas" /></th> 
 					<td> 
 						<ul class="w300" id="ajax_listaCoberturas">
 							<s:iterator value="listaCoberturas" var ="varObjLineaCoberturas" status="stat" > 
	 									<li>
	 									<!-- 
	 										<input type="checkbox" 
	 						                  		 name="listaCoberturasSeleccionadas[${stat.index}].idCobertura"  
	 						                  		 id="listaCoberturasSeleccionadas${stat.index}"  
	 						                  		 value="${varObjLineaCoberturas.id}"  
	 						                  		 checked="checked"
	 						                  		 class="jqcheckd"
	 						                  		 />              
										-->

	 						           		<s:if test="#varObjLineaCoberturas.checado==1">	 						               		 
	 						                 		 <input type="checkbox" 
	 						                  		 name="listaCoberturasSeleccionadas[${stat.index}].idCobertura"  
	 						                  		 id="listaCoberturasSeleccionadas${stat.index}"  
	 						                  		 value="${varObjLineaCoberturas.id}"  
	 						                  		 checked="checked"
	 						                  		 class="js_checkEnable checkCobertura"/>              
<!-- 			 						                <label for="varObjLineaCoberturas[%{#stat.index}].id">  		                                        -->
			 						                  ${varObjLineaCoberturas.valor}
<!-- 			 						               </label>  -->
	 						               </s:if>
	 						               <s:else>	 						                
		 						             <input type="checkbox"
		 						           		 name="listaCoberturasSeleccionadas[${stat.index}].idCobertura"  
		 						           		 id="listaCoberturasSeleccionadas${stat.index}"  
		 						           		 value="${varObjLineaCoberturas.id}"  
		 						           		 class="js_checkEnable checkCobertura"/>
<!-- 		 						           	<label for="varObjLineaCoberturas[%{#stat.index}].id">	                                                      -->
		 						              ${varObjLineaCoberturas.valor}
<!-- 		 						            </label> --> 
	 						         </s:else> 
	 						     </li>
 						     </s:iterator>
 						</ul> 
 					</td> 
 				</tr> 
 			</table> 
 		</td> 
 	</tr> 
	<tr>
		<td colspan="6">
			<table class="contenedorFormas w880 no-border">
				<tr>
					<th width="150px" class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.moneda" /></th>
					<td width="270px"><s:select name="configuracionBono.idMoneda" headerKey="" headerValue="Seleccione.."  list="listaTipoMoneda" listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w150 jQrequired" disabled="#readOnly"/></td>
											
											
					<td><s:checkbox name="configuracionBono.ajusteDeOficinaBoolean" id="js_ajusteOficina"  key="midas.fuerzaventa.configBono.ajusteOficina" labelposition="right" disabled="#readOnly"/></td>
					<td><s:checkbox name="configuracionBono.globaBoolean" id="js_global"  key="midas.fuerzaventa.configBono.global" labelposition="right" disabled="#readOnly"/></td>
				</tr>	
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.bonoAjustar" /></th>
					<td><s:select name="configuracionBono.idBonoAjustar" id="sel_idBonoAjustar" headerKey="" headerValue="Seleccione.."  list="listaDescripcionBono"
						      listKey="id" listValue="descripcion" lilabelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly" enable="true"/>
					</td>
					<th width="130px">&nbsp;
<%-- 					<s:text name="midas.fuerzaventa.configBono.periodoAjuste" /> --%>
					</th>
					<td>&nbsp;
<%-- 					<s:select name="configuracionBono.periodoAjuste.id" id="sel_periodoAjuste" headerKey="" headerValue="Seleccione.."  list="listaPeriodosAjuste" --%>
<!-- 						      listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly"/> -->
					</td>					
				</tr>	
				<tr>
					<th class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.produccionSobre" /></th>
					<td ><s:select name="configuracionBono.produccionSobre.id" headerKey="" headerValue="Seleccione.."  list="listaProduccionSobre"
						       listKey="id" listValue="valor" cssClass="jQrequired cajaTextoM2 w150" disabled="#readOnly"/>
					</td>
					
					<th><s:text name="midas.fuerzaventa.configBono.siniestralidadSobre" /></th>
					<td ><s:select name="configuracionBono.siniestralidadSobre.id" headerKey="" headerValue="Seleccione.."  list="listaSiniestralidadsobre"
						       listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly"/>
					</td>
				</tr>			
				<tr>
					<th id="produccionMinimaLbl"><s:text name="midas.fuerzaventa.configBono.produccionMinima" /></th>
					<td><s:textfield name="configuracionBono.produccionMinima" onkeypress="presionTecla();" onblur="validarEstandarEnCampos(this);" cssClass="cajaTextoM2 w150" disabled="#readOnly" id="produccionMinimaTxt"/></td>
					
					<th><s:text name="midas.fuerzaventa.configBono.maximoSiniestralidad" /></th>
					<td ><s:textfield id="pctSiniestralidadMaxima" name="configuracionBono.porcentajeSiniestralidadMaxima" onkeypress="presionTecla();" onblur="validarEstandarEnCampos(this);"  cssClass="cajaTextoM2 w150" disabled="#readOnly"/></td><!-- maxlength="2" -->
				</tr>	
				<tr>					
					<th><s:text name="midas.fuerzaventa.configBono.periodoInicial" /></th>
					<td id="js_habilitaPeriodoInicial1"><sj:datepicker name="configuracionBono.periodoInicial" id="txtFechaPeriodoInicial" buttonImage="../img/b_calendario.gif" 
						   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
						   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
   						   onblur="esFechaValida(this);" disabled="#readOnly"></sj:datepicker> 
					</td>			
					<td style="display:none" id="js_habilitaPeriodoInicial2"><s:textfield name="configuracionBono.periodoInicial" id="txtFechaPeriodoInicial2"  cssClass="cajaTextoM2 w150"/></td>		
					
					<th><s:text name="midas.fuerzaventa.configBono.periodoFinal" /></th>
					<td  id="js_habilitaPeriodoFinal1"><sj:datepicker name="configuracionBono.periodoFinal" id="txtFechaPeriodoFinal" buttonImage="../img/b_calendario.gif" 
						   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
						   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
   						   onblur="esFechaValida(this);" disabled="#readOnly"></sj:datepicker> 
					</td>				
					<td style="display:none"  id="js_habilitaPeriodoFinal2"><s:textfield name="configuracionBono.periodoFinal" id="txtFechaPeriodoFinal2" cssClass="cajaTextoM2 w150" disabled="#readOnly"/></td>
					
				</tr>	
			</table>
		</td>
	</tr>
</table>
<table class="contenedorFormas w910">
	<tr>
		<th class="jQIsRequired"><s:text name="midas.fuerzaventa.configBono.modoAplicacion" /></th>
		<td class="contenedorFormas">
			<div>
				<s:radio name="configuracionBono.modoAplicacion.id" id="modoAplicacion_id" list="listaModosAplicacion" listKey="id" listValue="valor" labelposition="left" disabled="#readOnly"  onclick="modifCampo(this)"/>
			</div>
		</td>
		<td>
			<div><s:radio name="tipoModoAplicacion" id="radioDirecto" list="#{'1':'Directo'}" disabled="#readOnly" onchange="requeridoCrecimientoYSupMeta();"/></div>
			<div><s:radio name="tipoModoAplicacion" id="radioConfigRangos" list="#{'1':'Configurar Rangos'}" disabled="#readOnly" onchange="requeridoCrecimientoYSupMeta();"/></div>
		</td>
		<td>
			<div><s:textfield name="configuracionBono.porcentajeImporteDirecto" id="valorPorcentajeImporteDirecto" cssClass="cajaTextoM2 w100" onkeypress="presionTecla();" disabled="#readOnly" onblur="validarCampoModoApl(this);"/></div><!-- maxlength="10" -->
			<s:if test="tipoAccion != 2">
				<div class="btn_back w130" id="btn_agregaFactores">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="mostrarModalRangoFactores();">
						<s:text name="midas.boton.agregarFactores"/>
					</a>
				</div>
			</s:if>
		</td>
		<td class="w300">&nbsp;</td>
	</tr>
</table>
<table class="contenedorFormas w910">
	<tr>
		<td colspan="2" class="titulo">
			<s:text name="midas.fuerzaventa.configBono.comparacionProduccion"></s:text>
		</td>
	</tr>
	<tr>
		<th class="w160" id="comparacionProduccionLbl"><s:text name="midas.fuerzaventa.configBono.periodoComparacion"/></th>
		<td><s:select name="configuracionBono.periodoComparacion.id" headerKey="" headerValue="Seleccione.."  list="listaPeriodoComparacion"
			       listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w150" disabled="#readOnly" id="periodoComparacion"/>
		</td>
	</tr>
</table>
<table class="contenedorFormas w910">
	<tr>
		<td colspan="6" class="titulo">
			<s:text name="midas.fuerzaventa.configBono.produccion"></s:text>
		</td>
	</tr>		
	<tr>
		<td>
			<table class="contenedorFormas no-border">
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.centroOperacion" /></th>
					<td  colspan="2">
						<ul class="w300" id="centroOperacionProduccion">
						    <s:iterator value="listaCentroOperacion" var="varListaCentroOperacion" status="stat" >
						           <li>             
						                  <input type="checkbox"
						                  		 name="centroOperacionesSeleccionados[${stat.index}].centroOperacion.id" 
						                  		 id="centroOperacionesSeleccionados${stat.index}" 
						                  		 value="${varListaCentroOperacion.id}" 
						                  		 class="js_checkEnable"/>
<!-- 						                  <label for="varListaCentroOperacion[%{#stat.index}].id"> 		                                                     -->
						                   ${varListaCentroOperacion.descripcion}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>                     
							<s:iterator value="configuracionBono.listaCentroOperaciones" var ="varListaCentroOper"status="status">	
								<s:iterator value="listaCentroOperacion" var ="varListaCentroOperacion" status="stat">
									<s:if test="%{#varListaCentroOperacion.id == configuracionBono.listaCentroOperaciones[#status.index].centroOperacion.id}">
										<script type="text/javascript">
											checarChec('centroOperacionesSeleccionados${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>		
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.gerencia" /></th>
					<td colspan="2">
						<ul class="w300" id="gerenciaProduccion">
						    <s:iterator value="listaGerencias" var="varListaGerencias" status="stat" >
						           <li>						               
						                  <input type="checkbox"
						                  		 name="gerenciaLong[${stat.index}]" 
						                  		 id="gerenciasSeleccionadas${stat.index}" 
						                  		 value="${varListaGerencias.id}" 
						                  		 class="js_checkEnable"/>
<!-- 						                    <label for="varListaGerencias[%{#stat.index}].id">		                                                     -->
						                   ${varListaGerencias.descripcion}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>                     
						<s:iterator value="configuracionBono.listaGerencias" status="status">	
								<s:iterator value="listaGerencias" var ="varListaGerencias" status="stat">
									<s:if test="%{#varListaGerencias.id == configuracionBono.listaGerencias[#status.index].gerencia.id}">
										<script type="text/javascript">
											checarChec('gerenciasSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	
					</td>					
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.ejecutivoVentas" /></th>
					<td colspan="2">
						<ul class="w300" id="ejecutivosProduccion">
						    <s:iterator value="listaEjecutivos" var="varListaEjecutivos" status="stat" >
						           <li>						               
						                  <input type="checkbox"
						                  		 name="ejecutivoLong[${stat.index}]" 
						                  		 id="ejecutivosSeleccionados${stat.index}" 
						                  		 value="${varListaEjecutivos.id}" 
						                  		 class="js_checkEnable" />
<!-- 						                    <label for="varListaEjecutivos[%{#stat.index}].id">		                                                     -->
						                   ${varListaEjecutivos.nombreCompleto}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>              
						<s:iterator value="configuracionBono.listaEjecutivos" status="status">	
								<s:iterator value="listaEjecutivos" var ="varListaEjecutivos" status="stat">
									<s:if test="%{#varListaEjecutivos.id == configuracionBono.listaEjecutivos[#status.index].ejecutivo.id}">
										<script type="text/javascript">
											checarChec('ejecutivosSeleccionados${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	       
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.promotoria" /></th>
					<td colspan="2">
						<ul class="w300" id="promotoriasProduccion">
						    <s:iterator value="listaPromotorias" var="varListaPromotorias" status="stat" >
						           <li>						               
						                  <input type="checkbox"
						                  		 name="promotoriaLong[${stat.index}]" 
						                  		 id="promotoriasSeleccionadas${stat.index}" 
						                  		 value="${varListaPromotorias.id}" 
						                  		 class="js_checkEnable" />
<!-- 						                    <label for="varListaPromotorias[%{#stat.index}].id">		                                                     -->
						                   ${varListaPromotorias.descripcion}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>
						<s:iterator value="configuracionBono.listaPromotorias" status="status">	
								<s:iterator value="listaPromotorias" var ="varListaPromotorias" status="stat">
									<s:if test="%{#varListaPromotorias.id == configuracionBono.listaPromotorias[#status.index].promotoria.id}">
										<script type="text/javascript">
											checarChec('promotoriasSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	                     
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.tipoPromotoria" /></th>
					<td colspan="2">
						<ul class="w300" id="tipoPromotoriaProduccion">
						    <s:iterator value="listaTiposPromotoria" var="varListaTiposPromotoria" status="stat" >
						           <li>						                
						                  <input type="checkbox"
						                  		 name="tipoPromotoriasSeleccionadas[${stat.index}].tipoPromotoria.id" 
						                  		 id="tipoPromotoriasSeleccionadas${stat.index}" 
						                  		 value="${varListaTiposPromotoria.id}" 
						                  		 class="js_checkEnable" />
<!-- 						                   <label for="varListaTiposPromotoria[%{#stat.index}].id">		                                                     -->
						                   ${varListaTiposPromotoria.valor}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>        
						<s:iterator value="configuracionBono.listaTiposPromotoria" status="status">	
								<s:iterator value="listaTiposPromotoria" var ="varListaTiposPromotoria" status="stat">
									<s:if test="%{#varListaTiposPromotoria.id == configuracionBono.listaTiposPromotoria[#status.index].tipoPromotoria.id}">
										<script type="text/javascript">
											checarChec('tipoPromotoriasSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	                 
					</td>					
				</tr>
				<tr>
					<th>
						<s:text name="midas.fuerzaventa.configBono.poliza" />
						<s:textfield name="txtIdPoliza" id="txtIdPoliza" onchange="addPolizaConfig();" style="display:none"></s:textfield>
					</th>
					<td>
						<s:if test="tipoAccion != 2">
							<div class="btn_back w100">
								<a href="javascript: void(0);" class="icon_buscar"
									onclick="mostrarModalPolizaConfig();">
									<s:text name="midas.boton.seleccionar"/>
								</a>
							</div>
						</s:if>
					</td>
					<td>
						<ul class="w190" id="listaPolizasProduccion">
							<s:iterator value="lista_Polizas" var="varPolizas" status="stat">
								<li id="${varPolizas.idPoliza}">
									<s:if test="tipoAccion != 2">
										<img src="../img/icons/ico_eliminar.gif" 
										onclick="delete_poliza(${varPolizas.idPoliza});" style="cursor:pointer;"/>&nbsp;&nbsp;
										<label name="listaConfigPolizas[${varPolizas.idPoliza}].id" for="${varPolizas.idPoliza}">${varPolizas.numeroPolizaMidasString}</label>
									</s:if>
									<s:else>
										&nbsp;&nbsp;
										<label name="listaConfigPolizas[${varPolizas.idPoliza}].id" for="${varPolizas.idPoliza}">${varPolizas.numeroPolizaMidasString}</label>
									</s:else>
								</li>
							</s:iterator>
						</ul>
					</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<table class="contenedorFormas no-border">
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.agente" />
						<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda" onchange="addAgenteProduccion();" style="display:none;"/>
					</th>
					<td>
						<s:if test="tipoAccion != 2">
							<div class="btn_back w100">
								<a href="javascript: void(0);" class="icon_buscar"
									onclick="pantallaModalBusquedaAgente();">
									<s:text name="midas.boton.seleccionar"/>
								</a>
							</div>
						</s:if>
					</td>
					<td>
						<ul class="w190" id="listaProduccionAgentes">
							<s:iterator value="lista_agentes" var="varAgentes" status="stat">
								<li id="${varAgentes.id}">
								<s:if test="tipoAccion != 2">
										<img src="../img/icons/ico_eliminar.gif" 
										onclick="delete_agente(${varAgentes.id});" style="cursor:pointer;"/>&nbsp;&nbsp;
								</s:if>
									<label for="${varAgentes.id}">${varAgentes.idAgente} - ${varAgentes.persona.nombreCompleto}</label>
									<s:hidden name ="listaAgentes[%{#stat.index}].agente.id"/>
								</li>
							</s:iterator>
						</ul>                     
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.negocio.clasificacionAgente" /></th>
					<td  colspan="2">
						<ul class="w300" id="tipoAgenteProduccion">
						    <s:iterator value="listaTiposAgente" var="varListaTiposAgente" status="stat" >
						           <li>						                 
						                  <input type="checkbox"
						                  		 name="tipoAgentesSeleccionados[${stat.index}].tipoAgente.id" 
						                  		 id="tipoAgentesSeleccionados${stat.index}" 
						                  		 value="${varListaTiposAgente.id}" 
						                  		 class="js_checkEnable"/>
<!-- 						                  <label for="varListaTiposAgente[%{#stat.index}].id">		                                                     -->
						                   ${varListaTiposAgente.valor}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>              
						<s:iterator value="configuracionBono.listaTipoAgentes" status="status">	
								<s:iterator value="listaTiposAgente" var ="varListaTiposAgente" status="stat">
									<s:if test="%{#varListaTiposAgente.id == configuracionBono.listaTipoAgentes[#status.index].tipoAgente.id}">
										<script type="text/javascript">
											checarChec('tipoAgentesSeleccionados${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>       
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.prioridad" /></th>
					<td  colspan="2">
						<ul class="w300" id="prioridadProduccion">
						    <s:iterator value="listaPrioridad" var="varListaPrioridades" status="stat" >
						           <li>						                 
						                  <input type="checkbox"
						                  		 name="prioridadesSeleccionadas[${stat.index}].prioridadAgente.id" 
						                  		 id="prioridadesSeleccionadas${stat.index}" 
						                  		 value="${varListaPrioridades.id}"
						                  		 class="js_checkEnable" />
<!-- 						                  <label for="varListaPrioridades[%{#stat.index}].id">		                                                     -->
						                   ${varListaPrioridades.valor}
<!-- 						                 </label> -->
						          </li>
						    </s:iterator>
						</ul>               
						<s:iterator value="configuracionBono.listaPrioridades" status="status">	
								<s:iterator value="listaPrioridad" var ="varListaPrioridad" status="stat">
									<s:if test="%{#varListaPrioridad.id == configuracionBono.listaPrioridades[#status.index].prioridadAgente.id}">
										<script type="text/javascript">
											checarChec('prioridadesSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	      
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.fuerzaventa.configBono.estatus" /></th>
					<td colspan="2">
						    <ul id="situacionProduccion">
						    <s:iterator value="listaSituacion" var="varCatalogoTipoSituacion" status="stat">
							     <li>							     
					
							     		<input name="situacionesSeleccionadas[${stat.index}].situacionAgente.id" 
							     				id="situacionesSeleccionadas${stat.index}" 
							     				value="${varCatalogoTipoSituacion.id}"
							     				 type="checkbox"
							     				 class="js_checkEnable" /> 		
<!-- 							     		<label for="varCatalogoTipoSituacion[%{#stat.index}].id">		 			   -->
							     		       ${varCatalogoTipoSituacion.valor}				
<!-- 							     			</label> -->
							    </li>
						    </s:iterator>
						</ul>
						<s:iterator value="configuracionBono.listaSituaciones" status="status">	
								<s:iterator value="listaSituacion" var ="varListaSituacion" status="stat">
									<s:if test="%{#varListaSituacion.id == configuracionBono.listaSituaciones[#status.index].situacionAgente.id}">
										<script type="text/javascript">
											checarChec('situacionesSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>
					</td>
				</tr>
			</table>
		</td>					
	</tr>
</table>
<table class="contenedorFormas w910">
	<tr>
		<td colspan="6" class="titulo">
			<s:text name="midas.fuerzaventa.configBono.beneficiarioBono"></s:text>
		</td>
	</tr>
	<tr>

		<td><s:checkbox name=""  key="midas.fuerzaventa.configBono.promotoria" id="benefPromotoria" labelposition="right" disabled="#readOnly" cssClass="benef"/></td>
			<s:textfield id="txtIdBeneficiarioPromotoria" name="txtIdBeneficiarioPromotoria" onchange="addPromotoriaBeneficiaria();" style="display:none;"/>
		<td>
			<div>

				<s:checkbox name="configuracionBono.todasPromotoriasBoolean" onclick="activaBtnSeleccionarPromoBenef();" id="todasPromotoriasBoolean"  key="midas.fuerzaventa.configBono.todos" labelposition="right" disabled="#readOnly" cssClass="benef js_promTodos"/>		
				<s:if test="tipoAccion != 2">		
					<div class="btn_back w100" id="btnBenefProm">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="pantallaModalPromotoria();">
							<s:text name="midas.boton.seleccionar"/>
						</a>
					</div>		
					
				</s:if>
			</div>
		</td>
		<td>
			<ul class="w190" id="listaPromotoriasBeneficiarias">
				<s:iterator value="listaAplicaPromotorias" var="varPromotorias" status="stat">
					<li id="${varPromotorias.promotoria.id}">
						<s:if test="tipoAccion != 2">
							<img src="../img/icons/ico_eliminar.gif" 
							onclick="deletePromotoriaBenef(${varPromotorias.promotoria.id});" style="cursor:pointer;"/>&nbsp;&nbsp;
						</s:if>
						<label name="listaConfigPolizas[${varPromotorias.promotoria.id}].id" for="${varPromotorias.promotoria.descripcion}">${varPromotorias.promotoria.descripcion}</label>
					</li>
				</s:iterator>
			</ul>
		</td>
		<td><s:checkbox name=""  key="midas.fuerzaventa.configBono.agente" id="benefAgente" labelposition="right" disabled="#readOnly" cssClass="benef"/></td>
			<s:textfield id="txtIdAgenteBeneficiario" name="txtIdAgenteBeneficiario" onchange="addAgenteBeneficiario();" style="display:none;"/>			
		<td>
			<div>
				<s:checkbox name="configuracionBono.todosAgentesBoolean" onclick="activaBtnSeleccionarAgtBenef();" id="todosAgentesBoolean"  key="midas.fuerzaventa.configBono.todos" labelposition="right" disabled="#readOnly" cssClass="benef js_agtTodos"/>	
				<s:if test="tipoAccion != 2">			
					<div class="btn_back w100" id="btnBenefAgt">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="pantallaModalBusquedaAgenteBeneficiario();">
							<s:text name="midas.boton.seleccionar"/>
						</a>
					</div>		
				</s:if>
			</div>
		</td>
		<td>
		<!--<label for="varLineaVenta[%{#stat.index}].id">--> 
			<ul class="w190" id="listaBeneficiariosAgentes">
				<s:iterator value="lista_AplicaAgentes" var="varAplicaAgentes" status="stat">
					<li id="${varAplicaAgentes.id}" class="benef${varAplicaAgentes.id}">
						<s:if test="tipoAccion != 2">
							<img src="../img/icons/ico_eliminar.gif" 
							onclick="deleteAgenteBenefi(${varAplicaAgentes.id});" style="cursor:pointer;"/>&nbsp;
						</s:if>
						<label name="listaAgentes[${varAplicaAgentes.id}].id" for="${varAplicaAgentes.persona.nombreCompleto}">${varAplicaAgentes.persona.nombreCompleto}</label>
					</li>
				</s:iterator>
			</ul>                   
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
		</td>
	</tr>
</table>
<div align="right" class="w910 inline" >
		<div class="btn_back w110">
			<a href="javascript: void(0)" class="icon_siguiente"
				onclick="javascript:atrasOSiguiente('excepciones');">
				<s:text name="midas.boton.siguiente"/>
			</a>
		</div>
		<s:if test="tipoAccion != 2">
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="guardarConfigBono_AndValidRangoConf();">
				<s:text name="midas.boton.guardar"/>
			</a>
		</div>
	</s:if>	
</div>
</s:form>
<script type="text/javascript">
	requeridoCrecimientoYSupMeta();
	validarCheckModoAplicacion();
</script>