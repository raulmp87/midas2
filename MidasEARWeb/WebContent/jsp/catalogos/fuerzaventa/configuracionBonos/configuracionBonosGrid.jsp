<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="50" sort="int" >id</column>
		<column id="descripcion" type="ro" width="200" sort="str">Descripcion Bono</column>
		<column id="" type="ro" width="*" sort="str">Tipo de Bono</column>
<%-- 		<column id="" type="ro" width="*" sort="str">Producto</column> --%>
<%-- 		<column id="" type="ro" width="*" sort="str">Linea de Negocio</column> --%>
		<column id="" type="ro" width="*" sort="str">Produccion</column>
		<column id="" type="ro" width="*" sort="str">Sin Factura</column>
		<column id="" type="ro" width="*" sort="str">Activo / Inactivo</column>
		<column id="" type="ro" width="*" sort="str">Ajuste de Oficina</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
<%-- 			<column id="accionBorrar" type="img" width="30" sort="na"/> --%>
		</s:if>
	</head>
	<s:iterator value="listaConfiguracionesBonos" var="rowConfiguracionesBonos" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowConfiguracionesBonos.id}]]></cell>
			<cell><![CDATA[${rowConfiguracionesBonos.descripcion}]]></cell>
			<cell><![CDATA[${rowConfiguracionesBonos.descripcionTipoBono}]]></cell>
<%-- 			<cell><![CDATA[${rowConfiguracionesBonos.descripcionProducto}]]></cell> --%>
<%-- 			<cell><![CDATA[${rowConfiguracionesBonos.idProducto}]]></cell>	 --%>
			<cell><![CDATA[${rowConfiguracionesBonos.produccionMinima}]]></cell>
			<s:if test="#rowConfiguracionesBonos.pagoSinFactura==0">	
				<cell>No</cell>
			</s:if>
			<s:else>
				<cell>Si</cell>
			</s:else>
			<s:if test="#rowConfiguracionesBonos.activo==0">
				<!-- 	
				<cell><![CDATA[${rowConfiguracionesBonos.activo}]]></cell>
				 -->
				<cell>No</cell>
			</s:if>
			<s:else>
				<cell>Si</cell>
			</s:else>
			<s:if test="#rowConfiguracionesBonos.ajusteDeOficina==0">
				<cell>No</cell>
			</s:if>
			<s:else>
				<cell>Si</cell>
			</s:else>		
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetallePath, 2,{"configuracionBono.id":${rowConfiguracionesBonos.id},"idRegistro":${rowConfiguracionesBonos.id},"idTipoOperacion":70})^_self</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetallePath, 4,{"configuracionBono.id":${rowConfiguracionesBonos.id},"idRegistro":${rowConfiguracionesBonos.id},"idTipoOperacion":70})^_self</cell>
			
						
		</row>
	</s:iterator>
</rows>