<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
	var idConfig='<s:property value="configuracionBono.id"/>';
	var url="/MidasWeb/fuerzaventa/configuracionBono/listarRangoFactores.action?configuracionBono.id="+idConfig;
	rangoFactorGrid = new dhtmlXGridObject('rangoFactoresGrid');   
	rangoFactorGrid.attachEvent("onCellChanged",validateNum);  
	rangoFactorGrid.load(url);
</script>
<s:textfield name="configuracionBono.id" id="configuracionBono_id" style="display:none;"></s:textfield>
<div id="divCarga" style="position:absolute;"></div>
<div align="center" id="rangoFactoresGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880 inline">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:validateGridFactores();">
			<s:text name="Validar Grid"/>
		</a>
	</div>
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:agregarRangoFactor();">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:guardarRangosFactores();">
			<s:text name="midas.boton.guardar"/>
		</a>
	</div>	
</div>
</s:if>