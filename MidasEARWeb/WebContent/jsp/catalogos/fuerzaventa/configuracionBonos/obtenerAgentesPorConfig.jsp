<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->

<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){

	
    var arrCentroOperacion= new Array();
    jQuery("#centroOperacionProduccion li input").each(function(){    
     	if(jQuery(this).attr("checked") == true){
     		var idCentroOperacion = {"centroOperacion.id":jQuery(this).val()};
     		arrCentroOperacion.push(idCentroOperacion);
     	}
     });
     
     var arrGerencia= new Array();
    jQuery("#gerenciaProduccion li input").each(function(){    
     	if(jQuery(this).attr("checked") == true){
     		var idGerencia = {"gerencia.id":jQuery(this).val()};
     		arrGerencia.push(idGerencia);
     	}
     });
    
     var arrEjecutivos= new Array();
    jQuery("#ejecutivosProduccion li input").each(function(){    
     	if(jQuery(this).attr("checked") == true){
     		var idEjecutivo = {"ejecutivo.id":jQuery(this).val()};
     		arrEjecutivos.push(idEjecutivo);
     	}
     });
     
    var arrPromotorias= new Array();
    jQuery("#promotoriasProduccion li input").each(function(){    
     	if(jQuery(this).attr("checked") == true){
//      		var idPromotoria = {"promotoria.id":jQuery(this).val()};
//      		arrPromotorias.push(idPromotoria);
     		arrPromotorias.push(jQuery(this).val());
     	}
     });
     var listaIdsPromotrias;
     for(var x=0; x<=arrPromotorias.length-1; x++){
     	if(x==0){
     		listaIdsPromotrias ="promotoriasSelecionadasLong["+x+"]="+arrPromotorias[x];
     	}else{
     		listaIdsPromotrias =listaIdsPromotrias+"&promotoriasSelecionadasLong["+x+"]="+arrPromotorias[x];
     	}
     }

     var arrTipoPromotoria= new Array();
    jQuery("#tipoPromotoriaProduccion li input").each(function(){    
     	if(jQuery(this).attr("checked") == true){
     		var idTipoPromotoria = {"tipoPromotoria.id":jQuery(this).val()};
     		arrTipoPromotoria.push(idTipoPromotoria);
     	}
     });
     
//     var arrTipoPromotoria= new Array();
//     jQuery("#tipoPromotoriaProduccion li input").each(function(){    
//      	if(jQuery(this).attr("checked") == true){
//      		var idTipoPromotoria = {"tipoPromotoria.id":jQuery(this).val()};
//      		arrTipoPromotorias.push(idTipoPromoria);
//      	}
//      });
    
    /****extrae las polizas que se agregaron en la configuracion de bonos***/
	var arrPolizas= new Array();
	jQuery("#listaPolizasProduccion li").each(function(index){
		var idPoliza = {"idPoliza":jQuery(this).attr("id")};
		arrPolizas.push(idPoliza);
    });
    
    
    var arrAgente= new Array();
	jQuery("#listaProduccionAgentes li").each(function(index){
		var idAgente = {"id":jQuery(this).attr("id")};
		arrAgente.push(idAgente);              
    });
    
    var arrTipoAgente= new Array();
	jQuery("#tipoAgenteProduccion li input").each(function(){
		if(jQuery(this).attr("checked") == true){
			var idTipoAgente = {"tipoAgente.id":jQuery(this).val()};
			arrTipoAgente.push(idTipoAgente);
		}
    });
    
    var arrPrioridad= new Array();
	jQuery("#prioridadProduccion li input").each(function(){
		if(jQuery(this).attr("checked") == true){
			var idPrioridad = {"prioridadAgente.id":jQuery(this).val()};
			arrPrioridad.push(idPrioridad);
		}              
    });
    
    var arrSituacion= new Array();
	jQuery("#situacionProduccion li input").each(function(index){
		if(jQuery(this).attr("checked") == true){
			var idSituacion = {"situacionAgente.id":jQuery(this).val()};
			arrSituacion.push(idSituacion);
		}              
    });
    
    
	var listaIdsCentroOperacion=jQuery.convertJsonArrayToParams(arrCentroOperacion,["centroOperacion.id"],"centroOperacionesSeleccionados");
	var listaIdsGerencia=jQuery.convertJsonArrayToParams(arrGerencia,["gerencia.id"],"gerenciasSeleccionadas");
	var listaIdsEjecutivo=jQuery.convertJsonArrayToParams(arrEjecutivos,["ejecutivo.id"],"ejecutivosSeleccionados");
// 	var listaIdsPromotrias=jQuery.convertJsonArrayToParams(arrPromotorias,["promotoria.id"],"promotoriasSeleccionadas");
	var listaIdsTipoPromotrias=jQuery.convertJsonArrayToParams(arrTipoPromotoria,["tipoPromotoria.id"],"tipoPromotoriasSeleccionadas");
	var listaIdsPolizas=jQuery.convertJsonArrayToParams(arrPolizas,["idPoliza"],"listaConfigPolizas");
	var listaIdsAgentes=jQuery.convertJsonArrayToParams(arrAgente,["id"],"lista_agentes");
	var listaIdsTipoAgentes=jQuery.convertJsonArrayToParams(arrTipoAgente,["tipoAgente.id"],"tipoAgentesSeleccionados");
	var listaIdsPrioridad=jQuery.convertJsonArrayToParams(arrPrioridad,["prioridadAgente.id"],"prioridadesSeleccionadas");
	var listaIdsSituacion=jQuery.convertJsonArrayToParams(arrSituacion,["situacionAgente.id"],"situacionesSeleccionadas");
	

	 var urlFiltro=listarAgentes+"?"+ 
	"idField=txtIdAgenteBeneficiario"
	+"&"+listaIdsCentroOperacion
	+"&"+listaIdsGerencia
 	+"&"+listaIdsEjecutivo
 	+"&"+listaIdsPromotrias
 	+"&"+listaIdsTipoPromotrias
 	+"&"+listaIdsPolizas
 	+"&"+listaIdsAgentes
 	+"&"+listaIdsTipoAgentes
 	+"&"+listaIdsPrioridad
 	+"&"+listaIdsSituacion;
	var idField='<s:property value="idField"/>';	 
	listarFiltradoGenerico(urlFiltro,"configuracionBonosGrid", null,idField,null); //'configuracionBonosModal'
	
 });
</script>
<div id="divCarga" style="position:absolute;"></div>
<div align="center" id="configuracionBonosGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>

<div id="pagingArea"></div><div id="infoArea"></div>
	
	