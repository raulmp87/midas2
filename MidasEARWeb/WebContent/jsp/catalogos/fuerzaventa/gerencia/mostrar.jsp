<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/gerencia/gerenciaHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"gerenciaGrid", null,idField,'gerenciaModal');
	 });
</script>

<s:form action="listarFiltrado" id="gerenciaCatalogoForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table  width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.catalogos.centro.operacion.gerencias"/>
			</td>
		</tr>
		<tr>
			<th width="70px"><s:text name="midas.fuerzaventa.ejecutivo.descripcion" /></th>
			<td width="275px"><s:textfield name="filtroGerencia.descripcion" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td>
			<th width="115px"><s:text name="midas.catalogos.centro.operacion.centroDeOperacion" /></th>
			<td width="355px">
				<s:select headerKey="" headerValue="Seleccione..." list="listaCentrosOperacion" listValue="descripcion" listKey="id" name="filtroGerencia.centroOperacion.id"  cssClass="cajaTextoM2 w150" ></s:select>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.fechaInicio"/></th>	
			<td><sj:datepicker name="filtroGerencia.fechaInicio" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 			<th><s:text name="midas.fuerzaventa.fechaFin"/></th>
			<td><sj:datepicker name="filtroGerencia.fechaFin" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
		
		<tr>		
			<th><s:text name="midas.fuerzaventa.negocio.responsable" /></th>	
			<td><s:textfield name="filtroGerencia.personaResponsable.nombreCompleto" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td>
		</tr>
		<tr class="JS_hide" >
			<td colspan="4">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">filtroGerencia.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">filtroGerencia.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">filtroGerencia.domicilio.claveCiudad</s:param>
					<s:param name="idColoniaName">filtroGerencia.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">filtroGerencia.domicilio.calleNumero</s:param>
					<s:param name="cpName">filtroGerencia.domicilio.codigoPostal</s:param>		
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado"><s:text name="midas.catalogos.centro.operacion.estado"/></s:param>
					<s:param name="labelColonia"><s:text name="midas.catalogos.centro.operacion.colonia"/></s:param>
					<s:param name="labelCodigoPostal"><s:text name="midas.catalogos.centro.operacion.codigoPostal"/></s:param>
					<s:param name="labelCalleNumero"><s:text name="midas.catalogos.centro.operacion.calleYNumero"/></s:param>
					<s:param name="labelCiudad"><s:text name="midas.catalogos.centro.operacion.municipio"/></s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">6</s:param>						
				</s:action>
			</td>
		</tr>
		<tr>
			<td>
				<div style="display: block" id="masFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('masFiltros');">
						<s:text name="midas.boton.masFiltros"/>
					</a>
				</div>
				<div style="display:none" id="menosFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('menosFiltros');">
						<s:text name="midas.boton.menosFiltros"/>
					</a>
				</div>
			</td>
			<td colspan="2"  align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="listarFiltradoGenerico(listarFiltradoGerenciaPath, 'gerenciaGrid',document.gerenciaCatalogoForm,'${idField}','gerenciaModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>			
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>	
	<div id="gerenciaGrid" class="w880 h200" style="overflow:hidden"></div>	
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<!-- Se checa si es solo consulta, si es asi, no debe de agregar -->
<s:if test="tipoAccion!=\"consulta\"">
<div class ="w880" align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar ." 
			onclick="operacionGenerica(verDetalleGerenciaPath,1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>