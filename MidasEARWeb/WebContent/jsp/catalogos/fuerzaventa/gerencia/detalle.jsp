<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/catalogos/gerencia.js'/>"></script>
<script type="text/javascript">
jQIsRequired();
	var ventanaCentroOperacion=null;
	var ventanaResponsable=null;
	function mostrarModalCentroOperacion(){
		var url="/MidasWeb/fuerzaventa/centrooperacion/mostrarContenedor.action?tipoAccion=consulta&idField=idCentroOperacion";
		sendRequestWindow(null, url, obtenerVentanaCentroOperacion);
	}
	
	function mostrarModalResponsable(){
		var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=idPersonaResponsable";
		sendRequestWindow(null, url,obtenerVentanaResponsable);
	}
	
	function obtenerVentanaCentroOperacion(){
		var wins = obtenerContenedorVentanas();
		ventanaCentroOperacion= wins.createWindow("centroOperacionModal", 400, 320, 930, 450);
		ventanaCentroOperacion.center();
		ventanaCentroOperacion.setModal(true);
		ventanaCentroOperacion.setText("Consulta de centros de operaci\u00F3n");
		ventanaCentroOperacion.button("park").hide();
		ventanaCentroOperacion.button("minmax1").hide();
		return ventanaCentroOperacion;
	}
	
	function obtenerVentanaResponsable(){
		var wins = obtenerContenedorVentanas();
		ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 930, 450);
		ventanaResponsable.center();
		ventanaResponsable.setModal(true);
		ventanaResponsable.setText("Consulta de Personas");
		ventanaResponsable.button("park").hide();
		ventanaResponsable.button("minmax1").hide();
		return ventanaResponsable;
	}
	
	function findByIdCentroOperacion(idCentroOperacion){
		var url="/MidasWeb/fuerzaventa/centrooperacion/findById.action";
		var data={"centroOperacion.id":idCentroOperacion};
		jQuery.asyncPostJSON(url,data,populateCentroOperacion);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populateCentroOperacion(json){
		if(json){
			var centroOperacion=json.centroOperacion;
			var idCentroOperacion=centroOperacion.id;
			jQuery("#idCentroOperacion").val(idCentroOperacion);
			var nombre=centroOperacion.descripcion;
			jQuery("#descripcionCentroOperacion").val(nombre);	
		}
	}
	
	function findByIdResponsable(idResponsable){
		var url="/MidasWeb/fuerzaVenta/persona/findById.action";
		var data={"personaSeycos.idPersona":idResponsable};
		jQuery.asyncPostJSON(url,data,populatePersona);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
	}
	
	function populatePersona(json){
		if(json){
			var idResponsable=json.personaSeycos.idPersona;
			var nombre=json.personaSeycos.nombreCompleto;
			jQuery("#idPersonaResponsable").val(idResponsable);
			jQuery("#nombreResponsable").val(nombre);
		}
	}
</script>
<s:include value="/jsp/catalogos/fuerzaventa/gerencia/gerenciaHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>

<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.gerencia.tituloAlta')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.gerencia.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.gerencia.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.gerencia.tituloEditar')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="%{getText('midas.catalogos.agente.gerencia.tituloCatalogo')}"/>
</s:else>
<s:form id="gerenciaForm">
<s:hidden name="gerencia.idGerencia"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="#titulo"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.fuerzaventa.gerencia.datosGerencia"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="midas.fuerzaventa.ejecutivo.IdGerencia" /></th>
			<td colspan="3"><s:textfield name="gerencia.id"  readonly="true" cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th class="jQIsRequired" width="130px"><s:text name="midas.fuerzaventa.ejecutivo.descripcion" /></th>
			<td colspan="3"><s:textfield name="gerencia.descripcion"  readonly="#readOnly" id="txtDescripcion" cssClass="cajaTextoM2 w350 jQrequired" maxlength="30"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.correoElectronico" /></th>
			<td colspan="3"><s:textfield name="gerencia.correoElectronico"  readonly="#readOnly" id="txtCorreo" cssClass="w250 cajaTextoM2 jQrequired jQemail" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.situacion" /></th>
			<td colspan="3">
				<s:select name="gerencia.claveEstatus" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'1':'ACTIVO', '0':'INACTIVO'}"/>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.centroDeOperacion" /></th>
			<td width="110px">
				<s:textfield id="idCentroOperacion" name="gerencia.centroOperacion.id"  readonly="true" cssClass="cajaTextoM2 jQrequired w80 jsSearchResultField" onchange="javascript:findByIdCentroOperacion(this.value);"></s:textfield>
			</td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<td width="100px">				
				<div class="btn_back w80">
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalCentroOperacion();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
			</s:if>
			<td>
				<s:textfield id="descripcionCentroOperacion" name="gerencia.centroOperacion.descripcion"  readonly="true" labelposition="left" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.centro.operacion.idResponsable" /></th>
			<td><s:textfield id="idPersonaResponsable" readonly="true" name="gerencia.personaResponsable.idPersona" cssClass="cajaTextoM2 jQrequired w80" onchange="javascript:findByIdResponsable(this.value);"></s:textfield></td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<td>
				<div class="btn_back w80">
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalResponsable();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			</s:if>
			<td><s:textfield id="nombreResponsable" name="gerencia.personaResponsable.nombreCompleto" labelposition="left" cssClass="cajaTextoM2 w200" readonly="true"></s:textfield></td>
		</tr>
		<tr>
			<td colspan="2">
				<s:if test="tipoAccion != 1">
					<div class="btn_back w110">
					<a href="javascript: void(0);"
						onclick="mostrarVentanaGenerica(mostrarEjecutivoPath, '<s:text name="midas.fuerzaventa.ejecutivo.titulo"/>', 400, 180, document.gerenciaForm);">
						<s:text name="midas.fuerzaventa.ejecutivo.titulo"/>
					</a>
				</div>	
				</s:if>
			</td>
		</tr>
	 </table>
	 <table  width="98%" class="contenedorFormas" align="center">	
		<tr>
			<td class="titulo">
				<s:text name="midas.fuerzaventa.negocio.tituloDomicilio"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:hidden name="gerencia.domicilio.idDomicilio.idDomicilio"/>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
	
					<s:param name="idPaisName">gerencia.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">gerencia.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">gerencia.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">gerencia.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">gerencia.domicilio.calleNumero</s:param>
					<s:param name="cpName">gerencia.domicilio.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">gerencia.domicilio.nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">gerencia.idColoniaCheck</s:param>				
					<s:param name="labelPais">Pais</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">top</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="%{#required}"></s:param>
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
					<s:param name="funcionResult" >populateDomicilioGerencia</s:param>
				</s:action>
			</td>
		</tr>		
	</table>
	<s:if test="gerencia.id != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
		    <s:if test="tipoAccion != 5">
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>	    
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2">
			<s:if test="tipoAccion != 5">
				<div class="btn_back w110">
					<a href="javascript: mostrarHistorico(20,${gerencia.id});" class="icon_guardar">
						<s:text name="midas.boton.historico"/>
					</a>
				</div>
		    </s:if>
			</td>
		</tr>
	</table>
	</s:if>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeGerencia()"><!-- javascript: mostrarCatalogoGenerico(mostrarGerenciaPath);" class="icon_regresar -->
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					<!-- 
					<s:if test="tipoAccion != 2 && tipoAccion!=null">
						<div id="divGuardarBtn">
							<div class="btn_back w110"  > 
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="${accionJsBoton} return false;">
									<s:property value="%{#claveTextoBoton}"/>
								</a>
							</div>
						</div>
					</s:if>
					-->
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarGerencia();"><!-- if(validateAll(true)){realizarOperacionGenerica(guardarGerenciaPath, document.gerenciaForm, null,false);} -->
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>	
					<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
							onclick="if(validateAll(true)){realizarOperacionGenerica(eliminarGerenciaPath, document.gerenciaForm , null);}">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>	
					</s:if>
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
