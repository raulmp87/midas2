<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
function addTooltTipJavascript(idElemento){
	var elementoOption = document.getElementById(idElemento).options;
	var numberOption = elementoOption.length;
	for(x=0; x<numberOption; x++){
		var option = elementoOption[x].text;
		elementoOption[x].setAttribute('title',option);
	}
}
//-->
</script>
<s:form action="listarFiltrado" id="gerenciaForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo">
				<s:text name="midas.catalogos.centro.operacion.ejecutivos.titulo"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:select 
					id="ejMostrar"
					multiple="true" 
					name="listaEjecutivos" 
					listValue="personaResponsable.nombreCompleto" 
					listKey="idEjecutivo" 
					cssClass="cajaTextoM w350" 
					list="listEjecutivos" 
					labelposition="left"
					javascriptTooltip="personaResponsable.nombreCompleto"/>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	<!--
	addTooltTipJavascript('ejMostrar');
	//-->
	</script>
</s:form>

