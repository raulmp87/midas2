<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<sj:head/>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<script type="text/javascript">
	jQuery.noConflict();
	jQuery(function(){
		listarFiltradoGenerico(listarFiltradoBusquedaResponsablePath, 'busquedaResponsableGrid');
	});	
</script>
<s:include value="/jsp/catalogos/fuerzaventa/centrooperacion/centroOperacionHeader.jsp"></s:include>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="required" value="1" />	
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
</s:if>
<s:form action="listarFiltrado" id="busquedaResponsable">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.centro.operacion.titulo"/>
			</td>
		</tr> 
		<tr>
			<td colspan="2">
				<s:textfield name="nombreResponsable" key="midas.catalogos.centro.operacion.nombre" cssClass="" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="razonSocial" id="txtClave" key="midas.catalogos.centro.operacion.razonSocial" labelposition="left"></s:textfield>
			</td>
			<td colspan="2"> 
				<s:textfield name="RFC" id="txtClave" key="midas.catalogos.centro.operacion.rfc" labelposition="left"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >

					<s:param name="idEstadoName">filtroCentroOperacion.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">filtroCentroOperacion.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">filtroCentroOperacion.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">filtroCentroOperacion.domicilio.calleNumero</s:param>
					<s:param name="cpName">filtroCentroOperacion.domicilio.codigoPostal</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">1</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>			
					<s:param name="requerido" value="%{#required}"></s:param>								
				</s:action>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textfield name="calleYNumero" id="txtClave" key="midas.catalogos.centro.operacion.calleYNumero" labelposition="left"></s:textfield>
			</td>
			<td colspan="2">
				<s:textfield name="codigoPostal" id="txtClave" key="midas.catalogos.centro.operacion.codigoPostal" labelposition="left"></s:textfield>
			</td>	 
			<td colspan="2">
				<s:textfield name="telefono" id="txtClave" key="midas.fuerzaventa.negocio.telefono" labelposition="left"></s:textfield>
			</td>
		</tr>
	</table>
	
	<div class="dataGridGenericStyle">
		<div id="busquedaResponsableGrid" class="w550 h100"></div>
	</div>
	
	
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>

<div align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="parent.mostrarCatalogoGenerico(mostrarCentroOperacionPath);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator" src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>