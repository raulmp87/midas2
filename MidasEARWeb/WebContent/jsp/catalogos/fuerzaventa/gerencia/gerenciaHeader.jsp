<%@ taglib prefix="s" uri="/struts-tags" %>
	
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
 
<script type="text/javascript">
	var mostrarGerenciaPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/gerencia"/>';
	var mostrarBusquedaResponsablePath = '<s:url action="mostrarBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
	var mostrarCentroOperacionPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/centrooperacion"/>';
	var mostrarEjecutivoPath = '<s:url action="mostrarEjecutivos" namespace="/fuerzaventa/gerencia"/>';
	
	var verDetalleGerenciaPath = '<s:url action="verDetalle" namespace="/fuerzaventa/gerencia"/>';
	var guardarGerenciaPath = '<s:url action="guardar" namespace="/fuerzaventa/gerencia"/>';
	var eliminarGerenciaPath = '<s:url action="eliminar" namespace="/fuerzaventa/gerencia"/>';
	
	var listarGerenciaPath = '<s:url action="lista" namespace="/fuerzaventa/gerencia"/>';
	var listarFiltradoGerenciaPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/gerencia"/>';
	
	var listarBusquedaResponsablePath = '<s:url action="listarBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
	var listarFiltradoBusquedaResponsablePath = '<s:url action="listarFiltradoBusquedaResponsable" namespace="/fuerzaventa/centrooperacion"/>';
	
	var listarCentroOperacionPath = '<s:url action="listarCentroOperacion" namespace="/fuerzaventa/gerencia"/>';
	var listarFiltradoCentroOperacionPath = '<s:url action="listarFiltradoCentroOperacion" namespace="/fuerzaventa/gerencia"/>';
</script>