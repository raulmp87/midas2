<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingAreaHist</param>
				<param>true</param>
				<param>infoAreaHist</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>				
		</beforeInit>		
		
		<column id="actualizacionAgente.id" type="ro" width="*" sort="str" hidden="true">Comentario</column>
		<column id="actualizacionAgente.idRegistro" type="ro" width="50" sort="str" hidden="true">Comentario</column>
		<column id="actualizacionAgente.comentarios" type="ro" width="500" sort="str">Comentario</column>
		<column id="actualizacionAgente.fechaHoraActualizacion" type="ro" width="200" sort="date"><s:text name="midas.fuerzaventa.historico.fechaHoraActualizacion"/></column>
		<column id="actualizacionAgente.usuarioActualizacion" type="ro" width="100" sort="str" ><s:text name="midas.fuerzaventa.historico.usuarioActualizacion"/></column>
		<column id="accionVer" type="img" width="100" sort="na" align="center"><s:text name="midas.general.acciones"/></column> 								
	</head>

	<% int a=0;%>
	<s:iterator value="historial" var="actualizacion">
		<% a+=1; %>
		<row id="<%=a%>">			
			<cell><s:property value="id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idRegistro" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="comentarios" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaHoraActualizacionString" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="usuarioActualizacion" escapeHtml="false" escapeXml="true" /></cell>
			<s:if test="idTipoOperacion == 10">		
			    <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoCentroOperacionPath, 5,{"centroOperacion.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":10,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>			
		    </s:if>
		    <s:elseif test="idTipoOperacion == 20">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoGerenciaPath, 5,{"gerencia.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":20,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>
		    </s:elseif>
		    <s:elseif test="idTipoOperacion == 30">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoEjecutivoPath, 5,{"ejecutivo.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":30,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>
		    </s:elseif>
		     <s:elseif test="idTipoOperacion == 40">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoPromotoriaPath, 5,{"promotoria.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":40,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>  
		    </s:elseif>
		    <s:elseif test="idTipoOperacion == 50">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoAgentePath, 5,{"agente.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":50,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>  
		    </s:elseif>
		    <s:elseif test="idTipoOperacion == 80">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoAfianzadoraPath, 5,{"afianzadora.id":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":80,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>  
		    </s:elseif>
		    <s:elseif test="idTipoOperacion == 90">
		        <cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParamsHistorial(verDetalleHistoricoPersonaPath, 5,{"personaSeycos.idPersona":${actualizacion.idRegistro},"idRegistro":${actualizacion.idRegistro},"idTipoOperacion":90,"ultimaModificacion.fechaHoraActualizacionString":"${actualizacion.fechaHoraActualizacionString}"},detalleDiv)^_self</cell>  
		    </s:elseif>
		</row>		
	</s:iterator>	
</rows>

