<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarFiltradoAfianzadoraPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"afianzadoraGrid", null,idField,'afianzadoraModal');
 });
	
</script>
<s:form action="listarFiltrado" id="afianzadoraForm" name="afianzadoraForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.fuerzaventa.afianzadora.titulo"/>
			</td>
		</tr>
		<tr>
			<th width="125px">&nbsp;<s:text name="midas.fuerzaventa.negocio.razonSocial" /></th>	
			<td width="287px"><s:textfield name="filtroAfianzadora.razonSocial" id="txtAfianzadora" cssClass="cajaTextoM2 w220"></s:textfield></td>			
			<th width="90px">				
				<s:text name="midas.fuerzaventa.negocio.rfc" />
			</th>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td ><s:textfield name="filtroAfianzadora.siglasRfc" id="txtClave" cssClass="cajaTextoM2 w30" maxlength="3"></s:textfield></td>
						<td ><s:textfield name="filtroAfianzadora.fechaRfc" id="txtClave" cssClass="cajaTextoM2 w50" maxlength="6"></s:textfield></td>
						<td ><s:textfield name="filtroAfianzadora.homoclaveRfc" id="txtClave" cssClass="cajaTextoM2 w30" maxlength="3"></s:textfield></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.fuerzaventa.fechaInicio"/></th>	
			<td><sj:datepicker name="filtroAfianzadora.fechaInicio" id="txtFechaInicio" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
 			<th><s:text name="midas.fuerzaventa.fechaFin"/></th>
			<td><sj:datepicker name="filtroAfianzadora.fechaFin" id="txtFechaFin" buttonImage="../img/b_calendario.gif" 
							   changeYear="true" changeMonth="true" maxlength="10" cssClass="w100 cajaTextoM2" 								   								  
							   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 							   onblur="esFechaValida(this);"></sj:datepicker>
 			</td>
		</tr>
		<tr class="JS_hide" >
			<td colspan="4">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">filtroAfianzadora.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">filtroAfianzadora.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">filtroAfianzadora.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">filtroAfianzadora.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">filtroAfianzadora.domicilio.calleNumero</s:param>
					<s:param name="cpName">filtroAfianzadora.domicilio.codigoPostal</s:param>	
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">6</s:param>						
				</s:action>
			</td>
			
		</tr>	
		<tr>
			<td>
				<div style="display: block" id="masFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('masFiltros');">
						<s:text name="midas.boton.masFiltros"/>
					</a>
				</div>
				<div style="display:none" id="menosFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('menosFiltros');">
						<s:text name="midas.boton.menosFiltros"/>
					</a>
				</div>
			</td>
			<td colspan="3" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoAfianzadoraPath, 'afianzadoraGrid', document.afianzadoraForm,'${idField}','afianzadoraModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="afianzadoraGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica(verDetalleAfianzadoraPath,1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>