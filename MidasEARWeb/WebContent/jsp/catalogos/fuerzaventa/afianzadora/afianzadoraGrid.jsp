<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="50" sort="int">Id</column>
		<column id="descripcion" type="ro" width="300" sort="str"><s:text name="midas.fuerzaventa.negocio.razonSocial"/></column>
		<column id="responsable" type="ro" width="*" sort="str">RFC</column>
		<column id="situacion" type="ro" width="*" sort="str">Sector</column>
		<column id="claveEstatus" type="ro" width="80" sort="str">Situación</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaAfianzadoras" var="rowAfianzadora" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowAfianzadora.id}]]></cell>
			<cell><![CDATA[${rowAfianzadora.razonSocial}]]></cell>
			<cell><![CDATA[${rowAfianzadora.siglasRfc}${rowAfianzadora.fechaRfc}${rowAfianzadora.homoclaveRfc} ]]></cell>
			<cell><![CDATA[${rowAfianzadora.sector.nombreSector}]]></cell>
			<s:if test="claveEstatus == 1">
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.activa"></s:text></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.catalogos.fuerzaventa.estatus.inactiva"></s:text></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleAfianzadoraPath, 2,{"afianzadora.id":${rowAfianzadora.id},"idRegistro":${rowAfianzadora.id},"idTipoOperacion":80})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleAfianzadoraPath, 4,{"afianzadora.id":${rowAfianzadora.id},"idRegistro":${rowAfianzadora.id},"idTipoOperacion":80})^_self</cell>
				<s:if test="claveEstatus==1">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleAfianzadoraPath, 3,{"afianzadora.id":${rowAfianzadora.id},"idRegistro":${rowAfianzadora.id},"idTipoOperacion":80})^_self</cell>
				</s:if>
			</s:if>			
		</row>
	</s:iterator>
</rows>