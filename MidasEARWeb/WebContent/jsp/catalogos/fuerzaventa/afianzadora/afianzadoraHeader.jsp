<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/afianzadora.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>

 
<script type="text/javascript">
	var mostrarContenedorAfianzadoraPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/afianzadora"/>';	
	var verDetalleAfianzadoraPath = '<s:url action="verDetalle" namespace="/fuerzaventa/afianzadora"/>';	
	var urlBusquedaDomicilio = '<s:url action="buscarDomicilio" namespace="/fuerzaventa/afianzadora"/>';
	
	var listarAfianzadoraPath = '<s:url action="lista" namespace="/fuerzaventa/afianzadora"/>';
	
	var listarFiltradoAfianzadoraPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/afianzadora"/>';
	
</script>
