<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:hidden name="afianzadora.id" />
<s:hidden name="rangoFechas" id="txtFechaActual"/>
<script type="text/javascript">
jQIsRequired();
//estatusDesahbilitaCampos();
mostrarOcultarFechaInactivo();
ocultaBtnEliminar();
</script>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.afianzadora.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.afianzadora.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.afianzadora.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.afianzadora.tituloEditar')}"/>
</s:if>
<s:if test="tipoAccion != 5">
    <div class="titulo w400"><s:text name="#titulo"/></div>
</s:if>
<s:form action="agregar" id="afianzadoraForm" name="afianzadoraForm">
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th width="110px" >				
<%-- 				<s:text name="midas.fuerzaventa.negocio.idAfianzadora" /> --%>
			</th>
			<td><s:hidden name="afianzadora.id" id="txtIdAfianzadora" readonly="#idReadOnly" /></td>		
			<th>				
				<s:text name="midas.catalogos.centro.operacion.situacion" />
			</th>
						
			<td><s:select disabled="#readOnly" name="afianzadora.claveEstatus" cssClass="cajaTextoM2" id="situacion" onchange="mostrarOcultarFechaInactivo();"
				       list="#{'1':'Activo','0':'Inactivo' }" /></td>			
		</tr>
		<tr>
			<th width="90px" class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.fechaAlta" />
			</th>	
			<td>	
				<s:textfield name="afianzadora.fechaAlta" id="txtFechaAlta"  cssClass="w100 cajaTextoM2 jQdate-mx" readonly="true"></s:textfield>
			</td>
			<th width="70px">				
				<s:text name="midas.fuerzaventa.fechaBaja" />
			</th>	
			<td>
				<s:textfield name="afianzadora.fechaBaja" id="txtFechaBaja" cssClass="w100 cajaTextoM2 jQdate-mx" readonly="true" ></s:textfield>				
			</td>			
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.razonSocial" /></th>
			<td><s:textfield name="afianzadora.razonSocial" id="txtRazonSocial"  cssClass="w250 cajaTextoM2  jQrequired" readonly="#readOnly" maxlength="120"></s:textfield></td>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.rfc" /></th>
			<td>
				<table>
					<tr>
						<td><s:textfield name="afianzadora.siglasRfc" id="txtSiglasRfc" cssClass="w40 jQsiglasRFCMoral cajaTextoM2 jQalphabetic jQrequired" maxlength="3" readonly="#readOnly" onblur="validaTxtSiglasRfc();"></s:textfield></td>
						<td><s:textfield name="afianzadora.fechaRfc" id="txtFechaRfc" cssClass="w60 jQfechaRFC cajaTextoM2 jQnumeric jQrequired" maxlength="6" readonly="#readOnly" onblur="validaTxtFechaRfc();"></s:textfield></td>
						<td><s:textfield name="afianzadora.homoclaveRfc" id="txtHomoclaveRfc" cssClass="w50 jQclaveRFC cajaTextoM2 jQalphanumeric jQrequired" maxlength="3" readonly="#readOnly" onblur="validaTxtHomoclaveRfc();"></s:textfield></td>
						<!-- jQclaveRFCMoral  -->
					</tr>
				</table>			
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.agentes.sector" /></th>
			<td>
				<s:select name="afianzadora.sector.idSector" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly"
				       headerKey="" headerValue="Seleccione.."
				       list="listaSectores" listKey="idSector" listValue="nombreSector" value="afianzadora.sector.idSector"/>
			</td>
		</tr>		
	</table>	
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="midas.boton.domicilio"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:hidden name="afianzadora.domicilio.idDomicilio.idDomicilio"></s:hidden>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					
					<s:param name="idPaisName">afianzadora.domicilio.clavePais</s:param>
					<s:param name="idEstadoName">afianzadora.domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">afianzadora.domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">afianzadora.domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">afianzadora.domicilio.calleNumero</s:param>
					<s:param name="cpName">afianzadora.domicilio.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">afianzadora.domicilio.nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck</s:param>								
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero"><s:text name="midas.fuerzaventa.negocio.calleNumero"/></s:param>
					<s:param name="labelCodigoPostal"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/></s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="%{#required}"></s:param>
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
				</s:action>
			<td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th width="110px" class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.telefonoCasa" /></th>
			<td><s:textfield name="afianzadora.telefonoParticular" id="txtTelefonoParticular" cssClass="w150 jQtelefono cajaTextoM2 jQrequired" maxlength="12" readonly="#readOnly"></s:textfield></td>
			<th width="70px" class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.telefonoCelular" /></th>
			<td><s:textfield name="afianzadora.telefonoCelular" id="txtTelefonoCelular" cssClass="w150 jQtelefono cajaTextoM2 jQrequired" maxlength="12" readonly="#readOnly"></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.telefonoOficina" /></th>
				<td><s:textfield name="afianzadora.telefonoOficina" id="txtTelefonoOficina" cssClass="w150 jQtelefono cajaTextoM2 jQrequired" maxlength="12" readonly="#readOnly"></s:textfield></td>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.extension" /></th>
			<td colspan="2"><s:textfield name="afianzadora.extensionOficina" id="txtExtensionOficina" cssClass="w150 jQnumeric jQrestrict jQrequired cajaTextoM2" maxlength="6" readonly="#readOnly"></s:textfield></td>
		</tr>		
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.fax" /></th>
			<td colspan="2"><s:textfield name="afianzadora.faxOficina" id="txtFaxOficina" cssClass="w150 cajaTextoM2 jQnumeric jQrestrict" maxlength="20" readonly="#readOnly"></s:textfield></td>			
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.fuerzaventa.negocio.correoElectronico" /></th>
			<td colspan="2"><s:textfield name="afianzadora.correoElectronico" id="txtCorreoElectronico" cssClass="w250 cajaTextoM2 jQemail jQrequired" maxlength="80" readonly="#readOnly"></s:textfield></td>		
		</tr>	
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.paginaWeb" /></th>
			<td colspan="2"><s:textfield name="afianzadora.paginaWeb" id="txtPaginaWeb" cssClass="w250 cajaTextoM2 " maxlength="80" readonly="#readOnly"></s:textfield></td>		
		</tr>	
	</table>
	<s:if test="afianzadora.id != null">
		<table width="98%" class="contenedorFormas" align="center">	
			<tr>
			    <s:if test="tipoAccion != 5">
				    <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
			    </s:if>
			     <s:else>
		            <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" cssClass="cajaTextoM2" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		        </s:else>
				<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
				<td colspan="2" align="right">
				    <s:if test="tipoAccion != 5">							
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_guardar" onclick="javascript: mostrarHistorico(80,${afianzadora.id});">	
							<s:text name="midas.boton.historico"/>	
							</a>
						</div>	
					</s:if>			
				</td>		
			</tr>		
		</table>
	</s:if>
	<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
			<tr>
				<td >
					<div align="right" class="inline" >
					    <s:if test="tipoAccion != 5">
							<div class="btn_back w110">
								<a href="javascript: void(0);" class="icon_regresar"
									onclick="javascript: salirAfianzadora();">
									<s:text name="midas.boton.regresar"/>
								</a>
							</div>
						</s:if>
						<s:if test="tipoAccion == 1 || tipoAccion == 4">
						<div>
						<div class="btn_back w110" id="btn_guardar">
							<a href="javascript: void(0);" class="icon_guardar"
								onclick="guardarAfianzadora();">
								<s:text name="midas.boton.guardar"/>
							</a>
						</div>
						</div>
						</s:if>	
						<s:if test="tipoAccion == 3">
						<div>
							<div class="btn_back w110" id="btnEliminar">
								<a href="javascript: void(0);" class="icon_borrar" onclick="eliminarAfianzadora();">
									<s:text name="midas.boton.borrar"/>
								</a>
							</div>
						</div>	
						</s:if>	
					</div>
				</td>	
			</tr>
		</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
