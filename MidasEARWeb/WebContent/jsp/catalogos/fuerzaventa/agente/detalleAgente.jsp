<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%-- <s:include value="/jsp/sapAmis/sapAmisHeader.jsp"></s:include> --%>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:hidden name="tipoAccion"/>
<s:hidden name="agente.id" id="agente.id"/>
<s:hidden name="claveNegocio"/>
<s:hidden name="tabActiva"/>
<s:hidden name="moduloOrigen"/>
<s:hidden name="ultimaModificacion.fechaHoraActualizacionString"/>
<%-- <s:hidden name="idTipoOperacion" value="50"/> --%>
<s:hidden name="idTipoOperacion"/>
<script type="text/javascript">
jQuery(function(){
	dhx_init_tabbars();
	incializarTabs();
});
</script>
<s:if test="tipoAccion==1">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.agente.altaAgente')}"/>
</s:if>
<s:if test="tipoAccion==2 || tipoAccion == 5">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.agente.consultarAgente')}"/>
</s:if>
<s:if test="tipoAccion==3">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.agente.eliminarAgente')}"/>
</s:if>
<s:if test="tipoAccion==4">
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.agente.editarAgente')}"/>
</s:if>
<s:if test="tipoAccion != 5">
    <div class="titulo w400"><s:text name="#titulo"/></div><!-- <s:text name="midas.fuerzaventa.agente.datosAgente"/> -->
</s:if>

<div select="<s:property value="tabActiva"/>" hrefmode="ajax-html" style="height: 380px;width:910px;margin-left:10px;" id="configuracionNegocioTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="info_general" name="Datos Generales" href="http://void" extraAction="javascript: verInfoGeneral();"></div>
	<div width="100%" id="domicilios" name="Domicilios" href="http://void" extraAction="javascript: verDomicilioAgente();"></div>
	<div width="100%" id="datosFiscales" name="Datos Fiscales" href="http://void" extraAction="javascript: verDatosFiscales();"></div>
	<div width="100%" id="datosContables" name="Datos Contables" href="http://void" extraAction="verDatosContables();"></div>
	<div width="100%" id="datosExtra" name="Datos Extra" href="http://void" extraAction="verDatosExtra();"></div>
	<div width="100%" id="documentos" name="Documentos" href="http://void" extraAction="verDocumentosAgente();"></div>
	<div width="100%" id="carteraClientes" name="Cartera Clientes" href="http://void" extraAction="verCarteraClientes();"></div>
</div>
	

<table width="910px" class="contenedorFormas" align="center">	
		<tr>
		<s:if test="agente.id!= null">		
		    <s:if test="tipoAccion != 5">
		        <td><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>	    
		    </s:if>
		    <s:else>
		        <td ><s:textfield name="ultimaModificacion.fechaHoraActualizFormatoMostrar" id="txtFechaHora" key="midas.fuerzaventa.negocio.fechaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
		    </s:else>			
			<td><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2" align="right">	
			    <s:if test="tipoAccion != 5">
			        <div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="mostrarHistoricoAgente();">
						<s:text name="midas.boton.historico"/>
					</a>
				</div>
			    </s:if>									
			</td>		
		</s:if>
		<s:if test="tipoAccion != 5">
		    <s:if test="moduloOrigen==null">
			<td align="right">
				<div class="btn_back w110">
					<a href="javascript: salirAgente(); " class="icon_regresar"
					onclick="">
					<s:text name="midas.boton.regresar"/>
					</a>
				</div>
			</td>
		</s:if>
		<s:else>
			<td align="right">
				<div class="btn_back w110">
					<a href="javascript: regresarAutorizacion();" class="icon_regresar"
					onclick="">
					<s:text name="midas.boton.regresar"/>
					</a>
				</div>
			</td>
		</s:else>
		</s:if>		
		</tr>		
</table>	
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>