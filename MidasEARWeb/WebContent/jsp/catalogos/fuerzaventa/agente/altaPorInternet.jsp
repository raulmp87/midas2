<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript">
jQIsRequired();
cargarGridDatosBancarios();
cargarGridHijos();
cargarGridEntretenimiento();
initAltaAgenteInternet();
</script>
<!-- select { width: 150px; /* Or whatever width you want. */ } -->
<style type="text/css">
	select.expand { width: auto; } 
</style>
<s:form name="agenteInternetForm" id="agenteInternetForm">
<s:hidden name="altaPorInternet.idAgente" value="-1"/>
<s:hidden name="tabActiva" value="altaInternet"/>
		<div class="titulo w400"><s:text name="Alta Agente Por Internet"/></div>
  		<table width="98%" align="center" class="contenedorFormas">
  			<tr>
				<td colspan="4">
					<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
				</td>
			</tr>	
	  		<tr>
  				<td>
  					<s:text name = "midas.fuerzaventa.altaPorInternet.fechaDeAlta"/>
  				</td>
  				<td>
  					<s:textfield name="altaPorInternet.fechaAlta" readonly="true" cssClass="cajaTextoM2 jQrequired"/>
				</td>
				<td>
  					<s:text name = "midas.agentes.sector"/>
  				</td>
  				<td>	
  					<s:select name="altaPorInternet.persona.claveTipoSector" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="sectorList" listKey="idSector" listValue="nombreSector"/>
  				</td>
  			</tr>
</table>
										<!-- Datos Personales -->

<table width="98%" class="contenedorFormas" align="center" id="DatosPersonales">	
		 <tr>
		 	<td class="titulo" colspan="8">
		 		<s:text name="midas.catalogos.centro.operacion.datosGenerales"/>
		 	</td>
		 </tr>
		 <tr>
			<td class="jQIsRequired" width="100">
				<s:text name="midas.fuerzaventa.tipoPersona"/>
			</td>
			<td>			
				<s:radio list="#{'1':'FISICA', '2':'MORAL'}" value="1" onclick="javascript:tipoPersona(this);" id="txtTipoPersona" name="altaPorInternet.persona.claveTipoPersona" cssClass="jQrequired"></s:radio>
			</td>	
		</tr>		
		<tr>
			<td colspan="2">
				<div id="txfrazonsocial1" style="display: none">
					<table  class="contenedorFormas no-Border" >
						<tr>
							<td class="jQIsRequired">
									<s:text name="midas.fuerzaventa.negocio.razonSocial"/>
							</td>			
							<td colspan="2">			
									<s:textfield name="altaPorInternet.persona.razonSocial" id="txtRazonSocial" onblur="javascript:copiarValor('txtRazonSocial', 'txtRazonSocialAfianzadora');" cssClass="cajaTextoM2 jQrequired w250" maxlength="200"/>
							</td>
						</tr>
					</table>
				</div>
			</td>				
		</tr>
		<tr>
			<td colspan="6">
				<div id="textnombre1">
						<table class="contenedorFormas no-Border" >
							<tr>
								<td class="jQIsRequired">
									<s:text name="midas.negocio.nombre"/>
								</td>	
								<td>
									<s:textfield name="altaPorInternet.persona.nombre" onblur="javascript:copiarValor('txtNombre', 'clienteNombreFiscal');" readonly="#readOnly" id="txtNombre" cssClass="w160 cajaTextoM2 jQrequired" maxlength="40" />
								</td>
								<td class="jQIsRequired">
									<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/>
								</td>	
								<td>
									<s:textfield name="altaPorInternet.persona.apellidoPaterno" onblur="javascript:copiarValor('txtApellidoPaterno', 'clienteApellidoPaternoFiscal');" readonly="#readOnly" id="txtApellidoPaterno" cssClass="w140 cajaTextoM2 jQrequired" maxlength="20" />				
								</td>
								<td class="jQIsRequired">
									<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/>
								</td>	
								<td>
									<s:textfield name="altaPorInternet.persona.apellidoMaterno" onblur="javascript:copiarValor('txtApellidoMaterno', 'clienteApellidoMaternoFiscal');" readonly="#readOnly" id="txtApellidoMaterno" cssClass="w150 cajaTextoM2 jQrequired" maxlength="20" />
								</td>
							</tr>
							<tr>
								<td class="jQIsRequired">
					            	<s:text name="midas.fuerzaventa.sexo"/>
					        	</td>
								<td>
									<s:select name="altaPorInternet.persona.claveSexo" id="sexo" disabled="#readOnly"
									list="#{'M':'Masculino', 'F':'Femenino'}" cssClass="cajaTextoM2 jQrequired w150"
									headerValue="Seleccione..." headerKey="" labelposition="left"/>		
								</td>		
								<td class="jQIsRequired">
									<s:text name="midas.fuerzaventa.estadoCivil"/>
								</td>	
								<td>
									<s:select name="altaPorInternet.persona.claveEstadoCivil" id="estadoCivil" disabled="#readOnly"
									list="estadosCiviles" labelposition="left" headerKey="" headerValue="Seleccione" 
									listKey="idEstadoCivil" listValue="nombreEstadoCivil" cssClass="cajaTextoM2 jQrequired"/>
								</td>							
							</tr>							
						</table>
				</div>
			</td>					
		</tr>
		</table>
		<table id="tablaNacimiento" width="98%" class="contenedorFormas" align="center"> 
		<tr>
			<td colspan="6">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">altaPorInternet.persona.claveNacionalidad</s:param>
					<s:param name="idEstadoName">altaPorInternet.persona.claveEstadoNacimiento</s:param>	
					<s:param name="idCiudadName">altaPorInternet.persona.claveMunicipioNacimiento</s:param>			
					<s:param name="labelPais">Nacionalidad</s:param>	
					<s:param name="labelEstado">Estado de Nacimiento</s:param>
					<s:param name="labelCiudad">Ciudad de Nacimiento</s:param>			
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">50</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido">1</s:param>							
				</s:action>
			</td>
		</tr>
		</table>
		<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td width="10%">
				<div id="fNacimiento" class="w120">
					<s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/>*
				</div>			
				<div id="fConstitucion" class="w120">
					<s:text name="Fecha Constitucion"/>*
				</div>
			</td>
			<td width="30%">
				<div id="tFechaNacimiento">
					<sj:datepicker name="altaPorInternet.persona.fechaNacimiento"
								   buttonImage="../img/b_calendario.gif" 
								   id="txtFechaNacimiento" maxlength="10" cssClass="cajaTextoM2 jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true" yearRange="-80:-18"  
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onblur='esFechaValida(this);'></sj:datepicker>
				</div>				
				<div id="tFechaConstitucion">		
					<sj:datepicker name="altaPorInternet.persona.fechaConstitucion"
								   buttonImage="../img/b_calendario.gif" 
								   id="txtFechaConstitucion" maxlength="10" cssClass="cajaTextoM2 jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true" 
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onblur='esFechaValida(this);'></sj:datepicker>
				</div>
			</td>
			<td width="5%" class="jQIsRequired">
					<s:text name="midas.negocio.agente.rfc"/>
			</td>
			<td width="17%">			
			<div class="elementoInline">
				<s:textfield name="rfcSiglas" id="txtsiglasRfc" maxlength="4" cssClass="cajaTextoM2 jQrequired w50 jQalphabetic jQrestrict" onchange="javascript:copiarValor('txtsiglasRfc', 'siglasRFC');"/>
			</div>
			<div class="elementoInline">
				 <s:textfield name="rfcFecha" id="txtfechaRfc" maxlength="6" cssClass="cajaTextoM2 jQrequired w50 jQnumeric jQrestrict" onchange="javascript:copiarValor('txtfechaRfc', 'fechaRFC');"/> 
			</div>
			<div class="elementoInline">
				 <s:textfield name="rfcHomoclave" id="txthomoclaveRfc" maxlength="4" cssClass="cajaTextoM2 jQrequired w50 jQalphanumeric" onchange="javascript:copiarValor('txthomoclaveRfc', 'HomoClaveRFC');"/>
			</div>
			</td>
			<td width="10%">
			<div id="btnGenerarRFC" class="elementoInline">
				<a href="javascript: void(0);" 
	            onmousedown='javascript:midasFormatRFC(jQuery("#txtNombre").val(),jQuery("#txtApellidoPaterno").val(),jQuery("#txtApellidoMaterno").val(),jQuery("#txtFechaNacimiento").val(),"txtsiglasRfc","txtfechaRfc","txthomoclaveRfc");copiarRFCFiscal();'>Generar RFC</a>
	        </div>
	        <div id="btnGenerarRFCMoral" class="elementoInline">
	        	<a href="javascript: void(0);" 
	       		onclick='javascript:generarYDividirRFCMoral(jQuery("#txtRazonSocial").val(),jQuery("#txtFechaConstitucion").val(),"txtsiglasRfc","txtfechaRfc");copiarRFCFiscal();'>Generar RFC</a>
	    	</div>    
	        </td>
		</tr>	
		<tr id="displayCURP">
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.curp"/>
			</td>		
			<td>
			<div id="linkRenapo">
			<div class="elementoInline">  
				<s:textfield name="altaPorInternet.persona.curp" readonly="#readOnly" id="txtCURP" cssClass="cajaTextoM2 jQalphanumeric jQrequired w150" maxlength="18"/>			
			</div>
			<div class="elementoInline">  
				<a href="http://www.renapo.gob.mx/swb/swb/RENAPO/consultacurp" target="_blank">Consulta CURP</a>	
			</div>					
			<div class="btn_derecha w50">
				<a href="javascript: void(0)"						
				onclick='javascript: validaCURPPersonaAgentesInterJS();'>
					<s:text name="Validar"/>
				</a>
			</div>
			</div>
			</td>
		</tr>			
	</table>									
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="Domicilio Personal"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">altaPorInternet.persona.domicilios[0].clavePais</s:param>
					<s:param name="idEstadoName">altaPorInternet.persona.domicilios[0].claveEstado</s:param>	
					<s:param name="idCiudadName">altaPorInternet.persona.domicilios[0].claveCiudad</s:param>		
					<s:param name="idColoniaName">altaPorInternet.persona.domicilios[0].nombreColonia</s:param>
					<s:param name="calleNumeroName">altaPorInternet.persona.domicilios[0].calleNumero</s:param>
					<s:param name="cpName">altaPorInternet.persona.domicilios[0].codigoPostal</s:param>
					<s:param name="nuevaColoniaName">altaPorInternet.persona.domicilios[0].nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck1</s:param>				
					<s:param name="labelPais">País</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>	
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido">1</s:param>	
					<s:param name="idDomicilioName">idDomicilio0</s:param>    
					<s:param name="funcionResult" >populateDomicilio0</s:param>  						
				</s:action>
				<s:hidden name="altaPorInternet.persona.domicilios[0].tipoDomicilio" value="PERS"/>
			<td>
		</tr>	
	</table>								
<%-- <s:hidden name="altaPorInternet.idPrioridadAgente" value="205"/> --%>
<%-- <s:hidden name="altaPorInternet.idMotivoEstatusAgente" value="237"/> --%>
<%-- <s:hidden name="altaPorInternet.idTipoAgente" value="186"/> --%>
<%-- <s:hidden name="altaPorInternet.tipoSituacion.id" value="237"/> --%>
<s:hidden name="altaPorInternet.porcentajeDividendos" value="1000"/>
<table width="98%" class="contenedorFormas" align="center">
	<tr>
			<td class="titulo" colspan="2" >
				<s:text name ="midas.fuerzaventa.altaPorInternet.promotoriaPerteneceAgente"/>
			</td>
		</tr>
		<tr>
			<th width="130px" class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.gerencia" />
			</th>
			<td>
				<s:select id="idGerencia" name="idGerencia" value="" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivo','idPromotoria',this.value)" cssClass="cajaTextoM2 jQrequired w200"/>
 			</td> <!--onChangeGerencia('idEjecutivo',this.value 'idGerencia') -->
		</tr>
		<tr>
			<th class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.ejecutivo" />
			</th>
			<td>
				<s:select id="idEjecutivo" name="idEjecutivo" value="idEjecutivo" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoria', this.value)" cssClass="cajaTextoM2 jQrequired w200 "/>
			</td>
		</tr>
		<tr>
			<th class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.ejecutivo.promotoria" />
			</th>
			<td>
				<s:select id="idPromotoria" name="altaPorInternet.promotoria.id" value="altaPorInternet.promotoria.id" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 jQrequired w200 "/>
			</td>
		</tr>
	</table>
<!-- 	cedula										 -->
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="10" >
				<s:text name="midas.fuerzaventa.altaPorInternet.tituloCedula"/>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.numeroCedula"></s:text>
			</td>
			<td>
				<s:textfield id="altaPorInternet.numeroCedula" maxLength="20" name="altaPorInternet.numeroCedula" cssClass="cajaTextoM2 jQrequired jQrestrict w200"/>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.fechaAutorizacion"/>
			</td>
			<td>									
  				<sj:datepicker name="altaPorInternet.fechaAutorizacionCedula" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="txtFechafechaAlta" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				changeYear="true" changeMonth="true"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.negocio.tipoCedula"/>
			</td>
			<td>
				<s:select name="altaPorInternet.tipoCedula.id" cssClass="cajaTextoM2 jQrequired w250 wide" labelposition="left"
				       headerKey="" headerValue="Seleccione.."
				       list="catalogoTipocedula" listKey="id" listValue="clave+' - '+valor" value="altaPorInternet.tipoCedula.id"/>				
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.fechaVencimientoCedula"/>
			</td>
			<td>
  				<sj:datepicker name="altaPorInternet.fechaVencimientoCedula" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="txtFechaBaja" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				changeYear="true" changeMonth="true"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				onblur="esFechaValida(this);"></sj:datepicker> 
			</td>
		</tr>
	</table>
<!-- 									 Sección de datos Fiscales -->
		<table width="98%" class="contenedorFormas" align="center">
		<tbody>
			<tr>
				<th class="titulo" colspan="10">
					<s:text name="midas.fuerzaventa.datosFiscales"/> 
				</th>
			</tr>
			<tr>				
				<td>
					<div id="fiscalesNombreL">			
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/>*
					</div>	
				</td>
				<td>
					<div id="fiscalesNombreT">
						<s:textfield id="clienteNombreFiscal" cssClass="cajaTextoM2 w140 jQrequired" readonly="true" maxlength="20"/>
					</div>
				</td>
				<td class="w100">
					<div id="fiscalesNombreAPL">
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.paterno"/>*
					</div>
				</td>
				<td>
					<div id="fiscalesNombreAPT">
						<s:textfield id="clienteApellidoPaternoFiscal" readonly="true" cssClass="cajaTextoM2 w140 jQrequired" maxlength="20"/>
					</div>
				</td>
				<td class="w100">
					<div id="fiscalesNombreAML">
						<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.materno"/>*
					</div>
				</td>
				<td>
					<div id="fiscalesNombreAMT">
						<s:textfield id="clienteApellidoMaternoFiscal" readonly="true" cssClass="cajaTextoM2 w140 jQrequired" maxlength="20"/>
					</div>	
				</td>					
			</tr>
			<tr>
				<td width="180">
					<div id="fiscalesMoralL">
						<s:text name="midas.fuerzaventa.negocio.razonSocial"/>*
					</div>
				</td>			
				<td colspan="3">
					<div id="fiscalesMoralT">
						<s:textfield name="altaPorInternet.afianzadora.razonSocial" readonly="true" id="txtRazonSocialAfianzadora" cssClass="cajaTextoM2 w250" maxlength="200" readonly="#readOnly"/>
					</div>
				</td>
			</tr>
			<tr>		
			<td class="jQIsRequired">
				<s:text name="midas.negocio.agente.rfc"/>
			</td>	
			<td>
				<table id="rfc">
					<tr>					
					<td width="60">
						<s:textfield id="siglasRFC"  cssClass="cajaTextoM2 jQrequired w40 jQalphabetic jQrestrict" maxLength="4"/>
					</td>
					<td width="60">	
						 <s:textfield id="fechaRFC" cssClass="cajaTextoM2 jQrequired w50 jQnumeric jQrestrict" maxlength="6" /> 
			 		</td>
					<td width="60">
						 <s:textfield id="HomoClaveRFC" cssClass="cajaTextoM2 jQrequired w40 jQalphanumeric" maxLength="4"/>
					</td>
				</tr>
			</table>	
		</tr>
		<tr>			
			<td>
				<div id="lRepLegal">
					<s:text name="midas.fuerzaventa.representanteLegal"/>*
				</div>
			</td>			
			<td>
				<div id="tRepLegal">
					<s:textfield name="altaPorInternet.persona.idRepresentante" id="txtIdRepresentanteFiscal" onchange="javascript:findByIdResponsable(this.value);" cssClass="cajaTextoM2 jQrequired w130 jQnumeric" readonly="true"/>
				</div>
			</td>
			<td>
				<div id="botonRep" class="btn_back w80">
					<a href="javascript: void(0)" class="icon_buscar" 
						onclick="mostrarModalContacto();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td>
				<div id=nombreCompletoT>
					<s:textfield name="altaPorInternet.persona.nombreCompleto" id="txtNombreAgente" cssClass="cajaTextoM2 jQrequired w180" readonly="true"/>
				</div>
			</td>					
		</tr>
		<tr>
			<td>Copiar Domicilio Personal</td>
			<td>
				<div class="btn_back w70">
					<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioAgente(1,jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.clavePais').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.claveEstado').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.claveCiudad').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.nombreColonia').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.calleNumero').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.codigoPostal').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.nuevaColonia').val(),jQuery('#idColoniaCheck1').attr('checked'));">
						copiar
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="Domicilio Fiscal"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >					
					<s:param name="idPaisName">altaPorInternet.persona.domicilios[1].clavePais</s:param>
					<s:param name="idEstadoName">altaPorInternet.persona.domicilios[1].claveEstado</s:param>	
					<s:param name="idCiudadName">altaPorInternet.persona.domicilios[1].claveCiudad</s:param>		
					<s:param name="idColoniaName">altaPorInternet.persona.domicilios[1].nombreColonia</s:param>
					<s:param name="calleNumeroName">altaPorInternet.persona.domicilios[1].calleNumero</s:param>
					<s:param name="cpName">altaPorInternet.persona.domicilios[1].codigoPostal</s:param>
					<s:param name="nuevaColoniaName">altaPorInternet.persona.domicilios[1].nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck2</s:param>				
					<s:param name="labelPais">País</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido">1</s:param>
					<s:param name="idDomicilioName">idDomicilio1</s:param>    
					<s:param name="funcionResult" >populateDomicilio1</s:param>
				</s:action>
				<s:hidden name="altaPorInternet.persona.domicilios[1].tipoDomicilio" value="FISC"/>
			<td>
		</tr>	
	</table>	
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="10">
				<s:text name="midas.fuerzaventa.altaPorInternet.fianza"/>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name = "midas.fuerzaventa.negocio.numero" />
			</td>
			<td>
				<s:textfield name="altaPorInternet.numeroFianza" id="altaPorInternet.numeroFianza" maxLength="15" cssClass = "cajaTextoM2 jQrequired jQrestrict"/>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizacion"/>
			</td>
			<td>
				<sj:datepicker name="altaPorInternet.fechaAutorizacionFianza" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif"
					id="fechaAutorizacionFianza" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);"
					changeYear="true" changeMonth="true"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.vencimiento"/>
			</td>
			<td>
				<sj:datepicker name="altaPorInternet.fechaVencimientoFianza" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif"
					id="altaPorInternet.fechaVencimientoFianza" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);"
					changeYear="true" changeMonth="true"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.negocio.montoFianza"/>
			</td>
			<td>
				<s:textfield name="altaPorInternet.montoFianza" id="txtMontoFianza"  cssClass="cajaTextoM2 jQrequired w100 jQfloat jQrestrict" onblur="validarCampoFianza(this)"></s:textfield>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.negocio.idAfianzadora"/>
			</td>
			<td>
				<s:textfield name="altaPorInternet.afianzadora.id" id="idAfianzadora" readonly="true" cssClass="cajaTextoM2 w130 jQrequired jQnumeric jQrestrict"  onchange="javascript:findByIdAfianzadora(this.value);"></s:textfield>
			</td>
			<td>
				<div class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarModalAfianzadora();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</td>
			<td>
				<s:textfield name="altaPorInternet.afianzadora.razonSocial" readonly="true" id="nombreAfianzadora" cssClass="cajaTextoM2 w250 "></s:textfield>
			</td>			
		</tr>		
	</table>
								<!-- Productos Bancarios -->								
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="10">
				<s:text name="midas.fuerzaventa.negocio.productosBancarios"/>
			</td>
		</tr>
		<tr>
			<td width="200" class="jQIsRequired">
				<s:text name="midas.catalogos.agente.productosAgente.banco"/>
			</td>
			<td>
				<s:select name="productoBancarioSeleccionado.banco.idBanco" id="idBanco" 
				list="listaBancos" listKey="idBanco" listValue="nombreBanco" headerKey="" 
 				headerValue="Seleccione..."  cssClass="cajaTextoM2 w150" onchange="onChangeBanco();"></s:select>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.catalogos.agente.productosAgente.producto"/>
			</td>
			<td>
				<s:select id="idProductoBancario" name="productoBancarioSeleccionado.id" list="productosBancarios" listKey="id" 
				listValue="descripcion" headerKey="" headerValue="Seleccione..." cssClass="cajaTextoM2 w150" 
 				onchange="onChangeProductoBancario();"></s:select> 
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.catalogos.agente.productosAgente.tipoProducto"/>
			</td>
			<td>
				<s:textfield name="tipoProducto" id="tipoProducto" cssClass="cajaTextoM2" readonly="true"/>
			</td>
			<td>
				<s:text name = "midas.catalogos.agente.productosAgente.claveDefault"/>
			</td>
			<td>
				<s:checkbox id="claveDefault" name="producto.claveDefaultBoolean" value="%{producto.claveDefault}"/>
				<s:hidden name="checarClaveDefault" id="checarClaveDefault"></s:hidden>
			</td>
		</tr>
		<tr>
			<td id="numeroCuentaLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroCuenta"/>
			</td>
			<td>
				<s:textfield name="numeroCuenta" id="numeroCuenta" maxlength="20" cssClass = "cajaTextoM2 jQnumeric jQrestrict"/>
			</td>
			<td id="numeroClabeLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroClabe"/>
			</td>
			<td>
				<s:textfield name="numeroClabe" id="numeroClabe" maxlength="20" cssClass = "cajaTextoM2 jQnumeric jQrestrict"/>
			</td>
		</tr>
		<tr>
			<td id="montoCreditoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoCredito"/>
			</td>
			<td>
				<s:textfield name="montoCredito" id="montoCredito" cssClass = "cajaTextoM2 jQfloat jQrestrict"/>
			</td>
			<td id="numeroPlazosLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroPlazos"/>
			</td>
			<td>
				<s:textfield name="numeroPlazos" id="numeroPlazos" maxlength="2" cssClass = "cajaTextoM2 jQnumeric jQrestrict"/>
			</td>
			<td id="montoPagoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoPago"/>
			</td>
			<td>
				<s:textfield name="montoPago" id="montoPago" cssClass = "cajaTextoM2 jQfloat jQrestrict"/>
			</td>
		</tr>
		<tr>
			<td id="sucursalLb">
				<s:text name = "midas.catalogos.agente.productosAgente.sucursal"/>
			</td>
			<td>
				<s:textfield name="sucursal" id="sucursal" cssClass = "cajaTextoM2"/>
			</td>
			<td id="plazaLb">
				<s:text name = "midas.catalogos.agente.productosAgente.plaza"/>
			</td>
			<td>
				<s:textfield name="plaza" id="plaza" cssClass = "cajaTextoM2"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.productosSeleccionados"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="productoBancario" class="w850 h150">
				
				</div>
			</td>
		</tr>
		<tr>
			<td  colspan="6">
				<div align="right">
					<div class="btn_back w130">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="agregarProductoTemp();">
							<s:text name="midas.boton.agregarProducto"/>
						</a>
					</div>	
				</div>				
			</td>	
		</tr>
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.fuerzaventa.datosOficina"/>
			</td>
		</tr>
		<tr>
			<td width="200" class="jQIsRequired">
				<s:text name ="midas.fuerzaventa.negocio.telefonoOficina"/>
			</td>
			<td width="170">
				<s:textfield name="altaPorInternet.persona.telefonoOficina" id="telefonoOficina" maxlength="12" cssClass="cajaTextoM2 jQnumeric jQrestrict jQrequired"/>
			</td>
			<td width="100">
				<s:text name ="midas.fuerzaventa.negocio.extension"/>
			</td>
			<td>
				<s:textfield id="extension" cssClass="cajaTextoM2"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.fax"/>
			</td>
			<td>
				<s:textfield name="altaPorInternet.persona.telefonoFax" id="fax" maxlength="12" cssClass="cajaTextoM2 jQtelefono"/>
			</td>
		</tr>
		<tr>	
			<td>Copiar Domicilio</td>
			<td>
			<div align="left" class="inline" >
			Personal				
				<div class="btn_back w20">				
					<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioAgente(2,jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.clavePais').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.claveEstado').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.claveCiudad').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.nombreColonia').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.calleNumero').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.codigoPostal').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[0\\]\\.nuevaColonia').val(),jQuery('#idColoniaCheck1').attr('checked'));">
						
					</a>
				</div>
				Fiscal
				<div class="btn_back w20">				
					<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioAgente(2,jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.clavePais').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.claveEstado').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.claveCiudad').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.nombreColonia').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.calleNumero').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.codigoPostal').val(),jQuery('#altaPorInternet\\.persona\\.domicilios\\[1\\]\\.nuevaColonia').val(),jQuery('#idColoniaCheck2').attr('checked'));">
						
					</a>
				</div>
				</div>
			</td>
		</tr>
	</table>
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="Domicilio Oficina"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					
								<s:param name="idPaisName">altaPorInternet.persona.domicilios[2].clavePais</s:param>
								<s:param name="idEstadoName">altaPorInternet.persona.domicilios[2].claveEstado</s:param>	
								<s:param name="idCiudadName">altaPorInternet.persona.domicilios[2].claveCiudad</s:param>		
								<s:param name="idColoniaName">altaPorInternet.persona.domicilios[2].nombreColonia</s:param>
								<s:param name="calleNumeroName">altaPorInternet.persona.domicilios[2].calleNumero</s:param>
								<s:param name="cpName">altaPorInternet.persona.domicilios[2].codigoPostal</s:param>
								<s:param name="nuevaColoniaName">altaPorInternet.persona.domicilios[2].nuevaColonia</s:param>
								<s:param name="idColoniaCheckName">idColoniaCheck3</s:param>				
								<s:param name="labelPais">País</s:param>	
								<s:param name="labelEstado">Estado</s:param>
								<s:param name="labelCiudad">Municipio</s:param>
								<s:param name="labelColonia">Colonia</s:param>
								<s:param name="labelCalleNumero">Calle y Número</s:param>
								<s:param name="labelCodigoPostal">Código Postal</s:param>
								<s:param name="labelPosicion">left</s:param>
								<s:param name="componente">2</s:param>
								<s:param name="readOnly" value="%{#readOnly}"></s:param>
								<s:param name="requerido">1</s:param>	
								<s:param name="idDomicilioName">idDomicilio2</s:param>   									
								<s:param name="funcionResult">populateDomicilio2</s:param>
							</s:action>
						<td>
						<s:hidden name="altaPorInternet.persona.domicilios[2].tipoDomicilio" value="OFIC"/>
					</tr>	
				</table>													
												
		<table width="98%" class="contenedorFormas" align="center">
			<tr>
				<td class="titulo" colspan="4">
					<s:text name = "midas.fuerzaventa.datosContacto"/>
				</td>
			</tr>
			<tr>
				<td class="jQIsRequired">
					<s:text name="midas.fuerzaventa.telefonoCasa"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.telefonoCasa" cssClass="jQrequired jQtelefono cajaTextoM2 w180" id="cliente.telefono" maxlength="12" onkeypress="return soloNumeros(this, event, false)"/>			
				</td>
				<td class="jQIsRequired">
					<s:text name="midas.fuerzaventa.negocio.telefonoCelular"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.telefonoCelular" id="cliente.celularContacto" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 jQtelefono w180 jQrequired"/>
				</td>
			</tr>
			<tr>
				<td class="jQIsRequired">
					<s:text name="midas.catalogos.centro.operacion.correoElectronico"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.email" id="cliente.correoElectronico" cssClass="cajaTextoM2 w180 jQemail jQrequired"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.fuerzaventa.facebook"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.facebook" id="cliente.facebook" maxlength="50" cssClass="cajaTextoM2 w180"/>
				</td>
				<td>
					<s:text name="midas.fuerzaventa.twitter"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.twitter" id="cliente.twitter" maxlength="50" cssClass="cajaTextoM2 w180"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.fuerzaventa.negocio.paginaWeb"/>
				</td>
				<td>
					<s:textfield name="altaPorInternet.persona.paginaWeb" id="cliente.paginaWeb" maxlength="50" cssClass="cajaTextoM2 w180"/>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<s:text name="midas.fuerzaventa.telefonosAdicionales"/>
				</td>
				<td>
					<s:textarea rows="20" name="altaPorInternet.persona.telefonosAdicionales" id="cliente.telefonosAdicionalesContacto" cssClass="areaTextoM2 jQalphanumeric" cols="30" readonly="#readOnly"/>					
				</td>
				<td valign="top">
					<s:text name="midas.fuerzaventa.correosAdicionales"/>*
				</td>
				<td>
					<s:textarea rows="20"  name="altaPorInternet.persona.correosAdicionales" id="cliente.correosAdicionalesContacto" cssClass="areaTextoM2 jQEmailList jQrestrict jQrequired" onkeypress="return quitaComa(event);" cols="30" readonly="#readOnly"/>					
				</td>
			</tr>
	</table>
															<!-- Datos Extra -->
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.hijos"/>
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.nombre"/></td>
			<td class="w120"><s:textfield cssClass="jQtoUpper cajaTextoM2 w120" id="txtNombreHijos" ></s:textfield></td>
			
			<td><s:text name="midas.fuerzaventa.negocio.apPaterno"/></td>
			<td class="w120"><s:textfield cssClass="jQtoUpper cajaTextoM2 w120" id="txtApPaternoHijos" ></s:textfield></td>
			
			<td><s:text name="midas.fuerzaventa.negocio.apMaterno"/></td>
			<td class="w120"><s:textfield cssClass="QtoUpper cajaTextoM2 w120" id="txtApMaternoHijos"></s:textfield></td>
		</tr>		
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/></td>
			<td>
				<sj:datepicker readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaNacimientoHijo" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td><s:text name="midas.fuerzaventa.negocio.ultimaEscolaridad"/></td>
			<td>
				<s:textfield cssClass="jQtoMayusc cajaTextoM2 w120" id="txtUltimaEscolaridad" ></s:textfield>
			</td>
			<td colspan="2">
				<div align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="if(validateForId('agenteHijosForm',true)){agregarHijoTemp();} return false;">
							<s:text name="midas.boton.agregar"/>
						</a>
					</div>	
				</div>				
			</td>	
		</tr>
		<tr>
			<td colspan="6">
				<div id="agenteHijosGrid" class="w750 h150" style="overflow:hidden">
				</div>
			</td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center"">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.conyuge"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.nombre" /> 
			</td>
			<td>
				<s:textfield name="altaPorInternet.nombresConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtNombreConyuge"></s:textfield>
			</td>
			
			<td>
				<s:text name="midas.fuerzaventa.negocio.apPaterno" />
			</td>
			<td>
				<s:textfield name="altaPorInternet.apellidoPaternoConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtApPaternoConyuge"></s:textfield>
			</td>
			
			<td>
				<s:text name="midas.fuerzaventa.negocio.apMaterno" />
			</td>
			<td>
				<s:textfield name="altaPorInternet.apellidoMaternoConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtApMaternoConyuge"></s:textfield>
			
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/>
			</td>
			<td>
				<sj:datepicker name="altaPorInternet.fechaNacimientoConyuge" readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaNacimeientoConyugue" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.aniversarioBoda"/>
			</td>
			<td>
				<sj:datepicker name="altaPorInternet.fechaMatrimonio" readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaMatrimonio" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		</tr>
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.catalogo.agente.propiedades"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.tipoVehiculo"/>
			</td>
			<td>
				<s:textfield name="altaPorInternet.tipoVehiculo" id="txtTipoVehiculo" cssClass="cajaTextoM2 jQtoUpper w500"></s:textfield>
			</td>
		</tr>
	</table>
	<s:form action="listarFiltrado" id="agenteEntretenimientoForm">	
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.negocio.entretenimiento"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.entretenimiento"/>
			</td>
			<td>
				<s:select id="txtTipoEntretenimiento" disabled="#readOnly" listKey="id" listValue="valor" list="listTipoEntretenimiento" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					headerValue="%{getText('midas.general.seleccione')}" cssClass="cajaTextoM2 w500"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.detalle"/>
			</td>
			<td>
				<s:textfield readonly="#readOnly"  id="txtComentariosEntretenimiento" cssClass="cajaTextoM2 jQtoUpper w500"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="if(validateForId('agenteEntretenimientoForm',true)){agregarEntretenimientoTem();} return false;">
							<s:text name="midas.boton.agregar"/>
						</a>
					</div>	
				</div>				
			</td>	
		</tr>
		<tr>
			<td colspan="6">
				<div id="gridEntretenimientosAgente"  class="w450 h150"></div>		
			</td>
		</tr>
	</table>
	</s:form>
<div align="right" class="w870 inline" >
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_agregar"
				onclick="javascript:guardarAgenteInternet();">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
			</a>
		</div>	
	</div>
</s:form>