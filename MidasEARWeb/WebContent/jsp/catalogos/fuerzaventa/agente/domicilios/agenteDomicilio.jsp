<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
	<s:set id="display" value="true" />
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />
	<s:set id="display" value="false" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>		
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />
	<s:set id="display" value="false" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="display" value="true" />
</s:elseif>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:form action="guardarDomicilioAgente" id="guardarDomicilioAgente">
<s:hidden name="agente.id" />
<s:hidden name="tipoAccion"/>
<s:hidden name="tabActiva" value="domicilios"/>
<s:hidden name="moduloOrigen"/>
<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="midas.fuerzaventa.negocio.tituloDomicilioPersonal"/>
			</td>
		</tr>
		<tr>
			<td>
<%-- 				<s:hidden name="agente.persona.domicilio[0].idDomicilio"></s:hidden> --%>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					
					<s:param name="idPaisName">agente.persona.domicilios[0].clavePais</s:param>
					<s:param name="idEstadoName">agente.persona.domicilios[0].claveEstado</s:param>	
					<s:param name="idCiudadName">agente.persona.domicilios[0].claveCiudad</s:param>		
					<s:param name="idColoniaName">agente.persona.domicilios[0].nombreColonia</s:param>
					<s:param name="calleNumeroName">agente.persona.domicilios[0].calleNumero</s:param>
					<s:param name="cpName">agente.persona.domicilios[0].codigoPostal</s:param>				
					<s:param name="labelPais">País</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>	
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">3</s:param>
					<s:param name="requerido">1</s:param>	
					<s:param name="idDomicilioName">idDomicilio0</s:param>    
					<s:param name="funcionResult" >populateDomicilioAgente0</s:param>  	
					<s:param name="readOnly" value="true"></s:param>	
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
				</s:action>
			<td>
		</tr>	
	</table>	
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo">
				<s:text name="midas.fuerzaventa.negocio.tituloDomicilioOficina"/>
			</td>
		</tr>
		<tr>
			<td>
<%-- 				<s:hidden name="agente.persona.domicilio[1].idDomicilio"></s:hidden> --%>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					
					<s:param name="idPaisName">agente.persona.domicilios[1].clavePais</s:param>
					<s:param name="idEstadoName">agente.persona.domicilios[1].claveEstado</s:param>	
					<s:param name="idCiudadName">agente.persona.domicilios[1].claveCiudad</s:param>		
					<s:param name="idColoniaName">agente.persona.domicilios[1].nombreColonia</s:param>
					<s:param name="calleNumeroName">agente.persona.domicilios[1].calleNumero</s:param>
					<s:param name="cpName">agente.persona.domicilios[1].codigoPostal</s:param>				
					<s:param name="labelPais">País</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>	
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">3</s:param>
					<s:param name="requerido">1</s:param>	
					<s:param name="idDomicilioName">idDomicilio1</s:param>    
					<s:param name="funcionResult" >populateDomicilioAgente1</s:param>  
					<s:param name="readOnly" value="true"></s:param>		
					<s:param name="enableSearchButton" value="%{#display}"></s:param>
				</s:action>
			<td>
		</tr>
		<tr>
			<td>
			    <s:if test="TipoAccion != 5">
				    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
				</s:if>
			</td>
		</tr>		
	</table>
	<div class="w870 inline" align="right">
<%-- 		<s:if test="tipoAccion == 1 || tipoAccion == 4"> --%>
<!-- 		<div class="btn_back w110"> -->
<!-- 			<a href="javascript: void(0);" class="icon_guardar ."  -->
<!-- 				onclick="guardaDomicilioAgente();"> -->
<%-- 				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/> --%>
<!-- 			</a> -->
<!-- 		</div> -->
<%-- 		</s:if>	 --%>
		<s:if test="tipoAccion != 5">
		    <div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="atrasOSiguiente('info_general');">
					<s:text name="midas.boton.atras"/>
				</a>
		    </div>
		    <div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="atrasOSiguiente('datosFiscales');">
					<s:text name="midas.boton.siguiente"/>
				</a>
		    </div>
		</s:if>		
	</div>
</s:form>

