<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- <style type="text/css"> --%>
<!-- 	select { width: 150px; /* Or whatever width you want. */ }  -->
<!-- 	select.expand { width: auto; }  -->
<%-- </style> --%>

<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>
<script type="text/javascript">
	var gridAgentes=null;
	var tipoAccionAgente='<s:property value="tipoAccion"/>';
	var noCerrarModal = '<s:property value="closeModal"/>';
	var varModal='agenteModal';
	var idFieldAgente = '<s:property value ="idField"/>';
	if(noCerrarModal=='No'){
		 varModal='';
	}
	jQuery(function(){
		//listarAgente();
		//initPagingGrid(urlFiltro,"agenteGrid", null,idField,'agenteModal',pagingCount);
	 	pageGridPaginadoAgente(1,true,true);
	 	
	 	/* se limita el combo de tipo de situacion a que solo muestre los agentes autorizados cuando el listado sea 
	 	   abierto como modal y el tipo de accion se de consulta
	 	  (se hizo para la herencia de clientes el cual manda el id del tipo de situacion)*/
	 	if(tipoAccionAgente=="consulta" && jQuery("#tipoSituacionAgente :selected").text()=="AUTORIZADO"){	 		
	 		var valor = jQuery("#tipoSituacionAgente :selected").val();
	 		var texto = jQuery("#tipoSituacionAgente :selected").text();
	 		jQuery("#tipoSituacionAgente").html("<option value='"+valor+"'>"+texto+"</option>");
	 	}
	 	
	 });
	
	function checkUncheckAlls() {
		var sel = jQuery("#checkAll").attr("checked");
		if (sel) {
			reloadGrid();
			grid.checkAll(true);
		} else {
			grid.getCheckedRows(1);
			grid.checkAll(false);
		}
	}
	
	
	function reloadGrid() {
		gridAgentes.filterBy(0, 0);
	}

	
	function agregarall() {
		var ids = gridAgentes.getCheckedRows(0);
		if (ids != "") {
			var countSelected = ids.split(",");
			var idRow = "";
			if (countSelected.length > 0) {
				for (i = 1; i <= countSelected.length; i++) {
					if (i != countSelected.length) {
						idRow += grid.cellById(countSelected[i - 1], 1)
								.getValue()
								+ ",";
					} else {
						idRow += grid.cellById(countSelected[i - 1], 1)
								.getValue();
					}
				}
				if (idRow != "") {
					if (confirm("\u00BFEsta seguro de relacionar al negocio?")) {
						var idField = 'idNuevoAgente';
						var idDom = null;
						var componentSearch = jQuery.getObject(idField);
						var nombreField = "nombre" + idField;
						var domField = "dom" + idField;
						var nombreComponentSearch = jQuery
								.getObject(nombreField);
						var domComponentSearch = jQuery.getObject(domField);

						if (jQuery.getObject(idField).html() != null) {
							componentSearch.val(idRow);
							nombreComponentSearch.val(nombre);
							domComponentSearch.val(idDom);
							//alert("Se ha seleccionado el registro con la clave:"+id);
							componentSearch.change();
						} else {
							parent.jQuery.getObject(nombreField).val(nombre);
							parent.jQuery.getObject(domField).val(idDom);
							parent.jQuery.getObject(idField).val(idValue);
							parent.jQuery.getObject(idField).change();
						}
						if (varModal) {
							closeDhtmlxWindows(varModal);
						}
						//registrarNuevoAgente(idRow);
					}
				}
			}
		} else {
			alert("No existe ningun registro marcado");
		}
	}

	function listarAgente(cargaInicial) {
		//var urlPath=(cargaInicial==true)?	listarAgentePath:listarFiltradoAgentePath;
		var urlFiltro = listarFiltradoAgentePath;//+"?tipoAccion="+tipoAccionAgente;
		var urlFil = "";
		// 		var urlFil=(cargaInicial==true)?urlFiltro+"&totalCount=0":urlFiltro;//Si es carga inicial muestra 0 registros
		if (cargaInicial == true) {
			urlFil = urlFiltro + "?tipoAccion=" + tipoAccionAgente
					+ "&totalCount=0";
		} else {
			urlFil = urlFiltro;

			jQuery("#btnagregarall").css("display", "block");
			jQuery("#checkagregarall").css("display", "block");
			jQuery("#checkAll").attr("checked", true);

		}
		//var posPath = 'posActual='+jQuery("#posActual").val() + '&'+ jQuery(document.paginadoGridForm).serialize();
		//urlFiltro+="&"+posPath;
		gridAgentes = listarFiltradoGenerico(urlFil, "agenteGrid",
				jQuery("#agenteForm"), idFieldAgente, varModal);
	}
	function mostrarModalCargaMasiva() {
		var url = "/MidasWeb/fuerzaventa/agente/mostrarCargaMasiva.action";
		sendRequestWindow(null, url, obtenerVentanacargaMAsivaAgente);
	}
	
	function obtenerVentanacargaMAsivaAgente() {
		var wins = obtenerContenedorVentanas();
		ventanaCargaMasiva = wins.createWindow("responsableModal", 250, 170,
				500, 300);
		ventanaCargaMasiva.center();
		ventanaCargaMasiva.setModal(true);
		ventanaCargaMasiva.setText("Carga Masiva de Agente");
		ventanaCargaMasiva.button("park").hide();
		ventanaCargaMasiva.button("minmax1").hide();
		return ventanaCargaMasiva;
	}
</script>
<s:form action="listarFiltrado" id="agenteForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.titulo"/>
			</td>
		</tr>
		<tr>	
			<th>
				<s:text name="midas.fuerzaventa.negocio.numeroAgente"></s:text>
			</th>	 
			<td>
				<s:textfield name="filtroAgente.idAgente" id="txtClave" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>
				<s:text name="midas.fuerzaventa.negocio.nombreAgente"></s:text>
			</th>	 
			<td>
				<s:textfield name="filtroAgente.persona.nombreCompleto" id="txtIdAgente" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>			
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaInicio"></s:text>
			</th>	 
			<td>
				<sj:datepicker name="filtroAgente.fechaAlta" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroAgente.fechaAlta" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAlta" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAltaFin"></s:text>
			</th>	 
			<td>
				<sj:datepicker name="filtroAgente.fechaAltaFin" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroAgente.fechaAltaFin" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAltaFin" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>
		</tr>
		<tr class="JS_hide">
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAutorizacionInicio"></s:text>
			</th>	 
			<td>
				<sj:datepicker name="filtroAgente.fechaAutorizacionAgenteInicio" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeMonth="true" changeYear="true"
					id="filtroAgente.fechaAutorizacionRechazo" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAutorizacionFianza" id="txtIdAgente" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>			
			<th>
				<s:text name="midas.fuerzaventa.negocio.fechaAutorizacionFin"></s:text>
			</th>
			<td>
				<sj:datepicker name="filtroAgente.fechaAutorizacionAgenteFin" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeMonth="true" changeYear="true"
					id="filtroAgente.fechaAutorizacionRechazoFin" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
<%-- 				<s:textfield name="filtroAgente.fechaAutorizacionFianzaFin" id="txtIdAgente" cssClass="cajaTextoM2 w200"></s:textfield> --%>
			</td>
		</tr>
		<tr class="JS_hide">
			<th>				
				<s:text name="midas.fuerzaventa.ejecutivo.gerencia" />
			</th>
			<td>
				<s:select id="idGerencia" name="filtroAgente.promotoria.ejecutivo.gerencia.id" value="idGerencia" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivoCatalogo','idPromotoriaCatalogo',this.value)" cssClass="cajaTextoM2 jQrequired w200 wide"/>
			</td>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text></th>
			<td>
				<s:select id="idEjecutivoCatalogo" name="filtroAgente.promotoria.ejecutivo.id" value="idEjecutivo" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoriaCatalogo',this.value)" cssClass="cajaTextoM2 jQrequired w200 wide"/>
			</td>			
		</tr>
		<tr class="JS_hide">
			<th><s:text name="midas.fuerzaventa.negocio.promotoria"></s:text></th>
			<td>
				<s:select id="idPromotoriaCatalogo" name="filtroAgente.promotoria.id" value="filtroAgente.promotoria.id" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 jQrequired w200 wide"/>
			</td>
			<th>				
				<s:text name="midas.catalogos.centro.operacion.situacion" />
			</th>
			<td>
				<s:select name="filtroAgente.tipoSituacion.id" id="tipoSituacionAgente" cssClass="cajaTextoM2 w250 wide" disabled="#readOnly" 
				       headerKey="" headerValue="Seleccione.." 
				      list="catalogoTipoSituacion" listKey="id" listValue="valor"/>				      
			</td>
		</tr>
		<tr class="JS_hide">	
			<th><s:text name="midas.fuerzaventa.negocio.rfc"></s:text></th>	 
			<td><s:textfield name="filtroAgente.persona.rfc" id="txtClave" cssClass="cajaTextoM2 w200 jQRFC"></s:textfield></td>
<%-- 			<th><s:text name="midas.fuerzaventa.negocio.razonSocial"></s:text></th>       --%>
<%-- 			<td><s:textfield name="filtroAgente.persona.razonSocial" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td> --%>
			<th><s:text name="midas.fuerzaventa.negocio.telefono"></s:text></th>
			<td><s:textfield name="filtroAgente.persona.telefonoOficina" id="txtClave" cssClass="cajaTextoM2 w200"></s:textfield></td>
		</tr>
		<tr>
		<td class="JS_hide" colspan="4">
		<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >			
			<s:param name="idPaisName">filtroAgente.persona.domicilios[0].clavePais</s:param>
			<s:param name="idEstadoName">filtroAgente.persona.domicilios[0].claveEstado</s:param>	
			<s:param name="idCiudadName">filtroAgente.persona.domicilios[0].claveCiudad</s:param>
			<s:param name="idColoniaName">filtroAgente.persona.domicilios[0].nombreColonia</s:param>
			<s:param name="calleNumeroName">filtroAgente.persona.domicilios[0].calleNumero</s:param>
			<s:param name="cpName">filtroAgente.persona.domicilios[0].codigoPostal</s:param>
			<s:param name="labelPais">Pais</s:param>				
			<s:param name="labelEstado"><s:text name="midas.catalogos.centro.operacion.estado"/></s:param>
			<s:param name="labelColonia"><s:text name="midas.catalogos.centro.operacion.colonia"/></s:param>
			<s:param name="labelCodigoPostal"><s:text name="midas.catalogos.centro.operacion.codigoPostal"/></s:param>
			<s:param name="labelCalleNumero"><s:text name="midas.catalogos.centro.operacion.calleYNumero"/></s:param>
			<s:param name="labelCiudad"><s:text name="midas.catalogos.centro.operacion.municipio"/></s:param>
			<s:param name="labelPosicion">left</s:param>
			<s:param name="componente">6</s:param>						
		</s:action>
		</td>
		</tr>
		<tr>
			<td>
				<div style="display: block" id="masFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('masFiltros');">
						<s:text name="midas.boton.masFiltros"/>
					</a>
				</div>
				<div style="display:none" id="menosFiltros">
					<a href="javascript: void(0);"
						onclick="toggle_Hidden();ocultarMostrarBoton('menosFiltros');">
						<s:text name="midas.boton.menosFiltros"/>
					</a>
				</div>
			</td>
			<td colspan="5">
				<div class="btn_back w110">
					<!-- 
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="listarFiltradoGenerico(listarFiltradoAgentePath,'agenteGrid',document.agenteForm,'<s:property value="idField"/>','agenteModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
					-->
					<a href="javascript: void(0);" id="agenteBuscar" class="icon_buscar"
						onclick="pageGridPaginadoAgente(1,true,false);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<s:if test="tipoAccion==\"consultaNegAge\"">
				<td width="200px">
					<div style="display: none" id="checkagregarall">
						<s:checkbox name="checkAll" id="checkAll" value="false"
							checked="true" label="Seleccionar Todos"
							onclick="	checkUncheckAlls();" labelposition="left" />
					</div>
				</td>
				<td>
					<div style="display: none" id="btnagregarall">
						<div class="btn_back w110" id="btnAgregar">
							<a href="javascript: void(0);" onclick="agregarall();"> <s:text
									name="midas.boton.agregar" /> a Todos
							</a>
						</div>
					</div>
				</td>
			</s:if>

		</tr>
	</table>
	<br>
	<!-- 
		<div id="divCarga" style="position:absolute;"></div>
	-->
	<div id="divCarga" style="display:inline-block;float:left;position:absolute;z-index:100;"></div>
	<!-- 
	<div class="w880 dataGridGenericStyle">
		<div id="agenteGrid" class="w880 h200" style="overflow:hidden"></div>
	</div>
	-->
	<div id="indicador"></div>
	<div id="gridAgentePaginado"  class="w880 h270 dataGridGenericStyle">
		<div id="agenteGrid" class="w870 h260" style="overflow:hidden"></div>
	</div>
	<!-- div id="pagingArea"></div>
	<div id="infoArea"></div--!>
<!-- 	carga masiva -->
	<s:if test="tipoAccion!=\"consulta\"">
		<table>
			<tr>
				<td>	
					<div class ="w720" align="right">
						<div class="btn_back w180">
							<a href="javascript: void(0);" class="icon_guardar ." 
								onclick="mostrarModalCargaMasiva();">
								<s:text name="midas.agentes.cargaMasiva.cargaMasivaAgentes"/>
							</a>
						</div>	
					</div>
					</td>
					<td>
				<!-- 	carga masiva -->
					<!-- Se checa si es solo consulta, si es asi, no debe de agregar -->
					<div class ="w100" align="right">
						<div class="btn_back w100">
							<a href="javascript: void(0);" class="icon_guardar ." 
								onclick="agregarAgente(1);">
								<s:text name="midas.boton.agregar"/>
							</a>
						</div>	
					</div>
				</td>
			</tr>
		</table>
	</s:if>
</s:form>
