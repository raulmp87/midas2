<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="id" type="ro" width="*" hidden="true" sort="int" align="center"><s:text name="midas.negocio.nombre"/></column>
		<column id="nombreHijo" type="ro" width="*" sort="int" align="center"><s:text name="midas.negocio.nombre"/></column>
		<column id="apellidoPatHijo" type="ro" width="*" sort="int" align="center"><s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/></column>
		<column id="apellidoMatHijo" type="ro" width="*" sort="int" align="center"><s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/></column>
		<column id="fechaNacimiento" type="ro" width="*" sort="date" align="center"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.fechaNacimiento"/></column>
		<column id="escolaridad" type="ro" width="*" sort="int" align="center"><s:text name="midas.fuerzaventa.negocio.ultimaEscolaridad"/></column>
		<s:if test="tipoAccion!=\"2\" && tipoAccion!=\"3\" && tipoAccion!=\"5\"">	
			<column id="accionBorrar" type="ro" width="69" sort="str">Acciones</column>
		</s:if>
	</head>
	<s:iterator value="listHijoAgentes" var="hijo" status="index">
		<row id="${index.count}">
			<cell>${hijo.id}</cell>
			<cell>${hijo.nombres}</cell>
			<cell>${hijo.apellidoPaterno}</cell>
			<cell>${hijo.apellidoMaterno}</cell>			
			<cell>${hijo.fechaNacimientoString}</cell>
			<cell>${hijo.ultimaEscolaridad}</cell>
			<cell><![CDATA[<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdataHijo(${index.count},${hijo.id});'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>]]></cell>
		</row>
	</s:iterator>
</rows>