<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript">
cargarGridHijos();
cargarGridEntretenimiento();
</script>
<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
</s:if>
<s:elseif test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
</s:elseif>
<s:elseif test="tipoAccion == 5">	
	<s:set id="readOnly" value="true" />	
</s:elseif>
<s:form name="guardarDatosExtraAgenteForm" id="guardarDatosExtraAgenteForm">	
<s:hidden name="agente.id" id="agente.id"/>
<s:hidden name="tipoAccion"/>
<s:hidden name="tabActiva" value="datosExtra"/>
<s:hidden name="moduloOrigen"/>
															<!-- Datos Extra -->
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.hijos"/>
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.nombre"/></td>
			<td><s:textfield cssClass="jQtoUpper cajaTextoM2 w120" id="txtNombreHijos" readonly="#readOnly"></s:textfield></td>
			
			<td><s:text name="midas.fuerzaventa.negocio.apPaterno"/></td>
			<td><s:textfield cssClass="jQtoUpper cajaTextoM2 w120" id="txtApPaternoHijos" readonly="#readOnly"></s:textfield></td>
			
			<td><s:text name="midas.fuerzaventa.negocio.apMaterno"/></td>
			<td><s:textfield cssClass="QtoUpper cajaTextoM2 w120" id="txtApMaternoHijos" readonly="#readOnly"></s:textfield></td>
		</tr>		
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/></td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<sj:datepicker readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaNacimientoHijo" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</s:if>	
			<s:else>
				<s:textfield id="txtFechaNacimientoHijo" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>
			</td>
			<td><s:text name="midas.fuerzaventa.negocio.ultimaEscolaridad"/></td>
			<td>
				<s:textfield cssClass="jQtoMayusc cajaTextoM2 w120" id="txtUltimaEscolaridad" readonly="#readOnly"></s:textfield>
			</td>
			<td></td>
			<td>
				<div>
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="if(validateForId('agenteHijosForm',true)){agregarHijoTemp();} return false;">
							<s:text name="midas.boton.agregar"/>
						</a>
					</div>	
				</s:if>	
				</div>				
			</td>	
		</tr>
		<tr>
			<td colspan="6">
				<div id="agenteHijosGrid" class="w750 h150" style="overflow:hidden">
				</div>
			</td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center"">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.conyuge"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.nombre" /> 
			</td>
			<td>
				<s:textfield name="agente.nombresConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtNombreConyuge" readonly="#readOnly"></s:textfield>
			</td>
			
			<td>
				<s:text name="midas.fuerzaventa.negocio.apPaterno" />
			</td>
			<td>
				<s:textfield name="agente.apellidoPaternoConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtApPaternoConyuge" readonly="#readOnly"></s:textfield>
			</td>
			
			<td>
				<s:text name="midas.fuerzaventa.negocio.apMaterno" />
			</td>
			<td>
				<s:textfield name="agente.apellidoMaternoConyuge" cssClass="jQtoUpper cajaTextoM2 w150" id="txtApMaternoConyuge" readonly="#readOnly"></s:textfield>
			
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.fechaNacimiento"/>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<sj:datepicker name="agente.fechaNacimientoConyuge" readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaNacimeientoConyugue" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</s:if>	
			<s:else>
				<s:textfield name="agente.fechaNacimientoConyuge" id="txtFechaNacimeientoConyugue" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.aniversarioBoda"/>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<sj:datepicker name="agente.fechaMatrimonio" readonly="#readOnly" buttonImage="../img/b_calendario.gif"
				   id="txtFechaMatrimonio" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx"								   								  
				   onkeypress="return soloFecha(this, event, false);"
				   changeYear="true" changeMonth="true"
				   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   onblur="esFechaValida(this);">
				</sj:datepicker>
			</s:if>	
			<s:else>
				<s:textfield name="agente.fechaMatrimonio" id="txtFechaMatrimonio" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>
			</td>
		</tr>
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.catalogo.agente.propiedades"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.tipoVehiculo"/>
			</td>
			<td>
				<s:textfield name="agente.tipoVehiculo" id="txtTipoVehiculo" cssClass="cajaTextoM2 jQtoUpper w500" readonly="#readOnly"></s:textfield>
			</td>
		</tr>
	</table>	
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.negocio.entretenimiento"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.entretenimiento"/>
			</td>
			<td>
				<s:select id="txtTipoEntretenimiento" disabled="#readOnly" listKey="id" listValue="valor" list="listTipoEntretenimiento" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					headerValue="%{getText('midas.general.seleccione')}" cssClass="cajaTextoM2 w500"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.detalle"/>
			</td>
			<td>
				<s:textfield readonly="#readOnly"  id="txtComentariosEntretenimiento" cssClass="cajaTextoM2 jQtoUpper w500"></s:textfield>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="right">
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="if(validateForId('agenteEntretenimientoForm',true)){agregarEntretenimientoTem();} return false;">
							<s:text name="midas.boton.agregar"/>
						</a>
					</div>	
				</s:if>	
				</div>				
			</td>	
		</tr>
		<tr>
			<td colspan="6">
				<div id="gridEntretenimientosAgente"  class="w450 h150"></div>		
			</td>
		</tr>
		<tr>
			<td>
				<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
			</td>
		</tr>	
	</table>
	<div align="right" class="w870 inline" >
		<s:if test="tipoAccion == 1 || tipoAccion == 4">
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_agregar"
				onclick="javascript:guardarDatosExtraAgente();">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
			</a>
		</div>
		</s:if>
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:atrasOSiguiente('datosContables');">
					<s:text name="midas.boton.atras"/>
				</a>
			</div>	
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:atrasOSiguiente('documentos');">
					<s:text name="midas.boton.siguiente"/>
				</a>
			</div>
		</div>
</s:form>