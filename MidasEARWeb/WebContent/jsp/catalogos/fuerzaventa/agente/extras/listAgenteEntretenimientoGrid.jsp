<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
        <column id="id" type="ro" width="0" sort="int" hidden="true"></column>
        <column id="idEntretenimiento" type="ro" width="0" sort="int" hidden="true"></column>
        <column id="TipoEntrenamiento" type="ro" width="*" sort="int" align="center">Entretenimiento</column>
		<column id="comentario" type="ro" width="*" sort="int" align="center"><s:text name="midas.catalogos.descripcion"/></column>
		<s:if test="tipoAccion!=\"2\" && tipoAccion!=\"3\" && tipoAccion!=\"5\"">	
			<column id="accionBorrar" type="ro" width="69" sort="str">Acciones</column>
		</s:if>
	</head>
	<s:iterator value="listEntretenimiento" var="entretenimiento" status="index">
		<row id="${index.count}">			
			<cell>${entretenimiento.id}</cell>
			<cell>${entretenimiento.tipoEntretenimiento.id}</cell>
			<cell>${entretenimiento.tipoEntretenimiento.valor}</cell>
			<cell>${entretenimiento.comentarios}</cell>
			<cell><![CDATA[<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdataEntretenimiento(${index.count},${entretenimiento.id});'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>]]></cell>
		</row>
	</s:iterator>
</rows>