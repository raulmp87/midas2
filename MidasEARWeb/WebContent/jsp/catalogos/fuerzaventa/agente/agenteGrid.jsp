<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <!--  -->   
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			 <!-- -->         
        </beforeInit>
        <afterInit>
        </afterInit> 
        <s:if test="tipoAccion==\"consultaNegAge\"">
			<column id="idchecke" type="ch" width="35" sort="int"></column>
		</s:if>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="midas.negocio.producto.id"/></column>
        <column id="idAgente" type="ro" width="60" sort="int" >Clave</column>
        <column id="nombreAgente" type="ro" width="300" sort="str"><s:text name="midas.fuerzaventa.negocio.nombre"/></column>
        <column id="RFCAgente" type="ro" width="150" sort="str"><s:text name="midas.fuerzaventa.negocio.rfc"/></column>
        <column id="tipoCedulaAgente" type="ro" width="300" sort="str"><s:text name="midas.fuerzaventa.agente.tipoCedula"/></column>
        <column id="ejecutivoAgente" type="ro" width="300" sort="str"><s:text name="midas.fuerzaventa.negocio.ejecutivo"/></column>
        <column id="promotoriaAgente" type="ro" width="300" sort="str"><s:text name="midas.fuerzaventa.ejecutivo.promotoria"/></column>
		
		<column id="situacion" type="ro" width="150" sort="str"><s:text name="midas.catalogos.centro.operacion.situacion"/></column>
		<s:if test="tipoAccion!=\"consulta\"">	
 			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
			<column id="accionEditar" type="img" width="30" sort="na" align="center"/>
			<column id="accionBorrar" type="img" width="30" sort="na" align="center"/>
		</s:if>
	</head>
	<s:iterator value="listaAgente" var="agenteX" status="index">
		<row id="${index.count}">
			<s:if test="tipoAccion==\"consultaNegAge\"">	
			<cell><![CDATA[${agenteX.id}]]></cell>
				</s:if>
			<cell><![CDATA[${agenteX.id}]]></cell>
			<cell><![CDATA[${agenteX.idAgente}]]></cell>						
			<cell><![CDATA[${agenteX.nombreCompleto}]]></cell>
			<cell><![CDATA[${agenteX.codigoRfc}]]></cell>
			<cell><![CDATA[${agenteX.tipoCedulaAgente}]]></cell>
			<cell><![CDATA[${agenteX.ejecutivo}]]></cell>
			<cell><![CDATA[${agenteX.promotoria}]]></cell>
			<cell><![CDATA[${agenteX.tipoSituacion}]]></cell>
			<s:if test="tipoAccion!=\"consulta\" && tipoAccion!=\"consultaNegAge\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleAgentePath, 2,{"agente.id":${agenteX.id},"idRegistro":${agenteX.id},"idTipoOperacion":50})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleAgentePath, 4,{"agente.id":${agenteX.id},"idRegistro":${agenteX.id},"idTipoOperacion":50})^_self</cell>				
				<s:if test="tipoSituacion!=\"INACTIVO\"">
				<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleAgentePath, 3,{"agente.id":${agenteX.id},"idRegistro":${agenteX.id},"idTipoOperacion":50})^_self</cell>
				</s:if>
			</s:if>
		</row>
	</s:iterator>
</rows>