<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<script type="text/javascript">
jQIsRequired();
</script>
<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
</s:elseif>

<s:set var="readOnly" value="true"/>
<s:set var="required" value="1"/>
<s:set id="display" value="false" />	


<s:form action="guardarDatosFiscales" id="guardarDatosFiscalesForm">
<s:hidden name="agente.id" />
<s:hidden name="tipoAccion"/>
<s:hidden name="tabActiva" value="datosFiscales"/>
<s:hidden name="moduloOrigen"/>
<!-- Hiddens para pasar el  domicilio personal mas reciente -->
<s:hidden id="persoPais" name="agente.persona.domicilios[1].clavePais" />
<s:hidden id="persoEstado" name="agente.persona.domicilios[1].claveEstado" />
<s:hidden id="persoCiudad" name="agente.persona.domicilios[1].claveCiudad" />
<s:hidden id="persoColonia" name="agente.persona.domicilios[1].nombreColonia" />
<s:hidden id="persoCalleNumero" name="agente.persona.domicilios[1].calleNumero" />
<s:hidden id="persoCP" name="agente.persona.domicilios[1].codigoPostal" />
<s:set name="peronalidadJueidica" value="%{agente.persona.claveTipoPersona}"></s:set>
<table  width="98%" class="contenedorFormas" align="center">	
	<tr>
		<td class="titulo" colspan="6">
			<s:text name="midas.fuerzaventa.negocio.datosFiscales"/>
		</td>
	</tr>
	<s:if test="agente.persona.claveTipoPersona == 1">
	<tr>
		<td class="w100"><s:text name="midas.fuerzaventa.negocio.nombre"/></td>
		<td><s:textfield name="agente.persona.nombre" readonly="true" cssClass="cajaTextoM2 w150" cssErrorStyle="errorField" id="txtNombre"></s:textfield></td>
		
		<td class="w100"><s:text name="midas.fuerzaventa.negocio.apPaterno"/></td>
		<td><s:textfield name="agente.persona.apellidoPaterno" readonly="true" cssClass="cajaTextoM2 w150" id="txtApPaterno"></s:textfield></td>
		
		<td class="w100"><s:text name="midas.fuerzaventa.negocio.apMaterno"/></td>
		<td><s:textfield name="agente.persona.apellidoMaterno" readonly="true" cssClass="cajaTextoM2 w150" id="txtApMaterno"></s:textfield></td>
	</tr>
	</s:if>
	<s:else>
		<tr>
			<td class="w100 jQIsRequired"><s:text name="midas.fuerzaventa.negocio.razonSocial"/></td>
			<td colspan ="5"><s:textfield name="agente.persona.razonSocial" readonly="true" cssClass="cajaTextoM2 w380 jQrequired" id="txtRazonSocial"></s:textfield></td>
		</tr>
	</s:else>
	
	<tr>
<!-- 		<td>Copiar Domicilio Personal</td> -->
<!-- 		<td> -->
<!-- 			<div class="btn_back w70"> -->
<!-- 				<a href="javascript: void(0);" class="icon_copiar" onclick="javascript: copiarDomicilioPersonal();"> -->
<!-- 					copiar -->
<!-- 				</a> -->
<!-- 			</div> -->
<!-- 		</td> -->
		
		<td class="jQIsRequired"><s:text name="midas.agentes.afianzadora.personalidadJuridica"></s:text></td>
		<td><s:radio list="#{'1':'Fisica', '2':'Moral'}" disabled="true" name="agente.persona.claveTipoPersona" onclick="javascript: void(0);" ></s:radio></td>
	
		<td class="w100 jQIsRequired"><s:text name="midas.fuerzaventa.negocio.rfc"/></td>
		<td><s:textfield name="agente.persona.rfc" readonly="true" cssClass="cajaTextoM2 w150 jQrequired" id="txtRfc" ></s:textfield></td>
	</tr>
	<tr>
		<td colspan="6">&nbsp;</td>
	</tr>
	<s:if test="%{#peronalidadJueidica==2}"> 
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.altaPorInternet.contacOrepresentanteLeg"/>
			</td>
			<td>
				<s:textfield name="personaSeycosDTO.nombreCompleto" cssClass="w380 cajaTextoM2" readonly="true"/>
			</td>
		</tr>
	</s:if>
</table>
<table  width="98%" class="contenedorFormas" align="center">
	<tr>
		<td class="titulo">
			<s:text name="midas.boton.domicilio"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
				<s:param name="idPaisName">agente.persona.domicilios[0].clavePais</s:param>
				<s:param name="idEstadoName">agente.persona.domicilios[0].claveEstado</s:param>	
				<s:param name="idCiudadName">agente.persona.domicilios[0].claveCiudad</s:param>		
				<s:param name="idColoniaName">agente.persona.domicilios[0].nombreColonia</s:param>
				<s:param name="calleNumeroName">agente.persona.domicilios[0].calleNumero</s:param>
				<s:param name="cpName">agente.persona.domicilios[0].codigoPostal</s:param>
				<s:param name="nuevaColoniaName">agente.persona.domicilios[0].nuevaColonia</s:param>
				<s:param name="idColoniaCheckName">idColoniaCheck1</s:param>				
				<s:param name="labelPais">Pais</s:param>	
				<s:param name="labelEstado">Estado</s:param>
				<s:param name="labelCiudad">Municipio</s:param>
				<s:param name="labelColonia">Colonia</s:param>
				<s:param name="labelCalleNumero">Calle y Número</s:param>
				<s:param name="labelCodigoPostal">Código Postal</s:param>
				<s:param name="labelPosicion">left</s:param>
				<s:param name="componente">2</s:param>
				<s:param name="readOnly" value="%{#readOnly}"></s:param>
				<s:param name="requerido" value="%{#required}"></s:param>
				<s:param name="enableSearchButton" value="%{#display}"></s:param>
			</s:action>
		<td>
	</tr>
	<tr>
		<td>
		    <s:if test="tipoAccion != 5">
		        <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
		    </s:if>			
		</td>
	</tr>	
</table>
<div class="w870 inline" align="right">
    <s:if test="tipoAccion != 5">
		<div class="btn_back w80">
			<a href="javascript: void(0);"
				onclick="javascript:atrasOSiguiente('domicilios');">
				<s:text name="midas.boton.atras"/>
			</a>
		</div>
		<div class="btn_back w80">
			<a href="javascript: void(0);"
				onclick="javascript:atrasOSiguiente('datosContables');">
				<s:text name="midas.boton.siguiente"/>
			</a>
		</div>	
	</s:if>
</div>
</s:form>

