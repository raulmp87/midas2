<%@page language="java"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:form  method="post" action="/fuerzaventa/agente/downloadLayoutCargaMasiva.action" enctype="multipart/form-data" name="formularioDescargarLayout">
<br>
	<div class="btn_back w180">
		<a href="javascript: void(0);" class="icon_guardar ." 
			onclick="javascript: descargarArchivo();">
			<s:text name="midas.agentes.cargaMasiva.descargarLayout"/>
		</a>
	</div>	
	<br>	
</s:form>
<s:form  method="post" action="/fuerzaventa/agente/cargaMasiva.action" target="iframeProcessData" enctype="multipart/form-data" name="formularioAltaMasivaAgente"> <!-- target="&quot;cargaMasivaAgente.jsp&quot;" -->
	<table width="90%" align="center"  class="contenedorFormas no-border"><!--  -->
		<tr>
			<td>
				<s:file id="archivo" name="archivo" cssClass="cajaTextoM2 w300 jQrequired" label="File"/>
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w120">
					<a href="javascript:cargaMasivaAgente()" class="icon_guardar ." 
						onclick="">
						<s:text name="Cargar Archivo"/>
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agentes.cargaMasiva.resultadoCarga"/><br>
<%-- 				<s:textarea cols="60" rows="6" readonly="true" cssClass="cajaTextoM2"></s:textarea> --%>
				<iframe name="iframeProcessData"  height="100px" width="400px" style="border: 1; border-color: #A0E0A0;"></iframe>
			</td>
		</tr>
</table>
</s:form><!-- src="/MidasWeb/jsp/catalogos/fuerzaventa/agente/respuestaCargaMasivaAgente.jsp" -->