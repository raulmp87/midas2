<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<s:form action="listarFiltrado" id="clientesForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo">
				<s:text name="midas.fuerzaventa.negocio.listaClientesAgentes"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:select multiple="true" name="listaClientes" listValue="cliente.nombre" listKey="cliente.idCliente" cssClass="cajaTextoM w400 h190" list="listaClientes" labelposition="left"/>
			</td>
		</tr>
	</table>
</s:form>