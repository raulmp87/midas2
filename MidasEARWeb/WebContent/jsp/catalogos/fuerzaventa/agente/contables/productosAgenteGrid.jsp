<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
<%--			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     --%>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
        <column id="id" type="ro" width="0" sort="int" hidden="true"><s:text name="midas.negocio.producto.id"/></column>
        <column id="tipoProducto" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.tipoProducto"/></column>
        <column id="nombreId" type="ro" width="*" hidden="true" sort="str"><s:text name="productoBancario"/></column>
		<column id="producto" type="ro" width="130" sort="str"><s:text name="midas.catalogos.agente.productosAgente.producto"/></column>
		<column id="bancoNombre" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.banco"/></column>
		<column id="numeroCuenta" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.numeroCuenta"/></column>
		<column id="numeroClabe" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.numeroClabe"/></column>
		<column id="claveDefault" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.claveDefault"/></column>
		<column id="montoCredito" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.montoCredito"/></column>
		<column id="numeroPlazos" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.numeroPlazos"/></column>
		<column id="montoPago" type="ro" width="*" sort="int"><s:text name="midas.catalogos.agente.productosAgente.montoPago"/></column>
		<column id="sucursal" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.sucursal"/></column>
		<column id="plaza" type="ro" width="*" sort="str"><s:text name="midas.catalogos.agente.productosAgente.plaza"/></column>
		<s:if test="tipoAccion!=\"2\" && tipoAccion!=\"3\"&&tipoAccion!=\"5\"">	
			<column id="accionBorrar" type="ro" width="69" sort="str">Acciones</column>
		</s:if>
	</head>
	<s:iterator value="productosAgente" var="productoAgente" status="index">
		<row id="${index.count}">
			<cell>${productoAgente.id}</cell>			
			<s:if test="productoBancario.claveCredito==1">
				<cell><![CDATA[Credito]]></cell>
			</s:if>
			<s:else>
				<cell><![CDATA[Transferencia Electronica]]></cell>
			</s:else>
			<cell>${productoAgente.productoBancario.id}</cell>
			<cell>${productoAgente.productoBancario.descripcion}</cell>
			<cell>${productoAgente.productoBancario.banco.nombreBanco}</cell>
			<s:if test="#productoAgente.numeroCuenta!=0 && !(#productoAgente.numeroCuenta.isEmpty())">
				<cell>${productoAgente.numeroCuenta}</cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#productoAgente.numeroClabe!=0 && !(#productoAgente.numeroCuenta.isEmpty())">
				<cell>${productoAgente.numeroClabe}</cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#productoAgente.claveDefault==1">
				<cell>Si</cell>
			</s:if>
			<s:else>
				<cell>No</cell>
			</s:else>
			<s:if test="#productoAgente.montoCredito!=0">
				<cell>${productoAgente.montoCredito}</cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#productoAgente.numeroPlazos!=0">
				<cell>${productoAgente.numeroPlazos}</cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<s:if test="#productoAgente.montoPago!=0">
				<cell>${productoAgente.montoPago}</cell>
			</s:if>
			<s:else>
				<cell>N/A</cell>
			</s:else>
			<cell>${productoAgente.sucursal}</cell>
			<cell>${productoAgente.plaza}</cell>
			<cell><![CDATA[<a href='javascript: void(0);'onclick='javascript:removeRowConfirmdata(${index.count},${productoAgente.id},${productoAgente.claveDefault});'><img src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>]]></cell>
		</row>
	</s:iterator>
</rows>