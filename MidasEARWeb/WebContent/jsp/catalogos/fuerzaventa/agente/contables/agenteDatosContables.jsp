<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<script type="text/javascript">
jQIsRequired();
cargarGridDatosBancarios();
</script>
<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
</s:elseif>
<s:form action="listarFiltrado" id="agenteDatosContablesForm">
	<s:hidden name="agente.id" id="agente.id"/>
	<s:hidden name="tipoAccion"/>
	<s:hidden name="tabActiva" value="datosContables"/>
	<s:hidden name="moduloOrigen"/>
	<div id="datosAfianzadoraForm">
	<table width="97%" class="contenedorFormas" align="center">	
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.fuerzaventa.negocio.fianza"/>
			</td>
		</tr>	
		<tr>
			<td class="jQIsRequired" width="80">
				<s:text name="midas.fuerzaventa.negocio.numero"/>
			</td>
			<td>
				<s:textfield name="agente.numeroFianza" id="txtNumero" maxlength="15" cssClass="cajaTextoM2 w150 jQrestrict jQrequired" readonly="#readOnly"></s:textfield> <!-- key="midas.fuerzaventa.negocio.numero" labelposition="left" -->
			</td>
			<td class="jQIsRequired" width="80">
				<s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizacion"/>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<sj:datepicker name="agente.fechaAutorizacionFianza"  
								   buttonImage="../img/b_calendario.gif" 
								   id="fechaAutorizacion" maxlength="10" cssClass="cajaTextoM2 jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true"  
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onblur='esFechaValida(this);'
								   onchange="validaFechasDatosContables();"></sj:datepicker>
			</s:if>
			<s:else>
				<s:textfield name="agente.fechaAutorizacionFianza" id="fechaAutorizacion" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>								   
			</td>
			<td class="jQIsRequired" width="80">
				<s:text name="midas.fuerzaventa.altaPorInternet.vencimiento"/>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<sj:datepicker name="agente.fechaVencimientoFianza"  
								   buttonImage="../img/b_calendario.gif" 
								   id="fechaVencimiento" maxlength="10" cssClass="cajaTextoM2 jQrequired"								   								  
								   onkeypress="return soloFecha(this, event, false);" 
								   changeYear="true" changeMonth="true"  
								   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								   onblur='esFechaValida(this);'
								   onchange="validaFechasDatosContables();"></sj:datepicker>
			</s:if>
			<s:else>
				<s:textfield name="agente.fechaVencimientoFianza" id="fechaVencimiento" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>
			</td>
			
			
		</tr>
		<tr>
			<td width="90px" class="jQIsRequired" >
				<s:text name="midas.fuerzaventa.negocio.idAfianzadora"/>
			</td>
			<td>
				<s:textfield name="agente.afianzadora.id" id="idAfianzadora"  cssClass="cajaTextoM2 w50 jQnumeric jQrequired" readonly="true" onchange="javascript:findByIdAfianzadora(this.value);"></s:textfield>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<div class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarModalAfianzadora();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</s:if>
			</td>
			<td>
				<s:textfield name="agente.afianzadora.razonSocial" id="nombreAfianzadora" readonly="true" cssClass="cajaTextoM2 jQrequired w230"></s:textfield>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.negocio.montoFianza"/>
			</td>
			<td>
				<s:textfield name="agente.montoFianza" id="txtMontoFianza"  cssClass="cajaTextoM2 w100 jQfloat jQrequired jQrestrict" maxlength="12" readonly="#readOnly" onblur="validarCampoFianza(this)"></s:textfield> <!-- key="midas.fuerzaventa.negocio.montoFianza" labelposition="left" -->
			</td>
		</tr>		
	</table>
	</div>
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="10">
				<s:text name="midas.fuerzaventa.negocio.productosBancarios"/>
			</td>
		</tr>
		<tr>
			<td width="200" class="jQIsRequired" >
				<s:text name="midas.catalogos.agente.productosAgente.banco"/>
			</td>
			<td>
				<s:select name="productoBancarioSeleccionado.banco.idBanco" id="idBanco"  disabled="#readOnly"
				list="listaBancos" listKey="idBanco" listValue="nombreBanco" headerKey="" 
 				headerValue="Seleccione..."  cssClass="cajaTextoM2 w150" onchange="onChangeBanco();"></s:select>
			</td>
			<td class="jQIsRequired" >
				<s:text name="midas.catalogos.agente.productosAgente.producto"/>
			</td>
			<td>
				<s:select id="idProductoBancario" name="productoBancarioSeleccionado.id" list="productosBancarios" listKey="id" disabled="#readOnly"
				listValue="descripcion" headerKey="" headerValue="Seleccione..." cssClass="cajaTextoM2 w150" 
 				onchange="onChangeProductoBancario();"></s:select> 
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.catalogos.agente.productosAgente.tipoProducto"/>
			</td>
			<td>
				<s:textfield name="tipoProducto" id="tipoProducto" cssClass="cajaTextoM2" readonly="true"/>
			</td>
			<td>
				<s:text name = "midas.catalogos.agente.productosAgente.claveDefault"/>
			</td>
			<td>
				<s:checkbox id="claveDefault" name="producto.claveDefaultBoolean" disabled="#readOnly"/>
				<s:hidden name="checarClaveDefault" id="checarClaveDefault"></s:hidden>
			</td>
		</tr>
		<tr>
			<td id="numeroCuentaLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroCuenta"/>
			</td>
			<td>
				<s:textfield name="numeroCuenta" id="numeroCuenta" cssClass = "cajaTextoM2 jQnumeric jQrestrict" maxlength="20" readonly="#readOnly"/>
			</td>
			<td id="numeroClabeLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroClabe"/>
			</td>
			<td>
				<s:textfield name="numeroClabe" id="numeroClabe" cssClass = "cajaTextoM2 jQnumeric jQrestrict" maxlength="20" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td id="montoCreditoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoCredito"/>
			</td>
			<td>
				<s:textfield name="montoCredito" id="montoCredito" cssClass = "cajaTextoM2 jQfloat jQrestrict" maxlength="10" readonly="#readOnly"/>
			</td>
			<td id="numeroPlazosLb">
				<s:text name = "midas.catalogos.agente.productosAgente.numeroPlazos"/>
			</td>
			<td>
				<s:textfield name="numeroPlazos" id="numeroPlazos" cssClass = "cajaTextoM2 jQnumeric jQrestrict" maxlength="4" readonly="#readOnly"/>
			</td>
			<td id="montoPagoLb">
				<s:text name = "midas.catalogos.agente.productosAgente.montoPago"/>
			</td>
			<td>
				<s:textfield name="montoPago" id="montoPago" cssClass = "cajaTextoM2 jQfloat jQrestrict" maxlength="10" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td class="jQIsRequired">
				<s:text name = "midas.catalogos.agente.productosAgente.sucursal"/>
			</td>
			<td>
				<s:textfield name="sucursal" id="sucursal" cssClass = "cajaTextoM2" maxlength="60" readonly="#readOnly"/>
			</td>
			<td class="jQIsRequired">
				<s:text name = "midas.catalogos.agente.productosAgente.plaza"/>
			</td>
			<td>
				<s:textfield name="plaza" id="plaza" cssClass = "cajaTextoM2" maxlength="60" readonly="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.negocio.productosSeleccionados"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="productoBancario" class="w1080 h260" style="overflow:hidden">				
				</div>				
			</td>
		</tr>
		<tr>
			<td  colspan="6">
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
				<div align="right">
					<div class="btn_back w130">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="agregarProductoTemp();">
							<s:text name="midas.boton.agregarProducto"/>
						</a>
					</div>	
				</div>				
			</s:if>	
			</td>	
		</tr>
		<tr>
		    <s:if test="tipoAccion != 5">
			    <td colspan="6"><span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span></td>
			</s:if>
		</tr>
	</table>	
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<div class="w870 inline" align="right" >
		<s:if test="tipoAccion == 1 || tipoAccion == 4">
		<div class="btn_back w130">
			<a href="javascript: guardarDatosContablesAgente();" class="icon_agregar"
				onclick="">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
			</a>
		</div>
		</s:if>
		<s:if test="tipoAccion != 5">
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:atrasOSiguiente('datosFiscales');">
					<s:text name="midas.boton.atras"/>
				</a>
			</div>
			<div class="btn_back w80">
				<a href="javascript: void(0);"
					onclick="javascript:atrasOSiguiente('datosExtra');">
					<s:text name="midas.boton.siguiente"/>
				</a>
			</div>	
		</s:if>
	</div>	
</s:form>

