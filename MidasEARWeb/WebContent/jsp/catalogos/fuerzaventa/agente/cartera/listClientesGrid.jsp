<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>5</param>
				<param>7</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="index" type="ro" width="0" sort="int" hidden="true"></column>
        <column id="id" type="ro" width="0" sort="int" hidden="true"></column>
		<column id="idCliente" type="ro" width="100" sort="int" align="center"><s:text name="midas.fuerzaventa.catalogos.agente.idCliente"/></column>
		<column id="nombre" type="ro" width="440" sort="str"><s:text name="midas.cliente.nombreCliente"/></column>
		<column id="RFC" type="ro" width="100" sort="str"><s:text name="midas.catalogos.centro.operacion.rfc"/></column>
		<column id="Estatus" type="ro" width="70" sort="str"><s:text name="midas.catalogos.centro.operacion.situacion"/></column>		
		<s:if test="tipoAccion==1 || tipoAccion==4">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<!-- column id="accionEditar" type="img" width="30" sort="na"/-->
		</s:if>
	</head>
	
	<s:iterator value="listaCarteraClienteGrid" var="clienteA" status="index">
		<row id="${clienteA.id}">		
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${clienteA.id}]]></cell>
			<cell><![CDATA[${clienteA.idCliente}]]></cell>
			<cell><![CDATA[${clienteA.nombreCliente}]]></cell>
			<cell><![CDATA[${clienteA.rfc}]]></cell>			
			<cell><s:text name="midas.catalogos.centro.operacion.activo"></s:text></cell>			
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verClienteAgente(${clienteA.idCliente})^_self</cell>			
			<!-- cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:editarClienteAgente(${clienteA.idCliente})^_self</cell-->
		</row>
	</s:iterator>
</rows>	
<%-- 	<s:iterator value="listaCarteraCliente" var="clienteA" status="index"> --%>
<%-- 	<s:if test="#clienteA.claveEstatus == idEstatusRelacion"> --%>
<%-- 		<row id="${clienteA.id}">		 
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${clienteA.id}]]></cell>
			<cell><![CDATA[${clienteA.cliente.idCliente}]]></cell>
			<cell><![CDATA[${clienteA.cliente.nombre}]]></cell>
			<cell><![CDATA[${clienteA.cliente.siglasRFC}${clienteA.cliente.fechaRFC}${clienteA.cliente.homoclave}]]></cell>			
			<cell><s:text name="midas.catalogos.centro.operacion.activo"></s:text></cell>			 --%>
<%-- 			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verClienteAgente(${clienteA.cliente.idCliente})^_self</cell>			 --%>
<%-- 			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:editarClienteAgente(${clienteA.cliente.idCliente})^_self</cell> --%>
<%-- 		</row>
	</s:if> --%>
<%-- 	</s:iterator> --%>
