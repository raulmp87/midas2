<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>5</param>
				<param>5</param>
				<param>pagingAreaSmall</param>
				<param>true</param>
				<param>infoAreaSmall</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
		<column id="idCliente" type="ro" width="70" sort="int" align="center"><s:text name="midas.fuerzaventa.catalogos.agente.idCliente"></s:text></column>
		<column id="clienteNombre" type="ro" width="370" sort="str"><s:text name="midas.cliente.nombreCliente"/></column>		
		<column id="rfc" type="ro" width="370" sort="str" hidden="true"><s:text name="midas.negocio.agente.rfc"/></column>
		<column id="email" type="ro" width="370" sort="str" hidden="true"><s:text name="midas.cotizacion.emails"/></column>
	</head>
	<s:iterator value="clientesList" var="clienteI" status="index">
		<row id="${index.count}">			
			<cell><![CDATA[${clienteI.idCliente}]]></cell>
			<cell><![CDATA[${clienteI.nombreCompleto}]]></cell>			
			<cell><![CDATA[${clienteI.codigoRFC}]]></cell>
			<cell><![CDATA[${clienteI.email}]]></cell>					
		</row>
	</s:iterator>
</rows>