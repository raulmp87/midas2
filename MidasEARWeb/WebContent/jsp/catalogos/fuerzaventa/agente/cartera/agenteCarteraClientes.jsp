<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>
<script type="text/javascript" >		
	listarFiltradoCarteraCliente();		
</script>
<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
</s:if>
<s:elseif test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
</s:elseif>
<s:elseif test="tipoAccion == 5">	
	<s:set id="readOnly" value="true" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>

<s:hidden name="tipoAccion"/>
<table width="97%" id="agregar" class="contenedorFormas">	
	<tr>
		<td class="titulo" colspan="2">
			<s:text name="midas.fuerzaventa.negocio.listaClientesAgentes"/>
		</td>
	</tr>	
	<tr>
		<td colspan="2">
			<div class="dataGridGenericStyle">
				<div id="agenteCarteraClientesGrid" class="w810 h150" style="overflow:hidden"></div>
			</div>
			<div id="pagingArea"></div><div id="infoArea"></div>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="right">	
		<s:if test="tipoAccion == 1 || tipoAccion == 4">
			<div align="right" class="btn_back w150">
				<a href="javascript: void(0);" class="icon_guardar"
				onclick="javascript: mostrarDetalleClienteAgente()">
				<s:text name="Agregar"/>
				</a>
			</div>	
		</s:if>	
		</td>
	</tr>				
</table>	
<s:form action="listarFiltrado" id="agenteCarteraClientesForm">
	<s:hidden name="moduloOrigen"/>
	<s:hidden id="idAgente" name="agente.id"/>
	<s:hidden id="idAgenteHidden" name="agente.idAgente"/>
	<s:hidden id="agente.idAgenteReclutador" name="agente.idAgenteReclutador"/>
	<s:hidden name="tabActiva" value="carteraClientes"/>

	<table id="agregar" class="contenedorFormas">
	<tr>
		<td width="45%">
		<table class="formulario">
					<tr>
						<td class="w100"><s:text name="midas.fuerzaventa.negocio.nombreInstitucion"></s:text></td>
						<td class="w200"><s:textfield name="clienteAgente.cliente.nombre" cssClass="jQtoUpper cajaTextoM2 w200" id="txtNombreInstitucion"  readonly="#readOnly"></s:textfield></td>
						<td class="w100"></td>
					</tr>	
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.apPaterno"></s:text></td>
						<td><s:textfield name="clienteAgente.cliente.apellidoPaterno" cssClass="jQtoUpper cajaTextoM2 w200" id="txtApPaterno"  readonly="#readOnly"></s:textfield></td>
						<td></td>
					</tr>	
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.apMaterno"></s:text></td>
						<td><s:textfield name="clienteAgente.cliente.apellidoMaterno" cssClass="jQtoUpper cajaTextoM2 w200" id="txtapMaterno"  readonly="#readOnly"></s:textfield></td>
						<td>
							<s:if test="tipoAccion == 1 || tipoAccion == 4">
							<div id="b_buscar">
								<a href="javascript: void(0);"
									onclick="listarFiltradoCliente(true);">
									<s:text name="midas.boton.buscar"/>
								</a>
							</div>
							</s:if>
						</td>
					</tr>
					<tr>
						<td colspan="3">
						<div id="divCarga" style="position:absolute;"></div>
							<div class="dataGridGenericStyle w450">
								<div id="clientesGrid" class="w450 h150" style="overflow: hidden"></div>
							</div>
							<div id="pagingAreaSmall"></div><div id="infoAreaSmall"></div>
						</td>
					</tr>					
		</table>
		</td>
		<td width="45%">
		<table class="formulario">
					<tr>
						<td class="titulo" colspan="2"><s:text name="midas.fuerzaventa.negocio.datosGralesClte"/></td>					
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.idClte"/></td>
						<td><span id="idCliente"></span></td><s:hidden id="clienteAgente.cliente.idCliente" name="clienteAgente.cliente.idCliente"/>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.nombreClte"/></td>
						<td><span id="nombreCliente"></span></td>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.rfc"/></td>
						<td><span id="rfcCliente"></span></td>
					</tr>
					<tr>
						<td><s:text name="midas.fuerzaventa.negocio.correoElectronico"/></td>
						<td><span id="emailCliente"></span></td>
					</tr>
					<tr>
						<td colspan="2">
							<s:if test="tipoAccion == 1 || tipoAccion == 4">
							<div class="btn_back w150">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="javascript: asignarClienteAAgente()">
									<s:text name="midas.boton.asignarAgente"/>
								</a>
							</div>	
							</s:if>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	<div align="right" id="contenedor" style="width: 880px;" class="inline">
	<s:if test="moduloOrigen!=null && tabActiva==carteraClientes">	
		<s:if test="tipoAccion == 1 || tipoAccion == 4">
		<div align="right" class="btn_back w150">
			<a href="javascript: void(0);" class="icon_guardar"
				onclick="autorizaAgente();">
				<s:text name="Autorizar Agente"/>
			</a>
		</div>				
		</s:if>	
	</s:if>
	<div class="btn_back w80">
		<a href="javascript: void(0);"
			onclick="javascript:atrasOSiguiente('documentos');">
			<s:text name="midas.boton.atras"/>
		</a>
	</div>
</div>
</s:form>