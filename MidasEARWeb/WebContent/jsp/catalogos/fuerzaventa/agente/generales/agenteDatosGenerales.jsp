<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style type="text/css">
	select { width: 150px; /* Or whatever width you want. */ } 
	select.expand { width: auto; } 
</style>
<script type="text/javascript">
jQIsRequired();
//checkboxDatosAgente();
function mostrarModalResponsableAgente(){
	var url="/MidasWeb/fuerzaVenta/persona/mostrarListadoPersona.action?tipoAccion=consulta&idField=txtIdPersona";
	sendRequestWindow(null, url,obtenerVentanaResponsableAgente);
}

function obtenerVentanaResponsableAgente(){
	var wins = obtenerContenedorVentanas();
	ventanaResponsable= wins.createWindow("responsableModal", 400, 320, 900, 450);
	ventanaResponsable.center();
	ventanaResponsable.setModal(true);
	ventanaResponsable.setText("Consulta de Personas");
	ventanaResponsable.button("park").hide();
	ventanaResponsable.button("minmax1").hide();
	return ventanaResponsable;
}

function findByIdResponsableAgente(idResponsable){
	var url="/MidasWeb/fuerzaVenta/persona/findById.action";
	var data={"personaSeycos.idPersona":idResponsable};
	jQuery.asyncPostJSON(url,data,populatePersonaAgenteFront);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
}

function populatePersonaAgenteFront(json){
	if(json){
		var idResponsable=json.personaSeycos.idPersona;
		var nombre=json.personaSeycos.nombreCompleto;
		var email=json.personaSeycos.email;
		var tipoPersona=json.personaSeycos.claveTipoPersona;
		jQuery("#txtIdPersona").val(idResponsable);
		jQuery("#txtNombreAgente").val(nombre);
		jQuery("#txtCorreoElectronico").val(email);
		jQuery("#claveTipoPersona").val(tipoPersona);
		
	var clasifAgenteSeleccionado = jQuery("#txtClasifAgente option:selected").html();
	if(clasifAgenteSeleccionado != "BANCA DE GOBIERNO" && clasifAgenteSeleccionado != "NEGOCIOS ESPECIALES VIDA" && clasifAgenteSeleccionado != "BANCA SEGUROS" && clasifAgenteSeleccionado != "NEGOCIOS ESPECIALES REASEGURO" &&  clasifAgenteSeleccionado != "VENTA DIRECTA"){
		if (tipoPersona==1){
			jQuery("#txtTipoAgente option[text=PERSONA FÍSICA]").attr("selected",true);
		}else if (tipoPersona==2){
			jQuery("#txtTipoAgente option[text=PERSONA MORAL]").attr("selected",true);
		}
	}
	}
}

function mostrarModalAgenteReclutador(){
	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=txtAgenteReclutador";
	sendRequestWindow(null, url, obtenerVentanaAgenteReclutador);
}

function obtenerVentanaAgenteReclutador(){
	var wins = obtenerContenedorVentanas();
	ventanaAgentes= wins.createWindow("agenteModal", 200, 200, 900, 450);
	ventanaAgentes.center();
	ventanaAgentes.setModal(true);
	ventanaAgentes.setText("Consulta de Agente");
	ventanaAgentes.button("park").hide();
	ventanaAgentes.button("minmax1").hide();
	return ventanaAgentes;
}

function findByIdAgenteReclutador(idagente){
	var url="/MidasWeb/fuerzaventa/agente/findByIdSimple.action";
	var data={"agente.id":idagente};
	jQuery.asyncPostJSON(url,data,populateAgenteReclutador);
}

function populateAgenteReclutador(json){
	if(json){		
	var idAgenteR = json.agente.id;
	if(idAgenteR!=jQuery("#hiddenIdAgente").val()){
		var nombre = json.agente.persona.nombreCompleto;
		jQuery("#txtNombreReclutador").val(nombre);
	}
	else{
	mostrarMensajeInformativo("El agente reclutador es igual al agente actual, favor de elegir un agente diferente","10");
	jQuery("#txtAgenteReclutador").val("");
	jQuery("#txtNombreReclutador").val("");
	}
		
	}
}
</script>

<s:if test="tipoAccion == 1">	
	<s:set id="readOnly" value="false" />
	<s:set id="readOnlya" value="false" />
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />
	<s:set id="readOnlya" value="true" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 3">	
	<s:set id="readOnly" value="true" />
	<s:set id="readOnlya" value="true" />	
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="readOnlya" value="true" />
</s:elseif>
<s:include value="/jsp/catalogos/fuerzaventa/agente/agenteHeader.jsp"></s:include>

<s:form action="guardarInfoGeneral" id="guardarInfoGeneral" name="guardarInfoGeneralForm">
	<s:hidden name="moduloOrigen"/>
	<s:hidden name="agente.id" id="hiddenIdAgente"/>
	<s:hidden name="tipoAccion"/>
<s:if test="agente.idTipoAgente == 974 || agente.id ==null" >
	<s:hidden name="agente.envioDeCorreo" value="true"/>
</s:if>
<s:else>
	<s:hidden name="agente.envioDeCorreo" value="false"/>
</s:else>

<%-- 	<s:hidden name="idTipoOperacion" value="50"/> --%>
<s:if test="moduloOrigen == 1">
	<s:hidden name="idTipoOperacion" value="120"/>
</s:if>
<s:else>
	<s:hidden name="idTipoOperacion" value="50"/>
</s:else>
	<s:hidden name="tabActiva" value="info_general"/>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.fuerzaventa.negocio.titulo"/>
			</td>
		</tr>
		<tr>
			<th width="130px" class="jQIsRequired">				
				<s:text name="midas.fuerzaventa.ejecutivo.gerencia" />
			</th>
			<td>
				<s:select id="idGerencia" name="idGerencia" value="idGerencia" disabled="%{agentePromotor || #readOnly}" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivo','idPromotoria',this.value)" cssClass="cajaTextoM2 jQrequired w250"/>
			</td>
			<td align="right">			
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="mostrarVentanaGenerica(mostrarClientesPath, 'Clientes', 500, 280, document.guardarInfoGeneralForm);">
						<s:text name="midas.boton.clientes"/>
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.ejecutivo.ejecutivo" />
			</th>
			<td>
				<s:select id="idEjecutivo" name="idEjecutivo" value="idEjecutivo" disabled="%{agentePromotor || #readOnly}" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoria',this.value)" cssClass="cajaTextoM2 jQrequired w250 "/>
			</td>
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.ejecutivo.promotoria" />
			</th>
			<td>
				<s:select id="idPromotoria" name="agente.promotoria.id" value="agente.promotoria.id" disabled="%{agentePromotor || #readOnly}" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 jQrequired w250 "/>
			</td>
		</tr>
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>			
			<td>
				<s:text name="midas.fuerzaventa.altaPorInternet.fechaDeAlta" />
			</td>
			<td>
				<s:textfield name="agente.fechaAlta" id="txtIdAgente" readonly="true" cssClass="cajaTextoM2" />
			</td>
			<td>
				<s:text name="midas.negocio.fechaActivacion" />
			</td>
			<td>
				<s:textfield name="agente.fechaActivacion" id="txtIdAgente" readonly="true" cssClass="cajaTextoM2" />
			</td>
		</tr>
		<tr>
			<td class = "jQIsRequired">
				<s:text name="midas.fuerzaventa.negocio.numeroAgente" />
			</td>
			<td>
				<s:textfield name="agente.idAgente" id="txtIdAgente" maxlength="15" cssClass="cajaTextoM2 jQnumeric jQrestrict jQrequired" onchange="javascript:validaNumeroAgente()" readonly="%{claveAgenteSoloLectura || #readOnly}"/>
			</td>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.idPersona" />
			</th>
			<td>
				<table>
					<tr>
						<td>
							<s:textfield name="agente.persona.idPersona" id="txtIdPersona" readonly="true"  cssClass="cajaTextoM2 jQnumeric jQrequired" onchange="findByIdResponsableAgente(this.value);"/>
							<s:textfield name ="agente.persona.claveTipoPersona" id="claveTipoPersona" onchange="cambiarTipoAgente();" cssStyle="display:none;"/>
						</td>
						<td>		
						<s:if test="tipoAccion == 1 || tipoAccion == 4">		
							<div class="btn_back w80">
								<a href="javascript: void(0);" class="icon_buscar"
									onclick="javascript:mostrarModalResponsableAgente();">
								<s:text name="midas.boton.buscar"/>
								</a>
							</div>
						</s:if>	
						</td>
					</tr>
				</table>
			</td>									
		</tr>
		<tr>	
			<th class = "jQIsRequired">				
				<s:text name="midas.catalogos.centro.operacion.situacion" />
			</th>
			<td>
				<s:if test="agente.codigoUsuario==null && agente.tipoSituacion.id==null">				
					<s:select name="agente.tipoSituacion.id" id="estatusAgenteSituacion" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnly" 
				       headerKey="" headerValue="Seleccione.." 
				      list="catalogoTipoSituacion" listKey="id" listValue="valor" value="agente.tipoSituacion.id" />
<%-- 				      <script type="text/javascript">muestraSoloOpcionPermitida();</script>	 --%>
				</s:if>
				<s:elseif test="agente.codigoUsuario==null && agente.tipoSituacion.id!=null">					
					<s:select name="agente.tipoSituacion.id" id="estatusAgenteSituacion" cssClass="cajaTextoM2 jQrequired w250" disabled="true" 
				       headerKey="" headerValue="Seleccione.." 
				      list="catalogoTipoSituacion" listKey="id" listValue="valor" value="agente.tipoSituacion.id"/>	
				</s:elseif>
				<s:else>				
					<s:select name="agente.tipoSituacion.id" id="estatusAgenteSituacion" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnlya" 
				       headerKey="" headerValue="Seleccione.." 
				      list="catalogoTipoSituacion" listKey="id" listValue="valor" value="agente.tipoSituacion.id"  onchange="muestraSoloOpcionPermitidaAgente()"/>
				</s:else>				
			</td>						
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.prioridad" />
			</th>
			<td>
				<s:select name="agente.idPrioridadAgente" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnly" 
				       headerKey="" headerValue="Seleccione.."
				       list="catalogoPrioridad" listKey="id" listValue="valor" value="agente.idPrioridadAgente"/>
			</td>	
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.nombreAgente" />
			</th>
			<td colspan="3"><s:textfield name="agente.persona.nombreCompleto" id="txtNombreAgente" cssClass="cajaTextoM2 jQrequired w500" readonly="true"></s:textfield></td>
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.tipoAgente" />
			</th>
			<td>				
		       <s:select name="agente.idTipoAgente" cssClass="cajaTextoM2 jQrequired w250" disabled="true" 				       
		       headerKey="" headerValue="Seleccione.." list="catalogoTipoAgente" id="txtTipoAgente"
		       listKey="id" listValue="valor" value="agente.idTipoAgente" />	
			</td>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.motivo" /> Estatus
			</th>
			<td>				       
		       <s:select name="agente.idMotivoEstatusAgente" id="idMotivoEstatusAgente" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnlya" 
		       headerKey="" headerValue="Seleccione.." 
		       list="catalogoMotivoEstatus" listKey="id" listValue="valor" value="agente.idMotivoEstatusAgente"/>
			</td>
				
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.correoElectronico" />
			</th>
			<td><s:textfield name="agente.persona.email" id="txtCorreoElectronico" cssClass="cajaTextoM2 jQrequired jQemail w200" readonly="true"></s:textfield></td>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.clasificacionAgente" />
			</th>
			<td>				
		       <s:select name="agente.clasificacionAgentes.id" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnly" 				       
		       headerKey="" headerValue="Seleccione.." list="catalogoClasificacionAgentes" id="txtClasifAgente"
		       listKey="id" listValue="valor" value="agente.clasificacionAgentes.id" onchange="javascript: checkboxDatosAgente();" />	
			</td>
		</tr>
		<tr>
			<th>				
				<s:text name="midas.fuerzaventa.negocio.agenteReclutador" />
			</th>
			<td colspan="2">
				<s:textfield name="agente.idAgenteReclutador" id="txtAgenteReclutador" onchange="findByIdAgenteReclutador(this.value);" cssStyle="display:none;"/>
				<s:textfield id="txtNombreReclutador" cssClass="cajaTextoM2 w400" readonly="true"/>				
			</td>
			<td>		
			<s:if test="tipoAccion == 1 || tipoAccion == 4">		
				<div class="btn_back w80">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:mostrarModalAgenteReclutador();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</s:if>
			</td>
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.negocio.observaciones" />
			</th>
			<td colspan="3"><s:textfield name="agente.observaciones" id="txtObservaciones" cssClass="cajaTextoM2 jQrequired w500" readonly="#readOnly"></s:textfield></td>
		</tr>
		<tr>
			<td colspan="4">
				<table class="contenedorFormas no-border w800" cellpading="0" cellspacing="0">
					<tr>
						<td><s:checkbox id="txtGeneraCheque" name="agente.claveGeneraChequeBoolean" value="agente.claveGeneraCheque" disabled="true"></s:checkbox></td>
						<th><s:text name="midas.fuerzaventa.negocio.generaCheques" /></th>
						<td><s:checkbox id="txtContabiliza" name="agente.claveContabilizaComisionBoolean" value="agente.claveContabilizaComision" disabled="true"></s:checkbox></td>
						<th class = "jQIsRequired"><s:text name="midas.fuerzaventa.negocio.contabilizaComision" /></th>		
						<td><s:checkbox id="txtComision" name="agente.claveComisionBoolean" value="agente.claveComision" disabled="true"></s:checkbox></td>
						<th class = "jQIsRequired"><s:text name="midas.fuerzaventa.negocio.comision" /></th>
						<td><s:checkbox id="txtEdoCuenta" name="agente.claveImprimeEstadoCtaBoolean" value="agente.claveImprimeEstadoCta" disabled="true"></s:checkbox></td>
						<th class = "jQIsRequired"><s:text name="midas.fuerzaventa.negocio.imprimeEdoCta" /></th>	
						<th class = "jQIsRequired"><s:text name="midas.fuerzaventa.negocio.dividendos" /></th>	
						<td><s:textfield name="agente.porcentajeDividendos" maxlength="4"  id="txtDividendos" cssClass="cajaTextoM2 jQpercent jQrequired jQfloat w80" readonly="#readOnly"></s:textfield></td>													
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
			<th width="130px" class = "jQIsRequired">			
				<s:text name="midas.fuerzaventa.negocio.numeroCedula" />
			</th>
			<td><s:textfield name="agente.numeroCedula" id="txtNumeroCedula" cssClass="cajaTextoM2 jQrequired W150" readonly="#readOnly" maxlength="20"></s:textfield></td>
			<td class = "jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.fechaAutorizacion"/>
			</td>
			<td>		
			<s:if test="tipoAccion == 1 || tipoAccion == 4">							
  				<sj:datepicker name="agente.fechaAutorizacionCedula" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="txtFechafechaAlta" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				changeYear="true" changeMonth="true"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);"
				onchange="validaFechasDatosGenerales();">
				</sj:datepicker>
			</s:if>	
			<s:else>
				<s:textfield name="agente.fechaAutorizacionCedula" id="txtFechafechaAlta" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>	
			</td>
		</tr>
		<tr>
			<th class = "jQIsRequired">				
				<s:text name="midas.fuerzaventa.agente.tipoCedula" />
			</th>
			<td>			
				<s:select name="agente.tipoCedula.id" style="display: block;" cssClass="cajaTextoM2 jQrequired w250 wide"  labelposition="left" disabled="#readOnly"
				       headerKey="" headerValue="Seleccione.." 
				       list="catalogoTipocedula" listKey="id" listValue="clave+' - '+valor" value="agente.tipoCedula.id"/>
			</td>
			<td class="jQIsRequired">
				<s:text name="midas.fuerzaventa.altaPorInternet.fechaVencimientoCedula"/>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4">
  				<sj:datepicker name="agente.fechaVencimientoCedula" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="txtFechaBaja" maxlength="10" cssClass="w100 jQrequired cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				changeYear="true" changeMonth="true"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				onblur="esFechaValida(this);"
 				onchange="validaFechasDatosGenerales();"></sj:datepicker>
 			</s:if>	
			<s:else>
				<s:textfield name="agente.fechaVencimientoCedula" id="txtFechaBaja" cssClass="cajaTextoM2" readonly="#readOnly"></s:textfield> 
			</s:else>	 
			</td>
		</tr>
		<tr>
		    <s:if test ="tipoAccion != 5">
		        <td colspan="4"><span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span></td>
		    </s:if>			
		</tr>
	</table>	
	<div align="right" class="w870 inline" >
		<s:if test="tipoAccion == 1 || tipoAccion == 4">
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_agregar"
				onclick="javascript:guardarAgenteDatosGenerales();">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.guardar"/>
			</a>
		</div>
		</s:if>
		<s:if test="tipoAccion == 3">
		<div class="btn_back w110">
			<a href="javascript: void(0);" class="icon_agregar"
				onclick="javascript:eliminarAgente();">
				<s:text name="midas.boton.borrar"/>
			</a>
		</div>
		</s:if>
		<s:if test="agente.id!=null && tipoAccion != 5">
		<div class="btn_back w80">
			<a href="javascript: void(0);"
				onclick="atrasOSiguiente('domicilios');">
				<s:text name="midas.boton.siguiente"/>
			</a>
		</div>
		</s:if>
	</div>
	<script type="text/javascript">
		var opcionMotEstatus = jQuery("#idMotivoEstatusAgente option:selected").val();
		muestraSoloOpcionPermitidaAgente();
		jQuery("#idMotivoEstatusAgente option[value="+ opcionMotEstatus +"]").attr("selected",true);
		deshabilitarClasificacionAgentes();
	</script>
</s:form>

