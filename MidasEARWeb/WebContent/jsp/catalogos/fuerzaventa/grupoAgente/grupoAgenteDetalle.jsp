<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:hidden name="mensaje" id="mensajeTxt"/>

<s:if test="tipoAccion == 1">
	<s:set id="titulo" value="%{getText('Agregar un nuevo grupo')}"/>
</s:if>
<s:elseif test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('Consultar Grupo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('Editar Grupo')}"/>
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('Eliminar Grupo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:elseif>

<div class="titulo w400"><s:text name="#titulo"/></div>

<s:form action="agregar" id="grupoAgentesForm" name="grupoAgentesForm">
	<s:hidden name="grupoAgenteDTO.id" ></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>	
			<th class="jQIsRequired"><s:text name="Nombre"></s:text></th>	 
			<td><s:textfield name="grupoAgenteDTO.nombre" id="" cssClass="cajaTextoM2 w350 jQrequired" readonly="#readOnly"></s:textfield></td>
			<th class="jQIsRequired"><s:text name="Descripcion"></s:text></th>	 
			<td><s:textfield name="grupoAgenteDTO.descripcion" id="" cssClass="cajaTextoM2 w350 jQrequired" readonly="#readOnly"></s:textfield></td>			
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="Situacion"></s:text></th>
			<td>
				<s:select disabled="#readOnly"
					 list="#{'0':'INACTIVO','1':'ACTIVO'}"
		 			 name="grupoAgenteDTO.estatus"
		       		 cssClass="cajaTextoM2"
			     />
		     </td>	
		</tr>
		<tr>
			<td colspan="2">
				<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
			</td>
		</tr>
	</table>
</s:form>

<table class="contenedorFormas no-Border" width="880px">
	<tr>
		<td class="inline" align="right">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_regresar"
				onclick="regresarPaginaPrincipal();">
				<s:text name="midas.boton.regresar"/>
				</a>
			</div>
			<s:if test="tipoAccion == 1">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
					onclick="guardarGrupo();">
					<s:text name="midas.boton.agregar"/>
					</a>
				</div>
			</s:if>
			<s:if test="tipoAccion==3">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="eliminarGrupo();">
						<s:text name="Eliminar"/>
					</a>
				</div>
			</s:if>
			<s:if test="tipoAccion == 4">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
					onclick="guardarGrupo();">
					<s:text name="midas.boton.guardar"/>
					</a>
				</div>
			</s:if>
		</td>
	</tr>
</table>

<script type="text/javascript">
	jQIsRequired();
	var mensaje = jQuery("#mensajeTxt").val();

	if (mensaje != "") {
		if (mensaje.includes("Error:")) {
			mostrarMensajeInformativo(mensaje, '20');
		} else {
			mostrarMensajeInformativo(mensaje, '30');
		}
	}
</script>