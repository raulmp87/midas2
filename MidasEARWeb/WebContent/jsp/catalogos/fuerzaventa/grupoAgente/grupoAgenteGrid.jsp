<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call> 
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>     
        </beforeInit>
        <afterInit>
        </afterInit>
        
        <!--  <column id="idcheck" type="ch" width="35" sort="int" align="center"></column> -->
        <column id="id" type="ro" width="52" sort="int" align="center"><s:text name="midas.negocio.producto.id"/></column>
        <column id="descripcion" type="ro" width="608" sort="int" ><s:text name="midas.fuerzaventa.catalogo.grupoAgente.descripcion" /></column>
		<column id="situacion" type="ro" width="80" sort="str" align="center"><s:text name="midas.catalogos.centro.operacion.situacion"/></column>
		<column id="accionVer" type="img" width="80" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="accionEditar" type="img" width="30" sort="na" align="center"/>
		<column id="accionBorrar" type="img" width="30" sort="na" align="center"/>
	</head>
	<s:iterator value="listaGrupoAgente" status="index" var="grupoAgenteListObject">
		<row id="${grupoAgenteListObject.id}">
			<!-- <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell> -->
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="#grupoAgenteListObject.estatus == 1">
				    <s:text name="ACTIVO"/>
				</s:if>
				<s:else>
				    <s:text name="INACTIVO"/>
				</s:else>
			</cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleGrupoAgentePath, 2,{"grupoAgenteDTO.id":${grupoAgenteListObject.id}})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleGrupoAgentePath, 4,{"grupoAgenteDTO.id":${grupoAgenteListObject.id}})^_self</cell>
				<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleGrupoAgentePath, 3,{"grupoAgenteDTO.id":${grupoAgenteListObject.id}})^_self</cell>
			</s:if>	
		</row>
	</s:iterator>
</rows>