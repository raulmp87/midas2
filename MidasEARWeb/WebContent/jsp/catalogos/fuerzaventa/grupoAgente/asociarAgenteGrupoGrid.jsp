<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call> 
			<call command="enableDragAndDrop"><param>true</param></call>    
        </beforeInit>
        <afterInit>
        </afterInit> 
        <column id="idAgente" type="ro" width="110" sort="int" ><s:text name="midas.fuerzaventa.catalogo.asociarAgente.clave"/></column>
        <column id="nombreAgente" type="ro" width="380" sort="int" ><s:text name="midas.fuerzaventa.catalogo.asociarAgente.nombreAgente" /></column>
        <column id="gerencia" type="ro" width="380" sort="str"><s:text name="midas.fuerzaventa.catalogo.asociarAgente.gerencia"/></column>
	</head>
	<s:iterator value="listaAgenteGrid" var="listaAgenteGridObject" status="index">
		<row id="${listaAgenteGridObject.idAgente}">
			<cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="gerencia" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>