<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/grupoAgente/grupoAgente.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<script type="text/javascript">
	var listarGridVacioPath = '<s:url action="lista" namespace="/fuerzaventa/grupoAgente"/>';
	var listarFiltradoGrupoAgentePath = '<s:url action="listarFiltradoGrupo" namespace="/fuerzaventa/grupoAgente"/>';
	var asociarGrupoAgentePath = '<s:url action="agenteAsociarGrupo" namespace="/fuerzaventa/grupoAgente"/>';
	var obtenerAsociarGrupoAgentePath = '<s:url action="obtenerAsociarGrupoAgentePorId" namespace="/fuerzaventa/grupoAgente"/>';
	var obtenerAgentesPath = '<s:url action="findAgentes" namespace="/fuerzaventa/grupoAgente"/>';
	var listarFiltradoAgentePath = '<s:url action="listarFiltradoAgentes" namespace="/fuerzaventa/grupoAgente"/>';
	var guardarGrupoPath = '<s:url action="guardar" namespace="/fuerzaventa/grupoAgente"/>';
	var eliminarGrupoPath = '<s:url action="eliminarGrupo" namespace="/fuerzaventa/grupoAgente"/>';
	var verDetalleGrupoAgentePath = '<s:url action="verDetalle" namespace="/fuerzaventa/grupoAgente"/>';
	var listarFiltradoAgenteAsociadoPath = '<s:url action="listarFiltradoAgenteAsociado" namespace="/fuerzaventa/grupoAgente"/>'; 
	
</script>