<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteHeader.jsp"></s:include>

<s:hidden name="tipoAccion"/>

<s:form action="agregar" id="asociarAgenteGrupoForm" name="asociarAgenteGrupoForm" >
	<s:hidden name="grupoAgenteDTO.id"/>
	<table width="97%" class="contenedorFormas" align="center">
		<tr>
			<th class="titulo" colspan="2" >
				<s:text name="midas.fuerzaventa.catalogo.asociarAgente.asociacionAgentes"/>
			</th>
		</tr>
		<tr>
			<th width="150px"> <s:text name="midas.fuerzaventa.catalogo.asociarAgente.idGrupo"/></th>
			<td> <s:textfield name="grupoAgenteDTO.id" id="txtIdGrupo" cssClass="cajaTextoM2 w50" readonly="true"> </s:textfield></td>
		</tr>
		<tr>
			<th> <s:text name="midas.fuerzaventa.catalogo.asociarAgente.descripcionGrupo"/></th>
			<td> <s:textfield name="grupoAgenteDTO.nombre" id="" cssClass="cajaTextoM2 w400" readonly="true"> </s:textfield></td>
		</tr>
		<tr>
			<td class="titulo" colspan="2"><s:text name="midas.fuerzaventa.catalogo.asociarAgente.asociarAgentes"/></td>
		</tr>
		<tr>
			<td colspan="2"><s:text name="midas.fuerzaventa.catalogo.asociarAgente.agentesAsociados"/></td>
		</tr>
		<tr>
			<td colspan="2"> 
				<table class="contenedorFormas no-Border">
					<tr>
						<th colspan="2"><s:text name="midas.fuerzaventa.catalogo.asociarAgente.clave"/></th>
						<td><s:textfield type="text" name="clave" id="campoBusqueda0_id" cssClass="cajaTextoM2 w70"></s:textfield></td>
						<th><s:text name="midas.fuerzaventa.catalogo.asociarAgente.nombreAgente"/></th>
						<td><s:textfield type="text" name="nombre" id="campoBusqueda0_nombre" cssClass="cajaTextoM2 w120"></s:textfield></td>
						<th><s:text name="midas.fuerzaventa.catalogo.asociarAgente.gerencia"/></th>
						<td><s:textfield type="text" name="gerencia" id="campoBusqueda0_g" cssClass="cajaTextoM2 w50"></s:textfield></td>
						<td>
							<div class="btn_back w20" id="">
								<a class="icon_buscar" href="javascript: void(0);" onclick="filtrarAgentesAsociados();">
								</a>
							</div>
						</td>
					</tr>
				</table>
				<div align="center" id="asociarGrupoAgenteGrid" class="w870 h260" style="overflow:hidden"></div>
			</td>
		</tr>
		<tr>
			<th colspan="2"><s:text name="midas.fuerzaventa.catalogo.asociarAgente.agentesDisponibles"/></th>
		</tr>
		<tr>
			<td colspan="2">
				<table class="contenedorFormas no-Border">
					<td><s:text name="midas.fuerzaventa.catalogo.asociarAgente.clave"></s:text></td>
					<td><input id="campoBusqueda_id" type="text" name="filtroAgente.idAgente" id="txtClave" class="cajaTextoM2 w70"></td>
					 
					<td><s:text name="midas.fuerzaventa.catalogo.asociarAgente.nombreAgente"></s:text></td>
					<td><input id="campoBusqueda_nombreCompleto" type="text" name="filtroAgente.persona.nombreCompleto" id="txtNombreCompleto" class="cajaTextoM2 w120"></td>
					<td><s:text name="midas.fuerzaventa.catalogo.asociarAgente.gerencia"></s:text></td>
					<td><input id="campoBusqueda_gerencia" type="text" name="filtroAgente.promotoria.ejecutivo.gerencia.idGerencia" id="txtGerencia"class="cajaTextoM2 w50"></td>
					 
					<td>
						<div class="btn_back w20" id="">
							<a class="icon_buscar" href="javascript: void(0);" id="agenteBuscar" onclick="filtrarAgentes();">
							</a>
						</div>
					</td>
				</table>
				<div id="asociarAgenteGrid" class="w870 h260" style="overflow:hidden"></div>
			</td>
		</tr>
	</table>
</s:form>
<div align="right" class="w900 inline">
	<div class="btn_back w110">
		<a class="icon_regresar" href="javascript: void(0);" onclick="regresarPaginaPrincipal();">
			<s:text name="midas.boton.regresar"></s:text>
		</a>
	</div>
	<div class="btn_back w110">
		<a class="icon_guardar" href="javascript: void(0);" onclick="guardarAgentesAsociadosenGrupo(); ">
			<s:text name="midas.boton.guardar"></s:text>
		</a>
	</div>
</div>
<script type="text/javascript">
	jQuery(function(){
		iniciaGrids();
	});
</script>