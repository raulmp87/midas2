<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>

<s:include value="/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="grupoAgenteForm" name="grupoAgenteForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6"><s:text name="midas.fuerzaventa.catalogo.grupoAgente.titulo"/></td>
		</tr>
		<tr>	
			<th><s:text name="midas.fuerzaventa.catalogo.grupoAgente.id"></s:text></th>	 
			<td><s:textfield name="filtroGrupoAgente.id" id="txtIdGrupoAgente" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict"></s:textfield></td>
			<th><s:text name="midas.fuerzaventa.catalogo.grupoAgente.descripcion"></s:text></th>	 
			<td><s:textfield name="filtroGrupoAgente.descripcion" id="txtDescripcion" cssClass="cajaTextoM2 w200"></s:textfield></td>			
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.fechaAltaInicio"></s:text></th>	 
			<td>
				<sj:datepicker name="filtroGrupoAgente.fechaCreacion" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroGrupoAgente.fechaCreacion" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<th><s:text name="midas.fuerzaventa.negocio.fechaAltaFin"></s:text></th>	 
			<td>
				<sj:datepicker name="filtroGrupoAgente.fechaModificacion" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif" changeYear="true" changeMonth="true"
					id="filtroGrupoAgente.fechaModificacion" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="5">
				<div class="btn_back w110">
					<a href="javascript: void(0);" id="agenteBuscar" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoGrupoAgentePath, 'grupoAgenteGrid', document.grupoAgenteForm);"> <!-- modificar aqui para filtrar grupos en el grid -->
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div id="grupoAgenteGrid" class="w880 h260" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
</s:form>


<div class ="w890" align="right">
	<div class="btn_back w180" style="display: inline-block;">
		<a href="javascript: void(0);" class="icon_guardar ." 
			onclick="asociarAgentes();"> <!-- modificar aqui para llevar a otro JSP -->
			<s:text name="midas.boton.asociarAgentes"/>
		</a>
	</div>
	<div class="btn_back w100" style="display: inline-block;">
		<a href="javascript: void(0);" class="icon_guardar ." 
			onclick="agregarAgente(1)">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>		
</div>

<script type="text/javascript">
	var gridAgentes=null;
	var tipoAccionAgente='<s:property value="tipoAccion"/>';
	var tipoAccion='<s:property value="tipoAccion"/>';
	var varModal='grupoAgenteModal';
	var idFieldAgente = '<s:property value ="idField"/>';

	jQuery(function(){
		urlFil = listarGridVacioPath+"?tipoAccion="+tipoAccionAgente;
		listarFiltradoGenerico(urlFil, "grupoAgenteGrid", jQuery("#grupoAgenteForm"), idFieldAgente, varModal);
	});
</script>