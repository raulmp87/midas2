<%@page	language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/catalogos/envioFacturaAgentes/envioFacturaAgentes.js'/>"></script>
<style type="text/css">
.errors {
	background-color:#FFCCCC;
	border:1px solid #CC0000;
	width:400px;
	margin-bottom:8px;
}
.success {
	background-color:#DDFFDD;
	border:1px solid #009900;
	width:200px;
}
</style>
<s:set id="titulo" value="%{getText('midas.suspensiones.tituloEnvioFactura')}"/>
<div class="titulo w800"><s:text name="#titulo"/></div>
<div id="divContentInfoAge" >
<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>
<s:if test="hasActionMessages()">
   <div class="success">
      <s:actionmessage/>
   </div>
</s:if>
<s:hidden id="isAgentePromotor" value="%{isAgentePromotor}" name="isAgentePromotor"></s:hidden>
<table>
	<tbody>
		<tr>
        	<td>
				<table class="contenedorConFormato">
				    <tbody>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.numeroAgente" />: </th>
				    		<td>
				    			<s:property value="idAgente" />
				    			<s:hidden name="idAgente" id="hiddenIdAgente" />
				    		</td>
				    		<th><s:text name="midas.prestamosAnticipos.numeroCedula" />: </th>
				    		<td><s:property value="numeroCedula" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.venceCedula" />: </th>
				    		<td><s:property value="vencimientoCedula" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.tipoAgente" />: </th>
				    		<td><s:property value="tipoAgente" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.nombreAgente" />: </th>
				    		<td><s:property value="nombreAgente" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.centroOperacion" />: </th>
				    		<td><s:property value="centroOperacion" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.gerencia" />: </th>
				    		<td><s:property value="gerencia" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.estatus" />: </th>
				    		<td><s:property value="estatus" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.rfc" />: </th>
				    		<td><s:property value="rfc" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.ejecutivo" />: </th>
				    		<td><s:property value="ejecutivo" /></td>
				    	</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div id="divContentForm" >
</div>
<br/>
<div id="divButtons">
	<table width="800" style="font-family: arial; font-size: 11px;">
		<tr>
		<s:if test="%{isAgentePromotor ==true}">
				<td>
					<font color="#FF6600">* </font>
					<s:text name="midas.fuerzaventa.configBono.agente" />:
					<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
							onchange="addAgenteProduccion();" style="display:none;" />
				</td>
				<td>
					<s:select name="idAgente"
						style="font-family: arial; font-size: 11px; width:250px;"
						id="agentes" 
						list="agenteList" 
						listKey="idAgente" 
						listValue="nombreCompleto" 
						headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="setAgente(this.value)"/>
				</td>
			</s:if>
			<td>
				<s:if test="%{anioMes!='N/A' && ((isAgente==true && tieneFacturasPendientes==false) || isAdministrador==true || isAgentePromotor==true)}">
					<div class="btn_back w170">
						<a id="loadButton" href="javascript: void(0);" class="icon_buscar" onclick="importarArchivo()">Cargar Factura XML</a>
					</div>
				</s:if>	
			</td>
			<s:if test="%{isAgentePromotor !=true}">
				<td></td>
				<td></td>
				<td></td>
			</s:if>
				<td colspan="3" align="right">
				<s:if test="%{isAdministrador==true}">
					<div>
						Escribe Clave Agente:
						<input id="inputIdAgente" type="text" class="ui-corner-all">
					</div>
				</s:if>
			</td>
			<td>
				<s:if test="%{isAdministrador==true}">
					<div class="btn_back w110">
						<a id="initButton" href="javascript: void(0);" class="icon_buscar" onclick="init()">Buscar</a>
					</div>
				</s:if>
			</td>
			<td>
		</tr>
		<tr>
			<td>
			<s:if test="%{isAutorizadorExcepcion==true}">
				<div id="autorizacionExcepcion">
				  
					<table class="contenedorConFormato">
						<tbody>
					    	<tr>
					    		<th>
					    			<div class="titulo w300"><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.titulo" /></div>
					    		</th>
					    		<td/>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.autorizador1" /> </th>
					    		<td><s:checkbox id="autorizacion1" name="autorizacion1" disabled="disableAutorizacion1" onchange="autorizaExcepcion()"/></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.autorizador2" /> </th>
					    		<td><s:checkbox id="autorizacion2" name="autorizacion2" disabled="disableAutorizacion2" onchange="autorizaExcepcion()"/></td>
					    	</tr>
						</tbody>
			    	</table>
				
				</div>
			</s:if>
			</td>
			<td align="center">
				<s:if test="%{tieneFacturasPendientes==true}">
					<td style="font-size: 11px;font-weight: bold;"><s:property value="msgFacturasPendientes" /></td>
				</s:if>
			</td>
			<s:if test="%{tieneFacturasPendientes==true && isAdministrador==true}">
				<td align="right" colspan="7">
					<midas:boton onclick="javascript: marcarFacturasAntiguas();"  tipo="seleccionar" key="midas.agente.factura.antigua.marcado.boton" style="width: 360px; display: inline; float: right; margin-left: 5px;"/>
				</td>	
			</s:if>
		</tr>
	</table>
</div>
<br/>
<div id="divContentTable" align="center" width="920px" height="200px" style="background-color:white;overflow:hidden">
</div>
<div id="pagingArea"></div><div id="infoArea"></div>
<div id="divContentModal" style="display: none;">
	<div id="divContentTableDet" align="center" width="560px" height="200px" style="background-color:white;overflow:hidden;">
	</div>
	<div id="pagingAreaDet"></div><div id="infoAreaDet"></div>
</div>