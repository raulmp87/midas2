<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>8</param>
				<param>5</param>
				<param>pagingAreaDet</param>
				<param>true</param>
				<param>infoAreaDet</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" hidden="true" width="50" sort="int"><s:text name="midas.suspensiones.tituloEnvioFactura"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="Tipo Mensaje"/></column>
		<column id="" type="ro" width="410" sort="str"><s:text name="Mensaje"/></column>
	</head>
	<s:iterator value="listaEnviosDet" var="envioFacturaAgenteDet" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${envioFacturaAgenteDet.tipoMsj.valor}]]></cell>
			<cell><![CDATA[${envioFacturaAgenteDet.mensaje}]]></cell>
		</row>
	</s:iterator>
</rows>