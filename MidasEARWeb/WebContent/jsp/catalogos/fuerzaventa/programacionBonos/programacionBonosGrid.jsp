<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="programacionBono.id" type="ro" width="30" sort="int" hidden="false" align="center"><s:text name="midas.negocio.producto.id"/></column>
        <column id="programacionBono.tipoBono.valor" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.fuerzaventa.configBono.tipoBono"/></column>
        <column id="programacionBono.descripcionBono" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.fuerzaventa.programacionBono.descripcionBonoNeg"/></column>
        <column id="programacionBono.fechaAlta" type="ro" width="100" sort="date_custom" hidden="false" align="center"><s:text name="midas.fuerzaventa.fechaAlta"/></column>          
        <column id="programacionBono.fechaEjecucion" type="ro" width="100" sort="date_custom" hidden="false" align="center"><s:text name="midas.fuerzaventa.programacionBono.fechaEjecucion"/></column>
        <column id="programacionBono.periodoEjecucion.valor" type="ro" width="100" sort="str" hidden="false" align="center"><s:text name="midas.fuerzaventa.programacionBono.periodoEjecucion"/></column>
        <column id="programacionBono.hora" type="ro" width="100" sort="int" hidden="false" align="center"><s:text name="midas.fuerzaventa.programacionBono.horario"/></column>
        <column id="programacionBono.claveEstatus.valor" type="ro" width="150" sort="str" hidden="false" align="center"><s:text name="midas.negocio.estatus"/></column>
        <column id="programacionBono.usuarioAlta" type="ro" width="100" sort="str" hidden="false" align="center"><s:text name="midas.fuerzaventa.negocio.usuario"/></column>
        <column id="programacionBono.activo" type="ch" width="70" sort="int" hidden="false" align="center"><s:text name="midas.catalogos.fuerzaventa.estatus.activo"/></column>
        <column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>      
	</head>

	<s:iterator value="programacionBonoList">
		<row id="<s:property value="id" escapeHtml="false"/>">
			<cell><s:property value="id" escapeHtml="false"/></cell>
			<cell><s:property value="tipoBono.valor" escapeHtml="false"/></cell>
			<cell><s:property value="descripcionBono" escapeHtml="false"/></cell>
			<cell><s:property value="fechaAlta" escapeHtml="false"/></cell>
			<cell><s:property value="fechaEjecucion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="periodoEjecucion.valor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="hora" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveEstatus.valor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioAlta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="activo" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/cross16.png^Eliminar Programacion^javascript:eliminarProgramacion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>
</rows>