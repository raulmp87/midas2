<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page  language="java" import="java.util.*"%>

<s:include value="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonosHeader.jsp"></s:include>
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
</style>    
<s:form id="programacionBonosForm" name="programacionBonosForm">
	<table width="880px" id="filtrosM2">
		<tr>
		<td colspan="6" class="titulo">
			<s:text name="midas.fuerzaventa.programacionBono.programacionBono" />
		</td>
		</tr>
		<tr>
		<th>
			<s:text name="midas.fuerzaventa.programacionBono.fechaAlta"></s:text>:
		</th>
		<td>
				<%java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
				<%=df.format(new java.util.Date())%>		
		</td>
		</tr>
		<tr>
		<th width="110px" class="jQIsRequired">
			<s:text name="midas.fuerzaventa.configBono.tipoBono" />
		</th>
		<td>
			<s:select name="programacionBono.tipoBono.id" id="tipoBono" 
			headerKey="" headerValue="Seleccione..."  list="tipoBonoList"
			listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w150"
			onchange="onchangeTipoBono();habilitarBusquedaPorDescripcion();" />
		</td>
		<th>
			<s:text name="midas.fuerzaventa.programacionBono.bonosProgramados" />:
		</th>
		<td>
			<div id="bonosProgramados">0</div>
		</td>
		<th>
			<s:text name="midas.fuerzaventa.programacionBono.bonosSinProgramar" />:
		</th>
		<td>
			<div id="bonosPorProgramar">0</div>
		</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.programacionBono.descripcionBono" />:
			</th>
			<td style="padding-left:0px;">
				<table>
					<tr>
						<td style="padding-left:0px;"><s:textfield id="txtDescBonoBusqueda" cssClass="cajaTextoM2 w200"></s:textfield></td>
						<td><div class="btn_back w100" id="botonBusqueda" >
					<a id="buscarXDesc" href="javascript: void(0);"  
						onclick="filtrarBonosPorDescripcion(document.getElementById('txtDescBonoBusqueda').value);" class="">	
						<s:text name="midas.boton.buscar"/>	
					</a>
                </div></td>
					</tr>
				</table>			
			</td>			
		</tr>
					<tr>						
						<th>
							<s:text name="midas.fuerzaventa.programacionBono.negociosEspeciales" />:
						</th>
						<td style="padding-left:4px;">
							<table id="filtrosM2" style="border: 0px solid transparent">
								<tr>
									<td><ul id="ajax_listaNegociosEspeciales"></ul></td>
									<td valign="top">
										<a target="_self" href="javascript: selectAllChecksb('ajax_listaNegociosEspeciales');">
	                    					<img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                					</a>		    
				    					<a target="_self" href="javascript: deselectAllChecks('ajax_listaNegociosEspeciales');">
	                    					<img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
	                					</a> 
									</td>
								</tr>
							</table>							
						</td>
						<th>
							<s:text name="midas.fuerzaventa.programacionBono.bonos" />:
						</th>
						<td colspan="3">
							<table id="filtrosM2" style="border: 0px solid transparent">
								<tr>
									<td><ul id="ajax_listaConfiguracionBonos"></ul></td>
									<td valign="top">
										<a target="_self"  href="javascript: selectAllChecksb('ajax_listaConfiguracionBonos');">
	                    					<img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                					</a>		    
				    					<a target="_self" href="javascript: deselectAllChecks('ajax_listaConfiguracionBonos');">
	                    					<img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
	                					</a> 
									</td>
								</tr>
							</table>						
						</td>		
					</tr> 
				</table>
	<br>	
	<table width="880px" id="filtrosM2">
		<tr>
		<td colspan="8" class="titulo">
			<s:text name="midas.fuerzaventa.programacionBono.modoEjecucion" />
		</td>
		</tr>
		<tr>
			<td>
				<s:radio labelposition="left"  list="#{'1':'Manual'}"  name="modoEjecucion" value="1"
					onclick="changeModoEjecucion();"
				/>
			</td>
			<td colspan="6">
			 &nbsp;
			</td>
			<td>
				<div class="btn_back w100" id="manualButton" >
					<a id="ejecutar" href="javascript: void(0);"  
						onclick="ejecutarCalculo();" class="">	
						<s:text name="midas.fuerzaventa.configuracionPagosComisiones.ejecutar"/>	
					</a>
                </div>
			</td>
		</tr>
		<tr>
			<td>
				<s:radio labelposition="left"  list="#{'2':'Automatico'}"  name="modoEjecucion"
					onclick="changeModoEjecucion();"
				/>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.programacionBono.periodoEjecucion" />*:
			</td>
			<td>
				<s:select disabled="true" name="programacionBono.periodoEjecucion.id" headerKey="" headerValue="Seleccione..."  
					list="periodoEjecucionList" id="periodoEjecucion"
					listKey="id" listValue="valor" labelposition="left" cssClass="cajaTextoM2 w100" />
			</td>
			<td>
				<s:text name="midas.fuerzaventa.programacionBono.fechaEjecucion" />*:
			</td>
			<td>
				<sj:datepicker disabled="true" name="programacionBono.fechaEjecucion" required="#requiredField" 
				   buttonImage="../img/b_calendario.gif"  
				   id="fechaEjecucion" maxlength="10" cssClass="txtfield w100"	
				   size="12"
				   changeMonth="true" 
				   changeYear="true"							   								  
				   onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.programacionBono.horario" />*:
			</td>
			<td>
				<s:textfield disabled="true" name="programacionBono.hora" id="hora" cssClass="txtfield  jQnumeric jQrestrict w100"  />
			</td>
			<td>
				<div class="btn_back w100" style="display:none;" id="automaticoButton" >
					<a id="programar" href="javascript: void(0);"  
						onclick="programarBono();" class="">	
						<s:text name="midas.fuerzaventa.programacionBono.programar"/>	
					</a>
                </div>
                <div class="btn_back w100" id="" >
					<a id="programar" href="javascript: void(0);"  
						onclick="verMonitorDeCalculosBonos();" class="">	
						<s:text name="Monitor"/>	
					</a>
                </div>
			</td>
		</tr>
	</table>
</s:form>
<br>
<div>
	<s:text name="midas.fuerzaventa.programacionBono.listaProgramaciones"/>
</div>
<div align="center" id="programacionBonoGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

<script type="text/javascript">
	changeModoEjecucion();
	cargaProgramacionBonoGrid();
	habilitarBusquedaPorDescripcion();
</script>