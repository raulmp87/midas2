<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page  language="java" import="java.util.*"%>

<s:include value="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonosHeader.jsp"></s:include>
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
</style>    
<s:form id="programacionBonosForm" name="programacionBonosForm">
	<s:hidden id="gridMonitor" name="gridMonitor"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
		<td colspan="6" class="titulo">
		<s:if test="gridMonitor == 'BONOS'">
			<s:text name="Monitor de Bonos" />
		</s:if>
		<s:elseif test="gridMonitor == 'COMIS'">
			<s:text name="Monitor de Comisiones" />
		</s:elseif>			
		</td>		
	</table>
</s:form>
<div class="btn_back w100" id="" >
	<s:if test="gridMonitor == 'BONOS'">	
		<a id="programar" href="javascript: void(0);"  
			onclick="verMonitorDeCalculosBonos();" class="">	
			<s:text name="Actualizar"/>		    
		</a>
	 </s:if>
	 <s:elseif test="gridMonitor == 'COMIS'">
	 	<a id="programar" href="javascript: void(0);"  
			onclick="verMonitorDeCalculoComisiones();" class="">	
			<s:text name="Actualizar"/>		    
		</a>
	 </s:elseif>
</div>
<div align="center" id="monitorBonoGrid" width="880px" height="350px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>
<div class="btn_back w100" id="" >
	<s:if test="gridMonitor == 'BONOS'">
		<a id="programar" href="javascript: void(0);"  
			onclick="regresarAProgramacionBonos();" class="">	
		<s:text name="Regresar"/>	
		</a>
	</s:if>	
	 <s:elseif test="gridMonitor == 'COMIS'">
	 	<a id="programar" href="javascript: void(0);"  
			onclick="regresarAConfigComisiones();" class="">	
		<s:text name="Regresar"/>	
		</a>
	 </s:elseif>	
</div>
<script type="text/javascript">
	if(jQuery('#gridMonitor').val() == 'BONOS')
	{
		cargaMonitorBonoGrid();
		
	}else if (jQuery('#gridMonitor').val() == 'COMIS')
	{
		cargaMonitorComisionesGrid();
	}	
	
</script>