<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
			 <call command="attachHeader">
			 		<s:if test="gridMonitor != 'COMIS' "><param>#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,&amp;nbsp;</param></s:if>
			 	    <s:else><param>#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,&amp;nbsp;</param></s:else>
			 </call> 
        </beforeInit>
        <column id="" type="ro" width="30" sort="int" hidden="false" align="center"><s:text name="id"/></column>
       <s:if test="gridMonitor != 'COMIS' "> <column id="" type="ro" width="*" sort="str" hidden="false"><s:text name="Nombre del Bono"/></column></s:if>
       <s:else><column id="" type="ro" width="*" sort="str" hidden="false"><s:text name="Descripcion estatus"/></column></s:else>
        <column id="" type="ro" width="100" sort="str" hidden="false"><s:text name="Modo de Ejecución"/></column>
        <column id="" type="ro" width="100" sort="date_custom" hidden="false" align="center"><s:text name="Estatus de Ejecución"/></column>          
        <column id="" type="ro" width="100" sort="date_custom" hidden="false" align="center"><s:text name="Fecha de Ejecución"/></column>
        <column id="" type="ro" width="100" sort="str" hidden="false" align="center"><s:text name="Usuario"/></column>
        <s:if test="gridMonitor == 'COMIS' "><column id="" type="ro" width="60" sort="str" hidden="false"><s:text name="Id Preview"/></column></s:if>
        <column id="avanceCell" type="ro" width="100" sort="str" hidden="false" align="center"><s:text name="% Avance"/></column>
<%--         <column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>       --%>
	</head>

	<s:iterator value="monitorEjecucionesBonoList">
		<row id="<s:property value="id" escapeHtml="false"/>">
			<cell><s:property value="id" escapeHtml="false"/></cell>
			<cell><s:property value="nombreConfiguracionBono" escapeHtml="false"/></cell>
			<cell><s:property value="modoEjecucion" escapeHtml="false"/></cell>
			<cell><s:property value="estatusEjecucion" escapeHtml="false"/></cell>
			<cell><s:property value="fechaEjecucion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuario" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="gridMonitor == 'COMIS' "><cell><s:property value="idPreviewCalculo" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<cell><s:property value="avance" escapeHtml="false" escapeXml="true"/></cell>
<%-- 			<cell>/MidasWeb/img/cross16.png^Eliminar Programacion^javascript:eliminarProgramacion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell> --%>
		</row>
	</s:iterator>
</rows>