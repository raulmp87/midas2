<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="" type="ro" width="50" sort="int">id</column>
		<column id="" type="ro" width="180" sort="int">Importe Mínimo</column>
		<column id="" type="ro" width="*" sort="str">Activo/ Inactivo</column>
		<column id="" type="ro" width="*" sort="str">Sin Factura</column>
		<column id="" type="ro" width="200" sort="str">Modo de Ejecución</column>
		<column id="" type="ro" width="*" sort="str">Periodo</column>
		<column id="" type="ro" width="*" sort="str">Horario</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaConfigPagoComisiones" var="rowConfigPagoscomisiones" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowConfigPagoscomisiones.id}]]></cell>
			<cell><![CDATA[${rowConfigPagoscomisiones.importeMinimo}]]></cell>
				<s:if test="%{#rowConfigPagoscomisiones.activo == 1}" >
					<cell><s:text name="midas.catalogos.centro.operacion.activo" /></cell>
			    </s:if>
				<s:else>
					<cell><s:text name="midas.catalogos.centro.operacion.inactivo" /></cell>
				</s:else>
					<s:if test="%{#rowConfigPagoscomisiones.pagoSinFactura == 1}" >
						<cell>Si</cell>
			    	</s:if>
					<s:else>
					<cell>No</cell>
					</s:else>
			<cell><![CDATA[${rowConfigPagoscomisiones.modoEjecucion.valor}]]></cell>
			<cell><![CDATA[${rowConfigPagoscomisiones.periodoEjecucion.valor}]]></cell>
			<cell><![CDATA[${rowConfigPagoscomisiones.horarioEjecucion}]]></cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleConfiguracionPagosComisionesaPath, 2,{"configComisiones.id":${rowConfigPagoscomisiones.id},"idRegistro":${rowConfigPagoscomisiones.id},"idTipoOperacion":60})^_self</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleConfiguracionPagosComisionesaPath, 4,{"configComisiones.id":${rowConfigPagoscomisiones.id},"idRegistro":${rowConfigPagoscomisiones.id},"idTipoOperacion":60})^_self</cell>
			</s:if>			
		</row>
	</s:iterator>
</rows>