<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/pagosComisionHeader.jsp"></s:include>

<script type="text/javascript">
botonEjecutar();
jQIsRequired();
mostrarComboDias();
jQuery(document).ready(function(){
	
	deshabilitarCampoAgentes();
	
	setTimeout(function() {
		checkActivo();
	}, 5);
});


function deshabilitarCampoAgentes(){
		
	if(jQuery('input[name="configComisiones.todosLosAgentesAfirmeBoolean"]:checked').length > 0){
		jQuery('#agentes').attr('readonly','true');
	}else{
		jQuery('#agentes').attr('readonly','');
	}	
}

</script>
<style type="text/css">
   ul { height: 100px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
		 list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }
   label { display: block; color: WindowText; background-color: Window; margin: 0; padding: 0; width: 100%; font-size: 10px;}
   label:hover { background-color: Highlight; color: HighlightText; }
  </style>
  <s:hidden name="tipoAccion"/>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloConsultar')}"/>
	<script type="text/javascript">
		enableListChecs();
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloEditar')}"/>
</s:if>



<div class="titulo w800"><s:text name="#titulo"/></div>
<s:form name="configPagoComisionesForm" id="configPagoComisionesForm"> 
	<s:hidden name="configComisiones.id"/>
	<table width="98%" class="contenedorFormas" bgcolor="white">
		<tr>
			<td class="Titulo" colspan="9">
				<s:text name="midas.fuerzaventa.configuracionPagosComisiones.produccion"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				&nbsp;
			</td>
			<td>
				Seleccionar Todo 
			</td>
			<td>
				<s:checkbox name="checarTodos" id="checarTodos" onchange="javascript:chec('js_checked')"/>
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.catalogos.centro.operacion.centroDeOperacion"></s:text></td>
			<td>
				<ul>
				    <s:iterator value="centroOperacion" var ="varCentroOperacion" status="stat">
					     <li>
					     	<s:if test="#varCentroOperacion.checado==1">
						     	<label for="varCentroOperacion[%{#stat.index}].id">
						     		<input name="centroOperacionesSeleccionados[${stat.index}].centroOperacion.id" 
						     			   id="centroOperacionesSeleccionados${stat.index}" 	
						     			   value="${varCentroOperacion.id}" class="js_checked"
						     			   validar = "true" type="checkbox"
						     			   checked="checked"/>					     					     		
						     		 ${varCentroOperacion.descripcion}
						     	</label>
						     </s:if>
						     <s:else>
						     	<label for="varCentroOperacion[%{#stat.index}].id">
						     		<input name="centroOperacionesSeleccionados[${stat.index}].centroOperacion.id" 
						     			   id="centroOperacionesSeleccionados${stat.index}" 	
						     			   value="${varCentroOperacion.id}" class="js_checked"
						     			   validar = "true" type="checkbox"/>					     					     		
						     		 ${varCentroOperacion.descripcion}
						     	</label>
						     </s:else>
					    </li>
				    </s:iterator>
				</ul>
			</td>
			
			<td>
		<s:text name="midas.excluir.config.idAgente"></s:text>
	</td>
			<td><s:textfield name="configComisiones.agentes" id="agentes" placeholder="Ejemplo : 10000-10020,10040,10047,10058,10061,10075"
				cssClass="cajaTextoM2 w300" disabled="#readOnly" 
				onblur="javascript:fnValidaInput()" ></s:textfield>
		</td>
	&nbsp;
			<td>
				<s:text name="midas.fuerzaventa.configuracionPagosComisiones.todos"/> 
			</td>
			<td>
				<s:checkbox name="configComisiones.todosLosAgentesBoolean" value="configComisiones.todosLosAgentes" disabled="#readOnly" onclick="habilitarAgenteInicioFin();"/>
		  	</td>
		  	
		  	<td>
				<s:text name="midas.fuerzaventa.configuracionPagosComisiones.todos.agentes.afirme"/> 
			</td>
			<td>
				<s:checkbox id="todosAgentesAfirme" name="configComisiones.todosLosAgentesAfirmeBoolean" value="configComisiones.todosLosAgentesAfirme" onclick="deshabilitarCampoAgentes();" />
		  	</td>
		  	
		  	
		</tr>
		</tr>
		<tr>
			<td>
				<s:text name="midas.negocio.gerencia"></s:text>
			</td>
			<td>
				<ul>
				    <s:iterator value="listarGerencia" var="varGerenciaList" status="stat">
				     	<li>
				     	<s:if test="#varGerenciaList.checado==1">
				    		 <label for="varGerenciaList[%{#stat.index}].id">
				    		 <input name="listarConfigComGerenciaInt[${stat.index}]" 
				    		 		id="listaGerencias${stat.index}" 
				    		 		value="${varGerenciaList.id}" class="js_checked"
				    		 		validar = "true" type="checkbox"
				    		 		checked="checked"/>
				    				${varGerenciaList.descripcion}
				    		 </label>
				    	</s:if>
				    	<s:else>
				    		<label for="varGerenciaList[%{#stat.index}].id">
				    		 <input name="listarConfigComGerenciaInt[${stat.index}]" 
				    		 		id="listaGerencias${stat.index}" 
				    		 		value="${varGerenciaList.id}" class="js_checked"
				    		 		validar = "true" type="checkbox">
				    				${varGerenciaList.descripcion}
				    		 </label>
				    	</s:else>
				     	</li>
				    </s:iterator>
				</ul>
			</td>
			<td colspan="3"><s:text name="midas.fuerzaventa.negocio.clasificacionAgente"/></td>
			<td colspan="5">
				     <ul>
				        <s:iterator value="catalogoTipoAgente" var ="varCatalogoTipoAgente" status="stat">
				     	<li>
				     	<s:if test="#varCatalogoTipoAgente.checado==1">
				    		 <label for="varCatalogoTipoAgente[%{#stat.index}].id">
				    		 <input name="tipoAgenteSeleccionado[${stat.index}].tipoAgente.id" 
				    		 		id="tipoAgenteSeleccionado${stat.index}" 
				    		 		value="${varCatalogoTipoAgente.id}" class="js_checked"
				    		 		validar = "true" type="checkbox"
				    		 		checked="checked">
				    		 		${varCatalogoTipoAgente.valor}
				    		 </label>
				    	</s:if>
				    	<s:else>
				    		<label for="varCatalogoTipoAgente[%{#stat.index}].id">
				    		 <input name="tipoAgenteSeleccionado[${stat.index}].tipoAgente.id" 
				    		 		id="tipoAgenteSeleccionado${stat.index}" 
				    		 		value="${varCatalogoTipoAgente.id}" class="js_checked"
				    		 		validar = "true" type="checkbox"/>
				    		 		${varCatalogoTipoAgente.valor}
				    		 </label>
				    	</s:else>
				     	</li>
				      </s:iterator>
				</ul>
			</td>
		</tr>
		<tr>
			<td><s:text name="midas.fuerzaventa.negocio.ejecutivo"/></td>
			<td>
			 <ul>
			    <s:iterator value="ejecutivoList" var="varEjecutivoList" status="stat">
				   	<li>
				   	<s:if test="#varEjecutivoList.checado==1">
			    		 <label for="varEjecutivoList[%{#stat.index}].id">
			    		 <input name="listarConfigComEjecutivoInt[${stat.index}]" 
			    		 		id="listarConfigComEjecutivo${stat.index}"
			    		 		value="${varEjecutivoList.id}" class="js_checked"
			    		 		validar = "true" type="checkbox" checked="checked"/>
 			    		${varEjecutivoList.nombreCompleto}
			    		 </label>
			    	</s:if>
			    	<s:else>
			    		 <label for="varEjecutivoList[%{#stat.index}].id">
			    		 <input name="listarConfigComEjecutivoInt[${stat.index}]" 
			    		 		id="listarConfigComEjecutivo${stat.index}"
			    		 		value="${varEjecutivoList.id}" class="js_checked"
			    		 		validar = "true" type="checkbox" />
 			    		${varEjecutivoList.nombreCompleto}
			    		 </label>
			    	</s:else>
			     	</li>
			      </s:iterator>
			</ul>
		 </td>

			<td colspan="3"><s:text name = "midas.fuerzaventa.negocio.prioridad"/></td>
			<td colspan="5">
				<ul>
			    <s:iterator value="catalogoPrioridad" var="varCatalogoPrioridad" status="stat">
				   	<li>
				   	<s:if test="#varCatalogoPrioridad.checado==1">
			    		 <label for="varCatalogoPrioridad[%{#stat.index}].id">
			    		 <input name="catalogoPrioridadSeleccionado[${stat.index}].prioridadAgente.id" 
			    		 		id="catalogoPrioridadSeleccionado${stat.index}" 
			    		 		value="${varCatalogoPrioridad.id}" class="js_checked"
			    		 		validar = "true" type="checkbox" checked="checked"/>
			    		         ${varCatalogoPrioridad.valor}
			    		 </label>
			    	</s:if>
			    	<s:else>
			    		<label for="varCatalogoPrioridad[%{#stat.index}].id">
			    		 <input name="catalogoPrioridadSeleccionado[${stat.index}].prioridadAgente.id" 
			    		 		id="catalogoPrioridadSeleccionado${stat.index}" 
			    		 		value="${varCatalogoPrioridad.id}" class="js_checked"
			    		 		validar = "true" type="checkbox"/>
			    		         ${varCatalogoPrioridad.valor}
			    		 </label>
			    	</s:else>
			     	</li>
			      </s:iterator>
				</ul>
			</td>
		</tr>
		<tr>
			<td><s:text name = "midas.boton.promotoria"/></td>
			<td>
				<ul>
			    <s:iterator value="promotoriaList" var="varPromotoriaList" status="stat">
				   	<li>
				   	<s:if test="#varPromotoriaList.checado==1">
			    		 <label for="varPromotoriaList[%{#stat.index}].id">
			    		 <input name="listarConfigComPromotoriaInt[${stat.index}]" 
			    		 		id="promotoria${stat.index}" 
			    		 		value="${varPromotoriaList.id}" class="js_checked"
			    		 		validar = "true" type="checkbox" checked="checked"/>
			    		 ${varPromotoriaList.descripcion}
			    		 </label>
			    	</s:if>
			    	<s:else>
			    		<label for="varPromotoriaList[%{#stat.index}].id">
			    		 <input name="listarConfigComPromotoriaInt[${stat.index}]" 
			    		 		id="promotoria${stat.index}" 
			    		 		value="${varPromotoriaList.id}" class="js_checked"
			    		 		validar = "true" type="checkbox"/>
			    		 ${varPromotoriaList.descripcion}
			    		 </label>
			    	</s:else>
			     	</li>
			      </s:iterator>
				</ul>
			</td>
			<td colspan="3"><s:text name="midas.cotizacion.estatus"/></td>
			<td colspan="5">
				<ul>
				    <s:iterator value="catalogoTipoSituacion" var="varCatalogoTipoSituacion" status="stat">
					     <li>
					     <s:if test="#varCatalogoTipoSituacion.checado==1">
					     	<label for="varCatalogoTipoSituacion[${stat.index}].id">
					     		<input name="seleccionarTipoSituacion[${stat.index}].situacionAgente.id" 
					     				id="seleccionarTipoSituacion${stat.index}" 
					     				value="${varCatalogoTipoSituacion.id}" class="js_checked"
					     				validar = "true" type="checkbox" checked="checked" /> 					  
					     		       ${varCatalogoTipoSituacion.valor}				
					     	</label>
					     </s:if>
					     <s:else>
					     	<label for="varCatalogoTipoSituacion[${stat.index}].id">
					     		<input name="seleccionarTipoSituacion[${stat.index}].situacionAgente.id" 
					     				id="seleccionarTipoSituacion${stat.index}" 
					     				value="${varCatalogoTipoSituacion.id}" class="js_checked"
					     				validar = "true" type="checkbox"/> 					  
					     		       ${varCatalogoTipoSituacion.valor}				
					     	</label>
					     </s:else>
					    </li>
				    </s:iterator>
				</ul>
			</td>
		</tr>
		<tr>
			<td> <s:text name = "midas.agentes.tipoPromotoria"/></td>
			<td>
				<ul>
				    <s:iterator value="listaTipoPromotoria" var="varListaTipoPromotoria" status="stat">
					     <li>
					     <s:if test="#varListaTipoPromotoria.checado==1">
					     	<label for="varListaTipoPromotoria[${stat.index}].id">
					     		<input name="TipoPromotoriaSeleccionado[${stat.index}].tipoPromotoria.id" 
					     				id="tipoPromotoria${stat.index}" class="js_checked"
					     				value="${varListaTipoPromotoria.id}"
					     				validar = "true" type="checkbox" checked="checked"/>
					     				${varListaTipoPromotoria.valor}
					     	</label>
					     </s:if>
					     <s:else>
					     	<label for="varListaTipoPromotoria[${stat.index}].id">
					     		<input name="TipoPromotoriaSeleccionado[${stat.index}].tipoPromotoria.id" 
					     				id="tipoPromotoria${stat.index}" class="js_checked"
					     				value="${varListaTipoPromotoria.id}"
					     				validar = "true" type="checkbox"/>
					     				${varListaTipoPromotoria.valor}
					     	</label>
					     </s:else>
					    </li>
				    </s:iterator>				    
				</ul>
				</td>
				<td colspan="3">
				<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.motivoEstatus"/> 
				</td>
				<td>
					<ul>
				    <s:iterator value="listMotivoStatus" var="varlistMotivoStatus" status="stat">
				    	<li>
				    	<s:if test="#varlistMotivoStatus.checado==1">
					     	<label for="varlistMotivoStatus[${stat.index}].id">
					     		<input name="listMotivoStatusSeleccionado[${stat.index}].idMotivoEstatus.id" 
					     				id="listMotivoStatusSeleccionado${stat.index}" 
					     				value="${varlistMotivoStatus.id}" class="js_checked"
					     				validar = "true" type="checkbox" checked="checked" /> 					  
					     		       ${varlistMotivoStatus.valor}				
					     	</label>
					     </s:if>
					     <s:else>
					     	<label for="varlistMotivoStatus[${stat.index}].id">
					     		<input name="listMotivoStatusSeleccionado[${stat.index}].idMotivoEstatus.id" 
					     				id="listMotivoStatusSeleccionado${stat.index}" 
					     				value="${varlistMotivoStatus.id}" class="js_checked"
					     				validar = "true" type="checkbox"/> 					  
					     		       ${varlistMotivoStatus.valor}				
					     	</label>
					     </s:else>
					    </li>
				    </s:iterator>
				</ul>
			</td>
		</tr>
	</table>
	<table width="98%" class="contenedorFormas no-Border" align="center">
		<tr>
			<td valign="top">
				<table class="contenedorFormas w290 h160" >
					<tr>
						<td class="titulo" colspan="2"><s:text name="midas.fuerzaventa.configuracionPagosComisiones.condiciones"/></td>
					</tr>
					<tr>
						<td class="jQIsRequired"> 
							<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.importeMinimo"/>
						</td>
						<td>
							<s:textfield name="configComisiones.importeMinimo" value="%{getText('struts.number.format',{configComisiones.importeMinimo})}" disabled="#readOnly" maxlength="12" cssClass="cajaTextoM2 jQrequired" />
						</td>
					</tr>
					<tr>
						<td>
							<s:checkbox name ="configComisiones.activoBoolean" id="activo" value="configComisiones.activo" disabled="#readOnly" onclick="javascript:checkActivo();"></s:checkbox>
						</td>
						<td>
							<s:text name = "midas.catalogos.centro.operacion.activo"></s:text>
						</td>
					</tr>
					<tr>
						<td>
							<s:checkbox name ="configComisiones.pagoSinFacturaBoolean" value="configComisiones.pagoSinFactura" id="pagoSinFacturaBoolean" disabled="#readOnly"></s:checkbox>
						</td>
						<td>
							<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.pagarSinFactura"/>
						</td>
					</tr>
					<tr>
						<td>
							<s:text name="midas.cotizacion.iniciovigencia"/>
						</td>
						<td>
							<s:if test="tipoAccion == 2">
								<s:textfield name="configComisiones.fechaInicioVigencia" id="inicioVigenciaInactivo" cssClass="cajaTextoM2" disabled="#readOnly"/>
							</s:if>
							<s:else>
								<sj:datepicker name="configComisiones.fechaInicioVigencia" disabled="#readOnly" 
								buttonImage="../img/b_calendario.gif" id="inicioVigencia"
								id="inicioVigencia" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx " 								   								  
								onkeypress="return soloFecha(this, event, false);" disabled="#readOnly"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="esFechaValida(this);"></sj:datepicker>
							</s:else>
						</td> 
					</tr>
				</table>
			</td>
			<td valign="top">
				<table class="contenedorFormas w290 h160">
					<tr>
						<td class="Titulo"><s:text name="midas.fuerzaventa.configuracionPagosComisiones.modoEjecicion"/></td>
					<tr/>
					<tr>
						<td>
							<s:select name="configComisiones.modoEjecucion.id" id ="comboModoEjecu" cssClass="cajaTextoM2 w150 jQrequired"
				      		 headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        	 list="modoEjecucion" listKey="id" onchange="botonEjecutar()" listValue="valor"/>
						</td>				
					</tr>
					<tr>
						<td>
							<s:if test="tipoAccion != 2">
								<div align="right" class="inline" style="display:none" id="btEjecutar">
									<div class="btn_back w110">
										<a href="javascript: void(0);" class=""
										onclick="javascript:ejecutarConfiguracion();">
										<s:text name="midas.fuerzaventa.configuracionPagosComisiones.ejecutar"/>
										</a>
									</div>	
								</div>
							</s:if>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top">
				<table class="contenedorFormas w290 h160">
					<tr>
						<td class = "Titulo" colspan="4"> <s:text name="midas.fuerzaventa.configuracionPagosComisiones.periodicidad"/></td>
					</tr>
					<tr>
						<td>
							<s:text name ="midas.fuerzaventa.configuracionPagosComisiones.periodo"/>
						</td>
						<td>
							<s:select name="configComisiones.periodoEjecucion.id" id="periodoEjec" cssClass="cajaTextoM2 w150"
				      				  headerKey="" headerValue="Seleccione.." disabled="#readOnly" onchange="javascript:mostrarComboDias();"
				        			 list="periodoejecucion" listKey="id" listValue="valor"/>
				        </td>
				     </tr>
				     <tr>
						<td>
							<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.dia"/>
						</td>
						<td>
							<s:select name="configComisiones.diaEjecucion"  id="diaSemana" cssClass="cajaTextoM2 w150" 
							disabled="#readOnly" headerKey="" headerValue="Seleccione.."
							list="diasEjecucion" listKey="id" listValue="valor"/>
						</td> 
					</tr>
<!-- 					<tr> -->
<!-- 						<td> -->
<%-- 							<s:text name="midas.fuerzaventa.configuracionPagosComisiones.horario"/> --%>
<!-- 						</td> -->
<!-- 						<td> -->
<%-- 							<s:select name="configComisiones.horarioEjecucion"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly" --%>
<%-- 						list="#{'00:00':'00:00','01:00':'01:00','02:00':'02:00','03:00':'03:00','04:00':'04:00','05:00':'05:00', --%>
<!-- 								'06:00':'06:00','07:00':'07:00','08:00':'08:00','09:00':'09:00','10:00':'10:00','11:00':'11:00','12:00':'12:00', -->
<!-- 								'13:00':'13:00','14:00':'14:00','15:00':'15:00','16:00':'16:00','17:00':'17:00','18:00':'18:00','19:00':'19:00', -->
<!-- 								'20:00':'20:00','21:00':'21:00','22:00':'22:00','23:00':'23:00'}"/>  -->
<!-- 						</td> -->
<!-- 					</tr> -->
				</table>
			</td>
		</tr> 
		<tr>
		<td colspan="6">
			<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
		</td>
	</tr>
 	</table> 
 		<table width="98%" class="contenedorFormas" align="center">	
			<tr>
				<s:if test="{configComisiones.id!=null}">
					<td>
						<s:text name="midas.fuerzaventa.negocio.ultimaModificacion"/>
					</td>
					<td>
						<s:textfield name="ultimaModificacion.fechaHoraActualizacion" id="txtFechaHora" readonly="true" cssClass="cajaTextoM2"></s:textfield>
					</td>
					<td>
						<s:text name="midas.fuerzaventa.negocio.usuario"/>
					</td>
					<td >
						<s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" cssClass="cajaTextoM2" readonly="true"></s:textfield>
					</td>
					<td colspan="2" align="right">							
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_guardar" onclick="javascript: mostrarHistorico(60,${configComisiones.id});">	
							<s:text name="midas.boton.historico"/></a>	
						</div>				
					</td>
				</s:if>		
			</tr>		
		</table>
	<div align="right" class="w910 inline" >
		<div class="btn_back w110">
			<a href="javascript: mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath);" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
	<s:if test="tipoAccion != 2">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar"
					onclick="javascript:GuardarConfigPagosComisiones();">
					<s:text name="midas.boton.guardar"/>
				</a>
			</div>
		</s:if>
		</div>
</s:form>