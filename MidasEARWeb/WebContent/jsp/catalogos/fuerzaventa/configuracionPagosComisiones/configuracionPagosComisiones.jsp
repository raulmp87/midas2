<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/pagosComisionHeader.jsp"></s:include>

<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarConfiguracionPagocomisionesPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"configuracionPagosComisionesGrid", null,idField);
 });
 </script>
 
<s:form action="listarFiltrado" name="catalogoConfigComForm" id = "catalogoConfigComForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td colspan="6" class="titulo"> <s:text name = "midas.catalogos.agente.pagocomisiones.tituloCatalogo"/></td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.catalogos.centro.operacion.centroDeOperacion"/>
			</td>
			<td>
				<s:select name="listaFiltro.idCentroOperacion" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="centroOperacion" listKey="id" listValue="descripcion"/>
			</td>
			<td>
				<s:text name="midas.negocio.gerencia"/>
			</td>			
			<td>
				<s:select name="listaFiltro.idGerencia" cssClass="cajaTextoM2 jQrequired w150"
				 	headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				 	list="listarGerencia" listKey="id" listValue="descripcion"/>
			</td>
				 
			<td>
				<s:text name="midas.fuerzaventa.negocio.prioridad"/>
			</td>
			<td>
				<s:select name="listaFiltro.idProridad" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="catalogoPrioridad" listKey="id" listValue="valor"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.ejecutivo.ejecutivo"/>
			</td>
			<td>
				<s:select name="listaFiltro.idEjecutivo" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="ejecutivoList" listKey="id" listValue="nombreCompleto"/>
			</td>
			<td>
				<s:text name="midas.boton.promotoria"/>
			</td>
			<td>
				<s:select name="listaFiltro.idPromotoria" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="promotoriaList" listKey="id" listValue="descripcion"/>
			</td>
			<td>
				<s:text name="midas.agentes.tipoPromotoria"/>
			</td>
			<td>
				<s:select name="listaFiltro.idTipoPromotoria" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="listaTipoPromotoria" listKey="id" listValue="valor"/>
			</td>
		</tr>
		<tr>
			<td class="inline">
				<s:checkbox name ="listaFiltro.pagoSinFacturaBoolean"/><s:text name="midas.fuerzaventa.configuracionPagosComisiones.pagarSinFactura"/>
				</td>&nbsp;<td>
			</td>
			<td>
				<s:text name = "midas.excepcion.suscripcion.auto.agente"/>
			</td>
			<td class="inline">
				<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.inicio"/>

				<s:textfield name="listaFiltro.agenteInicio" cssClass="W50 cajaTextoM2 jQnumeric jQrestrict"/>

				<s:text name = "midas.fuerzaventa.configuracionPagosComisiones.fin"/>
				<s:textfield name="listaFiltro.agenteFin" cssClass=" W50 cajaTextoM2 jQnumeric jQrestrict"/>
			</td> 
			<td>
				<s:text name = "midas.fuerzaventa.negocio.tipoAgente"/>
			</td>
			<td>
				<s:select name="listaFiltro.idTipoAgente" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="catalogoTipoAgente" listKey="id" listValue="valor"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name ="midas.fuerzaventa.configuracionPagosComisiones.modoEjecucion"/>
			</td>
			<td>
				<s:select name="listaFiltro.modoEjecucion.id" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="modoEjecucion" listKey="id" listValue="valor"/>
			</td>
			<td class="jQisRequired">
				<s:text name="midas.fuerzaventa.configuracionPagosComisiones.periodoEjecucion"/>
			</td>
			<td>
				<s:select name="listaFiltro.periodoEjecucion.id" cssClass="cajaTextoM2 w150"
				       headerKey="" headerValue="Seleccione.." disabled="#readOnly"
				        list="periodoejecucion" listKey="id" listValue="valor"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name ="midas.fuerzaventa.negocio.fechaAltaInicio"/>
			</td>
			<td>
				<sj:datepicker name="listaFiltro.fechaAltaInicio" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif"
					id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td>
				<s:text name = "midas.fuerzaventa.negocio.fechaAltaFin"/>
			</td>
			<td>
				<sj:datepicker name="listaFiltro.fechaAltaFin" disabled="#readOnly"
					buttonImage="../img/b_calendario.gif"
					id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx jQrequired" 								   								  
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		<td colspan="4" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:listarFiltradoGenerico(listarFiltradoConfigcomisionesPath, 'configuracionPagosComisionesGrid', document.catalogoConfigComForm);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="configuracionPagosComisionesGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>		
</s:form>

<div id="pagingArea"></div><div id="infoArea"></div>
<div class="w880">
<table style="width:30%;" align="right">
	<tr>
		<td>
			<div class="btn_back w100" style="display: inline; margin-left: 1%; float: right; ">
				<a href="javascript: void(0);" class="icon_guardar"
					onclick="javascript:operacionGenerica(verDetalleConfiguracionPagosComisionesaPath,1);">
					<s:text name="midas.boton.agregar"/>
				</a>
			</div>						
		</td>
	</tr>
</table>
</div>
