<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
        <column id="id" type="ro" width="*" sort="int">Id</column>
		<column id="descripcionBono" type="ro" width="*" sort="int"><s:text name="midas.fuerzaventa.configBono.descripcionBono"/> </column>
		<column id="tipoBono" type="ro" width="*" sort="date"><s:text name="midas.fuerzaventa.configBono.tipoBono"/></column>
		<column id="fechaCorte" type="ro" width="*" align="center" sort="int"><s:text name="midas.calculos.fechaCorte"/></column>
		<column id="totalBeneficiarios" type="ro" width="*" align="right" sort="int"><s:text name="midas.fuerzaventa.configBono.totalBeneficiario"/></column>
		<column id="importeTotal" type="ro" width="*" align="right" sort="int"><s:text name="midas.fuerzaventa.configBono.importeTotal"/></column>
		<column id="modoEjecucion" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.configuracionPagosComisiones.modoEjecucion"/></column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="calculoBonoList" var="listCalc" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${listCalc.id}]]></cell>
			<cell><![CDATA[${listCalc.descripcionBono}]]></cell>
			<cell><![CDATA[${listCalc.tipoBono.valor}]]></cell>						
			<cell><![CDATA[${listCalc.fechaCorteString}]]></cell>
			<cell><![CDATA[${listCalc.totalBeneficiarios}]]></cell>
			<cell><![CDATA[${listCalc.importeTotal}]]></cell>
			<cell><![CDATA[${listCalc.modoEjecucion.valor}]]></cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verDetalleCalculoBono(${listCalc.id})</cell>
				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:verDetalleCalculoBono(${listCalc.id})^_self</cell>
			</s:if>			
		</row>
	</s:iterator>
</rows>