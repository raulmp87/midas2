<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<table class="contenedorFormas" bgcolor="white" align="center">	
	<tr><s:hidden name="nombreDocumento"></s:hidden>
		<td>
			<s:text name="midas.fuerzaventa.motivoRechazo"/>
		</td>
		<td>
			<s:select name="recepDocs.motivoRechazo.id" cssClass="cajaTextoM2 jQrequired w250" disabled="#readOnly" 
				       headerKey="" headerValue="Seleccione.." 
				      list="motivosRechazo" listKey="id" listValue="valor" value="recepDocs.motivoRechazo.id"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.ejecutivo.descripcion"/>
		</td>
		<td>
			<s:textarea name="recepDocs.descripcion" cssClass="areaTextoM2" rows="10" cols="100"/>
		</td>
	</tr>
</table>