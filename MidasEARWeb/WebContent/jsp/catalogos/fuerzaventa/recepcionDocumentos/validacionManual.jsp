<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/agentes/recepcionDocumentos.js'/>"></script>
<%-- <script type="text/javascript">
// function mostrarModalAgente(){
// 	var url="/MidasWeb/fuerzaventa/agente/mostrarContenedor.action?tipoAccion=consulta&idField=idagente";
// 	sendRequestWindow(null, url, obtenerVentanaAgente);
// }

// function obtenerVentanaAgente(){
// 	var wins = obtenerContenedorVentanas();
// 	ventanaAgentes= wins.createWindow("agenteModal", 200, 200, 900, 450);
// 	ventanaAgentes.center();
// 	ventanaAgentes.setModal(true);
// 	ventanaAgentes.setText("Consulta de Agente");
// 	ventanaAgentes.button("park").hide();
// 	ventanaAgentes.button("minmax1").hide();
// 	return ventanaAgentes;
// }

// function findByIdAgente(idagente){
// 	dwr.util.setValue("agente.id",idagente);
// 	var url="/MidasWeb/fuerzaventa/agente/findAgenteDetallado.action";
// 	var data={"agente.id":idagente};
// 	jQuery.asyncPostJSON(url,data,populateAgente);
// }

// function populateAgente(json){
// 	if(json){			
// 		dwr.util.setValue("agente.persona.claveTipoPersona",json.agente.persona.claveTipoPersona);
// 		dwr.util.setValue("nombreAgente",json.agente.persona.nombreCompleto);
// 		dwr.util.setValue("rfcAgente",json.agente.persona.rfc);
// 		dwr.util.setValue("numFianza",json.agente.numeroFianza);
// 		dwr.util.setValue("numCedula",json.agente.numeroCedula);
// 		dwr.util.setValue("statusAgente",json.agente.tipoSituacion.valor);
// 		dwr.util.setValue("tipoAgente",json.agente.tipoAgente);
// 		dwr.util.setValue("venceFianza",json.agente.vencimientoFianzaString);
// 		dwr.util.setValue("venceCedula",json.agente.vencimientoCedulaString);
// 		dwr.util.setValue("promotoriaAgente",json.agente.promotoria.descripcion);
// 		dwr.util.setValue("ejecAgente",json.agente.promotoria.ejecutivo.personaResponsable.nombreCompleto);
// 		dwr.util.setValue("gerenciaAgente",json.agente.promotoria.ejecutivo.gerencia.descripcion);
// 		dwr.util.setValue("coAgente",json.agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion);
// 		dwr.util.setValue("domicilio",json.domicilioCompleto);
// 	}
// }	
 </script> --%>
 <script type="text/javascript">
<!--
// 	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteIngresos"></s:url>';
// 	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteIngresos"></s:url>';
	jQuery(document).ready(function() {
// 		listadoService.getMapMonths(function(data) {
// 			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
// 		});
		listadoService.getMapYears(3, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
	});
//-->
</script>
<s:textfield id="idagente" cssStyle="display:none;" onchange="findByIdAgente(this.value);"/>
<s:hidden name="agente.id"/>
<s:hidden name="agente.idAgente"/>
<s:hidden name="agente.persona.claveTipoPersona"/>
<table width="99%" class="contenedorConFormato" align="center">
	<tr>
		<td class="titulo" colspan="8" >
			<s:text name="midas.prestamosAnticipos.subtituloDatosGenerales"/>
		</td>
	</tr>
	<tr>
		<td class="w80">
			<s:text name="midas.prestamosAnticipos.numeroAgente"/>
		</td>
		<td class="w220">
			<div class="elementoInline">   
				<s:textfield cssClass="jQnumeric jQrestrict" onChange="findByClaveAgente();" id="claveAgente"/>
			</div>
			<div class="elementoInline">
				<div class="btn_back w80 btnAut">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:mostrarModalAgente();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>   
			</div>
		</td>
		<td>
			<s:text name="midas.prestamosAnticipos.estatus"/>
		</td>
		<td>
			<s:label name="statusAgente"/>
		</td>
		<td class="w80">
			<s:text name="midas.prestamosAnticipos.centroOperacion"/>
		</td>
		<td>
			<s:label name="coAgente"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.cargos.gridDetalle.nombreAgente"/>
		</td>
		<td>
			<s:label name="nombreAgente"/>
		</td>
		<td>
			<s:text name="midas.negocio.agente.rfc"/>
		</td>
		<td>
			<s:label name="rfcAgente"/>
		</td>
		<td>
			<s:text name="midas.suscripcion.solicitud.solicitudPoliza.oficina"/>
		</td>
		<td>
			<s:label name="ejecAgente"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.negocio.tipoAgente"/>
		</td>
		<td>
			<s:label name="tipoAgente"/>
		</td>
		<td>
			<div>
				<div style="float: left;width: 45%;"><s:text name="midas.prestamosAnticipos.numeroFianza"/></div>
				<div style="float: right;width: 55%;"><s:label name="numFianza"/></div>
			</div>
		</td>
		<td>
			<div>
				<div style="float: left;width: 45%;"><s:text name="midas.prestamosAnticipos.venceFianza"/></div>
				<div style="float: right;width: 55%;"><s:label name="venceFianza"/></div>
			</div>
		</td>
		<td>
			<s:text name="midas.prestamosAnticipos.gerencia"/>
		</td>
		<td>
			<s:label name="gerenciaAgente"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.prestamosAnticipos.domicilio"/>
		</td>
		<td>
			<s:label name="domicilio"/>
		</td>
		<td>
			<div>
				<div style="float: left;width: 45%;"><s:text name="midas.prestamosAnticipos.numeroCedula"/></div>
				<div style="float: right;width: 55%;"><s:label name="numCedula"/></div>
			</div>
		</td>
		<td>
			<div>
				<div style="float: left;width: 45%;"><s:text name="midas.prestamosAnticipos.venceCedula"/></div>
				<div style="float: right;width: 55%;"><s:label name="venceCedula"/></div>
			</div>
		</td>
		<td>
			<s:text name="midas.prestamosAnticipos.promotoria"/>
		</td>
		<td>
			<s:label name="promotoriaAgente"/>
		</td>
	</tr>
</table>
<table width="99%" class="contenedorConFormato" align="center">
	<tr>
		<th><s:text name="midas.reporteAgente.topAgente.anio"></s:text>
		</th>
		<td>
				<select id="anios" name="anio" class="txtfield w200" onchange="muestraDocumentosPorAnio(this.value);"></select>				 
		</td>
		<td>
			<div align="right" class="w80 inline " >
					<div class="btn_back w80">
						<a href="javascript: void(0);"
							onclick="auditarEntregaFacturas();">
							<s:text name="Auditar"/>
						</a>
					</div>		
	 			</div>	
		</td>
		
<!-- 		<td> -->
<%-- 			<s:text name="midas.fuerzaventa.configBono.periodoInicial"/> --%>
<!-- 		</td> -->
		
<!-- 		<td> -->
<%-- 			<s:select name="mesInicio" list="#{'0':'ENERO','1':'FEBRERO','2':'MARZO','3':'ABRIL','4':'MAYO','5':'JUNIO','6':'JULIO', --%>
<!-- 							  '7':'AGOSTO','8':'SEPTIEMBRE','9':'OCTUBRE','10':'NOVIEMBRE','11':'DICIEMBRE'}"/> -->
<!-- 		</td> -->
<!-- 		<td> -->
<%-- 			<s:text name="midas.fuerzaventa.configBono.periodoFinal"/> --%>
<!-- 		</td> -->
<!-- 		<td> -->
<%-- 			<s:select name="mesFin" list="#{'0':'ENERO','1':'FEBRERO','2':'MARZO','3':'ABRIL','4':'MAYO','5':'JUNIO','6':'JULIO', --%>
<!-- 							  '7':'AGOSTO','8':'SEPTIEMBRE','9':'OCTUBRE','10':'NOVIEMBRE','11':'DICIEMBRE'}"/> -->
<!-- 		</td> -->
<!-- 		<td> -->
<!-- 			<div align="right" class="w80 inline btnAut" >		 -->
<!-- 				<div class="btn_back w80"> -->
<!-- 					<a href="javascript: void(0);" -->
<!-- 						onclick="cargarDocumentosFortimax();"> -->
<%-- 						<s:text name="Autorizar"/> --%>
<!-- 					</a> -->
<!-- 				</div>		 -->
<!-- 			</div>	 -->
<!-- 		</td> -->
<!-- 		<td> -->
<!-- 			<div align="right" class="w80 inline btnAut" >		 -->
<!-- 				<div class="btn_back w80"> -->
<!-- 					<a href="javascript: void(0);" -->
<!-- 						onclick="mostrarModalRechazoRecibo();"> -->
<%-- 						<s:text name="Rechazar"/> --%>
<!-- 					</a> -->
<!-- 				</div>		 -->
<!-- 			</div>	 -->
<!-- 		</td> -->
	</tr>
	<tr>
		<td colspan ="4" id="muestraFacturasMesAnio">		
				
		</td>
	</tr>
	 
<!-- 	<tr> -->
<!-- 		<td colspan="8"> -->
<!-- 			<div id=documentos></div> -->
<!-- 		</td> -->
<!-- 	</tr> -->
</table>