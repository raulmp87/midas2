<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden name="documentoFortimax.id"/>
<table>
	<tr>
		<td>
		<div align="right" class="w890 inline" >
			<div class="btn_back w180">
				<a href="javascript: generarLigaIfimaxRecibos();" class="icon_imprimir"
					onclick="">
					<s:text name="midas.boton.digitalizarDoc"/>
				</a>
			</div>			
			<div class="btn_back w180">
				<a href="javascript: guardarDocumentosFortimaxRecibos();" class="icon_guardar2"
					onclick="">
					<s:text name="Guardar"/>
				</a>
			</div>
		</div>
		</td>
	</tr>
</table>