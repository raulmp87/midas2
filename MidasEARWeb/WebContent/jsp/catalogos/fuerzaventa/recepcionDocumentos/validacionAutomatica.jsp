<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<table>
	<tr>
		<td class="titulo" colspan="2" >
			<s:text name="midas.catalogos.centro.operacion.busqueda"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.reportesAgentes.fechaAltaInicio"/>
		</td>
		<td>
			<sj:datepicker   
							buttonImage="../img/b_calendario.gif" 
							id="fecha" maxlength="10" cssClass="jQrequired"								   								  
							onkeypress="return soloFecha(this, event, false);" 
							changeYear="true" changeMonth="true"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onchange='esFechaValida(this);'></sj:datepicker>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.reportesAgentes.fechaAltaFin"/>
		</td>
		<td>
			<sj:datepicker   
							buttonImage="../img/b_calendario.gif" 
							id="fecha" maxlength="10" cssClass="jQrequired"								   								  
							onkeypress="return soloFecha(this, event, false);" 
							changeYear="true" changeMonth="true"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onchange='esFechaValida(this);'></sj:datepicker>
		</td>		
	</tr>
	<tr>		
		<td>
			<s:text name="midas.calculos.numeroAgente"/>
		</td>
		<td>
			<s:textfield/>
		</td>
		<td>
			<s:text name="midas.cargos.gridDetalle.nombreAgente"/>
		</td>
		<td>
			<s:textfield/>
		</td>		
	</tr>	
</table>
<div id="divCarga" style="position:absolute;"></div>
<div align="center" id="personaGrid" width="918px" height="200px" style="background-color:white;overflow:hidden"></div>