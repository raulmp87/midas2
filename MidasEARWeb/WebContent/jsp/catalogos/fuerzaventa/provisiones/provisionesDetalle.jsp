<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/provisiones.js'/>"></script>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.provisiones.tituloAlta')}"/>
	
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.provisiones.tituloConsultar')}"/>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.provisiones.tituloEliminar')}"/>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.provisiones.tituloEditar')}"/>
</s:if>	
<s:if test="provision.claveEstatus.valor == \"AUTORIZADO\"">
	<s:set id="readOnlyEstatus" value="true" />
</s:if>
<div class="titulo w400"><s:text name="#titulo"/></div>
<s:form action="agregar" id="provisionesForm" name="provisionesForm">
<s:hidden name="tipoAccion"/>
<s:hidden name="provision.id"  id="provision_id"/>
<s:hidden name="provision.claveEstatus.valor"  id="provision_estatus"/>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th>&nbsp;<s:text name="midas.provisiones.bono" /></th>	
			<td><s:select id="idBono" list="comboBono" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.configBono.id" cssClass="cajaTextoM2 w150"></s:select></td>
			
			<th>&nbsp;<s:text name="midas.provisiones.agente" /></th>	
			<td colspan="2">
				<table class="contenedorFormas no-border">
					<tr>
						<td><s:text name="midas.provisiones.inicio" /></td>
						<td><s:textfield name="filtrosDetalleProvision.agenteOrigen.idAgente" id="idAgente" cssClass="cajaTextoM2 w80 jQnumeric jQrestrict"></s:textfield></td>
<%-- 						<td><s:text name="midas.provisiones.fin" /></td> --%>
<%-- 						<td><s:textfield name="filtrosDetalleProvision.agenteFin.idAgente" id="agenteFin" cssClass="cajaTextoM2 w80 jQnumeric jQrestrict"></s:textfield></td>			 --%>
					</tr>
				</table>					
			</td>			
					
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.provisiones.gerenciaDirectorReg" /></th>	
			<td><s:select id="idGerencia" list="comboGerencia" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idGerencia" cssClass="cajaTextoM2 w150"  onchange="loadEjecutivoByGerencias();"></s:select></td>		
			
			<th>&nbsp;<s:text name="midas.provisiones.ejecutivo" /></th>	
			<td><s:select id="idEjecutivo" list="comboEjecutivo" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idEjecutivo" cssClass="cajaTextoM2 w150"  onchange="loadPromotoriaByEjecutivo();"></s:select></td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.provisiones.promotoria" /></th>	
			<td><s:select id="idPromotoria" list="comboPromotoria" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.promotoria.id" cssClass="cajaTextoM2 w150" ></s:select></td>
			
			<th>&nbsp;<s:text name="midas.provisiones.lineaVenta" /></th>	
			<td><s:select id="idLineaVenta" list="comboLinaVenta" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idLineaVenta" cssClass="cajaTextoM2 w150" onchange="loadProductoByLineaVenta();" ></s:select></td>
						
<%-- 			<th>&nbsp;<s:text name="midas.provisiones.personaFisica" /></th>	 --%>
<%-- 			<td><s:select list="" listKey="" listValue="" headerKey="0" headerValue="Seleccione" name=""></s:select></td> --%>
		</tr>
		<tr>
<%-- 			<th>&nbsp;<s:text name="midas.provisiones.gerente" /></th>	 --%>
<%-- 			<td><s:select id="idGerente" list="comboGerente" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idGerente" cssClass="cajaTextoM2 w150"></s:select></td> --%>
			
			<th>&nbsp;<s:text name="midas.provisiones.producto" /></th>	
			<td><s:select id="idProducto" list="comboProducto" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idProducto" cssClass="cajaTextoM2 w150" onchange="loadLineaNegocioByProducto();"></s:select></td>
			
			<th>&nbsp;<s:text name="midas.provisiones.lineaNegocio" /></th>	
			<td><s:select id="idLineaNegocio" list="comboLineaNegocio" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione" name="filtrosDetalleProvision.idLineaNegocio" cssClass="cajaTextoM2 w150"></s:select></td>			
<%-- 			<th>&nbsp;<s:text name="midas.provisiones.personaMoral" /></th>	 --%>
<%-- 			<td><s:select list="" listKey="" listValue="" headerKey="0" headerValue="Seleccione" name=""></s:select></td> --%>
		</tr>
		<tr>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: buscaRamosPorFiltro();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>		
			</td>			
		</tr>
	</table>
	<table class="contenedorFormas" width="98%" >
		<tr>
			<td>
				<table class="contenedorFormas no-border">
				<s:iterator value="listaImporteTotalPorRamo" var ="varImporteTotalPorRamo" status="stat">
					<tr>
						<td width="250px">${varImporteTotalPorRamo.descripcion}</td>
						<td>${varImporteTotalPorRamo.importe}</td>
					</tr>		
				</s:iterator>
				</table>
			</td>
			<td valign="top">
				<table class="contenedorFormas no-border"  id="ramosProvisionados" ></table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="contenedorFormas no-border"  >
<!-- 					<tr> -->
<!-- 						<td> -->
<%-- 							<s:iterator value="listaDeRamos" var="varListaDeRamos" status="stat"> --%>
<%-- 							<div>${varListaDeRamos.valor}<s:textfield cssClass="cajaTextoM2 w70"></s:textfield> --%>
<!-- 							</div> -->
<%-- 							</s:iterator> --%>
<!-- 						</td> -->
<!-- 					<tr> -->
<!-- 					<tr> -->
<!-- 						<td>Provision (Agente Cero)</td> -->
<%-- 						<td><s:textfield name="" id="provisionAgenteCeroActual" cssClass="cajaTextoM2 w70"></s:textfield></td> --%>
<%-- 						<td><s:textfield name="" id="provisionAgenteCeroNueva" cssClass="cajaTextoM2 w70"></s:textfield></td> --%>
<!-- 					</tr> -->
				</table>
			</td>			
		</tr>
	</table>
	<table class="contenedorFormas" width="98%" >
		<tr>
			<td colspan="2">
				<div class="titulo w400"><s:text name="Ajuste de Provisión"/></div>
			</td>
		</tr>
		<tr>
			<td>
				<table class="contenedorFormas no-border">
					<tr>
						<td width="250px">
							&nbsp;<s:text name="midas.provisiones.ramo.IncendioPuro" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.incendioPuro" id="incendioPuro" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.maritimoTransporte" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.maritimoTransportes" id="maritimoTransportes" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.diversos" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.diversos" id="diversos" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.automoviles" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.automoviles" id="automoviles" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.responsabilidadCivilgral" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.responsabilidadCivilGeneral" id="responsabilidadCivilGeneral" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.ctastrofico" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.catastroficos" id="catastroficos" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.vidaGpoColectivo" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.vidaGrupoColectivo" id="vidaGrupoColectivo" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;<s:text name="midas.provisiones.ramo.vidaInd" />
						</td>
						<td>
							<s:textfield name="ajusteProvision.vidaInd" id="vidaInd" cssClass="cajaTextoM2 w70" readonly="#readOnlyEstatus"></s:textfield>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
		<table width="98%" class="contenedorFormas no-Border" align="center">	
				<tr>
					<td >
						<div align="right" class="inline" >
							<div class="btn_back w110">
								<a href="javascript: void(0);" class="icon_guardar"
									onclick="javascript: guardaImportesDeRamos();">
									<s:text name="midas.boton.guardar"/>
								</a>
							</div>
							<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_autorizar"
								onclick="javascript: autorizaImportesDeRamos();">
								<s:text name="midas.boton.autorizar"/>
							</a>
							</div>	
							<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_regresar"
								onclick="javascript: regresarCatalogoProvisiones();">
								<s:text name="midas.boton.regresar"/>
							</a>
							</div>	
						</div>
					</td>
				</tr>
			</table>			
		
<%-- 	<s:if test="provision.id != null"> --%>
<!-- 		<table width="98%" class="contenedorFormas" align="center">	 -->
<!-- 			<tr> -->
<%-- 				<td ><s:textfield name="ultimaModificacion.fechaHoraActualizacionString" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td> --%>
<%-- 				<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td> --%>
<!-- 				<td colspan="2" align="right">							 -->
<!-- 					<div class="btn_back w110"> -->
<%-- 						<a href="javascript: void(0);" class="icon_guardar" onclick="javascript: mostrarHistorico(80,${provision.id});">	 --%>
<%-- 						<s:text name="midas.boton.historico"/>	 --%>
<!-- 						</a> -->
<!-- 					</div>				 -->
<!-- 				</td>		 -->
<!-- 			</tr>		 -->
<!-- 		</table> -->
<%-- 	</s:if> --%>
</s:form>		
			