<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
		 
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/fuerzaventa/provisiones/listar.action?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"provisionesGrid", null,idField,'provisionesModal');
 });
	
</script>
<s:form action="listarFiltrado" id="provisionesForm" name="provisionesForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.provisiones.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="Fecha de Alta Inicio" /></th>	
				<td><sj:datepicker name="filtroProvision.fechaProvisionInicio" id="" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);"></sj:datepicker></td>
	 			<th>&nbsp;<s:text name="Fecha de Alta Fin" /></th>		   
	 			<td><sj:datepicker name="filtroProvision.fechaProvisionFin" id="" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);"></sj:datepicker></td>			
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.provisiones.idProvision" /></th>	
			<td><s:textfield name="filtroProvision.id" id="" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.suspensiones.estatus" /></th>	
	 		<td><s:select  name="filtroProvision.claveEstatus.id" id="estatusProvision" cssClass="cajaTextoM2 w130"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="ListEstatusProvision" listKey="id" listValue="valor" disabled="#readOnly"/></td>			
		</tr>		
		<tr>	
			
			<td colspan="6" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico('/MidasWeb/fuerzaventa/provisiones/listarFiltrado.action', 'provisionesGrid', document.provisionesForm,'${idField}','provisionesModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="provisionesGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
