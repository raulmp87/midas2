<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="80" sort="int">Id Provision</column>
		<column id="nombreAgente" type="ro" width="300" sort="str">Nombre Provision</column>
		<column id="estatusSolicitado" type="ro" width="*" sort="str">Fecha de Provision</column>
		<column id="fechaSolicitud" type="ro" width="*" sort="str">Importe de Provision</column>
		<column id="solicitante" type="ro" width="*" sort="str">Modo de Ejecucion</column>
		<column id="estatusMovimiento" type="ro" width="*" sort="str">Estatus</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
<%-- 			<column id="accionEditar" type="img" width="30" sort="na"/> --%>
		</s:if>
	</head>
	<s:iterator value="listaProvisiones" var="rowProvisiones" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${rowProvisiones.id}]]></cell>
			<cell><![CDATA[${rowProvisiones.nombreProvision}]]></cell>
			<cell><![CDATA[${rowProvisiones.fechaProvisionString}]]></cell>
			<cell><![CDATA[${rowProvisiones.importeProvision}]]></cell>
			<cell><![CDATA[${rowProvisiones.modoEjecucion}]]></cell>
			<cell><![CDATA[${rowProvisiones.estatusProvision}]]></cell>
			
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/provisiones/verDetalle.action", 2,{"provision.id":${rowProvisiones.id},"idRegistro":${rowProvisiones.id},"idTipoOperacion":80})^_self</cell>
<%-- 				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/suspensiones/verDetalle.action", 4,{"suspension.id":${rowSuspension.id},"idRegistro":${rowSuspension.id},"idTipoOperacion":80})^_self</cell> --%>
			</s:if>			
	</row> 
	</s:iterator>
</rows>