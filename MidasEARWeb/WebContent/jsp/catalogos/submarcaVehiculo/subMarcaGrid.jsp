<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>		
		<column id="numeroSecuencia" type="ro" width="100" sort="int" hidden="true">Secuencia</column>
		<column id="valor" type="ed" width="*" sort="int">Tipo (SuMarca)    </column>		
	</head>		
	<% int a=0;%>
	<s:iterator value="listaSubmarcas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idSubTcMarcaVehiculo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="descripcionSubMarcaVehiculo" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>	
</rows>




