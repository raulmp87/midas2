<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/gpovarmodprima/gpovarmodprimaHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarGpoVarModPrimaForm();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="grupoVariablesModificacionPrima.id != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.gpovarmodprima.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.gpovarmodprima.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="disabled" value="true" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.gpovarmodprima.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarGpoVarModPrima();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="disabled" value="true" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.gpovarmodprima.borrar.titulo')}" />
</s:else>

<s:form action="guardar" id="gpoVarModPrimaForm" name="gpoVarModPrimaForm" >
	<table  id="agregar" width="97%">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="grupoVariablesModificacionPrima.id"/>
				<s:hidden name="tipoAccion"/>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.clave"></s:text>
			</th>
			<td width="34%"> 
				<s:textfield id="txtClave" key=""
						name="grupoVariablesModificacionPrima.clave" 
						labelposition="left" readonly="#readEditOnly" 
						required="#requiredField" maxlength="5"
						onkeypress="return soloNumerosM2(this, event, false)"
				/>
			</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.descripcion"></s:text>
			</th>
			<td width="34%"> 
				<s:textfield id="txtDescripcion" key=""
						name="grupoVariablesModificacionPrima.descripcion"		  
						labelposition="left"  readonly="#readOnly" 
						required="#requiredField" 
						cssClass="jQToUpper jQAlphaExtra jQRestric"
				/> 
			</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				Grupos
			</th>
			<td width="34%">
		    	<s:select id="idClaveTipoDetalle" key=""
		    		name="grupoVariablesModificacionPrima.claveTipoDetalle"  
		    		labelposition="left" disabled="#readOnly"
		    		required="#requiredField" cssClass="cajaTexto w200"
		    		list="mapClaveTipoDetalle" 
		    		headerKey=""  headerValue = "Seleccione" 
		    		onchange="esconderCampo();"
		    	/>
		 	</td>
		 	<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
		</tr>
		<tr>	   		
	   		<th style="width:100px; text-align:right;">
	   			<div id="idDetalleE" style="display: none">
					<s:text name="midas.catalogos.gpovarmodprima.descripciondetalle"></s:text>
				</div>
			</th>			
			<td width="34%"> 	
				<div id="idDetalle" style="display: none">	  		
					<s:textfield id="txtDescripcionDetalle" key="" 
						name="grupoVariablesModificacionPrima.descripcionDetalle" 
						labelposition="left" readonly="#readOnly" 
						maxlength="20"
						cssClass="jQToUpper jQalphaextra jQRestric"/> 
				</div> 		  		
			</td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="6"> 
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back" >
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"
									  cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
				</td>				
			</tr>
		</s:if>
		<tr>
			<td colspan="6"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  > 
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoGpoVarModPrima(); return false;"
								  cssClass="b_submit icon_regresar w100"/> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="6"> 
					<span style="color:red">
						<s:text name="midas.catalogos.mensaje.requerido"/>
					</span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>
<s:if test="grupoVariablesModificacionPrima.id != null">
<script type="text/javascript">
	esconderCampo();
</script>
</s:if>