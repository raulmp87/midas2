<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/sapamis/supervisionCampo.js'/>"></script>

<script type="text/javascript">
	var listarCatSapAmisSupervisionCampoPath = '<s:url action="listar" namespace="/siniestros/catalogo/supervisionCampo"/>';
	var verDetalleCatSapAmisSupervisionCampoPath = '<s:url action="verDetalle" namespace="/siniestros/catalogo/supervisionCampo"/>';
	var guardarCatSapAmisSupervisionCampoPath = '<s:url action="guardar" namespace="/siniestros/catalogo/supervisionCampo"/>';
	var eliminarCatSapAmisSupervisionCampoPath = '<s:url action="eliminar" namespace="/siniestros/catalogo/supervisionCampo"/>';
	var mostrarCatalogoCatSapAmisSupervisionCampoPath = '<s:url action="mostrarCatalogo" namespace="/siniestros/catalogo/supervisionCampo"/>';
	var listarFiltradoCatSapAmisSupervisionCampoPath = '<s:url action="listarFiltrado" namespace="/siniestros/catalogo/supervisionCampo"/>';
</script>
