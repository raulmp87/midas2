<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/varmodifdescripcion/varModifDescripcionHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="varModifDescripcionForm" >
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.varmodifdescripcion.titulo"/>
			</td>
		</tr>

		<tr>
			<td colspan="6">
				<s:select name="idGrupo" id="idGrupo" 
					key="midas.catalogos.varmodifdescripcion.grupovariables" 
					labelposition="left" list="catalogoValorFijoDTOList" 
					listKey="id.idDato"  cssClass="cajaTexto2"
					listValue="descripcion" headerKey="-1" 
					headerValue="%{getText('midas.general.seleccione')}">
				</s:select>
			</td>
		</tr>
		
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript:listarFiltradoVarModifDescripcion(document.varModifDescripcionForm.idGrupo.value);return false;">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
	<br></br>
	<div id ="varModifDescripcionGrid" style="width:97%;height:250px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>
	
	<div class="alinearBotonALaDerecha" style="margin-right: 30px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoVarModifDescripcion);return false;">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>
	</div>

</s:form>

