<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/varmodifdescripcion/varModifDescripcionHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarVarModifDescripcion();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="varModifDescripcion.id != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.varmodifdescripcion.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.varmodifdescripcion.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.varmodifdescripcion.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarVarModifDescripcion();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.varmodifdescripcion.borrar.titulo')}" />
</s:else>


<s:form action="guardar" id="varModifDescripcionForm">	
	<table  id="agregar" >
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="varModifDescripcion.id"/>
				<s:hidden name="tipoAccion"/>
				<s:if test="varModifDescripcion.id != null">
					<s:hidden name="varModifDescripcion.idGrupo"/>
				</s:if>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.varmodifdescripcion.grupovariables"></s:text>
			</th>
			<td width="30%">
				<s:select name="varModifDescripcion.idGrupo"
						key="" cssClass="cajaTexto2"
						labelposition="left" list="catalogoValorFijoDTOList" 
						value="varModifDescripcion.idGrupo" listKey="id.idDato" 
						listValue="descripcion" disabled="#readEditOnly" 
						required="#requiredField" headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" >
				</s:select>
			</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.numeroSecuencia"></s:text>
			</th>
			<td width="30%">
				<s:textfield name="varModifDescripcion.numeroSecuencia" 
					id="txtNumeroSecuencia" key="" 
					labelposition="left" readonly="#readOnly" required="#requiredField" 
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.valor"></s:text>
			</th>
			<td width="30%"> 
				<s:textfield name="varModifDescripcion.valor" id="txtValor" 
					key="" labelposition="left"  
					readonly="#readOnly" maxlength="6" 
					required="#requiredField" 
					cssClass="jQToUpper jQalphaextra jQRestric"/> 
			</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="6"> 
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"
									  cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
   	 			</td>				
			</tr>
		</s:if>
		<tr>
			<td colspan="6"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  > 
						<s:submit key="midas.boton.regresar" 
						  		  onclick="mostrarCatalogoVarModifDescripcionDetalle(%{varModifDescripcion.idGrupo}); return false;"
						  		  cssClass="b_submit icon_regresar w100" /> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="6">
					<span style="color:red">
						<s:text name="midas.catalogos.mensaje.requerido"/>
					</span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>



