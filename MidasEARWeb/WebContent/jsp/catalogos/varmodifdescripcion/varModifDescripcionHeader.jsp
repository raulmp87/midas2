<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script src="<s:url value='/js/midas2/catalogos/varmodifdescripcion.js'/>"></script>
  
    <script type="text/javascript">
    var listarFiltradoVarModifDescripcionPath = '<s:url action="listarFiltrado" namespace="/varmodifdescripcion"/>';
	var verDetalleVarModifDescripcionPath = '<s:url action="verDetalle" namespace="/varmodifdescripcion"/>';
	var guardarVarModifDescripcionPath = '<s:url action="guardar" namespace="/varmodifdescripcion"/>';
	var eliminarVarModifDescripcionPath = '<s:url action="eliminar" namespace="/varmodifdescripcion"/>';
	var mostrarCatalogoVarModifDescripcionPath = '<s:url action="listarCombo" namespace="/varmodifdescripcion"/>';
	</script>