<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>11</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id.idToSeccion" type="ro" width="0px" sort="int" hidden="true">idToSeccion</column>
		<column id="id.idTcTipoVehiculo" type="ro" width="0px" sort="int" hidden="true">idTcTipoVehiculo</column>
		<column id="descripcionTipoVehiculo" type="ro" width="*" sort="str"><s:text name="midas.catalogos.servvehiculolinnegtipoveh.tipovehiculo"/></column>
		<column id="servVehiculoLinNegTipoVeh.tipoServicioVehiculoDTO.descripcionTipoServVehiculo" type="ro" width="*" sort="str"><s:text name="midas.catalogos.servvehiculolinnegtipoveh.tiposerviciovehiculo"/></column>
		<column id="accionVer" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>
		<column id="accionBorrar" type="img" width="30px" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="servVehiculoLinNegTipoVehList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idToSeccion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="id.idTcTipoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTipoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTipoServicioVehiculo" escapeHtml="false"  escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleServVehiculoLinNegTipoVeh)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleServVehiculoLinNegTipoVeh)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleServVehiculoLinNegTipoVeh)^_self</cell>
		</row>
	</s:iterator>
</rows>