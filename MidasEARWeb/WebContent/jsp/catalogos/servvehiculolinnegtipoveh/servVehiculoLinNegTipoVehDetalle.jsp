<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/servvehiculolinnegtipoveh/servVehiculoLinNegTipoVehHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarServVehiculoLinNegTipoVeh();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="id.idToSeccion != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.servvehiculolinnegtipoveh.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.servvehiculolinnegtipoveh.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.servvehiculolinnegtipoveh.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarServVehiculoLinNegTipoVeh();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.servvehiculolinnegtipoveh.borrar.titulo')}" />
</s:else>


<s:form action="guardar" id="servVehiculoLinNegTipoVehForm">
	<table  id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="tipoAccion"/>
			</td>
		</tr>
		<s:if test="id.idToSeccion != null">
			<s:hidden name="id.idToSeccion"/>
			<s:hidden name="id.idTcTipoVehiculo"/>
		</s:if>
		<tr>
			<th style="width:130px; text-align:right;">
				<s:text name="midas.general.seccion"></s:text>
			</th>
			<td >
				<s:select key="" 
					name="servVehiculoLinNegTipoVeh.id.idToSeccion" 
					id="servVehiculoLinNegTipoVeh.id.idToSeccion" 
					value="ServVehiculoLinNegTipoVeh.id.idToSeccion" 
					list="seccionList" listKey="idToSeccion" 
					listValue="%{descripcion + ' - Versi\u00F3n: ' + version}" labelposition="left"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readEditOnly" required="#requiredEditField"
					onchange="getTipoVehiculoPorSeccion($('servVehiculoLinNegTipoVeh.id.idToSeccion'), $('servVehiculoLinNegTipoVeh.id.idTcTipoVehiculo'));" 
				/>						
			</td>
		</tr>
		<tr>
			<th style="width:130px; text-align:right;">
				<s:text name="midas.general.tipovehiculo"></s:text>
			</th>
			<td >
				<s:select key="" 
					name="servVehiculoLinNegTipoVeh.id.idTcTipoVehiculo" 
					id="servVehiculoLinNegTipoVeh.id.idTcTipoVehiculo" 
					value="servVehiculoLinNegTipoVeh.id.idTcTipoVehiculo" 
					list="tipoVehiculoMap" labelposition="left"
					onchange="getTipoServicioVehiculoPorSeccion($('servVehiculoLinNegTipoVeh.id.idTcTipoVehiculo'), $('servVehiculoLinNegTipoVeh.idTcTipoServicioVehiculo'));"   
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readEditOnly" required="#requiredEditField"/>
			</td>
		</tr>
		<tr>
			<th style="width:130px; text-align:right;">
				<s:text name="midas.general.tiposerviciovehiculo"></s:text>
			</th>
			<td >
				<s:select key="" 
					name="servVehiculoLinNegTipoVeh.idTcTipoServicioVehiculo" 
					id="servVehiculoLinNegTipoVeh.idTcTipoServicioVehiculo" 
					value="servVehiculoLinNegTipoVeh.idTcTipoServicioVehiculo" 
					list="tipoServicioVehiculoMap" labelposition="left"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readOnly" required="#requiredField"/>
			</td>
			<td colspan="2" width="220px">&nbsp;</td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="4"> 
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  >
							<s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"
									  cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
				</td>
				
			</tr>
		</s:if>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoServVehiculoLinNegTipoVeh(); return false;"
								  cssClass="b_submit icon_regresar w100"/> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="4"> 
					<span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>



