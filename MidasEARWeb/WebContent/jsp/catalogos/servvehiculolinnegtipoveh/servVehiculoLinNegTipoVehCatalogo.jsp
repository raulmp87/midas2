<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/servvehiculolinnegtipoveh/servVehiculoLinNegTipoVehHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="servVehiculoLinNegTipoVehForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.servvehiculolinnegtipoveh.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:select key="midas.general.seccion" name="servVehiculoLinNegTipoVeh.id.idToSeccion" 
						  id="servVehiculoLinNegTipoVeh.id.idToSeccion" list="seccionList" listKey="idToSeccion" 
						  listValue="%{descripcion + ' - Versi\u00F3n: ' + version}" onchange="listarFiltradoServVehiculoLinNegTipoVeh();" 
						  labelposition="left"
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
			</td>
		</tr>
	</table>
</s:form>

<br></br>
<div id ="servVehiculoLinNegTipoVehGrid" style="width:97%;height:250px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>

<div class="alinearBotonALaDerecha" style="margin-right: 30px">
	<div id="b_agregar">
		<a href="javascript: void(0);"
			onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoServVehiculoLinNegTipoVeh);return false;">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>

