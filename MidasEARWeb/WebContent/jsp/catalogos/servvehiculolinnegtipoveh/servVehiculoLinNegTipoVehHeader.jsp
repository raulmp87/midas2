<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script> 
	<script src="<s:url value='/js/midas2/catalogos/servvehiculolinnegtipoveh.js'/>"></script>
  
    <script type="text/javascript">
    var listarFiltradoServVehiculoLinNegTipoVehPath = '<s:url action="listarFiltrado" namespace="/servvehiculolinnegtipoveh"/>';
	var verDetalleServVehiculoLinNegTipoVehPath = '<s:url action="verDetalle" namespace="/servvehiculolinnegtipoveh"/>';
	var guardarServVehiculoLinNegTipoVehPath = '<s:url action="guardar" namespace="/servvehiculolinnegtipoveh"/>';
	var eliminarServVehiculoLinNegTipoVehPath = '<s:url action="eliminar" namespace="/servvehiculolinnegtipoveh"/>';
	var mostrarCatalogoServVehiculoLinNegTipoVehPath = '<s:url action="mostrarCatalogo" namespace="/servvehiculolinnegtipoveh"/>';
	</script>