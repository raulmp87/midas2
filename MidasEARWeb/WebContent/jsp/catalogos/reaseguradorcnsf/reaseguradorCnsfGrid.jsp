<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>11</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>  
        </beforeInit>
        <afterInit>        	
        </afterInit>
		<column id="id" type="ro" width="0px" sort="int" hidden="true">id</column>
		<column id="corte" type="ro" width="100px" sort="str"><s:text name="midas.catalogos.reaseguradorescnsf.fechaCorte"/></column>
		<column id="nombreReas" type="ro" width="*" sort="str"><s:text name="midas.catalogos.reaseguradorescnsf.nombreReas"/></column>
		<column id="clave" type="ro" width="100px" sort="int"><s:text name="midas.catalogos.reaseguradorescnsf.claveReas"/></column>
		<column id="claveAnt" width="100px" type="ro"  sort="str"><s:text name="midas.catalogos.reaseguradorescnsf.claveReasAnt"/></column>
		<column id="agencia" width="100px" type="ro"  sort="str"><s:text name="midas.catalogos.reaseguradorescnsf.agencia"/></column>
		<column id="calificacion" width="100px" type="ro"  sort="str"><s:text name="midas.catalogos.reaseguradorescnsf.calificacion"/></column>
		<column id="accionVer" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>
		<column id="accionBorrar" type="img" width="30px" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="reaseguradorMovsList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idreaseguradormov" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechacorte" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreReasegurador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveReasegurador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveReaseguradorAnt" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="nombreAgencia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="calificacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleReaseguradorCnsf)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleReaseguradorCnsf)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleReaseguradorCnsf)^_self</cell>
		</row>
	</s:iterator>
</rows>