<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfCatalogoHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="reaseguradorCnsfForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="10">
				<s:text name="midas.catalogos.reaseguradorescnsf.titulo"/>
			</td>
		</tr>
		<tr>	
			<td colspan="2"><b>
			   	 <sj:datepicker key="midas.catalogos.reaseguradorescnsf.fechaCorte" name="reaseguradorMovs.fechacorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></b>
			</td>
			<td colspan="2">
				<s:select key="midas.catalogos.reaseguradorescnsf.agencia"  
							 		labelposition="top"
							 		id="agenciasList"  cssClass="txtfield" 
									style="width: 100px"
							     	list="agenciasList"
									name="reaseguradorMovs.idagencia"
									headerKey=""
									disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
									headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
									onchange="onChangeAgencia();"/>
			</td>		
			<td colspan="2"> 
							 <s:select key="midas.catalogos.reaseguradorescnsf.calificacion"  
							 		labelposition="top"
							 		id="Catcalificacion"  cssClass="txtfield" 
									style="width: 100px"
							     	list="calificacionesList"
									name="reaseguradorMovs.idcalificacion"
									headerKey=""
									disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
									headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
							
						</td>
			<td colspan="2">
				<s:textfield name="reaseguradorMovs.claveReasegurador" id="txtClaveCnsf" 
					key="midas.catalogos.reaseguradorescnsf.claveReas" cssStyle="cajaTexto"
					labelposition="left" 
				>
				</s:textfield>
			</td>

			<td colspan="2">
				<s:textfield name="reaseguradorMovs.nombreReasegurador" id="txtDescripcion" 
					key="midas.catalogos.reaseguradorescnsf.nombreReas" cssStyle="cajaTexto"
					labelposition="left">
				</s:textfield> 
			</td>
			
		</tr>
		<tr>
			<td class= "buscar" colspan="10">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript:listarFiltradoReaseguradorCnsf();return false;">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</s:form>
<div id ="reaseguradorCnsfGrid" style="width:97%;height:247px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div class="alinearBotonALaDerecha" style="width: 500px">
	<div id="b_agregar" style="padding-right: 5px; width: 80px">
		<a href="javascript: void(0);"
			onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoCatalogoReaseguradorCnsf);return false;">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
	<div id=b_agregar style="padding-right: 5px; width: 100px">
		<a href="javascript: void(0);"
			onclick="javascript:exportarInfo();return false;">
			<s:text name="midas.boton.exportarExcel"/>
		</a>
	</div>
	<div id="b_agregar" style="padding-right: 5px; width: 100px">
				<a href="javascript: void(0);"
					onclick="javascript: imprimirBitacora(document.reporteRCSForm);">
					<s:text name="Bit&aacute;cora"/>
				</a>
	</div>
</div>


