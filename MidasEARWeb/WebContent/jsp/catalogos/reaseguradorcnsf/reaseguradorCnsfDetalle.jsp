<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfCatalogoHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarCatalogoReaseguradorCnsf();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="reaseguradorMovs.idreaseguradormov != null">
		<s:set id="readEditOnly" value="false" />
		<s:set id="readOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.reaseguradorescnsf.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.reaseguradorescnsf.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.reaseguradorescnsf.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarCatalogoReaseguradorCnsf();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.reaseguradorcnsf.borrar.titulo')}" />
</s:else>
<s:if test="mensaje!=null">
 <s:set id="readOnly" value="false" />
</s:if>

<s:form action="guardar" id="reaseguradorCnsfForm">
	<table  id="agregar" >
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="reaseguradorMovs.idreaseguradormov"/>
				<s:hidden name="reaseguradorMovs.idreasegurador"/>
				<s:hidden name="reaseguradorMovs.idTcReasegurador"/>
				<s:hidden name="tipoAccion"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>
			   	 <sj:datepicker key="midas.catalogos.reaseguradorescnsf.fechaCorte" name="reaseguradorMovs.fechacorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></b>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.reaseguradorescnsf.nombreReas"></s:text>
			</th>
			<td> 
				<s:textfield name="reaseguradorMovs.nombreReasegurador" id="txtClave" 
					key="" cssClass="cajaTexto"
					labelposition="left" disabled="#readEditOnly"
					readonly="#readEditOnly" required="#requiredField"
				/>
				
			</td>
			<td></td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.reaseguradorescnsf.claveReas"></s:text>
			</th>
			<td> 
				<s:textfield name="reaseguradorMovs.claveReasegurador" 
				id="txtDescripcion" key="" maxlength="100"
				cssClass="cajaTexto"
				labelposition="left"  readonly="#readEditOnly" 
				required="#requiredField" disabled="#readEditOnly"
				/> 
			</td>
			<td></td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.reaseguradorescnsf.claveReasAnt"></s:text>
			</th>
			<td> 
				<s:textfield name="reaseguradorMovs.claveReaseguradorAnt" 
				id="txtDescripcion" key="" maxlength="100"
				cssClass="cajaTexto"
				labelposition="left"  readonly="true" 
				disabled="true"
				/> 
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<td colspan="2"> 
					 <s:select key="midas.catalogos.reaseguradorescnsf.agencia"  
					 		labelposition="top"
					 		id="agenciasList"  cssClass="txtfield" 
							style="width: 100px"
					     	list="agenciasList"
							name="reaseguradorMovs.idagencia"
							headerKey=""
							disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
							headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
							onchange="onChangeAgencia();"/>
					
				</td>
		</tr>
		<tr>
			<th>
						<td colspan="2"> 
							 <s:select key="midas.catalogos.reaseguradorescnsf.calificacion"  
							 		labelposition="top"
							 		id="Catcalificacion"  cssClass="txtfield" 
									style="width: 100px"
							     	list="calificacionesList"
									name="reaseguradorMovs.idcalificacion"
									headerKey=""
									disabled="%{accionEndoso ==@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()}"
									headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"/>
							
						</td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="4">
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" 
								value="%{#claveTextoBoton}" 
								cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
   	 			</td>
			</tr>
		</s:if>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoReaseguradorCnsf(); return false;" 
						          cssClass="b_submit icon_regresar w100" /> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="4"> 
					<span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>



