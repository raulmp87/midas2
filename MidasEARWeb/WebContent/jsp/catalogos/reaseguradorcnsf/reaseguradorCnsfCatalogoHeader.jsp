<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	<script src="<s:url value='/js/midas2/catalogos/reaseguradorcnsf/reaseguradorcnsf.js'/>"></script>
  
    <script type="text/javascript">
    var listarReaseguradorCnsfPath = '<s:url action="listarFiltrado" namespace="/catalogos/reaseguradorescnsf"/>';
    var exportarReaseguradorCnsfPath = '<s:url action="exportarFiltrado" namespace="/catalogos/reaseguradorescnsf"/>';
	var verDetalleReaseguradorCnsfPath = '<s:url action="verDetalle" namespace="/catalogos/reaseguradorescnsf"/>';
	var guardarReaseguradorCnsfPath = '<s:url action="guardar" namespace="/catalogos/reaseguradorescnsf"/>';
	var eliminarReaseguradorCnsfPath = '<s:url action="eliminar" namespace="/catalogos/reaseguradorescnsf"/>';
	var mostrarReaseguradorCnsfPath = '<s:url action="mostrarReaseguradores" namespace="/catalogos/reaseguradorescnsf"/>';
	</script>