<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/paquete.js'/>"></script>

<script type="text/javascript">
	var listarPaquetePath = '<s:url action="listar" namespace="/paquete"/>';
	var verDetallePaquetePath = '<s:url action="verDetalle" namespace="/paquete"/>';
	var guardarPaquetePath = '<s:url action="guardar" namespace="/paquete"/>';
	var eliminarPaquetePath = '<s:url action="eliminar" namespace="/paquete"/>';
	var mostrarCatalogoPaquetePath = '<s:url action="mostrarCatalogo" namespace="/paquete"/>';
	var listarFiltradoPaquetePath = '<s:url action="listarFiltrado" namespace="/paquete"/>';
</script>
