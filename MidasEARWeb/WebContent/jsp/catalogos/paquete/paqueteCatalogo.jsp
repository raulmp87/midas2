<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/paquete/paqueteHeader.jsp"></s:include>

<s:form action="listar" id="paqueteForm" onKeyPress="return disableEnterKey(event)">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.paquete.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:textfield name="paquete.descripcion" id="txtDescripcion" 
					key="midas.catalogos.descripcion" labelposition="left" 
					cssClass="jQToUpper jQAlphaExtra jQRestric"
					cssStyle="cajaTexto" >
				</s:textfield> 
			</td>
		</tr>
		<tr>
			<td id="selectTipoSeguro" colspan="6" style="width:100px; text-align:left;">
				<s:textfield name="paquete.claveAmisTipoSeguro" id="txtTipoSeguroSapAmis" 
					key="midas.catalogos.paquete.tipoSeguro" labelposition="left" 
					cssClass="jQToUpper jQAlphaExtra jQRestric"
					cssStyle="cajaTexto" >
				</s:textfield> 
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript:listarFiltradoPaquete();return false;">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
	<br></br>
	<div id ="paqueteGrid" style="width:97%;height:250px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>

	<div class="alinearBotonALaDerecha" style="margin-right: 30px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoPaquete);return false;">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>
	</div>
</s:form>