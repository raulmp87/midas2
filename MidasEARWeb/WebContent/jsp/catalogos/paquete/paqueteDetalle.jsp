<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/paquete/paqueteHeader.jsp"></s:include>
<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarPaquete();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="paquete.id != null">
		<s:set id="readOnly" value="true" />
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.paquete.modificar.titulo')}" />
		<s:set id="accion" name="accion" value="4"/>
		<s:if test="duplicado == \"true\"">
			<s:set id="tituloAccion" value="%{getText('midas.catalogos.paquete.agregar.titulo')}" />
			<s:set id="readOnly" value="false" />
			<s:set id="accion" value="1"/>
		</s:if>
	</s:if>
	<s:else>
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.paquete.agregar.titulo')}" />
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="readOnly" value="false" />	
		<s:set id="accion" value="1"/>	
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.paquete.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="tituloAccion" value="%{getText('midas.catalogos.paquete.borrar.titulo')}" />
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarPaquete();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
</s:else>

<s:form action="guardar" id="paqueteForm">
	<table  id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="tipoAccion"/>
				<s:hidden name="paquete.id"></s:hidden>
				<s:hidden name="accion" value="%{#accion}"></s:hidden>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.clave"></s:text>
			</th>
			<td>
				<s:textfield name="id" id="txtClave" 
					key="" maxlength="5" disabled="#readOnly"
					labelposition="left" readonly="#readOnly" 
					required="#requiredField" 
					onkeypress="return soloNumerosM2(this, event, false)" 
					cssClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200 h16"
				/>
				<s:if test="#readOnly == true">
					<s:hidden name="id"></s:hidden>
				</s:if>
			</td>
			<td></td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.descripcion"></s:text>
			</th>
			<td> 
				<s:textfield name="paquete.descripcion" id="txtDescripcion" 
					key="" 
					labelposition="left" readonly="#readEditOnly" 
					required="#requiredField" size="40" 
					cssClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200 h16"
				/> 				
			</td>
			<td></td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.catalogos.paquete.tipoSeguro"></s:text>
			</th>
			<td id="fondoCajaTextoTipoSeguro" class="fondoCajaTexto" style="width:200px; text-align:left;"> 
				<s:select 
				    name="paquete.claveAmisTipoSeguro" 
				    id="txtClaveAmisTipoSeguro"
					labelposition="left" readonly="#readEditOnly" 
					required="#requiredField"
					list="listaTipoDeSeguro" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					listValue="descripcionParametroGeneral" listKey="valor" 
				    cssClass="cajaTextoM2 w200 jQrequired"
				    />	
			</td>
			<td></td>
		</tr>
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="4">
					<div id="divRegresarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							<s:submit onclick="%{#accionJsBoton} return false;" 
								value="%{#claveTextoBoton}" 
								cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
   	 			</td>
				
			</tr>
		</s:if>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  >
						<s:submit key="midas.boton.regresar" 
							onclick="mostrarCatalogoPaquete(); return false;"
							cssClass="b_submit icon_regresar w100" /> 
					</div>
   	 			</div>
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="4"> 
					<span style="color:red">
						<s:text name="midas.catalogos.mensaje.requerido"/>
					</span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>