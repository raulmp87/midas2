<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/domicilio.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
 
<script type="text/javascript">
	var initContenedorDomicilioPath = '<s:url action="init" namespace="/catalogos/domicilio"/>';	
	var verDetalleDomicilioPath = '<s:url action="verDetalle" namespace="/catalogos/domicilio"/>';	
	var urlBusquedaDomicilio = '<s:url action="buscarDomicilio" namespace="/catalogos/domicilio"/>';
	
	var listarDomicilioPath = '<s:url action="lista" namespace="/catalogos/domicilio"/>';
	
	var listarFiltradoDomicilioPath = '<s:url action="filtrarDomicilio" namespace="/catalogos/domicilio"/>';
	
</script>