<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/domicilio/domicilioHeader.jsp"></s:include>
<script type="text/javascript">
jQIsRequired();
</script>
<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="required" value="1"/>
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.domicilio.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<s:set id="required" value="1"/>
	<s:set id="display" value="false" />	
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.domicilio.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.domicilio.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo" value="%{getText('midas.fuerzaventa.domicilio.tituloEditar')}"/>
	<s:set id="required" value="1"></s:set>
</s:if>

<s:form action="agregar" id="domicilioForm" name="domicilioForm">
<s:hidden name="tipoAccion"/>
<div class="titulo w400"><s:text name="#titulo"/></div>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td>
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="enableSearchButton">false</s:param>
					<s:param name="idDomicilioName">domicilio.idDomicilio</s:param>
					<s:param name="idPaisName">domicilio.clavePais</s:param>
					<s:param name="idEstadoName">domicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">domicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">domicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">domicilio.calleNumero</s:param>
					<s:param name="cpName">domicilio.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">domicilio.nuevaColonia</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck</s:param>		
					<s:param name="labelPais"><s:text name="midas.fuerzaventa.negocio.pais"/></s:param>	
					<s:param name="labelEstado"><s:text name="midas.fuerzaventa.negocio.estado"/></s:param>
					<s:param name="labelCiudad"><s:text name="midas.fuerzaventa.negocio.municipio"/></s:param>
					<s:param name="labelColonia"><s:text name="midas.fuerzaventa.negocio.colonia"/></s:param>
					<s:param name="labelCalleNumero"><s:text name="midas.fuerzaventa.negocio.calleNumero"/></s:param>
					<s:param name="labelCodigoPostal"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/></s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="requerido">1</s:param>		
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="%{#required}"></s:param>
					<s:param name="enableSearchButton" value="%{#display}"></s:param>		
				</s:action>
			<td>
		</tr>	
	</table>
	<s:if test="domicilio.idDomicilio != null">
	<table width="98%" class="contenedorFormas" align="center">	
		<tr>
			<td ><s:textfield name="ultimaModificacion.fechaHoraActualizacion" id="txtFechaHora" key="midas.fuerzaventa.negocio.ultimaModificacion"  labelposition="left" readonly="true"></s:textfield></td>
			<td ><s:textfield name="ultimaModificacion.usuarioActualizacion" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" readonly="true"></s:textfield></td>
			<td colspan="2" align="right">							
				<div class="btn_back w110">
					<a href="javascript: mostrarHistorico(100,${domicilio.idDomicilio});" class="icon_guardar">	
					<s:text name="midas.boton.historico"/>
					</a>
				</div>				
			</td>		
		</tr>		
	</table>
	</s:if>	
	 <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
<table width="98%" class="contenedorFormas no-Border" align="center">
	<tr>
		<td>
			<div align="right" class="inline" >
				<div class="btn_back w110">
					<a href="javascript: salirDeDomicilio();" class="icon_regresar"
						onclick="">
						<s:text name="midas.boton.regresar"/>
					</a>
				</div>
				<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
						onclick="guardarDomicilio();">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
				</s:if>
				<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
						onclick="eliminarDomicilio();">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>
				</s:if>
			</div>
		</td>
	</tr>	
</table>
</s:form>

