<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="idDomicilio" type="ro" width="90" sort="int">Id Domicilio</column>
		<column id="calleNumero" type="ro" width="200" sort="str"><s:text name="midas.fuerzaventa.negocio.calleNumero"/></column>
		<column id="codigoPostal" type="ro" width="*" sort="int"><s:text name="midas.fuerzaventa.negocio.codigoPostal"/></column>
		<column id="nombreColonia" type="ro" width="*" sort="str">Colonia</column>
		<column id="ciudad" type="ro" width="*" sort="str">Municipio</column>
		<column id="estado" type="ro" width="*" sort="str">Estado</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="domicilios" var="varDomicilio" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${varDomicilio.idDomicilio.idDomicilio}]]></cell>
			<cell><![CDATA[${varDomicilio.calleNumero}]]></cell>
			<cell><![CDATA[${varDomicilio.codigoPostal}]]></cell>
			<cell><![CDATA[${varDomicilio.nombreColonia}]]></cell>
			<cell><![CDATA[${varDomicilio.ciudad}]]></cell>
			<cell><![CDATA[${varDomicilio.estado}]]></cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleDomicilioPath, 2,{"domicilio.idDomicilio.idDomicilio":${varDomicilio.idDomicilio.idDomicilio},"idRegistro":${varDomicilio.idDomicilio.idDomicilio},"idTipoOperacion":100})^_self</cell>
<%-- 	            <cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(verDetalleDomicilioPath, 4,{"domicilio.idDomicilio":${varDomicilio.idDomicilio},"idRegistro":${varDomicilio.idDomicilio},"idTipoOperacion":100})^_self</cell> --%>
			</s:if>
		</row>
	</s:iterator>
</rows>