<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:include value="/jsp/catalogos/domicilio/domicilioHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=listarFiltradoDomicilioPath+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"domicilioGrid", null,idField,'domicilioModal');
 });
	
 function aplicaBusqueda(){
	 var estado = jQuery("#filtroDomicilio\\.claveEstado").val();
	 var ciudad = jQuery("#filtroDomicilio\\.claveCiudad").val();	 
	 var colonia = jQuery("#filtroDomicilio\\.nombreColonia").val();
	 
	 if(estado == "" || ciudad == "" || colonia == ""){
		 return "false";
	 }else{
		return "true";
	 }
}
</script>
<s:form action="listarFiltrado" id="domicilioForm" name="domicilioForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2" cellpadding="0" cellspacing="0">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.catalogos.agente.domicilio.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >

					<s:param name="idEstadoName">filtroDomicilio.claveEstado</s:param>	
					<s:param name="idCiudadName">filtroDomicilio.claveCiudad</s:param>		
					<s:param name="idColoniaName">filtroDomicilio.nombreColonia</s:param>
					<s:param name="calleNumeroName">filtroDomicilio.calleNumero</s:param>
					<s:param name="cpName">filtroDomicilio.codigoPostal</s:param>
					<s:param name="labelPais">País</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y Número</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">4</s:param>						
				</s:action>
			</td>
			
		</tr>	
		<tr>
			<td colspan="3" style="color:red">&nbsp;Para buscar se requiere seleccionar Estado, Municipio y Colonia</td>
			<td colspan="2" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: (aplicaBusqueda()=='true')? listarFiltradoGenerico(listarFiltradoDomicilioPath, 'domicilioGrid', document.domicilioForm,'${idField}','domicilioModal') : alert('Favor de llenar los filtros Estado, Municipio y Colonia')">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="domicilioGrid" width="880px" height="220px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<!-- Se checa si es solo consulta, si es asi, no debe de agregar -->
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica(verDetalleDomicilioPath,1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>

