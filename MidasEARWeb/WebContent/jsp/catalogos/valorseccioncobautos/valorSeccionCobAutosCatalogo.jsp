<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/valorseccioncobautos/valorSeccionCobAutosHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="valorSeccionCobAutosForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.sumasaseguradasdefaultflotillas.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:select key="midas.general.seccion" name="valorSeccionCobAutos.id.idToSeccion" 
					id="valorSeccionCobAutos.id.idToSeccion" list="seccionList" 
					listKey="idToSeccion" listValue="%{descripcion + ' - Versi\u00F3n: ' + version}" 
					labelposition="left"
					onchange="listarFiltradoValorSeccionCobAutos();" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
			</td>
		</tr>
	</table>
</s:form>

<br></br>
<div id ="valorSeccionCobAutosGrid" style="width:97%;height:250px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>

<div class="alinearBotonALaDerecha" style="margin-right: 30px">
	<div id="b_agregar">
		<a href="javascript: void(0);"
			onclick="javascript:TipoAccionDTO.getAgregarModificar(nuevoValorSeccionCobAutos);return false;">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>

