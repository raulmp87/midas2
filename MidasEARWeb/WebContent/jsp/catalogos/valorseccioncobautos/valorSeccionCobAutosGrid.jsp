<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>11</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id.idToSeccion" type="ro" width="0px" sort="na" hidden="true">idToSeccion</column>
		<column id="id.idToCobertura" type="ro" width="0px" sort="na" hidden="true">idToCobertura</column>
		<column id="id.idMoneda" type="ro" width="0px" sort="na" hidden="true">idMoneda</column>
		<column id="id.idTcTipoUsoVehiculo" type="ro" width="0px" sort="na" hidden="true">idTcTipoUsoVehiculo</column>
		<column id="coberturaSeccionDTO.coberturaDTO.descripcion" type="ro" width="300" sort="str">Cobertura</column>
		<column id="monedaDTO.descripcion" type="ro" width="90" sort="str">Moneda</column>
		<column id="tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo" type="ro" width="200" sort="str"><s:text name="midas.catalogos.sumasaseguradasdefaultflotillas.tipousovehiculo"/></column>
		<column id="descripcionTipoSumaAsegurada" type="ro" width="80" sort="str">Tipo S.A.</column>
		<column id="valorSumaAseguradaMin" format="$0,000.00" type="ron" width="100" sort="int"><s:text name="midas.catalogos.sumasaseguradasdefaultflotillas.saminima"/></column>
		<column id="valorSumaAseguradaMax" format="$0,000.00" type="ron" width="100" sort="int"><s:text name="midas.catalogos.sumasaseguradasdefaultflotillas.samaxima"/></column>
		<column id="valorSumaAseguradaDefault" format="$0,000.00" type="ron" width="100" sort="int">S.A. Default</column>
		<column id="accionVer" type="img" width="25" sort="na"/>
		<column id="accionEditar" type="img" width="25" sort="na"/>
		<column id="accionBorrar" type="img" width="25" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="valorSeccionCobAutosList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idToSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idToCobertura" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="id.idMoneda" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="id.idTcTipoUsoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaSeccionDTO.coberturaDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionTipoSumaAsegurada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaMin" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaMax" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaDefault" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleValorSeccionCobAutos)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleValorSeccionCobAutos)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript: TipoAccionDTO.getEliminar(verDetalleValorSeccionCobAutos)^_self</cell>
		</row>
	</s:iterator>
</rows>