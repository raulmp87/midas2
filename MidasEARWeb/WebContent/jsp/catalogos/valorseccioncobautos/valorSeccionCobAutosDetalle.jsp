<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/valorseccioncobautos/valorSeccionCobAutosHeader.jsp"></s:include>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarValorSeccionCobAutos();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="id.idToSeccion != null">
		<s:set id="readEditOnly" value="false" />
		<s:set id="readOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.sumasaseguradasdefaultflotillas.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="false" />
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.sumasaseguradasdefaultflotillas.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.sumasaseguradasdefaultflotillas.detalle.titulo')}" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarValorSeccionCobAutos();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.sumasaseguradasdefaultflotillas.borrar.titulo')}" />
</s:else>


<s:form action="guardar" id="valorSeccionCobAutosForm">
	<table  id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:hidden name="tipoAccion"/>
			</td>
		</tr>
		<s:if test="id.idToSeccion != null">
			<s:hidden name="id.idToSeccion"/>
			<s:hidden name="id.idToCobertura"/>
			<s:hidden name="id.idMoneda"/>
			<s:hidden name="id.idTcTipoUsoVehiculo"/>
		</s:if>
		<tr>
			<th style="width:150px; text-align:right;">
				<s:text name="midas.general.seccion"></s:text>
			</th>
			<td>
				<s:select key="" 
					name="valorSeccionCobAutos.id.idToSeccion" 
					id="valorSeccionCobAutos.id.idToSeccion" 
					value="valorSeccionCobAutos.id.idToSeccion" 
					list="seccionList" listKey="idToSeccion" 
					listValue="%{descripcion + ' - Versi\u00F3n: ' + version}" 
					onchange="getCoberturasSeccionPorSeccion($('valorSeccionCobAutos.id.idToSeccion'), $('valorSeccionCobAutos.id.idToCobertura')); getTipoUsoVehiculoPorSeccion($('valorSeccionCobAutos.id.idToSeccion'), $('valorSeccionCobAutos.id.idTcTipoUsoVehiculo'));" 
					headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readOnly" 
					required="#requiredField" labelposition="left" 
                    cssClass="txtfield"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<s:text name="midas.general.cobertura"></s:text>
			</th>
			<td>
				<s:select key="" 
					name="valorSeccionCobAutos.id.idToCobertura" 
					required="#requiredField" labelposition="left" 
					id="valorSeccionCobAutos.id.idToCobertura" 
					value="valorSeccionCobAutos.id.idToCobertura" 
					list="coberturaSeccionMap"  headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readOnly" cssClass="txtfield"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<s:text name="midas.general.moneda"></s:text>
			</th>
			 <td>
				<s:select key="" name="valorSeccionCobAutos.id.idMoneda" 
					id="valorSeccionCobAutos.id.idMoneda" 
					value="valorSeccionCobAutos.id.idMoneda" 
					list="monedaList" listKey="idTcMoneda" 
					listValue="descripcion" headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readOnly" 
					required="#requiredField" labelposition="left" 
					cssClass="txtfield"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<s:text name="midas.general.tipousovehiculo"></s:text>
			</th>
			<td>
				<s:select key="" 
					name="valorSeccionCobAutos.id.idTcTipoUsoVehiculo" 
					id="valorSeccionCobAutos.id.idTcTipoUsoVehiculo" 
					value="valorSeccionCobAutos.id.idTcTipoUsoVehiculo" 
					list="tipoUsoVehiculoMap" headerKey="" 
					required="#requiredField" labelposition="left" 
					headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readOnly" cssClass="txtfield"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<s:text name="midas.general.tiposumaasegurada"></s:text>
			</th>
			<td>
				<s:select key="" 
					name="valorSeccionCobAutos.claveTipoLimiteSumaAseg" 
					id="claveTipoLimiteSumaAseg" 
					value="valorSeccionCobAutos.claveTipoLimiteSumaAseg" 
					list="tipoLimiteSumaAsegMap" headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}" 
					disabled="#readEditOnly" required="#requiredField" 
					labelposition="left" cssClass="txtfield"
					onchange="ocultarSumaAsegurada();limpiarValorSA();"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>

		<tr>
			<th style="width:150px; text-align:right;">
				<div id="tiposSumasAseguradas1" style="display: none">
					<s:text name="midas.general.sumaasegurdaminima"></s:text>
				</div>
			</th>
			<td> 
				<div id="tiposSumasAseguradas2" style="display: none">
				<s:textfield name="valorSeccionCobAutos.valorSumaAseguradaMin" 
					value="%{getText('{0,number,#,###.00}',{valorSeccionCobAutos.valorSumaAseguradaMin})}"
					id="valorSumaAseguradaMin" disabled="#readEditOnly" 
					key="" maxlength="17"
					readonly="#readEditOnly" required="#requiredField" 
					labelposition="left" class="cajaTexto"
					onkeypress="return soloNumeros(this, event, true)"
					onchange="this.value = number_format_simple(this.value,2,'.',',',' ');"
				/>
				</div>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<div id="tiposSumasAseguradas3" style="display: none">
				<s:text name="midas.general.sumaasegurdamaxima"></s:text>
				</div>
			</th>
			<td> 
				<div id="tiposSumasAseguradas4" style="display: none">
				<s:textfield name="valorSeccionCobAutos.valorSumaAseguradaMax" 
					value="%{getText('{0,number,#,###.00}',{valorSeccionCobAutos.valorSumaAseguradaMax})}"
					id="valorSumaAseguradaMax" 
					key="" maxlength="17" disabled="#readEditOnly" 
					readonly="#readEditOnly" 
					required="#requiredField" labelposition="left" 
					class="cajaTexto" 
					onkeypress="return soloNumeros(this, event, true)"
					onchange="this.value = number_format_simple(this.value,2,'.',',',' ');"
				/> 
				</div>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th style="width:150px; text-align:right;">
				<div id="tiposSumasAseguradas5" style="display: none">
				<s:text name="midas.general.sumaasegurdadefault"></s:text>
				</div>
			</th>
			<td> 
				<div id="tiposSumasAseguradas6" style="display: none">
				<s:textfield name="valorSeccionCobAutos.valorSumaAseguradaDefault" 
					value="%{getText('{0,number,#,###.00}',{valorSeccionCobAutos.valorSumaAseguradaDefault})}"
					id="valorSumaAseguradaDefault" readonly="#readEditOnly" 
					key=""  maxlength="17" disabled="#readEditOnly" 
					required="#requiredField" labelposition="left" 
					onkeypress="return soloNumeros(this, event, true)" 					
					onchange="this.value = number_format_simple(this.value,2,'.',',',' ');"
				/> 
				</div>
			</td>
			<td colspan="2" width="220px">&nbsp;</td>
		</tr>
	
		<s:if test="tipoAccion != 2">
			<tr>
				<td colspan="4">
					<div id="divGuardarBtn" style="display: block; float:right;">
						<div class="btn_back"  > 
							 <s:submit onclick="%{#accionJsBoton} return false;" 
							 	value="%{#claveTextoBoton}" 
							 	cssClass="b_submit icon_guardar w100"/> 
						</div>
   	 				</div>
				</td>
				
			</tr>
		</s:if>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  > 
						<s:submit key="midas.boton.regresar" onclick="mostrarCatalogoValorSeccionCobAutos(); return false;"
								  cssClass="b_submit icon_regresar w100"/>
					</div>
   	 			</div> 
			</td>
		</tr>
		<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
			<tr>
				<td colspan="4">
					<span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span>
				</td>
			</tr>
		</s:if>
	</table>
</s:form>
<script type="text/javascript">
	ocultarSumaAsegurada();
</script>


