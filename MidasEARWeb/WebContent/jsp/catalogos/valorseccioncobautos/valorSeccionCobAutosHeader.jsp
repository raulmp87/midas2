<%@ taglib prefix="s" uri="/struts-tags" %>

	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script> 
	<script src="<s:url value='/js/midas2/catalogos/valorseccioncobautos.js'/>"></script>
  
    <script type="text/javascript">
    var listarFiltradoValorSeccionCobAutosPath = '<s:url action="listarFiltrado" namespace="/valorseccioncobautos"/>';
	var verDetalleValorSeccionCobAutosPath = '<s:url action="verDetalle" namespace="/valorseccioncobautos"/>';
	var guardarValorSeccionCobAutosPath = '<s:url action="guardar" namespace="/valorseccioncobautos"/>';
	var eliminarValorSeccionCobAutosPath = '<s:url action="eliminar" namespace="/valorseccioncobautos"/>';
	var mostrarCatalogoValorSeccionCobAutosPath = '<s:url action="mostrarCatalogo" namespace="/valorseccioncobautos"/>';
	</script>