<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>     	    					              
        </beforeInit>
		<column id="id" type="ro" width="50" sort="int" >id</column>
		<column id="secuencia" type="ro" width="*" sort="int">SECUENCIA</column>
		<column id="valor" type="ro" width="*" sort="str">VALOR</column>
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="modPrimaList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valor" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:url value="/img/icons/ico_verdetalle.gif"/>^Ver Detalle^javascript: TipoAccionDTO.getVer(verDetalleModPrima);^_self
			</cell>
			<cell>
				<s:url value="/img/icons/ico_editar.gif"/>^Editar^javascript: TipoAccionDTO.getAgregarModificar(verDetalleModPrima);^_self
			</cell>
			<cell>
				<s:url value="/img/icons/ico_eliminar.gif"/>^Eliminar^javascript: TipoAccionDTO.getEliminar(verDetalleModPrima);^_self
			</cell>			
		</row>
	</s:iterator>
</rows>