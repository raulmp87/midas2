<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/modprima/modprimaHeader.jsp"></s:include>

<s:form action="listar" id="modPrimaForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.catalogos.modprima.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<s:select key="midas.catalogos.modprima.titulo" name="modPrima.id" 
						  id="idgrupo" list="grupoList" listKey="clave" listValue="descripcion" 
						  onchange="getIdDetalle($('modPrima.id'));" headerKey="" 
						  headerValue="%{getText('midas.general.seleccione')}"/>
			</td>
		</tr>	
	</table>
	<br></br>
	
	<div id ="modprimaGrid" style="width:97%;height:284px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>

	<div id="alinearBotonCatalogoDerecha">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript:TipoAccionDTO.getAgregarModificar(verDetalleModPrima);return false;">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>
	</div>
</s:form>