<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/paquete/paqueteHeader.jsp"></s:include>
<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'guardarModPrima();'" />
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="paquete.id != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
</s:elseif>
<s:else>
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'eliminarPaquete();'" />
	<s:set id="readOnly" value="true" />
	<s:set id="readEditOnly" value="true" />
	<s:set id="requiredField" value="false" />
    <s:set id="requiredEditField" value="false" />
</s:else>

<s:form action="guardar" id="paqueteForm">
	<s:hidden name="tipoAccion"/>
	
	<table  id="agregar">
		<tr>
			<td class="titulo">
				<s:text name="midas.catalogos.modprima.detalle.titulo"/>
			</td>
		</tr>
		<tr><s:hidden name="modPrima.id"/>
			<td> <s:textfield name="modPrima.id"  value = "grupoVariablesModificacionPrima.id" id="txtClave" key="midas.modprima.clave" labelposition="left" readonly="#readEditOnly" required="#requiredEditField"/></td>
		</tr>
		<tr>
			<td> <s:textfield name="modPrima.id.numeroSecuencia" id="txtDescripcion" key="midas.modprima.numeroSecuencia" labelposition="left"  readonly="#readOnly" required="#requiredField" size="40"/> </td>
		</tr>
		<tr>
			<td> <s:textfield name="modPrima.rangoMinimo" id="txtDescripcion" key="midas.modprima.rangoMinimo" labelposition="left"  readonly="#readOnly" required="#requiredField" size="40"/> </td>
		</tr>
		
		<tr>
			<td> <s:textfield name="modPrima.rangoMaximo" id="txtDescripcion" key="midas.modprima.rangoMaximo" labelposition="left"  readonly="#readOnly" required="#requiredField" size="40"/> </td>
		</tr>		
		<s:if test="tipoAccion != 2">
			<tr>
				<td> <s:submit onclick="%{#accionJsBoton} return false;" value="%{#claveTextoBoton}"/> </td>
				
			</tr>
		</s:if>
		<tr>
			<td> <s:submit key="midas.boton.regresar" onclick="getIdDetalle($(document.modPrima.id.paqueteForm.value)); return false;"/> </td>
		</tr>
		<tr>
			<td> <span style="color:red"><s:text name="midas.catalogos.mensaje.requerido"/></span></td>
		</tr>
	</table>
</s:form>



