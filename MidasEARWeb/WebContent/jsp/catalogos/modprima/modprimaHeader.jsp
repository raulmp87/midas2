<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/modprima.js'/>"></script>

<script type="text/javascript">
	var listarModprimaPath          = '<s:url action="listar" namespace="/modprima"/>';
	var verDetalleModprimaPath      = '<s:url action="verDetalle" namespace="/modprima"/>';
	var verDetalleModprimaDosPath   = '<s:url action="verDetalleDos" namespace="/modprima"/>';
	var guardarModprimaPath         = '<s:url action="guardar" namespace="/modprima"/>';
	var eliminarModprimaPath        = '<s:url action="eliminar" namespace="/modprima"/>';
	var mostrarCatalogoModprimaPath = '<s:url action="mostrarCatalogo" namespace="/modprima"/>';
	var obtenerClaveDetallePath     = '<s:url action="getDetalleId" namespace="/modprima"/>';
	
</script>
