<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="clave" type="ro" width="200" sort="int">Codigo Color</column>
		<column id="descripcion" type="ro" width="*" sort="str">Descripcion</column>
		<column id="accionVer" type="ro" width="70" sort="na" align="center">Acciones</column>
		<column id="accionVer" type="ro" width="30" sort="na"/>
		<column id="accionBorrar" type="ro" width="30" sort="na"/>
	</head>
	<% int a=0;%>
	<s:iterator value="colorVehiculoList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clave" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: TipoAccionDTO.getVer(verDetalleColorVehiculo);' href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/icons/ico_verdetalle.gif' title='Ver'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: TipoAccionDTO.getAgregarModificar(verDetalleColorVehiculo);' href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/icons/ico_editar.gif' title='Editar'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>
			<cell>
				&lt;div align ='center'&gt;
					 &lt;a onclick='javascript: TipoAccionDTO.getEliminar(verDetalleColorVehiculo);' href='javascript: void(0);'&gt;
						   &lt;img border='0' src='/MidasWeb/img/icons/ico_eliminar.gif' title='Borrar'/&gt;
					 &lt;/a&gt;
				&lt;/div&gt;
			</cell>
		</row>
	</s:iterator>
</rows>