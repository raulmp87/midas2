<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/persona/personaHeader.jsp"></s:include>
<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="divCarga"></s:hidden>
<script type="text/javascript">
/**
 * Funcion ejecutada al cargar el catalogo.
 */
jQuery(function(){
	/*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	var tipoAccion='<s:property value="tipoAccion"/>';
	var urlFiltro=listarFiltradoPersonaPath+"?tipoAccion="+tipoAccion;
	var idField='<s:property value="idField"/>';
	var idDivCarga='<s:property value="divCarga"/>';
	idDivCarga=(jQuery.valid(idDivCarga))?idDivCarga:"divCarga";
	listarFiltradoGenerico(urlFiltro,"personaGrid", null,idField,'responsableModal',idDivCarga);
});
</script>
<s:form action="listarFiltrado" id="personaForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.fuerzaventa.negocio.titulo"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.oficina"  name="agente.oficina" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>			
			</td>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.promotoria"  name="agente.promotoria" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>	 
			<td colspan="2"><s:textfield name="agente.rfc" id="txtClave" key="midas.fuerzaventa.negocio.rfc" labelposition="left" ></s:textfield></td>
		</tr>		
		<tr>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.estado"  name="agente.estado" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>	       
			<td colspan="2"><s:textfield name="agente.razonSocial" id="txtClave" key="midas.fuerzaventa.negocio.razonSocial" labelposition="left"></s:textfield></td>
			<td colspan="2"><s:textfield name="agente.calleNumero" id="txtClave" key="midas.fuerzaventa.negocio.calleNumero" labelposition="left" ></s:textfield></td>
		</tr>
		<tr>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.colonia"  name="agente.colonia" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2">
				<s:select key="midas.fuerzaventa.negocio.municipio"  name="agente.municipio" cssClass="cajaTextoM2 w200"
				       headerKey="" headerValue="Seleccione.."
				       list="#{'01':'Jan', '02':'Feb'}"
				       value="selectedMonth" labelposition="left"/>
			</td>
			<td colspan="2"><s:textfield name="agente.telefono" id="txtClave" key="midas.fuerzaventa.negocio.telefono" labelposition="left" ></s:textfield></td>
		</tr>
		
		<tr>
			<td colspan="2"><s:textfield name="agente.nombre" id="txtClave" key="midas.fuerzaventa.negocio.nombre" labelposition="left" ></s:textfield></td>
			<td colspan="2"><s:textfield name="agente.codigoPostal" id="txtClave" key="midas.fuerzaventa.negocio.codigoPostal" labelposition="left"></s:textfield></td>
			<td colspan="2"  align="right">				
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>				
			</td>
		</tr>				
	</table>
	<table>
		<tr>
			<td colspan="6">
				<div id="personaGrid" width="880px" height="220px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
	</table>	
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<br></br>
<div class="w880" align="right">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:agregarInciso(1);">
			<s:text name="agregar inciso"/>
		</a>
	</div>	
</div>
<script type="text/javascript">
	listarFiltradoPersona();
</script>