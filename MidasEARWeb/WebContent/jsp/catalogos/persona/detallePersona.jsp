<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script>
<s:include value="/jsp/negocio/personaHeader.jsp"></s:include>
<s:hidden name="tipoAccion"/>
<s:hidden name="id" />
<s:hidden name="claveNegocio"/>
<div hrefmode="ajax-html" style="height: 420px; width: 920px" id="configuracionNegocioTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="info_general" name="Datos Generales" href="http://void" extraAction="javascript: verInfoGeneral();"></div>
	<div width="100%" id="domicilios" name="Domicilios" href="http://void" extraAction="javascript: verDomicilioAgente();"></div>
	<div width="100%" id="datosFiscales" name="Datos Fiscales" href="http://void" extraAction="javascript: verDatosFiscales();"></div>
	<div width="100%" id="datosContables" name="Datos Contables" href="http://void" extraAction="verDatosContables();"></div>
	<div width="100%" id="datosExtra" name="Datos Extra" href="http://void" extraAction="verDatosExtra();"></div>
	<div width="100%" id="documentos" name="Documentos" href="http://void" extraAction="verDocumentosAgente();"></div>
	<div width="100%" id="carteraClientes" name="Cartera Clientes" href="http://void" extraAction="verCarteraClientes();"></div>
</div>
<table width="97%" id="agregar">	
		<tr>
			<td ><s:textfield name="agente.fechaHora" id="txtFechaHora"  labelposition="left" ></s:textfield></td>
			<td ><s:textfield name="agente.usuario" id="txtUsuario" key="midas.fuerzaventa.negocio.usuario" labelposition="left" ></s:textfield></td>
			<td colspan="2">							
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_guardar"
						onclick="">
						<s:text name="midas.boton.historico"/>
					</a>
				</div>				
			</td>		
		</tr>		
	</table>		