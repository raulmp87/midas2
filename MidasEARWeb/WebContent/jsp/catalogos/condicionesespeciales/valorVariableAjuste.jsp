<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<m:varAjuste idVarAjuste="${variableAjusteDTO.variableAjuste.id}" 
    		 idCondicionEsp="${variableAjusteDTO.idCondicionEspecial}"
    		 readOnly="${consulta}"></m:varAjuste>


<script language="javascript">
inicializarComponente();
</script>
