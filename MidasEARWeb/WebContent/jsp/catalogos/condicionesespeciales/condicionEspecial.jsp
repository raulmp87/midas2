<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/condicionEspecial.js'/>"></script>
<script src="<s:url value='/js/validaciones.js'/>"></script>

<script type="text/javascript">
	var guardarCondicionPath = '<s:url action="guardarCondicion" namespace="/catalogos/condicionespecial/catalogo"/>';
	var salirPath = '<s:url action="salir" namespace="/catalogos/condicionespecial/catalogo"/>';		
	var mostrarAdjuntarArchivosPath = '<s:url action="mostrarAdjuntarArchivos" namespace="/catalogos/condicionespecial/catalogo"/>';			
</script>


<s:form id="condicionEspecialForm" action="guardarCondicion" namespace="/catalogos/condicionespecial/catalogo">

	<div class="titulo" style="width: 98%;">
		<s:text name="midas.condicionespecial.title"/>	
	</div>	
	<s:hidden name="consulta" id="consulta"></s:hidden>
	<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"></s:hidden>
	<div id="contenedor">
		<jsp:include page="condicionEspecialHeader.jsp" flush="true"></jsp:include>
		<table id="agregar" border="0">
			<s:if test="!consulta && idCondicionEspecial != null">
				<tr>
					<td align="right">
						<div class="btn_back w180" style="display: inline; float: right;">
							 <a href="javascript: void(0);" onclick="mostrarCopiarCondicion(<s:property value="idCondicionEspecial" escapeHtml="false" escapeXml="true"/>, false);">
							 <s:text name="midas.condicionespecial.copiar" /> </a>
						</div>
						<div class="btn_back w180" style="display: inline; float: right;">
							 <a href="javascript: void(0);" onclick="imprimirCondicion();">
							 <s:text name="midas.condicionespecial.imprimir" /> </a>
						</div>
					</td>
				</tr>
			</s:if>
			<tr>
				<td><s:textfield cssClass="txtfield" 
						key="midas.general.nombre"
						name="condicion.nombre"
						labelposition="top" 
						size="100"					
						id="condicion_nombre"
						disabled="consulta"
						onkeypress="return soloAlfanumericosM1(event, true, true);"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textarea name="condicion.descripcion" 
						id="condicion_descripcion" labelposition="top"
						key="midas.general.descripcion" 
						cols="100"
						rows="15"						
						cssClass="textarea" 
						disabled="consulta"
						onkeypress="return soloAlfanumericos(event ,null ,true);"/>
				</td>			   
			</tr>			
			<tr>
				<td align="right">						
					<div class="btn_back w140" style="display: inline; float: right;">
						 <a href="javascript: void(0);" onclick="salir();">
						 <s:text name="midas.boton.salir" /> </a>
					</div>
					<s:if test="!consulta">
						<div class="btn_back w140" style="display: inline; float: right;">
							 <a href="javascript: void(0);" onclick="guardarCondicion();">
							 <s:text name="midas.boton.guardar" /> </a>
						</div>	
						<s:if test="idCondicionEspecial != null">
							<div class="btn_back w140" style="display: inline; float: right;">
								 <a href="javascript: void(0);" onclick="mostrarAdjuntarArchivos(jQuery('#idCondicionEspecial').val(), ${consulta});">
								 <s:text name="midas.boton.Adjuntar" /> </a>
							</div>
						</s:if>			
					</s:if>			
				</td>					
			</tr>				
		</table>		
	</div>
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
</s:form>
