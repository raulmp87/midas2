<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<table>
	<tr> 
		<s:iterator value="areasImpacto">
			<td>			
				<s:checkbox id = "areaImpacto_%{area.id}"
					name="%{area.id}" 
					value="selected" onclick="guardarAreaCondicion(this.id);"
					disabled="consulta">
				</s:checkbox>
			</td>
			<td>
				<a name= "<s:property value= "area.nombre" escapeHtml="false" escapeXml="true" />" 
				id = "linkArea_<s:property value= "area.id" escapeHtml="false" escapeXml="true"/>"
				href="javascript: void(0);" 
				style="font-size: 9px;" class="linkArea"
				onclick="changeTabArea(<s:property value= "area.id" escapeHtml="false" escapeXml="true"/>);">
				<s:property value= "area.nombre" escapeHtml="false" escapeXml="true" /></a> 
					
			</td>
		</s:iterator>
	</tr>
 </table>