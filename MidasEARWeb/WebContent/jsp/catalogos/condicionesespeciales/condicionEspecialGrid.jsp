<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="idCondicion" type="ro" width="0" sort="int" hidden="true"/>
		<column id="codigo" type="ro" width="80" sort="str" escapeXml="true" align="center" ><s:text name="midas.general.codigo"/></column>
		<column id="nombre" type="ro" width="350" sort="str"><s:text name="midas.condicionespecial.condicionEspecial"/></column>
		<column id="fechaRegistro" type="ro" width="130" sort="str" align="center"><s:text name="midas.condicionespecial.fechaRegistro"/></column>
		<column id="fechaAlta" type="ro" width="130" sort="str" align="center" ><s:text name="midas.condicionespecial.fechaAlta"/></column>
		<column id="fechaBaja" type="ro" width="130"  sort="str" align="center" ><s:text name="midas.condicionespecial.fechaBaja"/></column>
		<column id="estatus" type="ro" width="*" sort="str"><s:text name="midas.general.estatus"/></column>
		<s:if test="tipoVista == 2">
			<column id="polizas" type="ro" width="100" sort="int"><s:text name="midas.condicionespecial.numeroPolizas"/></column>
		</s:if>
		<s:if test="tipoVista == 1">
			<column id="editar" type="img" width="30" sort="na" align="center"/>
			<column id="consultar" type="img" width="30" sort="na" align="center"/>
			<column id="cambioEstatus" type="img" width="30" sort="na"/>
			<column id="copiar" type="img" width="30" sort="na"/>
			<column id="consultarDocs" type="img" width="30" sort="na"/>
			<column id="imprimir" type="img" width="30" sort="na"/>			
		</s:if>
	</head>			
	<s:iterator value="listCondiciones" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="codigo" escapeHtml="false" escapeXml="true"/></cell>
			<cell title='<s:property value="descripcion" escapeHtml="false" escapeXml="true"/>'><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="fechaAlta" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaBaja" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="estatusDescripcion" escapeHtml="true" escapeXml="true"/></cell>
			<s:if test="tipoVista == 2">
				<cell><s:property value="ranking" escapeHtml="true" escapeXml="true"/></cell>
			</s:if>
			<s:if test="tipoVista == 1">
				<s:if test="estatus==0">
					<cell>../img/icons/ico_editar.gif^Editar^javascript: editarCondicion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
				<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript: consultarCondicion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				<cell>../img/icons/ico_regresar.gif^Cambiar Estatus^javascript: modificarEstatus(<s:property value="id" escapeHtml="false" escapeXml="true"/>, <s:property value="estatus" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				<cell>../img/icons/ico_copiar1.gif^Copiar^javascript: mostrarCopiarCondicionFromList(<s:property value="id" escapeHtml="false" escapeXml="true"/>, true)^_self</cell>
				<cell>../img/related.png^Consultar Documentos^javascript: mostrarAdjuntarArchivos(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				<cell>../img/b_printer.gif^Imprimir^javascript: imprimirCondicion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>																
			</s:if>
		</row>
	</s:iterator>	
</rows>
