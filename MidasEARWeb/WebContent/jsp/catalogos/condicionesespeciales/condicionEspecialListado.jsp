<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/condicionEspecialListado.js'/>"></script>
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}
.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

</style>

<script type="text/javascript">
	var buscarCondicionesPath       = '<s:url action="buscarCondiciones" namespace="/catalogos/condicionespecial"/>';	
	var buscarCondicionPath         = '<s:url action="buscarCondicionAutocompletado" namespace="/catalogos/condicionespecial"/>';	
	var modificarEstatusPath        = '<s:url action="modificarEstatus" namespace="/catalogos/condicionespecial"/>';	
	var mostrarCatalogoPath         = '<s:url action="mostrarContenedorCatalogo" namespace="/catalogos/condicionespecial"/>';	
	var mostrarAdjuntarArchivosPath = '<s:url action="mostrarAdjuntarArchivos" namespace="/catalogos/condicionespecial/catalogo"/>';
	var salirPath = '<s:url action="salir" namespace="/catalogos/condicionespecial/catalogo"/>';
</script>

<s:form id="condicionEspecialForm" >

	<div class="titulo" style="width: 98%;">
		<s:text name="midas.condicionespecial.title"/>	
	</div>	
	<s:hidden name="tipoVista" id="tipoVista"></s:hidden>
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tr>
				<td><s:textfield cssClass="txtfield jQrestrict" 
						key="midas.general.codigo"
						name="condicionEspecialFiltro.condicion.codigo"
						labelposition="top" 
						size="10"					
						id="codigo"/>
				<td colspan="2">
					<div>
						<sj:autocompleter cssStyle="display:none;"/>
						 <sj:textfield labelposition="top" 
							cssClass="cajaTextoM2 w200" key="midas.general.nombre"
						    id="condicionEspecialNombre" /> 
					</div>
				</td>
				
				<td colspan="2">

					<s:select id="estatus" key="midas.general.estatus"
								labelposition="top" 
								name="condicionEspecialFiltro.condicion.estatus"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="listEstatus" listKey="value" listValue="label"  
						  		cssClass="txtfield" /> 	
				</td>	
			</tr>
			<tr>
			    <td>
					<sj:datepicker name="condicionEspecialFiltro.fechaIniRegistro"
						key="midas.condicionespecial.fechaRegistro"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecialFiltro.fechaIniRegistro"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				</td>
				<td>
					<sj:datepicker name="condicionEspecialFiltro.fechaIniAlta"
						key="midas.condicionespecial.fechaAlta"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecialFiltro.fechaIniAlta"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				 </td>	
				 <td>
					<sj:datepicker name="condicionEspecialFiltro.fechaIniBaja"
						key="midas.condicionespecial.fechaBaja"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecialFiltro.fechaIniBaja"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				 </td>	
			</tr>
			
			<tr>
			    <td>
					<sj:datepicker name="condicionEspecialFiltro.fechaFinRegistro"
						key="midas.general.a"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecial.fechaFinRegistro"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				</td>
				<td>
					<sj:datepicker name="condicionEspecialFiltro.fechaFinAlta"
						key="midas.general.a"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecialFiltro.fechaFinAlta"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				 </td>	
				 <td>
					<sj:datepicker name="condicionEspecialFiltro.fechaFinBaja"
						key="midas.general.a"
						labelposition="top"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="condicionEspecialFiltro.fechaFinBaja"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				 </td>	 
			</tr>
			
			
		</table>
	</div>
	<div  style="width: 98%;">
		<table id="agregar">
			<tr>		
				<td colspan="4">		
					<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
									<div class="btn_back w140" style="display: inline; float: right;">
										 <a href="javascript: void(0);" onclick="mostrarListadoCondiciones();">
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
									
									<s:if test="tipoVista == 1">
										<div class="btn_back w140" style="display: inline; float: right;">
											<a href="javascript: void(0);" onclick="crearCondicion();"> 
											<s:text name="midas.boton.agregar" /> </a>
										</div>	
									</s:if>								

							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		</table>
	</div>	
</s:form>
<div id="indicador"></div>
<div id="listadoCondiciones" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<div id="leyendaPolizaErrorDiv" style="display: none; color:red;" >
	<s:text name="midas.poliza.renovacionmasiva.leyendaError" />
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	inicializarCondicionListado();
	
	jQuery(document).ready(function() {		
		jQuery(function(){
			jQuery('#condicionEspecialNombre' ).autocomplete({
               source: function(request, response){  
               		jQuery.ajax({
			            type: "POST",
			            url: buscarCondicionPath,
			            data: {nombreCondicion:request.term},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( jQuery( "item", xmlResponse ).map( function() {	
								return {
									value: jQuery( "codigo", this ).text() + ' - ' + jQuery( "nombre", this ).text(),
									condicionId: jQuery( "condicionId", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 4,
               delay: 500,
               select: function( event, ui ) {
            	   try{	         
            		   mostrarListadoCondiciones(ui.item.condicionId);  
            	   }catch(e){
            	   }
	               
               }		          
		     });
		});	
	});	
</script>

