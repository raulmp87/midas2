<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

	<div id="encabezadoCondicion">
		<table id="agregar" border="0">
			<tr>
				<td><s:textfield cssClass="txtfield" 
						name="condicion.codigo"
						key="midas.general.codigo" labelposition="top" size="10"	
						readonly="true" disabled="true"	id="codigo"/>
				</td>
				
				<td colspan="2">

					<s:select id="condicion_estatus" key="midas.general.estatus"
						labelposition="top" 
						name="condicion.estatus"
						headerKey="" 
				  		list="listEstatus" listKey="value" listValue="label"  
				  		cssClass="txtfield"  disabled="consulta"/> 	
				</td>	
			</tr>
			<tr>
			    <td>
			    	<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.condicionespecial.fechaRegistro"
					labelposition="top"  
					size="10" readonly="true"
					name="condicion.fechaRegistro" disabled="true"/>  					

				</td>
				<td>
					<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.condicionespecial.fechaAlta"
					labelposition="top"  
					size="10" readonly="true"
					name="condicion.fechaAlta" disabled="true"/>  	
				</td>
				<td>
					<s:textfield cssClass="txtfield" cssStyle="width: 80px;"
					key="midas.condicionespecial.fechaBaja"
					labelposition="top"  
					size="10" readonly="true"
					name="condicion.fechaBaja" disabled="true"/>  	
				</td>
			</tr>	
		</table>

	</div>
	
