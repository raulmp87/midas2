<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<m:factor idFactor="${factorDTO.factor.id}"
		   idAreaImpacto="${factorDTO.idAreaImpacto}" 
		   idCondicionEsp="${factorDTO.idCondicionEspecial}"
		   readOnly="${consulta}">
 </m:factor> 

