<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/clasifCondicionEspecial.js'/>"></script>

<script type="text/javascript">
	var guardarClasificacionPath = '<s:url action="guardar" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var salirPath                = '<s:url action="salir" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var obtenerAsociadasPath     = '<s:url action="obtenerCoberturasAsociadas" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var obtenerDisponiblesPath   = '<s:url action="obtenerCoberturasDisponibles" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var asociarCoberturaPath     = '<s:url action="asociarCobertura" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var eliminarCoberturaPath    = '<s:url action="eliminarCobertura" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var obtenerFactoresPath      = '<s:url action="obtenerFactoresCondicion" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var guardarAreaPath          = '<s:url action="guardarAreaImpacto" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var guardarFactoresPath      = '<s:url action="guardarFactores" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var mostrarValorFactorPath   = '<s:url action="mostrarValorFactor" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var actualizaValorFactorPath = '<s:url action="actualizarValorFactor" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var guardarNivelImpactoPath  = '<s:url action="guardarNivelImpacto" namespace="/catalogos/condicionespecial/clasificacion"/>';
	var guardarVIPPath           = '<s:url action="guardarVIP" namespace="/catalogos/condicionespecial/clasificacion"/>';
</script>


<s:form id="clasifCondicionEspecialForm" >

	<div class="titulo" style="width: 98%;">
		<s:text name="midas.condicionespecial.clasificacion"/>	
	</div>	
	<s:hidden name="consulta" id="consulta"></s:hidden>
	<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"></s:hidden>
	
	<div id="contenedor">
		<jsp:include page="condicionEspecialHeader.jsp" flush="true"></jsp:include>
	</div>
	
	<div id="contenedorNivel">
		<jsp:include page="nivelCondicion.jsp" flush="true"></jsp:include>			
	</div>
	
		<table id="agregar" border="0">		
			<tr><td  style="font-size: 10px;font-weight: bold;"> <s:text name="midas.condicionespecial.areasImpacto" /></td></tr>
			<tr>
				<td>
					<div id="areasImpacto" style="overflow: hidden;"> 
					<jsp:include page="areasCondicion.jsp" flush="true"></jsp:include>
					</div>
				</td>
				
			</tr>	
			<tr><td><div style=" border: 1px solid #73D54A"></div></td></tr>
			
			<tr><td style="font-size: 10px;font-weight: bold;"> <s:text name="midas.condicionespecial.factores" /> </td></tr>			
			
			<tr>
				<td>
					<div id="factores" style="overflow: hidden;"></div>
				</td>
				
			</tr>	
			
			<tr><td><div style=" border: 1px solid #73D54A"></div></td></tr>
			<tr><td style="font-size: 10px;font-weight: bold;"> <s:text name="midas.condicionespecial.valorFactor" /></td></tr>
			<tr>
				<td>
					<div id="valorFactor" style="overflow: hidden;"></div>
				</td>
				
			</tr>		
		</table>

	
		<table id="agregar" border="0">		
			<tr>
				<td colspan="2">
	
						<s:select id="seccion" key="midas.general.seccion"
							labelposition="top" 
							name="idSeccion"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					  		list="listSeccion" listKey="idToSeccion" listValue="nombreComercial"  
					  		cssClass="txtfield"  disabled="consulta"
					  		onchange="obtenerCoberturasDisponibles();"/> 	
				</td>
			</tr>
			<tr>
				<td colspan="4" style="font-size: 10px;font-weight: bold;">
					<s:text name="midas.condicionespecial.coberturasDisponibles"/>
				</td>
				
				<td width="20%"/>
				<td colspan="4" style="font-size: 10px;font-weight: bold;">
					<s:text name="midas.condicionespecial.coberturasAsociadas"/>
				</td>
			</tr>	
			<tr>
				<td colspan="4" >
					<div id="coberturasDisponiblesTree" style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
				
				<td width="20%"/>
				
				<td colspan="4">
					<div id="coberturasAsociadasTree" style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
			</tr>
		</table>
		
	<table id="agregar" border="0">	
			<tr>
				<td>
					<div class="btn_back w140" style="display: inline; float: right;">
						 <a href="javascript: void(0);" onclick="salir();">
						 <s:text name="midas.boton.salir" /> </a>
					</div>
				</td>
			</tr>
		</table>		
</s:form>


<script  type="text/javascript">
	iniciaGrids();
</script>