<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/condicionEspecial.js'/>"></script>
<script type="text/javascript">
	var condicionPath = "/MidasWeb/catalogos/condicionespecial/catalogo/mostrarCondicion.action"
	var clasificacionPath = "/MidasWeb/catalogos/condicionespecial/clasificacion/mostrarClasificacion.action"
	var variablesPath = "/MidasWeb/catalogos/condicionespecial/variablesajuste/mostrarVariables.action"
</script>	
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"/>
<s:hidden name="consulta" id="consulta"/>

<div hrefmode="ajax-html" style="height: 100%" id="condicionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="200px" id="condicion" name="Condición Especial" href="http://void" extraAction="javascript: verTab(1);"></div>
	
	<s:if test="idCondicionEspecial != null">
		<div width="200px" id="clasificacion" name="Clasificación Condición Especial" href="http://void" extraAction="javascript: verTab(2);"></div>
		<div width="200px" id="variables" name="Variables de Ajuste" href="http://void" extraAction="javascript: verTab(3);"></div>
	</s:if>
</div>

<script type="text/javascript">
	dhx_init_tabbars();
</script>