<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/variablesAjuste.js'/>"></script>
<link rel="stylesheet" type="text/css" href="../css/dhtmlxcalendar.css">
<script src="../js/dhtmlxcalendar.js"></script>

<script type="text/javascript">
	var salirPath = '<s:url action="salir" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	
	var obtenerVariablesAsociadasPath = '<s:url action="obtenerVariablesAsociadas" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var mostrarValorVariablePath = '<s:url action="mostrarValorVariable" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var accionVariableAjustePath = '<s:url action="accionVariableAjuste" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var obtenerVariablesDisponiblesPath = '<s:url action="obtenerVariablesDisponibles" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var actualizarValorVariablePath = '<s:url action="actualizarValorVariable" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var asociarTodasVariablesAjustePath = '<s:url action="asociarTodasVariablesAjuste" namespace="/catalogos/condicionespecial/variablesajuste"/>';
	var eliminarTodasVariablesAjustePath = '<s:url action="eliminarTodasVariablesAjuste" namespace="/catalogos/condicionespecial/variablesajuste"/>';

</script>


<s:form id="variableAjusteForm" >

	<div class="titulo" style="width: 98%;">
		<s:text name="midas.condicionespecial.variablesAjuste"/>	
	</div>	
	<s:hidden name="consulta" id="consulta"></s:hidden>
	<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"></s:hidden>
	
	<div id="contenedor" style="width: 98%;">
		<jsp:include page="condicionEspecialHeader.jsp" flush="true"></jsp:include>
		<s:if test="!consulta">
			<table id="agregar" border="0">
				<tr>
					<td>
						<s:textfield key="midas.condicionespecial.variableAjuste" id="nombreVariable" labelposition="left" name="nombreVariable" cssStyle="width:242px;" disabled="consulta"/>
						<div class="btn_back w140" >
							 <a href="javascript: void(0);" onclick="buscarVariableAjuste();">
							 <s:text name="midas.boton.buscar" /> </a>
						</div>
					</td>
				</tr>
			</table>
		</s:if>
	</div>
	
	<table id="agregar" border="0">

		<tr>
			<td  style="font-size: 10px;font-weight: bold;"><s:text name="midas.condicionespecial.valorVariableAjuste"/></td>
		</tr>	
		<tr>
			<td ><div id="valorVariable"> </div></td>
		</tr>
	</table>
	
	
	<table id="agregar" border="0">		

			<tr>
				<td colspan="4"  style="font-size: 10px;font-weight: bold;">
					<s:text name="midas.condicionespecial.variablesDisponibles"/>
				</td>
				
				<td colspan="2"/>
				<td colspan="4"  style="font-size: 10px;font-weight: bold;">
					<s:text name="midas.condicionespecial.variablesAsociadas"/>
				</td>
		
			</tr>	

			<tr>
				<td colspan="4" >				
					<div id="varAjustesDisponibles" style="height: 250px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
				
				<s:if test="!consulta">				
					<td><div style="margin-left:5px; width: 4%;   position: relative;" id="divID">
						<button class="btnActionForAll" onclick="borrarAsociadas();" type="button"><<</button>
						</div>
					
					</td>
					
					<td><div style="margin-left:5px; width: 4%;   position: relative;" id="divID">
						<button class="btnActionForAll" onclick="asociarTodas();" type="button">>></button>
						</div>
					
					</td>
				</s:if>
				<s:else>
					<td width="20%"/>
				</s:else>
				
				<td colspan="4">
					<div id="varAjustesAsociadas" style="height: 250px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
				<td width="20%"/>
				
				
			</tr>
		</table>	
		
		<table id="agregar" border="0">	
			<tr>
				<td>
					<div class="btn_back w140" style="display: inline; float: right;">
						 <a href="javascript: void(0);" onclick="salir();">
						 <s:text name="midas.boton.salir" /> </a>
					</div>
				</td>				
			</tr>			
		</table>	
	
</s:form>


 <script  type="text/javascript">
	iniciaGridsVariables();
</script>