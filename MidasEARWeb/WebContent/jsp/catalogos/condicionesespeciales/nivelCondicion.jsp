<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<table id="agregar">
	<tr>
		<td style="width:auto;">					
			<s:radio name="condicion.nivelAplicacion"
			list="#{'0':' P\u00f3liza ','1':' Inciso '}"
			id="radioNivel"  disabled="consulta" labelposition="left"
			key="midas.condicionespecial.nivelImpacto"
			onclick="guardarNivelImpacto(this.value)"/>					
		</td>				
		<td style="width:auto;">					
			<s:checkbox id = "condicion.vip"  name="condicion.vip" 
			onchange="guardarVIP(this)" disabled="consulta" key="midas.condicionespecial.vip" 
			labelposition="left"/>
		</td>	
	</tr>			
</table>