<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<s:if test="!consulta">
				<call command="enableDragAndDrop"><param>true</param></call>	
			</s:if>	
					
		</beforeInit>		
		
		<column id="variableAjusteDTO.variableAjuste.id" type="ro" width="*" hidden="true"></column>
		<column id="variableAjusteDTO.variableAjuste.nombre" type="ro" width="*" ></column>
	</head>
	<s:iterator value="variablesDisponibles">
		
		<row id="<s:property value="variableAjuste.id" escapeHtml="false" escapeXml="true" />">
			<cell><s:property value="variableAjuste.id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="variableAjuste.nombre" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>