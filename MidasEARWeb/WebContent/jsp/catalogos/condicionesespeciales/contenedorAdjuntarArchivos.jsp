<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/condicionEspecial.js'/>"></script>
<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"/>
<s:hidden name="consulta" id="consulta"/>

<script type="text/javascript">
	var mostrarCatalogoPath =  '<s:url action="mostrarContenedorCatalogo" namespace="/catalogos/condicionespecial"/>';
	var eliminarCatalogoPath = '<s:url action="eliminarArchivoAdjunto" namespace="/catalogos/condicionespecial/catalogo"/>';
	var salirPath = '<s:url action="salir" namespace="/catalogos/condicionespecial/catalogo"/>';			
</script>

<!--  -->
<div class="titulo">
	Archivos adjuntos a la Condición Especial
</div>

<div id="archivosAdjuntosGrid" style="margin:10px; min-height: 200px; max-height: 400px;">
</div>
<div class="btn_back w140" style="display: inline; float: right;">
	 <a href="javascript: void(0);" onclick="salir();">	
	 <s:text name="midas.boton.salir" /> </a>
</div>
<div class="btn_back w140" style="display: inline; float: right;">
	 <a href="javascript: void(0);" onclick="regresarAdjuntarArchivos(jQuery('#idCondicionEspecial').val(), ${consulta});">
	 <s:text name="midas.boton.regresar" /> </a>
</div>
<div class="btn_back w140" style="display: inline; float: right;">
	 <a href="javascript: void(0);" onclick="anexarArchivoAdjunto();">
	 <s:text name="midas.agentes.cargaMasiva.cargarArchivo" /> </a>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
	iniciaArchivosAdjuntosGrid();
</script>