<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script src="<s:url value='/js/midas2/catalogos/condicionesEspeciales/condicionEspecialListado.js'/>"></script>
<script type="text/javascript">
	var condicionesPath = "/MidasWeb/catalogos/condicionespecial/mostrarCondiciones.action"
	var rankingPath = "/MidasWeb/catalogos/condicionespecial/mostrarRanking.action"
</script>	
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div hrefmode="ajax-html" style="height: 90%; margin-right: 10px;margin-bottom: 10px;" id="condicionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="200px" id="condiciones" name="Condiciones Especiales" href="http://void" extraAction="javascript: verTabCondiciones()"></div>
	<div width="200px" id="ranking" name="Ranking de Condiciones Especiales" href="http://void" extraAction="javascript: verTabRanking();"></div>
</div>

<script type="text/javascript">
	dhx_init_tabbars();
</script>