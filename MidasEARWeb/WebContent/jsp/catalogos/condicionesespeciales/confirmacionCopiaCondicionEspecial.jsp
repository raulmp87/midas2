<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<style type="text/css">
.required {
	font-size: 10px;
	color: red;
	font-weight: lighter;
	font-style: italic;
	text-align: center;
	display: none;
	margin-top: -12;
}
#footer {
	float: right;
}
</style>
<script>
	function cerrarVentanaCopiarCondiciones(){
		var origen = jQuery('#muestraCondiciones').val();
		if(origen){
			var funOnClose;
			funcOnClose = 'parent.mostrarCondicionesEspeciales();';
			parent.cerrarVentanaModal('confCopiarCondicionEspecial', funcOnClose);
		}else{
			parent.cerrarVentanaModal('confCopiarCondicionEspecial');
		}
	}
	function copiarCondicionEsp(idCondicionEspecial, nombreCondicionEspecial){
		var url="/MidasWeb/catalogos/condicionespecial/copiarCondicionEspecial.action?idCondicionEspecial="+idCondicionEspecial+"&nombreCondicionEspecial="+nombreCondicionEspecial+"&muestraCondiciones="+muestraCondiciones;
		parent.redirectVentanaModal('confCopiarCondicionEspecial', url, null);
	}
	
</script>
<s:hidden name="idCondicionEspecial" id="idCondicionEspecial"></s:hidden>
<s:hidden name="muestraCondiciones" id="muestraCondiciones"></s:hidden>
<table id="desplegarDetalle">
	<tr>
		<td>
			<s:text name="midas.condicionespecial.copiar.confirmacion.mensaje" />
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.condicionespecial.copiar.confirmacion.nombre" />
		</td>
	</tr>
	<tr>
		<td><s:textfield
				key="midas.general.nombre"
				name="nombreCondicionEspecial"
				labelposition="top" 
				size="70"					
				id="nombreCondicionEspecial"/>
		</td>
	</tr>
	<tr>
		<td align="right">					
			<div class="btn_back w140" style="display: inline; float: right;">
				 <a href="javascript: void(0);" onclick="cerrarVentanaCopiarCondiciones();">
				 <s:text name="midas.boton.cancelar" /> </a>
			</div>

			<div class="btn_back w140" style="display: inline; float: right;">
				 <a href="javascript: void(0);" onclick="copiarCondicionEsp(<s:property value="idCondicionEspecial" escapeHtml="false" escapeXml="true"/>, jQuery('#nombreCondicionEspecial').val());">
				 <s:text name="midas.boton.aceptar" /> </a>
			</div>
		</td>					
	</tr>
</table>		
<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator" src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
