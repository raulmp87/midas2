<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<rows> 
	<head> 
		<beforeInit> 
			<call command="setImagePath"> 
					<param>
						<s:url value="/img/dhtmlxgrid/" />
					</param>
			</call> 

			<call command="setSkin"> 
				<param>light</param>
			</call>

			<call command="enablePaging"> 
				<param>true</param>
				<param>13</param> 
				<param>5</param> 
				<param>pagingAreaHist</param> 
				<param>true</param>
				<param>infoAreaHist</param> 
			</call>

			<call command="setPagingSkin"> 
					<param>bricks</param>
			</call> 
		</beforeInit> 
	
		<column id="id" type="ro" width="*" sort="na">
			<s:text name="midas.retencion.impuestos.historico.label.id" />
		</column>

		<column id="usuario" type="ro" width="70" short="na"> 
			<s:text	name="midas.retencion.impuestos.historico.label.usuariomodificacion" />
		</column> 
		
		<column id="fecha" type="ro" width="60" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.fechamodificacion" />
		</column>
		
		<column type="ro" width="*" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.accion" />
		</column> 

		<column id="estado.stateName" type="ro" width="50" sort="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.estado" />
		</column> 

		<column id="porcentaje" type="ro" width="*" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.porcentaje" />
		</column> 
		
		<column id="concepto" type="ro" width="60" short="na">
			<s:text name="midas.retencion.impuestos.historico.label.concepto" />
		</column> 

		<column id="tipoComision.valor" type="ro" width="*" short="na"> 
			<s:text	name="midas.retencion.impuestos.historico.label.tipocomision" />
		</column> 

		<column id="personalidadJuridica.valor" type="ro" width="*" short="na">
		<s:text name="midas.retencion.impuestos.historico.label.personalidadjuridica" />
		</column> 

		<column id="estatus" type="ro" width="*" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.estatus" />
		</column> 

		<column id="fechainiciovig" type="ro" width="60" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.fechainiciovigencia" />
		</column> 

		<column id="fechafinvig" type="ro" width="60" short="na"> 
			<s:text name="midas.retencion.impuestos.historico.label.fechafinvigencia" />
		</column> 

	</head> 

	<s:iterator value="listaConfiguracionesRetencionesHistory" var="rowlistRetenciones" status="index">
		<row id="${index.count}"> 
			<cell><![CDATA[${rowlistRetenciones.id}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.usuario}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.fechaModificacionString}]]></cell>
			<s:if test="#index.last">
				<cell><s:text name="midas.retencion.impuestos.historico.label.configuracion.accion.alta" /></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.retencion.impuestos.historico.label.configuracion.accion.modificacion" /></cell>
			</s:else> 
			<cell><![CDATA[${rowlistRetenciones.estado.stateName}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.porcentaje}]]></cell> 
			<cell><![CDATA[${rowlistRetenciones.concepto}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.tipoComision.valor}]]></cell> 
			<cell><![CDATA[${rowlistRetenciones.personalidadJuridica.valor}]]></cell>
			<s:if test="%{#rowlistRetenciones.estatus == 1}">
				<cell><s:text name="midas.retencion.impuestos.historico.label.configuracion.estatus.activa" /></cell>
			</s:if>
			<s:elseif test="%{#rowlistRetenciones.estatus == 0}">
				<cell><s:text name="midas.retencion.impuestos.historico.label.configuracion.estatus.inactiva" /></cell>
			</s:elseif>
			<s:else>
				<cell><s:text name="midas.componente.error.message" /></cell>
			</s:else>
			<cell><s:date name="fechaInicioVig" format="dd/MM/yyyy" /></cell>
			<cell><s:date name="fechaFinVig" format="dd/MM/yyyy" /></cell> 
		</row>
	</s:iterator> 
</rows>