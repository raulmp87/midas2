<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page  language="java" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/impuestosEstatales/retencionImpuestosHeader.jsp"></s:include>

<script language="JavaScript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'></s:url>"></script>

<s:hidden name="load" value="false" />
<s:form action="mostrarRetencionImpuestos" id="mostrarRetencionImpuestosForm" name="mostrarRetencionImpuestosForm">
	<table id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text name="midas.retencion.impuestos.label.titulo" /></td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.retencion.impuestos.label.id" />
			</th>

			<td>
				<s:textfield name="filtroConfiguracion.id" id="idIE" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict" disabled="true" value=""></s:textfield>
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.retencion.impuestos.label.estado" />
			</th>
	
			<td>
				<s:select cssClass="cajaTexto" name="filtroConfiguracion.estado.stateId" id="stateId" list="listaEstados" listKey="stateId" listValue="stateName" headerKey="" headerValue="%{getText('midas.general.seleccione')}" />
			</td>

			<th>
				&nbsp;<s:text name="midas.retencion.impuestos.label.fechainivigencia" />
			</th>

			<td>
				<sj:datepicker id="dtFechaInicioVigencia"
						name="filtroConfiguracion.fechaInicioVig" buttonImage="../img/b_calendario.gif"
						changeYear="true" changeMonth="true" maxlength="10"
						cssClass="cajaTextoM2 w100" value=""
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						onblur="esFechaValida(this);" minDate="0"></sj:datepicker>
			</td>
	
			<td colspan="3" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="" onclick="javascript: guardar();">
						<s:text name="midas.retencion.impuestos.boton.guardar" />
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.retencion.impuestos.label.porcentaje" />
			</th>
			<td>
				<s:textfield id="txPorcentaje" name="filtroConfiguracion.porcentaje" cssClass="cajaTextoM2 w170 jQnumeric jQrestrict" maxlength="2" value=""/>
			</td>

			<th>
			<s:text name="midas.retencion.impuestos.label.fechafinvigencia" />
			</th>
			
			<td>
			<sj:datepicker id="dtFechaFinVigencia"
					name="filtroConfiguracion.fechaFinVig" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="cajaTextoM2 w100" value=""
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" minDate="0"></sj:datepicker>
			</td>

			<td colspan="3" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="" onclick="javascript: cancel();">
						<s:text name="midas.retencion.impuestos.boton.cancelar" />
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.retencion.impuestos.label.concepto" />
			</th>

			<td>
				<s:textfield id="txConcepto" name="filtroConfiguracion.concepto" cssClass="cajaTextoM2 w170" value="" maxlength="20" />
			</td>


			<th>
				<s:text name="midas.retencion.impuestos.label.activo" />
			</th>
			<td>
				<s:checkbox name="filtroConfiguracion.estatus" id="active" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict" fieldValue="1"></s:checkbox>
			</td>

			<td colspan="3" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="" onclick="javascript: buscar();">
						<s:text name="midas.retencion.impuestos.boton.buscar" />
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				&nbsp;<s:text name="midas.retencion.impuestos.label.tipocomision" />
			</th>
			<td>
				<s:select cssClass="cajaTexto" name="filtroConfiguracion.tipoComision.id" id="comision"
						list="listaTipoComisiones" listKey="id" listValue="valor" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"/>
			</td>

			<th>
				&nbsp;<s:text name="midas.retencion.impuestos.label.personalidadjuridica" />
			</th>
			<td>
				<s:select cssClass="cajaTexto" name="filtroConfiguracion.personalidadJuridica.id" id="juridica" list="listaTipoPersonalidadesJuridicas" listKey="id" listValue="valor" headerKey="" headerValue="%{getText('midas.general.seleccione')}" />
			</td>
			
			<td colspan="3" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="" onclick="javascript: monitor();">
						<s:text name="midas.retencion.impuestos.boton.monitor" />
					</a>
				</div>
			</td>
		</tr>
	</table>
	<br>
	<div id="divCarga" style="position: absolute;"></div>
	<div align="center" id="detalleRetGrid" class="w1000 h200" style="width: 1020;background-color: white; overflow: hidden"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</s:form>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar" onclick="javascript:mostrarHistorico();">
			<s:text name="midas.retencion.impuestos.boton.historial"/>
		</a>
	</div>	
</div>