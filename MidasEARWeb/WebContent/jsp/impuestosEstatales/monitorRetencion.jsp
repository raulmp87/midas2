<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/impuestosEstatales/monitorRetencionHeader.jsp"></s:include>

<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>

<style type="text/css">
ul {
	height: 60px;
	overflow: auto;
	width: 200px;
	border: 1px solid;
	border-color: #B2DBB2;
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow-x: hidden;
}

li {
	margin: 0;
	padding: 0;
}

li label:hover {
	background-color: Highlight;
	color: HighlightText;
}
</style>
<s:form action="monitorRetencionImpuestos" id="monitorRetencion" name="monitorRetencion">
	
	<table id="filtrosM2" width="880px">
		<tbody>
			<tr>
				<td class="titulo" colspan="6"><s:text name="midas.retencion.impuestos.monitor.label.titulo" /></td>
			</tr>
		</tbody>
	</table>

</s:form>
<div class="btn_back w100" id="btnActualizar">
	<a href="javascript:void(0);" onclick="javascript: actualizar();"><s:text name="midas.boton.actualizar" /></a>
</div>
<br>
<br>
<s:text name="midas.retencion.impuestos.monitor.leyenda" />
<div align="center" id="monitorRetencionGrid" class="w880 h200" style="background-color: white; overflow: hidden"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<br>
<br>
<div class="btn_back w100" id="btnRegresarRet">
	<a href="javascript:void(0);" onclick="javascript: regresar();"><s:text name="midas.boton.regresar" /></a>
</div>