<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<rows> 
	<head> 
		<beforeInit> 
				<call command="setImagePath"> 
					<param>
					<s:url value="/img/dhtmlxgrid/" />
					</param>
				</call>

				<call command="setSkin">
					<param>light</param>
				</call> 

				<call command="enablePaging"> 
					<param>true</param>
					<param>13</param>
			 		<param>5</param>
			 		<param>pagingArea</param> 
			 		<param>true</param>
			 		<param>infoArea</param>
		 		</call> 

		 		<call command="setPagingSkin"> 
		 			<param>bricks</param>
				</call>

 				<call command="attachHeader">
			 		<s:if test="gridMonitor != 'COMIS' ">
			 			<param>#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,&amp;nbsp;</param>
			 		</s:if>
			 	    <s:else>
			 	    	<param>#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,&amp;nbsp;</param>
			 	    </s:else>
			 	</call> 
		</beforeInit> 

		<column id="id" type="ro" width="100" sort="int">
			<s:text name="midas.retencion.impuestos.monitor.id" />
		</column> 

		<column id="tejecucion" type="ro" width="150" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.tipoejecucion" />
		</column> 

		<column id="eEjecucion" type="ro" width="150" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.estatusejecucion" />
		</column> 
		
		<column id="fechaEjecucion" type="ro" width="150" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.fechainicioejecucion" />
		</column> 
		
		<column id="usuario" type="ro" width="120" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.usuario" />
		</column> 

		<column id="idConfiguracion" type="ro" width="100" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.idconfiguracion" />
		</column> 

		<column id="avance" type="ro" width="90" sort="na">
			<s:text name="midas.retencion.impuestos.monitor.avance" />
		</column> 
	</head> 

	<s:iterator value="listaConfiguracionesRetencionesMonitor" var="rowlistaMonitor" status="index">
				<row id="${index.count}"> 
					<cell><![CDATA[${rowlistaMonitor.id}]]></cell>
					<cell><![CDATA[${rowlistaMonitor.tipoEjecucion.valor}]]></cell>
					<cell><![CDATA[${rowlistaMonitor.estatusEjecucion.valor}]]></cell>
					<cell><s:date name="fechaEjecucion" format="dd/MM/yyyy hh:mm a" /></cell> 
					<cell><![CDATA[${rowlistaMonitor.usuario}]]></cell>
					<cell><![CDATA[${rowlistaMonitor.idConfiguracion}]]></cell>
					<cell><![CDATA[${rowlistaMonitor.avance}]]></cell>
				</row>

	</s:iterator> 

</rows>