<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/impuestosEstatales/reporteRetencionImpuestos.js'/>"></script>

<div class="row">
	<div class="titulo">
		<s:text name="midas.retencion.impuestos.reportes.label.titulo" />
	</div>
</div>

<s:form action="mostrarReporteRetencionImpuestos" namespace="/fuerzaventa/retencionImpuestos/">
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td>
				<s:text name="midas.retencion.impuestos.reportes.label.anio" />
			</td>
			<td>
				<select id="anios" name="reporteRetencionImpuestosEstatales.anio" class="txtfield w200 jQrequired" ></select>
			</td>
			<td>
				<s:text name="midas.retencion.impuestos.reportes.label.mes" />
			</td>
			<td>
				<select id="meses" name="reporteRetencionImpuestosEstatales.mes" class="txtfield w200 jQrequired"></select>
			</td>
			<td>
				<s:text name="midas.retencion.impuestos.reportes.label.estado" />
			</td>
			<td>
				<s:select cssClass="cajaTexto" id="estado" name="stateId" list="listaEstados" listKey="stateId" listValue="stateName"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}" />
			</td>
		</tr>
	</table>

	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td>
				<s:textfield id="claveAgente" cssStyle="display:none" />
				<div style="display: inline; float: left;">
					<s:textfield cssClass="txtfield" cssStyle="width:250px;" key="Agente" placeholder="Id del Agente" labelposition="left"
						size="280" onblur="javascript: onChangeIdAgente();" id="idAgente" name="agente.idAgente" disabled="false" />
				</div>
				<midas:boton onclick="javascript: pantallaModalBusquedaAgente();" tipo="buscar"
					style="display: inline; float: left; margin-left: 5px;padding-top: 3px;" />
					
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:textfield cssClass="txtfield" cssStyle="width:250px;" key="midas.mediciones.agentes.conf.nombre" placeholder="Nombre del Agente"
					labelposition="left" size="280px" onblur="this.value=jQuery.trim(this.value)" id="nombreAgente"
					name="agente.persona.nombreCompleto" readonly="true" />
			</td>
		</tr>
	</table>
</s:form>

<div class="row">
	<div class="c2" style="float: right;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class="" onclick="exportTo();"><s:text name="midas.retencion.impuestos.boton.exportar" /></a>
		</div>
	</div>
</div>