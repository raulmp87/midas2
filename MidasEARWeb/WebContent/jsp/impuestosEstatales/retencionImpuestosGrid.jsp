<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<rows> 
	<head> 
		<beforeInit> 
			<call command="setImagePath"> 
				<param>
					<s:url value="/img/dhtmlxgrid/" />
				</param>
			</call>

			<call command="setSkin">
				<param>light</param>
			</call> 

			<call command="enablePaging"> 
				<param>true</param>
				<param>13</param>
		 		<param>5</param>
		 		<param>pagingArea</param> 
		 		<param>true</param>
		 		<param>infoArea</param>
	 		</call> 

	 		<call command="setPagingSkin"> 
	 			<param>bricks</param>
			</call> 
		</beforeInit> 
		<column id="idImpuestoAgente" type="ro" width="40" sort="int">
			<s:text name="midas.retencion.impuestos.detalle.label.id" />
		</column> 

		<column id="idEstado.id" hidden="true" type="ro" width="80" sort="str">
			<s:text name="midas.retencion.impuestos.detalle.label.estado" />
		</column> 

		<column id="idEstado.description" type="ro" width="115" sort="str"> 
			<s:text	name="midas.retencion.impuestos.detalle.label.estado" />
		</column> 

		<column id="porcentaje" type="ro" width="80" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.porcentaje" />
		</column>

		<column id="concepto" type="ro" width="120" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.concepto" />
		</column>

		<column id="tipocomision.id" hidden="true" type="ro" width="*" short="str">
			<s:text name="midas.retencion.impuestos.detalle.label.tipocomision" />
		</column> 

		<column id="tipocomision.valor" type="ro" width="60" short="str">
			<s:text	name="midas.retencion.impuestos.detalle.label.tipocomision" />
		</column> 

		<column id="personalidjuridica.id" hidden="true" type="ro" width="100" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.personalidadjuridica" />
		</column> 

		<column id="personalidjuridica.valor" type="ro" width="80" short="str">
			<s:text name="midas.retencion.impuestos.detalle.label.personalidadjuridica" />
		</column> 

		<column id="fechainivig" type="ro" width="70" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.fechainiciovigencia" />
		</column> 

		<column id="fechafinvig" type="ro" width="70" short="str">
			<s:text	name="midas.retencion.impuestos.detalle.label.fechafinvigencia" />
		</column> 

		<column id="usuarioModifica" type="ro" width="150" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.usuariomodifica" />
		</column> 

		<column id="fechaModifica" type="ro" width="120" short="str"> 
			<s:text name="midas.retencion.impuestos.detalle.label.fechamodifica" />
		</column>

		<column id="estatus" hidden="true" type="ro" width="*" short="str"></column> 

		<column id="accionEditar" type="img" width="70" sort="na" align="right"> 
			<s:text name="midas.general.acciones" />
		</column>

		<column id="accionBorrar" type="img" width="*" sort="na" align="left"></column>
	</head> 
	<s:iterator value="listaConfiguracionesRetenciones" var="rowlistRetenciones" status="index">
		<row id="${index.count}"> 
			<cell><![CDATA[${rowlistRetenciones.id}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.estado.stateId}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.estado.stateName}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.porcentaje}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.concepto}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.tipoComision.id}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.tipoComision.valor}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.personalidadJuridica.id}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.personalidadJuridica.clave}]]></cell>
			<cell><s:date name="fechaInicioVig" format="dd/MM/yyyy" /></cell>
			<cell><s:date name="fechaFinVig" format="dd/MM/yyyy" /></cell>
			<cell><![CDATA[${rowlistRetenciones.usuario}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.fechaModificacionString}]]></cell>
			<cell><![CDATA[${rowlistRetenciones.estatus}]]></cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript:editarConfiguracion(${index.count})^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript:eliminar(${rowlistRetenciones.id})^_self</cell>
		</row>
	</s:iterator> 
</rows>