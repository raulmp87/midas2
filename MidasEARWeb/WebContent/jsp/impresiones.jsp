<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<sj:head/>
<script src="<s:url value='/js/midas2/util.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet"
	type="text/css">
<style type="text/css">
.green_box {
	width: 400px;
	height: 200px;
	clear: both;
	background: url('<s:url value="/img/green_box_bg.gif"></s:url>')
		no-repeat left #50a842;
	background-position: 50px 0px;
}

h1 {
	padding: 5px 0 5px 0;
	margin: 0px;
	font-size: 14px;
	color: #FFFFFF;
}
</style>
<title>impresiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body class="green_box">
	<div class="grid">
		<div class="row">
			<div class="c5">
<img src="/MidasWeb/img/loading-green-circles.gif"/><h1>Procesando la informaci&oacute;n, espere un momento por favor...</h1>
			</div>
		</div>
		<div class="row">
			<div class="c4">
				<h1>
					<s:property value="tituloImpresion" />
				</h1>
			</div>
		</div>
	</div>
	<div id="pdf"></div>
<script type="text/javascript">
<!--
jQuery(document).ready(function(){
	var url = '<s:property value="urlSendRequest"/>';
	sendRequestJQ(null,url,"pdf",null,null);
    var win = window.open(url, 'Procesando','width=100,height=200,screenX=20,screenY=100,status=0,toolbar=0');   
// 	window.open(url,null,
//  "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
    var timer = setInterval(function() {   
        if(win.closed) {  
            clearInterval(timer);  
            parent.cerrarVentanaModal("impresionNegocio");  
        }  
    }, 1000);  
// 	parent.mostrarVentanaModal("impresionNegocioStream",null, null, null, 100, 100, url, null);
})
//-->
</script>

</body>
</html>