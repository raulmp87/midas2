<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

		<script src="<s:url value='/js/midas2/relaciones/cobertura/relacionescobertura.js'/>"></script>
		<script type="text/javascript">
			var obtenerGruposVariablesAsociadosPath = '<s:url action="obtenerGruposVariablesAsociados" namespace="/relaciones/cobertura"/>';
			var obtenerGruposVariablesDisponiblesPath = '<s:url action="obtenerGruposVariablesDisponibles" namespace="/relaciones/cobertura"/>';
			var accionSobreGruposVariablesAsociadosPath = '<s:url action="accionSobreGruposVariablesAsociados" namespace="/relaciones/cobertura"/>';
		</script>
 

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarRelacionGruposVariables">
			<s:hidden name="idCobertura"/>
			<s:hidden name="idSeccion"/>

			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.variablesmodprima.grupo" />
					&nbsp;<midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" /></td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.variablesmodprima.grupo.asociado" />
					&nbsp;<midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="gruposVariablesAsociadosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.variablesmodprima.grupo.disponible" />
					&nbsp;<midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="gruposVariablesDisponiblesGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: gruposVariablesProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionCoberturaTabBar, 'detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</s:form>
	</center>
</div>		