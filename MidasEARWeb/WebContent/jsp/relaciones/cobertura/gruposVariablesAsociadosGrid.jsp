<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="idGrupoVariables" type="ro" width="0" sort="str" hidden="true">idGrupoVariables</column>
		<column id="descripcionGrupoVariables" type="ro" width="*" sort="str">Grupo Variables Modificadores de Prima</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="gruposAsociados">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>