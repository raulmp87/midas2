<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

		<script src="<s:url value='/js/midas2/relaciones/seccion/relacionesseccion.js'/>"></script>
		<script type="text/javascript">
			var obtenerPaquetesAsociadosPath = '<s:url action="obtenerPaquetesAsociados" namespace="/relaciones/seccion"/>';
			var obtenerPaquetesDisponiblesPath = '<s:url action="obtenerPaquetesDisponibles" namespace="/relaciones/seccion"/>';
			var accionSobrePaquetesAsociadosPath = '<s:url action="accionSobrePaquetesAsociados" namespace="/relaciones/seccion"/>';
		</script>
 

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarRelacionPaquetes">
			<s:hidden name="idSeccion"/>

			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.paquete" />
					&nbsp;<midas:mensaje clave="configuracion.seccion.mostrar.seccion" /></td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.paquete.asociado" />
					&nbsp;<midas:mensaje clave="configuracion.seccion.mostrar.seccion" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="paquetesAsociadosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.paquete.disponible" />
					&nbsp;<midas:mensaje clave="configuracion.seccion.mostrar.seccion" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="paquetesDisponiblesGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: paquetesProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionSeccionTabBar, 'detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</s:form>
	</center>
</div>		