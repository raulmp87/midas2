<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="idPaquete" type="ro" width="0" sort="int" hidden="true">idPaquete</column>
		<column id="descripcionPaquete" type="ro" width="130" sort="str">Paquete</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="paquetesDisponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false"  escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>