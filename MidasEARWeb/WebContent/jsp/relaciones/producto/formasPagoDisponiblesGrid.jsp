<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
			<!--call command="attachEvent"><param>onRowCreated</param><param>registrarCoberturaPaquete</param></call-->
		</beforeInit>
		<!--afterInit>
			<call command="groupBy"><param>0</param></call>
		</afterInit-->
		
		<column id="idFormaPago" type="ro" width="0" sort="int" hidden="true">idFormaPago</column>
		<column id="descripcionFormaPago" type="ro" width="130" sort="str">Formas de Pago</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="formasPagoDisponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idFormaPago" escapeHtml="false"  escapeXml="true" /></cell>
			<cell><s:property value="descripcion" escapeHtml="false"  escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>