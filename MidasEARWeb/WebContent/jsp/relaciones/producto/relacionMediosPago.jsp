<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <s:include value="/jsp/relaciones/producto/relacionesProductoHeader.jsp"/>
 

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarRelacionMediosPago">
			<s:hidden name="idProducto"/>

			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.mediopago" /> Producto</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.mediopago.asociado" /> Producto</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="mediosPagoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.mediopago.disponible" /> Producto</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="mediosPagoDisponiblesGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: mediosPagoProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionProductoTabBar, 'detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</s:form>
	</center>
</div>		