<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/folio/reexpedible/contenedorHeaderFolio.jsp"></s:include>

<div id="contenedorFiltros" style="width: 98%;">
	<s:form id="buscarFolioForm" name="toFolio" action="buscar">
		<s:hidden name="filtroFolio" id="isFiltro"/>
		<table  style="width: 100%;" border="0">
			<tr >
				<td style="width: 98%;" class="titulo" colspan="4" ><s:text name="midas.folio.reexpedible.head.buscar" /></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="mensajes" style="display: none;">
						<div id="textMessage" >  </div>
					</div>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"></th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
		</table>
		<table id="desplegarDetalle" border="0">
			<tr>
				<th style="width: 34%;"><s:text name="midas.folio.reexpedible.field.tipo.folio" /></th>
				<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.folio.inicio" /></th>
				<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.folio.fin" /></th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:select cssClass="cajaTexto" id="idtiposFolioB"
						name="filtroFolioRe.tipoFolio.idTipoFolio" list="tipoFolios"
						listKey="idTipoFolio" listValue="claveTipoFolio" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}" />
				</td>
				<td style="width: 33%;">
					<s:textfield id="folioInicioB" name="filtroFolioRe.folioInicioL" cssClass="cajaTexto"  
						onfocus="javascript: new Mask('##########', 'int').attach(this)" />
				</td>
				<td style="width: 33%;">
					<s:textfield id="folioFinB" name="filtroFolioRe.folioFinL" cssClass="cajaTexto" 
						onfocus="javascript: new Mask('##########', 'int').attach(this)" />
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"><s:text name="midas.general.negocio" /></th>
				<th style="width: 33%;"><s:text name="midas.suscripcion.cotizacion.producto" /></th>
				<th style="width: 33%;"><s:text name="midas.negocio.seccion.tiposPoliza" /></th>
			</tr>
			<tr>
				<td style="width: 34%;">
					<s:select cssClass="cajaTexto"
						name="filtroFolioRe.negocio.idToNegocio" id="idToNegocioB" list="negocios"
						listValue="descripcionNegocio" listKey="idToNegocio"
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
				</td>
				<td style="width: 33%;">
					<s:select cssClass="cajaTexto"
						name="filtroFolioRe.producto.idToProducto" id="ToNegProductoB" list="{}"
						listKey="idToProducto" listValue="descripcion"
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" /> 
				</td>
				<td style="width: 33%;">
					<s:select id="idToTipoPolizaB" cssClass="cajaTexto" list="{}" 
							name="filtroFolioRe.tipoPoliza.idToTipoPoliza" listKey="idToNegTipoPoliza"
							headerKey="-1" listValue="descripcion"
							headerValue="%{getText('midas.general.seleccione')}"
							required="#requiredField"/>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"><s:text name="midas.folio.reexpedible.field.vigencia.folio" /></th>
				<th style="width: 33%;"><s:text name="midas.general.moneda" /></th>
				<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.fecha.vigencia" /></th>
			</tr>
			<tr>
				<td style="width: 33%;">
					<s:select cssClass="cajaTexto"
						name="filtroFolioRe.vigencia.idTcVigencia" id="idTcVigenciaB" list="{}"
						listKey="idTcVigencia" listValue="descripcion" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}" /> 
				</td>
				<td style="width: 34%;">
					<s:select name="filtroFolioRe.moneda.idTcMoneda" id="idTcMonedaB"
						cssClass="cajaTexto" list="{}" listKey="idTcMoneda" listValue=""
						headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" /> 
				</td>
				<td style="width: 33%;">
					<sj:datepicker name="filtroFolioRe.fechaValidez"
						labelposition="left" size="10" changeMonth="true"
						changeYear="true" readonly="readonly" cssClass="cajaTexto"
						cssStyle="width: 89%;" buttonImage="../img/b_calendario.gif"
						id="fechaValidezHastaB" maxlength="10"
						onkeypress="return soloFecha(this, event, false);"
						onchange="ajustaFechaFinal();"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" >
					</sj:datepicker>
				</td>
			</tr>
		</table>
		<div id="botonesBusqieda" style="width: 98%;">
			<table width="100%">
				<tr>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="realizarBusqueda(true, true)" href="javascript: void(0);"> 
								<s:text name="midas.boton.buscar" />
							</a>
						</div>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a onclick="mostrarFormularios(true)" >
								<s:text name="midas.boton.nuevo" />
							</a>
						</div>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="limpiarBusqueda()" href="javascript: void(0);"> 
								<s:text name="midas.boton.limpiar" />
							</a>
						</div>
					</td>
				</tr>
			</table>
			</div>
	</s:form>
</div>

<div id="contenedorAlta" style="width: 98%; display : none;">
	<s:form id="nuevoFolioForm" name="toFolio" action="guardar">
		
			<table  style="width: 100%;" border="0">
				<tr >
					<td style="width: 98%;" class="titulo" colspan="4" ><s:text name="midas.folio.reexpedible.head.alta" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="mensajesAlta" style="display: none;">
							<div id="textMessageAlta" >  </div>
						</div>
					</td>
				</tr>
				<tr>
					<th style="width: 34%;"></th>
					<th style="width: 33%;"></th>
					<th style="width: 33%;"></th>
				</tr>
			</table>
			<table id="desplegarDetalle" style="width: 100%;" border="0">
				<tr>
					<th style="width: 34%;"><s:text name="midas.general.negocio" />*</th>
					<th style="width: 33%;"><s:text name="midas.suscripcion.cotizacion.producto" />*</th>
					<th style="width: 33%;"><s:text name="midas.negocio.seccion.tiposPoliza" />*</th>
				</tr>
				<tr>
					<td style="width: 34%;">
						<s:select cssClass="cajaTexto" required="#requiredField"
								name="toFolio.negocio" id="idToNegocio" list="negocios"
								listValue="descripcionNegocio" listKey="idToNegocio"
								headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
					</td>
					<td style="width: 33%;">
						<s:select cssClass="cajaTexto" required="#requiredField"
								name="toFolio.negprod" id="idToNegProducto" list="{}"
								listKey="idToNegProducto" listValue="productoDTO.descripcion"
								headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
					</td>
					<td style="width: 33%;">
						<s:select id="idToTipoPoliza" cssClass="cajaTexto" list="{}" 
								name="toFolio.negTpoliza" listKey="idToTipoPoliza"
								headerKey="-1" listValue="tipoPolizaDTO.descripcion"
								headerValue="%{getText('midas.general.seleccione')}"
								required="#requiredField"/>
					</td>
				</tr>
				<tr>
					<th style="width: 34%;"><s:text name="midas.general.moneda" />*</th>
					<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.vigencia.folio" />*</th>
					<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.fecha.vigencia" />*</th>
				</tr>
				<tr>
					<td style="width: 34%;">
						<s:select name="toFolio.idTipoMoneda" id="idTcMoneda" required="#requiredField"
								cssClass="cajaTexto" list="{}" listKey="idTcMoneda" listValue=""
								headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
					</td>
					<td style="width: 33%;">
						<s:select cssClass="cajaTexto" required="#requiredField"
								name="toFolio.vigencia" id="idTcVigencia" list="{}"
								listKey="idTcVigencia" listValue="descripcion" headerKey="-1"
								headerValue="%{getText('midas.general.seleccione')}" />
					</td>
					<td style="width: 33%;">
						<sj:datepicker name="toFolio.fechaValidesHasta"
							id="fechaValidesHasta" labelposition="left" size="10" changeMonth="true"
							changeYear="true" readonly="readonly" cssClass="cajaTexto"
							cssStyle="width: 89%;" buttonImage="../img/b_calendario.gif" maxlength="10"
							onkeypress="return soloFecha(this, event, false);"
							onchange="ajustaFechaFinal();"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" >
						</sj:datepicker>
					</td>
				</tr>
				<tr>
					<th style="width: 34%;"><s:text name="midas.folio.reexpedible.field.folios.generar" />*</th>
					<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.agente.facultativo" />*</th>
					<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.comentarios.folio" /></th>
				</tr>
				<tr>
					<td style="width: 34%;">
						<s:textfield name="numeroFolios" id="numeroFolios" cssClass="cajaTexto" />
					</td>
					<td>
						<s:select cssClass="cajaTexto" name="toFolio.agenteFacultativo" id="idAgenteFacultativo"
								list="agentes" listKey="idTcAgente" listValue="nombre"
								headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
					</td>
					<td>
						<s:textfield name="toFolio.comentarios" id="comentarios" cssClass="cajaTexto" />
					</td>
				</tr>
				<tr>
					<th style="width: 34%;"><s:text name="midas.folio.reexpedible.field.tipo.folio" />*</th>
				</tr>
				<tr>
					<td style="width: 34%;">
						<s:select cssClass="cajaTexto" id="idtiposFolio" name="toFolio.tipoFolio.idTipoFolio" list="tipoFolios" listKey="idTipoFolio" 
							listValue="claveTipoFolio" headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a onclick="mostrarFormularios(false)" >
								<s:text name="midas.boton.cancelar" />
							</a>
						</div>
					</td>
					<td >
						<div class="btn_back w140" style="display: inline; float: right;">
							<sj:a onclick="guardarFolio()"  ><s:text name="midas.boton.guardar" /> </sj:a>
						</div>
					</td>
				</tr>
			</table>
		
	</s:form>
</div>

<br/><br/><br/>

<div id="folioReexpedibleList" class="w1000 h200" style="overflow: hidden"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
