<%@ taglib prefix="s" uri="/struts-tags"%>
<!--  imports Js files -->
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/solicitud/autorizacion/autorizacionSolicitud.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/validaciones.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/foliosReexpedibles/folios.js'/>"></script>
<script type="text/javascript">
	var guardar = '/folio/reexpedible/guardar.action';
	var inicializar = '<s:url action="inicializar" namespace="/folio/reexpedible"/>';
	var listar = '<s:url action="listar" namespace="/folio/reexpedible"/>';
	var actionGuardar = "/MidasWeb/folio/reexpedible/guardar.action";
	var limpiar = "/MidasWeb/folio/reexpedible/inicializar.action";
</script>