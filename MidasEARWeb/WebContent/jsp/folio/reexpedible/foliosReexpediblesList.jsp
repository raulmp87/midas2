<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="foliosExpedidos" var="folio" status="index">
		<row>
			<cell>
				<s:property value="tipoFolio.claveTipoFolio" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="negocio.descripcionNegocio" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="descripcionProducto" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="desctipcionTipoPoliza" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="descVigencia" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="folioInicio" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="fechaValidez" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:property value="comentarios" escapeHtml="false" escapeXml="true"/>
			</cell>
		</row>
	</s:iterator>
</rows>