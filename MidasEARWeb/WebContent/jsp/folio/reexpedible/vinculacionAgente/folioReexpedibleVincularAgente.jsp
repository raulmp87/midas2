<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<style type="cssClass">
dhx_pager_info_light {
    margin: 3px;
    text-align: center;
    font-family: tahoma;
    font-size: 12px;
    float: left;
    cursor: pointer;
    color: #055A78;
    background-color: #FFFFFF;
    padding: 1px;
}

</style>
<s:include value="/jsp/folio/reexpedible/contenedorHeaderFolio.jsp"></s:include>

<div id="contenedorFiltros" style="width: 98%;">
	<s:form id="vincular" name="toFolio" action="vincularFolios">
		<table  style="width: 100%;" border="0">
			<tr >
				<td style="width: 98%;" class="titulo" colspan="4" ><s:text name="midas.folio.reexpedible.head.asociar" /></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="mensajes" style="display: none;">
						<div id="textMessage" >  </div>
					</div>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"></th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
		</table>
		<table id="desplegarDetalle" border="0">
			<tr>
				<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.folio.inicio" /></th>
				<th style="width: 33%;"><s:text name="midas.folio.reexpedible.field.folio.fin" /></th>
			</tr>
			<tr>
				<td style="width: 33%;">
					<s:textfield id="folioInicioB" name="filtroFolioRe.folioInicio" cssClass="cajaTexto"  
						onfocus="javascript: new Mask('****_##########', 'string').attach(this)" />
				</td>
				<td style="width: 33%;">
					<s:textfield id="folioFinB" name="filtroFolioRe.folioFin" cssClass="cajaTexto" 
						onfocus="javascript: new Mask('****_##########', 'string').attach(this)" />
				</td>
			</tr>
		</table>
		<div id="botonesBusqieda" style="width: 98%;">
			<table width="100%">
				<tr>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="vincularFolios()" href="javascript: void(0);"> 
								<s:text name="midas.folio.reexpedible.boton.vincular" />
							</a>
						</div>
					</td>
					<td>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="limpiarVinculacion()" href="javascript: void(0);"> 
								<s:text name="midas.boton.limpiar" />
							</a>
						</div>
					</td>
				</tr>
			</table>
			</div>
	</s:form>
</div>
<div id="tablaInfo" style="width: 100%; display:none;">
	<div align="left" style="width:45%; float:left;">
		<table id="folioExito" width="100%">
		</table>
		<br/>
		<div width="100%" id="pagingAreafolioExito"></div>
	</div>
	<div style="width:45%; float: right;">
		<table id="folioError" width="100%">
		</table>
		<br/>
		<div width="100%" id="pagingAreafolioError"></div>
	</div>
</div>