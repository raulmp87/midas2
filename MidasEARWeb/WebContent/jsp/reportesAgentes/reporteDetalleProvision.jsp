<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">
</script>
<script type="text/javascript"	src="<s:url value='/js/midas2/agentes/reporteDetalleProvision.js'/>"></script>
<script type="text/javascript">
<!--
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteDetalleProvision"></s:url>';
//-->

jQuery(document).ready(function() {
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		})
		listadoService.getMapYears(16, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
	});
</script>

<div class="row">
	<div class="titulo c5">
		<label>Reporte Detalle Provisión</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteDetalleProvision/">
	<table class="contenedorFormas">
		<tr>
			<div class="btn_back " style="width: 270px; height: 20px;">
			 	 <s:text name="Año"/>&nbsp;<select id="anios" name="anio" class="txtfield w100 jQrequired"></select> &nbsp; 
			 	 <s:text name="Mes"/>&nbsp;<select id="meses" name="mes" class="txtfield w100 jQrequired"/></select> 
			</div>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agentes.configBono.descripcionBono"></s:text>
			</td>
			<td colspan="2">
				<s:select  name="filtroReporteProvision.configBono.id" id="tipoBono" cssClass="cajaTextoM2 w250" list="listTipoBono" 
					listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.." 
					 disabled="#readOnly"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text>
			</td>
			<td colspan="2">
<%-- 				<s:select  name="filtroReporteProvision.idCentroOperacion" id="centroOperacion" cssClass="cajaTextoM2 w250" list="centroOperacionList"  --%>
<!-- 					listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.."  -->
<!-- 					 disabled="#readOnly"/> -->
				<s:select id="centroOperacionList" headerKey=""
					headerValue="Seleccione..." list="centroOperacionList" listKey="id"
					listValue="descripcion" name="filtroReporteProvision.idCentroOperacion"
					cssClass="cajaTextoM2 w200"
					onchange="loadGerenciasByCentroOperacion(this.value);cleanList(this.id);">
				</s:select>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia"></s:text>
			</td>
			<td>
<%-- 				<s:select name="filtroReporteProvision.idGerencia" list="gerenciaList" id="gerenciaList"	headerKey="" headerValue="Seleccione.."  cssClass="w250 cajaTextoM2"> --%>
<%-- 				</s:select> --%>
				<s:select id="gerenciaList" headerKey=""
					headerValue="Seleccione..." list="gerenciaList"
					listKey="id" listValue="descripcion" name="filtroReporteProvision.idGerencia"
					cssClass="cajaTextoM2 w200"
					onchange="loadEjecutivoByGerencia(this.value);cleanList(this.id);">
				</s:select>

			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas
			</td>
			<td colspan="2">
				<s:select name="filtroReporteProvision.ejecutivo.id" list="ejecutivoList" 
				id="ejecutivoList"	headerKey="" headerValue="Seleccione.."  
				cssClass="w250 cajaTextoM2" 
				onchange="loadPromotoriaByEjecutivo(this.value);">
				</s:select>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.promotoria"></s:text>
			</td>
			<td>
				<s:select name="filtroReporteProvision.promotoria.id" list="promotoriaList" id="promotoriaList"	headerKey="" headerValue="Seleccione.." cssClass="w250 cajaTextoM2">
				</s:select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield  name="filtroReporteProvision.agenteOrigen.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="filtroReporteProvisiona.genteOrigen.id"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td width="180px">
				<s:textfield id="nombreAgente" name="filtroReporteProvision.agenteOrigen.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="Tipo de salida del archivo"></s:text>
			</td>
			<td>
				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly"
						list="#{'xlsx':'Excel','txt':'Texto'}"/>
			</td>
		</tr>
	</table>
</s:form>

<div class="row">
	<div class="c2" style="margin-left: 490px;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class="icon_excel"
				onclick="exportTo('excel');"> <s:text
					name="midas.boton.exportar" /> </a>
		</div>
	</div>
</div>