<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteResumenPreviewBonos.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reportePreviewBonos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reportePreviewBonos"></s:url>';
//-->

jQuery(document).ready(function() {
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		})
		listadoService.getMapYears(11, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
	});
</script>
<div class="row">
	<div class="titulo">
		<label class="">Reporte Resumen de Preview de Bonos </label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reportePreviewBonos/">
	<table width="98%" class="contenedorFormas" align="center">
<!-- 		<tr> -->
<!-- 			<td colspan="4"> -->
<!-- 				<div align="right"> -->
<%-- 					<s:text name="midas.cargos.reporte.tipoReporte"></s:text> --%>
<%-- 					<s:radio theme="simple" name="filtroCargos.tipoReporte" --%>
<%-- 							list="#{0:'Detallado',1:'General'}"></s:radio> --%>
<!-- 				</div> -->
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td colspan="">
				 	 <s:text name="Año"/>
			</td>
			<td>
				<select id="anios" name="anio" class="cajaTextoM2 w250 jQrequired"></select>
			 </td>
			 <td>
				 <s:text name="Mes"/>
			</td>
			<td>
				<select id="meses" name="mes" class="cajaTextoM2 w250 jQrequired"/></select> 
			</td>
		</tr>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td colspan=""> -->
<%-- 				<s:select  name="idCentroOperacion" id="centroOperacion" cssClass="cajaTextoM2 w250" list="centroOperacionList"  --%>
<!-- 					listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.."  -->
<!-- 					 onchange="loadGerenciasByCentroOperacion();" disabled="#readOnly"/> -->
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.prestamosAnticipos.gerencia"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:select name="idGerencia" list="gerenciaList" id="gerenciaList"	headerKey="" headerValue="Seleccione.." listKey="" --%>
<!-- 					listValue="" cssClass="w250 cajaTextoM2" onchange="loadEjecutivoByGerencia();"> -->
<%-- 				</s:select> --%>

<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas --%>
<!-- 			</td> -->
<!-- 			<td colspan=""> -->
<%-- 				<s:select name="idEjecutivo" list="ejecutivoList" id="ejecutivoList"	headerKey="" headerValue="Seleccione.." listKey="" --%>
<!-- 					listValue="" cssClass="w250 cajaTextoM2" onchange="loadPromotoriaByEjecutivo();"> -->
<%-- 				</s:select> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.prestamosAnticipos.promotoria"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:select name="idPromotoria" list="promorotiaList" id="promorotiaList"	headerKey="" headerValue="Seleccione.." listKey="" --%>
<!-- 					listValue="" cssClass="w250 cajaTextoM2"> -->
<%-- 				</s:select> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.agentes.configBono.descripcionBono"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td colspan=""> -->
<%-- 				<s:select  name="bono.id" cssClass="cajaTextoM2 w250" list="listTipoBono"  --%>
<!-- 				listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.." disabled="#readOnly"> -->
<%-- 				 </s:select> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.prestamosAnticipos.tipoAgente"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:select  name="tipAgente.id" id="tipoAgente" cssClass="cajaTextoM2 w250" list="tipoAgente"  --%>
<!-- 				listKey="id" listValue="valor" headerKey="" headerValue="Seleccione.." disabled="#readOnly"> -->
<%-- 				 </s:select> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<th> -->
<%-- 				<s:text name="midas.prestamosAnticipos.numeroAgente" /> --%>
<!-- 			</th>	 -->
<!-- 			<td> -->
<%-- 				<s:textfield  name="agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w250 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeAgentePrevBono();"></s:textfield> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<!-- 				<div class="btn_back w110"> -->
<!-- 					<a href="javascript: void(0);" class="icon_buscar" -->
<!-- 						onclick="mostrarListadoAgentesPrevBono();"> -->
<%-- 						<s:text name="midas.boton.buscar"/> --%>
<!-- 					</a> -->
<!-- 				</div> -->
<%-- 					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/> --%>
<%-- 					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.prestamosAnticipos.nombreAgente" /> --%>
<!-- 			</td> -->
<!-- 			<td width="180px"> -->
<%-- 				<s:textfield id="nombreAgente" name="agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" /> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td>
				<s:text name="Tipo de salida del archivo"></s:text>
			</td>
			<td>
				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly"
						list="#{'xlsx':'Excel','txt':'Texto'}"/>
			</td>
		</tr>
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>
       
 