<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteIngresos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteIngresos"></s:url>';
	jQuery(document).ready(function() {
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		});
		listadoService.getMapYears(16, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
	});
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Ingresos por Agente</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/"
	id="exportarToPDF">
	<table class="contenedorFormas">
		<!-- 		<tr> -->
		<%-- 			<th><label style="font-weight: bold;"><s:text --%>
		<%-- 						name="midas.fuerzaventa.reportesAgentes.tipoReporte"></s:text> </label> --%>
		<!-- 			</th> -->
		<%-- 			<td><s:radio theme="simple" name="tipoReporte" --%>
		<%-- 					list="#{0:'Detallado',1:'General'}"></s:radio> --%>
		<!-- 			</td> -->
		<!-- 		</tr> -->
		<tr><th>Periodo</th></tr>
		<tr>
			<th><s:text name="midas.reporteAgente.topAgente.anio"></s:text>
			</th>
			<td>
					<select id="anios" name="anio" class="txtfield w200 jQrequired"></select>
			</td>
			<th><s:text name="midas.reporteAgente.topAgente.mes"></s:text>
			</th>
			<td>
					<select id="meses" name="mes" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<!-- 
		<tr>
			<th>Rango de Agentes</th>
		</tr>
		 -->
		<tr>
			<th>Agente Inicio:</th>
			<td><s:textfield name="rangoInicio"
					cssClass="txtfield w200 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>Agente Fin:</th>
			<td><s:textfield name="rangoFin"
					cssClass="txtfield w200 jQnumeric jQrestrict"></s:textfield>
			</td>
		</tr>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="Tipo de salida del archivo"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly" --%>
<%-- 						list="#{'xlsx':'Excel','txt':'Texto'}"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>