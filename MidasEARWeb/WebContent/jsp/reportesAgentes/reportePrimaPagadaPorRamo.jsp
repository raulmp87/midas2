<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reportePrimaPagadaPorRamo"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reportePrimaPagadaPorRamo"></s:url>';
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Prima Pagada Por
			Ramo</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/"
	id="exportarToPDF">
<jsp:include page="reporteAgenteAllFiltersForm.jsp"></jsp:include>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>
<script type="text/javascript">
<!--
	jQuery("#s_ClasificacionAgente").hide();
//-->
</script>