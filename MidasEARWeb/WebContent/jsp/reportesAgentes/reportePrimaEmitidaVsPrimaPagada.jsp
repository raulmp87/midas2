<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/repPrimaEmitidaVsPrimaPagada.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/repPrimaPagadaVsPrimaEmitida"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/repPrimaPagadaVsPrimaEmitida"></s:url>';
//-->

jQuery(document).ready(function() {
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		})
		listadoService.getMapYears(11, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
// 		listadoService.getMapMonths(function(data) {
// 			addOptionsHeaderAndSelect("mesesFin", data, null, $_month, "Seleccione...");
// 		})
// 		listadoService.getMapYears(11, function(data) {
// 			addOptionsHeaderAndSelect("aniosFin", data, null, $_year, "Seleccione...");
// 		});
	});
</script>
<div class="row">
	<div class="titulo">
		<label class="">Reporte Prima Emitida VS Prima Pagada</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reportePreviewBonos/">
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
		<td>
			<s:text name="Periodo"></s:text>
		</td>
		</tr>
		<tr>
<!-- 			<td> -->
<%-- 				<s:text name="Año"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td colspan="2"> -->
<%-- 					<select id="anios" name="anio" onchange="selectFin(this.value)" class="cajaTextoM2 w200 jQrequired"></select> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:text name="Mes"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<select id="meses" name="mes" class="cajaTextoM2 w200 jQrequired"></select> --%>
<!-- 			</td> -->
		<th> <s:property value="labelFechaInicio"/>
			</th>
			<td><sj:datepicker name="fechaInicio"
					cssStyle="width: 170px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaInicio"
					maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			<th><s:property value="labelFechaFin"/>
			</th>
			<td><sj:datepicker name="fechaFin" cssStyle="width: 170px;"
					required="#requiredField" buttonImage="../img/b_calendario.gif"
					id="fechaFinal" maxDate="today" changeMonth="true"
					changeYear="true" maxlength="10" cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
		</tr>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.reporteAgente.topAgente.anio.fin"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td colspan="2"> -->
<%-- 					<select id="aniosFin" name="anioFin" class="cajaTextoM2 w200"></select> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:text name="midas.reporteAgente.topAgente.mes.fin"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<select id="mesesFin" name="mesFin" class="w200 cajaTextoM2"></select> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text>
			</td>
			<td colspan="2">
				<s:select  name="idCentroOperacion" id="centroOperacion" cssClass="cajaTextoM2 w250" list="centroOperacionList" 
					listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.." 
					 onchange="loadGerenciasByCentroOperacion();" disabled="#readOnly"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia"></s:text>
			</td>
			<td>
				<s:select name="idGerencia" list="gerenciaList" id="gerenciaList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2" onchange="loadEjecutivoByGerencia();">
				</s:select>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas
			</td>
			<td colspan="2">
				<s:select name="idEjecutivo" list="ejecutivoList" id="ejecutivoList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2" onchange="loadPromotoriaByEjecutivo();">
				</s:select>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.promotoria"></s:text>
			</td>
			<td>
				<s:select name="idPromotoria" list="promorotiaList" id="promorotiaList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2">
				</s:select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield  name="agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td width="180px">
				<s:textfield id="nombreAgente" name="agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 				<s:text name="Tipo de salida del archivo"></s:text> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly" --%>
<%-- 						list="#{'xlsx':'Excel','txt':'Texto'}"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>
