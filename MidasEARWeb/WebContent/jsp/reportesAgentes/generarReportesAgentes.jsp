<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

	<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>	
	
	<script type="text/javascript" src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">	</script>
	<script type="text/javascript" src="<s:url value='/js/midas2/agentes/generarDocumentosAgentes.js'/>">	</script>

<script type="text/javascript">
	var buscarCancelacionesPendientesPath = '<s:url action="listarCancelaciones" namespace="/endoso/previo"/>';
	var buscarCancelacionesPendientesPaginadoPath = '<s:url action="listarCancelacionesPaginado" namespace="/endoso/previo"/>';
	var actualizarBloqueoPath = '<s:url action="actualizarBloqueo" namespace="/endoso/previo"/>';	
	var emitirPendientePath = '<s:url action="emitirPendiente" namespace="/endoso/previo"/>';   
	var excelCancelacionesPendientesPath = '<s:url action="getExcelCancelacionesPendientes" namespace="/endoso/previo"/>';
	var aplicarAccionSeleccionadasPath = '<s:url action="aplicarAccionSeleccionadas" namespace="/endoso/previo"/>';
	var iniciarCancelacionAutomaticaPath = '<s:url action="iniciarCancelacionAutomatica" namespace="/endoso/previo"/>';
	var monitorearCancelacionAutomaticaPath = '<s:url action="monitorearProcesoCancelacionAutomatica" namespace="/endoso/previo"/>';
	var buscarAgentePath = '<s:url action="buscarAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';	
	
	var enviarDocsPath = '<s:url action="enviarDocumentos" namespace="/fuerzaventa/calculos/generarDocumentos"/>';	
	var buscarAgentesParaGenDocsPath = '<s:url action="listarDocumentos" namespace="/fuerzaventa/calculos/generarDocumentos"/>';
	var verDocumentoPath = '<s:url action="verDocumento" namespace="/fuerzaventa/calculos/generarDocumentos"/>';
</script>	
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
   
   .wwgrp {
    padding: 4px 5px;
    padding-left: 0px;}
   
</style> 

<div class="row">
	<div class="titulo c5">
		<label class="">Reportes para Agente</label>
	</div>
</div>
Indicar Par&#225;metros para Generar los Reportes
<s:form id="generarDocumentosForm">
	<table class="contenedorFormas" style="width:95%">
	    <tr>
			<th width="80px;"><font color="#FF6600">* </font><s:property value="labelFechaInicio" default="Tipo de Documento:"/>
			</th>
			<td width="250px;">		 	    
				<s:select name="tipoDocumento" id="tiposDocs"					      
					      labelposition="left" required="true" 
					      value="tipoDocumento"
					      onchange="ocultarFormatoSalida(); validarTipoReciboHonorarios();"
					      headerKey="" headerValue="Seleccione ..." 
					      list="tipoDocumentos" listKey="clave" listValue="valor" 					      	   
				          cssClass=" txtfield w200"/> 
			</td>
		   <th width="80px;">
		   		<div id="lbFormatoSalida" style="display:inline">
		   			<font color="#FF6600">* </font><s:property value="formatoDeSalida" default="Formato de Salida:"/>
		   		</div>
			</th>
			<td><div id="txfFormatoSalida" style="display:inline">
					<s:select name="tipoSalidaArchivo" id="formatosal" cssClass="cajaTextoM2 w150" disabled="false"
					list="#{'pdf':'PDF'}"/>
				</div>
			</td>
	    </tr>
		<tr>
		    <th><font color="#FF6600">* </font><s:text name="Mes:"></s:text>
			</th>
			<td width="250px;">
			    <select id="meses" name="mes" class="txtField w200" ></select>			   
			</td>		
			<!--<td width="80px;"></td>	-->
			<th><font color="#FF6600">* </font><s:text name="A&#241;o:"></s:text>
			</th>
			<td width="250px;">
			    <select id="anios" name="anio" class="txtField w200" ></select>
			</td>			
		</tr>
		
		<s:if test="%{isPromotor==true}">
		
			<tr>
				<th>
				<div id="div_lbl_agente">
					<font color="#FF6600">* </font>
					<s:text name="midas.fuerzaventa.configBono.agente" />:
					<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
							onchange="addAgenteProduccion();" style="display:none;" />
				</div>
				</th>
				<td>
					<div id="div_agente"><select id="agentes" name="agentes" class="txtField w200" onchange="setAgente(this.value)"></select></div>
				</td>
				<td colspan="2" align="right">
				</td>
			</tr>	
			
		</s:if>
		
		<tr height="40px;">
		
			<td align="center" valign="bottom" colspan="4">
				<table style="width:100%">
					<tr>
						<td  align="right">
							<div class="btn_back w150"  id="botonCorreoTxt">
		    					<a href="javascript: void(0);"
									onclick="enviarReportePorCorreo();">
									<s:text name="Enviar Por Correo"/>
								</a>
							</div>
						</td>
						<td  align="right" width="15%">
							<div class="btn_back w150">
								<a href="javascript: void(0);" id="botonTxt"
									onclick="verDocumento(document.getElementById('idRegAgente').value);">
									<s:text name="Generar Reporte"/>
								</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>	
		<s:hidden id="idProm" value="%{idPromotoria}" name="idPromotoria"></s:hidden>
		<s:hidden id="idRegAgente" value="%{idRegAgente}" name="idRegAgente"></s:hidden>
		<s:hidden id="isAgente" value="%{isAgente}" name="isAgente"></s:hidden>
		<s:hidden id="chkDetalle" value="true" name="chkDetalle"></s:hidden>
		<s:hidden id="chkGuiaLectura" value="true" name="chkGuiaLectura"></s:hidden>
		<s:hidden id="chkMostrarColAdicionales" value="true" name="chkMostrarColAdicionales"></s:hidden>
	</table>
<div id="spacer1" style="height: 10px"></div>

<div class="titulo" style="width: 98%;">
		<s:text name="Lista de Documentos generados"/>	
	</div>

	<div id="indicador"></div>
	<div id="guiaHonorariosGrid" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>

	<div id="SIS">
		<div id="divExcelBtn" class="w150"
			style="float: right; display: none;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="regresar();"> <s:text
						name="midas.boton.regresar" /> </a>
			</div>
		</div>
	</div>
    

<div class="w930" align="center">
	<div class="btn_back w150" style="visibility: hidden;" id="botonCorreo2Txt">
	    <a href="javascript: void(0);"
		onclick="enviarRecibosHonorariosPorCorreo();">
		<s:text name="Enviar Por Correo"/>
		</a>
	</div>
</div>

</s:form>
<div id="spacer1" style="height: 10px"></div>