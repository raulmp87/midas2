<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"/>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/agentes/reportes.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>	
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<style>
<!--
table{
	<%--width: 908px;--%>
}
ul {
	border: 1px solid #B2DBB2
}
.lihiden {
	display: none;
}
-->
</style>
<script type="text/javascript">
<!--
/**
 * Optiene el anio actual
 */
var $_date = new Date();
var $_year = $_date.getFullYear();
var $_month = ($_date.getMonth()+1);
/**
 * Oculta los li que no cumplen con la descripcion proporcionada
 */
	function $_findUi(value, targetFind) {
		jQuery("#" + targetFind).find('li')
				.each(
						function(i, e) {
							var text = e.innerHTML;
							if (value.length >= 1
									&& text.toLowerCase().indexOf(
											value.toLowerCase()) == -1) {
								e.className = "lihiden";
							} else {
								e.className = "";
							}
						})
	}
/**
 * Oculta los option que no cumplen con la descripcion proporcionada
 */
	function $_findOption(value, targetFind) {
		jQuery("#" + targetFind).find('option')
				.each(
						function(i, e) {
							var text = e.innerHTML;
							if (value.length >= 1
									&& text.toLowerCase().indexOf(
											value.toLowerCase()) == -1) {
								e.className = "lihiden";
							} else {
								e.className = "";
							}
						})
	}
/**
 * Oculta los option que son menores aun valor proporcionado
 * Solo Numeros enteros
 @param targetFind select en donde se quiere buscar
 */
	function $_hidenOptionInt(value, targetFind) {
		jQuery("#" + targetFind).find('option')
				.each(
						function(i, e) {
							var text = e.value;
							if (parseInt(text) < value) {
								e.className = "lihiden";
							} else {
								e.className = "";
							}
						})
	}

	/**
	 * Valida que las fechas ingresas en un periodo sean correctas
	 **/
	function $_validaPeriodo() {
	var fechaIni = jQuery("#fechaInicio").val();
	var fechaFin = jQuery("#fechaFinal").val()
	if(fechaIni !='' && fechaFin !=''){
			if (jQuery("#fechaInicio").size() == 1
					&& jQuery("#fechaFinal").size() == 1) {
				var year1 = jQuery("#fechaInicio").val().substring(6);
				var year2 = jQuery("#fechaFinal").val().substring(6);
				var day1 = jQuery("#fechaInicio").val().substring(0, 2);
				var day2 = jQuery("#fechaFinal").val().substring(0, 2);
				var month1 = jQuery("#fechaInicio").val().substring(3, 5);
				var month2 = jQuery("#fechaFinal").val().substring(3, 5);
				if (!fechaMenorIgualQue(jQuery("#fechaInicio").val(),jQuery("#fechaFinal").val())) {
					mostrarMensajeInformativo(
							"La fecha de inicio no puede ser mayor a la de fin",
							"20", null, null);
					return false;
				}
	// 			if ( year2 - year1 >= 1) {
	// 				mostrarMensajeInformativo(
	// 						"Por rendimiento solo es posible imprimir periodos de 1 a\u00f1o",
	// 						"20", null, null);
	// 				return false;
	// 			}
				return true;
			} else {
				return true;
			}
		}
		return true;
	}
	
	/**
	 * Valida que no se pueda generar un reporte del mes actual en la fecha actual
	 */
	function $_validaAnioMes() {
		if (jQuery("#anios").size() == 1 && jQuery("#meses").size() == 1) {
			var month = jQuery("#meses").val();
			var year = jQuery("#anios").val();
// 			if (year == $_year && month == $_month) {
// 				mostrarMensajeInformativo(
// 						"No esposible generar un reporte del mes actual",
// 						"20", null, null);
// 				return false;
// 			}
			if (jQuery("#aniosFin").size() == 1 && jQuery("#mesesFin").size() == 1) {
				var monthEnd = jQuery("#mesesFin").val();
				var yearEnd = jQuery("#aniosFin").val();
// 				if (yearEnd == $_year && monthEnd == $_month) {
// 					mostrarMensajeInformativo(
// 							"No esposible generar un reporte del mes actual",
// 							"20", null, null);
// 					return false;
// 				}
				if (yearEnd - year > 1) {
					mostrarMensajeInformativo(
							"Por rendimiento solo es posible imprimir periodos de 1 a\u00f1o",
							"20", null, null);
					return false;
				}
			}

			return true;
		} else {
			return true;
		}
	}
//-->

</script>