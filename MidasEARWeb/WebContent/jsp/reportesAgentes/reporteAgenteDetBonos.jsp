<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteDetalleBonos.js'/>">
</script>

	<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteDetalleBonos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteDetalleBonos"></s:url>';
	jQuery(document).ready(function() {
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("meses", data, null, "", "Seleccione...");
		})
		listadoService.getMapYears(11, function(data) {
			addOptionsHeaderAndSelect("anios", data, null, "", "Seleccione...");
		});
		listadoService.getMapMonths(function(data) {
			addOptionsHeaderAndSelect("mesesFin", data, null,"", "Seleccione...");
		})
		listadoService.getMapYears(11, function(data) {
			addOptionsHeaderAndSelect("aniosFin", data, null,"", "Seleccione...");
		});
	});
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Detalle de Bonos </label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteDetalleBonos/"
	id="exportarToPDF">
	
	<table class="contenedorFormas">
	<tr id="s_fechas">
			<th> <s:property value="labelFechaInicio"/>
			</th>
			<td><sj:datepicker name="fechaInicial"
					cssStyle="width: 170px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaInicio"
					maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			<th><s:property value="labelFechaFin"/>
			</th>
			<td><sj:datepicker name="fechaFinal" cssStyle="width: 170px;"
					required="#requiredField" buttonImage="../img/b_calendario.gif"
					id="fechaFinal" maxDate="today" changeMonth="true"
					changeYear="true" maxlength="10" cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
		</tr>
<!-- 		<tr> -->
<!-- 			<th>fecha corte</th> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<th> -->
<%-- 				<s:text name="midas.agentes.calculo.comisiones.pendientesPago.fechaCorteInicio"></s:text> --%>
<!-- 			</th> -->
<!-- 			<td> -->
<%-- 				<select id="anios" name="anio" onchange="" class="txtfield w200 jQrequired"></select> --%>
<!-- 			</td> -->
<!-- 			<th> -->
<%-- 				<s:text name="midas.agentes.calculo.comisiones.pendientesPago.fechaCorteFin"></s:text> --%>
<!-- 			</th> -->
<!-- 			<td> -->
<%-- 				<select id="meses" name="mes" class="txtfield w200 jQrequired"></select> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<th> -->
<%-- 				<s:text name="midas.reporteAgente.topAgente.anio.fin"></s:text> --%>
<!-- 			</th> -->
<!-- 			<td> -->
<%-- 				<select id="aniosFin" name="anioFin" class="txtfield w200"></select> --%>
<!-- 			</td> -->
<!-- 			<th> -->
<%-- 				<s:text name="midas.reporteAgente.topAgente.mes.fin"></s:text> --%>
<!-- 			</th> -->
<!-- 			<td> -->
<%-- 				<select id="mesesFin" name="mesFin" class="txtfield w200"></select> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield  name="agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeIdAgenteDetBono();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentesDetBono();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgenteDetBono();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td width="180px">
				<s:textfield id="nombreAgente" name="agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="Tipo de salida del archivo"></s:text>
			</td>
			<td>
				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly"
						list="#{'xlsx':'Excel','txt':'Texto'}"/>
			</td>
		</tr>
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>