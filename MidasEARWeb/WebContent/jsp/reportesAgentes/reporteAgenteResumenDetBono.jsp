<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteResumenDetBono.js'/>"></script>
<script type="text/javascript">
<!--
	/*var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteResumenDetalleBonos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteResumenDetalleBonos"></s:url>';*/
	//-->
	jQuery(document).ready(
			function() {
				listadoService.getMapMonths(function(data) {
					addOptionsHeaderAndSelect("mes", data, null, "",
							"Seleccione...");
				})
				listadoService.getMapMonths(function(data) {
					addOptionsHeaderAndSelect("mesFin", data, null, "",
							"Seleccione...");
				})
				listadoService.getMapYears(5, function(data) {
					addOptionsHeaderAndSelect("anios", data, null, "",
							"Seleccione...");
				});
			});
</script>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 
	 var urlFiltro="/MidasWeb/fuerzaventa/reporteAgenteResumenDetalleBonos/listarDocumentos.action?sinFiltro=1";
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"agentesDocumentosGrid", null,idField,'prestamosModal');
 });
	
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Resumen Detalle de Bonos</label>
	</div>
</div>
<s:form id="exportarToPDF">
<s:hidden name="sinFiltro" value="0"></s:hidden>
	<table class="contenedorFormas">
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.anio"></s:text>
			</th>
			<td>
				<select id="anios" name="anio" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.inicio"/>
			</th>
			<td>
				<select id="mes" name="mes" class="cajaTextoM2 w200 jQrequired"></select>
			</td>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.fin"/>
			</th>
			<td>
				<select id="mesFin" name="mesFin" class="cajaTextoM2 w200 jQrequired"></select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text>
			</th>
			<td>
				<s:select  name="centroOperacion.id" id="centroOperacion" cssClass="cajaTextoM2 w250" list="centroOperacionList" 
					listKey="id" listValue="descripcion" headerKey="" headerValue="Seleccione.." 
					 onchange="loadGerenciasByCentroOperacion(this.value);" disabled="#readOnly"/>
			</td>
			<th>
				<s:text name="midas.prestamosAnticipos.gerencia"></s:text>
			</th>
			<td>
				<s:select name="gerencia.id" list="gerenciaList" id="gerenciaList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2" onchange="loadEjecutivoByGerencia(this.value);">
				</s:select>

			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas
			</th>
			<td>
				<s:select name="ejecutivo.id" list="ejecutivoList" id="ejecutivoList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2" onchange="loadPromotoriaByEjecutivo(this.value);">
				</s:select>
			</td>
			<th>
				<s:text name="midas.prestamosAnticipos.promotoria"></s:text>
			</th>
			<td>
				<s:select name="promotoria.id" list="promorotiaList" id="promotoriaList"	headerKey="" headerValue="Seleccione.." listKey=""
					listValue="" cssClass="w250 cajaTextoM2">
				</s:select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.negocio.clasificacionAgente"></s:text>
			</th>
			<td>
				<s:select  name="tipoAgentes.id" id="tipoAgente" cssClass="cajaTextoM2 w250" list="tipoAgente" 
				listKey="id" listValue="valor" headerKey="" headerValue="Seleccione.." disabled="#readOnly">
				 </s:select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield  name="agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w250 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/>
			</td>
		</tr>	
		<tr>
			<th>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</th>
			<td>
				<s:textfield id="nombreAgente" name="agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
		<tr height="40px;">
	    <td colspan="6" align="right" valign="bottom">
	        <div class="btn_back w150">
			<a href="javascript: void(0);"
				onclick="javascript: listarFiltradoGenerico('/MidasWeb/fuerzaventa/reporteAgenteResumenDetalleBonos/listarDocumentos.action', 'agentesDocumentosGrid', document.exportarToPDF,'','');">
				<s:text name="Generar Documento"/>
			</a>
		</div>
	    </td>
	</tr>
	</table>
</s:form>
<div id="spacer1" style="height: 10px"></div>
Lista de Documentos Generados
<table style="width: 930px;">
    <tr>
        <td>
            <div id="tablaAgentesDocumentos" class="detalle" style="width: 98%;">			    
			    <div id="indicador"></div>
					<div id="gridAgentesDocumentosPaginado">
						<div id="agentesDocumentosGrid" style="height:300px"></div>
				        <div id="pagingArea"></div><div id="infoArea"></div>
			    </div>
		    </div>
        </td>
    </tr>
</table>
