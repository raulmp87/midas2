<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="id" type="ro" width="120" sort="int"><s:text name="No. Agente"/></column>
		<column id="" type="ro" width="650" sort="str"><s:text name="Nombre del Agente"/></column>
		<column id="" type="img" width="140" sort="na"><s:text name="Exportar Archivo"/></column>	
	</head>
	<s:iterator value="listaAgentesView" var="lista" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${lista.idAgente}]]></cell>
			<cell><![CDATA[${lista.nombreCompleto}]]></cell>
			<cell><s:url value="/img/b_printer.gif"/>^<s:text name="midas.boton.imprimir"/>^javascript:imprimirReporte(${lista.idAgente})^_self</cell>					
		</row>
	</s:iterator>
</rows>