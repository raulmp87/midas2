<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteProvisiones.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteProvisiones"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteProvisiones"></s:url>';
//-->
</script>

<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
</style>  
<div class="row">
	<div class="titulo c5">
		<label>Filtros de busqueda Reporete Provisiones</label>
	</div>
</div>
<div class="row">
	<div class="btn_back w130" style="display: inline; float: right;">
		<a href="javascript: void(0);" class="icon_limpiar"
			onclick="limpiarForm('exportarToPDF');"> <s:text
				name="midas.suscripcion.cotizacion.limpiar" /> </a>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteProvisiones/">
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td colspan="4">
				<div align="right">
					<s:text name="midas.cargos.reporte.tipoReporte"></s:text>
					<s:radio theme="simple" name="tipoReporte"
 							list="#{0:'Detallado',1:'General'}"></s:radio> 
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.provision.reporte.periodoInicial"></s:text>
			</td>
			<td>
				<sj:datepicker name="periodoInicial" id="" buttonImage="../img/b_calendario.gif"			 
			   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
			   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 	 		   	onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<s:text name="midas.provision.reporte.periodoFial"></s:text>
			</td>
			<td>
				<sj:datepicker name="periodoFial" id="" buttonImage="../img/b_calendario.gif"			 
			   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
			   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 		   	onblur="esFechaValida(this);"></sj:datepicker> 
			</td>
		</tr>
				<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text>
			</td>
			<td>
				<ul id="operacionesList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="centroOperacionList"
						var="varListaCentroOperacion" status="stat">
						<li><label for="varListaCentroOperacion[%{#stat.index}].id">
								<input type="checkbox" onchange=""
								onclick="loadGerenciasByCentroOperacion();hiddenList('operacionesList');"
								name="centroOperacionesSeleccionados[${stat.index}].id"
								id="centroOperacionesSeleccionados${stat.index}"
								value="${varListaCentroOperacion.id}" class="js_checkEnable" />
								${varListaCentroOperacion.descripcion} </label>
							</li>
					</s:iterator>
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia"></s:text>
			</td>
			<td>
				<ul id="gerenciaList" class="w300"
					style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas
			</td>
			<td>
				<ul id="ejecutivoList" class="w300"
						style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.promotoria"></s:text>
			</td>
			<td>
				<ul id="promorotiaList" class="w300"
						style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.tipoPromotoria"></s:text>
			</td>
			<td>
				<ul id="tipoPromotoriaList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="tipoPromotoria" var="varListTipoPromotoria"
							status="stat">
						<li>
							<label for="varListTipoPromotoria[%{#stat.index}].id">
								<input type="checkbox"
								name="tipoPromotoriasSeleccioadas[${stat.index}].id"
								id="tipoPromotoriasSeleccioadas${stat.index}"
								value="${varListTipoPromotoria.id}" class="js_checkEnable" />
								${varListTipoPromotoria.valor} </label>
						</li>
					</s:iterator>
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.tipoAgente"></s:text>
			</td>
			<td>
				<ul id="tipoAgenteList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="tipoAgente" var="varListTipoAgente"
							status="stat">
							<li>
								<label for="varListTipoAgente[%{#stat.index}].id">
								<input type="checkbox"
								name="tipoAgentesSeleccionados[${stat.index}].id"
								id="tipoAgentesSeleccionados${stat.index}"
								value="${varListTipoAgente.id}" class="js_checkEnable" />
								${varListTipoAgente.valor} </label>
						  </li>
					</s:iterator>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.cargos.gridDetalle.estatus"></s:text>
			</td>
			<td>
				<ul id="estatus" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="estatus" var="varListEstatus"
							status="stat">
							<li><label for="estatus[%{#stat.index}].id">
									<input type="checkbox"
									name="estatus[${stat.index}].id"
									id="estatusCargos${stat.index}"
									value="${varListEstatus.id}" class="js_checkEnable" />
									${varListEstatus.valor} </label>
							</li>
						</s:iterator>
					</ul>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.tipoCedula"></s:text>
			</td>
			<td>
				<s:select name="tipoCedulaSeleccionada" list="tipoCedula"
				headerKey="" headerValue="Seleccione.." listKey="id"
				listValue="valor" cssClass="w300 cajaTextoM2"></s:select>
			</td>
		</tr>
		<tr>
		<th><s:text name="midas.fuerzaventa.configBono.lineaVenta"></s:text>
			</th>
			<td>
				<ul id="ajax_listaLineaVenta" class="w300"> 
					<s:iterator value="lineaVenta" var ="varLineaVenta" status="stat" > 
						   <li> 
								<s:if test="#varLineaVenta.checado==1">
									<label for="varLineaVenta[%{#stat.index}].id"> 
										 <input type="checkbox" 
										 name="listaLineaVentaSeleccionadas[${stat.index}].clave"  
										 id="listaLineaVentaSeleccionadas${stat.index}"  
										 value="${varLineaVenta.clave}"  
										 onchange='javascript:onChangeLineaVenta();' 
										 checked="checked"
										 class="js_checkEnable"/>                                                    
								  ${varLineaVenta.valor}
							   </label> 
							   </s:if>
							   <s:else>
							   <label for="varLineaVenta[%{#stat.index}].id"> 
									  <input type="checkbox" 
											 name="listaLineaVentaSeleccionadas[${stat.index}].clave"  
											 id="listaLineaVentaSeleccionadas${stat.index}"  
											 value="${varLineaVenta.clave}"  
											 onchange='javascript:onChangeLineaVenta();' 
											 class="js_checkEnable"/>                                                    
									  ${varLineaVenta.valor}
								   </label>
							   </s:else> 
						  </li>
					</s:iterator>
				</ul>       
			</td>
			<th><s:text name="midas.fuerzaventa.configBono.subramo"></s:text>
			</th>
			<td>
				<ul id="ajax_listaSubRamos" class="w300">
					<s:iterator value="listaSubRamos" var ="varObjSubRamos" status="stat" > 
								<li> 
									<s:if test="#varObjSubRamos.checado==1">
										<label for="varObjSubRamos[%{#stat.index}].id"> 
											 <input type="checkbox" 
											 name="listaSubRamosSeleccionados[${stat.index}].idSubramo"  
											 id="listaSubRamosSeleccionados${stat.index}"  
											 value="${varObjSubRamos.id}"  
											 checked="checked"
											 class="js_checkEnable"/>                                                    
									  ${varObjSubRamos.valor}
								   </label> 
								   </s:if>
								   <s:else>
								   <label for="varObjSubRamos[%{#stat.index}].id"> 
									 <input type="checkbox"
										 name="listaSubRamosSeleccionados[${stat.index}].idSubramo"  
										 id="listaSubRamosSeleccionados${stat.index}"  
										 value="${varObjSubRamos.id}"  
										 class="js_checkEnable"/>                                                    
									  ${varObjSubRamos.valor}
									</label>
							 </s:else> 
						 </li>
					 </s:iterator>
				</ul> 
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.configBono.producto"></s:text>
			</th>
			<td>
				<ul id="ajax_listaProductos" class="w300">
						 <s:iterator value="listaProductos" var ="varProductos" status="stat" > 
							<li> 
								<s:if test="#varProductos.checado==1">
									<label for="varProductos[%{#stat.index}].id"> 
										 <input type="checkbox" 
										 name="listaProductosSeleccionados[${stat.index}].idProducto"  
										 id="listaProductosSeleccionados${stat.index}"  
										 value="${varProductos.id}"  
										 onchange="onChangeProducto();" 
										 checked="checked"
										 class="js_checkEnable"/>                                                    
								  ${varProductos.valor}
							   </label> 
							   </s:if>
							   <s:else>
							   <label for="varProductos[%{#stat.index}].id"> 
									  <input type="checkbox"
											 name="listaProductosSeleccionados[${stat.index}].idProducto"  
											 id="listaProductosSeleccionados${stat.index}"  
											 value="${varProductos.id}"  
											 onchange="onChangeProducto();" 
											 class="js_checkEnable"/>                                                    
									  ${varProductos.valor}
								   </label>
							   </s:else> 
						  </li>
					</s:iterator>
				</ul> 
			</td>
				<th><s:text name="midas.fuerzaventa.configBono.lineaNegocio"></s:text>
			</th>
			<td>
				<ul id="ajax_lineaNegocio" class="w300">
					<s:iterator value="listaLineaNegocio" var ="varObjLineaNegocio" status="stat" > 
								<li> 
									<s:if test="#varObjLineaNegocio.checado==1">
										<label for="varObjLineaNegocio[%{#stat.index}].id"> 
											 <input type="checkbox" 
											 name="listaLineaNegocioSeleccionados[${stat.index}].idSeccion"  
											 id="listaLineaNegocioSeleccionados${stat.index}"  
											 value="${varObjLineaNegocio.id}"  
											 onchange="onChangeLineaNegocio();" 
											 checked="checked"
											 class="js_checkEnable"/>                                                    
									  ${varObjLineaNegocio.valor}
								   </label> 
								   </s:if>
								   <s:else>
								   <label for="varObjLineaNegocio[%{#stat.index}].id"> 
									 <input type="checkbox"
										 name="listaLineaNegocioSeleccionados[${stat.index}].idSeccion"  
										 id="listaLineaNegocioSeleccionados${stat.index}"  
										 value="${varObjLineaNegocio.id}"  
										 onchange="onChangeLineaNegocio();" 
										 class="js_checkEnable"/>                                                    
									  ${varObjLineaNegocio.valor}
									</label>
							 </s:else> 
						 </li>
					 </s:iterator>
				</ul> 
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.configBono.ramo"></s:text>
			</th>
			<td>
				<ul id="ajax_listaRamos" class="w300">
						<s:iterator value="listaRamos" var ="varObjRamos" status="stat" > 
								<li> 
									<s:if test="#varObjRamos.checado==1">
										<label for="varObjRamos[%{#stat.index}].id"> 
											 <input type="checkbox" 
											 name="listaRamosSeleccionados[${stat.index}].idRamo"  
											 id="listaRamosSeleccionados${stat.index}"  
											 value="${varObjRamos.id}"  
											 onchange="onChangeRamo();" 
											 checked="checked"
											 class="js_checkEnable"/>                                                    
									  ${varObjRamos.valor}
								   </label> 
								   </s:if>
								   <s:else>
								   <label for="varObjRamos[%{#stat.index}].id"> 
									 <input type="checkbox"
										 name="listaRamosSeleccionados[${stat.index}].idRamo"  
										 id="listaRamosSeleccionados${stat.index}"  
										 value="${varObjRamos.id}"  
										 onchange="onChangeRamo();" 
										 class="js_checkEnable"/>                                                    
									  ${varObjRamos.valor}
									</label>
							 </s:else> 
						 </li>
					 </s:iterator>
				</ul> 	
			</td>
				<th><s:text name="midas.fuerzaventa.configBono.coberturas"></s:text>
			</th>
			<td>
				<ul id="ajax_listaCoberturas" class="w300">
					<s:iterator value="listaCoberturas" var ="varObjLineaCoberturas" status="stat" > 
							<li> 
								<s:if test="#varObjLineaCoberturas.checado==1">
									<label for="varObjLineaCoberturas[%{#stat.index}].id"> 
										 <input type="checkbox" 
										 name="listaCoberturasSeleccionadas[${stat.index}].idCobertura"  
										 id="listaCoberturasSeleccionadas${stat.index}"  
										 value="${varObjLineaCoberturas.id}"  
										 checked="checked"
										 class="js_checkEnable"/>                                                    
								  ${varObjLineaCoberturas.valor}
							   </label> 
							   </s:if>
							   <s:else>
							   <label for="varObjLineaCoberturas[%{#stat.index}].id"> 
								 <input type="checkbox"
									 name="listaCoberturasSeleccionadas[${stat.index}].idCobertura"  
									 id="listaCoberturasSeleccionadas${stat.index}"  
									 value="${varObjLineaCoberturas.id}"  
									 class="js_checkEnable"/>                                                    
								  ${varObjLineaCoberturas.valor}
								</label>
							 </s:else> 
						 </li>
					 </s:iterator>
				</ul> 
			</td>
		</tr>
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>