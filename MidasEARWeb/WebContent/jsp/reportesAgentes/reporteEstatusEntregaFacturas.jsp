<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"/>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"/>
<script type="text/javascript" src="<s:url value='/js/midas2/agentes/reporteEstatusEntregaFacturas.js'/>" ></script>
<script type="text/javascript">

	var urlMostrarContenedorAgente = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/agente"/>';
	var urlObtenerAgente = '<s:url action="obtenerAgente" namespace="/fuerzaventa/reportePreviewBonos"/>';
	var urlObtenerAgenteCargos = '<s:url action="obtenerAgente" namespace="/cargos/cargosAgentes"/>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteEstatusEntregaFacturas"/>';
	
</script>
<div class="row">
	<div class="titulo c11">
		<label><s:text name="midas.fuerzaventa.reporteEstatusEntregaFacturas.title"/></label>
	</div>
</div>
<s:form name="exportarToPDF" id="formReporteEstatusEntrefaFactura">
	<table style="width: 98%" class="contenedorFormas" align="center">
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.reporteEstatusEntregaFacturas.fechaCorteInicio"/>
				<s:hidden name="fechaBase" id="fechaBase"/>
			</td>
			<td>
				<sj:datepicker name="datos.fechaInicio" id="fechaInicio"
					cssStyle="width: 170px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif"
					changeMonth="true" changeYear="true" maxlength="6"
					cssClass="txtfield jQrequired" size="12"
					minDate = "fechaMinFiltro"
					onkeypress="return soloFecha(this, event, false);"
					onchange="validaFechasInicioFin()"
					dateFormat="yymm"
					displayFormat="yymm"/>
					<s:hidden name="datosFechaInicio" id="datosFechaIni"/>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.reporteEstatusEntregaFacturas.fechaCorteFin"/>
			</td>
			<td>
				<sj:datepicker name="datos.fechaFin" id="fechaFin" cssStyle="width: 170px;"
					required="#requiredField" buttonImage="../img/b_calendario.gif" 
					changeMonth="true"
					changeYear="true" maxlength="6" cssClass="txtfield jQrequired" size="12"
					minDate = "fechaMinFiltro"
					onkeypress="return soloFecha(this, event, false);"
					onchange="validaFechasInicioFin()"
					dateFormat="yymm"
					displayFormat="yymm"/>
					<s:hidden name="datosfechaFinal" id="datosFechaFin"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"/>
			</td>
			<td>
				<s:select name="datos.centroOperacionId" id="centroOperacion" 
					cssClass="cajaTextoM2 w250" list="centroOperacionList" 
					listKey="id" listValue="descripcion"
					headerKey="" headerValue="Seleccione.." 
					onchange="loadGerenciasByCentroOperacion();"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia"/>
			</td>
			<td>
				<s:select name="datos.gerenciaId"  id="gerenciaList"	
					list="#{}" headerKey="" headerValue="Seleccione.."
					cssClass="w250 cajaTextoM2" onchange="loadEjecutivoByGerencia();"/>

			</td>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.reporteEstatusEntregaFacturas.ejecutivoVentas" />
			</td>
			<td>
				<s:select name="datos.ejecutivoId" id="ejecutivoList" 
					list="#{}"
					headerKey="" headerValue="Seleccione.."
					cssClass="w250 cajaTextoM2" onchange="loadPromotoriaByEjecutivo();"/>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.promotoria"/>
			</td>
			<td>
				<s:select name="datos.promotoriaId" id="promorotiaList"
					list="#{}" headerKey="" headerValue="Seleccione.."
					cssClass="w250 cajaTextoM2"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</td>	
			<td>
				<s:textfield  name="datos.idAgente" id="idAgente" 
					cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" readonly="readOnly" 
					onchange="onChangeIdAgente();"/>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar" onclick="mostrarListadoAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onChange="onChangeAgente();"/>
					<s:hidden id="id" cssClass="cajaTextoM2 w50" name="idAgente"/>
			</td>
		</tr>	
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td colspan="3">
				<s:textfield id="nombreAgente" name="datos.nombreAgente" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.reporteEstatusEntregaFacturas.estatusFactura"/>
			</td>
			<td colspan="3">
			<s:iterator value="listStatus" var="estatus" status="stat">
				<div style="width: 120px; float:left">
					<div style="width: 25px;float:left">
						<s:checkbox name="datos.listEstatus[%{#stat.index}]" id="listStatus%{#stat.index}" value="0"/>
					</div><label><s:property value='%{#estatus}' /></label>
				</div>
			</s:iterator>
			</td>
		</tr>
	</table>
</s:form>
<div class="row">
	<div class="c2" style="float: right;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class="" onclick="validaExporTo();"> 
				<s:text	name="Exportar" /> 
			</a>
		</div>
	</div>
</div>