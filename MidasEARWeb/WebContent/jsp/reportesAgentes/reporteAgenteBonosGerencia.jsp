<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteBonosGerencia"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteBonosGerencia"></s:url>';
	jQuery(document).ready(
			function() {
				jQuery("#s_fechas").remove();
				listadoService.getMapYears(16, function(data) {
					addOptionsHeaderAndSelect("anios", data, null, "",
							"Seleccione...");
				});
				listadoService.getMapMonths(function(data) {
				addOptionsHeaderAndSelect("mes", data, null, "", "Seleccione...");
				});
				listadoService.getMapMonths(function(data) {
				addOptionsHeaderAndSelect("mesFin", data, null, "", "Seleccione...");
				});
			});
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="label">Reporte de Bonos por
			Gerencia</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/" id="exportarToPDF">
	<table class="contenedorFormas">
<!-- 		<tr id="anio"> -->
<%-- 			<th><s:text name="midas.reporteAgente.topAgente.anio"></s:text> --%>
<!-- 			</th> -->
<%-- 			<td><select id="anios" name="anio" --%>
<%-- 				class="txtfield w200 jQrequired"></select></td> --%>
<!-- 		</tr> -->
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.anio"></s:text>
			</th>
			<td>
				<select id="anios" name="anio" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.inicio"/>
			</th>
			<td>
				<select id="mes" name="mes" class="txtfield w200 jQrequired"></select>
			</td>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.fin"/>
			</th>
			<td>
				<select id="mesFin" name="mesFin" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<!-- 
		<tr>
			<th>Rango de Agentes</th>
		</tr>
		 -->
	</table>
	<jsp:include page="reporteAgenteAllFiltersForm.jsp"></jsp:include>
</s:form>
<div class="row">
	<div class="c2" style="float: right;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class=""
				onclick="validaPeriodoReportes();"> <s:text
					name="Exportar" /> </a>
		</div>
	</div>
</div>