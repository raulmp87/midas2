<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript">
	jQuery( document ).ready(function(){
	jQuery("input").removeClass("jQrequired");
});
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteDatos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteDatos"></s:url>';
	jQuery("#s_Agente").hide();
	//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Datos de Agente</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/"
	id="exportarToPDF">
<jsp:include page="reporteAgenteAllFiltersForm.jsp"></jsp:include>
<table class="contenedorFormas">
<tr>
			<th>Rango de Agentes</th>
		</tr>
		<tr>
			<th>Del</th>
			<td><s:textfield name="rangoInicio"
					cssClass="txtfield w200 jQnumeric jQrestrict"></s:textfield>
			</td>
			<th>Al</th>
			<td><s:textfield name="rangoFin"
					cssClass="txtfield w200 jQnumeric jQrestrict"></s:textfield>
			</td>
		</tr>
</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>
<script type="text/javascript">
<!--
	jQuery("#s_ClasificacionAgente").hide();
//-->
</script>