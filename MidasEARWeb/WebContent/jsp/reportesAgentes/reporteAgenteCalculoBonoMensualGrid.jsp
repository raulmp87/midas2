<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>			
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>50</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="selected" type="ch" width="30" sort="int" >#master_checkbox</column>		
		<column id="numeroAgente" type="ro" width="70" sort="int" align="center"><s:text name="No. Agente" /></column>
		<column id="nombreAgente" type="ro" width="*" sort="int" align="center"><s:text name="Nombre del Agente" /></column>		
		<column id="ver" type="img" width="30" align="center"></column>				
	</head>	  		
	<s:iterator value="listaAgentes" status="stats">
		<row id="<s:property value="id"/>">
		    <cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>				
			<cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true"/></cell>			
			<cell>/MidasWeb/img/logoPDF_small.png^Descargar Reporte^javascript: descargarReporte(<s:property value="id"/>)^_self</cell>			
		</row>
	</s:iterator>
</rows>