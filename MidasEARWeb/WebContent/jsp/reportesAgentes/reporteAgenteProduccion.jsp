<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript">
<!--
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteProduccion"></s:url>';
//-->
</script>

<div class="row">
	<div class="titulo c5">
		<label>Reporte de Producción</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteProduccion/">
	<table class="contenedorFormas">
		<tr>
			<td>
				Fecha Inicio
			</td>
			<td>
				<sj:datepicker name="fechaInicio" id="fechaInicio" buttonImage="../img/b_calendario.gif"			 
				   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 								   								  
				   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   maxDate="today"
	 	 		   	onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				Fecha Fin
			</td>
			<td>
				<sj:datepicker name="fechaFin" id="fechaFinal" buttonImage="../img/b_calendario.gif"			 
				   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 	
				   	maxDate="today"							   								  
				   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 	 		   	onblur="esFechaValida(this);"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="Tipo de salida del archivo"></s:text>
			</td>
			<td>
				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly"
						list="#{'xls':'Excel','txt':'Texto'}"/>
			</td>
		</tr>
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>