<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
	<table class="contenedorFormas">
		<tr id="s_fechas">
			<th> <s:property value="labelFechaInicio"/>
			</th>
			<td><sj:datepicker name="fechaInicial"
					cssStyle="width: 170px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaInicio"
					maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			<th><s:property value="labelFechaFin"/>
			</th>
			<td><sj:datepicker name="fechaFinal" cssStyle="width: 170px;"
					required="#requiredField" buttonImage="../img/b_calendario.gif"
					id="fechaFinal" maxDate="today" changeMonth="true"
					changeYear="true" maxlength="10" cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.titulo" /></th>
			<td><s:select id="centroOperacionList" headerKey=""
					headerValue="Seleccione..." list="centroOperacionList" listKey="id"
					listValue="descripcion" name="idCentroOperacion"
					cssClass="txtfield w200"
					onchange="loadGerenciasByCentroOperacion(this.value, this.id,'ejecutivoList');" size="6"></s:select>
			</td>
			<th><s:text name="midas.catalogos.centro.operacion.gerencias" />
			</th>
			<td><s:select id="gerenciaList" headerKey=""
					headerValue="Seleccione..." list="gerenciasSeleccionadas"
					listKey="id" listValue="descripcion" name="idGerencia"
					cssClass="txtfield w200"
					onchange="loadEjecutivoByGerencia(this.value, this.id);" size="6"></s:select>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text>
			</th>
			<td><s:select id="ejecutivoList" headerKey=""
					headerValue="Seleccione..." list="ejecutivosSeleccionados"
					listKey="id" listValue="personaResponsable.nombreCompleto"
					name="idEjecutivo" cssClass="txtfield w200"
					onchange="loadPromotoriaByEjecutivo(this.value, this.id);" size="6"></s:select>
			</td>
			<th><s:text name="midas.prestamosAnticipos.promotorias"></s:text>
			</th>
			<td><s:select id="promotoriaList" headerKey=""
					headerValue="Seleccione..." list="promotoriasSeleccionadas"
					listKey="id" listValue="descripcion" name="idPromotoria"
					cssClass="txtfield w200" size="6"></s:select>
			</td>
		</tr>
		<tr id="s_Agente">
			<th><s:text name="midas.excepcion.suscripcion.auto.agente"></s:text>
				<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
					onchange="addAgenteProduccion();" style="display:none;" /></th>
			<td>
				<ul id="listaProduccionAgentes" class="w300"
					style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
				<div id="b_sAgente" class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="pantallaModalBusquedaAgente();"> <s:text
							name="midas.boton.seleccionar" /> </a>
				</div>
			</td>
		</tr>
		<tr id="s_ClasificacionAgente">
			<th>
				<s:text name="midas.fuerzaventa.negocio.clasificacionAgente"></s:text>
			</th>
			<td>
				<s:select id="clasificacionAgente" headerKey=""
					headerValue="Seleccione..." list="clasificacionAgente" listKey="id"
					listValue="valor" name="idClasificacionAgente"
					cssClass="txtfield w200"/>
			</td>
		</tr>
	</table>
