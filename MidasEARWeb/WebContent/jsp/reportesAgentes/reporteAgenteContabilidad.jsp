<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript">
<!--
	jQuery("#pdf").css("display","block");
	var exportarToPDFUrl = '<s:url action="exportMizar" namespace="/fuerzaventa/reporteAgenteContabilidad"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteContabilidad"></s:url>';
	jQuery(document).ready(
			function() {
				jQuery("#s_fechas").remove();
				listadoService.getMapMonths(function(data) {
					addOptionsHeaderAndSelect("meses", data, null, "",
							"Seleccione...");
				});
				listadoService.getMapYears(16, function(data) {
					addOptionsHeaderAndSelect("anios", data, null,"",
							"Seleccione...");
				});
// 				listadoService.getMapMonths(function(data) {
// 					addOptionsHeaderAndSelect("mesesFin", data, null,"",
// 							"Seleccione...");
// 				});
// 				listadoService.getMapYears(16, function(data) {
// 					addOptionsHeaderAndSelect("aniosFin", data,null,"",
// 							"Seleccione...");
// 				});
			});
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="label">Reporte Contabilidad Midas vs Mizar</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/"
	id="exportarToPDF">
	<table class="contenedorFormas">
		<tr>
			<th>Periodo</th>
		</tr>
		<tr>
			<th><s:text name="midas.reporteAgente.topAgente.anio.inicio"></s:text>
			</th>
			<td><select id="anios" name="anio"
				class="txtfield w200 jQrequired" ></select></td><!-- onchange="selectFin(this.value);" -->
			<th><s:text name="midas.reporteAgente.topAgente.mes.inicio"></s:text></th>
			<td><select id="meses" name="mes"
				class="txtfield w200 jQrequired"></select></td>
		</tr>
<!-- 		<tr> -->
<%-- 			<th><s:text name="midas.reporteAgente.topAgente.anio.fin"></s:text> --%>
<!-- 			</th> -->
<%-- 			<td><select id="aniosFin" name="anioFin" --%>
<%-- 				class="txtfield w200"></select></td> --%>
<%-- 			<th><s:text name="midas.reporteAgente.topAgente.mes.fin"></s:text></th> --%>
<%-- 			<td><select id="mesesFin" name="mesFin" --%>
<%-- 				class="txtfield w200"></select></td> --%>
<!-- 		</tr> -->
	</table>
	<jsp:include page="reporteAgenteAllFiltersForm.jsp"></jsp:include>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>
<!-- <div class="row"> -->
<!-- 	<div class="c2" style="float: right;"> -->
<!-- 		<div class="btn_back w150"> -->
<!-- 			<a href="javascript: void(0);" class="icon_excel" -->
<%-- 				onclick="exportTo('excel');"> <s:text --%>
<!-- 					name="Midas" /> </a> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- 	<div class="c1" id="pdf"> -->
<!-- 		<div class="btn_back w150"> -->
<!-- 			<a href="javascript: void(0);" class="icon_excel" -->
<%-- 				onclick="exportTo('pdf');"> <s:text --%>
<!-- 					name="Mizar" /> </a> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->