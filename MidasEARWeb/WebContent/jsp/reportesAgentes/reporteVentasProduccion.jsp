<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
	<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteVentasProduccion.js'/>"></script>
<script type="text/javascript">
<!--
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteVenteasProduccion"></s:url>';
	jQuery(document).ready(
			function() {
				listadoService.getMapYears(2014, 10,function(data) {
					addOptionsHeaderAndSelect("anios", data, null, "",
							"Seleccione...");
				});
				listadoService.getMapMonths(function(data) {
				addOptionsHeaderAndSelect("mes", data, null, "", "Seleccione...");
				});
				listadoService.getMapMonths(function(data) {
				addOptionsHeaderAndSelect("mesFin", data, null, "", "Seleccione...");
				});
			});
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte  Ventas de Producción</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteAgente/" id="exportarToPDF">
	<table class="contenedorFormas">
		<tr id="anio">
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.anio"></s:text>
			</th>
			<td>
				<select id="anios" name="anio" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.inicio"/>
			</th>
			<td>
				<select id="mes" name="mes" class="txtfield w200 jQrequired"></select>
			</td>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes.fin"/>
			</th>
			<td>
				<select id="mesFin" name="mesFin" class="txtfield w200 jQrequired"></select>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.fuerzaventa.reportesAgentes.crecimiento"></s:text>
			</th>
			<td>
				<s:select id="crecimiento" headerKey=""
					name="crecimiento"
					headerValue="Seleccione..." 
					list="#{'1':'Global', '2':'Mes'}"
					cssClass="txtfield w200 jQrequired"/>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.titulo" /></th>
			<td><s:select id="centroOperacionList" headerKey=""
					headerValue="Seleccione..." list="centroOperacionList" listKey="id"
					listValue="descripcion" name="idCentroOperacion"
					cssClass="txtfield w200"
					onchange="loadGerenciasByCentroOperacion(this.value, this.id,'ejecutivoList');" size="6"></s:select>
			</td>
			<th><s:text name="midas.catalogos.centro.operacion.gerencias" />
			</th>
			<td><s:select id="gerenciaList" headerKey=""
					headerValue="Seleccione..." list="gerenciasSeleccionadas"
					listKey="id" listValue="descripcion" name="idGerencia"
					cssClass="txtfield w200"
					onchange="loadEjecutivoByGerencia(this.value, this.id);" size="6"></s:select>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text>
			</th>
			<td><s:select id="ejecutivoList" headerKey=""
					headerValue="Seleccione..." list="ejecutivosSeleccionados"
					listKey="id" listValue="personaResponsable.nombreCompleto"
					name="idEjecutivo" cssClass="txtfield w200"
					onchange="loadPromotoriaByEjecutivo(this.value, this.id);" size="6"></s:select>
			</td>
			<th><s:text name="midas.prestamosAnticipos.promotorias"></s:text>
			</th>
			<td><s:select id="promotoriaList" headerKey=""
					headerValue="Seleccione..." list="promotoriasSeleccionadas"
					listKey="id" listValue="descripcion" name="idPromotoria"
					cssClass="txtfield w200" size="6"></s:select>
			</td>
		</tr>
		<tr id="s_Agente">
			<th>
				<s:text name="midas.prestamosAnticipos.numeroAgente" />
			</th>	
			<td>
				<s:textfield  name="agente.idAgente" id="idAgente" cssClass="cajaTextoM2 w250 jQnumeric jQrestrict" readonly="readOnly" onchange="onChangeIdAgente();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
					<s:textfield id="txtId" name ="txtIdAgente" cssClass="cajaTextoM2 w50" cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none" cssClass="cajaTextoM2 w50" name="agente.id"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.nombreAgente" />
			</td>
			<td>
				<s:textfield id="nombreAgente" name="agente.persona.nombreCompleto" readonly="true" cssClass="cajaTextoM2 w160" />
			</td>
		</tr>
	</table>
</s:form>
<div class="row">
	<div class="c2" style="float: right;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class=""
				onclick="validaPeriodoReportes();"> <s:text
					name="Exportar" /> </a>
		</div>
	</div>
</div>