<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/SaldosVsContabilidad"></s:url>';
//-->
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte  saldos VS Contabilidad</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/SaldosVsContabilidad/"
	id="exportarToPDF">
<table class="contenedorFormas">
	<tr>
		<th> 
			<s:property value="labelFechaFinCorte"/>
		</th>
		<td>
			<sj:datepicker name="fechaCorte"
				cssStyle="width: 170px;" required="#requiredField"
				buttonImage="../img/b_calendario.gif" id="fechaInicio"
				maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
				cssClass="txtfield jQrequired" size="12"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>
		</td>
		<th>
			<s:text name="Tipo de salida del archivo"></s:text>
		</th>
		<td>
			<s:select name="tipoSalidaArchivo" cssClass="cajaTextoM2 w150" disabled="#readOnly"
					list="#{'xlsx':'Excel','txt':'Texto'}"/>
		</td>
	</tr>
</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>