<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>"></script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteTopAgente"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteAgenteTopAgente"></s:url>';
	//-->
	jQuery(document).ready(
			function() {
				listadoService.getMapMonths(function(data) {
					addOptionsHeaderAndSelect("meses", data, null, "",
							"Seleccione...");
				})
				listadoService.getMapYears(5, function(data) {
					addOptionsHeaderAndSelect("anios", data, null, "",
							"Seleccione...");
				});
			});
</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Top Agente</label>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteBono/"
	id="exportarToPDF">
	<table class="contenedorFormas">

		<tr>
			<th>Top Agente</th>
			<td><s:textfield id="topAgente" name="topAgente" cssClass="cajaTextoM2 jQnumeric jQrestrict jQrequired"></s:textfield></td>
		</tr>
<!-- 		<tr> -->
<!-- 		<td> -->
<!-- 				Fecha Inicio -->
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<sj:datepicker name="fechaInicio" id="fechaInicio" buttonImage="../img/b_calendario.gif"			  --%>
<!-- 				   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 								   								   -->
<!-- 				   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" -->
<!-- 				   maxDate="today" -->
<%-- 	 	 		   	onblur="esFechaValida(this);"></sj:datepicker> --%>
<!-- 			</td> -->
<!-- 			<td> -->
<!-- 				Fecha Fin -->
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 				<sj:datepicker name="fechaFin" id="fechaFinal" buttonImage="../img/b_calendario.gif"			  --%>
<!-- 				   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 	 -->
<!-- 				   	maxDate="today"							   								   -->
<!-- 				   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" -->
<%-- 	 	 		   	onblur="esFechaValida(this);"></sj:datepicker> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<tr>
			<th><s:text name="midas.reporteAgente.topAgente.anio"></s:text>
			</th>
			<td>
				<div>
					<select id="anios" name="anio" class="cajaTextoM2 "></select>
				</div></td>
			<th><s:text name="midas.reporteAgente.topAgente.mes"></s:text></th>
			<td>
				<div>
					<select id="meses" name="mes" class="cajaTextoM2 "></select>
				</div></td>
		</tr>
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.titulo" /></th>
			<td><s:select id="centroOperacionList" headerKey="" headerValue="Seleccione..."
					 list="centroOperacionList" listKey="id"
					listValue="descripcion" name="idCentroOperacion"
					cssClass="cajaTextoM2 w200"
					onchange="loadGerenciasByCentroOperacion(this.value)" size="6"></s:select>
			</td>
			<th><s:text name="midas.catalogos.centro.operacion.gerencias" />
			</th>
			<td><s:select id="gerenciaList" 
					 list="gerenciasSeleccionadas"
					listKey="id" listValue="descripcion" name="idGerencia"
					cssClass="cajaTextoM2 w200"
					onchange="loadEjecutivoByGerencia(this.value)" size="6"></s:select>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text>
			</th>
			<td><s:select id="ejecutivoList" 
					 list="ejecutivosSeleccionados"
					listKey="id" listValue="personaResponsable.nombreCompleto"
					name="idEjecutivo" cssClass="cajaTextoM2 w200"
					onchange="loadPromotoriaByEjecutivo(this.value)" size="6"></s:select>
			</td>
			<th><s:text name="midas.prestamosAnticipos.promotorias"></s:text>
			</th>
			<td><s:select id="promotoriaList" 
					 list="promotoriasSeleccionadas"
					listKey="id" listValue="descripcion" name="idPromotoria"
					cssClass="cajaTextoM2 w200" size="6"></s:select>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.excepcion.suscripcion.auto.agente"></s:text>
				<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
					onchange="addAgenteProduccion();" style="display:none;" /></th>
			<td>
				<ul id="listaProduccionAgentes" class="w300"
					style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
				<div class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="pantallaModalBusquedaAgente();"> <s:text
							name="midas.boton.seleccionar" /> </a>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="Tipo de salida del archivo"></s:text>
			</td>
			<td>
				<s:select name="tipoSalidaArchivo"  id="horario" cssClass="cajaTextoM2 w150" disabled="#readOnly"
						list="#{'xlsx':'Excel','txt':'Texto'}"/>
			</td>
		</tr>
	</table>
</s:form>
<jsp:include page="reporteAgenteFooter.jsp"></jsp:include>