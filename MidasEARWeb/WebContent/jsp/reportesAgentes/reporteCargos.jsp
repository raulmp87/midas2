<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">
</script>
<script type="text/javascript">
<!--
	var exportarToPDFUrl = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteCargos"></s:url>';
	var exportarToExcelUrl = '<s:url action="exportarToExcel" namespace="/fuerzaventa/reporteCargos"></s:url>';
//-->
</script>

</script>
<div class="row">
	<div class="titulo c5">
		<label class="">Filtros de busqueda Reporte Cargos</label>
	</div>
</div>
<div class="row">
	<div class="btn_back w130" style="display: inline; float: right;">
		<a href="javascript: void(0);" class="icon_limpiar"
			onclick="limpiarForm('exportarToPDF');"> <s:text
				name="midas.suscripcion.cotizacion.limpiar" /> </a>
	</div>
</div>
<s:form action="exportarToPDF" namespace="/fuerzaventa/reporteCargos/">
	<table  width="98%" class="contenedorFormas" align="center">
		<tr>
			<td colspan="4">
				<div align="right">
					<s:text name="midas.cargos.reporte.tipoReporte"></s:text>
					<s:radio theme="simple" name="filtroCargos.tipoReporte"
							list="#{0:'Detallado',1:'General'}"></s:radio>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.cargos.gridDetalle.tipoCargo"></s:text>
			</td>
			<td>
				<s:select  name="filtroCargos.tipoCargo.id" id="" cssClass="cajaTextoM2 w150" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					list="listaTiposCargos" listKey="id" listValue="valor" disabled="#readOnly"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.cargos.reporte.fechaAltaInicio"></s:text>
			</td>
			<td>
				<sj:datepicker name="filtroCargos.fechaInicioCoBro" id="" buttonImage="../img/b_calendario.gif"			 
			   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
			   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 		   	onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<s:text name="midas.cargos.reporte.fechaAltaFin"></s:text>
			</td>
			<td>
				<sj:datepicker name="filtroCargos.fechaFinCobro" id="" buttonImage="../img/b_calendario.gif"			 
			   	changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
			   	onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 		   	onblur="esFechaValida(this);"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.centroOperacion"></s:text>
			</td>
			<td>
				<ul id="operacionesList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="centroOperacionList"
						var="varListaCentroOperacion" status="stat">
						<li><label for="varListaCentroOperacion[%{#stat.index}].id">
								<input type="checkbox" onchange=""
								onclick="loadGerenciasByCentroOperacion();hiddenList('operacionesList');"
								name="centroOperacionesSeleccionados[${stat.index}].id"
								id="centroOperacionesSeleccionados${stat.index}"
								value="${varListaCentroOperacion.id}" class="js_checkEnable" />
								${varListaCentroOperacion.descripcion} </label>
							</li>
					</s:iterator>
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.gerencia"></s:text>
			</td>
			<td>
				<ul id="gerenciaList" class="w300"
					style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.prestamosAnticipos.ejecutivo"></s:text> de Ventas
			</td>
			<td>
				<ul id="ejecutivoList" class="w300"
						style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.promotoria"></s:text>
			</td>
			<td>
				<ul id="promorotiaList" class="w300"
						style="height: 95px; overflow-y: scroll; overflow-x: hidden;">
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.fuerzaventa.configBono.tipoPromotoria"></s:text>
			</td>
			<td>
				<ul id="tipoPromotoriaList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="tipoPromotoria" var="varListTipoPromotoria"
							status="stat">
						<li>
							<label for="varListTipoPromotoria[%{#stat.index}].id">
								<input type="checkbox"
								name="tipoPromotoriasSeleccioadas[${stat.index}].id"
								id="tipoPromotoriasSeleccioadas${stat.index}"
								value="${varListTipoPromotoria.id}" class="js_checkEnable" />
								${varListTipoPromotoria.valor} </label>
						</li>
					</s:iterator>
				</ul>
			</td>
			<td>
				<s:text name="midas.prestamosAnticipos.tipoAgente"></s:text>
			</td>
			<td>
				<ul id="tipoAgenteList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="tipoAgente" var="varListTipoAgente"
							status="stat">
							<li>
								<label for="varListTipoAgente[%{#stat.index}].id">
								<input type="checkbox"
								name="tipoAgentesSeleccionados[${stat.index}].id"
								id="tipoAgentesSeleccionados${stat.index}"
								value="${varListTipoAgente.id}" class="js_checkEnable" />
								${varListTipoAgente.valor} </label>
						</li>
					</s:iterator>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.cargos.gridDetalle.estatus"></s:text>
			</td>
			<td>
				<ul id="estatusCargosList" class="w300"
						style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						<s:iterator value="estatusCargos" var="varListEstatus"
							status="stat">
							<li><label for="varListTipoPromotoria[%{#stat.index}].id">
									<input type="checkbox"
									name="estatusCargos[${stat.index}].id"
									id="estatusCargos${stat.index}"
									value="${varListEstatus.id}" class="js_checkEnable" />
									${varListEstatus.valor} </label>
							</li>
						</s:iterator>
					</ul>
			</td>
			<td>
				<s:text name="midas.fuerzaventa.negocio.tipoCedula"></s:text>
			</td>
			<td>
				<s:select name="tipoCedulaSeleccionada" list="tipoCedula"
				headerKey="" headerValue="Seleccione.." listKey="id"
				listValue="valor" cssClass="w350 cajaTextoM2"></s:select>
			</td>
		</tr>
	</table>
</s:form>

<div class="row">
	<div class="c2" style="margin-left: 490px;">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class="icon_excel"
				onclick="exportTo('excel');"> <s:text
					name="midas.boton.exportarExcel" /> </a>
		</div>
	</div>
	<div class="c1">
		<div class="btn_back w150">
			<a href="javascript: void(0);" class="icon_adjuntarPDF"
				onclick="exportTo('pdf');"> <s:text
					name="midas.boton.exportarPDF" /> </a>
		</div>
	</div>
</div>