<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<%-- <s:include value="/jsp/calculos/generarDocumentosHeader.jsp"></s:include> --%>
<s:include value="/jsp/reportesAgentes/reporteAgenteCalculoBonoMensualHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">	
</script>
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
   
   .wwgrp {
    padding: 4px 5px;
    padding-left: 0px;}
   
</style> 

<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Calculo Bono Mensual</label>
	</div>
</div>
    
<s:form id="generarDocumentosForm">
<s:hidden id="tipoUsuario" name="tipoUsuario" ></s:hidden>
<s:hidden id="idAgenteUsuario" name="idAgenteUsuario" ></s:hidden>
<s:hidden id="idPromotoria" name="idPromotoria" ></s:hidden>
	<table class="contenedorFormas">    
		
		<tr>
		    <th><font color="#FF6600">* </font><s:text name="Mes:"></s:text>
			</th>
			<td width="250px;">
			    <select id="meses" name="mes" class="txtField w200" ></select>			   
			</td>		
			<td width="80px;"></td>	
			<th><font color="#FF6600">* </font><s:text name="A�o:"></s:text>
			</th>
			<td width="250px;">
			    <select id="anios" name="anio" class="txtField w200" ></select>
			</td>			
		</tr>
		<s:if test="tipoUsuario == 'ADMIN' || tipoUsuario == 'PROMOTOR'">
			<tr>
		    <th width="80px;"><font color="#FF6600">* </font><s:property value="labelFechaInicio" default="Tipo de Beneficiario:"/>
				</th>
				<td width="250px;">			    
				    <s:select name="tipoBeneficiario" id="tiposBeneficiarios"					      
						      labelposition="left" required="true" 
						      onchange="ocultarMostrarComponentes();"
						      headerKey="" headerValue="Seleccione ..." 
						      list="tiposBeneficiario"
						      listKey="id" listValue="valor" 
						      cssClass=" txtfield w200"/>
	<%-- 					      					      	    --%>
					         
				</td>		   
		    </tr>
		    <%-- agregar componente agentes y seleccionar opcion por default --%>
	    </s:if>
	    <s:if test="tipoUsuario == 'ADMIN'">
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.titulo" />:</th>
			<td width="250px;">
				<ul id="operacionesList" class="w250"
					style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="centroOperacionList"
						var="varListaCentroOperacion" status="stat">
						<li><label for="varListaCentroOperacion[%{#stat.index}].id">
								<input type="checkbox" onchange="" 
								onclick="loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');"
								name="centroOperacionesSeleccionados[${stat.index}].id"
								id="centroOperacionesSeleccionados${stat.index}"
								value="${varListaCentroOperacion.id}" class="js_checkEnable" />
								${varListaCentroOperacion.descripcion} </label></li>
					</s:iterator>
				</ul>
			</td>
			<td width="80px;">	
			    <a target="_self" href="javascript: selectAllChecks('operacionesList');loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('operacionesList');loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>                
			</td>			
			<th><s:text name="midas.catalogos.centro.operacion.gerencias" />:
			</th>
			<td width="250px;">
						<ul id="gerenciaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						    <s:iterator value="gerenciasSeleccionadas" var="varListaGerencias" status="stat" >
						           <li>
						                 <label for="varListaGerencias[%{#stat.index}].id">
						                  <input type="checkbox"
						                         onclick="loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');"
						                  		 name="gerenciaLong[${stat.index}]" 
						                  		 id="gerenciasSeleccionadas${stat.index}" 
						                  		 value="${varListaGerencias.id}" 
						                  		 class="js_checkEnable"/>                                                   
						                   ${varListaGerencias.descripcion}
						                 </label>
						          </li>
						    </s:iterator>
						</ul>                     
						<s:iterator value="gerenciasSeleccionadas" status="status">	
								<s:iterator value="listaGerencias" var ="varListaGerencias" status="stat">
									<s:if test="%{#varListaGerencias.id == configuracionBono.listaGerencias[#status.index].gerencia.id}">
										<script type="text/javascript">
											checarChec('gerenciasSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	
					</td>
					<td width="80px;">
					    <a target="_self" href="javascript: selectAllChecks('gerenciaList');loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                </a>		    
				    <a target="_self" href="javascript: deselectAllChecks('gerenciaList');loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');">
	                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
	                </a>
					</td>							
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text>:
			</th>
			<td width="250px;">
				<ul id="ejecutivoList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="ejecutivosSeleccionados" var="varListaEjecutivos"
						status="stat">
						<li>
							<label
							for="varListaEjecutivos[%{#stat.index}].id"> 
							    <input
								type="checkbox" name="ejecutivoLong[${stat.index}]"
								onclick="loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');"
								id="ejecutivosSeleccionados${stat.index}"
								value="${varListaEjecutivos.id}" class="js_checkEnable" />
								${varListaEjecutivos.nombreCompleto} </label></li>
					</s:iterator>
				</ul> <s:iterator value="configuracionBono.listaEjecutivos"
					status="status">
					<s:iterator value="ejecutivosSeleccionados" var="varListaEjecutivos"
						status="stat">
						<s:if
							test="%{#varListaEjecutivos.id == configuracionBono.listaEjecutivos[#status.index].ejecutivo.id}">
							<script type="text/javascript">
								checarChec('ejecutivosSeleccionados${stat.index}');
							</script>
						</s:if>
					</s:iterator>
				</s:iterator></td>
				<td width="80px;">
				    <a target="_self" href="javascript: selectAllChecks('ejecutivoList');loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                </a>		    
				    <a target="_self" href="javascript: deselectAllChecks('ejecutivoList');loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');">
	                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
	                </a>
				</td>
		<th><s:text name="midas.prestamosAnticipos.promotorias"></s:text>:
			</th>
			<td width="250px;">
				<ul id="promotoriaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="promotoriasSeleccionadas" var="varListaPromotorias"
						status="stat">
						<li>
							<label
							for="varListaPromotorias[%{#stat.index}].id"> <input
								type="checkbox" name="promotoriaLong[${stat.index}]"
								id="promotoriasSeleccionadas${stat.index}"
								value="${varListaPromotorias.id}" class="js_checkEnable" />
								${varListaPromotorias.descripcion} </label></li>
					</s:iterator>
				</ul> <s:iterator value="configuracionBono.listaPromotorias"
					status="status">
					<s:iterator value="promotoriasSeleccionadas" var="varListaPromotorias"
						status="stat">
						<s:if
							test="%{#varListaPromotorias.id == configuracionBono.listaPromotorias[#status.index].promotoria.id}">
							<script type="text/javascript">
								checarChec('promotoriasSeleccionadas${stat.index}');
							</script>
						</s:if>
					</s:iterator>
				</s:iterator>
			</td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('promotoriaList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('promotoriaList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
	</tr>
		<tr id="s_Agente">
			<th><s:text name="Tipo Promotoria"></s:text>:
			</th>
		<td width="250px;">
			<ul id="tipoPromotoriaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				<s:iterator value="tipoPromotoria"
					var="varListaTiposPromotoria" status="stat">
					<li><label for="varListaTiposPromotoria[%{#stat.index}].id">
							<input type="checkbox"
							name="tipoPromotoriasSeleccioadas[${stat.index}].id"
							id="tipoPromotoriasSeleccioadas${stat.index}"
							value="${varListaTiposPromotoria.id}" class="js_checkEnable" />
							${varListaTiposPromotoria.valor} </label></li>
				</s:iterator>
			</ul> <s:iterator value="tipoPromotoria"
				status="status">
				<s:iterator value="listaTiposPromotoria"
					var="varListaTiposPromotoria" status="stat">
					<s:if
						test="%{#varListaTiposPromotoria.id == configuracionBono.listaTiposPromotoria[#status.index].id}">
						<script type="text/javascript">
							checarChec('tipoPromotoriasSeleccioadas${stat.index}');
						</script>
					</s:if>
				</s:iterator>
			</s:iterator></td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('tipoPromotoriaList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('tipoPromotoriaList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
		<th class="filtrosAplicablesAgente">
		    <s:text name="midas.fuerzaventa.negocio.clasificacionAgente"></s:text>:
		</th>
		<td width="250px;" class="filtrosAplicablesAgente">
			<ul id="tipoAgenteList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				<s:iterator value="tipoAgente" var="varListaTiposAgente"
					status="stat">
					<li><label for="varListaTiposAgente[%{#stat.index}].id">
							<input type="checkbox"
							name="tipoAgentesSeleccionados[${stat.index}].id"
							id="tipoAgentesSeleccionados${stat.index}"
							value="${varListaTiposAgente.id}" class="js_checkEnable" />
							${varListaTiposAgente.valor} </label></li>
				</s:iterator>
			</ul> <s:iterator value="tipoAgente"
				status="status">
				<s:iterator value="listaTiposAgente" var="varListaTiposAgente"
					status="stat">
					<s:if
						test="%{#varListaTiposAgente.id == configuracionBono.listaTipoAgentes[#status.index].id}">
						<script type="text/javascript">
											checarChec('tipoAgentesSeleccionados${stat.index}');
						</script>
					</s:if>
				</s:iterator>
			</s:iterator></td>
			<td width="80px;" class="filtrosAplicablesAgente">
			    <a target="_self" href="javascript: selectAllChecks('tipoAgenteList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('tipoAgenteList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
	</tr>
	<tr height="100px;" class="filtrosAplicablesAgente">
		<th>
		    <s:text name="midas.fuerzaventa.configBono.agente" />:
		    <s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
					onchange="addAgenteProduccion();" style="display:none;" />
		</th>
		<td>
			<div>				
				<input type="checkbox" name="chkTodosAgentes" value="true" id="chkTodosAgentes"/>Todos	
				<div id="b_sAgente" class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="pantallaModalBusquedaAgente();"> <s:text
							name="midas.boton.seleccionar" /> </a>
				</div>
			</div>
		</td>
		<td colspan="3" align="right">
			<ul id="listaProduccionAgentes" class="w420 h100"
				style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
			</ul></td>
	</tr>
	</s:if>
	<s:if test="(tipoUsuario == 'PROMOTOR' && tipoBeneficiario == 839)">
		<tr>
			<th><s:text name="Agentes" />:</th>
			<td width="250px;">
				<ul id="agentesPromoList" class="w250"
					style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="agentesPromoList"
						var="varAgentesPromoList" status="stat">
						<li><label for="varAgentesPromoList[%{#stat.index}].id">
								<input type="checkbox" onchange=""								
								name="agenteList[${stat.index}].id" 
								id="agenteList${stat.index}"
								value="${varAgentesPromoList.id}" class="js_checkEnable" />
								${varAgentesPromoList.persona.nombreCompleto} </label></li>
					</s:iterator>
				</ul>
			</td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('agentesPromoList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('agentesPromoList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
	</s:if>
<%-- 	<s:if test="tipoUsuario == 'ADMIN' || (tipoUsuario == 'PROMOTOR' && tipoBeneficiario == 839)"> --%>
		<tr height="40px;" class="btnBuscar">
		    <td colspan="6" align="right" valign="bottom">
		        <div class="btn_back w150">
				<a href="javascript: void(0);"
					onclick="buscarAgentesParaReporte(1);">
					<s:text name="Buscar"/>
				</a>
			</div>
		    </td>
		</tr>
<%-- 	</s:if>	 --%>
<%-- 	<s:if test="tipoUsuario == 'AGENTE' || (tipoUsuario == 'PROMOTOR' && tipoBeneficiario == 838)"> --%>
		<tr height="40px;" class="btnDescargar">
		    <td colspan="6" align="right" valign="bottom">
		        <div class="btn_back w150">
				<a href="javascript: void(0);"
					onclick="descargarReporte(0);">
					<s:text name="Descargar Reporte"/>
				</a>
			</div>
		    </td>
		</tr>
<%-- 	</s:if> --%>
</table>
<div id="spacer1" style="height: 10px"></div>
</s:form>
<div id="spacer1" style="height: 10px"></div>
<table style="width: 98%;">
	<s:if test="tipoUsuario == 'ADMIN' || (tipoUsuario == 'PROMOTOR' && tipoBeneficiario == 839)">
    <tr>
        <td>
            <div id="tablaAgentesDocumentos" class="detalle" style="width: 98%;">			    
			    <div id="indicador"></div>
					<div id="gridAgentesDocumentosPaginado">
						<div id="agentesDocumentosGrid" style="height:220px"></div>
				        <div id="pagingArea"></div><div id="infoArea"></div>
			    </div>
		    </div>
        </td>
    </tr>
    <tr height="40px;" class="btnEnvCorreo">
	    <td colspan="6" align="center" valign="bottom">
	        <div class="btn_back w150">
			<a href="javascript: void(0);"
				onclick="enviarRepCalculoBonoMensualPorCorreo();">
				<s:text name="Enviar Por Correo"/>
			</a>
		</div>
	    </td>
	</tr>
	</s:if>
</table>
<script type="text/javascript">
//buscarAgentesParaGeneracionDocs(0);
</script>