<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>	
<script src="<s:url value='/js/midas2/agentes/reporteAgenteCalculoBonoMensual.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	<!-- TODO agregar actions-->
	var buscarAgentesReportePath = '<s:url action="buscarAgentes" namespace="/fuerzaventa/reporteAgenteCalculoBonoMensual"/>';
	var descargarReportePath = '<s:url action="exportarToPDF" namespace="/fuerzaventa/reporteAgenteCalculoBonoMensual"/>';
	var enviarReportePath = '<s:url action="enviarDocumentos" namespace="/fuerzaventa/reporteAgenteCalculoBonoMensual"/>';	

</script>