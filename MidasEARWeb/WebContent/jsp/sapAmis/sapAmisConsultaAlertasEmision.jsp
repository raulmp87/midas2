<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">
	<s:form  id="clienteForm" name="clienteForm">
	
		<!-- Parametro de la forma para que sea reutilizable -->
		<s:hidden name="tipoAccion"></s:hidden>
		<s:hidden name="tipoRegreso"  id="tipoRegreso"/>
		<s:hidden name="idNegocio"  id="idNegocio"/>
		<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
		<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
		<s:hidden name="idField"></s:hidden>
		<s:hidden name="divCarga"></s:hidden>
		<table width="890px" id="filtrosM2" cellpadding="0" cellspacing="0" >
			<tr>
				<td class="titulo" colspan="4">Consulta Alertas en Linea</td>
			</tr>
			<tr class="pf">
				<th width="105px"  >				
					Numero de serie <s:textfield name="consultaNuemeroSerie" id="consultaNuemeroSerie" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="50;"/>
				</th>
				<!--  	
				<td width="315px">
					<s:textfield name="bitacoraPoliza" id="bitacoraPoliza" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="50;"/>
				</td>
				-->
			</tr>
			<tr>
				<td colspan="6" align="right">				
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar" 
						onclick="javascript: consultarAlertasLinea( jQuery('#consultaNuemeroSerie').val());">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>				 
				</td>
			</tr>	
		</table>
		<br>
	</s:form>
	
	<div id="alertasGrid" style="width:800px;height:190px"></div>
	<script type="text/javascript">
		initConsultaLineaGrid();
	</script>
	
	
</div>
<div id="spacer2" style="height: 40px"></div>


