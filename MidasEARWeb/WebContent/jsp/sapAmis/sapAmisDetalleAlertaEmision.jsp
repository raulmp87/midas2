<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.td{
	max-width: 150px;
}
</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">
	<s:form  id="clienteForm" name="clienteForm">
	
		<!-- Parametro de la forma para que sea reutilizable -->
		<s:hidden name="tipoAccion"></s:hidden>
		<s:hidden name="tipoRegreso"  id="tipoRegreso"/>
		<s:hidden name="idNegocio"  id="idNegocio"/>
		<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
		<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
		<s:hidden name="idField"></s:hidden>
		<s:hidden name="divCarga"></s:hidden>
		<table width="890px" id="filtrosM2" cellpadding="0" cellspacing="0" >
			<tr>
				<td class="titulo td" colspan="4">Consulta Detalle Alertas en Linea</td>
			</tr>
			<tr class="pf">
				<th width="105px" >				
					Numero de serie <s:textfield name="consultaNuemeroSerie" id="consultaNuemeroSerieDet" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="50;"/>
				</th>
			</tr>
			<tr>
				<td colspan="6" align="right" class="td">
					<table  cellpadding="0" cellspacing="0" >
						<tr>
							<td>
								<div class="btn_back w110">
									<a href="javascript: void(0);" class="icon_buscar" 
									onclick="javascript: consultarDetalleAlertasLinea( jQuery('#consultaNuemeroSerieDet').val());">
										<s:text name="midas.boton.buscar"/>
									</a>
								</div>
							</td>
							<td>
								<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;">
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="imprimirDetalleAlertasEnLinea(jQuery('#consultaNuemeroSerieDet').val());">
											<s:text name="midas.boton.exportarPDF" />
										</a>
									</div>
								</div>
							</td>
						</tr>
					</table>				
				
				</td>
			</tr>	
		</table>
		<br>
	</s:form>
	
	
	<c:if test="${ ! empty detalleAlertaLinea.cesvi}">
	
	
		
		<s:iterator value="detalleAlertaLinea.cesvi" var="cesvi" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4"  class="td">
						<div class="titulo w400"><s:text name="Alerta Cesvi"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td" >
						<label style="color: black;">Añio:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cesvi.anio}</label>
					</td>
					<td class="td">
						<label style="color: black;">Complemento:</label>
					</td>
					<td class="td" >	
						<label style="color: gray;">${cesvi.complemento}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Estatus:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cesvi.estatus}</label>
					</td>
					<td class="td">
						<label style="color: black;">Linea:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cesvi.linea}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cesvi.marca}</label>
					</td>
					<td class="td">
						<label style="color: black;">Mensage:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cesvi.mensaje}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Estatus Mensaje:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cesvi.mensajeEstatus}</label>
					</td >
					<td class="td">
						<label style="color: black;">Motor:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cesvi.motor}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Submarva:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cesvi.submarca}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo Vehiculo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cesvi.tipoVehiculo}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">VIN:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cesvi.vin}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if>
	 
	
	<c:if test="${ ! empty detalleAlertaLinea.cii}">
	 
		<s:iterator value="detalleAlertaLinea.cii" var="cii" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4"  class="td">
						<div class="titulo w400"><s:text name="Alerta CII"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cii.cia}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha deteccion:</label>
					</td>
					<td  class="td">	
						<label style="color: gray;">${cii.fechadeteccion}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cii.fechasiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Importe:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cii.importe}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Poliza:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cii.numPoliza}</label>
					</td>
					<td class="td">
						<label style="color: black;">Siniestro:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cii.numSiniestro}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo atencion:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${cii.tipoAtencion}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo hecho:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cii.tipoHecho}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo siniestro:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${cii.tipoSiniestro}</label>
					</td>
					
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.emision}"> 
		<s:iterator value="detalleAlertaLinea.emision" var="emi" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td" >
						<div class="titulo w400"><s:text name="Alerta Emision"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td" >
						<label style="color: black;">Agente:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${emi.agente}</label>
					</td>
					<td class="td">
						<label style="color: black;">Canal de venta:</label>
					</td>
					<td class="td" >	
						<label style="color: gray;">${emi.canalVenta}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${emi.cia}</label>
					</td>
					<td class="td">
						<label style="color: black;">Cliente Materno:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.cliente1Materno}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Cliente Paterno:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${emi.cliente1Paterno}</label>
					</td>
					<td class="td">
						<label style="color: black;">Nombre:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.cliente1Nombre}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha Inicio vigencia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${emi.inicioVigencia}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha Fin Vigencia:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.finVigencia}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Inciso:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.inciso}</label>
					</td>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${emi.marcaDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Modelo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.modelo}</label>
					</td>
					<td class="td">
						<label style="color: black;">Persona c1:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.personaC1}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Poliza:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.poliza}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo Descripcion:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.tipoDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo Servicio:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.tipoServicio}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo Transporte:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${emi.ttransDesc}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.ocra}"> 
		<s:iterator value="detalleAlertaLinea.ocra" var="ocr" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td" >
						<div class="titulo w400"><s:text name="Alerta Ocra"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${ocr.cia}</label>
					</td>
					<td class="td">
						<label style="color: black;">Estatus:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.estatus}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha Localizacion:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${ocr.fechaLocalizacion}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha Recuperacion:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.fechaRecuperacion}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha Robo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${ocr.fechaRobo}</label>
					</td>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.marcaDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Modelo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${ocr.modelo}</label>
					</td>
					<td class="td">
						<label style="color: black;">Numero Siniestro:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.numSiniestro}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Placa:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.placa}</label>
					</td>
					<td class="td">
						<label style="color: black;">Remarcado:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${ocr.remarcado}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.tipoDesc}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo de robo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.tipoRobo}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo trasnporte:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${ocr.ttransDesc}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.prevencion}"> 
		
		<s:iterator value="detalleAlertaLinea.prevencion" var="prev" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td" >
						<div class="titulo w400"><s:text name="Alerta Prevencion"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Causa prevencion:</label>
					</td >
					<td class="td">
						<label style="color: gray;">${prev.causaprevencion}</label>
					</td>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${prev.cia}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Monto:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${prev.monto}</label>
					</td>
					<td class="td">
						<label style="color: black;">Vin:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${prev.noserie}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Observaciones:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${prev.observaciones}</label>
					</td>
					<td class="td">
						<label style="color: black;">Placa:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${prev.placa}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
		
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.pt}"> 
		<s:iterator value="detalleAlertaLinea.pt" var="pts" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td">
						<div class="titulo w400"><s:text name="Alerta Pt"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${pts.cia}</label>
					</td>
					<td class="td">
						<label style="color: black;">Estatus de pago:</label>
					</td>
					<td  class="td">	
						<label style="color: gray;">${pts.estatusVehPago}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Estatus venta:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${pts.estatusVehVta}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha factura:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.fechaFactura}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha finiquito:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${pts.fechaFiniquito}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha pago:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.fechaPago}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${pts.fechaSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha venta:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.fechaVenta}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Importe venta:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.importeventa}</label>
					</td>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${pts.marcaDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Modelo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.modelo}</label>
					</td>
					<td class="td">
						<label style="color: black;">Monto pago:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.montoPago}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Numero siniestro:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.numSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo Descripcion:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.placa}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Porcentage perdida:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.porcentajePerdida}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo daño:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.tipoDano}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.tipoDesc}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo pago:</label>
					</td class="td">
					<td>	
						<label style="color: gray;">${pts.tipoPago}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo venta:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.tipoVenta}</label>
					</td>
					<td class="td">
						<label style="color: black;">Transporte:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.ttransDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Uso:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${pts.uso}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.scd}"> 
		<s:iterator value="detalleAlertaLinea.scd" var="scd" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td">
						<div class="titulo w400"><s:text name="Alerta SCD"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Modelo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${scd.anniomodelo}</label>
					</td>
					<td class="td">
						<label style="color: black;">AVE previa:</label>
					</td>
					<td  class="td">	
						<label style="color: gray;">${scd.avePrevia}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${scd.compania}</label>
					</td>
					<td class="td">
						<label style="color: black;">Deposito:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.deposito}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha egreso:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${scd.fechaEgreso}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha ingreso:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.fechaingreso}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${scd.marcaDesc}</label>
					</td>
					<td class="td">
						<label style="color: black;">Numero economico:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.numEconomico}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Placa:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.placas}</label>
					</td>
					<td class="td">
						<label style="color: black;">Siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${scd.siniestro}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.tipoDesc}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo transporte:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${scd.ttransDesc}</label>
					</td>
				</tr>
				
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.siniestro}"> 
		<s:iterator value="detalleAlertaLinea.siniestro" var="sin" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td">
						<div class="titulo w400"><s:text name="Alerta Siniestro"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Causa siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${sin.causaSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td  class="td">	
						<label style="color: gray;">${sin.cia}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Estatus:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${sin.estatus}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha siniestro:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${sin.fechaSiniestro}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Inciso:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${sin.inciso}</label>
					</td>
					<td class="td">
						<label style="color: black;">Monto:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${sin.montoSiniestro}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Numero Siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${sin.numSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Poliza:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${sin.poliza}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.sipac}"> 
		<s:iterator value="detalleAlertaLinea.sipac" var="spc" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td">
						<div class="titulo w400"><s:text name="Alerta SIPAC"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Cia acreedora:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.ciaAcreedora}</label>
					</td>
					<td class="td">
						<label style="color: black;">Cia deudora:</label>
					</td>
					<td  class="td">	
						<label style="color: gray;">${spc.ciaDudora}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha estatus:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.fechaEstatus}</label>
					</td>
					<td class="td">
						<label style="color: black;">Fecha expiracion:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${spc.fechaExp}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha siniestro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.fechaSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Marca:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${spc.marcaDesc}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Modelo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.modelo}</label>
					</td>
					<td class="td">
						<label style="color: black;">Placa:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${spc.placa}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Responsabilidad:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.responsabilidad}</label>
					</td>
					<td class="td">
						<label style="color: black;">Siniestro deudor:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${spc.siniestroDeudor}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.tipoDesc}</label>
					</td>
					<td class="td">
						<label style="color: black;">Tipo orden:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${spc.tipoOrden}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Tipo:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${spc.ttransDesc}</label>
					</td>
				</tr>
			</table>
		</s:iterator>
	</c:if> 
	
	<c:if test="${ ! empty detalleAlertaLinea.valuacion}"> 
		<s:iterator value="detalleAlertaLinea.valuacion" var="val" status="index"> 
			<table width="98%" align="center" class="contenedorFormas" >
				<tr>
					<td colspan="4" class="td">
						<div class="titulo w400"><s:text name="Alerta Valuacion"/></div>
					</td>
				</tr>	
				<tr>
					<td class="td">
						<label style="color: black;">Cia:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${val.cia}</label>
					</td>
					<td>
						<label style="color: black;">Fecha:</label>
					</td >
					<td  class="td">	
						<label style="color: gray;">${val.fecha}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Fecha monto:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${val.monto}</label>
					</td>
					<td class="td">
						<label style="color: black;">VIN:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${val.noSerie}</label>
					</td>
				</tr>
				<tr>
					<td class="td">
						<label style="color: black;">Numero sinietro:</label>
					</td>
					<td class="td">
						<label style="color: gray;">${val.numSiniestro}</label>
					</td>
					<td class="td">
						<label style="color: black;">Numero valuacion:</label>
					</td>
					<td class="td">	
						<label style="color: gray;">${val.numValuacion}</label>
					</td>
				</tr>
			</table>
		</s:iterator> 
	</c:if>
	
	
</div>





<div id="spacer2" style="height: 40px"></div>




