<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/sapAmis/sapAmisHeader.jsp"></s:include>

<script type="text/javascript">
jQuery(function(){
	dhx_init_tabbars();
	incializarTabs();
});
</script>
<div select="<s:property value="tabActiva"/>" hrefmode="ajax-html" style="height: 380px;width:910px;margin-left:10px;" id="configuracionNegocioTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="bitacoraEmision" name="Carga Emision" href="http://void" extraAction="javascript: desplegarBitacoraEmision();"></div>
	<div width="100%" id="bitacoraSiniestros" name="Carga Siniestros" href="http://void" extraAction="javascript: desplegarBitacoraSiniestros();"></div>
	<div width="100%" id="consAlertasEmision" name="Consulta Alertas" href="http://void" extraAction="javascript: desplegarConsultaAlertaEmision();"></div>
	<div width="100%" id="detalleAlertas" name="Detalle Alertas" href="http://void" extraAction="javascript: desplegarDetalleAlertaEmision();"></div>
	<div width="130%" id="ejecucionesManuales" name="Ejecuciones Manuales" href="http://void" extraAction="javascript: desplegarEjecucionesManuales();"></div>
</div>