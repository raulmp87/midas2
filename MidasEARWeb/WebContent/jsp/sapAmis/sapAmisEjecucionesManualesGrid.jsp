<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
	
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea_log</param>
				<param>true</param>
				<param>infoArea_log</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
			
		</beforeInit>		
		
        <afterInit>
        </afterInit>


        <column id="sessionid" type="ro" width="65" sort="str" align="center"><s:text name="Session ID"/></column>
        <column id="fechaInicioProceso" type="ro" width="85" sort="str" align="center"><s:text name="Fecha Inicio"/></column>
        <column id="fechaFinProceso" type="ro" width="85" sort="str" align="center"><s:text name="Fecha Fin"/></column>
        <column id="fechaFin" type="ro" width="85" sort="str" align="center"><s:text name="Fecha Termino"/></column>
        <column id="usuario" type="ro" width="80" sort="str" align="center" ><s:text name="Usuario"/></column>
        <column id="estatus" type="ro" width="80" sort="str" align="center" ><s:text name="Estatus"/></column>
        <column id="procesados" type="ro" width="82" sort="str" align="center"><s:text name="Procesados"/></column>
        <column id="noaplican" type="ro" width="82" sort="str" align="center" ><s:text name="No aplican"/></column>
        <column id="pendientes" type="ro" width="82" sort="str" align="center" ><s:text name="Por Procesar"/></column>
        <column id="modulo" type="ro" width="100" sort="str" align="center" ><s:text name="Modulo"/></column>

        <column id="Ejecutar" type="img" width="30"  align="center"></column>
       

	</head>  		
	
	<s:iterator value="listSapAmisEjecucionLog" var="c" status="index">
       <row id="<s:property value="sessionid"/>">
               <cell><s:property value="sessionid" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="fechaInicioProceso" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="fechaFinProceso" escapeHtml="false" escapeXml="true"/></cell>			
	           <cell><s:property value="fechaFin" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="usuario" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="procesados" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="noaplican" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="pendientes" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="modulo" escapeHtml="false" escapeXml="true"/></cell>
               <cell>/MidasWeb/img/confirmAll_disabled.gif^Ejecutar^javascript:ejecucionWebService(<s:property value="sessionid"/>)^_self</cell>                         
            
            </row> 
         </s:iterator>
    </rows>
