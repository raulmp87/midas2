<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="enableMultiline">
				<param>true</param>
			</call>
        </beforeInit>
        <afterInit>
			<call command="setColAlign">
				<param>left</param>
			</call>
			<call command="enableResizing">
				<param>false,false,false,false</param>
			</call>
		</afterInit>
		<column id="NoSerie" type="ro" width="140" sort="int">No. Serie</column>
        <column id="Sistema" type="ro" width="80" sort="int">Sistema</column>
		<column id="Alerta" type="ro" width="180" sort="int">Alerta</column>
		<column id="Acciones" type="ro" width="480" sort="int">Acciones</column>
	</head>
	<s:iterator value="sapAmisRespuestaAlertas" var="sapAmisRespuestaAlertas" status="userStatus">
		<row id="${userStatus.count}">
			<cell><![CDATA[<s:property value="vin" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="sistema" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="alerta" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<rows><encabezado><column id="Acciones" type="ro" width="445" sort="int">Acciones</column><column id="Eliminar" type="img" width="25" sort="int"></column></encabezado><s:iterator value="acciones" var="acciones" status="subStatus"><row id="${userStatus.count}|@|${subStatus.count}|@|<s:property value="#acciones.descAccion" escapeHtml="false" escapeXml="true"/>|@|<s:property value="#acciones.idAccion" escapeHtml="false" escapeXml="true"/>"><cell colspan="2"><s:property value="#acciones.descAccion" escapeHtml="false" escapeXml="true"/></cell></row></s:iterator><row id="nueva"><cell>nuevaAccion<s:property value="idRelacion" escapeHtml="false" escapeXml="true"/></cell><cell>/MidasWeb/img/icons/ico_agregar.gif^Agregar^javascript:agregarAccion("<s:property value="idRelacion" escapeHtml="false" escapeXml="true"/>",${userStatus.count});^_self</cell></row></rows>]]></cell>
	 	</row>
	</s:iterator>
</rows>