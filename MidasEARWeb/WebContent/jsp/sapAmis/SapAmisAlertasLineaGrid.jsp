<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea_Clientes</param>
				<param>true</param>
				<param>infoArea_Clientes</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="Sistema" type="ro" width="300" sort="int" >Sistema</column>
		<column id="Alerta" type="ro" width="500" sort="int" >Alerta</column>
		

	</head>
	
	
	
	<s:iterator value="respuesConsultaLinea" var="c" status="index">
		<row id="${index.count}">
			<cell><![CDATA[<s:property value="sistema" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="descripcion" escapeHtml="false" escapeXml="true"/>]]></cell>
		</row>
	</s:iterator>
	
	
	
	
</rows>