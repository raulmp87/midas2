<%@ taglib prefix="s" uri="/struts-tags" %>
	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	 <!--  
	<script src="<s:url value='/js/midas2/generadorCURPRFC.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/agente.js'/>"></script>
	<script src="<s:url value='/js/midas2/catalogos/altaPorInternet.js'/>"></script>
	<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
	<script src="<s:url value='/js/midas2/catalogos/configuracionBono/calculoBono.js'/>"></script>
	--> 
	
<style type="text/css" >
	#botonSapAmis{  
		position: fixed;
		z-index: 9;
		font-size: 2.5em;
		font-weight: bold;
	    color: #fff;
	    opacity: 0.9;
		bottom: 30px;
		right: 30px;
		background-color: #00a433;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 15px;
		padding-right: 15px;
	}
	#botonRepuve{  
		position: fixed;
		z-index: 9;
		font-size: 2.5em;
		font-weight: bold;
	    color: #fff;
	    opacity: 0.9;
		bottom: 60px;
		right: 30px;
		background-color: #00a433;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 15px;
		padding-right: 15px;
	}
	#pantallaBitacoras{
		position: fixed;
		z-index: 10;
		background-color: #FFFFFF;
		top: 0px;
		left: 0px;
		height: 100%;
		width: 100%;
	}
</style>
	<script src="<s:url value='/js/midas2/operacioensSapAmis/opreacionesSapAmis.js'/>"></script>
	<!--<script src="<s:url value='/js/midas2/operacioensSapAmis/bitacorasSapAmis.js'/>"></script>--!>
	<!-- Agregar la referencia al js que ejecuta el action para cargar cada Tab de la pantalla -->
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
	<script type="text/javascript">
		inicializarBotonSapAmis();
		inicializarBotonRepuve();
	</script>