<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<div class="contenedor" style="width:99%">
    <h2 class="text-center" style="font-size=11px !important">Acciones - Alertas SAP-AMIS</h2>
	<div id="alertasGrid" style="width:900px;height:360px"></div>
	<script type="text/javascript">
		initConsultaLineaGrid();
		consultarAlertasLinea();
	</script>
    </div>
</div>