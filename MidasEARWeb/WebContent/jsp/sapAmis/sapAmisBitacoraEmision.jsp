<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">
	<s:form  id="clienteForm" name="clienteForm">
	
		<!-- Parametro de la forma para que sea reutilizable -->
		<s:hidden name="tipoAccion"></s:hidden>
		<s:hidden name="tipoRegreso"  id="tipoRegreso"/>
		<s:hidden name="idNegocio"  id="idNegocio"/>
		<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
		<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
		<s:hidden name="idField"></s:hidden>
		<s:hidden name="divCarga"></s:hidden>
		<table width="890px" id="filtrosM2" cellpadding="0" cellspacing="0" >
			<tr>
				<td class="titulo" colspan="4">Bitacora Envios Emision</td>
			</tr>
			<tr class="pf">
				<th width="105px"  >				
					Poliza
				</th>	
				<td width="315px">
					<s:textfield name="bitacoraPoliza" id="bitacoraPoliza" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="50;"/>
				</td>
				<th width="105px">				
					VIN
				</th>	
				<td width="315px">
					<s:textfield name="bitacoraVin" id="bitacoraVin" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="30"/>
				</td>			
				<th width="105px">				
					Fecha de Envio
				</th>	
				<td width="315px">
					<sj:datepicker name="bitacoraFechaEnvio" id="bitacoraFechaEnvio" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);">
	 				</sj:datepicker>	
					<!--  
					<s:textfield name="filtroCliente.apellidoMaterno" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200"
					maxlength="30"/>
					-->
				</td>
			</tr>
			<tr class="pf" >
				<th width="105px">				
					Estatus Envio
				</th>	
				<td width="315px">
				    <s:select name="estatusEnvio" id="estatusEnvio" list="#{'2':'Estado del envio','0':'Aceptado','1':'Rechazado'}" value="2"></s:select>
				</td>
			</tr>
			<tr>
				<th width="105px" >
					Sistamas de Alerta
				</th>	
				<td width="315px" colspan="3">
					<div style="border: 1px solid #A0E0A0; font-size: 9px; text-transform: uppercase; width: 290px; background-color:white;">
						<ul id="listaSistemasAlerta"  style=" margin: 0 0 0 -20px; list-style-type: none; height: 100px; overflow-y: scroll; overflow-x: hidden;">
							<li>
								<input class="sisAlertas" name="alerCesvi" id="alerCesvi" value="cesvi" type="checkbox" /><label>CESVI</label> 
							</li>
							<li>
								<input class="sisAlertas" name="alertCII" id="alertCII" value="cii" type="checkbox" /><label>CII</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertEmision" id="alertEmision" value="emision" type="checkbox" /><label>Emision</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertOcra" id="alertOcra" value="ocra" type="checkbox" /><label>OCRA</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertPrevencion" id="alertPrevencion" value="prevencion" type="checkbox" /><label>Prevencion</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertPT" id="alertPT" value="pt" type="checkbox" /><label>PT</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertCSD" id="alertCSD" value="csd" type="checkbox" /><label>CSD</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertSiniestro" id="alertSiniestro" value="siniestro" type="checkbox" /><label>Siniestro</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertSIPAC" id="alertSIPAC" value="sipac" type="checkbox" /><label>SIPAC</label>
							</li>
							<li>
								<input class="sisAlertas" name="alertValuacion" id="alertValuacion" value="valuacion" type="checkbox" /><label>Valuacion</label>
							</li>
						</ul>
					</div>
					
				</td>
			</tr>
			<tr>
				<td colspan="6" align="right">				
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar" 
						onclick="javascript: filtrarBitacoraEmision( jQuery('#bitacoraPoliza').val(), jQuery('#bitacoraVin').val(), jQuery('#bitacoraFechaEnvio').val(), jQuery('#estatusEnvio').val());">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>				 
				</td>
			</tr>	
		</table>
		<br>
	</s:form>
	
	<div id="emisionGrid" style="width:820px;height:190px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	
       
	<script type="text/javascript">
		initBitacoraEmisionClientesGrid();
	</script>
	
	<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="exportarExcelBitacoraEmision( jQuery('#bitacoraPoliza').val(), jQuery('#bitacoraVin').val(), jQuery('#bitacoraFechaEnvio').val(), jQuery('#estatusEnvio').val());">
				<s:text name="midas.boton.exportarExcel" />
			</a>
		</div>
	</div>
	
</div>



<div id="spacer2" style="height: 40px"></div>


