<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.td{
	max-width: 150px;
}
</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">
	<s:form  id="clienteForm" name="clienteForm">
	
		<!-- Parametro de la forma para que sea reutilizable -->
		<s:hidden name="tipoAccion"></s:hidden>
		<s:hidden name="tipoRegreso"  id="tipoRegreso"/>
		<s:hidden name="idNegocio"  id="idNegocio"/>
		<s:hidden name="idToCotizacion"  id="idToCotizacion"/>
		<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
		<s:hidden name="idField"></s:hidden>
		<s:hidden name="divCarga"></s:hidden>
		<table width="890px" id="filtrosM2" cellpadding="0" cellspacing="0" >
			<tr>
				<td class="titulo td" colspan="4">Ejecuciones Manuales</td>
			</tr>
			<tr class="pf">
					<th width="105px">				
					Fecha de Inicio
				</th>	
				<td width="315px">
					<sj:datepicker name="operacionManualFechaInicio" id="operacionManualFechaInicio" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);">
	 				</sj:datepicker>	
				</td>
			</tr>
			<tr class="pf">
					<th width="105px">				
					Fecha Fin
				</th>	
				<td width="315px">
					<sj:datepicker name="operacionManualFechaFin" id="operacionManualFechaFin" buttonImage="../img/b_calendario.gif"			 
					   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
					   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   onblur="esFechaValida(this);">
	 				</sj:datepicker>	
				</td>
			</tr>
			
			<tr class="pf" >
				<th width="105px">				
					Tipo de Proceso
				</th>	
				<td width="360px">
				    <s:select name="modulo" id="modulo" list="#{'2':'Seleccione Proceso','0':'Siniestros','1':'Emision'}" value="2"></s:select>
				</td>
			</tr>
			
			
			<tr>
				<td colspan="6" align="right" class="td">
					<table  cellpadding="0" cellspacing="0" >
						<tr>

							<td>
								<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;">
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" class="icon_buscar" 
											onclick="javascript: ejecucionAmisManual(jQuery('#operacionManualFechaInicio').val(),jQuery('#operacionManualFechaFin').val(),jQuery('#modulo').val());">
											<s:text name="Ejecutar"/>
						               </a>
									</div>
									
								</div>
							</td>
							
							<td>
								<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;">
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" class="refresh" icon="refresh.png"
											onclick="javascript: getGridLogEjecucion();">
											<s:text name="Actualizar Lista"/>
						               </a>
									</div>
									
								</div>
							</td>

						</tr>
					</table>				
				
				</td>
			</tr>	
		</table>
		<br>
	</s:form>
	

<div id="spacer2" style="height: 3px"></div>

<div id=pagingArea_log></div><div id="infoArea_log"></div>
<div id="logListadoGrid" style="width:856px;height:200px"></div>


<s:hidden property value="mensaje" />

<script type="text/javascript">
jQuery( document ).ready(function($) {
   getGridLogEjecucion();
   
  // alert($('#mensaje').val);
  res = "${mensaje}";
  if (res!="")
	  {
	  alert("${mensaje}");
	  res = "";
	  }
  
 
   
   
});
</script>  


