<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea_s</param>
				<param>true</param>
				<param>infoArea_s</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="num" type="ro" width="50" sort="int" ></column>
		<column id="poliza" type="ro" width="140" sort="int" >Poliza</column>
		<column id="vin" type="ro" width="140" sort="str">VIN</column>
		<column id="inciso" type="ro" width="120" sort="str">Inciso</column>
		<column id="oper" type="ro" width="120" sort="str">Operacion</column>
		<column id="respOper" type="ro" width="120" sort="str">Respuesta Opracion</column>
		<column id="iniVig" type="ro" width="160" sort="str">Fecha Siniestro</column>
		<column id="fehcaEnv" type="ro" width="120" sort="str">Fecha Envio</column>
		<column id="imprimir" type="img" width="80" sort="na" align="center">Imprimir</column>
		<column id="detalle" type="img" width="80" sort="na" align="center">Alertas</column>

	</head>
	  
	<s:iterator value="listaBitacoraSiniestros" var="c" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[<s:property value="numeropoliza" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="numeroserie" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="inciso" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="banderaOperacion" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="estatusRespOperacion" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="fechasiniestro" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="fechaEnvioSap" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><s:url value="/img/b_printer.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:imprimirDetalleAlertasBitacoraEmision(${c.sapAlertasistemasEnvio.sapIdalertasistemasEnvio})^_self</cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:cargarDetalleAlertasBietacoraSiniestros(${c.sapAlertasistemasEnvio.sapIdalertasistemasEnvio})^_self</cell>
			
		</row>
	</s:iterator>
	

</rows>