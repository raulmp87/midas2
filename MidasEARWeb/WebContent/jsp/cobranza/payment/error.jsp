<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Error</title>
</head>
<body>
	<div class="content">
		<div class="alert alert-danger text-center" role="alert">
			<strong><i class="fa fa-times"></i> Error:</strong> Es posible que la informaci&oacute;n proporcionada sea incorrecta.
		</div>
	</div>
</body>
</html>