<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>&Eacute;xito</title>
</head>
<body>
	<div class="content">
		<div class="alert alert-success text-center" role="alert">
			<strong><i class="fa fa-check"></i> Gracias por su pago:</strong> En breve recibir&aacute; su comprobante por correo.
		</div>
	</div>
</body>
</html>