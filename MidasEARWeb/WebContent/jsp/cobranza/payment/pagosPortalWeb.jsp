<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pago de P&oacute;liza</title>
	<link rel="stylesheet" href="../../css/cobranza/pagos/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/cobranza/pagos/payment.css">
	
	<script type="text/javascript" src="../../js/cobranza/pagos/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="../../js/cobranza/pagos/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../js/cobranza/pagos/payment.js"></script>
</head>
<body>
	<div class="content">
		<br>
		<div class="col-md-12">
				<div class="col-md-5">
					<form id="searchForm" action="findReceipts.action" class="form-inline" method="post">
						<div class="form-group">
							<strong>Nota:</strong><i style="color: red; font-family: Arial; font-size: 10px;"> Favor de introducir tu RFC sin homoclave y tu n�mero de p�liza como aparecen en la caratula.</i>
							<br>
							<label>RFC</label>
							<input type="text" style="width: 100px; text-transform: uppercase;" value="<s:property value="siglasRFC"/>" class="form-control" name="siglasRFC" maxlength="4" minlength="3" id="siglasRFC" placeholder="Iniciales" required autocomplete="off">
							<input type="text" style="width: 100px; text-transform: uppercase;" value="<s:property value="fechaRFC"/>" class="form-control" name="fechaRFC" maxlength="6" minlength="6" id="fechaRFC" placeholder="Fecha" required autocomplete="off">							
						</div>
						<br>
						<br>
						<div class="form-group">
							<input type="text" style="width: 200px" value="<s:property value="insurancePolicy" />" class="form-control" name="insurancePolicy" maxlength="22" id="insurancePolicy" placeholder="00000-000000000000-00" required autocomplete="off" >
						</div>
						<div class="form-group">
							<div class="input-group">
								<s:if test="%{subsection > 0 }">
									<span class="input-group-addon">
										<input type="checkbox" aria-label="includeSubsection" id="includeSubsection" checked>
									</span>
									<input type="text" id="subsection" value="<s:property value="subsection" />" name="subsection" style="width: 100px" min="0" class="form-control" aria-label="subsection" required="required">
								</s:if>
								<s:else>
									<span class="input-group-addon">
										<input type="checkbox" aria-label="includeSubsection" id="includeSubsection">
									</span>
									<input type="text" id="subsection" name="subsection" style="width: 100px" min="0" class="form-control" aria-label="subsection" disabled>
								</s:else>
							</div>
						</div>
						<button class="btn btn-success" id="btnSearch" type="button"><i class="fa fa-search"></i></button>
					</form>
				</div> <!-- Termina seccion de busqueda -->
				<br>
				<br>
				<br>
				<div class="col-md-7">
					<div class="page-header">
						<h4><s:property value="policy.nombreAsegurado" /> <small><s:property value="policy.nombreProducto" /></small></h4>
					</div>
				</div> <!-- Termina seccion de datos del asegurado y producto -->
				<br>
				<br>
				<br>
				<br>
				<div class="col-md-12">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Pendientes de pago</a></li>
						<li role="presentation"><a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">Recibos pagados</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="pending">
							<form action="startTransaction.action" method="post" id="payForm">
								<table class="table text-center">
									<thead>
										<tr>
											<th></th>
											<th>Recibo</th>
											<th>Total</th>
											<th>Vencimiento</th>
											<th>Estado</th>
											<th width="50"></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="%{ getPendingReceipts().isEmpty() }">
											<tr>
												<td colspan="6">
													<br>
													<p class="text-center">No hay informaci&oacute;n para mostrar.</p>
												</td>
											</tr>
										</s:if>
										<s:else>
										<s:iterator value="pendingReceipts" var="receipt" status="index">
											<tr>
												<td></td>
												<td>${receipt.numeroFolioRecibo}</td>
												<td>$ ${receipt.importe}</td>
												<td><s:date name="fechaLimitePago" format="dd/MM/yyyy" /></td>
												<td>
													<span class="label label-${receipt.situacion}">${receipt.situacion}</span>
												</td>
												<td>
													<s:if test="%{ getDiasVencido() >= 0}">
														<input type="checkbox" class="seleccionado" name="selectedReceipts" value="${receipt.idRecibo}">
													</s:if>
													<s:else>
														<span class="label label-danger">Vencido</span>
													</s:else>	
												</td>
											</tr>
										</s:iterator>
										</s:else>
									</tbody>
								</table>
					<input type="hidden" name="insurancePolicy" value="<s:property value="insurancePolicy"/>" />
					<input type="hidden" name="siglasRFC" value="<s:property value="siglasRFC"/>" />
					<input type="hidden" name="fechaRFC" value="<s:property value="fechaRFC"/>" />
					<s:if test="%{subsection != null }">
						<input type="hidden" name="subsection" value="<s:property value="subsection"/>" />
					</s:if>
					<input type="hidden" name="tipoCambio" value="<s:property value="tipoCambio"/>" />
					
					<s:if test="%{getUrlDestino() != null }"> 
					<input type="hidden" name="url" id="vUrl" value="<s:property value="UrlDestino"/>" />
            		<script>
					openWindows(); 
					</script>
					</s:if>
				</form>
			</div> <!-- Termina panel de Recibos pendientes -->
			
			<div role="tabpanel" class="tab-pane fade" id="paid">
				<table class="table text-center">
					<thead>
						<tr>
							<th width="50"></th>
							<th class="text-center">Recibo</th>
							<th class="text-right">Total</th>
							<th class="text-center">Vencimiento</th>
							<th class="text-center">Estado</th>
						</tr>
					</thead>
					<tbody>
						<s:if test="%{ getPaidReceipts().isEmpty() }">
							<tr>
								<td colspan="6">
									<br>
									<p class="text-center">No hay informaci&oacute;n para mostrar.</p>
								</td>
							</tr>
						</s:if>
						<s:else>
						<s:iterator value="paidReceipts" var="receipt" status="index">
							<tr>
								<td width="50"></td>
								<td>${receipt.numeroFolioRecibo}</td>
								<td class="text-right">$ ${receipt.importe}</td>
								<td><s:date name="fechaLimitePago" format="dd/MM/yyyy" /></td>
								<td class="text-center">
									<span class="label label-${receipt.situacion}">${receipt.situacion}</span>
								</td>
							</tr>
						</s:iterator>
						</s:else>
					</tbody>
				</table>
			</div> <!-- Termina panel de Recibos pagados -->
			
		</div>
	<br>				
</div>
	
<div class="col-md-12">
	<div class="alert alert-info">
		Cualquier duda o aclaraci&oacute;n acerca del estado de su p&oacute;liza, por favor comun&iacute;quese al (01) (81) 81501111.
	</div>
</div>

<br>
<div class="col-md-10">
	<ol class="breadcrumb">
		<li><a href="https://www.afirme.com/Portal/VisualizadorContenido.do?archivoId=10474&menu=12" target="_blank">Aviso de privacidad</a></li>
		<li><a href="https://www.afirme.com/Portal/VisualizadorContenido.do?directorioId=7" target="_blank">T&eacute;rminos y condiciones</a></li>
	</ol>
</div>

<br>
<div class="col-md-2">
	<div class="text-center">
		<button id="pay" class="btn btn-success"><i class="fa fa-credit-card"></i> Pagar recibos</button>
	</div>
</div>

<div class="clearfix"></div>
</div>
</div>

<div class="modal fade" id="window">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Procesar pago</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<iframe id="ifrProsa" src="" width="100%" height="500" style="border:0px;" ></iframe>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mensajeCustom">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">
						<i class="fa fa-bell"></i> Mensaje
				</h4>
			</div>
			<div class="modal-bodyCustom">
				<br>
				<div style="margin-bottom: 0" class="alert alert-warning">
					<p id="textDinamico"></p><!--valor mensaje -->
				</div>
			</div><!-- /.modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div><!-- /.modal-footer -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="mensaje">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-bell"></i> Mensaje</h4>
			</div>
			<div class="modal-body">
				<br>
				<div style="margin-bottom: 0" class="alert alert-<s:property value="message.type" />">
				<s:property value="message.message" />
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="../../js/cobranza/pagos/jquery-1.11.3.min.js"></script>
<script src="../../js/cobranza/pagos/bootstrap.min.js"></script>
<script src="../../js/cobranza/pagos/payment.js"></script>
<script>
$(document).ready(function(){
	<s:if test="%{ message != null }">
	$('#mensaje').modal();
	</s:if>
});
</script>
</body>
</html>