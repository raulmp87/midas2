<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Procesando...</title>
</head>
<body>
	<div class="alert text-center" role="alert">
			<i class="fa fa-spinner fa-spin fa-2x"></i>
			<br>
			<br>
			<strong>Procesando... </strong> Por favor espere.
		</div>
</body>
</html>