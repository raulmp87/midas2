<%@ taglib prefix="s" uri="/struts-tags" %>
	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	<script src="<s:url value='/js/midas2/generadorCURPRFC.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/agente.js'/>"></script>
	<script src="<s:url value='/js/midas2/catalogos/altaPorInternet.js'/>"></script>
	<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
	<script src="<s:url value='/js/midas2/catalogos/configuracionBono/calculoBono.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
  
    <script type="text/javascript">
    var mostrarContenedorAgentePath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/agente"/>';
    var mostrarAutorizacionAgentePath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/autorizacionagentes"/>';
    var listarAgentePath = '<s:url action="lista" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoAgentePath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoAgentePaginadoPath = '<s:url action="paginarAgente" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoCarteraClienteAgentePath = '<s:url action="listarFiltradoCarteraCliente" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoBusquedaClienteAgentePath = '<s:url action="listarFiltradoBusquedaCliente" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoHijoAgentePath = '<s:url action="listarFiltradoHijoAgente" namespace="/fuerzaventa/agente"/>';
    var listarFiltradoEntretenimientoAgentePath = '<s:url action="listarFiltradoEntretenimiento" namespace="/fuerzaventa/agente"/>';
	var verDetalleAgentePath = '<s:url action="verDetalle" namespace="/fuerzaventa/agente"/>';
	var guardarAgentePath = '<s:url action="guardar" namespace="/fuerzaventa/agente"/>';
	var guardarInfoGralPath = '<s:url action="guardarInfoGeneral" namespace="/fuerzaventa/agente"/>';
	var guardarDomicilioAgentePath = '<s:url action="guardarDomicilioAgente" namespace="/fuerzaventa/agente"/>';
	var guardarDatosFiscalesPath = '<s:url action="guardarDatosFiscales" namespace="/fuerzaventa/agente"/>';
	var guardarDatosContablesPath = '<s:url action="guardarDatosContables" namespace="/fuerzaventa/agente"/>';
	var guardarDatosExtraPath = '<s:url action="guardarDatosExtra" namespace="/fuerzaventa/agente"/>';
	var guardarDatosHijoExtraPath = '<s:url action="guardarDatosHijoExtra" namespace="/fuerzaventa/agente"/>';
	var guardarDatosEntretenimientoExtraPath = '<s:url action="guardarDatosEntretenimientoExtra" namespace="/fuerzaventa/agente"/>';
	var guardarDocumentosPath = '<s:url action="guardarDocumentos" namespace="/fuerzaventa/agente"/>';
	var guardarCarteraClientesPath = '<s:url action="guardarCarteraClientes" namespace="/fuerzaventa/agente"/>';
	
	
	var verDetalleClientePorAgentePath = '<s:url action="obtenerClientePorAgente" namespace="/fuerzaventa/agente"/>';
	var eliminarAgentePath = '<s:url action="eliminar" namespace="/fuerzaventa/agente"/>';
	var eliminarDatosHijoExtraPath = '<s:url action="eliminarDatosHijoExtra" namespace="/fuerzaventa/agente"/>';
	var eliminarDatosEntretenimientoExtraPath = '<s:url action="eliminarDatosEntretenimientoExtra" namespace="/fuerzaventa/agente"/>';
	
	var mostrarInfoGralPath = '<s:url action="mostrarInfoGeneral" namespace="/fuerzaventa/agente"/>';
	var mostrarDomicilioAgentePath = '<s:url action="mostrarDomicilioAgente" namespace="/fuerzaventa/agente"/>';
	var mostrarDatosFiscalesPath = '<s:url action="mostrarDatosFiscales" namespace="/fuerzaventa/agente"/>';
	var mostrarDatosContablesPath = '<s:url action="mostrarDatosContables" namespace="/fuerzaventa/agente"/>';
	var mostrarDatosExtraPath = '<s:url action="mostrarDatosExtra" namespace="/fuerzaventa/agente"/>';
	var mostrarDocumentosPath = '<s:url action="mostrarDocumentos" namespace="/fuerzaventa/agente"/>';
	var mostrarCarteraClientesPath = '<s:url action="mostrarCarteraClientes" namespace="/fuerzaventa/agente"/>';
	var generarLigaIfimaxPath = '<s:url action="generarLigaIfimax" namespace="/fuerzaventa/agente"/>';
	
	var mostrarClientesPath = '<s:url action="mostrarClientes" namespace="/fuerzaventa/agente"/>';
	
	</script>