<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<rows>
  <head>
    <beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
      <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="noRecibo" type="ro" width="100"><s:text name="Numero Recibo" /></column>
		<column id="factura" type="ro" width="200"><s:text name="Factura" /></column>
		<column id="numExhibicion" type="ro" width="100"><s:text name="Exhibicion" /></column>
		<column id="remesaDesp" type="ro" width="150"><s:text name="Remesa" /></column>
		<column id="numPoliza" type="ro" width="150"><s:text name="Poliza" /></column>
		<column id="numRenPoliza" type="ro" width="150"><s:text name="Num Renov Poliza" /></column>
		<column id="situacionRecibo" type="ro" width="100"><s:text name="Situacion Recibo" /></column>
		<column id="fCubreDesde" type="ro" width="100"><s:text name="Fecha Desde" /></column>
		<column id="fCubreHasta" type="ro" width="100"><s:text name="Fecha Hasta" /></column>
	    <column id="nomCliente" type="ro" width="300"><s:text name="Nombre Cliente" /></column>
		<column id="fContab" type="ro" width="100"><s:text name="Fecha Contab" /></column>
		<column id="usuarioCreacion" type="ro" width="150"><s:text name="Usuario Creacion" /></column>
		<column id="fechaEmision" type="ro" width="100"><s:text name="Fecha Emision" /></column>
	    <column id="tipoMovimiento" type="ro" width="150"><s:text name="Tipo Movimiento" /></column>
	    <column id="folioFiscal" type="ro" width="150"><s:text name="Folio Fiscal" /></column>
	    <column id="error" type="ro" width="350"><s:text name="Error" /></column>
  </head>
  <s:iterator value="listComplementoPago" status="index" var="listComplementoPagoObject">
		<row id="${listComplementoPagoObject.id}">
			<cell><s:property value="noRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numExhibicion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="remesaDesp" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monitorNumPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monitorNumRenovPoliza" escapeHtml="false" escapeXml="true"/></cell>
      		<cell><s:property value="situacionRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCubreDesde" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCubreHasta" escapeHtml="false" escapeXml="true"/></cell>
      		<cell><s:property value="nomCliente" escapeHtml="false" escapeXml="true"/></cell>			
      		<cell><s:property value="fechaContab" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
      		<cell><s:property value="fechaEmision" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoMovimiento" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="folioFiscal" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="error" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>
