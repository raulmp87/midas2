<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/cobranza/pagos/complementopago/HeaderComplementoPago.jsp"></s:include>

<style type="text/css">
 div.ui-datapicker{
   font-size:10px;
 }
 table tr td div span label{
   color:black;
   font-weight: normal;
   text-align: left;
 }
 .td{
   max-width:150px;
 }
</style>


<div id="spacer1" style="height: 20px"></div>
<div align="center">
  <s:form id="monitorForm" name="monitorForm#">
      <table id="filtrosM2" width="80%" border="0" cellpadding="5" cellspacing="2">
        <tr>
          <td class="titulo td" colspan="6">Monitor de Complemento Pago</td>
        </tr>
        <tr class="pf" align="left">
          <th width="25%" align="left" style="text-align:left;">
            Fecha de Pago Inicio:&nbsp;
          </th>
          <td>
            <sj:datepicker name="filtroComplementoPago.fechaPagoInicio" id="fechaPagoInicio" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:12px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
       </sj:datepicker>
          </td>
          <th width="25%" align="right" style="text-align:right;">
            Fecha de Pago Fin:&nbsp;
          </th>
          <td>
            <sj:datepicker name="filtroComplementoPago.fechaPagoFin" id="fechaPagoFin" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:12px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
       </sj:datepicker>
          </td>
        </tr>
         <tr>
          <td width="25%" align="left" style="text-align:left;color: #666666;font-weight: bold;">Numero Recibo:&nbsp;</td>
           <td>
            <input type="text" class="cajaTextoM2" 
            name="filtroComplementoPago.noRecibo" id="nomRecibo" 
            style="align:right;" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.keyCode === 8" />
          </td>
        </tr>
        <tr>
          <td colspan="6" align="right" class="td">
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <div id="searchBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
                    <div class="btn_back w140" style="display: inline; float: right;width: 170px;" >
                      <a href="javascript: void(0);" class="icon_buscar"
                        onclick="javascript: buscarErroresPorFiltros();">
                        <s:text name="Buscar"/>
                      </a>
                    </div>
                  </div>
                  </td>
				<td>
                	<div id="execButon"  class="w150" style="float:right; padding-right: 10px;width: 200px;">
                      <div class="btn_back w140" style="display: inline; float: left;width: 200px;" >
                        <a href="javascript: void(0);" class="icon_ejecutar"
                          onclick="javascript: envioNotificacion();">
                          <s:text name="Enviar Notificacion"/>
                        </a>
                      </div>
                    </div>
                  </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>
      <div id="monitorCPGrid" class="h300" style="background-color:white;overflow:hidden"></div>
      <div id="pagingArea"></div><div id="infoArea"></div>
  </s:form>
</div>
