<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<rows>
  <head>
    <beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
      <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
    <column id="agente" type="ro" width="100"><s:text name="Agente" /></column>
	<column id="poliza" type="ro" width="150"><s:text name="Poliza" /></column>
	<column id="cliente" type="ro" width="300"><s:text name="Cliente" /></column>
	<column id="recibo" type="ro" width="150"><s:text name="Recibo" /></column>
	<column id="exhibicion" type="ro" width="150"><s:text name="Exhibicion" /></column>
	<column id="folioFiscal" type="ro" width="150"><s:text name="Folio Fiscal" /></column>
    <column id="pdf" type="img" width="100"><s:text name="PDF" /></column>
    <column id="xml" type="img" width="100"><s:text name="XML" /></column>
  </head>
  <s:iterator value="listComplementoPago" status="index" var="listComplementoPagoObject">
		<row id="${listComplementoPagoObject.id}">
			<cell><s:property value="claveAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nomCliente" escapeHtml="false" escapeXml="true"/></cell>
      <cell><s:property value="noRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numExhibicion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="folioFiscal" escapeHtml="false" escapeXml="true"/></cell>
      <cell>../img/logoPDF_small.png^Descargar PDF^javascript: exportarPDF("<s:property value="folioFiscal"/>")^_self</cell>
	    <cell>../img/xml-small.gif^Descargar XML^javascript: exportarXML("<s:property value="folioFiscal"/>")^_self</cell>
		</row>
	</s:iterator>
</rows>
