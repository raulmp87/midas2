<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript" src="<html:rewrite page="/js/complementoPago.js"/>"></script>

<script type="text/javascript">
  var listarGridVacioPath = '<s:url action="lista" namespace="/cobranza/pagos/complementopago"/>';
  var listarGridErrores = '<s:url action="listarErroresCP" namespace="/cobranza/pagos/complementopago"/>';
  var execService = '<s:url action="execService" namespace="/cobranza/pagos/complementopago"/>';
</script>
