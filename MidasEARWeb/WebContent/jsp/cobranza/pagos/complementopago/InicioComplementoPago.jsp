<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/cobranza/pagos/complementopago/HeaderComplementoPago.jsp"></s:include>

<style type="text/css">
 div.ui-datapicker{
   font-size:10px;
 }
 table tr td div span label{
   color:black;
   font-weight: normal;
   text-align: left;
 }
 .td{
   max-width:150px;
 }
</style>

<div id="spacer1" style="height: 20px"></div>
<div align="center">
  <s:form id="complementoForm" name="complementoForm#">
      <table id="filtrosM2" style="height: 200px"  width="100%" border="0" cellpadding="5" cellspacing="2">
        <tr>
          <td class="titulo td" colspan="6">Complementos de Pago</td>
        </tr>
        <tr class="pf" width="30%">
          <th width="5%" style="text-align:left;">
            Centro Emisor:&nbsp;
          </th>
          <td align="left"  width="5%" style="text-align:left;">
        <input type="text" name="filtroComplementoPago.emisionPoliza" id="centroEmision"  value="" style="text-align:left;"
           onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.keyCode === 8"
           onkeydown="limit(this,3);" onkeyup="limit(this,3);"/>
      </td>
      <th width="10%" style="text-align:left;">
        Póliza:&nbsp;
      </th>
      <td align="left"  width="20%" style="text-align:left;">
        <input type="text" name="filtroComplementoPago.numPoliza" id="numPolizaComplemento" style="text-align:left;"
               onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.keyCode === 8"
               onkeydown="limit(this,23);" onkeyup="limit(this,23);"/>
      </td>
      <th width="25%" style="text-align:right;">
        Numero Renovacion:&nbsp;
      </th>
      <td  align="left" width="5%" style="text-align:left;">
        <input type="text" name="filtroComplementoPago.numRenPoliza" id="numRenovPol" value="" style="text-align:left;"
           onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.keyCode === 8"
           onkeydown="limit(this,3);" onkeyup="limit(this,3);"/>
      </td>
        </tr>
        <tr class="pf">
          <th width="10%" style="text-align:left;">
            Clave Agente:&nbsp;
          </th>
          <td width="20%" style="text-align:left;">
            <input type="text" name="filtroComplementoPago.claveAgente" id="claveAgente" style="align:right;"/>
          </td>
          <th width="10%" style="text-align:left;">
            Nombre Cliente:&nbsp;
          </th>
          <td width="20%" style="text-align:left;">
            <input type="text" name="filtroComplementoPago.nomCliente" id="nomCliente" style="align:right;"/>
          </td>
          <th width="25%" style="text-align:right;" id="rfc">
            Cliente RFC:&nbsp;
          </th>
          <td width="20%" style="text-align:rigth;">
            <input type="text" name="filtroComplementoPago.clienteRfc" id="clienteRfc" style="align:right;"/>
          </td>
        </tr>
        <tr class="pf" align="left">
          <th width="20%" align="left" >
            Fecha Aplicacion De Pago:&nbsp;
          </th>
          <td cosplan="3">
            <sj:datepicker name="filtroComplementoPago.fechaPago" id="fechaPago" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:14px; align:right;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
             </sj:datepicker>
          </td>
          <th width="20%" style="text-align:left;">
            Fecha Expedicion Inicial:&nbsp;
          </th>
          <td>
            <sj:datepicker name="filtroComplementoPago.fechaExpedicionInicial" id="fechaExpedicionInicial" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:14px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
             </sj:datepicker>
          </td>
          <th width="25%" style="text-align:right;" id="rfc">
            Fecha Expedicion Final:&nbsp;
          </th>
          <td>
            <sj:datepicker name="filtroComplementoPago.fechaExpedicionFinal" id="fechaExpedicionFinal" buttonImage="../img/b_calendario.gif"
               changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100"  style="font-size:14px;"
               onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
               onblur="esFechaValida(this);">
             </sj:datepicker>
          </td>
        </tr>
        <tr>
          <td colspan="6" align="right" class="td">
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  <div id="searchBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
                    <div class="btn_back w140" style="display: inline; float: right;width: 170px;" >
                      <a href="javascript: void(0);" class="icon_buscar"
                        onclick="javascript: buscarPorFiltros();">
                        <s:text name="Buscar"/>
                      </a>
                    </div>
                  </div>
                  </td>
                  <td  align="left">
                    <div id="execButon"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
                      <div class="btn_back w140" style="display: inline; float: left;width: 170px;" >
                        <a href="javascript: void(0);" class="icon_ejecutar"
                          onclick="javascript: ejecutarProceso();">
                          <s:text name="Generar Complemento Pago"/>
                        </a>
                      </div>
                    </div>
                  </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br><br>
      <div id="complementoGrid" class="h260" style="background-color:white;overflow:hidden"></div>
      <div id="pagingArea"></div><div id="infoArea"></div>
  </s:form>
</div>
