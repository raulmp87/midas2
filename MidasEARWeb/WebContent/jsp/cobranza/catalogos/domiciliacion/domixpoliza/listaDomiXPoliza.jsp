<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Domicialiaci&oacute;n por P&oacute;liza</title>
		<link rel="stylesheet" href="../../../css/cobranza/pagos/bootstrap.min.css">
		<link rel="stylesheet" href="../../../css/font-awesome.min.css">

		
		<script type="text/javascript" src="../../../js/cobranza/pagos/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="../../../js/cobranza/pagos/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../../js/cobranza/pagos/catalogos/domiXPoliza.js"></script>
		<script>
			var filtrarURL = '<s:url action="mostrar" namespace="/cobranza/catalogos/domi"/>';
			var modificarURL='<s:url action="modifica" namespace="/cobranza/catalogos/domi"/>';
			var bajaURL='<s:url action="baja" namespace="/cobranza/catalogos/domi"/>';

			$(document).ready(function(){
				<s:if test="%{ message != null }">
				$('#mensaje').modal();
				</s:if>
			});
		</script>
</head>
<body>
	<div class="content">
		<div class="col-md-12">
			<div class="">
					<legend>
						P&oacute;lizas Configuradas
					</legend>
					<div class="navbar well">
							<form id="searchForm" action="mostrar.action" class="navbar-form" method="post">
							   	<div class="row">
								<div class="col-md-3">
									<label>Centro Emisor</label>
									<input type="text" style="width: 50px" value="" class="form-control" name="relacionGrupoAgente.id.centroEmisor" maxlength="2" id="centroEmisor" placeholder="00" 
									 autocomplete="off" >
								</div>
								<div class="col-md-3">
									<label>P&oacute;liza</label>
									<input type="text" style="width: 150px" value="" class="form-control" name="relacionGrupoAgente.id.numeroPoliza" maxlength="12" id="idNumeroPoliza" placeholder="000000000000"  autocomplete="off" >
								</div>
								<div class="col-md-3">
									<label>Renovaci&oacute;n</label>
									<input type="text" style="width: 50px" value="" class="form-control" name="relacionGrupoAgente.id.numeroRenovacion" maxlength="2" id="renovacion" placeholder="00" 
									 autocomplete="off" >
								</div>
								</div>
							   	<div class="row">
								<div class="col-md-3">
								<label>Prefijo</label>
									<input type="text" style="width: 100px" value="" class="form-control" name="relacionGrupoAgente.numeroCuenta" maxlength="10" id="referencia"
									 autocomplete="off" >
								</div>
								<span class="pull-right">
									<button class="btn btn-success" id="btnSearch" type="button" onclick="filtrar()"><i class="fa fa-search"></i> Buscar</button>
									<button id="btnAgregar" class="btn btn-success" type="button" onclick="agregar()"><i class="fa fa-plus"></i> Agregar</button>
								</span>
								</div>
							</form>
						</div>
					</div><!-- Termina seccion de busqueda -->
					<div class="row">
						<div class="col-md-8">
							<div class="well">
								<form action="modifica.action" method="post" id="domiXPolizaForm">
									<table class="table table-hover" style="margin-bottom:0">
										<thead>
											<tr>
												<th>Centro Emisor</th>
												<th>Numero P&oacute;liza</th>
												<th>Renovaci&oacute;n</th>
												<th>Prefijo</th>
												<th></th>
											</tr>
										</thead>
										<tbody style="background:#fff">
											<s:if test="%{ listaRelacionGrupoAgenteDTO().isEmpty() }">
												<tr>
													<td colspan="6">
														<br>
														<p class="text-center">No hay informaci&oacute;n para mostrar.</p>
													</td>
												</tr>
											</s:if>
											<s:else>
												<s:iterator value="listaRelacionGrupoAgenteDTO" var="relacion" status="index">
													<tr>
														<td>${relacion.id.centroEmisor}</td>
														<td>${relacion.id.numeroPoliza}</td>
														<td>${relacion.id.numeroRenovacion}</td>
														<td>${relacion.numeroCuenta}</td>
														<td>
															<button class="btn btn-success" id="btnSearch" type="button" onClick="modifica(${relacion.id.centroEmisor},${relacion.id.numeroPoliza},${relacion.id.numeroRenovacion}, ${relacion.numeroCuenta})">
																<i class="fa fa-search"></i>
															</button>
															<button class="btn btn-success" id="btnSearch" type="button" onClick="baja(${relacion.id.centroEmisor},${relacion.id.numeroPoliza},${relacion.id.numeroRenovacion},${relacion.numeroCuenta})">
																<i class="fa fa-trash"></i>
															</button>
														</td>
													</tr>
												</s:iterator>
											</s:else>
										</tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
			<br>				
		</div>
	<div class="clearfix"></div>
	</div>
	</div>

	<div class="modal fade" id="mensaje">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">
							<i class="fa fa-bell"></i> Mensaje
					</h4>
				</div>
				<div class="modal-body">
					<br>
					<div style="margin-bottom: 0" class="alert alert-warning">
						<p id="textDinamico"></p><!--valor mensaje -->
					</div>
				</div><!-- /.modal-body -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div><!-- /.modal-footer -->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="window">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<form id="addForm" action="modifica.action" method="post">
						<div class="row">
							<div class="col-md-3">
								<label>Centro Emisor</label>
								<input type="text" style="width: 50px" value="" class="form-control" name="relacionGrupoAgente.id.centroEmisor" maxlength="2" 
								id="centroEmisor" placeholder="00" required autocomplete="off" >
							</div>
							<div class="col-md-4">
								<label>P&oacute;liza</label>
								<input type="text" style="width: 150px" value="" class="form-control" name="relacionGrupoAgente.id.numeroPoliza" maxlength="12" 
								id="idNumeroPoliza" placeholder="000000000000" required autocomplete="off" >
							</div>
							<div class="col-md-3">
								<label>Renovaci&oacute;n</label>
								<input type="text" style="width: 50px" value="" class="form-control" name="relacionGrupoAgente.id.numeroRenovacion" maxlength="2" 
								id="renovacion" placeholder="00" required autocomplete="off" >
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Prefijo</label>
								<input type="text" style="width: 100px" value="" class="form-control" name="relacionGrupoAgente.numeroCuenta" maxlength="10" 
								id="referencia" required autocomplete="off">
							</div>
							<div class="col-md-4" style="display:none">
								<label>Estatus</label>
								<input type="text" style="width: 100px:display:none" readOnly="true" value="1" class="form-control" name="relacionGrupoAgente.estatus" maxlength="10" 
								id="idEstatus" required autocomplete="off">
							</div>
							<br/>
						</div>
						<div class="row">
							<div class="col-md-12">
								<span class="pull-right">									
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									<button id="btnAgregar" class="btn btn-success" >Aceptar</button>
								</span>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</body>
</html>