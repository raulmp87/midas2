<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Reporte Previo de Cargo Automatico Domi y TC</title>
		<link rel="stylesheet" href="../../../css/cobranza/pagos/bootstrap.min.css">
		<link rel="stylesheet" href="../../../css/font-awesome.min.css">

		
		<script type="text/javascript" src="../../../js/cobranza/pagos/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="../../../js/cobranza/pagos/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../../js/cobranza/reportes/cargoautomaticodomitc.js"></script>
		<script>
			var generarExcelURL='<s:url action="exportExcel" namespace="/cobranza/reportes/cargoauto"/>';
			var confirmProgURL='<s:url action="saveConfrimProg" namespace="/cobranza/reportes/cargoauto"/>';

			$(document).ready(function(){
				<s:if test="%{ message != null }">
				$('#mensaje').modal();
				</s:if>
			});
		</script>
	</head>
	<body>
		<div class="content">
			<div class="col-md-12">
				<div class="">
						<legend>
							Reporte Previo de Cargo Automatico Domi y TC
						</legend>
						<div class="navbar well">
							<s:hidden id="idCalendario" name="calendarioGonherDTO.idCalendar"/>
							<s:form id="searchForm" action="mostrar.action" cssClass="navbar-form" method="post">
								<div class="row">
									<div class="col-md-3">
										<label>Fecha Programaci&oacute;n</label>
										<s:textfield style="width: 100px" cssClass="form-control" name="calendarioGonherDTO.fechaProgramacion" 
										maxlength="10" id="fecha_prog"  readOnly="readOnly"/>
									</div>
									<s:if test="calendarioGonherDTO.confirmUser==1">
										<div class="col-md-3">
											<label>Usuario Confirmo Carga Domi y TC</label>
											<s:textfield style="width: 150px" cssClass="form-control" name="calendarioGonherDTO.idUsuarioModif"
											 maxlength="10" id="usuario_modifica" readOnly="readOnly"/>
										</div>
									</s:if>
								</div>
								<br/>
								<br/>
								<div class="row">
								<span class="pull-left">
									<s:if test="calendarioGonherDTO.confirmUser==0">
										<button class="btn btn-success" id="btnConfirmProg" type="button" onclick="openModalConfirm()"><i class="fa fa-search"></i> Confirmar Carga</button>
									</s:if>
									
	<button class="btn btn-success" id="btnExporExcel" type="button" onclick="generarReporte()"><i class="fa fa-search"></i> Generar Reporte</button>
								</span>
								</div>
							</s:form>
						</div>
					</div>
				</div>
			</div>	
			<div class="modal fade" id="window">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<form id="addForm" action="modifica.action" method="post">
							<div class="row">
								<div class="col-md-12">
									<label>Confirmar</label>
									<input type="checkbox" class="form-control" name="confirmUsuario" maxlength="2" 
									id="confirmUsuario" required autocomplete="off" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<span class="pull-right">									
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
										<button type="button" id="btnAgregar" class="btn btn-success" onclick="confirmaProgramacion()">Aceptar</button>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</body>
</html>