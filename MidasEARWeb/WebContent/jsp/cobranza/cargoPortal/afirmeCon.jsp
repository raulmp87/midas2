<%@page	language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="java.util.Enumeration" %>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<style type="text/css">
.errors {
	background-color:#FFCCCC;
	border:1px solid #CC0000;
	width:400px;
	margin-bottom:8px;
}
.success {
	background-color:#DDFFDD;
	border:1px solid #009900;
	width:200px;
}
</style>
<s:set id="titulo" value="%{getText('midas.suspensiones.tituloCargoPortal')}"/>
<div class="titulo w800"><s:text name="#titulo"/></div>
<div id="divContentSearch" >
<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>
<s:if test="hasActionMessages()">
   <div class="success">
      <s:actionmessage/>
   </div>
</s:if>
	<% 
	 Enumeration<String> en = request.getParameterNames();
	%>
	<table>
	<%
		
		String name = null;
		String value = null;
			
		while(en.hasMoreElements()){
			name = (String)en.nextElement();
			value = request.getParameter(name);
		  
		%>
		<tr>
			<td><%=name %>: </td><td><%=value %></td>
		</tr>
		<%
					
		}
					
	 %>
</table>
</div>
<br/>
<br/>