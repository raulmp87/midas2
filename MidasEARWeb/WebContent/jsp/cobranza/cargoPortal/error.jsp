<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<s:url value='/js/cobranza/cargoPortal/cargoPortal.js'/>"></script>
<script type="text/javascript">
	parent.resize();
</script>
<div id="mensajeGlobal" class="prosamsg prosaerrormsg">
	<!--  <img src='/MidasWeb/img/b_no.jpg'>-->
	<p>
		<a href="javascript: void(0);" onclick="javascript: parent.cerrarVentana();">
			<s:text name="midas.componente.error.message.prosa"/>
		</a>
	</p>
</div>
