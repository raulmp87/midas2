<%
/*
#java/jsp/xhtml
################################################################################
# Nombre del Programa :FormularioPromociones.jsp                               #
# Autor               :Alan Cuevas                                             #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :P-02-0466-08                          Fecha: 28/07/2008 #
# Descripcion General :Formulario para Promociones                             #
# Programa Dependiente:N/A                                                     #
# Programa Subsecuente:N/A                                                     #
# Cond. de ejecucion  :N/A                                                     #
# Dias de ejecucion   :N/A                                      Horario:N/A    #
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :Cruz Felipe Rodr�guez L�pez                             #
# Compania            :AFIRME Seguros                                          #
# Proyecto/Procliente :N/A                                   Fecha:N/A         #
# Modificacion        :N/A                                                     #
#------------------------------------------------------------------------------#
# Numero de Parametros:N/A                                                     #
# Parametros Entrada  :N/A                                      Formato:N/A    #
# Parametros Salida   :N/A                                      Formato:N/A    #
################################################################################
*/
%>
<%@page import="com.acriter.abi.procom.model.constants.RequestParam,java.util.Hashtable,java.util.Enumeration" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario de Promociones</title>
    <link rel="stylesheet" href="https://midas2.afirme.com.mx/MidasWeb/css/agente/bootstrap.css">
	<link rel="stylesheet" href="https://segurosafirme.com.mx/MidasWeb/css/agente/bootstrap.css">
    <style>
    #form{
        padding: 20px;
    }
    #encabezado{
        height: 70px;
        background-image: url(https://www.segurosafirme.com.mx/MidasWeb/img/logo_AS.jpg);
        background-repeat: no-repeat;
        background-color:#01AA4F;
    }
    
    input[type=submit]{
        padding: 8px 40px;
    }
    </style>
</head>
<body>
    <div id="encabezado">
    </div>
    <form action="../validaciones/validaPromotion.do" id="form">
        <input type="hidden" name="merchant"  value="<%= request.getParameter("merchant")%>" />
        <input type="hidden" name="cc_number" value="<%= request.getParameter("cc_number")%>" />
        <input type="hidden" name="total"     value="<%= request.getParameter("total")%>" />
        <input type="hidden" name="order_id"  value="<%= request.getParameter("order_id")%>" />
        <div class="alert alert-warning">
            <p>Puede que su banco no tenga opciones de Diferimiento por lo que es posible que su transacci�n sea rechazada.</p>
        </div>
        <p>Elija una promoci�n: </p>
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Diferimiento</th>
                    <th>Plazo</th>
                    <th>Descripci�n</th>
                </tr>
            </thead>
            <tbody>
                <!-- -->
                <% 
                    
                    Hashtable ht = new Hashtable();
                    boolean flag=false;
                    if(request.getSession().getAttribute("promo")!= null)
                    ht =(Hashtable) request.getSession().getAttribute("promo"); 
                    Enumeration en = ht.keys();
                    String diferi = null;
                    String pagos = null;
                    String promo= null;
                    String desc = null;
                    while(en.hasMoreElements())
                        {
                            promo = (String)en.nextElement();
                            desc = (String)ht.get(promo);
                            diferi = promo.substring(0,2);
                            pagos = promo.substring(2,4); 
                %>
                <!-- -->
                <tr>
                    <td>
                        <input type="radio" name="promo" value="6">
                    </td>
                    <td>
                        <%= diferi %>
                    </td>
                    <td>
                        <%= pagos %>
                    </td>
                    <td>
                        <%= desc %>
                    </td>
                </tr>
                <%
                    flag=true;
                }
                %>
                <!-- -->
                <tr>
                    <td>
                        <input type="radio" name="promo" value="000000" checked>
                    </td>
                    <td colspan="3">Sin promoci�n</td>
                </tr>
            </tbody>
        </table>
        <div class="pull-right">
            <input type="submit" class="btn btn-primary" value="Aceptar">
        </div>
    </form>
</body>
</html>