<!DOCTYPE html>
<html>
<%--
#java/jsp/html
################################################################################
# Nombre del Programa :prosa_comercio_validaciones.jsp                         #
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :N/A                                   Fecha: N/A        #
# Descripcion General :Pagina para comercio electronico                        # 
# Programa Dependiente:N/A                                                     #
# Programa Subsecuente:N/A                                                     #
# Cond. de ejecucion  :N/A                                                     #
# Dias de ejecucion   :N/A                                      Horario:N/A    #
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10                             Fecha:14/10/10 #
# Modificacion        :Nivelacion de Procom                                    #
# Marca de cambio     :C-04-2761-10 Acriter NAC                                #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10 Fase2                       Fecha:20/01/11 #
# Modificacion        :Nivelacion de Procom Fase2                              #
# Marca de cambio     :Acriter NAC C-04-2761-10 Fase2                          #
#------------------------------------------------------------------------------#
# Numero de Parametros:N/A                                                     #
# Parametros Entrada  :N/A                                      Formato:N/A    #
# Parametros Salida   :N/A                                      Formato:N/A    #
################################################################################
--%>
<head>
<title>Purchase Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://midas2.afirme.com.mx/MidasWeb/css/agente/bootstrap.css">
<link rel="stylesheet" href="https://segurosafirme.com.mx/MidasWeb/css/agente/bootstrap.css">
<style>
	#formularioPago{
		padding: 0 20px 20px 20px;
	}

	#encabezado{
		height: 70px;
		background-image: url(https://www.segurosafirme.com.mx/MidasWeb/img/logo_AS.jpg);
		background-repeat: no-repeat;
		background-color:#01AA4F;
	}
	.page-header{
		padding-top: 0;
		margin-top: 0;
		padding-right: 20px;
	}
	
	input[type=submit]{
		padding: 8px 60px;
	}

</style>

<script type="text/javascript" src="https://midas2.afirme.com.mx/MidasWeb/struts/js/base/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="https://segurosafirme.com.mx/MidasWeb/struts/js/base/jquery-1.4.3.min.js"></script>

<script type="text/javascript">
	$(function(){
		$('#cc_name').focus();

		$('.cc_number').keyup(function(event){
			var len = $(this).val().toString().length;
			if(len == 4){
				$(this).closest('div').next().find('.cc_number').focus();
			}
	 	});

		$(':submit').click(function(e){
			e.preventDefault();
			var complete_number = '';
			$('.cc_number').each(function(index){
				complete_number += $(this).val();
			});

			$('#cc_number').val(complete_number);

			var formularioPago = $('#formularioPago');
			$(formularioPago).submit();
		});
	});
	
</script>
</HEAD>
<%@page import="java.util.Enumeration"%>
<%@page import="com.acriter.abi.procom.utils.StringHelper" %>
<%@page import="com.acriter.abi.procom.model.constants.RequestParam" %>
<BODY>
	<%
		String host = request.getParameter("host");
		String sessionid = request.getParameter("sessionid");
		Enumeration en = request.getParameterNames();

		if (host != null && !host.equals("null") && !host.equals("")
				&& sessionid != null && !sessionid.equals("null")
				&& !sessionid.equals("")) {
	%>

	<link rel=stylesheet
		href="http://<%=host%>/clear.png?session=<%=sessionid%>">
	<object type="application/x-shockwave-flash"
		data="https://<%=host%>/fp.swf" width="1" height="1" id="thm_fp">
		<param name="movie" value="https://<%=host%>/fp.swf" />
		<param name="FlashVars" value="session=<%=sessionid%>" />
	</object>
	<script src="https://<%=host%>/check.js?session=<%=sessionid%>"
		type="text/javascript"></script>
	<%
		}
	%>

	<div id="encabezado">
	</div>
	<!-- Nuevo formulario con bootstrap <-->
	
	<!-- Invalidando Session -->
	<%
		session.invalidate();
	%>

	<form name="formularioPago" action="./validaciones/valida.do" method="post" autocomplete="off" class="form-horizontal" id="formularioPago">
		<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->

			<input type="hidden" name="data_sent" value="1">


			<%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>
			<input type="hidden" name="returnContext"
				value="<%=request.getContextPath()%>" /> <input type="hidden"
				name="urlMerchant" value="<%=request.getServletPath()%>" />
			<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
			<input type="hidden" name="urlpost" value="/urlpost.jsp" /> <input
				type="hidden" name="urlerror" value="/urlpost.jsp" />
			<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
			<input type="hidden" name="acquirer" value="83"> <input
				type="hidden" name="source" value="100">


			<%
				String name = null;
				String value = null;

				while(en.hasMoreElements()){
				name = (String)en.nextElement();
				value = request.getParameter(name);
			%>
			<input type="hidden" name="<%=name%>" value="<%=value%>">
			<%
				}
			%>
			<br>
			<div class="form-group">
				<label class="col-xs-3 control-label">Monto a pagar</label>
				<div class="col-xs-9">
					<% double totalPagar = Double.parseDouble(request.getParameter("total")) / 100; %>
					<p class="form-control-static lead">$<%=totalPagar %></p>
				</div>
			</div>
		<div class="form-group">
			<label for="cc_name" class="col-xs-3 control-label">Tarjetahabiente</label>
    		<div class="col-xs-9">
      			<input type="text" class="form-control" id="cc_name" name="cc_name" placeholder="Nombre como aparece en la tarjeta" MAXLENGTH="30">
    		</div>
		</div>
		<div class="form-group">
			<label for="cc_number" class="col-xs-3 control-label">N�mero de tarjeta</label>
    			<input type="hidden" name="cc_number" id="cc_number" value="">

    			<div class="col-xs-2">
    				<input type="password" MAXLENGTH="4" class="form-control cc_number" placeholder="xxxx">
    			</div>
    			<div class="col-xs-2">
    				<input type="password" MAXLENGTH="4" class="form-control cc_number" placeholder="xxxx" >
    			</div>
    			<div class="col-xs-2">
    				<input type="password" MAXLENGTH="4" class="form-control cc_number"  placeholder="xxxx">
    			</div>
    			<div class="col-xs-2">
    				<input type="text" MAXLENGTH="4" class="form-control cc_number" placeholder="xxxx">
    			</div>
    		
		</div>
		<div class="form-group">
			<label for="cc_type" class="col-xs-3 control-label">Tipo</label>
    		<div class="col-xs-4">
      			<select name="cc_type" class="form-control">
      				<option value="Visa">VISA</option>
      				<option value="Mastercard">MasterCard</option>
      				<option value="Carnet">Carnet</option>
      			</select>
    		</div>
		</div>
		<div class="form-group">
			<label for="_cc_expmonth" class="col-xs-3 control-label">Fecha de vencimiento</label>
    		<div class="col-xs-2">
      			<select name="_cc_expmonth" class="form-control">
      				<option value="01">01</option>
      				<option value="02">02</option>
      				<option value="03">03</option>
      				<option value="04">04</option>
      				<option value="05">05</option>
      				<option value="06">06</option>
      				<option value="07">07</option>
      				<option value="08">08</option>
      				<option value="09">09</option>
      				<option value="10">10</option>
      				<option value="11">11</option>
      				<option value="12">12</option>
      			</select> 
    		</div>
    		<div class="col-xs-2">
      			<select name="_cc_expyear" class="form-control">
      				<option>2015</option>
      				<option>2016</option>
      				<option>2017</option>
      				<option>2018</option>
      				<option>2019</option>
      				<option>2020</option>
      				<option>2021</option>
      				<option>2022</option>
      				<option>2023</option>
      				<option>2024</option>
      				<option>2025</option>
      				<option>2026</option>
      				<option>2027</option>
      			</select>
    		</div>
		</div>
		<div class="form-group">
			<label for="cc_cvv2" class="col-xs-3 control-label">C�digo de Seguridad (CVV2/CVC2)</label>
    		<div class="col-xs-2">
      			<input type="password" name="cc_cvv2" class="form-control" MAXLENGTH="3">
    		</div>
		</div>
		<%

			if (host!=null) {
				if(!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_EMAIL)))
					{
		%>

		<div class="form-group">
			<label for="<%=RequestParam.PARAM_DM_EMAIL %>" class="col-xs-3 control-label">Email</label>
    		<div class="col-xs-9">
      			<input type="text" name="<%=RequestParam.PARAM_DM_EMAIL %>" value="" class="form-control">
    		</div>
		</div>
				<% } %>

		<%

				if(!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_CLIENTFIRSTNAME)))
					{
		%>
		<div class="form-group">
			<label for="<%=RequestParam.PARAM_DM_CLIENTFIRSTNAME %>" class="col-xs-3 control-label">Nombre</label>
    		<div class="col-xs-9">
      			<input type="text" name="<%=RequestParam.PARAM_DM_CLIENTFIRSTNAME %>" value="" class="form-control">
    		</div>
		</div>
		<% } %>
		<% 
			if(!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_CLIENTLASTNAME)))
				{
		%>
		<div class="form-group">
			<label for="<%=RequestParam.PARAM_DM_CLIENTLASTNAME %>" class="col-xs-3 control-label">Apellidos</label>
    		<div class="col-xs-9">
      			<input type="text" name="<%=RequestParam.PARAM_DM_CLIENTLASTNAME %>" value="" class="form-control">
    		</div>
		</div>
		<%
			 }
		 	}
		%>
		<div class="form-group">
			<div class="col-xs-3"></div>
			<div class="col-xs-9">
				<input type="submit" value="Pagar" class="btn btn-primary">
			</div>
		</div>
	</form>
	
	<!-- Fin de nuevo formulario -->
</BODY>
</HTML>
