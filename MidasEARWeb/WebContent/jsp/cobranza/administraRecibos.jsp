<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="<s:url value='/js/cobranza/notificarecibo.js'/>"></script>

<%-- <style type="text/css"> --%>
<!-- 	select { width: 150px; /* Or whatever width you want. */ }  -->
<!-- 	select.expand { width: auto; }  -->
<%-- </style> --%>

 


<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>

<s:form action = "mostrarContenedor" id="recibos" name="recibosForm">	
	<table width="880px" id="filtrosM2">
		
		<tr>
			<td 
				class="titulo" colspan="5">
				<s:text name="midas.cobranza.recibos.titulo" />
			</td>
		</tr>
		
		<tr>
			<th>
				AGENTE:
			</th>
			<td>
				<s:textfield  name="FiltroRecibos.Agente" id="Agente" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgt();"></s:textfield>
					<s:textfield id="txtId"  cssStyle="display:none" onchange="onChangeAgente();"/>
					<s:textfield id="id" cssStyle="display:none"/>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="null();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 w200" disabled="true" id="nombreCompleto"/>
			</td>
		</tr>
			
		<tr>
			<th>
			CONTRATANTE:
			</th>
			<td>
				<s:textfield  name="FiltroRecibos.Asegurado" id="Asegurado" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgt();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="null();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 w200" disabled="true" id="nombreCompleto"/>
			</td>
		</tr>
		<tr>
			<th>
			POLIZA:
			</th>
			<td>
				<s:textfield  name="FiltroRecibos.Poliza" id="Poliza" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgt();"></s:textfield>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="notificapoliza();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 w200" disabled="true" id="nombreCompleto"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_enviar"
						onclick="javascript: notifica();">
						<s:text name="midas.boton.aceptar"/>
					</a>
				</div>	
			</td>
		</tr>
		 
	</table>
	
	<div id="divCarga" style="position:absolute;"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>