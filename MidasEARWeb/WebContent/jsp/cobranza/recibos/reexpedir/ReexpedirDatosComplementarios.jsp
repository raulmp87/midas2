	<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
	<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
	<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
	<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<head>
	<link href="/MidasWeb/css/midas.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" src="/MidasWeb/struts/js/base/jquery-1.4.3.min.js"></script>	
	<script type="text/javascript" src="/MidasWeb/js/midas2/cobranza/recibos/reexpedir/reexpedirRecibos.js"></script>
	<script src="/MidasWeb/js/midas2/util.js"></script>
</head>
<div align="center">
	<s:form  id="polizaForm" name="polizaForm">
		<table width="100%" height="90%" id="filtrosM2" cellpadding="0" cellspacing="10">
			<tr class="pf">
				<th  width="120px"  style="text-align:left;">				
					Método de Pago:&nbsp;
				</th>	
				<td colspan="4" width="140px" >
					<s:select id="idMedioPago_0"
						list="formasPagoSAT"
						name="idMedioPago" 
						cssStyle="width:200px;max-width: 300px;"
						cssClass="cajaTexto jQrequired"
						listKey="clave" listValue="concepto"
						labelposition="left" 
						headerKey="" headerValue="Seleccione ..."
						 onchange="cambiarMetodoDePago(this.value)"
					/>	
				</td>				
			</tr>
			<tr class="pf" id="trNumeroTarjeta" style="display:none">
				<th  width="120px"  style="text-align:left;">				
					Número de Cuenta:&nbsp;
				</th>	
				<td colspan="4" width="140px" >
				&nbsp; <input type="text" name="numTarjeta" id="numTarjeta" width="200px" value=""    style="width: 200px;align:right;"
						  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					  onkeydown="limit18(this);" onkeyup="limit18(this);"
					  /> 	  
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="btn_back w110" style="float: right;">
						<a href="javascript: void(0);" id="reciboBuscar" class="icon_enviar"
							onclick="reexpedirComprobantes(jQuery('#idMedioPago_0').val(), jQuery('#numTarjeta').val());">
							<s:text name="midas.boton.aceptar"/>
						</a>
					</div>
				</td>		
			</tr>							
		</table>
	</s:form>
</div>