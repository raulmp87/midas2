<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="recibos" var="c" status="stat">
	<row id="${numeroRecibo}"> 
		<cell align="center"><![CDATA[0]]></cell>
		<cell>
			<s:property value="numeroPoliza" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="numeroEndoso" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="numeroExhibicion" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="numeroRecibo" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="clave" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="primaTotal" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="situacion" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="idAgente" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="numeroCliente" escapeHtml="false" escapeXml="true" /></cell>
		<cell><s:property value="nombreCliente" escapeHtml="false" escapeXml="true" /></cell>
		
		<cell>/MidasWeb/img/logoPDF_small.png^Descargar PDF^javascript: descargarComprobante("<s:property value="llaveFiscal"/>", "pdf")^_self</cell>
		<cell>/MidasWeb/img/xml-small.gif^Descargar XML^javascript: descargarComprobante("<s:property value="llaveFiscal"/>", "xml")^_self</cell>
	</row>
 	</s:iterator>
</rows>