<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reexpedir Comprobantes</title>

<script type="text/javascript" src="/MidasWeb/js/midas2/cobranza/recibos/reexpedir/reexpedirRecibos.js"></script>
<script type="text/javascript" src="/MidasWeb/js/midas2/util.js"></script>

</head>

<div align="center">
	<s:form action="listarFiltrado" id="recibosForm">
		<table width="1090px" id="filtrosM2" cellpadding="5" cellspacing="5">
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.cobranza.reexpedicion.titulo"/>
				</td>
			</tr>	
			<tr>	
				<th>
					<s:text name="midas.cobranza.reexpedicion.poliza"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.numeroPoliza" id="txtNoPoliza" cssClass="cajaTextoM2 w200 h20 "></s:textfield>
				</td>
				<th>
					<s:text name="midas.cobranza.reexpedicion.numeroRecibo"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.numeroRecibo" id="txtNoRecibo" cssClass="cajaTextoM2 w200 h20 jQnumeric jQrestrict"></s:textfield>
				</td>			
			</tr>
			
			<tr>	
				<th>
					<s:text name="midas.cobranza.reexpedicion.numeroCliente"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.idCliente" id="txtNoCliente" cssClass="cajaTextoM2 w200 h20 jQnumeric jQrestrict"></s:textfield>
				</td>
				<th>
					<s:text name="midas.cobranza.reexpedicion.numeroEndoso"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.numeroEndoso" id="txtNoEndoso" cssClass="cajaTextoM2 w50 h20 jQnumeric jQrestrict"></s:textfield>
				</td>
			</tr>		
	
			<tr>	
				<th>
					<s:text name="midas.cobranza.reexpedicion.numeroInciso"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.numeroInciso" id="txtNoInciso" cssClass="cajaTextoM2 w50 h20 jQnumeric jQrestrict"></s:textfield>
				</td>
				<th>
					<s:text name="midas.cobranza.reexpedicion.claveAgente"></s:text>
				</th>	 
				<td>
					<s:textfield name="filtroRecibo.idAgente" id="txtClaveAgente" cssClass="cajaTextoM2 w200 h20 jQnumeric jQrestrict"></s:textfield>
				</td>				
			</tr>		
	
			<tr>
				<td colspan="4">
					<div class="btn_back w110" style="float: right;">
						<a href="javascript: void(0);" id="reciboBuscar" class="icon_buscar"
							onclick="buscarRecibos(1,true,false);">
							<s:text name="midas.boton.buscar"/>
						</a>
					</div>
				</td>		
			</tr>	
		</table>
		
		<br><br><br><br>

			<table width="1090px" id="filtrosM2" cellpadding="0" cellspacing="5" >
				<tr><td colspan="2"><div id="indicador"></div></td></tr>
				<tr>
					<td class="titulo td" colspan="4">Recibos</td>
				</tr>
				<tr>
					<td colspan="6" align="right" class="td">
						<table  cellpadding="0" cellspacing="0" >
							<tr>
								<td>
								<div id="divReexpedirBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
										<div class="btn_back w140" style="display: inline; float: right;width: 170px;">
										
										<a href="javascript: void(0);" onclick="showModalReexpedirDatosComplementarios();">
						<s:text name="Reexpedir Comprobantes"/>
						&nbsp;&nbsp;<img align="middle" border='0px' alt='Reexpedir Comprobantes' title='Reexpedir Comprobantes' src='/MidasWeb/img/common/b_aceptar.gif' style="vertical-align: middle;"/> 
						             </a>
										</div>
									</div>
								</td>
								<td>
								</td>
							</tr>
						</table>				
					</td>
				</tr>	
						
			</table>		
		
		
		<div id="spacer2" style="height: 3px"></div>
		<div id="listadoGridRecibos" style="width:1090px;height:500px"></div>
		<div id="pagingArea"></div>
		<div id="infoArea"></div>		
	</s:form>
</div>

<script type="text/javascript">
	jQuery( document ).ready(function($) {
		$("#txtNoInciso").mask("9999");
		$("#txtNoEndoso").mask("99");
		
	});
</script>
</html>