<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<!--call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingAreaRecibosCosolidar_log</param>
				<param>true</param>
				<param>infoAreaRecibosCosolidar_log</param>
			</call-->
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
		</beforeInit>		
        <afterInit>
        </afterInit>
		<column id="idRecibo" type="ro" width="0" sort="int" >Id</column>
		<column id="idcheck" type="ch" width="75" sort="int"><s:text name="Seleccionar"/></column>
        <column id="recibo" type="ro" width="100" sort="str" align="center"><s:text name="Recibo"/></column>
        <column id="endoso" type="ro" width="50" sort="str" align="center"><s:text name="Endoso"/></column>
        <column id="fechaEmision" type="ro" width="100" sort="str" align="center"><s:text name="Fecha Emision"/></column>
        <column id="finivigencia" type="ro" width="100" sort="str" align="center"><s:text name="Fecha Desde"/></column>
        <column id="ffinvigencia" type="ro" width="100" sort="str" align="center"><s:text name="Fecha Hasta"/></column>
        <column id="primaneta" type="ro" width="75" sort="str" align="center"><s:text name="Prima Neta"/></column>
       	<column id="iva" type="ro" width="75" sort="str" align="center"><s:text name="IVA"/></column> 
       	<column id="primatotal" type="ro" width="75" sort="str" align="center" ><s:text name="Total"/></column>
        <column id="sitrbo" type="ro" width="60" sort="str" align="center" ><s:text name="Estatus"/></column>
        <column id="titular" type="ro" width="400" sort="str" align="center" ><s:text name="Titular"/></column>
	</head>  		
	<s:iterator value="listRecibosAConsolidarGrid" var="c" status="stat">
	<row id="${index.count}"> 
	<cell align="center"><![CDATA[${idRecibo}]]></cell> 
	<cell align="center"><![CDATA[0]]></cell>
	<cell><s:property value="recibo" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="endoso" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="fechaEmision" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="finivigencia" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="ffinvigencia" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="primaneta" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="iva" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="primatotal" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="sitrbo" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="titular" escapeHtml="false" escapeXml="true" /></cell>
	</row>
 	</s:iterator>
</rows>
