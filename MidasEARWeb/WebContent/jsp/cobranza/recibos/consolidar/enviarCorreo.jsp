	<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
	<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
	<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
	<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript">
function mostrarIndicador(){
	//mostrarIndicadorCarga('indicador');
}
jQuery(document).ready(function() {
	
 
    jQuery('#btn-submit').click(function() { 
        jQuery(".error").hide();
        var hasError = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 
        var emailaddressVal = jQuery('#txtUserEmail').val().split(",");
        if(emailaddressVal == '') {	
            jQuery("#btn-submit").before('<span class="error">Proporcione la direcci&oacute;n de correo</span>');
            hasError = true;
        }
 
         for(i = 0; i < emailaddressVal.length; i++){  
	         if(!emailReg.test(emailaddressVal[i])) { 	
	           jQuery("#btn-submit").before('<span class="error">Proporcione una direcci&oacute;n v&aacute;lida</span> ');
	           hasError = true;
	          break;
	         }
	        }

 
        if(hasError == true) { 
			//ocultarIndicadorCarga('indicador');
        	return false; }else{
			mandarCorreoReciboConsolidado(txtUserEmail,numFolioCorreoEnviar);
			}
    });
});
</script>

<script type="text/javascript" src="<html:rewrite page="/js/midas2/cobranza/recibos/consolidar/operacionesConsolidadas.js"/>"></script>

	<div id="spacer1" style="height: 10px"></div>
	<div align="center">
			<table width="650px" id="filtrosM2" cellpadding="0" cellspacing="0" style="border:1px;border-style=solid;border-color=black;" >
				<tr>
					<td class="titulo td" colspan="6">Enviar XML y PDF con el Folio Fiscal CON <b>${numFolio}</b> 
					</td>
				</tr>
				<tr>
					<th width="280px" style="text-align:left;	">				
					Recibo Consolidado:

						
					</th>	
					<td  align="right"  width="450px" style="text-align: left;" >					
						<input readonly="true" id="numFolioCorreoEnviar" type="text" width="450px" value="${numFolio}">			
					 
					</td>
	
				</tr>	
						
					<tr>
					<th width="280px" style="text-align:left;	">				
					Correo Electrónico

						
					</th>	
					<td  align="right"  width="450px" style="text-align: left;">					
							<s:textarea 
							id="txtUserEmail"  cssClass="cajaTextoM2 jQemail jQrequired" cssStyle="font-size:11px;"
							title="Correos electronicos separados por punto y coma"
							alt="Correos electronicos separados por punto y coma"
							rows="5" cols="77"  value="" />				
					 
					</td>
	
				</tr>	
				
					<tr class="pf" >
					<th width="280px" style="text-align:left;	">				
					<br><br><br>

						 
					</th>	
					<td  align="right"  width="450px" style="text-align: left;">
					 <br><br><br>
					</td>
					</tr>
					<tr class="pf" >
					<th width="280px" style="text-align:left;	">				
					<br><br><br>
					</th>	
					<td  align="right"  width="450px" style="text-align: left;">
			<input type= "button"	id="btn-submit" value="Enviar"
				cssClass="b_submit" />	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
					 <br><br><br>
					</td>
					</tr>
			</table>
						<br><br><br><br>
				<script type="text/javascript">
					jQuery( document ).ready(function($) {
						  
							res = "${mensaje}";
								if (res!=""){
											alert("${mensaje}");
											res = "";
											}
					
					});
				</script>