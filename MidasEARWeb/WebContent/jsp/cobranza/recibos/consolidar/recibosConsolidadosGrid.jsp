<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="lisConsolidadoGrid" var="c" status="stat">
	<row id="${index.count}"> 
	<cell align="center"><![CDATA[${referenciacargoconsolidado}]]></cell> 
	<cell align="center"><![CDATA[0]]></cell>
	<cell>
<s:property value="poliza" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="foliofiscal" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="referenciacargoconsolidado" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="fechaconsolidado" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="fechacorte" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="fechalimite" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="fechacreacion" escapeHtml="false" escapeXml="true" /></cell>
	<cell><s:property value="importecargoconsolidado" escapeHtml="false" escapeXml="true" /></cell>
	
	
<s:if  test="estatus == \"C\""  >
			<cell>Cancelada</cell>
			<cell><s:property value="usuariocreacion" escapeHtml="false" escapeXml="true" /></cell>	
			<cell>/MidasWeb/img/common/ico_pdfDeshabilitado.gif^Recibo Cancelado^javascript: exportarAviso("<s:property value="foliofiscal"/>")^</cell>
			<cell>/MidasWeb/img/common/ico_xmlDeshabilitado.gif^Recibo Cancelado^javascript: exportarAviso("<s:property value="foliofiscal"/>")^</cell>
			<cell>/MidasWeb/img/common/ico_email.gif^Recibo Cancelado^javascript: mandarCorreo("<s:property value="referenciacargoconsolidado"/>")^</cell>
</s:if>
<s:elseif  test="estatus == \"N\"" >
			<cell>Nueva</cell>
			<cell><s:property value="usuariocreacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/logoPDF_small.png^Descargar PDF^javascript: exportarPDF("<s:property value="foliofiscal"/>")^_self</cell>
			<cell>/MidasWeb/img/xml-small.gif^Descargar XML^javascript: exportarXML("<s:property value="foliofiscal"/>")^_self</cell>
			<cell>/MidasWeb/img/common/ico_email.gif^Enviar correo electrónico^javascript: mandarCorreo("<s:property value="referenciacargoconsolidado"/>")^_self</cell>
</s:elseif>
<s:else>
			<cell>Sin Estatus <s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="usuariocreacion" escapeHtml="false" escapeXml="true" /></cell>		
			<cell>/MidasWeb/img/common/ico_pdfDeshabilitado.gif^Recibo Sin Estatus^</cell>
			<cell>/MidasWeb/img/common/ico_xmlDeshabilitado.gif^Recibo Sin Estatus^</cell>
			<cell>/MidasWeb/img/common/ico_email.gif^Recibo Sin Estatus^</cell>
</s:else>
	</row>
 	</s:iterator>
</rows>
