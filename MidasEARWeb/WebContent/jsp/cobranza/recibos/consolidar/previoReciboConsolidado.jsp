	<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
	<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
	<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
	<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
	<style type="text/css">
	div.ui-datepicker {
		font-size: 10px;
	}
	table tr td div span label {
	    color:black;
	    font-weight: normal;
	    text-align: left;
	}
	.td{
		max-width: 150px;
	}
	</style>
	<div id="spacer1" style="height: 10px"></div>
	<div align="center">
		<s:form  id="polizaForm" name="polizaForm">
			<s:hidden name="numeroPoliza"></s:hidden>
			<s:hidden name="nombreAsegurado"  id="nombreAsegurado"/>
			<s:hidden name="fechaCreacion"  id="fechaCreacion"/>
			<s:hidden name="folioFiscal"  id="folioFiscal"/>
			<s:hidden name="accion"  id="accion"/>
			<s:hidden name="idField"></s:hidden>
			<s:hidden name="divCarga"></s:hidden>
			<table width="1090px" id="filtrosM2" cellpadding="0" cellspacing="0">
				<tr>
					<td class="titulo td" colspan="6">Consolidar Recibos</td>
				</tr>
					<tr class="pf" >
					<th width="80px" style="text-align:left;	">				
						Centro de Emisión: &nbsp  
						 
					</th>	
					<td  align="right"  width="50px" style="text-align: left;">
					  <input type="text" name="centroEmision" id="centroEmision"  value="01"  align="right" style="width:50px;align:right;"
					  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					  onkeydown="limit(this,3);" onkeyup="limit(this,3);"
					  
					  /> 
					</td>
					
					<th width="70px" style="text-align:right;">				
						Poliza: &nbsp  
						
					</th>	
					<td width="20px"  style="text-align:left;">
					<input type="text" name="numPolizaConsolidar" id="numPolizaConsolidar"      style="width: 180px;align:right;"
					  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
					onkeydown="limit(this,23);" onkeyup="limit(this,23);" 
					  />    
					</td>
					
					
					<th width="150px" style="text-align:right;">				
						  Numero de Renovación de Póliza:&nbsp;
					</th>	
					<td width="10px"  style="text-align:left;">
					  
					    <input type="text" name="numRenovPol" id="numRenovPol" width="10px" value="00"    style="width: 50px;align:right;"
						  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
						  onkeydown="limit(this,3);" onkeyup="limit(this,3);"
						  /> 
					</td>
					
					
					
				</tr>
				<tr class="pf">
						<th  width="80px"  style="text-align:left;">				
					Metodo de Pago:&nbsp;
					</th>	
					<td colspan="4" width="140px" >
												<s:select id="idMedioPago_0"
							list="medioPagoDTOs"
							name="idMedioPago" 
							cssStyle="width:200px;max-width: 300px;"
							cssClass="cajaTexto jQrequired"
							listKey="idMedioPago" listValue="descripcion"
							labelposition="left" 
							headerKey="" headerValue="Seleccione ..."
							 onchange="cambiarMetodoDePago(this.value)"
							 />	
					</td>
					<td colspan="2"></td>
				</tr>
				
					<tr class="pf" id="trNumeroTarjeta" style="display:none">
						<th  width="80px"  style="text-align:left;">				
					Numero de Tarjeta:&nbsp;
					</th>	
					<td colspan="4" width="140px" >
					&nbsp; <input type="text" name="numTarjeta" id="numTarjeta" width="200px" value=""    style="width: 200px;align:right;"
							  onkeypress="return event.charCode >= 48 && event.charCode <= 57"
						  onkeydown="limit(this,18);" onkeyup="limit(this,18);"
						  /> 	  
					</td>
					<td colspan="2"></td>
				</tr>				
				
				<tr class="pf">
						<th colspan="3">				
						Fecha de consolidado: (La fecha mensual)
					</th>	
					<td colspan="3">
						<sj:datepicker name="fechaConsolidado" id="fechaConsolidado" buttonImage="../img/b_calendario.gif"			 
						   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
						   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
		 				   onblur="esFechaValida(this);">
		 				</sj:datepicker>	
					</td>
				</tr>
						<tr class="pf">
						<th colspan="3">				
						Fecha de Corte: (Las fechas de los pago convenidos en la poliza)
					</th>	
					<td colspan="3">
						<sj:datepicker name="fechaCorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
						   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
						   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
		 				   onblur="esFechaValida(this);">
		 				</sj:datepicker>	
					</td>
				</tr>
				<tr>
					<td colspan="6" align="right" class="td">
						<table  cellpadding="0" cellspacing="0" >
							<tr>
								<td>
									<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
										<div class="btn_back w140" style="display: inline; float: right;width: 170px;" >
											<a href="javascript: void(0);" class="icon_buscar" 
												onclick="javascript: buscarRecibosDePoliza(jQuery('#fechaConsolidado').val(),jQuery('#fechaCorte').val(),jQuery('#numPolizaConsolidar').val(),jQuery('#centroEmision').val(),jQuery('#numRenovPol').val());">
												<s:text name="Buscar"/>
							               </a>
										</div>
										
									</div>
								</td>
								<td>
									<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
										<div class="btn_back w140" style="display: inline; float: right;width: 170px;">
											<a href="javascript: void(0);" class="icon_buscar" 
												onclick="javascript: consolidarRecibosSeleccionados(jQuery('#fechaConsolidado').val(),jQuery('#fechaCorte').val(),jQuery('#numPolizaConsolidar').val());">
												&nbsp;&nbsp;<s:text name="Consolidar Recibos"/>&nbsp;&nbsp;
							               </a>
										</div>
									</div>
								</td>
								<td>
								<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
										<div class="btn_back w140" style="display: inline; float: right;width: 170px;">
										
										<a href="javascript: void(0);" onclick="exportarExcel();">
						<xs:text name="midas.boton.exportarExcel" />
						<s:text name="Exportar Excel"/>
						&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Excel' title='Exportar Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
						             </a>
										</div>
									</div>
								</td>
								<td>
								</td>
	
							</tr>
						</table>				
					
					</td>
				</tr>	
			</table>
			<!--TABLA PRINCIPAL DE BUSQUEDA DE RECIBOS PARA CONSOLIDAR-->
		<div id="divSelectAll">
		<div width="1090px">
					<table width="1090px" id="filtrosM2" cellpadding="0" cellspacing="0" >
						<tr class="pf" >
					<th width="200px">				
						<s:checkbox name="checkAll" id="checkAll" value="false"  label="Seleccionar Todos" onclick="checkUncheckAll();" labelposition="left" />
					</th>	
						<th  width="300px">
						Cantidad de Recibos Seleccionado:&nbsp;
					<input type="text" name="numeroRecibos" id="numeroRecibos" style="
    width: 40px; text-align: center">
		</th >	
					<th  width="300px">
					  Suma Total&nbsp;:<input type="text" name="sumaTotal" id="sumaTotal"  style="
    width: 150px;text-align: right
">
					</th >
				</tr>
			</table>
		</div>
		</div>
	<div id="spacer2" style="height: 3px"></div>
	<div id=pagingAreaRecibosCosolidar_log></div>
	<div id="infoAreaRecibosCosolidar_log"></div>
	<div id="logListadoGridRecibosCosolidar" style="width:1090px;height:200px"></div>
			
	<br><br><br><br>
	<!--TABLA DE RECIBOS CONSOLIDADOS-->
			<table width="1090px" id="filtrosM2" cellpadding="0" cellspacing="0" >
				<tr>
					<td class="titulo td" colspan="4">Recibos Consolidados</td>
				</tr>
				<tr>
					<td colspan="6" align="right" class="td">
						<table  cellpadding="0" cellspacing="0" >
							<tr>
								<td>
									<div id="divCancelarPolizas"  class="w150" style="float:right; padding-right: 10px;">
										<div class="btn_back w140" style="display: inline; float: right;">
											<a href="javascript: void(0);" class="icon_buscar" 
												onclick="javascript: cancelarPolizas(jQuery('#fechaConsolidado').val(),jQuery('#fechaCorte').val(),jQuery('#numPolizaConsolidar').val());">
												<s:text name="Cancelar"/>
							               </a>
										</div>
										
									</div>
								</td>
											<td>
								<div id="divExcelBtn"  class="w150" style="float:right; padding-right: 10px;width: 170px;">
										<div class="btn_back w140" style="display: inline; float: right;width: 170px;">
										
										<a href="javascript: void(0);" onclick="exportarExcelConsolidado();">
						<xs:text name="midas.boton.exportarExcel" />
						<s:text name="Exportar Excel"/>
						&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Excel' title='Exportar Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
						             </a>
										</div>
									</div>
								</td>
								<td>
								</td>
							</tr>
						</table>				
					</td>
				</tr>	
			</table>
		<div id="divSelectAllCancelar">
		<div class ="w900 inline" style="width:1090px">
			<table width="1090px" id="filtrosM2" cellpadding="0" cellspacing="0" >
						<tr class="pf" >
					<th width="200px">				
						<s:checkbox name="checkAllCancelar" id="checkAllCancelar" value="false"  label="Seleccionar Todos" onclick="checkUncheckAllCancelar();" labelposition="left"/>
					</th>	
						<th  width="10px">
		</th >	
					<th  width="210px">
					Busqueda de Recibos Consolidados y Polizas:
					</th >
					<th  width="80px"><input type="text" id="txtFiltroConsolidado">
		</th >	
		<th  width="300px">
		<input type="Button" value="Buscar" onclick="applyFilter()">
		</th >	
				</tr>
			</table>
		</div>
		</div>
	<div id="spacer2" style="height: 3px"></div>
	<div id="pagingAreaRecibosConsolidados_log"></div>
	<div id="infoAreaRecibosConsolidados_log"></div>
	<div id="logListadoGridRecibosConsolidados" style="width:1090px;height:500px"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
	<s:hidden property value="mensaje" />
	</s:form>
	<div id="loadingIndicatorComps" style="display: none;"></div>
<script type="text/javascript">
	jQuery( document ).ready(function($) {
		  	getGridConsolidados();
			$("#numeroRecibos").attr('disabled', 'disabled');
			$("#sumaTotal").attr('disabled', 'disabled');
			document.getElementById('divSelectAll').style.display='none'; 
	  		res = "${mensaje}";
	  			if (res!=""){
		  					alert("${mensaje}");
		  					res = "";
		  					}
	
	});
</script>