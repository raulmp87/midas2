    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgProveedor.js'/>"></script>

<s:form id="cgPform" name="cgPform"  cssStyle="background:white;">
<table width="98%" border="0" id="filtros">
	<tbody>
		<tr>
			<td colspan="6" class="titulo">
				<s:label value="Proveedores" />
			</td>
		</tr>
		<tr>			
			<td>
				<s:textfield  cssClass="txtfield" cssStyle="width: 300px;"
					key="midas.condicionesGenerales.proveedor.nombre"
			        labelposition="left"
					id="cgProveedor.nombrePrestador" name="cgProveedor.nombrePrestador"
					/>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			</td>
			<td colspan="2" class="buscar">
				<div style="display: block;" class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a id="submit" href="javascript: void(0);" onclick="traerListaProveedor();" class="icon_buscar">
					<s:text name="midas.boton.buscar" /> </a>
					</div>
				</div>
			</td>      		
		</tr>
	</tbody>
</table>
</s:form>

<div>
	<div id="indicador"></div>
	<div id="cgProveedorGrid" style="width: 98%; height: 230px; display: none;"></div>
	<div id="pagArea_CgProveedor"></div>
	<div id="infoArea_CgProveedor"></div>
</div>

<div style="margin-right: 20px" class="alinearBotonALaDerecha">
	<div id="b_agregar">
			<a id="submit" href="javascript: void(0);" onclick="crearCgProveedor();">
					<s:text name="midas.boton.agregar" /> 
			</a>	
	</div>
</div>
