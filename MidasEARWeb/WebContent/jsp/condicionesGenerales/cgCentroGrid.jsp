<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea_CgCentros</param>
				<param>true</param>
				<param>infoArea_CgCentros</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="numero" type="ro" width="100"><s:text name="midas.condicionesGenerales.centro.numero" /></column>
		<column id="nombre" type="ro" width="550"><s:text name="midas.condicionesGenerales.centro.centro" /></column>
		<column id="situacion" type="ro" width="350"><s:text name="midas.condicionesGenerales.centro.situacion" /></column>
		<column id="acciones" type="img" width="*"><s:text name="midas.condicionesGenerales.centro.acciones" /></column>
		<column id="acciones" type="img" width="*"></column>
</head>
	<s:iterator value="cgCentroList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">		
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="gerencia.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus==1">
				<cell><s:property value="'Activo'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:if test="estatus!=1">
				<cell><s:property value="'Inactivo'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarCgCentro("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			
			<s:if test="estatus==1">
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarCgCentro("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
			<s:if test="estatus!=1">
			<cell>/MidasWeb/img/icons/ico_aceptar.gif^Activar^javascript: activarCgCentro("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
			
			
			
						
		</row>
	</s:iterator>	
</rows>