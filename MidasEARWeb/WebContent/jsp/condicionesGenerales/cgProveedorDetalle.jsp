<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgProveedor.js'/>"></script>

<s:if test="cgProveedor.id != null">	
	<s:set id="readOnly" value="true" />
</s:if>
<s:else>
    <s:set id="readOnly" value="false" />
</s:else>

<s:form action="editarProveedor" id="cgPform" name="cgPform"  cssStyle="background:white;">
<s:hidden id="idCgProveedor" name="cgProveedor.id" value="%{cgProveedor.id}"></s:hidden>

<div hrefmode="ajax-html" style="height: 430px; width: 920px" id="cotizacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="150px" id="detalle" name="Datos generales" href="http://void" extraAction="javascript:jQuery('#contenido_detalle').css('overflow','none')">
		<div>
			<table width="100%" border="0" class="contenedorConFormato">
				<tbody>
					<tr>
						<td colspan="6" class="titulo">
							<s:label value="Proveedor Condiciones Generales" />
						</td>
					</tr>
					<tr>
					<td>
						<s:select name="cgProveedor.tcprestadorservicioId" id="cgProveedor.tcprestadorservicioId" value="cgProveedor.prestadorServicio.id"  list="prestadorServicioList" 
						listKey="id" listValue="persona.nombre" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
						cssClass="cajaTexto" cssStyle="width: 300px;" labelposition="%{labelPosition}" key="midas.condicionesGenerales.proveedor.proveedor"
						disabled="%{readOnly}"/>			
					</td>
					</tr>
					<tr>			
						<td>
							<s:textfield  cssClass="txtfield" cssStyle="width: 300px;"
								key="midas.condicionesGenerales.proveedor.contacto"
						        labelposition="left"
								id="cgProveedor.contacto" name="cgProveedor.contacto"
								/>
						</td>
					</tr>
			  		<tr>
						<td colspan="4"><s:textarea id="cgProveedor.observaciones" name="cgProveedor.observaciones" cssClass="cajaTexto"
						key="midas.condicionesGenerales.proveedor.observaciones"></s:textarea></td>
					</tr>
					<tr>
						<td class="guardar"  colspan="6" align="right">
						<table>
							<tr>
								<td><div style="display: block;" class="alinearBotonALaDerecha">
										<div id="b_regresar">
											<a id="submit" href="javascript: void(0);" onclick="irInicioProveedor();">
										<s:text name="midas.boton.regresar" /> </a>
										</div>
									</div>
								</td>
								<td>
									<div style="display: block;" class="alinearBotonALaDerecha">
											<div id="b_guardar">
												<a id="submit" href="javascript: void(0);" onclick="guardarCgProveedor();">
											<s:text name="midas.boton.guardar" /> </a>
											</div>
										</div>
									</td>
								</tr>						
							</table>							
						</td>    		
					</tr>			
				</tbody>
			</table>
		</div>
	</div>
	<div width="150px" id="complementar" name="Centro emisor" href="http://void" extraAction="javascript:jQuery('#contenido_detalle').css('overflow','none');cargarlistaCentro();">
		<p>Lista de Centros Emisores.</p>
		<div id="listCentroEmisorGrid" style="height: 300px; width: 850"></div>
		
		<div style="display: block" id="masCentros">
		
			<div>
				<a href="javascript: void(0);"
					onclick="toggle_Hidden();ocultarMostrarBoton('masCentros');">
					<s:text name="midas.condicionesGenerales.proveedor.agregarCentro"/>
				</a>
			</div>

		</div>
		<div style="display:none" id="menosCentros">
			<div>
				<a href="javascript: void(0);"
					onclick="toggle_Hidden();ocultarMostrarBoton('menosCentros');">
					<s:text name="midas.condicionesGenerales.proveedor.ocultarCentro"/>
				</a>
			</div>
			<div style="display: inline; float: left; margin-top: 5px;">
				<s:select id="idToCentro" name="idToCentro" 
										list="cgCentrosList" 
										headerKey="%{getText('midas.general.defaultHeaderKey')}" 
										headerValue="%{getText('midas.general.seleccione')}"
										cssClass="cajaTextoM2 jQrequired w200 wide"
										key="midas.condicionesGenerales.centro.centro"/>
			</div>
			<div class="btn_back w140" style="display: inline; float: left; margin-top: 30px;">
				<a id="submit" href="javascript: void(0);"
					onclick="guardarCentroProveedor();" class="icon_agregar">
					<s:text name="midas.condicionesGenerales.proveedor.agregar" />
				</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>
</s:form>