<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagArea_CgProveedor</param>
				<param>true</param>
				<param>infoArea_CgProveedor</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>			
		</beforeInit>		
		<column id="id" type="ro" width="100"><s:text name="midas.condicionesGenerales.proveedor.id" /></column>
		<column id="proveedor" type="ro" width="350"><s:text name="midas.condicionesGenerales.proveedor.proveedor" /></column>
		<column id="tipo" type="ro" width="250"><s:text name="midas.condicionesGenerales.proveedor.tipo" /></column>
		<column id="situacion" type="ro" width="150"><s:text name="midas.condicionesGenerales.proveedor.situacion" /></column>
		<column id="acciones" type="img" width="*"><s:text name="midas.condicionesGenerales.proveedor.acciones" /></column>
		<column id="acciones" type="img" width="*"></column>
		<column id="acciones" type="img" width="*"></column>
</head>
	<s:iterator value="cgProveedorList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">		
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="prestadorServicio.persona.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="prestadorServicio.getTipoPrestadoresStr()" escapeHtml="false" escapeXml="true"/></cell>
			
			<s:if test="prestadorServicio.estatus==1">
				<cell><s:property value="'Activo'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:if test="prestadorServicio.estatus!=1">
				<cell><s:property value="'Inactivo'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			
<!--		<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: verCgProveedor("<s:property value="id" escapeHtml="false" escapeXml="true"/>","<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell> -->
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarCgProveedor("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarCgProveedor("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
		</row>
	</s:iterator>	
</rows>