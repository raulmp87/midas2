    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgCentro.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>

<s:form id="cgCentroForm" name="cgCentroForm"
	cssStyle="background:white;">
	<table width="98%" border="0" id="filtros">
		<tbody>
			<tr>
				<td colspan="6" class="titulo"><s:label value="Centro Emisor" />
				</td>
			</tr>
			<tr>
				<td><s:textfield cssClass="txtfield" cssStyle="width: 300px;"
						key="midas.condicionesGenerales.proveedor.nombre"
						labelposition="left" id="cgCentro.gerencia.descripcion" name="cgCentro.gerencia.descripcion" />
				</td>
			</tr>
			<tr>
				<td colspan="4"></td>
				<td colspan="2" class="buscar">
					<div style="display: block;" class="alinearBotonALaDerecha">
						<div id="b_buscar">
							<a id="submit" href="javascript: void(0);"
								onclick="traerListaCentro();" class="icon_buscar"> <s:text
									name="midas.boton.buscar" /> </a>
						</div>
					</div></td>
			</tr>
		</tbody>
	</table>
</s:form>

<div>
	<div id="indicador"></div>
	<div id="cgCentroGrid" style="width: 98%; height: 230px; display: none;""></div>
	<div id="pagingArea_CgCentros"></div>
	<div id="infoArea_CgCentros"></div>
</div>

<div style="margin-right: 20px" class="alinearBotonALaDerecha">
	<div id="b_agregar">
		<a id="submit" href="javascript: void(0);" onclick="crearCgCentro();">
			<s:text name="midas.boton.agregar" /> </a>
	</div>
</div>