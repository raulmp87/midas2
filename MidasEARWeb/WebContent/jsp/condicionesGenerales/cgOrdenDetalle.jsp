<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgOrden.js'/>"></script>

<s:set id="requiredField" value="true" />
<s:if test="cgOrden.id != null">	
	<s:set id="readOnly" value="true" />
</s:if>
<s:else>
    <s:set id="readOnly" value="false" />
</s:else>
<s:if test="isAgente != null">	
	<s:set id="isAgente" value="true" />
</s:if>
<s:else>
    <s:set id="isAgente" value="false" />
</s:else>

<s:form action="editarOrden" id="cgOrdenForm" name="cgOrdenForm"
	cssStyle="background:white;">
	<s:hidden id="idToOrden" name="idToOrden" value="%{cgOrden.id}"></s:hidden>
	<s:hidden id="esAgente" name="esAgente" value="%{isAgente}"></s:hidden>
	<table width="75%" id="filtros">
		<tbody>
			<tr>
				<td colspan="4" class="titulo"><label><s:text
							name="midas.condicionesGenerales.orden.editar" /> </label></td>
			</tr>
			<tr>
				<td>
					<table width="90%" class="contenedorDomicilio">
						<tbody>
							<tr>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 100px;"
										key="midas.condicionesGenerales.orden.cantidad"
										labelposition="top" id="cgOrden.cantidad"
										name="cgOrden.cantidad" disabled="%{readOnly}"
										onkeypress="return soloNumerosM2(this, event, false)"
										required="#requiredField"/></td>
								<td>
								<sj:datepicker id="cgOrden.fecha" name="cgOrden.fecha"
										cssStyle="width: 170px;"
										key="midas.condicionesGenerales.orden.fecha"
										labelposition="top" buttonImage="../img/b_calendario.gif"
										changeMonth="true" changeYear="true"
										maxlength="10" cssClass="txtfield" size="12"
										onkeypress="return soloFecha(this, event, false);"
										required="#requiredField"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
								</td>
							</tr>
							<tr>
								<td colspan="2"><s:textarea id="cgOrden.motivos"
										name="cgOrden.motivos" cssClass="cajaTexto"
										cssStyle="width: 500px;"
										key="midas.condicionesGenerales.orden.motivos"
										labelposition="left" 
										required="#requiredField"/>
								</td>
								<td><s:select id="cgOrden.tocgcentroId" name="cgOrden.tocgcentroId" 
									value="cgOrden.tocgcentroId" list="cgCentrosList" 
									headerKey="%{getText('midas.general.defaultHeaderKey')}" 
									headerValue="%{getText('midas.general.seleccione')}"
									cssClass="cajaTextoM2 jQrequired w200 wide"
									key="midas.condicionesGenerales.centro.centro"
									disabled="%{isAgente}"/>
								</td>
							</tr>

						</tbody>
					</table></td>
				<td></td>
			</tr>

			<tr>
				<td class="guardar" colspan="6" align="right">
					<table>
						<tr>
							<td><div style="display: block;"
									class="alinearBotonALaDerecha">
									<div id="b_regresar">
										<a id="submit" href="javascript: void(0);"
											onclick="irInicioOrden();"> <s:text
												name="midas.boton.regresar" /> </a>
									</div>
								</div>
							</td>
							<td>
								<div style="display: block;" class="alinearBotonALaDerecha">
									<div id="b_guardar">
										<a id="submit" href="javascript: void(0);"
											onclick="actualizarGuardarOrden();"> <s:text
												name="midas.boton.guardar" /> </a>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</s:form>