<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea_CgOrdenes</param>
				<param>true</param>
				<param>infoArea_CgOrdenes</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="numero" type="ro" width="100"><s:text name="midas.condicionesGenerales.orden.numero" /></column>
		<column id="agente" type="ro" width="300"><s:text name="midas.condicionesGenerales.agente.agente" /></column>
		<column id="centro" type="ro" width="300"><s:text name="midas.condicionesGenerales.centro.centro" /></column>
		<column id="cantidad" type="ro" width="*"><s:text name="midas.condicionesGenerales.orden.cantidad" /></column>
		<column id="tipo" type="ro" width="*"><s:text name="midas.condicionesGenerales.orden.tipo" /></column>
		<column id="fechaPedido" type="ro" width="*"><s:text name="midas.condicionesGenerales.orden.fechaPedido" /></column>
		<column id="fechaEntrega" type="ro" width="*"><s:text name="midas.condicionesGenerales.orden.fechaEntrega" /></column>
		<column id="confirmar" type="img" width="*"><s:text name="midas.condicionesGenerales.orden.confirmar" /></column>
</head>
	<s:iterator value="cgOrdenList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">		
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombrecompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cgCentro.gerencia.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cantidad" escapeHtml="false" escapeXml="true"/></cell>
			
			<s:if test="tipo=='PET'">
				<cell><s:property value="'PETICION'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:if test="tipo!='PET'">
				<cell><s:property value="'AUTOMATICA'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			
			<cell><s:property value="fecha" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fecha_entrega" escapeHtml="false" escapeXml="true"/></cell>			
			<s:if test="estatus==1">
			<cell>/MidasWeb/img/checked.gif^Confirmada^</cell>
			</s:if>
			<s:if test="estatus==0">
			<cell>/MidasWeb/img/icons/ico_terminar.gif^Confirmar^javascript: confirmarCgOrden("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
		</row>
	</s:iterator>	
</rows>