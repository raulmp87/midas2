<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgCentro.js'/>"></script>

<s:if test="cgCentro.id != null">	
	<s:set id="readOnly" value="true" />
</s:if>
<s:else>
    <s:set id="readOnly" value="false" />
</s:else>

<s:if test="cgCentro.estatus == 1">	
	<s:set id="centroEstatus" value="'ACTIVO'" />
</s:if>
<s:else>
    <s:set id="centroEstatus" value="'INACTIVO'" />
</s:else>

<s:form action="editarCentro" id="cgCentroForm" name="cgCentroForm"
	cssStyle="background:white;">
	<s:hidden id="idToCentro" name="idToCentro" value="%{cgCentro.id}"></s:hidden>
	<table width="75%" id="filtros">
		<tbody>
			<tr>
				<td colspan="4" class="titulo"><label><s:text
							name="midas.condicionesGenerales.centro.editar" />
				</label>
				</td>
			</tr>
			<tr>
				<td>
					<table width="90%" class="contenedorDomicilio">
						<tbody>
						<tr>										
								<td><s:select id="cgCentro.id" name="cgCentro.id" 
									value="cgCentro.id" list="gerenciaList" 
									headerKey="%{getText('midas.general.defaultHeaderKey')}" 
									headerValue="%{getText('midas.general.seleccione')}"
									cssClass="cajaTextoM2 jQrequired w200 wide"
									key="midas.condicionesGenerales.centro.centro"
									disabled="%{readOnly}"/>
								</td>
																		
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.situacion"
										labelposition="left" value="%{centroEstatus}" 
										id="cgCentro.estatus"
										name="cgCentro.estatus" disabled="true"/>
								</td>
							</tr>
							<tr>
								<td><s:select id="cgCentro.cvePais"
										name="cgCentro.cvePais" list="paises"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										onchange="onChangePais('cgCentro.cveEstado','cgCentro.cveCiudad','cgCentro.colonia','cgCentro.codigoPostal','cgCentro.calleNumero','cgCentro.cvePais')"
										key="midas.condicionesGenerales.centro.pais"
										labelposition="left" /></td>
								<td><s:select id="cgCentro.cveEstado"
										name="cgCentro.cveEstado" list="estados"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										onchange="onChangeEstadoGeneral('cgCentro.cveCiudad','cgCentro.colonia','cgCentro.codigoPostal','cgCentro.calleNumero','cgCentro.cveEstado')"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.estado"
										labelposition="left" /></td>
							</tr>
							
							<tr>
								<td><s:select id="cgCentro.cveCiudad"
										name="cgCentro.cveCiudad" list="ciudades"
										onchange="onChangeCiudad('cgCentro.colonia','cgCentro.codigoPostal','cgCentro.calleNumero','cgCentro.cveCiudad')"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.municipio"
										labelposition="left" /></td>
								<td><s:select id="cgCentro.colonia"
										name="cgCentro.colonia" list="colonias"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										onchange="onChangeColonia('cgCentro.codigoPostal','cgCentro.calleNumero', 'cgCentro.colonia','cgCentro.cveCiudad')"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.colonia"
										labelposition="left" /></td>
							</tr>							
							<tr>
								<td><s:textfield id="cgCentro.codigoPostal"
										name="cgCentro.codigoPostal"
										onchange="onChangeCodigoPostal(this.value, 'cgCentro.colonia','cgCentro.cveCiudad','cgCentro.cveEstado','cgCentro.calleNumero','cgCentro.cvePais','cgCentro.codigoPostal');"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.codigo"
										labelposition="left" /></td>
								<td><s:textfield id="cgCentro.calleNumero"
										name="cgCentro.calleNumero" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.calleNumero"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgCentro.personaRecibe"
										name="cgCentro.personaRecibe" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.personaRecibe"
										labelposition="left" /></td>
								<td><s:textfield id="cgCentro.telefonoRecibe"
										name="cgCentro.telefonoRecibe" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.telefono"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td colspan="4"><s:textarea id="cgCentro.observaciones"
										name="cgCentro.observaciones" cssClass="cajaTexto"
										cssStyle="width: 500px;"
										key="midas.condicionesGenerales.centro.observaciones"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgCentro.inventario"
										name="cgCentro.inventario" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.inventario"
										labelposition="left" /></td>
								<td><s:textfield id="cgCentro.ventas"
										name="cgCentro.ventas" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.ventas"
										labelposition="left" /></td>
							</tr>
							<tr>								
								<td><s:textfield id="cgCentro.promedioSurtir"
										name="cgCentro.promedioSurtir" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.promedio"
										labelposition="left" /></td>
								<td><s:textfield id="cgCentro.sobrado"
										name="cgCentro.sobrado" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.centro.sobrado"
										labelposition="left" /></td>
							</tr>
							</tbody>
							</table>
						</td>
				</tr>
				<tr>
				<td class="guardar" colspan="6" align="right">
					<table>
						<tr>
							<td><div style="display: block;"
									class="alinearBotonALaDerecha">
									<div id="b_regresar">
										<a id="submit" href="javascript: void(0);"
											onclick="irInicioCentro();"> <s:text
												name="midas.boton.regresar" /> </a>
									</div>
								</div></td>
							<td>
								<div style="display: block;" class="alinearBotonALaDerecha">
									<div id="b_guardar">
										<a id="submit" href="javascript: void(0);"
											onclick="actualizarGuardarCentro();"> <s:text
												name="midas.boton.guardar" /> </a>
									</div>
								</div></td>
						</tr>
					</table></td>
			</tr>
	</tbody>
	</table>
</s:form>