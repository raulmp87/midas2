    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgOrden.js'/>"></script>

<s:form id="cgOrdenForm" name="cgOrdenForm"  cssStyle="background:white;">
	<table width="98%" id="filtros">
		<tbody>
			<tr>
				<td colspan="6" class="titulo">
					<s:label value="Ordenes de Distribucion" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sj:datepicker name="cgOrden.fechaInicio" cssStyle="width: 170px;"
						key="midas.condicionesGenerales.orden.fechaInicial"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td> 
		    	<td colspan="2">
					<sj:datepicker name="cgOrden.fechaFin" cssStyle="width: 170px;"
						key="midas.condicionesGenerales.orden.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			 	</td>
			 </tr>
			 <tr>			
			 	<td colspan="4">
				</td>
				<td colspan="2" class="buscar">
					<div style="display: block;" class="alinearBotonALaDerecha">
						<div id="b_buscar">
							<a id="submit" href="javascript: void(0);" onclick="traerListaOrden();" class="icon_buscar">
						<s:text name="midas.boton.buscar" /> </a>
						</div>
					</div>
				</td>      		
			</tr>
		</tbody>
	</table>
</s:form>

<div>
	<div id="indicador"></div>
	<div id="cgOrdenGrid" style="width: 98%; height: 230px; display:none;"></div>
	<div id="pagingArea_CgOrdenes"></div>
	<div id="infoArea_CgOrdenes"></div>
</div>
<div style="margin-right: 20px" class="alinearBotonALaDerecha">
	<div id="b_agregar">
			<a id="submit" href="javascript: void(0);" onclick="crearCgOrden();">
					<s:text name="midas.boton.agregar" /> 
			</a>	
	</div>
</div>