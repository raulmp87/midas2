<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea_CgAgentes</param>
				<param>true</param>
				<param>infoArea_CgAgentes</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="numero" type="ro" width="100"><s:text name="midas.condicionesGenerales.agente.numero" /></column>
		<column id="nombre" type="ro" width="350"><s:text name="midas.condicionesGenerales.agente.nombre" /></column>
		<column id="ejecutivo" type="ro" width="250"><s:text name="midas.condicionesGenerales.agente.ejecutivo" /></column>
		<column id="promotoria" type="ro" width="250"><s:text name="midas.condicionesGenerales.agente.promotoria" /></column>
		<column id="situacion" type="ro" width="100"><s:text name="midas.condicionesGenerales.agente.situacion" /></column>
		<column id="acciones" type="img" width="*"><s:text name="midas.condicionesGenerales.agente.acciones" /></column>
<!-- 	<column id="acciones" type="img" width="*"></column>  -->
		<column id="acciones" type="img" width="*"></column>
</head>
	<s:iterator value="cgAgenteViewList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">		
			<cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ejecutivo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="promotoria" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoSituacion" escapeHtml="false" escapeXml="true"/></cell>
			
<!--		<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: verCgProveedor("<s:property value="id" escapeHtml="false" escapeXml="true"/>","<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell> -->
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarCgAgente("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarCgAgente("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>			
		</row>
	</s:iterator>	
</rows>