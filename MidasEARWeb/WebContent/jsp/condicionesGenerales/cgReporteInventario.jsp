<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgReporteInventario.js'/>"></script>

<s:form id="cgReporteForm" name="cgReporteForm"
	cssStyle="background:white;">
	<table width="98%" border="0" id="filtros">
		<tbody>
			<tr>
				<td colspan="4" class="titulo"><label><s:text
							name="midas.condicionesGenerales.reporte.title" /> </label></td>
			</tr>
			<tr>
				<td colspan="2"><s:select id="idCentro" name="idCentro"
						list="cgCentrosList"
						headerKey="%{getText('midas.general.defaultHeaderKey')}"
						headerValue="%{getText('midas.general.seleccione')}"
						cssClass="cajaTextoM2 jQrequired w200 wide"
						key="midas.condicionesGenerales.centro.centro" /></td>
<%-- 				<td><sj:datepicker id="mesAnio" name="mesAnio" --%>
<!-- 						cssStyle="width: 170px;" key="midas.condicionesGenerales.reporte.mesAnio" -->
<!-- 						labelposition="top"	buttonImage="../img/b_calendario.gif"  -->
<!-- 						maxDate="today" changeMonth="true" changeYear="true" -->
<!-- 						maxlength="10" cssClass="txtfield" size="12" -->
<!-- 						onkeypress="return soloFecha(this, event, false);" -->
<%-- 						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker> --%>
<!-- 				</td> -->

			</tr>
			<tr>
				<td colspan="4"></td>
				<td colspan="2" class="buscar">
					<div style="display: block;" class="alinearBotonALaDerecha">
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" href="javascript: void(0);"
								onclick="obtenerReporteInventario();" class="icon_buscar">
								<s:text name="midas.condicionesGenerales.reporte.generar" /> </a>
						</div>
					</div></td>
			</tr>
		</tbody>
	</table>
</s:form>

<div>
	<div id="indicador"></div>
	<div id="cgOrdenGrid"
		style="width: 98%; height: 230px; display: none;""></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</div>