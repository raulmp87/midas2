<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>	
		<column id="nombre" type="ro" width="550"><s:text name="midas.condicionesGenerales.centro.centro" /></column>
		<column id="acciones" type="img" width="*"><s:text name="midas.condicionesGenerales.centro.acciones" /></column>
	</head>
	<s:iterator value="centrosProveedorList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">	
			<cell><s:property value="gerencia.descripcion" escapeHtml="false" escapeXml="true"/></cell>			
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarCentroProveedor("<s:property value="id" escapeHtml="false" escapeXml="true"/>")^_self</cell>			
		</row>
	</s:iterator>	
</rows>