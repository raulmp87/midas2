<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgAgente.js'/>"></script>

<s:if test="cgAgente.id != null">	
	<s:set id="readOnly" value="true" />
</s:if>
<s:else>
    <s:set id="readOnly" value="false" />
</s:else>

<s:form action="editarAgente" id="cgAgenteForm" name="cgAgenteForm"
	cssStyle="background:white;">
	<s:hidden id="cgAgente.id" name="cgAgente.id" value="%{cgAgente.id}"></s:hidden>

	<table width="75%" id="filtros">
		<tbody>
			<tr>
				<td colspan="4" class="titulo"><label><s:text
							name="midas.condicionesGenerales.agente.editar" />
				</label>
				</td>
			</tr>
			<tr>
				<td>
					<table width="90%" class="contenedorDomicilio">
						<tbody>
							<tr>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.numero"
										labelposition="right" id="cgAgente.cgAgenteView.idAgente"
										name="cgAgente.cgAgenteView.idAgente" disabled="%{readOnly}" /></td>
							</tr>
							<tr>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.agente"
										labelposition="left"
										id="cgAgente.cgAgenteView.nombreCompleto"
										name="cgAgente.cgAgenteView.nombreCompleto"
										disabled="%{readOnly}" /></td>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.gerencia"
										labelposition="left" id="cgAgente.cgAgenteView.gerencia"
										name="cgAgente.cgAgenteView.gerencia" disabled="%{readOnly}" />
								</td>
							</tr>
							<tr>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.ejecutivo"
										labelposition="left" id="cgAgente.cgAgenteView.ejecutivo"
										name="cgAgente.cgAgenteView.ejecutivo" disabled="%{readOnly}" />
								</td>
								<td><s:textfield cssClass="txtfield"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.promotoria"
										labelposition="left" id="cgAgente.cgAgenteView.promotoria"
										name="cgAgente.cgAgenteView.promotoria"
										disabled="%{readOnly}" /></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<table width="90%" class="contenedorDomicilio">
						<tbody>
							<tr>
								<td><s:select id="cgAgente.cvePais"
										name="cgAgente.cvePais" list="paises"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										onchange="onChangePais('cgAgente.cveEstado','cgAgente.cveCiudad','cgAgente.colonia','cgAgente.codigoPostal','cgAgente.calleNumero','cgAgente.cvePais')"
										key="midas.condicionesGenerales.agente.pais"
										labelposition="left" /></td>
								<td><s:select id="cgAgente.cveEstado"
										name="cgAgente.cveEstado" list="estados"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										onchange="onChangeEstadoGeneral('cgAgente.cveCiudad','cgAgente.colonia','cgAgente.codigoPostal','cgAgente.calleNumero','cgAgente.cveEstado')"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.estado"
										labelposition="left" /></td>
							</tr>
							<tr>

								<td><s:select id="cgAgente.cveCiudad"
										name="cgAgente.cveCiudad" list="ciudades"
										onchange="onChangeCiudad('cgAgente.colonia','cgAgente.codigoPostal','cgAgente.calleNumero','cgAgente.cveCiudad')"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.municipio"
										labelposition="left" /></td>
								<td><s:select id="cgAgente.colonia"
										name="cgAgente.colonia" list="colonias"
										headerKey="%{getText('midas.general.defaultHeaderKey')}"
										headerValue="%{getText('midas.general.seleccione')}"
										onchange="onChangeColonia('cgAgente.codigoPostal','cgAgente.calleNumero', 'cgAgente.colonia','cgAgente.cveCiudad')"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.colonia"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.codigoPostal"
										name="cgAgente.codigoPostal"
										onchange="onChangeCodigoPostal(this.value, 'cgAgente.colonia','cgAgente.cveCiudad','cgAgente.cveEstado','cgAgente.calleNumero','cgAgente.cvePais','cgAgente.codigoPostal');"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.codigo"
										labelposition="left" /></td>
								<td><s:textfield id="cgAgente.calleNumero"
										name="cgAgente.calleNumero" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.calleNumero"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.personaRecibe"
										name="cgAgente.personaRecibe" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.personaRecibe"
										labelposition="left" /></td>
								<td><s:textfield id="cgAgente.telefonoRecibe"
										name="cgAgente.telefonoRecibe" cssClass="cajaTexto"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.telefono"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td colspan="4"><s:textarea id="cgAgente.observaciones"
										name="cgAgente.observaciones" cssClass="cajaTexto"
										cssStyle="width: 500px;"
										key="midas.condicionesGenerales.agente.observaciones"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.inventario"
										name="cgAgente.inventario" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.inventario"
										labelposition="left" /></td>
								<td><s:textfield id="cgAgente.promedioMensual"
										name="cgAgente.promedioMensual" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.promedioMensual"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.factorSurtir"
										name="cgAgente.factorSurtir" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.factor"
										labelposition="left" /></td>
								<td><s:textfield id="cgAgente.porcentajeSurtir"
										name="cgAgente.porcentajeSurtir" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.porcentaje"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.diasEntrega"
										name="cgAgente.diasEntrega" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.diasEntrega"
										labelposition="left" /></td>
								<td><s:textfield id="cgAgente.surtidoEjecutivo"
										name="cgAgente.surtidoEjecutivo" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.surtidoEjecutivo"
										labelposition="left" /></td>
							</tr>
							<tr>
								<td><s:textfield id="cgAgente.fechaUltimo"
										name="cgAgente.fechaUltimo"
										cssClass="cajaTexto" cssStyle="width: 300px;"
										key="midas.condicionesGenerales.agente.ultimoSurtido"
										labelposition="left" 
										disabled="%{readOnly}"/></td>
								<td><s:textfield id="cgAgente.ventas"
										name="cgAgente.ventas" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict"
										cssStyle="width: 150px;"
										key="midas.condicionesGenerales.agente.ventas"
										labelposition="left" /></td>
							</tr>


						</tbody>
					</table>
				</td>
			</tr>
			<tr>

				<td class="guardar" colspan="6" align="right">
					<table>
						<tr>
							<td><div style="display: block;"
									class="alinearBotonALaDerecha">
									<div id="b_regresar">
										<a id="submit" href="javascript: void(0);"
											onclick="irInicioAgente();"> <s:text
												name="midas.boton.regresar" /> </a>
									</div>
								</div></td>
							<td>
								<div style="display: block;" class="alinearBotonALaDerecha">
									<div id="b_guardar">
										<a id="submit" href="javascript: void(0);"
											onclick="actualizarCgAgente();"> <s:text
												name="midas.boton.guardar" /> </a>
									</div>
								</div></td>
						</tr>

					</table></td>
			</tr>

		</tbody>
	</table>
</s:form>