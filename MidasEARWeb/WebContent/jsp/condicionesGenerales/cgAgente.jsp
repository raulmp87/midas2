    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>

	<script type="text/javascript" src="<s:url value='/js/midas2/condicionesGenerales/cgAgente.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>

<s:form id="cgAgenteForm" name="cgAgenteForm"  cssStyle="background:white;">
	<table width="98%" border="0" id="filtros">
		<tbody>
			<tr>
				<td class="titulo" colspan="4">Agentes</td>
			</tr>
			<tr>
			<td colspan="2">
				<s:textfield  cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" cssStyle="width: 300px;"
					key="midas.condicionesGenerales.agente.numero"
			        labelposition="left"
					id="cgAgente.agente.idAgente" name="cgAgente.agente.idAgente"
					/>
			</td>			
			<td colspan="2">
				<s:textfield  cssClass="cajaTextoM2 w200" cssStyle="width: 300px;"
					key="midas.condicionesGenerales.agente.nombre"
			        labelposition="left"
					id="cgAgente.agente.persona.nombreCompleto" name="cgAgente.agente.persona.nombreCompleto"
					/>
			</td>
			</tr>
			<tr style="display: table-row;" class="JS_hide">
			<td colspan="2">
				<s:select id="idGerencia" name="cgAgente.agente.promotoria.ejecutivo.gerencia.id" value="idGerencia" 
				list="gerenciaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeGerencia('idEjecutivoCatalogo','idPromotoriaCatalogo',this.value)" cssClass="cajaTextoM2 jQrequired w200 wide"
				key="midas.condicionesGenerales.agente.gerencia"/>
			</td>
			<td>
				<s:select id="idEjecutivoCatalogo" name="cgAgente.agente.promotoria.ejecutivo.id" value="idEjecutivo" 
				list="ejecutivoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEjecutivo('idPromotoriaCatalogo',this.value)" cssClass="cajaTextoM2 jQrequired w200 wide"
				key="midas.condicionesGenerales.agente.ejecutivo"/>
			</td>			
			</tr>
			<tr style="display: table-row;" class="JS_hide">
			<td colspan="2">
				<s:select id="idPromotoriaCatalogo" name="cgAgente.agente.promotoria.id" value="filtroAgente.promotoria.id" 
				list="promotoriaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTextoM2 jQrequired w200 wide" key="midas.condicionesGenerales.agente.promotoria"/>
			</td>
			<td>
				<s:select id="tipoSituacionAgente" name="cgAgente.agente.tipoSituacion.id" cssClass="cajaTextoM2 w250 wide" 
				headerKey="" headerValue="Seleccione.." 
				list="catalogoTipoSituacion" listKey="id" listValue="valor" key="midas.condicionesGenerales.agente.situacion"/>				      
			</td>
			</tr>
		</tbody>
	</table>
</s:form>
<div>&nbsp;</div>

<table width="20%">
	<tr>
		<td>
			<div style="display: block;" class="alinearBotonALaDerecha">
				<div id="b_buscar">
					<a id="submit" href="javascript: void(0);"
						onclick="traerListaAgente();" class="icon_buscar"> <s:text
							name="midas.boton.buscar" /> </a>
				</div>
			</div>
			<div style="display: block;" class="alinearBotonALaDerecha">
				<div id="b_modificar">
					<a id="submit" href="javascript: void(0);"
						onclick="cargarExcelActualizarDatos();"> <s:text
							name="midas.condicionesGenerales.agente.actualizar" /> </a>
				</div>
			</div>
			<div style="display: block;" class="alinearBotonALaDerecha">
				<div id="b_abrir">
					<a id="submit" href="javascript: void(0);"
						onclick="descargarLayout();"> <s:text
							name="midas.condicionesGenerales.agente.layout" /> </a>
				</div>
			</div></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>
<div>
	<div id="indicador"></div>
	<div id="cgAgenteGrid" style="width: 98%; height: 230px; display: none;""></div>
	<div id="pagingArea_CgAgentes"></div>
	<div id="infoArea_CgAgentes"></div>
</div>
<!-- PRUEBA DE SUBVERSION -->