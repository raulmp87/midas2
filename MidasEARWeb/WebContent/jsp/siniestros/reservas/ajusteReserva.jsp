<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.error {
	background-color: red;
	opacity: 0.4;
}

</style>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/reservas/ajusteReserva.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>



<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>


<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
var idReporteCabina = '<s:property value="idReporteCabina" />';
</script>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.cabina.reportecabina.reservas.datosGenerales"/></div>
<div id="contenedorFiltros">
	<s:form id="infoGeneralForm" >
		<table id="filtrosM2" border="0" style="width: 98%;">
			<tbody>
				<tr>
					<td>
						<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noSiniestro"
							 name="numeroSiniestro"
					labelposition="left" 
							 size="12"	
						readOnly="true"				
							   id="txt_numeroSiniestro"/>
					</td>	
					<td>
						<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noReporte"
							 name="numeroReporte"
					labelposition="left" 
							 size="13"	
						readOnly="true"				
							   id="txt_numeroReporte"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.poliza"
							 name="numeroPoliza"
					labelposition="left" 
							 size="18"	
						readOnly="true"				
							   id="txt_numeroPoliza"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.inciso"
							 name="numeroInciso"
					labelposition="left" 
							 size="12"	
						readOnly="true"				
							   id="txt_numeroInciso"/>
					</td>
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	
<br/>
<br/>
<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.cabina.reportecabina.reservas.listaDeCoberturas"/></div>
<div id="indicador"></div>
<div id="listaDeCoberturasGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>
<s:include value="/jsp/siniestros/reservas/reservas.jsp"></s:include>
<script type="text/javascript">
jQuery(document).ready(
		function(){
			 mostrarPasesEnGrid();
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
