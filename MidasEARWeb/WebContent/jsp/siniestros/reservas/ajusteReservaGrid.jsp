<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="c_coberturas" type="ro" width="200" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.reservas.coberturas"/></column>
		<column id="c_paseDeAtencion" type="ro" width="130" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.reservas.pasesDeAtencion"/></column>
		<column id="c_tipoVehiculo" type="ro" width="180" sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.tipoDeVehiculo"/></column>
		<column id="c_nombreDelAfectado" type="ro" width="*"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.nombreDelAfectado"/></column>		
		<column id="c_sumaAsegurada" type="ro" width="130"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.sumaAsegurada"/></column>
		<column id="c_porcentajeDeducible" type="ro" width="95"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.porcentajeDeducible"/></column>
		<column id="c_importeDeducible" type="ro" width="125"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.importeDeducible"/></column>
		<column id="c_estatus" type="ro" width="110"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.reservas.estatus"/></column>
	</head>			
	<s:iterator value="listCoberturas" status="row">
		<row id="<s:property value="estimacionCoberturaReporte.id" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoPaseAtencionDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionVehiculo" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="estimacionCoberturaReporte.nombreAfectado" escapeHtml="false" escapeXml="true"/></cell>		
			<cell>		
				<s:if test="estimacionCoberturaReporte.coberturaReporteCabina.claveTipoCalculo == 'RCV'">
					<s:property  value="%{coberturaReporteCabina.diasSalarioMinimo}" escapeHtml="false" escapeXml="true"/> <s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.DSMGVDF"/>
				</s:if>					
				<s:elseif test="estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 0">
					<s:property  value="%{getText('struts.money.format',{estimacionCoberturaReporte.coberturaReporteCabina.valorSumaAsegurada})}" escapeHtml="false" escapeXml="true"  />		
				</s:elseif>	
				<s:elseif test="estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 1">				
					&lt;div style='font-size:8px;'&gt;<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>&lt;/div&gt;
					<s:property value="%{getText('struts.money.format',{estimacionCoberturaReporte.sumaAseguradaObtenida})}" escapeHtml="true" escapeXml="true"/>
				</s:elseif>
				<s:elseif test="estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 2">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
					<s:property  value="%{coberturaReporteCabina.valorSumaAsegurada}" escapeHtml="false" escapeXml="true"/>				
				</s:elseif>
				<s:elseif test="estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 9">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />				
				</s:elseif>
				<s:elseif test="estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 3">				
					<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
				</s:elseif>
			</cell>	
			<cell>
				<s:if test='estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveTipoDeducible == "0"'>
					</s:if>
					<s:elseif test='estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveTipoDeducible == "1"'>
						<s:property  value="%{estimacionCoberturaReporte.coberturaReporteCabina.porcentajeDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:elseif>
					<s:else>
						<s:property  value="%{estimacionCoberturaReporte.coberturaReporteCabina.valorDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:else>
					<s:if test='estimacionCoberturaReporte.coberturaReporteCabina.coberturaDTO.claveTipoDeducible != "0"'>
						<s:property  value="%{estimacionCoberturaReporte.coberturaReporteCabina.descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:if>
			</cell>	
			<cell><s:property value="%{getText('struts.money.format',{importeDeducible})}" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>