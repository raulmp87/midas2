<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="div_reservas">
	<s:form id="estimacionFormEstimacion" >
		<s:hidden name="estimacionCoberturaSiniestroDTO.estimacionCoberturaReporte.id" ></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.estimacionCoberturaReporte.coberturaReporteCabina.id" ></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.cobertura.claveFuenteSumaAsegurada" ></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>
		<s:hidden name="requiereAutorizacion" ></s:hidden>
		<s:hidden name="idParametroReserva" ></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.estimacionCoberturaReporte.tipoEstimacion" id="cveCobertura"></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.estimacionCoberturaReporte.folio" id="txt_folio"></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.datosReporte.reporteCabinaId" id="h_reporteCabinaId"></s:hidden>
		<s:hidden name="estimacionCoberturaSiniestroDTO.datosReporte.numeroReporte" id="txt_numeroReporte"></s:hidden>
		
		
		<div class="titulo" "><s:text name="midas.siniestros.cabina.reportecabina.reservas.Reservas"/></div>
		
		
			<table id="filtrosM2" border="0" style="width: 98%;">
				<tbody>
					<tr>
						<td>
							<s:textfield cssClass="txtfield cajaTextoM2 w130 formatCurrency requeridoGrid" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
								 name="estimacionCoberturaSiniestroDTO.datosEstimacion.estimacionActual"
								 onkeypress="return soloNumeros(this, event, true)" 		
						labelposition="right" 
								 size="12"	
							readOnly="true"		
								   id="txt_estimacionActual"/>
						</td>
						<td>
							<s:textfield cssClass="txtfield cajaTextoM2 w130 formatCurrency requeridoGrid" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
								 name="estimacionCoberturaSiniestroDTO.datosEstimacion.reserva"
								 onkeypress="return soloNumeros(this, event, true)" 		
						labelposition="right" 
								 size="12"	
							readOnly="true"		
								   id="txt_reserva"/>
						</td>
						<td>
							<s:select id="s_causaMovimiento" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
								     labelposition="left" 
									 name="estimacionCoberturaSiniestroDTO.datosEstimacion.causaMovimiento"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="causasMovimientoMap" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield requerido"
							  		 /> 	
						</td>
						
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="txtfield cajaTextoM2 w130 formatCurrency"  
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
								 name="estimacionCoberturaSiniestroDTO.datosEstimacion.importePagado"
								 onkeypress="return soloNumeros(this, event, true)" 	
						labelposition="right" 
								 size="12"	
							readOnly="true"				
								   id="txt_importePagado"/>
						</td>
						<td colspan="2">
							<s:textfield cssClass="txtfield requerido cajaTextoM2 w130 formatCurrency" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
								 name="estimacionCoberturaSiniestroDTO.datosEstimacion.estimacionNueva"
								 onkeypress="return soloNumeros(this, event, true)" 		
						labelposition="right" 
								 size="10"
								 maxlength="10"	
							maxlength="10"
								   id="txt_estimacionNueva"/>
						</td>
					<tr>
				</tbody>
			</table>
	</s:form>
	<table align="right" style="padding: 0px; margin: 0px; border: none;">
	<tr>
		<td>	
			
				<div style="height: 100%;float: right;">
					<div class="btn_back w100" style="display: inline; margin-left: 10%;float: right;margin-top: 28%;">
						<a href="javascript: void(0);" onclick="cerrar();"> 
						<s:text name="midas.boton.cerrar" /> </a>
					</div>
				</div>
		</td>
		<td>		
					<div style="height: 100%;float: right;">
						<div class="btn_back w100" style="display: inline; margin-left: 10%;float: right;margin-top: 28%;">
							<a href="javascript: void(0);" onclick="verImagenes(txt_folio.value , 'FORTIMAX',h_reporteCabinaId.value,txt_numeroReporte.value);"> 
							<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> </a>
						</div>
					</div>								
				
		</td>
		<td>
				<s:if test="requiereAutorizacion==0">
					<div style="height: 100%;float: right;" >
						<div class="btn_back w100" style="display: inline; margin-left: 10%;float: right;margin-top: 28%;">
							<a href="javascript: void(0);" onclick="guardarMovimiento();"> 
							<s:text name="midas.boton.guardar" /> </a>
						</div>
					</div>								
				</s:if>
				<s:if test="requiereAutorizacion!=0">
					<div style="height: 100%;float: right;">
						<div class="btn_back w190" style="display: inline; margin-left: 10%;float: right;margin-top: 15%;">
							<a href="javascript: void(0);" onclick="autorizar();"> 
							<s:text name="midas.configuracionParametrosAutorizacion.solicitud.reserva" /> </a>
						</div>		
					</div>						
				</s:if>				
		</td>

	</tr>
</table>
</div>