<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>	
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloMotivoCancelacion">
								<s:text name="midas.siniestros.recuperacion.titulocancelacion" />
							</div> 
	                    </td>
	                  </tr>
					  
	                  <tr>
						<td colspan="6" >
							<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
								<tbody>
									<tr>
										<td>
											<div id="contenedorTextArea">
												<s:textarea value="%{recuperacion.motivoCancelacion}" id="motivoCancelacion_a"
												cssClass="textarea" cssStyle="font-size: 10pt;"	cols="80" rows="3" disabled="true"
												onchange="truncarTexto(this,500);"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					  </tr>
	            </tbody>
	</table>

<script type="text/javascript">
	dhx_init_tabbars();
	jQuery(document).ready(function(){
		
	});
</script>