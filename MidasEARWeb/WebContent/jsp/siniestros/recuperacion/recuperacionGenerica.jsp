<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">

.disabled{
	background: #dddddd;
}

div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;  
}
</style>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>	
<script type="text/javascript" 	src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>

<div id="contenido_Definir" style="width:99%;position: relative;">
		<s:hidden id="idReporteCabina" name="recuperacion.reporteCabina.id" />
		<s:hidden id="coberturasSeleccionadas" name="coberturasSeleccionadas" />
		<s:hidden id="pasesSeleccionadas" name="pasesSeleccionadas" />
		<s:hidden id="tipoOC" name="tipoOC" />
		<s:hidden id="esActivo" name="recuperacion.activo" />
		<s:hidden id="recuperacionId" name="recuperacionId" />
		<s:hidden id="tipoRecuperacion" name="recuperacion.tipo"/>	
		<s:hidden id="editarDatosGenericos" name="editarDatosGenericos"/>
	
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.titulo')}" />
			</div>
		 
			<div id="divGenerales" style="width: 100%;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 100%; height: 280px;">
					
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numero" name="recuperacion.numero"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.noRecuperacion')}" labelposition="left" cssStyle="width:30%;" readonly="true" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="fechaRegistro" name="recuperacion.fechaCreacion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.fechaR')}" labelposition="left" cssStyle="width:65%;" readonly="true" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="fechaCancelacion" name="recuperacion.fechaCancelacion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.fechaC')}" labelposition="left" cssStyle="width:60%;" readonly="true" ></s:textfield>
							</div>
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="tipo" name="tipoRecuperacionDesc"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.tipo')}" labelposition="left" cssStyle="width:60%;" readonly="true" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="lstMedioRecuperacion" id="lstMedioRecuperacion"
									name="recuperacion.medio" label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.medio')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:52%;"
									onchange="changeMedioRecupera(this);" 
									>
									</s:select>							
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="estatus" name="estatusRecuperacionDesc"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.estatus')}" labelposition="left" cssStyle="width:78%;" readonly="true" ></s:textfield>
							</div>
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="asignaNumSiniestro" name="noSiniestro"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.noSiniestro')}" labelposition="left" cssStyle="width:65%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 10%; padding-top:5px;" >
								<div class="btn_back w50" id="btn_Busqueda" 
									style="display: inline; float: left; vertical-align: top; position: relative; margin-top: -3%;">
									<a href="javascript: void(0);" onclick="modalSiniestro();">
										<s:text name="midas.boton.buscar" /> </a>
								</div>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="asignaNumReporte" name="noReporteSiniestro"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.noReporte')}" labelposition="left" cssStyle="width:50%;" readonly="true" ></s:textfield>
								
							</div>
							
							
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="origen" name="recuperacion.origen"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.origen')}" labelposition="left" cssStyle="width:60%;" readonly="true" ></s:textfield>
							</div>
						</div>
				
						<div class="divFormulario">	
				
							<div class="floatLeft divInfDivInterno" id="divTipoOC" style="width: 26%;padding-left:5%" >
								<div class="subtituloLeft" align="left" >
									<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.ordenesC')}"  />
								</div>								
								<s:if test=" (null!=recuperacion.tipoOrdenCompra && recuperacion.tipoOrdenCompra !='') && (editarDatosGenericos!=true) ">
										<div class="floatLeft divContenedorO" style="width: w150;  " >
												 <ul id="lisTipoOC"  class="w170"
													style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
														<s:iterator value="lstTipoOC"
															status="stat">
															<li><label for="${key}">																	
																	<s:property  value="value"/></label></li>
														</s:iterator>
													</ul>  
											</div>
								</s:if>
								<s:else>
									<div class="floatLeft divContenedorO" style="width: w150;  " >
												<ul id="lisTipoOC" type="none" class="w170"
													style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
														<s:iterator value="lstTipoOC"
															status="stat">
															<li><label for="${key}">
																	<input type="radio" 
																	class="desabilitable"
																	name="recuperacion.tipoOrdenCompra"
																	id="tipoOC${key}"
																	onchange="changeTipoOC(this);" 
																	value="${key}" class="js_checkEnable" 
																	style=""/>
																	<s:property  value="value"/></label></li>
														</s:iterator>
													</ul> 
										</div>
								</s:else>
							</div>
							
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;padding-left:10px" >
								<div class="subtituloLeft" align="left" >
									<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.cobertura')}"  />
								</div>
								<div id='regionCoberturas' class="floatLeft divContenedorO" style="width: w270;  " >
										<!--  <ul id="lisCoberturas" class="w270"
											style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
												<s:iterator value="lstCoberturas"
													status="stat">
													<li><label for="${key}">
															<input type="checkbox" 
															class="desabilitable"
															name="coberturasConcat"
															id="cobertura${key}"
															onclick="OnChangeCheckboxCobertura (this)"
															value="${key}" class="js_checkEnable" 
															style=""/>
															<s:property  value="value"/></label></li>
												</s:iterator>
											</ul> -->
											<ul id="lisCoberturas" class="w270"
											style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
												<s:iterator value="lstCoberturas"
													status="stat">
													<li><label for="${key}">															
															<s:property  value="value"/></label></li>
												</s:iterator>
											</ul>
									</div>
								</div>
						
								<div class="floatLeft divInfDivInterno" style="width: 30%;padding-left:50px" >
									<div class="subtituloLeft" align="left" >
										<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.pase')}"  />
									</div>
									<div id='regionPases' class="floatLeft divContenedorO" style="width: w270;  " >
										<!--  	<ul id="lisPases" class="w270"
												style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
													<s:iterator value="lstPases"
														status="stat">
														<li><label for="${key}">
																<input type="checkbox" 
																class="desabilitable"
																name="pasesConcat"
																id="pase${key}"
																onchange="OnChangeCheckboxPase(this);" 
																value="${key}" class="js_checkEnable" 
																style=""/>
																<s:property  value="value"/></label></li>
													</s:iterator>
												</ul>-->
												
												<ul id="lisPases"   class="w270"
												style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
													<s:iterator value="lstPases"
														status="stat">
														<li><label for="${key}">															
																<s:property  value="value"/></label></li>
													</s:iterator>
												</ul> 
										</div>
								</div>
						</div>
					</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		iniContenedorRecuperacion();
	});
	</script>
	
</div>







