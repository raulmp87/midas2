<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/notasDeCredito/notasDeCreditoRecuperacionSiniestros.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}
.divFormulario {
	height: 35px;
	position: relative;
}
#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}

</style>
<s:form  id="contenedorRecuperacionPRVForm">
<s:hidden id="soloConsulta" 	name="soloConsulta" />
<s:hidden id="h_soloLectura" 	name="soloConsulta" />
<s:hidden id="esNuevoRegistro" 	name="esNuevoRegistro" />
<s:hidden id="esAccionCancelar" 	name="esAccionCancelar" />
<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
<s:hidden id="recuperacionMotivoCancelacion" name="recuperacion.motivoCancelacion"/>
<s:hidden id="esRefaccion" name="recuperacion.esRefaccion" />
<s:hidden id="h_idRecuperacion" name="recuperacion.id" />
<s:hidden id="tienePermisoCapturaDatosVenta" name="tienePermisoCapturaDatosVenta" />



	<div id="recuperacionGenericaDiv">
			<s:include value="/jsp/siniestros/recuperacion/recuperacionGenerica.jsp"></s:include>
	</div>
	
	<div id="contenido_DefinirTitulo" style="width:99%;position: relative;">
		
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.titulo')}"/>
			</div>
		</div>
	</div>
	
	<div id="contenido_Pestañas" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 700px; width: 100%" id="recuperacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div width="200px" id="devolucionProveedor" name="Devolucion Proveedor" href="http://void" extraAction="javascript: verTabDevolucion()">
				</div>
				<s:if test="esNuevoRegistro==false && recuperacion.esRefaccion==true  ">
						<div width="200px" id="ventaProveedor" name="Venta" href="http://void" extraAction="javascript: verTabVenta()">
						</div>	
				</s:if>
				<s:else>
						
				</s:else>
				<s:if test="recuperacion.estatus==\"CANCELADO\"">
					<div width="200px" id="cancelar" name="Cancelacion" href="http://void" extraAction="javascript: verTabCancelacion();"></div>
				</s:if>
				
			</div>
		</div>
		
	</div>
	<div id="contenido_Btn" style="width:100%;position: relative; padding-top: 10px">
			<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
					<div id="cerrarBtn" class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="cerrarRecuperacion();"> <s:text
								name="Cerrar" /> </a>
					</div>
			</div>	
			
			<s:if test="soloConsulta==false  ">
						<s:if test="esNuevoRegistro==false  ">
							<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
								<div id="guardarBtnPRV" class="btn_back w150"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="guardarVentaPRV();"> <s:text
											name="Guardar" /> </a>
								</div>
							</div>
								
						</s:if>
						<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
								<div id="guardarBtn" class="btn_back w150"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="guardarRecuperacionPRV();"> <s:text
											name="Guardar" /> </a>
								</div>
						</div>
				</s:if>	
				
				<s:if test="esAccionCancelar==true && recuperacion.estatus != \"CANCELADO\"">
				<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
					<div id="cancelarBtn" class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="mostrarVentanaCancelarRecuperacionProveedor();"> <s:text
									name="Cancelar" /> </a></div></div>
				</s:if>
				<s:elseif test="recuperacion.estatus != \"CANCELADO\" ">
							<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
								<div id="notaCred" class="btn_back w155"
									style="display: inline; float: left;margin-left: 1%; ">
									<a href="javascript: void(0);" onclick="mostrarContenedorNotasDeCredito();"> <s:text
									name="Notas de Credito" /> </a>
								</div>
							</div>
				</s:elseif>
				
				
						
	
	</div>
	
</s:form>
<script type="text/javascript">
	jQuery(document).ready(function() {
		dhx_init_tabbars();
      	incializarTabs();
	});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>