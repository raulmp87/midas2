<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/listadoIngresos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>

<style type="text/css">
	#superior{
		height: 300px;
		width: 99%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.recuperacion.ingresos.titulo"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="buscarIngresosForm">
			<s:hidden id="ingresosFacturablesConcat" name="" />
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 14%;">
						<s:textfield id="noIngreso" label="%{getText('midas.siniestros.recuperacion.ingresos.numero')}" name="filtroIngreso.numeroIngreso"
								cssStyle="float: left;width: 50%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaTipoRecuperacion" cssStyle="width: 90%;"
							name="filtroIngreso.tipoRecuperacion" 
							label="%{getText('midas.siniestros.recuperacion.ingresos.tiporecuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							id="listaTipoRecuperacion"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaMedioRecuperacion" cssStyle="width: 90%;"
							name="filtroIngreso.medioRecuperacion" 
							label="%{getText('midas.siniestros.recuperacion.ingresos.mediorecuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaEstatus" cssStyle="width: 90%;"
							name="filtroIngreso.estatus" 
							label="%{getText('midas.siniestros.recuperacion.ingresos.estatus')}"
							cssClass="cleaneable txtfield obligatorio"
							id="estatusLiquidaciones"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaOficinas" cssStyle="width: 90%;"
							name="filtroIngreso.oficinaId" 
							label="%{getText('midas.siniestros.recuperacion.ingresos.oficina')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
				</div>
				
				<div id="SIS">
				
					<div class="floatLeft" style="width: 14%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresos.numerorecuperacion')}" name="filtroIngreso.numeroRecuperacion"
								cssStyle="float: left;width: 50%;" maxlength="30" cssClass="cleaneable txtfield obligatorio jQnumeric jQrestrict"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresos.numerodeudor')}" name="filtroIngreso.claveDeudor"
								cssStyle="float: left;width: 50%;" maxlength="30" cssClass="cleaneable txtfield obligatorio jQnumeric jQrestrict"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresos.nombredeudor')}" name="filtroIngreso.nombreDeudor" 		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresos.numerosiniestro')}" name="filtroIngreso.numeroSiniestro" 
								onBlur="validaFormatoSiniestro(this);"		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresos.numeroreporte')}" name="filtroIngreso.numeroReporte" 
								onBlur="validaFormatoSiniestro(this);"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
				</div>
								
				<div id="SIS">
					
					<div class="floatLeft" style="width: 14%;padding-top: 1%;">
						<s:text name="midas.siniestros.recuperacion.ingresos.montofinaldesde"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;" >
						<s:textfield id="montofinaldesde"
							label="%{getText('midas.siniestros.recuperacion.ingresos.de')}"
							name="filtroIngreso.montoIniIngreso"
							onkeyup="mascaraDecimales('#montofinaldesde',this.value);"
							onblur="mascaraDecimales('#montofinaldesde',this.value);" maxlength="10"
							disabled="false" cssStyle="float: left;width: 70%;"
							cssClass="cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 12%;" >
						<s:textfield id="montofinalhasta"
							label="%{getText('midas.siniestros.recuperacion.ingresos.hasta')}"
							name="filtroIngreso.montoFinIngreso"
							onkeyup="mascaraDecimales('#montofinalhasta',this.value);"
							onblur="mascaraDecimales('#montofinalhasta',this.value);" maxlength="10"
							disabled="false" cssStyle="float: left;width: 70%;"
							cssClass="cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 14%;padding-top: 1%;">
						<s:text name="midas.siniestros.recuperacion.ingresos.fechacancelaciondesde"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.de')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroIngreso.fechaIniCancelacion"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaCancelacionDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroIngreso.fechaFinCancelacion"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaCancelacionHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 14%; margin-top:2%;">
						<s:checkbox id="servicioPublico" onClick="javascript:validaCheck(this);" title="Publico" name="filtroIngreso.servicioPublico" cssClass="tipoServicio" labelposition="right" fieldValue="0" value="0" label="%{getText('midas.siniestros.recuperacion.ingresos.serviciopublico')}"/>
					</div>
					
				</div>
				
				<div id="SIS">
					
					<div class="floatLeft" style="width: 14%;">
						<s:text name="midas.siniestros.recuperacion.ingresos.fecharegistrodesde"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.de')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroIngreso.fechaIniPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroIngreso.fechaFinPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 14%;">
						<s:text name="midas.siniestros.recuperacion.ingresos.fechaaplicaciondesde"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.de')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroIngreso.fechaIniAplicacion"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaAplicacionDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresos.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroIngreso.fechaFinAplicacion"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaAplicacionHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 14%; margin-top:2%;">
						<s:checkbox id="servicioParticular" onClick="javascript:validaCheck(this);" title="Privado" name="filtroIngreso.servicioParticular" cssClass="tipoServicio" fieldValue="0" value="0" labelposition="right" label="%{getText('midas.siniestros.recuperacion.ingresos.servicioparticular')}"/>
					</div>
					
				</div>
				
				
				<div id="SIS">
					
					&nbsp;

				</div>
				<div id="SIS">
					
					<div id="btnBuscar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="buscarIngresos();"> <s:text name="Buscar" /> </a>
					</div>
					
					<div id="btnLimpiar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarFormularioIngresos();"> <s:text name="Limpiar" /> </a>
					</div>

				</div>
				
			</div>
			
			
			<input id="hTipoServicio" type="hidden" value="0" name="filtroIngreso.servicio"></input>
		</form>
	</div>
	
	<br />
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.ingresos.facturar.subtitulo.grid"/>	
	</div>
	<div id="indicador"></div>
	<div id="listadoIngresosGrid" style="width: 99%; height: 380px;display:none;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>

	<div id="SIS">
		<div id="divExcelBtn" class="w150"
			style="float: right; display: none;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="exportarExcel();"> <s:text
						name="midas.boton.exportarExcel" /> </a>
			</div>
		</div>
		<div id="divFacturarBtn" class="w150"
			style="float: right;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="mostrarFacturarIngresos();"> <s:text
						name="midas.siniestros.recuperacion.ingresos.facturarIngresos" /> </a>
			</div>
		</div>
		<div id="divAplicarBtn" class="w150"
			style="float: right;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="mostrarAplicarIngresos();"> <s:text
						name="midas.siniestros.recuperacion.ingresos.aplicarIngresos" /> </a>
			</div>
		</div>
	</div>


</div>
<script>
jQuery(document).ready(function(){
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	

	jQuery("#noIngreso").keypress(function(event) {
			var code = event.keyCode || event.which;
			if (code == 13) {
				buscarIngresos();
			} else {
				return soloNumeros(event, event, true);
			}
		});
		
	});
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
