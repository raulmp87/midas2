<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/listadoRecuperaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script	src="<s:url value='/js/validaciones.js'/>"></script>


<style type="text/css">
	#superior{
		height: 340px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
	<s:hidden id="url"/>
	<s:hidden id="recuperacionId"/>
	
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.recuperacion.listado.recuperaciones.busqueda"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="buscarRecuperacionesForm">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:textfield id="noRecuperacion" label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.no.recuperacion')}" name="filtroRecuperacionDto.numeroRecuperacion"
								cssStyle="float: left;width: 50%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="lstTipoRecuperacion" cssStyle="width: 90%;"
							name="filtroRecuperacionDto.tipoRecuperacion" 
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.tipo.recuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							onchange = "javascript:habilitarDatosRecuperacion();"
							id="lstTipoRecuperacion"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="lstEstatusRecuperacion" cssStyle="width: 90%;"
							name="filtroRecuperacionDto.estatus" 
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.estatus.recuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							id="estatusLiquidaciones"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style=" padding-right:2%; " >
						<s:textfield label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.proveedor.comprador')}" name="filtroRecuperacionDto.nombrePrestador" 
								cssStyle="float: left;width: 380px;" maxlength="100" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>

					
				</div>
				
				<div id="SIS">
					 
					 <div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.pase.atencion')}" 
								name="filtroRecuperacionDto.paseAtencion" 
								onBlur="validaFormatoSiniestro(this);"				
								cssStyle="float: left;width: 90%;" 
								cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.no.siniestro')}" name="filtroRecuperacionDto.numeroSiniestro" 
								onBlur="validaFormatoSiniestro(this);"		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.usuario')}" name="filtroRecuperacionDto.usuario" 
								cssStyle="float: left;width: 175px" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="lstEstatusCarta" cssStyle="width: 90%;"
							name="filtroRecuperacionDto.estatusCarta" 
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.estatus.carta')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="lstIndemnizado" cssStyle="width: 90%;"
							name="filtroRecuperacionDto.esIndemnizado" 
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.indenmizado')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
				</div>
				
				<div id="SIS">

					<div class="floatLeft" style="width: 18%;">
						<s:textfield id="montoDe"
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.monto.de')}"
							name="filtroRecuperacionDto.montoInicial"
							onkeyup="mascaraDecimales('#montoDe',this.value);"
							onblur="mascaraDecimales('#montoDe',this.value);" maxlength="20"
							disabled="false" cssStyle="float: left;width: 70%;"
							cssClass="cleaneable txtfield formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
							<s:textfield id="montoHasta"
								label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.monto.hasta')}"
								name="filtroRecuperacionDto.montoFinal"
								onkeyup="mascaraDecimales('#montoHasta',this.value);"
								onblur="mascaraDecimales('#montoHasta',this.value);"
								maxlength="20" disabled="false"
								cssStyle="float: left;width: 70%;"
								cssClass="cleaneable txtfield formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
								<label><s:text
										name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.fecha.aplicacion')}" />:</label>
								<div style="margin-top: 9%">
									<sj:datepicker name="filtroRecuperacionDto.fechaAplicacionDe"
										changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaIniAplicacion" maxlength="10" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield ">
									</sj:datepicker>
								</div>
					 </div>
						
					 <div class="floatLeft" style="width: 18%; ">
								<label><s:text
										name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.monto.hasta')}" />:</label>
								<div style="margin-top: 9%">
									<sj:datepicker name="filtroRecuperacionDto.fechaAplicacionHasta"
										changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaFinAplicacion" maxlength="10"
										disabled="false"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield ">
									</sj:datepicker>
								</div>
						</div>
						
					 	<div class="floatLeft divDocumentada" style="width: 18%; display:none;">
						<s:select list="lstPtDocumentada" cssStyle="width: 90%;"
							name="filtroRecuperacionDto.ptDocumentada" 
							label="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.pt.documentada')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
						</div>

				</div>
				
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.fecha.registro.de')}" />:</label>
							<div style="margin-top: 8%">
								<sj:datepicker name="filtroRecuperacionDto.fechaIniRegistro"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaSolDe" maxlength="10" 
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield ">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.monto.hasta')}" />:</label>
							<div style="margin-top: 8%">
								<sj:datepicker name="filtroRecuperacionDto.fechaFinRegistro"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaSolHasta" maxlength="10"
									 disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield ">
								</sj:datepicker>
							</div>
					</div>
					
						<div class="floatLeft" style="width: 18%;">
								<label><s:text
										name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.fecha.siniestro.de')}" />:</label>
								<div style="margin-top: 9%">
									<sj:datepicker name="filtroRecuperacionDto.fechaIniSiniestro"
										changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaIniSiniestro" maxlength="10" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield ">
									</sj:datepicker>
								</div>
						</div>
						
						<div class="floatLeft" style="width: 18%; ">
								<label><s:text
										name="%{getText('midas.siniestros.recuperacion.listado.recuperaciones.monto.hasta')}" />:</label>
								<div style="margin-top: 9%">
									<sj:datepicker name="filtroRecuperacionDto.fechaFinSiniestro"
										changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaFinSiniestro" maxlength="10"
										disabled="false"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield ">
									</sj:datepicker>
								</div>
						</div>
									
												
				</div>

				<div id="SIS">
					
					<div id="btnRechazar" class="btn_back w120" style="display: inline; margin-top:5%; margin-right: 2%; margin-bottom: 1%; float: right;">
						<a href="javascript: void(0);" onclick="nuevaRecuperacion();"> <s:text name="Nuevo" /> </a>
					</div>
					
					<div id="btnRechazar" class="btn_back w120" style="display: inline; margin-top:5%;  margin-right: 2%; margin-bottom: 1%; float: right;">
						<a href="javascript: void(0);" onclick="buscarRecuperaciones();"> <s:text name="Buscar" /> </a>
					</div>
					
					<div id="btnRechazar" class="btn_back w120" style="display: inline; margin-top:5%;  margin-right: 2%; margin-bottom: 1%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarFormulario();"> <s:text name="Limpiar" /> </a>
					</div>

				</div>
				
			</div>
		</form>
	</div>
	
	<br />
	<br />
	<br />
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.listado.recuperaciones.listado"/>	
	</div>

	<div id="indicador"></div>
	<div id="listadoGridRecuperaciones" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>

	<div id="SIS">
		<div id="divExcelBtn" class="w150"
			style="float: right; display: none;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="exportarExcel();"> <s:text
						name="midas.boton.exportarExcel" /> </a>
			</div>
		</div>
	</div>


</div>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	
	jQuery("#noRecuperacion").keypress(function(event){
         var code = event.keyCode || event.which;
		 if(code == 13) { 
		   		buscarRecuperaciones();
		 }else{
		 	return soloNumeros(event, event, true);
		 }
    });
    
    initComboTipoRecuperacion();
	
});
</script>
