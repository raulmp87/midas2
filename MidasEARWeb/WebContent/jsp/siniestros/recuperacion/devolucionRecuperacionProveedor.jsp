<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
.disabledgray{
	background-color: #fafaf0;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<div id="contenido_DefinirPRV" style="width:99%;position: relative;">
		<s:hidden id="filtro_esDanosMateriales" name="datosRecuperacion.esDanosMateriales" />
		<s:hidden id="filtro_esResponsabilidadCivil" name="datosRecuperacion.esResponsabilidadCivil" />
		<s:hidden id="filtro_marcaVehiculo"  />
		<s:hidden id="filtro_tipoVehiculo"  />
		<s:hidden id="filtro_modeloVehiculo"  />
		<s:hidden id="filtro_nombreTaller"  />	
		<s:hidden id="filtro_esRefaccion"  name="datosRecuperacion.esRefaccion"  />	
		
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
		 
			<div id="divGenerales" style="width: 100%;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">					
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.tituloOC')}" />
						</div>
					
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<div class="floatLeft divInfDivInterno" style="width: 65%;" >
								<s:textfield id="filtro_numOrdenCompra" name="recuperacion.ordenCompra.id"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.noOc')}" labelposition="left" cssStyle="width:65px;" readonly="true" ></s:textfield>
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 20%;" >
									<div class="btn_back w50" id="btn_BusquedaOC"
										style="display: inline; float: left; vertical-align: top; position: relative; margin-top: -3%;">
										<a href="javascript: void(0);" onclick="buscarOrdenesCompra();">
											<s:text name="midas.boton.buscar" /> </a>
									</div>
								</div>					
							
							</div>
						
							
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
							
								<s:textfield id="filtro_fechaOrdenCompra" name="recuperacion.ordenCompra.fechaCreacion"   
								cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.fechaOC')}"
								onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
								 labelposition="left" cssStyle="width:37%;" readonly="true" ></s:textfield>
							</div>
						
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_montoOrdenCompra" name="datosRecuperacion.montoTotalOC"    
								label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.montoOC')}" 
								cssClass="txtfield  setNew formatCurrency" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#filtro_montoOrdenCompra',this.value);" 
								onblur="mascaraDecimales('#filtro_montoOrdenCompra',this.value);validarTotal(this);"
								labelposition="left" cssStyle="width:45%;" readonly="true" ></s:textfield>
							</div>
							
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_factura" name="recuperacion.ordenCompra.factura"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.noFac')}" labelposition="left" cssStyle="width:30%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								
								<s:textfield id="filtro_fechaOrdenPago" name="recuperacion.ordenCompra.ordenPago.fechaCreacion"   
								cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.fechaOP')}"
								onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
								 labelposition="left" cssStyle="width:45%;" readonly="true" ></s:textfield>
							
							</div>
						
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_montoOrdenPago" name="datosRecuperacion.montoTotalOP"  
								cssClass="txtfield  setNew formatCurrency" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#filtro_montoOrdenCompra',this.value);" 
								onblur="mascaraDecimales('#filtro_montoOrdenCompra',this.value);validarTotal(this);" 
								label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.montoOP')}" labelposition="left" cssStyle="width:45%;" readonly="true" ></s:textfield>
							</div>
							
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_nombreProveedor" name="datosRecuperacion.nombreProveedor"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.provedor')}" labelposition="left" cssStyle="width:80%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_telefonoProveedor" name="datosRecuperacion.telProveedor"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.tel')}" labelposition="left" cssStyle="width:62%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_correoProveedor" name="recuperacion.correoProveedor"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.correo')}" labelposition="left" cssStyle="width:65%;" readonly="true"></s:textfield>
							</div>
						</div>
						
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.tituloRefac')}" />
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								
								
								<s:checkbox id="ch_esRefaccion"  name="datosRecuperacion.esRefaccion"   cssClass="desabilitable" onchange="checkRefaccion();" label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.refaccion')}" labelposition="left"></s:checkbox>
								
								
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_numValuacion" name="recuperacion.numeroValuacion"  onkeypress="return soloNumeros(this, event, true);" cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.noVal')}"  labelposition="left" cssStyle="width:45%;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="filtro_nombreValuador" name="recuperacion.nombreValuador"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.nombreVal')}" labelposition="left" cssStyle="width:60%;"  ></s:textfield>
							</div>
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 60%;" >
								<s:textfield id="adminRefacciones" name="recuperacion.adminRefacciones"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.adminRef')}" labelposition="left" cssStyle="width:60%;"></s:textfield>
							</div>					
						</div>
							<div class="divFormulario" id="divTaller" >
								<div class="floatLeft divInfDivInterno" style="width: 60%;" >
									<s:textfield id="nombreTaller" name="recuperacion.nombreTaller"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.nomTaller')}" labelposition="left" cssStyle="width:70%;"  ></s:textfield>
								</div>						
							</div>
							<div class="divFormulario" id="divRefacciones" >
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="marcaVehiculo" name="recuperacion.marcaDesc"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.marca')}" labelposition="left" cssStyle="width:45%;"  ></s:textfield>
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="tipoVehiculo" name="recuperacion.estiloDesc"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.tipo')}" labelposition="left" cssStyle="width:45%;"  ></s:textfield>
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="modeloVehiculo" name="recuperacion.modeloVehiculo"  maxlength="4" onkeypress="return soloNumeros(this, event, true);" cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.modelo')}" labelposition="left" cssStyle="width:45%;"  ></s:textfield>
								</div>					
							</div>
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.tituloDevolucion')}" />
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:textfield id="personaDevolucion" name="recuperacion.personaDevolucion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.personaDevuelve')}" labelposition="left" cssStyle="width:45%;"  ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<!--<s:textfield id="conceptoDevolucion" name="recuperacion.conceptoDevolucion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.concepDev')}" labelposition="left" cssStyle="width:45%;"  ></s:textfield>-->
								<s:textarea id="conceptoDevolucion" name="recuperacion.conceptoDevolucion" cols="40" rows="5" labelposition="left" cssClass="txtfield" label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.concepDev')}" />
								
							</div>
						</div>						
						<div class="divFormulario" >
							
							<div class="floatLeft divInfDivInterno" style="width: 50%;" >
									<s:select list="lstMotivo" id="lstMotivo"
									name="recuperacion.motivoDevolucion" label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.motivoDev')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:60%;"
									onchange="changeMotivo();"
									>
									</s:select>							
							</div>	
							
							<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:textfield id="otroMotivoDevolucion" name="recuperacion.otroMotivoDevolucion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionPrv.otroMot')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
							</div>
						</div>
						
						<div class="subtituloLeft" align="left" >
						<s:text name="Totales" />
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 15%;" >
									<s:textfield id="text_subtotal"  name="recuperacion.subTotal"    cssClass="txtfield  setNew formatCurrency" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_subtotal',this.value);" onblur="mascaraDecimales('#text_subtotal',this.value);validarTotal(this);" label="SubTotal" labelposition="left" cssStyle="width:70px;"></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 13%;" >
									<s:textfield id="text_Iva"   name="recuperacion.iva" cssClass="txtfield  setNew formatCurrency"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_Iva',this.value);" onblur="mascaraDecimales('#text_Iva',this.value);validarTotal(this);"  label="I.V.A." labelposition="left" cssStyle="width:70px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 16%;" >
									<s:textfield id="text_IvaRetenido"   name="recuperacion.ivaRetenido" cssClass="txtfield  setNew formatCurrency" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_IvaRetenido',this.value);" onblur="mascaraDecimales('#text_IvaRetenido',this.value);validarTotal(this);"   label="I.V.A. Retenido" labelposition="left" cssStyle="width:70px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 13%;" >
									<s:textfield id="text_Isr"   name="recuperacion.isr" cssClass="txtfield  setNew formatCurrency"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_Isr',this.value);" onblur="mascaraDecimales('#text_Isr',this.value);validarTotal(this);"  label="I.S.R." labelposition="left" cssStyle="width:70px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
									<s:textfield id="text_montoTotal"   name="recuperacion.montoTotal" cssClass="txtfield  setNew formatCurrency"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_montoTotal',this.value);" onblur="mascaraDecimales('#text_montoTotal',this.value);validarTotal(this);"  label="Monto Total Original" labelposition="left" cssStyle="width:70px;" ></s:textfield>
							</div>
						</div>
						<div id="divInferior" style="width: 1050px !important; padding-left: 15px;"
							class="floatLeft">							
							<div id="cuentaDevolucionGrid" style="width: 790px; height: 100px;"></div>
							<div id="pagingArea" style="padding-top: 8px"></div>
							<div id="infoArea"></div>
						</div>
						
						
						
					</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		initCurrencyFormatOnTxtInput();
		iniContenedorRecuperacionPRV();
		buscarCuentasDevolucion();
	});
	</script>
	
</div>



