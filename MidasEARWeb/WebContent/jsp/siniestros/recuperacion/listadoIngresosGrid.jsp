<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numeroIngreso"		type="ro" width="75"  sort="int" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.numeroingreso'}"/></column>	
        <column id="numeroRecuperacion" type="ro" width="120" sort="int" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.numerorecuperacion'}"/></column>
        <column id="tipoRecuperacion" 	type="ro" width="100" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.tiporecuperacion'}"/> </column>
		<column id="medioRecuperacion"  type="ro" width="110" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.mediorecuperacion'}"/> </column>
		<column id="numeroDeudor"	    type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.recuperacion.ingresos.numerodeudor'}"/></column>
		<column id="nombreDeudor"       type="ro" width="100" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.nombredeudor'}"/> </column>	
		<column id="numeroSiniestro"    type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.numerosiniestro'}"/> </column>
		<column id="numeroReporte"      type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.numeroreporte'}"/> </column>
		<column id="oficina"	        type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.oficina'}"/> </column>
		<column id="montoRecuperado"    type="ron" width="180" format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.recuperacion.ingresos.montofinaldesde'}"/> </column>
		<column id="fechaPendiente"		type="ro" width="100" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.ingresos.fecharegistrodesde'}"/> </column>
		<column id="fechaAplicacion"    type="ro" width="120" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.ingresos.fechaaplicaciondesde'}"/> </column>
		<column id="fechaCancelacion"   type="ro" width="100" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.ingresos.fechacancelaciondesde'}"/> </column>
		<column id="estatus" 			type="ro" width="155" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.estatus'}"/> </column>
		<column id="estatusFacturacion" type="ro" width="85" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.estatusfacturacion'}"/> </column>
		<column id="selected" 			type="ch" width="75"  sort="na"  align="center" >Facturar? &#60;input type=&#34;checkbox&#34; id=&#34;mcheckbox&#34; &#47;&#62; </column>
		<column                         type="img" width="70" sort="na" align="center" >Acciones</column>
		<m:tienePermiso nombre="FN_M2_SN_Ingresos_Restringido">
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>
		</m:tienePermiso>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>

	  	</head>
		<s:iterator value="ingresos">
		<s:if test="montoFinalRecuperado != null">
			<row id="<s:property value="ingresoId"/>" factura="<s:property value="permitirFacturacion"/>">
				<cell><s:property value="numeroIngreso" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="medioRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="claveDeudor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreDeudor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficinaDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="montoFinalRecuperado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaIngreso" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaCancelacion" escapeHtml="false" escapeXml="true" /></cell> 
				<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/> </cell>
				<cell><s:property value="estatusFacturado" escapeHtml="false" escapeXml="true"/> </cell>
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true"/></cell>
				<s:if test=" medioRecuperacion != \"NOTACRED\" " >
				<s:if test="estatus == \"PENDIENTE\" && (tipoRecuperacionDesc!=\"Sipac\" || (numeroSiniestro != null && numeroSiniestro!=\"\"))">
					<cell>/MidasWeb/img/icons/ico_aceptar.gif^Aplicar Ingreso^javascript:aplicarIngreso(<s:property value="ingresoId" escapeHtml="false" escapeXml="true" />, "<s:property value="tipoRecuperacion" />" )^_self</cell>
					<m:tienePermiso nombre="FN_M2_SN_Ingresos_Restringido">
						<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Cancelar Ingreso Pendiente^javascript:mostrarVentanaCancelacionIngresoPendientePorAplicar(<s:property value="ingresoId" escapeHtml="false" escapeXml="true" />,true, "<s:property value="tipoRecuperacion" />" )^_self</cell>
					</m:tienePermiso>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Aplicar Deshabilitado^^_self</cell>
					<m:tienePermiso nombre="FN_M2_SN_Ingresos_Restringido">
						<cell>../img/pixel.gif^Cancelar Pendiente por Aplicar Deshabilitado^^_self</cell>
					</m:tienePermiso>
				</s:else>
				<m:tienePermiso nombre="FN_M2_SN_Ingresos_Restringido">
	                <s:if test="estatus == \"APLICADO\" || estatus == \"CANCDEV\" || estatus == \"CANCPEND\" || estatus == \"CANCENVAC\" ">
	                	<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Ingreso^javascript:consultarIngreso(<s:property value="ingresoId" escapeHtml="false" escapeXml="true" />,  "<s:property value="tipoRecuperacion" />" )^_self</cell>
	                </s:if>
	                <s:else>
						<cell>../img/pixel.gif^Consultar Deshabilitado^^_self</cell>
	                </s:else>
                </m:tienePermiso>
                <s:if test="estatus == \"APLICADO\" ">
					<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Cancelar Ingreso Aplicado^javascript:mostrarOpcionesCancelarIngresoAplicado(<s:property value="ingresoId" escapeHtml="false" escapeXml="true" />,true, "<s:property value="tipoRecuperacion" />" )^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Cancelar Ingreso Aplicado Deshabilitado^^_self</cell>
				</s:else>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^No disponible para Ingresos Pendientes de Recuperacion de Nota de Credito^^_self</cell>
					<cell>../img/pixel.gif^No disponible para Ingresos Pendientes de Recuperacion de Nota de Credito^^_self</cell>
					<cell>../img/pixel.gif^No disponible para Ingresos Pendientes de Recuperacion de Nota de Credito^^_self</cell>
					<cell>../img/pixel.gif^No disponible para Ingresos Pendientes de Recuperacion de Nota de Credito^^_self</cell>
				</s:else>
			</row>
		</s:if>
		</s:iterator>
	
</rows>