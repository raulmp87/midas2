<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingAreaHistorial</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin"> 
				<param>bricks</param>
			</call>   
        </beforeInit>       
				<column id="fechaCierreSubasta"       type="ro" width="140" sort="date" align="left" ><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.fecha.cierre.subasta'}"/></column>
				<column id="fechaAsignacion"          type="ro" width="120" sort="date" align="left" ><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.fecha.asignacion'}"/></column>
				<column id="comprador"                type="ro" width="140" sort="str" align="left" ><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.comprador'}"/></column>	
				<column id="totalVenta"               type="ro" width="60"  sort="int" format="0.00"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.total.venta'}"/></column>  
		        <column id="estatusMovimiento"        type="ro" width="130" sort="str"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.estatus.movimiento'}"/> </column>
		        <column id="eficienciaIndemnizacion"  type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.eficiencia.indemnizacion'}"/> </column>
		        <column id="eficienciaProvision"      type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.eficiencia.provision'}"/> </column>
		        <column id="tipoSalvamento"           type="edn" width="190" sort="str"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.tipo.salvamento'}"/> </column>
		        <column id="fechaLimiteProrroga"      type="ro" width="120" sort="date"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.fecha.limite.prorroga'}"/> </column>
		        <column id="fechaCancAsignacion"      type="ro" width="100" sort="date"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.fecha.cancelacion.asignacion'}"/> </column>
		        <column id="causaCancelacion"         type="ro" width="155" sort="date"><s:text name="%{'midas.siniestros.recuperacion.recuperacionSvm.causa.cancelacion'}"/> </column> 	
	</head>
	<s:iterator value="listadoVentaSalvamento" status="stats">
		<row id="<s:property value="#row.index"/>">
		    <cell><s:property value="fechaCierreDeSubasta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="comprador.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="%{getText('struts.money.format',{totalVenta})}" escapeHtml="false" escapeXml="true"/></cell>
            <cell><s:property value="nombreEstatus" escapeHtml="false" escapeXml="true" /></cell>
            <cell><s:property value="eficienciaIndeminizacion" escapeHtml="false" escapeXml="true" />%</cell>
            <cell><s:property value="eficienciaProvision" escapeHtml="false" escapeXml="true" />%</cell>
            <cell><s:property value="recuperacionSalvamento.tipoSalvamentoStr" escapeHtml="false" escapeXml="true" /></cell>
            <cell><s:if test="%{ nombreEstatus == 'PRORROGA' }" ><s:property value="recuperacionSalvamento.fechaLimiteProrroga" escapeHtml="false" escapeXml="true" /></s:if> </cell>
            <cell><s:property value="fechaCancelacionDeSubasta" escapeHtml="false" escapeXml="true" /></cell>
            <cell><s:property value="comentarioCancelacion" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
	
</rows>