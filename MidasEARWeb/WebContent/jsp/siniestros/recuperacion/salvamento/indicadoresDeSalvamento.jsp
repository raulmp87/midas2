<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css" >

</style>
						
<div id="contenido_indicadoresSalvamento" style="width:99%;position: relative;">	
	<div id="divInferior" style="width: 100%;" class="floatLeft">
		<div id="divGenerales" style="width: 100%;"  class="floatLeft">
			<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
						<s:textfield id="fechaSiniestro" 
									 maxlength="9"
									 name="indicadores.fechaSiniestro"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.siniestro')}" labelposition="top"
									 disabled="disabled" 
									 />		
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%;" >						
						<s:textfield id="fechaIndemnizacion" 
									 maxlength="9"
									 name="indicadores.fechaIndeminizacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.indemnizacion')}" labelposition="top"
									 disabled="disabled" 
									 />								
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaProvision" 
									 maxlength="9"
									 name="indicadores.fechaProvision"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.provision')}" labelposition="top"
									 disabled="disabled" 
									 />	
					</div>
				</div>			
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaSubasta" 
									 maxlength="9"
									 name="indicadores.fechaInicioSubasta"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fechaSubasta')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaConfirmacionIngreso" 
									 maxlength="9"
									 name="indicadores.fechaConfirmacionIngreso"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.fechaConfirmacionIngreso')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaAplicacionIngreso" 
									 maxlength="9"
									 name="indicadores.fechaAplicacionIngreso"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.aplicion.ingreso')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>	

				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaRecepcionPapeleo" 
									 maxlength="9"
									 name="indicadores.fechaRecepcionPapeleo"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.recepcion.papeleria')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="fechaTiempoAutorizacionPagoOrdenCompra" 
									 maxlength="9"
									 name="indicadores.tiempoAutorizacionPagoOrdenCompra"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.aut.pago.orden')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>
				
				
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="valorEstimadoDeterminacion" 
									 maxlength="9"
									 name="indicadores.valorEstimadoEnDeterminacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento formatCurrency" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.valor.estimado.determinacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="valorProvisionado" 
									 maxlength="9"
									 name="indicadores.valorProvisionado"  
									 cssClass="txtfield modoSoloLecturaSalvamento formatCurrency" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.valor.provisionado')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="totalVenta" 
									 maxlength="9"
									 name="indicadores.totalDeVenta"  
									 cssClass="txtfield modoSoloLecturaSalvamento formatCurrency" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.valor.total.venta')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>	
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoDocumentacion" 
									 maxlength="9"
									 name="indicadores.tiempoDeDocumentacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.documentacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoPagoDesdeDeterminacion" 
									 maxlength="9"
									 name="indicadores.tiempoPagoDesdeDeterminacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.pago.determinacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoPagoDesdeDocumentacion" 
									 maxlength="9"
									 name="indicadores.tiempoPagoDesdeDocumentacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.pago.documentacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoIndenmizacion" 
									 maxlength="9"
									 name="indicadores.tiempoDeIndeminizacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.pago.indemnzacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoSubasta" 
									 maxlength="9"
									 name="indicadores.tiempoDeSubasta"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.subasta')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoAsignacion" 
									 maxlength="9"
									 name="indicadores.tiempoDeAsignacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.asignacion')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>
				
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoConfirmacionIngreso" 
									 maxlength="9"
									 name="indicadores.tiempoConfirmacionIngreso"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.confirmacion.ingreso')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoAplicacionIngreso" 
									 maxlength="9"
									 name="indicadores.tiempoAplicacionIngreso"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.aplicacion.ingreso')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoPromedioVenta" 
									 maxlength="9"
									 name="indicadores.tiempoPromedioVenta"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.promedio.venta')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>
				
				<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="tiempoPromedioTotalVenta" 	
									 maxlength="9"
									 name="indicadores.tiempoPromedioTotalVenta"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tiempo.promedio.total.venta')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>		
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="eficienciaDeIndemnizacion" 
									 maxlength="9"
									 name="indicadores.eficienciaIndeminizacion"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.eficiencia.indemnizacion')}" labelposition="top"
									 disabled="disabled" 
									 
									 />	
						</div>	
						<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
							<s:textfield id="eficienciaProvision" 
									 maxlength="9"
									 name="indicadores.eficienciaProvision"  
									 cssClass="txtfield modoSoloLecturaSalvamento" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.eficiencia.provision')}" labelposition="top"
									 disabled="disabled" 
									 />	
						</div>															

				</div>
			
			</div>
		</div>
	</div>
</div>	


<script type="text/javascript" >
jQuery(".modoSoloLecturaSalvamento").css("background-color","#EEEEEE");
jQuery(".modoSoloLecturaSalvamento").attr("disabled", true);
</script>
