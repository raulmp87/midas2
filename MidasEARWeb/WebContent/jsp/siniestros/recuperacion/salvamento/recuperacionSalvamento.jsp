<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<div id="contenido_recuperacionSalvamento" style="width:99%;position: relative;">	
	<div id="divInferior" style="width: 100%;" class="floatLeft">
		<div id="divGenerales" style="width: 100%;"  class="floatLeft">
			<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
				<br/>
				<br/>
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:textfield id="marca" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.descMarca"  
									 cssClass="txtfield cleaneable"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.marca')}" labelposition="top"
									 cssStyle="width:100%;" 
									 readonly="true" />														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:textfield id="tipo" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.descEstilo"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tipo')}"  labelposition="top"
									 cssStyle="width:100%;" 								
									 readonly="true" />
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:textfield id="modelo" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.modeloVehiculo"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.modelo')}" labelposition="top" 
									 readonly="true" />							
					</div>
				</div>
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:textfield id="serie" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.serie')}" labelposition="top" 
									 cssStyle="width:100%;" 
									 readonly="true" />														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:textfield id="motor" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroMotor"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.motor')}" labelposition="top" 
									 cssStyle="width:100%;" 
									 readonly="true" />
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:textfield id="color" name="recuperacion.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.descColor"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.color')}" labelposition="top" 									  
									 readonly="true" />							
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:checkbox  id="abandono" name="recuperacion.abandonoIncosteable"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.abandonoIncosteable')}" 
									 labelposition="left"/>														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:select id="estadoUbicacion" name="recuperacion.edoUbicacion.id" list="estados"  onchange="cargarCiudades(this.value);" 
								  cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.edoUbicacion')}" labelposition="top"								  
								 />	
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:select id="ciudadUbicacion" name="recuperacion.ciudad.id" list="ciudades"  
								  cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.ciudadUbicacion')}" labelposition="top" 
								 />							
					</div>									
				</div>
				<div class="">					
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:textfield id="ubicacionDeterminacion" name="recuperacion.ubicacionDeterminacion"  
									 cssClass="txtfield jQrequired"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.ubicacionDeterminacion')}" labelposition="top" 
									 cssStyle="width:100%;"  maxlength="500" />
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 65%;" >
						<s:textfield  id="direccionSalvamento" name="recuperacion.direccionSalvamento"  
									 cssClass="txtfield jQrequired"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.direccionUbicacion')}" labelposition="top" 
									 cssStyle="width:100%;" maxlength="500" />														
					</div>	
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:textfield  id="estacionamiento" name="recuperacion.estacionamientoTaller"  
									 cssClass="txtfield jQrequired"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.estacionamientoTaller')}" labelposition="top"									  
									 cssStyle="width:100%;"  maxlength="500"/>														
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >						
						<s:textfield id="contactoTallerUbicacion" name="recuperacion.contactoTallerUbicacion"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.contactoTallerUbicacion')}" labelposition="top" 
									 cssStyle="width:100%;" maxlength="500"/>							
					</div>				
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >						
						<s:textfield id="valorEstimado" name="recuperacion.subtotalValorEstimado"  
									 cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.subtotalValorEstimado')}" labelposition="top" 
									 readonly="true" />							
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >						
						<s:textfield id="valorEstimado" name="recuperacion.ivaValorEstimado"  
									 cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.ivaValorEstimado')}" labelposition="top" 
									 readonly="true" />							
					</div>		
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:textfield id="valorEstimado" name="recuperacion.valorEstimado"  
									 cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.valorEstimado')}" labelposition="top" 
									 readonly="true" />							
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:hidden name="recuperacion.tipoSalvamento"/>
						<s:textfield  id="tipoSalvamento" name="recuperacion.tipoSalvamentoStr"  size="30"
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.tipoSalvamento')}" labelposition="top" 
									 readonly="true" />														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:textfield  id="fechaDevolucion" name="recuperacion.fechaDevolucion"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fechaDevolucion')}" labelposition="top" 
									 readonly="true" />		
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
							<s:textfield  id="ptDocumentada" name="recuperacion.ptDocumentada"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.ptDocumentada')}" labelposition="top" 
									 size="6"
									 readonly="true" />	
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:textfield  id="indemnizada" name="recuperacion.indemnizada"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.indemnizada')}" labelposition="top" 
									 size="6"
									 readonly="true" />														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >					
						<s:textfield  id="fechaIndemnizacion" name="recuperacion.fechaIndemnizacion"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fechaIndemnizacion')}" labelposition="top" 
									 readonly="true" />		
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >	
						<div id="fechaInicioSubastaPicker">			
						<sj:datepicker name="recuperacion.fechaInicioSubasta"
								label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fechaInicioSubasta')}" 
								labelposition="top" changeMonth="true"
								changeYear="true" buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" id="fechaOcurrido" maxlength="18"
								cssClass="txtfield setNew" size="12"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</div>
						<div id="fechaInicioSubastaInput" style="display:none" >
							<s:textfield name="recuperacion.fechaInicioSubasta"  
								cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fechaInicioSubasta')}" labelposition="top" 
								readonly="true" />							
						</div>					
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >
						<s:textfield  id="antiguedadSubasta" name="recuperacion.antiguedadSubasta"  
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.antiguedadSubasta')}" labelposition="top"
									 size="6" 
									 readonly="true" />														
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 35%;" >							
						<s:label name="recuperacion.subastaActual"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.cantidadSubastas')}" labelposition="top"/>
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						&nbsp;
					</div>
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 70%;" >
						<s:textarea  id="motivoAbandonoIncosteable" name="recuperacion.motivoAbandonoIncosteable" cols="126" rows="4" 
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.motivoAbandonoIncosteable')}" labelposition="top" 
									 />														
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:textfield  id="determinadoAbandonoPor" name="recuperacion.codigoUsuarioAbandono"  
							 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.determinadaAbandonoIncosteablePor')}" labelposition="top" 
							 readonly="true" />	
					</div>			
				</div>
				<div class="">
					<div class="floatLeft divInfDivInterno" style="width: 70%;" >
						<s:textarea  id="motivoCancelacion" name="recuperacion.motivoCancelacion" cols="126" rows="4" 
									 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.motivoCancelacion')}" labelposition="top" 
									 />														
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
						<s:textfield  id="codigoUsuarioCancelacion" name="recuperacion.codigoUsuarioCancelacion"  
							 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionGeneral.canceladaPor')}" labelposition="top" 
							 readonly="true" />	
					</div>			
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
mostrarSiDatePickerSiVentaActiva();
</script>