<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


						
<div id="contenido_prorrogaSalvamento" style="width:99%;position: relative;">	
	<div id="divInferior" style="width: 100%;" class="floatLeft">
		<div id="divGenerales" style="width: 100%;"  class="floatLeft">
			<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
				<br/>
				<!--  <div>
					<div class="floatLeft divInfDivInterno" style="width: 85%;  " >					
						<s:checkbox name="" onClick="habilitaCamposSolicitudProrroga();" label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.solicitud.prorroga')}" />
					</div>
				</div>-->
				
<%-- 				<s:if test=" (recuperacion.solicitudProrroga == true) && (recuperacion.autorizacionProrroga == false ) ">
					<div>
						<div class="floatLeft divInfDivInterno" style="width: 85%;  " >
							<span style="background-color:red; color:white; font-size:14px;  ">PRORROGA SOLICITADA EN ESPERA DE AUTORIZACION</span>
						</div>
					</div>
				</s:if>
				<s:if test="(recuperacion.solicitudProrroga == true) && (recuperacion.autorizacionProrroga == true ) ">
					<div>
						<div class="floatLeft divInfDivInterno" style="width: 85%;  " >
							<span style="background-color:red; color:white; font-size:14px;  ">SOLICITUD DE PRORROGA APROBADA</span>
						</div>
					</div>
				</s:if>
				<s:if test="(recuperacion.solicitudProrroga == true) && (recuperacion.autorizacionProrroga == false ) && (recuperacion.fechaAutorizacionProrroga == '' ) ">
					<div>
						<div class="floatLeft divInfDivInterno" style="width: 85%;  " >
							<span style="background-color:red; color:white; font-size:14px;  ">SOLICITUD DE PRORROGA RECHAZADA</span>
						</div>
					</div>
				</s:if> --%>
				
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 85%;  " >					
						<s:textarea 
							name="recuperacion.comentariosProrroga"
							rows="5" 
							cssClass="prorrogaaAutorizada modoConsulta solicitudEnviada"
							cols="150" 
							id="comentarios" 
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comentarios')}" 
							
							/>
						
					</div>
				</div>	
				<div>
					<s:if test="(recuperacion.solicitudProrroga == true) ">
						<div class="floatLeft divInfDivInterno" style="width: 20%;" >
							<s:textfield id="prorrogaReadOnly" 
									 name="recuperacion.fechaLimiteProrroga"  
									 cssClass="txtfield modoConsulta prorrogaaAutorizada" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.limie.prorroga')}" labelposition="top" 
									 readonly="true" />			
						</div>
					</s:if>
					<s:else>
						<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<label><s:text
										name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.limie.prorroga')}" />:</label>
								<div style="margin-top: 5%">
									<sj:datepicker name="recuperacion.fechaLimiteProrroga"
										changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaLimiteProrroga" maxlength="10"
										size="12" disabled="false"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield modoConsulta prorrogaaAutorizada">
									</sj:datepicker>
								</div>	
	 							
						</div>
					</s:else>
					<div class="floatLeft divInfDivInterno" style="width: 20%;" >						
						<s:textfield id="solicitante" 
									 name="recuperacion.solicitanteProrroga"  
									 cssClass="txtfield modoConsulta prorrogaaAutorizada" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.solicitante')}" labelposition="top" 
									 readonly="true" />							
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 50%;" >						
						<s:textfield id="fechaSolicitud" 
									name="recuperacion.fechaSolicitudProrroga"  
									cssClass="txtfield prorrogaaAutorizada modoConsulta"  
									label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.solicitud')}" labelposition="top" 
									readonly="true" />							
					</div>
				</div>			
			
				<s:if test=" (recuperacion.solicitudProrroga == true) ">
					<div>
						
						<div>
							<div class="floatLeft divInfDivInterno" style="width: 100%; margin-top:5%; " >
								<hr>
							</div>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 20%; margin-top:1%;" >	
							
								<s:checkbox
									id="opcAutorizacion" 
									name="opcAutorizacion"
									labelposition="right"
									onclick="opcAutorizarProrroga(this);"
									label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.autorizacion.prorroga')}" 
								/>
								<s:hidden id="seleccionAutorizaSubasta" name="seleccionAutorizaSubasta" value="n" />				
												
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 20%;  margin-top:1%;" >						
							<s:textfield id="autorizador" 
										 name="recuperacion.autorizadorProrroga"   
										 size="20"
										 cssClass="txtfield prorrogaaAutorizada" 
										 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.autorizador')}" 
										 labelposition="top" 
										 readonly="true" />							
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 20%;  margin-top:1%;" >						
							<s:textfield id="fechaAutorizacion" 
										 name="recuperacion.fechaAutorizacionProrroga"
										 cssClass="txtfield prorrogaaAutorizada" 
										 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.autorizacion')}" labelposition="top" 
										 readonly="true" />						
						</div>
						
					</div>
				</s:if>
			</div>
		</div>
	</div>
</div>	

<s:hidden name="recuperacion.solicitudProrroga"         id="solicitudProrroga" />
<s:hidden name="recuperacion.autorizacionProrroga"      id="autorizacionProrroga" />
<s:hidden name="recuperacion.fechaAutorizacionProrroga" id="fechaAutorizacionProrroga" />
<s:hidden name="recuperacion.fechaSolicitudProrroga"    id="fechaSolicitudProrroga" />
<s:hidden name="isUsuarioValidoAprobarProrroga"         id="isUsuarioValidoAprobarProrroga" />

<script type="text/javascript">
initProrrogaAutorizada();
</script>
