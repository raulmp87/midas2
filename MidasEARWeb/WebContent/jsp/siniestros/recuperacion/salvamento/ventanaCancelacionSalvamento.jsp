<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>

<s:form id="cancelarRecuperacionForm" action="cancelarSalvamento" namespace="/siniestros/recuperacion/recuperacionSalvamento" name="cancelarRecuperacionForm">
	<s:hidden name="recuperacion.id"/>
	<s:hidden id="idRecuperacion" name="recuperacionId"/>
	<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloMotivoCancelacion">
								<s:text name="midas.siniestros.recuperacion.titulocancelacion" />
							</div> 
	                    </td>
	                  </tr>
					  
	                  <tr>
						<td colspan="6" >
							<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
								<tbody>
									<tr>
										<td>
											<div id="contenedorTextArea">
												<s:textarea name="recuperacion.motivoCancelacion" id="motivoCancelacion"
												cssClass="textarea" cssStyle="font-size: 10pt;"	cols="80" rows="3"
												onchange="truncarTexto(this,500);"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					  </tr>
	                  
	                  <tr>
	                  	<td colspan="6">
	                  		<div  id="cerrarBtn" class="btn_back w80" style="display: inline; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="if(confirm('La informaci\u00F3n que no haya sido guardada se perder\u00E1')){javascript:cerrarVentanaCancelarRecuperacion('vmCancelarRecueperacionSalvamento', jQuery('#idRecuperacion').val(), jQuery('#estatusRecuperacion').val(), 'consultarRecuperacion(' + jQuery('#idRecuperacion').val() + ')');}"> 
		                        <s:text name="midas.boton.cerrar" /> </a>
							</div>
							
							<div id="cancelarBtn" class="btn_back w180" style="display: none; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="javascript:cancelarRecuperacionSalvamento(jQuery('#idRecuperacion').val());"> 
		                        <s:text name="midas.siniestros.recuperacion.botoncancelar" /> </a>
							</div>
						</td>
	                  </tr>
	            </tbody>
	</table>
</s:form>
<script type="text/javascript">
jQuery(document).ready(function(){
	configurarVentanaCancelacion();
});
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionSalvamento.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>