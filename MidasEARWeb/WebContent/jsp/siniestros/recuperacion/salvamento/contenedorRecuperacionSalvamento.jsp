<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/recuperacion/contenedorGeneralRecuperacionSalvamento.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionSalvamento.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/ventaDeSalvamento.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/solicitudProrrogaSalvamento.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/historialVentaSalvamento.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/confirmacionIngreso.js'/>"></script>


<s:form name="salvarSalvamento" id="salvarSalvamentoForm" >
<s:hidden id="soloConsulta" 	name="soloConsulta" />
<s:hidden id="h_soloLectura" 	name="soloConsulta" />
<s:hidden id="esNuevoRegistro" 	name="esNuevoRegistro" />
<s:hidden id="esAccionCancelar" 	name="esAccionCancelar" />
<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
<s:hidden name="recuperacion.id" />
<s:hidden name="recuperacion.estimacion.id" />
<s:hidden name="recuperacion.tipoOrdenCompra" />
<s:hidden name="pestaniaSeleccionada" id="pestaniaSeleccionada" />


<!-- VENTA SALVAMENTO -->
<s:hidden name="tieneVentaActiva" id="tieneVentaActiva" />
<s:hidden name="esEdicionVentaRecuperacion" id="esEdicionVentaRecuperacion" />


	<div id="recuperacionGenericaDiv">
			<s:include value="/jsp/siniestros/recuperacion/recuperacionGenerica.jsp"></s:include>
	</div>
	
	<div id="contenido_DefinirTitulo" style="width:99%;position: relative;">
		
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.titulo')}"/>
			</div>
		</div>
	</div>
	
	<div id="contenido_Pestanas" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 99%; width: 100%" id="recuperacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div selected="1" id="recuperacionSalvamento" name="Recuperaci&oacute;n">
						<s:include value="/jsp/siniestros/recuperacion/salvamento/recuperacionSalvamento.jsp"></s:include>			
				</div>
				<div id="ventaSalvamento" name="Venta Salvamento" href="http://void" extraAction="javascript: void(0);">
					<s:include value="/jsp/siniestros/recuperacion/salvamento/ventaDeSalvamento.jsp"></s:include>	
				</div>
				<div id="confirmacionIngresos" name="Confirmación de Ingresos"  href="http://void" extraAction="javascript: void(0);">
					<s:include value="/jsp/siniestros/recuperacion/salvamento/confirmacionDeSalvamento.jsp"></s:include>	
				</div>
				<div id="indicadores" name="Indicadores"  href="http://void" extraAction="javascript: void(0);">
					<s:include value="/jsp/siniestros/recuperacion/salvamento/indicadoresDeSalvamento.jsp"></s:include>	
				</div>
				<div id="prorrogaSalvamento" name="Prorroga" href="http://void" extraAction="javascript: void(0);">
					<s:include value="/jsp/siniestros/recuperacion/salvamento/prorrogaDeSalvamento.jsp"></s:include>	
				</div>		
				<div id="historialSubasta" name="Historial Subasta"  href="http://void" extraAction="javascript: void(0);">
					<s:include value="/jsp/siniestros/recuperacion/salvamento/historialDeSubastasDeSalvamento.jsp"></s:include>	
				</div>							
			</div>
		</div>
		
	</div>
	
	<div id="contenido_Btn" style="width:100%;position: relative; padding-top: 10px">
			<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
				<div id="cerrarBtn" class="btn_back w150"
						style="display: inline; float: left;margin-left: 1%;">
						<a href="javascript: void(0);" onclick="cerrarRecuperacion();"> <s:text
							name="Cerrar" /> </a>
				</div>
			</div>				
			
			<s:if test="!soloConsulta">
			<div class="floatRight divInfDivInterno" id="btnSalvarSalvamento" style="width: 15%; padding-top: 5px" >
				<div id="guardarBtn" class="btn_back w150"
						style="display: inline; float: left;margin-left: 1%;">
						<a href="javascript: void(0);" onclick="guardarSVM();"> <s:text
							name="Guardar" /> </a>
				</div>
			</div>
			</s:if>
				
			<s:if test="esAccionCancelar==true && recuperacion.estatus != \"CANCELADO\"">
				<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
					<div id="cancelarBtn" class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="cancelarRecuperacionSalvamento();"> <s:text
									name="Cancelar" /> </a>
					</div>
				</div>
			</s:if>
	</div>
	
</s:form>
<script type="text/javascript">
	jQuery(document).ready(
			function(){
				initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA      
				dhx_init_tabbars();  
				initReadOnly();
				// ESCRIBE EN HIDDEN LA PESTAÑA SOBRE LA QUE ESTA EL USUARIO
				initIdentificadorTabs();
			}
		);
</script>
