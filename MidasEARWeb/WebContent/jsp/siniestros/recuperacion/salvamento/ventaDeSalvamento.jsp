<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<style type="text/css">
	#superior{
		height: 300px;
		width: 99%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>

	<div id="superior" class="divContenedorO" style="width: 100%; height: 100%;">
	 		<s:hidden name="ventaSalvamento.id"/>
			<div id="superiorI">
				<div id="ventaActivaDiv" style="display:none" >
					<div id="SIS"><span style="background-color:red; color:white; font-size:14px;  ">NO TIENE VENTA ACTIVA</span></div>
				</div>
				<div id="SIS">
					<div class="floatLeft" style="width: 30%;">
						<s:select list="ctgCompradores" label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprador')}" headerKey="" onchange="obtenerCorreoPrestador(this.value);"
							headerValue="%{getText('midas.general.seleccione')}" name="ventaSalvamento.comprador.id"
							id="compradores" cssStyle="width: 90%;" cssClass="cleaneable txtfield obligatorio modoConsulta modoEdicion readOnlyCombo " />
					</div>
					<div class="floatLeft" style="width: 40%;">
						<s:textfield id="correo" name="ventaSalvamento.correo" cssClass="txtfield  modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.correo')}" labelposition="top" cssStyle="width:90%;"/>
					</div>
					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="fechaAsignacion" name="ventaSalvamento.fechaCreacion" cssClass="txtfield modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.asignacion')}" readOnly="true" labelposition="top"
							cssStyle="width:40%;" />
					</div>
				</div>
				<div id="SIS">
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="subtotal" name="ventaSalvamento.subtotal" cssClass="txtfield formatCurrency modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.subtotal')}" readOnly="true" labelposition="top" cssStyle="width:60%;"/>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="porIva" name="ventaSalvamento.porcentajeIva" cssClass="txtfield modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.porIva')}" readOnly="true" labelposition="top" cssStyle="width:60%;"/>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="iva" name="ventaSalvamento.iva" cssClass="txtfield formatCurrency modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.iva')}" readOnly="true" labelposition="top"
							cssStyle="width:60%;" />
					</div>
					<div class="floatLeft" style="width: 22%;">
						<s:textfield id="totalVenta" name="ventaSalvamento.totalVenta" cssClass="txtfield formatCurrency modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.total.venta')}" onblur="recalcularVentaSalvamentoTotal();" labelposition="top" cssStyle="width:60%;"/>
					</div>
				</div>
				<div id="SIS">
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="noSubasta" name="ventaSalvamento.noSubasta" cssClass="txtfield modoConsulta modoEdicion"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.no.subasta')}" maxlength="20" labelposition="top" cssStyle="width:60%;"/>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:if test="esEdicionVentaRecuperacion">
							<s:textfield name="ventaSalvamento.fechaCierreDeSubasta" id="fechaCierreSubasta" cssClass="txtfield modoConsulta modoEdicion" 
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.cierre.subasta')}" readOnly="true" labelposition="top"
							cssStyle="width:60%;" />
						</s:if>
						<s:else>
						<label>
							<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.cierre.subasta')}" />:</label>
							<div style="margin-top: 5%">
								
								<sj:datepicker name="ventaSalvamento.fechaCierreDeSubasta"
											changeMonth="true" changeYear="true"
											buttonImage="/MidasWeb/img/b_calendario.gif"
											buttonImageOnly="true" id="fechaCierreSubasta" maxlength="10"
											size="12" 
											onkeypress="return soloFecha(this, event, false);"
											onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
											cssClass="cleaneable txtfield modoConsulta modoEdicion ">
								</sj:datepicker>
							</div>
						</s:else>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="cantidadSubasta" name="recuperacion.subastaActual" cssClass="txtfield modoConsulta modoEdicion" 
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.cantidad.subastas')}" readOnly="true" labelposition="top"
							cssStyle="width:60%;" />
					</div>
				</div>
				<div id="SIS">
					<div class="floatLeft" style="width: 25%;">
						<s:textfield id="noFactura" name="ventaSalvamento.noFactura" maxlength="20" cssClass="txtfield modoConsulta"
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.no.factura')}" labelposition="top" cssStyle="width:60%;"/>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<label><s:text
									name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.factura')}" />:</label>
							<div style="margin-top: 5%">
								<sj:datepicker name="ventaSalvamento.fechaFactura"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaFacura" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield modoConsulta">
								</sj:datepicker>
							</div>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<label><s:text
									name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.salvamento')}" />:</label>
							<div style="margin-top: 5%">
								<sj:datepicker name="ventaSalvamento.fechaEntrega"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaEntregaSalvamento" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield modoConsulta">
								</sj:datepicker>
							</div>
					</div>
					<!-- <div class="floatLeft botonSalvarSalvamento" style="width: 15%;">
						<div id="btnGuardar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
							<a href="javascript: void(0);" onclick="guardarVentaSalvamento();"> <s:text name="Guardar" /> </a>
						</div>
					</div>-->
				</div>
				<div id="SIS">
					<div id="indicador"></div>
					<div id="listadoGridReferencias" style="width: 72%; height: 180px;"></div>
					<div id="pagingArea"></div>
					<div id="infoArea"></div>
				</div>				
			</div>	
		
	</div>	

	
<script type="text/javascript">
	cargaUrlReferenciasBancarias();
	aplicarSoloConsulta();
	aplicarSoloEdicion();
	mostrarSiVentaActiva();
</script>	