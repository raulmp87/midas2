<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style>
<!--
.fieldset1
{
    border:2px solid green;
    -moz-border-radius:8px;
    -webkit-border-radius:8px;	
    border-radius:8px;	
}
-->
</style>
						
<div id="contenido_historialSubastaSalvamento" style="width:99%;position: relative;">	
	<div id="divInferior" style="width: 100%;" class="floatLeft">
		<div id="divGenerales" style="width: 100%;"  class="floatLeft">
			<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
				<br/>
				<br/>
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 95%;" >	
						<div id="indicador"></div>
						<div id="listadoSubastasSalvamentoGrid" style="width: 95%; height: 200px;"></div>
						<div id="pagingAreaHistorial"></div>
						<div id="infoArea"></div>
					</div>
				</div>
				<div style="margin-top:4%; float:left; width :91% ">
				<fieldset class="fieldset1" style="min-height:120px;">
					<legend><b> Cancelación de la Adjudicación </b> </legend>
					<div style="width:100%; float: left; ">
							<div id="cancelarAdjudicacion" class="btn_back "
								style="display: inline; float: left;">
								<a href="javascript: void(0);"
									onclick="habilitarTextComentario();"> <s:text
										name="midas.siniestros.recuperacion.recuperacionSvm.cancelar.adjudicacion" />
								</a>
							</div>
					</div>
					
					<div>
										
							<s:textfield id="tipo" name="ventaSalvamento.comentarioCancelacion"  
										 disabled="true"
										 cssClass="txtfield" id="comentarioCancelacion" label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.causa.cancelacion')}"  labelposition="top"
										 cssStyle="width:100%;" />
						
					</div>	
					<div>
						<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
							<s:textfield id="modelo" name="fechaCancelacion"  
										 cssClass="txtfield" label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.fecha.cancelacion')}" labelposition="top" 
										 readonly="true" />							
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 30%;" >						
							<s:textfield id="modelo" name="usuarioCancelacion"  
										 cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.cancelada.por')}" labelposition="top" 
										 readonly="true" />							
						</div>
						<!-- <div class="floatLeft" style="width:90%;">
							<div id="btnGuardar" class="btn_back " style="display: inline; float: right;">
								<a href="javascript: void(0);" onclick="cancelarAdjudicacion();"> <s:text name="midas.siniestros.recuperacion.recuperacionSvm.cancelar.adjudicacion" /> </a>
							</div>
						</div> -->
					</div>	
					</fieldset>	
				</div>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
cargaUrlSubastaSalvamento();
</script>