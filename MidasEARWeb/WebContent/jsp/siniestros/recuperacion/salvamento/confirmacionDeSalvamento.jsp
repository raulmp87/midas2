<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


						
<div id="contenido_confirmacionSalvamento" style="width:99%;position: relative;">	
	<div id="divInferior" style="width: 100%;" class="floatLeft">
		<div id="divGenerales" style="width: 100%;"  class="floatLeft">
			<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
						<s:select 
							list="ctgBancos" 
							label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.bancos')}" 
							headerKey="" 
							headerValue="%{getText('midas.general.seleccione')}" 
							name="recuperacion.bancoConfirmacionIngreso"
							id="bancos" cssStyle="width: 90%;" cssClass="cleaneable txtfield modoConsulta " />
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%;" >						
						<s:textfield id="cuenta" 
									 maxlength="9"
									 name="recuperacion.cuentaConfirmacionIngreso"  
									 cssClass="txtfield modoConsulta " 
									 onkeypress="return soloNumeros(this, event, true)"
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.cuenta')}" labelposition="top" 
									 />							
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%; " >
					
						<label><s:text
								name="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.fechaConfirmacionDeposito')}" />:</label>
						<div style="margin-top: 5%">
						<sj:datepicker name="recuperacion.fechaDepositoConfirmacionIngreso"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaDepositoConfirmacionIngreso" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield modoConsulta ">
						</sj:datepicker>
						</div>	
 						
					</div>

				</div>			
				<div>
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%;" >						
						<s:textfield id="montoConfirmacion" 
									 name="recuperacion.montoConfirmacionIngreso"  
									 cssClass="txtfield modoConsulta formatCurrency  modoConsultaConfirmacion "
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.montoConfirmado')}" 
									 labelposition="top"
									 onkeypress="return soloNumeros(this, event, true)" 
									  />							
					</div>	
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%;" >						
						<s:textfield id="confirmadoPor" 
									 name="recuperacion.confirmadoPor"  
									 cssClass="txtfield modoConsulta modoConsultaConfirmacion " 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.confirmadoPor')}" labelposition="top" 
									 readonly="true"  />							
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 30%; margin-top:2%;" >						
						<s:textfield id="fechaConfirmacionIngreso" 
									 name="recuperacion.fechaDepositoConfirmacion"  
									 cssClass="txtfield modoConsulta modoConsultaConfirmacion" 
									 label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.comprobante.fechaConfirmacionIngreso')}" labelposition="top" 
									 readonly="true" />							
					</div>					

				</div>			
			
			</div>
		</div>
	</div>
</div>	


<s:hidden id="confirmacionDepositoSalvamento" name="recuperacion.fechaDepositoConfirmacionIngreso" />

<script type="text/javascript" >
initMostrarConfirmacionIngreso();
</script>
