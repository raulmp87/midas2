<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<%-- <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script> --%>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<%-- <script type="text/javascript"> --%>
<!-- 	jquery143 = jQuery.noConflict(true); -->
<%-- </script> --%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
	var validarRegistrarPath = '<s:url action="registrar" namespace="/siniestros/recuperacion/recuperacionProveedor/notasDeCredito"/>';
	var mostrarNotasPendientesPath = '<s:url action="mostrarNotasPendientes" namespace="/siniestros/recuperacion/recuperacionProveedor/notasDeCredito"/>';
	var cancelarNotaCreditoPath = '<s:url action="cancelar" namespace="/siniestros/recuperacion/recuperacionProveedor/notasDeCredito"/>';
</script>

<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>

<s:form id="recepcionNotaCreditoForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.recuperacion.notasDeCredito.informacionRecuperacion"/>	
</div>	
<s:hidden id="h_batchId" name="batchId"/>
<s:hidden id="h_idRecuperacion" name="recuperacion.id"/>
<s:hidden id="h_soloLectura" name="soloLectura"/>
<s:hidden id="h_esEditable" name="esEditable"/>
<s:hidden id="h_noProveedor" name="idProveedor"/>
<s:hidden id="h_nombreProveedor" name="nombreProveedor"/>
<s:hidden id="h_folioFactura" name="folioFactura"/>

<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.noProveedor"/>
				</th>					
			    <td>  
			    	<s:textfield id="idProveedor" disabled="true" value="%{idProveedor}" cssClass="cajaTextoM2 w50" />				    	  				 
				</td>			   	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.proveedor"/>
				</th>
				<td>
					<s:textfield id="nombrePrestadorServicio" disabled="true" value="%{nombreProveedor}" cssClass="cajaTextoM2 w250" />										 
				</td>	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.noFactura"/>
				</th>
				<td colspan="5" align="left">
					<s:textfield id="numeroFactura" disabled="true" value="%{folioFactura}" cssClass="cajaTextoM2 w100" />										 
				</td>
	    	</tr>	    	
	    	<tr>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.siniestro"/>
				</th>					
			    <td>  
			    	<s:textfield id="t_siniestro" disabled="true" name="recuperacion.reporteCabina.siniestroCabina.numeroSiniestro" cssClass="cajaTextoM2 w100" />				    	  				 
				</td>			   	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.ordenCompra"/>
				</th>
				<td>
					<s:textfield id="t_ordenCompra" disabled="true" name="recuperacion.ordenCompra.id" cssClass="cajaTextoM2 w50" />										 
				</td>	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.noRecuperacion"/>
				</th>
				<td>
					<s:textfield id="t_idRecuperacion" disabled="true" name="recuperacion.numero" cssClass="cajaTextoM2 w50" />										 
				</td>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.fechaRegistro"/>
				</th>
				<td colspan="3" align="left">
					<s:textfield id="t_fechaRegistro" disabled="true" name="recuperacion.fechaCreacion" cssClass="cajaTextoM2 w100" />										 
				</td>
	    	</tr>
	    	<tr>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.concepto"/>
				</th>					
			    <td colspan="9" align="left">  
			    	<s:textfield id="t_siniestro" disabled="true" name="recuperacion.conceptoDevolucion" cssClass="cajaTextoM2 w400" />				    	  				 
				</td>			   	
	    	</tr>
	    	<tr>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.iva"/>
				</th>					
			    <td>  
			    	<s:textfield id="t_iva" disabled="true" name="recuperacion.iva" cssClass="cajaTextoM2 w100 formatCurrency" />				    	  				 
				</td>			   	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.ivaRetenido"/>
				</th>
				<td>
					<s:textfield id="t_ivaRetenido" disabled="true" name="recuperacion.ivaRetenido" cssClass="cajaTextoM2 w100 formatCurrency" />										 
				</td>	
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.isr"/>
				</th>
				<td>
					<s:textfield id="t_isr" disabled="true" name="recuperacion.isr" cssClass="cajaTextoM2 w100 formatCurrency" />										 
				</td>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.subTotal"/>
				</th>
				<td>
					<s:textfield id="t_subTotal" disabled="true" name="recuperacion.subTotal" cssClass="cajaTextoM2 w100 formatCurrency" />										 
				</td>
				<th>
					<s:text name="midas.siniestros.recuperacion.notasDeCredito.total"/>
				</th>
				<td>
					<s:textfield id="t_total" disabled="true" name="recuperacion.montoTotal" cssClass="cajaTextoM2 w100 formatCurrency" />										 
				</td>
	    	</tr>
	 	</tbody>
	 </table>		
</div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.recuperacion.notasDeCredito.listadoDeNotas"/>	
</div>	
<div id="spacer2" style="height: 15px"></div>
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<s:if test="esEditable">
		<table style="width: 98%;">
			<tr>
				<td width="200px;" >
					<div class="btn_back w200" id="cargar" >
						<a href="javascript: void(0);" title="Seleccione un arhivo comprimido extensión ZIP que contenga las notas de crédito a cargar (XMLs) o bien un solo archivo extensión XML para 1 sola nota de crédito"							
						   onclick="cargarZipNotasCredito();">
							<s:text name="midas.siniestros.pagos.notasCredito.cargarNotas.boton"/>
						</a>
					</div>
				</td>
				<td>
				<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
					<font color="#FF6600">
					<s:text name="test">El tamaño máximo del archivo puede ser 40 MB</s:text>			
					</font>
				</div>
				</td>
				<td>
				<td>
					<div style="display:inline; width:175px; float: right; color: #00a000; font-size: 10;" id="divExcelBtn">
		            	<s:text name="test">Total Notas Crédito Cargadas:</s:text>	
		            </div>
				</td>
				<td width="100px;">
					<s:textfield id="t_umNotasCargadas" disabled="true" name="numNotasCargadas" cssStyle="float: right;" cssClass="cajaTextoM2 w80" />
				</td>					
			</tr>		
		</table>
	</s:if>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="divNotasDeCredito">    
   <div id="indicador"></div>	
	<div id="notasCreditoListadoGrid" style="width:98%;height:180px">	
	<div id="pagingArea"></div><div id="infoArea"></div>
   </div>
</div>


<div id="spacer1" style="height: 10px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right" style="width: 100%;">
			<tbody>
				<tr>
				<td> 
					         
            	</td>
            	<td>
            		<div id="cerrarBoton" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
						<a href="javascript: void(0);" onclick="cerrarNotas();">
							<s:text name="midas.boton.cerrar" /> 
						</a>
					</div>
					
					<s:if test="batchId != null && esEditable ">
						<div id="validarRegistrarBoton" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="validarRegistrarNC();">
								<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.validarRegistrar.boton" /> 
							</a>
						</div>
					</s:if> 
				</td>				
				</tr>
			</tbody>
	</table>		
</div>
</s:form>


<script	src="<s:url value='/js/midas2/siniestros/recuperacion/listadoRecuperaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/notasDeCredito/notasDeCreditoRecuperacionSiniestros.js'/>"></script>

<script type="text/javascript">
	jQuery(document).ready(
		function(){
			listarNotasCreditoPendientes();
			initCurrencyFormatOnTxtInput();
			
		}
	);

</script>
