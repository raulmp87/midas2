<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="checkSeleccion" type="ch" width="75" sort="int"><s:text name="midas.siniestros.pagos.notasCredito.seleccionNC"/></column>		
		<column id="folio" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.folio"/></column>
		<column id="fechaNotaCredito" type="ro" width="100" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.fecha"/></column>
		<column id="fechaRegistro" type="ro" width="80" sort="str"><s:text name="midas.siniestros.pagos.notasCredito.fechaRegistro"/></column>		
		<column id="estatus" type="ro" width="*" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.estatus"/></column>
		<column id="subtotal" type="ron" width="100" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.subtotal"/></column>		
		<column id="iva" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.iva"/></column>
		<column id="ivaRet" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ivaRetenido"/></column>
		<column id="isr" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.isr"/></column>
		<column id="total" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>	
		<column id="ver" type="img" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.accion.verDetalle"/></column>	
		<column id="cancelar" type="img" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.recuperacion.notasDeCredito.cancelar"/></column>	
	</head>	  		
	<s:iterator value="listaNotasCredito" status="stats">
		<row id="<s:property value="factura.id"/>">
			<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>	
		    <cell><s:property value="factura.numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura.fechaFactura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="factura.estatusStr" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura.subTotal" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.iva" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.ivaRetenido" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.isr" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.montoTotal" escapeHtml="false" escapeXml="true"/></cell>	
			<s:if test="mensajes.size > 0">		
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle Validaciones^javascript: verDetalleValidacionNC(<s:property value="id"/>,'<s:property value="factura.numeroFactura"/>')^_self</cell>								
			</s:if>	
			<s:else>
                	<cell>../img/pixel.gif^Sin mensajes^^_self</cell>
             </s:else>		
			<s:if test="esEditable && factura.estatus == \"R\"">
					<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Cancelar^javascript:cancelarNC(<s:property value="factura.id" escapeHtml="false" escapeXml="true" />,true)^_self</cell>
             </s:if>
             <s:else>
                	<cell>../img/pixel.gif^Cancelar Deshabilitado^^_self</cell>
             </s:else>	
		</row>
	</s:iterator>
</rows>
