<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="filtrosBusquedaListarRecuperacionCia" id="filtrosBusquedaListarRecuperacionCia" method="post">

	<table width="95%" id="filtros">
		<tr>
			<td class="titulo" colspan="3">
				<s:if test="tipoModulo==1">
					<s:text name="midas.siniestros.recuperacion.compania.listar.programarappingresos"/>					
				</s:if>
				<s:elseif test="tipoModulo==2">
					<s:text name="midas.siniestros.recuperacion.compania.listar.registroentregacartas"/>					
				</s:elseif>
				<s:elseif test="tipoModulo==3">
					<s:text name="midas.siniestros.recuperacion.compania.listar.reasignacionelaboracioncarta"/>			
				</s:elseif>
				<s:elseif test="tipoModulo==4">
					<s:text name="midas.siniestros.recuperacion.compania.listar.enviorecepcioncartas"/>					
				</s:elseif>
				<s:else>
					<s:text name="midas.siniestros.recuperacion.compania.listar.titulobusqueda"/>
				</s:else>											
			</td>
		</tr>
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.numrecuperacion" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict jQnumeric"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="numeroRecuperacion" 
								name="recuperacionCiaFiltro.numeroRecuperacion"								
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.compania.listar.tiporecuperacion" />:
				<s:select 	list="lstTipoRecuperacion"
							id="lstTipoRecuperacion"
							name="recuperacionCiaFiltro.tipoRecuperacion"
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130" 
							value="CIA"/>	
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.compania.listar.estatus" />:
				<s:select 	list="lstEstatusRecuperacion"
							id="lstEstatusRecuperacion"
							name="recuperacionCiaFiltro.estatus"
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>	
			
		</tr>
		<tr>
			<td align="right"><s:text	name="midas.siniestros.recuperacion.compania.listar.mediorecuperacion" />
				<s:select 	list="lstMedioRecuperacion"
							id="lstMedioRecuperacion"
							name="recuperacionCiaFiltro.medioRecuperacion"
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130"/>
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.compania.listar.oficina" />
				<s:select 	list="lstOficinas"
							id="lstOficinas"
							name="recuperacionCiaFiltro.oficinaId"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.compania.listar.tiposervicio" />:
				<s:select 	list="lstTipoServicio"
							id="lstTipoServicio"
							name="recuperacionCiaFiltro.tipoServicioPoliza"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.numcompania" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict jQnumeric"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="numeroCia" 
								name="recuperacionCiaFiltro.numeroCia"
								theme="simple" />
			</td>
			<td colspan="2" align="right" style="padding-right: 80px"><s:text name="midas.siniestros.recuperacion.compania.listar.compania" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 529px;" 
							 	labelposition="left" size="10"
							 	maxlength="100" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="compania" 
								name="recuperacionCiaFiltro.compania"
								theme="simple" />
			</td>		
		</tr>
		
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.numsiniestro" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="15" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="numeroSiniestro" 
								name="recuperacionCiaFiltro.numeroSiniestro"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text name="midas.siniestros.recuperacion.compania.listar.usuario" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="usuario" 
								name="recuperacionCiaFiltro.usuario"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px" ><s:text	name="midas.siniestros.recuperacion.compania.listar.estatuscarta" />:
				<s:select 	list="lstEstatusCarta"
							id="lstEstatusCarta"
							name="recuperacionCiaFiltro.estatusCarta"
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		<s:elseif test="tipoModulo == 3">
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.oficinaReasignacion" />
				<s:select 	list="lstOficinas"
							id="lstOficinasReasignacion"
							name="recuperacionCiaFiltro.oficinaReasignacionId"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130" />
			</td>
		</tr>		
		</s:elseif>
		<s:elseif test="tipoModulo == 4">
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.oficinadestino" />
				<s:select 	list="lstOficinas"
							id="lstOficinasRecepcion"
							name="recuperacionCiaFiltro.oficinaRecepcionId"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130" />
			</td>
		</tr>		
		</s:elseif>
		<tr>
			<td align="right"><s:text	name="midas.siniestros.recuperacion.compania.listar.monto" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.compania.listar.de" />
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict jQ2float"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="montoInicial" 
								name="recuperacionCiaFiltro.montoInicial"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 190px"><s:text	name="midas.siniestros.recuperacion.compania.listar.hasta" />
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict jQ2float"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="montoFinal" 
								name="recuperacionCiaFiltro.montoFinal"
								theme="simple" />
			</td>
		</tr>

		<tr>
			<td align="right"><s:text	name="midas.siniestros.recuperacion.compania.listar.fecharegistro" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.compania.listar.de" />
				<sj:datepicker  name="recuperacionCiaFiltro.fechaIniRegistro"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaIniRegistro"
						  		maxlength="10"
							   	size="18"
							   	readonly="true"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>	
			</td>
			<td align="right" style="padding-right: 190px"><s:text	name="midas.siniestros.recuperacion.compania.listar.hasta" />
				<sj:datepicker  name="recuperacionCiaFiltro.fechaFinRegistro"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaFinRegistro"
						  		maxlength="10"
							   	size="18"
							   	readonly="true"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>	
			</td>
			<td align="right" >
				<div id="divLimpiarBtn" class="w150" style="float: right;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="reset();limpiarReasignacion();"> <s:text name="midas.boton.limpiar" />
						</a>
					</div>
				</div>
				<div id="divBuscarBtn" class="w150" style="float: right;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="buscar();"> <s:text name="midas.boton.buscar" /> </a>
					</div>
				</div>
			</td>
		</tr>
		
		<tr>

		</tr>
	</table>

</s:form>