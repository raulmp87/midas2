<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<div id="contenido_CancelarCIA" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
			<div id="divGenerales" style="width: 100%" class="floatLeft">		
					<div id="contenedorFiltrosSInietro" class="" style="width: 100%; height: 99%;">
						<div class="subtituloLeft" align="left" >
						<s:text name="midas.siniestros.recuperacion.recuperacionCIA.datoscancelacion" />
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 100%;" >
								<s:select id="motivosCancLis" 
									labelposition="left" 
									label="%{getText('midas.siniestros.recuperacion.deducible.motivoCancelacion')}"
									name="recuperacion.motivoCancelacion"
									onchange="changeMotivoCancelacion(this);" 			
									  list="lstMotivosCanc" listKey="key" listValue="value"  
									  cssClass="txtfield cajaTextoM2 w370"   
								/>
							</div>
						</div>
				</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
	});
	</script>
	
</div>



