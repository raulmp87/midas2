<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="refBancariaForm" id="refBancariaForm" method="post">

	<table align="center">
		<tr>
			<td align="right" >
				<div id="divAsignarReferenciaBtn" class="w250" style="float: right;">
					<div class="btn_back w240" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="eliminarEnvio();"><s:text name="midas.siniestros.recuperacion.compania.listar.botoneliminarenvio" /></a>
					</div>
				</div>
			</td>
			<td align="right" >
				<div id="divAsignarReferenciaBtn" class="w250" style="float: right;">
					<div class="btn_back w240" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="eliminarRecepcion();"><s:text name="midas.siniestros.recuperacion.compania.listar.botoneliminarrecepcion" /></a>
					</div>
				</div>
			</td>			
		</tr>
	</table>

	<table width="99%" id="agregar">
		<tr>
			<td class="titulo" colspan="3">
				<s:text name="midas.siniestros.recuperacion.compania.listar.titulodatosenvio"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:radio name="condicion.nivelAplicacion"
						list="#{'E':' Env\u00EDo de Cartas ','R':' Recepci\u00F3n de Cartas'}"
						id="radioEnvioRecepcion"  
						labelposition="left"
						onclick="onChangeEnvioRecepcion(this.value)"/>	
			</td>
			<td id="datosEnvio" style="display:none;">

				<s:text name="midas.siniestros.recuperacion.compania.listar.oficinadestino" />:
				<s:select 	list="lstOficinas"
							id="listOficinasRecepcion"
							name="oficinaRecepcion"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w130" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<s:text name="midas.siniestros.recuperacion.compania.listar.fechaenvio" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 80px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
								id="fechaEnvioRecepcion" 
								name="fechaEnvioRecepcion"
								theme="simple"
								disabled="true"
								readonly="true"/>
			</td>
			<td id="datosRecepcion" style="display:none;">

				<s:text name="midas.siniestros.recuperacion.compania.listar.personarecibe" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 230px;" 
							 	labelposition="left" size="82"
							 	maxlength="82" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="personaRecepcion" 
								name="personaRecepcion"
								theme="simple" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<s:text name="midas.siniestros.recuperacion.compania.listar.fecharecepcion" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 80px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
								id="fechaEnvioRecepcion" 
								name="fechaEnvioRecepcion"
								theme="simple"
								disabled="true"
								readonly="true"/>
			</td>			
		</tr>
		<tr>			
			<td align="right" colspan="3">
				<div id="divAsignarReferenciaBtn" class="w250" style="float: right;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="enviarRecibirCartas();"><s:text name="midas.boton.guardar" /></a>
					</div>
				</div>
			</td>
		</tr>

	</table>
	
</s:form>