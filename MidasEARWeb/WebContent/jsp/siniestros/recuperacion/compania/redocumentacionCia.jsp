<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<s:hidden id="cartaRedocumentacionID" name="cartaRedocumentacion.id" />
<s:hidden id="conceptoCartaRedocumentacionID" name="cartaRedocumentacion.concepto" />
<s:hidden id="estatusCartaRedocumentacionID" name="cartaRedocumentacion.estatus" />
<s:hidden id="folio" name="cartaRedocumentacion.folio" />
<s:hidden id="codigoUusarioPCartasMigradas" name="recuperacion.codigoUsuarioCreacion" />

<div id="contenido_RedocumentacionCIA" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
			<div id="divGenerales" style="width: 100%" class="floatLeft">		
					<div id="contenedorFiltrosSInietro" class="" style="width: 100%; height: 99%;">
						<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 60%;" >
											<s:select id="lstTipoRedocumentacion" 
										labelposition="left" 
										label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.tipo')}"
										name="cartaRedocumentacion.tipoRedocumentacion"	
								  		list="lstTipoRedocumentacion" listKey="key" listValue="value"  
								  		onchange="changeTipoRedocumentacion();"
								  		cssClass="txtfield cajaTextoM2 w160 setConsulta"   
								  		/>			
								</div>
						</div>
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.datosRechazo')}" />
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 51%;" >
										<s:select id="lstCausaRechazo" 
										labelposition="left" 
										label="Causa de Rechazo"
										name="cartaRedocumentacion.causaRechazo"	
								  		list="lstCausaRechazo" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 w470 setConsulta"   
								  		/>				
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 28%;" >
										<s:select id="lstTipoRechazoRedocumentacion" 
										labelposition="left" 
										label="Tipo Rechazo"
										name="cartaRedocumentacion.tipoRechazo"	
								  		list="lstTipoRechazoRedocumentacion" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 w230 setConsulta"   
								  		/>				  
							</div>
							
							<div id="rechazoConsulta" class="floatLeft divInfDivInterno" style="width: 21%;" >
											 <s:textfield id="fechaRechazoConsulta" name="cartaRedocumentacion.fechaRechazo"   
											cssClass="txtfield setConsulta" 	
											style="width:40%;"										
											label="Fecha Rechazo"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:63%;" ></s:textfield>
							</div>
							<div id="rechazoCaptura" class="floatLeft divInfDivInterno" style="width: 21%;" >
									<sj:datepicker name="cartaRedocumentacion.fechaRechazo" id="datepickerFechaRechazo"
									label="Fecha Rechazo" labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaRechazoCaptura"
									value="%{cartaRedocumentacion.fechaRechazo}" maxlength="10"
									cssClass="txtfield setConsulta" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this);">
									</sj:datepicker> 
							</div>
							
							
							
							
						</div>
						<div class="subtituloLeft" align="left" >
							<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.datosExclusion')}" />
							
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 45%;" >
										<s:select id="lstMotivoExclusion" 										
										label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.motivoEx')}"
										labelposition="left" 
										name="cartaRedocumentacion.motivoExclusion"	
								  		list="lstMotivoExclusion" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 w400 setConsulta"   
								  		/>				
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 17%;" >
								<s:textfield id="montoExclusion"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.montoEx')}"  
								name="cartaRedocumentacion.montoExclusion"  
								onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#montoExclusion',this.value); calcularMontoRecuperar()"  
								onblur="mascaraDecimales('#montoExclusion',this.value);  calcularMontoRecuperar()"   
								cssClass="txtfield  setNew formatCurrency setConsulta"  readonly="true" labelposition="left" 
								cssStyle="width:43%;" readonly="true"></s:textfield>
								
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 17%;" >
								<s:textfield id="montoRegistado" label="Monto Registrado" name="cartaCiaActiva.montoARecuperar"  
								 cssClass="txtfield  setNew formatCurrency setConsulta"  readonly="true" labelposition="left" 
								 cssStyle="width:49%;" readonly="true"></s:textfield>
								
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 21%;" >
								<s:textfield id="nuevoMontoRegistrar" label="Nuevo Monto a Recuperar" 
								 name="cartaRedocumentacion.montoARecuperar"   cssClass="txtfield  setNew formatCurrency setConsulta"  
								 readonly="true" labelposition="left" cssStyle="width:40%;" readonly="true"></s:textfield>
							</div>
						</div>
						<div class="subtituloLeft" align="left" >
							<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.datosRed')}" />						
						</div>
						<div class="divFormulario">		
						
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
									<s:select list="lstEstatusCartaRedoc" id="lstEstatusCartaRedoc"
									name="cartaCiaActiva.estatus" label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.estatusCarta')}"
									labelposition="left"
									cssClass="txtfield setConsulta" 
									cssStyle="width:68%;"
									>
									</s:select>							
							</div>	
								<div class="floatLeft divInfDivInterno" style="width: 25%;" >
											 <s:textfield id="fechaRedoc" name="cartaRedocumentacion.fechaCreacion"   
											label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.fechaRed')}" 
											cssClass="txtfield setConsulta" 	
											style="width:40%;"	
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
								</div>
							
								<div class="floatLeft divInfDivInterno" style="width: 25%;" >
											 <s:textfield id="fechaAcuseRedoc" name="cartaCiaActiva.fechaAcuse"   
											cssClass="txtfield setConsulta" 
											style="width:40%;" 
											label="Ultima Fecha de Acuse"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 25%;" >
									<s:textfield id="usuarioRedoc" name="cartaRedocumentacion.codigoUsuarioCreacion"   
									cssClass="txtfield setConsulta"  label="Usuario que Redocumenta" labelposition="left" cssStyle="width:48%;"    readonly="true" ></s:textfield>
								</div>
							
								

					   </div>
						
						<div class="divFormulario">		
								
								<div class="floatLeft divInfDivInterno" style="width: 25%;" >
											 <s:textfield id="primerFechaAcuse" name="primeraCarta.fechaAcuse"   
											cssClass="txtfield setConsulta" 	
											style="width:40%;"										
											label="Primer Fecha Acuse"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 75%;" >
									<s:textfield id="comentariosRedoc" name="cartaRedocumentacion.comentarios"   
									cssClass="txtfield setConsulta"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.comentarios')}" labelposition="left" cssStyle="width:91%;" ></s:textfield>
								</div>
						</div>
						
					 	
					 
					 <div class="divFormulario" style=" height: 200px;" >	
						<div id="divInferior" style="width: 1050px !important; padding-left: 15px;" class="floatLeft">							
							<div id="redocumentaGrid" style="width:1050px; height: 190px;"></div>
							<div id="pagingAreaR" style="padding-top: 8px"></div>
							<div id="infoAreaR"></div>
						</div>
					</div>
					<s:if test='%{  soloConsulta != true }'>
						<div class="divFormulario">
							 	<s:if test='%{  cartaCiaActiva.estatus == "REGISTRADO"  || cartaCiaActiva.estatus == "POR_REDOC"  }'>
									<div id="divElabora" class="floatLeft divInfDivInterno"  style="width: 15%; padding-left: 15px;" >
											<div id="guardarElabora" class="btn_back w150"
												style="display: inline; float: left;margin-left: 1%;">
												<a href="javascript: void(0);" onclick="elaborarCartaRedoc();"> <s:text
													name="Elaborar Carta" /> </a>
											</div>					
									</div>
								</s:if>
								<s:if test='%{  cartaCiaActiva.estatus == "ELABORADO" || cartaCiaActiva.estatus == "IMPRESO" }'>
									<div id="divImprime" class="floatLeft divInfDivInterno"  style="width: 15%; padding-left: 15px;" >
											<div id="guardarImprime" class="btn_back w150"
												style="display: inline; float: left;margin-left: 1%;">
												<a href="javascript: void(0);" onclick="imprimirCartaRedoc();"> <s:text
													name="Imprimir Carta" /> </a>
											</div>					
									</div>
								</s:if>
								
								<s:if test='%{  cartaCiaActiva.estatus == "IMPRESO" }'>
									<div id="divEntrega" class="floatLeft divInfDivInterno"  style="width: 15%;  padding-left: 15px;" >
											<div id="guardarEntrega" class="btn_back w200"
												style="display: inline; float: left;margin-left: 1%;">
												<a href="javascript: void(0);" onclick="registarEntregaCarta();"> <s:text
													name="Registrar Entrega de Carta" /> </a>
											</div>					
									</div>
								</s:if>
								
								<s:if test='%{  cartaCiaActiva.estatus == "REGISTRADO"  ||  cartaCiaActiva.estatus == "ENTREGADO"   ||  cartaCiaActiva.estatus ==   "POR_REDOC"}'>
									<div id="divGuardaRedoc" class="floatLeft divInfDivInterno"  style="width: 15%;  padding-left: 15px;" >
											<div id="guardarRedocumentacion" class="btn_back w110"
												style="display: inline; float: left;margin-left: 1%;">
												<a href="javascript: void(0);" onclick="guardarRedocumentacion();"> <s:text
													name="Guardar" /> </a>
											</div>					
									</div>
								</s:if>
								
						 </div>
					
					</s:if>
					 
					 
				</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
	iniRedocumentacion();
	});
	</script>
	
</div>



