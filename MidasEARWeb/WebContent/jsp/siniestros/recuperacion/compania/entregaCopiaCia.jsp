<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>


<sj:head/>


<style type="text/css">
	#superior{
		height: 90px;
		width: 99%;
	}

	#superiorI{
		height: 60px;
		width: 250px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		width: 210px;
		float: left;
		margin-left: 70%;
	}
	
	
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	label{
		font-family: Verdana,Arial,Helvetica,sans-serif; font-size:   9.33333px; text-align:  left
	}

</style>
<div id="contenido_entrega" style="margin-left: 2% !important;height: 90px;">

	<div id="superior" class="divContenedorO">
	<s:hidden id="recuperacionId" name="recuperacionId" />
		<form id="entregaForm">
			<div id="superiorI">
				<div id="SIS">

					<div class="floatLeft" style=" margin-top:2%; ">
						<div >
							<label>Fecha de Acuse:</label><br/>
							<sj:datepicker id="datepickerFechaAcuse" 
									changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif" id="fechaAcuseCia"
									buttonImageOnly="true"  name="fechaAcuse"
									value="%{fechaAcuseCia}" 
									maxlength="10" cssClass="txtfield cleanable"
										   size="18"
									 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield">
							</sj:datepicker>
							
							
							<br/>
							
						</div>
					</div>
					<br/>
				</div>

			</div>
		
		
		
		
		
		
		
		
		
		
		
			<div id="superiorI" >
				<div>
					<div class="btn_back w100" style="display: inline; float: left;">
						<s:if test='%{ codeUserCartasMigradas == "MIGSINIE" }'>
							<a href="javascript: void(0);" onclick="registrarNuevaFechaAcuseCartMig();"> 
								<s:text	name="Continuar"/> 
							</a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" onclick="reenviarEntregaCia();"> 
								<s:text	name="Continuar"/> 
							</a>
						</s:else>
					</div> 
					<div class="btn_back w100" style="display: inline; float: left;">
						<s:if test='%{ codeUserCartasMigradas == "MIGSINIE" }'>
							<a href="javascript: void(0);" onclick="cerrarVentanaFechaAcuseCartMig();"> 
							<s:property value="#codeUserCartasMigradas" />
								<s:text	name="Cerrar"/> 
							</a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" onclick="cerrarEntregaCIA();"> 
								<s:text	name="Cerrar"/> 
							</a>
						</s:else>
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
</div>































