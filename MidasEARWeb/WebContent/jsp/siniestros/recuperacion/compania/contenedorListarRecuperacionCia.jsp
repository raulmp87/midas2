<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="listarRecuperacionCiaHeader.jsp"></s:include>
<s:hidden name="tipoModulo" id="tipoModulo"></s:hidden>
<s:div name="divBusqFiltro">
	<s:include value="listarRecuperacionCiaFiltro.jsp" />
</s:div>

<div class="titulo" style="display: block;">
	<s:text name="midas.siniestros.recuperacion.compania.listar.titulolistado"/>
</div>

<span><a href="javascript: void(0);" onclick="maxminGrid('listarRecuperacionCiaGrid', 350);">Maximizar/minimizar</a></span>
<s:div name="listarRecuperacionCiaGrid" class="listarRecuperacionCiaGrid"	id="listarRecuperacionCiaGrid" style="width: 95%; height: 200px; display: block;"></s:div>




<div id="pagingArea"></div>
<div id="infoArea" style="display: block;"></div>
<div style="display: inline; font-weight: bold; color: #00AA4E; font-size: 10pt;">
	Monto total de&nbsp;<div id="cantidadRegistros" style="display: inline;"></div>&nbsp;registros seleccionados
</div>
<div style="display: inline; font-weight: bold; color: black; font-size: 10pt;">
	<div id="sumaRegistros" style="display: inline;">
</div>
</div>

<div class="btn_back w120" style="display: block; float: right; margin: 10px 20px 0px 0px;" id="btn_exportar">
	<a href="javascript: void(0);" onclick="javascript:exportar();">
		<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img
		align="middle" border='0px' alt='Exportar a Excel'
		title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif'
		style="vertical-align: middle;" /> </a>
</div>

<s:div name="divAcciones"  id="divAcciones" style="display: block; margin: 20px 0px 0px 0px;">
	<s:if test="tipoModulo==1">
		<s:include value="asignarRefBancaria.jsp" />
	</s:if>
	<s:elseif test="tipoModulo==2">
		<s:include value="recepcionCarta.jsp" />
	</s:elseif>
	<s:elseif test="tipoModulo==3">
		<s:include value="reasignarOficina.jsp" />
	</s:elseif>
	<s:elseif test="tipoModulo==4">
		<s:include value="enviarRecibirCartas.jsp" />
	</s:elseif>	
</s:div>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
		initCurrencyFormatOnTxtInput();
		initGridListarRecuperacionCia();
		init();		
</script>