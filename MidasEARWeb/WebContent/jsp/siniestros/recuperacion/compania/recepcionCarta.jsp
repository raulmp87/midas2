<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="refBancariaForm" id="refBancariaForm" method="post">

	<table width="99%" id="agregar">
		<tr>
			<td class="titulo" colspan="3">
				<s:text name="midas.siniestros.recuperacion.compania.listar.tituloDatosRecepcionCartas"/>
			</td>
		</tr>
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.fechaacuse" />:
				<sj:datepicker  name="fechaAcuse"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaAcuse"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>					
			</td>
			<td align="right" >
				<div id="divAsignarReferenciaBtn" class="w250" style="float: right;">
					<div class="btn_back w240" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="entregarCartas();">Asignar Fecha de Acuse</a>
					</div>
				</div>
			</td>
		</tr>

	</table>
</s:form>