<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<s:hidden id="userID" name="currentUser" />

<div id="contenido_DefinirCIA" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
			<div id="divGenerales" style="width: 100%" class="floatLeft">		
					<div id="contenedorFiltrosSInietro" class="" style="width: 100%; height: 99%;">
					
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.tituloTab2')}" />
						</div>
						 <div class="divFormulario" id="fechaUsuarioDiv">
						 	<div class="floatLeft divInfDivInterno" style="width: 30%;" >
											 <s:textfield id="fechaSeguimiento" name="dto.fechaSeguimientoStr"   
											cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.fechaSeguimiento')}"											
											 labelposition="left" cssStyle="width:49%;"  readonly="true" ></s:textfield>
							</div>
							<div class="floatRight divInfDivInterno" style="width: 60%;" >
								<s:textfield id="usuario" name="userName"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.usuario')}" labelposition="left" cssStyle="width:66%;" readonly="true" ></s:textfield>
							</div>							
						</div>
					   
					   <div class="divFormulario" id="comentarioDiv">							
							<div class="floatLeft divInfDivInterno" style="width: 90%;" >
								<s:textfield id="comentario" name="dto.comentarios"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.comentario')}" labelposition="left" cssStyle="width:87%;" ></s:textfield>
							</div>
					   </div>
					   
				   
				   	<div class="divFormulario">
						<div id="divElabora" class="floatRight divInfDivInterno" style="width: 30%;" >
							<div id="guardarSeguimiento" class="btn_back w150">
								<a href="javascript: void(0);" onclick="guardarSeguimiento();"> 
									<s:text name="Agregar" /> 
								</a>
							</div>					
						</div>						
					</div>		
					<div class="divFormulario" style=" height: 200px;" >	
						<div id="divInferior" style="width: 1050px !important; padding-left: 15px;" class="floatLeft">							
							<div id="seguimientoCiaGrid" style="width:1050px; height: 190px;"></div>
							<div id="pagingAreaS" style="padding-top: 8px"></div>
							<div id="infoAreaS"></div>
						</div>
					</div>				
				</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		initSeguimientoTab();
		buscarSeguimientoCia();
	});
	</script>
	
</div>



