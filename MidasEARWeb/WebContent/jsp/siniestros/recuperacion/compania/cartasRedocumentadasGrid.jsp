<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaR</param>
				<param>true</param>
				<param>infoAreaR</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="id" type="ro" width="80" sort="int" ><s:text name="midas.siniestros.recuperacion.recuperacionGeneral.noRecuperacion" /> </column>
       <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.folCarta" />    </column> 
       <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.fechaCarta" />   </column>
       <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="midas.siniestros.recuperacion.recuperacionCIA.fechaImp" />   </column>
       <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.fechaAcuse" />   </column> 
       <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="Fecha Rechazo" />   </column> 
        <column id="tipoRechazo" 	type="ro" width="80" sort="int" > <s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.tipoRechazo" /> </column>   
        <column id="motivoExclusion" 	type="ro" width="250" sort="int" ><s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.motivoRechazoExclu" /> </column>
        <column id="montoExclusion"   type="ron" width="80" format="$0,000.00" sort="int"><s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.montoExclusion" /> </column>
		<column id="montoARecuperar"   type="ron" width="80" format="$0,000.00" sort="int"><s:text name="midas.siniestros.recuperacion.recuperacionCIA.redocumentacion.montoRecupera" /> </column>
		<column  type="img"   width="80" sort="na" align="center" >Imprimir</column>
	  	</head>
		<s:iterator value="lstCartaCompania">
			<row id="<s:property value="id"/>">
				<cell><s:property value="recuperacion.numero" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="folio!=0">
			    	<cell><s:property value="recuperacion.numero" escapeHtml="false" escapeXml="true" />-<s:property value="folio" escapeHtml="false" escapeXml="true" /></cell>
				</s:if>
				<s:else>
			    	<cell></cell>
				</s:else>
				<cell><s:property value="fechaElaboracion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaImpresion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaAcuse" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaRechazo" escapeHtml="false" escapeXml="true" /></cell>				
				<cell><s:property value="tipoRechazo" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="motivoExclusion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="montoExclusion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="montoARecuperar" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="soloConsulta">
					<cell>../img/pixel.gif^<s:text name="Desactivado"/>^^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/menu_icons/print.gif^<s:text name="Imprimir"/>^javascript:reimprimirCarta(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			    </s:else>
			</row>
		</s:iterator>
</rows>