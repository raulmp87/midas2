<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="refBancariaForm" id="refBancariaForm" method="post">

	<table width="99%" id="agregar">
		<tr>
			<td class="titulo" colspan="3">
				<s:text name="midas.siniestros.recuperacion.compania.listar.tituloRefBancaria"/>
			</td>
		</tr>
		<tr>
			<td align="right"><s:text name="midas.siniestros.recuperacion.compania.listar.referenciabancaria" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 180px;" 
							 	labelposition="left" size="100"
							 	maxlength="100" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="referenciaBancaria" 
								name="referenciaBancaria"
								theme="simple" />
			</td>
			<td align="right" >
				<div id="divAsignarReferenciaBtn" class="w250" style="float: right;">
					<div class="btn_back w240" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="asignarReferencia();">Asignar Referencia Bancaria</a>
					</div>
				</div>
			</td>
		</tr>

	</table>
</s:form>