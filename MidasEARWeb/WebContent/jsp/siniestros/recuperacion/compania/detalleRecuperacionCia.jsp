<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}

.disabledgray{
	background-color: #fafaf0;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<s:hidden id="cartaCiaId" name="cartaCia.id" />
<s:hidden id="cartaCiaFolio" name="cartaCia.folio" />
<s:hidden id="cartaCiaMontoARecuperar" name="cartaCia.montoARecuperar" />
<s:hidden id="tieneComplemento" name="tieneComplemento" />
<s:hidden id="urlImprimir" name="urlImprimir"/>
<s:hidden id="prestadorServicioId" name="prestadorServicioId"/>
<div id="contenido_DefinirCIA" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 100% !important;" class="floatLeft">
			<div id="divGenerales" style="width: 100%" class="floatLeft">		
					<div id="contenedorFiltrosSInietro" class="" style="width: 100%; height: 99%;">
					
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.tituloSin')}" />
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="poliza" name="datosEstimacion.numeroPoliza" cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.numPoliza')}" labelposition="left" cssStyle="width:62%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="estadoUbica" name="datosEstimacion.estadoUbicacion" cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.edoUbica')}" labelposition="left" cssStyle="width:45%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="ciudadUbica" name="datosEstimacion.ciudadUbicacion" cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.ciudadUbica')}" labelposition="left" cssStyle="width:68%;" readonly="true" ></s:textfield>
							</div>	
						</div>
						 <div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="lugarUbica" name="datosEstimacion.lugarSiniestro" cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.lugarSin')}" labelposition="left" cssStyle="width:65%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
											 <s:textfield id="fechaSin" name="datosEstimacion.fechaSiniestroStr"   
											cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.fecSiniestro')}"											
											 labelposition="left" cssStyle="width:49%;"  readonly="true" ></s:textfield>
							</div>							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="tipoPerdida" name="datosEstimacion.tipoPerdida"   cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.tipoPerdida')}" labelposition="left" cssStyle="width:72%;" readonly="true" ></s:textfield>
							</div>	
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 60%;" >
								<s:textfield id="vehiculoAsegurado" name="datosEstimacion.vehiculoAsegurado"   cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.vehiculoAsegurado')}" labelposition="left" cssStyle="width:71%;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="personaLesioada" name="datosEstimacion.personaLesionada"   cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.peronaLesionada')}" labelposition="left" cssStyle="width:66%;" readonly="true" ></s:textfield>
							</div>
								
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 60%;" >
								<s:textfield id="vehiculoTercero" name="datosEstimacion.vehiculoTercero"   cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.verhiculoTercero')}" labelposition="left" cssStyle="width:74%;" readonly="true" ></s:textfield>
							</div>	
						</div> 
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.tituloCompania')}" />
						</div>
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="nombreCia" name="recuperacion.compania.personaMidas.nombre"  
								disabled="true" cssClass="txtfield"  labelposition="left" cssStyle="width:73%;" 
								label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.compania')}" ></s:textfield>			
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="polizaCia" name="recuperacion.polizaCia"  cssClass="txtfield disabledgray " cssStyle="width:42%;" 
								label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.numPolizaCia')}" labelposition="left"></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numSiniestroCia" name="recuperacion.siniestroCia"  cssClass="txtfield disabledgray"  cssStyle="width:60%;"  
								label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.numSinCia')}" labelposition="left"></s:textfield>
							</div>
						</div>
					
						<div class="divFormulario">							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="paseCia" name="recuperacion.paseAtencionCia"  cssClass="txtfield disabledgray" cssStyle="width:45%;"
								label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.paseCia')}" labelposition="left" readonly="true"  ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="porcParticipa" name="recuperacion.porcentajeParticipacion" readonly="true" 
								cssClass=" txtfield disabledgray"   labelposition="left" cssStyle="width:44%;" 
								 label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.porParticipa')}" ></s:textfield>
							</div>	
						
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="recAnterior"  cssClass="txtfield disabledgray"  labelposition="left" cssStyle="width:40%;"  readonly="true"
								name="recuperacion.recuperacionOriginal.numero"
								label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.recuperacionAnterior')}" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:checkbox id="ch_esCarta"  name="recuperacion.esComplemento "  disabled="true"								  
								 cssClass="desabilitable disabledgray" onchange="checkCartaComp();" 
								 label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.cartaComplemento')}" labelposition="left"></s:checkbox>
							</div>
								
					   </div>
					   
						<div class="subtituloLeft" align="left" >
						<s:text name="Datos para Carta Compañia " />
						</div>
						
						<div class="divFormulario">							
						
										<div class="floatLeft divInfDivInterno" style="width: 22%;" >
											 <s:textfield id="fechaCarta" name="cartaCia.fechaElaboracion"   
											cssClass="txtfield disabledgray" 	
											style="width:40%;"										
											label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.fecCarta')}"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
										</div>
							
										<div class="floatLeft divInfDivInterno" style="width: 22%;" >
											 <s:textfield id="fechaLimiteCobro" name="recuperacion.fechaLimiteCobro"   
											cssClass="txtfield disabledgray" 
											style="width:40%;" 
											label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.fecLimite')}"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
										</div>
							
										<div class="floatLeft divInfDivInterno" style="width: 22%;" >
											 <s:textfield id="fechaImpresion" name="cartaCia.fechaImpresion"   
											cssClass="txtfield disabledgray"  
											style="width:40%;"
											label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.fechaImp')}"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
										</div>
							
										<div class="floatLeft divInfDivInterno" style="width: 24%;" >
											 <s:textfield id="fechaAcuse" name="cartaCia.fechaAcuse"   
											cssClass="txtfield disabledgray"  
											style="width:40%;"
											label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.primerFechaAcuse')}"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;"  readonly="true"  ></s:textfield>
										</div>

					   </div>
					   <div class="divFormulario">							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="lstEstatusCarta" id="estatusCartaLis"
									name="cartaCia.estatus" label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.estatusCarta')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:68%;"
									>
									</s:select>							
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 60%;" >
								<s:textfield id="conceptoCarta" name="cartaCia.concepto"  cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.conceptoRecupera')}" labelposition="left" cssStyle="width:77%;" ></s:textfield>
							</div>									
							
					   </div>
					   <div class="divFormulario">							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="usuarioElaboracion" name="cartaCia.usuarioElaboracion"   cssClass="txtfield disabledgray"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.usuarioElabora')}" labelposition="left" cssStyle="width:40%;"    readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:if test="!soloConsulta">
									<div class="btn_back w200" id="btn_BusquedaAnexo"
										style="display: inline; float: left; vertical-align: top; position: relative; margin-top: 0%;">
										<a href="javascript: void(0);" onclick="anexarArchivo();">
											<s:text name="midas.siniestros.recuperacion.recuperacionCIA.anexar" /> </a>
									</div>
								</s:if>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 40%;" >
								<s:select list="lstCausasNoElaboracion" id="causasNoElaboracion"
									name="cartaCia.causasNoElaboracion" label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.causasNoElaboraCarta')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:62%;"
									>
								</s:select>								
							
							</div>
					   </div>
					   
					   <div class="divFormulario">							
							<div class="floatLeft divInfDivInterno" style="width: 90%;" >
								<s:textfield id="comentarios" name="cartaCia.comentarios"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.recuperacionCIA.comentarios')}" labelposition="left" cssStyle="width:90%;" ></s:textfield>
							</div>
					   </div>
					   
				   <div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.ordenesCompraAsociadas')}" />
					</div>
					<div class="divFormulario" style=" height: 250px;" >	
						<div id="divInferior" style="width: 1050px !important; height: 250px;  padding-left: 15px;" class="floatLeft">							
							<div id="ordenesCiaGrid" style="width:1050px; height: 190px;"></div>
							<div id="pagingAreaOC" style="padding-top: 8px"></div>
							<div id="infoAreaOC"></div>
						</div>
					</div>
					<div class="divFormulario">
							<div id="divElabora" class="floatLeft divInfDivInterno" style="width: 15%;" >
									<div id="guardarElabora" class="btn_back w150"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="elaborarCarta();"> <s:text
											name="Elaborar Carta" /> </a>
									</div>					
							</div>
							<div id="divImprime" class="floatLeft divInfDivInterno" style="width: 15%;" >
									<div id="guardarImprime" class="btn_back w150"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="imprimirCarta();"> <s:text
											name="Imprimir Carta" /> </a>
									</div>					
							</div>
							<div id="divEntrega" class="floatLeft divInfDivInterno" style="width: 15%;" >
									<div id="guardarEntrega" class="btn_back w200"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="registarEntregaCarta();"> <s:text
											name="Registrar Entrega de Carta" /> </a>
									</div>					
							</div>
					
					
							<div class="floatRight divInfDivInterno" style="width: 15%; padding-right: 35px" >
								<s:textfield id="montoIndemnizacion" name="recuperacion.montoIndemnizacion"  
								cssClass="txtfield disabledgray setNew formatCurrency" cssStyle="width:65%;" readonly="true" ></s:textfield>
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 15%;  padding-top: 8px;" >
								<label for="montoIndemnizacion" class="label" > <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.indemPagos')}" /></label>
							</div>
								
					 </div>
					 
					 <div class="divFormulario">
					 		<div class="floatRight divInfDivInterno" style="width: 15%;padding-right: 35px" >
								<s:textfield id="montoSalvamento" name="recuperacion.montoSalvamento"    
								cssClass="txtfield disabledgray setNew formatCurrency" cssStyle="width:65%;" readonly="true" ></s:textfield>
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 15%;  padding-top: 8px; " >
								<label for="montoSalvamento" class="label" > <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.salvamento')}" /></label>
							</div>
								
					 </div>
					 <div class="divFormulario">
					 		<div class="floatRight divInfDivInterno" style="width: 15%;padding-right: 35px" >
								<s:textfield id="montoGAGrua" name="recuperacion.montoGAGrua"  
								cssClass="txtfield disabledgray setNew formatCurrency" cssStyle="width:65%; background-color: #fafaf0;" readonly="true"  ></s:textfield>								
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 15%;  padding-top: 8px; " >
								<label for="montoGAGrua" class="label" > <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.gastoAjuste')}" /></label>
							</div>
								
					 </div>
					 
					 <div class="divFormulario">
					 		<div class="floatRight divInfDivInterno" style="width: 15%;padding-right: 35px" >
								<s:textfield id="montoPiezas" name="recuperacion.montoPiezas"  
								cssClass="txtfield disabledgray setNew formatCurrency" cssStyle="width:65%; background-color: #fafaf0;" readonly="true"  ></s:textfield>								
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 15%;  padding-top: 8px; " >
								<label for="montoPiezas" class="label" > <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.gastoPiezas')}" /></label>
							</div>
								
					 </div>
					 					 
					 
					  <div class="divFormulario">
					 		<div class="floatRight divInfDivInterno" style="width: 15%;padding-right: 35px" >
								<s:textfield id="reservaDisponible"   name="reservaDisponible" cssClass="txtfield disabledgray setNew formatCurrency" 
								 labelposition="left" cssStyle="width:65%;" readonly="true"></s:textfield>
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 15%;  padding-top: 8px; " >
								<label for="reservaDisponible" class="label" >    <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.reservaDisponible')}" /></label>
							</div>
								
					 </div>
					 <div class="divFormulario">
					 		<div class="floatRight divInfDivInterno" style="width: 15%;padding-right: 35px" >
								<s:textfield id="importeCobrar"   name="recuperacion.importe"   cssClass="txtfield disabledgray setNew formatCurrency"  labelposition="left" cssStyle="width:65%;" readonly="true"></s:textfield>
							</div>							
							<div class="floatRight divInfDivInterno" style="width: 19%;  padding-top: 8px; " >
								<label for="importeCobrar" class="label" >    <s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.importeCobrar')}" /></label>
							</div>
								
					 </div>
					 
					
					
						
				</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		initCurrencyFormatOnTxtInput();
		iniContenedorRecuperacionCIA();
		buscarOrdenCompraCia();
	});
	</script>
	
</div>



