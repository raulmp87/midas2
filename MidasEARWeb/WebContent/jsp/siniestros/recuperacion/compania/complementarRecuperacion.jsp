<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>

<div class="titulo" style="margin-left: 2%"><s:text name="midas.siniestros.recuperacion.compania.registro.complemento"/></div>
	<br/>
<table id="agregar" border="0">
<tr>
	<td>
		<s:text name="midas.siniestros.recuperacion.compania.registro.complemento.leyenda"></s:text>
	</td>
</tr>
<tr>	
	<td>
		<s:radio id="respuestas"
				 name="opcion" 
				 value="1" 
				 list="#{'1':' Se recupera Carta.<br/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
					En caso de elegir esta opci\u00f3n se cancelar\u00E1 la Recuperaci\u00f3n
					actual y se generar\u00E1 autom\u00E1ticamente una nueva <br/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
					Recuperaci\u00f3n con todas las Ordenes de Compra relacionadas
					incluyendo el ajuste de m\u00E1s.<br/>',
					'2':' No se recupera Carta.<br/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
					\u00BF Desea generar una recuperaci\u00f3n por complemento con el ajuste de m\u00E1s \u003F'}" 				
				onchange="enableSub();"
				/>
		<div style="display: inline; float: left;padding-left: 25px;">		
			<s:radio id="subRespuestas"
					 value="1"
					 name="subOpcion"
					 list="#{'1':'Si','2':'No'}"></s:radio>	
		</div>				
	</td>	
</tr>
</table>	

<div class="btn_back w140" style="display: inline; float: right;">
				 <a href="javascript: void(0);" onclick="parent.popUpRegistroCompl.close();">
				 <s:text name="midas.boton.salir" /> </a>
			</div>

<div id="botonContinuar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="procesarOpcionSeleccionada();">
		<s:text name="midas.boton.continuar" /> 
	</a>
</div>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript">	
		
		function enableSub()
		{
			var bandera = true;
			if(jQuery('input:radio[name=opcion]:checked').val() == "2")
			{
				bandera = false;
			}
			
			jQuery('input:radio[name=subOpcion]').attr( "disabled", bandera);								
		}
		
		function procesarOpcionSeleccionada()
		{
			var url;
			var refrescarPantalla = false;
		
			if(jQuery('input:radio[name=opcion]:checked').val() == "1")
			{
				url="/MidasWeb/siniestros/recuperacion/recuperacionCia/regeneraRecuperacion.action?recuperacionId="
					+parent.jQuery('#recuperacionId').val();	
					
				refrescarPantalla = true;
								
			}else
			{
				if(jQuery('input:radio[name=subOpcion]:checked').val() == "1")
				{
					url="/MidasWeb/siniestros/recuperacion/recuperacionCia/complementarCarta.action?recuperacionId="
						+parent.jQuery('#recuperacionId').val();					
				}else
				{
					url="/MidasWeb/siniestros/recuperacion/recuperacionCia/cancelarComplemento.action?recuperacionId="
						+parent.jQuery('#recuperacionId').val();				
				}
			}
			
			if(confirm("\u00BFEst\u00E1 seguro de continuar con el proceso \u003F "))
			{	console.log(url);
				/*sendRequestJQ(null,url,targetWorkArea,null);
				parent.popUpRegistroCompl.close();*/
				parent.registrarRecuperacion(url,refrescarPantalla);
			}
		}
		
		jQuery(document).ready(function() {
		
			enableSub();
		});		
						
</script>