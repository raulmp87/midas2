<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaS</param>
				<param>true</param>
				<param>infoAreaS</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="txt_creationDate" type="ro" width="120" sort="int" >Fecha Seguimiento </column>
        <column id="txt_userName" 	type="ro" width="200" sort="str" >Usuario</column>
		<column id="txt_comments"   type="ron" width="*" sort="str">Comentarios</column>
		</head>
		<s:iterator value="lstSeguimientos">
			<row id="<s:property value=""/>">
				<cell><s:date name="fechaCreacion" format="dd/MM/yyyy HH:mm" /></cell>
				<cell><s:property value="usuarioCreacionNombre" escapeHtml="false" escapeXml="true"/></cell>				
				<cell><s:property value="comentario" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
</rows>