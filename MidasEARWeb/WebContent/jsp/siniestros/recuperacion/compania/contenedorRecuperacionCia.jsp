<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}
.divFormulario {
	height: 35px;
	position: relative;
}
#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
</style>
<s:form  id="contenedorRecuperacionCIAForm">
<s:hidden id="soloConsulta" 	name="soloConsulta" />
<s:hidden id="h_soloLectura" 	name="soloConsulta" />
<s:hidden id="esNuevoRegistro" 	name="esNuevoRegistro" />
<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
<s:hidden id="h_idRecuperacion" name="recuperacion.id" />
<s:hidden id="tipoMostrar" name="tipoMostrar" />
<s:hidden id="ordenesCiaConcat" name="ordenesCiaConcat" />
<s:hidden id="termioAjuste" name="termioAjuste" />
<s:hidden id="idCobertura" name="idCobertura" />
<s:hidden id="idPaseAtencion" name="idPaseAtencion" />
<s:hidden id="esCartaComplemento" name="recuperacion.esComplemento" />
<s:hidden id="verRedocumentacion" name="verRedocumentacion" />
<s:hidden id="tieneComplemento" name="tieneComplemento" />


<s:hidden id="urlRedirect" ></s:hidden>
<script type="text/javascript">

	var entregarCartaUrl = '<s:url action="entregaCarta" namespace="/siniestros/recuperacion/recuperacionCia"/>';
	
</script>
	<div id="recuperacionGenericaDiv">
			<s:include value="/jsp/siniestros/recuperacion/recuperacionGenerica.jsp"></s:include>
	</div>	
	<div id="contenido_DefinirTitulo" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.titulo')}"/>
			</div>
		</div>
	</div>	
	
	
	<div id="contenido_Pestanas" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 99%; width: 100%" id="recuperacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div selected="1" id="tabRecuperacionCIA" name="Recuperaci&oacute;n">
						<s:include value="/jsp/siniestros/recuperacion/compania/detalleRecuperacionCia.jsp"></s:include>			
				</div>	

				<div selected="2" id="tabSegRecuperacionCIA" name="Seguimiento">
						<s:include value="/jsp/siniestros/recuperacion/compania/seguimientoRecuperacionCia.jsp"></s:include>			
				</div>
				<s:if test='%{   (  tipoMostrar=="C"  || tipoMostrar=="R"  ) &&   (cartaCia.estatus == "PEND_CANC"  ||  cartaCia.estatus == "CANCELADO"      )  }'>
						<div selected="2" id="tabRecuperacionCancelarCIA" name="Cancelaci&oacute;n">
							<s:include value="/jsp/siniestros/recuperacion/compania/cancelacionRecuperacionCIa.jsp"></s:include>			
						</div>
				</s:if>
				<s:if test='%{ verRedocumentacion == true    }'>
						<div selected="3" id="tabRedocumentacionCia" name="Redocumentaci&oacute;n">
							<s:include value="/jsp/siniestros/recuperacion/compania/redocumentacionCia.jsp"></s:include>			
						</div>
				</s:if>
		
				
				
				
				
				
				
				
				
				
				
				
				
			</div>
		</div>
		
	</div>
	
	
	<div id="contenido_Btn" style="width:100%;position: relative; padding-top: 10px">
			<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
					<div id="cerrarBtn" class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="cerrarRecuperacion();"> <s:text
								name="Cerrar" /> </a>
					</div>
			</div>			
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
								<div id="guardarBtn" class="btn_back w150"
										style="display: inline; float: left;margin-left: 1%;">
										<a href="javascript: void(0);" onclick="guardarRecuperacionCIA();"> <s:text
											name="Guardar" /> </a>
								</div>
					</div>
					
					<s:if test='%{tipoMostrar=="C"   &&   recuperacion.estatus != "RECUPERADO"   &&  recuperacion.estatus != "CANCELADO"     }'>
					
						<m:tienePermiso nombre="FN_M2_SN_Autorizar_Cancelacion_RecuperacionCIA"> 
					
							<s:if test='%{ cartaCia.estatus == "PEND_CANC"    }'>
									<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
										<div id="guardarBtn" class="btn_back w150"
												style="display: inline; float: left;margin-left: 1%;">
												<a href="javascript: void(0);" onclick="cancelacionCia();"> <s:text
													name="Autorizar Cancelaci&oacute;n" /> </a>
										</div>
									</div>
							</s:if>	
					    </m:tienePermiso>
							
							<m:tienePermiso nombre="FN_M2_SN_Solicitar_Cancelacion_RecuperacionCIA"> 
								<s:if test='%{ cartaCia.estatus != "PEND_CANC"    }'>
										<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
											<div id="guardarBtn" class="btn_back w150"
													style="display: inline; float: left;margin-left: 1%;">
													<a href="javascript: void(0);" onclick="solicitarCancelacion();"> <s:text
														name="Solicitar Cancelaci&oacute;n" /> </a>
											</div>
										</div>
								</s:if>   
							</m:tienePermiso>
													
							
					</s:if>
				
	</div>
	
</s:form>
<script type="text/javascript">
	jQuery(document).ready(function() {
		dhx_init_tabbars();
      	incializarTabs();
      	validarRegistroComplemento();
	});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>