<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaOC</param>
				<param>true</param>
				<param>infoAreaOC</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="selected" 			type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="ordenCompra" type="ro" width="120" sort="int" >Orden de Compra </column>
        <column id="conceptoDesc" 	type="ro" width="*" sort="str" >Concepto</column>
        <column id="tipoOcDesc" 	type="ro" width="*" sort="str" >Tipo</column>
		<column id="subtotalOC"   type="ron" width="180" format="$0,000.00" sort="int">Subtotal</column>
		<column id="porcentajeParticipacion"	    type="ro" width="150" sort="int">% Participacion</column>
		<column id="subTotalRecupProveedor"   type="ron" width="180" format="$0,000.00" sort="int">Recuperacion Proveedor</column>
		<column id="montoARecuperar"   type="ron" width="180" format="$0,000.00" sort="int">Monto a Recuperar</column>
		<column id="idRecuperacion" type="ro" width="*" sort="str"  hidden="true">idRecuperacion </column>
		<column id="montoPiezasConPorcentaje" type="ron" width="*" sort="int" hidden="true" >montoPiezasConPorcentaje </column>
	  	</head>
		<s:iterator value="lstOrdenesCompra">
			<row id="<s:property value="ordenCompra.id"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="ordenCompra.id" escapeHtml="false" escapeXml="true"/></cell>				
				<cell><s:property value="conceptoDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoOcDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="subtotalOC" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="porcentajeParticipacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="subTotalRecupProveedor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="montoARecuperar" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="recuperacion.id" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="montoPiezasConPorcentaje" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
</rows>