<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
        </beforeInit>
        <column id="selectAll"			type="ch"	hidden="false"	width="30" sort="str"  align="right">&lt;input type="checkbox" id="chAll" onChange="onCheckAll()"/&gt;</column>
      	<column id="numeroRecuperacion"	type="ro"	hidden="false"	width="90"  sort="int" align="center"><s:text name="midas.siniestros.recuperacion.compania.listar.numrecuperacion" /></column>
		<column id="folioCarta"			type="ro" 	hidden="false"	width="60"  sort="str" align="center"><s:text name="midas.siniestros.recuperacion.compania.listar.foliocarta" /></column>
		<column id="fechaRegistro"		type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.compania.listar.fecharegistro" /></column>
		<column id="nombreOficina"		type="ro"	hidden="false"	width="150" sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.oficina" /></column>
		<column id="tipoServicioPoliza"	type="ro"	hidden="false"	width="90"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.tiposervicio" /></column>
		<column id="numeroCia"			type="ro"	hidden="false"	width="80"  sort="int"  align="center"><s:text name="midas.siniestros.recuperacion.compania.listar.numcompania" /></column>
		<column id="compania"			type="ro"	hidden="false"	width="200" sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.compania" /></column>
		<column id="numeroSiniestro"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.numsiniestro" /></column>
		
		<s:if test="tipoModulo==2"> <!-- Entrega de Carta -->
			<column id="fechaSiniestro"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.fechasiniestro" /></column>
			<column id="edoOcurrencia"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.edoocurrencia" /></column>
			<column id="tipoSiniestro"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.tiposiniestro" /></column>
			<column id="cobertura"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.cobertura" /></column>
			<column id="nombrelesionado"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.nombrelesionado" /></column>
			<column id="vehiculotercero"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.vehiculotercero" /></column>			
		</s:if>
		
		
		
		<column id="usuario"			type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.usuario" /></column>
		<s:if test="tipoModulo==1">
			<column id="referenciaBancaria"	type="ro"	hidden="false"	width="120"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.referenciabancaria" /> </column>
			<column id="estatus"			type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.estatus" /></column>
		</s:if>
		
		<s:if test="tipoModulo==3">			
			<column id="oficinaReasignacion"	type="ro"	hidden="false"	width="150"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.oficinaReasignada" /></column>
		</s:if>		
		<s:if test="tipoModulo==4">
			<column id="fechaEnvio"			type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.compania.listar.fechaenvio" /></column>
			<column id="oficinaRecepcion"	type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.oficinadestino" /></column>
			<column id="fechaRecepcion"		type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.compania.listar.fecharecepcion" /></column>			
		</s:if>		
		<column id="monto"				type="ron"	hidden="false"	width="80"  sort="int" format="$0,000.00"><s:text name="midas.siniestros.recuperacion.compania.listar.monto" /></column>


		<s:if test="tipoModulo==2"> <!-- Entrega de Carta -->
			<column id="fechaAcuse"	type="ro"	hidden="false"	width="100"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.compania.listar.fechaacuse" /></column>
			<column id="fechaUltimoAcuse"	type="ro"	hidden="false"	width="100"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.compania.listar.fechaultimoacuse" /></column>
			<column id="observaciones"	type="ro"	hidden="false"	width="100"  sort="str" ><s:text name="midas.siniestros.recuperacion.compania.listar.observaciones" /></column>					
		</s:if>
		
		
	<!-- Columnas Ocultas -->
		<column id="oficinaId"			type="ro" hidden="true"></column>
	<!-- Columnas Ocultas -->
	</head>
	<s:if test="tipoModulo==4"> <!-- Entrega de Carta -->
			<s:iterator value="recuperaciones" var="index">
			 <row id="<s:property value="idRecuperacion" 		escapeHtml="false" escapeXml="true" />">
				<cell><s:property value=""						escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="numeroRecuperacion"	escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="folioCarta"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaRegistro"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="nombreOficina"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="tipoServicioPoliza"	escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="numeroCia"				escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="compania"				escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="numeroSiniestro"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="usuario"				escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaEnvio"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="oficinaRecepcion"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaRecepcion"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="monto"					escapeHtml="false" escapeXml="true"/></cell>
			<!-- Columnas Ocultas -->
				<cell><s:property value="oficinaId" 			escapeHtml="false" escapeXml="true" /></cell>
			<!-- Columnas Ocultas -->
			</row>
		</s:iterator>		
	</s:if>
	<s:else>
		<s:iterator value="recuperaciones" var="index">
		 <row id="<s:property value="idRecuperacion" 		escapeHtml="false" escapeXml="true" />">
			<cell><s:property value=""						escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroRecuperacion"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="folioCarta"			escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRegistro"			escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreOficina"			escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoServicioPoliza"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroCia"				escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="compania"				escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro"		escapeHtml="false" escapeXml="true"/></cell>
			
			<s:if test="tipoModulo==2"> <!-- Entrega de Carta -->
				<cell><s:property value="fechaSiniestro"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="edoOcurrencia"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="tipoSiniestro"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="cobertura"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="nombrelesionado"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="vehiculoTercero"		escapeHtml="false" escapeXml="true"/></cell>						
			</s:if>
			
			
			<cell><s:property value="usuario"				escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="tipoModulo==1">			
				<cell><s:property value="referenciaBancaria"	escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="estatusDesc"			escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			
			<s:if test="tipoModulo==3">			
				<cell><s:property value="oficinaReasignacion"	escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:if test="tipoModulo==4">		
				<cell><s:property value="fechaEnvio"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="oficinaRecepcion"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaRecepcion"		escapeHtml="false" escapeXml="true"/></cell>
			</s:if>			
			<cell><s:property value="monto"					escapeHtml="false" escapeXml="true"/></cell>
			
			
			<s:if test="tipoModulo==2"> <!-- Entrega de Carta -->
				<cell><s:property value="fechaAcuse"			escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaUltimoAcuse"		escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="observaciones"		escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
		
		<!-- Columnas Ocultas -->
			<cell><s:property value="oficinaId" 			escapeHtml="false" escapeXml="true" /></cell>
		<!-- Columnas Ocultas -->
		</row>
	</s:iterator>
		

	</s:else>
</rows>