<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>       
		 <column id="noRec"   type="ro"  width="*" sort="int" hidden="false"><s:text name="midas.siniestros.recuperacion.ingresospendientes.numerorecuperacion" /></column>
        <column id="cia"  type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.recuperacion.recuperacionCIA.compania" /> </column>
        <column id="fecha"  type="ro"  width="*" sort="str" hidden="false">Fecha Actual </column>
        <column id="fLimiteC"  type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.recuperacion.recuperacionCIA.fecLimite" /> </column>
        <column id="dias"  type="ro"  width="*" sort="int" hidden="false">Dias Restantes</column>

	</head>
	<s:iterator value="recuperacionesVencer" var="c" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="numero" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="compania.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaModificacion" escapeHtml="false" escapeXml="true"/></cell>				
			<cell><s:property value="fechaLimiteCobro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="diasVencimiento" escapeHtml="false" escapeXml="true"/></cell>	
			
			  
				
		</row>
	</s:iterator>
	
</rows>




