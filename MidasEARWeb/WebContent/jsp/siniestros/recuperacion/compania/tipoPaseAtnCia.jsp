<%@page pageEncoding="ISO-8859-1" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<div class="titulo" style="width: 98%;">
<s:hidden id="recuperacionId" name="recuperacionId" />
<s:hidden id="impresionPreparada" name="impresionPreparada" />
	<s:text name="midas.siniestros.recuperacion.recuperacionCIA.paseCia"/>
</div>
<s:select list="lstTipoPaseCia" id="lstTipoPaseCia"
		name="tipoPaseCia" 		
		cssClass="txtfield" 
		cssStyle="width:90%;" 
		>
</s:select>

	<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="btnImprimir">
					<a href="javascript: void(0);" onclick="reenviarImprimeCarta();"> 
					<s:text name="midas.boton.imprimir" /> </a>
				</div>
			</td>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="btnImprimir">
					<a href="javascript: void(0);" onclick="cerrarVentanaTipoPase();"> 
					<s:text name="midas.boton.cerrar" /> </a>
				</div>
			</td>								
		</tr>
	</table>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
