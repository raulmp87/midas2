<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionCia.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/listadoRecuperaciones.js'/>"></script>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:form id="alertaCIAForm" name="alertaCIAForm"  class="floatLeft">
<div id="contenido_AlertCia" style="width:99%;position: relative;">
	<s:hidden name="url" id="urlCia"/>
	<s:hidden name="recuperacionId" id="recuperacionIdCia"/>

	<div class="subtituloLeft"  style="width: 100%"align="left" >
		<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionCIA.leyendaAlerta')}" />
	</div>
	
	<div id="divInferior" style="width: 100% !important; padding-top: 10px" class="floatLeft">
		<div id="recuperacionesVencerGrid" class="dataGridConfigurationClass" style="width: 100%; height: 400px; "></div>
		<div id="pagingArea"></div><div id="infoArea"></div>			
	</div>
	<div id="divInferior" style="width: 100% !important;" class="floatLeft">			
		<div class="btn_back w80"
			style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="cerrarAlerta();"> <s:text
				name="Cerrar" />&nbsp;&nbsp;<img
				align="middle" border='0px' alt='Consultar'
				title='Cerrar' src='/MidasWeb/img/common/b_borrar.gif'
				style="vertical-align: left;" /> 
			</a>
		</div>
		<div class="btn_back w80"
			style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="continuarCIA();"> <s:text
				name="Continuar" />&nbsp;&nbsp;<img
				align="middle" border='0px' alt='Consultar'
				title='Continuar' src='/MidasWeb/img/common/b_siguiente.gif'
				style="vertical-align: left;" /> 
			</a>
		</div>
		
	</div> 
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script>
	jQuery(document).ready(function() {
		buscarRecuperacionesVencer();
		
	});
	
</script>
</div>
</s:form>










































