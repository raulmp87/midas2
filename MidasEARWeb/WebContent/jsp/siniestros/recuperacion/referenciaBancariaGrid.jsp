<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="setPagingSkin"> 
				<param>bricks</param>
			</call>   
        </beforeInit>       
				<column id="banco" type="ro" width="200" sort="str"  align="left" >Banco</column>
				<column id="cuenta" type="ro" width="120" sort="str" align="left" >Cuenta</column>
				<column id="clabe" type="ro" width="200" sort="str" align="left"  >CLABE</column>	
				<column id="referencia" type="ro" width="320" sort="str" align="left" >Referencia Bancaria</column>	
	</head>
	<s:iterator value="lstReferenciasBancarias" status="stats">
		<row id="<s:property value="#row.index"/>">
		    <cell><s:property value="bancoMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cuentaBancaria" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="clabe" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="numeroReferencia" escapeHtml="false" escapeXml="true"/></cell>
						
		</row>
	</s:iterator>
	
	
</rows>