<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<s:hidden id="compradorId" name="recuperacion.comprador.id" />
<s:hidden id="perfilUsuario" name="perfilUsuario" />
<s:hidden id="pantallaSeleccion" name="pantallaSeleccion" />


<div id="contenido_DefinirVentaPRV" style="width:99%;position: relative;">
	
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			
		 
			<div id="divGenerales" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="" style="width: 100%; height: 99%;">
						<div class="subtituloLeft" align="left" >
						<s:text name="%{getText('midas.siniestros.recuperacion.venta.tituloPreventa')}" />
						</div>						
						
						<div class="divFormulario" id="divRefacciones" >
								<s:if test=" soloConsulta!=true  ">
									<div class="floatLeft divInfDivInterno" style="width: 30%;" >
										<label for="fechaRegistroVenta" class="label"> Fecha Registro:</label>
										<sj:datepicker name="recuperacion.fechaRegistroVenta" id="datepickerFechaR"
											label="Fecha de Robo" labelposition="left" changeMonth="true" changeYear="true"
											buttonImage="../img/b_calendario.gif" id="fechaRegistroVenta"
											value="%{recuperacion.fechaRegistroVenta}" maxlength="10"
											cssClass="txtfield" size="12"
											onkeypress="return soloFecha(this, event, false);"
											onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
											onblur="esFechaValida(this);">
										</sj:datepicker>
									</div>
								</s:if>
								<s:else>
										<div class="floatLeft divInfDivInterno" style="width: 30%;" >
											 <s:textfield id="fechaRegistroVenta" name="recuperacion.fechaRegistroVenta"   
											cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.fechaR')}"
											onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
											 labelposition="left" cssStyle="width:37%;" ></s:textfield>
										</div>
								</s:else>
								
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="estados" id="estadosLis"
									name="recuperacion.estadoVenta.id" label="%{getText('midas.siniestros.recuperacion.venta.estado')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:60%;"
									 headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									onchange="changeEstados();"
									>
									</s:select>							
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="municipios" id="ciudadLis"
									name="recuperacion.ciudadVenta.id" label="%{getText('midas.siniestros.recuperacion.venta.ciudad')}"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:60%;"
									>
									</s:select>							
								</div>
							</div>
							
							<div class="divFormulario" >
							
								<div class="floatLeft divInfDivInterno" style="width: 50%;" >
									<s:textfield id="ubicacionVenta" name="recuperacion.ubicacionVenta"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.ubicacion')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 50%;" >
									<s:textfield id="personaRecogeRefaccion" name="recuperacion.personaRecogeRefaccion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.personaRecoge')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
								</div>
							</div>
							
							<div class="divFormulario" >
							
								<div class="floatLeft divInfDivInterno" style="width: 50%;" >
									<s:textfield id="personaEntregaRefaccion" name="recuperacion.personaEntregaRefaccion"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.personaEntrega')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
								</div>
								
								
								
								<s:select list="estatusVenta" id="estatusVentaLis"
									name="recuperacion.estatusPreventa" label="Estatus de Preventa"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:28%;"
									onchange="changeEstatusVenta();"
									>
									</s:select>	
							</div>
							
							
							<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 15%;padding-left:50px" >
										<div class="subtituloLeft" style="width: 100%;" align="left" >
											<s:text name="Folio para Venta"  />
										</div>
										
										
									</div>
									<div class="floatLeft divInfDivInterno" style="width: 20%;" >
										
										
										<div class="subtituloLeft" style="width: 100%;" align="left" >
											<s:text name="Concepto"  />
										</div>
										
									</div>
								
							</div>
							<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 100%;padding-left:50px" >
										
										<div id='lstFoliosVentaDIV' class="floatLeft divContenedorO" style="width: w270;  " >
											  		<ul id="lisFolios" class="w270"
														style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
														<s:iterator value="lstFoliosVenta"
															status="stat">
															<li>
																<label for="${id}"><s:property  value="folio"/></label> 
														
																<label  style="padding-left:50px" for="${id}"><s:property  value="concepto"/></label> 																		
															</li>
															
														</s:iterator>
													</ul>
											</div>
									</div>
								
							</div>
							<s:if test="%{tienePermisoCapturaDatosVenta==true || soloConsulta==true}">	
							 <div id="divDatosVenta" >
								<div class="subtituloLeft" align="left" >
									<s:text name="%{getText('midas.siniestros.recuperacion.venta.tituloVenta')}" />
								</div>
								<div class="divFormulario" >
								
									<div class="floatLeft divInfDivInterno" style="width: 35%;" >
										<s:textfield id="comprador" name="recuperacion.comprador.personaMidas.nombre"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.comprador')}" labelposition="left" cssStyle="width:70%;" readonly="true" ></s:textfield>
									</div>
									
										<div class="floatLeft divInfDivInterno" style="width: 5%;" >
										<s:if test="soloConsulta!=true  ">
											<div class="btn_back w50" id="btn_BusquedaComprador"
												style="display: inline; float: left; vertical-align: top; position: relative; margin-top: -3%;">
												<a href="javascript: void(0);" onclick="mostrarComprador();">
													<s:text name="midas.boton.buscar" /> </a>
											</div>
										</s:if>
										</div>
									<div class="floatLeft divInfDivInterno" style="width: 40%;" >
										<s:textfield id="correoComprador" name="recuperacion.correoComprador"   cssClass="txtfield"   label="%{getText('midas.siniestros.recuperacion.venta.correo')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
									</div>
								</div>
								
								<div class="divFormulario" >
								
									<div class="floatLeft divInfDivInterno" style="width: 50%;" >
										<s:textfield id="telefonoComprador" name="recuperacion.telefonoComprador"  onkeypress="return soloNumeros(this, event, true);"  cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.venta.tel')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 50%;" >
										<s:textfield id="comentariosVenta" name="recuperacion.comentariosVenta"   cssClass="txtfield"   label="%{getText('midas.siniestros.recuperacion.venta.comentarios')}" labelposition="left" cssStyle="width:45%;" ></s:textfield>
									</div>
								</div>
							
							<div class="subtituloLeft" align="left" >
							<s:text name="Totales" />
							</div>
							<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 20%;" >
										<s:textfield id="text_subtotalVenta"  name="recuperacion.subTotalVenta"    cssClass="txtfield  setNew formatCurrency" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_subtotal',this.value);" onblur="mascaraDecimales('#text_subtotal',this.value);validarTotalVenta(this);" label="SubTotal" labelposition="left" cssStyle="width:70px;"></s:textfield>
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 20%;" >
										<s:textfield id="text_IvaVenta"   name="recuperacion.ivaVenta" cssClass="txtfield  setNew formatCurrency"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_Iva',this.value);" onblur="mascaraDecimales('#text_Iva',this.value);validarTotalVenta(this);"  label="I.V.A." labelposition="left" cssStyle="width:70px;" ></s:textfield>
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 60%;" >
										<s:textfield id="text_montoTotalVenta"   name="recuperacion.montoVenta" cssClass="txtfield  setNew formatCurrency"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#text_montoTotal',this.value);" onblur="mascaraDecimales('#text_montoTotal',this.value);validarTotalVenta(this);"  label="Monto Final Recuperado" labelposition="left" cssStyle="width:70px;" ></s:textfield>
								</div>
							</div>
							<div id="divInferior" style="width: 1050px !important; padding-left: 15px;"
								class="floatLeft">							
								<div id="cuentaVentaGrid" style="width: 790px; height: 100px;"></div>
								<div id="pagingArea" style="padding-top: 8px"></div>
								<div id="infoArea"></div>
							</div>
						</div>
					</s:if>	
						
					</div>
			</div>
		</div>
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
	iniContenedorVentaPRV();
		initCurrencyFormatOnTxtInput();
		buscarCuentasVenta();
		
	});
	</script>
	
</div>



