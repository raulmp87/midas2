<%@page pageEncoding="ISO-8859-1" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/listadoRecuperaciones.js'/>"></script>


<div class="titulo" style="width: 98%;">
	Tipo Recuperacion
	<!--<s:text name="midas.liquidaciones.busqueda.busqueda.liquidaciones.proveedor"/>-->	
</div>
<s:select list="lstTipoRecuperacion" cssStyle="width: 90%;"
	name="tipoRecuperacion" id="tipoRecuperacion"
	cssClass="cleaneable txtfield obligatorio"
	onchange = "reenviarRecuperacion();"
	headerKey="" headerValue="%{getText('midas.general.seleccione')}">
</s:select>

