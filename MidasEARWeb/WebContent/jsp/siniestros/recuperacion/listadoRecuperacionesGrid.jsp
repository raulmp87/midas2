<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="noRecuperacion"     type="ro" width="75"  sort="int" ><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.no.recuperacion'}"/></column>	
        <column id="tipoRecuperacion"   type="ro" width="120" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.tipo.recuperacion'}"/></column>
		<column id="fechaRegistro"      type="ro" width="150"  sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.fecha.registro.de'}"/></column>	
		<column id="nombreProveedor"    type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.proveedor.comprador'}"/> </column>
		<column id="noSiniestro"        type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.no.siniestro'}"/> </column>
		<column id="noReporte"          type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.no.reporte'}"/> </column>
		<column id="fechaSiniestro"     type="ro" width="180" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.fecha.siniestro.de'}"/> </column>
		<column id="monto"              type="ron" width="120" sort="int" format="$0,000.00"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.monto'}"/> </column>
		<column id="fechaAplicacion" 	type="ro" width="155" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.fecha.aplicacion'}"/> </column>
		<column id="estatus"            type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.estatus.recuperacion'}"/> </column>
		<column id="usuario" 		    type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.usuario'}"/> </column>
		<column id="paseAtencion"       type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.pase.atencion'}"/> </column>
		<column id="ptDocumentada" 		type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.pt.documentada'}"/> </column>
		<column id="folios" 		    type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.folios'}"/> </column>
		<column id="estatusCarta" 		type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.estatus.carta'}"/> </column>
		<column id="indemnizado" 		type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.indenmizado'}"/> </column>
		<column id="ensubasta" 		    type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.subasta'}"/> </column>
		<column                         type="img" width="70" sort="na" align="center" >Acciones</column>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column> 
		

	  	</head>
		<s:iterator value="listadoRecuperacionDto">
			<row id="<s:property value="#row.index"/>">
				<cell><s:property value="numeroRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombrePrestador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="monto" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaAplicacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="usuario" escapeHtml="false" escapeXml="true" /></cell> 
				<cell><s:property value="paseAtencion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="ptDocumentada" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="folioCarta" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusCartaCia" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="esIndemnizado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="enSubasta" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test=" tipoRecuperacion != 'CRU' ">
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:consultarRecuperacion(<s:property value="idRecuperacion" escapeHtml="false" escapeXml="true" />, "<s:property value="tipoRecuperacion" />")^_self</cell>
				 </s:if>
                <s:else>
                		<cell>../img/pixel.gif^Consultar Deshabilitado^^_self</cell>
                </s:else>
                
				<s:if test=" (estatus == \"REGISTRADO\"  || estatus == \"INACTIVO\" || estatus == \"PENDIENTE\")   &&  (estatusPreventa != \"VENTA\") &&  (tipoRecuperacion != 'CRU') && (tipoRecuperacion != 'SIP')">
					<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript:editarRecuperacion(<s:property value="idRecuperacion" escapeHtml="false" escapeXml="true" />, "<s:property value="tipoRecuperacion" />")^_self</cell>
				</s:if>
				<s:else>
                	<cell>../img/pixel.gif^Editar Deshabilitado^^_self</cell>
                </s:else>
                
                <s:if test=" ( estatus == \"REGISTRADO\" || estatus == \"PENDIENTE\" ) && ( tipoRecuperacion != 'CRU' )  &&  (tipoRecuperacion != 'SIP')  ">
					<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Cancelar^javascript:cancelarRecuperacion(<s:property value="idRecuperacion" escapeHtml="false" escapeXml="true" />,true, "<s:property value="tipoRecuperacion" />")^_self</cell>
                </s:if>
                <s:else>
                	<cell>../img/pixel.gif^Cancelar Deshabilitado^^_self</cell>
                </s:else>
                
                <cell><s:property value="estatusPreventa" escapeHtml="false" escapeXml="true"/> </cell>
            
                
			</row>
		</s:iterator>
	
</rows>
   
