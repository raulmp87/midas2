<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<%-- <style type="text/css"> --%>
<!-- div.ui-datepicker { -->
<!-- 	font-size: 10px; -->
<!-- } -->

<!-- #divSup { -->
<!-- 	height: 300px; -->
<!-- } -->

<!-- #divInf { -->
<!-- 	height: 200px; -->
<!-- } -->

<!-- .divInterno { -->
<!-- 	height: 60px; -->
<!-- 	position: relative; -->
<!-- } -->
<!-- .divFormulario { -->
<!-- 	height: 35px; -->
<!-- 	position: relative; -->
<!-- } -->

<!-- .divContenedorO { -->
<!-- 	border: 1px solid #28b31a; -->
<!-- } -->

<!-- .divContenedorU { -->
<!-- 	border-bottom: 1px solid #28b31a; -->
<!-- 	border-right: 1px solid #28b31a; -->
<!-- } -->

<!-- .estilodias { -->
<!-- 	 background-color: #FFCCCC; -->
<!-- 	 font-family: Verdana, Arial, Helvetica, sans-serif; -->
<!-- 	font-size: 7pt; -->
<!-- 	color: red; -->
<!-- } -->


<!-- .floatRight { -->
<!-- 	float: right; -->
<!-- 	position: relative; -->
<!-- } -->

<!-- .floatLeft { -->
<!-- 	float: left; -->
<!-- 	position: relative; -->
<!-- } -->
<!-- .floatLeftNoLabel{ -->
<!-- 	float: left; -->
<!-- 	position: relative; -->
<!-- 	margin-top: 12px; -->
<!-- } -->
<!-- .divInfDivInterno { -->
<!-- 	width: 17%; -->
<!-- } -->
<!-- .error { -->
<!-- 	background-color: red; -->
<!-- 	opacity: 0.4; -->
<!-- } -->
<!-- #b_copiar { -->
<!-- 	background-image: url("../img/icons/ico_copiar1.gif"); -->
<!-- 	background-repeat: no-repeat; -->
<!-- 	text-align: center; -->
<!-- } -->
<%-- </style> --%>
<style type="text/css">
table#agregar th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionDeducible.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionProveedor.js'/>"></script>
<%-- <script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script> --%>

<s:form id="detalleRecuperacionDeducibleForm" >
<div id="contenido_DefinirLiq" style="width:98%;">

<s:hidden id="idLiquidacion" name="liquidacionSiniestro.id"/>
<s:hidden id="estatusLiquidacion" name="liquidacionSiniestro.estatus"/>
<s:hidden id="soloLectura" name="soloLectura"/>
<s:hidden id="tipoMostrar" name="tipoMostrar"/>
<s:hidden id="proveedorTieneInfoBancaria" name="proveedorTieneInfoBancaria"></s:hidden>
<s:hidden id="origenBusquedaLiquidaciones" name="origenBusquedaLiquidaciones"/>

		<div id="contenedorFiltros">		
			<table id="agregar" style="width:98%;" border="0"> 				
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.tipoSiniestro"/>
					</th>
					<td colspan="3">
						<s:textfield id="solicitudCheque" disabled="true" name="recuperacion.tipoSiniestro"   cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.antiguedadCreacion"/>
					</th>
					<td>
						<s:textfield id="idLiquidacionSiniestro" disabled="true" name="recuperacion.antiguedadCreacion" 
						value="%{recuperacion.antiguedadCreacion} %{recuperacion.antiguedadCreacion != null ? '   dias':''}" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>  
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.antiguedadCobro"/>
					</th>
					<td>
						<s:textfield id="estatusLiq" disabled="true" name="recuperacion.antiguedadCobro" 
						value="%{recuperacion.antiguedadCobro} %{recuperacion.antiguedadCobro != null ? '   dias':''}" cssClass="cajaTextoM2 w180"></s:textfield>
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.tipoProceso"/>
					</th>
					<td  colspan="3">
						<s:textfield id="tipoProceso" disabled="%{tipoMostrar != \"U\" || (recuperacion.tipoProceso != \"\" && recuperacion.tipoProceso != null)}" 
						name="recuperacion.tipoProceso" cssClass="cajaTextoM2 w200 jQrequired"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.fechaIngreso"/>
					</th>
					<s:if test="%{tipoMostrar == \"U\" && (recuperacion.fechaIngreso == \"\" || recuperacion.fechaIngreso == null)}">
						<td>
							<sj:datepicker 
									name="recuperacion.fechaIngreso"
									cssStyle="width: 80px;" required="#requiredField" labelposition="top"
									buttonImage="../img/b_calendario.gif" id="fechaIngreso"
									changeMonth="true" changeYear="true" maxlength="10"
									cssClass="txtfield jQrequired" size="12" readOnly="%{soloLectura}" disabled="%{soloLectura}"
									onkeypress="return soloFecha(this, event, false);"
									onchange="onChangeFechaIngreso();"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</td>	
					</s:if>
					<s:else>
						<td>
							<s:textfield id="fechaIngreso" disabled="true" name="recuperacion.fechaIngreso" cssClass="cajaTextoM2 w200"></s:textfield>
						</td>
					</s:else>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.fechaEntrega"/>
					</th>
					<s:if test="%{tipoMostrar == \"U\" && (recuperacion.fechaEntrega == \"\" || recuperacion.fechaEntrega == null)}">
						<td>
							<sj:datepicker 
									name="recuperacion.fechaEntrega"
									cssStyle="width: 80px;" required="#requiredField" labelposition="top"
									buttonImage="../img/b_calendario.gif" id="fechaEntrega"
									changeMonth="true" changeYear="true" maxlength="10"
									cssClass="txtfield jQrequired" size="12" readOnly="%{soloLectura}" disabled="%{soloLectura}"
									onkeypress="return soloFecha(this, event, false);"
									onchange="onChangeFechaEntrega();"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
								</sj:datepicker>
						</td>	
					</s:if>
					<s:else>
						<td>
							<s:textfield id="fechaEntrega" disabled="true" name="recuperacion.fechaEntrega" cssClass="cajaTextoM2 w180"></s:textfield>
						</td>	
					</s:else>							
				</tr>
				<tr>					
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.numeroValuacion"/>
					</th>
					<td colspan="3">
						<s:textfield id="numeroValuacion" disabled="%{tipoMostrar != \"U\" || (recuperacion.numeroValuacion != \"\" && recuperacion.numeroValuacion != null)}" 
						name="recuperacion.numeroValuacion" cssClass="cajaTextoM2 w200 jQrestrict jQrequired jQnumeric"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.nombreValuador"/>
					</th>
					<td>
						<s:textfield id="nombreValuador" disabled="%{tipoMostrar != \"U\" || (recuperacion.nombreValuador != \"\" && recuperacion.nombreValuador != null)}" 
						name="recuperacion.nombreValuador" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.nombreAfectado"/>
					</th>
					<td colspan="3">
						<s:textfield id="nombreAfectado" disabled="%{tipoMostrar != \"U\" || (recuperacion.afectado != \"\" && recuperacion.afectado != null)}" 
						name="recuperacion.afectado" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.marca"/>
					</th>
					<td colspan="3">
						<s:textfield id="correo" disabled="true" name="recuperacion.descripcionMarca" cssClass="cajaTextoM2 jQnumeric w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.tipo"/>
					</th>
					<td>						
						<s:textfield id="telefonoLada" disabled="true" name="recuperacion.descripcionEstilo" cssClass="cajaTextoM2 jQalphanumeric w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.modelo"/>
					</th>
					<td>						
						<s:textfield id="telefonoNumero" disabled="true" name="recuperacion.modeloVehiculo" cssClass="cajaTextoM2 jQalphanumeric w180"></s:textfield>
					</td>
				</tr>
				<tr>					
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.tipoDeducible"/>
					</th>
					<td colspan="1">
						<s:textfield id="estatusLiq" disabled="true" name="recuperacion.tipoDeducible" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.porcentaje"/>
					</th>
					<td>
						<s:textfield id="estatusLiq" disabled="true" name="recuperacion.porcentajeDeducible" cssClass="cajaTextoM2 w50"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.montoDeducible"/>
					</th>
					<td>
						<s:textfield id="moneda" disabled="true" name="recuperacion.montoDeducible" cssClass="cajaTextoM2 w200 formatCurrency"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.montoFinalDeducible"/>
					</th>
					<td>
						<s:textfield id="estatusLiq" disabled="true" name="recuperacion.montoFinalDeducible" cssClass="cajaTextoM2 w180 formatCurrency"></s:textfield>
					</td>
				</tr>	
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.taller"/>
					</th>
					<td colspan="3">
						<s:textfield id="estatusLiq" disabled="true" name="recuperacion.taller" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.referencia"/>
					</th>
					<td nowrap="nowrap">
						<s:textfield id="moneda" readonly="true" name="recuperacion.referencia" cssStyle="background-color:#FAFAF0;" cssClass="cajaTextoM2 w200"></s:textfield>
						
					</td>
					<td colspan="2">
						<div id="btn_imprimirInstructivo" class="btn_back w180" style="display: inline; float: right;" >
								<a href="javascript: void(0);" onclick="imprimirInstructivoDeposito();"> 
									<s:text name="midas.siniestros.recuperacion.deducible.boton.imprimirinstructivo" /> 							
								</a>
							</div>
					</td>
				</tr>	
				<s:if test="%{tipoMostrar == \"C\" || recuperacion.estatus == \"CANCELADO\"}">								
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.motivoCancelacion"/>
					</th>
					<td colspan="8">
						<s:textarea cols="5" rows="5" disabled="recuperacion.estatus == \"CANCELADO\" " required="true" 
						id="motivo" name="recuperacion.motivoCancelacion" 
						cssClass="cajaTextoM2 w800" />	
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.deducible.canceladoPor"/>
					</th>
					<td colspan="6">
						<s:textfield id="usuarioCancelacion" disabled="true" name="recuperacion.codigoUsuarioCancelacion" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>					
				</tr>	
			</s:if>						
 			</table> 
 		</div>	
 	</div>
 	<div id="spacer" style="height:10px;"></div>
 	<s:if test="%{tipoMostrar == \"U\"}">
	
						<div style="width: 99%;background:white;">
							<div id="btn_guardar" class="btn_back w140" style="display: inline; float: right;" >
								<a href="javascript: void(0);" onclick="guardarRecuperacionDeducible();"> 
									<s:text name="midas.boton.guardar" /> 							
								</a>
							</div>
	    				</div>
	    				<s:if test="recuperacion.ingresoActivo == null 
	    				&& recuperacion.estatus == \"REGISTRADO\" ">
	    				<div style="width: 99%;background:white;">
							<div id="btn_generarIngreso" class="btn_back w170" style="display: inline; float: right;" >
								<a href="javascript: void(0);" onclick="generarMovIngreso();"> 
									<s:text name="midas.siniestros.recuperacion.deducible.boton.generarIngreso" /> 							
								</a>
							</div>
	    				</div>	
	    				</s:if>	
			</s:if>	
	<s:if test="%{tipoMostrar == \"C\"}">	
		<td>
			<div style="width: 99%;background:white;">
				<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="cancelarRecuperacionDeducible(motivo.value);"> 
						<s:text name="midas.boton.cancelar" /> 							
					</a>
				</div>
	 				</div>	
		</td>						   
	</s:if>				
 	<div style="width: 99%;background:white;">
		<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="cerrarRecuperacion();"> 
						<s:text name="midas.boton.cerrar" /> 							
					</a>
				</div>
	    </div>	
</s:form> 		
<script>
	jQuery(document).ready(function() {
		initCurrencyFormatOnTxtInput();
		//iniContenedorRecuperacionPRV();
		//buscarCuentasDevolucion();
	});
</script>
	




