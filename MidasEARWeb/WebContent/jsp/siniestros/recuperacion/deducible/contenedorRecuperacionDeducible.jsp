
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionDeducible.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<%-- <style type="text/css"> --%>
<!-- .error { -->
<!-- 	background-color: red; -->
<!-- 	opacity: 0.4; -->
<!-- } -->
<!-- .divFormulario { -->
<!-- 	height: 35px; -->
<!-- 	position: relative; -->
<!-- } -->
<!-- #divSup { -->
<!-- 	height: 300px; -->
<!-- } -->

<!-- #divInf { -->
<!-- 	height: 200px; -->
<!-- } -->

<!-- .divInterno { -->
<!-- 	height: 60px; -->
<!-- 	position: relative; -->
<!-- } -->
<!-- .divFormulario { -->
<!-- 	height: 35px; -->
<!-- 	position: relative; -->
<!-- } -->

<!-- .divContenedorO { -->
<!-- 	border: 1px solid #28b31a; -->
<!-- } -->

<!-- .divContenedorU { -->
<!-- 	border-bottom: 1px solid #28b31a; -->
<!-- 	border-right: 1px solid #28b31a; -->
<!-- } -->

<!-- .estilodias { -->
<!-- 	 background-color: #FFCCCC; -->
<!-- 	 font-family: Verdana, Arial, Helvetica, sans-serif; -->
<!-- 	font-size: 7pt; -->
<!-- 	color: red; -->
<!-- } -->


<!-- .floatRight { -->
<!-- 	float: right; -->
<!-- 	position: relative; -->
<!-- } -->

<!-- .floatLeft { -->
<!-- 	float: left; -->
<!-- 	position: relative; -->
<!-- } -->
<!-- .floatLeftNoLabel{ -->
<!-- 	float: left; -->
<!-- 	position: relative; -->
<!-- 	margin-top: 12px; -->
<!-- } -->

<%-- </style> --%>
<s:form  id="contenedorRecuperacionDeducibleForm">
<s:hidden id="soloConsulta" 	name="soloConsulta" />
<s:hidden id="h_soloLectura" 	name="soloConsulta" />
<s:hidden id="esNuevoRegistro" 	name="esNuevoRegistro" />
<s:hidden id="esAccionCancelar" 	name="esAccionCancelar" />
<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
<s:hidden id="recuperacionMotivoCancelacion" name="recuperacion.motivoCancelacion"/>
<s:hidden id="tipoMostrar" name="tipoMostrar" />
<s:hidden id="idRecuperacion" name="recuperacion.id" />
<s:hidden id="tienePermisoCapturaDatosVenta" name="tienePermisoCapturaDatosVenta" />

	<div id="recuperacionGenericaDiv">
			<s:include value="/jsp/siniestros/recuperacion/recuperacionGenerica.jsp"></s:include>
	</div>
	
	<div id="contenido_DefinirTitulo" style="width:99%;position: relative;">
		
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.titulo')}"/>
			</div>
		</div>
	</div>
	
	<div id="contenidoTabs" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 460px; width: 1050px" 
					id="recuperacionDeducibleTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div width="200px" id="detalleRecupDeducible" name="Recuperación de Deducible" 
				href="http://void" extraAction="javascript: verTabDetalleRecuperacion()">
				</div>
			</div>
		</div>		
	</div>	
</s:form>
<script type="text/javascript">
      dhx_init_tabbars();
     // incializarTabs();  
</script>