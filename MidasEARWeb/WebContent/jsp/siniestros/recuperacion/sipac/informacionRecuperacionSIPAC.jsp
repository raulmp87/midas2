<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>


<style type="text/css">
table#agregar th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
</style>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionSIPAC.js'/>"></script>

<s:form id="detalleRecuperacionSIPACForm" >
<div id="contenido_DefinirLiq" style="width:98%;">


<s:hidden id="soloLectura" name="soloLectura"/>
<s:hidden id="tipoMostrar" name="tipoMostrar"/>
<s:hidden id="proveedorTieneInfoBancaria" name="proveedorTieneInfoBancaria"></s:hidden>
<s:hidden id="origenBusquedaLiquidaciones" name="origenBusquedaLiquidaciones"/>

		<div id="contenedorFiltros">		
			<table id="agregar" style="width:98%;" border="0"> 			
				<tr>	
					<th>
							<s:text name="midas.siniestros.recuperacion.sipac.numeroPoliza"/>  
					</th>
					<td>
							<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.numPoliza"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.lugarSiniestro"/>  
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.lugarSiniestro"></s:textfield>
					</td>  
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.fechaSiniestro"/>  
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.fechaSiniestro"></s:textfield>					
					</td>					
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.marca"/>				
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.descripcionMarca"></s:textfield>
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.tipo"/> 
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.descripcionEstilo"></s:textfield>
					</td>
					
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.modelo"/> 
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.modeloVehiculo"></s:textfield>
					</td>
								
				</tr>
				<tr>					
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.dua"/>
					</th>
					<td>
							<s:textfield disabled = "true" name="recuperacion.estimacion.dua" cssClass="cajaTextoM2 w200"></s:textfield>
					
					</td>	
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.vehiculoTercero"/>
					
					</th>
					<td>
						<s:textfield disabled = "true" name="recuperacion.vehiculoTercero" cssClass="cajaTextoM2 w200"></s:textfield>
					
					</td>	
				</tr>
				<tr>
					<th>
					 
						
					
					</th>
					<td>
						
				
					</td>
				</tr>
				<tr>
					<th>		 
						<s:text name="midas.siniestros.recuperacion.sipac.companiaResponsable"/>					
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.ciaSeguros"></s:textfield>				
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.polizaCompania"/>
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.polizaCia"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.siniestroCompania"/>
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.siniestroCia"></s:textfield>		
					</td>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.incisoCompania"/>	
					</th>
					<td>		
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.incisoCia"></s:textfield>
					</td>
				</tr>	
				
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.valorEstimado"/>
					</th>
					<td>
						<s:textfield disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.totalEstimado"></s:textfield>
					</td>
					<th>
						
					</th>
					<td>
						
					</td>
					<th>
						
					</th>
					<td>		
						
					</td>
				</tr>	
				<s:if test="%{recuperacion.estatus == \"CANCELADO\"}">								
				<tr>
					<th>
						<s:text name="midas.siniestros.recuperacion.sipac.cancelacion"/>	
					</th>
					<td>
						<s:textfield id="fechaCancelacion" disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.fechaCancelacion"></s:textfield>	
					</td>
					<th>
					   <s:text name="midas.siniestros.recuperacion.sipac.motivoCancelacion" />
					</th>
					<td>
					  <s:textfield id="motivoCancelacion" disabled = "true" cssClass="cajaTextoM2 w200" name="recuperacion.motivoCancelacion"></s:textfield>
					</td>					
				</tr>	
			</s:if>						
 			</table> 
 		</div>	
 	</div>
 	<div id="spacer" style="height:10px;"></div>
 		
			
 	<div style="width: 99%;background:white;">
 	
		<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="cerrarRecuperacion();"> 
						<s:text name="midas.boton.cerrar" /> 							
					</a>
				</div>
				
	</div>	
</s:form> 		
<script>
	jQuery(document).ready(function() {
		initCurrencyFormatOnTxtInput();
	});
</script>
	




