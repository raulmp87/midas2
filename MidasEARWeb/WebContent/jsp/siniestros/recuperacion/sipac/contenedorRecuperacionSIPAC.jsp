
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/recuperacion/recuperacionSIPAC.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>


<s:form  id="contenedorRecuperacionSIPACForm">
<s:hidden id="soloConsulta" 	name="soloConsulta" />
<s:hidden id="h_soloLectura" 	name="soloConsulta" />
<s:hidden id="esNuevoRegistro" 	name="esNuevoRegistro" />
<s:hidden id="esAccionCancelar" 	name="esAccionCancelar" />
<s:hidden id="estatusRecuperacion" name="recuperacion.estatus"/>
<s:hidden id="recuperacionMotivoCancelacion" name="recuperacion.motivoCancelacion"/>
<s:hidden id="tipoMostrar" name="tipoMostrar" />
<s:hidden id="idRecuperacion" name="recuperacion.id" />
<s:hidden id="idRecuperacion_alt" name="rcp_id" />
<s:hidden id="tienePermisoCapturaDatosVenta" name="tienePermisoCapturaDatosVenta" />

	<div id="recuperacionGenericaDiv">
			<s:include value="/jsp/siniestros/recuperacion/recuperacionGenerica.jsp"></s:include>
	</div>
	
	<div id="contenido_DefinirTitulo" style="width:99%;position: relative;">
		
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="%{getText('midas.siniestros.recuperacion.recuperacionPrv.titulo')}"/>
			</div>
		</div>
	</div>
	
	<div id="contenidoTabs" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 460px; width: 1050px" 
					id="recuperacionSIPACTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div width="200px" id="detalleRecupSIPAC" name="Recuperación SIPAC" 
				href="http://void" extraAction="javascript: verTabDetalleRecuperacion()">
				</div>
			</div>
		</div>		
	</div>	
</s:form>
<script type="text/javascript">
      dhx_init_tabbars();
</script>