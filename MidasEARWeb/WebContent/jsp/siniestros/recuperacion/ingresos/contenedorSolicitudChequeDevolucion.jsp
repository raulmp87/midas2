<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="ingresoDevolucionHeader.jsp"></s:include>
<s:div name="divInformacion">
	<s:include value="informacionSolicitudCheque.jsp" />
</s:div>

<table width="98%">
	<tr>

		<td>
			<table id="agregar"
				style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;"
							id="b_cerrar">
							<a href="javascript: void(0);" onclick="mostrarBusqueda();"><s:text
									name="midas.boton.cerrar" /> </a>
						</div>
						<s:if test="consulta==false">
							<div class="btn_back w140" style="display: inline; float: right;"
								id="b_enviar">
								<a href="javascript: void(0);" onclick="enviar();"> <s:text
										name="midas.boton.enviar" /> </a>
							</div>
						</s:if>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
 
<s:div name="divCargaFactura">
	<s:include value="cargaFacturaIngresoDevolucion.jsp" />
</s:div>
 

<div id="pagingArea"></div>
<div id="infoArea"></div>

<script type="text/javascript">
initCurrencyFormatOnTxtInput();
getFacturaGrid();
init('<s:property value="flujo" escapeHtml="false" escapeXml="true"/>');
onChangeFormaPago();
</script>