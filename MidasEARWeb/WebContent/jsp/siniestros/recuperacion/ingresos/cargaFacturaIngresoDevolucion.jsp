<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:form name="cargaFacturaIngresoDevolucionForm" id="cargaFacturaIngresoDevolucionForm" method="post">
<s:hidden name="ingresoDevolucion.id" id="id"></s:hidden>
<table width="80%" align="center">
<tr>
<td>
	<div class="titulo">
		<s:text name="midas.siniestros.recuperacion.ingresodevolucion.registrofacturatitulo" />
	</div>

	<div id="spacer1" style="height: 10px"></div>
	<div align="left">
		<table>
			<tr>
				<td>
					<div class="btn_back w110" id="cargar" >
						<a href="javascript: void(0);" title="Seleccione una factura en formato xml"
						   onclick="cargarFactura();">
							<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.cargarFacturas.boton"/>
						</a>
					</div>
				</td>
				<td>
				<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
					<font color="#FF6600">
						<s:text name="midas.siniestros.recuperacion.ingresodevolucion.errorarchivo"></s:text>			
					</font>
				</div>
				</td>
			</tr>		
		</table>
		
	</div>
	<div id="spacer1" style="height: 10px"></div>
	<div id="divFacturas">    
	    <div id="indicador"></div>
		<div id="gridFacturasPaginado">
			<div id="facturaListadoGrid" style="width:98%;height:180px">
		</div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	    </div>
	</div>



</td>
</tr>	
</table>

</s:form>