<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	
	<form id="buscarMovsManualesForm">
	<s:hidden id="movsManualesConcat" name="filtroMovManual.movManualConcat" />
	<table width="99%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
					  
	                  <tr>
						<td colspan="6" >
							<table  id="filtrosM2" width="98%">
					            <tr>
					                  <td class="titulo" colspan="6"><s:text name="midas.siniestros.recuperacion.movsmanuales.titulobusquedamovsmanual" /></td>
					            </tr>
					          
					          	<tr>
					            	<td>
						            	<s:select name="filtroMovManual.idCuentaContable" id="cuentaContable" list="listaCuentaManual"
						            	cssClass="cajaTextoM2 w230 obligatorio" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						            	label="%{getText('midas.siniestros.recuperacion.movsmanuales.cuentacontable')}"/>
					            	</td>
									
									<td colspan="5">
						            	<s:textfield name="filtroMovManual.causaMovimiento" id="causaMovimiento" cssClass="cajaTextoM2 w230 obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.movsmanuales.causamovimiento')}" disabled="false" />
					            	</td>
									
					            </tr>
					            
					            <tr>
					            	<td>
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.movsmanuales.fechatraspasodesde')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroMovManual.fechaTraspasoCuentaDesde"
										id="fechaInicioTraspaso_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td >
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.movsmanuales.fechatraspasohasta')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroMovManual.fechaTraspasoCuentaHasta"
										id="fechaFinTraspaso_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td>
						            	<s:textfield id="importeInicial_t"
										label="%{getText('midas.siniestros.recuperacion.movsmanuales.importedesde')}"
										name="filtroMovManual.importeDesde"
										onkeyup="mascaraDecimales('#importeInicial_t',this.value);"
										onblur="mascaraDecimales('#importeInicial_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            	
					            	<td colspan="3">
						            	<s:textfield id="importeFinal_t"
										label="%{getText('midas.siniestros.recuperacion.movsmanuales.importehasta')}"
										name="filtroMovManual.importeHasta"
										onkeyup="mascaraDecimales('#importeFinal_t',this.value);"
										onblur="mascaraDecimales('#importeFinal_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            </tr>
					            
					            <tr>		
										<td colspan="6">
											<table style="padding: 0px; width: 100%; margin: 0px; margin-top:1%; border: none;">
												<tr>
													<td>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
															 <a href="javascript: void(0);" onclick="onClickBuscarMovsManuales();" >
															 <s:text name="midas.boton.buscar" /> </a>
														</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right; margin-left: 2%;" id="b_borrar">
															<a href="javascript: void(0);" onclick="limpiarFiltrosMovsManuales();"> 
															<s:text name="midas.boton.limpiar" /> </a>
														</div>
														<m:tienePermiso nombre="FN_M2_SN_Movimientos_Manuales_Gerente">
														<div class="btn_back w140 alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_registrar">
															 <a href="javascript: void(0);" onclick="mostrarVentanaRegistrarMovManual();" >
															 Agregar Movimiento</a>
														</div>
														</m:tienePermiso>
													</td>							
												</tr>
											</table>				
										</td>		
								</tr>
					
					      </table>
						</td>
					  </tr>
					  
					  <tr><td>
					  </td></tr>
					  
					<tr>
						<td colspan="6">
							<div id="tituloListadoMovsManuales" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.recuperacion.movsacreedores.titulolistadomovsacreedor" /></div>
							<div id="indicadorMovsManuales"></div>	
							<div id="movsManualesGridContainer" style="display:none;">
								<div id="movtosManualesGrid" style="width:98%;height:200px;"></div>
								<div id="pagingAreaMovsManuales"></div><div id="infoAreaMovsManuales"></div>
							</div>
						</td>
					</tr>
					
	            </tbody>
	</table>
	</form>

<script type="text/javascript">
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		iniDetaManual();
	});
</script>