<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/direccion/direccionSiniestroMidas.js'/>"  type="text/javascript"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/facturacionIngresos.js'/>"></script>

<style type="text/css">
	#superior{
		height: 240px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
	<form id="facturarIngresoForm">  
		<s:hidden id="ingresosConcat" name="filtroIngresos" />
		<s:hidden id="emisionFacturaId" name="emisionFacturaId" />
		<s:hidden id="tipoRecuperacion" name="tipoRecuperacion" />
		<div id="divFacturarIngreso">
			<table width="98%">
				<tr>
					<td valign="top">
						<div class="titulo">
							<s:text name="midas.siniestros.recuperacion.ingresos.facturar.titulo"></s:text>
						</div>
						<div id="indicador"></div>
						<div id="listadoIngresosFacturablesGrid" style="width: 90%; height: 380px;display:none;"></div>
						<div id="pagingArea"></div>
						<div id="infoArea"></div>
					</td>
					<td>
						<div class="titulo">
							<s:text name="midas.siniestros.recuperacion.ingresospendientes.tituloDatosGral"/>	
						</div>
						<table class="contenedorConFormato">
							<tr>
								<td class="jQIsRequired">	
									<label for="${idTipoPersona}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.tipopersona')}"/>
 									</label>
 								</td>
 								<td>
									<s:select list="listaTiposPersona" name="tiposPersona" 
									cssClass="cleaneable txtfield obligatorio"
									id="listaTiposPersona"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
									onchange = "facturarATipoPersona('CRE');">
									</s:select>
								</td>
								<td>
								 	&nbsp;
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="1">	
									<label for="${idNombrePersona}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.nombre')}"/>
 									</label>
 								</td>
								<td colspan="3">	
									<s:textfield id="idNombrePersona" name="" cssStyle="float: left; width: 100%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
								<td colspan="1">	
									<label for="${idRFC}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.rfc')}"/>
 									</label>
 								</td>
								<td colspan="1">	
									<s:textfield id="idRFC" name="" cssStyle="float: left; width: 100%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
							<tr>
								<td colspan="1">	
									<label for="${idCalle}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.calle')}"/>
 									</label>
 								</td>
								<td colspan="5">
									<s:textfield id="idCalle" name="" cssStyle="float: left; width: 100%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
							<tr>
								<td colspan="6">	
								<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccionSiniestroMidas" ignoreContextParams="true" executeResult="true" >
									<s:param name="idEstadoName">idEstadoName</s:param>
									<s:param name="idCiudadName">idCiudadName</s:param>
									<s:param name="idColoniaName">idColoniaName</s:param>
									<s:param name="cpName">cpName</s:param>
									<s:param name="cp"></s:param>
									<s:param name="labelPais">País</s:param>
									<s:param name="labelEstado">Estado</s:param>
									<s:param name="labelCiudad">Municipio</s:param>
									<s:param name="labelColonia">Colonia</s:param>
									<s:param name="labelCalle">Calle y Número</s:param>
									<s:param name="labelNumero">Número Ext.</s:param>
									<s:param name="labelNumeroInt">Número Int.</s:param>
									<s:param name="labelCodigoPostal">Código Postal</s:param>
									<s:param name="labelReferencia">Referencia</s:param>
									<s:param name="labelPosicion">left</s:param>
									<s:param name="componente">6</s:param>
									<s:param name="readOnly" value="false"></s:param>
									<s:param name="requerido" value="0"></s:param>
									<s:param name="incluirReferencia" value="false"></s:param>
									<s:param name="enableSearchButton" value="false"></s:param>
	                             	</s:action>
								</td>
							</tr>
							<tr>
								<td>	
									<label for="${idTelefono}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.telefono')}"/>
 									</label>
 								</td>
								<td>
									<s:textfield id="idTelefono" name="" cssStyle="float: left;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
								<td>	
									<label for="${idEmail}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.email')}"/>
 									</label>
 								</td>
								<td>	
									<s:textfield id="idEmail" name="" cssStyle="float: left;width:160%" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
						</table>
						<div class="titulo">
							<s:text name="midas.siniestros.recuperacion.ingresos.facturar.datos.tituloDatosImportes"/>	
						</div>
						<table class="contenedorConFormato">
							<tr>
								<td>	
									<label for="${idImporte}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.importe')}"/>
 									</label>
 								</td>
								<td>
									<s:textfield id="idImporte" readonly="true" name="emisionFactura.nombreRazonSocial" cssStyle="float: left; background-color:#EEEEEE" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
								<td>	
									<label for="${idIVA}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.iva')}"/>
 									</label>
 								</td>
								<td>	
									<s:textfield id="idIVA" readonly="true" name="" cssStyle="float: left; background-color:#EEEEEE" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
								<td>	
									<label for="${idTotal}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.total')}"/>
 									</label>
 								</td>
								<td>	
									<s:textfield id="idTotal" readonly="true" name="" cssStyle="float: left; background-color:#EEEEEE" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
							<tr>
								<td colspan="1">	
									<label for="${idMetodoPago}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.metodopago')}"/>
 									</label>
 								</td>
								<td colspan="3">
									<s:select list="listaMetodosPago" name="metodoPagoId" 
									cssClass="cleaneable txtfield obligatorio requerido"
									id="listaMetodosPago"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
									onchange = "facturarPorMetodoPago();">
									</s:select>
								</td>
								
								<td colspan="1">	
									<label for="${idConcepto}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.concepto')}"/>
 									</label>
 								</td>
								<td colspan="1">	
									<s:textfield id="idConcepto" readonly="true" style="background-color:#EEEEEE" name="" cssStyle="float: left;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>	
									<label id="idCuentaLabel" for="${idCuenta}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.cuenta')}"/>
 									</label>
 								</td>
								<td>	
									<s:textfield id="idCuenta" name="" cssStyle="float: left;" cssClass="cleaneable txtfield obligatorio jQalphaextra requerido"></s:textfield>
								</td>
							</tr>
							<tr colspan="6">
								<td colspan="1">	
									<label for="${idDescripcion}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.descripcion')}"/>
 									</label>
 								</td>
								<td colspan="5">	
									<s:textfield id="idDescripcion" readonly="true" name="" cssStyle="float: left; background-color:#EEEEEE; width: 100%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
								</td>
							</tr>
							<tr>
								<td colspan="1">	
									<label for="${idObservaciones}">
 										<s:text name="%{getText('midas.siniestros.recuperacion.ingresos.facturar.datos.observaciones')}"/>
 									</label>
 								</td>
								<td colspan="5">	
									<s:textfield id="idObservaciones" name="" cssStyle="float: left; width: 100%;" cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w420 requerido"></s:textfield>
								</td>
							</tr>
						</table>
						
						<div id="idTipoSalvamento" style="display:none;" > 
							<div class="titulo">
								<s:text name="midas.siniestros.emision.facturas.detalle.factura.ver.titulo3"/>	
							</div>
							<table class="contenedorConFormato"  width="100%" >
								<tr>
									<td>
										<s:label name=""
											id="svmMarca"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.marca')}" 
											labelposition="top"/>
									</td>
									<td>
										<s:label name=""
											id="svmTipo"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.tipo')}" 
											labelposition="top"/>
									</td>
									<td>
										<s:label name=""
											id="svmModelo"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.modelo')}" 
											labelposition="top"/>
									</td>
									<td>
										<s:label name=""
											id="svmSerie"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.no.serie')}" 
											labelposition="top"/>
									</td>
									<td>
										<s:label name=""
											id="svmMotor"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.no.motor')}" 
											labelposition="top"/>
									</td>
									<td>
										<s:label name=""
											id="svmColor"
											label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.color')}" 
											labelposition="top"/>
									</td>
								</tr>
							</table>
						</div>
						
						<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
						<td>
							<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
								<a href="javascript: void(0);" onclick="cerrar();"> 
								<s:text name="midas.boton.cerrar" /></a>
							</div>	
							<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar" >
								<a href="javascript: void(0);" onClick="guardarInformacionFacturacion()" class="icon_guardar2"> 
								<s:text name="midas.boton.guardar" /></a>	
							</div> 	
							<div class="btn_back w210" style="display: inline; float: right;" id="bGenerarFactura" >
								<a href="javascript: void(0);" onClick="generarFacturaElectronica()" class=""> 
								<s:text name="midas.siniestros.recuperacion.ingresos.facturar.generarFactura" /></a>	
							</div> 		
						</td>							
					</tr>
				</table>		
					</td>
				</tr>
			</table>
		</div>	
	</form>	
</div>


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		initEmisionFacturacion();
		obtenerIngresosFacturables();
		facturarPorMetodoPago();
		jQuery("#cpName").val("");
	});
</script>
