<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaMovsManuales</param>
				<param>true</param>
				<param>infoAreaMovsManuales</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="selected" 				type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="cuentaContable"		 	type="ro" width="140" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.movsmanuales.cuentacontable'}"/></column>
        <column id="causaMovimiento"	 	type="ro" width="290" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.movsmanuales.causamovimiento'}"/> </column>
		<column id="usuario"			  	type="ro" width="225" sort="str"><s:text name="%{'midas.siniestros.recuperacion.movsmanuales.alta.usuario'}"/> </column>
		<column id="fechaTraspaso"	    	type="ro" width="180" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.movsmanuales.fechatraspaso'}"/> </column>
		<column id="importe"      			type="ron" width="180" format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.recuperacion.movsmanuales.importe'}"/> </column>
		<s:if test="  soloConsulta !=true  ">
			 <m:tienePermiso nombre="FN_M2_SN_Movimientos_Manuales_Gerente">
				<column  type="img"   width="30" sort="na" align="center" ></column>
			</m:tienePermiso>
		 </s:if>
		
		
		
		
		
	  	</head>
	  	
		<s:iterator value="listaMovManuales">
			<row id="<s:property value="idMovimiento"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="cuentaContable" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="causaMovimiento" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="usuario" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaTraspasoCuenta" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="importe" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="  soloConsulta !=true  ">
			 		<m:tienePermiso nombre="FN_M2_SN_Movimientos_Manuales_Gerente">
						<cell>../img/icons/ico_eliminar.gif^Eliminar Movimiento^javascript:eliminarMovimientoManual(<s:property value="idMovimiento" escapeHtml="false" escapeXml="true"/>)^_self</cell>
					</m:tienePermiso>
				</s:if>
			</row>
		</s:iterator>
	
</rows>