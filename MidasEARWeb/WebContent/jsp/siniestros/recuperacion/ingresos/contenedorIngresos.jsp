<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/ingresos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script	src="<s:url value='/js/validaciones.js'/>"></script>

<style type="text/css">
	#superior{
		height: 240px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">

	<form id="buscarIngresosPendientesForm">  
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.ingresospendientes.tituloDatosGral"/>	
	</div>	
	
		<div id="superior" class="divContenedorO" style="height: 60px">
			<s:hidden id="ingresosConcat" name="filtroIngresosPendientes.ingresosConcat" />
			<s:hidden id="variacion" name="variacion" />
			<s:hidden id="soloConsulta" name="soloConsulta" />
			<s:hidden id="consultaSinFiltro" name="consultaSinFiltro" />
			<s:hidden id="ingresoId" name="ingresoId" />
			<s:hidden id="idToReporte" name="idToReporte" />
			<s:hidden id="tipoCancelacion" name="tipoCancelacion" />
				
				<div id="superiorI">
					<div id="SIS">
						<div class="floatLeft" style="width: 26%;">
								<s:textfield id="fechaRegistro" name="ingresosPendDto.fechaRegistro"   
												cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.fechaRP')}"
												onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
												 labelposition="left" cssStyle="width:37%;" readonly="true" ></s:textfield>
						</div>
						<div class="floatLeft" style="width: 25%;">
								<s:textfield id="fechaAplicacion" name="ingresosPendDto.fechaIngreso"   
												cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.fechaApIngreso')}"
												onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
												 labelposition="left" cssStyle="width:37%;" readonly="true" ></s:textfield>
						</div>
						<div class="floatLeft" style="width: 25%;">
								<s:textfield id="fechaCancelacion" name="ingresosPendDto.fechaCancelacion"   
												cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.fechaCancelaIngreso')}"
												onblur="esFechaValida(this);" onkeypress="return soloFecha(this, event, false);"
												 labelposition="left" cssStyle="width:37%;" readonly="true" ></s:textfield>
						</div>
						<div class="floatLeft" style="width: 23%;">
							<s:textfield id="estatus" name="ingresosPendDto.estatusDesc"   cssClass="txtfield"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.estatusIngreso')}" labelposition="left" cssStyle="width:75%;" readonly="true" ></s:textfield>
						</div>
									
					</div>
				</div>
		</div>
<div id="divIngresos">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.recuperacion.ingresospendientes.titulobusquedaingresos"/>	
</div>	
	<div id="superior" class="divContenedorO">
			<div id="superiorI">
				<div id="SIS">
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaTipoRecuperacion" cssStyle="width: 90%;"
							name="filtroIngresosPendientes.tipoRecuperacion" 
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.tiporecuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							id="listaTipoRecuperacion"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaMedioRecuperacion" cssStyle="width: 90%;"
							name="filtroIngresosPendientes.medioRecuperacion" 
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.mediorecuperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaOficinas" cssStyle="width: 90%;"
							name="filtroIngresosPendientes.oficinaId" 
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.oficina')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 3%; margin-top:2%;">
							<s:checkbox id="habilitarIngresosIdentificados" onClick="validaCheckReferencias(this);onClickHabilitarField(this,jQuery('#referenciasIdentificadas'),'');" name="ingresosIndentificadosCheck" cssClass="tipoServicio" fieldValue="0" value="0" />
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaReferenciasIdentif" cssStyle="width: 90%;"
							name="filtroIngresosPendientes.referenciaIdentificada" 
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.ingresosidentificados')}"
							cssClass="cleaneable txtfield obligatorio"
							id="referenciasIdentificadas" disabled="true"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
				</div>
				
				<div id="SIS">
					<div class="floatLeft" style="width: 14%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerodeudor')}" name="filtroIngresosPendientes.claveDeudor"
								cssStyle="float: left;width: 50%;" maxlength="30" cssClass="cleaneable txtfield obligatorio jQnumeric jQrestrict"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresospendientes.nombredeudor')}" name="filtroIngresosPendientes.nombreDeudor" 		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerosiniestro')}" name="filtroIngresosPendientes.numeroSiniestro" 
								onBlur="validaFormatoSiniestro(this);"		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numeroreporte')}" name="filtroIngresosPendientes.numeroReporte" 
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
					</div>
				</div>
								
				<div id="SIS">
					
					<div class="floatLeft" style="width: 14%;padding-top: 1%;">
						<s:text name="midas.siniestros.recuperacion.ingresospendientes.montorecuperado"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;" >
						<s:textfield id="montofinaldesde"
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.desde')}"
							name="filtroIngresosPendientes.montoIniIngreso"
							onkeyup="mascaraDecimales('#montofinaldesde',this.value);"
							onblur="mascaraDecimales('#montofinaldesde',this.value);" maxlength="10"
							disabled="false" cssStyle="float: left;width: 70%;"
							cssClass="cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 12%;" >
						<s:textfield id="montofinalhasta"
							label="%{getText('midas.siniestros.recuperacion.ingresospendientes.hasta')}"
							name="filtroIngresosPendientes.montoFinIngreso"
							onkeyup="mascaraDecimales('#montofinalhasta',this.value);"
							onblur="mascaraDecimales('#montofinalhasta',this.value);" maxlength="10"
							disabled="false" cssStyle="float: left;width: 70%;"
							cssClass="cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 14%;padding-top: 1%;">
						<s:text name="midas.siniestros.recuperacion.ingresospendientes.fecharegistropendiente"></s:text>
					</div>
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresospendientes.desde')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroIngresosPendientes.fechaIniPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresospendientes.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroIngresosPendientes.fechaFinPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
				</div>
				
				<div id="SIS">		
					&nbsp;
				</div>
				
				<div id="SIS">
					
					<div id="btnBuscar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="buscarIngresosPendientes();"> <s:text name="Buscar" /> </a>
					</div>
					
					<div id="btnLimpiar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarFormularioIngresosPendientes();"> <s:text name="Limpiar" /> </a>
					</div>

				</div>
				
			</div>
		
	</div>
</div>	
	<br />

	<div id="tituloListado" class="titulo" style="width: 98%;display:none;">
		<s:text name="midas.siniestros.recuperacion.ingresospendientes.titulolistado"/>	
	</div>	

	<div id="indicadorIngresosPendientes"></div>
	<div id="listadoIngresosPendientesGrid" style="width: 98%; height: 380px;display:none;"></div>
	<div id="pagingAreaIngresosPendientes"></div>
	<div id="infoAreaIngresosPendientes"></div>

	<div id="SIS">
		<div id="divExcelBtn" class="w150"
			style="float: right; display: none;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="exportarExcel();"> <s:text
						name="midas.boton.exportarExcel" /> </a>
			</div>
		</div>
	</div>

	</form>
	
<!-- BUSQUEDA SOLO PARA CANCELACION -->
<s:if test="tipoDeCancelacion == 3 ">
<form id="buscarIngresosPorCancelarReversaForm">
	<s:hidden id="ingresosConcatReversaCancelarTemporalGrid" />
	<s:hidden id="ingresosConcatReversaCancelar" />
	<s:hidden id="variacion" name="variacion" />
	<s:hidden id="soloConsulta" name="soloConsulta" />
	<s:hidden id="ingresoId" name="ingresoId" />
	<s:hidden name="filtroIngresosPendientes.tipoRecuperacion" />
	
	<div class="titulo" style="width: 96%;">
		<s:text name="midas.siniestros.recuperacion.ingresospendientes.titulobusquedaingresos"/>	
	</div>

			<div id="superior" class="divContenedorO">
				<div id="superiorI">
					<div id="SIS">
						<div class="floatLeft" style="width: 18%;">
							<s:select list="listaMedioRecuperacion" cssStyle="width: 90%;"
								name="filtroIngresosPendientes.medioRecuperacion"
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.mediorecuperacion')}"
								cssClass="filtroReversa cleaneable txtfield obligatorio" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}">
							</s:select>
						</div>
						<div class="floatLeft" style="width: 18%;">
							<s:select list="listaOficinas" cssStyle="width: 90%;"
								name="filtroIngresosPendientes.oficinaId"
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.oficina')}"
								cssClass="filtroReversa cleaneable txtfield obligatorio" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}">
							</s:select>
						</div>
						<div class="floatLeft" style="width: 3%; margin-top: 2%;">
							<s:checkbox id="habilitarIngresosIdentificados"
								onClick="javascript:validaCheckReferencias(this);"
								name="ingresosIndentificadosCheck" cssClass="filtroReversa tipoServicio"
								fieldValue="0" value="0" />
						</div>
						<div class="floatLeft" style="width: 18%;">
							<s:select list="listaReferenciasIdentif" cssStyle="width: 90%;"
								name="filtroIngresosPendientes.referenciaIdentificada"
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.ingresosidentificados')}"
								cssClass="filtroReversa cleaneable txtfield obligatorio"
								id="referenciasIdentificadas" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}">
							</s:select>
						</div>
					</div>

					<div id="SIS">
						<div class="floatLeft" style="width: 14%;">
							<s:textfield
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerodeudor')}"
								name="filtroIngresosPendientes.claveDeudor"
								cssStyle="float: left;width: 50%;" maxlength="30"
								cssClass="filtroReversa cleaneable txtfield obligatorio jQnumeric jQrestrict"></s:textfield>
						</div>

						<div class="floatLeft" style="width: 15%;">
							<s:textfield
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.nombredeudor')}"
								name="filtroIngresosPendientes.nombreDeudor"
								cssStyle="float: left;width: 90%;"
								cssClass=" filtroReversa cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
						</div>

						<div class="floatLeft" style="width: 15%;">
							<s:textfield
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerosiniestro')}"
								name="filtroIngresosPendientes.numeroSiniestro"
								onBlur="validaFormatoSiniestro(this);"
								cssStyle="float: left;width: 90%;"
								cssClass=" filtroReversa cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
						</div>

						<div class="floatLeft" style="width: 15%;">
							<s:textfield
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numeroreporte')}"
								name="filtroIngresosPendientes.numeroReporte"
								cssStyle="float: left;width: 90%;"
								cssClass="filtroReversa cleaneable txtfield obligatorio jQalphaextra"></s:textfield>
						</div>
					</div>

					<div id="SIS">

						<div class="floatLeft" style="width: 14%; padding-top: 1%;">
							<s:text
								name="midas.siniestros.recuperacion.ingresospendientes.montorecuperado"></s:text>
						</div>

						<div class="floatLeft" style="width: 12%;">
							<s:textfield id="montofinaldesde"
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.desde')}"
								name="filtroIngresosPendientes.montoIniIngreso"
								onkeyup="mascaraDecimales('#montofinaldesde',this.value);"
								onblur="mascaraDecimales('#montofinaldesde',this.value);"
								maxlength="10" disabled="false"
								cssStyle="float: left;width: 70%;"
								cssClass="filtroReversa cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
						</div>

						<div class="floatLeft" style="width: 12%;">
							<s:textfield id="montofinalhasta"
								label="%{getText('midas.siniestros.recuperacion.ingresospendientes.hasta')}"
								name="filtroIngresosPendientes.montoFinIngreso"
								onkeyup="mascaraDecimales('#montofinalhasta',this.value);"
								onblur="mascaraDecimales('#montofinalhasta',this.value);"
								maxlength="10" disabled="false"
								cssStyle="float: left;width: 70%;"
								cssClass="filtroReversa cleaneable obligatorio txtfield jQrestrict jQ2float formatCurrency"></s:textfield>
						</div>

						<div class="floatLeft" style="width: 14%; padding-top: 1%;">
							<s:text
								name="midas.siniestros.recuperacion.ingresospendientes.fecharegistropendiente"></s:text>
						</div>

						<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresospendientes.desde')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroIngresosPendientes.fechaIniPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroDe" maxlength="10"
									size="12" onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="filtroReversa cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
						</div>

						<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.recuperacion.ingresospendientes.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroIngresosPendientes.fechaFinPendiente"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaRegistroHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="filtroReversa cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
						</div>

					</div>

					<div id="SIS">&nbsp;</div>

					<div id="SIS">

						<div id="btnBuscar" class="btn_back w120"
							style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
							<a href="javascript: void(0);"
								onclick="buscarIngresosPendientesPorCancelar();"> <s:text name="Buscar" />
							</a>
						</div>

						<div id="btnLimpiar" class="btn_back w120"
							style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
							<a href="javascript: void(0);"
								onclick="limpiarFormularioIngresosPendientesPorCancelar();"> <s:text
									name="Limpiar" /> </a>
						</div>

					</div>

				</div>

			</div>
			
		<div id="indicador"></div>
		<div id="listadoIngresosReversaAplicarGrid" style="width:98%;height:340px;"></div>
		<div id="pagingArea"></div>
		<div id="infoArea"></div>			
		
		</form>
</s:if>	
<!-- FIN BUSQUEDA SOLO CANCELACION -->	
	
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.ingresospendientes.titulodepositos"/>	
	</div>	

	<div id="contenido_Pestañas" style="width:99%;position: relative;">
		<div id="divInferior" style="width: 99% !important;" class="floatLeft">
			<div hrefmode="ajax-html" style="height: 400px; width: 100%;" id="depositosIngresosTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
				<div width="200px" id="depositos" name="<s:text name='midas.siniestros.recuperacion.ingresospendientes.titulodepositosbancarios' />" 
					href="http://void" extraAction="javascript: verTabDepositosBancarios();">
				</div>
				<div width="200px" id="movimientosAcreedores" name="Movimientos Acreedores" href="http://void" extraAction="javascript: verTabMovimientosAcreedores();">
				</div>
	    		<div width="200px" id="movimientosManuales" name="Movimientos Manuales" href="http://void" extraAction="javascript: verTabMovimientosManuales();">
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.recuperacion.ingresospendientes.tituloTotal"/>	
		</div>	
	
		<div id="superior" class="divContenedorO" <s:if test="tipoDeCancelacion == 0" > style="height: 70px" </s:if> >
			<s:hidden id="ingresosConcat" name="filtroIngresosPendientes.ingresosConcat" />
				<div id="superiorI">
					<div id="SIS">
						<div class="floatLeft" style="width: 30%;">
							<s:textfield id="totalesIngresosAplicar" name="importesDTO.totalesIngresosAplicar"   cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.totales.ingresoAplicar')}" labelposition="left" cssStyle="width:39%;" readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft" style="width: 30%;">
							<s:textfield id="totalesDepositosBancarios" name="importesDTO.totalesDepositosBancarios"   cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.totales.depositoBanc')}" labelposition="left" cssStyle="width:35%;" readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft" style="width: 30%;">
							<s:textfield id="diferencia" name="importesDTO.diferencia"   cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.totales.diferencia')}" labelposition="left" cssStyle="width:30%;" readonly="true" ></s:textfield>
						</div>
									
					</div>
					<div id="SIS">
						<div class="floatLeft" style="width: 30%;">
							<s:textfield id="totalesMovimientosAcreedores" name="importesDTO.totalesMovimientosAcreedores"   cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.totales.totalesMovAcre')}" labelposition="left" cssStyle="width:30%;" readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft" style="width: 30%;">
							<s:textfield id="totalesMovimientosManuales" name="importesDTO.totalesMovimientosManuales"   cssClass="txtfield formatCurrency"  label="%{getText('midas.siniestros.recuperacion.ingresospendientes.totales.manual')}" labelposition="left" cssStyle="width:30%;" readonly="true" ></s:textfield>
						</div>
									
					</div>
					
					<!-- CANCELACION OPCIONES-->
					<s:if test="tipoDeCancelacion > 0 ">
						<div id="SIS" >
							<div class="floatLeft" style="width: 30%;">
								<s:select list="listaMotivoCancelacion" cssStyle="width: 90%;"
									name="motivoCancelacion" 
									label="%{getText('midas.siniestros.recuperacion.ingresospendientes.motivo.cancelacion')}"
									cssClass="cleaneable txtfield "
									id="motivosCancelacion"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							<div class="floatLeft" style="width: 25%;">
								<s:select list="listaCuentaAcredora" cssStyle="width: 90%;"
									name="cuentaAcredora" 
									label="%{getText('midas.siniestros.recuperacion.ingresospendientes.envio.cuenta.acreedora')}"
									cssClass="cleaneable txtfield "
									id="cuentaAcredora"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							<div class="floatLeft" style="width: 25%; margin-top: 2%;  ">
								<s:radio
									list="opcionesCancelarIngreso"
									labelposition="right" id="todasRecuperaciones"
									name="todasRecuperaciones" value="2" />
							</div>

							<s:if test="tipoDeCancelacion == 1 ">
								<div class="floatLeft btnCancelarIngresoAplicado" style="width: 14%; margin-top:2.2%;">
									<div class="w150" style="float: right;">
										<div class="btn_back w140"
											style="display: inline; float: right;">
											<a href="javascript: void(0);"
												onclick="aplicarCancelacionPorDevolucion();"> <s:text
													name="midas.siniestros.recuperacion.ingresospendientes.aplicar.cancelacion" />
											</a>
										</div>
									</div>
								</div>
							</s:if>
						</div>
						
						<div id="SIS" >
							<div class="floatLeft" style="width: 30%;">
								<s:textarea 
									label="%{getText('midas.siniestros.recuperacion.ingresospendientes.cancelacion.comentario')}"
									name="comentarios"
									id = "comentarios" 
									cols="150" 
									rows="3"/>
							</div>
						</div>
						
					</s:if>					
					<!-- FIN CANCELACION OPCIONES -->
				</div>
			</div>
			
			
		<!-- CANCELACION INGRESO APLICADO -->
		<s:if test="tipoDeCancelacion == 2 || tipoDeCancelacion == 3 ">
		<br/>
			<div class="titulo" style="width: 98%;">
				<s:text name="midas.siniestros.recuperacion.ingresospendientes.opciones.cancelacion"/>	
			</div>
		
			<div id="superior" class="divContenedorO" style="height: 70px">
				<div id="superiorI">
					<div id="SIS">
		
						<div class="floatLeft" style="width: 30%;">
							<s:radio cssClass="txtfield tipoCancelacionIngresoAplicado" cssStyle="font-size:9.33333px"
								name="tipoCancelacion"
								list="#{'1':'Regresar el Ingreso a Pendiente por Aplicar'}" />
						</div>
		
						<div class="floatLeft" style="width: 30%;">
							<s:radio cssClass="txtfield tipoCancelacionIngresoAplicado" name="tipoCancelacion"
								list="#{'2':'Cancelar el Ingreso Aplicado'}" />
						</div>
		
						<div class="floatLeft btnCancelarIngresoAplicado" style="width: 30%;">
							<div  class="w150" style="float: right;">
								<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" <s:if test="tipoDeCancelacion == 2"> onclick="aplicarCancelacionCuentaAcreedora();" </s:if><s:elseif test="tipoDeCancelacion == 3" > onclick="aplicarCancelacionReversa();" </s:elseif> > 
											<s:text name="midas.siniestros.recuperacion.ingresospendientes.aplicar.cancelacion" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</s:if>			
			
		<div id="SIS">
			<div id="btnCerrar" class="w150"
				style="float: right;">
				<div class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="cerrarDetalle();"> <s:text
							name="Cerrar" /> </a>
				</div>
			</div>
			
			<div id="btnAplicar" class="w150"
				style="float: right;">
				<div class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="aplicarIngresos();"> <s:text
							name="Aplicar" /> </a>
				</div>
			</div>
			<div id="btnHistorico" class="w150"
					style="float: right;">
					<div class="btn_back w210" style="display: inline; float: right;">
						<a href="javascript: void(0);"
							onclick="consultarHistorico();"> <s:text
								name="Consultar Histórico de Movimientos" /> </a>
					</div>
			</div>
			
		</div>

</div>

<!-- USADO SOLO PARA CANCELACION -->
<s:hidden id="tipoDeCancelacion" name="tipoDeCancelacion" />
<s:hidden id="origenRecuperacionDeducible" name="origenRecuperacionDeducible" />

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		iniDetaingreso();
		initIngresoCancelacion();
		validaOrigenDeducibleRecuperacionIngreso();
	});
</script>
