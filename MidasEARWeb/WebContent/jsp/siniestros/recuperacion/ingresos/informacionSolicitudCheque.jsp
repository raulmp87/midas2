<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:form name="informacionSolicitudCheque" id="informacionSolicitudCheque" method="post">
<s:hidden name="ingresoDevolucion.id" id="id"></s:hidden>
<s:hidden name="ingresoDevolucion.subtotalRec" id="h_subtotal"></s:hidden>
<s:hidden name="ingresoDevolucion.ivaRec" id="h_iva"></s:hidden>
<s:hidden name="ingresoDevolucion.ivaRetenidoRec" id="h_ivaRetenido"></s:hidden>
<s:hidden name="ingresoDevolucion.isrRec" id="h_isr"></s:hidden>
<s:hidden name="ingresoDevolucion.importeTotalRec" id="h_importeTotal"></s:hidden>

<table width="80%" align="center">
<tr>
<td>
	<div class="titulo">
		<s:text name="midas.siniestros.recuperacion.ingresodevolucion.solicitudchequetitulo" />
	</div>
	<table width="99%" class="contenedorConFormato">
		<tr>
			<td>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.tipodevolucion" />:
	       		<s:radio list="tipoDevolucionList" value="%{ingresoDevolucion.tipoDevolucion}" 
	       				 onclick="" 
	       				 id="tipoDevolucion" 
	       				 name="ingresoDevolucion.tipoDevolucion"
	       				 cssClass="jQrequired " 
	       				 disabled="%{consulta}"
	       				 theme="simple"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.solicitadopor" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w200"
								name="ingresoDevolucion.usuarioSolicita"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="true"	
								id="usuarioSolicita" 
								disabled="true"
								theme="simple"/>
			</td>
			<td  align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechasolicitud" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w100"
								name="ingresoDevolucion.fechaSolicita"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="true"	
								id="fechaSolicita" 
								disabled="true"
								theme="simple"/>
			</td>			
		</tr>
		<tr>
			<td>
	           	<input id="formaPago_hidden"  type="hidden"/>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.formapago" />:
	       		<s:radio list="formaPagoList" value="%{ingresoDevolucion.formaPago}" 
	       				 onclick="" 
	       				 onchange="onChangeFormaPago()"
	       				 id="formaPago" 
	       				 name="ingresoDevolucion.formaPago"
	       				 cssClass="jQrequired " 
	       				 disabled="%{consulta}"
	       				 theme="simple"/>
			</td>
			<td align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.nosolicitudpago" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w100"
								name="ingresoDevolucion.solicitudCheque.id"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="%{consulta}"	
								id="solicitudId" 
								disabled="true"
								theme="simple"/>
			</td>				
		</tr>
		
		<tr>
			<td>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.autorizadopor" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w200"
								name="ingresoDevolucion.usuarioAutorizaRechaza"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="true"	
								id="usuarioAutorizaRechaza" 
								disabled="true"
								theme="simple"/>
			</td>
			<td  align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechaautorizacion" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w100"
								name="ingresoDevolucion.fechaAutorizaRechaza"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="%{consulta}"	
								id="fechaAutorizaRechaza" 
								disabled="true"
								theme="simple"/>
			</td>			
		</tr>		
		
		<tr>
			<td>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.beneficiario" />:
				<s:textfield 	cssClass="txtfield jQrestrict jQrequired cajaTextoM2 w200 requerido"
								name="ingresoDevolucion.beneficiario"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="%{consulta}"	
								id="beneficiario" 
								disabled="%{consulta}"
								theme="simple"/>
			</td>
			<td  align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechacheque" />:
				<s:textfield 	cssClass="txtfield cajaTextoM2 w100"
								name="ingresoDevolucion.fechaChequeReferencia"
								labelposition="left"
								size="20"	
								readonly="%{consulta}"	
								id="fechaChequeReferencia" 
								disabled="true"
								theme="simple"/>
			</td>				
		</tr>		

		<tr>
			<td>
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.telefono" />:
				<s:textfield 	cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w30 requerido"
								name="ingresoDevolucion.ladaTelefono"
								labelposition="left"
								size="10"
								maxLength="3"
								readonly="%{consulta}"	
								id="ladaTelefono" 
								disabled="%{consulta}"
								theme="simple"/>
				<s:textfield 	cssClass="txtfield jQphone jQrestrict cajaTextoM2 w100 requerido jQrequired"
								name="ingresoDevolucion.telefono"
								labelposition="left"
								size="20"	
								maxLength="8"
								readonly="%{consulta}"	
								id="telefono" 
								disabled="%{consulta}"
								theme="simple"/>	
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.correo" />:
				<s:textfield 	cssClass="txtfield jQemail jQrestrict cajaTextoM2 w200 requerido jQrequired"
								name="ingresoDevolucion.correo"
								labelposition="left"
								size="30"	
								maxLength="100"
								readonly="%{consulta}"	
								id="correo" 
								disabled="%{consulta}"
								theme="simple"/>															
			</td>
			<td  align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.nochequeref" />:
				<s:textfield 	cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w100"
								name="ingresoDevolucion.chequeReferencia"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="%{consulta}"	
								id="chequeReferencia" 
								disabled="true"
								theme="simple"/>
			</td>			
		</tr>				
				
		<tr>
			<td>
			<s:text name="midas.siniestros.recuperacion.ingresodevolucion.banco" />:
				<s:select 
					list="bancoList" 
					headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}" 					
					name="ingresoDevolucion.banco"
					disabled="%{consulta}" 
					id="banco" 
					cssClass="jQrequired cajaTexto w150" 
					onchange="" theme="simple"/>
			<s:text name="midas.siniestros.recuperacion.ingresodevolucion.clabe" />:
				<s:textfield 	cssClass="txtfield jQClabe jQrestrict cajaTextoM2 w200 requerido "
								name="ingresoDevolucion.clabe"
								labelposition="left"
								size="20"	
								maxLength="18"
								readonly="%{consulta}"	
								id="clabe" 
								disabled="%{consulta}"
								theme="simple"/>					
			</td>
			<td  align="right">
				<s:text name="midas.siniestros.recuperacion.ingresodevolucion.importe" />:
				<s:textfield 	cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w100 formatCurrency"
								value="%{getText('struts.money.format',{ingresoDevolucion.importeTotal})}"
								labelposition="left"
								size="20"	
								maxLength="82"
								readonly="true"	
								id="importeCheque" 
								disabled="true"
								theme="simple">
							</s:textfield>
			</td>			
		</tr>
		<tr>
			<td colspan="4">
				<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.observaciones" />:
				<s:textarea cols="50" 
							maxlength="2500" 
							name="ingresoDevolucion.observaciones"
							labelposition="left" 
							rows="2" 
							readonly="%{consulta}"
							disabled="%{consulta}"
							cssClass="textarea w840 h80"
							theme="simple"
							id="observaciones" />
			</td>
		</tr>
	</table>
</td>
</tr>	
</table>
	<table width="80%" align="center">
		<tr>
			<td width="50%">
				<table width="99%" class="contenedorConFormato"   height="160">
					<tr>
						<td>
						<div class="subtituloIzquierdaDiv" align="center">
						<s:text name="midas.siniestros.recuperacion.ingresodevolucion.montosingreso" />
						</div>
						</td>
						<td>
						</td>						
						<td>
						<div class="subtituloIzquierdaDiv"  align="center">
						<s:text name="midas.siniestros.recuperacion.ingresodevolucion.importeadevolver" />
						</div>
						<td>
					</tr>
					<tr>
						<td align="center">
							<s:textfield 	cssClass="txtfield cajaTextoM2 w100" 
											value="%{getText('struts.money.format',{ingresoDevolucion.subtotalRec})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="subtotal" 
											disabled="true"
											theme="simple"/>						
						</td>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.subtotal" />						
						</td>
						<td  align="center">
							<s:textfield 	cssClass="txtfield cajaTextoM2 w100 formatCurrency"
											name="ingresoDevolucion.subtotal"
											onchange="onChangeSubtotal(this);"
											labelposition="left"
											size="20"	
											readonly="%{consulta}"	
											id="subtotalNew" 
											disabled="%{consulta}"
											theme="simple"/>							
						</td>						
					</tr>
					<tr>
						<td align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100"
											value="%{getText('struts.money.format',{ingresoDevolucion.ivaRec})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="iva" 
											disabled="true"
											theme="simple"/>						
						</td>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.iva" />						
						</td>
						<td  align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100 formatCurrency"
											value="%{getText('struts.money.format',{ingresoDevolucion.iva})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="ivaNew" 
											disabled="true"
											theme="simple"/>							
						</td>					</tr>
					<tr>
						<td align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100"
											value="%{getText('struts.money.format',{ingresoDevolucion.ivaRetenidoRec})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="ivaRetenido" 
											disabled="true"
											theme="simple"/>						
						</td>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.ivaret" />						
						</td>
						<td  align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100 formatCurrency"
											value="%{getText('struts.money.format',{ingresoDevolucion.ivaRetenido})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="ivaRetenidoNew" 
											disabled="true"
											theme="simple"/>							
						</td>					
					</tr>
					<tr>
						<td align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100"
											value="%{getText('struts.money.format',{ingresoDevolucion.isrRec})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="isr" 
											disabled="true"
											theme="simple"/>						
						</td>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.isr" />						
						</td>
						<td  align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100 formatCurrency"
											value="%{getText('struts.money.format',{ingresoDevolucion.isr})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="isrNew" 
											disabled="true"
											theme="simple"/>							
						</td>					
					</tr>
					<tr>
						<td align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100"
											value="%{getText('struts.money.format',{ingresoDevolucion.importeTotalRec})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="importeTotal" 
											disabled="true"
											theme="simple"/>						
						</td>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.importe" />						
						</td>
						<td  align="center">
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100 formatCurrency"
											value="%{getText('struts.money.format',{ingresoDevolucion.importeTotal})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="importeTotalNew" 
											disabled="true"
											theme="simple"/>							
						</td>					
					</tr>
				</table>
			</td>
			<td>
				<table width="99%" class="contenedorConFormato"  height="160">
					<tr>
						<td>
						<div class="subtituloIzquierdaDiv">
						<s:text name="midas.siniestros.recuperacion.ingresodevolucion.desglosemontorestante" />
						</div>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="center">
							<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.montorestante" />						
							<s:textfield 	cssClass="txtfield jQmoney cajaTextoM2 w100 formatCurrency"
											value="%{getText('struts.money.format',{ingresoDevolucion.montoRestante})}"
											labelposition="left"
											size="20"	
											readonly="true"	
											id="montoRestante" 
											disabled="true"
											theme="simple">
							</s:textfield>							
						</td>
					</tr>
					<tr>
						<td align="right" style="padding-right: 80px"><s:text name="midas.siniestros.recuperacion.ingresodevolucion.cuentaacreedora" />:
							<s:select 	list="cuentaAcreedoraList"
										id="cuentaAcreedoraList"
										name="ingresoDevolucion.cuentaAcreedoraRestante.id"
										headerValue="%{getText('midas.general.seleccione')}" 
										headerKey="" 
										theme="simple"
										disabled="%{consulta}"
										cssClass="cajaTextoM2 w150" />
						</td>							
					</tr>
					<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
				</table>
			</td>
		</tr>
	</table>
</s:form>