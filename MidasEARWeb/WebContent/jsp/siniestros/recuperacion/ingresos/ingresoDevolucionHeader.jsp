<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/ingresoDevolucion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/SiniestrosUtil.js'/>"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/struts/utils.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<s:set var="actionNameMostrarBusqueda" 		value="actionNameMostrarBusqueda" />
<s:url var="mostrarBusquedaURL" 			action="%{actionNameMostrarBusqueda}"				namespace="/siniestros/recuperacion/ingreso/cancelacionDevolucion" />

<script type="text/javascript">
	divIdBusqFiltro        = '<s:property value="divBusqFiltro"      />';
	var buscarIngresoDevolucionPath = '<s:url action="buscar" namespace="/siniestros/recuperacion/ingreso/cancelacionDevolucion"/>';

</script>

<style type='text/css'>
.objbox tabe tbody tr {
	padding: 10px, 0;
}

/*  SECTIONS  */
.section {
	clear: both;
	padding: 0px;
	margin: 0px;
}

/*  COLUMN SETUP  */
.col {
	display: block;
	float: left;
	margin: 1% 0 1% 1.6%;
}

.col:first-child {
	margin-left: 0;
}

/*  GROUPING  */
.group:before,.group:after {
	content: "";
	display: table;
}

.group:after {
	clear: both;
}

.group {
	zoom: 1; /* For IE 6/7 */
}

/*  GRID OF THREE  */
.span_3_of_3 {
	width: 100%;
}

.span_2_of_3 {
	width: 66.1%;
}

.span_1_of_3 {
	width: 32.2%;
}

/*  GO FULL WIDTH AT LESS THAN 480 PIXELS */
@media only screen and (max-width: 480px) {
	.col {
		margin: 1% 0 1% 0%;
	}
}

@media only screen and (max-width: 480px) {
	.maincontent {
		text-align: left;
	}
	.span_3_of_3 {
		width: 100%;
		height: auto;
	}
	.span_2_of_3 {
		width: 100%;
		height: auto;
	}
	.span_1_of_3 {
		width: 100%;
		height: auto;
	}
}

/*  GRID OF FOUR   ============================================================================= */
.span_4_of_4 {
	width: 100%;
}

.span_3_of_4 {
	width: 74.6%;
}

.span_2_of_4 {
	width: 49.2%;
}

.span_1_of_4 {
	width: 23.8%;
}

/*  GO FULL WIDTH AT LESS THAN 480 PIXELS */
@media only screen and (max-width: 480px) {
	.span_4_of_4 {
		width: 100%;
	}
	.span_3_of_4 {
		width: 100%;
	}
	.span_2_of_4 {
		width: 100%;
	}
	.span_1_of_4 {
		width: 100%;
	}
}
</style>