<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numeroIngreso" 		type="ro" width="70" sort="int" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.numeroingreso'}"/></column>
        <column id="tipoRecuperacion" 	type="ro" width="100" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.tiporecuperacion'}"/> </column>
		<column id="estatusIngreso"  	type="ro" width="110" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.estatusingreso'}"/> </column>
		<column id="selected" 			type="ch" width="75"  sort="int"  align="center" ><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.datos.facturar'}"/></column>
		<column id="estatusFactura"     type="ro" width="100" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.estatusfactura'}"/> </column>	
		<column id="folioFactura"    	hidden="true" type="ro" width="180" sort="str"></column>
		<column id="detalleRechazo"    	type="img" width="160" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresos.facturar.detallerechazo'}"/> </column>
		
	  	</head>
		<s:iterator value="listaIngresosFacturables">
			<row id="<s:property value="ingresoId"/>" emisionFacturaId="<s:property value="emisionFacturaId"/>" codeRecuperacionId="<s:property value="tipoRecuperacion"/>">	
				<cell><s:property value="numeroIngreso" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusIngresoDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="facturar" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusFactura" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="folioFactura" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="%{estatusFactura=='RECHAZADA'}" >
					<cell>	/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle^javascript:verDetalleErrores(<s:property value="numeroIngreso" escapeHtml="false" escapeXml="true" />,"<s:property value="folioFactura" escapeHtml="false" escapeXml="true" />")^_self</cell>
				</s:if>	
				<s:else>
					<cell>../img/blank.gif^NULL^^_self</cell>
				</s:else>
						
			</row>
		</s:iterator>
</rows>