<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/recuperacion/listadoIngresos.js'/>" ></script>


<style>
<!--
table{
	text-transform: uppercase;
}
</style>

<s:form id="cancelacionPendientePorAplicar" action="mostrarVentanaCancelacionPendientePorAplicar" namespace="/siniestros/recuperacion/ingresoCancelacion" name="cancelacionPendientePorAplicar">

<table id="agregar" style=" border:0; ">
  		<tr>
  			<th> <s:text name="midas.siniestros.ingresos.cancelacion.ingreso.aplicado.titulo" /></th>
  		</tr>
  		<tr>
  			<td>
  				<s:radio cssClass="tipoCancelacion"
  					name="tipoCancelacion" 
  					list="#{'1':'Cancelar Ingresos por devoluci\u00F3n'}"
  					 />
  			</td>
  		</tr> 
  		<s:if test="( (tipoDeRecuperacion != 'SVM') && (tipoDeRecuperacion != 'CRU') ) " >
	  		<tr>
	  			<td>
	  				<s:radio cssClass="tipoCancelacion"
	  					name="tipoCancelacion" 
	  					list="#{'2':' Enviar cancelaci\u00F3n a cuenta acreedora'}"
	  					 />
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<s:radio cssClass="tipoCancelacion"
	  					name="tipoCancelacion" 
	  					list="#{'3':'Reversa de ingreso aplicado'}"
	  					 />
	  			</td>
	  		</tr>	
  		</s:if>	  		
  		<tr>
  			<td>
	  			<div id="btnBuscar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
	  				<a href="javascript: void(0);" onclick="continuarCancelacionIngresoAplicado();"> 
						<s:text name="midas.siniestros.ingresos.cancelacion.ingreso.aplicado.continuar" /> 
					</a>
				</div>
			</td>
		</tr>
</table>

<s:hidden name="ingresoId" id="ingresoId" />

</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

