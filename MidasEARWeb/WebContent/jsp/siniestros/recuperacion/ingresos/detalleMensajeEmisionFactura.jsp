<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">	
<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">

<div id="contenedorFiltros" style="background:white;height: 98%">
<s:form id=mensajesValidacionForm>
<div style="width: 98%;">
	<table id="agregar">
	<tr>			    
		<td>				
			<ul class="subtituloLeft">
				<s:iterator value="listaErroresValidacion" status="statusVar">
					<li>								
						<s:property/>
					</li>	
				</s:iterator>
			</ul>				
		</td>
	</tr>
	</table>
	<div style="width: 99%;background:white;">
			<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:parent.cerrarVentanaModal('DetValidacionFactIngreso');"> 
							<s:text name="midas.boton.cerrar" /> 							
						</a>
					</div>
		    </div>			
</div>
</s:form>
</div>