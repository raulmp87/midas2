<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
        </beforeInit>
        <column id="cuentaAcreedora"	type="ro"	hidden="false"	width="100" sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.cuentaacreedora" /></column>
      	<column id="tipoRecuperacion"	type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.tiporecuperacion" /></column>
		<column id="tipoDevolucion"		type="ro" 	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.tipodevolucion" /></column>
		<column id="siniestroOrigen"	type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.siniestroorigen" /></column>
		<column id="motivoCancelacion"	type="ro"	hidden="false"	width="120" sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.motivocancelacion" /></column>
		<column id="formaPago"			type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.formapago" /></column>
		<column id="numSolicitud"		type="ro"	hidden="false"	width="80"  sort="int" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.nosolicitudpago" /></column>
		<column id="solicitadoPor"		type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.solicitadopor" /></column>
		<column id="autorizadoPor"		type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.autorizadopor" /></column>
		<column id="numChequeRef"		type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.nochequeref" /></column>
		<column id="fechaSolicitud"		type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechasolicitud" /> </column>
		<column id="fechaAutorizacion"	type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechaautorizacion" /></column>
		<column id="fechaCheque"		type="ro"	hidden="false"	width="80"  sort="date_custom" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.fechacheque" /></column>
		<column id="importe"			type="ron"	hidden="false"	width="80"  sort="int" format="$0,000.00"><s:text name="midas.siniestros.recuperacion.ingresodevolucion.importe" /></column>
		<column id="estatus"			type="ro"	hidden="false"	width="80"  sort="str" ><s:text name="midas.siniestros.recuperacion.ingresodevolucion.estatus" /></column>
		
		<column id="solicitar" 			type="img" 					width="40" 	sort="na" align="center">Acciones</column>
		<column id="cancelar" 			type="img" 					width="40" 	sort="na" align="center">#cspan</column>
		<column id="consultar" 			type="img" 					width="40" 	sort="na" align="center">#cspan</column>
		<column id="imprimir" 			type="img" 					width="40" 	sort="na" align="center">#cspan</column>

	</head>
	<s:iterator value="ingresoDevolucionList" var="index">
		 <row id="<s:property value="id" escapeHtml="false" escapeXml="true" />">
			<cell><s:property	value="cuentaAcreedoraStr"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="tipoRecuperacion"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="tipoDevolucion"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="siniestroOrigen"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="motivoCancelacion"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="formaPago"			escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="numSolicitud"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="solicitadoPor"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="autorizadoPor"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="numChequeRef"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="fechaSolicitud"		escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="fechaAutorizacion"	escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="fechaCheque"			escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="importe"				escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property	value="estatus"				escapeHtml="false" escapeXml="true"/></cell>

           	<s:if test="%{flujo ==  'SOL'}">
           		<s:if test="estatus == 'Sin Solicitud'">
				<cell>/MidasWeb/img/icons/ico_terminar.gif^Solicitar Cheque^javascript: solicitar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
					<s:else>
						<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
					</s:else>
				<s:if test="estatus == 'Pendiente por Autorizar' || estatus == 'Autorizada'">
					<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Cancelar Solicitud de Cheque^javascript: cancelar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
					<s:else>
						<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
					</s:else>
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Solicitud de Cheque^javascript: consultar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				<s:if test="estatus != 'Sin Solicitud'">
					<cell>/MidasWeb/img/ico_impresion.gif^Imprimir^javascript: imprimirSolicitudChequeId(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
					<s:else>
						<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
					</s:else>
			</s:if>
			<s:elseif test="%{flujo=='AUT'}">
				<s:if test="estatus == 'Pendiente por Autorizar'">
					<cell>/MidasWeb/img/b_autorizar.gif^Autorizar Solicitud de Cheque^javascript: autorizar(<s:property value="id" escapeHtml="false" escapeXml="true"/>, "LISTADO")^_self</cell>
					<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Rechazar Solicitud de Cheque^javascript: rechazar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
					<s:else>
						<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
						<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
					</s:else>
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Solicitud de Cheque^javascript: mostrarImprimirSolicitud(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
 				<s:if test="estatus == 'Pagada'">
 					<cell>/MidasWeb/img/ico_impresion.gif^Imprimir^javascript: imprimirSolicitudChequeId(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
 				</s:if>
	 				<s:else>
	 					<cell>/MidasWeb/img/blank.png^^javascript: return void()^_self</cell>
	 				</s:else>
			
			</s:elseif>
		</row>
	</s:iterator>
</rows>