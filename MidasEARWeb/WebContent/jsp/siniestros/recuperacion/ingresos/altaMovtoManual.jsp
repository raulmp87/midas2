<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script	src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/ingresos.js'/>"></script>

<s:form id="movtoManualForm" namespace="/siniestros/recuperacion/ingresos" name="movtoManualForm" action="registrarMovtoManual">
	<s:hidden id="ingresoId" name="ingresoId" />
	
	<div id="contenedorFiltros" style="width: 96%;">
		<table id="agregar" border="0">
			
			<tr><td>
			<div class="titulo" style="width: 98%;">
				<s:text name="midas.siniestros.recuperacion.movsmanuales.tituloregistrarmovtomanual"/>	
			</div>
			</td></tr>
			
			<tr><td>
			<div id="usuario" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="filtroMovManual.usuario" id="usuario_t" cssClass="cajaTextoM2 w370" maxlength="100"
		            		label="%{getText('midas.siniestros.recuperacion.movsmanuales.alta.usuario')}" labelposition="left" disabled="false" readonly="true" />
			</div>
			</td></tr>
			
			<tr><td>
			<div id="fechaRegistro" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="filtroMovManual.fechaRegistro" id="fechaRegistro_t" cssClass="cajaTextoM2 w370" maxlength="100"
		            		label="%{getText('midas.siniestros.recuperacion.movsmanuales.alta.fecharegistro')}" labelposition="left" disabled="false" readonly="true"/>
			</div>
			</td></tr>
			
			<tr><td>
			<div id="importe" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="filtroMovManual.importe" id="importe_t" cssClass="cajaTextoM2 w320 jQrequired formatCurrency jQmoney jQrestrict" maxlength="100"
				onkeyup="mascaraDecimales('#importe_t',this.value);"
				onblur="mascaraDecimales('#importe_t',this.value);" maxlength="10"
				onfocus="validateAll();"
		            		label="%{getText('midas.siniestros.recuperacion.movsmanuales.alta.importe')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
			
			<tr><td>
			<div id="cuentaContable" style="margin-left: 10%;margin-top: 2%;">
		        <s:select list="listaCuentaManual" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroMovManual.idCuentaContable" id="cuentaContable_s" cssClass="cajaTextoM2 w400 jQrequired" 
					label="%{getText('midas.siniestros.recuperacion.movsmanuales.alta.enviocuenta')}" labelposition="left" disabled="false"
					onfocus="validateAll();" 
					onChange="validateAll();")/>
			</div>
			</td></tr>
			
			<tr><td>
			<div id="causaMovto" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="filtroMovManual.causaMovimiento" id="causaMovto_t" cssClass="cajaTextoM2 w320 jQrequired jQalphaextra" maxlength="100"
							onfocus="validateAll();"
		            		label="%{getText('midas.siniestros.recuperacion.movsmanuales.alta.causamovto')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
		
	</table>
	</div>
	
</s:form>

	<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
					<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:parent.cerrarVentanaModal('vm_altaMovtoManual');}"> 
					<s:text name="midas.boton.cerrar" /> </a>
				</div>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
					<a href="javascript: void(0);" onclick="onClickRegistrarMovManual();"> 
					<s:text name="midas.boton.agregar" /> </a>
				</div>		
			</td>							
		</tr>
	</table>
<script type="text/javascript">
	jQuery(document).ready(function(){
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	});
</script>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>