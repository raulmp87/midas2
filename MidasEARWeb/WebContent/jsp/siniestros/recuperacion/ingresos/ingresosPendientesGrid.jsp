<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaIngresosPendientes</param>
				<param>true</param>
				<param>infoAreaIngresosPendientes</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="selected" 			type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="numeroRecuperacion" type="ro" width="120" sort="int" ><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerorecuperacion'}"/></column>
        <column id="tipoRecuperacion" 	type="ro" width="100" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.tiporecuperacion'}"/> </column>
		<column id="medioRecuperacion"  type="ro" width="110" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.mediorecuperacion'}"/> </column>
		<column id="numeroDeudor"	    type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerodeudor'}"/></column>
		<column id="nombreDeudor"       type="ro" width="100" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.nombredeudor'}"/> </column>	
		<column id="numeroSiniestro"    type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerosiniestro'}"/> </column>
		<column id="numeroReporte"      type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numeroreporte'}"/> </column>
		<column id="oficina"	        type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.oficina'}"/> </column>
		<column id="referenciaBancaria" type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.referenciabancaria'}"/> </column>
		<column id="montoRecuperado"    type="ron" width="180" format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.montorecuperado'}"/> </column>
		<column id="fechaPendiente"		type="ro" width="100" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.fecharegistro'}"/> </column>

	  	</head>
		<s:iterator value="listaIngresos">
		<s:if test="montoFinalRecuperado != null">
			<row id="<s:property value="ingresoId"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="numeroRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="medioRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="claveDeudor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreDeudor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficinaDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell>
					<s:if test="tipoRecuperacion !='CIA' " >
						<s:property value="referencia" escapeHtml="false" escapeXml="true" />
					</s:if>
					<s:else>
						<s:property value="referenciaIdentificada" escapeHtml="false" escapeXml="true" />
					</s:else>
				</cell>
				<cell><s:property value="montoFinalRecuperado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="medioRecuperacionHidden"><s:property value="medioRecuperacion" escapeHtml="false" escapeXml="true"/></userdata>
			</row>
		</s:if>
		</s:iterator>
	
</rows>