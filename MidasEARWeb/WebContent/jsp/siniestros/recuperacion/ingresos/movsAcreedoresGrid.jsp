<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaMovsAcreedores</param>
				<param>true</param>
				<param>infoAreaMovsAcreedores</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="selected" 				type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="cuentaContable"		 	type="ro" width="140" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.movsacreedores.cuentacontable'}"/></column>
        <column id="refunic"			 	type="ro" width="170" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerorefunic'}"/> </column>
		<column id="tipoRecuperacion"	  	type="ro" width="120" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.tiporecuperacion'}"/> </column>
		<column id="siniestroOrigen"    	type="ro" width="150" sort="str"><s:text name="%{'midas.siniestros.recuperacion.movsacreedores.siniestroorigen'}"/></column>
		<column id="motivoCancelacion"     	type="ro" width="105" sort="str"><s:text name="%{'midas.siniestros.recuperacion.movsacreedores.motivocancelacion'}"/> </column>	
		<column id="fechaTraspaso"	    	type="ro" width="180" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.movsacreedores.fechatraspaso'}"/> </column>
		<column id="importe"      			type="ron" width="180" format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.recuperacion.movsacreedores.importe'}"/> </column>

	  	</head>
		<s:iterator value="listaMovAcreedores">
			<row id="<s:property value="movimientoId"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="cuentaContable" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="refunic" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="siniestroOrigen" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="motivoCancelacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaTraspaso" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="importe" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
	
</rows>