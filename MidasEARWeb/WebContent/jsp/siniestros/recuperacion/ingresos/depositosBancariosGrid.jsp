<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaDepositos</param>
				<param>true</param>
				<param>infoAreaDepositos</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="selected" 				type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="referenciaBancaria" 	type="ro" width="140" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.referenciabancaria'}"/></column>
        <column id="descripcionMovimiento" 	type="ro" width="170" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.descripcionmovimiento'}"/> </column>
		<column id="nombreBanco"		  	type="ro" width="120" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.nombrebanco'}"/> </column>
		<column id="numeroCuenta"	    	type="ro" width="150" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerocuenta'}"/></column>
		<column id="numeroRefunic"       	type="ro" width="105" sort="str"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.numerorefunic'}"/> </column>	
		<column id="fechaDeposito"	    	type="ro" width="180" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.fechadeposito'}"/> </column>
		<column id="monto"      			type="ron" width="180" format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.recuperacion.ingresospendientes.monto'}"/> </column>

	  	</head>
		<s:iterator value="listaDepositos">
			<row id="<s:property value="refunic"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="referencia" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="descripcionMovimiento" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="banco" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroCuenta" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="refunic" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaDeposito" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="monto" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
	
</rows>