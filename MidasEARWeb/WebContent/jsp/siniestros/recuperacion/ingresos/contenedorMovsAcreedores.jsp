<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	
	<form id="buscarMovsAcreedoresForm">
	<s:hidden id="movsAcreedoresConcat" name="filtroMovAcreedor.movAcreedoresConcat" />
	<table width="99%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
					  
	                  <tr>
						<td colspan="6" >
							<table  id="filtrosM2" width="98%">
					            <tr>
					                  <td class="titulo" colspan="6"><s:text name="midas.siniestros.recuperacion.movsacreedores.titulobusquedamovsacreedor" /></td>
					            </tr>
					          
					          	<tr>
					            	<td>
						            	<s:select name="filtroMovAcreedor.cuentaContable" id="cuentaContable" list="listaCuentaAcredora"
						            	cssClass="cajaTextoM2 w230 obligatorio" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						            	label="%{getText('midas.siniestros.recuperacion.movsacreedores.cuentacontable')}"/>
					            	</td>
									
									<td>
						            	<s:textfield name="filtroMovAcreedor.refunic" id="numeroRefunic" cssClass="cajaTextoM2 w230 jQnumeric jQrestrict obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerorefunic')}" disabled="false" />
					            	</td>
									
									<td>
										<s:select list="listaTipoRecuperacion" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
										name="filtroMovAcreedor.tipoRecuperacion" id="tipoRecuperacion" cssClass="cajaTextoM2 w230 obligatorio" onchange=""
										label="%{getText('midas.siniestros.recuperacion.ingresospendientes.tiporecuperacion')}"/>									
					            	</td>
					            	
					            	<td colspan="3">
						            	<s:textfield name="filtroMovAcreedor.siniestroOrigen" id="siniestroOrigen" cssClass="cajaTextoM2 w230 jQalphaExtra obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.movsacreedores.siniestroorigen')}" disabled="false" />
					            	</td>
					            </tr>
					            
					            <tr>
					            	<td colspan="6">
					            		<s:select list="listaMotivosCancelacion" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
										name="filtroMovAcreedor.motivoCancelacion" id="tipoRecuperacion" cssClass="cajaTextoM2 w230 obligatorio" onchange=""
										label="%{getText('midas.siniestros.recuperacion.movsacreedores.motivocancelacion')}"/>
					            	</td>
					            </tr>
					            
					            <tr>
					            	<td>
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.movsacreedores.fechatraspasodesde')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroMovAcreedor.fechaTraspasoIni"
										id="fechaInicioTraspaso_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td >
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.movsacreedores.fechatraspasohasta')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroMovAcreedor.fechaTraspasoFin"
										id="fechaFinTraspaso_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td>
						            	<s:textfield id="importeInicial_t"
										label="%{getText('midas.siniestros.recuperacion.movsacreedores.importedesde')}"
										name="filtroMovAcreedor.importeIni"
										onkeyup="mascaraDecimales('#importeInicial_t',this.value);"
										onblur="mascaraDecimales('#importeInicial_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            	
					            	<td colspan="3">
						            	<s:textfield id="importeFinal_t"
										label="%{getText('midas.siniestros.recuperacion.movsacreedores.importehasta')}"
										name="filtroMovAcreedor.importeFin"
										onkeyup="mascaraDecimales('#importeFinal_t',this.value);"
										onblur="mascaraDecimales('#importeFinal_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            </tr>
					            
					            <tr>		
										<td colspan="6">		
											<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
												<tr>
													<td  class= "guardar">
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
															 <a href="javascript: void(0);" onclick="onClickBuscarMovsAcreedores();" >
															 <s:text name="midas.boton.buscar" /> </a>
														</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
															<a href="javascript: void(0);" onclick="limpiarFiltrosMovsAcreedores();"> 
															<s:text name="midas.boton.limpiar" /> </a>
														</div>	
													</td>							
												</tr>
											</table>				
										</td>		
								</tr>
					
					      </table>
						</td>
					  </tr>
					  
					  <tr><td>
					  </td></tr>
					  
					<tr>
						<td colspan="6">
							<div id="tituloListadoMovsAcreedores" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.recuperacion.movsacreedores.titulolistadomovsacreedor" /></div>
							<div id="indicadorMovsAcreedores"></div>	
							<div id="movsAcreedoresGridContainer" style="display:none;">
								<div id="movsAcreedoresGrid" style="width:98%;height:200px;"></div>
								<div id="pagingAreaMovsAcreedores"></div><div id="infoAreaMovsAcreedores"></div>
							</div>
						</td>
					</tr>
	            </tbody>
	</table>
	</form>

<script type="text/javascript">
	dhx_init_tabbars();
	jQuery(document).ready(function(){
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		iniDetaAcreedor();
	});
</script>