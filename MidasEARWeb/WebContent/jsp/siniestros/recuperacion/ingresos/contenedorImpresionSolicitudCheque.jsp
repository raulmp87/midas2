<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
	
.warningField {
    background-color: #f5a9a9;
}
</style>

<script type="text/javascript">
	var mostrarImpresionLiquidacion = '<s:url action="consultarImpresion" namespace="/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion"/>';
	var imprimirLiquidacion = '<s:url action="imprimirSolicitudCheque" namespace="/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion"/>';
</script>

<s:form id="impresionLiquidacionForm" >
		<s:hidden id="ingresoDevolucionId" name="ingresoDevolucionId"/>
		<s:hidden id="porConceptoRecuperacion" value="%{getText('midas.siniestros.recuperacion.ingresodevolucion.conceptorecuperacion')}" />
		<s:hidden id="esImprimible" name="esImprimible"/>
		<s:hidden id="pantallaOrigen" name="pantallaOrigen"/>
		<s:hidden id="estatusIngresoDev" name="ingresoDevolucion.estatus" />
		
		<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.ingresodevolucion.tituloimpresion"/>	
	</div>
	
	<div id="contenedorPDF" style="width: 96%; display:inline; margin-left: 10px;">
	<s:if test="ingresoDevolucionId != null">
		<s:if test="esImprimible == true">
			<iframe height="400" width="94%"
				src="/MidasWeb/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion/imprimirSolicitudCheque.action?ingresoDevolucionId=<s:text name="ingresoDevolucionId" />&esPreview=true">
				<p>No se pudo generar la previsualizacion de la impresion</p>
			</iframe>
		</s:if>
		<s:else>
			<div style="color:red;font-weight:bold;"><s:text name="midas.siniestros.recuperacion.ingresodevolucion.error.NoImprimible" /></div>
		</s:else>
	</s:if>
	<s:else>
		<div style="color:red;font-weight:bold;"><s:text name="midas.siniestros.recuperacion.ingresodevolucion.error.idIngresoDev" /></div>
	</s:else>
	</div>
</s:form>

<br/>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:cerrarConsultaSolicitud();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
						</a>
					</div>
					<div id="btn_imprimir" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript:imprimirSolicitudCheque();"> 
						<s:text name="midas.boton.imprimir" />
						<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/common/b_imprimir.gif'/>
					</a>
					</div>
						<div id="btn_rechazar" class="btn_back w140" style="display: none; float: right;">
						<a href="javascript: void(0);" onclick="javascript:rechazarSolicitudCheque();"> 
							<s:text name="midas.boton.rechazar" />
							<img border='0px' alt='Rechazar' title='Rechazar' src='/MidasWeb/img/common/b_borrar.gif'/>
						</a>
						</div>
						<div id="btn_autorizar" class="btn_back w140" style="display: none; float: right;">
						<a href="javascript: void(0);" onclick="javascript:autorizarSolicitudCheque();"> 
							<s:text name="midas.boton.autorizar" />
							<img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/b_autorizar.gif'/>
						</a>
						</div>
					<div id="btn_observaciones" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript:mostrarObservaciones();"> 
						<s:text name="midas.siniestros.recuperacion.ingresodevolucion.observaciones" />
					</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div id="contenedorObservaciones" style="display:none;">
	<div id="contenedorFiltros" style="width: 96%;">
			<div class="floatLeft" style="width:9%; margin-right: 1%; margin-left: 1%; font-weight: bold;">
				<div class="floatLeft" style="width: 90%;">
					<s:text name="midas.siniestros.recuperacion.ingresodevolucion.observaciones" />
				</div>
			</div>
			<div class="floatLeft" style="width:90%;">
				<div class="floatLeft" style="width: 90%;">
					<div id="contenedorTextArea">
					<s:textarea name="observaciones" id="observaciones" 
					cssClass="textarea" cssStyle="font-size: 10pt;"	cols="80" rows="3"
					onblur="truncarTexto(this,300);"/>
					</div>
					<div id="btn_enviarObservaciones" class="btn_back w140" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="javascript:actualizarObservaciones();"> 
						<s:text name="midas.boton.enviar" />
					</a>
					</div>
				</div>
			</div>
	
	</div>
	</div>

<script type="text/javascript">
jQuery(document).ready(
	function(){
	 	mostrarBotonesAutorizarRechazar();
 	}
 );
</script>

<script src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/ingresoDevolucion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>