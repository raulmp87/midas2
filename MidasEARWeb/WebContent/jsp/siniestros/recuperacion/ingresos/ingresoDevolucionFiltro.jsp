<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="filtrosBusquedaIngresoDevolucion" id="filtrosBusquedaIngresoDevolucion" method="post">

	<table width="99%" id="filtros">
		<tr>
			<td class="titulo" colspan="3">
				<s:text name="%{'midas.siniestros.recuperacion.ingresodevolucion.busquedatitulo.' + flujo}"/>
			</td>
		</tr>
		<tr>
			<td align="right" style="padding-right: 80px"><s:text name="midas.siniestros.recuperacion.ingresodevolucion.cuentaacreedora" />:
				<s:select 	list="cuentaAcreedoraList"
							id="cuentaAcreedoraList"
							name="ingresoDevolucionFiltro.cuentaAcreedora"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w150" />							
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.siniestroorigen" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="20" 
							 	onblur="this.value=jQuery.trim(this.value);validaFormatoSiniestro(this)"
								id="siniestroOrigen" 
								name="ingresoDevolucionFiltro.siniestroOrigen"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.estatus" />:
				<s:select 	list="estatusList"
							id="estatusList"
							name="ingresoDevolucionFiltro.estatus"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>	
			
		</tr>
		<tr>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.importe" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.de" />
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict formatCurrency"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="importeDesde" 
								name="ingresoDevolucionFiltro.importeDesde"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.hasta" />
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict formatCurrency"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="importeHasta" 
								name="ingresoDevolucionFiltro.importeHasta"
								theme="simple" />
			</td>
			<td align="right"  style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.tiporecuperacion" />:
				<s:select 	list="tipoRecuperacionList"
							id="tipoRecuperacionList"
							name="ingresoDevolucionFiltro.tipoRecuperacion"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		
		<tr>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.fechasolicitud" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.de" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaSolicitudDesde"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaSolicitudDesde"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>	
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.hasta" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaSolicitudHasta"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaSolicitudHasta"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>	
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.solicitadopor" />:
				<s:select 	list="solicitadoPorList"
							id="solicitadoPorList"
							name="ingresoDevolucionFiltro.solicitadoPor"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		
		<tr>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.fechaautorizacion" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.de" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaAutorizacionDesde"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaAutorizacionDesde"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>									
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.hasta" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaAutorizacionHasta"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaAutorizacionHasta"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>									
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.autorizadopor" />:
				<s:select 	list="autorizadoPorList"
							id="autorizadoPorList"
							name="ingresoDevolucionFiltro.autorizadoPor"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		
		<tr>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.fechacheque" />:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<s:text	name="midas.siniestros.recuperacion.ingresodevolucion.de" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaChequeDesde"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaChequeDesde"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>									
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.hasta" />
				<sj:datepicker  name="ingresoDevolucionFiltro.fechaChequeHasta"
								changeMonth="true"
						 		changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
	                      		id="fechaChequeHasta"
						  		maxlength="10"
							   	size="18"
						 		onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield" parentTheme="simple"/>									
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.formapago" />:
				<s:select 	list="formaPagoList"
							id="formaPagoList"
							name="ingresoDevolucionFiltro.formaPago"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>
		<tr>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.nochequeref" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="numChequeReferencia" 
								name="ingresoDevolucionFiltro.numChequeReferencia"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.tipodevolucion" />:
				<s:select 	list="tipoDevolucionList"
							id="tipoDevolucionList"
							name="ingresoDevolucionFiltro.tipoDevolucion"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>				
		</tr>

		<tr>

			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.nosolicitudpago" />:
				<s:textfield 	cssClass="cleaneable txtfield jQrestrict"
							 	cssStyle="width: 130px;" 
							 	labelposition="left" size="10"
							 	maxlength="10" 
							 	onblur="this.value=jQuery.trim(this.value)"
								id="numSolicitud" 
								name="ingresoDevolucionFiltro.numSolicitud"
								theme="simple" />
			</td>
			<td align="right" style="padding-right: 80px"><s:text	name="midas.siniestros.recuperacion.ingresodevolucion.motivocancelacion" />:
				<s:select 	list="motivoCancelacionList"
							id="motivoCancelacionList"
							name="ingresoDevolucionFiltro.motivoCancelacion"
							headerValue="%{getText('midas.general.seleccione')}" 
							headerKey="" 
							theme="simple"
							cssClass="cleaneable cajaTextoM2 w180" />
			</td>
		</tr>		
		<tr>
			<td colspan="3" align="right"  style="padding-right: 80px">
				<div id="divLimpiarBtn" class="w150" style="float: right;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="reset();"> <s:text name="midas.boton.limpiar" />
						</a>
					</div>
				</div>
				<div id="divBuscarBtn" class="w150" style="float: right;">
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="buscar();"> <s:text name="midas.boton.buscar" /> </a>
					</div>
				</div>
			</td>
		</tr>
	</table>

</s:form>