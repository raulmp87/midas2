<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="ingresoDevolucionHeader.jsp"></s:include>
<s:hidden name="flujo" id="flujo"></s:hidden>
<s:div name="divBusqFiltro">
	<s:include value="ingresoDevolucionFiltro.jsp" />
</s:div>

<div class="titulo">
	<s:text name="%{'midas.siniestros.recuperacion.ingresodevolucion.listadotitulo.' + flujo}"/>
</div>

<s:div name="ingresoDevolucionGrid" class="ingresoDevolucionGrid"	style="width: 98%; height: 330px;"></s:div>

<div class="btn_back w120" style="display: inline; float: right;"  id="btn_exportar" >
	<a href="javascript: void(0);" onclick="javascript:exportar();">
		<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
	</a>
</div>
<div id="pagingArea"></div>
<div id="infoArea"></div>

<script type="text/javascript">
		initCurrencyFormatOnTxtInput();
		initGridIngresoDevolucion();
		init('<s:property value="flujo" escapeHtml="false" escapeXml="true"/>');
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>