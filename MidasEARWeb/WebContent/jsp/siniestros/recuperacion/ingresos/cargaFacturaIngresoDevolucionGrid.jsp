<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="numeroFactura" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.numeroFactura" /></column>
		<column id="fechaFactura"  type="ro" width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.fechaFactura" /></column>
		<column id="subtotal" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.subtotal" /></column>
		<column id="iva" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.iva"/></column>
		<column id="ivaRetenido"  type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ivaRetenido" /></column>		
		<column id="isr" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.isr"/></column>
		<column id="total" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
		<column id="estatus" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.estatus" /></column>
		<column id="ver" type="img" width="120" align="center">Ver Detalle</column>	
	
	</head>	  		
	<s:iterator value="facturasCargadas" status="stats">
		<row id="<s:property value="factura.id"/>">
		    <cell><s:property value="factura.numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura.fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="factura.subTotal" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="factura.iva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura.ivaRetenido" escapeHtml="false"/></cell>
			<cell><s:property value="factura.isr" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="factura.montoTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura.estatus" escapeHtml="false" escapeXml="true"/></cell>
				<s:if test="mensajes.size > 0">		
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver Detalle Validaciones^javascript:verDetalleValidacion(<s:property value="id"/>,"<s:property value="factura.numeroFactura"/>");^_self</cell>								
				</s:if>	
		</row>
	</s:iterator>	
</rows>
