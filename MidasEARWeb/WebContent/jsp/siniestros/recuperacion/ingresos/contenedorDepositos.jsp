<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<form id="buscarDepositosForm">
	<s:hidden id="depositosConcat" name="filtroDepositos.depositosConcat" />
	<table width="99%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td colspan="6" >
							<table  id="filtrosM2" width="98%">
					            <tr>
					                  <td class="titulo" colspan="6"><s:text name="midas.siniestros.recuperacion.ingresospendientes.titulobusquedadepositos" /></td>
					            </tr>
					          
					          	<tr>
					            	<td>
						            	<s:textfield name="filtroDepositos.referencia" id="referencia_t" cssClass="cajaTextoM2 w230 jQalphaextra obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.ingresospendientes.referenciabancaria')}" disabled="false" />
					            	</td>
									
									<td>
										<s:select list="bancos" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
										name="filtroDepositos.bancoId" id="bancos_s" cssClass="cajaTextoM2 w230 obligatorio" onchange="onChangeBancosDepositos(this.value)"
										label="%{getText('midas.siniestros.recuperacion.ingresospendientes.nombrebanco')}"/>									
					            	</td>
					            	
					            	<td>
						            	<s:select list="listaReferenciasIdentif" 
											headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
											name="filtroDepositos.numeroCuenta" id="numeroCuenta_t" cssClass="cajaTextoM2 w230 obligatorio"
											label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerocuenta')}"/>									
					            	</td>
					            	
					            	<td colspan="3">
						            	<s:textfield name="filtroDepositos.refunic" id="numeroRefunic_t" cssClass="cajaTextoM2 w230 jQnumeric jQrestrict obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.ingresospendientes.numerorefunic')}" disabled="false" />
					            	</td>
					            </tr>
					            
					            <tr>
					            	<td colspan="6">
						            	<s:textfield name="filtroDepositos.descripcionMovimiento" id="descripcionMovimiento_t" cssClass="cajaTextoM2 w770 jQalphaextra obligatorio" 
						            		label="%{getText('midas.siniestros.recuperacion.ingresospendientes.descripcionmovimiento')}" disabled="false" />
					            	</td>
					            </tr>
					            
					            <tr>
					            	<td>
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.ingresospendientes.fechadepositodesde')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroDepositos.fechaDepositoIni"
										id="fechaInicio_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td >
					            		<div style="padding-bottom: 3%; padding-left: 2%;">
					            		<label><s:text
										name="%{getText('midas.siniestros.recuperacion.ingresospendientes.fechadepositohasta')}" />:</label><br/>
										</div>
										<div style="padding-left: 2%;">
					            		<sj:datepicker name="filtroDepositos.fechaDepositoFin"
										id="fechaFin_t" buttonImage="../img/b_calendario.gif"
										changeYear="true" changeMonth="true" maxlength="10"
										cssClass="w200 cajaTextoM2 obligatorio" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										></sj:datepicker>
										</div>
									</td>
					            	
					            	<td>
						            	<s:textfield id="montoInicial_t"
										label="%{getText('midas.siniestros.recuperacion.ingresospendientes.montodesde')}"
										name="filtroDepositos.montoIni"
										onkeyup="mascaraDecimales('#montoInicial_t',this.value);"
										onblur="mascaraDecimales('#montoInicial_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            	
					            	<td colspan="3">
						            	<s:textfield id="montoFinal_t"
										label="%{getText('midas.siniestros.recuperacion.ingresospendientes.montohasta')}"
										name="filtroDepositos.montoFin"
										onkeyup="mascaraDecimales('#montoFinal_t',this.value);"
										onblur="mascaraDecimales('#montoFinal_t',this.value);" maxlength="10"
										disabled="false" cssStyle="float: left;"
										cssClass="cleaneable txtfield w230 jQrestrict jQ2float formatCurrency obligatorio"></s:textfield>
					            	</td>
					            </tr>
					            
					            <tr>		
										<td colspan="6">		
											<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
												<tr>
													<td  class= "guardar">
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
															 <a href="javascript: void(0);" onclick="onClickBuscarDepositosBancarios();" >
															 <s:text name="midas.boton.buscar" /> </a>
														</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
															<a href="javascript: void(0);" onclick="limpiarFiltrosDepositosBancarios();"> 
															<s:text name="midas.boton.limpiar" /> </a>
														</div>	
													</td>							
												</tr>
											</table>				
										</td>		
								</tr>
					
					      </table>
						</td>
					  </tr>
				
					  <tr><td>
					  </td></tr>
					<tr>
						<td colspan="6">
							<div id="tituloListadoDepositos" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.recuperacion.ingresospendientes.titulolistadodepositos" /></div>
							<div id="indicadorDepositos"></div>	
							<div id="depositosBancariosGridContainer" style="display:none;">
								<div id="depositosBancariosGrid" style="width:98%;height:200px;"></div>
								<div id="pagingAreaDepositos"></div><div id="infoAreaDepositos"></div>
							</div>
						</td>
					</tr>
	            </tbody>
	</table>
	</form>

<script type="text/javascript">
	dhx_init_tabbars();
	jQuery(document).ready(function(){
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		iniDetaDeposito();
	});
</script>