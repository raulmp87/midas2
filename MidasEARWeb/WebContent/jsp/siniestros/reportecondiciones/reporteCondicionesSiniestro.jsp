<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/dateUtils.js'/>"></script>
<script type="text/javascript">
	var generarReportePath = '<s:url action="generarReporte" namespace="/siniestros/reportecondiciones"/>';
</script>

<s:form id="reporteCondicionesForm" >

	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.reportecondiciones.title"/>	
	</div>
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tr>
			    <td width="30%">
					<sj:datepicker name="fechaSiniestroIni"
						key="midas.siniestros.reportecondiciones.fechaIni"
						labelposition="left"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="fechaSiniestroIni"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				</td>
				<td colspan="4">
					<sj:datepicker name="fechaSiniestroFin"
						key="midas.siniestros.reportecondiciones.fechaFin"
						labelposition="left"
						changeMonth="true"
						changeYear="true"				
						buttonImage="../img/b_calendario.gif"
                        id="fechaSiniestroFin"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
					</sj:datepicker>
				 </td>	
				
			</tr>
			
			<tr>
				<td width="30%">
					<s:radio name="tipoPoliza"		
					list="#{'1':' MIDAS','2':'SEYCOS'}"
					onchange="changePolizaDiv(value)"
					id="tipoPoliza" value="1"/>
				</td>
				
				<td colspan="4">
					<div id="polizaMidas"><s:textfield cssClass="txtfield" 
						key="midas.fuerzaventa.configBono.poliza"
						name="numeroPolizaMidas"
						labelposition="top" 
						size="20"				
						onfocus="javascript: new Mask('####-########-##', 'string').attach(this)"	
						id="numeroPolizaMidas"/>
					</div>
					
					<div id="polizaSeycos" style="display: none;"><s:textfield cssClass="txtfield" 
						key="midas.fuerzaventa.configBono.poliza"
						name="numeroPolizaSeycos"
						labelposition="top" 
						size="20"				
						onfocus="javascript: new Mask('###-##########-##', 'string').attach(this)"	
						id="numeroPolizaSeycos"/>
					</div>
				</td>
			</tr>
			
			<tr>
				<td width="30%">
					<s:select id="estatus" key="midas.poliza.renovacionmasiva.gerencia"
								labelposition="top" 
								name="idGerencia"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="lstGerencias" listKey="id" listValue="descripcion"  
						  		cssClass="txtfield" /> 	</td>
				
				<td colspan="4" align="left">

					<s:select id="estatus" key="midas.condicionespecial.title"
								labelposition="top" 
								name="idCondicionEspecial"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="lstCondiciones" listKey="id" listValue="codigoNombre"  
						  		cssClass="txtfield" /> 	
				</td>	
			</tr>
			
			<tr>
				<td >
				<br/>
					<div class="btn_back w140" style="display: inline; float: right;">
						 <a href="javascript: void(0);" onclick="generarReporteCondiciones();">
						 <s:text name="midas.reporteSesas.generarReporte" /> </a>
					</div>
					
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
						<s:text name="midas.boton.limpiar" /> </a>
					</div>	
				</td>	
			</tr>
			
			
		</table>
	</div>

</s:form>

<div id="leyendaPolizaErrorDiv" style="display: none; color:red;" >
	<s:text name="midas.poliza.renovacionmasiva.leyendaError" />
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>

	function generarReporteCondiciones() {		
		
		if (validate()) {
			var formParams = jQuery(document.reporteCondicionesForm).serialize();
			window.open(generarReportePath + "?" + formParams, "REPORTE_CONDICIONES");
		}
	}
	
	function validate() {
		var numeroPolizaMidas = jQuery('#numeroPolizaMidas').val();
		var numeroPolizaSeycos = jQuery('#numeroPolizaSeycos').val();
		var fechaSiniestroInicio = jQuery('#fechaSiniestroIni').val();
		var fechaSiniestroFin = jQuery('#fechaSiniestroFin').val();
		
		if ((numeroPolizaMidas == null || numeroPolizaMidas == '') && (numeroPolizaSeycos == null || numeroPolizaSeycos == '')
				&& ((fechaSiniestroInicio == null || fechaSiniestroInicio == '') || (fechaSiniestroFin == null || fechaSiniestroFin == ''))) {
			mostrarMensajeInformativo('Debe de ingresar las Fechas de Siniestro ó el Número de Póliza', '20');
			return false;
		} else {
			if (fechaSiniestroInicio != null && fechaSiniestroInicio != '' && fechaSiniestroFin != null && fechaSiniestroFin != '') {
				var fechaIni = getDateFromString(fechaSiniestroInicio);
				var fechaFin = getDateFromString(fechaSiniestroFin);
				if (isDateAfterAllowSameDay(fechaIni, fechaFin)) {
					mostrarMensajeInformativo('La Fecha Inicial no debe ser mayor a la Fecha Final', '20');
					return false;
				}				
			}
		}		
		return true;
	}
	
	function limpiarFiltros() {
		jQuery('#reporteCondicionesForm').each (function(){
			  this.reset();
		});
	}
	
	function changePolizaDiv(value) {
		jQuery('#numeroPolizaMidas').val(null);
		jQuery('#numeroPolizaSeycos').val(null);
		if (value == 1) {	
			document.getElementById("polizaMidas").show();
			document.getElementById("polizaSeycos").hide();
			
		} else if (value == 2 ) {
			document.getElementById("polizaMidas").hide();
			document.getElementById("polizaSeycos").show();
		}
		
		
	}
</script>

