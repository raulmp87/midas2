<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="listLoteLayoutSipac.id" type="ro" width="150"><s:text name="midas.siniestros.reportes.sipac.idLote"/></column>
		<column id="listLoteLayoutSipac.fechaCreacion" type="ro" width="150"><s:text name="midas.siniestros.reportes.sipac.fechaCreacion"/></column>
		<column id="listLoteLayoutSipac.usuarioCreacion" type="ro" width="150"><s:text name="midas.siniestros.reportes.sipac.usuarioCreacion"/></column>
		<column id="listLoteLayoutSipac.estatus" type="ro" width="90"><s:text name="midas.siniestros.reportes.sipac.estatus"/></column>
		<column id="listLoteLayoutSipac.fechaOcurridoDes" type="ro" width="150"><s:text name="midas.siniestros.reportes.sipac.fechaOcurridoDes"/></column>
		<column id="listLoteLayoutSipac.fechaOcurridoHas" type="ro" width="150"><s:text name="midas.siniestros.reportes.sipac.fechaOcurridoHas"/></column>
		<column id="cancelar" type="img" width="80">Cancelar</column>
		<column id="subirLote" type="img" width="80">Consultar</column>
		<column id="descargar" type="img" width="80">Descargar</column>			
	</head>	
	<s:iterator value="listLoteLayoutSipac" status="index" var="listLoteSipac">
		<row id="${index.count}">
			<cell id="loteId"><s:property value="id"/></cell>
			<cell id="loteFC"><s:date name="fechaCreacion" format="dd/MM/yyyy"></s:date></cell>				
			<cell id="loteUsuario"><s:property value="usuarioCreacion"/></cell>
			<cell id="loteEstatus"><s:property value="estatus"/></cell>
			<cell id="loteFOD"><s:date name="fechaOcurridoDesde" format="dd/MM/yyyy"></s:date></cell>
			<cell id="loteFOH"><s:date name="fechaOcurridoHasta" format="dd/MM/yyyy"></s:date></cell>
			<s:if test="estatus == \"En Proceso\"">
				<cell>../img/icons/ico_eliminar.gif^Cancelar Lote^javascript: cancelarLote("<s:property value="id"/>")^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif^No disponible^^_self</cell>
			</s:else>
			<cell>../img/icons/ico_editar.gif^Consultar Lote^javascript: subirLote("<s:property value="id"/>")^_self</cell>			
			<cell>../img/download-icon.jpg^Descargar Registros Lote^javascript: descargarLote("<s:property value="id"/>")^_self</cell>
		</row>
	</s:iterator>
</rows>