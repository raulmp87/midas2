<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script src="<s:url value='/js/midas2/siniestros/sipac/layoutSipac.js'/>"></script>

<s:hidden name="mensaje" id="mensajeTxt"/>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		<s:text name="midas.siniestros.reportes.sipac.reporteLayoutSipac"></s:text>
	</div>
		
	<s:form id="reporteLayoutSipacForm">
		<table id="filtros" width="100%">
			<tr>
				<td width="20%">
					<s:text name="midas.siniestros.reportes.sipac.fechaInicial"></s:text>
				</td>
				<td width="30%">
					<sj:datepicker
						name="fechaInicial" required="true"
						cssStyle="width: 100px;" buttonImage="../img/b_calendario.gif"
						id="fechaInicial" maxlength="10" labelposition="left"
						readonly="false" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						onblur="esFechaValida(this);" changeMonth="true" changeYear="true">
					</sj:datepicker>
				</td>
				<td width="20%">
					<s:text name="midas.siniestros.reportes.sipac.fechaFinal"></s:text>
				</td>
				<td width="30%">
					<sj:datepicker
						name="fechaFinal" required="true"
						cssStyle="width: 100px;" buttonImage="../img/b_calendario.gif"
						id="fechaFinal" maxlength="10" labelposition="left"
						readonly="false" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						onblur="esFechaValida(this);" changeMonth="true" changeYear="true">
					</sj:datepicker>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div class="alinearBotonALaDerecha">
						<midas:boton onclick="javascript:
						imprimirReporteLayoutSipac();" tipo="agregar" texto="Generar Reporte"
						style="width: 150px;"/>
					</div>
				</td>
			</tr>
		</table>
		<br>
		<div id="divCarga" style="position:absolute;"></div>
		<div id="loteSipacGrid" class="w1200 h260" style="background-color:white;overflow:hidden"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</s:form>
</div>

<script type="text/javascript">
	jQuery(function(){
		listarFiltradoGenerico(filtrarLotesPath, "loteSipacGrid", jQuery("#reporteLayoutSipacForm"));
	});
</script>
<script type="text/javascript">
jQIsRequired();
var mensaje = jQuery("#mensajeTxt").val();

if (mensaje != "") {
	if (mensaje.includes("Error:")) {
		mostrarMensajeInformativo(mensaje, '20');
	} else {
		mostrarMensajeInformativo(mensaje, '30');
	}
}
</script>