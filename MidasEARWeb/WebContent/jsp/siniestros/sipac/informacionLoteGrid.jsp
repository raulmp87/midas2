<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="layoutSipac.id" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="layoutSipac.numeroSecuencial" type="ro" width="30" sort="int" >#</column>
		<column id="layoutSipac.tipoOperacion" type="ro" width="50" sort="str" >Tipo Op.</column>
		<column id="layoutSipac.ciaDeudora" type="ro" width="50" sort="str" >Cia D.</column>
		<column id="layoutSipac.folioOrden" type="ro" width="100" sort="str" >DUA</column>
		<column id="layoutSipac.tipoCaptura" type="ro" width="50" sort="str" >Tipo Capt.</column>
		<column id="layoutSipac.siniestroDeudor" type="ro" width="100" sort="str" >Siniestro D.</column>
		<column id="layoutSipac.polizaDeudor" type="ro" width="100" sort="str" >Poliza D.</column>
		<column id="layoutSipac.incisoDeudor" type="ro" width="100" sort="str" >Inciso D.</column>
		<column id="layoutSipac.fechaSiniestro" type="ro" width="100" sort="str" >Fecha Sin.</column>
		<column id="layoutSipac.siniestroAcreedor" type="ro" width="100" sort="str" >Siniestro A.</column>
		<column id="layoutSipac.polizaAcreedor" type="ro" width="100" sort="str" >Poliza A.</column>
		<column id="layoutSipac.incisoAcreedor" type="ro" width="100" sort="str" >Inciso A.</column>
		<column id="layoutSipac.estatus" type="combo"  xmlcontent="1" width="*">Estatus
		    	<option value="2">Enviado</option> 
		    	<option value="3">Error</option>
		    	<option value="4">Aceptado</option>  
		</column>
	</head>
		
	<% int a=0;%>
	<s:iterator value="listLayoutSipac">
		<% a+=1; %>
		<row id="<%=a%>">
		<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSecuencial" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoOperacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciaDeudora" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="folioOrden" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoCaptura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="siniestroDeudor" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="polizaDeudor" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="incisoDeudor" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:date name="fechaSiniestro" format="dd/MM/yyyy"/></cell>			
			<cell><s:property value="siniestroAcreedor" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="polizaAcreedor" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="incisoAcreedor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>