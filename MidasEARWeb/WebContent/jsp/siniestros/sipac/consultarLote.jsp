<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/sipac/layoutSipac.js'/>"></script>

<s:form name="reporteLayoutSipacForm" cssStyle="background:white;">
	<s:hidden name="idLote" id="idLote"/>
	<table width="98%" border="0" id="filtros">
		<tbody>
			<tr>
				<td colspan="6" class="titulo"><s:label value="Consultar Lote" />
				</td>
			</tr>
			<tr>
			<td colspan="6">		
				<div class="alinearBotonALaDerecha">		
				<div id="b_agregar" style="width: 250px">
					<a href="javascript: void(0);"
						onclick="javascript:marcarTodosAceptarLote();return false;">
						Marcar Todos como Aceptados
					</a>
				</div>
				</div>
			</td>
			</tr>
			
		</tbody>
	</table>
	<div>
		<div id="loteGrid" style="width: 98%; height:450px;"></div>
	</div>
	<div style="width: 99%;background:white;">		
		<div id="btn_regresar" class="btn_back w140" style="display: inline; float: right;" >
		<a href="javascript: void(0);" onclick="cerrarConsultaLote();"> 
			<s:text name="midas.boton.regresar" /> 							
			</a>
		</div>
	</div>	
</s:form>
<div>&nbsp;</div>
<script type="text/javascript">
obtenerInformacionLote();
</script>