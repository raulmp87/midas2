<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<html>
	<head>
		<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>	
	</head>
	<body id="contenido">
		<table id="desplegar" class="contenedor">
			<tr>
				<td colspan="2" align="center"><s:text name="midas.siniestros.monitor.ayuda.menu"/></td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/home_L_8x.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.home"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/lines.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.lineas"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/eliminar.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.eliminar"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  width="40px;" height="15px;" src="/MidasWeb/img/maps/on.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.monitor"/>
				</td>
			</tr>		
		</table>
		<table id="desplegar" class="contenedor">
			<tr>
				<td colspan="2" align="center"><s:text name="midas.siniestros.monitor.ayuda.ajustadores"/></td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/car_white.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.noreportes"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/car_yellow.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.conreportes"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/car_blue.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.asignadoreporteactual"/>
				</td>
			</tr>			
		</table>
		<table id="desplegar" class="contenedor">
			<tr>
				<td colspan="2" align="center"><s:text name="midas.siniestros.monitor.ayuda.reportes"/></td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/letter_r_red.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.sinajustador"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/letter_r_yellow.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.conajustador"/>
				</td>
			</tr>
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/letter_r_green.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.contacto"/>
				</td>
			</tr>	
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/letter_r_blue.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.asignadoajustadoractual"/>
				</td>
			</tr>	
			<tr>
				<td align="center">
					<img  src="/MidasWeb/img/maps/letter_r_bluenavy.png" />
				</td>
				<td>
					<s:text name="midas.siniestros.monitor.ayuda.alternoajustadoractual"/>
				</td>
			</tr>			
		</table>
	</body>
</html>
