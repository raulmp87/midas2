<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript"
	src="<s:url value='/js/midas2/siniestros/cabina/monitor/monitorCabina.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/siniestros/cabina/monitor/contador_eXcell.js'/>"></script>
<script type="text/javascript">
	listarReportesAsignacion = '<s:url action="listarReportesAsignacion" namespace="/siniestros/cabina/monitor"/>';
	listarReportesArribo 	 = '<s:url action="listarReportesArribo"	 namespace="/siniestros/cabina/monitor"/>';
	listarReportesTermino	 = '<s:url action="listarReportesTermino" 	 namespace="/siniestros/cabina/monitor"/>';
	exportarExcelUrl    	 = '<s:url action="exportarExcel" 	         namespace="/siniestros/cabina/monitor"/>';
	cabinaId 				 = '<s:property value="usuarioActual.cabina.id"/>';
	paginationEnabled		 = '<s:property value="paginationEnabled"/>';
	alertasActivas   		 = '<s:property value="alertasActivas"/>';
	
	notificacionSound = soundManager.createSound({
		      url: config.baseUrl + '/audio/notificacion/sounds-960-no-trespassing.mp3'
		    });	
</script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<s:form action="listarReportes" namespace="/siniestros/cabina/monitor">
	
	<div style="display: flex; padding-right: 100;">
					<s:select list="ctgOficinas" id="lstOficinaId"
						name="oficinaId"
						onchange="onChangeOficina()"
						headerValue="%{getText('midas.general.seleccione')}" headerKey=""
						cssClass="cajaTextoM2 w300" 
						disabled="true" 					
						key="midas.siniestros.configuracion.horario.ajustador.oficinas" labelposition="left"/>
	</div>
	<div id="divActualizar" style="float: right;">
		<div class="btn_back w140" style="display: inline; float: right; ">
			<a href="javascript: void(0);" onclick="unsubscribeCometd(); aviso();" style="color: red;"
				id="btnDesactivarMonitor"> <s:text name="Desactivar Monitor" /> </a>
		</div>
	</div>
	<div class="titulo">
		<s:text name="midas.siniestros.cabina.monitor.asignacionGrid.title" />
	</div>
	<div id= indicadorAsignacion></div>
	<div align="center" id="asignacionGrid" width="98%" height="150px"
		style="background-color: white; overflow: hidden">
		<pre class="javascript" style="font-family: monospace;">
			
		</pre>
	</div>
	<div><span id="pagingAreaasignacionGrid"></span>&nbsp;<span id="infoAreaasignacionGrid"></span></div>

	<div class="titulo">
		<s:text name="midas.siniestros.cabina.monitor.arriboGrid.title" />
	</div>
	<div id= indicadorArribo></div>
	<div align="center" id="arriboGrid" width="98%" height="150px"
		style="background-color: white; overflow: hidden"></div>
	<div><span id="pagingAreaarriboGrid"></span>&nbsp;<span id="infoAreaarriboGrid"></span></div>

	<div class="titulo">
		<s:text name="midas.siniestros.cabina.monitor.terminoGrid.title" />
	</div>
	<div id= indicadorTermino></div>
	<div align="center" id="terminoGrid" width="98%" height="150px"
		style="background-color: white; overflow: hidden"></div>
	<div><span id="pagingAreaterminoGrid"></span>&nbsp;<span id="infoAreaterminoGrid"></span></div>
		

	<div id="divExcelBtn"  class="w150" style="float:left;">
	     <div class="btn_back w140" style="display: inline; float: right;">
		     <a href="javascript: void(0);" onclick="exportarExcel();">
		         <s:text name="midas.boton.exportarExcel" /> </a>
	     </div>
    </div> 
	<div id="divActualizar" style="float: right;">
		<div class="btn_back w140" style="display: inline; float: left;">
			<a href="javascript: void(0);" onclick="permutarAlertasActivas();"
				id="btnAlertasActivas"> <s:text name="midas.siniestros.cabina.monitor.desactivarAlerta" /> </a>
		</div>
	</div>
</s:form>

<script>
	init();	
</script>