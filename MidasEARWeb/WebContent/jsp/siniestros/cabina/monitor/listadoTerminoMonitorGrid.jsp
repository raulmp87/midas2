<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<s:set value="reporteSiniestrosTermino" var="x"/>

<rows total_count="<s:property value="total"/>" pos="<s:property value="posStart"/>">
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>      
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id"                  type="ro"    hidden="true"  width="12"  sort="str" >id</column>
		<column id="seccion"             type="ro"    hidden="true"  width="65"  sort="str" >seccion</column>
		<column id="oficina"             type="ro"    hidden="true"  width="65"  sort="str" >oficina</column>
		<column id="cveOficina"          type="ro"    hidden="true"  width="65"  sort="str" >cveOficina</column>
		<column id="consecutivo"         type="ro"    hidden="true"  width="65"  sort="str" >consecutivo</column>
		<column id="anio"                type="ro"    hidden="true"  width="65"  sort="str" >anio</column>				
        <column id="numeroReporte"       type="ro"    hidden="false" width="90"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.numeroReporte"     /></column>
        <column id="fechaHoraReporte"    type="ro"    hidden="false" width="140" sort="dateTime_custom"><s:text name="midas.siniestros.cabina.monitor.fechaHoraReporte"  /></column>
        <column id="tiempoTranscurido"   type="conta" hidden="false" width="75"  sort="na">  <s:text name="midas.siniestros.cabina.monitor.tiempoTranscurido" /></column>
        <column id="tiempoParaAlarma"    type="ro"    hidden="true"  width="75"  sort="dateTime_custom"><s:text name="midas.siniestros.cabina.monitor.tiempoParaAlarma"  /></column>
        <column id="tipoAlarma"          type="ro"    hidden="true"  width="15"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.tipoAlarma"        /></column>
        <column id="reporta"             type="ro"    hidden="false" width="180" sort="str"> <s:text name="midas.siniestros.cabina.monitor.reporta"           /></column>
        <column id="telefono"            type="ro"    hidden="false" width="80"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.telefono"          /></column>
        <column id="ciudad"              type="ro"    hidden="false" width="80"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.ciudad"            /></column>
        <column id="ubicacion"           type="ro"    hidden="false" width="80"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.ubicacion"         /></column>
        <column id="ajustador"           type="ro"    hidden="false" width="180" sort="str"> <s:text name="midas.siniestros.cabina.monitor.ajustador"         /></column>
        <column id="citaCruzero"         type="ro"    hidden="false" width="75"  sort="str"> <s:text name="midas.siniestros.cabina.monitor.citaCruzero"       /></column>
        <column id="fechaHoraCita"       type="ro"    hidden="false" width="140" sort="dateTime_custom"><s:text name="midas.siniestros.cabina.monitor.fechaHoraCita"     /></column>
        <column id="fechaHoraContacto"   type="ro"    hidden="false" width="140" sort="dateTime_custom"><s:text name="midas.siniestros.cabina.monitor.fechaHoraContacto" /></column>
        <column id="asegurado"           type="ro"    hidden="false" width="180" sort="str"> <s:text name="midas.siniestros.cabina.monitor.asegurado"         /></column>
        <column id="agente"              type="ro"    hidden="false" width="180" sort="str"> <s:text name="midas.siniestros.cabina.monitor.agente"            /></column>
        <column id="estatus"             type="ro"    hidden="false" width="120" sort="str"> <s:text name="midas.siniestros.cabina.monitor.estatus"           /></column>
	</head>
	<s:iterator value="reporteSiniestrosTermino" var="reporte">
		<row id="<s:property value="id"/>">
			<cell><s:property value="id"             escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[${reporte.seccion.toString()}]]></cell>
			<cell><![CDATA[${reporte.oficina.nombreOficina}]]></cell>
			<cell><![CDATA[${reporte.oficina.claveOficina}]]></cell>
			<cell><![CDATA[${reporte.consecutivo}]]></cell>
			<cell><![CDATA[${reporte.anioReporte}]]></cell>			
			<cell><s:property value="numeroReporte"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaHoraReporte"    format="dd/MM/yyyy HH:mm" /></cell>
			<cell><s:date name="tiempoTranscurido"   format="MM/dd/yyyy HH:mm:ss" />|<s:date name="tiempoParaAlarma"    format="MM/dd/yyyy HH:mm:ss" />|<s:property value="tipoAlarma"     escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="tiempoParaAlarma"    format="dd/MM/yyyy HH:mm:ss 'GMT' '('Z')'" /></cell>
			<cell><s:property value="tipoAlarma"     escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaReporta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="telefono"       escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciudad"         escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ubicacion"      escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ajustador"      escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				  <s:if test="citaCruzero == 'Cita'">
				  	&lt;div style="background-color:red;color:white;" &gt;<s:property value="citaCruzero" escapeHtml="false" escapeXml="true"/>&lt;/div&gt;				
				  </s:if>
				  <s:else>				  	
					<s:property value="citaCruzero"    escapeHtml="false" escapeXml="true"/>
				  </s:else>
			</cell>		
			<cell><s:date name="fechaHoraCita"       format="dd/MM/yyyy HH:mm" />       </cell>
			<cell><s:date name="fechaHoraContacto"   format="dd/MM/yyyy HH:mm" />       </cell>
			<cell><s:property value="asegurado"      escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agente"         escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus"        escapeHtml="false" escapeXml="true"/></cell>					
		</row>
	</s:iterator>
</rows>