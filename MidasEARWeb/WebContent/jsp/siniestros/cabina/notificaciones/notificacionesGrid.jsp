<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="configuracionNotificacion.id" type="ro" hidden="true" align="center"></column>
		<column id="numero" type="ro" width="50" sort="int"></column>
		<column id="oficina" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.configNotificaciones.oficina"/></column>
		<column id="proceso" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.proceso"/></column>
		<column id="movimiento" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.movimiento"/></column>
		<column id="configuracionNotificacion.fechaCreacion}" type="ro" width="*" sort="date_custom"><s:text name="midas.siniestros.cabina.configNotificaciones.fechaCreacion"/></column>
		<column id="configuracionNotificacion.codigoUsuarioCreacion" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.configNotificaciones.usuario"/></column>
		<column id="configuracionNotificacion.esActivo" type="ch" width="80" align="center" >Activo</column>
		<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
	</head>
	<s:iterator value="configuraciones" var="configuracion" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${configuracion.id}]]></cell>
			<cell><![CDATA[${configuracion.id}]]></cell>
			<cell><![CDATA[${configuracion.oficina.nombreOficina}]]></cell>
			<cell><![CDATA[${configuracion.procesoCabina.descripcion}]]></cell>
			<cell><![CDATA[${configuracion.movimientoProceso.descripcion}]]></cell>
			<cell><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${configuracion.fechaCreacion}" /></cell>
			<cell><![CDATA[${configuracion.codigoUsuarioCreacion}]]></cell>
			<cell><![CDATA[${configuracion.esActivo}]]></cell>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:cargarConfiguracionNotificacion(<s:property value="id" />, false);^_self</cell>
			<cell>../img/icons/ico_editar.gif^Editar^javascript:cargarConfiguracionNotificacion(<s:property value="id" />, true);^_self</cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript:eliminarConfiguracionNotificacion(<s:property value="id" />);^_self</cell>		
		</row>
	</s:iterator>
</rows>