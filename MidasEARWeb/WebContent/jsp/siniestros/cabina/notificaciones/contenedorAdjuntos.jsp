<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>

<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.form.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<style>

	.styleFileInput {
		position: relative;
	}
	
	.browseButton {
		border: 1px solid #1cb261;
	/* 	background:url(../images/bt.gif) repeat-x; color:#fff;  */
		font-size: 0.9em;
		color: #fff;
		padding: 3px 8px;
	/* 	border-radius: 4px; */
		background: #1cb261;
		text-transform: uppercase;
		cursor: pointer;
		width: 70px !important;
	}
	
	.browseText {
		width: 150px;
		margin: 0 48px 0 0;
		padding: 2px 0;
	}
	
	input.theFileInput {
		position: absolute;
		top: 12%;
		left: 39%;
		opacity: 0;
		-moz-opacity: 0;
		filter: alpha(opacity : 0);
		z-index: 2;
		width: 80px;
		font-size: 1em;
		cursor: pointer;
	}
	
	input.theFileInput:hover{
	color: #5a409a;
	}
</style>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script>

	jQuery(document).ready(function() {

		jQuery('#adjunto').css("visibility", "hidden");
		jQuery('#browseButton').click(function(e) {
			e.preventDefault();
			jQuery('#adjunto').trigger('click');
		});
		iniciaArchivosAdjuntosGrid();
		jQuery('input[type="text"]').keypress(function(e) {
			e.preventDefault();
		});

		jQuery('#adjunto').change(function() {
			jQuery('#browseText').val(jQuery(this).val());
		});

		// if condition for Webkit and IE
		if (jQuery.browser.webkit || jQuery.browser.msie) {
			jQuery('.theFileInput').css('left', '39%');
			jQuery('.theFileInput').css('top', '12%');
			jQuery('.theFileInput').css('cursor', 'pointer');
		}
		jQuery("#adjuntos").ajaxForm({
			beforeSend : function() {
			},
			complete : function(xhr) {
				/* status.html(xhr.responseText); */
				iniciaArchivosAdjuntosGrid();
				jQuery('#browseText').val('');
				jQuery('#adjunto').val('');
			}
		});
	});
</script>
<div class="titulo">
	<s:text name="midas.configuracionNotificaciones.adjuntos.titulo"/>
</div>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/notificaciones/configuracionNotificaciones.js'/>"></script>
<form method="POST" action="/MidasWeb/siniestros/cabina/notificaciones/agregarAdjunto.action" enctype="multipart/form-data" name="adjuntos" id="adjuntos"  class="">
<s:hidden name="configuracionNotificacion.id" id="configuracionId"></s:hidden>

<div class="divContenedorO" style="padding-left: 10px; padding-bottom: 10px; padding-top: 10px;">
	<div class="styleFileInput" style="padding-left: 5px;">
		<input type="text"  size="45"  class="setNew txtfield" name="nombreArchivo" id="browseText"/> 
		<input type="file" class="theFileInput"	size="1" name="adjunto" id="adjunto" />
	</div> 
	<div style="padding-top: 5px; padding-left: 0px !important; ">
	<input type="button"  class="btn_back w140" id="browseButton" value="Buscar">
	<input type="submit" class="btn_back w140" value="Adjuntar">
	</div>
</div>
</form>
<div class="titulo">
	<s:text name="midas.configuracionNotificaciones.adjuntos.listado"/>
</div>
<div id="archivosAdjuntosGrid" style="margin:10px; min-height: 200px; max-height: 350px;">
</div>
<div class="contenedorConFormato" ><s:text name="midas.configuracionNotificaciones.adjuntos.mensajepermitido"/> </div>
<div class="btn_back w140" style="display: inline; float: right;">
	 <a href="javascript: void(0);" onclick="parent.cerrarAdjuntos();">	
	 <s:text name="midas.boton.cerrar" /> </a>
</div>

