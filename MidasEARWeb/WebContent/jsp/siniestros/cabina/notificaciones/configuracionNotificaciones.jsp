<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/notificaciones/configuracionNotificaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
	var obtenerConfiguraciones = '<s:url action="obtenerConfiguraciones" namespace="/siniestros/cabina/notificaciones"/>';
	var mostrarAdjuntos = '<s:url action="mostrarAdjuntos" namespace="/siniestros/cabina/notificaciones"/>';
	var busquedaGeneralPath = '<s:url action="busquedaGeneral" namespace="/siniestros/cabina/notificaciones"/>';
	var cargarConfiguracionPath = '<s:url action="cargarConfiguracion" namespace="/siniestros/cabina/notificaciones"/>';
	var cargaDestinatariosPath = '<s:url action="cargaDestinatarios" namespace="/siniestros/cabina/notificaciones"/>';
	var guardarConfiguracionPath = '<s:url action="guardarActualizarConfiguracion" namespace="/siniestros/cabina/notificaciones"/>';
	var eliminarConfiguracionPath = '<s:url action="eliminarConfiguracion" namespace="/siniestros/cabina/notificaciones"/>';
	var asignarEstatusPath = '<s:url action="asignarEstatusConfiguracion" namespace="/siniestros/cabina/notificaciones"/>';
</script>
<s:form id="notificacionesForm" action="guardarActualizarConfiguracion" namespace="/siniestros/cabina/notificaciones" name="notificacionesForm">
	<s:hidden id="accion"/>
	<s:hidden name="configuracionNotificacion.id" id="configuracionId"></s:hidden>
	
	<table id="filtrosM2" width="98%">
		<tr>
			<td colspan="7">
				<div class="titulo" style="width:100%">
					<s:text name="midas.siniestros.cabina.configNotificaciones.title"/>	
				</div>
			</td>
		</tr>
		<tr>
			<td width="25%">	
				<table align="center">
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.siniestros.cabina.configNotificaciones.oficina"/>
							</span>
						</td>
						<td>
							<s:select id="oficina" name="configuracionNotificacionDTO.oficinasId" multiple="true"
							list="oficinas" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
							listKey="key" listValue="value" cssClass="cajaTextoM2 w200"/>
							<s:hidden id="oficina"/>					
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.proceso"/>
							</span>
						</td>
						<td>
							<s:select id="procesos" name="configuracionNotificacionDTO.procesoCabinaId"
							list="procesos" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
							listKey="key" listValue="value" cssClass="cajaTextoM2 w200"
							onselect="onChangeProceso('movimientos')" onchange="onChangeProceso('movimientos')" />
							<s:hidden id="proceso"/>					
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.movimiento"/>
							</span>
						</td>
						<td>
							<s:select id="movimientos"  name="configuracionNotificacionDTO.movimientoId"
							list="movimientosCabina" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
							listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200 "/>
							<s:hidden id="movimiento"/>
						</td>			
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.modoEnvio"/>
							</span>
						</td>
						<td>
							<s:select id="modoEnvio"  name="configuracionNotificacionDTO.modoEnvioId"
								list="modosEnvio" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
								listKey="key" listValue="value" cssClass="cajaTextoM2 w200 "/>
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.agenteId"/>
							</span>
						</td>
						<td>
							<s:textfield id="agenteId"  name="configuracionNotificacionDTO.agenteId" 
								cssClass="cajaTextoM2 w200" maxlength="5"/>
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.numPoliza" />
							</span>
						</td>
						<td>
							<s:textfield id="numPoliza"  name="configuracionNotificacionDTO.numPoliza"
							  cssClass="cajaTextoM2 w200" maxlength="20" />
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size: 11px;font-family:arial;" >
								<s:text name="midas.configuracionNotificaciones.numInciso"/>
							</span>
						</td>
						<td>
							<s:textfield id="numInciso"  name="configuracionNotificacionDTO.numInciso" 
								cssClass="cajaTextoM2 w200" maxlength="30"/>
						</td>
					</tr>
				</table>
			</td>
			<td width="25%">	
				<table align="center">
					<tr>	
						<td class="contenedorConFormato">
							<s:label>Tipos Destinatario</s:label>
							<s:iterator value="tiposDestinatario"> 
						  		<s:radio theme="simple" name="configuracionNotificacionDTO.tipoDestinatarioId" cssClass="destinatario" onclick="javascript: onChangeTipoDestinatario(this.value);" list="#{key:value}"/><br>
							</s:iterator>
<%-- 							<s:radio label="Tipos Destinatario" list="tiposDestinatario" id="tiposDestinatario" cssClass="destinatario" --%>
<!-- 								listKey="key" listValue="value" onclick="javascript: onChangeTipoDestinatario(this.value);"  name="configuracionNotificacionDTO.tipoDestinatarioId" /> -->
						</td>
					</tr>
				</table>
			</td>
			<td width="25%">
				<div id="divTipoDestinatario">
					<table align="center">
						<tr>	
							<td class="contenedorConFormato">
								<s:label>Tipos de Correo</s:label>
								<s:iterator value="tiposCorreoEnvio"> 
						  			<s:radio theme="simple" name="configuracionNotificacionDTO.correoEnvio" cssClass="correos" onclick="javascript: onChangeTipoDestinatario(this.value);" list="#{key:value}"/><br>
								</s:iterator>
<%-- 								<s:radio label="Tipos de Correo" list="tiposCorreoEnvio" id="tipoCorreos" cssClass="correos"  --%>
<!-- 									listKey="key" listValue="value" onclick="javascript: void(0);" name="configuracionNotificacionDTO.correoEnvio" /> -->
							</td>
						</tr>
					</table>
				</div>
				<div id="divTipoDestinatarioOtros" style="display:none">
					<table class="contenedorConFormato">
						<tr>
							<td>
								<s:text name="midas.configuracionNotificaciones.puesto"/>
							</td>
							<td>
								<s:textfield id="puesto" name="configuracionNotificacionDTO.puesto"
									labelposition="left" cssClass="cajaTextoM2 w200"/>
							</td>
						</tr>
						<tr>
							<td>
								<s:text name="midas.configuracionNotificaciones.nombre"/>
							</td>
							<td>
								<s:textfield id="nombre" name="configuracionNotificacionDTO.nombre"
									labelposition="left" cssClass="cajaTextoM2 w200"/>
							</td>
						</tr>
						<tr>
							<td>
								<s:text name="midas.configuracionNotificaciones.correo"/>
							</td>
							<td>
								<s:textfield id="correo" name="configuracionNotificacionDTO.correo"
									labelposition="left" cssClass="cajaTextoM2 w200"/>
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td width="10%">
				<table>
					<tr>
						<td>
							<div class="btn_back w100">
								<a href="javascript: void(0);"
									onclick="limpiarNotificaciones();">
									<s:text name="midas.boton.limpiar" />
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="btn_back w100">
								<a href="javascript: void(0);"
									onclick="busquedaConfiguracion();" class="icon_buscar">
									<s:text name="midas.boton.buscar" />
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="btn_back w100">
								<a href="javascript: void(0);" id="btnAgregar"
									onclick="agregarDestinatario();">
									<s:text name="midas.boton.agregar" />
								</a>
							</div>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
	<br>
	<table id="filtrosM2" width="98%">
	  <tr>
	    <td><s:text name="Notas"/></td>
	    <td><s:textarea id="notas" cssClass="textarea" cssStyle="width:800px;height:50px;"/></td>
	    <td>
	    	<div class="btn_back w130">
				<a href="javascript: void(0);"
					onclick="ventanaAdjuntos()" id="btnAdjuntar">
					<s:text
						name="midas.boton.Adjuntar" />
				</a>
			</div>
	    </td>
	  </tr>
	</table>
	<br>
	<div align="center" id="destinatariosGrid" width="98%" height="120px" style="background-color:white;overflow:hidden"></div>
	<div class="titulo">
		<s:text name="midas.siniestros.cabina.configNotificaciones.listado.title"/>	
	</div>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="notificacionesGrid" width="98%" height="150px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<div id="divGuardar" style="float:right;">
		<div class="btn_back w140"  style="display:inline; float: left; ">
			<a href="javascript: void(0);"
				onclick="guardarConfiguracion();" id="btnGuardar">
				<s:text
					name="midas.boton.guardar" />
			</a>
		</div>
	</div>	
</s:form>
<script>
	inicializarListadoConfiguraciones();
</script>