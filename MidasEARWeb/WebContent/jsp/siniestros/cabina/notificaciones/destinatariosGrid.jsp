<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="modo" type="ro" width="*" hidden="true"></column>
		<column id="modoDesc" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.configNotificaciones.modoEnvio"/></column>
		<column id="tipoDestinatario" type="ro" width="*" hidden="true"></column>
		<column id="tipoDestinatarioDesc" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.tipoDestinatario"/></column>
		<column id="tipoCorreo" type="ro" width="*" hidden="true"></column>
		<column id="tipoCorreoDesc" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.configNotificaciones.tipoCorreo"/></column>
		<column id="puesto" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.puesto"/></column>
		<column id="nombre" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.nombre"/></column>
		<column id="correo" type="ro" width="*" sort="str"><s:text name="midas.configuracionNotificaciones.correo"/></column>
		<column id="acciones" type="ro" width="*"><s:text name="midas.general.acciones"/></column>
	</head>
	<s:iterator value="destinatariosList" var="destinatario" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${destinatario['modoEnvio']}]]></cell>
			<cell><![CDATA[${destinatario['modoEnvioDesc']}]]></cell>
			<cell><![CDATA[${destinatario['tipoDestinatario']}]]></cell>
			<cell><![CDATA[${destinatario['tipoDestinatarioDesc']}]]></cell>
			<cell><![CDATA[${destinatario['correoEnvio']}]]></cell>
			<cell><![CDATA[${destinatario['correoEnvioDesc']}]]></cell>
			<cell><![CDATA[${destinatario['puesto']}]]></cell>
			<cell><![CDATA[${destinatario['nombre']}]]></cell>
			<cell><![CDATA[${destinatario['correo']}]]></cell>
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript:eliminarDestinatario(<s:property value="#{index.count}" />);^_self</cell>		
		</row>
	</s:iterator>
</rows>