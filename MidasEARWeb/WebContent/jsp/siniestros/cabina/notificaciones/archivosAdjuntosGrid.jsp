<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<rows>
	<head>		
		 <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>               
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="id" type="ro" width="200" sort="int" hidden="true">id</column>
        <column id="archivo" type="ro" width="*" sort="str" hidden="false">	<s:text name="midas.configuracionNotificaciones.adjuntos.archivoadjunto"/></column>
		<column id="acciones" type="img" colspan="2" width="70" sort="na" align="center"><s:text name="midas.general.acciones" /></column>
	</head>	 
	<s:iterator value="archivosAdjuntos" var="notList"  status="row">
		<row id="<s:property value="#row.index"/>">
			<cell>${notList.id}</cell>
			<cell>${notList.descripcion}</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar archivo adjunto^javascript:confirmaEliminacionArchivoAdjunto(${notList.id});^_self</cell>			
		</row>
	</s:iterator> 		
</rows>
