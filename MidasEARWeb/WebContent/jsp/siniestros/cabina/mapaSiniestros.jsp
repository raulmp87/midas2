<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ taglib prefix="sj" uri="/struts-jquery-tags"%> --%>
<%-- <%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> --%>
<%-- <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%> --%>
<%-- <%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%> --%>

<style>

.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 350px;
}

#pac-input:focus {
  border-color: #4d90fe;
}


.pac-container {
  font-family: Roboto;
  z-index: 100000000 !important; 
}


</style>

<!-- Si ubica las direcciones -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&client=gme-segurosafirmesade&libraries=places&region=MX"></script>

<!-- Carga en IE -->
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/google/maps/gMaps.js?sensor=false'/>"></script> --%>

<div id="map" style="width:100px;height:100px;position: relative;">	
</div>
<input id="pac-input" class="controls" type="text" placeholder="B&uacute;squeda para confirmaci&oacute;n de ubicaci&oacute;n">
<script type="text/javascript" src="<s:url value='/js/midas2/google/maps/generalFunctions.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mapaSiniestros.js'/>"></script>