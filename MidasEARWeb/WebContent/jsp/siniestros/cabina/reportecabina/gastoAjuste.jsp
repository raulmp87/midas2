<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script> --%>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_splt.js'/>"></script>
<sj:head/>
<script type="text/javascript" src="<s:url value='/js/ajaxScript.js'/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar_start.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/gastoAjuste.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}
.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
	font-size: 9px;
}

</style>


<script type="text/javascript">
	var mostrarGastoAjustePath = '<s:url action="cargarDatosAjuste" namespace="/siniestros/reporte/gastoAjuste"/>';
	var buscarNombrePrestadorPath = '<s:url action="buscarNombrePrestador" namespace="/siniestros/reporte/gastoAjuste"/>';	
	var buscarNombreTallerPath = '<s:url action="buscarNombreTaller" namespace="/siniestros/reporte/gastoAjuste"/>';	
	var buscarDatosPrestadorPath = '<s:url action="buscarDatosPrestador" namespace="/siniestros/reporte/gastoAjuste"/>';
	var mostrarGastoPath         = '<s:url action="mostrarGasto" namespace="/siniestros/reporte/gastoAjuste"/>';
	var nuevoGastoPath         = '<s:url action="nuevo" namespace="/siniestros/reporte/gastoAjuste"/>';
	var onChangeTipoPrestadorPath	= '<s:url action="onChangeTipoPrestador" namespace="/siniestros/reporte/gastoAjuste"/>';
	var valeTallerPath	= '<s:url action="mostrarVale" namespace="/siniestros/cabina/reporteCabina/valeTaller"/>';
</script>

<div class="titulo">
	<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.ListaTitle"/>	
</div>	
<div id="indicador"></div>
<div id="gastoAjusteGrid" class="dataGridConfigurationClass" style="width: 98%;max-height:200px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:form id="gastoAjusteForm" action="guardar" namespace="/siniestros/reporte/gastoAjuste" name="gastoAjusteForm">
	<div id="gastoAjusteCatalogo">
		<s:hidden name="idToReporte" id="reporteCabinaId"></s:hidden>
		<s:hidden name="gastoAjusteDTO.id" id="gastoId"></s:hidden>
		<s:hidden name="gastoAjusteDTO.prestadorAjusteId" id="prestadorId"></s:hidden>
		<s:hidden name="gastoAjusteDTO.tallerAsignadoId" id="tallerAsignadoId"></s:hidden>
		<s:hidden name="gastoAjusteDTO.mostrarSeccionGrua" id="mostrarSeccionGrua"></s:hidden>
		<s:hidden name="gastoAjusteDTO.origen" id="origen"></s:hidden>
		<s:hidden name="prestadorOrigen" id="prestadorOrigen"></s:hidden>
		<s:hidden name="soloConsulta" id="h_soloConsulta"/>
		<s:hidden id="urlRedirect" name="urlRedirect"></s:hidden>
		<s:if test="%{gastoAjusteDTO.id != null}">	
			<s:set id="readOnly" value="true" />
		</s:if>
		<s:else>
			<s:set id="readOnly" value="false" />
		</s:else>
		
		<div class="titulo">
			<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.prestador"/>	
		</div>	
		<table id="filtrosM2" style="width: 98%;">
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tipoPrestador"/>	
				</td>
				<td>
					<s:select id="tipoPrestador"
								labelposition="left" 
								name="tipoPrestadorId"
								disabled="readOnly"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="onChangeTipoPrestador(this.value, jQuery('#reporteCabinaId').val())"
						  		list="tiposPrestador" listKey="key" listValue="value"  
						  		cssClass="txtfield setNew" /> 	
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.nombrePrestador"/>	
				</td>
				<td>
					<div>
						<sj:autocompleter cssStyle="display:none;"/>
						 <sj:textfield labelposition="top" readonly="readOnly" 
							cssClass="cajaTextoM2 w200 setNew"
						    id="prestadorNombre" name="nombrePrestador" /> 
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.rfc"/>	
				</td>
				<td>
					<s:textfield id="txt_rfc" name="gastoAjusteDTO.rfc" readonly="true"
							labelposition="left" cssClass="txtfield jQalphanumeric jQrestrict setNew" size="20"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.curp"/>	
				</td>
				<td>
					<s:textfield id="txt_curp" name="gastoAjusteDTO.curp" readonly="true" size="25"
							labelposition="left" cssClass="txtfield jQalphanumeric jQrestrict setNew" />
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.factura"/>	
				</td>
				<td>
					<s:textfield id="txt_factura" name="gastoAjusteDTO.numeroFactura"
							labelposition="left" cssClass="txtfield  jQalphanumeric jQrestrict setNew" maxlength="50"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.motivoSituacion"/>	
				</td>
				<td>
					<s:select id="motivoSituacion"
								labelposition="left" 
								name="gastoAjusteDTO.motivoSituacion"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="motivosSituacion" listKey="key" listValue="value"  
						  		cssClass="txtfield setNew" /> 	
				</td>
			</tr>
		</table>
		<s:if test="gastoAjusteDTO.mostrarSeccionGrua">
			<div class="titulo">
				<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.datosGrua"/>	
			</div>
			<table id="filtrosM2" style="width: 98%;">
				<tr>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.numReporte"/>	
					</td>
					<td>
						<s:textfield id="txt_num_reporte" name="gastoAjusteDTO.numeroReporteExterno"
								labelposition="left" cssClass="txtfield jQalphanumeric jQrestrict setNew" maxlength="50"/>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tipoGrua"/>	
					</td>
					<td>
						<s:select id="tipoGrua"
									labelposition="left" 
									name="gastoAjusteDTO.tipoGrua"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="tiposGrua" listKey="key" listValue="value"  
							  		cssClass="txtfield setNew" /> 	
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tallerAsignado"/>	
					</td>
					<td>
						<div>
							<sj:autocompleter cssStyle="display:none;"/>
							 <sj:textfield labelposition="top" name="nombreTaller" 
								cssClass="cajaTextoM2 w200 setNew"
							    id="tallerNombre" /> 
						</div>
					</td>
				</tr>
			</table>
		</s:if>
		<br>
		<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
			<tr>
				<td>
					<div class="btn_back w140" style="display: inline; float: left;" id="b_nuevaVersion">
						<a href="javascript: void(0);" onclick="nuevoGastoAjuste(jQuery('#reporteCabinaId').val());"> 
						<s:text name="midas.boton.nuevo" /> </a>
					</div>	
					<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
						<a href="javascript: void(0);" onclick="cerrarVentanaAjuste();"> 
						<s:text name="midas.boton.cerrar" /> </a>
					</div>	
					<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
						<a href="javascript: void(0);" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea guardar el ajuste?')){guardarAjuste(jQuery('#gastoId').val());}"> 
						<s:text name="midas.boton.guardar" /> </a>
					</div>	
					<s:if test="tipoPrestadorId == 'GRUA'">
					<div class="btn_back w80" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="imprimirValeTaller();"> 
						<s:text name="midas.boton.imprimir" /> </a>
					</div>	</s:if>	
				</td>							
			</tr>
		</table>
	</div>
</s:form>		
<script>

function cerrarVentanaAjuste(){
	parent.cerrarVentanaModal("mostrarGastoAjuste", null);}

	inicializarListadoGastoAjuste();
	
	jQuery(document).ready(function() {	
		
		if (jQuery('#h_soloConsulta').val() == 1) {
			setConsulta();
		} else {
			jQuery(function(){
				jQuery('#prestadorNombre' ).autocomplete({
	               source: function(request, response){  
	               		jQuery.ajax({
				            type: "POST",
				            url: buscarNombrePrestadorPath,
				            data: {	nombrePrestador	:	request.term,
	            				   	tipoPrestadorId : $('#tipoPrestador').val()},              
				            dataType: "xml",	                
				            success: function( xmlResponse ) {
				           		response( jQuery( "item", xmlResponse ).map( function() {	
									return {
										value: jQuery( "nombre", this ).text(),
										prestadorId: jQuery( "prestadorId", this ).text()
									}
								}));			           
	               		}
	               	})},
	               minLength: 4,
	               delay: 500,
			       select: function( event, ui ) {
			            	try{
			            		jQuery('#prestadorOrigen').val(ui.item.prestadorId);   
			            		buscarDatosPrestador(ui.item.prestadorId);  
			            	}catch(e){
			            		alert("error" + e);
			            	}
				               
			               }
	               		          
			     });
			});
			jQuery(function(){
				jQuery('#tallerNombre' ).autocomplete({
	               source: function(request, response){  
	               		jQuery.ajax({
				            type: "POST",
				            url: buscarNombreTallerPath,
				            data: {	nombreTaller	:	request.term,
	            				   	tipoPrestadorId : $('#tipoPrestador').val()},              
				            dataType: "xml",	                
				            success: function( xmlResponse ) {
				           		response( jQuery( "item", xmlResponse ).map( function() {	
									return {
										value: jQuery( "nombre", this ).text(),
										prestadorId: jQuery( "prestadorId", this ).text()
									}
								}));			           
	               		}
	               	})},
	               minLength: 4,
	               delay: 500,
			       select: function( event, ui ) {
			            	try{
			            		setPrestador('tallerAsignadoId', ui.item.prestadorId)   
			            	}catch(e){
			            	}
				               
			               }		          
			     });
			});	
		}
		
		
	});
</script>