<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/solicitudautorizacion/solicitudAutorizacion.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<script type="text/javascript">
	var mostrarDetalleInciso = '<s:url action="mostrarDetalleInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarDatosRiesgo = '<s:url action="mostrarDatosRiesgo" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarDatosVehiculo = '<s:url action="mostrarDatosVehiculo" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var buscarPoliza = '<s:url action="buscarPoliza" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarEndoso = '<s:url action="buscarPoliza" namespace="/siniestros/cabina/reporteCabina/consulta"/>';
	var mostrarSolicitudPath = '<s:url action="mostrarCartaCobertura" namespace="/siniestros/cabina/reporteCabina/cartaCobertura"/>';
	var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
</script>

<style type="text/css">

.divContenedorO {
	border: 1px solid #28b31a;
}

.formulario , .label{
	font-size: 10px;
	font-weight: bold;
	text-align: center;	
}



</style>

<s:form  id="busquedaIncisoPolizaForm">
<s:hidden id="busquedaPolizas" name="busquedaPolizas"/>
	<!-- Parametro de la forma para que sea reutilizable -->
<s:hidden id="fechaReporteSiniestro" name="fechaReporteSiniestro"/>
<s:hidden id="solicitudPoliza" name="solicitudPoliza"/>
<s:hidden id="idReporte" name="idReporte"/>
<s:hidden id="busquedaRealizada" name="busquedaRealizada"/>
<s:hidden id="incisoContinuityId" name="incisoContinuityId"/>
<s:hidden id="validOn" name="validOn"/>
<s:hidden id="recordFrom" name="recordFrom"/>
<s:hidden name="validOnMillis" id="validOnMillis"></s:hidden>
<s:hidden name="recordFromMillis" id="recordFromMillis"></s:hidden>
<s:hidden name="horaOcurrido" id="horaOcurrido"></s:hidden>
<s:hidden name="fechaReporteSiniestroMillis" id="fechaReporteSiniestroMillis"></s:hidden>
	
	<div class="titulo"><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.tituloBusqueda" /> </div>
	<table  class=" divContenedorO" width="98%">
	
		<tr>
		
			<td> <s:checkbox name="filtroBusqueda.servicioPublico"  id="servicioPublico"  > </s:checkbox> </td>	
			<td class="formulario" ><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioPublico" /> </td>
			
			<td class="formulario" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie" />
			<s:textfield name="filtroBusqueda.autoInciso.numeroSerie" id="numeroSerie" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
			
			<td class="formulario"  ><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.noPlanca" /> 
			<s:textfield name="filtroBusqueda.autoInciso.placa" id="placa" cssClass="cajaTexto w150 alphaextra"></s:textfield></td>
			
			<td class="formulario" > <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza" />
			<s:textfield name="filtroBusqueda.numeroPoliza" id="poliza" cssClass="cajaTexto w200 alphaextra" onfocus="javascript: new Mask('####-########-##').attach(this)" >
			</s:textfield></td>
			
			
			
		</tr>
		
		
		<tr>
			<td> <s:checkbox name="filtroBusqueda.servicioParticular" id="servicioParticular" > </s:checkbox> </td>
			<td  class="formulario"  ><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioParticular" /> </td>
		
			<td colspan="2" class="subtitulo" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.vigenciaInciso" /> </td>
			
			
			
			<td class="formulario" > <s:text name="midas.poliza.numeroPoliza" /></td>
			<td><s:textfield name="filtroBusqueda.cvePolizaMidas" id="polizaMidas" cssClass="cajaTexto w200 alphaextra" onfocus="javascript: new Mask('########').attach(this)" >
			</s:textfield></td>
			
			
			
		
		</tr>
		
		
		
		<tr>
		
			<td class="formulario" ><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.noMotor" /> </td>
			<td><s:textfield name="filtroBusqueda.autoInciso.numeroMotor" id="motor" cssClass="cajaTexto w150 alphaextra"></s:textfield></td>
		
		
		
			
			<td  align="right"> <sj:datepicker name="filtroBusqueda.fechaIniVigencia"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2" 
					label="Desde"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
				
			</td>
			
			<td ><sj:datepicker name="filtroBusqueda.fechaFinVigencia"
					id="txtFechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					label="Hasta"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			
			
			<td class="formulario" > <s:text name="midas.poliza.numeroPolizaSeycos" /> </td>
			<td><s:textfield name="filtroBusqueda.numPolizaSeycos" id="polizaSeycos" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			<s:checkbox name="filtroBusqueda.busquedaPolizaSeycos" id="busquedaPolizaSeycos" style="display:none" cssClass="cajaTexto"/>
		</tr>
		
		<tr>
		
		<td class="formulario"  ><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.asegurado" /> </td>
			<td><s:textfield name="filtroBusqueda.nombreContratante" id="asegurado" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>


			<td class="formulario"> <s:text name="midas.cotizacion.numeroinciso" /> 
			<s:textfield name="filtroBusqueda.numeroInciso" id="numeroInciso" cssClass="cajaTexto w200 jQnumeric"></s:textfield></td>
			
			<td class="formulario"> <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.folioCarta" />
			<s:textfield name="filtroBusqueda.idToSolicitud" id="idToSolicitud" cssClass="cajaTexto w200"></s:textfield></td>
			
			
			<td class="formulario">
				<s:text name="midas.folio.reexpedible.field.numero.folio.poliza"/>
				<s:textfield name="filtroBusqueda.numeroFolio" id="numeroFolio"
						cssClass="cajaTexto w200"></s:textfield>
			</td>
		
		
		</tr>

		<tr>		
		<td colspan="8">		
			<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td  class= "guardar">
						<div class="alinearBotonALaDerecha" style="display: inline; float: right; margin-left: 2%; " id="b_buscar">
							 <a href="javascript: void(0);" onclick="realizarBusquedaIncisoPoliza();" >
							 <s:text name="midas.boton.buscar" /> </a>
						</div> 
						<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
							<a href="javascript: void(0);" onclick="limpiarFormulario();"> 
							<s:text name="midas.boton.limpiar" /> </a>
						</div>	
					</td>							
				</tr>
			</table>				
		</td>		
	</tr>


	</table>
	
	<br>
	
	<div id="resultadoPolizaIncisoGrid" style="width:98%;height:200px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<div id="infoArea"></div>
	<br />
	<br />

</div>	
	<div class="titulo"><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.tituloDatosVehiculos" /> </div>
	
	<div id="datosVehiculoSeleccionado" > 	
	<table  class=" divContenedorO"  width="98%">

		<tr>
			<td width="90px" class="formulario"  ><s:text name="midas.general.descripcion" /> </td>
			<td colspan="3"><s:textfield  name="detalleInciso.autoInciso.descripcionFinal" id="txtClave" cssClass="cajaTexto w600 alphaextra"  readonly="true"></s:textfield></td>
			
	
			<td width="90px" class="formulario"  ><s:text name="midas.general.estatus" /> </td>
			<td><s:textfield name="detalleInciso.descEstatus"  id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		
		<tr>
			<td width="90px" class="formulario" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.vigenciaDesde" /> </td>
			<td><s:textfield name="detalleInciso.fechaVigenciaIniRealInciso"  id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario" ><s:text name="midas.suscripcion.cotizacion.fecha.hasta" /> </td>
			<td><s:textfield  name="detalleInciso.fechaFinVigencia" id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario" ><s:text name="midas.fuerzaventa.negocio.motivo" /> </td>
			<td><s:textfield name="detalleInciso.motivo"  id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		
		<tr>
			<td width="90px"  class="formulario"  ><s:text name="Nombre de Linea " /> </td>
			<td colspan="3" ><s:textfield  name="detalleInciso.nombreLinea"  id="txtClave" cssClass="cajaTexto w600 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario" ><s:text name="midas.emision.consulta.siniestro.numeroinciso" /> </td>
			<td><s:textfield  name="detalleInciso.numeroInciso" id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		


	</table>
		<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td  class= "alinearBotonALaDerecha">
<!-- 						<div class="btn_back w140" style="display: inline; float: right; "> -->
<!-- 							 <a href="javascript: void(0);" onclick="regresarReporteBusqueda();" > -->
<%-- 							 <s:text name="midas.boton.cerrar" /> </a> --%>
<!-- 						</div> -->
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="consultaInciso();"> 
							<s:text name="midas.boton.consultar" /> </a>
						</div>	
					</td>							
				</tr>
				
	</table>
		
</div>	


</s:form>

<script type="text/javascript">
  getIncisosPoliza();
</script>




