<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="100"  align="center" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.seguroObligatorio" />  </column>
		<column  type="ro"  width="110" align="center"> <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza" />   </column>
		<column  type="ro"  width="150"  align="center" > <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie" />  </column>
		<column  type="ro"  width="70"  align="center" > <s:text name="midas.emision.consulta.siniestro.numeroinciso" />  </column>
		<column  type="img"  width="70"  align="center" > <s:text name="midas.general.estatus" />  </column>
	    <column  type="ro"  width="250"  align="center" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.asegurado" />  </column>
	    <column  type="ro"  width="80"  align="center" > <s:text name="midas.negocio.seccion.estilos.marcas" />  </column>
	    <column  type="ro"  width="80"  > <s:text name="midas.excepcion.suscripcion.auto.modelo" />  </column>
	    <column  type="ro"  width="170"  align="center" > <s:text name="midas.general.tipovehiculo" />  </column>
	    <column  type="ro"  width="130"  align="center" > <s:text name="midas.emision.consulta.poliza.claveseycos" />  </column>
	    <column  type="ro"  width="120"  align="center" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.noMotor" />  </column>
	    <column  type="ro"  width="70"  align="center" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.noPlanca" />  </column>
	    <column  type="ro"  width="70"  align="center" > <s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.solicitudPoliza" />  </column>
	    <column  type="ro"  width="70"  align="center" > <s:text name="midas.folio.reexpedible.column" />  </column>
	    
		
	</head>
<%-- <s:if test="busquedaRealizada"> 
	<s:if test="incisosEmpty">
	
		<row id="error1">
		<cell></cell>
		<cell></cell>
		<cell></cell>
		<cell></cell>
			<cell>
			<s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.mensaje1" />
			</cell>
			
		</row>
		<row id="error2">
		<cell></cell>
		<cell></cell>
		<cell></cell>
		<cell></cell>
			<cell>
			<s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.mensaje2" />
			</cell>
			
		</row>
		<row id="error3">
		<cell></cell>
		<cell></cell>
		<cell></cell>
		<cell></cell>
			<cell>
			<s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.mensaje3" />
			</cell>
			
		</row>
	</s:if>
</s:if--%>

	<s:iterator value="incisos">
		
		<row id="<s:property value="#row.index"/>">
			 <cell><s:if test="provieneSeguroObligatorio==0">No</s:if>
		    	<s:elseif test="provieneSeguroObligatorio==1">Si</s:elseif>
			</cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:if test="estatus==0">/MidasWeb/img/ico_green.gif</s:if><s:elseif test="estatus==1">/MidasWeb/img/ico_red.gif</s:elseif><s:elseif test="estatus==2">/MidasWeb/img/ico_red.gif</s:elseif><s:else>/MidasWeb/img/ico_na.gif</s:else></cell>
		    <cell><s:property value="nombreContratante" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="marca" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="modeloVehiculo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numPolizaSeycos" escapeHtml="false" escapeXml="true"/></cell>		    
		    <cell><s:property value="numeroMotor" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="placa" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/></cell>
		   

			
			<userdata name="valorIncisoContinuityId"><s:property value="incisoContinuityId" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorValidOn"><s:property value="validOn" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorRecordFrom"><s:property value="recordFrom" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorValidOnMillis"><s:property value="validOnMillis" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorRecordFromMillis"><s:property value="recordFromMillis" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorIdToSolicitud"><s:property value="idToSolicitud" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valoridToSolicitudDataEnTramite"><s:property value="idToSolicitudDataEnTramite" escapeHtml="false" escapeXml="true"/></userdata>
			<userdata name="valorNumeroSerie"><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></userdata>
		</row>
	</s:iterator>
	
</rows>