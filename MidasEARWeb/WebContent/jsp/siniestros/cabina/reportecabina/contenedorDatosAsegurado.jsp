<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>          
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<script type="text/javascript">
	
var buscarDireccionesClientePath = '<s:url action="buscarDireccionesCliente" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
</script>
	<s:hidden name="idCliente" id="h_idCliente"></s:hidden>
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.consultaIncisoPoliza.consultaDatosAsegurado.titulo"/>	
	</div>		
	<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td colspan="3">
					<s:radio list="tiposPersona" 
                                   onclick="javascript:onClickTipoPersona();" 
                                   id="r_tipoPersona" 
                                   name="filtroCliente.claveTipoPersonaString"
                                   cssClass="jQrequired"
                                   disabled="true"/>
				</td>
			</tr>
			<tr>
				<s:if test="filtroCliente.claveTipoPersona == 1">
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w220"
                                 name="filtroCliente.nombre"
                                 labelposition="left"
                                 label = "Nombre(s)" 
                                 size="50"   
                                 value="%{filtroCliente.nombre}"
                                 id="txt_nombre" 
                                 readOnly="readOnly"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
                                 name="filtroCliente.apellidoPaterno"
                                 labelposition="left"
                                 label = "Apellido Paterno" 
                                 size="10"         
                                 value="%{filtroCliente.apellidoPaterno}"        
                                 id="txt_apellido_paterno" 
                                 readOnly="readOnly" />
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130" 
                                 name="filtroCliente.apellidoMaterno"
                                 labelposition="left" 
                                 label="Apellido Materno"
                                 value="%{filtroCliente.apellidoMaterno}"
                                 size="10"                          
                                 id="txt_apellido_materno"
                                 readOnly="readOnly"/>
				</td>	
				</s:if>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.codigoRFC"
                                 labelposition="left" 
                                 label="RFC"
                                 size="10"
                                 value="%{filtroCliente.codigoRFC}"                             
                                 id="txt_rfc" 
                                 readOnly="readOnly"/>
				</td>
				<s:if test="filtroCliente.claveTipoPersona == 2">
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w360" 
                                 name="filtroCliente.razonSocial"
                                 labelposition="left" 
                                 label="Razon Social"
                                 size="10"
                                 value="%{filtroCliente.razonSocial}"                             
                                 id="txt_razonSocial" 
                                 readOnly="readOnly"/>
				</td>
				</s:if>
				<s:if test="filtroCliente.claveTipoPersona == 1">
				<td>
						<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.codigoCURP"
                                 labelposition="left" 
                                 label="CURP"
                                 size="10"
                                 value="%{filtroCliente.codigoCURP}"                             
                                 id="txt_curp" 
                                 readOnly="readOnly"/>
				</td>	
				</s:if>	
			</tr>
			<tr>
				<td colspan="3">
					<table id="agregar" style="padding: 0px; width: 98%; margin: 0px; border: none;">
						<tbody>
							<tr>
								<td>
									<div class="btn_back w100" style="display: inline; float: right;">
										<a href="http://www.renapo.gob.mx/swb/" target="_blank">
											<s:text name="midas.boton.renapo" />
										</a>
									</div>	
								</td>							
							</tr>
						</tbody>
					</table>	
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<br/>
					<div id="listadoDireccionesClienteGrid" class="dataGridConfigurationClass" style="width:98%;"></div>
					<br/>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<br/>	
	<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.telefonoOficinaContacto"
                                 labelposition="left" 
                                 label="Oficina"
                                 size="10"
                                 value="%{filtroCliente.telefonoOficinaContacto}"                             
                                 id="txt_telefonoOficina" 
                                 readOnly="readOnly"/>
				</td>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.celularContacto"
                                 labelposition="left" 
                                 label="Celular"
                                 size="10"
                                 value="%{filtroCliente.celularContacto}"                             
                                 id="txt_telefonoCelular" 
                                 readOnly="readOnly"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.numeroTelefono"
                                 labelposition="left" 
                                 label="Casa"
                                 size="4"
                                 value="%{filtroCliente.numeroTelefono}"                             
                                 id="txt_telefonoCasa" 
                                 readOnly="readOnly"/>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<br/>
	<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<s:if test="filtroCliente.claveTipoPersona == 1">
			<tr>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.fechaNacimiento"
                                 labelposition="left" 
                                 label="Fecha de Nacimiento"
                                 size="10"
                                 value="%{filtroCliente.fechaNacimiento}"                             
                                 id="txt_fechaNacimiento" 
                                 readOnly="readOnly"/>
				</td>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.estadoNacimiento"
                                 labelposition="left" 
                                 label="Estado de Nacimiento"
                                 size="10"
                                 value="%{filtroCliente.estadoNacimiento}"                             
                                 id="txt_estadoNacimiento" 
                                 readOnly="readOnly"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.descripcionSexo"
                                 labelposition="left" 
                                 label="Genero"
                                 size="4"
                                 value="%{filtroCliente.descripcionSexo}"                             
                                 id="txt_sexo" 
                                 readOnly="readOnly"/>
				</td>	
			</tr>
			</s:if>	
			<tr>
				<s:if test="filtroCliente.claveTipoPersona == 2">
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.fechaConstitucion"
                                 labelposition="left" 
                                 label="Fecha de Constitucion"
                                 size="10"
                                 value="%{filtroCliente.fechaConstitucion}"                             
                                 id="txt_fechaConstitucion" 
                                 readOnly="readOnly"/>
                </td>
                </s:if>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w160" 
                                 name="filtroCliente.idGiro"
                                 labelposition="left" 
                                 label="Giro"
                                 size="10"
                                 value="%{filtroCliente.idGiro}"                             
                                 id="txt_giro" 
                                 readOnly="readOnly"/>
				</td>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w260" 
                                 name="filtroCliente.email"
                                 labelposition="left" 
                                 label="Email"
                                 size="10"
                                 value="%{filtroCliente.email}"                             
                                 id="txt_email" 
                                 readOnly="readOnly"/>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table id="agregar" style="padding: 0px; width: 98%; margin: 0px; border: none;">
						<tr>
							<td>
								<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar" >
									<a href="javascript: void(0);" onclick="javascript:cerrarDatosAsegurado();"> 
										<s:text name="midas.boton.cerrar" /> 
									</a>
								</div>	
							</td>							
						</tr>
					</table>	
				</td>		
			</tr>
		</tbody>
	</table>
	</div>
	<br/>			
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarListadoDatosCliente();
	});
</script>
