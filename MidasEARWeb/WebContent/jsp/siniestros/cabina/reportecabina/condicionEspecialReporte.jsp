<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>


<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<script type="text/javascript">
	var buscarCondicionesEspecialesIncisoGridPath = '<s:url action="mostrarCondicionesEspecialesIncisoGrid" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var guardarCondicionesEspecialesIncisoPath = '<s:url action="asociarCondicionesEspecialesInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
</script>
<s:form id="condicionesEspecialesIncisoForm" >
	<s:hidden name="idToPoliza" id="h_idPoliza"></s:hidden>
	<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
	<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
	<s:hidden name="incisoReporte.numeroInciso" id="h_numeroInciso"></s:hidden>
	<s:hidden name="validOn" id="h_validOn"></s:hidden>
	<s:hidden name="recordFrom" id="h_recordFrom"></s:hidden>
	<s:hidden name="incisoContinuityId" id="h_incisoContinuityId"></s:hidden>
	<s:hidden name="idReporte" id="h_idReporte"></s:hidden>
	<s:hidden name="incisosAsociadosReporte" id="h_incisosAsociadosReporte"></s:hidden>
	<s:hidden name="tipoValidacion" id="h_tipoValidacion"></s:hidden>
	<s:hidden name="fechaReporteSiniestroMillis" id="h_fechaReporteSiniestroMillis"></s:hidden>
	<s:hidden name="soloConsulta" id="h_soloConsulta"/>
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.consultaIncisoPoliza.consultaCondicionesEspeciales.titulo"/>	
	</div>		
	<br/>
	<div id="condicionesEspecialesIncisoGrid" class="dataGridConfigurationClass" style="width:98%;height:300px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<div id="contenedorWinDetalle" style="display: none;"></div>
</s:form>
<br/>
<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
				<a href="javascript: void(0);" onclick="cerrarConfirmar();"> 
					<s:text name="midas.boton.cerrar" /> 
				</a>
			</div>	
			<div class="btn_back w140" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="guardarCondicionesEspecialesInciso();"> 
					<s:text name="midas.boton.guardar" /> 
				</a>
			</div>		
		</td>							
	</tr>
</table>	
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		mostrarCondicionesEspecialesInciso();
		if( jQuery("#h_incisosAsociadosReporte").val()==0 || jQuery("#h_soloConsulta").val() == 1 ){
			jQuery("#b_agregar").hide();
		}
	});
</script>
