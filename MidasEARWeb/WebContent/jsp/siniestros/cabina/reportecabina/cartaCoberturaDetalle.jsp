<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/cartaCoberturaDetalle.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/solicitud/solicitudPoliza.js'/>"></script>


<style type="text/css">

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>
<script type="text/javascript">
	var asignarCartaCoberturaPath           = '<s:url action="asignarCartaCobertura" namespace="/siniestros/cabina/reporteCabina/cartaCobertura"/>';
</script>



<s:form id="cartaCoberturaForm" >

<s:hidden name="idToSolicitud" id="h_idToSolicitud"></s:hidden>
<s:hidden name="idReporteCabina" id="h_idReporteCabina"></s:hidden>
<s:hidden name="serie" id="h_serie"></s:hidden>
<s:hidden name="solicitudReporteCabina.documentoDigitalSolicitudDTO.idToControlArchivo" id="h_idToControlArchivo"></s:hidden>


	<div class="titulo" style="width: 98%;">
		<s:text name="midas.solicitudSuspensionServicio.title"/>	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarSolicitud.idToSolicitud"
										 name="solicitudReporteCabina.solicitud.idToSolicitud"
										 value="%{solicitudReporteCabina.solicitud.idToSolicitud}"
										labelposition="left" 
										 size="12"					
										   id="txt_idToSolicitud"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarSolicitud.asegurado"
										 name="solicitudReporteCabina.solicitud.nombreCompleto"
										 value="%{solicitudReporteCabina.solicitud.nombreCompleto}"
										labelposition="left" 
										 size="60"					
										   id="txt_asegurado"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarSolicitud.fechaAlta"
										 name="solicitudReporteCabina.solicitud.fechaCreacion"
										 value="%{solicitudReporteCabina.solicitud.fechaCreacion}"
										labelposition="left" 
										 size="12"					
										   id="txt_fechaAlta"/>
					</td>		
				</tr>
				<tr>
				    <td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarSolicitud.fechaInicio"
										 name="solicitudReporteCabina.solicitudDataEnTramite.fechaInicioVigencia"
										 value="%{solicitudReporteCabina.solicitudDataEnTramite.fechaInicioVigencia}"
										labelposition="left" 
										 size="12"					
										   id="txt_fechaInicio"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarSolicitud.fechaFin"
										 name="solicitudReporteCabina.solicitudDataEnTramite.fechaFinVigencia"
										 value="%{solicitudReporteCabina.solicitudDataEnTramite.fechaFinVigencia}"
										labelposition="left" 
										 size="12"					
										   id="txt_fechaFin"/>
					 </td>	
					 <td></td>
				</tr>	
				<tr>		
					<td colspan="3">		
						<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td>
									<div class="btn_back w80" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="javascript: cerrar();"> 
										<s:text name="midas.boton.cerrar" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif' style="vertical-align: middle;"/> </a>
									</div>	
									<div class="btn_back w170" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="javascript:mostrarDocumentoCartaCobertura(h_idToControlArchivo.value);"> 
										<s:text name="midas.boton.consultarDocumentos" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Consultar Documento' title='Consultar Documento' src='/MidasWeb/img/b_ico_busq.gif' style="vertical-align: middle;"/> </a>
									</div>	
									<div class="btn_back w80" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="javascript: asignarCartaCobertura();"> 
										<s:text name="midas.boton.asignar" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Asignar' title='Asignar' src='/MidasWeb/img/b_mas_agregar.gif' style="vertical-align: middle;"/> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
	</div>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
