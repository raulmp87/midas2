<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.numero" />  </column>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.numSiniestro" />  </column>
		<column  type="ro"  width="250"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.cobertura" />   </column>
		<column  type="ro"  width="250"  align="center" sort="str"> <s:text name="midas.consulta.endoso.inciso.tipo" />   </column>
		<column  type="ro"  width="250"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.movimiento" />  </column>
		<column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.causaMovimiento" />  </column>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.folio" />  </column>
		<column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.usuario" />  </column>
		<column  type="ro"  width="90"   align="center" sort="date_custom"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.fecha" />  </column>
	    <column  type="ron" width="150"	 align="center" sort="int" format="$0,000.00"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.importe" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.descripcionmovs" />  </column>
	</head>

	<s:iterator value="movimientos">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idMovimiento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoDocumentoDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoMovimientoDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="causaMovimientoDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="folio" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="usuario" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaMovimiento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="importe" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	<userdata name="userData_totalGastos"><s:property value="totalGastos" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_totalRecuperacionGastos"><s:property value="totalRecuperacionGastos" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_totalEstimado"><s:property value="totalEstimado" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_totalPagado"><s:property value="totalPagado" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_reservaPendiente"><s:property value="reservaPendiente" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_totalIngresos"><s:property value="totalIngresos" escapeHtml="false" escapeXml="true"/></userdata>
</rows>