<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
 <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
 <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/valeTaller.js'/>"></script>

<style type="text/css">

.labelBlack{
 	color:black;
    width: 98%;
	line-height: 20px;
	margin-top: 6px;
	margin-bottom: 6px;
	font-size: 12px;
	font-weight: bold;
	text-align: center;	
}

.label{
 	color:black;
    width: 98%;
	line-height: 20px;
	margin-top: 6px;
	margin-bottom: 6px;
	font-size: 12px;
	text-align: center;	
}

.error {
	background-color: red;
	opacity: 0.4;
}

table td {
	font-size: 9px;
	border-collapse: collapse;
	padding: 10px;
	border: 1px solid #73D54A;		
}

</style>


<s:form id="valeTallerForm">
	<s:hidden name="idReporteCabina" id="h_idReporteCabina"></s:hidden>
	<s:hidden name="idGrua" id="h_idGrua"></s:hidden>
	<s:hidden name="soloConsulta" id="h_soloConsulta"/>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.valeTaller.title"/>	
	</div>	
	<table id="vale" width="95%">
		<tbody>
			<tr>
				<td>
					<s:text name="midas.valeTaller.fecha"/>	
				</td>
				<td>
					<s:text name="midas.valeTaller.dia"/>
					<s:textfield id="t_dia"
								 name="valeTaller.dia"
								 cssClass="txtfield" 
								 size="3"			
								 maxlength="2"		
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.mes"/>
					<s:textfield id="t_mes"
								 name="valeTaller.mes"
								 cssClass="txtfield" 
								 size="3"			
								 maxlength="2"		
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.anio"/>
					<s:textfield id="t_anio"
								 name="valeTaller.anio"
								 cssClass="txtfield" 
								 size="5"			
								 maxlength="4"		
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.numeroPoliza"/>
					<s:textfield id="t_numeroPoliza"
								 name="valeTaller.numeroPoliza"
								 cssClass="txtfield" 
								 size="15"				
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.inciso"/>
					<s:textfield id="t_inciso"
								 name="valeTaller.numeroInciso"
								 cssClass="txtfield" 
								 size="3"				
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.numeroSiniestro"/>
					<s:textfield id="t_numeroSiniestro"
								 name="valeTaller.numeroSiniestro"
								 cssClass="txtfield" 
								 size="15"				
								 readonly="true" />
				</td>
				<td>
					<s:text name="midas.valeTaller.hora"/>
					<s:textfield id="t_hora"
								 name="valeTaller.hora"
								 cssClass="txtfield" 
								 size="10"			
								 readonly="true" />
				</td>
				<td>
					<s:radio id="r_hora"
							 name="valeTaller.amPm"  
							 list="#{'AM':'AM', 'PM':'PM'}"
							 disabled="true" />
				</td>
				
			</tr>
			<tr>
				<td colspan="3">
					<s:radio id="r_ubicacion" 
							 name="valeTaller.ubicacion"
							 list="listUbicacion"
			 				 listKey="key" listValue="value"/>
				</td>
				<td colspan="3">
					<s:radio id="r_tipoVehiculo"
							 name="valeTaller.tipoVehiculo"  
							 list="#{'1':'Veh\u00EDculo Asegurado', '2':'Veh\u00EDculo Tercero'}" 
							 onclick="javascript: mostrarInfoValeTaller();"/>
				</td>
				<td colspan="4">
					<div id="nombreAsegurado">
						<s:text name="midas.valeTaller.nombreAseguradoTercero"/>	
						<s:textfield id="t_nombreAsegurado"
								 name="valeTaller.nombreAsegurado"
								 cssClass="txtfield datosTaller" 
								 readonly="true"
								 size="50" />
					</div>
					<div id="listadoPases" style="display: none;">
						<s:text name="midas.valeTaller.nombreAseguradoTercero"/>	
						<s:select id="terceroId"
								labelposition="left" 
								name="valeTaller.idTercero"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="mostrarInfoPase(this.value)"
						  		list="listPases" listKey="estimacionCoberturaReporte.id" 
						  		listValue="estimacionCoberturaReporte.nombreAfectado"  
						  		cssClass="txtfield setNew" /> 
					</div>
					
				</td>	
			</tr>
			<tr>
				<td colspan="6">
					<s:text name="midas.valeTaller.marcaTipo"/>
					<s:textfield id="t_marcaTipo"
								 name="valeTaller.marcaVehiculo"
								 cssClass="txtfield datosTaller" 
								 readonly="true"								 
								 size="30" />
				</td>
				<td >
					<s:text name="midas.valeTaller.numeroPlacas"/>
					<s:textfield id="t_numeroPlacas"
								 name="valeTaller.numeroPlacas"
								 cssClass="txtfield datosVehiculo datosTaller" 
								 maxlength="20"
								 size="20" />
				</td>
				<td>
					<s:text name="midas.valeTaller.modelo"/>
					<s:textfield id="t_modelo"
								 name="valeTaller.modelo"
								 cssClass="txtfield datosVehiculo datosTaller jQnumeric jQrestrict" 
								 readonly="true"	
								 size="4" maxlength="4"/>
				</td>					
				<td>
					<s:text name="midas.valeTaller.color"/>
					<s:textfield id="t_color"
								 name="valeTaller.color"
								 cssClass="txtfield datosTaller" 
								 maxlength="20"
								 size="20" />
				</td>	
			</tr>
			<tr>
				<td colspan="6">
					<s:text name="midas.valeTaller.tipoCarroceria"/>
					<s:textfield id="t_tipoCarroceria"
								 name="valeTaller.tipoCarroceria"
								 cssClass="txtfield"
								 maxlength="50"
								 size="50" />
				</td>
				<td colspan="3">
					<s:text name="midas.valeTaller.servicioDe"/>
					<s:radio id="r_tipoServicio" 
							 name="valeTaller.tipoServicio"
							 list="listServicio"
			 				 listKey="key" listValue="value"/>
				</td>	
			</tr>
			<tr>
				<td colspan="6">
					<s:text name="midas.valeTaller.recogerEn"/>
					<s:textfield id="t_recogerEn"
								 name="valeTaller.recogerEn"
								 cssClass="txtfield " 
								 maxlength="50"
								 size="50" />
				</td>
				<td colspan="3">
					<s:text name="midas.valeTaller.entregarEn"/>
					<s:textfield id="t_entregarEn"
								 name="valeTaller.entregarEn"
								 cssClass="txtfield"
								 maxLength="50"
								 size="50" />
				</td>	
			</tr>
			<tr>
				<td colspan="6">
					<s:text name="midas.valeTaller.importe"/>
					<s:textfield id="t_importe"
								 name="valeTaller.importe"
								 cssClass="txtfield jQnumeric jQrestrict " 
								 maxlength="10"
								 size="10" />
				</td>	
				<td colspan="3">
					<s:text name="midas.valeTaller.grua"/>
					<s:textfield id="t_grua"
								 name="valeTaller.nombreGrua"
								 cssClass="txtfield" 
								 maxlength="50"
								 size="50" />
				</td>	
				
			</tr>
		</tbody>
	</table>
	<div class="labelBlack" ><s:text name="midas.valeTaller.notaImportante"/></div>
	<div class="label" ><s:text name="midas.valeTaller.notaTexto"/></div>
	<table  width="95%">
			<tr>
			  	<td style="width: 70%">
			  		<s:text name="midas.valeTaller.ajustador"/>	
					<s:textfield id="t_ajustador"
								 name="valeTaller.nombreAjustador"
								 cssClass="txtfield"  readonly="true"
								 size="50" />
				</td>
				<td style="width: 30%">
					<s:text name="midas.valeTaller.claveAjustador"/>	
					<s:textfield id="t_claveAjustador"
								 name="valeTaller.claveAjustador"
								 cssClass="txtfield"  readonly="true"
								 size="50" />
				</td>	
			</tr>
		</tbody>
	</table>

	<br/>
	<div class="btn_back w140" style="display: inline; float: left;">
		 <a href="javascript: void(0);" onclick="imprimirVale();">
		 <s:text name="midas.boton.imprimir" /> </a>
	</div>
	
	<div class="btn_back w140" style="display: inline; float: right;">
		 <a href="javascript: void(0);" onclick="cerrarVentanaVale();">
		 <s:text name="midas.boton.cerrar" /> </a>
	</div>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
</s:form>
<script>
jQuery(document).ready(function(){
	var tipoVehiculo = jQuery('input[name=valeTaller.tipoVehiculo]:checked', '#valeTallerForm').val();
	if( tipoVehiculo == null || tipoVehiculo == "1" ){
		jQuery("#nombreAsegurado").show();
		jQuery("#listadoPases").hide();
		jQuery("#valeTaller.idTercero").val('');
		jQuery('#terceroId option:first').attr('selected',true);
		jQuery('#t_numeroPlacas').attr('readonly',true);
		jQuery('#t_color').attr('readonly',true);
	} else {
		jQuery("#nombreAsegurado").hide();
		jQuery("#listadoPases").show();
	}
});

</script>