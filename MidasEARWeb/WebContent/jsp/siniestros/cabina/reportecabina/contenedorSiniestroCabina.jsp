<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
	var incisoPath = '<s:url action="mostrarIncisoAfectado" namespace="/siniestros/cabina/siniestrocabina"/>';
	var cierrePath = '<s:url action="mostrarCierreReporte" namespace="/siniestros/cabina/siniestrocabina"/>';	
	var guardarPath = '<s:url action="guardar" namespace="/siniestros/cabina/siniestrocabina"/>';	
	var convertirSiniestroPath = '<s:url action="convertirSiniestro" namespace="/siniestros/cabina/siniestrocabina"/>';
	var enviarHGSOCRAPath = '<s:url action="enviarHGSOCRA" namespace="/siniestros/cabina/siniestrocabina"/>';


	var cerrarPath = '<s:url action="mostrarContenedor" namespace="/siniestros/cabina/reportecabina"/>';
	var mostrarCoberturasAfectacionIncisoPath = '<s:url action="mostrarCoberturasAfectacionInciso" namespace="/siniestros/cabina/siniestrocabina"/>';
	var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var buscarCondicionesEspecialesIncisoPath = '<s:url action="mostrarCondicionesEspecialesInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
	var datosRiesgoCoberturaPath = '<s:url action="obtenerDatosRiesgo" namespace="/siniestros/cabina/siniestrocabina"/>';
	var mostrarSolicitudValuacionPath = '<s:url action="mostrarEnviarValuacion" namespace="/siniestros/cabina/siniestrocabina"/>';
	var mostrarContenedorLlegadaOtraCiaPath = '<s:url action="mostrarLlegadaOtraCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var mostrarContenedorSiniestrosCiaPath = '<s:url action="mostrarSiniestrosCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var cargaLlegadaCiaGridPath = '<s:url action="cargaLlegadasCiaGrid" namespace="/siniestros/cabina/siniestrocabina"/>';
	var eliminarLlegadaCiaPath = '<s:url action="eliminarLlegadaCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var buscarReferenciasBancariasPath = '<s:url action="buscarReferenciasBancarias" namespace="/siniestros/cabina/siniestrocabina"/>';
	var generarReferenciasRecuperacionPath = '<s:url action="generarReferenciaRecuperacion" namespace="/siniestros/cabina/siniestrocabina"/>';
	var actualizarEstatusValuacionPath = '<s:url action="actualizarEstatusValuacion" namespace="/siniestros/cabina/siniestrocabina"/>';
	var buscarSiniestrosCiaPath = '<s:url action="buscarSiniestrosCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var guardarSiniestroCiaPath = '<s:url action="guardarSiniestroCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var editarSiniestroCiaPath = '<s:url action="editarSiniestroCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var rechazarSiniestroPath = '<s:url action="rechazarSiniestro" namespace="/siniestros/cabina/siniestrocabina"/>';
	var termiinarSiniestroPath = '<s:url action="terminarSiniestro" namespace="/siniestros/cabina/siniestrocabina"/>';
	var cancelarSiniestroPath = '<s:url action="cancelarSiniestro" namespace="/siniestros/cabina/siniestrocabina"/>';
	var reaperturarReportePath = '<s:url action="reaperturarReporte" namespace="/siniestros/cabina/siniestrocabina"/>';
	var validarPendientesReportePath = '<s:url action="validarPendientesReporte" namespace="/siniestros/cabina/siniestrocabina"/>';
	var mostrarCoberturasRechazoPath = '<s:url action="mostrarCoberturasRechazo" namespace="/siniestros/cabina/siniestrocabina"/>';
	var buscarCoberturasRechazoPath = '<s:url action="buscarCoberturasRechazo" namespace="/siniestros/cabina/siniestrocabina"/>';
	var solicitarAutorizacionVigencia = '<s:url action="solicitarAutorizacionVigencia" namespace="/siniestros/cabina/reportecabina"/>';
	
</script>

<style type="text/css">
.heightCell {
	height: 30px !important;
}
</style>

<s:form id="funcionalidadVentanaModal">
<s:hidden id="urlRedirect" name="urlRedirect"></s:hidden>
</s:form>

<s:form id="siniestroCabinaForm">
<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="soloConsulta" id="h_soloConsulta"></s:hidden>
<s:hidden name="urlEnvioValuacion" id="urlEnvioValuacion"></s:hidden>

	<s:hidden name="siniestroDTO.reporteCabinaId" id="idToReporte"></s:hidden>
	<s:hidden name="siniestroDTO.id" id="siniestroDTO.id"></s:hidden>
	<s:hidden name="siniestroDTO.esSiniestro" id="siniestroDTO.esSiniestro"></s:hidden>
	<s:hidden name="siniestroDTO.idToPoliza" id="h_idPoliza"></s:hidden>
	<s:hidden name="siniestroDTO.fechaOcurridoMillis" id="h_fechaReporteSiniestroMillis"></s:hidden>
	<s:hidden name="siniestroDTO.tipoCotizacion" id="h_tipoCotizacion"></s:hidden>
	<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
	<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
	<s:hidden name="siniestroDTO.incisoContinuity" id="h_incisoContinuityId"></s:hidden>
	<s:hidden name="siniestroDTO.importeEstimado" id="h_importeEstimado"></s:hidden>
	<s:hidden name="siniestroDTO.importeReserva" id="h_importeReserva"></s:hidden>
	<s:hidden name="siniestroDTO.estatusReporte" id="h_estatusReporte"></s:hidden>
	<s:hidden name="vieneExpedienteJuridico" id="h_expedienteJuridico" class="setNew"  />
	<s:hidden id="idExpedienteJuridico" name="expedienteJuridico.id"/>
	<s:hidden id="h_modoConsultaExpediente" name="modoConsultaExpediente"/>
<%-- 	<s:hidden name="siniestroDTO.noAplicaDepuracion" id="h_noAplicaDepuracion"></s:hidden> --%>

	<s:hidden name="detalleInciso.estatus" id="h_estatusInciso" class="setNew"  />
	<s:hidden name="incisoAutorizado" id="h_incisoAutorizado" class="setNew"  />

	<s:hidden name="tieneCiaAsociada" id="h_tieneCiaAsociada" />

		
	<div class="titulo">
		<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.title"/>	
	</div>	
	<div id="identificacionSiniestroDiv">
		<table id="agregar" style="width: 98%;">
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numSiniestro"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.numeroSiniestro" name="siniestroDTO.numeroSiniestro"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numReporte"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.numeroReporte" name="siniestroDTO.numeroReporte"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.poliza"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.poliza" name="siniestroDTO.poliza"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
			</tr>
		</table>
	</div>
	<div class="titulo">
		<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.incisoAfectado"/>	
	</div>
	<div id="incisoAfectadoDiv">
		<table id="agregar" style="width: 98%;">	
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.lineaNegocio"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.lineaNegocio" name="siniestroDTO.lineaNegocio"
							labelposition="left" cssClass="txtfield jQrestrict w200" readOnly="true" size="100"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numInciso"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.numeroInciso" name="siniestroDTO.numeroInciso"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.iniVigencia"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.iniVigencia" name="siniestroDTO.fechaVigIniRealIncSiniestro"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.estatus"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.estatus" name="siniestroDTO.estatus"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.motivoSituacion"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.motivoSituacion" name="siniestroDTO.motivoSituacion"
							labelposition="left" cssClass="txtfield jQrestrict w300" readOnly="true" size="100"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.finVigencia"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.finVigencia" name="siniestroDTO.finVigencia"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.conducHabitual"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.conductorHabitual" name="siniestroDTO.conductorHabitual"
							labelposition="left" cssClass="txtfield jQrestrict w200" readOnly="true"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.descripcion"/>	
				</td>
				<td>
					<s:textfield id="siniestroDTO.descripcion" name="siniestroDTO.descripcion"
							labelposition="left" cssClass="txtfield jQrestrict w300" readOnly="true"/>
				</td>				
				<td>  			
		 			 <s:checkbox id = "ch_noAplicaDepuracion"  name="siniestroDTO.noAplicaDepuracion" 
						key="midas.siniestros.cabina.reportecabina.afectacionInciso.noAplicaDepuracion" 
						cssClass="setNew"/>	
				</td>
			</tr>
		</table>
	</div>
	<div hrefmode="ajax-html" style="height: 350px; margin-right: 10px;margin-bottom: 10px;" id="IncisoSiniestroTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">
		<div width="200px" id="inciso" name="Datos del Inciso Afectado" href="http://void" extraAction="javascript: verTabInciso()"></div>
		<div width="200px" id="cierre" name="Cierre de Reporte de Siniestro" href="http://void" extraAction="javascript: verTabCierre();"></div>
	</div>
	<div id="seccionCoberturasReporte" style="display:none;">
		<s:if test="siniestroDTO.enviosPendientesHGSOCRA">
					<div style="font-weight:bold; font-size:11px; color:red"> <s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.enviosHGSOCRA"/></div>
					<br>
				</s:if>
		<div class="titulo" style="width: 98%;" id="d_listadoCoberturas">
			<s:text name="midas.consultaIncisoPoliza.listadoCoberturas"/>		
		</div>
		<br/>
		<div id="indicador"></div>
		<div id="coberturasAfectacionIncisoGrid" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br/>
		
		<div id="divBotones" style="float:right; margin-right:5%;">
			<div id="divCondiciones" style="float:right;">					
					<div class="btn_back w140"  style="display:inline; float: left; ">								
						<s:if test="%{siniestroDTO.tieneCondicionesEspeciales}">																		
							<a href="javascript: void(0);" style="color:red;"
								onclick="inicializarCondicionesEspecialesFromSiniestro(2);" id="btnCondiciones">
								<s:text name="midas.boton.condicionesEspeciales" />
							</a>
						</s:if>										
						<s:else>
							<a href="javascript: void(0);"
								onclick="inicializarCondicionesEspecialesFromSiniestro(2);" id="btnCondiciones">
								<s:text name="midas.boton.condicionesEspeciales" />
							</a>
						</s:else>					
					</div>
				</div>
			
			<s:if test="siniestroDTO.enviosPendientesHGSOCRA && !soloConsulta">
				<div class="btn_back w140"  style="display:inline; float: left; " id="div_enviosPendientesHGSOCRA">
					<a href="javascript: void(0);" onClick="javascript:envioAHGSOCRA();" id="btnenviosPendientesHGSOCRA">
						<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.enviosPendientesHGSOcra" />
					</a>
				</div>
			</s:if>
			<s:if test="siniestroDTO.esSiniestro">
				<div class="btn_back w140"  style="display:inline; float: left; " id="btn_OrdenCompra">
					<a href="javascript: void(0);"
						onclick="ordenCompra()">
						<s:text
							name="Ordenes de Compra" />
					</a>
				</div>
				
	
			</s:if>
			
				<div class="btn_back w140"  style="display:inline; float: left; " id="btn_Fortimax">
			<a href="javascript: void(0);"
				onclick="abrirFortimax();" id="abtn_Fortimax">
				<s:text
					name="Imagenes" /> 
			</a>
		</div>
		
			<div class="btn_back w150"
				style="display: inline; float: left;">
				<a href="javascript: void(0);" onclick="imprimirReporte();"> 
					<s:text name="midas.boton.midas.boton.impresionReporte" />
				</a>
			</div>
		</div>
		<br></br>
		<br></br>
		
		<div id="divDatosRiesgo" style="display:none;">
			<div class="titulo" >
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.observaciones"/>		
			</div>
			<br/>
			<div id="datosRiesgoCobertura" class="dataGridConfigurationClass" style="width:98%;height:200px;"></div>
			<br/>
		</div>
	</div>
	<div id="divBotones" style="float:right; margin-right:5%;">
		
		
	
		<div class="btn_back w140"  style="display:inline; float: left; " id="btn_guardar">
			<a href="javascript: void(0);"
				onclick="guardar();" id="btnGuardar">
				<s:text
					name="midas.boton.guardar" />
			</a>
		</div>
		<s:if test="!siniestroDTO.esSiniestro">
			<div class="btn_back w140"  style="display:inline; float: left; " id="btn_convertirSiniestro">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea convertir a siniestro?')){convertirSiniestro();}" id="btnConvertir">
					<s:text
						name="midas.siniestros.cabina.reportecabina.afectacionInciso.convertirSiniestro" />
				</a>
			</div>
		</s:if>
		<s:if test="siniestroDTO.esSiniestro">
			<div class="btn_back w140"  style="display:inline; float: left; " id="btn_terminarSiniestro">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea terminar el siniestro?')){validarPendientesReporte(3);}" id="btnTerminar">
					<s:text
						name="midas.siniestros.cabina.reportecabina.afectacionInciso.terminarSiniestro" />
				</a>
			</div>	
		</s:if>
		<s:if test="siniestroDTO.terminoAjuste == 'RECH' && siniestroDTO.estatusReporte == 3">
			<div class="btn_back w140"  style="display:inline; float: left; " id="btn_rechazaSiniestro">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea rechazar siniestro?')){validarPendientesReporte(1);}" id="btnRechazar">
					<s:text
						name="midas.boton.rechazar" />
				</a>
			</div>		
		</s:if>
		<div class="btn_back w140"  style="display:inline; float: left; " id="btn_cancelarSiniestro">
			<a href="javascript: void(0);"
				onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cancelar el siniestro?')){validarPendientesReporte(2);}" id="btnCancelar">
				<s:text
					name="midas.boton.cancelar" />
			</a>
		</div>	
		<s:if test="siniestroDTO.estatusReporte == 2  || siniestroDTO.estatusReporte == 4 || siniestroDTO.estatusReporte == 5">
			<div class="btn_back w140"  style="display:inline; float: left; " id="btn_Reaperturar">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFDesea reaperturar el reporte?')){reaperturarReporte();}" id="btnReaperturar">
					<s:text
						name="midas.siniestros.cabina.reportecabina.reaperturar" />
				</a>
			</div>	
		</s:if>
		<div class="btn_back w140"  style="display:inline; float: left; " id="btnCerrar">
			<a href="javascript: void(0);"
				onclick="regresarReporteBusquedaAfectacion();" id="abtnCerrar">
				<s:text
					name="midas.boton.cerrar" />
			</a>
		</div>	
		
	</div>
</s:form>		
<script type="text/javascript">
	initCurrencyFormatOnTxtInput();	
	dhx_init_tabbars();
</script>