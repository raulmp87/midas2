<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="id" type="ro" width="1" sort="int" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>
		<column id="cobertura" type="ro" width="310" sort="str"><s:text name="midas.consultaIncisoPoliza.coberturas"/></column>
		<column id="sumaAsegurada" type="ro" width="300" sort="str" align="center"><s:text name="midas.consultaIncisoPoliza.sumaAsegurada"/></column>
		<column id="porcentajeDeducible" type="ro" width="180"  sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.porcentajeDeducible"/></column>
		<column id="monteDeducible" type="ro" width="110" sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.monteDeducible"/></column>
		<column id="claveTipoCalculo" type="ro" width="1" sort="int" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>			
	</head>			
	<s:iterator value="biCoberturaSeccionList" status="stat">
		<row id="<s:property value="continuity.coberturaDTO.idToCobertura"/>">
			<cell><s:property value="id"/></cell>
			<cell><s:property value="continuity.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true"/></cell>
			
			<cell>	
				<s:if test="continuity.coberturaDTO.claveTipoCalculo == 'RCV'">
					<s:property  value="%{value.diasSalarioMinimo}" escapeHtml="false" escapeXml="true"/> <s:property  value="%{sumaStr}" escapeHtml="false" escapeXml="true"/>
				</s:if>						
				<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 0">
<%-- 					<s:if test="value.valorSumaAseguradaMax != 0.0 && value.valorSumaAseguradaMin!= 0.0"> --%>
<%-- 						&lt;div style='font-size:8px;'&gt;<s:text name="midas.poliza.inciso.cobertura.entre" />						 --%>
<%--  							<s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true"  /> --%>
<%-- 								<s:text name="midas.general.y"/>  --%>
<%-- 								<s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />&lt;/div&gt; --%>
<%-- 					</s:if>					 --%>
					<s:property  value="%{getText('struts.money.format',{value.valorSumaAsegurada})}" escapeHtml="false" escapeXml="true"  />		
				</s:elseif>	
	
				<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 1">				
					<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
				</s:elseif>
				<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 2">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
					<s:property  value="%{value.valorSumaAsegurada}" escapeHtml="false" escapeXml="true"/>				
				</s:elseif>
				<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 9">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />				
				</s:elseif>
				<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 3">				
					<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
				</s:elseif>
			</cell>	
			<cell>
				<s:if test='continuity.coberturaDTO.claveTipoDeducible == "0"'>
					</s:if>
					<s:elseif test='continuity.coberturaDTO.claveTipoDeducible == "1"'>
						<s:property  value="%{value.porcentajeDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:elseif>
					<s:else>
						<s:property  value="%{value.valorDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:else>
					<s:if test='continuity.coberturaDTO.claveTipoDeducible != "0"'>
						<s:property  value="%{value.descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:if>
			</cell>		
			<!--s:property value="%{getText('struts.money.format',{value.valorDeducible})}" escapeHtml="false" escapeXml="true"/-->
 			<cell></cell>				
			<cell><s:property value="continuity.coberturaDTO.claveTipoCalculo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>