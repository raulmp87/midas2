<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>


<script type="text/javascript"	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>

<script type="text/javascript">
	var enviarValuacionPath = '<s:url action="enviarSolicitudValuacion" namespace="/siniestros/cabina/siniestrocabina"/>';
	
</script>
<s:form id="envioValuacionForm" action="enviarSolicitudValuacion" namespace="/siniestros/cabina/siniestrocabina" name="envioValuacionForm">
	<s:hidden id="idToReporte" name="siniestroDTO.reporteCabinaId"/>
	<s:hidden id="idAjustadorReporte" name="idAjustadorReporte"/>
	<s:hidden name="siniestroDTO.numValuacion" id="numValuacion"></s:hidden>
	
	<s:if test="siniestroDTO.numValuacion == null">
	<div id="seleccionarValuador" style="margin-left: 25%;margin-top: 5%;">
		<s:select   list="valuadores" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="codigoValuador" id="codigoValuador_s" cssClass="cajaTextoM2 w250"  onchange=""/>
	</div>
	<div>
		<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
			<tr>
				<td>
					<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
						<a href="javascript: void(0);" onclick="parent.cancelarSolicitud();"> 
						<s:text name="midas.boton.cancelar" /> </a>
					</div>	
					<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
						<a href="javascript: void(0);" onclick="enviarSolicitudValuacionCrucero();"> 
						<s:text name="midas.boton.enviar" /> </a>
					</div>		
				</td>
			</tr>
		</table>
	</div>
	</s:if>
	<s:else>
		<div>
		<table id="info" style="padding: 0px; width: 100%; margin: 0px; border: none;">
			<tr>
				<td>
					<a><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numeroValuacion" />: <s:property value="siniestroDTO.numValuacion"/> </a>
				</td>			
			</tr>

			<tr>
				<td>
					<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
						<a href="javascript: void(0);" onclick="parent.cerrarEnviarSolicitudValuacion('<s:property value="siniestroDTO.numValuacion"/>');"> 
						<s:text name="midas.boton.cerrar" /> </a>
					</div>	
				</td>
			</tr>
		</table>
	</div>
	</s:else>
</s:form>
