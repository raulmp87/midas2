<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>	

<s:hidden name="siniestroDTO.conductor" id="conductor"></s:hidden>
<s:hidden name="siniestroDTO.edad" id="edad"></s:hidden>
<s:hidden name="siniestroDTO.genero" id="genero"></s:hidden>
<s:hidden name="siniestroDTO.condMismoAsegurado" id="condMismoAsegurado"></s:hidden>
<s:hidden name="siniestroDTO.numOcupantes" id="numOcupantes"></s:hidden>
<s:hidden name="siniestroDTO.tipoLicencia" id="tipoLicencia"></s:hidden>
<s:hidden name="siniestroDTO.ladaTelefono" id="ladaTelefono"></s:hidden>
<s:hidden name="siniestroDTO.telefono" id="telefono"></s:hidden>
<s:hidden name="siniestroDTO.ladaCelular" id="ladaCelular"></s:hidden>
<s:hidden name="siniestroDTO.celular" id="celular"></s:hidden>
<s:hidden name="siniestroDTO.curp" id="curp"></s:hidden>
<s:hidden name="siniestroDTO.rfc" id="rfc"></s:hidden>
<s:hidden name="siniestroDTO.placas" id="placas"></s:hidden>
<s:hidden name="siniestroDTO.color" id="color"></s:hidden>
<s:hidden name="siniestroDTO.importeEstimado" id="importeEstimado"></s:hidden>
<s:hidden name="siniestroDTO.importeReserva" id="importeReserva"></s:hidden>
<s:hidden name="siniestroDTO.verificado" id="verificado"></s:hidden>
<s:hidden name="siniestroDTO.cargoDetalle" id="siniestroDTO.cargoDetalle"></s:hidden>
<s:hidden name="siniestroDTO.esSiniestro" id="esSiniestro"></s:hidden>
<s:hidden name="siniestroDTO.recuperacionId" id="recuperacionId"></s:hidden>
<s:hidden name="siniestroDTO.tipoRecuperacion" id="tipoRecuperacion"></s:hidden>
<s:hidden name="siniestroDTO.montoDanos" id="t_montoDanos" ></s:hidden>
<s:hidden name="idCompaniaSiniestro" id="h_idCompaniaSiniestro" cssClass="companiaData"></s:hidden>
<s:hidden name="mostrarBotonGenerarReferencia" id="h_mostrarBotonGenerarReferencia" ></s:hidden>


<style type="text/css">
table#contenidoSiniestro {
	border : 1px solid #73d54a;
	font-size: 9px;
	margin: auto;
	white-space: nowrap;
	width: 98%;
	padding: 5px;	
}

 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<br/>
<table id="contenidoSiniestro" >
	<tr>
		<td>
			<s:select id="causaSiniestro"
					name="siniestroDTO.causaSiniestro" 
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.causaSiniestro"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catCausaSiniestro" listKey="key" listValue="value"
			  		onselect="onChangeCausaSiniestro('tipoResponsabilidad')" 
			  		onchange="onChangeCausaSiniestro('tipoResponsabilidad');validaComboSeRecibioOrdenCia();"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>
		<td>
			<s:select id="tipoResponsabilidad"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.tipoResponsabilidad"
					name="siniestroDTO.tipoResponsabilidad" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catTipoResponsabilidad" listValue="value" listKey="key" 
			  		onselect="onChangeResponsabilidad('terminoAjuste','terminoSiniestro')" 
			  		onchange="onChangeResponsabilidad('terminoAjuste','terminoSiniestro');  mostrarOcultarEnviarValuacion();validaComboSeRecibioOrdenCia();"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w100" />
		</td>
		<td>
			<s:select
					id="terminoAjuste"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.terminoAjuste"
					name="siniestroDTO.terminoAjuste"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catTerminoAjusteSiniestro" listValue="value" listKey="key" 
			  		onchange="onChangeTerminoDeAjuste(this);mostrarOcultarDanosRechazo();validaComboSeRecibioOrdenCia();"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>
		
		<td>
			<s:select id="terminoSiniestro" 
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.terminoSiniestro"
					name="siniestroDTO.terminoSiniestro"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catTerminoSiniestro" listKey="key" listValue="value"
			  		onchange="mostrarOcultarValuacionRecuperacion();validaComboSeRecibioOrdenCia();"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>	
		
	
	</tr>
	<tr>
		<td>
			<s:select id="fugoTerceroResponsable"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.fugoTercero"
					name="siniestroDTO.fugoTerceroResponsable" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catRespuestaSimple" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w100" />
		</td>
		<td>
			<s:select id="unidadEquipoPesado"
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.unidadEquipoPesado"
					name="siniestroDTO.unidadEquipoPesado" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catRespuestaSimple" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w100" />
		</td>
		<td>
			<s:select id="ciaSeguros" 
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.companiaSeguros"
					name="siniestroDTO.ciaSeguros" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="ciaSeguros" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>
		<td>
			<s:select id="claveAmisSupervisionCampo"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.sapAmisSupervisionCampo"
					name="siniestroDTO.claveAmisSupervisionCampo" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catSapAmisSupervisionCampo" listKey="key" listValue="value"
			  		onselect="" 
			  		onchange=""
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>		
		<td>
			<table style="font-size: 9px;">
				<tr>
					<td>
						<s:select id="llegaOtraCia"
								disabled="true"
								key="midas.siniestros.cabina.reportecabina.afectacionInciso.recibimosOrdenCia"
								name="siniestroDTO.recibidaOrdenCia" 
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="catRespuestaSimple" listKey="key" listValue="value"  
						  		cssClass="txtfield cajaTextoM2 requerido setNew w100" 
						  		onchange="mostrarOcultarEnviarValuacion(); mostrarOcultarCompanias();"/>
					</td>
					<td>
					    <div class="btn_back w140"  style="display:inline; float: left; " id="div_llegadaOtraCia">
							<a href="javascript: void(0);" onclick ="capturaLlegadaOtraCia()" id="btnLlegadaOtraCia">
								<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.llegadaOtraCia" />
							</a>
						</div>
					</td>
				</tr>
			</table>
		</td>
		
		
		<td colspan="2">
		<div id="solicitaValuacion">
			<table style="font-size: 9px;">
				<tr>
					<td>
						<s:textfield id="numeroValuacion" name="siniestroDTO.numValuacion"
						key="midas.siniestros.cabina.reportecabina.afectacionInciso.numeroValuacion"
								cssClass="cajaTextoM2 setNew w50" readonly="true"  disabled="true"/>
					</td>
					<td>
						<div id="solicitarValuacion" class="btn_back w140"  style="display:inline; float: left;">
							<a href="javascript: void(0);" onclick="mostrarEnviarValuacion()" id="btnSolValRec">
								<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.solValRec" />
							</a>
						</div>
					</td>
				</tr>
			</table>
			</div>
		</td>
		
		
	</tr>
	<tr id="contenidoCompania">

		<td>
  			<s:textfield id="t_numeroSin" 
  				key="midas.siniestros.companias.participacion.siniestro"
  				name="siniestroDTO.siniestroCia" 
  				maxlength = "20"
  				cssClass="cajaTextoM2 requerido setNew companiaData" />
  		</td>
            		
            		
  		<td>
  			<s:textfield id="t_porcentaje" 
  				key="midas.siniestros.companias.participacion.porcentaje"
  				name="siniestroDTO.porcentajeParticipacion" 
  				maxlength = "3"
  				cssClass="cajaTextoM2 requerido setNew jQnumeric companiaData" />
  		</td>
            		
   		<td>
   			<s:textfield id="t_poliza" 
   				key="midas.siniestros.companias.participacion.poliza"
   				name="siniestroDTO.polizaCia" 
   				maxlength = "20"
   				cssClass="cajaTextoM2 requerido setNew companiaData" />
   		</td>
   		
   		<td>
   			<s:textfield id="t_inciso" 
   				key="midas.siniestros.companias.participacion.inciso"
   				name="siniestroDTO.incisoCia" 
   				maxlength = "5"
   				cssClass="cajaTextoM2 requerido setNew companiaData" />
   		</td>
   		
    </tr>
    
    <tr id="contenidoSIPAC">
   		<td>
			<s:text name="midas.siniestros.companias.participacion.fechaExpedicion"/>
			<sj:datepicker name="siniestroDTO.fechaExpedicion" changeMonth="true"
				changeMonth="true"
				changeYear="true"
				buttonImage="../img/b_calendario.gif"
				buttonImageOnly="true" id="fechaExpedicion" maxlength="10"
				cssClass="txtfield setNew" size="10"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>	
		</td>
		
		<td>
			<s:text name="midas.siniestros.companias.participacion.fechaDictamen"/>
			<sj:datepicker name="siniestroDTO.fechaDictamen" changeMonth="true"
				changeMonth="true"
				changeYear="true"
				buttonImage="../img/b_calendario.gif"
				buttonImageOnly="true" id="fechaDictamen" maxlength="10"
				cssClass="txtfield setNew" size="10"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>	
		</td>
		
		
		<td>
			<s:text name="midas.siniestros.companias.participacion.fechaJuicio"/>
			<sj:datepicker name="siniestroDTO.fechaJuicio" changeMonth="true"
				changeMonth="true" key=""
				changeYear="true"
				buttonImage="../img/b_calendario.gif"
				buttonImageOnly="true" id="fechaJuicio" maxlength="10"
				cssClass="txtfield setNew" size="10"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>	
		</td>
		
		<td>
			<s:text name="midas.siniestros.companias.participacion.origen"/>
			<s:select id="origenSiniestro"
					name="siniestroDTO.origen"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catOrigenSiniestro" listKey="key" listValue="value"
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>	
    </tr>
    
    <tr id="contenidoValuacionDanos">

		<td>
  			<s:textfield 	cssClass="txtfield cajaTextoM2 w100 formatCurrency setNew"
							key="midas.siniestros.cabina.reportecabina.afectacionInciso.montoValuadoDanos"
							name="%{siniestroDTO.montoDanos}"
							maxlength="12"
							size="20"
							id="d_montoDanos" disabled="true"
							onkeypress="return soloNumeros(this, event, true)" />
  		</td>
  		
  		<td>
  			<div id="coberturasRechazo" class="btn_back w225"  style="display:inline; float: left;">
							<a href="javascript: void(0);" onclick="mostrarMontosCoberturasRechazo()" id="btnCobRechazo">
								<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.coberturasRechazoBtn" />
							</a>
						</div>
  		</td>
  		
       <td>
  			<s:select id="s_motivoRechazo" 
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.motivoRechazo"
					name="siniestroDTO.motivoRechazo"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="motivosRechazo" listKey="key" listValue="value"
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
  		</td>
  		
  		<td>
  			<s:select id="s_rechazoDesistimiento" 
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.rechazoDesistimiento"
					name="siniestroDTO.rechazoDesistimiento"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catRespuestaSimple" listKey="key" listValue="value"
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
  		</td>
    </tr>
    <tr id="contenidoValuacionDanos2">
		<td colspan="4" >
			<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
				<tbody>
					<tr>
						<td colspan="4"><label class="labelBlack" style="text-align: left;" ><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.observacionesrechazo" /></label></td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="contenedorTextArea">
								<s:textarea name="siniestroDTO.observacionRechazo" id="observacionesRechazo" 
								cssClass="textarea " cssStyle="font-size: 10pt;"
								cols="80" rows="3" onkeypress="" onchange="truncarTexto(this,3000);"/>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr id="detalleValuacion" >
	
		<td>
			<table style="font-size: 9px;">
				<tr>
					<td>
						<s:textfield id="estatusValuacion" name="siniestroDTO.estatusValuacion" 
						key="midas.siniestros.cabina.reportecabina.afectacionInciso.estatusvaluacion"
						cssClass="cajaTextoM2 setNew w140" readonly="true"  disabled="true"/>
					</td>
					<td>
						<div class="btn_back w20"  style="display:inline; float: left;" id="btn_actualizaEstatusValuacion">
							<a href="javascript: void(0);" onclick="actualizaEstatusValuacion();">
								<img alt="Actualizar" src="/MidasWeb/img/icons/ico_actualizar.gif"> 
							</a>
						</div>	
					</td>
				</tr>
			</table>
		</td>
		<td>
			<s:textfield id="montoValuacion" 
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.montovaluacion"
						value="%{getText('struts.money.format',{siniestroDTO.montoValuacion})}"
					cssClass="cajaTextoM2 setNew" readonly="true"  disabled="true"/>
		</td>
		<td>
			<s:textfield 	cssClass="txtfield cajaTextoM2 w100 formatCurrency setNew"
			key="midas.siniestros.cabina.reportecabina.afectacionInciso.montorecuperado"
							name="siniestroDTO.montoRecuperado"
							size="20"
							id="montoRecuperado"
							onkeypress="return soloNumeros(this, event, true)" />
		</td>
		<td>
			<s:select id="banco" 
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.banco"
					name="siniestroDTO.banco"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="bancoList" listKey="idBanco" listValue="nombreBanco"
			  		cssClass="txtfield cajaTextoM2 requerido setNew w170" />
		</td>
	</tr>
	<tr id="detalleRecuperacion">
		
		<td>
			<s:textfield id="numAprobacion" name="siniestroDTO.numAprobacion"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.numaprobacion"
					 cssClass="cajaTextoM2 setNew jQnumeric" maxlength="50"
					onkeypress="return soloNumeros(this, event, false);">
				<form></form>
			</s:textfield>
		</td>
		<td>
			<s:textfield id="numDeposito" name="siniestroDTO.numDeposito"
					key="midas.siniestros.cabina.reportecabina.afectacionInciso.numdeposito"
					 cssClass="cajaTextoM2 setNew w100 jQnumeric"  maxlength="50"
					onkeypress="return soloNumeros(this, event, false);"/>
						
		</td>	
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.fechadeposito"/>
			<sj:datepicker name="siniestroDTO.fechaDeposito" changeMonth="true"
				changeMonth="true"
				changeYear="true"
				buttonImage="../img/b_calendario.gif"
				buttonImageOnly="true" id="fechaDeposito" maxlength="10"
				cssClass="txtfield setNew" size="10"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>	
		</td>
		<s:if test="siniestroDTO.recuperacionId == '' || siniestroDTO.recuperacionId == null || siniestroDTO.recuperacionId == undefined">	
		<td>
			<s:if test='%{ mostrarBotonGenerarReferencia == true }'>
				<div id="generarReferencias" class="btn_back w140"  style="display:inline; float: left;">
					<a href="javascript: void(0);" onclick="generarReferenciaRecuperacion()" id="btnSolValRec">
						Generar Referencia
					</a>
				</div>
			</s:if>
		</td>
		</s:if>					
	</tr>
	<tr id="referenciasBancarias">
	<td align="center" colspan="5">
		<div id="referenciasBancariasGrid" style="width: 865px; height: 115px; "></div>
	</td>
	
	</tr>
	

	
    
    
  
</table>



<script type="text/javascript">
	dhx_init_tabbars();
	mostrarOcultarEnviarValuacion();
	validaBtnLLegadaOtraCia(jQuery("#causaSiniestro").val());
	mostrarOcultarValuacionRecuperacion();
	mostrarOcultarCompanias();
	mostrarOcultarDanosRechazo();
	jQuery(document).ready(function() {
		initCurrencyFormatOnTxtInput();
		buscarReferenciasBancarias();
		validaComboSeRecibioOrdenCia();
	});

	
</script>