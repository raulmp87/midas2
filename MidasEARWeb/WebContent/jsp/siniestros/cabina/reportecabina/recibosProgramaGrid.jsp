<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingAreaRecibo</param>
				<param>true</param>
				<param>infoAreaRecibo</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.numerorecibo" />   </column>
		<column  type="ro"  width="*"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.folio" />  </column>
		<column  type="ro"  width="150"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.vencimiento" />  </column>
	    <column  type="ro"  width="150"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.finprorroga" />  </column>
	    <column  type="ro"  width="150"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.cubredesde" />  </column>
	    <column  type="ro"  width="150"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.cubrehasta" />  </column>
	    <column  type="ro"  width="*"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.situacion" />  </column>
	</head>

	<s:iterator value="recibosList">
		<row id="<s:property value="#row.index"/>">
		
			<cell><s:property value="numeroRecibo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="folio" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaVencimiento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinProrroga" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaInicioCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaFinCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="situacion" escapeHtml="false" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>
	
</rows>