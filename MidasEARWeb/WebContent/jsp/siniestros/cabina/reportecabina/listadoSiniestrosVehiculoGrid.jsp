<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
	    <column id="numeroReporte" type="ro"  width="100"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.numeroReporte"/></column>
	    <column id="numeroSiniestro" type="ro"  width="100"  sort="str" align="center"><s:text name="midas.listadoSiniestrosAction.titulo.numeroSiniestro"/></column>
	    <column id="nombreAsegurado" type="ro"  width="*"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.asegurado"/></column>
	    <column id="numeroPoliza" type="ro"  width="100"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.numeroPoliza"/></column>
	    <column id="tipoSiniestro" type="ro"  width="190"  sort="str"><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.causaSiniestro"/></column>
	    <column id="terminoSiniestro" type="ro"  width="190"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.terminoSiniestro"/></column>
	    <column id="ajustador" type="ro"  width="100"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.ajustador"/></column>
	    <column id="fechaSiniestro" type="ro"  width="90"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.fechaSiniestro"/></column>
	    <column id="numeroSerie" type="ro"  width="100"  sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.numeroSerie"/></column>
		
	</head>
	<% int a=0;%>
	<s:iterator value="reporteSiniestroList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAsegurado" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="terminoSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="ajustador" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>