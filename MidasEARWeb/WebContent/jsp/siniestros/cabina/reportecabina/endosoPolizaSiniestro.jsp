<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarDetalleInciso = '<s:url action="mostrarDetalleInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
</script>

<s:form id="historialBusquedaInciso" >
		<s:hidden name="filtroBusqueda.servicioPublico"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroSerie"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.placa"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.nombreAsegurado"></s:hidden>
		<s:hidden name="filtroBusqueda.servicioParticular"></s:hidden>
		<s:hidden name="filtroBusqueda.numeroPoliza"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroMotor"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaIniVigencia"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaFinVigencia"></s:hidden>
		<s:hidden name="fechaReporteSiniestro"></s:hidden>
		<s:hidden name="idReporte"></s:hidden>
		<s:hidden name="validOnMillis"></s:hidden>
		<s:hidden name="recordFromMillis"></s:hidden>
		<s:hidden name="fechaReporteSiniestroMillis"></s:hidden>
		<s:hidden name="soloConsulta" id="h_soloConsulta"/>
</s:form>

<s:form id="endososForm">
		
		<s:hidden name="incisoContinuity" id="incisoContinuity"></s:hidden>
		<s:hidden name="fechaReporteSiniestro" id="fechaReporteSiniestro"></s:hidden>
		
		
		
			<div class="titulo">
			<s:text name="midas.consulta.endoso.inciso.titulo" />
			</div>
			<table height="29%" width="99%" class="contenedorConFormato" style="width: 99%; height: 250px;" >
				<tr>
					<td valign="top" height="29%" width="99%" >
						<div id="indicador"></div>
						<div id="consultaEndosoInciso" style="width: 98%;height:250px;"></div>
						<div id="pagingArea"></div><div id="infoArea"></div>
					</td>
				</tr>			
			</table>
			
			<br>
			
			<div class="titulo">
				<s:text name="midas.consulta.endoso.inciso.descripcion" />
			</div>
			
			<table height="15%" width="99%" class="contenedorConFormato">
				<tr>
					<td valign="top">
						<s:textarea cssStyle="width:99%; height=99%; " id="resumenDatos" rows="20" name="resumen" />
					</td>
				</tr>
			</table>
			
			<div class="btn_back w80" style="display: inline; float: right;">
				       <a href="javascript: void(0);" onclick="cerrarEndosos();"> 
				       <s:text name="midas.boton.cerrar" /> </a>
				</div>
		
</s:form>

<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/endosoPolizaSiniestro.js'/>" ></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>


<script type="text/javascript">
	iniciaListadoGrid();
</script>