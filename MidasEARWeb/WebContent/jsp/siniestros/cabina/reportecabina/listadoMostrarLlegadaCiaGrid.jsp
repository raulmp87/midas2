<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		    <column id="c_cia" type="ro"  width="604" align="center"><s:text name="midas.siniestros.cabina.reportecabina.llegadacia.compania"/></column>
            <column id="c_orden" type="ro"  width="*"  sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.llegadacia.orden"/></column>
          	<s:if test="soloConsulta != 1">
          		<column id="eliminar"  type="img" width="80" sort="na" align="center" ></column>
          	</s:if> 
	</head>			
	<s:iterator value="llegadaCiaSegurosList" status="row">
		<row id="<s:property value="id"/>">
			<cell ><s:property value="proveedor.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="orden" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/delete14.gif^Eliminar^javascript: eliminarLlegadaCia(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>	
</rows>
