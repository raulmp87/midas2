<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="*"  align="center"> <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.programa" />   </column>
		<column  type="ro"  width="*"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.documentoorigen" />  </column>
		<column  type="ro"  width="*"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.tipoendoso" />  </column>
	    <column  type="ro"  width="200"  align="center" > <s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.numerorecibos" />  </column>
	</head>

	<s:iterator value="programasPagoList">
		<row id="<s:property value="numeroPrograma"/>">
		
		    <cell><s:property value="numeroPrograma" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="documentoOrigen" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoEndoso" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroRecibos" escapeHtml="false" escapeXml="true"/></cell>
		    
		    <userdata name="idCotizacionSeycosCell"><s:property value="idCotizacionSeycos" escapeHtml="false" escapeXml="true"/></userdata>
			
		</row>
	</s:iterator>
	
</rows>