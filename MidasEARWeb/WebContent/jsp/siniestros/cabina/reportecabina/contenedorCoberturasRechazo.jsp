<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<sj:head/>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_sub_row.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
var guardarMontosCoberturasRechazoPath = '<s:url action="guardarCoberturasRechazo" namespace="/siniestros/cabina/siniestrocabina"/>';
var buscarCoberturasRechazoPath = '<s:url action="buscarCoberturasRechazo" namespace="/siniestros/cabina/siniestrocabina"/>';
</script>

<s:form id="coberturasRechazoForm" action="guardarMontosCoberturasRechazoPath" namespace="/siniestros/cabina/siniestrocabina" name="coberturasRechazoForm">
	<s:hidden name="idReporteRechazo" id="idReporteRechazo"></s:hidden>
	<s:hidden name="siniestroDTO.montoDanos" id="montoRechazoTotal"></s:hidden>
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.coberturasRechazoBtn"/>	
	</div>
	
	<div id="indicadorCoberturasRechazo"></div>
	<div id="coberturasRechazoGridContainer" style="">
		<div id="contenedorCoberturasRechazoGrid" style="width:98%;height:340px;"></div>
		<div id="pagingAreaCoberturasRechazo"></div><div id="infoAreaCoberturasRechazo"></div>
	</div>
	
	<br/>
	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:cerrarVentanaCoberturasRechazo();}"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
	<div id="btn_guardarMontosRechazo" class="btn_back w80 " style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="javascript:guardarMontosCoberturasRechazo();"> 
		<s:text name="midas.boton.guardar" /> </a>
	</div>
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
	<br/>
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
		buscarMontosCoberturasRechazo();
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>