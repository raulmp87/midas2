<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="afectada" type="img" width="80" sort="na" align="center"><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.afectadas"/></column>
		<column id="nombreCobertura" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.coberturas"/></column>
		<column id="valorSumaAsegurada" type="ro" width="190" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.sumaAsegurada"/></column>
		<column id="saEstimada" type="ro" width="130" sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.saestimada"/></column>
		<column id="montoPorcentaje" type="ro" width="130" sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.montoPorcentaje"/></column>
		<column id="importeDeducible" type="ro" width="120"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.importeDeducible"/></column>
		<column id="estimado" type="ro" width="130"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.estimado"/></column>
		<column id="claveTipoCalculo" type="ro" width="1" sort="int" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>		
		<column id="acciones" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>		
	</head>			
	<s:iterator value="listAfectacionCoberturaSiniestroDTO" status="row">
		<row id="<s:property value="%{coberturaReporteCabina.coberturaDTO.idToCobertura}"/>_<s:property value="#row.index"/>">
			<cell>
				<s:if test='siniestrado == true'>../img/checked16.gif</s:if>
				<s:else>../img/pixel.gif</s:else>
			</cell>	
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="coberturaReporteCabina.claveTipoCalculo == 'RCV'">
					<s:property  value="%{coberturaReporteCabina.diasSalarioMinimo}" escapeHtml="false" escapeXml="true"/> <s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.UMA"/>
				</s:if>	
				<s:elseif test="configuracionCalculoCobertura.valorSumaEstimada == 'PASAJERO'">
					<s:property  value="%{getText('struts.money.format',{montoSumaAsegurada})}" escapeHtml="false" escapeXml="true"  />
				</s:elseif>						
				<s:elseif  test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 0">
<%-- 					<s:if test="coberturaReporteCabina.valorSumaAseguradaMax != 0.0 && coberturaReporteCabina.valorSumaAseguradaMin!= 0.0"> --%>
<%-- 						&lt;div style='font-size:8px;'&gt;<s:text name="midas.poliza.inciso.cobertura.entre" />						 --%>
<%--  							<s:property value="%{getText('struts.money.format',{coberturaReporteCabina.valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true"  /> --%>
<%-- 								<s:text name="midas.general.y"/>  --%>
<%-- 								<s:property value="%{getText('struts.money.format',{coberturaReporteCabina.valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />&lt;/div&gt; --%>
<%-- 					</s:if>					 --%>
					<s:property  value="%{getText('struts.money.format',{coberturaReporteCabina.valorSumaAsegurada})}" escapeHtml="false" escapeXml="true"  />		
				</s:elseif>	
				<s:elseif test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 1">				
					<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
				</s:elseif>
				<s:elseif test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 2">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
					<s:property  value="%{coberturaReporteCabina.valorSumaAsegurada}" escapeHtml="false" escapeXml="true"/>				
				</s:elseif>
				<s:elseif test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 9">
					<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />				
				</s:elseif>
				<s:elseif test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada == 3">				
					<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
				</s:elseif>
			</cell>	
			<cell>
				<s:property value="%{getText('struts.money.format',{sumaAseguradaEstimada})}" escapeHtml="false" escapeXml="true"/>
			</cell>
			<cell>
				<s:if test='coberturaReporteCabina.coberturaDTO.claveTipoDeducible == "0"'>
					</s:if>
					<s:elseif test='coberturaReporteCabina.coberturaDTO.claveTipoDeducible == "1"'>
						<s:property  value="%{coberturaReporteCabina.porcentajeDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:elseif>
					<s:else>
						<s:property  value="%{coberturaReporteCabina.valorDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:else>
					<s:if test='coberturaReporteCabina.coberturaDTO.claveTipoDeducible != "0"'>
						<s:property  value="%{coberturaReporteCabina.descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:if>
			</cell>	
			<cell></cell>	
			<cell><s:property value="%{getText('struts.money.format',{montoEstimado})}" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/></cell>
			
			<s:if test="coberturaReporteCabina.coberturaDTO.claveFuenteSumaAsegurada != 3">
					<s:if test="soloConsulta == 1"><cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:validarVigenciaPoliza(1,<s:property value="coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/>","<s:property  value="configuracionCalculoCobertura.tipoEstimacion" escapeHtml="false" escapeXml="true"/>",<s:property  value="coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" escapeHtml="false" escapeXml="true"/>)^_self</cell></s:if>
					<s:if test= 'esSiniestrable == true'><cell>../img/icons/ico_editar.gif^Editar^javascript:validarVigenciaPoliza(0,<s:property value="coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/>","<s:property  value="configuracionCalculoCobertura.tipoEstimacion" escapeHtml="false" escapeXml="true"/>",<s:property  value="coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" escapeHtml="false" escapeXml="true"/>)^_self</cell></s:if>
					<s:else><cell>../img/blank.gif</cell></s:else>
			</s:if>
			<s:else><cell>../img/blank.gif</cell></s:else>
			<s:if test="tieneDatosRiesgo == 1"><cell>../img/b_comentario.jpg^Observaciones^javascript:mostrarDatosRiesgoCoberturaAfectacion(${coberturaReporteCabina.coberturaDTO.idToCobertura})^_self</cell></s:if>
			
		
		</row>
	</s:iterator>	
</rows> 

