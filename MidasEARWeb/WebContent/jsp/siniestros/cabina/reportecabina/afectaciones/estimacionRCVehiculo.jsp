<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>
<script src="<s:url value='/js/siniestros/valuacion/valuacionreporte.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#wwlbl_txt_lada1, #wwlbl_txt_lada2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 20px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_tel1, #wwlbl_txt_tel2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 70px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_companiaSegurosTercero, #wwlbl_txt_companiaSegurosTercero_sre  {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 160px !important;
    float:left; 
}


</style>

<script type="text/javascript">
var crearNuevoPasePath = '<s:url action="crearNuevoPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
</script>

<s:form id="estimacionForm" action="guardar" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura" name="estimacionForm">
<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="permiteCapturarCiaInfo" id="h_permiteCapturarCiaInfo"></s:hidden>
<s:hidden name="permiteCapturarNoSiniestroTercero" id="h_permiteCapturarNoSiniestroTercero"></s:hidden>


<s:hidden name="estimacionCoberturaSiniestro.estimacionVehiculos.id" id="idEstimacionCoberturaReporteCabina" cssClass="setNew" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionVehiculos.coberturaReporteCabina.claveTipoCalculo" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionVehiculos.tipoEstimacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.idToCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>
<s:hidden id="h_consultaPase"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionVehiculos.secuenciaPaseAtencion" id="h_secuenciaPaseAtencion"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionVehiculos.secuenciaPaseDeCobertura" id="h_secuenciaPaseDeCobertura"></s:hidden>
<s:hidden name="idEstimacionCoberturaReporte" ></s:hidden>
<s:hidden id="tieneAntiguedadELNoSerie" name="estimacionCoberturaSiniestro.estimacionVehiculos.involucradoEnSiniestros"></s:hidden>
<s:hidden id="h_porDeducible" name="porcentajeDeducible"></s:hidden>
<s:hidden id="h_estimacionCobertura" name="estimacionCoberturaSiniestro.tipoCoberturaAutoInciso" ></s:hidden>
<s:hidden id="montoDeducibleCalculado" name="estimacionCoberturaSiniestro.montoDeducibleCalculado" ></s:hidden>
<s:hidden id="h_motivoDeducible" name="motivoNoAplicaDeducibleSel" ></s:hidden>

<s:hidden id="h_requieerAutorizacion" name="requiereAutorizacion" ></s:hidden>
<s:hidden id="h_idParametroAntiguedad" name="idParametroAntiguedad" ></s:hidden>


<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionVehiculo.title"/>	
</div>
</br>
</br>
<s:include value="/jsp/siniestros/cabina/reportecabina/afectaciones/datosGeneralesReporte.jsp"></s:include>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield setNew deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.folio"
				labelposition="top" 
						 size="20"	
					readonly="true"				
						   id="txt_folio"/>
				</td>	
				<td colspan="1">
					<s:select id="s_tipoPaseAtencion" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.tipoPaseAtencion"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.tipoPaseAtencion"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
							 onfocus="this.oldvalue = this.value;"
							 onchange="aplicaSeleccionTipoPaseDeAtencion(this);validaSiPermiteCapturarCiaInfo();cambioTieneCiaSegurosSre();"
					  		 list="pasesAtencion" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield  requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA  setNew" /> 	
				</td>
				<td colspan="1">
					<div class = "elegible elegibleSIPAC elegibleSIPACSRE">
						<s:select id="s_tipoTransporte"
							 key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tipoTransporte"
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.tipoTransporte"
							 onchange="obtenerEstimacionNueva();"
							 labelposition="top"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
							 list="ctgTipoTransporte" 
					  		 listKey="key" listValue="value"  
							 cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew"
						/>
					</div>
				</td>
				<td colspan="1">
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA">
						<s:select id="s_causaMovimiento" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="causasMovimiento" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoPDA setNew requeridoParaAutorizar" /> 	
					</div>
				</td>
				<td>
					<s:select id="s_estatus" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estatus"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.estatus"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield setNew" 
					  		 onchange="onChangeEstatus();" 
				  		 	 disabled="true" /> 
					 <s:hidden id="h_estatus" name="idEstatus"></s:hidden> 
				</td>
			</tr>
			
			<tr>
				<td width="17%">
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA" style="float:left">
						<s:textfield cssClass="txtfield jQrestrict jQ2float requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoPDA setNew formatCurrency requeridoParaAutorizar" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
							 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"
					labelposition="top" 
							 size="10"
					    maxlength="10"	
					onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
					onblur ="mascaraDecimales('#txt_estimacionNueva',this.value);"	 	
							   id="txt_estimacionNueva"/>
					</div>
					<div class="btn_back w50 elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA" style="display: inline; float: right; margin-top:26px;" id="b_copiarEstimacion" >
					<a href="javascript: void(0);" onclick="copiarValorEstimacionActual();">
						<s:text name="&lt&lt" /> 
					</a>
					</div>
				</td>
				<td width="10%">
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA">
								<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
							 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"
					labelposition="top" 
							 size="12"	
							 readonly="true"			
							   id="txt_estimacionActual"/>
					</div>
				</td>
				<td width="10%">
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA">
						<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
						 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"
				labelposition="top" 
						 size="12"	
						 readonly="true"				
						   id="txt_importePagado"/>
					</div>
					
				</td>
				<td width="10%">
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA">
							<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
							 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"
					labelposition="top" 
							 size="12"	
							 readonly="true"				
							 id="txt_reserva"/>
					</div>
					
				</td>
				<td width="10%">
				
						<div class = "elegible elegibleSIPAC elegibleSIPACSRE">
							<s:textfield
							  cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew jQalphanumeric jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.clave"
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.claveAjustador"
							labelposition="top" 
							 size="10"	
							 maxlength="10"		
							 width="25%"			
							 id="txt_clave"/>
						</div>
					
				</td> 
				<td width="10%">
						<div class = "elegible elegibleSIPAC elegibleSIPACSRE">
							<s:textfield 
							  cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew jQalphabeticExt jQrestrict"
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAjustadorTercero"
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.nombreAjustador"
					labelposition="top" 
							 size="20"		
							 width="40%"		
							 id="txt_nombreAjustador"/>
						</div>
					
				</td>
				
			</tr>			
			
			<tr>	   
				<td>
					<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegiblePDA">
							<s:select id="s_aplicaDeducible"
								key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
								labelposition="top" name="apDeducible" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								list="aplicaDeducible" listKey="key" listValue="value"
								onchange="onChangeApDeducible('RCV');"
								cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoPDA setNew" />
						</div>
				</td>	
				<td width="10%">
					<div class="montoDeducible">
						<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict formatCurrency" 
										 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.cantidad.deducible"
										 name="estimacionCoberturaSiniestro.montoDeducible"
								labelposition="top" 
										 maxlength="20"
										 size="10"			
										   id="montoDeducible"/>
					</div>
				</td>
				<td> 
					<div class="ctgMotivoNoaplicaDeducible">  
						<s:select id="ctgMotivoNoaplicaDeducible" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.motivo.no.deducible"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.motivoNoAplicaDeducible"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="ctgNoAplicaDeducible" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield setNew" /> 
					</div>
				</td>	
				<td>
						
					<div class = "elegible elegibleSIPAC elegibleSIPACSRE">
							<s:textfield  
							  cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew jQalphanumeric jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.DUA"
							  name="estimacionCoberturaSiniestro.estimacionVehiculos.dua"
							  labelposition="top" 
							  size="25"	
							  maxlength="25"	
							  width="75%"			
							  id="txt_DUA"/>
					</div>
						
				</td> 
				<td colspan = "2">
				
				      <div class = "elegible elegibleSIPAC elegibleSIPACSRE">
							<s:select id="s_circunstancia"
							 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.circunstancia"
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.circunstancia"
							 labelposition="top"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
							 list="circunstancia" 
					  		 listKey="key" listValue="value" 
					  		 cssStyle="max-width:50%;"
							 cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew" />
					  </div>
				</td>				
			</tr>	
			<tr>
					<td colspan="4">
					
					</td>
					
					<td>
						 <div class = "elegible elegibleSIPAC elegibleSIPACSRE">
							<s:checkbox 
				             	key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.otraCircunstancia"
				             	name="estimacionCoberturaSiniestro.estimacionVehiculos.esOtraCircunstancia"
				             	labelposition="left"
				    			id="ckb_otraCircunstancia"
				    			onclick="validarCampoCircunstancia('RCV')"
				             />
				         </div>				             
				    </td>
				             
				             
				     <td>
				         <div class = "elegible elegibleSIPAC elegibleSIPACSRE">
				             <s:textfield  			              
							  cssClass= "txtfield setNew jQalphabeticExt jQrestrict"
							  name="estimacionCoberturaSiniestro.estimacionVehiculos.otraCircunstancia"
							  labelposition="top" 
							  size="25"	
							  width="75%"			
							  id="txt_otraCircunstancia_detalle"
							 />
						</div>
					</td>	
			</tr>
		</tbody>
	</table>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.title"/>
</div>	
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td colspan="2">
				<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew jQalphabeticExt jQrestrict" 
					 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.nombreAfectado"
			labelposition="top" 
					 size="50"
					 maxlength="100"				
					   id="txt_nombreAsegurado_"  />
			</td>	
			<td colspan="3">
				<s:textfield cssClass="txtfield setNew jQalphabeticExt jQrestrict" 
					 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.personaContacto"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.nombrePersonaContacto"
			labelposition="top" 
					 size="50"
					 maxlength="100"				
					   id="txt_nombreContacto_"/>
			</td>
			<td width="5%">
				<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.ladaTelContacto"
			labelposition="top" 
					 maxlength="3"
					 size="4"			
					   id="txt_lada1"  />
			</td>
			<td width="10%">
				<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.telContacto"
			labelposition="top" 
					 maxlength="8"
					 size="12"				
					   id="txt_tel1"  />
			</td>
			<td width="10%">
				<div class = "elegible elegibleSIPAC elegibleSIPACSRE">
					<s:select id="s_tipoPersona"
							 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.tipoPersona"
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.tipoPersona"
							 labelposition="top"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
							 list="tiposPersona" 
					  		 listKey="key" listValue="value"  
							 cssClass="txtfield requeridoSIPAC requeridoSIPACSRE setNew"
						/>
				</div>
			</td>
			<td width="15%">&nbsp;</td>
		</tr>
		<tr>
			<td width="15%">
				<table>
					<tbody>
						<tr>
							<td width="5%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
									 name="estimacionCoberturaSiniestro.estimacionVehiculos.ladaTelContactoDos"
							labelposition="top" 
									 maxlength="3"
									 size="4"				
									   id="txt_lada2"/>
							</td>
							<td width="10%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
									 name="estimacionCoberturaSiniestro.estimacionVehiculos.telContactoDos"
							labelposition="top" 
									 maxlength="8"
									 size="12"			
									   id="txt_tel2"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20%">
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailinteresado"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.email"
			labelposition="top"
					 maxlength="50" 
					 size="30"				
					  id="txt_email"
					  onblur="onBlurEmailInteresado();"
					 onchange="validationEmail(jQuery('#txt_email').val())"/>
			</td>
			<td colspan="4">
			<div style="width:35%;float:left;margin-top: 22px;">
				<s:checkbox id="emailNoProporcionado" name="estimacionCoberturaSiniestro.estimacionVehiculos.emailNoProporcionado" cssClass="setNew" 
				key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionado"
				labelposition="right" onchange="onChangeEmailNoProporcionado();" ></s:checkbox>
			</div>
			<div style="width:55%;float:right;margin-top:4px;">
				<s:select id="emailNoProporcionadoMotivo" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionadomotivo"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.emailNoProporcionadoMotivo"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="motivosCorreoNoPropocionado" 
					  		 cssClass="txtfield setNew" />
			</div>
			</td>
			<%-- <td width="10%">
				<s:select id="s_llegadaCia" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.rcBienes.llegadaCia"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.estimacionVehiculos.llegadas"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="ciaLlegadas" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield setNew"/> 
			</td>--%>
			<td id="td_numeroSiniestro" width="10%">
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.noSiniestroTercero"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.numeroSiniestroTercero"
			labelposition="top" 
					 value="%{numeroSiniestroAfectacion}"
					 size="18"
					 maxlength="18"		
					 readonly="true"
					   id="txt_noSiniestroTercero"/>
			</td>
			<td id="td_numeroSiniestro_estimacion" width="10%">
				<s:textfield cssClass="txtfield setNew elegible elegibleSRECIA" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.noSiniestroTercero"
					 name="estimacionCoberturaSiniestro.estimacionVehiculos.numeroSiniestroTercero"
			labelposition="top" 
					value="%{numeroSiniestroEstimacion}"
					 size="18"
					 maxlength="18"		
					   id="txt_noSiniestroTercero_estimacion"/>
			</td>
		</tr>
		<tr>
			 <s:hidden name="idCompaniaSegurosTercero" id="txt_idcompaniaSegurosTercero"></s:hidden>
			<s:if test="permiteCapturarCiaInfo == true">
					<td>	
						<div id="info_compania_id">
						<s:textfield cssClass="txtfield setNew deshabilitado"
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
								  value="%{estimacionCoberturaSiniestro.nombreCompania}"
						labelposition="top" 
								 size="40"	
								 readonly="true"
								   id="txt_companiaSegurosTercero"/>
						</div>
					</td>
					<td id="td_tieneCia">
				<div class="elegible  elegibleSRECIA">
						<s:select id="s_tieneCiaSeguros" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.rcvehiculo.tieneCiaSeguros"
							     labelposition="top" 
								 name="tieneCiaSegurosValor"
								 onchange="cambioTieneCiaSeguros(this, 'txt_companiaSegurosTercero', 'txt_idcompaniaSegurosTercero', 'td_ciaSeguros')"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="tieneCiaSeguros" 
						  		 listKey="key" listValue="value"
						  		 cssClass="txtfield setNew" /> 
				</div>
				
			</td>
				<td id="td_ciaSeguros" colspan="2">
					<table>
						<tbody>
							<tr>
								<td>
									<div>
										<s:textfield cssClass="txtfield setNew deshabilitado" 
											  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
											 value="%{estimacionCoberturaSiniestro.estimacionVehiculos.companiaSegurosTercero.personaMidas.nombre}"
									         labelposition="top" 
											 size="40"		
											 readonly="true"
											 id="txt_companiaSegurosTercero_sre"/>
											
									</div>
									
								</td>
								<td>
									<div class="btn_back w50" style="display: inline; float: left;"  id="btnBuscarCia">
										<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('CIA',jQuery('#txt_idcompaniaSegurosTercero'),jQuery('#txt_companiaSegurosTercero_sre'));">
											<s:text name="midas.boton.buscar" />
										</a>
									</div>
								</td>		
							</tr>
						</tbody>
					</table>
				</td>
			</s:if>
		
			<s:else>
			<td id="td_tieneCia">
				<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC elegibleSRECIA">
						<s:select id="s_tieneCiaSeguros" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.rcvehiculo.tieneCiaSeguros"
							     labelposition="top" 
								 name="tieneCiaSegurosValor"
								 onchange="cambioTieneCiaSeguros(this, 'txt_companiaSegurosTercero', 'txt_idcompaniaSegurosTercero', 'td_ciaSeguros')"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="tieneCiaSeguros" 
						  		 listKey="key" listValue="value"
						  		 cssClass="txtfield setNew" /> 
				</div>
				
			</td>
				<td id="td_ciaSeguros" colspan="2">
					<table>
						<tbody>
							<tr>
								<td>
									<div class="elegible elegiblePAT elegibleRAC elegibleSIPAC">
										<s:textfield cssClass="txtfield setNew deshabilitado" 
											  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
											 value="%{estimacionCoberturaSiniestro.estimacionVehiculos.companiaSegurosTercero.personaMidas.nombre}"
									         labelposition="top" 
											 size="40"		
											 readonly="true"
											 id="txt_companiaSegurosTercero"/>
											
									</div>
									
								</td>
								<td>
									<div class="btn_back w50 elegible elegiblePAT elegibleRAC elegibleSIPAC" style="display: inline; float: left;"  id="btnBuscarCia">
										<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('CIA',jQuery('#txt_idcompaniaSegurosTercero'),jQuery('#txt_companiaSegurosTercero'));">
											<s:text name="midas.boton.buscar" />
										</a>
									</div>
								</td>		
							</tr>
						</tbody>
					</table>
				</td>
			</s:else>
			<td colspan="5">&nbsp;</td>		
		</tr>
	</tbody>
</table>
</div>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td width="20%">
					<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.marca"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.marca"
				labelposition="top" 
						 size="30"				
						   id="txt_marca"  
					 	maxlength="50"/>
				</td>	
				<td >
					<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estilo"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.estiloVehiculo"
				labelposition="top" 
						 size="30"			
						   id="txt_estilo" 
					 	maxlength="49"/>
				</td>
				<td  width="10%">
					<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew jQnumeric jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.modelo"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.modeloVehiculo"
				labelposition="top" 
						 size="20"				
						   id="txt_modelo" 
					 	maxlength="4"/>
				</td>
				<td  width="15%">
					<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.placas"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.placas"
				labelposition="top" 
						 size="20"					
						   id="txt_placas" 
					 	maxlength="12"/>
				</td>
				<td  width="35%">
				<div class="btn_back w70" style="display: inline; float: left;" id="b_imagenes" >
					<a href="javascript: void(0);" onclick="ventanaFortimax('PRCV',txt_folio.value , 'FORTIMAX',jQuery('#h_reporteCabinaId').val() , jQuery('#txt_numeroReporte').val() );">
						<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> 
					</a>
				</div>
				</td>
			</tr>
			<tr>
				<td> 
				</td>
				<td colspan="6">
					<div id="mensajeNoSerieCESVI" style="font-weight:bold; font-size:11px; color:blue"> 
						<label for="respuestaCESVI" style="vertical-align: middle"></label>
					</div>
				</td>
			</tr>
			<tr>
				<td><s:select id="s_llegadaCia" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.color"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.estimacionVehiculos.color"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="colores" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew" /> 
				</td>
				<td>
					<div id="mensajeNoSerie" style="font-weight:bold; font-size:11px; color:red"> 
					
					<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.avisoNumeroDeSerie"/> <br/>
					
					
					</div>
					
					<s:textfield cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew jQalphabeticExt jQrestrict"
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noSerie"
						 name="estimacionCoberturaSiniestro.estimacionVehiculos.numeroSerie"
							labelposition="top" 
						 size="30"	
						 onblur ="validaNoSerie();"	 				
						   id="txt_noSerie" 
					 	maxlength="20" />
				</td>
				<td>
					<div 	class="btn_back w50" 
							style="display: inline; float: left;"  
							id="btnBuscarCia">
							
						<a href="javascript: void(0);" 
						onclick="javascript:validaNoSerieCESVI();"
						>
							<s:text name="midas.boton.validar" />
						</a>
					</div>
				</td>	
				
				<td>
					<s:textfield 
							id="txt_idNumeroMotorTercero" 
							cssClass="txtfield requerido requeridoPAT requeridoRAC requeridoSIPAC requeridoSRE requeridoPDA setNew jQalphabeticExt jQrestrict" 
						  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noMotor"
						 	name="estimacionCoberturaSiniestro.estimacionVehiculos.numeroMotor"
							labelposition="top" 
						 	size="30"		
					 		maxlength="20" 
					 		/>
				</td>			
				<td id="td_estado">
					<div class="elegible elegiblePAT elegiblePDA">
							<s:select id="s_estado" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estado"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionVehiculos.estado.id"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estados" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido requeridoPAT requeridoPDA setNew" /> 
					</div>
				</td>
				<td id="td_taller">
					<div class="elegible elegiblePAT elegiblePDA">
							<s:textfield cssClass="txtfield requerido requeridoPAT requeridoPDA setNew deshabilitado" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tallerAsignado"
								 value="%{estimacionCoberturaSiniestro.estimacionVehiculos.taller.personaMidas.nombre}"
						labelposition="top" 
								 size="40"	
							readonly="true"  		
								   id="txt_tallerAsignado"/>
							<s:hidden name="idTaller" id="txt_idTaller"></s:hidden>
					</div>
				</td>
				<td>
					<div class="btn_back w50 elegible elegiblePAT elegiblePDA" style="display: inline; float: left;"  id="btnBuscarTaller">
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicioByEstado('TALL_AGENC',jQuery('#txt_idTaller'),jQuery('#txt_tallerAsignado'),jQuery('#s_estado').val());">
							<s:text name="midas.boton.buscar" /> 
						</a>
					</div>
				</td>
				
			</tr>
		</tbody>
	</table>
</div>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.observacion"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textarea name="estimacionCoberturaSiniestro.estimacionVehiculos.danosCubiertos" 
							id="ta_danosCubiertos" labelposition="top"
							key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.danosCubiertos" 
							cols="150"
							rows="5"
							cssClass="textarea setNew" />
				</td>	
			</tr>
			<tr>
				<td>
					
						<s:checkbox 
									key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.SIONODanosPreExistentes"
				             		name="estimacionCoberturaSiniestro.estimacionVehiculos.tieneDanosPreexistentes"
				             		id="ckb_danosPrexistentes"
				             	    labelposition="left"
				             	    onclick="validarDanosPrexistentes();"
				        />
				     
				</td>
			</tr>
			<tr>
				<td>
					<div id = "area_danosPrexistentes">
					<s:textarea name="estimacionCoberturaSiniestro.estimacionVehiculos.danosPreexistentes" 
							id="ta_danosPreexistentes" labelposition="top"
							key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.danosPreexistentes" 
							cols="150"
							rows="5"
							cssClass="textarea setNew" />
					</div>
				</td>	
			</tr>			
		</tbody>
	</table>
</div>
</s:form>
<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarCoberturaGral();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>
			<s:if test="requiereAutorizacion == 0">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
						<a href="javascript: void(0);" onclick="javascript: guardarEstimacionGral();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
						</a>
					</div>	
			</s:if>
		    <s:else>
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_autorizar" >
						<a href="javascript: void(0);" onclick="javascript: autorizarSolicitudDeReserva();"> 
							<img border='0px' alt='Autorizar' title='Autorizar'/>
						</a>
					</div>
		    </s:else>
			<s:if test="idEstimacionCoberturaReporte != null">
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_valuacion" >
					<a href="javascript: void(0);" onclick="javascript: mostrarModalValuacionHgs();"> 
						<s:text name="midas.boton.valuacion" />
						<img border='0px' alt='Valuacion' title='Valuacion' src='/MidasWeb/img/b_mas_agregar.gif'/> 
					</a>
				</div>
			</s:if>	
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_nuevo" >
				<a href="javascript: void(0);" onclick="javascript: iniciarEstimacion();"> 
					<s:text name="midas.boton.nuevo" />
					<img border='0px' alt='Nuevo' title='Nuevo' src='/MidasWeb/img/b_mas_agregar.gif'/> 
				</a>
			</div>	
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_indemnizar" >
				<a id="btn_Indemnizar" href="javascript: void(0);" onclick="javascript: ventanaModalCompra(jQuery('#idEstimacionCoberturaReporteCabina').val());"> 
					<s:text name="Indemnizar" /> 
				</a>
			</div>
			
		</td>							
	</tr>
</table>				
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script>
jQuery(document).ready(function(){
	onBlurRequeridos();
	aplicaSeleccionTipoPaseDeAtencion(jQuery('#s_tipoPaseAtencion'));
	
	validarCampoCircunstancia('RCV');
	validarDanosPrexistentes();
	
	onChangeEstatus();
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	muestraMensajeNoSerie();
	onChangeEmailNoProporcionado();
	//cia seguros
	var ciaSRE = jQuery("#h_permiteCapturarCiaInfo").val();

	var ciaSegurosVal = jQuery('#s_tieneCiaSeguros').val();
	
	validaSiPermiteCapturarCiaInfo();
	
	if(ciaSegurosVal == null || ciaSegurosVal == '' || ciaSegurosVal == 'N'){
		jQuery('#td_ciaSeguros').hide();
	}else{
		jQuery('#td_ciaSeguros').show();
	}	
	
	initDeducible();
	
	
});
</script>	