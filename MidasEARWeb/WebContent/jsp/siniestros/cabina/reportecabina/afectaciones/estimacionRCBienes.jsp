<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/direccion/direccionSiniestroMidas.js'/>"  type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script> 
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

#wwlbl_txt_lada1, #wwlbl_txt_lada2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 20px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_tel1, #wwlbl_txt_tel2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 70px !important;
    position: relative;
    float: left;
}

#wwlbl_s_tiposBien label{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}

#wwlbl_txt_responsableReparacion label{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 170px !important;
}

table tr td table tr td label{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 65px !important;
}
.oculto{
	display: block;
}

.error {
	background-color: red;
	opacity: 0.4;
}

</style>
<script type="text/javascript">
var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
var crearNuevoPasePath = '<s:url action="crearNuevoPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
</script>
<s:form id="estimacionForm" >
<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="idEstimacionCoberturaReporte" ></s:hidden>

<s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.id" id="h_id" cssClass="setNew"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.claveTipoCalculo" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.tipoEstimacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.idToCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>
<s:hidden id="h_consultaPase"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.secuenciaPaseAtencion" id="h_secuenciaPaseAtencion"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.secuenciaPaseDeCobertura" id="h_secuenciaPaseDeCobertura"></s:hidden>

<s:hidden id="h_porDeducible" name="porcentajeDeducible"></s:hidden>
<s:hidden id="h_estimacionCobertura" name="estimacionCoberturaSiniestro.tipoCoberturaAutoInciso" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.montoDeducibleCalculado" id="montoDeducibleCalculado"></s:hidden>
<s:hidden id="h_requieerAutorizacion" name="requiereAutorizacion" ></s:hidden>
<s:hidden id="h_idParametroAntiguedad" name="idParametroAntiguedad" ></s:hidden>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionBienes.title"/>	
</div>
</br>
</br>
<s:include value="/jsp/siniestros/cabina/reportecabina/afectaciones/datosGeneralesReporte.jsp"></s:include>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.title"/>	
</div>
</br>
</br>
<div id="contenedorEstimacion" style="width: 95%;">
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td>
				<s:textfield cssClass="txtfield setNew deshabilitado" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio"
					 name="estimacionCoberturaSiniestro.estimacionBienes.folio"
			labelposition="top" 
					 size="20"	
				readOnly="true"				
					   id="txt_folio"/>
			</td>	
			<td colspan="2">
				<s:select id="s_tipoPaseAtencion" 
				         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.tipoPaseAtencion"
					     labelposition="top" 
						 name="estimacionCoberturaSiniestro.estimacionBienes.tipoPaseAtencion"
						 headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="pasesAtencion" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield requerido requeridoOBC requeridoSRE setNew"
				  		 onchange="aplicaSeleccionTipoPaseDeAtencion(this);" /> 	
			</td>
			<td>
				<div class="elegible elegibleOBC elegiblePDA">
					<s:select id="s_causaMovimiento" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="causasMovimiento" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield setNew requerido  requeridoOBC requeridoPDA requeridoParaAutorizar"
					  		 /> 	
				</div>
			</td>
			<td>
				<s:select id="s_estatus" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estatus"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionBienes.estatus"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield" 
						  	 onchange="onChangeEstatus();" 
					  		 disabled="true" /> 
					<s:hidden id="h_estatus" name="idEstatus"></s:hidden>
			</td>
			<s:if test="estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.claveTipoDeducible > 0 && ( estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.valorDeducible > 0 || estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.porcentajeDeducible > 0  )">   
					<!--  <td>
						<div class="elegible elegibleOBC">
								<s:select id="s_aplicaDeducible" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
								     labelposition="top" 
									 name="apDeducible"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="aplicaDeducible" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield requerido  requeridoOBC setNew" /> 
						</div>
					</td>	-->
			</s:if>
		</tr>
		<tr>
			<td width="17%">
				<div class="elegible elegibleOBC elegiblePDA" style="float:left">
						<s:textfield cssClass="txtfield requerido  requeridoOBC requeridoPDA requeridoParaAutorizar setNew jQ2float jQrestrict formatCurrency" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
							 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"
					labelposition="top" 
							 size="10"
							 maxlength="10"	
						  onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
						  onblur ="mascaraDecimales('#txt_estimacionNueva',this.value);"
							   id="txt_estimacionNueva"/>
				</div>
				<div class="btn_back w50 elegible elegibleOBC elegiblePDA" style="display: inline; float: right; margin-top:26px;" id="b_copiarEstimacion" >
					<a href="javascript: void(0);" onclick="copiarValorEstimacionActual();">
						<s:text name="&lt&lt" /> 
					</a>
				</div>
			</td>
			<td width="10%">
				<div class="elegible elegibleOBC elegiblePDA">
					<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"	
				labelposition="top" 
						 size="12"	
					readOnly="true"				
						   id="txt_estimacionActual"/>
				</div>
			</td>
			<td width="10%">
				<div class="elegible elegibleOBC elegiblePDA">
					<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
						 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"
				labelposition="top" 
						 size="12"	
					readOnly="true"				
						   id="txt_importePagado"/>
				</div>
			</td>
			<td width="60%">
				<div class="elegible elegibleOBC elegiblePDA">
					<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"	
				labelposition="top" 
						 size="12"	
					readOnly="true"				
						   id="txt_reserva"/>
				</div>
			</td>
		</tr>
		<s:if test="estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.claveTipoDeducible > 0 && ( estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.valorDeducible > 0 || estimacionCoberturaSiniestro.estimacionBienes.coberturaReporteCabina.porcentajeDeducible > 0  )">
			<tr>	   
					<td>
						<div class="elegible elegibleOBC elegiblePDA">
								<s:select id="s_aplicaDeducible"
									key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
									labelposition="top" name="apDeducible" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									list="aplicaDeducible" listKey="key" listValue="value"
									onchange="onChangeApDeducible('RCB');"
									cssClass="txtfield requerido  requeridoOBC setNew requeridoPDA" />
							</div>
					</td>	
					<td>
						<div class="montoDeducible">
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict formatCurrency" 
											 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.cantidad.deducible"
											 name="estimacionCoberturaSiniestro.montoDeducible"
									labelposition="top" 
											 maxlength="20"
											 size="10"			
											   id="montoDeducible"/>
						</div>
					</td>
					<td> 
						<div class="ctgMotivoNoaplicaDeducible">  
							<s:select id="ctgMotivoNoaplicaDeducible" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.motivo.no.deducible"
								     labelposition="top" 
									 name="estimacionCoberturaSiniestro.motivoNoAplicaDeducible"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="ctgNoAplicaDeducible" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield setNew" /> 
						</div>
					</td>				
			</tr>
		</s:if>
	</tbody>
</table>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.title"/>	
</div>	
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td style="width: 25%; text-align: left;" colspan="2">
				<s:textfield cssClass="txtfield requerido  requeridoOBC requeridoPDA requeridoSRE setNew jQalphabeticExt jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
					 name="estimacionCoberturaSiniestro.estimacionBienes.nombreAfectado"
			labelposition="top" 
					 size="60"	
					 maxlength="100"
					   id="txt_nombreAfectado"/>
			</td>
			<td style="width: 10%; text-align: left;">
				<s:select id="s_danio" 
				         name="estimacionCoberturaSiniestro.estimacionBienes.dano"
						 headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="danos" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield setNew"
				  		 cssStyle = "display: none;" /> 
			</td>
			<td style="width: 65%; text-align: left;">
				<s:textfield cssClass="txtfield requerido  requeridoOBC requeridoPDA requeridoSRE setNew jQalphabeticExt jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.personaContacto"
					  name="estimacionCoberturaSiniestro.estimacionBienes.nombrePersonaContacto"
			labelposition="top" 
					 size="60"	
					 maxlength="100"
					   id="txt_personaContacto"/>
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<tbody>
						<tr>
							<td style="width: 15%; text-align: left;">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
									 name="estimacionCoberturaSiniestro.estimacionBienes.ladaTelContacto"
							labelposition="top" 
									 size="3"	
									 maxlength="3"	
									   id="txt_lada1"/>
							</td>
							<td style="width: 85%; text-align: left;">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
									 name="estimacionCoberturaSiniestro.estimacionBienes.telContacto"
							labelposition="top" 
									 size="12"	
									 maxlength="8"	
									   id="txt_tel1"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td>
				<table>
					<tbody>
						<tr>
							<td style="width: 15%; text-align: left;">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict"  
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
									 name="estimacionCoberturaSiniestro.estimacionBienes.ladaTelContactoDos"
							labelposition="top" 
									 size="3"	
									 maxlength="3"	
									   id="txt_lada2"/>
							</td>
							<td style="width: 85%; text-align: left;">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
									 name="estimacionCoberturaSiniestro.estimacionBienes.telContactoDos"
							labelposition="top" 
									 size="12"	
									 maxlength="8"	
									   id="txt_tel2"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td >
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailinteresado"
					 name="estimacionCoberturaSiniestro.estimacionBienes.email"
			labelposition="top" 
					 size="30"	
					   id="txt_email"
					   maxlength="50"
					   onblur="onBlurEmailInteresado();"
					   onchange="validationEmail(jQuery('#txt_email').val())"/>
			</td>
			<td >
			<div style="width:35%;float:left;margin-top: 22px;">
				<s:checkbox id="emailNoProporcionado" name="estimacionCoberturaSiniestro.estimacionBienes.emailNoProporcionado" cssClass="setNew" 
				key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionado"
				labelposition="right" onchange="onChangeEmailNoProporcionado();" ></s:checkbox>
			</div>
			<div style="width:55%;float:left;margin-top:4px;">
				<s:select id="emailNoProporcionadoMotivo" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionadomotivo"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionBienes.emailNoProporcionadoMotivo"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="motivosCorreoNoPropocionado" 
					  		 cssClass="txtfield setNew" />
			</div>
			</td>
		</tr>
	</tbody>
</table>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.title"/>	
</div>	
<table id="agregar" border="0">
		<tr>
			<td width="88%">
				<table>
					<tbody>
						<tr>
							<td width="37%">
								<s:select id="s_tiposBien" 
									         key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.tipoBien"
										     labelposition="left" 
											 name="estimacionCoberturaSiniestro.estimacionBienes.tipoBien"
											 headerKey="" 
											 headerValue="%{getText('midas.general.seleccione')}"
									  		 list="tiposBien" 
									  		 listKey="key" listValue="value"  
									  		 cssClass="txtfield requerido  requeridoOBC requeridoPDA  requeridoSRE setNew" /> 
							</td>
							<td width="56%" align="right">
								<div class="elegible elegibleOBC">
									<s:textfield cssClass="txtfield requerido requeridoOBC deshabilitado" 
										  key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.responsableReparacion"
										 value="%{estimacionCoberturaSiniestro.estimacionBienes.responsableReparacion.personaMidas.nombre}"
										 size="50"	
										 labelposition="left"
										   id="txt_responsableReparacion"
										   readonly="true"/>
									<s:hidden name="idResponsableReparacion" id="h_idResponsableReparacion"></s:hidden>
								</div>
								
							</td>
							<td width="9%">
								<div class="btn_back w50 elegible elegibleOBC" style="display: inline; float: left;"  id="b_buscarResponsableReparacion">
									<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('ING',jQuery('#h_idResponsableReparacion'),jQuery('#txt_responsableReparacion'));">
										<s:text name="midas.boton.buscar" /> 
									</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="12%">
				<div class="btn_back w70" style="display: inline; float: left;" id="b_imagenes" >
					<a href="javascript: void(0);" onclick="ventanaFortimax('PRCB', jQuery('#txt_folio').val()  , 'FORTIMAX',  jQuery('#h_reporteCabinaId').val() ,jQuery('#txt_numeroReporte').val() );">
						<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> 
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<td id="td_pais">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccionSiniestroMidas" ignoreContextParams="true" executeResult="true" >
                       <s:param name="idPaisName">estimacionCoberturaSiniestro.estimacionBienes.pais.id</s:param>
	                  <s:param name="idEstadoName">estimacionCoberturaSiniestro.estimacionBienes.estado.id</s:param>      
	                  <s:param name="idCiudadName">estimacionCoberturaSiniestro.estimacionBienes.municipio.id</s:param>        
	                  <s:param name="labelPais">País</s:param>
	                  <s:param name="labelEstado">Estado</s:param>
	                  <s:param name="labelCiudad">Municipio</s:param>
	                  <s:param name="labelPosicion">left</s:param>
	                  <s:param name="componente">5</s:param>
	                  <s:param name="readOnly" value="%{#readOnly}"></s:param>
	                  <s:param name="requerido" value="0"></s:param>
	                  <s:param name="incluirReferencia" value="false"></s:param>
	                  <s:param name="enableSearchButton" value="false"></s:param>
       		   </s:action>
       		   <s:hidden name="estimacionCoberturaSiniestro.estimacionBienes.pais.id"></s:hidden>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:textarea name="estimacionCoberturaSiniestro.estimacionBienes.ubicacion" 
						id="ta_ubicacion" labelposition="top"
						key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.ubicacion"
						cols="150"
						rows="5"
						cssClass="textarea setNew" />
			</td>	
		</tr>
	</tbody>
</table>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.observacion"/>	
</div>	
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td>
				<s:textarea name="estimacionCoberturaSiniestro.estimacionBienes.observacion" 
						id="ta_observacion" labelposition="top"
						key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.observacion" 
						cols="150"
						rows="5"
						maxlength="500"
						cssClass="textarea setNew" />
			</td>	
		</tr>
		<tr>
			<td>
				<s:textarea name="estimacionCoberturaSiniestro.estimacionBienes.danosPreexistentes" 
						id="ta_danosPreexistentes" labelposition="top"
						key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.danosPreexistentes" 
						cols="150"
						rows="5"
						maxlength="500"
						cssClass="textarea setNew"/>
			</td>	
		</tr>
	</tbody>
</table>
</div>
</s:form>

<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarCoberturaGral();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>	
			<s:if test="requiereAutorizacion == 0">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
						<a href="javascript: void(0);" onclick="javascript: guardarEstimacionGral();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
						</a>
					</div>	
			</s:if>
		    <s:else>
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_autorizar" >
						<a href="javascript: void(0);" onclick="javascript: autorizarSolicitudDeReserva();"> 
							<img border='0px' alt='Autorizar' title='Autorizar'/>
						</a>
					</div>
		    </s:else>
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_nuevo">
				<a href="javascript: void(0);" onclick="javascript: iniciarEstimacion();"> 
					<s:text name="midas.boton.nuevo" />
					<img border='0px' alt='Nuevo' title='Nuevo' src='/MidasWeb/img/b_mas_agregar.gif'/> 
				</a>
			</div>	
		</td>							
	</tr>
</table>				
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
	onBlurRequeridos();
	onChangeEmailNoProporcionado();
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	   var tab = document.getElementById("td_pais").getElementsByTagName("TABLE")[0];
       var firstTR     = tab.getElementsByTagName("tbody")[0].getElementsByTagName("tr")[0];
       var secondTD    = firstTR.getElementsByTagName("td")[1];
	   var secondDIV   = secondTD.getElementsByTagName("div")[1];
	   var firstSelect = secondDIV.getElementsByTagName("select")[0]; 
	   firstSelect.disabled=true;
	   onChangeEstatus();
	   aplicaSeleccionTipoPaseDeAtencion(jQuery('#s_tipoPaseAtencion'));
	   
	   initDeducible();
	   
});
</script>