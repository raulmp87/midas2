<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
        
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 

 

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.dhx_pbox_light{
	-moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none; 
    border-color: -moz-use-text-color #C2D5DC #C2D5DC;
    border-image: none;
    border-right: 1px solid #C2D5DC;
    border-style: none solid solid;
    border-width: 0 1px 1px;
    font-size: 10px;
    margin-top: 3px; 
    width: 98%;
}

.dhx_pager_info_light{
	color: #055A78;
    cursor: pointer;
    font-family: tahoma;
    font-size: 12px;
    text-align: center; 
}
</style>


<script type="text/javascript">
var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
var obtenerListadoPasesPath = '<s:url action="obtenerListadoPases" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
var crearNuevoPasePath = '<s:url action="crearNuevoPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
var mostrarPasePath = '<s:url action="mostrarPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
</script>
<s:form id="paseAtencionForm" >
<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_reporteCabinaId"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="soloConsulta" id="h_soloConsulta"></s:hidden>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<br/>
					<div id="listadoPasesAtencionGrid" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
					<div id="pagingArea" >
						<div style="width: 100%; clear: both;">
							<div id="d_boxLight">
								&nbsp;
							</div>
							<div id="d_pagerInfo">
							</div>
							&nbsp;
						</div>
					</div>
					<div id="infoArea"></div>
					<br/>
				</td>
			</tr>	
			<tr>		
				<td>		
					<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
								
							</td>
							<td>
								<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
									<a id="btn_cerrar" href="javascript: void(0);" onclick="cerrarPaseAtencion();"> 
										<s:text name="midas.boton.cerrar" /> 
									</a>
								</div>
								
								<div class="btn_back w140" style="display: inline; float: right;" id="b_nuevo">
									<a href="javascript: void(0);" onclick="crearNuevoPase();"> 
										<s:text name="midas.boton.nuevo" /> 
									</a>
								</div>
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		</tbody>
	</table>
</div>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarListadoPasesAtencion();		
		var soloConsulta = jQuery("#h_soloConsulta").val();
		if(soloConsulta == 1){
			jQuery("#b_nuevo").remove();
		}
	});
</script>
