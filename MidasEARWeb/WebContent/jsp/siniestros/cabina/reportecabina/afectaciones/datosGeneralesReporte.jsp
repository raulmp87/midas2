<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:hidden name="soloConsulta" id="h_soloConsulta"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosReporte.reporteCabinaId" id="h_reporteCabinaId"></s:hidden>
<s:hidden id="h_estatusHabilitado" name="estatusHabilitado"></s:hidden> 
<s:hidden name="isListadoPases" id="h_isListadoPases"></s:hidden>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td style="width: 40%">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noSiniestro"
						 name="estimacionCoberturaSiniestro.datosReporte.numeroSiniestro"
				labelposition="top" 
						 size="20"	
					readOnly="true"				
						   id="txt_numeroSiniestro"/>
				</td>	
				<td style="width: 40%; text-align: left; ">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noReporte"
						 name="estimacionCoberturaSiniestro.datosReporte.numeroReporte"
				labelposition="top" 
						 size="20"	
					readOnly="true"				
						   id="txt_numeroReporte"/>
				</td>
				<td style="width: 15%; text-align: left;">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.poliza"
						 name="estimacionCoberturaSiniestro.datosReporte.numeroPoliza"
				labelposition="top" 
						 size="20"	
					readOnly="true"				
						   id="txt_numeroPoliza"/>
				</td>
				<td style="width: 5%; text-align: left;">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.inciso"
						 name="estimacionCoberturaSiniestro.datosReporte.numeroInciso"
				labelposition="top" 
						 size="3"	
					readOnly="true"				
						   id="txt_numeroInciso"/>
				</td>
			</tr>
		</tbody>
		
		
	</table>
</div>		