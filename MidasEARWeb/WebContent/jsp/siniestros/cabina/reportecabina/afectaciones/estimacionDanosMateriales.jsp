<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>
<script src="<s:url value='/js/siniestros/valuacion/valuacionreporte.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">

#wwlbl_txt_lada1, #wwlbl_txt_lada2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 20px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_tel1, #wwlbl_txt_tel2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 70px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_companiaSegurosTercero{
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 160px !important;
    float:left; 
}

div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.error {
	background-color: red;
	opacity: 0.4;
}

</style>
<s:form id="estimacionForm" action="guardar" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura" name="estimacionForm">
<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="siniestroDTO.idToPoliza" id="h_idPoliza"></s:hidden>
<s:hidden name="siniestroDTO.fechaOcurridoMillis" id="h_fechaReporteSiniestroMillis"></s:hidden>
<s:hidden name="siniestroDTO.incisoContinuity" id="h_incisoContinuityId"></s:hidden>
<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
<s:hidden name="idEstimacionCoberturaReporte" ></s:hidden>
<s:hidden name="siniestroDTO.terminoAjuste" id="h_terminoAjuste"></s:hidden>

	
<s:hidden name="estimacionCoberturaSiniestro.estimacionDanosMateriales.id" id="idEstimacionCoberturaReporteCabina"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.claveTipoCalculo" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionDanosMateriales.tipoEstimacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.idToCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>

<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion1" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion2" ></s:hidden>
<s:hidden name="siniestroDTO.recibidaOrdenCia" id="h_recibidaOrdenCia"></s:hidden>
<s:hidden name="siniestroDTO.ciaSeguros" id="h_ciaSeguros"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionDanosMateriales.secuenciaPaseAtencion" id="h_secuenciaPaseAtencion"></s:hidden>

<s:hidden id="h_porDeducible" name="porcentajeDeducible" ></s:hidden>
<s:hidden id="h_estimacionCobertura" name="estimacionCoberturaSiniestro.tipoCoberturaAutoInciso" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.montoDeducibleCalculado" id="montoDeducibleCalculado"></s:hidden>
<s:hidden id="h_motivoDeducible" name="motivoNoAplicaDeducibleSel" ></s:hidden>
<s:hidden id="h_requieerAutorizacion" name="requiereAutorizacion" ></s:hidden>
<s:hidden id="h_idParametroAntiguedad" name="idParametroAntiguedad" ></s:hidden>

<!-- SOLO DMA -->
<s:hidden id="h_porDeducibleValorComercial" name="porcentajeDeducibleValorComercial"></s:hidden>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.title"/>	
</div>
<br/>

<s:include value="/jsp/siniestros/cabina/reportecabina/afectaciones/datosGeneralesReporte.jsp"></s:include>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield setNew deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio"
						 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.folio"
				labelposition="top" 
						 size="20"	
					readonly="true"				
						   id="txt_folio"/>
				</td>	
				<td colspan="2">
					<s:select id="s_tipoPaseAtencion" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.tipoPaseAtencion"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.tipoPaseAtencion"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="pasesAtencion" 
					  		 listKey="key" listValue="value"
					  		 onchange="aplicaSeleccionTipoPaseDeAtencion(this);" 
					  		 cssClass="txtfield requerido setNew" /> 	
					  		 
				</td>
				<td colspan="2">
					<s:select id="s_causaMovimiento" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="causasMovimiento" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido requeridoPAT requeridoPDA setNew" /> 	
				</td>
              <td colspan="2">
					<div class = "campoSIPAC">		
						<s:select id="s_tipoTransporte"
							 key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tipoTransporte"
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.tipoTransporte"
							 labelposition="top"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
							 list="ctgTipoTransporte" 
					  		 listKey="key" listValue="value"  
							 cssClass="txtfield setNew"
						/>
					</div>		
			</td>
			
			</tr>
			<tr>
				<td  width="15%">
				<div style="float:left">
					<s:textfield 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"
				labelposition="top" 
						 size="15"
					maxlength="15"	
					 cssClass="txtfield requerido requeridoPAT requeridoPDA setNew formatCurrency"
					  onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
				      onblur ="mascaraDecimales('#txt_estimacionNueva',this.value);"	
						   id="txt_estimacionNueva"/>
			   </div>
						   
				<div class="btn_back w50" style="display: inline; float: right; margin-top:26px;" id="b_copiarEstimacion" >
					<a href="javascript: void(0);" onclick="copiarValorEstimacionActual();">
						<s:text name="&lt&lt" /> 
					</a>
				</div>
				</td>
				<td  width="10%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"
				labelposition="top" 
						 size="15"	
						 readonly="true"			
						   id="txt_estimacionActual"/>
				</td>
				<td  width="10%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
						 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"
						labelposition="top" 
						 size="15"	
						 readonly="true"				
						   id="txt_importePagado"/>
				</td>
				<td  width="10%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"
							labelposition="top" 
						 size="15"	
						 readonly="true"				
						 id="txt_reserva"/>
				</td>
				<td  width="55%">
					<s:select id="s_estatus" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estatus"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.estatus"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"
					  		 cssClass="txtfield setNew" 
					  		 onchange="onChangeEstatus();" 
				  		     disabled="true" /> 
				   <s:hidden id="h_estatus" name="idEstatus"></s:hidden>
				</td>
				<td>
						<div class = "campoSIPAC">		
							<s:textfield  
							  cssClass="txtfield setNew jQalphanumeric jQrestrict"
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.DUA"
							  name="estimacionCoberturaSiniestro.estimacionDanosMateriales.dua"
							  labelposition="top" 
							  size="25"
							  maxlength="25"	
							  width="75%"			
							  id="txt_DUA"/>	
						</div>		
				</td>
			</tr>
			
			<tr>
				<td>
					<!-- cssClass="txtfield jQrestrict jQnumeric jQrestrict formatCurrency"  -->
					<s:textfield 
						  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaAmparada"
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.sumaAseguradaObtenida"
							labelposition="top" 
						 	size="15"
							maxlength="15"	
							onblur = "calcularDeducibleValorComercial();"
						  	cssClass="txtfield requerido requeridoPAT requeridoPDA setNew formatCurrency"				
						   	id="txt_sumaAseguradaObtenida"/>
					<s:if test='estimacionCoberturaSiniestro.estimacionDanosMateriales.consultaSumaAsegurada == true'>
						<img border="0" src="../img/checked16.gif" title="Suma asegurada actualizada" style="margin-left: 85%; margin-top: -12%;"></s:if><s:else>
						<img border="0" src="../img/pixel.gif" style="margin-left: 85%; margin-top: -12%;"></s:else>
				</td>
				<td colspan="2">
					<s:textfield 
						  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaProporcionada1"
							 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion1"
							labelposition="top" 
						 	size="20"	
						  	cssClass="txtfield formatCurrency"	
						  	disabled="true"				
						   	id="txt_sumaAseguradaProporcionada1"/>
				</td>
				
				<td colspan="2">
					<s:textfield 
						  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaProporcionada2"
							 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion2"
							labelposition="top" 
						 	size="20"	
						  	cssClass="txtfield formatCurrency"	
						  	disabled="true"				
						   	id="txt_sumaAseguradaProporcionada2"/>
				</td>
				
				<td>
						<div class = "campoSIPAC">
							<s:select id="s_circunstancia"
							 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.circunstancia"
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.circunstancia"
							 labelposition="top"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
							 list="circunstancia" 
					  		 listKey="key" listValue="value" 
					  		 width="50%" 
							 cssClass="txtfield setNew" />
				             				             
				             <s:checkbox 
				             		name="estimacionCoberturaSiniestro.estimacionDanosMateriales.esOtraCircunstancia"
				             		key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.otraCircunstancia" 
				             	    labelposition="left"
				             	    id="ckb_otraCircunstancia"
				             	    onclick="validarCampoCircunstancia('DM')"
				             />
				             
				             <s:textfield  
							  cssClass= "txtfield setNew"  
							  name="estimacionCoberturaSiniestro.estimacionDanosMateriales.otraCircunstancia"
							  labelposition="top" 
							  size="25"	
							  width="90%"			
							  id="txt_otraCircunstancia_detalle"
							  />
						</div>
				
				</td>
				
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
<div id="contenedorFiltros" style="width: 95%;">
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.title"/>
</div>	
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td colspan="2">
				<s:textfield cssClass="txtfield requerido requeridoPAT setNew jQalphabeticExt jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
					 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.nombreAfectado"
			labelposition="top" 
					 size="50"
					 maxlength="100"				
					   id="txt_nombreAsegurado_"/>
			</td>
			<td>
				<div style="float:left;margin-top: 22px;" id="btnCondiciones">
					<div class="btn_back w140"  style="display:inline; float: left; ">
						<a href="javascript: void(0);"
							onclick="inicializarCondicionesEspecialesFromSiniestro(2);">
							<s:text
								name="midas.boton.condicionesEspeciales" />
						</a>
					</div>
				</div>
			</td>
			<td style="width: 50%;"></td>
		</tr>
			<s:if test="estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.claveTipoDeducible > 0 && ( estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.valorDeducible > 0 ||  estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.porcentajeDeducible > 0 ) ">
			<tr>	
				<td>
						<s:select id="s_aplicaDeducible" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
							     labelposition="top" 
								 name="apDeducible"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="aplicaDeducible" 
						  		 listKey="key" listValue="value"  
						  		 onchange="onChangeApDeducible('DMA');" 
						  		 cssClass="txtfield requerido requeridoPAT requeridoPDA setNew" /> 
					
				</td>
				<td>
					<div class="montoDeducible">
						<s:textfield cssClass="txtfield setNew jQ2float jQrestrict formatCurrency" 
										 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.cantidad.deducible"
										 name="estimacionCoberturaSiniestro.montoDeducible"
								labelposition="top" 
										 maxlength="20"
										 size="10"			
										   id="montoDeducible"/>
					</div>
				</td>
				<td>
	   				<div class="ctgMotivoNoaplicaDeducible">  
						<s:select id="ctg_aplicaDeducible" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.motivo.no.deducible"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.motivoNoAplicaDeducible"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="ctgNoAplicaDeducible" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield setNew" /> 
					</div>
				</td>
			</tr>
			</s:if>		
		<tr>
			<td>
				<table>
					<tr>
						<td style="width: 10px;">
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
								 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
								 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.ladaTelContacto"
						labelposition="top" 
								 maxlength="3"
								 size="4"			
								   id="txt_lada1"/>
						</td>
						<td>
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
								 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.telContacto"
						labelposition="top" 
								 maxlength="8"
								 size="12"				
								   id="txt_tel1"/>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
						<td style="width: 10px;">
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
								 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
								 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.ladaTelContactoDos"
								labelposition="top" 
								 maxlength="3"
								 size="4"				
								   id="txt_lada2"/>
						</td>
						<td>
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
								 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.telContactoDos"
								labelposition="top" 
								 maxlength="8"
								 size="12"			
								   id="txt_tel2"/>
						</td>
					</tr>
				</table>
			</td>
			<td >
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailinteresado"
					 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.email"
						labelposition="top"
					 maxlength="50" 
					 size="30"
					 onblur="onBlurEmailInteresado();"		
					   id="txt_email"/>
			</td>
			<td colspan="2">
			<div style="width:35%;float:left;margin-top: 22px;">
				<s:checkbox id="emailNoProporcionado" name="estimacionCoberturaSiniestro.estimacionDanosMateriales.emailNoProporcionado" cssClass="setNew" 
				key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionado"
				labelposition="right" onchange="onChangeEmailNoProporcionado();" ></s:checkbox>
			</div>
			<div style="width:65%;float:right;">
				<s:select id="emailNoProporcionadoMotivo" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionadomotivo"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.emailNoProporcionadoMotivo"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="motivosCorreoNoPropocionado" 
					  		 cssClass="txtfield setNew" />
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<s:hidden name="idCompaniaSegurosTercero" id="txt_idcompaniaSegurosTercero"></s:hidden>
			<s:if test="estimacionCoberturaSiniestro.recibidaOrdenCia == 1">
				<s:textfield cssClass="txtfield setNew deshabilitado"
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
							  value="%{estimacionCoberturaSiniestro.nombreCompania}"
					labelposition="top" 
							 size="40"	
							 readonly="true"
							   id="txt_companiaSegurosTercero"/>
			</s:if>
		
			<s:else>
				<s:textfield cssClass="txtfield setNew deshabilitado" 
								  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
								 value="%{estimacionCoberturaSiniestro.estimacionDanosMateriales.companiaSegurosTercero.personaMidas.nombre}"
						labelposition="top" 
								 size="40"		
								 readonly="true"
								   id="txt_companiaSegurosTercero"  
								name="estimacionCoberturaSiniestro.estimacionDanosMateriales.companiaSegurosTercero.personaMidas.nombre"/>
			</s:else>	
			</td>
			<td id="td_folioCiaSeguros">
				<s:textfield cssClass="txtfield jQrestrict setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.folioCiaSeguros"
					 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.folioCompaniaSeguros"
			labelposition="top" 
					 size="12"
					 maxlength="20"		
					   id="txt_folioCiaSeguros"/>
			</td>
			<td id="td_numeroSiniestro">
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.noSiniestroTercero"
					 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.numeroSiniestroTercero"
			labelposition="top" 
					 size="18"
					 maxlength="18"		
					 readonly="true"	
					   id="txt_noSiniestroTercero"/>
			</td>
			<td>
				<div class="btn_back w70" style="display: inline; float: left;" id="b_imagenes" >
					<a href="javascript: void(0);" onclick="ventanaFortimax('PDM','' , 'FORTIMAX',  jQuery('#h_reporteCabinaId').val() ,jQuery('#txt_numeroReporte').val() );">
						<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> 
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>
</div>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td width="20%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.marca"
							 name="estimacionCoberturaSiniestro.marca"
							labelposition="top" 
							size="30"	
							readOnly="true"				
						   id="txt_marca"/>
				</td>	
				<td width="20%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estilo"
						 name="estimacionCoberturaSiniestro.estilo"
						labelposition="top" 
						 size="60"	
							readOnly="true"			
						   id="txt_estilo"/>
				</td>
				<td width="10%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.modelo"
						 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.modeloVehiculo"
						labelposition="top" 
						 size="12"	
							readOnly="true"				
						   id="txt_modelo"/>
				</td>
				<td width="10%">
					<s:textfield cssClass="txtfield jQrestrict setNew" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.placas"
						 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.placas"
						labelposition="top" 
							 size="12"					
						   id="txt_placas"/>
				</td>
				<td width="40%">
					<s:textfield cssClass="txtfield jQrestrict deshabilitado" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noMotor"
						 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroMotor"
						 labelposition="top" 
						 size="30"	
						 readOnly="true"			
						 id="txt_noMotor"/>
				
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield 
							cssClass="txtfield jQrestrict deshabilitado" 
							key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noSerie"
							name="estimacionCoberturaSiniestro.estimacionDanosMateriales.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie"
							labelposition="top" 
							size="30"	
							readOnly="true"			
							id="txt_noSerie"
					/>

					</td>
					<td>
					<div 	class="btn_back w50" 
							style="display: inline; float: left;"  
							id="btnBuscarCia">
							
						<a href="javascript: void(0);" 
						onclick="javascript:validaNoSerieCESVI();"
						>
							<s:text name="midas.boton.validar" />
						</a>
					</div>
					
					</td>
					
				
				<td colspan="4">
			
					<div id="mensajeNoSerieCESVI" style="font-weight:bold; font-size:11px; color:blue"> 
						<label for="respuestaCESVI" style="vertical-align: middle"></label>
					</div>
			
				</td>
			</tr>
			<tr>		
				<td>
					<div class="elegible elegiblePAT elegiblePDA">
						<s:select id="s_estado" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estado"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.estimacionDanosMateriales.estado.id"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="estados" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield setNew requeridoPAT requeridoPDA"/> 
					</div>
				</td>
				<td  colspan="2">
					<div class="elegible elegiblePAT elegiblePDA">
						<s:textfield cssClass="txtfield requeridoPAT requeridoPDA setNew deshabilitado" 
							    key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tallerAsignado"
							    value="%{estimacionCoberturaSiniestro.estimacionDanosMateriales.taller.personaMidas.nombre}"
								labelposition="top" 
							    size="40"	
						        readonly="true"			
							    id="txt_tallerAsignado"/>
						<s:hidden name="idTaller" value="%{estimacionCoberturaSiniestro.estimacionDanosMateriales.taller.id}" id="txt_idTaller"></s:hidden>
					</div>
				</td>
				<td>
					<div class="elegible elegiblePAT elegiblePDA">
						<div class="btn_back w50" style="display: inline; float: left;" id="btnBuscarTaller" >
							<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicioByEstado('TALL',jQuery('#txt_idTaller'),jQuery('#txt_tallerAsignado'),jQuery('#s_estado').val());">
								<s:text name="midas.boton.buscar" /> 
							</a>
						</div>
					</div>
				</td>
				<td colspan="2">	
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.observacion"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textarea name="estimacionCoberturaSiniestro.estimacionDanosMateriales.danosCubiertos" 
							id="ta_danosCubiertos" labelposition="top"
							key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.danosCubiertos" 
							cols="150"
							rows="5"
							maxlength="500"
							cssClass="textarea setNew" />
				</td>	
			</tr>
			<tr>
				<td>
					
						<s:checkbox 
									key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.SIONODanosPreExistentes"
				             		name="estimacionCoberturaSiniestro.estimacionDanosMateriales.tieneDanosPreexistentes"
				             		id="ckb_danosPrexistentes"
				             	    labelposition="left"
				             	    onclick="validarDanosPrexistentes();"
				        />
				     
				</td>
			</tr>
			<tr>
				<td>
					<div id = "area_danosPrexistentes">
						<s:textarea name="estimacionCoberturaSiniestro.estimacionDanosMateriales.danosPreexistentes" 
								id="ta_danosPreexistentes" labelposition="top"
								key="midas.siniestros.cabina.reportecabina.ordenPago.danosMateriales.danosPreexistentes" 
								cols="150"
								rows="5"
								maxlength="500"
								cssClass="textarea setNew" />
					</div>
				</td>	
			</tr>			
		</tbody>
	</table>
</div>
</s:form>
<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
	<tr>
		<td>
		
			
			
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarDM();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>	
			<s:if test="requiereAutorizacion == 0">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
						<a href="javascript: void(0);" onclick="javascript: guardarEstimacionGral();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
						</a>
					</div>	
			</s:if>
		    <s:else>
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_autorizar" >
					<a href="javascript: void(0);" onclick="javascript: autorizarSolicitudDeReserva();"> 
						<img border='0px' alt='Autorizar' title='Autorizar'/>
					</a>
				</div>
		    </s:else>
			    <s:if test="idEstimacionCoberturaReporte != null">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_valuacion" >
						<a href="javascript: void(0);" onclick="javascript:mostrarModalValuacionHgs();"> 
							<s:text name="midas.boton.valuacion" />
							<img border='0px' alt='Valuacion' title='Valuacion' src='/MidasWeb/img/b_mas_agregar.gif'/> 
						</a>
					</div>	
				</s:if>
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_indemnizar">
				<a id="btn_Indemnizar" href="javascript: void(0);" onclick="javascript: ventanaModalCompra(jQuery('#idEstimacionCoberturaReporteCabina').val());"> 
					<s:text name="Indemnizar" /> 
				</a>
			</div>
		</td>							
	</tr>
</table>				
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
	onBlurRequeridos();
	onChangeApDeducible();
	onChangeEstatus();
	onChangeEmailNoProporcionado();
	recibidaOrdenCia();
	habilitaEstatusDMA();	
	aplicaSeleccionTipoPaseDeAtencion(jQuery('#s_tipoPaseAtencion'));	
	initDeducible();
	initCurrencyFormatOnTxtInput();
	verificarTerminosAjuste();
	validarDanosPrexistentes();
});
</script>