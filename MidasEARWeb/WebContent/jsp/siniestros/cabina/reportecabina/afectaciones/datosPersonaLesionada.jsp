<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:select id="s_estadoPersona" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.estado"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.estado"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="tiposEstado" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 	
				</td>	
				<td>
					<s:select id="s_tipoPaseAtencion" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.tipoAtencion"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.tipoAtencion"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="tiposAtencion" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 	
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.hospital"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.hospital"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_hospital"/>
				</td>
				<td>
					<div class="btn_back w40" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(8,jQuery('#txt_hospital'));">
							<s:text name="midas.boton.buscar" /> 
						</a>
					</div>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.otro"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.otroHospital"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_otroHospital"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.medico"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.medico"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_medico"/>
				</td>
				<td>
					<div class="btn_back w40" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(9,jQuery('#txt_medico'));">
							<s:text name="midas.boton.buscar" /> 
						</a>
					</div>
				</td>
				<td colspan="3">
					<div class="btn_back w60" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick=";">
							<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> 
						</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>		