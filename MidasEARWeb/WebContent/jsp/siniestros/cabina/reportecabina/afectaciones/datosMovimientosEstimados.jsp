<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.folio"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_folio"/>
				</td>	
				<td>
					<s:select id="s_tipoPaseAtencion" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.tipoPaseAtencion"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.tipoPaseAtencion"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="pasesAtencion" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 	
				</td>
				<td>
					<s:select id="s_causaMovimiento" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="causasMovimiento" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 	
				</td>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaAmparada"
						 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_sumaAseguradaAmparada"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_estimacionActual"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
						 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_importePagado"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_reserva"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_estimacionNueva"/>
				</td>
				<td>
					<s:select id="s_estatus" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estatus"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estatus"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 
				</td>
			</tr>
		</tbody>
	</table>
</div>		