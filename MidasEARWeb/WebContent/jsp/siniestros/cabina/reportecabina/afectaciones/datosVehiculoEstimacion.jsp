<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
<s:if test="tipoEstimacion == \"DMA\"">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.marca"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.marcaId"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_marca"/>
				</td>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estilo"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.estiloId"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_estilo"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.modelo"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.modeloVehiculo"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_modelo"/>
				</td>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.placas"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.placa"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_placas"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noSerie"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_noSerie"/>
				</td>
				<td>
					<s:select id="s_estado" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estado"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.estado"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tallerAsignado"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.taller"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_tallerAsignado"/>
				</td>
				<td>
					<div class="btn_back w50" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(5,jQuery('#txt_tallerAsignado'));">
							<s:text name="midas.boton.buscar" /> 
						</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</s:if>
<s:if test="tipoEstimacion == \"RCV\"">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.marca"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.marca"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_marca"/>
				</td>	
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tipoVehiculo"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.estiloVehiculo"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_estilo"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.modelo"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.modeloVehiculo"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_modelo"/>
				</td>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.placas"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.placas"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_placas"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.noSerie"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.numeroSerie"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_noSerie"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.color"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.color"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_color"/>
				</td>
				<td colspan="2">
					<div class="btn_back w80" style="display: inline; float: left;" >
						<a href="javascript: void(0);" onclick=";"> 
							<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.imagenes" /> 
						</a>
					</div>	 
				</td>
			</tr>
			<tr>
				<td>
					<s:select id="s_estado" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.estado"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.estado"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="estatus" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tallerAsignado"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.taller"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_tallerAsignado"/>
				</td>
				<td colspan="2">
					<div class="btn_back w50" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(5,jQuery('#txt_tallerAsignado'));">
							<s:text name="midas.boton.buscar" /> 
						</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</s:if>
</div>		