<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>



<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>
<%-- <script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>" charset="ISO-8859-1"></script>
 --%>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

</style>
<script type="text/javascript">
var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
 </script>
<s:form id="estimacionForm" action="guardar" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura" name="estimacionForm">


<s:hidden name="idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" ></s:hidden>
<s:hidden name="tipoEstimacion" ></s:hidden>
<s:hidden name="soloConsulta" id="h_soloConsulta"></s:hidden>
<s:hidden name="idEstimacionCoberturaReporte" ></s:hidden>

<s:hidden name="estimacionCoberturaSiniestro.estimacionGenerica.id" id="idEstimacionCoberturaReporteCabina" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGenerica.coberturaReporteCabina.claveTipoCalculo" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGenerica.tipoEstimacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.idToCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada" id="claveFuenteSumaAsegurada"></s:hidden>


<s:hidden name="estimacionCoberturaSiniestro.moneda" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.nombreCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGenerica.fechaCreacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.importePagado" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.reserva" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion1" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion2" ></s:hidden>

<s:hidden id="h_porDeducible" name="porcentajeDeducible" ></s:hidden>
<s:hidden id="h_estimacionCobertura" name="estimacionCoberturaSiniestro.tipoCoberturaAutoInciso" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.montoDeducibleCalculado" id="montoDeducibleCalculado"></s:hidden>
<s:hidden id="h_motivoDeducible" name="motivoNoAplicaDeducibleSel" ></s:hidden>
<s:hidden id="h_requieerAutorizacion" name="requiereAutorizacion" ></s:hidden>
<s:hidden id="h_idParametroAntiguedad" name="idParametroAntiguedad" ></s:hidden>

<!-- SOLO DMA -->
<s:hidden id="h_porDeducibleValorComercial" name="porcentajeDeducibleValorComercial"></s:hidden>


<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.cobertura"/>	<s:text name="estimacionCoberturaSiniestro.nombreCobertura"/>	
	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield cssClass="txtfield " 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.moneda"
						 name="estimacionCoberturaSiniestro.moneda"
				labelposition="left" 
						 size="30"	
					disabled="true"				
						   id="txt_moneda"/>
				</td>	
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield " 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.cobertura"
						 name="estimacionCoberturaSiniestro.nombreCobertura"
				labelposition="left" 
						 size="50"	
					disabled="true"				
						   id="txt_cobertura"/>
				</td>	
			</tr>
			<tr>
				<td>
				<s:textfield cssClass="txtfield " 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.fecha"
						 name="estimacionCoberturaSiniestro.estimacionGenerica.fechaCreacion"
				labelposition="left" 
						 size="12"	
					disabled="true"				
						   id="txt_fechaCreacion"/>
						   
						   		
				</td>	
			</tr>
			
		<s:if test="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada == 1 || estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada == 2 || estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada == 9 ">                   
                        <tr>
							<td>
								<s:textfield cssClass="txtfield  requerido setNew formatCurrency requeridoParaAutorizar" 
									  	key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.sumaAseguradaAmparada"
										 name="estimacionCoberturaSiniestro.estimacionGenerica.sumaAseguradaObtenida"
										labelposition="left" 
									 	size="15"
							       maxlength="15"							
									   	id="txt_sumaAseguradaObtenida"
									   	onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
					  					onblur ="mascaraDecimales('#txt_estimacionNueva',this.value); calcularDeducibleValorComercial();"
							    onkeypress="return soloNumeros(this, event, true)"  />
							</td>	
						</tr>    
						
					    <tr>			
							<td>
								<s:textfield cssClass="txtfield " 
									  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaProporcionada1"
										 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion1"
										labelposition="left" 
									 	size="12"	
									  	cssClass="txtfield formatCurrency"	
									  	disabled="true"				
									   	id="txt_sumaAseguradaProporcionada1"/>
							</td>
						</tr>
						
						<tr>
							<td>
								<s:textfield cssClass="txtfield " 
									  	key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.sumaAseguradaProporcionada2"
										 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaProporcionadaOpcion2"
										labelposition="left" 
									 	size="12"	
									  	cssClass="txtfield formatCurrency"	
									  	disabled="true"				
									   	id="txt_sumaAseguradaProporcionada2"/>
							</td>
						</tr>	       
             </s:if>
             			
			<s:if test="estimacionCoberturaSiniestro.estimacionGenerica.coberturaReporteCabina.claveTipoDeducible > 0 && (estimacionCoberturaSiniestro.estimacionGenerica.coberturaReporteCabina.valorDeducible > 0 || estimacionCoberturaSiniestro.estimacionGenerica.coberturaReporteCabina.porcentajeDeducible > 0 )" >			   
				<tr>
					<td>
						<s:select id="s_aplicaDeducible" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
						     labelposition="top" 
							 name="apDeducible"
							 headerKey=""
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="aplicaDeducible" 
					  		 onchange="onChangeApDeducible('RCB');"
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido setNew" /> 
					</td>	
				</tr>
				<tr>
					<td>
						<div class="montoDeducible">
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict formatCurrency" 
											 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.cantidad.deducible"
											 name="estimacionCoberturaSiniestro.montoDeducible"
									labelposition="top" 
											 maxlength="20"
											 size="10"			
											   id="montoDeducible"/>
						</div>
					</td>
				</tr>
				<tr>
					<td> 
						<div class="ctgMotivoNoaplicaDeducible">  
							<s:select id="ctgMotivoNoaplicaDeducible" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.motivo.no.deducible"
								     labelposition="top" 
									 name="estimacionCoberturaSiniestro.motivoNoAplicaDeducible"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="ctgNoAplicaDeducible" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield setNew" /> 
						</div>
					</td>				
				</tr>
			</s:if>
			<tr>
				<td>
					<s:textfield cssClass="txtfield formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.estimacionActual"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"
				labelposition="left" 
						 size="12"	
					disabled="true"				
						   id="txt_estimacionActual"/>
				</td>	
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.importePagado"
						 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"
				labelposition="left" 
						 size="12"	
					disabled="true"				
						   id="txt_importePagado"/>
				</td>	
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.reserva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"
				labelposition="left" 
						 size="12"	
					disabled="true"				
						   id="txt_reserva"/>
				</td>	
			</tr>
			
			<!--  <tr>
				<td>
					<s:textfield cssClass="txtfield " 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.sumaAseguradaAmparada"
						 name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada"
				labelposition="left" 
						 size="12"		
						 disabled="true"				
						   id="txt_sumaAseguradaAmparada"/>
				</td>	
			</tr>-->
			
			
			<tr>
				<td>
				<div style="float:left;">
					<s:textfield cssClass="txtfield requerido setNew jQ2float jQrestrict formatCurrency" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.estimacionNueva"
						 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"
				labelposition="left" 
						 size="10"
					maxlength="10"		
						   id="txt_estimacionNueva"
					  onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
					  onblur ="mascaraDecimales('#txt_estimacionNueva',this.value);"
			       onkeypress="return soloNumeros(this, event, true)"  />
			       </div>
			       <div class="btn_back w50 " style="display: inline; float: left; margin-top:4px;" id="b_copiarEstimacion" >
					<a href="javascript: void(0);" onclick="copiarValorEstimacionActual();">
						<s:text name="&lt&lt" /> 
					</a>
				</div>
				</td>	
			</tr>
			<tr>
				<td>
					<s:select id="s_causaMovimiento" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.causaMovimiento"
						     labelposition="left" 
							 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="causasMovimiento" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido setNew requeridoParaAutorizar"
					  		 /> 	
				</td>	
			</tr>
			
			<tr>
                        <td>
                              <s:textfield cssClass="txtfield setNew" 
                                      key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailinteresado"
                                    name="estimacionCoberturaSiniestro.estimacionGenerica.correo"
                        			labelposition="left" 
                                     size="50"        
                                    maxlength="50"         
                                    onblur="onBlurEmailInteresado();"        
                                       id="txt_correo"
                             		onchange="validationEmail(jQuery('#txt_correo').val())"/>
                        </td> 
                  </tr>
                  <tr>
                        <td>
                              <div style="width:35%;float:left;margin-top: 15px;">
                                    <s:checkbox id="emailNoProporcionado" name="estimacionCoberturaSiniestro.estimacionGenerica.emailNoProporcionado" cssClass="" 
                                    key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionado"
                                    labelposition="right" onchange="onChangeEmailNoProporcionado();" ></s:checkbox>
                              </div>
                              <div style="width:65%;float:right;">
                                    <s:select id="emailNoProporcionadoMotivo" 
                                                   key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionadomotivo"
                                                     labelposition="top" 
                                                       name="estimacionCoberturaSiniestro.estimacionGenerica.emailNoProporcionadoMotivo"
                                                      headerKey="" 
                                                       headerValue="%{getText('midas.general.seleccione')}"
                                                       list="motivosCorreoNoPropocionado" 
                                                       cssClass="txtfield setNew" />
                              </div>
                        </td>
                  </tr>
		</tbody>
	</table>
</div>
</s:form>
<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarEstimacionDirecta();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>	
			
			<s:if test="requiereAutorizacion == 0">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
						<a href="javascript: void(0);" onclick="javascript: guardarEstimacion();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar' title='Regresar' src='/MidasWeb/img/btn_guardar.jpg'/>
						</a>
					</div>	
			</s:if>
		    <s:else>
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_autorizar" >
						<a href="javascript: void(0);" onclick="javascript: autorizarSolicitudDeReservaDesdeModal();"> 
							<img border='0px' alt='Autorizar' title='Autorizar'/>
						</a>
					</div>
		    </s:else>
		</td>							
	</tr>
</table>				
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script>
jQuery(document).ready(function(){
	var soloConsulta = jQuery("#h_soloConsulta").val();
	if(soloConsulta == 1){
            consultaEstimacionDirecta();
      }else{
            onChangeEmailNoProporcionado();
      }

	initCurrencyFormatOnTxtInput();
	
	var win = parent.mainDhxWindow.window("vm_estimacion");
	win.button("close").hide();
	
	initDeducible();
});
</script>