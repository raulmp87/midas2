<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectaciones/estimacionCobertura.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

label[for="s_tieneCiaSeguros"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 20px !important;
}

label[for="txt_noSiniestroTercero"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 20px !important;
}

#wwlbl_txt_lada1, #wwlbl_txt_lada2 {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 20px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_tel1, #wwlbl_txt_tel2, #wwlbl_txt_hospital, #wwlbl_txt_medico {
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 70px !important;
    position: relative;
    float: left;
}

#wwlbl_txt_companiaSegurosTercero{
    color:black;
    font-weight: normal;
    font-size:7pt;
    width: 160px !important;
    float:left; 
}

.oculto{
	display: none;
}

.error {
	background-color: red;
	opacity: 0.4;
}

</style>
<script type="text/javascript">
var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
var crearNuevoPasePath = '<s:url action="crearNuevoPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
</script>
<s:form id="estimacionForm" >

<s:hidden name="idCoberturaReporteCabina" id="h_idCoberturaReporteCabina"></s:hidden>
<s:hidden name="reporteCabinaId" id="h_idReporteCabina"></s:hidden>
<s:hidden name="tipoCalculo" id="h_tipoCalculo"></s:hidden>
<s:hidden name="tipoEstimacion" id="h_tipoEstimacion"></s:hidden>
<s:hidden name="idEstimacionCoberturaReporte" ></s:hidden>

<s:hidden name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.id" cssClass="setNew" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.coberturaReporteCabina.claveTipoCalculo" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.tipoEstimacion" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.idToCobertura" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.cobertura.claveFuenteSumaAsegurada" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.datosEstimacion.sumaAseguradaAmparada" ></s:hidden>
<s:hidden id="h_consultaPase"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.secuenciaPaseAtencion" id="h_secuenciaPaseAtencion"></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.secuenciaPaseDeCobertura" id="h_secuenciaPaseDeCobertura"></s:hidden>

<s:hidden id="h_porDeducible" name="porcentajeDeducible"></s:hidden>
<s:hidden id="h_estimacionCobertura" name="estimacionCoberturaSiniestro.tipoCoberturaAutoInciso" ></s:hidden>
<s:hidden name="estimacionCoberturaSiniestro.montoDeducibleCalculado" id="montoDeducibleCalculado"></s:hidden>
<s:hidden id="h_requieerAutorizacion" name="requiereAutorizacion" ></s:hidden>
<s:hidden id="h_idParametroAntiguedad" name="idParametroAntiguedad" ></s:hidden>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionGastosMedicosConductor.title"/>	
</div>
<br><br>
<s:include value="/jsp/siniestros/cabina/reportecabina/afectaciones/datosGeneralesReporte.jsp"></s:include>

<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.title"/>	
</div>
<div id="contenedorFiltros" style="width: 95%;">
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td>
				<s:textfield cssClass="txtfield setNew deshabilitado" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio"
					 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.folio"
			labelposition="top" 
					 size="20"	
				readOnly="true"				
					   id="txt_folio"/>
			</td>	
			<td id="td_tipoPaseAtencion" colspan="2">
				<s:select id="s_tipoPaseAtencion" 
				         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.tipoPaseAtencion"
					     labelposition="top" 
						 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.tipoPaseAtencion"
						 headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="pasesAtencion" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield requerido requeridoGMT requeridoRAC setNew"
				  		 onchange="onChangeTipoPaseAtencionGastosMedicosConductor();"
				  		  /> 	
			</td>
			<td colspan="2">
				<s:select id="s_causaMovimiento" 
				         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.causaMovimiento"
					     labelposition="top" 
						 name="estimacionCoberturaSiniestro.datosEstimacion.causaMovimiento"
						 headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="causasMovimiento" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield requerido requeridoGMT requeridoRAC setNew"
				  		  /> 	
			</td>

		</tr>
		<tr>
			<td width="17%">
			<div style="float:left;">
				<s:textfield cssClass="txtfield  requerido requeridoGMT requeridoRAC  setNew jQ2float jQrestrict formatCurrency" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionNueva"
					 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionNueva"	
			labelposition="top" 
					 size="10"
			    maxlength="10"		
				  onkeyup="mascaraDecimales('#txt_estimacionNueva',this.value);"	
				  onblur ="mascaraDecimales('#txt_estimacionNueva',this.value);"
					   id="txt_estimacionNueva"/>
			</div>
				<div class="btn_back w50" style="display: inline; float: right; margin-top:26px;" id="b_copiarEstimacion" >
					<a href="javascript: void(0);" onclick="copiarValorEstimacionActual();">
						<s:text name="&lt&lt" /> 
					</a>
				</div>
			</td>
			<td width="10%">
				<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estimacionActual"
					 name="estimacionCoberturaSiniestro.datosEstimacion.estimacionActual"		
			labelposition="top" 
					 size="12"	
				readOnly="true"				
					   id="txt_estimacionActual"/>
			</td>
			<td width="10%">
				<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.importePagado"
					 name="estimacionCoberturaSiniestro.datosEstimacion.importePagado"		
			labelposition="top" 
					 size="12"	
				readOnly="true"				
					   id="txt_importePagado"/>
			</td>
			<td width="10%">
				<s:textfield cssClass="txtfield deshabilitado formatCurrency" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.reserva"
					 name="estimacionCoberturaSiniestro.datosEstimacion.reserva"
			labelposition="top" 
					 size="12"	
				readOnly="true"				
					   id="txt_reserva"/>
			</td>
			<td width="60%">
				<s:select id="s_estatus" 
				         key="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.estatus"
					     labelposition="top" 
						 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.estatus"
						 headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="estatus" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield" 
					  	 onchange="onChangeEstatus();" 
				  		 disabled="true" /> 
				<s:hidden id="h_estatus" name="idEstatus"></s:hidden> 
			</td>
		</tr>
		<tr>
			<s:if test="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.coberturaReporteCabina.claveTipoDeducible > 0 && ( estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.coberturaReporteCabina.valorDeducible > 0 || estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.coberturaReporteCabina.porcentajeDeducible > 0 ) ">   
					<td>
								<s:select id="s_aplicaDeducible" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
								     labelposition="top" 
									 name="apDeducible"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="aplicaDeducible" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield  requerido requeridoGMT requeridoRAC  setNew" /> 
					</td>	
					
					<td>
						<div class="montoDeducible">
							<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict formatCurrency" 
											 key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.cantidad.deducible"
											 name="estimacionCoberturaSiniestro.montoDeducible"
									labelposition="top" 
											 maxlength="20"
											 size="10"			
											   id="montoDeducible"/>
						</div>
					</td>
					<td> 
						<div class="ctgMotivoNoaplicaDeducible">  
							<s:select id="ctgMotivoNoaplicaDeducible" 
							         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.motivo.no.deducible"
								     labelposition="top" 
									 name="estimacionCoberturaSiniestro.motivoNoAplicaDeducible"
									 headerKey="" 
									 headerValue="%{getText('midas.general.seleccione')}"
							  		 list="ctgNoAplicaDeducible" 
							  		 listKey="key" listValue="value"  
							  		 cssClass="txtfield setNew" /> 
						</div>
					</td>					
			</s:if>		
		</tr>
	</tbody>
</table>
</div>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.title"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td colspan="2">
				<s:textfield cssClass="txtfield  requerido requeridoGMT requeridoRAC  setNew jQalphabeticExt jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
					 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.nombreAfectado"
			labelposition="top" 
					 size="50"	
					 maxlength="100"
					   id="txt_nombreAfectado"/>
			</td>
			<td>
				<s:textfield cssClass="txtfield  requerido requeridoGMT requeridoRAC  setNew jQnumeric jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.edad"
					 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.edad"
			labelposition="top" 
					 size="3"	
					 maxlength="3"
					   id="txt_edad"/>
			</td>
			<td colspan="4">
				<s:textfield cssClass="txtfield  requerido requeridoGMT requeridoRAC  setNew jQalphabeticExt jQrestrict" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.personaContacto"
					 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.nombrePersonaContacto"
			labelposition="top" 
					 size="50"	
					 maxlength="100"
					   id="txt_personaContacto"/>
			</td>
		</tr>
		<tr>
			<td width="15%">
				<table>
					<tbody>
						<tr>
							<td width="30%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
									 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.ladaTelContacto"
							labelposition="top" 
									 size="3"
					 				maxlength="3"	
									   id="txt_lada1"/>
							</td>
							<td width="70%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
									 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.telContacto"
							labelposition="top" 
									 size="12"	
					 				maxlength="8"
									   id="txt_tel1"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td  width="15%">
				<table>
					<tbody>
						<tr>
							<td width="30%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"
									 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.ladaTelContactoDos"
							labelposition="top" 
									 size="3"
					 				maxlength="3"	
									   id="txt_lada2"/>
							</td>
							<td width="70%">
								<s:textfield cssClass="txtfield setNew jQnumeric jQrestrict" 
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
									 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.telContactoDos"
							labelposition="top" 
									 size="12"	
									 maxlength="8"
									   id="txt_tel2"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="5%">&nbsp;</td>
			<td width="60%">
				<div style="display:inline;float:left;width:30%">
				<s:textfield cssClass="txtfield setNew" 
					  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailinteresado"
					 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.email"
					labelposition="top" 
					 size="25"	
					 maxlength="50"
					 onblur="onBlurEmailInteresado();"
					 onchange="validationEmail(jQuery('#txt_email').val())"
					 id="txt_email"/>
				</div>
				<div style="display:inline;float:left;width:30%;margin-top:20px;">
				<s:checkbox id="emailNoProporcionado" name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.emailNoProporcionado" cssClass="setNew" 
				key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionado"
				labelposition="right" onchange="onChangeEmailNoProporcionado();" ></s:checkbox>
			</div>
			<div style="display:inline;float:left;width:40%">
			<s:select id="emailNoProporcionadoMotivo" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.emailnoproporcionadomotivo"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.emailNoProporcionadoMotivo"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="motivosCorreoNoPropocionado" 
					  		 cssClass="txtfield setNew" />
			</div>
			</td>
			<td id="td_numerosiniestro">
				<div class="elegible elegibleRAC">
						<s:textfield cssClass="txtfield setNew elegible elegibleRAC" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.numeroSiniestroTercero"
							 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.numeroSiniestroTercero"
					labelposition="top" 
							 size="18"	
							 maxlength="18"
							   id="txt_numeroSiniestroTercero"/>
				</div>
			</td>
		</tr>
		<div class="elegible elegibleRAC">
			<tr id="tr_companiaSeguros">
				<td>
					<s:select id="s_tieneCompania" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.tieneCompania"
						     labelposition="top" 
							 name="tieneCiaSegurosValor"
							 headerValue="%{getText('midas.general.seleccione')}"
							 headerKey="" 
					  		 list="tieneCiaSeguros" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield setNew"
					  		 /> 	
				</td>
				<s:hidden name="idCompaniaSegurosTercero" id="h_idCompaniaSegurosTercero"></s:hidden>
				
					
					<s:if test="estimacionCoberturaSiniestro.recibidaOrdenCia == 1">
						<td style="width: 20%;">
								<div class="elegible elegibleRAC elegibleSRE">
							<s:textfield cssClass="txtfield setNew deshabilitado elegible elegibleRAC elegibleSRE"
									  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
									  value="%{estimacionCoberturaSiniestro.nombreCompania}"
							labelposition="top" 
									 size="40"	
									 readonly="true"
									   id="txt_companiaSegurosTercero"/></div></td>
					</s:if>
					<s:else>
						<td colspan="5">
							<div style="display:inline;float:left;width:30%">
							<s:textfield cssClass="txtfield setNew deshabilitado"
								 label="Compañía de Seguros de Terceros"
								 value="%{estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.companiaSegurosTercero.personaMidas.nombre}"
								labelposition="top" 
								 size="40"	
								 readonly="true"
								   id="txt_companiaSegurosTercero"/>
							</div>
							<div class="btn_back w50" style="display: inline; float: left; margin-top: 14px; margin-left:14px;"  id="b_buscarCompaniaSeguros">
								<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('CIA',jQuery('#h_idCompaniaSegurosTercero'),jQuery('#txt_companiaSegurosTercero'));">
									<s:text name="midas.boton.buscar" /> 
								</a>
							</div>
						</td>
					</s:else>
			</tr>
		</div>
	</tbody>
</table>
</div>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.title"/>	
</div>
<div id="contenedorFiltros" style="width: 95%;">
<table id="agregar" border="0">
	<tbody>
		<tr>
			<td style="width: 10%; text-align: left;">
				<s:select 
					list="tiposEstado" 
					id="s_estadoPersona" 
					headerKey="" 
					labelposition="top"
					key="midas.siniestros.cabina.reportecabina.ordenPago.rcbienes.estado" 
					headerValue="%{getText('midas.general.seleccione')}"
					name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.estado"
					onchange="onChangeEstadoPersona();"
					cssClass="cajaTextoM2 w90 setNew" /> 
			</td>		
			<td id="td_tipoAtencion" style="text-align: left;">
				<s:select id="s_tipoAtencion" 
				         key="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.tipoAtencion"
					     labelposition="top" 
						 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.tipoAtencion"
						 headerKey="" 	
						 headerValue="%{getText('midas.general.seleccione')}"
				  		 list="tiposAtencion" 
				  		 listKey="key" listValue="value"  
				  		 cssClass="txtfield setNew"
				  		 onchange="onChangeTipoAtencion()"
				  		  /> 	
			</td>
			<td>&nbsp;</td>
		</tr>
		<div class="elegible elegibleGMT">	
			<tr>
				<td id="td_hospital" style="text-align: left;" colspan="2">
					<table>
						<tbody>
							<tr>
								<td width="50%" align="right">
									<s:textfield cssClass="txtfield setNew deshabilitado" 
										  label="Hospital"
							 			value="%{estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.hospital.personaMidas.nombre}"
								labelposition="top" 
										 size="40"	
										 readonly="true"
										   id="txt_hospital"/>
									<s:hidden name="idHospital" id="h_idHospital"></s:hidden>
								</td>
								<td width="50%">
									<div class="btn_back w40" style="display: inline; float: left;"  id="b_hospital">
										<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('HOSP',jQuery('#h_idHospital'),jQuery('#txt_hospital'));">
											<s:text name="midas.boton.buscar" /> 
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td id="td_otroHospital">
					<s:checkbox id ="ch_otroHospital"  name="otroHospital"  
						key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.otroHospital"
						cssClass="setNew"
						value="%{otroHospital}" 
						onclick="onChangeOtroHospital()"/>		
				</td>
				<td id="td_nombreOtroHospital">
					<s:textfield cssClass="txtfield setNew" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreOtroHospital"
							 name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.otroHospital"
					labelposition="top" 
							 size="12"	
						maxlength="50"
							   id="txt_otroHospital"/>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td id="td_medico" colspan="2">
					<table>
						<tbody>
							<tr>
								<td width="50%" align="left">
									<s:textfield cssClass="txtfield setNew deshabilitado" 
										  label="Médico"
							 			value="%{estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.medico.personaMidas.nombre}"
								labelposition="top" 
										 size="40"	
										 readonly="true"	
										   id="txt_medico"/>
									<s:hidden name="idMedico" id="h_idMedico"></s:hidden>
								</td>
								<td colspan="3">
									<div class="btn_back w40" style="display: inline; float: left;" id="b_medico" >
										<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio('MED',jQuery('#h_idMedico'),jQuery('#txt_medico'));">
											<s:text name="midas.boton.buscar" /> 
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td width="70%"colspan="3">&nbsp;</td>
				<!-- <td colspan="5">
					<div class="btn_back w60" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick=";">
							<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosPersonaLesionada.imagenes" /> 
						</a>
					</div>
				</td> -->
			</tr>
	</div>
	</tbody>
</table>
</div>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.descripcionLesiones"/>	
</div>	
<div id="contenedorFiltros" style="width: 95%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textarea name="estimacionCoberturaSiniestro.estimacionGastosMedicosConductor.descripcion" 
							id="ta_descripcionLesiones"
							cols="150"
							rows="5"
							maxlength="500"
							cssClass="textarea setNew" />
				</td>	
			</tr>
		</tbody>
	</table>
</div>
</s:form>
<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
	<tr>
		<td>
		<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarCoberturaGral();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>
			<s:if test="requiereAutorizacion == 0">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
						<a href="javascript: void(0);" onclick="javascript: guardarEstimacionGral();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
						</a>
					</div>	
			</s:if>
		    <s:else>
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_autorizar" >
						<a href="javascript: void(0);" onclick="javascript: autorizarSolicitudDeReserva();"> 
							<img border='0px' alt='Autorizar' title='Autorizar'/>
						</a>
					</div>
		    </s:else>
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_nuevo">
				<a href="javascript: void(0);" onclick="javascript: iniciarEstimacion();"> 
					<s:text name="midas.boton.nuevo" />
					<img border='0px' alt='Nuevo' title='Nuevo' src='/MidasWeb/img/b_mas_agregar.gif'/> 
				</a>
			</div>
		</td>							
	</tr>
</table>				
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
	initRCGMConductor();
	onBlurRequeridos();
	onChangeEmailNoProporcionado();
	onChangeEstatus();
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	
	initDeducible();
});
</script>
