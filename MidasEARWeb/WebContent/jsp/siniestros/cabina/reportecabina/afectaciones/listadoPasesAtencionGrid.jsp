<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
<%-- 		<column id="id" type="ro" width="100" sort="int" escapeXml="true" align="center" ><s:text name="midas.general.codigo"/></column> --%>
		<column id="numeroFolio" type="ro" width="100" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.numeroFolio"/></column>
		<column id="nombreCobertura" type="ro" width="300" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"/></column>
		<column id="tipoPaseAtencion" type="ro" width="300" sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.tipoPaseAtencion"/></column>
		<column id="montoReserva" type="ro" width="100"  sort="str" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.reserva"/></column>		
		<s:if test="%{soloConsulta == 0}">
			<column id="editar" type="img" width="40" sort="na" align="center"/>
		</s:if>
		<column id="consultar" type="img" width="40" sort="na" align="center"/>	
	</head>			
	<s:iterator value="pasesAtencionTercero" status="row">
		<row id="<s:property value="#row.index"/>">
<%-- 			<cell><s:property value="estimacionCoberturaReporte.coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/></cell>			 --%>
			<cell><s:property value="estimacionCoberturaReporte.folio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estimacionCoberturaReporte.nombreAfectado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estimacionTipoPaseDeAtencion" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="%{getText('struts.money.format',{montoReserva})}" escapeHtml="true" escapeXml="true"/></cell>	
			<s:if test="%{soloConsulta == 0}">
				<s:if test="estimacionCoberturaReporte.esEditable == 'EDT'">
					<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: mostrarEditarPaseAtencion(${estimacionCoberturaReporte.id})^_self</cell>
				</s:if>			
				<s:else>
					<cell>/MidasWeb/img/pixel.gif</cell>
				</s:else>
			</s:if>			
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: mostrarConsultaPaseAtencion(${estimacionCoberturaReporte.id})^_self</cell>																			
		</row>
	</s:iterator>	
</rows>