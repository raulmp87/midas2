<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="titulo" style="width: 98%;">
	<s:if test="tipoEstimacion == \"DMA\"">
		<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.title"/>
	</s:if>
	<s:else>
		<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.title"/>	
	</s:else>
</div>	
<s:if test="tipoEstimacion == \"DMA\"">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td colspan="3">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.nombreAfectado"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_nombreAsegurado_"/>
				</td>	
				<td colspan="2">
					<s:select id="s_aplicaDeducible" 
					         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.aplicaDeducible"
						     labelposition="top" 
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.aplicaDeducible"
							 headerKey="" 
							 headerValue="%{getText('midas.general.seleccione')}"
					  		 list="aplicaDeducible" 
					  		 listKey="key" listValue="value"  
					  		 cssClass="txtfield requerido"
					  		 disabled="soloConsulta" /> 
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  label=" "
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContacto"
				labelposition="top" 
						 size="4"	
					disabled="soloConsulta"				
						   id="txt_lada1"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContacto"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_tel1"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  label=" "
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContactoDos"
				labelposition="top" 
						 size="4"	
					disabled="soloConsulta"				
						   id="txt_lada2"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContactoDos"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_tel2"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.email"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.email"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_email"/>
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.companiaSegurosTercero"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_companiaSegurosTercero"/>
				</td>
				<td>
					<div class="btn_back w50" style="display: inline; float: left;"  >
						<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(22,jQuery('#txt_companiaSegurosTercero'));">
							<s:text name="midas.boton.buscar" />
						</a>
					</div>
				</td>
				<td colspan="2">
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.folioCiaSeguros"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.folioCompaniaSeguros"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_folioCiaSeguros"/>
				</td>
				<td>
					<s:textfield cssClass="txtfield jQrestrict" 
						  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danosMateriales.noSiniestroTercero"
						 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.numeroSiniestroTercero"
				labelposition="top" 
						 size="12"	
					disabled="soloConsulta"				
						   id="txt_noSiniestroTercero"/>
				</td>
			</tr>
		</tbody>
	</table>
</s:if>	
<s:else>
	<s:if test="tipoEstimacion == \"RCB\"">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td colspan="2">
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.nombreAfectado"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_nombreAfectado"/>
					</td>
					<td>
						<s:select id="s_danio" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.danio"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.dano"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="danos" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield requerido"
						  		 disabled="soloConsulta" /> 
					</td>
					<td colspan="2">
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.personaContacto"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.nombrePersonaContacto"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_personaContacto"/>
					</td>
				</tr>
				<tr>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  label=" "
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContacto"
					labelposition="top" 
							 size="4"	
						disabled="soloConsulta"				
							   id="txt_lada1"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContacto"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_tel1"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  label=" "
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContactoDos"
					labelposition="top" 
							 size="4"	
						disabled="soloConsulta"				
							   id="txt_lada2"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContactoDos"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_tel2"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.email"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.email"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_email"/>
					</td>
				</tr>
			</tbody>
		</table>
	</s:if>
	<s:else>
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td colspan="2">
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.nombreAfectado"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.nombreAfectado"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_nombreAfectado"/>
					</td>
					<td colspan="3">
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.personaContacto"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.nombrePersonaContacto"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_personaContacto"/>
					</td>
				</tr>
				<tr>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  label=" "
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContacto"
					labelposition="top" 
							 size="4"	
						disabled="soloConsulta"				
							   id="txt_lada1"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono1"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContacto"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_tel1"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  label=" "
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.ladaTelContactoDos"
					labelposition="top" 
							 size="4"	
						disabled="soloConsulta"				
							   id="txt_lada2"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.telefono2"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.telContactoDos"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_tel2"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.email"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.email"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_email"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<s:textfield cssClass="txtfield jQrestrict" 
							  key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.companiaSegurosTercero"
							 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.companiaSegurosTercero"
					labelposition="top" 
							 size="12"	
						disabled="soloConsulta"				
							   id="txt_companiaSegurosTercero"/>
					</td>
					<td>
						<div class="btn_back w170" style="display: inline; float: right;"  >
							<a href="javascript: void(0);" onclick="javascript: mostrarPrestadorServicio(22,jQuery('#txt_companiaSegurosTercero'));">
								<s:text name="midas.boton.buscar" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Buscar Compañía de Seguros' title='Buscar Compañía de Seguros' src='/MidasWeb/img/ib_ico_busq.gif' style="vertical-align: middle;"/> 
							</a>
						</div>
					</td>
					<td colspan="2">
						<s:select id="s_llegadaCia" 
						         key="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.rcBienes.llegadaCia"
							     labelposition="top" 
								 name="estimacionCoberturaSiniestro.estimacionCoberturaReporte.llegada"
								 headerKey="" 
								 headerValue="%{getText('midas.general.seleccione')}"
						  		 list="" 
						  		 listKey="key" listValue="value"  
						  		 cssClass="txtfield requerido"
						  		 disabled="soloConsulta" /> 
					</td>
				</tr>
			</tbody>
		</table>
	</s:else>
</s:else>