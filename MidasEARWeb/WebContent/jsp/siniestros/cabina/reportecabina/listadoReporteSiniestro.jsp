<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/listadoReporteSiniestro.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>

<style type="text/css">
	#superior{
		height: 200px;
		width: 98%;
	}

	
	
	#superiorI{
		height: 150px;
		width: 820px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		height: 60px;
		width: 850px;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px;">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.listadoReporteSiniestro.busqueda.title"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="formReporteSiniestroDTO">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 20%;">
						<s:select list="oficinasActivas" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.oficinaId" 
							label="Oficinas"
							cssClass="cleaneable txtfield"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						>
						</s:select>
					</div>
					<div class="floatLeft" style="width: 21%;">
						<s:textfield label="No. Siniestro" name="reporteSiniestroDTO.numeroSiniestro" 
								onBlur="validaFormatoSiniestro(this);"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 21%;">
						<s:textfield label="No. Reporte" name="reporteSiniestroDTO.numeroReporte"
								onBlur="validaFormatoSiniestro(this);"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 22%;">
						<div class="floatLeft" style="width: 60%;">
							<s:textfield label="No. Póliza" name="reporteSiniestroDTO.numeroPoliza"
									cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 40%;">
							<s:textfield label="No. Inciso" name="reporteSiniestroDTO.numeroInciso"
								onkeypress="return soloNumeros(this, event, false)"
								maxlength = "10"
								cssClass="cleaneable numeric txtfield" cssStyle="float: left;width: 70%;"></s:textfield>
						</div>
					</div>
					<div class="floatLeft" style="width: 16%;">
						<s:textfield label="No. Serie" maxlength="17" name="reporteSiniestroDTO.numeroSerie"
								cssClass="cleaneable txtfield"  cssStyle="float: left;width: 96%;"></s:textfield>
					</div>
				</div>
				<div id="SII">
					<s:radio  name="tipoPersona"  list="#{'1':'Persona Fisica','2':'Persona Moral'}" value="1" onchange="validatePersona();" />
					<div class="floatLeft" style="width: 45%;"  >
					<div id="nombre" > Contratante:</div>  <div id="razonSocial" >Razón Social: </div>
						<s:textfield  name="reporteSiniestroDTO.nombreAsegurado"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 45%;"  >
					<div id="nombre" > Nombre de quien Reporta: </div> 
						<s:textfield  name="reporteSiniestroDTO.nombrePersona"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
				</div>
				<div id="SIN">
					<div class="floatLeft" style="width: 45%;"  >
					<div id="nombreConductor" > Nombre del Conductor: </div> 
						<s:textfield  name="reporteSiniestroDTO.nombreConductor"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="estatusReporte" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.estatus" 
							label="Estatus"
							labelposition="left"
							cssClass="cleaneable txtfield"
							
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						>
						</s:select>
					</div>
					
					
				</div>
			</div>
			<div id="superiorD" style="float:right">
				<div id="SDS">
					<div style="height: 50%;width: 100%;margin-top:4%;">
						<label>Fecha de Reporte</label><br/>
					</div>
					<div style="height: 50%;width: 100%;">
						<label>Del:</label><br/>
						<sj:datepicker name="reporteSiniestroDTO.fechaIniReporte"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraReporte"
								  maxlength="10" cssClass="txtfield cleanable"
									   size="18"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield">
						</sj:datepicker>
						<br/>
						<label>Al:</label><br/>
						<sj:datepicker name="reporteSiniestroDTO.fechaFinReporte"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraTerminacion"
								  maxlength="10" cssClass="txtfield cleanable"
									   size="18"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield">
						</sj:datepicker>
					</div>
				</div>
				<div>
					<div class="btn_back w100" style="display: inline; margin-left: 10%;float: left;margin-top: 28%;">
						<a href="javascript: void(0);" onclick="limpiar();"> 
							<s:text	name="midas.boton.limpiar"/> 
						</a>
					</div>
					<div class="btn_back w100" style="display: inline; margin-left: 10%;float: left;margin-top: 28%;">
						<a href="javascript: void(0);" onclick="realizarBusqueda();"> 
							<s:text	name="midas.boton.buscar"/> 
						</a>
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
</div>

</br>
	<div class="titulo" style="margin-left: 2%">Listado de Reportes de Siniestros</div>
	</br>
	<div id="indicador"></div>
	<div id="inferior">
		<div id="listadoReportesSiniestrosGrid" style="width:95%;height:300px;margin-left: 2%">
		</div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>

	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tr>
			<td>	
				<div class="btn_back w120" style="display: inline; float: right;"  >
					<a href="javascript: void(0);" onclick="exportarExcelReporteSiniestro();">
						<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Perdida Total' title='Perdida Total' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
					</a>
				</div>
			</td>
		</tr>
	</table>
	
<script>
	jQuery(document).ready(function(){
	
		validatePersona();
	});
	
	function validatePersona(){
		var radioSelect 		= jQuery('input:radio[name=tipoPersona]:checked').val();

		if(radioSelect == 1){
			jQuery("#razonSocial").hide();
			jQuery("#nombre").show();
			
		}else if(radioSelect == 2){
			jQuery("#nombre").hide();
			jQuery("#razonSocial").show();
		
		}
	}
</script>
