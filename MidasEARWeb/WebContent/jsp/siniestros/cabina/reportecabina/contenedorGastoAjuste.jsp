<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_splt.js'/>"></script>
<sj:head/>
<script type="text/javascript" src="<s:url value='/js/ajaxScript.js'/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar_start.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/gastoAjuste.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}
.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

</style>


<script type="text/javascript">
	var mostrarGastoAjustePath = '<s:url action="cargarDatosAjuste" namespace="/siniestros/reporte/gastoAjuste"/>';
	var buscarNombrePrestadorPath = '<s:url action="buscarNombrePrestador" namespace="/siniestros/reporte/gastoAjuste"/>';	
	var buscarNombreTallerPath = '<s:url action="buscarNombreTaller" namespace="/siniestros/reporte/gastoAjuste"/>';	
	var buscarDatosPrestadorPath = '<s:url action="buscarDatosPrestador" namespace="/siniestros/reporte/gastoAjuste"/>';
	var mostrarGastoPath         = '<s:url action="mostrarGasto" namespace="/siniestros/reporte/gastoAjuste"/>';
</script>

<div class="titulo">
	<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.ListaTitle"/>	
</div>	
<div id="gastoAjusteGrid" class="dataGridConfigurationClass" style="width:930px;;max-height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<s:form id="gastoAjusteForm">
	<div id="gastoAjusteCatalogo">
		<s:hidden name="gastoAjusteId" id="gastoAjusteId"></s:hidden>
		<div class="titulo">
			<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.prestador"/>	
		</div>	
		<table id="filtrosM2" style="width: 98%;">
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tipoPrestador"/>	
				</td>
				<td>
					<s:select id="tipoPrestador"
								labelposition="left" 
								name="gastoAjuste.tipoPrestadorServicio.id"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="tiposPrestador" listKey="key" listValue="value"  
						  		cssClass="txtfield" /> 	
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.nombrePrestador"/>	
				</td>
				<td>
					<div>
						<sj:autocompleter cssStyle="display:none;"/>
						 <sj:textfield labelposition="top" 
							cssClass="cajaTextoM2 w200"
						    id="prestadorNombre" /> 
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.rfc"/>	
				</td>
				<td>
					<s:textfield id="txt_rfc"
							labelposition="left" cssClass="txtfield jQrestrict"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.curp"/>	
				</td>
				<td>
					<s:textfield id="txt_curp"
							labelposition="left" cssClass="txtfield jQrestrict"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.factura"/>	
				</td>
				<td>
					<s:textfield id="txt_factura"
							labelposition="left" cssClass="txtfield jQrestrict"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.motivoSituacion"/>	
				</td>
				<td>
					<s:select id="motivoSituacion"
								labelposition="left" 
								name="gastoAjuste.motivoSituacion"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="motivosSituacion" listKey="key" listValue="value"  
						  		cssClass="txtfield" /> 	
				</td>
			</tr>
		</table>
		<div class="titulo">
			<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.datosGrua"/>	
		</div>
		<table id="filtrosM2" style="width: 98%;">
			<tr>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.numReporte"/>	
				</td>
				<td>
					<s:textfield id="txt_num_reporte"
							labelposition="left" cssClass="txtfield jQrestrict"/>
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tipoGrua"/>	
				</td>
				<td>
					<s:select id="tipoGrua"
								labelposition="left" 
								name="gastoAjuste.tipoGrua"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="tiposGrua" listKey="key" listValue="value"  
						  		cssClass="txtfield" /> 	
				</td>
				<td>
					<s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tallerAsignado"/>	
				</td>
				<td>
					<div>
						<sj:autocompleter cssStyle="display:none;"/>
						 <sj:textfield labelposition="top" 
							cssClass="cajaTextoM2 w200"
						    id="tallerNombre" /> 
					</div>
				</td>
			</tr>
		</table>
	</div>
</s:form>		
<script>
	inicializarListadoGastoAjuste();
	jQuery(document).ready(function() {		
		jQuery(function(){
			jQuery('#prestadorNombre' ).autocomplete({
               source: function(request, response){  
               		jQuery.ajax({
			            type: "POST",
			            url: buscarNombrePrestadorPath,
			            data: {	nombrePrestador	:	request.term,
            				   	tipoPrestadorId : $('#tipoPrestador').val()},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( jQuery( "item", xmlResponse ).map( function() {	
								return {
									value: jQuery( "nombre", this ).text(),
									prestadorId: jQuery( "prestadorId", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 4,
               delay: 500		          
		     });
		});
		jQuery(function(){
			jQuery('#tallerNombre' ).autocomplete({
               source: function(request, response){  
               		jQuery.ajax({
			            type: "POST",
			            url: buscarNombrePrestadorPath,
			            data: {	nombreTaller	:	request.term,
            				   	tipoPrestadorId : $('#tipoPrestador').val()},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( jQuery( "item", xmlResponse ).map( function() {	
								return {
									value: jQuery( "nombre", this ).text(),
									prestadorId: jQuery( "prestadorId", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 4,
               delay: 500		          
		     });
		});	
	});
</script>