<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="id" type="ro" width="10" sort="str" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>
		<column id="tipoDireccion" type="ro" width="140" sort="str" align="center"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.tipoDireccion"/></column>
		<column id="pais" type="ro" width="80" sort="str" align="center"><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.pais"/></column>
		<column id="estado" type="ro" width="150" sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.estado"/></column>
		<column id="municipio" type="ro" width="150"  sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.municipio"/></column>		
		<column id="colonia" type="ro" width="150"  sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.colonia"/></column>	
		<column id="calleNumero" type="ro" width="*"  sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.calleNumero"/></column>	
		<column id="codigoPostal" type="ro" width="100"  sort="str" align="center" ><s:text name="midas.consultaIncisoPoliza.consultaAsegurado.codigoPostal"/></column>			
	</head>			
	<s:iterator value="listDireccionesCliente" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id"/></cell>	 
			<cell><s:property value="tipoDireccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="pais" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="estado" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="municipio" escapeHtml="false" escapeXml="true"/></cell>						
			<cell><s:property value="colonia" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="calleNumero" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="codigoPostal" escapeHtml="false" escapeXml="true"/></cell>														
		</row>
	</s:iterator>	
</rows>