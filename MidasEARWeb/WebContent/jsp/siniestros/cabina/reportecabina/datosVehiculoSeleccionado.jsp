<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<s:hidden id="serieCartaCobertura" name="detalleInciso.autoInciso.numeroSerie"/>
<s:hidden id="idToSolicitud" name="detalleInciso.idToSolicitud"/>
<s:hidden id="estatus" name="detalleInciso.estatus"/>
<s:if test="estatusVigenciaInciso != null">
	<div style="font-weight:bold; font-size:11px; color:red"> <s:text name="estatusVigenciaInciso"/></div>
	<br>
</s:if>
<table  class=" divContenedorO"   width="98%">
		
		<tr>
			<td width="90px" class="formulario"  ><s:text name="midas.general.descripcion" /> </td>
			<td colspan="3"><s:textfield  name="detalleInciso.autoInciso.descripcionFinal" id="txtClave" cssClass="cajaTexto w600 alphaextra"  readonly="true"></s:textfield></td>
			
	
			<td width="90px" class="formulario"  ><s:text name="midas.general.estatus" /> </td>
			<td><s:textfield name="detalleInciso.descEstatus"  id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		
		<tr>
			<td width="90px" class="formulario"  ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.vigenciaDesde" /> </td>
			<td><s:textfield name="detalleInciso.fechaVigenciaIniRealInciso"  id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario"  ><s:text name="midas.suscripcion.cotizacion.fecha.hasta" /> </td>
			<td><s:textfield  name="detalleInciso.fechaFinVigencia" id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario"  ><s:text name="midas.fuerzaventa.negocio.motivo" /> </td>
			<td><s:textfield  name="detalleInciso.motivo" id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		
		<tr>
			<td width="90px" class="formulario"  ><s:text name="Nombre de Linea " /> </td>
			<td colspan="3" ><s:textfield  name="detalleInciso.nombreLinea"  id="txtClave" cssClass="cajaTexto w600 alphaextra" readonly="true"></s:textfield></td>
			
			<td width="90px" class="formulario"  ><s:text name="midas.emision.consulta.siniestro.numeroinciso" /> </td>
			<td><s:textfield  name="detalleInciso.numeroInciso" id="txtClave" cssClass="cajaTexto w200 alphaextra" readonly="true"></s:textfield></td>
		</tr>
		


	</table>
	
		<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td  class= "alinearBotonALaDerecha">						
						<div id="btnCerrar" class="btn_back w140" style="display: inline; float: right; ">
							 <a href="javascript: void(0);" onclick="javascript:regresarReporteBusqueda();" >
							 <s:text name="midas.boton.cerrar" /> </a>
						</div>				
						<div class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="consultaInciso();"> 
							<s:text name="midas.boton.consultar" /> </a>
						</div>	
					</td>							
				</tr>
			</table>	
			<script type="text/javascript">
				jQuery(document).ready(function(){
					var reporteObj = jQuery('#idReporte');
					if(reporteObj == null || reporteObj.val() == null || reporteObj.val() == ''){
						jQuery('#btnCerrar').hide();
					}
				});
			</script>