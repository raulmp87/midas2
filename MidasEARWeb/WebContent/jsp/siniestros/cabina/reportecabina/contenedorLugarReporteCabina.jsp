<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/direccion/direccionSiniestroMidas.js'/>"  type="text/javascript"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"  type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/lugarReporteCabina.js'/>" type="text/javascript"></script>

<%-- <s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include> --%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script type="text/javascript">
var guardarLugarMidasPath = '<s:url action="guardarLugar" namespace="/siniestros/cabina/reportecabina"/>';
blockPageInWindow();
jQuery(document).ready(
	function(){
	 	configurarTitulo();
	 	onChangeZona();
	 	changeColoniaCheckEvent();
	 	
	 	parent.document.getElementById("idLugarAtencion").value=jQuery("#idLugarAtencion").val();
	 	parent.document.getElementById("latitudLugar").value=jQuery("#latitudLugar").val();
	 	parent.document.getElementById("longitudLugar").value=jQuery("#longitudLugar").val();

	 	setTimeout("readyMap()",'200');
	 	
 	}
 );

</script>

<style type="text/css">
 table,th,td
 {
 word-wrap:break-word;
 height:auto;
 }
 .label
 {
  width: 65px;
  word-wrap:break-word;
  height:auto;
 }
 
 
  .label
 {
 	 font-size: 80%;
 }
</style>

<s:hidden name="lugarAtencion.id" id="idLugarAtencion" class="setNew"  />
<s:hidden name="lugarAtencionOcurrido.id" id="idLugarAtencionOcurrido" class="setNew"  />
<s:hidden name="lugarAtencion.coordenadas.latitud" id="latitudLugar" class="setNew"  />
<s:hidden name="lugarAtencion.coordenadas.longitud" id="longitudLugar" class="setNew"  />

<s:form id="lugarForm" action="guardarLugar" namespace="/siniestros/cabina/reportecabina" name="lugarForm">
	<s:hidden id="idToReporte" name="idToReporte"/>
	<s:hidden id="tipoLugar" name="tipoLugar"/>
	<s:hidden id="tituloVentana" name="tituloVentana"/>
	<s:hidden id="nombreReporta" name="nombreReporta"/>
	<s:hidden id="telLadaReporta" name="telLadaReporta"/>
	<s:hidden id="telefonoReporta" name="telefonoReporta"/>
	
	<s:hidden id="latitud" name="latitud"/>
	<s:hidden id="longitud" name="longitud"/>
	<s:hidden id="isConfirmarUbicacion" name="isConfirmarUbicacion"/>
	<s:hidden id="coordenadasConfirmadas" name="coordenadasConfirmadas" />
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="4">
							<div id="tituloAtencion" style="display:none;">
								<s:text name="midas.boton.lugarAtencion" />
							</div> 
							<div id="tituloOcurrido" style="display:none;">
								<s:text name="midas.boton.midas.boton.lugarOcurrido" />
							</div>
	                    </td>
	                  </tr>
	                  <tr>
	                  	<td class="label">
							<s:text name="midas.siniestros.cabina.reporte.lugar.zona"/>
	               		</td>
	               		<td>
							<s:select list="zonas" headerKey="" headerValue="%{getText('midas.general.seleccione')}" name="lugarAtencionOcurrido.zona" id="zona_s" cssClass="cajaTextoM2 w250 setNew" onchange="onChangeZona();"/>
	               		</td>
	               		<td colspan="2">
	               			<s:if test='%{!coordenadasConfirmadas}'>
	               				<div style="color:red;">
	               					<s:text name="midas.siniestros.cabina.reporte.lugar.noconfirmacion"/>
	               				</div>
	               			</s:if>	               			
	               		</td>
	                  </tr>
	                  <tr>
	                        <td colspan="4">
								<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccionSiniestroMidas" ignoreContextParams="true" executeResult="true" >
	                                <s:param name="idPaisName">lugarAtencionOcurrido.pais.id</s:param>
									<s:param name="idEstadoName">lugarAtencionOcurrido.estado.id</s:param>
									<s:param name="idCiudadName">lugarAtencionOcurrido.ciudad.id</s:param>
									<s:param name="idColoniaName">lugarAtencionOcurrido.colonia.id</s:param>
									<s:param name="calleName">lugarAtencionOcurrido.calleNumero</s:param>
									<s:param name="numeroName">dir.num</s:param>
									<s:param name="numeroIntName">dir.numInt</s:param>
									<s:param name="cpName">lugarAtencionOcurrido.codigoPostal</s:param>
									<s:param name="nuevaColoniaName">lugarAtencionOcurrido.otraColonia</s:param>
									<s:param name="idColoniaCheckName">lugarAtencionOcurrido.idColoniaCheck</s:param>
									<s:param name="referenciaName">lugarAtencionOcurrido.referencia</s:param>
									<s:param name="labelPais">País</s:param>
									<s:param name="labelEstado">Estado</s:param>
									<s:param name="labelCiudad">Municipio</s:param>
									<s:param name="labelColonia">Colonia</s:param>
									<s:param name="labelCalle">Calle y Número</s:param>
									<s:param name="labelNumero">Número Ext.</s:param>
									<s:param name="labelNumeroInt">Número Int.</s:param>
									<s:param name="labelCodigoPostal">Código Postal</s:param>
									<s:param name="labelReferencia">Referencia</s:param>
									<s:param name="labelPosicion">left</s:param>
									<s:param name="componente">2</s:param>
									<s:param name="readOnly" value="false"></s:param>
									<s:param name="requerido" value="0"></s:param>
									<s:param name="incluirReferencia" value="true"></s:param>
									<s:param name="enableSearchButton" value="false"></s:param>
	                             </s:action>
	                        </td>
	                  </tr>
	                  <tr>
	                  		<td class="label">
								<s:text name="midas.siniestros.cabina.reporte.lugar.cp"/>
	               			</td>
	                  		<td>
	                  					<s:textfield name="lugarAtencionOcurrido.codigoPostalOtraColonia" id="codigoPostalOtraColonia_t" cssClass="cajaTextoM2 w250" disabled="false" />
	                  		</td>
	                  </tr>
<!-- 	                  <tr> -->
<!-- 	                  	<td> -->
<%-- 	                  					<s:select list="oficinas" headerKey="" headerValue="%{getText('midas.general.seleccione')}" label="Oficina" labelposition="left" name="lugarAtencionOcurrido.oficina.id" id="oficina_s" cssClass="cajaTexto w250" onchange=""/> --%>
<!-- 	                  	</td> -->
<!-- 	                  	<td> -->
<%-- 	                  					<s:textfield name="lugarAtencionOcurrido.referencia" id="referencia_t" label="Referencia" labelposition="left" cssClass="cajaTextoM2 w250" disabled="false" /> --%>
<!-- 	                  	</td> -->
<!-- 	                  </tr> -->
	                  <tr>
	                  	<td class="label">
							<s:text name="midas.siniestros.cabina.reporte.lugar.tipocarretera"/>
	               		</td>
		                  <td>
							<s:select list="tipoCarreteras" headerKey="" headerValue="%{getText('midas.general.seleccione')}" name="lugarAtencionOcurrido.tipoCarretera" id="tipoCarretera_s" cssClass="cajaTexto w250" onchange=""/>
		                  </td>
		                  <td class="label">
							<s:text name="midas.siniestros.cabina.reporte.lugar.kilometro"/>
	               		</td>
		                  <td>
							<s:textfield name="lugarAtencionOcurrido.kilometro" id="kilometro_t" cssClass="cajaTextoM2 w250" disabled="false" maxlength="20"/>
		                  </td>
	                  </tr>
	                  <tr>
	                 	<td class="label">
							<s:text name="midas.siniestros.cabina.reporte.lugar.nombrecarretera"/>
	               		</td>
						<td>
							<s:textfield name="lugarAtencionOcurrido.nombreCarretera" id="nombreCarretera_t"  cssClass="cajaTextoM2 w250" disabled="false" />
						</td>
	                  </tr>
	                  <tr>
	                  	<td colspan="4">
	                  		
	                  		<div  id="confirmarUbicacion" class="btn_back w140" style="display: inline; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="guardarLugarMidas( true );"> 
		                        <s:text name="midas.boton.confirmarUbicacion" /> </a>
							</div>
							
							<div id="btn_guardar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="guardarLugarMidas( false );"> 
		                        <s:text name="midas.boton.guardar" /> </a>
							</div>
							
							<div class="btn_back w80"  style="display:inline; float: right; position: relative;">
					            <a href="javascript: void(0);"            	
					            onclick="cerrarVentanaLugar()">
					            <s:text name="midas.boton.cerrar" /> </a>
							</div>
							
							<div id="btn_limpiar" class="btn_back w80"  style="display:inline; float: right;position: relative; ">
					             <a href="javascript: void(0);" onclick="limpiarFormularioLugar();"> 
		                        <s:text name="midas.boton.limpiar" /> </a>
							</div>
							<div style=" font-size: 90%;">
								<s:text name="midas.siniestros.cabina.reporte.lugar.notamarcador"/>
							</div> 
	                  	</td>
	                  </tr>
	            </tbody>
	</table>
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
</s:form>

<div id="contenedor_map" style="float: left;position: relative;margin-left:5px;width: 980px; height: 350px;margin-right: 5px;">
	<s:include value="/jsp/siniestros/cabina/mapaSiniestros.jsp"></s:include>
</div>

<script type="text/javascript">
jQuery(window).load(
	function(){
		if(parent.jQuery("#h_soloConsulta").val() == 1){
			jQuery(".setNew").attr("disabled","disabled");
			jQuery(".setNew").addClass("consulta");
			jQuery("#btn_limpiar").remove();
			jQuery("#btn_guardar").remove();
			jQuery("#confirmarUbicacion").remove();
		}
		
		//DESHABILITAR AUTOCOMPLETE IE
		jQuery(".autocompletar").attr("autocomplete",false);
		jQuery("#lugarForm").attr("autocomplete","off");
		unblockPageInWindow();
	}
);
</script>
</div>
<script type="text/javascript" src="<s:url value='/js/midas2/google/maps/generalFunctions.js'/>"></script>
<%-- <script type="text/javascript" src="<s:url value='/js/mapaSiniestros.js'/>"></script> --%>