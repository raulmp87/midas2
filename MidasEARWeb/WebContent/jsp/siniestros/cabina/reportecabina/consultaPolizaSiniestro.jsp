<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<style type="text/css">
.dhxcont_main_content {
	background: white;
}

.heightCell {
	height: 30px !important;
}
</style>



<script type="text/javascript">
	var mostrarDatosClientePath = '<s:url action="mostrarDatosCliente" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var buscarCondicionesEspecialesIncisoPath = '<s:url action="mostrarCondicionesEspecialesInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var buscarCoberturasIncisoPath = '<s:url action="mostrarCoberturasInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var imprimirIncisoPolizaPath = '<s:url action="imprimirPolizaInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var datosRiesgoCoberturaPath = '<s:url action="obtenerDatosRiesgo" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var asignarIncisoPath = '<s:url action="asignarIncisoReporte" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarSolicitarAutorizacionPath = '<s:url action="mostrarSolicitarAutorizacion" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarDetalleInciso = '<s:url action="mostrarDetalleInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarDatosRiesgo = '<s:url action="mostrarDatosRiesgo" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarDatosVehiculo = '<s:url action="mostrarDatosVehiculo" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var buscarPoliza = '<s:url action="buscarPoliza" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarEndoso = '<s:url action="mostrarEndoso" namespace="/siniestros/cabina/reporteCabina/consulta"/>';
	var mostrarDatosProrrogaPath = '<s:url action="mostrarProrroga" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
	
	
</script>

<s:form id="historialBusquedaInciso" >

		<s:hidden name="filtroBusqueda.servicioPublico"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroSerie"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.placa"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.nombreAsegurado"></s:hidden>
		<s:hidden name="filtroBusqueda.nombreContratante"></s:hidden>
		<s:hidden name="filtroBusqueda.servicioParticular"></s:hidden>
		<s:hidden name="filtroBusqueda.numeroPoliza"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroMotor"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaIniVigencia"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaFinVigencia"></s:hidden>
		<s:hidden name="filtroBusqueda.numeroInciso"></s:hidden>
		<s:hidden name="filtroBusqueda.busquedaPolizaSeycos"></s:hidden>
		<s:hidden name="incisoContinuityId"></s:hidden>
		<s:hidden name="idReporte" id="idReporte"></s:hidden>
		<s:hidden name="fechaReporteSiniestro"></s:hidden>
		<s:hidden name="validOnMillis"></s:hidden>
		<s:hidden name="recordFromMillis"></s:hidden>
		<s:hidden name="fechaReporteSiniestroMillis"></s:hidden>
		<s:hidden name="soloConsulta"/>
</s:form>

	<s:form id="polizaSiniestroForm" >		
		<s:hidden name="idToPoliza" id="h_idPoliza"></s:hidden>
		<s:hidden name="detalleInciso.numeroInciso" id="h_numeroInciso"></s:hidden>
		<s:hidden name="detalleInciso.numeroSecuencia" id="h_numeroSecuencia"></s:hidden>
		<s:hidden name="validOn" id="h_validOn"></s:hidden>
		<s:hidden name="recordFrom" id="h_recordFrom"></s:hidden>
		<s:hidden name="incisoContinuityId" id="h_incisoContinuityId"></s:hidden>
		<s:hidden name="detalleInciso.idToCotizacion" id="detalleInciso.idToCotizacion"></s:hidden>
		<s:hidden name="idReporte" id="h_idReporte"></s:hidden>
		<s:hidden name="incisoVigenteAutorizado" id="h_incisoVigenteAutorizado"></s:hidden>
		<s:hidden name="fechaReporteSiniestro" id="h_fechaReporteSiniestro"></s:hidden>
		<s:hidden name="solicitudPoliza" id="h_solicitudPoliza"></s:hidden>
		<s:hidden name="detalleInciso.descEstatus" id="h_descEstatus"></s:hidden>
		<s:hidden name="detalleInciso.datosContratante.idPersona" id="h_idPersona"></s:hidden>
		<s:hidden name="detalleInciso.autoInciso.numeroSerie" id="h_numeroSerie"></s:hidden>
		<s:hidden name="detalleInciso.datosContratante.idPersonaContratante" id="h_idPersonaContratante"></s:hidden>
		<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
		<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
		<s:hidden name="fechaReporteSiniestroMillis" id="h_fechaReporteSiniestroMillis"></s:hidden>
		<s:hidden name="soloConsulta" id="h_soloConsulta"/>
		<s:hidden name="detalleInciso.estatus" id="h_incisoEstatus"/>
		
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.consultaIncisoPoliza.datosIncisoPoliza"/>	
		</div>	
		<div id="contenedorFiltros" style="width: 95%;">
			<table id="agregar" border="0">
				<tbody>
					
					<tr>
		
						<th><s:text name="midas.fuerzaventa.configBono.poliza" /> </th>
						<td><s:text name="detalleInciso.numeroPoliza"  /> </td>
						
						<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.polizaAnterior" /> </th>
						<td><s:text name="detalleInciso.numeroPolizaAnterior"   /></td>
						
						<th><s:text name="midas.suscripcion.cotizacion.estatusPoliza" /> </th>
						<td><s:text name="detalleInciso.descEstatus"  /></td>
						
						<th colspan="2"><s:text name="midas.consultaIncisoPoliza.motivoSituacion" /> </th>
						<td><s:text name="detalleInciso.motivo"  /></td>
			
						<td><td>
						<%--  <td>
						 	
								<s:if test="%{estatusSolicitud !=-2 }">
									<div style="display:table-row; ; float: right;">						
										<s:if test="%{estatusSolicitud!=0 && estatusSolicitud!=1 }">
										<s:if test ="%{idReporte != null }">
											<div id="btnAutorizar"  class="btn_back w140" >
												<a href="javascript: void(0);"  onclick="solicitarAutorizacion();"><s:text name="midas.boton.solicitarAutorizacionVigencia"/></a>
											</div>
											</s:if>
										</s:if>
										<div id="txtAutorizar"  style="text-align: center;">
											<s:if test="%{estatusSolicitud==0}"><b><a style="color:gray;font-size: 12px;"><s:text name="midas.solicitarAutorizacionVigencia.estatusPendiente"/></a></b></s:if>
											<s:elseif test="%{estatusSolicitud==1}"><b><a style="color:green;font-size: 12px;"><s:text name="midas.solicitarAutorizacionVigencia.estatusAprobado"/></a></b></s:elseif>
											<s:elseif test="%{estatusSolicitud==2}"><b><a style="color: red;font-size: 12px;"><s:text name="midas.solicitarAutorizacionVigencia.estatusRechazado"/></a></b></s:elseif>
										</div>
									</div>
								</s:if>
						 </td>--%>
						 
					</tr>
					
					
					<tr>
					
						<th><s:text name="midas.emision.consulta.vehiculo.paquete" /> </th>
						<td><s:text name="detalleInciso.autoInciso.paquete.descripcion"  /></td>
						
						<th><s:text name="midas.emision.consulta.siniestro.numeroinciso" /> : <s:text  name="detalleInciso.numeroInciso" ></s:text> </th>
						<td></td>
						
						<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.vigencia" /> :</th>
						<th><s:text name="midas.fuerzaventa.configuracionPagosComisiones.inicio" /> </th>
						<td><s:text name="detalleInciso.fechaVigenciaIniRealInciso" /></td>
						
						<th><s:text name="midas.fuerzaventa.configuracionPagosComisiones.fin" /> </th>
						<td><s:text name="detalleInciso.fechaFinVigencia"  /></td>
						
						<th><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.fechaEmision" /> </th>
						<td><s:text name="detalleInciso.fechaRealEmisionPoliza"  /></td>
						
						<td>
							<s:if test="%{detalleInciso.tieneProrroga==true}"> 
								<div class="btn_back w170" style="display: inline; float: right;"  >
									<a href="javascript: void(0);" onclick="javascript:obtenerProrroga();">
										<s:text name="midas.boton.consultarProrroga" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Consultar de Prórroga' title='Consulta de Prórroga' src='/MidasWeb/img/b_ico_busq.gif' style="vertical-align: middle;"/> 
									</a>
								</div>
 							</s:if> 
						</td>	
							
					</tr>
					
					
				</tbody>
			</table>
		</div>
		
		<br/>
		
		<div id="contenedorFiltros" style="width: 95%;">
		<table id="agregar" border="0">
			<tbody>
				
				<tr>
					<th><s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.asegurado" /> </th>
					<td colspan="4">
						<s:textfield name="detalleInciso.datosContratante.nombreContratante"  cssClass="cajaTexto w350 alphaextra setNew" readonly="true"  readonly="true" ></s:textfield>
					</td>
					
					<td colspan="3">
						<div class="btn_back w170" style="display: inline; float: right;"  >
							<a href="javascript: void(0);" onclick="javascript:consultarAsegurado();">
								<s:text name="midas.boton.consultarAsegurado" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Consultar Asegurado' title='Consultar Asegurado' src='/MidasWeb/img/icons/ico_buscarcliente.gif' style="vertical-align: middle;"/> 
							</a>
						</div>
					<td>
	
					
					<th><s:text name="midas.siniestros.cabina.reporte.gastoAjuste.rfc" /> </th>
					<td colspan="2"><s:textfield name="detalleInciso.datosContratante.rfcFiscal" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
				
				</tr>
				
				<tr>
				
					<th><s:text name="midas.catalogos.centro.operacion.calleYNumero" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.calleNumeroFiscal"  cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.catalogos.centro.operacion.colonia" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.coloniaFiscal"  cssClass="cajaTexto w100 alphaextra setNew"  readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.catalogos.centro.operacion.codigoPostal" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.cpFiscal"  cssClass="cajaTexto w40 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.catalogos.centro.operacion.municipio" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.municipioFiscal"  cssClass="cajaTexto w90 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.catalogos.centro.operacion.estado" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.estadoFiscal"  cssClass="cajaTexto w90 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.catalogos.oficina.pais" /> </th>
					<td><s:textfield name="detalleInciso.datosContratante.paisFiscal"  cssClass="cajaTexto w90 alphaextra setNew" readonly="true" ></s:textfield></td>
					
				
				</tr>
				
			</tbody>
		</table>
		</div>
		
		<br/>
			
		<div id="contenedorFiltros" style="width: 95%;">
		<table id="agregar" border="0">
			<tbody>
				
				<tr>
		
					<th><s:text name="midas.negocio.producto.relacionar.producto" /> </th>
					<td><s:textfield name="detalleInciso.producto" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.cliente.datosCobranza.grid.column.medioPago" /> </th>
					<td><s:textfield name="detalleInciso.medioPago" id="polizaAnterior" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.provisiones.lineaNegocio" /> </th>
					<td><s:textfield name="detalleInciso.nombreLinea" id="lineaNegocio" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
						 
				</tr>
				
				<tr>
		
					<th><s:text name="midas.agtSaldos.moneda" /> </th>
					<td><s:textfield name="detalleInciso.moneda" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.cotizacion.formapago" /> </th>
					<td><s:textfield name="detalleInciso.formaPago" id="polizaAnterior" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
		
					<td></td>
					<td>
						 <div class="btn_back w170" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="listadoSiniestros();"> 
						    	<s:text name="midas.boton.siniestros" />
							</a>
					    </div>
					 <td>
					 
						 
					</tr>
					
			</tbody>
		</table>
		</div>
		
		<br/>
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.consultaIncisoPoliza.informacionVehiculo"/>	
		</div>	
		<div id="contenedorFiltros" style="width: 95%;">
			<br/>
			
			<div id="a_tabbar" class="dhtmlxTabBar"   style="width:100%; height:180px;" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE"  >
				
				<div id="html_1" style="display: none;" name="Veh&iacute;culo" >
				
					<table id="agregar" border="0">
						<tbody>
							
							<tr>
								<th><s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.marca" /> </th>
								<th colspan="3"><s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.estilo" /> </th>
								<th colspan="3"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosVehiculoEstimacion.tipoVehiculo" /> </th>
								<th colspan="2"><s:text name="midas.general.version" /> </th>
							</tr>
							
							<tr>
							
								<td><s:textfield name="detalleInciso.autoInciso.descMarca" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td colspan="3"><s:textfield name="detalleInciso.autoInciso.descripcionFinal" id="poliza" cssClass="cajaTexto w400 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td colspan="2" ><s:textfield name="detalleInciso.autoInciso.descTipoUso" id="poliza" cssClass="cajaTexto w300 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td colspan="2" ><s:textfield id="poliza" cssClass="cajaTexto w100 alphaextra setNew" readonly="true" ></s:textfield></td>
								
							</tr>
							
							<tr>
								<th><s:text name="midas.excepcion.suscripcion.auto.modelo" /> </th>
								<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.puertas" /> </th>
								<th><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor" /> </th>
								<th><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie" /> </th>
								<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.vecchiculoVerificado" /> </th>
								<th></th>
							</tr>
							
							<tr>
							
								<td><s:textfield name="detalleInciso.autoInciso.modeloVehiculo" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td><s:textfield name="detalleInciso.datosPersonalizacion.puertas" id="poliza" cssClass="cajaTexto w50 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td><s:textfield name="detalleInciso.autoInciso.numeroMotor" id="poliza" cssClass="cajaTexto w100 alphaextra setNew"  readonly="true" ></s:textfield></td>
								<td><s:textfield name="detalleInciso.autoInciso.numeroSerie" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
								<td> <s:checkbox name="" readonly="true" disabled="true" ></s:checkbox> </td>
								<td>
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="viewDatosRiesgo();"> 
											<s:text name="midas.boton.datosRiesgo" />
											
										</a>
									</div>	
								</td>
							</tr>
						
							
						</tbody>
					</table>
					
				</div>
				
				
        		<div id="html_2" name="Personalizaci&oacute;n"  style="display: none;" >
        				
        				<table id="agregar" border="0">
							<tbody>
								
								<tr>
									<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.uso" /> </th>
									<th colspan="2"><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.conductorHabitual" /> </th>
									<th><s:text name="midas.emision.consulta.vehiculo.placa" /> </th>
									<th><s:text name="midas.poliza.transmision" /> </th>
								</tr>
								
								<tr>
								
									<td><s:textfield name="detalleInciso.autoInciso.descTipoUso" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td colspan="2"><s:textfield name="detalleInciso.autoInciso.nombreCompletoConductor" id="poliza" cssClass="cajaTexto w350 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td><s:textfield name="detalleInciso.autoInciso.placa" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td><s:textfield name="detalleInciso.datosPersonalizacion.transmision" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									
								</tr>
								
								<tr>
									<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.repuve" /> </th>
									<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.cilindros" /> </th>
									<th><s:text name="midas.siniestros.cabina.reportecabina.consultaincisopoliza.tipoAlarma" /> </th>
									<th></th>
									<th></th>
								</tr>
								
								<tr>
								
									<td><s:textfield name="detalleInciso.autoInciso.repuve" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td><s:textfield name="detalleInciso.datosPersonalizacion.cilindros" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td><s:textfield name="detalleInciso.datosPersonalizacion.tipoAlarma" id="poliza" cssClass="cajaTexto w150 alphaextra setNew" readonly="true" ></s:textfield></td>
									<td></td>
									<td></td>
								</tr>
							
								
							</tbody>
						</table>
        		
        		
        		</div>	
        		
			</div>
	
		</div>
		
		<br/>
		
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.consultaIncisoPoliza.listadoCoberturas"/>		
		</div>	
		<div id="contenedorFiltros" style="width: 95%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td width="85%">
						<br/>
						<div id="coberturasIncisoGrid" class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
						<div id="pagingArea"></div><div id="infoArea"></div>
						<br/>
					</td>	
					<td width="15%">
						<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
							<tbody>
								<tr>
									<td>	
										<div class="btn_back w160" style="display: inline; float: right;">
											<a href="javascript: void(0);" onclick="mostrarProgramasPago();"> 
												<s:text name="midas.boton.programasPago" />
											</a>
										</div>	
									</td>
								</tr>
								<tr>
									<td>
										<div class="btn_back w160" style="display: inline; float: right;" >
											<a href="javascript: void(0);" onclick="mostrarEndosos()"> 
												<s:text name="midas.boton.endosos" /> 
											</a>
										</div>
									</td>	
								</tr>
								<tr>
									<td>																									
										<div id="condicionesBtn" class="btn_back w160" style="display: inline; float: right;">
											<s:if test="%{!condicionesEspecialesList.isEmpty()}">												
												<a href="javascript: void(0);" style="color:red;" onclick="javascript:inicializarCondicionesEspeciales(0);"> 
													<s:text name="midas.boton.condicionesEspeciales" />
												</a>
											</s:if>										
											<s:else>
												<a href="javascript: void(0);" onclick="javascript:inicializarCondicionesEspeciales(0);"> 
													<s:text name="midas.boton.condicionesEspeciales" />
												</a>
											</s:else>
										</div>										
									</td>	
								</tr>
							</tbody>
						</table>		
					</td>	
				</tr>
				<tr><td>
				<div id="divDatosRiesgo" style="display: none;">
				<table>
				
				<tr>
					<td  style="font-size: 16px;font-weight: bold;"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.observaciones"/></td>
				</tr>
				<tr>
					<td ><div id="datosRiesgoCobertura"> </div></td>
				</tr>
				
				</table>
				</div>
				</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</s:form>
	<br/>
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>	
					<div class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="javascript:mostrarContenedorImpresionIncisoPoliza(2, jQuery('#h_idPoliza'), -1, -1, jQuery('#h_validOn'), jQuery('#h_validOnMillis'), jQuery('#h_recordFromMillis'), jQuery('#h_recordFrom'), null,false, jQuery('#h_numeroSecuencia'));"> 
							<s:text name="midas.boton.imprimir" />
							<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
						</a>
					</div>	
					<div id="btn_regresarBusqueda" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:regresarBusqueda();"> 
							<s:text name="midas.boton.regresarBusqueda" /> 
							<img border='0px' alt='Regresar' title='Regresar' src='/MidasWeb/img/b_anterior.gif'/>
						</a>
					</div>	
					<s:if test="%{idReporte != null}">
						<div id="btn_regresarReporte" class="btn_back w140" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:regresarReporte();"> 
								<s:text name="midas.boton.regresarReporte" /> 
								<img border='0px' alt='Regresar Reporte' title='Regresar Reporte' src='/MidasWeb/img/b_anterior.gif'/>
							</a>
						</div>	
						<div id="d_asignarInciso" class="btn_back w170" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:asignarInciso();"> 
								<s:text name="midas.boton.asignarPolizaReporte" />
								<img border='0px' alt='Asignar Inciso' title='midas.boton.consultarAsegurado' src='/MidasWeb/img/b_mas_agregar.gif'/> 
							</a>
						</div>
					</s:if>	
				</td>
			</tr>
		</tbody>
	</table>		
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarPoliza();
		dhx_init_tabbars();		
	});
</script>

