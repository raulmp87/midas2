<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>


<s:form id="busquedaHistoricoMovimientos" >
	<s:hidden id="mostrarSoloEncabezados" name="mostrarSoloEncabezados"/>
	<s:hidden id="esPantallaGA" name="esPantallaGA"/>
	<s:hidden id="idToReporte" name="idToReporte"/>
	<s:hidden id="h_soloConsulta" name="soloConsulta"/>
	<s:hidden id="h_numeroSiniestro" name="filtroMovimientos.numeroSiniestro"/>

	<table width="99%" bgcolor="white" align="center" class="contenedorConFormato">
    <tbody>
  	<tr>
	<td colspan="3" >
	<table  id="filtrosM2" width="99%" border="0">
            <tr>
                  <td class="titulo" colspan="3"><s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.busqueda" /></td>
            </tr>
            <tr>
            
            	<td class="label" >
					<s:textfield id="siniestro_t" cssClass="cajaTextoM2 w230" 
	            		value="%{filtroMovimientos.numeroSiniestro}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.numSiniestro')}" disabled="true" />
          		</td>
          		
            	 <td  align="left"> <sj:datepicker name="filtroMovimientos.fechaIni"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2"
					label="Fecha desde"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				
				</td>
			
				<td > <sj:datepicker name="filtroMovimientos.fechaFin"
					id="txtFechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2"
					label="Fecha hasta"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
				</td>
				
				<td class="label" >
					<s:select label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.usuario')}"
					list="listaUsuarios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroMovimientos.usuario" id="usuario_s" cssClass="cajaTextoM2 w230" 
					cssStyle="display:inline;" onchange=""/>
          		</td>
          		
            </tr>
            
            <tr>
				
				
          		
          		<td class="label" >
					<s:select label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.movimientos')}"
					list="tipoMovimientos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroMovimientos.tipoMovimiento" id="movimientos_s" cssClass="cajaTextoM2 w230" 
					cssStyle="display:inline;" onchange=""/>
          		</td>
          		
          		<td class="label" >
          			<s:if test="!esPantallaGA">
						<s:select label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.cobertura')}" 
						list="coberturas" headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						listKey="idCoberturaTipoAfectacion" listValue="nombreCobertura" 
						name="filtroMovimientos.idCoberturaTipoEstimacion" id="coberturas_s" cssClass="cajaTextoM2 w230" 
						cssStyle="display:inline;" onchange=""/>
					</s:if>
          		</td>
          		
          		<td class="label" colspan="3" >
					<s:textfield id="descripcion_t" cssClass="cajaTextoM2 w600" 
	            		name="filtroMovimientos.descripcion" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.descripcionmovs')}" disabled="false" />
          		</td>
          		
            </tr>
            
            <tr>		
            		<td colspan="3">
						<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="buscarHistoricoMovimientos(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right; margin-left: 2%;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar" >
										<a href="javascript: void(0);" onclick="regresarReporteBusqueda();">
											<s:text name="midas.boton.cerrar" /> 
										</a>
									</div>
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      						</td>
					  </tr>
					  
					  <tr><td>
					  </td></tr>
					  
					<tr>
						<td colspan="3">      
							<div id="tituloListado" style="display:none;" class="titulo" style="width:63%;"><s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.listado" /></div>	
							
							<div id="historicoMovimientosGridContainer">
								<div id="historicoMovimientosGrid" style="width:63%;height:300px;"></div>
								<div id="pagingArea"></div><div id="infoArea"></div>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="3">  
							<table bgcolor="white" class="contenedorConFormato" width="99%" border="0">
								<tr>
									<td width="100%" colspan="4">
										<div id="tituloResumen" style="display:inline;" class="titulo" style="width:63%;"><s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.resumenmovimientos" /></div>
									</td>
								</tr>
								
								<tr>
									<s:if test="esPantallaGA">
										<td>
											<s:textfield id="totalGastos_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{totalGastos}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.totalgastos')}" disabled="true" />
										</td>
										
										<td colspan="3">
											<s:textfield id="totalRecuperacionGastos_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{totalRecuperacionGastos}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.totalrecupgastos')}" disabled="true" />
										</td>
									</s:if>
									<s:else>
										<td>
											<s:textfield id="totalEstimado_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{totalEstimado}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.totalestimado')}" disabled="true" />
										</td>
										
										<td>
											<s:textfield id="totalPagado_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{totalPagado}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.pagado')}" disabled="true" />
										</td>
										
										<td>
											<s:textfield id="reservaPendiente_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{reservaPendiente}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.reservapendiente')}" disabled="true" />
										</td>
										
										<td>
											<s:textfield id="totalIngresos_t" cssClass="cajaTextoM2 w230 formatCurrency" 
		            							value="%{totalIngresos}" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.ingresos')}" disabled="true" />
										</td>
									</s:else>					
								</tr>
							</table>
						</td>
					</tr>
	            </tbody>
	</table>
</s:form>

<script type="text/javascript">
jQuery(document).ready(function(){
});
</script>
