<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaCoberturasRechazo</param>
				<param>true</param>
				<param>infoAreaCoberturasRechazo</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="nombreCobertura" type="ro" width="425"  sort="str" ><s:text name="%{'midas.siniestros.cabina.reportecabina.afectacionInciso.cobsrechazo.nombrecobs'}"/></column>	
        <column id="montoRechazo"  type="edn"  width="*"  align="center" format="$0,000.00" sort="int"> <s:text name="%{'midas.siniestros.cabina.reportecabina.afectacionInciso.cobsrechazo.montorechazo'}" /> </column>
		
		</head>
		<s:iterator value="listAfectacionCoberturaSiniestroDTO">
			<row id="<s:property value="#row.index"/>">
				<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true" /></cell>
				<cell class="jQfloat jQrestrict"><s:property  value="montoRechazo" escapeHtml="false" escapeXml="true"  /></cell>
		    
			    <userdata name="coberturaReporteCabinaIdCell"><s:property value="coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/></userdata>
			    <userdata name="claveTipoCalculoCell"><s:property value="coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/></userdata>
			    <userdata name="claveSubTipoCalculoCell"><s:property value="configuracionCalculoCobertura.cveSubTipoCalculoCobertura" escapeHtml="false" escapeXml="true"/></userdata>
			</row>
		</s:iterator>
	
</rows>
