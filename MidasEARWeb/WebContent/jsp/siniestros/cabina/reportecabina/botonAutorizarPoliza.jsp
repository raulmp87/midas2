<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">


<s:form id="polizaSiniestroForm" >

	<s:hidden name="estatusSolicitud" id="h_estatusSolicitud"></s:hidden>
	
</s:form>


<script type="text/javascript">

var estatusSolicitud 		= jQuery('#h_estatusSolicitud').val();
var btnAutorizar 			= parent.getParentObject('btnAutorizar');
var txtAutorizar 			= parent.getParentObject('txtAutorizar');

	if(estatusSolicitud ==0 || estatusSolicitud==1 ){
		if(btnAutorizar != null){
			btnAutorizar.style.display = 'none';
		 }
	}
	txtAutorizar.innerHTML = '';
	
	if(estatusSolicitud==0){
		txtAutorizar.innerHTML = ' <a style="color:gray ;"> Solicitud Pendiente </a>';
	}else if (estatusSolicitud==1){
		txtAutorizar.innerHTML = ' <a style="color:green ;"> Solicitud Aprobada </a>';
	}else if(estatusSolicitud==2){
		txtAutorizar.innerHTML = ' <a style="color:red ;"> Solicitud rechazada </a>';
	}
	
	parent.cerrarVentanaModal('vm_solicitarAutorizacion');
	

</script>


			