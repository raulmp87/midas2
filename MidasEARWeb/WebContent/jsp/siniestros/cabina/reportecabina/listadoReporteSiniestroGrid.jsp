<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:set var="noPermiteAfectar" value="false" />				
	<m:tienePermiso nombre="FN_M2_SN_No_Permite_Afectacion_Reserva">		
		<s:set var="noPermiteAfectar" value="true" />
	</m:tienePermiso>
	<s:iterator value="reportesSiniestro" var="index">
		<s:set var="editable" value="true" />		
		<s:if test="%{numeroSiniestro != null && #noPermiteAfectar}">			
			<s:set var="editable" value="false" />	
		</s:if>
		<row>			
		    <cell><s:property value="nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="ajustador" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAsegurado" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombrePersona" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreConductor" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/> </cell>
		    <cell>../img/icons/ico_verdetalle.gif^Consultar^javascript: consultarReporte("<s:property value="claveOficina" escapeHtml="false" escapeXml="true"/>","<s:property value="consecutivoReporte" escapeHtml="false" escapeXml="true"/>","<s:property value="anioReporte" escapeHtml="false" escapeXml="true"/>")^_self</cell>		    		
			<s:if test='%{estatus != 2 && estatus != 4 && estatus != 5  && #editable}'   >
				<cell>../img/icons/ico_editar.gif^Editar^javascript: editarReporte("<s:property value="claveOficina" escapeHtml="false" escapeXml="true"/>","<s:property value="consecutivoReporte" escapeHtml="false" escapeXml="true"/>","<s:property value="anioReporte" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
			<cell>../img/b_printer.gif^Imprimir^javascript: imprimirReporte("<s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			<s:if test="(numeroPoliza != null)"   >			
				<s:if test='%{estatus != 2 && estatus != 4 && estatus != 5 &&#editable}'   >
					<cell>../img/signopesos.gif^Afectar Reserva^javascript: afectarReserva("<s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/>","<s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/>","<s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/>","<s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/>","<s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/>")^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif</cell>
				</s:else>																	
				<cell>../img/b_pendiente.gif^Listado de Pases^javascript: mostrarListadoDeReporte("<s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>	
			<s:else>
				<cell>../img/pixel.gif</cell>	
				<cell>../img/pixel.gif</cell>
			</s:else>
			
			
			<s:if test="(reporteCabinaId != null && estatus != 2 && estatus != 4)"   >
				<cell>../img/esquemaPagos.jpg^Gasto de Ajuste/ Reembolso Gasto de Ajuste^javascript: mostrarGastoAjuste("<s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>	
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
			
			<s:if test="(reporteCabinaId != null &&  siniestroCabinaId != null && estatus != 2 && estatus != 4)"   >
				<cell>../img/esquemaPagos.jpg^Ordenes de Compra^javascript: mostrarOrdenesCompra("<s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>	
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
				
		</row>
	</s:iterator>
</rows>