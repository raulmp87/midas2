<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="tipoPrestador" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.reporte.gastoAjuste.tipoPrestador"/></column>
		<column id="nombrePrestador" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.reporte.gastoAjuste.nombrePrestador"/></column>
		<column id="motivoSituacion" type="ro" width="*" sort="str"><s:text name="midas.siniestros.cabina.reporte.gastoAjuste.motivoSituacion"/></column>
		<column id="fechaAsignacion" type="ro" width="*" sort="date_custom"><s:text name="midas.siniestros.cabina.reporte.gastoAjuste.fechaAsignacion"/></column>
		<s:if test="soloConsulta != 1">
			<column id="editar" type="img" width="30" sort="na" align="center"/>
		</s:if>
	</head>
	<s:iterator value="gastosAjusteList" var="gridListGastoAjuste" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${gridListGastoAjuste['tipoPrestador']}]]></cell>
			<cell><![CDATA[${gridListGastoAjuste['nombrePrestador']}]]></cell>
			<cell><![CDATA[${gridListGastoAjuste['motivoSituacion']}]]></cell>
			<cell><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${gridListGastoAjuste['fechaAsignacion']}" /></cell>
			<s:if test="soloConsulta != 1">
				<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarGastoAjuste(<s:property value="['id']" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
			</s:if>
		</row>
	</s:iterator>
</rows>