<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script
	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/reportecabina.js'/>"></script>
<script
	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}

.divInfDivInterno {
	width: 17%;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}

#b_generar {
	background-image: url("../img/icons/ico_regresar.gif");
	background-repeat: no-repeat;
	text-align: center;
	width: 16px;
	height: 16px;
}
</style>

<script type="text/javascript">
	var mostrarDatosIncisoPoliza = '<s:url action="mostrarDatosIncisoPoliza" namespace="/siniestros/cabina/reportecabina"/>';
	var mostrarDetalleInciso = '<s:url action="mostrarDetalleInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
	var validarPendientesReporte = '<s:url action="validarPendientesReporte" namespace="/siniestros/cabina/reportecabina"/>';
	var solicitarAutorizacionVigencia = '<s:url action="solicitarAutorizacionVigencia" namespace="/siniestros/cabina/reportecabina"/>';
	 
</script>


<div id="contenido_reportecabina" style="width:99%;position: relative;">
	<s:hidden id="h_soloConsulta"/>
	<s:form id="reporteCabinaForm" class="floatLeft">
		<s:hidden id="ventanaOrigen" name="ventanaOrigen" class="setNew" />
		<s:hidden id="idToReporte" name="idToReporte" class="setNew" />
		<s:hidden id="id" name="entidad.id" class="setNew" />
		<s:hidden name="entidad.fechaModEstatus" class="setNew" />
		<s:hidden id="entidad_estatus" name="entidad.estatus" class="setNew" />
		<s:hidden name="entidad.fechaHoraReporte" id="h_fechaReporteSiniestro" class="setNew"  />
		<s:hidden name="dateReporteMillis" id="h_dateReporteMillis" class="setNew"  />
		<s:hidden name="dateAsignacionMillis" id="h_dateAsignacionMillis" class="setNew"  />
		<s:hidden name="dateContactoMillis" id="h_dateContactoMillis" class="setNew"  />
		<s:hidden name="dateTerminacionMillis" id="h_dateTerminacionMillis" class="setNew"  />
		<s:hidden name="detalleInciso.validOnMillis" id="h_validOnMillis"></s:hidden>
		<s:hidden name="detalleInciso.recordFromMillis" id="h_recordFromMillis"></s:hidden>
		<s:hidden name="detalleInciso.incisoContinuityId" id="h_incisoContinuityId" class="setNew"  />
		<s:hidden name="detalleInciso.idToSolicitud" id="h_idToSolicitud" class="setNew"  />
		<s:hidden name="detalleInciso.estatus" id="h_estatusInciso" class="setNew"  />
		<s:hidden name="incisoAutorizado" id="h_incisoAutorizado" class="setNew"  />
		<s:hidden name="validacionFechaHoraOcurrido" id="h_validacionFechaHoraOcurrido" class="setNew"  />
		<s:hidden name="vieneBandejaSolicitudAntiguedad" id="h_bandejaSolicitudAntiguedad" class="setNew"  />
		<s:hidden name="vieneExpedienteJuridico" id="h_expedienteJuridico" class="setNew"  />
		<s:hidden id="idExpedienteJuridico" name="expedienteJuridico.id"/>
		<s:hidden id="h_modoConsultaExpediente" name="modoConsultaExpediente"/>
		
		<s:hidden name="isEditable" id="isEditable" />
						
			
		
		<s:hidden name="lugarAtencion.id" id="idLugarAtencion" class="setNew"  />
		<s:hidden name="lugarAtencion.coordenadas.latitud" id="latitudLugar" class="setNew"  />
		<s:hidden name="lugarAtencion.coordenadas.longitud" id="longitudLugar" class="setNew"  />
	
		<s:hidden name="noEditableParaRol" id="noEditableParaRol" class="setNew"  />
		
		<div class="titulo"><s:text name="midas.listadoSiniestrosAction.titulo.servicioCabina"/></div>
		<div id="divSup">
			<div class="floatLeft divContenedorO" style="width: 70%; margin-right: 1%; height: 315px;">
				<div style="height: 60px;">
					<div class="floatLeft"
						style="width: 37.5%; margin-left: 1%; height: 20px;">
						<s:text name="midas.listadoSiniestrosAction.titulo.numSiniestro"/>	
						</br>
						</br>
						<div style="width: 95%;" class="floatLeft">
							<div class="floatLeft" style="height: 30px; width: 25%;">
								<s:textfield id="nsOficina" maxlength="5"
									name="entidad.claveOficina" cssStyle="width:88%;"
									cssClass="floatLeft setNew txtfield" onkeypress="buscarReporteEnter(event);" ></s:textfield>
								<label style="margin-left: 5%; width: 5%;" class="floatLeft">
									<b>-</b> </label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 40%;">
								<s:textfield id="nsConsecutivoR" maxlength="15"
									name="entidad.consecutivoReporte" cssStyle="width: 95%;"
									cssClass="floatLeft numeric setNew txtfield" onkeypress="buscarReporteEnter(event);" ></s:textfield>
								<label style="margin-left: 2%; width: 2%;" class="floatLeft">
									<b>-</b> </label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 35%; float:right">
								<s:textfield id="nsAnio" name="entidad.anioReporte"
									cssStyle="width: 30%;" maxlength="2"
									cssClass="floatLeft numeric setNew txtfield" onkeypress="buscarReporteEnter(event);" ></s:textfield>
								<div class="btn_back w50" id="btn_Busqueda"
									style="display: inline; float: left; vertical-align: top; position: relative; margin-top: -3%;">
									<a href="javascript: void(0);" onkeypress="buscarReporte();" onclick="buscarReporte();">
										<s:text name="midas.boton.buscar" /> </a>
								</div>
							</div>
						</div>
						<s:hidden id="entidadNumeroReporte" name="entidad.numeroReporte"
							class="setNew" />
					</div>
					<div class="floatLeft" style="width: 250px;">
						<s:select list="oficinasActivas" id="ofinasActivas"
							name="entidad.oficina.id" 
							key="midas.servicio.siniestros.oficina"
							cssClass="requerido setNew txtfield txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 20%;">
						<s:textfield label="Estatus del Reporte" name="estatusReporte" id="estatusReporte"
							cssStyle="float: left;" readonly="true" cssClass="setNew txtfield" size="25"></s:textfield>
					</div>
				</div>
				<div style="height: 60px; margin-left: 1%;">
					<div class="floatLeft" style="width: 37%;">
						<s:textfield id="reporta" label="*Nombre de quien reporta"
							name="entidad.personaReporta" maxlength="100"
							cssClass="requerido setNew txtfield" cssStyle="float: left;width:84%;" onkeypress="copiarEnter(event);"></s:textfield>
						<div class="btn_back w30" id="btn_copiarPersona"
							style="display: inline; float: left; margin-top: -1%;">
							<a id="b_copiar" href="javascript: void(0);"
								title="Persona que reporta, es el conductor."
								onclick="mismoConductor();"> </a>
						</div>
					</div>
					<div class="floatLeft" style="width: 30.3%; margin-left: 1%;">
						<s:textfield id="conductor" label="Conductor"
							name="entidad.personaConductor" cssStyle="float: left;width:93%;"
							maxlength="100" cssClass="setNew txtfield">
						</s:textfield>
					</div>
					<div class="floatLeft" style="width: 29%;">
						<div class="floatLeft" style="width: 24%;">
							<s:textfield label="*Lada" name="entidad.ladaTelefono"
								cssStyle="float: left;width:100%;" maxlength="3"
								cssClass="requerido numeric setNew txtfield"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 42%;">
							<s:textfield label="*Teléfono" name="entidad.telefono"
								cssStyle="float: left;" maxlength="8"
								cssClass="requerido numeric setNew txtfield" ></s:textfield>
						</div>
					</div>
				</div>
				<div style="height: 60px; margin-left: 1%;">
					<div class="floatLeft" style="width: 38%;">
						<s:textfield label="Atendido por" name="atendidoPor"
							cssStyle="float: left;width:94%;"
							readonly="true" cssClass="txtfield setNew">
						</s:textfield>
					</div>
					<div class="floatLeft" style="width: 20%;">
						<div class="btn_back w130" style="margin-top: 27px;">
							<a href="javascript: void(0);"
								onclick="guardarReporteYMostrarLugarAtencion();"> <s:text
									name="midas.boton.lugarAtencion" /> </a>
						</div>
					</div>
					<div class="floatLeft" style="width: 20%;">
						<input id="target" type="hidden"  class="setNew" />
						<div class="btn_back w130 isSetId" style="margin-top: 27px;">
							<s:hidden id="informacionAdicional"
								name="entidad.informacionAdicional" cssClass="setNew" />
							<a class=""
								onclick="agregarTextoLibre('mostrarInfoAdicional','Informaci\u00F3n Adicional','informacionAdicional','salvarInformacionAdicional');"
								alt="Inf. Adicional" href="javascript: void(0);"> <s:text
									name="midas.boton.infoAdicional" /> </a>
						</div>
					</div>
					<div class="floatLeft" style="width: 20%;">
						<div class="btn_back w130 isSetId" style="margin-top: 27px;">
							<a href="javascript: void(0);"
								onclick="mostrarLugarReporte('Lugar de Ocurrido', 'OC');"> <s:text
									name="midas.boton.midas.boton.lugarOcurrido" /> </a>
						</div>
					</div>
				</div>
				<div style="height: 60px; width: 830px; margin-top: 3%;"
					class="floatLeft">
					<div class="floatLeft" style="width: 250px; margin-left: 1%;">
						<div class="floatLeft" style="width: 40%;">
							<sj:datepicker name="entidad.fechaOcurrido"
								label="Fecha Ocurrido" labelposition="left" changeMonth="true"
								changeYear="true" buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" id="fechaOcurrido" maxlength="18"
								cssStyle="margin-top:13%;" cssClass="txtfield setNew" size="10"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</div>
						<div class="floatLeft" style="width: 140px; height: 100%;">
							<s:textfield id="horaOcurrido" label="Hora Ocurrido (24:00)"
								name="entidad.horaOcurrido" cssStyle="width: 100px;"
								cssClass="floatLeft setNew txtfield"  onblur="actualizaValorTime(this.id, this.value); CheckTime(this)"></s:textfield>
						</div>
					</div>
					<div style="height: 60px; width: 560px;" class="floatLeft">
						<div style="width: 50px;" class="floatLeft">
							<s:textfield id="ajustadorIdTcAjustador" label="Ajustador"
								name="entidad.ajustador.id" readonly="true" cssClass="floatLeft setNew txtfield"
								cssStyle="width: 90%;margin-left: 1%;"></s:textfield>
						</div>
						<div style="height: 60px; width: 350px;" class="floatLeft">
							<s:textfield id="ajustadorNombre" name="entidad.ajustador.personaMidas.nombre" label=""
								cssClass="floatLeft setNew txtfield" readonly="true"
								cssStyle="margin-left: 1%;width:98%;"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno"  id="div_asignacionAjustador">
						
							<div class="btn_back w120" style="display: inline; float: left; margin-top: 3%; margin-left: 10%;">
									<a href="javascript: void(0);" onclick="viewWindow( 'asignarAjustador' ,'','' );"> <s:text name="midas.boton.asignarManualmente" />
									</a>
									</div>
							<div class="btn_back w120" style="display: inline; float: left; margin-top: 3%; margin-left: 10%;">
								<a href="javascript: void(0);" onclick="viewWindow( 'mostrarAjustadorMapa' ,'','' );">
									<s:text name="midas.boton.asignarAjustadorEnMapa" />
								</a>
							</div>
						</div>
						
					</div>
				</div>				
				
				
			</div>
			<div class="floatLeft"
				style="height: 300px; width: 300px;">
				<div class="divContenedorO"
					style="height: 254px; width: 100%; margin-bottom: 7px;">
					<div style="height: 20px; margin-left: 10%;">
						<label><b>Fecha y Hora</b> </label>
					</div>
					<div style="height: 35px; margin-left: 25%;">
						<div class="floatLeft">
							<label>De Reporte</label>
							<s:textfield name="entidad.fechaHoraReporte"
								labelposition="left"
								id="entidad.fechaHoraReporte" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
									cssStyle="width:105px;">
								</s:textfield>
						</div>
						
						<div class="floatLeftNoLabel" style="margin-left: 4px;" >
							<s:textfield name="horaReporte"
								id="horaReporte" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
								cssStyle="width:45px;"
								>
							</s:textfield>
						</div>
						
						
					</div>
					<div style="height: 35px; margin-left: 25%; margin-top: 2%;">
						<div class="floatLeft">
							<label>De Asignaci&oacute;n</label>
							<s:textfield name="entidad.fechaHoraAsignacion"
								labelposition="left"
								id="fechaHoraAsignacion" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
									cssStyle="width:105px;"
								>
							</s:textfield>
						</div>						
						 <div class="floatLeftNoLabel" style="margin-left: 4px;"  >
							<s:textfield name="horaAsignacion"
								id="horaAsignacion" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
								cssStyle="width:45px;">
							</s:textfield>
						</div>						
					</div>
					<div style="height: 35px; margin-left: 25%; margin-top: 2%;">						
						<div class="floatLeft">							
							<label>De Contacto</label>							
							<s:textfield name="entidad.fechaHoraContacto"
								labelposition="left"
								id="entidad.fechaHoraContacto" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
									cssStyle="width:105px;"
								>
							</s:textfield>
						</div>						
					 	<div class="" style="margin-top:2%;position:relative;">
							<s:textfield name="horaContacto"
								id="horaContacto" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
								cssStyle="width:45px;margin-top: 5%; margin-left:10px;float:left;">
							</s:textfield>		
							<div class="btn_back w20" id="btn_generaFechaContacto"
								style="float: right;margin-right:8%;margin-top:2%;position:relative;">
								<a id="b_generar" href="javascript: void(0);"
									title="Generar fecha y hora de contacto"
									onclick="generarFechaHoraContacto();"> </a>
							</div>					
						</div>
					</div>					
					<div style="height: 35px; margin-left: 25%; margin-top: 2%;">					
						<div class="floatLeft">
							<label>De Terminaci&oacute;n</label>
							<s:textfield name="entidad.fechaHoraTerminacion"
								labelposition="left"
								id="fechaTerminacion" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
								cssStyle="width:105px;"
								>
							</s:textfield>
						</div>	
						<div class="" style="margin-top:2%;position:relative;">
							<s:textfield name="horaTerminacion"
								id="horaTerminacion" maxlength="20"
								cssClass="txtfield setNew"
								disabled="true"
								cssStyle="width:45px;margin-top: 5%; margin-left:10px;float:left;">
							</s:textfield>		
							<div class="btn_back w20" id="btn_generaFechaTermino"
								style="float: right;margin-right:8%;margin-top:2%;position:relative;">
								<a id="b_generar" href="javascript: void(0);"
									title="Generar fecha y hora de termino"
									onclick="generarFechaHoraTerminoAjustador();"> </a>
							</div>					
						</div>															
					</div>
					
					
					<div style="height: 35px; margin-left: 15%; margin-top: 3%;">
						<div class="floatLeft" style="width: 25%;margin-right:30px"">
							<div class="btn_back w80">
								<a onclick="mostrarCitaReporte('mostrarCita','Citas','citas');"
								href="javascript: void(0);">
									<s:text name="midas.boton.midas.boton.citas"/> </a>
							</div>
						</div>
						<div class="floatLeft" style="width: 40%;">
							<div class="btn_back w120">
								<a onclick="mostrarGastoAjuste('mostrarGastosAjuste','Gastos de Ajuste','gastosAjuste');"
									href="javascript: void(0);">
									<s:text name="midas.boton.midas.boton.gastosAjuste" /> </a>
							</div>
						</div>
					</div>
					<div style="height: 35px; margin-left: 30%; margin-top: 1%;"  >
						<div class="floatLeft" style="width: 25%;">
							<div class="btn_back w120" style="display: none;" id="btnActualizaFechas">
								<a onclick="actualizaFechas();"
								href="javascript: void(0);">
									<s:text name="midas.boton.midas.boton.actualizaFechas"/> </a>
							</div>
						</div>
					</div>
				</div>
				<div class="divContenedorO" style="height: 52px; width: 100%;">
					<div id="btn_declaracionSiniestro" class="btn_back w160"
						style="margin-left: 75px; margin-top: 15px;">
						<s:hidden id="declaracionTexto" name="entidad.declaracionTexto"
							cssClass="setNew" />
						<a class=""
							onclick="agregarTextoLibre('mostrarDeclaracion','Declaraci\u00F3n Siniestro','declaracionTexto','salvarDeclaracionSiniestro');"
							alt="Declaración de Siniestro" href="javascript: void(0);">Declaraci&oacute;n
							de Siniestro</a>
					</div>
					
				</div>
			</div>
		</div>
		<div id="divInf" style="width: 1142px !important;" class="floatLeft">
			</br>
			<div class="titulo">P&oacute;liza</div>
	 
			<div id="contenedorPoliza" class="divContenedorO">
				<s:if test="estatusVigenciaInciso != null">
					<div style="font-weight:bold; font-size:11px; color:red"> <s:text name="estatusVigenciaInciso"/></div>
					<br>
				</s:if>
				<s:if test="incisoAutorizado != null">
							<div id="txtAutorizar"  style="font-weight:bold; font-size:12px"  class=" divInfDivInterno">
							<s:if test="%{incisoAutorizado==0}"><b><a style="color:gray;"><s:text name="midas.solicitarAutorizacionVigencia.estatusPendiente"/></a></b></s:if>
							<s:elseif test="%{incisoAutorizado==1}"><b><a style="color:green;"><s:text name="midas.solicitarAutorizacionVigencia.estatusAprobado"/></a></b></s:elseif>
							<s:elseif test="%{incisoAutorizado==2}"><b><a style="color: red;"><s:text name="midas.solicitarAutorizacionVigencia.estatusRechazado"/></a></b></s:elseif>
							</div>
						</s:if>
				<div class="divInterno">
					<div class="floatLeft divInfDivInterno">
						<s:textfield id="idToSolicitud" name="detalleInciso.idToSolicitud" cssClass="readOnly txtfield setNew" label="Solicitud Póliza" cssStyle="" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft divInfDivInterno" style="height: 35px;" >
						<s:textfield id="numeroPoliza" name="detalleInciso.numeroPoliza"  cssClass="readOnly txtfield setNew"  label="Póliza" cssStyle="" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft" style="width: 8%;">
						<s:textfield name="detalleInciso.numeroInciso" cssClass="readOnly txtfield setNew" label="Inciso" cssStyle="width:60px;float:left;" readonly="true" ></s:textfield>
						<div class="btn_back w30"
							style="display: none; float: left; vertical-align: top;">
							<a href="javascript: void(0);" onclick=""> <s:text name="" />
							</a>
						</div>
					</div>
					<div class="floatLeft " style="width: 13%;">
						<s:textfield name="detalleInciso.descEstatus" cssClass="readOnly txtfield setNew" label="Estatus de Póliza" cssStyle="" readonly="true"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield name="detalleInciso.motivo" label="Estatus Cobranza" cssClass="readOnly txtfield setNew"  cssStyle="width: 100%;" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft divInfDivInterno">
						<s:textfield name="detalleInciso.moneda" cssClass="readOnly txtfield setNew"  label="Moneda" cssStyle="" readonly="true" ></s:textfield>
					</div>
				</div>
				<div class="divInterno">
					<div class="floatLeft divInfDivInterno">
						<s:textfield name="detalleInciso.autoInciso.numeroSerie" cssClass="readOnly txtfield setNew" label="No. de Serie" cssStyle="" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft divInfDivInterno">
						<s:textfield name="detalleInciso.fechaVigenciaIniRealInciso" cssClass="readOnly txtfield setNew" label="Inicio de Vigencia" cssStyle="" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft divInfDivInterno">
						<s:textfield name="detalleInciso.fechaFinVigencia" cssClass="readOnly txtfield setNew" label="Termino de Vigencia" cssStyle="" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield name="detalleInciso.autoInciso.descripcionFinal" label="Descripción del Vehículo" cssClass="readOnly txtfield setNew" cssStyle="width:100%;" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft divInfDivInterno">
						<div id="btn_BusquedaPoliza" class="btn_back w120"
							style="display: inline; float: left; margin-top: 3%; margin-left: 10%;">
							<a href="javascript: void(0);" onclick="mostrarBusquedaIncisoPoliza();"> 
							<s:text	name="midas.boton.midas.boton.busquedaPoliza" /> </a>
						</div>
						
						<div id="btn_ConsultaPoliza" class="btn_back w120"
							style="display: none; float: left; margin-top: 3%; margin-left: 10%;">
							<a href="javascript: void(0);" onclick="mostrarConsultaIncisoPoliza();"> 
							<s:text	name="Consulta de Póliza" /> </a>
						</div>
					</div>
					
				</div>
				<div class="divInterno">
					<div class="floatLeft" style="width: 30%;">
						<s:textfield name="detalleInciso.datosContratante.nombreContratante" cssClass="readOnly txtfield setNew" label="Contratante" cssStyle="width:90%;" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft" style="width: 30%;">
						<s:textfield name="detalleInciso.agente" cssClass="readOnly txtfield setNew" label="Agente" cssStyle="width:90%;" readonly="true" ></s:textfield>
					</div>
					<div class="floatLeft" style="width: 30%;">
						<s:textfield name="detalleInciso.producto" cssClass="readOnly txtfield setNew" label="Producto" cssStyle="width:90%;" readonly="true" ></s:textfield>
					</div>
					
				</div>
			</div>
			
			<div class="divInterno"	style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
				
				<div  class="btn_back w150"
					style="display: inline; float: left;margin-left: 1%;">
					<a href="javascript: void(0);" onclick="mostrarReferenciasBancarias();"> <s:text
							name="midas.boton.referencia" /> </a>
				
				</div>
				
				
				<div id="nuevo" class="btn_back w80"
					style="display: inline; float: left;margin-left: 1%;">
					<a href="javascript: void(0);" onclick="setNew();"> <s:text
							name="midas.boton.nuevo" /> </a>
				</div>
				<div class="btn_back w80" style="display: inline;margin-left: 1%;float: left;">
				<a href="javascript: void(0);"  onclick="mostrarBitacora('mostrarContenedor');"> 
					<s:text	name="midas.boton.midas.boton.bitacora"/> 
				</a>
				</div>
				<div class="btn_back w150"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="viewWindow('mostrarHistoricoMovimientos','','');"> <s:text
							name="midas.boton.midas.boton.historicoMovimientos" /> </a>
				</div>
				<div class="btn_back w150"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="imprimirReporte();"> 
						<s:text name="midas.boton.midas.boton.impresionReporte" />
					</a>
				</div>
				<div class="btn_back w150" id="btn_reaperturar"
					style="display: none; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="reaperturarReporte();"> <s:text
							name="midas.siniestros.cabina.reportecabina.reaperturar" /> </a>
				</div>
				
				<div class="btn_back w150" id="btn_afectar"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="mostrarAfectacionInciso();"> <s:text
							name="midas.boton.midas.boton.afectacionInciso" /> </a>
				</div>
				
				<div id="guardar" class="btn_back w90"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="salvarReporte(true);"> <s:text
							name="midas.boton.guardar" /> </a>
				</div>
				<div id="cancelar" class="btn_back w90"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="cancelarReporte();"> <s:text
							name="midas.boton.cancelar" /> </a>
				</div>
				<s:if test="vieneBandejaSolicitudAntiguedad == true">
						<div class="btn_back w80"
							style="display: inline; margin-left: 1%; float: left;">
							<a href="javascript: void(0);" onclick="regresaBandejaDeAutorizaciones();">
								<s:text name="midas.boton.cerrar" /> </a>
						</div>
				</s:if>	
				<s:elseif test="vieneExpedienteJuridico == true">
						<div class="btn_back w80"
							style="display: inline; margin-left: 1%; float: left;">
							<a href="javascript: void(0);" onclick="regresaExpedienteJuridico();">
								<s:text name="midas.boton.cerrar" /> </a>
						</div>
				</s:elseif>
				<s:else>
						<div class="btn_back w80"
							style="display: inline; margin-left: 1%; float: left;">
							<a href="javascript: void(0);" onclick="cerrarVentanaReporte();">
									<s:text name="midas.boton.cerrar" /> </a>
						</div>
				</s:else>
				<div class="w30"
							style="display: inline; margin-left: 1%; float: left;">
							<s:property value="version" default=""/>
						</div>
			</div>
		</div>
	</s:form>
</div>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
validateBotonConsulta();
//validateFechaHoraTerminacion();
validateFechaHoraOcurrido();
validacionEstatusReporte();
validaBotonesAsignacionAjustador();
isSupervisorCabina();
</script>