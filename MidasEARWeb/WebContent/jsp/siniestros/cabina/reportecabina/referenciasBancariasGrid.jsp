<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="noRecuperacion"     type="ro" width="110"  sort="int" ><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.no.recuperacion'}"/></column>	
        <column id="tipoRecuperacion"   type="ro" width="120" sort="str" ><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.tipo.recuperacion'}"/></column>
		<column id="fechaRegistro"      type="ro" width="120"  sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.fecha.registro.de'}"/></column>	
		<column id="nombreProveedor"    type="ro" width="305" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.proveedor.comprador'}"/> </column>
		<column id="fechaSiniestro"     type="ro" width="110" sort="date_custom"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.fecha.siniestro.de'}"/> </column>
		<column id="monto"              type="ron" width="50" sort="int" format="$0,000.00"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.monto'}"/> </column>
		<column id="estatus"            type="ro" width="135" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.estatus.recuperacion'}"/> </column>
		<column id="paseAtencion"       type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.recuperacion.listado.recuperaciones.pase.atencion'}"/> </column>
		<column id="VerPDF"             type="img" width="70" sort="na" align="center" >Acciones</column>
		

	  	</head>
		<s:iterator value="listadoRecuperacion">
			<row id="<s:property value="#row.index"/>">
				<cell><s:property value="numeroRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacionDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombrePrestador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="monto" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="paseAtencion" escapeHtml="false" escapeXml="true" /></cell>
				<cell>../img/b_printer.gif^Imprimir^javascript: imprimirReferenciaBancaria("<s:property value="idRecuperacion" escapeHtml="false" escapeXml="true" />")^_self</cell>	
                
            
                
			</row>
		</s:iterator>
	
</rows>
 
 
 
  
