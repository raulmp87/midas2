<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>

<script type="text/javascript">
	var mostrarDetalleInciso = '<s:url action="mostrarDetalleInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
</script>


<div id="contenido_programaspago">
	<s:form id="programasPagoForm">
		
		<s:hidden name="incisoContinuityId" id="incisoContinuityId_h"></s:hidden>
		<s:hidden name="idCotizacionSeycos" id="idCotizacionSeycos_h"></s:hidden>
		<s:hidden name="numeroPrograma" id="numeroPrograma_h"></s:hidden>
		<s:hidden name="fechaReporteSiniestro" id="fechaReporteSiniestro_h"></s:hidden>
		<s:hidden name="idReporte" id="idReporte" />
		<s:hidden name="soloConsulta" id="h_soloConsulta"/>
		
		<s:hidden name="filtroBusqueda.servicioPublico"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroSerie"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.placa"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.nombreAsegurado"></s:hidden>
		<s:hidden name="filtroBusqueda.servicioParticular"></s:hidden>
		<s:hidden name="filtroBusqueda.numeroPoliza"></s:hidden>
		<s:hidden name="filtroBusqueda.autoInciso.numeroMotor"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaIniVigencia"></s:hidden>
		<s:hidden name="filtroBusqueda.fechaFinVigencia"></s:hidden>
		<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
		<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
		<s:hidden name="fechaReporteSiniestroMillis"></s:hidden>
		
		<div class="titulo">
			<s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.recibos.tituloconsulta"/>
		</div>
	
	<div id="contenedorFiltros" style="width: 98%;">
		<table border="0">
			<tr>
				<td align="right">
				<label class="labelBlack" ><s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.poliza" /></label>
					
				</td>
				<td>
					<s:textfield name="incisoSiniestro.numeroPoliza" id="numeroPoliza_t" cssClass="cajaTexto alphaextra" size="14"></s:textfield>
				</td>
				
				<td align="right">
					<label class="labelBlack"><s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.inciso" /></label>
				</td>
				<td>
					<s:textfield name="incisoSiniestro.numeroInciso" id="numeroInciso_t" cssClass="cajaTexto alphaextra" size="10"></s:textfield>
				</td>
				
				<td align="right">
					<label class="labelBlack"><s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.contratante" /></label>
				</td>
				<td>
					<s:textfield name="incisoSiniestro.datosContratante.nombreContratante" id="contratante_t" cssClass="cajaTexto w450 alphaextra" ></s:textfield>
				</td>
				
				<td align="right">
					<label class="labelBlack"><s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.moneda" /></label>
				</td>
				<td>
					<s:textfield name="incisoSiniestro.moneda" id="moneda_t" cssClass="cajaTexto alphaextra" size="10"></s:textfield>
				</td>
			</tr>
			<tr>
				<td colspan="8">
					<div id="programasPagoGridContainer">
						<div id="programasPagoGrid" style="width:98%;height:200px;"></div>
						<div id="pagingArea"></div><div id="infoArea"></div>
					</div>
				</td>
			</tr>			
		</table>
		<table id="tablaRecibos" border="0">
			<tr>
				<td>
					<div id="tituloRecibos" class="titulo">
						<s:text name="midas.siniestros.cabina.reporte.consulta.busquedainciso.recibos"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="8">
					<div id="recibosProgramaPagoGridContainer">
						<div id="recibosProgramaPagoGrid" style="width:98%;height:200px;"></div>
						<div id="pagingAreaRecibo"></div><div id="infoAreaRecibo"></div>
					</div>
				</td>
			</tr>			
		</table>
		
	</div>
	
	<div class="btn_back w80" style="display: inline; float: right;">
                                                     <a href="javascript: void(0);" onclick="cerrarProgramasPago();"> 
                                                     <s:text name="midas.boton.cerrar" /> </a>
	</div>
		
	</s:form>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/programaspago.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<script type="text/javascript">
	jQuery(document).ready(function(){
		initConfiguration();
		initProgramasPagoGrid();
		});
</script>
</div>
