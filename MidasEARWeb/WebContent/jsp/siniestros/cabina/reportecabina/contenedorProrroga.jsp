<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<style type="text/css">

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<s:form id="contenedorProrrogaForm" >
	<s:hidden name="idToPoliza" id="h_idPoliza"></s:hidden>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.consultarProrroga.title"/>	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarProrroga.inicioProrroga"
										 name="prorroga.fechaInicio"
										 value="%{prorroga.fechaInicio}"
										labelposition="top" 
										 size="12"					
										   id="txt_inicioProrroga"
									 readonly="true"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarProrroga.terminoProrroga"
										 name="prorroga.fechaFin"
										 value="%{prorroga.fechaFin}"
										labelposition="top" 
										 size="12"					
										   id="txt_terminoProrroga"
									 readonly="true"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield jQrestrict" 
										  key="midas.consultarProrroga.diasVencer"
										 name="prorroga.diasPorVencer"
										 value="%{prorroga.diasPorVencer}"
										labelposition="top" 
										 size="12"					
										   id="txt_diasVencer"
									 readonly="true"/>
					</td>		
				</tr>
				<tr>		
					<td colspan="3">		
						<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td>
									<div class="btn_back w80" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="javascript: cerrarProrroga();"> 
										<s:text name="midas.boton.cerrar" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif' style="vertical-align: middle;"/> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
	</div>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
