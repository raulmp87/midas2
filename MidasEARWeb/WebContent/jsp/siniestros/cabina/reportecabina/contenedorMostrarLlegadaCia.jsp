<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>


<sj:head/>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
	var cargaLlegadaCiaGridPath = '<s:url action="cargaLlegadasCiaGrid" namespace="/siniestros/cabina/siniestrocabina"/>';
	var eliminarLlegadaCiaPath = '<s:url action="eliminarLlegadaCia" namespace="/siniestros/cabina/siniestrocabina"/>';

</script>

<s:form id="llegadaCiaForm" action="agregarLlegadaOtraCia" namespace="/siniestros/cabina/siniestrocabina" name="llegadaCiaForm">
	<s:hidden name="idReporteCabina" id="h_idReporteCabina"></s:hidden>
	<s:hidden name="saltaPrepare" id="h_saltaPrepare" value="true"></s:hidden>
	<s:hidden id="h_soloConsulta" name="soloConsulta"></s:hidden>
	<div class="titulo" colspan="2"><s:text name="midas.siniestros.cabina.reportecabina.llegadacia.titulo"/></div>
	<s:if test="soloConsulta != 1 ">
		<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		            <tbody>
		                  <tr>
		                  		<td>
		                  			<s:select id="s_cias"
										key="midas.siniestros.cabina.reportecabina.llegadacia.compania"
										labelposition="left" 
										name="proveedorId" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="ciaSeguros" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 requerido setNew"/>
		                  		</td>
		                  		<td>
		                  			<s:textfield id="t_orden" 
		                  				key="midas.siniestros.cabina.reportecabina.llegadacia.orden"
		                  				name="llegadaCiaSeguros.orden" 
		                  				onkeypress="return soloNumeros(this, event, true)"  
		                  				labelposition="left" 
		                  				maxlength = "3"
		                  				cssClass="cajaTextoM2 requerido setNew" />
		                  		</td>
		                  </tr>
		                  <tr>
			                  	<td colspan="2" align="right">
			                  		<div id="btn_guardar" class="btn_back w80" style="display: inline; float: right;position: relative;">
										<a href="javascript: void(0);" onclick="agregarLlegadaOtraCia()"> 
										<s:text name="midas.boton.guardar" /> </a>
									</div>
								</td>
		                 </tr>
		            </tbody>
		</table>
	</s:if>
	<br/>
	<div id="indicador"></div>
	<div id="llegadasCiaGrid"  class="dataGridConfigurationClass" style="width:98%;height:240px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br/>
	<br/>

	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="cerrarVentanaLlegadaCia()"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
		cargaLlegadaCiaGrid();
 	}
 );
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>