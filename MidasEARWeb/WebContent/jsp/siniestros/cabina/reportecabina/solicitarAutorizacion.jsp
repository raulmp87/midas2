<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/prestadorservicio/prestadorDeServicio.js'/>" type="text/javascript"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript">			
var solicitarAutorizacionPath = '<s:url action="enviarSolicitudAutorizacion" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
</script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<div id="divSolicitarAutorizacion"> 

<%-- <s:form id="formSolicitarAutorizacion" > --%>
<s:form id="formSolicitarAutorizacion" >
	<s:hidden id="tf_id_poliza" name="idToPoliza" />
	<s:hidden id="tf_id_reporte" name="idReporte" />
	<s:hidden id="tf_codigo_estatus" name="codigoEstatusPoliza" />
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
			<tr>
                  <td class="titulo" colspan="6"><s:text name="midas.solicitarAutorizacionVigencia.titulo" /></td>
            </tr>

		<tr>
			<td>
		 		<s:textfield 	id="txt_reporte"
		 						cssClass="txtfield jQrestrict cajaTextoM2 w130" 
								value="%{idReporte}"
								labelposition="right"
								label="Numero de Reporte" 
								size="10"
								readonly="true"/>
		 	</td>
			<td>
		 		<s:textfield 	id="txt_poliza"
		 						cssClass="txtfield jQrestrict cajaTextoM2 w130" 
								name="numeroPolizaFormateado"
								labelposition="right"
								label="Poliza" 
								size="10"
								readonly="true"/>
		 	</td>
		 	<td>
		 		<s:textfield 	id="txt_inciso"
		 						cssClass="txtfield jQrestrict cajaTextoM2 w130" 
								name="numeroInciso"
								labelposition="right"
								label="Inciso" 
								size="10"
								readonly="true"/>
		 	</td>
		 	<td>
		 		<s:textfield 	id="txt_estatus_poliza"
		 						cssClass="txtfield jQrestrict cajaTextoM2 w130" 
								name="estatusPoliza"
								labelposition="right"
								label="Estatus de Póliza" 
								size="10"
								readonly="true"/>
		 	</td>
		</tr>
		<tr>
			<table id="t_solicitarAutorizacion" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
								
							</td>
							<td>
							
							<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="enviarSolicitudAutorizacion();"> 
									<s:text name="midas.boton.enviar" /> </a>
							</div>	
							
							<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="salirSolicitarAutorizacion();"> 
									<s:text name="midas.boton.cerrar" /> </a>
								</div>
								
							
							</td>
						</tr>
			</table>	
		</tr>
     </table>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
</s:form>

</div>

