<%@page pageEncoding="ISO-8859-1" contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>        
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jquery.mask.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/citaReporteCabina.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>
<div id="indicador"></div>
<div id="ventanaCita" style="overflow:hidden">
	<s:form action="salvarCita" namespace="/siniestros/cabina/reportecabina" name="complementarCitaForm" id="complementarCitaForm" >
		<s:hidden name="idToReporte" id="idToReporte"></s:hidden>
		<s:hidden name="cita.id" id="cita.id"></s:hidden>
		<div class="titulo">
			<s:text name="midas.siniestros.cabina.reporte.cita.title"/>		
		</div>
			<table id ="agregar">
				<tr>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.cita"/>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.fecha"/>
					</td>
					<td>
						<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
							name="cita.fechaCita"
							id="fechaCita"			 
					   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
					   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   		onblur="esFechaValida(this);">
	 					</sj:datepicker>	
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.hora"/>
					</td>
					<td>
						<s:textfield name="cita.horaCita" id="horaCita" cssClass="cajaTextoM2 setNew" maxLength="5" onblur="actualizaValorVariableTime(this.id, this.value);">
						</s:textfield>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.termino"/>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.fecha"/>
					</td>
					<td>
						<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
							name="cita.fechaTermino"
							id="fechaTermino"				 
					   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
					   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
	 				   		onblur="esFechaValida(this);">
	 					</sj:datepicker>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.hora"/>
					</td>
					<td>
						<s:textfield name="cita.horaTermino" id="horaTermino" cssClass="cajaTextoM2 setNew" maxLength="5" onblur="actualizaValorVariableTime(this.id, this.value);">
						</s:textfield>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.asegurado"/>
					</td>
					<td colspan="9">
						<s:textfield name="cita.asegurado" id="asegurado" cssClass="cajaTextoM2 setNew" size="100" maxLength="100"></s:textfield>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.telefonos"/>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.local"/>
					</td>
					<td>
						<table>
							<tr>
								<td>
									<s:textfield name="cita.ladaTelefono" id="cita.ladaTelefono" maxlength="2" cssStyle="width: 30px;" 
										onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="3"/>
								</td>
								<td>
									<s:textfield  name="cita.telefono" id="cita.telefono" maxlength="8" 
									onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="8"/>	
								</td>
							</tr>
						</table>
					</td>
					<td>
						<s:text name="midas.siniestros.cabina.reporte.cita.celular"/>
					</td>
					<td>
						<table>
							<tr>
								<td>
									<s:textfield name="cita.ladaCelular" id="cita.ladaCelular" maxlength="2" cssStyle="width: 30px;" 
										onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="3"/>
								</td>
								<td>
									<s:textfield  name="cita.celular" id="cita.celular" maxlength="8" 
									onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="8"/>	
								</td>
							</tr>
						</table>
					</td>
				</tr>
	  		</table>
			<div id="divAgregar" style="float: right;" class="btn_back w100">
				<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('mostrarCitaRep');"> <s:text
						name="midas.boton.cerrar" /> </a>
			</div>
			
			<div id="divGuardar" style="float:right;">
				<div class="btn_back w140"  style="display:inline; float: left; ">
				   <a href="javascript: void(0);"
					  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea guardar la cita?')){guardarCita();}"> 
					  <s:text name="midas.boton.guardar" /> </a>
				</div>				
			</div>
			<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
		  	</div>
	</s:form>
</div>


<script type="text/javascript">

jQuery(window).load(
	function(){
		
		if( jQuery("#consulta").length || parent.consulta ){
			jQuery(".setNew").attr("disabled","disabled");
			jQuery(".setNew").addClass("consulta");
			jQuery("#divGuardar").remove();
			jQuery(".ui-datepicker-trigger").remove();
		}
	}
);
</script>