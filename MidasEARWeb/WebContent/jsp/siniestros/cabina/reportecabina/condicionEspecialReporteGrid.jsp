<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="condicion.id" type="ro" width="1" sort="int" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>
 		<s:if test="incisosAsociadosReporte==1">
 			<column id="asignadoReporte" type="ch" width="80" sort="int" align="center" ><s:text name="midas.condicionEspecialInciso.asignado"/></column>
 		</s:if>
		<!--><column id="condicion.nivelAplicacionStr" type="ro" width="130" sort="str" align="left"><s:text name="midas.condicionEspecialInciso.nivelImpacto"/></column></-->
		<column id="condicion.codigo" type="ro" width="100" sort="str" align="left" ><s:text name="midas.condicionEspecialInciso.codigo"/></column>
		<column id="condicion.nombre" type="ro" width="*"  sort="str" align="left" ><s:text name="midas.condicionEspecialInciso.nombre"/></column>	
		<column id="condicion.detalle" type="img" width="70"  sort="str" align="center" ><s:text name="midas.general.acciones"/></column>			
	</head>			
	<s:iterator value="condicionesEspecialesList" status="row">
		<row id="<s:property value="condicion.id"/>">
		
			<cell><s:property value="condicion.id"/></cell>	
			<s:if test="incisosAsociadosReporte==1">
				<s:if test="claveContrato">
			        <cell>1</cell>
				 </s:if> 
				 <s:else>
			         <cell>0</cell>
				 </s:else> 		
			</s:if>
			<!--><cell><s:property value="condicion.nivelAplicacionStr" escapeHtml="false" escapeXml="true"/></cell>	</-->
			<cell><s:property value="condicion.codigo" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="condicion.nombre" escapeHtml="true" escapeXml="true"/></cell>			
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Detalle^javascript:abrirVentanaDetalleCondicion("<s:property value="condicion.id"/>")^_self</cell>																		
		</row>
	</s:iterator>	
</rows>