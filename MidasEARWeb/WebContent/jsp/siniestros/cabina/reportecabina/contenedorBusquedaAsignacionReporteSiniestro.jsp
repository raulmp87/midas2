<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_splt.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>

<sj:head/>


<script	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/listadoReporteSiniestro.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>

<style type="text/css">
	label{ 10px; }
	#superior{
		height: 200px;
		width: 98%;
	}	
	#superiorI{
		height: 150px;
		width: 700px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 100px;
		width: 230px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		height: 60px;
		width: 750px;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}




.label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}

label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}

.error {
	background-color: red;
	opacity: 0.4;
}
	
.labelTelefono  {
   color:black;
   font-weight: normal;
   text-align: left;
    font-size:7pt;
    width: 88px !important;
}
	
.labelBlack{
 	color:#000000 !important;
     width: 88px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
}

.titulo{
	color:#000000 !important;
}

.mensaje_encabezado {
	background-image: url(../img/popA_head4.gif);
	background-repeat: no-repeat;
	width: 100px;
	height: 29px;
}

.mensaje_texto {
	width: 100px;
	height: 70px;
	float: right;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
	font-weight: bold;
	text-align: left;
	vertical-align: bottom;
	overflow: auto;
}

.mensaje_botones {
	text-align: right;
	clear: both;
	height: 13px;
}

.mensaje_pie {
	background-image: url(../img/popA_bottom.gif);
	background-repeat: no-repeat;
	height: 44px;
	width: 100px;
}

.mensaje_contenido {
	background-color: #FFFFFF;
	height: 100px;
	padding-top: 10px;
	padding-right: 15px;
	padding-left: 15px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-right-color: #dddddd;
	border-bottom-color: #dddddd;
	border-left-color: #dddddd;
	overflow: auto;
	width: 400px;
	z-index:1;
}

div#mensajeImg {
	background-repeat: no-repeat;
	width: 40px;
	height: 34px;
	float: left;
}

</style>

<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px;">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.listadoReporteSiniestro.busqueda.title"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="formReporteSiniestroDTO">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:select list="oficinasActivas" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.oficinaId" 
							label="Oficinas"
							cssClass="cleaneable txtfield"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						>
						</s:select>
					</div>
					<div class="floatLeft" style="width: 12%;">
						<s:textfield label="No. Siniestro" name="reporteSiniestroDTO.numeroSiniestro" 
								onBlur="validaFormatoSiniestro(this);"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 12%;">
						<s:textfield label="No. Reporte" name="reporteSiniestroDTO.numeroReporte"
								onBlur="validaFormatoSiniestro(this);"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 22%;">
						<div class="floatLeft" style="width: 60%;">
							<s:textfield label="No. Poliza" name="reporteSiniestroDTO.numeroPoliza"
									cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 40%;">
							<s:textfield label="No. Inciso" name="reporteSiniestroDTO.numeroInciso"
								onkeypress="return soloNumeros(this, event, false)"
								maxlength = "12"
								cssClass="cleaneable numeric txtfield" cssStyle="float: left;width: 70%;"></s:textfield>
						</div>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="No. Serie" maxlength="17" name="reporteSiniestroDTO.numeroSerie"
								cssClass="cleaneable txtfield"  cssStyle="float: left;width: 96%;"></s:textfield>
					</div>
				</div>
				<div id="SII">
					<div class="floatLeft" style="width: 60%;">
							<s:radio cssClass="cleaneable txtfield" name="tipoPersona"  list="#{'1':'Persona Fisica','2':'Persona Moral'}" value="1" onchange="validatePersona();" />
					</div>
					
					<div class="floatLeft" style="width: 45%;"  >
						<label id="nombre"> Contratante:</label><label id="razonSocial"> Raz&oacute;n Social:</label>
						<s:textfield  name="reporteSiniestroDTO.nombreAsegurado"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 45%;"  >
						<label> Nombre de quien Reporta: </label> 
						<s:textfield  name="reporteSiniestroDTO.nombrePersona"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
				</div>
				<div id="SIN">
					<div class="floatLeft" style="width: 45%;"  >
						<label> Nombre del Conductor: </label> 
						<s:textfield  name="reporteSiniestroDTO.nombreConductor"
								cssClass="cleaneable txtfield" cssStyle="float: left;width: 99%;" ></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="estatusReporte" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.estatus" 
							label="Estatus"
							labelposition="left"
							cssClass="cleaneable txtfield"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						>
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="tipoServicio" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.tipoServicio" 
							label="Tipo Servicio"
							labelposition="left"
							cssClass="cleaneable txtfield"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						>
						</s:select>
					</div>
					
				</div>
			</div>
			<div id="superiorD">
				<div id="SDS">
					<div style="height: 50%;width: 100%; margin-top:4%;">
						<label>Fecha de Reporte</label><br/>
					</div>
					<div style="height: 50%;width: 100%;margin-left: 10%;">
						<label>Del:</label><br/>
						<sj:datepicker name="reporteSiniestroDTO.fechaIniReporte"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../../../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraReporte"
								  maxlength="10" cssClass="txtfield cleanable"
									   size="18"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield">
						</sj:datepicker>
						<br/>
						<label>Al:</label><br/>
						<sj:datepicker name="reporteSiniestroDTO.fechaFinReporte"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="../../../img/b_calendario.gif"
								buttonImageOnly="true" 
			                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraTerminacion"
								  maxlength="10" cssClass="txtfield cleanable"
									   size="18"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield">
						</sj:datepicker>
					</div>
				</div>
			</div>
			<div id="superiorD">
				<div  >
					<div class="btn_back w100" style="display: inline; margin-left: 2%;float: left;margin-top: 28%;">
						<a href="javascript: void(0);" onclick="limpiar();"> 
							<s:text	name="midas.boton.limpiar"/> 
						</a>
					</div>
					<div class="btn_back w100" style="display: inline; margin-left: 2%;float: left;margin-top: 28%;">
						<a href="javascript: void(0);" onclick="realizarBusquedaRecuperaciones(0);"> 
							<s:text	name="midas.boton.buscar"/> 
						</a>
					</div>
				</div>
			</div>
		</form>
	</div>
	
</div>

</br>
	<div class="titulo" style="margin-left: 2%">Listado de Reportes de Siniestros</div>
	</br>
	<div id="inferior">
		<div id="indicador"></div>
		<div id="listadoReportesSiniestrosGrid" style="width:95%;height:300px;margin-left: 2%">
		</div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>

<script>
	jQuery(document).ready(function(){
	
		validatePersona();
	});
	
	function validatePersona(){
		var radioSelect 		= jQuery('input:radio[name=tipoPersona]:checked').val();
		if(radioSelect == 1){
			jQuery("#razonSocial").hide();
			jQuery("#nombre").show();
			
		}else if(radioSelect == 2){
			jQuery("#nombre").hide();
			jQuery("#razonSocial").show();
		
		}
	}
</script>
