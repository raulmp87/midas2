<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>	
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>

<s:hidden name="siniestroDTO.causaSiniestro" id="causaSiniestro"></s:hidden>
<s:hidden name="siniestroDTO.tipoResponsabilidad" id="tipoResponsabilidad"></s:hidden>
<s:hidden name="siniestroDTO.terminoAjuste" id="terminoAjuste"></s:hidden>
<s:hidden name="siniestroDTO.fugoTerceroResponsable" id="fugoTerceroResponsable"></s:hidden>
<s:hidden name="siniestroDTO.recibidaOrdenCia" id="recibidaOrdenCia"></s:hidden>
<s:hidden name="siniestroDTO.ciaSeguros" id="ciaSeguros"></s:hidden>
<s:hidden name="siniestroDTO.unidadEquipoPesado" id="unidadEquipoPesado"></s:hidden>
<s:hidden name="siniestroDTO.terminoSiniestro" id="terminoSiniestro"></s:hidden>
<s:hidden name="siniestroDTO.numValuacion" id="numValuacion"></s:hidden>
<s:hidden name="siniestroDTO.estatusValuacion" id="estatusValuacion"></s:hidden>
<s:hidden name="siniestroDTO.montoValuacion" id="montoValuacion"></s:hidden>
<s:hidden name="siniestroDTO.cargoDetalle" id="cargoDetalle"></s:hidden>
<s:hidden name="siniestroDTO.tipoPerdida" id="tipoPerdida"></s:hidden>
<s:hidden name="siniestroDTO.rfcAsegurado" id="rfcAsegurado"></s:hidden>
<s:hidden name="siniestroDTO.curpAsegurado" id="curpAsegurado"></s:hidden>
<s:hidden name="siniestroDTO.polizaCia" id="polizaCia"></s:hidden>
<s:hidden name="siniestroDTO.porcentajeParticipacion" id="porcentajeParticipacion"></s:hidden>
<s:hidden name="siniestroDTO.siniestroCia" id="siniestroCia"></s:hidden>
<s:hidden name="siniestroDTO.incisoCia" id="incisoCia"></s:hidden>
<s:hidden name="siniestroDTO.montoDanos" id="montoDanos"></s:hidden>
<s:hidden name="siniestroDTO.motivoRechazo" id="motivoRechazo"></s:hidden>
<s:hidden name="siniestroDTO.rechazoDesistimiento" id="rechazoDesistimiento"></s:hidden>
<s:hidden name="siniestroDTO.observacionRechazo" id="observacionRechazo"></s:hidden>
<s:hidden name="mostrarCoberturasAfectacion" id="h_mostrarCoberturasAfectacion"></s:hidden>
<s:hidden name="siniestroDTO.fechaExpedicion" id="fechaExpedicion"></s:hidden>
<s:hidden name="siniestroDTO.fechaDictamen" id="fechaDictamen"></s:hidden>
<s:hidden name="siniestroDTO.fechaJuicio" id="fechaJuicio"></s:hidden>
<s:hidden name="siniestroDTO.origen" id="origen"></s:hidden>
<s:hidden name="siniestroDTO.claveAmisSupervisionCampo" id="claveAmisSupervisionCampo"></s:hidden>

<table id="agregar">
	<tr>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.conductor"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.edad"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.genero"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.condMismoAseg"/>	
		</td>
		<td colspan="3">
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numOcupantes"/>	
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield id="conductor" name="siniestroDTO.conductor" 
					labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="80" size="40"/>
		</td>
		<td>
			<s:textfield id="edad" name="siniestroDTO.edad"	onkeypress="return soloNumeros(this, event, false)"
					labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="3" />
		</td>
		<td>
			<s:select id="genero"
					labelposition="left" 
					name="siniestroDTO.genero" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catGenero" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew" />
		</td>
		<td>
			<s:select id="condMismoAsegurado"
					labelposition="left" 
					name="siniestroDTO.condMismoAsegurado" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catRespuestaSimple" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew"
			  		onchange="onChangeCondMismoAsegurado(this.value)"   />
		</td>
		<td colspan="3">
			<s:textfield id="numOcupantes" name="siniestroDTO.numOcupantes" onkeypress="return soloNumeros(this, event, false)"
					labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="2" size="5" />
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.tipoLicencia"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.telefono"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.celular"/>	
		</td>
		<td colspan="2">
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.curp"/>	
		</td>
		<td colspan="2">
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.rfc"/>	
		</td>
		
	</tr>
	<tr>
		<td>
			<s:select id="tipoLicencia"
					labelposition="left" 
					name="siniestroDTO.tipoLicencia" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catTipoLicencia" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew" />
		</td>
		<td>
			<table>
				<tr>
					<td style="width: 35px;">
						<s:textfield id="ladaTelefono" name="siniestroDTO.ladaTelefono" cssStyle="width: 30px;" 
							onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="3"
							/>
					</td>
					<td>
						<s:textfield id="telefono" name="siniestroDTO.telefono"
							onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="8"
							/>	
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<td style="width: 35px;">
						<s:textfield id="ladaCelular" name="siniestroDTO.ladaCelular" cssStyle="width: 30px;" 
							onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="3"
						/>
					</td>
					<td>
						<s:textfield id="celular" name="siniestroDTO.celular"
							onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 setNew" maxLength="8"
						/>	
					</td>
				</tr>
			</table>
		</td>
		<td>
			<s:textfield id="curp" name="siniestroDTO.curp"
					labelposition="left" cssClass="cajaTextoM2 setNew jQCURP" maxLength="18" size="27"/>
		</td>
		<td align="left">
			<div class="btn_back w40" style="display:table-cell; float: right !important;" id="btn_curp">
				<a href="javascript: void(0);" onclick="window.open('http://consultas.curp.gob.mx/CurpSP/', 'CURP');">
					<s:text name="midas.boton.buscar" /> 
				</a>
			</div>
		</td>
		<td>
			<s:textfield id="rfc" name="siniestroDTO.rfc"
					labelposition="left" cssClass="cajaTextoM2 setNew jQRFC-NoHomoclave" maxLength="13" size="17"/>
		</td>
		<td align="left">
			<div class="btn_back w40" style="display: inline; float: left !important;" id="btn_rfc">
				<a href="javascript: void(0);" onclick="window.open('http://giro.com.mx/?page_id=72', 'RFC');">
					<s:text name="midas.boton.buscar" /> 
				</a>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.placas"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.color"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.importeEstimado"/>	
		</td>
		<td>
			<s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.importeReserva"/>	
		</td>
		<td colspan="3">&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<s:textfield id="txt_placas" name="siniestroDTO.placas" 
					labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="8"/>
		</td>
		<td>
			<s:select id="color"
					labelposition="left" 
					name="siniestroDTO.color" 
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
			  		list="catColor" listKey="key" listValue="value"  
			  		cssClass="txtfield cajaTextoM2 requerido setNew" />
		</td>
		<td>
			<s:textfield id="siniestroDTO.importeEstimado" 
					value="%{getText('struts.money.format',{siniestroDTO.importeEstimado})}"
					onkeypress="return soloNumeros(this, event, false)" labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="8"
					readonly="true" disabled="true"/>
		</td>
		<td>
			<s:textfield id="siniestroDTO.importeReserva" 
					onkeypress="return soloNumeros(this, event, false)" labelposition="left" cssClass="cajaTextoM2 setNew" maxLength="8"
					value="%{getText('struts.money.format',{siniestroDTO.importeReserva})}"
					readonly="true" disabled="true"/>
		</td>
		<td colspan="3">
			<s:checkbox id = "siniestroDTO.verificado"  name="siniestroDTO.verificado" 
				key="midas.siniestros.cabina.reportecabina.afectacionInciso.verificado" cssClass="setNew" disabled="true"/>
		</td>
	</tr>
</table>
<script type="text/javascript">
	dhx_init_tabbars();
	jQuery(document).ready(function(){
		if( jQuery("#h_mostrarCoberturasAfectacion").val() == 1 ){
			jQuery("#seccionCoberturasReporte").show();
			mostrarCoberturasAfectacionInciso();
		}else{
			jQuery("#seccionCoberturasReporte").hide();
		}
	});
</script>