<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
	
<s:include value="/jsp/siniestros/cabina/reportecabina/referenciasBancariasHeader.jsp"></s:include>

<s:form action="listarFiltrado" id="referenciasBancariasForm" name="referenciasBancariasForm">
	<s:hidden id="id" name="entidad.id"/>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.recuperacion.listado.recuperaciones.consultaReferencias"/>	
	</div>
	
	<div id="indicador"></div>
	<div id="referenciasBancariasGrid" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
	
	<div class="w150" style="float: right;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);"
				onclick="regresarReporteBusqueda()"> 
				<s:text name="midas.boton.regresar" /> 
			</a>
		</div>
	</div>
</s:form>

<script type="text/javascript">
	jQuery(document).ready(function(){
		iniciarGrid();
	});
</script>

