<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="fechaEmisionEndoso"  type="ro" width="160" sort="date_custom" hidden="false" format="%d/%m/%Y" > <s:text name="midas.consulta.endoso.inciso.fecha.aplicacion"/> </column>
        <column id="fechaInicioVigencia" type="ro" width="160" sort="date_custom" hidden="false" format="%d/%m/%Y" > <s:text name="midas.consulta.endoso.inciso.inicio.vigencia"/> </column>
        <column id="tipo"                type="ro" width="70"  sort="str"  hidden="false"> 							 <s:text name="midas.consulta.endoso.inciso.tipo"/> </column>
        <column id="numero"              type="ro" width="70"  sort="int"  hidden="false">							 <s:text name="midas.consulta.endoso.inciso.numero"/></column>
        <column id="afectacion"          type="ro" width="*"   sort="str"  hidden="false">							 <s:text name="midas.consulta.endoso.inciso.afectacion"/></column>
		<column id="primaEndoso"         type="ron" width="*"  sort="int"  hidden="false" format="$0,000.00" >		 <s:text name="midas.consulta.endoso.inciso.prima.total"/></column>
		<column id="situacion"           type="ro" width="*"   sort="str"  hidden="false">					         <s:text name="midas.consulta.endoso.inciso.situacion"/></column>
		<column id="motivo"              type="ro" width="*"   sort="str"  hidden="false">							 <s:text name="midas.consulta.endoso.inciso.motivo"/></column>
		<column id="descripcion"         type="ro" width="*"   sort="str"  hidden="true">descripcion</column>
	</head>
	
	<s:iterator value="endososList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:date name="fechaEmisionEndoso" format="dd/MM/yyyy" /></cell>
			<cell><s:date name="fechaInicioVigencia" format="dd/MM/yyyy" /></cell>
			<cell><s:property value="descripcionTipoEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroEndoso"          escapeHtml="false" escapeXml="true"/></cell>	
			<cell>
			    <s:if test="idTipoEndoso== 5">ALTA DE INCISO</s:if><s:if test="idTipoEndoso== 6">BAJA DE INCISO</s:if><s:if test="idTipoEndoso== 7">CAMBIO DE DATOS</s:if><s:if test="idTipoEndoso== 8">CAMBIO DE AGENTE</s:if><s:if test="idTipoEndoso== 9">CAMBIO DE FORMA PAGO</s:if><s:if test="idTipoEndoso== 10">CANCELACION DE ENDOSO</s:if><s:if test="idTipoEndoso== 11">CANCELACION DE POLIZA</s:if><s:if test="idTipoEndoso== 12">EXTENSION DE VIGENCIA</s:if><s:if test="idTipoEndoso== 13">INCLUSION DE ANEXO</s:if><s:if test="idTipoEndoso== 14">INCLUSION DE TEXTO</s:if><s:if test="idTipoEndoso== 15">DE MOVIMIENTOS</s:if><s:if test="idTipoEndoso== 16">REHABILITACION DE INCISOS</s:if><s:if test="idTipoEndoso== 17">REHABILITACION DE ENDOSO DE CANCELACION DE ENDOSO</s:if><s:if test="idTipoEndoso== 18">REHABILITACION DE POLIZA</s:if><s:if test="idTipoEndoso== 19">AJUSTE DE PRIMA</s:if><s:if test="idTipoEndoso== 20">EXCLUSION DE TEXTO</s:if><s:if test="idTipoEndoso== 21">CAMBIO DE IVA</s:if>				
			</cell>
			<cell><s:property value="primaEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell>Emitida</cell>
			<cell><s:property value="descripcionMotivoEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionMovimiento" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>

</rows>