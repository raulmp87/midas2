<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>
<rows>

<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<afterInit>
			<call command="splitAt"><param>5</param></call>
		</afterInit>
		
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.negocio.oficina" />   </column>
		<column  type="ro"  width="100"  align="center" sort="date_custom"> <s:text name="midas.siniestros.cabina.reporte.cita.fecha" />  </column>
 		<column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.numeroSiniestro" />  </column>
		<column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.numeroReporte" />  </column>
	    <column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.numeroPoliza" />  </column>
	    <column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.general.estatus" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.catalogos.fuerzaventa.nombreORazonSocial" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.nombreQuienReporta" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.finiquito.conductor" />  </column>
	    <column  type="ro"  width="100"  align="center" sort="str"> <s:text name="midas.poliza.numeroSerie" />  </column>
	    <column  type="ro"  width="100"  align="center" hidden="true"> #ReporteId  </column>
	</head>
	
	<s:iterator value="reportesSiniestro" var="index">
		<row>			
		    <cell><s:property value="nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAsegurado" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombrePersona" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreConductor" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/> </cell>	
		    <cell><s:property value="reporteCabinaId" escapeHtml="false" escapeXml="true"/> </cell>	
		</row>
	</s:iterator>
</rows>