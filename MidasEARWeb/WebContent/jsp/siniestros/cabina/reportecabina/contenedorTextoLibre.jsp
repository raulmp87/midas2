<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript"
	src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"
	charset="ISO-8859-1"></script>

<script type="text/javascript"	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/contenedorTextoLibre.js'/>"></script>

<div id="contenedorTextArea">
	<s:textarea cssClass="textoLibre" name="valueTextoLibre" cssStyle="height: 80%;width: 100%;font-size: 10pt;"/>
</div>

		
	<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
					<a href="javascript: void(0);" onclick="cerrar();"> 
					<s:text name="midas.boton.cerrar" /> </a>
				</div>	
				<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: guardarTextoLibre();"> 
					<s:text name="midas.boton.guardar" /> </a>
				</div>		
			</td>							
		</tr>
	</table>				
