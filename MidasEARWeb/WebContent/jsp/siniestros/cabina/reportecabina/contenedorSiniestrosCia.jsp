<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>


<sj:head/>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/afectacionSiniestroIncisoReporte.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
	var buscarSiniestrosCiaGridPath = '<s:url action="buscarSiniestrosCia" namespace="/siniestros/cabina/siniestrocabina"/>';
	var editarSiniestrosCiaGridPath = '<s:url action="editarSiniestroCia" namespace="/siniestros/cabina/siniestrocabina"/>';

</script>

<s:form id="siniestrosCiaForm" action="guardarSiniestrosCia" namespace="/siniestros/cabina/siniestrocabina" name="siniestrosCiaForm">
	<s:hidden name="idReporteCabina" id="h_idReporteCabina"></s:hidden>
	<s:hidden name="idCompaniaSiniestro" id="h_idCompaniaSiniestro"></s:hidden>
	<s:hidden name="saltaPrepare" id="h_saltaPrepare" value="true"></s:hidden>
	<s:hidden id="h_soloConsulta" name="soloConsulta"></s:hidden>
	<div class="titulo" colspan="2"><s:text name="midas.siniestros.companias.participacion.titulo"/></div>
	<s:if test="soloConsulta != 1 ">
		<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		            <tbody>
		                  <tr>
		                  		<td>
		                  			<s:select id="s_cias"
										key="midas.siniestros.cabina.reportecabina.llegadacia.compania"
										labelposition="left" 
										name="companiaSiniestro.compania.id" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="ciaSeguros" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 requerido setNew w100"/>
		                  		</td>
		                  		<td>
		                  			<s:select id="s_coberturas"
										key="midas.general.cobertura"
										labelposition="left" 
										name="companiaSiniestro.coberturaId" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="mapCoberturasAfectables" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 requerido setNew w100"/>
		                  		</td>
		                  		
		                  		<td>
		                  			<s:textfield id="t_folio" 
		                  				key="midas.siniestros.companias.participacion.folioCia"
		                  				name="companiaSiniestro.folioCia" 
		                  				labelposition="left" 
		                  				maxlength = "20"
		                  				cssClass="cajaTextoM2 requerido setNew jQalphabeticExt" />
		                  		</td>
		                  		
		                  		<td>
		                  			<s:textfield id="t_numeroSin" 
		                  				key="midas.siniestros.companias.participacion.siniestro"
		                  				name="companiaSiniestro.numeroSiniestro" 
		                  				labelposition="left" 
		                  				maxlength = "50"
		                  				cssClass="cajaTextoM2 requerido setNew jQalphabeticExt" />
		                  		</td>
		                  		
		                  		
		                  		<td>
		                  			<s:textfield id="t_porcentaje" 
		                  				key="midas.siniestros.companias.participacion.porcentaje"
		                  				name="companiaSiniestro.porcentaje" 
		                  				labelposition="left" 
		                  				maxlength = "3"
		                  				cssClass="cajaTextoM2 requerido setNew jQnumeric" />
		                  		</td>
		                  </tr>
		                  <tr>
			                  	<td colspan="2" align="right">
			                  		<div id="btn_guardar" class="btn_back w80" style="display: inline; float: right;position: relative;">
										<a href="javascript: void(0);" onclick="guardarSiniestrosCia()"> 
										<s:text name="midas.boton.guardar" /> </a>
									</div>
								</td>
		                 </tr>
		            </tbody>
		</table>
	</s:if>
	<br/>
	<div id="indicador"></div>
	<div id="siniestrosCiaGrid"  class="dataGridConfigurationClass" style="width:98%;height:350px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br/>
	<br/>

	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="cerrarVentanaSiniestrosCia()"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
		buscarSiniestrosCiaGrid();
 	}
 );
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>