<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/bitacoraEventoSiniestro.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<style type="text/css">


table tr td div span label {
	color: black;
	font-weight: normal;
	text-align: left;
}

table#filtrosM2 {
	background-color: #FFFFFF;
	width: 100%;
}

.centro {
	margin-left: 5%;
}
</style>

<script type="text/javascript">
	var buscarEventosPath = '<s:url action="buscarEventos" namespace="/siniestros/cabina/reporteCabina/bitacoraEvento"/>';
	var cargaComboEventoPath = '<s:url action="cargaComboEvento" namespace="/siniestros/cabina/reporteCabina/bitacoraEvento"/>';
	var excelPath = '<s:url action="exportar" namespace="/siniestros/cabina/reporteCabina/bitacoraEvento"/>';
</script>

<div id="spacer1" style="height: 10px"></div>

<s:form id="contenedorForm" name="contenedorForm">
	<s:hidden id="reporteCabinaId" name="reporteCabinaId" />
	<s:hidden name="soloConsulta" id="h_soloConsulta"/>
	<div align="center">

		<div class="titulo" align="left">
			<s:text name="midas.servicio.siniestros.bitacoraEvento.titulo" />
		</div>
		<div>
			<table width="800px" id="filtrosM2" cellpadding="2" cellspacing="1">
				<tr class="pf">
					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.numeroReporte" />
					</th>
					<td width="300px" class="centro"><s:textfield id="noReporte"
							name="numeroReporte"
							cssClass="jQalphanumeric jQrestrict cajaTextoM2 w150"
							maxlength="30;" readonly="true" /></td>

					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.fechaD" />
					</th>

					<td><sj:datepicker name="filtroEvento.fechaInicio"
							labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif"
							id="txtFechaInicio"
							value="%{filtroEvento.fechaInicio}" maxlength="10"
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker></td>

					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.usuario" />
					</th>
					<td width="315px"><s:textfield id="nombreUsu"
							name="filtroEvento.nombreUsuario"
							cssClass="jQalphanumeric cajaTextoM2 w200" maxlength="50"
							onblur="validaNombre(this);" />
					</td>
					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.modulo" />
					</th>
					<td><s:select id="s_modulo" labelposition="left"
							name="filtroEvento.modulo" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							list="tiposModulo" listKey="key" listValue="value"
							cssClass="txtfield" onchange="onChangeModulo()" />
					</td>
				</tr>
				<tr class="pf">

					<th width="105px"></th>
					<td width="315px"></td>

					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.fechaA" />
					</th>
					<td width="315px"><sj:datepicker name="filtroEvento.fechaFin"
							labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif"
							id="txtFechaFin"
							value="%{filtroEvento.fechaInicio}" maxlength="10"
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker></td>	

					<th width="105px"><s:text
							name="midas.servicio.siniestros.bitacoraEvento.evento" />
					</th>
					<td><s:select id="s_evento" labelposition="left"
							name="filtroEvento.evento" headerKey="" list="tiposEvento"
							listKey="key" listValue="value" cssClass="txtfield" />
					</td>
					<th width="105px"></th>
					<td width="105px" colspan="2" align="right">
					</td>
				</tr>
				
				<tr>
					<td colspan="8">
						<div class="btn_back w110 alinearBotonALaDerecha" style="display: inline-block;">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="buscarListaEventos( );">
								<s:text name="midas.boton.buscar" /> </a>
						</div>
					</td>
				</tr>

			</table>
			<br>
		</div>

		<div class="titulo" align="left">
			<s:text name="midas.servicio.siniestros.bitacoraEvento.lista" />
		</div>
		<div id="indicadorBitacora" align="left"></div>
		<div id="eventoGrid" class="dataGridConfigurationClass"
			style="width: 99%; height: 230px;"></div>
		<div class="titulo" align="left">
			<s:text name="Observaciónes" />
		</div>	
		<s:textarea cssStyle="width:99%;height:100px;" id="observacion"
			rows="10" name="filtroEvento.observaciones" readonly="true"
			cssClass="textarea" />
		<div id="pagingArea"></div>
		<div id="infoArea"></div>
	</div>
</s:form>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
		<div class="btn_back w120"  style="display: inline; float: right;  padding-left: 10pxz" id="b_regresar">
				<a href="javascript: void(0);" onclick="cerrar();"> 
				<s:text name="midas.boton.cerrar" /> </a>
			</div>	
			<!-- Cuando se avilite la visibilidad del boton...poner arriba para que tenga la posicion como los demas catalogos -->
			<div class="btn_back w130" style="display: inline; float: right; padding-left: 10px"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px'  src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			
		</td>
	</tr>
</table>
<script>
	jQuery(document).ready(function() {
		iniContenedorGrid();
	});
</script>

<div id="spacer2" style="height: 40px"></div>


