<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head> 
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>       
		<column id="fechaHora" type="ro" width="250" sort="str" >Fecha/Hora</column>
		<column id="evento" type="ro" width="350" sort="str">Evento</column>
		<column id="modulo" type="ro" width="250" sort="str">Modulo</column>
		<column id="nombreUsuario" type="ro" width="350" sort="str">Evento Generado por</column>	
		<column id="observaciones" type="ro" width="350" sort="str" hidden="true">Evento Generado por</column>	
	</head>
	
	<s:iterator value="bitacoraEventos" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="fechaHora" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="evento" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="modulo" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="nombreUsuario" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="observaciones" escapeHtml="true" escapeXml="true"/></cell>	
		</row>
	</s:iterator>
	
</rows>