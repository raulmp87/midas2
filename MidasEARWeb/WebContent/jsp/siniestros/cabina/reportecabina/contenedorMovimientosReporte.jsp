<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>

<script type="text/javascript">
var mostrarHistoricoMovimientosPath = '<s:url action="mostrar" namespace="/siniestros/cabina/reportecabina/historicomovimientos"/>';
var buscarHistoricoMovimientosPath = '<s:url action="buscar" namespace="/siniestros/cabina/reportecabina/historicomovimientos"/>';
var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
var regresarOrdenPago = '<s:url action="mostrar" namespace="/siniestros/pagos/pagosSinestro"/>';
</script>

<s:hidden id="idToReporte" name="idToReporte"/>
<s:hidden id="h_soloConsulta" name="soloConsulta"/>
<s:hidden id="pantallaOrigen" name="pantallaOrigen"/>
<s:hidden id="ingresoId" name="ingresoId" />
<s:hidden id="accion" name="accion"/>
<s:hidden id="idOrdenCompra" name="idOrdenCompra"/>
<s:hidden id="idOrdenPago" name="idOrdenPago"/>
<s:hidden id="tipoDeCancelacion" name="tipoDeCancelacion" />
	

<div id="contenido_Pestañas" style="width:99%;position: relative;">
	<div id="divInferior" style="width: 99% !important;" class="floatLeft">
		<div hrefmode="ajax-html" style="height: 680px; width: 100%;" id="historicoMovimientosTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="white,white">	
			<div width="200px" id="afectacionReserva" name="<s:text name='midas.siniestros.cabina.reportecabina.historicomovimienots.afectacionreserva' />" 
				href="http://void" extraAction="javascript: verTabAfectacionReserva();">
			</div>
			<div width="200px" id="gastosAjuste" name="<s:text name='midas.siniestros.cabina.reportecabina.historicomovimienots.gastosajuste' />" 
				href="http://void" extraAction="javascript: verTabGastosAjuste();">
			</div>
		</div>
	</div>
</div>

<script src="<s:url value='/js/midas2/siniestros/pagos/ordenPago.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/ingresos.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/historicoMovimientosReporte.js'/>"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		dhx_init_tabbars();
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>