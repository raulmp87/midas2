<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<style>
.googleStyle {
	direction: ltr;
	overflow: hidden;
	text-align: left;
	position: relative;
	color: rgb(0, 0, 0);
	font-family: Roboto, Arial, sans-serif;
	-moz-user-select: none;
	padding: 1px 6px;
	border-bottom-left-radius: 2px;
	border-top-left-radius: 2px;
	background-clip: padding-box;
	border: 1px solid rgba(0, 0, 0, 0.15);
	box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3);
	min-width: 30px;
	font-weight: 500;
}

.ajustadorItem{
	width: 225px;
	height: 120px;
	position:relative;
	float:left;
	margin-top: 5px;
	font-size: 8pt;
	background-color: white !important;
}

label[for="direccionField"]{
	font-size:10pt;
} 
</style>

<s:hidden id="mostrarInformacionReporte" name="mostrarInformacionReporte"></s:hidden>
<s:hidden id="mostrarListadoAjustadores" name="mostrarListadoAjustadores"></s:hidden>
<div id="contenido_maps"
	style="margin-left: 8px; margin-right: 1px; height: 93%; width: 98%;">
	<div id="gMapaAjustador"
		style="width: 100%; height: 100%; margin-left: 1px; margin-right: 1px; position: relative; text-align: center; float: left;">
		<s:if test="mostrarOficinas == true">
			<div id="oficinas" class="googleStyle" style="position: absolute !important; margin-top: 10px; font-size: 13px !important; width: 430px; margin-left: 100px; z-index: 999; background-color: FFFFFF; border: 1px solid cccccc; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3);">											
				<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
					<img alt="Refrescar" title="Refrescar" src="../img/maps/home_L_8x.png" onclick="refrescar(jQuery('#ofinasActivas').val());">		
				</div>
				<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
					<img alt="Ayuda" title="Ayuda" src="../img/maps/question.png" onclick="showMonitorHelp();">		
				</div>	
				<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
					<img alt="Lineas ajustador - reportes" title="Lineas ajustador - reportes" src="../img/maps/lines.png" onclick="activarDesactivarWayPoint();">		
				</div>	
				<m:tienePermiso nombre="FN_M2_SN_Eliminar_Historico_Posicion">
				<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
					<img alt="Eliminar historial mes anterior" title="Eliminar historial mes anterior" src="../img/maps/eliminar.png" onclick="eliminarHistorial();">		
				</div>	
				</m:tienePermiso>
				<div style="padding-top:4px; display: inline; float: right; vertical-align: middle; position: relative; margin-top: 4px; margin-right: 10px;">
					<img alt="Monitor cada 30 seg" title="Monitor cada 30 seg" width="40px;" height="15px;" src="../img/maps/off.png"  id="accionMonitor" onclick="monitorear(this)";">		
				</div>
				<s:select id="ofinasActivas" list="oficinasActivas" name="oficinaId"
					cssClass="googleStyle" label="Oficinas" labelposition="left"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					cssStyle="width: 180px;">
				</s:select>		
			</div>
			<div id="infoAlertContenedor" style="height: 80px;width: 240px; right:110px; float: left;position:absolute; z-index:999; display:none;" >
				<div id="infoAlert" class="googleStyle" style="background-color:#eaeaea;float:left;height:99%;position:relative;"></div>				
			</div>			
		</s:if>
		<div id="contenido_maps2" style="margin-left: 8px; margin-right: 1px; height: 540px; width: 100%;">
			<div style="height: 540px;width: 850px;float: left;position: relative;" >
				<div id="map" style="width:905px;height:540px;position: relative;background-color:#eaeaea;"></div>	
			</div>
			<div style="height: 540px;width: 250px;;float: left;position: relative;" >
				<div id="listadoAjustadores" class="googleStyle" style="background-color:#eaeaea;float:left;width:275px;height:99%;position:relative;overflow-y: scroll;"></div>
			</div>
		</div>							
	</div>
	<s:if test="mostrarInformacionReporte == true">
		<div class="googleStyle" style="position: absolute !important;background-color:white !important; margin-top: 445px;margin-left: 100px;z-index: 999;">
			<s:textfield id="direccionField" label="Dirección siniestro" labelposition="left" name="direccionSiniestro" readonly="true" cssStyle="width: 570px;" cssClass="googleStyle" ></s:textfield>
		</div>
	</s:if>
</div>
<script type="text/javascript" src="<s:url value='/js/midas2/google/maps/generalFunctions.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/ajustadoresSiniestro.js'/>"></script>
<script type="text/javascript">
	jQuery(document).ready(
			function(){
				blockPage();
				var script = document.createElement('script');
				script.type = 'text/javascript';
				script.src = 'http://maps.googleapis.com/maps/api/js?sensor=false&client=gme-segurosafirmesade&callback=initialize&libraries=places';
				document.body.appendChild(script);								
				jQuery("#listadoAjustadores").html(setListadoVacio());		
				unblockPage();
			}
		);
</script>


