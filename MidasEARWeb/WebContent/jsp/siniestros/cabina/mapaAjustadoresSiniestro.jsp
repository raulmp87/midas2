<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&client=gme-segurosafirmesade"></script>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/google/maps/gMaps.js?sensor=false'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script> --%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style>
.googleStyle {
	direction: ltr;
	overflow: hidden;
	text-align: left;
	position: relative;
	color: rgb(0, 0, 0);
	font-family: Roboto, Arial, sans-serif;
	-moz-user-select: none;
	padding: 1px 6px;
	border-bottom-left-radius: 2px;
	border-top-left-radius: 2px;
	background-clip: padding-box;
	border: 1px solid rgba(0, 0, 0, 0.15);
	box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3);
	min-width: 30px;
	font-weight: 500;
}

.ajustadorItem{
	width: 240px;
	height: 165px;
	position:relative;
	float:left;
	margin-top: 5px;
	font-size: 8pt;
	background-color: white !important;
}

label[for="direccionField"]{
	font-size:10pt;
} 
</style>
<s:form name="mostrarMapaAjustadoresSiniestro" id="mostrarMapaAjustadoresSiniestro">
<s:hidden id="mostrarInformacionReporte" name="mostrarInformacionReporte"></s:hidden>
<s:hidden id="mostrarListadoAjustadores" name="mostrarListadoAjustadores"></s:hidden>

<s:hidden id="idToReporte" name="idToReporte"/>
<s:hidden id="ajustadorAsignado" name="reporte.ajustador.id"/>
<s:hidden id="tipoLugar" name="tipoLugar"/>
<s:hidden id="tituloVentana" name="tituloVentana"/>
<s:hidden id="nombreReporta" name="nombreReporta"/>
<s:hidden id="telLadaReporta" name="telLadaReporta"/>
<s:hidden id="telefonoReporta" name="telefonoReporta"/>

<s:hidden id="latitud" name="latitud"/>
<s:hidden id="longitud" name="longitud"/>
<s:hidden id="isConfirmarUbicacion" name="isConfirmarUbicacion"/>
<div id="slider"></div>
<div id="oficinas" class="googleStyle" style="position: absolute !important; margin-top: 10px; font-size: 13px !important; width: 310px; margin-left: 100px; z-index: 999; background-color: FFFFFF; border: 1px solid cccccc; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3);">
	<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
		<img alt="Refrescar" title="Refrescar" src="/MidasWeb/img/maps/home_L_8x.png" onclick="refrescar(jQuery('#idToReporte').val());">		
	</div>
	<div style="display: inline; float: right; vertical-align: middle; position: relative; margin-top: 3px; margin-right: 10px;">
		<img alt="Ayuda" title="Ayuda" src="/MidasWeb/img/maps/question.png" onclick="showMonitorHelp();">		
	</div>	
	<s:select id="ofinasActivas" list="oficinasActivas" name="oficinaId"
		cssClass="googleStyle" label="Oficinas" labelposition="left"
		headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		cssStyle="width: 180px;" disabled="true">
	</s:select>		
</div>
<div class="googleStyle" style="position: absolute !important;background-color:white !important; margin-top: 485px;margin-left: 100px;z-index: 999;">
	<s:textfield id="direccionField" label="Dirección siniestro" labelposition="left" name="direccionSiniestro" readonly="true" cssStyle="width: 570px;" cssClass="googleStyle" ></s:textfield>
</div>
<div id="contenido_maps" style="margin-left: 8px; margin-right: 1px; height: 540px; width: 1210px;">
	<div style="height: 540px;width: 910px;float: left;position: relative;" >
		<div id="map" style="width:905px;height:540px;position: relative;background-color:#eaeaea;"></div>	
	</div>
	<div style="height: 540px;width: 300px;;float: left;position: relative;" >
		<div id="listadoAjustadores" class="googleStyle" style="background-color:#eaeaea;float:left;width:275px;height:99%;position:relative;overflow-y: scroll;"></div>
	</div>
</div>
</s:form>
<script type="text/javascript" src="<s:url value='/js/midas2/google/maps/generalFunctions.js'/>"></script>
<script type="text/javascript" ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/mapaAjustadoresSiniestro.js'/>"></script>