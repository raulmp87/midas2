<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!--  eftregue -->
<script	src="<s:url value='/js/midas2/siniestros/cabina/pasesatencion/listadoPaseAtencion.js'/>"></script>
<s:hidden id="urlRedirect" ></s:hidden>
<s:hidden name="isListadoPases" id="h_isListadoPases"></s:hidden>

<script type="text/javascript">

	var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var obtenerListadoPasesPath = '<s:url action="obtenerListadoPases" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var mostrarPasePath = '<s:url action="mostrarPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';

</script>



<div id="contenido_listadoPasesAtencion">
	<div id="divSuperior" >
		<form id="formPasesAtencion">
			<s:hidden name="paseAtencion.idReporteCabina" id="h_idReporte"></s:hidden>
			
		</form>
	</div>
	<br>
	
</div>
<div id="divGrid" >
		<div class="titulo"><s:text name="midas.siniestros.cabina.reportecabina.pasesAtencion.listadoPasesAtencion"></s:text></div>
		<br>
		<div id="indicadorListadoPase"></div>
		<div id="siniestrosVehiculoGrid" style="width: 95%; height: 350px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>	


<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td width="90%">
					<div id="btn_exportar" class="btn_back w110"  style="display:inline; float: right; ">
						            <a href="javascript: void(0);" onclick="exportarExcel();">
						            <s:text name="midas.boton.exportarExcel" /> </a>
								</div>
				</td>
				<td>
					<div id="btn_exportar" class="btn_back w110"  style="display:inline; float: right;">
				            <a href="javascript: void(0);" onclick="regresaAListadoDeReporte();">
				            <s:text name="midas.boton.cerrar" /> </a>
						</div>
				</td>
			</tr>
		</tbody>
	</table>		

<script type="text/javascript">
jQuery(document).ready(
		function(){
			vieneDeReportes = true;
			buscarPaseAtencion(); 
		}
);
</script>
