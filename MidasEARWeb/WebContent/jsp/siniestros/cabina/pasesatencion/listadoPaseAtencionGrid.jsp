<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows>
<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column  type="ro"  width="150"  align="center" sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.numeroReporte" /></column>
		<column  type="ro"  width="150"  align="center" sort="str"><s:text name="midas.listadoSiniestrosAction.titulo.numeroSiniestro" /></column>
		<column  type="ro"  width="100"  align="center" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.numeroFolio" /></column>
		<column  type="ro"  width="200"  align="center" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.tipoPaseAtencion" /></column>
		<column  type="ro"  width="*"  align="center" sort="str"><s:text name="midas.general.cobertura" /></column>	    
	    <column  type="ro"  width="150"  align="center" sort="date_custom"><s:text name="midas.negocio.fechaCreacion" /></column>
	    <column  type="img"  width="35"  align="center" ><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.envioHGS"/></column>
	    <column  type="img"  width="30"  align="center" ></column>
	    <column  type="img"  width="30"  align="center" ></column>
	    <column  type="img"  width="30"  align="center" ></column>
		<column  type="img"  width="30"  align="center" ></column>
		
	</head>
	<s:set var="noPermiteAfectar" value="false" />				
	<m:tienePermiso nombre="FN_M2_SN_No_Permite_Afectacion_Reserva">		
		<s:set var="noPermiteAfectar" value="true" />
	</m:tienePermiso>
	<s:iterator value="pasesAtencion">
		<s:set var="editable" value="true" />	
		<s:set var="estatusReporte" value="estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.estatus" />		
		<s:if test="%{numeroSiniestro != null && numeroSiniestro != '--' && #noPermiteAfectar}">			
			<s:set var="editable" value="false" />	
		</s:if>
		<row>
			<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estimacionCoberturaReporte.folio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoPaseAtencionDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaInicial" escapeHtml="false" escapeXml="true"/></cell>

		    <s:if test="AplicaHGS">
		    	<s:if test="fueEnviadoAHGS">
					<cell>/MidasWeb/img/ico_green.gif^Enviado</cell>
				</s:if>
				<s:else>
					<s:if test=" estimacionCoberturaReporte.tipoPaseAtencion != 'SRE' && estimacionCoberturaReporte.tipoPaseAtencion != 'RAC'  ">
						<cell>/MidasWeb/img/ico_red.gif^No Enviado</cell>
					</s:if>
					<s:else>
						<cell>../img/pixel.gif^Solo Registro y Reembolso a Compania no se derivan</cell>
					</s:else>
				</s:else>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript: mostrarEstimacion(1,"<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.tipoEstimacion" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			<s:if test="%{estatus != null && estatus != '2' && estimacionCoberturaReporte.esEditable == 'EDT' && #editable && #estatusReporte != 2 && #estatusReporte != 4 && #estatusReporte != 5}">			
				<cell>../img/icons/ico_editar.gif^Editar^javascript: mostrarEstimacion(0,"<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.id" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.claveTipoCalculo" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.tipoEstimacion" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" escapeHtml="false" escapeXml="true"/>","<s:property value="estimacionCoberturaReporte.id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
			<s:if test="puedeImprimir == true">
				<cell>../img/b_printer.gif^Imprimir^javascript: imprimirPaseAtencion("<s:property value="estimacionCoberturaReporte.id" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif</cell>	
			</s:else>
		</row>
	</s:iterator>
</rows>