<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!--  eftregue -->
<script	src="<s:url value='/js/midas2/siniestros/cabina/pasesatencion/listadoPaseAtencion.js'/>"></script>
<s:hidden id="urlRedirect" ></s:hidden>
<s:hidden name="isListadoPases" id="h_isListadoPases"></s:hidden>

<script type="text/javascript">

	var mostrarEstimacionPath = '<s:url action="mostrarEstimacion" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var obtenerListadoPasesPath = '<s:url action="obtenerListadoPases" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var crearNuevoPasePath = '<s:url action="crearNuevoPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';
	var mostrarPasePath = '<s:url action="mostrarPase" namespace="/siniestros/cabina/reporteCabina/estimacioncobertura"/>';

</script>

<style>
	div.ui-datepicker {
		font-size: 10px;
	}
	#divSuperior{
		width: 1140px;
		height: 200px;
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	#divInferior{
		width: 1140px;
		height: 280px;
	}

	.divSup{
		height: 100px;
		width: 320px;
		float: left;
		position: relative;
		margin-left: 20px;
	}
	.campo{
		height:60px;	
		width: 378px;
	}
</style>

<div id="contenido_listadoPasesAtencion" style="margin-left: 2% !important;height: 500px;">
	<div class="titulo"><s:text name="midas.siniestros.cabina.reportecabina.pasesAtencion.busquedaPasesAtencion"></s:text></div>
	<div id="divSuperior" >
		<form id="formPasesAtencion">
			<div class="divSup" >
				<div class="campo">
						<s:textfield maxlength="15" label="Número de Reporte" id="txfNumRep" name="paseAtencion.numeroReporte" cssClass="cleaneable txtfield" ></s:textfield>
				</div>
				<div class="campo">
					<s:select list="tiposPaseAtencion" id="tipoPasesAtencion"
						name="paseAtencion.estimacionCoberturaReporte.tipoPaseAtencion" 
						label="*Tipo Pase de Atención"
						cssClass="cleaneable txtfield param" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						cssStyle="width:220px;">
					</s:select>
				</div>
				<div class="campo">
					<s:select list="tipoPases" id="tipoPases"
						name="paseAtencion.estimacionCoberturaReporte.tipoEstimacion" 
						label="*Cobertura"
						cssClass="cleaneable txtfield param" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						cssStyle="width:220px;">
					</s:select>
				</div>
			</div>
			<div class="divSup" >
				<div class="campo">
					<s:textfield maxlength="15" label="Número de Siniestro" id="txfNumSini" cssStyle="width:208px;"  name="paseAtencion.numeroSiniestro" cssClass="cleaneable txtfield" ></s:textfield>
				</div>
				<div class="campo" style="margin-left:4px;">
					<div style="width: 120px; float: left; position: relative;">
						<label style="" ><s:text name="midas.auto.reportes.basesEmision.fechaInicial"></s:text></label><br>
						<sj:datepicker name="paseAtencion.fechaInicial"
							label="" labelposition="left" changeMonth="true"
							changeYear="true" buttonImage="../img/b_calendario.gif"
							buttonImageOnly="true" id="fechaInicial" maxlength="18"
							cssStyle="margin-top:15.5px;" cssClass="txtfield cleaneable param" size="10"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
					</div>
					<div style="width: 120px; float: left; position: relative;">
						<label style="" ><s:text name="midas.auto.reportes.basesEmision.fechaFinal"></s:text></label><br>
						<sj:datepicker name="paseAtencion.fechaFinal"
							label="" labelposition="left" changeMonth="true"
							changeYear="true" buttonImage="../img/b_calendario.gif"
							buttonImageOnly="true" id="fechaFinal" maxlength="18"
							cssStyle="margin-top:15.5px;" cssClass="txtfield cleaneable param" size="10"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
					</div>
				</div>
			</div>
			<div class="divSup" >
				<div class="campo">
					<s:textfield maxlength="15" label="No. Folio" id="txfNumFol" name="paseAtencion.estimacionCoberturaReporte.folio" cssClass="cleaneable param txtfield" ></s:textfield>
				</div>
				<div style="margin-top: 24px;" class="campo">			
					<div id="btn_buscar" class="btn_back w80" style="display: inline; float: left;position: relative;">
			            <a href="javascript: void(0);" onclick="buscarPaseAtencion();"> 
			            <s:text name="midas.boton.buscar" /> </a>
					</div>
					<div id="btn_limpiar" class="btn_back w80"  style="display:inline; float: left;position: relative; ">
			            <a href="javascript: void(0);" onclick="limpiarFormulario();">
			            <s:text name="midas.boton.limpiar" /> </a>
					</div>
					<div id="btn_exportar" class="btn_back w110"  style="display:inline; float: left;position: relative; ">
			            <a href="javascript: void(0);" onclick="exportarExcel();">
			            <s:text name="midas.boton.exportarExcel" /> </a>
					</div>
				</div>
			</div>
		</form>
	</div>
	<br>
	<div id="divInferior">
		<div class="titulo"><s:text name="midas.siniestros.cabina.reportecabina.pasesAtencion.listadoPasesAtencion"></s:text></div>
		<br>
		<div id="indicadorListadoPase"></div>
		<div id="siniestrosVehiculoGrid" style="width: 100%; height: 100%;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>	
</div>

<script type="text/javascript">
 jQuery(document).ready(
 	function(){
 		getPasesAtencion(false);
 	}
 );
</script>
