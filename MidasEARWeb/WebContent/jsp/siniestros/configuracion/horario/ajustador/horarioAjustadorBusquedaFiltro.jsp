<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="%{formFiltro}" id="%{formFiltro}" method="post"
	action="%{actionNameListarConfiguracion}">

<table width="99%" id="filtros">
	<tr>
		<td class="titulo" colspan="4">
			<s:text	name="midas.siniestros.configuracion.horario.ajustador.busquedaTitulo" />
		</td>
	</tr>
	<tr>
		<td>
				<!-- OFICINAS -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.oficinas" />	:
				<s:select list="ctgOficinas" id="lstOficinaId"
					name="horarioAjustadorFiltro.oficinaId"
					onchange="onChangeOficina('lstAjustadoresId')"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w200" />
			
		</td>
		<td colspan="2">
				<!-- TIPO DISPONIBILIDAD -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.tipoDisponibilidad" />:
				<s:select list="ctgTipoDisponibilidad"
					name="horarioAjustadorFiltro.tipoDisponibilidadCode"
					required="true"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w200" />
		</td>
	</tr>
	<tr>
		<td rowspan="2">
				<!-- AJUSTADORES -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.ajustadores" />:
				<s:select list="ctgAjustadores"
					size="5" multiple="true"
					id="lstAjustadoresId"
					name="horarioAjustadorFiltro.ajustadoresIds"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w200" />
		</td>
		<td>
				<!-- FECHA INICIAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.fechaInicial" />:
				<sj:datepicker id="fechaInicial"
    				name="horarioAjustadorFiltro.fechaInicial"
					minDate="" maxDate=""
					changeMonth="true" changeYear="true" numberOfMonths="2"
					readonly="readonly" buttonImage="../img/b_calendario.gif"
					maxlength="10" cssClass="txtfield" size="12"
					onCompleteTopics="onSelectFechaInicial"/>
		</td>
		<td>
				<!-- HORARIO INICIAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.horarioInicial" />:
				<s:select list="ctgHorarios" headerKey=""
					name="horarioAjustadorFiltro.horarioInicialCode"
					headerValue="%{getText('midas.general.seleccione')}"
					cssClass="cajaTextoM2 w120" />
		</td>
		<td>
			<div id="indicador" style="width:80%; float:center;"></div>
		</td>
	</tr>
	<tr>
		<td>
				<!-- FECHA FINAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.fechaFinal" />:
				<sj:datepicker id="fechaFinal"
					name="horarioAjustadorFiltro.fechaFinal"
					minDate="" maxDate=""
					changeMonth="true" changeYear="true" numberOfMonths="2"
					readonly="readonly" buttonImage="../img/b_calendario.gif"
					maxlength="10" cssClass="txtfield" size="12"
					onCompleteTopics="onSelectFechaFinal" />
		</td>
		<td>
				<!-- HORARIO FINAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.horarioFinal" />	:
				<s:select list="ctgHorarios" headerKey=""
					name="horarioAjustadorFiltro.horarioFinalCode"
					headerValue="%{getText('midas.general.seleccione')}"
					cssClass="cajaTextoM2 w120" />
		</td>		
		<td>
				<!-- BOTON BUSCAR -->
				<div class="btn_back w140" style="display: block; float: left; width: 40%;"
					id="b_buscar">
					<a id="buscarBtnId" href="javascript: void(0);"
						onClick="listarConfiguracionAction();" class="icon_buscar"><s:text
							name="midas.boton.buscar" /> </a>
				</div>
				<!-- BOTON LIMPIAR -->
				<input id="resetForm" type="reset" style="display:none;">
				<div class="btn_back w140" style="display: block; float: left; width: 40%;"
					id="b_limpiar">
					<a id="limpiarBtnId" href="javascript: void(0);"
						onClick='jQuery("#resetForm").click(); jQuery("#lstAjustadoresId").empty();' class="icon_limpiar"><s:text
							name="midas.boton.limpiar" /> </a>
				</div>
		</td>
	</tr>
</table>

</s:form>