<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<rows><s:if test="soloDatos == false ">
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enableMultiline"><param>true</param></call>
            <call command="verticalPadding"><param>5</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
			<call command="setDateFormat">
				<param>%d/%m/%Y</param>
			</call>
        </beforeInit>
        <column id="ajustador"   type="ro" hidden="false" width="235" sort="str" >Ajustador</column>
        <column id="dia"         type="ro" hidden="false" width="80"  sort="str" >Dia</column>
      	<column id="fecha"       type="ro" hidden="false" width="90"  sort="date_custom" >Fecha</column>
		<column id="normal"      type="ro" hidden="false" width="90"  sort="str" >Normal</column>
		<!-- <column id="delnormal"   type="ro" hidden="false" width="30" sort="na" >#cspan</column>  -->
		<column id="delnormalselected" 	 type="ch"    	  width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="guardia"     type="ro" hidden="false" width="90"  sort="str" >Guardia</column>
		<column id="delguardiaselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="apoyo"       type="ro" hidden="false" width="90"  sort="str" >Apoyo</column>
		<column id="delapoyoselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="descanso"    type="ro" hidden="false" width="90"  sort="str" >Descanso</column>
		<column id="deldescansoselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="incapacidad" type="ro" hidden="false" width="90"  sort="str" >Incapacidad</column>
		<column id="delincapaselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="permiso"     type="ro" hidden="false" width="90"  sort="str" >Permiso</column>
		<column id="delpermisoselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
		<column id="vacaciones"  type="ro" hidden="false" width="90"  sort="str" >Vacaciones</column>
		<column id="delvacaselected" 	 type="ch"    width="30"  sort="na"  align="center" >#master_checkbox</column>
	</head></s:if>
	<s:iterator value="listadoDeDisponibilidadGrid" status="row">
		 <row  id="<s:property                                value="id"                                         escapeHtml="false" escapeXml="true"/>">
			<cell><s:property                                 value="ajustador"                                  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date                                     name="fecha"                                       format="EEEE" /></cell>
			<cell><s:date                                     name="fecha"                                       format="dd/MM/yyyy" /></cell>
			
			<cell><s:property value="dispNormal.horarioLaboral.descripcion"    escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="dispNormal.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">			
				<cell><s:property value="dispNormalSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispNormal"><s:property value="dispNormal.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispNormal.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispNormal.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispNormal.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property value="dispGuardia.horarioLaboral.descripcion"   escapeHtml="false" escapeXml="true"/></cell>			
			<s:if test="dispGuardia.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispGuardiaSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispGuardia"><s:property value="dispGuardia.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispGuardia.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispGuardia.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispGuardia.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property value="dispApoyo.horarioLaboral.descripcion"     escapeHtml="false" escapeXml="true"/></cell>				
			<s:if test="dispApoyo.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispApoyoSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispApoyo"><s:property value="dispApoyo.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispApoyo.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispApoyo.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispApoyo.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property value="dispDescanso.horarioLaboral.descripcion"   escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="dispDescanso.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispDescansoSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispDescanso"><s:property value="dispDescanso.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispDescanso.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispDescanso.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispDescanso.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>				
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property  value="dispIncapacidad.horarioLaboral.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="dispIncapacidad.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispIncapacidadSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispIncapacidad"><s:property value="dispIncapacidad.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispIncapacidad.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispIncapacidad.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispIncapacidad.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>		
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property  value="dispPermiso.horarioLaboral.descripcion"    escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="dispPermiso.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispPermisoSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispPermiso"><s:property value="dispPermiso.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispPermiso.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispPermiso.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispPermiso.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>		
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			
			<cell><s:property value="dispVacaciones.horarioLaboral.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="dispVacaciones.horarioLaboral.descripcion != '' && validarFechaHoyOMayor">
				<cell><s:property value="dispVacacionesSeleccionado" escapeHtml="false" escapeXml="true" /></cell>
				<userdata name="userData_dispVacaciones"><s:property value="dispVacaciones.id.fecha" escapeHtml="false" escapeXml="true"/>|<s:property value="dispVacaciones.id.ajustadorId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispVacaciones.id.oficinaId" escapeHtml="false" escapeXml="true"/>|<s:property value="dispVacaciones.id.horarioLaboralId" escapeHtml="false" escapeXml="true"/></userdata>	
			</s:if>
			<s:else>
				<cell><![CDATA[&nbsp;]]></cell>
			</s:else>
			<userdata name="userData_validarFechaHoyOMayor"><s:property value="validarFechaHoyOMayor" escapeHtml="false" escapeXml="true"/></userdata>			
		</row>
	</s:iterator>
</rows>