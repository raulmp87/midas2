<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="%{formEnvioCorreo}" id="%{formEnvioCorreo}" method="post"
	action="%{actionNameEnviarPorCorreo}">
	<div class="contenedorConFormato">
		<div class="section group">
			<div class="col span_1_of_4"></div>
			<div class="col span_1_of_4">
				<!-- ENVIAR A ROLES -->
				<s:text
					name="midas.siniestros.configuracion.horario.ajustador.enviarARoles" />
				:
				<s:checkboxlist labelposition="left" list="listaCorreoRoles"
					name="correoRolesSeleccionados" 
					cssStyle="vertical-checkbox"/>
			</div>
			<div class="col span_1_of_4">
				<!-- FORMA DE ENVIO -->
				<s:text
					name="midas.siniestros.configuracion.horario.ajustador.formaDeEnvio" />
				:
				<s:select list="ctgFormasDeEnvio" name="correoFormaDeEnvioCode"
					required="true"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w120" />
			</div>
			<div class="col span_1_of_4">
				<!-- BOTON ENVIAR -->
				<div class="btn_back w140" style="display: inline; float: left;"
					id="b_enviar">
					<a id="enviarBtnId" href="javascript: void(0);"
						onClick="enviarPorCorreoAction();" class="icon_enviar"><s:text
							name="midas.siniestros.configuracion.horario.ajustador.enviarPorCorreo" /> </a>
				</div>
			</div>
		</div>
	</div>
</s:form>
<script type="text/javascript">
	initBusqueda();
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>