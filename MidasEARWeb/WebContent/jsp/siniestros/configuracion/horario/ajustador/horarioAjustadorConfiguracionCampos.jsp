<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:form name="%{formConfig}" id="%{formConfig}" method="post"
	action="%{actionNameMostrarConfiguracion}">
	<table width="98%" class="contenedorMarco">
		<tr>
			<td width="25%">
				<!-- OFICINAS -->
				<s:text
					name="midas.siniestros.configuracion.horario.ajustador.oficinas" />
				:
				<s:select list="ctgOficinas" id="lstOficinaId"
					name="config.oficinaId"
					onchange="onChangeOficina('lstAjustadoresId')"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w200" />
			</td>
			<td width="20%">
				<!-- TIPO DISPONIBILIDAD -->
				<s:text
					name="midas.siniestros.configuracion.horario.ajustador.tipoDisponibilidad" />
				:
				<s:select list="ctgTipoDisponibilidad"
					name="config.tipoDisponibilidadCode"
					required="true"
					headerValue="%{getText('midas.general.seleccione')}" headerKey=""
					cssClass="cajaTextoM2 w200" />
			
			</td>
			<td width="20%">
				<!-- FECHA INICIAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.fechaInicial" />:
				<sj:datepicker id="fechaInicial"
					name="config.fechaInicial"
					minDate="fechaInicialLimite" maxDate="fechaFinalLimite"
					changeMonth="true" changeYear="true" numberOfMonths="2"
					readonly="readonly" buttonImage="../img/b_calendario.gif"
					maxlength="10" cssClass="txtfield" size="12"
					onCompleteTopics="onSelectFechaInicial"
					onchange="intentarSacarInfoDeSeleccion()"/>
			</td>
			<td rowspan="2">
				<!-- COMENTARIOS -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.comentarios" />:
				<s:textarea cols="50" maxlength="500" 
					name="config.comentarios"
					labelposition="left" rows="4" disabled="false" cssClass="textarea w350 h80"
					id="comentarios" />
			</td>
		</tr>

		<tr>
			<td rowspan="3">
				<!-- AJUSTADORES -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.ajustadores" />:
				<s:select list="ctgAjustadores"
					size="5" multiple="true"
					id="lstAjustadoresId"
					onchange="intentarSacarInfoDeSeleccion()"
					name="config.ajustadoresIds"
					cssClass="cajaTextoM2 w200 h100"/>
			</td>
			<td>
				<!-- HORARIO LABORAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.horario" />:
				<s:select list="ctgHorariosLaborales" headerKey=""
					name="config.horarioLaboralId"
					headerValue="%{getText('midas.general.seleccione')}"
					value="%{(config!=null)?config.horarioLaboralId:horarioLaboralDefault}"
					cssClass="cajaTextoM2 w200" />
			</td>
			<td>
				<!-- FECHA FINAL -->
				<s:text	name="midas.siniestros.configuracion.horario.ajustador.fechaFinal" />:
				<sj:datepicker id="fechaFinal"
					name="config.fechaFinal"
					minDate="fechaInicialLimite" maxDate="fechaFinalLimite"
					changeMonth="true" changeYear="true" numberOfMonths="2"
					readonly="readonly" buttonImage="../img/b_calendario.gif"
					maxlength="10" cssClass="txtfield" size="12"
					onCompleteTopics="onSelectFechaFinal" 
					onchange="intentarSacarInfoDeSeleccion()"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- LISTA DIAS -->
				<s:checkboxlist labelposition="top" list="listaDias"
					name="config.diasSeleccion" 
					value="diasPredeterminado" />
			</td>
			<td align="right">
				<!-- BOTON GUARDAR -->
				<div class="btn_back w140" style="display: inline; float: left;"
					id="b_guardar">
					<a id="guardarBtnId" href="javascript: void(0);"
						onClick="salvarActualizarAction();" class="icon_guardar2"><s:text
							name="midas.boton.guardar" /> </a>
				</div>
				
				<div class="btn_back w120" style="display: inline; float: left;">
					<a id="eliminarBtnId" href="javascript: void(0);"	onClick="eliminarConfiguraciones();" class="">
						<s:text name="Eliminar" />
					</a>
				</div>
				
				<!-- BOTON CERRAR -->
				<div class="btn_back w120" style="display: inline; float: left;"
					id="b_cerrar">
					<a id="cerrarBtnId" href="javascript: void(0);"
						onClick="mostrarBusquedaAction();" class="icon_cerrar"><s:text
							name="midas.boton.cerrar" /> </a>
				</div>
			</td>
		</tr>
	</table>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
