<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript"
	src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/siniestros/configuracion/horarioAjustador.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js"
	type="text/javascript"></script>

<s:set var="actionNameMostrarConfiguracion"
	value="actionNameMostrarConf" />
<s:set var="actionNameMostrarBusqueda" value="actionNameMostrarBusqueda" />
<s:set var="actionNameListarConfiguracion"
	value="actionNameListarConfiguracion" />
<s:set var="actionNameValidarListarConfiguracion"
	value="actionNameValidarListarConf" />
<s:set var="actionNameSalvarActualizar"
	value="actionNameSalvarActualizar" />
<s:set var="actionNameEnviarPorCorreo" value="actionNameEnviarPorCorreo" />

<s:url var="mostrarConfiguracionURL"
	action="%{actionNameMostrarConfiguracion}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="mostrarBusquedaURL" action="%{actionNameMostrarBusqueda}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="listarConfiguracionURL"
	action="%{actionNameListarConfiguracion}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="validarListarConfiguracionURL"
	action="%{actionNameValidarListarConfiguracion}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="salvarActualizarURL" action="%{actionNameSalvarActualizar}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="borrarConfigURL" action="%{actionNameBorrarConf}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="enviarPorCorreoURL" action="%{actionNameEnviarPorCorreo}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />
<s:url var="exportarConfiguracionURL"
	action="%{actionNameExportarExcel}"
	namespace="/siniestros/configuracion/horario/ajustador/horarioAjustador" />

<script type="text/javascript">
	mostrarConfiguracionURL =       '<s:property value="mostrarConfiguracionURL"        />';
	mostrarBusquedaURL =            '<s:property value="mostrarBusquedaURL"             />';
	listarConfiguracionURL =        '<s:property value="listarConfiguracionURL"         />';
	validarListarConfiguracionURL = '<s:property value="validarListarConfiguracionURL"  />';
	salvarActualizarURL =           '<s:property value="salvarActualizarURL"            />';
	borrarConfigURL =               '<s:property value="borrarConfigURL"                />';
	enviarPorCorreoURL =            '<s:property value="enviarPorCorreoURL"             />';
	exportarConfiguracionURL =      '<s:property value="exportarConfiguracionURL"       />';

	divIdConfCamposIngreso = '<s:property value="divConfCampos"      />';
	formIdConfig           = '<s:property value="formConfig"         />';
	divIdBusqFiltro        = '<s:property value="divBusqFiltro"      />';
	formIdFiltro           = '<s:property value="formFiltro"         />';
	divIdEnvioCorreo       = '<s:property value="divEnvioCorreo"     />';
	formEnvioCorreo        = '<s:property value="formEnvioCorreo"    />';

	divIdLstDispGrid = '<s:property value="divLstDispGrid"     />';
	txtAgregarQuitar = '<s:property value="agregarQuitarTodos" />';
</script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<style type='text/css'>
.objbox tabe tbody tr {
	padding: 10px, 0;
}

/*  SECTIONS  */
.section {
	clear: both;
	padding: 0px;
	margin: 0px;
}

/*  COLUMN SETUP  */
.col {
	display: block;
	float: left;
	margin: 1% 0 1% 1.6%;
}

.col:first-child {
	margin-left: 0;
}

/*  GROUPING  */
.group:before,.group:after {
	content: "";
	display: table;
}

.group:after {
	clear: both;
}

.group {
	zoom: 1; /* For IE 6/7 */
}

/*  GRID OF THREE  */
.span_3_of_3 {
	width: 100%;
}

.span_2_of_3 {
	width: 66.1%;
}

.span_1_of_3 {
	width: 32.2%;
}

/*  GO FULL WIDTH AT LESS THAN 480 PIXELS */
@media only screen and (max-width: 480px) {
	.col {
		margin: 1% 0 1% 0%;
	}
}

@media only screen and (max-width: 480px) {
	.maincontent {
		text-align: left;
	}
	.span_3_of_3 {
		width: 100%;
		height: auto;
	}
	.span_2_of_3 {
		width: 100%;
		height: auto;
	}
	.span_1_of_3 {
		width: 100%;
		height: auto;
	}
}

/*  GRID OF FOUR   ============================================================================= */
.span_4_of_4 {
	width: 100%;
}

.span_3_of_4 {
	width: 74.6%;
}

.span_2_of_4 {
	width: 49.2%;
}

.span_1_of_4 {
	width: 23.8%;
}

/*  GO FULL WIDTH AT LESS THAN 480 PIXELS */
@media only screen and (max-width: 480px) {
	.span_4_of_4 {
		width: 100%;
	}
	.span_3_of_4 {
		width: 100%;
	}
	.span_2_of_4 {
		width: 100%;
	}
	.span_1_of_4 {
		width: 100%;
	}
}
</style>