<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="%{jspHeader}"></s:include>
<div class="titulo">
	<s:text
		name="midas.siniestros.configuracion.horario.ajustador.configuracionTitulo" />
</div>
<s:div name="%{divConfCampos}">
	<s:include value="%{jspCamposIngreso}" />
</s:div>
<span><a href="javascript: void(0);" onclick="maxminGrid(divIdLstDispGrid, 250);">Maximizar/minimizar</a></span>
<s:div name="%{divLstDispGrid}" class="%{divLstDispGrid}"  style="width: 98%; height: 241px">
</s:div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<script type="text/javascript">
	init();
</script>