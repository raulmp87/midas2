<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="%{jspHeader}"></s:include>

<s:div name="%{divBusqFiltro}">
	<s:include value="%{jspFiltro}" />
</s:div>
<span><a href="javascript: void(0);" onclick="maxminGrid(divIdLstDispGrid, 425);">Maximizar/minimizar</a></span><div class="titulo">
	<s:text name="midas.siniestros.configuracion.horario.ajustador.listado" />
</div>

<s:div name="%{divLstDispGrid}" class="%{divLstDispGrid}"
	style="width: 98%; height: 150px;">
</s:div>

<div id="pagingArea"></div>
<div id="infoArea"></div>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />
					&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<!-- BOTON IR A CONFIGURAR -->
			<div class="btn_back w140" style="display: block; float: right;">
				<a id="configurarBtnId" href="javascript: void(0);"	onClick="mostrarConfiguracionAction();" class="">
					<s:text name="Configurar" />
				</a>
			</div>
			<div class="btn_back w140" style="display: block; float: right;">
				<a id="eliminarBtnId" href="javascript: void(0);"	onClick="eliminarConfiguraciones();" class="">
					<s:text name="Eliminar" />
				</a>
			</div>	
		</td>
	</tr>
</table>	
<div class="titulo"><s:text
		name="midas.siniestros.configuracion.horario.ajustador.enviarPorCorreo" />
</div>
<s:div name="%{divEnvioCorreo}">
	<s:include value="%{jspCorreos}" />
</s:div>

<script type="text/javascript">
	init();
</script>