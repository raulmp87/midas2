<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows><s:if test="cargaInicial == 1 ">
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="id"          type="ro"  width="*" sort="int" hidden="true"> <s:text name="id" /> </column>
        <column id="horaEntrada" type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.configuracion.horario.laboral.horaDeEntrada" /> </column>
      	<column id="horaSalida"  type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.configuracion.horario.laboral.horaDeSalida" /> </column>
		<column id="estatus"     type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.configuracion.horario.laboral.estatus" /> </column>
		<column id="default"     type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.configuracion.horario.laboral.predeterminado" /> </column>
		<column id="extra"       type="img" width="80" sort="na"  hidden="false" align="center"><s:text name="midas.siniestros.catalogo.pieza.acciones" /></column>
	</head></s:if>
	<s:iterator value="horarioLaboralGrid" status="row">
		<row  id="<s:property value="id"          escapeHtml="false" escapeXml="true"/>" >
			<cell><s:property value="id"          escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="horaEntrada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="horaSalida"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:if 
				test="estatus == 1">Activo</s:if><s:elseif  
				test="estatus == 0">Inactivo</s:elseif></cell>
			<cell><s:if 
				test="predeterminado == 1">SI</s:if><s:elseif  
				test="predeterminado == 0">NO</s:elseif></cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^Editar^javascript:editarDetalleServicio(<s:property value="id" />);^_self</cell>
		</row>
	</s:iterator>
</rows>