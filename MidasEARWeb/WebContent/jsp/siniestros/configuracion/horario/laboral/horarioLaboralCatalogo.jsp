<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript"
    src="<s:url value='/js/midas2/siniestros/configuracion/horarioLaboral.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript">
    
    listarCargaURL      = '<s:url action="listarCarga"      namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    mostrarURL          = '<s:url action="mostrar"          namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    busquedaURL         = '<s:url action="busqueda"         namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    mostrarAltaURL      = '<s:url action="mostrarAlta"      namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    verDetalleURL       = '<s:url action="verDetalle"       namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    salvarActualizarURL = '<s:url action="salvarActualizar" namespace="/siniestros/configuracion/horario/laboral/horarioLaboral"/>';
    
    guardarText = '<s:text name="midas.boton.guardar" />';
    agregarText = '<s:text name="midas.boton.agregar" />';
       
</script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<s:form name="busqueda" id="busqueda" method="post"
	action="busqueda">

	<table width="99%" id="filtros">
		<tr>
			<td>
				<div class="titulo">
					<s:text name="midas.siniestros.configuracion.horario.laboral.titulo" />
				</div>
			</td>
		</tr>
		<tr>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.horaDeEntrada" />:</th>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.horaDeSalida" />:</th>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.estatus" />:</th>
			<th width="20%">
				<!-- Botones -->&nbsp;</th>
		</tr>
		<tr>
			<td><s:select list="ctgHorarios" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="horarioLaboralFiltro.horaEntradaCode" id="horaEntradaCode"
					cssClass="cajaTextoM2 w120" onchange="" />
			</td>
			<td><s:select list="ctgHorarios" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="horarioLaboralFiltro.horaSalidaCode" id="horaSalidaCode"
					cssClass="cajaTextoM2 w120" onchange="" />
			</td>
			<td><s:select list="ctgEstatus" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="horarioLaboralFiltro.estatus" id="estatus"
					cssClass="cajaTextoM2 w120" onchange="" />
			</td>
			<td width="20%" class="guardar">
				<div style="display: inline; float: right; margin-right: 10%;"
					id="b_borrar">
					<a href="javascript: void(0);" onclick="regresar();"> <s:text
							name="midas.boton.limpiar" /> </a>
				</div>
				<div style="display: inline; float: left; margin-left: 10%;"
					id="b_buscar">
					<a href="javascript: void(0);" onclick="busquedaServicio();"> <s:text
							name="midas.boton.buscar" /> </a>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div id="horarioLaboralGrid" style="width: 98%; height: 241px"></div>
				<div id="pagingArea">
				
				</div><div id="infoArea">
				
				</div>
			</td>
			<td>
			</td>
		</tr>
	</table>
	<br>
</s:form>
<div id="horarioLaboralDetalleDiv">
<s:include value="horarioLaboralDetalle.jsp"></s:include>
</div>
<div id="indicador"></div>
<script type="text/javascript">
	init();
</script>