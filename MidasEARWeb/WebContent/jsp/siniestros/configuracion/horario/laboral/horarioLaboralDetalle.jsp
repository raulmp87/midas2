<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:form name="salvarActualizar" id="salvarActualizar" method="post"
	action="salvarActualizar">
	<s:hidden name="accion" id="accion"/>
	<div class="titulo">
		<s:text name="midas.siniestros.configuracion.horario.laboral.configuracionTitulo" />
	</div>

	<table width="99%" id="filtros">
		<tr>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.horaDeEntrada" />:</th>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.horaDeSalida" />:</th>
			<th><s:text
					name="midas.siniestros.configuracion.horario.laboral.estatus" />:</th>
            <th><s:text
					name="midas.siniestros.configuracion.horario.laboral.predeterminado" />:</th>
            <th width="20%"><!-- Botones -->&nbsp;</th>
        </tr>
        <tr>
			<td><s:if test="accion == 1 ">
					<s:select list="ctgHorarios" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						name="horarioLaboral.horaEntradaCode" id="horaEntradaCode"
						cssClass="cajaTextoM2 w120" />
				</s:if>
				<s:elseif test="accion == 0 ">
					<s:hidden name="horarioLaboral.id"/>
					<s:hidden name="horarioLaboral.horaEntradaCode"	id="horaEntradaCode"/>
					<s:select list="ctgHorarios" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						name="horarioLaboral.horaEntradaCode" id="horaEntradaCode"
						cssClass="cajaTextoM2 w120" disabled="true" />
				</s:elseif>
			</td>
			<td><s:if test="accion == 1 ">
					<s:select list="ctgHorarios" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						name="horarioLaboral.horaSalidaCode" id="horaSalidaCode"
						cssClass="cajaTextoM2 w120" />
				</s:if>
				<s:elseif test="accion == 0 ">
					<s:hidden name="horarioLaboral.horaSalidaCode" id="horaSalidaCode"/>
					<s:select list="ctgHorarios" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						name="horarioLaboral.horaSalidaCode" id="horaSalidaCode"
						cssClass="cajaTextoM2 w120" disabled="true" />
				</s:elseif>
			</td>
			<td><s:if test="accion == 1">
					<s:select list="ctgEstatus" headerKey=""
						headerValue="%{getText('midas.general.seleccione')}"
						name="horarioLaboral.estatus" id="estatus"
						cssClass="cajaTextoM2 w120" value="estatusPorDefault" />
				</s:if>
				<s:else>
					<s:if test="accion == 0">
						<s:if test="horarioLaboral.predeterminado == 1">
							<s:hidden name="horarioLaboral.estatus" id="estatus"/>
							<s:select list="ctgEstatus" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								name="horarioLaboral.estatus" id="estatus"
								cssClass="cajaTextoM2 w120" disabled="true" />
						</s:if>
						<s:else>
							<s:select list="ctgEstatus" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								name="horarioLaboral.estatus" id="estatus"
								cssClass="cajaTextoM2 w120" value="horarioLaboral.estatus"/>
						</s:else>
					</s:if>
				</s:else>
			</td>
			<td><s:if
					test="accion == 0 &&  
							(horarioLaboral.predeterminado == 1 || 
							horarioLaboral.estatus == 2)">
					<s:checkbox id="predeterminado"
						name="horarioLaboral.predeterminado"
						value="%{horarioLaboral.predeterminado}"
						cssClass="cajaTextoM2 w120" disabled="true" onchange="" />
				</s:if> <s:else>
					<s:checkbox id="predeterminado"
						name="horarioLaboral.predeterminado"
						value="%{horarioLaboral.predeterminado}"
						cssClass="cajaTextoM2 w120" enabled="false" onchange="" />
				</s:else></td>
			<td></td>
			<td class="guardar">
				<div class="btn_back w140" style="display: inline; float: left;"
					id="b_guardar">
					<a id="guardarBtnId" href="javascript: void(0);"
						onClick="agregarDetalleServicio()" 
						class="icon_guardar2"><s:if 
							test="accion == 0 "><s:text 
								name="midas.boton.guardar" /></s:if><s:elseif 
							test="accion == 1 "><s:text 
								name="midas.boton.agregar" /></s:elseif></a>
				</div>
			</td>
			<td class="cancelar">
				<input id="resetForm" type="reset" style="display:none;" 
						onclick="confirm('La información no guardada se perderá. ¿Desea continuar?')">
				<div class="btn_back w140" style="display: inline; float: left;"
					id="b_borrar">
					<a id="guardarBtnId" href="javascript: void(0);"
						onClick='jQuery("#resetForm").click();' class="icon_cancelar">
							<s:text name="midas.boton.cancelar" />
					</a>
				</div>
			</td>
		</tr>
    </table>
    <br>
</s:form>
<script type="text/javascript">
	initDetalle();
</script>


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>