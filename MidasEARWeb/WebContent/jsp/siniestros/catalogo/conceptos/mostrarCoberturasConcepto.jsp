<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>


<style>
<!--
table{
	text-transform: uppercase;
}
tr:hover{
 	background-color:#fafaf0;
}

td:hover{
	background-color:#fafaf0;
}
-->
</style>

<s:hidden name="keyToCoberturasNegocio" id="keyToCoberturasNegocio" />
<script src="<s:url value='/js/midas2/siniestros/catalogo/conceptos/conceptoAjuste.js'/>" ></script>

<table id="agregar" style=" border:0; ">
  	<thead>
  		<tr>
  			<th>Nombre: <s:text name="nombreConcepto"/></th>
  		</tr>
	</thead>
</table>

<div id="indicador"></div>
<div id="inferior">
	<div id="coberturasGrid"
		style="width: 95%; height: 400px; margin-left: 2%"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</div>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script type="text/javascript">

 jQuery( document ).ready(function() {
	cargaGridCoberturaAsociadas();
 });
 
</script>
