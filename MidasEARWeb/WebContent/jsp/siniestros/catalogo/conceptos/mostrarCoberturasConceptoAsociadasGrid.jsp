<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows> 

<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
        
        <column id="concepto"   type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.general.seccion" /></column>
        <column id="categoria"  type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.fuerzaventa.movimientomanual.alta.concepto" /> </column>
</head>

	<s:iterator value="conceptoAjusteDtoGrid" status="row">
		<row> 
			<cell><s:property value="nombre"       escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoConcepto" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>