<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows> 
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>17</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        
        <column id="concepto"   type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.catalogo.conceptos.nombre" /></column>
        <column id="estatus"    type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.catalogo.conceptos.estatus" /> </column>
      	<column id="fechaAlta"  type="ro"  width="*" sort="date_custom" hidden="false"><s:text name="midas.siniestros.catalogo.conceptos.fecha.alta" /> </column>
		<column id="fechaBaja"  type="ro"  width="*" sort="date_custom" hidden="false"><s:text name="midas.siniestros.catalogo.conceptos.fecha.baja" /> </column>
		<column id="extra"      type="img" width="40" sort="na" align="center">Acciones</column>
		<column id="extra"      type="img" width="40" sort="na" align="center">#cspan</column>
		<column id="extra"      type="img" width="40" sort="na" align="center">#cspan</column>
		<column id="aplicaIva" type="ro" width="*" sort="int" hidden="true" >aplicaIva</column>	
		<column id="aplicaIvaRetenido" type="ro" width="*" sort="int" hidden="true" >aplicaIvaRetenido</column>
		<column id="aplicaIsr" type="ro" width="*" sort="int" hidden="true" >aplicaIsr</column>	
		<column id="porcIva" type="ro" width="*" sort="int" hidden="true" >porcIva</column>	
		<column id="porcIsr" type="ro" width="*" sort="int" hidden="true" >porcIsr</column>	
		<column id="porcIvaRetenido" type="ro" width="*" sort="int" hidden="true" >porcIvaRetenido</column>
	</head>
	<s:iterator value="conceptoAjusteAfectacionReservaGrid" status="row">
		<row> 
			<cell><s:property value="nombre"                  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:if 
						test="estatus == 1">Activo</s:if><s:elseif  
						test="estatus == 0">Inactivo</s:elseif></cell>
			<cell><s:property value="fechaCreacion"       escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCambioEstatus"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^Editar Concepto^javascript:verConceptoPrincipal("<s:property value="id" />","<s:property value="nombre" />","<s:property value="esValidoHGS" />","<s:property value="estatus" />","<s:property value="categoria" />",
																		"<s:property value="aplicaIva" />","<s:property value="aplicaIvaRetenido" />","<s:property value="aplicaIsr" />",
																"<s:property value="porcIva" />","<s:property value="porcIsr" />","<s:property value="porcIvaRetenido" />","1");^_self</cell>
			<cell><s:url value="/img/details.png"/>^Ver Coberturas Asociadas ^javascript:mostarCoberturasAsociadas("<s:property value="id" />","<s:property value="nombre" />");^_self</cell>
			<cell><s:url value="/img/listsicon.gif"/>^Editar Coberturas^javascript:inicializarConceptoCobertura("<s:property value="id" />","<s:property value="nombre" />");^_self</cell>
			<cell><s:property value="aplicaIva"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="aplicaIvaRetenido"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="aplicaIsr"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="porcIva"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="porcIsr"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="porcIvaRetenido"  escapeHtml="false" escapeXml="true"/></cell>
		
		</row>
	</s:iterator>
</rows>