<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>90</param>
				<param>5</param>
				<param>paginaCobertura</param>
				<param>true</param>
				<param>infoAreaCobertura</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        
        <column id="clave"    type="ro"  width="30" sort="int" hidden="false"></column>
        <column id="concepto" type="ro"  width="*" sort="str" hidden="false"></column>
		
	</head>
	<s:iterator value="conceptosGrid" status="row">
		<row >
			<cell> &#60;input type=&#34;checkbox&#34;  <s:if test="claveEstatus == 1" >checked=&#34;checked&#34;</s:if>  class=&#34;negocioId&#34; value=<s:property value="idToCobertura" escapeHtml="false" escapeXml="true"/>-<s:property value="claveTipoCalculo" escapeHtml="false" escapeXml="true"/> name=&#34;negocioId&#34; &#47;&#62; </cell>
			<cell> <s:property value="descripcion" escapeHtml="false" escapeXml="true"/> </cell>
		</row>
	</s:iterator>
</rows>