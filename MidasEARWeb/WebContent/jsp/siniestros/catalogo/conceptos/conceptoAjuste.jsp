<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:form  id="altaConcepto" method="post" action="" >
	<s:hidden name="keyNegocio" id="keyNegocio" />
	<s:hidden name="keyToCoberturasNegocio" id="keyToCoberturasNegocio" />

	<!-- CONCEPTO AJUSTES -->
	<table width="99%" id="filtros">
		<tr>
			<td colspan="7">
				<div class="titulo"><s:text name="midas.siniestros.catalogo.conceptos.titulo" /></div>
			</td>
		</tr>
		<tr>
			<th> <s:text name="midas.siniestros.catalogo.conceptos.nombre" />: </th>
			<td> <s:textfield 
								name="conceptoAjuste.nombre" 
								id="nombre" cssClass="jQrequired cajaTextoM2 w120" 
								maxlength="100"
								onkeypress="" /> 
			</td>
			<th> <s:text name="midas.siniestros.catalogo.conceptos.tipo.categoria" />: </th>
			<td>
					<s:select 
						list="ctgTipoCategoria" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="conceptoAjuste.categoria" 
						id="ctgTipoCategoria" 
						cssClass="jQrequired cajaTextoM2 w120" 
						onchange="javascript:validarCategoria(this);"/> 
			</td>
		</tr>
		<tr>
			<th> <s:text name="midas.siniestros.catalogo.conceptos.estatus" />: </th>
			<td> <s:select 
						list="ctgEstatus" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="conceptoAjuste.estatus" 
						id="estatus" 
						cssClass="jQrequired cajaTextoM2 w120" 
						onchange=""/> 
			</td>
			<th> <s:text name="Se envia HGS" />: </th>
			<td>
					<!--
					<s:select 
						list="ctgTipoConcepto" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="conceptoAjuste.tipoConcepto" 
						id="ctgTipoConcepto" 
						cssClass="jQrequired cajaTextoM2 w120" 
						onchange=""/> 
					-->
					<s:checkbox name="conceptoAjuste.esValidoHGS"  id="enviaHGS" > </s:checkbox>

			</td>
		</tr>
		
		<tr>
			<th> <s:text name="I.V.A." />: </th>
			<td>
				<s:checkbox name="conceptoAjuste.aplicaIva"  id="aplicaIva"  onchange="checkAplicaIva();"   > </s:checkbox>
			</td>
			<th> <s:text name="% I.V.A." />: </th>
			<td>
				<s:textfield 
								name="conceptoAjuste.porcIva" 
								id="porcIva" cssClass="txtfield jQ2float" 
								maxlength="10"								
								onkeypress="mascaraDecimales('#porcIva',this.value);"  
								onblur="mascaraDecimales('#porcIva',this.value); aplicaIVA(this);" 
								/> 
 
			</td>
		</tr>
		<tr>
			<th> <s:text name="I.V.A. Retenido" />: </th>
			<td>
				<s:checkbox name="conceptoAjuste.aplicaIvaRetenido"  id="aplicaIvaRetenido"  onchange="checkAplicaIvaRetenido();" > </s:checkbox>
			</td>
			<th> <s:text name="% I.V.A. Retenido" />: </th>
			<td>
				<s:textfield 
								name="conceptoAjuste.porcIvaRetenido" 
								id="porcIvaRetenido" cssClass="txtfield jQ2float" 
								maxlength="10"								
								onkeypress="mascaraDecimales('#porcIvaRetenido',this.value);"  
								onblur="mascaraDecimales('#porcIvaRetenido',this.value); aplicaIVARetenido(this);" 
								/> 
 
			</td>
		</tr>
		
		<tr>
			<th> <s:text name="I.S.R. Retenido" />: </th>
			<td> 
				<s:checkbox name="conceptoAjuste.aplicaIsr"  id="aplicaIsr"   onchange="checkAplicaIsr();"> </s:checkbox>
			
			</td>
			
			<th> <s:text name="% I.S.R. Retenido" />: </th>
			<td>
				<s:textfield 
								name="conceptoAjuste.porcIsr" 
								id="porcIsr" cssClass="txtfield jQ2float" 
								maxlength="10"
								onkeypress="mascaraDecimales('#porcIsr',this.value);"  
								onblur="mascaraDecimales('#porcIsr',this.value); aplicaISR(this);" 
								
								/> 
 
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="2" align="right" >
				<table width="99%">
					<tr>
						<td>
							<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; width:130px; " id="b_guardar">
							    <a href="javascript: void(0);" onclick="mostrar();" >
							    	Cancelar/Nuevo
							    </a>
							</div>
						</td>
						<td>
							<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_guardar">
								<a href="javascript: void(0);" onclick="crearConceptoPrincipal();" >
									 <div id="accionAlta">
									 	<s:text name="midas.siniestros.catalogo.conceptos.alta" />
								     </div> 
								     <div id="accionEditar" style="display: none;">
									     <s:text name="midas.siniestros.catalogo.conceptos.editar" /> 
									 </div>
								</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
</s:form>

<div class="titulo"><s:text name="midas.siniestros.catalogo.conceptos.listado" /></div>
<div id="a_tabbar" class="dhtmlxTabBar"   style="width:100%; height:500px;" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE"  >
	<!-- 
	<div id="html_1"  name="Gastos Ajuste"  >
		<div id="indicador"></div>
			<div id="conceptosGridGastos" style="width: 98%;height:400px"></div>
		<div id="pagingArea2"></div><div id="infoArea2"></div>
	</div> -->
	<div id="html_1" name="Gastos Ajuste"  >
		<table width="99%" >
			<tr>
				<td width="75%" valign="top" >
					<div id="indicador"></div>
						<div id="conceptosGridGastos" style="width: 98%;height:400px"></div>
					<div id="pagingArea2"></div><div id="infoArea2"></div>
				</td>
				<td  width="23%" valign="top">
					<table id="filtros" class="accionesGastoAjuste" style="display:none;" width="99%">
						<tr>
							<th> <s:text name="midas.siniestros.catalogo.conceptos.gasto.ajuste" /> <hr/> </th>
						</tr>
						<tr>
							<th> <s:text name="midas.siniestros.catalogo.conceptos.nombre" />: </th>
							<td class="nombreConcepto">  </td>
						</tr>
						<tr>
							<td colspan="2" > 
							  
							    <div id="indicador"></div>
									<div id="tipoPrestadorGrid" style="width: 98%;height:300px"></div>
								<div id="pagingArea3"></div><div id="infoArea3"></div>
								
							</td>
						</tr>
						<tr>
							<td  class= "guardar"></td>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="vincularPrestador();" >
									 <s:text name="midas.boton.guardar" /> </a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	
	
	<div id="html_2" name="Afectaci&oacute;n de Reserva"  >
		<table width="99%" >
			<tr>
				<td width="75%" valign="top" >
					<div id="indicador"></div>
						<div id="conceptosGridReserva" style="width: 92%;height:400px"></div>
					<div id="pagingArea"></div><div id="infoArea"></div>
				</td>
				<td  width="23%" valign="top">
					<table id="filtros" width="99%" class="accionesAfectacion" style="display:none;" >
						<tr>
							<th> <s:text name="midas.siniestros.catalogo.conceptos.afectacion.reserva" /> <hr/> </th>
						</tr>
						<tr>
							<th> <s:text name="midas.siniestros.catalogo.conceptos.nombre" />: </th>
							<td class="nombreConcepto">  </td>
						</tr>
						<tr>
							<th> <s:text name="midas.siniestros.catalogo.conceptos.linea.negocio" />: </th>
							<td> <s:select 
									list="seccionDtoGrid" 
									headerKey="0" 
									headerValue="%{getText('midas.general.seleccione')}" 
									name="" 
									id="lineaDeNegocio" 
									cssClass="cajaTextoM2 w120" 
									onchange="cargaGridCobertura(this.value)"/>
							</td>
						</tr>
						<tr>
							<th colspan="2" valign="top"> <span id="lista_afectacion_reserva" style="display:none;" > <s:text name="midas.siniestros.catalogo.conceptos.afectacion.reserva" />: </span> </th>
						</tr>
						<tr>
							<td colspan="2" > 
							    <div id="indicador"></div>
									<div id="coberturasGrid" style="width: 98%;height:300px"></div>
								<div id="pagingArea"></div><div id="infoArea"></div>
							</td>
						</tr>
						<tr>
							<td  class= "guardar"></td>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="vincularCoberturas();" >
									 <s:text name="midas.boton.guardar" /> </a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="html_3" name="Reembolso de Gasto"  >
		<table width="99%" >
			<tr>
				<td width="99%" valign="top" >
					<div id="indicador"></div>
						<div id="conceptosGridReembolso" style="width: 98%;height:400px"></div>
					<div id="pagingArea4"></div><div id="infoArea4"></div>
				</td>
			</tr>
		</table>
	</div>
	
</div>


<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/conceptos/conceptoAjuste.js'/>" ></script>
<script type="text/javascript">
	cargaGrids();
	dhx_init_tabbars();
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>