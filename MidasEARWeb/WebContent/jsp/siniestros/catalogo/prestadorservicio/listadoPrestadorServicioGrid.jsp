<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
      <head>
          <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
            <column id="" type="ro"  width="40"	sort="int" ><s:text name="midas.prestadorservicio.numPrestador"/></column>
            <column id="" type="ro"  width="450"  sort="str" ><s:text name="midas.prestadorservicio.nombre"/></column>
          	<column id="" type="ro"  width="90"  sort="str"><s:text name="midas.prestadorservicio.oficina"/></column>
          	<column id="" type="ro"  width="*"  sort="str"><s:text name="midas.prestadorservicio.tiposPrestador"/></column>
          	<column id="" type="ro"  width="80"  sort="str" ><s:text name="midas.prestadorservicio.estatus"/></column>
            <column id="editar" type="img" width="40" sort="na" align="center">Acciones</column>
			      <column id="consultar" type="img" width="40" sort="na" align="center">#cspan</column>	   
      </head>
      <s:iterator value="listPrestadorServicio">
            <row id="<s:property value="id" escapeHtml="false" escapeXml="true" />">
                <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="personaMidas.nombre"  escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="tipoPrestador" escapeHtml="false" escapeXml="true" /></cell>
                <cell><s:if test="estatus">ACTIVO</s:if><s:else>INACTIVO</s:else></cell>
                <cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
            </row>
      </s:iterator>
       
</rows>
