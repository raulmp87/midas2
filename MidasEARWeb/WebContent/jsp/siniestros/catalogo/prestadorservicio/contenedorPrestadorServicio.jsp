<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>


<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/prestadorservicio/prestadorDeServicio.js'/>"></script>

<s:if test="modoAsignar==true">
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>


	
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
</s:if>


<script type="text/javascript">
var buscarPrestadorServicioPath = '<s:url action="buscar" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var mostrarPrestadorServicioPath = '<s:url action="mostrarAltaDePrestador" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var editarPrestadorServicioPath = '<s:url action="editarPrestador" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var consultarPrestadorServicioPath = '<s:url action="cosultarPrestador" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
</script>

<style type="text/css">
.label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}
</style>

<s:form id="busquedaPrestadorServicio" >

<input id="estadoToAssign"  name="filtroCatalogo.estado" value="<s:property value='filtroCatalogo.estado'/>" type="hidden"/>


	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="3"><s:text name="midas.prestadorservicio.busqueda.titulo" /></td>
            </tr>
            <tr>
                  <td><s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w160" 
							name="filtroCatalogo.numeroPrestador"
							id="numeroPrestador" 
							labelposition="left" 
							label="No. Prestador"
							size="10"					
							id="txt_num_prestador" 
							onkeypress="return soloNumeros(this, event, true)"/></td>
                  
                  <td><s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w160" 
							name="filtroCatalogo.nombrePersona" 
							id="nombrePrestador" 
							labelposition="left" 
							label ="Nombre Prestador "
							size="10"					
							id="txt_nombre_prestador"/></td>
                  
                  <td><s:select id="s_oficinas" 
								labelposition="left" 
								label="Oficinas"
								name="filtroCatalogo.oficinaId"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="oficinas" listKey="key" listValue="value"  
						  		cssClass="txtfield cajaTextoM2 w160" /></td>

            </tr>

            <tr>
            
            	 <td><s:select id="s_estatus" 
								labelposition="left" 
								label="Estatus"
								name="filtroCatalogo.estatus"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="estatus" listKey="key" listValue="value"  
						  		cssClass="txtfield cajaTextoM2 w160" /></td>
				

            		<td colspan="2">
            		<s:if test="modoAsignar">
            			<s:hidden id="h_multiplesTipoPrestador" name="filtroCatalogo.tipoPrestadorStr" ></s:hidden>
						
						  	    		
            		</s:if>
            		<s:else>
            			<s:select id="s_tipo_prestador" 
								labelposition="left" 
								label="Tipo Prestador"
								name="filtroCatalogo.tipoPrestadorStr"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="tiposPrestador" listKey="key" listValue="value"  
						  		cssClass="txtfield cajaTextoM2 w160" />
            		</s:else>
					</td>
            </tr>
           
            
            		<tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="getPrestadorDeServicios();" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
															<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
									
								</td>							
							</tr>
						</table>				
					</td>		
	</tr>

      </table>
</s:form>
								


<div class="titulo" style="width: 98%;"><s:text name="midas.prestadorservicio.listado.titulo" /></div>
<div id="msjInicio" class="subtituloLeft" style="display:none;"><br/><s:text name="midas.prestadorservicio.msj.entrada" /></div>
<s:iterator value="listPrestadorServicio" var="item"> <!-- here myList contains the list of objects -->  
   <s:property value="%{#item.id}" /><br/>  
   <s:property value="%{#item.numPolizaRC}" /><br/>  
</s:iterator>  	
		
<div id="indicador"></div>
<div id="prestadorDeServicioGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  id="btn_exportar" >
				<a href="javascript: void(0);" onclick="javascript:exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="mostrarPrestadorServicio();"> 
				<s:text name="midas.boton.agregar" /> </a>
			</div>	
		</td>
	</tr>
</table>

	

<script type="text/javascript">
jQuery(document).ready(
		function(){
			var modo = <s:property value="modoAsignar"/>;			
			if(modo == false || modo === 'false'){
				jQuery('#msjInicio').show();				
			}else{
				initGridPrestadorDeServicio(modo);	
			}						 
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
