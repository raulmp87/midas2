<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
      <head>
          <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
            <column id="numPrestador" type="ro"  width="*"	sort="int" ><s:text name="midas.prestadorservicio.numPrestador"/></column>
            <column id="nombre"       type="ro"  width="*"  sort="str" ><s:text name="midas.prestadorservicio.nombre"/></column>   
            <column id="telefono"     type="ro" width="*"   sort="str"  hidden="true">telefono</column>
      </head>
      <s:iterator value="listPrestadorServicio">
            <row >
                <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="personaMidas.nombre"  escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="personaMidas.contacto.telOtro1"  escapeHtml="false" escapeXml="true"/></cell>
            </row>
      </s:iterator>
       
</rows>
