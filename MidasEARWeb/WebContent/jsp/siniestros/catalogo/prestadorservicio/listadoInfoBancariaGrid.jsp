<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>

		</beforeInit>
		
		 	<column id="id" type="ro"  width="200" align="center">Banco</column>
            <column id="numeroCuenta" type="ro"  width="100"  sort="str" align="center">Numero de Cuenta</column>
          	<column id="clabe" type="ro"  width="100"  sort="str" align="center">Clabe</column>
          	<column id="location" type="ro"  width="100"  sort="str" align="center">Location</column>
          	<column id="aba" type="ro"  width="80"  sort="str" align="center">ABA</column>
          	<column id="swift" type="ro"  width="80"  sort="str" align="center">SWIFT</column>
          	<column id="beneficiary" type="ro"  width="100"  sort="str" align="center">Beneficiary</column>
          	<column id="ffc" type="ro"  width="80"  sort="str" align="center">FFC</column>
          	<column id="fechaCambioEstatus" type="ro"  width="*" sort="str" align="center">Fecha de Cambio de Estatus</column>
			<column id="estatus"  type="img" width="*" sort="na" align="center" >Estatus</column> 
	</head>			
	<s:iterator value="listInfoBancaria" status="row">
		<row id="row_<s:property value="id"/>">
			<cell ><s:property value="bankName" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="numeroCuenta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clabe" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="location" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="aba" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="swift" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="beneficiary" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="ffc" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaModEstatus" escapeHtml="false" escapeXml="true"/></cell>		
			<s:if test="estatus == 1" >
				<cell>/MidasWeb/img/ico_green.gif^Activo^javascript: return null;^_self</cell>
			</s:if>
			<s:else>
				<s:if test="consulta == true" >
					<cell>/MidasWeb/img/ico_red.gif^Activo^javascript: return null;^_self</cell>
				</s:if>
				<s:else>
					<cell>/MidasWeb/img/ico_red.gif^Activo^javascript: activar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:else>	
			</s:else>	
		</row>
	</s:iterator>	
</rows>
