<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>


<script src="<s:url value='/js/midas2/siniestros/catalogo/prestadorservicio/prestadorDeServicio.js'/>" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"  type="text/javascript"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>


<script type="text/javascript">
var salvarPrestadorServicioPath = '<s:url action="salvarCatalogo" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var mostrarTipoPrestadorPath = '<s:url action="mostrarTipoPrestador" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var mostrarInfoBancariaPath = '<s:url action="mostrarInfoBancaria" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var buscarPrestadorServicioPath = '<s:url action="mostrarContenedor" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
var tipoPersona = '<s:property value="dto.tipoPersona"/>';
</script>

<style type="text/css">
.label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}

table tr td label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}

.error {
	background-color: red;
	opacity: 0.4;
}
	
.labelTelefono  {
   color:black;
   font-weight: normal;
   text-align: left;
    font-size:7pt;
    width: 88px !important;
}
	
.labelBlack{
 	color:#000000 !important;
     width: 88px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
}

.titulo{
	color:#000000 !important;
}

</style>


<input id="esConsulta" value="<s:property value='consulta' />" type="hidden"/>
<input id="prestadorId" value="<s:property value='entidad.id' />" type="hidden"/>
<!--  contenedor tabs -->
<div id="prestadorServicioTabBar" class="dhtmlxTabBar" hrefmode="ajax-html" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">

	<!-- tab 1 informacion general -->
	<div id="tabInfoGeneral" name="Info General" selected="1" >
		<div id="tipoPrestadorSeleccionado" style="display: none;" ></div>
		<div id="informacionBancariaSeleccionada" style="display: none;"></div>
		
		<s:form id="altaPrestadorServicio" action="mostrarAltaDePrestador" namespace="/siniestros/catalogos/prestadorDeServicio" name="altaPrestadorServicio">
				<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
					<tbody>
		            	<tr>
							<td class="titulo" colspan="4"><s:text name="midas.prestadorservicio.datos.titulo" /></td>
						</tr>
			            <tr>
			                  <td>
			                  		<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130"
										name="dto.numPrestador"
										labelposition="left"
										label = "No. Prestador" 
										size="10"	
										readonly="true"	
										id="txt_numPrestador" 
										disabled="%{consulta}"/>
							 </td>
			                  <td>
			                  <s:if test="consulta==true">
			                  	<s:select id="s_oficina" 
											labelposition="left"
											label = "Oficina" 
											name="dto.oficinaId"
											headerKey="" headerValue="%{getText('midas.general.seleccione')}"
											list="oficinas" listKey="key" listValue="value"  
									  		cssClass="txtfield cajaTextoM2 w130 requerido"
									  		disabled="%{consulta}"/>
							  </s:if>
							  <s:else>
							  	<s:select id="s_oficina" 
											labelposition="left"
											label = "Oficina" 
											name="dto.oficinaId"
											headerKey="" headerValue="%{getText('midas.general.seleccione')}"
		 									list="oficinas" listKey="key" listValue="value"  
									  		cssClass="txtfield cajaTextoM2 w130 requerido jQrequired"
									  		disabled="%{consulta}"/>
							  	
							  </s:else>
			                  		
							 </td>
			                 <td colspan="2" style="width:35%;" >
			                  		<input id="tipo_persona_hidden" type="hidden"/>
			                  		<s:radio list="tiposPersona" value="%{dto.tipoPersona}" 
			                  				 onclick="javascript:onClickTipoPersona();" 
			                  				 id="r_tipoPersona" 
			                  				 name="dto.tipoPersona"
			                  				 cssClass="jQrequired " 
			                  				 disabled="%{consulta}"/>
			                  				 
							  </td>	
							  <td>
							  
							  	<s:if test="consulta==true">
									<s:select id="s_tipo_estatus" 
											labelposition="left"
											label = "Estatus" 
											name="dto.estatus"
											headerKey="" headerValue="%{getText('midas.general.seleccione')}"
											list="estatus" listKey="key" listValue="value"  
									  		cssClass="txtfield cajaTextoM2 w130 requerido"
									  		disabled="%{consulta}"/>					  		
								</s:if>
								<s:else>
									<s:select id="s_tipo_estatus" 
											labelposition="left"
											label = "Estatus" 
											name="dto.estatus"
											headerKey="" headerValue="%{getText('midas.general.seleccione')}"
											list="estatus" listKey="key" listValue="value"  
									  		cssClass="txtfield cajaTextoM2 w130 requerido jQrequired"
									  		disabled="%{consulta}" />
								</s:else>
							  		
							 </td>
			            </tr>
						<tr id="tr_personaFisica">
							<td colspan="2">
			                  <s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w420 requerido "
										name="dto.nombre"
										labelposition="left"
										label = "Nombre(s)" 
										size="50"	
										maxLength="40"
										readonly="%{consulta}"	
										id="txt_nombres" 
										disabled="%{consulta}"/>
										
							 </td>
							 <td>
			                  <s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 requerido " 
										name="dto.apellidoPaterno"
										labelposition="left"
										label = "Apellido Paterno" 
										size="10"		
										maxLength="20"
										readonly="%{consulta}"
										disabled="%{consulta}"		
										id="txt_apellido_paterno"/>
							 </td>
							 <td>
			                  <s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 requerido " 
										name="dto.apellidoMaterno"
										labelposition="left" 
										label="Apellido Materno"
										size="10"		
										maxLength="20"			
										id="txt_apellido_materno"  
										readonly="%{consulta}"
										disabled="%{consulta}"/>
							 </td>
						</tr>
						<tr>
							<td>
								<s:textfield id="txt_fechaAlta"
										 cssClass="textfield" 
										 key="midas.prestadorservicio.fechaAlta"
										 labelposition="left"  
										 size="10" readonly="true"
										 name="dto.fechaDeAlta"/> 
							</td>
							<td colspan="3">
								<s:textfield id="txt_fechaBaja"
										 cssClass="textfield" 
										 key="midas.prestadorservicio.fechaBaja"
										 labelposition="left"  
										 size="10" readonly="true"
										 name="dto.fechaDeBaja"/> 
							</td>
						</tr>
						<tr id="tr_personaMoral">
							<td>
			                  <s:textfield cssClass="txtfield cajaTextoM2 w400 requerido" 
										name="dto.nombreDeLaEmpresa"
										labelposition="right"
										label="Nombre de la Empresa" 
										size="10"					
										maxLength="80"
										id="txt_nombre_de_la_empresa" 
										readonly="%{consulta}"
										disabled="%{consulta}"
										onkeypress="return soloAlfanumericos(this, event, true)" />
							 </td>
							 <td>
			                  <s:textfield cssClass="txtfield cajaTextoM2 w400 requerido" 
										name="dto.nombreComercial"
										labelposition="right"
										label="Nombre Comercial" 
										size="10"			
										maxLength="80"		
										id="txt_nombre_comercial" 
										readonly="%{consulta}" 
										disabled="%{consulta}"
										onkeypress="return soloAlfanumericos(this, event, true)"/>
							 </td>
							 <td colspan="3">
			                  <s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 requerido"
										name="dto.administrador"
										labelposition="right"
										label="Administrador" 
										size="10"				
										maxLength="80"
										value="%{entidad.personaMidas.representanteLegal}"	
										id="txt_administrador" 
										readonly="%{consulta}"
										disabled="%{consulta}" />
							 </td>
						</tr>
						<tr id="tr_fechas">
								<td>
									<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130 "
										labelposition="right"
										label="Fecha de Alta" 
										name="dto.fechaDeAlta"
										size="10"					
										id="txt_fechaDeAlta" 
										readonly="true"
										disabled="%{consulta}"/>
								</td>
							<td colspan="3">
								<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130 "
										labelposition="right"
										name="dto.fechaDeBaja"
										label="Fecha de Baja" 
										size="10"					
										id="txt_fechaDeBaja" 
										readonly="true"
										disabled="%{consulta}"/>
							</td>
						</tr>
			            <tr>
			            	<td colspan="8">
			            		<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="mostrarTipoPrestador('<s:property value="entidad.id" />','<s:property value="consulta" />');">
										<s:text name="midas.prestadorservicio.tipoPrestador.button" />
									</a>
								</div>
							</td>
			            </tr>
					</tbody>	  
		      	</table>		      
			  	</br>
			<s:if test="%{dto.tienePermisoCA == false}">
			      <table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
			      	<tbody>
			      		<tr>
			                  <td class="titulo" colspan="3"><s:text name="midas.prestadorservicio.polizaRC.titulo" /></td>
			            </tr>
			            <tr> 
			            	  <td>
								<s:textfield cssClass="txtfield cajaTextoM2 w200 requerido" 
											name="dto.numPoliza"
											labelposition="left" 
											label="Número de Póliza"
											size="10"	
											maxLength="50"
											id="txt_numeroPoliza"
											readonly="%{consulta}" 
											disabled="%{consulta}" />
							</td>
							<td>
									<s:if test="consulta==true">
										<s:textfield cssClass="txtfield cajaTextoM2 w130 requerido"
											labelposition="right"
											label="Cía. de Seguros" 
											size="10"					
											id="txt_ciaSeguros" 
											readonly="%{consulta}"
											disabled="%{consulta}"
											value="%{entidad.ciaSegurosPolizaRC}" />							
									</s:if>
									<s:else>
										<s:select id="s_ciaSeguros" 
											labelposition="left"
											label = "Cía. de Seguros" 
											name="dto.ciaDeSeguros"
											headerKey="" headerValue="%{getText('midas.general.seleccione')}"
									  		list="ciaSeguros" listKey="key" listValue="value"  
									  		cssClass="txtfield cajaTextoM2 w200 requerido" />
									</s:else>
							  		
							 </td>
							
							<td colspan="3">
								<s:if test="consulta==true">
									<s:textfield cssClass="txtfield cajaTextoM2 w130 requerido"
											labelposition="right"
											label="Término de Vigencia"
											size="10"					
											id="txt_terminoVigencia" 
											readonly="true"
											value="%{entidad.vigenciaPolizaRC}" />					
								</s:if>
								<s:else>
									<sj:datepicker name="dto.terminoVigencia"
												label="Término de Vigencia"
										  labelposition="left"
											changeMonth="true"
											 changeYear="true"				
											buttonImage="/MidasWeb/img/b_calendario.gif"
											buttonImageOnly="true" 
				                        			 id="dp_terminoVigencia"
											  maxlength="10" cssClass="txtfield cajaTextoM2 w130 requerido"
												   size="12"
											 onkeypress="return soloFecha(this, event, false);"
											onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>
								</s:else>
							</td>
			            </tr>
			      	</tbody>
			      </table>
			  </s:if>		      	      
		      <br/>
		      <div id="componenteDireccionMidas">
			<table width="98%" bgcolor="white" align="center"
				class="contenedorConFormato">
				<tbody>
					<tr>
						<td class="titulo"><s:text name="midas.prestadorservicio.domicilio.titulo" /></td>
					</tr>
					<tr>
						<td colspan="3">
							<s:action name="combosDireccion" var="combosDireccion"	namespace="/componente/direccionSiniestroMidas"	ignoreContextParams="true" executeResult="true">
								<s:param name="idPaisName">dto.idPaisString</s:param>
								<s:param name="idEstadoName">dto.idEstadoString</s:param>
								<s:param name="idCiudadName">dto.idMunicipioString</s:param>
								<s:param name="idColoniaName">dto.nombreColonia</s:param>
								<s:param name="calleName">dto.nombreCalle</s:param>
								<s:param name="numeroName">dto.nombreCalleNumero</s:param>
								<s:param name="numeroIntName">dto.numeroInterno</s:param>
								<s:param name="cpName">dto.codigoPostal</s:param>
								<s:param name="nuevaColoniaName">dto.nombreColoniaDiferente</s:param>
								<s:param name="idColoniaCheckName">idColoniaCheck</s:param>
								<s:param name="referenciaName">dto.referencia</s:param>
								<s:param name="labelPais">País</s:param>
								<s:param name="labelEstado">Estado</s:param>
								<s:param name="labelCiudad">Municipio</s:param>
								<s:param name="labelColonia">Colonia</s:param>
								<s:param name="labelCalle">Calle</s:param>
								<s:param name="labelNumero">Número Ext.</s:param>
								<s:param name="labelNumeroInt">Número Int.</s:param>
								<s:param name="labelCodigoPostal">Código Postal</s:param>
								<s:param name="labelReferencia">Referencia</s:param>
								<s:param name="labelPosicion">left</s:param>
								<s:param name="componente">3</s:param>
								<s:param name="readOnly" value="%{#readOnly}"></s:param>
								<s:param name="requerido" value="0"></s:param>
								<s:param name="incluirReferencia" value="true"></s:param>
								<s:param name="enableSearchButton" value="true"></s:param>
							</s:action>
						</td>
					</tr>
					<tr >
					<td colspan="3">
					<div class="btn_back w200" style="display: inline; float: right;">
								<a href="javascript: void(0);" onclick="mostrarInfoBancaria('<s:property value="entidad.id" />',<s:property value="consulta" />);"> 
								<s:text name="midas.prestadorservicio.docBancaria.button" />
								</a>
						</div>
					</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		      <br/>
			<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
				<tbody>
		            <tr>
		                  <td class="titulo" colspan="3"><s:text name="midas.prestadorservicio.datosLocalizacion.titulo" /></td>
		            </tr>
		            
		            <tr> 
		            	<td>
		            	 <table>
		            	 	<tr>
		            	 		<td>
		            	 			<label class="labelBlack"><s:text name="midas.prestadorservicio.datos.telefono1" /></label>
		            	 		</td>
		            	 		<td>
		            	 			<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w30 numeric jQrequired"   
										name="dto.telefono1Lada"
										labelposition="left" 
										maxlength="3"	
										id="txt_telefono_1_lada"  
										readonly="%{consulta}"
										disabled="%{consulta}"/>
		            	 		</td>
		            	 		<td>
		            	 			<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w130 numeric jQrequired"  
										name="dto.telefono1"
										labelposition="left" 
										maxlength="8"	
										id="txt_telefono_1"  
										readonly="%{consulta}"
										disabled="%{consulta}"/>
		            	 		</td>
		            	 	</tr>
		            	 </table>	
		            	  
							
						</td>
						<td>
							<table>
								<tr>
									<td>
										<label class="labelBlack"><s:text name="midas.prestadorservicio.datos.telefono2" /></label>
									</td>
									<td>
										<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w30 numeric" 
										name="dto.telefono2Lada"
										labelposition="left" 
										maxlength="3"		
										id="txt_telefono_2_lada" 
										readonly="%{consulta}" 
										disabled="%{consulta}"/>
									</td>
									<td>
										<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w130 numeric" 
										name="dto.telefono2"
										labelposition="left" 
										maxlength="8"		
										id="txt_telefono_2" 
										readonly="%{consulta}" 
										disabled="%{consulta}"/>
									</td>
								</tr>
							</table>
						</td>	
						<td>
							<s:textfield cssClass="txtfield jQemail jQrestrict cajaTextoM2 w160" 
										name="dto.emailPrincipal"
										labelposition="left" 
										label="E-mail Principal"
										size="10"
										maxLength="60"	
										id="txt_email_principal" 
										readonly="%{consulta}" 
										disabled="%{consulta}"/>
						</td>						
		            </tr>
		            <tr>
		            	<td>
		            		<table>
								<tr>
									<td>
										<label class="labelBlack"><s:text name="midas.prestadorservicio.datos.celular" /></label>
									</td>
									<td>
										<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w30 numeric" 
										name="dto.celularLada"
										labelposition="left" 
										maxlength="3"
										id="txt_celular_lada"  
										readonly="%{consulta}"
										disabled="%{consulta}"/>
									</td>
									<td>
										<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w130 numeric" 
										name="dto.celular"
										labelposition="left" 
										maxlength="8"
										id="txt_celular"  
										readonly="%{consulta}"
										disabled="%{consulta}"/>
									</td>
								</tr>
							</table>
						</td>
						<td>
						<table>
						<tr>
							<td>
								<label class="labelBlack"><s:text name="midas.prestadorservicio.datos.otros" /></label>
							</td>
							<td>
								<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w30" 
										name="dto.otrosLada"
										labelposition="left" 
										maxlength="3"		
										id="txt_otros_lada" 
										readonly="%{consulta}"
										disabled="%{consulta}" />
							</td>
							<td>
								<s:textfield cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w130" 
										name="dto.otros"
										labelposition="left" 
										maxlength="8"		
										id="txt_otros_lada" 
										readonly="%{consulta}"
										disabled="%{consulta}" />
							</td>
						</tr>
						</table>
							
							
						</td>
						<td>
							<s:textfield cssClass="txtfield jQemail jQrestrict cajaTextoM2 w160" 
										name="dto.emailAdicional"
										labelposition="left" 
										label="E-mail Adicional"
										size="10"	
										maxLength="60"
										id="txt_email_adicional" 
										readonly="%{consulta}"
										disabled="%{consulta}" />
						</td>							
		            </tr>
		            <tr id="tr_datosComplementariosPersona"> 
						<td>
		            	  <s:textfield cssClass="txtfield jQCURP jQrestrict cajaTextoM2 w160 "
										name="dto.curp"
										labelposition="left"
										label="CURP"
										maxlength="18"				
										id="txt_curp" 
										readonly="%{consulta}"
										disabled="%{consulta}" 
										onkeypress="return soloAlfanumericos(this, event, false)" />	
						</td>
						<td>
						<s:if test="consulta==true">
						  	<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w130"
										labelposition="right"
										label="Fecha Nacimiento"
										size="10"					
										id="txt_fecNacimiento" 
										readonly="true"
										value="%{entidad.personaMidas.fechaNacimiento}" />	
						</s:if>
						<s:else>
							
							<sj:datepicker name="dto.fechaNacimiento"
											label="Fecha Nacimiento"
									  labelposition="left"
										changeMonth="true"
										 changeYear="true"
										 yearRange="-100:+0"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" 
			                        			 id="dp_fechaNacimiento"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w160"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										readonly="%{consulta}">
							</sj:datepicker>
						</s:else>
						</td>
						<td rowspan="2"/>
		            </tr>
		            <tr> 
						 <td colspan="">
							<s:textfield cssClass="txtfield jQRFC jQrequired jQrestrict cajaTextoM2 w160" 
										name="dto.rfc"
										labelposition="left" 
										label="RFC"
										maxlength="13"
										id="txt_rfc" 
										readonly="%{consulta}"
										disabled="%{consulta}"
										onkeypress="return soloAlfanumericos(this, event, false)" />
						</td>
						<td colspan="2" nowrap="nowrap">
							<div style="display:inline;">
								<div style="position:relative">
									<s:textfield cssClass="txtfield jQalphanumeric jQrestrict cajaTextoM2 w160" 
										name="claveUsuarioPrestador"
										labelposition="left" 
										label="Clave Usuario"
										maxlength="8"
										id="claveUsuarioPrestador" 
										readonly="true"
										disabled="%{consulta}"										
										 />
								</div>							
								<div class="btn_back w60" style="position:relative;top:-23px;left:240px;" >
									<s:if test="%{!consulta}">
										<m:tienePermiso nombre="FN_M2_SN_Generar_Usuario_Prestador">
											<a href="javascript: void(0);" onclick="generarUsuarioPrestador();"> 
												<s:text name="midas.prestadorservicio.boton.generar" />
											 </a>
										 </m:tienePermiso>
									 </s:if>
								</div>								
							</div>	
						</td>		
		            </tr>
		      	</tbody>
		      </table>
		      <br/>
		      <s:if test="%{dto.tienePermisoCA == false}">
			      <table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
			      	<tbody>
			      		 <tr>
			                  <td class="titulo" colspan="3"><s:text name="midas.prestadorservicio.datos.pago" /></td>
			            </tr>
			            <tr>
			                 <td><label class="labelBlack"><s:text name="midas.prestadorservicio.servicios.profesionales" /></label></td>
			            </tr>
			            <tr> 
			            	<td>
			            	    <s:checkbox onclick="javascript:validaServiciosProfesionales();" cssClass="txtfield" id="chkProfesionales" name="serviciosProfesionales" readonly="%{consulta}" disabled="%{consulta}"  />
			            	    <s:hidden id="profesionales" name="dto.serviciosProfesionales" />
							</td>
			            </tr>
			      	</tbody>
			      </table>
		      </s:if>
		      </br>
		      <table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		      	<tbody>
		      		 <tr>
		                  <td class="titulo" colspan="3"><s:text name="Compensaciones Adicionales" /></td>
		            </tr>
		            <tr>
		                 <td><label class="labelBlack"><s:text name="Ramos" /></label></td>
		            </tr>
		            <tr> 
		            	<td width="12%">
		            	    <s:checkbox onclick="javascript:validaAutosCa();" cssClass="txtfield" id="chkAutosCa" name="autosCa" readonly="%{consulta}" disabled="%{consulta}"  />
		            	    <s:hidden id="autosCa" name="dto.autosCa" />
		            	    <label class="labelBlack"><s:text name="Autos" /></label>
						</td>
						<td width="12%">
		            	    <s:checkbox onclick="javascript:validaDaniosCa();" cssClass="txtfield" id="chkDaniosCa" name="daniosCa" readonly="%{consulta}" disabled="%{consulta}"  />
		            	    <s:hidden id="daniosCa" name="dto.daniosCa" />
		            	    <label class="labelBlack"><s:text name="Danios" /></label>
						</td>
						<td width="20%">
		            	    <s:checkbox onclick="javascript:validaVidaCa();" cssClass="txtfield" id="chkVidaCa" name="vidaCa" readonly="%{consulta}" disabled="%{consulta}"  />
		            	    <s:hidden id="vidaCa" name="dto.vidaCa" />
		            	    <label class="labelBlack"><s:text name="Vida" /></label>
						</td>
						<td>
							<label class="labelBlack"><s:text name="Forma de Pago" /></label>
							<s:radio list="formaPago" 
			                  				 id="formaPagoCa" 
			                  				 name="dto.formaPagoCa"
			                  				 cssClass="jQrequired " 
			                  				 disabled="%{consulta}"/>			                  				
						</td>
						
		            </tr>
		      	</tbody>
		      </table>
		      </br>
		      <table width="98%">
		       <tr>		       
		       <td colspan="4">		
								<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
									<tr>
										<td>
											<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
												<a href="javascript: void(0);" onclick="cerrar();"> 
												<s:text name="midas.boton.cerrar" /> </a>
											</div>	
											
											<s:if test="consulta==false">
												<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
													<a href="javascript: void(0);" onclick="salvarPrestadorServicio();"> 
													<s:text name="midas.boton.guardar" /> </a>
												</div>	
											
											
												<div class="btn_back w140" style="display: inline; float: right; visibility:%{consulta} " id="icon_agregar" >
													<a href="javascript: void(0);" onclick="ventanaFortimax('PREST_SERVICIO','' , 'FORTIMAX',  jQuery('#prestadorId').val() ,   jQuery('#txt_rfc').val()  );"> <s:text
													name="Documentos" /> </a>
												</div>	
											</s:if>
										</td>							
									</tr>
								</table>				
							</td>
		            	
		            </tr>
		      </table>       
		</s:form>
	</div> <!-- Cierre TAB 1 Info General -->
	<s:if test="entidad.id != null">
	
	
	<!-- seccion para incluir tabs adicionales. Ejemplos en detalleNegocio.jsp  -->
	
		<!-- ejemplo
	
		<div id="tabComisiones" name="Comisiones" >
			asdas
		</div>
		<div id="tabBonos" name="Bonos" >
			asdas
		</div>
		
		-->
		
		
	</s:if>	
</div> <!-- Cierre contenedor TABS -->

<script type="text/javascript">
	dhx_init_tabbars();
	function activaCheksCompensaciones(){
		if(jQuery("#autosCa").val() == 1 ){
			jQuery("#chkAutosCa").attr('checked',true);
		}else{
			jQuery("#chkAutosCa").attr('checked',false);
		}
		if(jQuery("#daniosCa").val() == 1 ){
			jQuery("#chkDaniosCa").attr('checked',true);
		}else{
			jQuery("#chkDaniosCa").attr('checked',false);
		}
		if(jQuery("#vidaCa").val() == 1 ){
			jQuery("#chkVidaCa").attr('checked',true);
		}else{
			jQuery("#chkVidaCa").attr('checked',false);
		}
	}
	jQuery(document).ready(function(){
		jQuery(".numeric").keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g,'');
        });
		ocultarDivsTipoPersona();
//		onClickTipoPersona( );
// 		disableAddressInfo(<s:property value="consulta" escapeHtml="false" escapeXml="true"/>);
// 		jQuery(".requerido").blur(
// 			function () {
// 				validaRequeridos();
// 			}
// 		);
	 	validaComboTipoPersona(<s:property value="modoEditar"/>);
	 	validaCompenenteDireccion(<s:property value="consulta"/>);
	 	validaOtraColonia();
	 	init();
	 	activaCheksCompensaciones()
		
	});		
</script>	

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>			