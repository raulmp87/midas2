<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/catalogo/prestadorservicio/prestadorDeServicio.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<input id="prestadorId" value="<s:property value='prestadorId' />" type="hidden"/>
<input id="consulta" value="<s:property value='consulta' />" type="hidden"/>

<script type="text/javascript">
var buscarInfoBancariaPath = '<s:url action="mostrarContenedorInfoBancaria" namespace="/siniestros/catalogos/prestadorDeServicio"/>';
function ocultarTrsMoneda( ){
		jQuery("#tr_datosBanco_inf").hide();
		jQuery("#tr_datosBanco").hide();
}
function ocultarTipoMoneda( ){
	var value = jQuery("#tipoMoneda option:selected").val();
	if(value == "484"){
		jQuery("#tr_datosBanco_inf").hide();
		jQuery("#tr_datosBanco").hide();
		jQuery("#txt_clabe").removeAttr("disabled");
	}else if(value == "840"){
		jQuery("#tr_datosBanco_inf").show();
		jQuery("#tr_datosBanco").show();
		jQuery("#txt_clabe").attr("disabled","disabled");
		jQuery("#txt_numeroCuenta").toggleClass("jQrequired");
	}
}
jQuery(document).ready(function(){
	 	ocultarTrsMoneda();
		
	})
</script>


<s:hidden name="actionNameOrigen"  id="actionNameOrigen"/>
<s:hidden name="prestadorId"  id="prestadorId"/>

<div id="informacionBancaria"> 

<script type="text/javascript">
	jQuery(window).load(
		function(){

			if( jQuery(parent.informacionBancaria).length ){
				jQuery("#informacionBancaria").html(jQuery(parent.informacionBancaria));
			}
			
			
			if(<s:property value="consulta"/>){				
				jQuery('#btnGuardar').hide();
			}else{				
				jQuery('#btnGuardar').show();
			}
		}

	);
</script>
<s:form id="infoBancariaForm" >
	<s:hidden name="consulta"/>	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.prestadorservicio.infobancaria.titulo" />	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tr>
				<td>
				<s:if test="consulta==true">
					<s:select id="s_banco" 
								   labelposition="left" 
											name="informacionBancaria.bancoId"
											label="Banco"
									       	list="bancos"  
						  					cssClass="txtfield jQrequired"
						  					disabled="%{consulta}" />
				</s:if>
				<s:else>
					<s:select id="s_banco" 
								   labelposition="left" 
											name="informacionBancaria.bancoId"
											label="Banco"
											headerValue="%{getText('midas.general.seleccione')}"
									       	list="bancos" headerValue="%{getText('midas.general.seleccione')}"
						  					cssClass="txtfield jQrequired"
						  					disabled="%{consulta}" 
						  					onchange="requiereGuardar();"/>
				
				</s:else>
					 	
				</td>
				<td>
				<s:if test="consulta==true">
					<s:select id="tipoMoneda" 
								   			labelposition="left" 
											name="informacionBancaria.tipoMoneda"
											label="Moneda"
									        list="tipoMoneda" 
						  					cssClass="txtfield jQrequired"
						  					disabled="%{consulta}" 
						  					/>
				</s:if>
				<s:else>
					<s:select id="tipoMoneda" 
								   			labelposition="left" 
											name="informacionBancaria.tipoMoneda"
											label="Moneda"
											headerValue="%{getText('midas.general.seleccione')}"
									       	list="tipoMoneda"
									       	headerValue="%{getText('midas.general.seleccione')}"
						  					cssClass="txtfield jQrequired"
						  					disabled="%{consulta}"
						  					onchange="ocultarTipoMoneda();"/>
				
				</s:else>
					 	
				</td>	
				<td><s:textfield cssClass="txtfield jQrestrict numeric jQrequired" 
									label="Numero de Cuenta"
									name="informacionBancaria.numeroCuenta"
									labelposition="left" 
									maxlength="16"
									id="txt_numeroCuenta"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
				<td><s:textfield cssClass="txtfield jQrestrict numeric jQrequired" 
									label="CLABE"
									name="informacionBancaria.clabe"
									labelposition="left" 
									maxlength="18"					
									id="txt_clabe"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>	
			</tr>
			<tr id="tr_datosBanco">
				<td><s:textfield cssClass="cajaTextoM2 w150 " 
									label="Location"
									name="informacionBancaria.location"
									labelposition="left" 
									maxlength="30"
									id="txt_Location"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
				<td><s:textfield cssClass="cajaTextoM2 w150 " 
									label="ABA"
									name="informacionBancaria.aba"
									labelposition="left" 
									maxlength="15"
									id="txt_Aba"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
				<td><s:textfield cssClass="cajaTextoM2 w150 " 
									label="SWIFT"
									name="informacionBancaria.swift"
									labelposition="left" 
									maxlength="15"
									id="txt_Swift"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
			</tr>
			<tr id="tr_datosBanco_inf" >
				<td><s:textfield cssClass="cajaTextoM2 w150 " 
									label="Beneficiary"
									name="informacionBancaria.beneficiary"
									labelposition="left" 
									maxlength="15"
									id="txt_Beneficiary"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
				<td><s:textfield cssClass="cajaTextoM2 w150 " 
									label="FFC"
									name="informacionBancaria.ffc"
									labelposition="left" 
									maxlength="50"
									id="txt_Ffc"
									disabled="%{consulta}"
									onchange="requiereGuardar();"/>
				</td>
			</tr>
			<tr>		
				<td colspan="4">		
					<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
								<div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="salirInfoBancaria(<s:property value="consulta"/>);"> 
									<s:text name="midas.boton.cerrar" /> </a>
								</div>
								<s:if test="!consulta">								
									<div id="btnGuardar" class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="guardarDocBancaria();"> 
										<s:text name="midas.boton.guardar" /> </a>
									</div>
								</s:if>	
								<div class="btn_back w150" style="display: inline; float: right;">
									<a href="javascript: void(0);" onclick="verLigaDocumento('PREST_SERVICIO','' , 'FORTIMAX', prestadorId.value, 'x', 'Documentacion Bancaria');" > 
									<s:text name="Documentacion Bancaria" /> </a> 
								</div>																
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		</table>
	</div>
</s:form>
</div>
<!-- <div id="indicador"></div> -->
<div id="listadoInfoBancGrid" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<br/>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">
 	jQuery(document).ready(function(){ 
 		mostrarListadoInfoBancaria('<s:property value="prestadorId" />','<s:property value="consulta" />'); 
	});		 
	jQuery(".numeric").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g,'');
	});
</script>



