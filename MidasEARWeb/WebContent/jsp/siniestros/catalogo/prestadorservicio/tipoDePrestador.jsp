<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/prestadorservicio/prestadorDeServicio.js'/>" type="text/javascript"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<style type="text/css">

.checkboxLabel{
 	color:#000000 !important;
     width: 88px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
	vertical-align: top;
}

.titulo{
	color:#000000 !important;
}
</style>

<script type="text/javascript">
	jQuery(window).load(
		function(){
			if( jQuery(parent.tipoDePrestador).length ){					
				jQuery("#tipoDePrestador").html(jQuery(parent.tipoDePrestador));
			}
			jQuery('input[name=strTipoPrestadorSelected]').before("<tr><td>");
			jQuery('input[name=strTipoPrestadorSelected]').after("</tr></td>");		

			if(<s:property value="consulta"/>){
				jQuery('input[name=strTipoPrestadorSelected]').attr("disabled","disabled");
				jQuery('#btnGuardar').hide();
			}else{
				jQuery('input[name=strTipoPrestadorSelected]').removeAttr("disabled");
				jQuery('#btnGuardar').show();
			}
		}
	);
</script>

<div id="tipoDePrestador"> 
<s:form id="formTiposPrestador" >
	<s:hidden name="consulta"/>	
	<table id="tiposPrestadorTable" width="98%" bgcolor="white" align="center" class="contenedorConFormato">
			<tr>
                  <td class="titulo" colspan="6">Tipos de Prestador</td>
                  
            </tr>

		<tr>
			<td>				 					
				<s:checkboxlist list="tiposPrestador" name ="strTipoPrestadorSelected" value="tiposPrestadorParaConsultar" />									
		 	</td>
		</tr>
		<tr>
			<td colspan="2">
				<table id="tiposPrestador" style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td>
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);" onclick="salirTipoPrestador(<s:property value="consulta"/>);"> 
										<s:text name="midas.boton.cerrar" /> </a>
									</div>
								</td>
								<td>
									<s:if test="consulta!=true">
										<div id="btnGuardar" class="btn_back w140" style="display: inline; float: right;">
											<a href="javascript: void(0);" onclick="guardarTipoDePrestador();"> 
											<s:text name="midas.boton.guardar" /> </a>
										</div>	
									</s:if>	
								</td>							
							</tr>
				</table>
			</td>	
		</tr>
     </table>
</s:form>

</div>




<!-- 			<td> -->
<%-- 				<a name= "<s:property value= "area.nombre" escapeHtml="false" escapeXml="true" />"  --%>
<%-- 				id = "linkArea_<s:property value= "area.id" escapeHtml="false" escapeXml="true"/>" --%>
<!-- 				href="javascript: void(0);"  -->
<!-- 				style="font-size: 9px;" class="linkArea" -->
<%-- 				onclick="changeTabArea(<s:property value= "area.id" escapeHtml="false" escapeXml="true"/>);"> --%>
<%-- 				<s:property value= "area.nombre" escapeHtml="false" escapeXml="true" /></a>  --%>
					
<!-- 			</td> -->