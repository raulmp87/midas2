<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="70" align="center" sort="int"> <s:text name="midas.suscripcion.solicitud.autorizacion.numSolicitud" />   </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.general.estatus" />  </column>
		<column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noReporte" />  </column>
		<column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.numeroFolio" />  </column>
		<column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.poliza.renovacionmasiva.numPoliza" />  </column>
		<column  type="ro"  width="50"  align="center" sort="int"> <s:text name="midas.emision.consulta.siniestro.numeroinciso" />  </column>
	    <column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.suscripcion.solicitud.autorizacion.usuarioSolicitante" />  </column>
	    <column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.suscripcion.solicitud.autorizacion.usuarioAutorizador" />  </column>
	    <column  type="ro"  width="90"  align="center" sort="date_custom"> <s:text name="midas.suspensiones.fechaSolicitud" />  </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.autorizado" />  </column>
	    <column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.emision.consulta.agente.oficina" />  </column>
	    <column  type="ro"  width="120"  align="center" sort="str"> <s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.tipoSolicitud" />  </column>
	    <column  type="ro"  width="120"  align="center" sort="str"> <s:text name="Comentarios" />  </column>
	    
	    
	    <column  type="ro"  width="90"  align="center" sort="int"> <s:text name="Estimacion Actual" />  </column>
	    <column  type="ro"  width="90"  align="center" sort="int"> <s:text name="Estimacion Nueva" />  </column>
	    <column  type="ro"  width="90"  align="center" sort="int"> <s:text name="Reserva" />  </column>
	    <column  type="img"   width="40" sort="na" align="center" >  </column>
	    <column  type="img"   width="40" sort="na" align="center" >  </column>
	    
		
		
	</head>

	<% int a=0;%>
	<s:iterator value="resultados">
		<% a+=1; %>
		<row id="<s:property value="#row.index"/>">
		
		
		    <cell><s:property value="idTipoSolicitud" escapeHtml="false" escapeXml="true"/></cell>
		    
		     <s:if test="estatusSolicitud==2">
		    	<cell> Terminada</cell>
		    </s:if>
		    
		    <s:if test="estatusSolicitud==1">
		    	<cell> Pendiente</cell>
		    </s:if>
		    
		    
		    <cell><s:property value="numReporteCabina" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="noPaseAtencion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="codUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="codUsuarioAutorizador" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		    
		    <s:if test="estatusTipoSolicitud==2">
		    	<cell> Rechazado</cell>
		    </s:if>
		    
		    <s:if test="estatusTipoSolicitud==1">
		    	<cell> Aprobado</cell>
		    </s:if>
		    
		     <s:if test="estatusTipoSolicitud==0">
		    <cell> Pendiente</cell>
		    </s:if>
		    
		    <cell><s:property value="oficina" escapeHtml="false" escapeXml="true"/></cell>
		   	<cell><s:property value="tipoSolicitud" escapeHtml="false" escapeXml="true"/></cell>
		     <cell><s:property value="comentarios" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property  value="%{getText('struts.money.format',{estimacionActual})}" escapeHtml="false" escapeXml="true"  /></cell>
		    <cell><s:property  value="%{getText('struts.money.format',{estimacionNueva})}" escapeHtml="false" escapeXml="true"  /></cell>
		    <cell><s:property  value="%{getText('struts.money.format',{reserva})}" escapeHtml="false" escapeXml="true"  /></cell>
		     <s:if test="estatusSolicitud!=2">
		    	<cell>../img/icons/ico_terminar.gif^Aprobar^javascript:autorizarSolicitud(<s:property value="idSolicitud" escapeHtml="false" escapeXml="true"/>,"<s:property value="nombreTipoSolicitud" escapeHtml="false" escapeXml="true"/>",<s:property value="idTipoSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
				<cell>../img/icons/ico_rechazar2.gif^Rechazar^javascript:cancelarSolicitud(<s:property value="idSolicitud" escapeHtml="false" escapeXml="true"/>,"<s:property value="nombreTipoSolicitud" escapeHtml="false" escapeXml="true"/>",<s:property value="idTipoSolicitud" escapeHtml="false" escapeXml="true"/>)^_self</cell>						
		    </s:if>
		    

			
		</row>
	</s:iterator>
	
</rows>