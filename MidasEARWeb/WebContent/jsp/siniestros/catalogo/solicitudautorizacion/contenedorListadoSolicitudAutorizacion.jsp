<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/solicitudautorizacion/solicitudAutorizacion.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<s:hidden id="urlRedirect" ></s:hidden>

<s:form  id="catSolicitudAutorizacionForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	
	
	<table  id="filtrosM2" width="98%">
		<tr>
			<td class="titulo" colspan="6"><s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.tituloBusqueda" /></td>
		</tr>
		
		<tr>
			<th width="90px"> <s:text name="midas.suscripcion.solicitud.autorizacion.numSolicitud" /> </th>
			<td width="273px"> <s:textfield name="filtroBusqueda.numeroSolicitud" id="txtClave" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
			<th><s:text name="midas.suscripcion.solicitud.solicitudPoliza.fecha" /></th>
			<td><sj:datepicker name="filtroBusqueda.fechaSolicitudDe"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			
			
			<th ><s:text name="midas.suscripcion.solicitud.solicitudPoliza.fechaA" /></th>
			<td><sj:datepicker name="filtroBusqueda.fechaSolicitudA"
					id="txtFechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			
			
		</tr>
		
		
		<tr>
		
			<th width="90px"><s:text name="midas.poliza.renovacionmasiva.numPoliza" /> </th>
			<td><s:textfield name="filtroBusqueda.numeroPoliza" id="txtClave" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
			<th width="90px"><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioSolicitante" /> </th>
			<td><s:textfield name="filtroBusqueda.usuarioSolicitante" id="txtClave" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
			<th width="90px"><s:text name="midas.emision.consulta.agente.oficina" /> </th>
			<td>  <s:select list="oficinasMap" name="filtroBusqueda.oficina"  cssClass="cajaTexto w200 alphaextra" headerKey="" headerValue="Seleccione ..."></s:select></td>

		</tr>
		
		
		<tr>
		
			<th width="90px"><s:text name="midas.general.estatus" /> </th>
			<td> <s:select   list="estatusMap" name="filtroBusqueda.estatus"  cssClass="cajaTexto w200 alphaextra" headerKey="" headerValue="Seleccione ...">			
			</s:select></td>
			
			<th width="90px"><s:text name="midas.suscripcion.solicitud.autorizacion.usuarioAutorizador" /> </th>
			<td><s:textfield name="filtroBusqueda.usuarioAutorizador" id="txtClave" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
			<th width="90px"><s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.tipoSolicitud" /> </th>
			<td> <s:select   list="tipoSolicitudMap" name="filtroBusqueda.tipoSolicitud"  cssClass="cajaTexto w200 alphaextra" headerKey="" headerValue="Seleccione ...">			
			</s:select></td>

		</tr>
		
		<tr>
		
			<th width="90px"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noReporte" /> </th>
			<td><s:textfield name="filtroBusqueda.noReporteCabina" id="noReporte" onblur="validaFormatoSiniestro(this);" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
			<th width="90px"><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.numeroFolio" /> </th>
			<td><s:textfield name="filtroBusqueda.noFolio" id="noFolio" onblur="validaFormatoSiniestro(this);" cssClass="cajaTexto w200 alphaextra"></s:textfield></td>
			
		</tr>		
		
		<tr>		
		<td colspan="6">		
			<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td  class= "guardar">
						<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
							 <a href="javascript: void(0);" onclick="realizarBusqueda();" >
							 <s:text name="midas.boton.buscar" /> </a>
						</div>
												<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
							<a href="javascript: void(0);" onclick="limpiarFormularioSolicitudAut();"> 
							<s:text name="midas.boton.limpiar" /> </a>
						</div>	
						
					</td>							
				</tr>
			</table>				
		</td>		
	</tr>
		


	</table>
	<br>
	
	<div class="titulo"><s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.tituloResultadoBusqueda" /></div>
	<div id="indicador"></div>
	<div id="solicitudAutorizacionGrid" style="width:98%;height:340px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Consultar Asegurado' title='Consultar Asegurado' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			
		</td>
	</tr>
</table>	
	

</s:form>


<script type="text/javascript">
    initGridsSolicitudAutorizacion();
</script>


