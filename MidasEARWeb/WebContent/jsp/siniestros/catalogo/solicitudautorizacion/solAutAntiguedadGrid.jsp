<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="140"  align="center" sort="str"> <s:text name="Oficina" />  </column>
		<column  type="ro"  width="220"  align="center" sort="str"> <s:text name="Cobertura" />  </column>
		<column  type="ro"  width="100"  align="center" sort="str"> <s:text name="Siniestro" />  </column>
		<column  type="ro"  width="190"  align="center" sort="str"> <s:text name="Termino Ajuste" />  </column>
	    <column  type="ro"  width="92"  align="center" sort="str"> <s:text name="Tipo Ajuste" />  </column>
	    <column  type="ro"  width="80"  align="center" sort="int"> <s:text name="Monto Ajuste" />  </column>
	    <column  type="ro"  width="90"  align="center" sort="int"> <s:text name="Meses de Antiguedad" />  </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="Estatus" />  </column>
	    <column  type="ch"  width="80"  align="center" > <s:text name="Autorizar" />  </column>
	    <column  type="ch"  width="55"  align="center" > <s:text name="Rechazar" />  </column>
	    <column  type="img" width="55" sort="na" align="center">#cspan</column>	   
	</head>

	
	<s:iterator value="solicitudes">
		<row id="<s:property value="id"/>">
			
			<cell><s:property value="oficinaDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.siniestroCabina.numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="terminoAjusteDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoAjusteDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="montoAjuste" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="mesesAntiguedad" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell></cell>
		    <cell></cell>
		    <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarReporte(<s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    <userdata name="autoriza"><s:property value="autoriza" escapeHtml="false" escapeXml="true"/></userdata>
		    
		</row>
	</s:iterator>
	
</rows>