<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c" %>



<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/catalogo/solicitudautorizacion/solicitudAutorizacion.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: black;
	margin:0px;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:form id="alertaCIAForm" name="alertaCIAForm"  class="floatLeft">
<div id="contenido_AlertCia" style="width:99%;position: relative;">
	<s:hidden name="idSolicitudAutorizacion" id="idSolicitudAutorizacion"/>
	<s:hidden name="tipoSolicitud" id="tipoSolicitud"/>
	<s:hidden name="estatus" id="estatus"/>
	
	<p class="estilodias">
		<b>Acción: </b><s:text name="nombreAccion" />
	</p>
	<p class="estilodias">
		<b>Solicitud:</b><s:text name="idSolicitudPorTipoAutorizacion" />
	</p>
	<textarea  name="comentarios" cols="84" rows="5" id="comentarios" class="textarea jQrequired" maxlength="400"> </textarea>

	
	<div id="divInferior" style="width: 100% !important;" class="floatLeft">			
		<div class="btn_back w80"
			style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="cerrarComentarios();"> <s:text
				name="Cerrar" />&nbsp;&nbsp;<img
				align="middle" border='0px' alt='Consultar'
				title='Cerrar' src='/MidasWeb/img/common/b_borrar.gif'
				style="vertical-align: left;" /> 
			</a>
		</div>
		<div class="btn_back w80"
			style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="guardarComentarios();"> <s:text
				name="Continuar" />&nbsp;&nbsp;<img
				align="middle" border='0px' alt='Consultar'
				title='Guardar' src='/MidasWeb/img/common/b_siguiente.gif'
				style="vertical-align: left;" /> 
			</a>
		</div>
		
	</div> 
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script>
	//jQuery(document).ready(function() {
	//	buscarRecuperacionesVencer();
		
//	});
	
</script>
</div>
</s:form>










































