<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/solicitudautorizacion/solAutAntiguedad.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>


<style type="text/css">
.derecha {
    text-align: right;
}
.centro {
    text-align: center !important;
}

#tablaInterna,#tablaInterna2 {
    font-family: arial;
    font-size: 11px;
     color: #666666;
    font-weight: bold;
}

.error {
	background-color: red;
	opacity: 0.4;
}


table#filtrosM2 td {
    color: #666666;
    font-weight: bold;
}



</style>

<s:form  id="busquedaAutorizacionesForm">
	
	
	<div class="titulo" colspan="6"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.busquedaAutorizacionesAjuste"/></div>
	<table  id="filtrosM2" width="98%" >
		<tr>
			<td colspan="2">
				<table id="tablaInterna">
					<tr>
						<td><label id="l_oficina"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.oficina" /> </label></td>
						<td colspan="2"> <s:select id="s_oficina" 
												name="filtroSolicitud.oficinaId"
												headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
			 									list="lstOficina" listKey="key" listValue="value"  
										  		cssClass="txtfield cajaTextoM2 w170"/> 
						</td>
					</tr>
				</table>
			</td>
			
			<td class="derecha"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.cobertura" /></td>
			<td> <s:select id="s_cobertura" 
									name="filtroSolicitud.coberturaId"
									headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
 									list="lstCoberturas"  listKey="key" listValue="value"   
							  		cssClass="txtfield cajaTextoM2 w230"/> 
			</td>
			<td colspan="2" class="derecha"> <s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.tipoDeAjuste" /> </td>
			<td colspan="2"> <s:select id="s_tipoAjuste" 
									name="filtroSolicitud.tipoAjuste"
									headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
 									list="lstTipoAjuste" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w130"/> 
			</td>
			<td class="derecha"> <s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.numeroSiniestro" /> </td>
			<td><s:textfield  	id="txt_numSiniestro" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	onBlur="validaFormatoSiniestro(this);"
								name="filtroSolicitud.numeroSiniestro"/>
			</td>
		</tr>	
		<tr>

			<td colspan="3" class="centro">
							<b><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.rangoMontosAjuste" /></b>
			</td>
			<td colspan="3" class="centro">
							<b><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.rangoAntiguedad" /></b>
			</td>
			<td colspan="4" class="centro">
							<b><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.fechaAutorizacion" /></b>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="s_condicionesMonto" 
						name="filtroSolicitud.condicionMonto"
						headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
							list="lstCondicion" listKey="key" listValue="value"  
				  		cssClass="txtfield cajaTextoM2 w155"/> 
			</td>
			<td>
				<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.monto" />
			</td>
			<td><s:textfield  	id="txt_monto" 
				 	cssClass="txtfield jQrestrict cajaTextoM2 w130 formatCurrency"
					name="filtroSolicitud.monto"
					onkeypress="return soloNumeros(this, event, true)"/>
			</td>
			<td>
				<s:select id="s_condicionesMeses" 
						name="filtroSolicitud.condicionAntiguedad"
						headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
							list="lstCondicion" listKey="key" listValue="value"  
				  		cssClass="txtfield cajaTextoM2 w155"/> 
			</td>
			<td>
				<s:textfield  	id="txt_meses" 
				 	cssClass="txtfield jQrestrict jQnumeric cajaTextoM2 w80"
					name="filtroSolicitud.meses"
					maxlength = "4"
					onkeypress="return soloNumeros(this, event, true)"/>
			</td>
			<td>
				<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.meses" />
			</td>
			
			<td>
				<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.de" />
			</td>
			<td>
				<sj:datepicker name="filtroSolicitud.fechaIni"
					changeMonth="true"
					 changeYear="true"				
					buttonImage="/MidasWeb/img/b_calendario.gif"
					buttonImageOnly="true" 
                			 id="dp_fechaDe"
					  maxlength="10" cssClass="txtfield cajaTextoM2 w80"
						   size="12"
					 onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
				</sj:datepicker>
			</td>
			<th align="right">
				<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.hasta" />
			</th>
			<td>
				<sj:datepicker name="filtroSolicitud.fechaFin"
					changeMonth="true"
					 changeYear="true"				
					buttonImage="/MidasWeb/img/b_calendario.gif"
					buttonImageOnly="true" 
                			 id="dp_fechaHasta"
					  maxlength="10" cssClass="txtfield cajaTextoM2 w80"
						   size="12"
					 onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
				</sj:datepicker>
			</td>

		</tr>
		<tr>

			<td>
				<table id="tablaInterna2">
					<tr>
						<td><label> <s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.estatus" /> </label></td>
						<td> <s:select id="s_estatus" 
												name="filtroSolicitud.estatus"
												headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
			 									list="lstEstatus" listKey="key" listValue="value"  
										  		cssClass="txtfield cajaTextoM2 w160"/> 
						</td>
					</tr>

				</table>
			</td>
			<td colspan="2" align="right"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.tipoSiniestro" /></td>
			<td> <s:select id="s_tipoSiniestro" 
									name="filtroSolicitud.tipoSiniestro"
									headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
 									list="lstTipoSiniestro" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w170"/> 
			</td>
			<td colspan="5">&nbsp;</td>

		</tr>
	</table>
	<br/>
	<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a href="javascript: void(0);" onclick="javascript: limpiarFormularioSolicitudAut();"> 
					<s:text name="midas.boton.limpiar" /> 
					<img border='0px' alt='Limpiar' title='Limpiar' src='/MidasWeb/img/b_borrar.gif'/>
				</a>
			</div>	
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar">
				<a href="javascript: void(0);" onclick="javascript: realizarBusqueda(true);"> 
					<s:text name="midas.boton.buscar" /> 
					<img border='0px' alt='Buscar' title='Buscar' src='/MidasWeb/img/b_ico_busq.gif'/>
				</a>
			</div>
</table>

	
</s:form>
<br/>
<div class="titulo" colspan="6"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.ajustesPorAutorizar"/></div>
<br/>
<br/>
<div id="indicadorSolAutAntiguedad"></div>
<div id="solicitudAutorizacionGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>
<table style="padding: 0px; width: 100%; margin: 0px; border: none;" width="98%">
		<tr>
			<td  class= "guardar">
				<div class="btn_back w110" style="display: inline; float: right;" >
					<a id="btn_autorizar" href="javascript: void(0);" onclick="javascript: autorizarRechazar();"> 
						<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.autorizarRechazar" /> 
					</a>
				</div>
				<div class="btn_back w130" style="display: inline; float: right;" >
					<a id="btn_excel" href="javascript: void(0);" onclick="javascript: exportarExcel();"> 
						<s:text name="midas.boton.exportarExcel" /> &nbsp;<img align="middle" border='0px' alt='Exportar' title='Exportar' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
					</a>
				</div>
			</td>							
		</tr>
</table>


<script type="text/javascript">
    jQuery(document).ready(
		function(){
			realizarBusqueda(false);
			initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		}
	);
</script>
