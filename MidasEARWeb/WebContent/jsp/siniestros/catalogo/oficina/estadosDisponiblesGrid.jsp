<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<s:if test="consulta">
				<call command="enableDragAndDrop"><param>false</param></call>
			</s:if>	
			<s:else>
				<call command="enableDragAndDrop"><param>true</param></call>
				
			</s:else>
			
		</beforeInit>		
		<column id="estadoTransaccion.descripcion" type="ro" width="*" > Nombre del Estado </column>
		<column id="estadoTransaccion.id" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="estadoTransaccion.pais.id" type="ro" width="0" sort="int" hidden="true">id</column>
	</head>
	<% int a=0;%>
	
	<s:iterator value="estadoListDispobibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="pais.id" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	

	
	
</rows>