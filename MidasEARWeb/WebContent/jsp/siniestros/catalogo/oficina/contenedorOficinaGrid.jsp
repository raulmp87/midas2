<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="num" type="ro" width="50" sort="int" align="center" hidden="true"></column>          
		<column id="id" type="ro" width="100" sort="int" align="center" >No de Oficina</column>
		<column id="nombreOficina" type="ro" width="*" sort="str" align="center">Nombre de Oficinas</column>
		<column id="claveOficina" type="ro" width="120" sort="str" align="center">Clave de Oficina</column>
		<column id="estatus" type="ro" width="100" sort="str" align="center">Estatus</column>
		<column id="acciones" type="img" width="40" sort="na" align="center">Acciones</column>
		<column id="acciones" type="img" width="40" sort="na" align="center">#cspan</column>
		
		
	</head>
	
	
	<s:iterator value="listaOficinas" var="c" status="index">
		<row id="${index.count}">
			
			<cell><![CDATA[${index.count}]]></cell>
			  
			<cell><![CDATA[<s:property value="id" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell align="left"><![CDATA[<s:property value="nombreOficina" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="claveOficina" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><s:text name="%{'midas.catalogos.oficina.estatus.' + estatus}"/></cell>			
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:editarOficina(${c.id})^_self</cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:consultarOficina(${c.id})^_self</cell>
			
			
		</row>
	</s:iterator>
	
</rows>