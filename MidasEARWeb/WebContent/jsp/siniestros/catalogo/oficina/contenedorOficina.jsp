<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script src="<s:url value='/js/midas2/siniestros/catalogo/oficina/catOficina.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.centro{
	margin-left: 5%;
}

table#filtrosM2 th{
 	color:#000000 !important;
     width: 88px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
	vertical-align: middle;
}

</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">

		<s:form  id="contenedorForm" name="contenedorForm">
			<div >
			<table id="filtros" cellpadding="2" cellspacing="1" >
				<tr>
					<td colspan="6">
						<div class="titulo" align="left">
						<s:text name="midas.catalogos.oficina.busquedaOficina" />
						</div>
					</td>
				</tr>
				<tr class="pf">
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.numeroOficina"/>
					</th>	
					<td width="315px" class="centro">
						<s:textfield  id="noOficina" name="filtroOficina.id" cssClass="jQnumeric jQrestrict cajaTextoM2 w200"
						maxlength="5;"/>
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.nombreOficina"/>
					</th>	
					<td width="315px">
						<s:textfield  id="nombreOficina" name="filtroOficina.nombreOficina" cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w200"
						maxlength="40"/>
					</td>			
					<th width="105px">				
						
					</th>	
					<td width="315px">
	
					</td>
				</tr>
				<tr class="pf" >
					<th width="105px"  >				
						<s:text name="midas.catalogos.oficina.claveOficina"/>
					</th>	
					<td width="315px">
						<s:textfield  id="claveOficina" name="filtroOficina.claveOficina" cssClass="jQalphanumeric jQrestrict cajaTextoM2 w200"
						maxlength="5;"/>
					</td>
					<th width="105px">				
						<s:text  name="midas.catalogos.oficina.estatus"/>
					</th>	
					<td width="315px">
					    <s:select list="estatus" name="filtroOficina.estatus" headerKey="" headerValue="%{getText('midas.general.seleccione)}"  id="estatus" cssClass="cajaTexto w200" onchange=""/>
					</td>
	
					<td width="105px" colspan="2" align="right">
					
						
					</td>
				</tr>
				
				<tr>		
				<td colspan="6">		
					<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="buscarListaOficinas();" >
									 <s:text name="midas.boton.buscar" /> </a>
								</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
									<a href="javascript: void(0);" onclick="limpiarPantalla();"> 
									<s:text name="midas.boton.limpiar" /> </a>
								</div>	
								
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		
			</table>
			<br>
			</div>
		</s:form>
	
	<div class="titulo" align="left">
		<s:text name="midas.catalogos.oficina.listadoOficinas" />
	</div>
	
	   
	<div id="oficinaGrid"  style="height:320px; width: 99%;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	
	<div id="spacer1" style="height: 10px"></div>
	
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="btn_back w120" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="agregarOficina();"> 
				<s:text name="midas.boton.agregar" /> </a>
			</div>	
		</td>
	</tr>
</table>	
	
	
	
       
	<script type="text/javascript">
		iniContenedorOficinaGrid();
	</script>
	

	
</div>



<div id="spacer2" style="height: 40px"></div>


