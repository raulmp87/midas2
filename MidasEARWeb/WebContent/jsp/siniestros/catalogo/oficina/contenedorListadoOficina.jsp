<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script src="<s:url value='/js/midas2/siniestros/catalogo/oficina/catOficina.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

table#filtrosM2{
	background-color: #FFFFFF;
	width: 100%;
}
.centro{
	margin-left: 5%;
}

table#filtrosM2 th{
 	color:#000000 !important;
     width: 88px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
	vertical-align: middle;
}

</style>


<div id="spacer1" style="height: 10px"></div>
<div align="center">
	<s:form id="contendorListadoForm" name="contendorListadoForm">
		<div class="titulo" align="left">
			<s:text name="midas.catalogos.oficina.datosOficina" />
		</div>
		
		<div>
			<table width="890px" id="filtrosM2" cellpadding="2" cellspacing="1" >
				<tr class="pf">
					<th width="105px" id="th_numeroOficina">				
						<s:text name="midas.catalogos.oficina.numeroOficina"/>
					</th>	
					<td width="315px" class="centro">
						<s:textfield name="entidad.id" id="id" cssClass="jQalphanumeric cajaTextoM2 w200"
						maxlength="50;"  readonly="true"/>
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.claveOficina"/>
					</th>	
					<td width="315px">
						<s:if  test="entidad.id == null">
						<s:textfield name="entidad.claveOficina"  id="claveOficina" cssClass="jQalphanumeric jQrestrict jQrequired cajaTextoM2 w200 campoForma"
						maxlength="5"/>
						</s:if>
						<s:else>
						<s:textfield name="entidad.claveOficina"  id="claveOficina" cssClass="jQalphanumeric jQrestrict jQrequired cajaTextoM2 w200 campoForma" maxlength="5" readonly="true"/>
						</s:else>
					</td>			
					<th width="105px">				
						
					</th>	
					<td width="315px">
						<table>
							<tr>
								<td>
									<span style="font-size:9px;" ><s:text name="midas.catalogos.oficina.fechacreacion" /></span>	
									<s:textfield name="entidad.fechaCreacion"  cssClass="cajaTextoM2 w60" readonly="true" disabled="true" />
							   </td>
							   <td>
									<span style="font-size:9px;" ><s:text name="midas.servicio.siniestros.fecha.inactivo" /></span>	
									<s:textfield name="entidad.fechaCambioEstatus" cssClass="jQdate-mx cajaTextoM2 w60" readonly="true"   disabled="true"/>
								</td>
							</tr>
						</table>
						
					</td>					
				</tr>
				<tr class="pf" >
					<th width="105px"  >				
						<s:text name="midas.catalogos.oficina.nombreOficina"/>
					</th>	
					<td width="315px">
						<s:textfield name="entidad.nombreOficina" id="nombreOficina" cssClass="jQalphabeticExt jQrestrict jQrequired cajaTextoM2 w200 campoForma"
						maxlength="50;"/>
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.estatus"/>
					</th>	
					<td width="315px">
						<s:if  test="entidad.id == null">
					    	<s:select list="estatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" name="entidad.estatus" id="estatus" cssClass="cajaTexto jQrequired w200 campoForma" onchange="" readonly="true" disabled="true"/>
					    </s:if>
					    <s:else>
					    	<s:select list="estatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" name="entidad.estatus" id="estatus" cssClass="cajaTexto jQrequired w200 campoForma" onchange=""/>
					    </s:else>
					</td>
					
					<th width="105px">				
						<s:text name="Tipo Servicio"/>
					</th>	
					<td width="315px">
					    <s:select list="tipoServicio" headerKey="" headerValue="%{getText('midas.general.seleccione')}" name="entidad.tipoServicio" id="tipoServicio" cssClass="cajaTexto jQrequired w200 campoForma" />
					</td>
					
				</tr>
				<tr class="pf" >
					<th width="105px"  >				
						<s:text name="midas.catalogos.oficina.responsable"/>
					</th>
					<td>
					    <s:textfield name="entidad.responsable"  id="responsable" cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w200 campoForma"
						maxlength="100"/>
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.derivaHGS"/>	
					</th>	
					<td width="315px" align="left">
						<s:checkbox name="entidad.derivaHGS" id="derivaHGS" cssClass="cajaTextoM2 campoForma"/>
					</td>
					<th width="105px">										
						<s:text name="midas.catalogos.oficina.referenciaBancariaRecuperacion"/>					
					</th>	
					<td width="315px" align="left" colspan="3">
						<s:textfield name="entidad.consecutivoReferencia" readonly="true" disabled="true" id="consecutivoReferencia" maxlength="3" cssClass="jQnumeric jQrestrict cajaTextoM2"/>
					</td>
				</tr>	
				<tr class="pf" >
					<th width="105px"  >				
						<s:text name="Web Service" />
					</th>
					<td width="315px" align="left">
						<s:checkbox onclick="habilitarWS();" name="entidad.isWebService" id="aplicaWS" cssClass="cajaTextoM2 campoForma"/>
					    <s:textfield name="entidad.endpoint" disabled="true"  id="endpoint" cssClass=" cajaTextoM2 w200 campoForma"
						maxlength="100"/>
					</td>
				</tr>
			</table>
		</div>
		
		<br/><br/><br/>
	<div class="titulo" align="left">
		<s:text name="midas.catalogos.oficina.datosLocalizacion" />
	</div>
	
	<div >
			<table width="890px" id="filtrosM2" cellpadding="2" cellspacing="1" >
				<tr class="pf">
					<th width="105px">
						<s:text name="midas.catalogos.oficina.telefono1" />				
					</th>	
					<td width="115px" class="centro">
						<table>
							<tr>
								<td>
									<s:textfield name="entidad.telOficinaLada" id="telOficinaLada" cssClass="jQnumeric jQrequired jQrestrict cajaTextoM2 w30 campoForma"
									maxlength="3;" style="width: 30px;"  />
								</td>
								<td>
									<s:textfield name="entidad.telOficina" id="telOficina" cssClass="jQnumeric jQrequired jQrestrict cajaTextoM2 w200 campoForma"
									maxlength="8;"/>
								</td>
							</tr>
						</table>
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.telefono2" />
					</th>	
					<td width="115px" colspan="3">
						<table>
							<tr>
								<td>
									<s:textfield name="entidad.telOficinaLada2" id="telOficinaLada2" cssClass="jQnumeric jQrestrict cajaTextoM2 w200 campoForma"
									maxlength="3" style="width: 30px;" />
								</td>
								<td>									
									<s:textfield name="entidad.telOficina2" id="telOficina2" cssClass="jQnumeric jQrestrict cajaTextoM2 w200 campoForma"
									maxlength="8"/>
								</td>	
							</tr>
						</table>								
					</td>	
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.latitud" />
					</th>	
					<td width="115px" colspan="3">
						<s:textfield name="entidad.coordenadas.latitud" id="latitud" cssClass="jQfloat jQrestrict cajaTextoM2 w200 campoForma"
							style="width: 100px;" />								
					</td>								
				</tr>
				<tr class="pf">
					<th width="105px">
						<s:text name="midas.catalogos.oficina.correo1" />				
					</th>	
					<td width="315px">
							<s:textfield name="entidad.correo1" id="correo1" cssClass="jQemail jQrequired jQrestrict cajaTextoM2 w240 campoForma"
							maxlength="100;" />						
					</td>
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.correo2" />
					</th>	
					<td width="315px" colspan="3">						
							<s:textfield name="entidad.correo2" id="correo2" cssClass="jQemail jQrestrict cajaTextoM2 w240 campoForma"
							maxlength="100" />				
					</td>	
					<th width="105px">				
						<s:text name="midas.catalogos.oficina.longitud" />
					</th>	
					<td width="115px" colspan="3">
						<s:textfield name="entidad.coordenadas.longitud" id="longitud" cssClass="jQfloat jQrestrict cajaTextoM2 w200 campoForma"
							 style="width: 100px;" />								
					</td>			
				</tr>
			</table>
		</div>
		<!--<s:hidden name="entidad.isWebService" id="ws" ></s:hidden>-->
	</s:form>
		
</div>

<s:hidden name="consulta" id="banderaConsulta" ></s:hidden>
<s:hidden name="cantEstadosAsociados" id="cantEstadosAsociados" ></s:hidden>

<s:if test="entidad.id != null">
	<div class="titulo" align="left">
		<s:text name="midas.catalogos.oficina.direccion"></s:text>
	</div>
	<div>
		<table id="filtrosM2">
			<tr class="pf" >
			<th width="105px"  >				
				<s:text name="midas.catalogos.oficina.pais" />
	
			</th>
			<td>
				<s:select list="paises" headerKey="PAMEXI" headerValue="Mexico" name="" id="comboPais" cssClass="cajaTexto w200 campoForma" onchange="javascript: cargarGridsEstadosPorPais( jQuery('#comboPais').val(), jQuery('#id').val());"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<s:text name="midas.catalogos.oficina.leyenda.1"/>
			</td>
		</tr>
		</table>
		
	</div>
	<div id="spacer2" style="height: 40px"></div>
	<div>
		<table>
			<tr>
				<td colspan="4" >
					<div id="estadosDisponibles" style="width: 420px; height: 240px;"></div>
				</td>
				
				<s:if test="consulta">
					
					<td align="center" valign="middle" width="8%">
						<div class="btn_back w40 btnActionForAll">
							<a class=""  alt="Desligar todas" > << </a>
						</div>
						<div class="btn_back w40 btnActionForAll">
							<a class=""  alt="Asociar todas" > >> </a>
						</div>
					</td >
					
				</s:if>
				<s:else>
					<td align="center" valign="middle" width="8%">
						<div class="btn_back w40 btnActionForAll">
							<a class=""  alt="Desligar todas" href="javascript: eliminarTodosEstadosRelacionados(jQuery('#id').val());"> << </a>
						</div>
						<div class="btn_back w40 btnActionForAll">
							<a class=""  alt="Asociar todas" href="javascript: asociarTodosEstados(jQuery('#id').val(), jQuery('#comboPais').val());"> >> </a>
						</div>
					</td >
				</s:else>
				
		
				<td colspan="4" >
					<div id="estadosAsociados" style="width: 420px; height: 240px;float:right;"></div>
				</td>
			</tr>
		</table>
	</div>
</s:if>

<s:form  id="clienteForm" name="clienteForm">
	
	<table  style="padding: 0px; width: 98%; margin: 0px; border: none;">
		 <tr>
     
     				<td>		
				<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
					<tr>
						<td>
							<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
								<a href="javascript: void(0);" onclick="cerrarAltaOficina();"> 
								<s:text name="midas.boton.cerrar" /> </a>
							</div>	
							<s:if  test="!consulta">
								<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
									<a href="javascript: void(0);" onclick="guardarOficina();"> 
									<s:text name="midas.boton.guardar" /> </a>
								</div>		
							</s:if>
						</td>							
					</tr>
				</table>				
			</td>
          	
          </tr>
	</table>
	
</s:form>	

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
    cargarGridsEstadosPorPais( jQuery('#comboPais').val(), jQuery('#id').val(), jQuery('#banderaConsulta').val() );
    habilitaEdicionWS();
    edicionDeForma( jQuery('#banderaConsulta').val());
    
</script>
