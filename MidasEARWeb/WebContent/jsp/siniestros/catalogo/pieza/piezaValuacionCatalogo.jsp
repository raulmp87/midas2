<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<s:form name="busqueda" id="busquedaServicio" method="post" action="busquedaServicio" >
	
	<table width="99%" id="filtros">
	
	<!-- PIEZAS -->
	
			<tr>
				<td class="titulo" colspan="5">
					<s:text name="midas.siniestros.catalogo.pieza.busqueda" />
				</td>
			</tr>
			<tr>
				<th> <s:text name="midas.siniestros.catalogo.pieza.noPieza" />: </th>
				<th> <s:text name="midas.siniestros.catalogo.pieza.seccionAutomovil" />: </th>
				<th> <s:text name="midas.siniestros.catalogo.pieza.descripcionPieza" />: </th>
				<th> <s:text name="midas.siniestros.catalogo.pieza.estatus" />: </th>
				<th> <!-- Botones -->&nbsp;</th>
			</tr>
			<tr>
				<td> <s:textfield 
								name="piezaValuacionFiltro.numPieza" 
								id="numPieza" cssClass="cajaTextoM2 w40" 
								maxlength="10"
								onkeypress="return soloNumeros(this, event, false)" /> </td>
				<td>
					<s:select 
						list="ctgSeccionAuto" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacionFiltro.seccionAutomovil" 
						id="seccion" 
						cssClass="cajaTextoM2 w120" 
						onchange=""/> 
				</td>				
				
				<td> <s:textfield 
								name="piezaValuacionFiltro.descripcion"
								maxlength="60" 
								id="descripcion" 
								cssClass="cajaTextoM2 w200"  /> </td>
				<td>
					<s:select 
						list="ctgEstatus" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacionFiltro.estatus" 
						id="estatus" 
						cssClass="cajaTextoM2 w120" 
						onchange=""/> 
				</td>				
			</tr>

			<tr>		
				<td colspan="5">		
					<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="busquedaServicio();" >
									 <s:text name="midas.boton.buscar" /> </a>
								</div>
														<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
									<a href="javascript: void(0);" onclick="regresar();"> 
									<s:text name="midas.boton.limpiar" /> </a>
								</div>	
								
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
			
			
	</table>
	
	
	<br>

</s:form>

<div class="titulo"><s:text name="midas.siniestros.catalogo.pieza.listado" /></div>
<div id="indicador"></div>
<div id="piezaValuacionGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />
					&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="muestraAlta();"> 
				<s:text name="midas.boton.agregar" /> </a>
			</div>	
		</td>
	</tr>
</table>	




<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/pieza/piezaValuacion.js'/>" ></script>
<script type="text/javascript">
	iniciaListadoGrid();
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>