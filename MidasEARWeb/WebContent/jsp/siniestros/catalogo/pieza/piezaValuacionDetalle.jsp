<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<jsp:useBean id="newsDate" class="java.util.Date" />

<script type="text/javascript">
var obtenerPath = '<s:url action="obtener" namespace="/siniestros/valuacion"/>';
</script>

<s:form id="informacionBusquedaValuacionesForm" >
		<s:hidden id="valuacionReporteFiltro.ajustador" name="valuacionReporteFiltro.ajustador"></s:hidden>
		<s:hidden id="valuacionReporteFiltro.estatus" name="valuacionReporteFiltro.estatus"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.fechaFinal" id="valuacionReporteFiltro.fechaFinal"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.fechaInicial" id="valuacionReporteFiltro.fechaInicial"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroReporte" id="valuacionReporteFiltro.numeroReporte"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroSerie" id="valuacionReporteFiltro.numeroSerie"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroValuacion" id="valuacionReporteFiltro.numeroValuacion"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.valuador" id="valuacionReporteFiltro.valuador"></s:hidden>
</s:form>

<s:form name="altaModificarCtg" id="altaModificarCtg" method="post" action="alta" >
	<s:hidden id="idValuacionReporte" name="idValuacionReporte"/>
	<s:hidden id="valuacionEsPantallaPrevia" name="valuacionEsPantallaPrevia"/>
	<s:hidden id="esConsulta" name="esConsulta"/>
	<table width="99%" id="filtros">
	<!-- PIEZAS -->
	
			<tr>
				<td class="titulo" colspan="4">
					<s:text name="midas.siniestros.catalogo.pieza.datos" />
				</td>
			</tr>
			<tr>
				<th> <s:text name="midas.siniestros.catalogo.pieza.noPieza" />: </th>
				<th> <s:text name="midas.siniestros.catalogo.pieza.seccionAutomovil" />: </th>
				<th> <s:text name="midas.siniestros.catalogo.pieza.estatus" />: </th>
				<th> <!-- Botones -->&nbsp;</th>
			</tr>
			<tr>
				<td> <s:textfield 
								name="piezaValuacion.id" 
								id="numPieza" 
								cssClass="cajaTextoM2 w40" 
								maxlength="10"
								readonly="true" cssStyle="background-color:#EEEEEE"  /></td>
				<td>
					<s:select 
						list="ctgSeccionAuto" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacion.seccionAutomovil" 
						id="seccion" 
						cssClass="jQrequired cajaTextoM2 w120" 
						onchange=""/> 
				</td>				
				
				
				<td>
					<s:select 
						list="ctgEstatus" 
						headerKey="" 
						headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacion.estatus" 
						id="estatus" 
						cssClass="jQrequired cajaTextoM2 w120" 
						onchange=""/> 
				</td>				
			</tr>
			<tr>
				<th colspan="4"> <s:text name="midas.siniestros.catalogo.pieza.descripcionPieza" />: </th>
			</tr>
			<tr>
				<td colspan="4"> <s:textfield 
								name="piezaValuacion.descripcion"
								maxlength="150" 
								id="descripcion" 
								cssClass="jQrequired cajaTextoM2 w320"  /> </td>
			</tr>
			
	</table>
	<br>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		 <tr>
       
	       <td>		
				<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
					<tr>
						<td>
							<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
								<a href="javascript: void(0);" onclick="regresar();"> 
								<s:text name="midas.boton.cerrar" /> </a>
							</div>	
							
							<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar" >
								
								<s:if test="accion == 0 " > 
										<a href="javascript: void(0);" onClick="agregarDetalleServicio()" class="icon_guardar2"> 
												<s:text name="midas.boton.guardar" /> 
										</a>
								</s:if>
								<s:elseif test="accion == 1 ">
										<a href="javascript: void(0);" onClick="agregarDetalleServicio()" class="icon_guardar2"> 
												<s:text name="midas.boton.agregar" /> 
										</a>
								</s:elseif>
											
							</div> 
							
						</td>							
					</tr>
				</table>				
			</td>
            	
            </tr>
	</table>
	<s:hidden name="piezaValuacion.codigoUsuarioCreacion" />
	
</s:form>

<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/pieza/piezaValuacion.js'/>" ></script>

<s:if test="accion == 2" > 
	<script type="text/javascript">
		deshabilitarComponentes();
	</script>
</s:if>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>