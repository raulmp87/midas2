<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="noPieza"          type="ro"  width="*" sort="int" hidden="false"><s:text name="midas.siniestros.catalogo.pieza.noPieza" /> </column>
        <column id="seccionAutomovil" type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.catalogo.pieza.seccionAutomovil" /> </column>
      	<column id="descripcion"      type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.catalogo.pieza.descripcionPieza" /> </column>
		<column id="estatus"          type="ro"  width="*" sort="str" hidden="false"><s:text name="midas.siniestros.catalogo.pieza.estatus" /> </column>
		<column id="extra"            type="img" width="40"><s:text name="midas.siniestros.catalogo.pieza.acciones" />  </column>
		<column id="extra"            type="img" width="40">#cspan</column>
		
	</head>
	<s:iterator value="piezaValuacionGrid" status="row">
		<row >
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreSeccionAutomovil" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^Consultar^javascript:mostrarDetalleServicio(<s:property value="id" />);^_self</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^Editar^javascript:editarDetalleServicio(<s:property value="id" />);^_self</cell>
		</row>
	</s:iterator>
</rows>