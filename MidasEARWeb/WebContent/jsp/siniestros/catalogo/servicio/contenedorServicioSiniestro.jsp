<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<style type="text/css">
table#filtros th{
 	color:#000000 !important;
     width: 20px !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
	vertical-align: middle;
	padding-left:  9%;
	font-weight: normal !important;
}
</style>

<s:form name="busqueda" id="busquedaServicio" method="post" action="busquedaServicio" >

	<!-- HIDDEN -->
	<s:hidden name="tipoServicioOperacion"  id="tipoServicioOperacion" />
	
	<table width="98%" id="filtros" style="padding-left: 10px;">
	
	<!-- AJUSTADORES -->
	<s:if test="tipoServicioOperacion == 1">
	
			<tr>
				<td class="titulo" colspan="6">
					<s:property value="leyendaServicioCabeza[0]" />	
				</td>
			</tr>
			<tr>
				<th> <s:property value="leyendaServicioCampo[0]" />: </th>
				<td> <s:textfield 
								name="servicioSiniestroFiltro.noAbogadoAjustadorId" 
								id="noAbogadoAjustadorId" 
								cssClass="jQnumeric jQrestrict cajaTextoM2 w150" 
								maxlength="10"/>  </td>
				
				<th> <s:property value="leyendaServicioCampo[4]" />: </th>
				<td> <s:textfield 
								name="servicioSiniestroFiltro.nombreAbogadoAjustador" 
								id="nombreAbogadoAjustador" 
								maxlength="60"
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w150" />  </td>
				
				<th> <s:property value="leyendaServicioCampo[2]" />: </th>
				<td>
					<s:select 
						list="valoresTipoServicio" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.tipoAbogadoAjustador" 
						id="tipoAbogadoAjustador" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/> 
				</td>
			</tr>
			<tr>
				<th> <s:property value="leyendaServicioCampo[3]" />: </th>
				<td> <s:textfield 
								name="servicioSiniestroFiltro.noPrestador" 
								id="noPrestador" cssClass="jQnumeric jQrestrict cajaTextoM2 w150" 
								maxlength="10"/> 
				</td>
				<th> <s:property value="leyendaServicioCampo[8]" />: </th>
				<td colspan="3"> <s:textfield 
								name="servicioSiniestroFiltro.nombrePrestador"
								maxlength="60" 
								id="nombrePrestador" 
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w150"  /> </td>
			</tr>
			<tr>
				<th> <s:property value="leyendaServicioCampo[5]" />: </th>
				<td> <s:select 
						list="valoresOficinas" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.oficinaId" 
						id="oficinaId" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/>
				 </td>
				
				<th> <s:property value="leyendaServicioCampo[6]" />: </th>
				<td colspan="3">
					<s:select 
						list="valoresEstatus" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.estatus" 
						id="estatus" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/> 
				</td>
			</tr>
	
	</s:if>
	
	
	<s:elseif test="tipoServicioOperacion == 2">
		
		<!-- ABOGADOS -->
			<tr>
				<td class="titulo" colspan="6">
					<s:property value="leyendaServicioCabeza[0]" />	
				</td>
			</tr>
			<tr>
				<th> <s:property value="leyendaServicioCampo[4]" />: </th>
				<td> <s:textfield 
								name="servicioSiniestroFiltro.nombreAbogadoAjustador"
								maxlength="60" 
								id="nombreAbogadoAjustador" 
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w150" />  </td>
				
				<th> <s:property value="leyendaServicioCampo[2]" />: </th>
				<td>
					<s:select 
						list="valoresTipoServicio" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.tipoAbogadoAjustador" 
						id="tipoAbogadoAjustador" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/> 
				</td>
				
				<th> <s:property value="leyendaServicioCampo[5]" />: </th>
				<td> <s:select 
						list="valoresOficinas" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.oficinaId" 
						id="oficinaId" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/>
				 </td>
			</tr>
			<tr>
				<th> <s:property value="leyendaServicioCampo[3]" />: </th>
				<td> <s:textfield name="servicioSiniestroFiltro.noPrestador"
								 id="noPrestador"
								 maxlength="10"
								 cssClass="jQnumeric jQrestrict cajaTextoM2 w150" /> </td>
				<th> <s:property value="leyendaServicioCampo[8]" />: </th>
				<td> <s:textfield name="servicioSiniestroFiltro.nombrePrestador" 
								  id="nombrePrestador" 
								  cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w150" 
								  maxlength="60"/> </td>
				<th> <s:property value="leyendaServicioCampo[6]" />: </th>
				<td>
					<s:select 
						list="valoresEstatus" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="servicioSiniestroFiltro.estatus" 
						id="estatus" 
						cssClass="cajaTextoM2 w150" 
						onchange=""/> 
				</td>
			</tr>
		
	</s:elseif>
	
			<tr>		
				<td colspan="6">		
					<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="busquedaServicioSiniestro();" >
									 <s:text name="midas.boton.buscar" /> </a>
								</div>
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
									<a href="javascript: void(0);" onclick="limpiar();"> 
									<s:text name="midas.boton.limpiar" /> </a>
								</div>	
								
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
			
			
	</table>
	
	
	<br>
	<div class="titulo">
		<s:property value="leyendaServicioCabeza[1]" />	
	</div>
</s:form>

<div id="indicador"></div>
<div id="servicioSiniestroGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w150" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />
					&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="muestraAlta();"> 
				<s:text name="midas.boton.agregar" /> </a>
			</div>	
		</td>
	</tr>
</table>	




<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/siniestros/catalogo/serviciosiniestro/servicioSiniestros.js'/>" ></script>
<script type="text/javascript">
	iniciaListadoGrid();
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>