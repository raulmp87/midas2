<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <s:if test="tipoServicioOperacion==1" ><column id="id" type="ro" width="*" sort="int" hidden="false">No. Ajustador</column></s:if><s:elseif test="tipoServicioOperacion==2"><column id="id" type="ro" width="*" sort="int" hidden="false">No. Abogado</column></s:elseif>
		<s:if test="tipoServicioOperacion==1" ><column id="nombrePersona" type="ro" width="*" sort="str" hidden="false">Nombre del Ajustador</column></s:if><s:elseif test="tipoServicioOperacion==2"><column id="nombrePersona" type="ro" width="*" sort="str" hidden="false">Nombre del Abogado</column></s:elseif>
      	<column id="oficina.nombreOficina"            type="ro" width="*" sort="str" hidden="false">Oficina</column>
      	<s:if test="tipoServicioOperacion==1" ><column id="ambitoPrestadorServicio" type="ro" width="*" sort="str" hidden="false">Tipo de Ajustador</column></s:if><s:elseif test="tipoServicioOperacion==2"><column id="ambitoPrestadorServicio" type="ro" width="*" sort="str" hidden="false">Tipo de Abogado</column></s:elseif>		
		<column id="nombreEstatus"                    type="ro" width="*" sort="str" hidden="false">Estatus</column>
		<column id="prestadorServicio.id"    		  type="ro" width="*" sort="int" hidden="false">No. Prestador</column>
		<column id="prestadorServicio.personaMidas.nombre" type="ro" width="*" sort="str" hidden="false">Nombre Prestador</column>
		<column id="extra"                            type="img" align="center">Acciones</column>
		<column id="extra"                            type="img"  align="center">#cspan</column>
	</head>
	<s:iterator value="serviciosSiniestrosGrid" status="row">
		<row >
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="ambitoPrestadorServicio == 1">Interno</s:if><s:elseif  test="ambitoPrestadorServicio == 2">Externo</s:elseif>
			</cell>
			<cell><s:property value="nombreEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="prestadorServicio.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="prestadorServicio.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^Consultar^javascript:mostrarDetalleServicio(<s:property value="id" />,<s:property value="personaMidas.id" />);^_self</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^Editar^javascript:editarDetalleServicio(<s:property value="id" />,<s:property value="personaMidas.id" />);^_self</cell>
		</row>
	</s:iterator>
</rows>