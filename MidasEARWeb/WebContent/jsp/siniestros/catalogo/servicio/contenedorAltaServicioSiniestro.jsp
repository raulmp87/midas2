<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<jsp:useBean id="random" class="java.util.Random" scope="page"></jsp:useBean>

<style type="text/css">
	
	.noborder {
		border: none;
	}
	
	.fotoPersona {
		max-width: 450px;
		max-height: 450px;
		margin:auto;
		padding:10px;
		display:block;
	}
	
	#fotoPersonaMin {
		max-width: 100px;
		max-height: 100px;
	}
	
	.center {
		margin:auto;
		width: 50%;
	}
	
</style>

<s:form name="altaModificacionServcio" id="altaModificacionServcio"  >

	<s:hidden name="tipoServicioOperacion"                 id="tipoServicioOperacion" />
	<s:hidden name="servicioSiniestroDto.estatusBase"      id="estatusBase" />
	<s:hidden name="servicioSiniestroDto.idDatoContacto"   id="idDatosContacto" />
	<s:hidden name="servicioSiniestroDto.idDatosDireccion" id="idDatosDireccion" />
	<s:hidden name="servicioSiniestroDto.idPersonaMidas"   id="idPersonaMidas" />
	<s:hidden name="accion"   							   id="accion" />
	
	<s:if test=" tipoServicioOperacion == 1 ">
		<s:hidden id="keyOperacionPrestadorServicio" value="AJEXT" />
	</s:if>
	<s:elseif test=" tipoServicioOperacion == 2 ">
		<s:hidden id="keyOperacionPrestadorServicio" value="ABOG" />
	</s:elseif>
	
	<!-- Datos Generales -->
	<div class="titulo">
		<s:property value="leyendaServicioCabeza[2]" />
	</div>
	<table width="99%" class="contenedorConFormato">
		<tr>
			<td>
				<table>
					<tr>
						<td><img id="fotoPersonaMin" alt="Foto"
							src="${pageContext.request.contextPath}/persona/obtenerFoto.action?idPersona=${servicioSiniestroDto.idPersonaMidas}&random=${random.nextInt()}" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="99%" class="contenedorConFormato noborder">
					<tr>
						<td>
							<!-- No. Servicio --> <s:text name="leyendaServicioCampo[0]" />
							:
						</td>
						<td><s:textfield name="servicioSiniestroDto.id" id="id"
								readonly="true" cssClass="cajaTextoM2 w40"
								cssStyle="background-color:#EEEEEE" /></td>

						<th><s:text name="midas.fuerzaventa.negocio.oficina" />:</th>
						<td><s:select list="valoresOficinas" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								name="servicioSiniestroDto.oficinaId" id="oficinaId"
								cssClass="jQrequired cajaTexto w100" onchange="" /></td>


						<td colspan="2"><s:radio list="tiposPersona"
								value="%{servicioSiniestroDto.tipoPersona}"
								onclick="javascript:onClickTipoPersonaSiniestro();"
								id="r_tipoPersona" name="servicioSiniestroDto.tipoPersona"
								cssClass="jQrequired " disabled="%{consulta}" /></td>

						<th><s:text name="midas.general.estatus" />:</th>
						<td><s:select list="valoresEstatus" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								name="servicioSiniestroDto.estatus" id="estatus"
								cssClass="jQrequired cajaTexto w100" onchange="" /></td>
					</tr>

					<tr id="tr_personaFisica">
						<th><s:text name="midas.negocio.nombres" />:</th>
						<td><s:textfield name="servicioSiniestroDto.nombrePersona"
								id="nombrePersona" maxlength="40"
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w120" /></td>

						<th><s:text
								name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno" />:
						</th>
						<td><s:textfield name="servicioSiniestroDto.apellidoPaterno"
								id="apellidoPaterno" maxlength="20"
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w120" /></td>

						<th><s:text
								name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno" />:
						</th>
						<td><s:textfield name="servicioSiniestroDto.apellidoMaterno"
								maxlength="20" id="apellidoMaterno"
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w120" /></td>

						<th><s:text
								name="midas.suscripcion.cotizacion.agentes.claveUsuario" />:</th>
						<td><s:textfield name="servicioSiniestroDto.claveUsuario"
								maxlength="20" id="claveUsuario"
								cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w120" /></td>
					</tr>

					<tr id="tr_personaMoral">
						<th><s:text
								name="%{getText('midas.servicio.siniestros.empresa')}" />:</th>
						<td colspan="3"><s:textfield
								name="servicioSiniestroDto.nombreDeLaEmpresa" maxLength="80"
								id="txt_nombre_de_la_empresa"
								cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w420 requerido"
								onkeypress="return soloAlfanumericos(this, event, false)" /></td>
						<th><s:text
								name="%{getText('midas.servicio.siniestros.comercio')}" />:</th>
						<td><s:textfield name="servicioSiniestroDto.nombreComercial"
								maxLength="80" id="txt_nombre_comercial"
								cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 requerido"
								onkeypress="return soloAlfanumericos(this, event, false)" /></td>
						<th><s:text
								name="%{getText('midas.servicio.siniestros.administrador')}" />:
						</th>
						<td><s:textfield name="servicioSiniestroDto.administrador"
								maxLength="80" id="txt_administrador"
								cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 requerido" /></td>
					</tr>
					<tr>
						<td><span><s:text
									name="midas.servicio.siniestros.fecha.activo" /></span> <s:textfield
								name="servicioSiniestroDto.fechaActivo"
								cssClass="cajaTextoM2 w60" disabled="true" /></td>
						<td><span><s:text
									name="midas.servicio.siniestros.fecha.inactivo" /></span> <s:textfield
								name="servicioSiniestroDto.fechaInactivo"
								cssClass="cajaTextoM2 w60" disabled="true" /></td>

						<th><s:property value="leyendaServicioCampo[2]" />:</th>
						<td><s:select list="valoresTipoServicio" headerKey=""
								headerValue="%{getText('midas.general.seleccione')}"
								name="servicioSiniestroDto.ambitoPrestadorServicio"
								id="ambitoPrestadorServicio"
								cssClass="jQrequired cajaTexto w100"
								onchange="validarTipoPrestador(this.value);" /></td>

						<th>
							<!-- Prestador de servicio --> <s:text
								name="leyendaServicioCampo[1]" />:
						</th>
						<td>
							<table>
								<tr>
									<td><s:textfield name="servicioSiniestroDto.prestadorId"
											id="prestadorServicioId" readonly="true"
											cssClass="cajaTextoM2 w40" /></td>
									<s:if test="servicioSiniestroDto.ambitoPrestadorServicio == 2">
										<td><img id="buscarPrestador"
											onClick="mostrarPrestadorServicio()"
											alt="Agregar prestador servicio"
											src="<s:url value="/img/b_ico_busq.gif"/>" /></td>
									</s:if>
									<s:else>
										<td><img id="buscarPrestador"
											onClick="mostrarPrestadorServicio()"
											alt="Agregar prestador servicio"
											src="<s:url value="/img/b_ico_busq.gif"/>"
											style="display: none" /></td>
									</s:else>
								</tr>
							</table>
						</td>

						<s:if test="codigoServicio == true">
							<th><s:text name="leyendaServicioCampo[7]" />:</th>
							<td><s:select list="#{'1':'Si', '2':'No'}" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									name="servicioSiniestroDto.certificacion" id="certificacion"
									cssClass="cajaTexto w100" onchange="" /></td>
						</s:if>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<br>
	<!-- Dirección -->
	<div class="titulo">
		<s:text name="midas.servicio.siniestros.direccion"/>		
	</div>
		<table width="99%" class="contenedorConFormato" >
		<tr><td>
			<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccionSiniestroMidas" ignoreContextParams="true" executeResult="true" >
               		<s:param name="idPaisName">servicioSiniestroDto.idPaisName</s:param>
                    <s:param name="idEstadoName">servicioSiniestroDto.idEstadoName</s:param>      
                    <s:param name="idCiudadName">servicioSiniestroDto.idCiudadName</s:param>        
                    <s:param name="idColoniaName">servicioSiniestroDto.idColoniaName</s:param>
                    <s:param name="calleName">servicioSiniestroDto.calleName</s:param>
                    <s:param name="numeroName">servicioSiniestroDto.numeroName</s:param>
                    <s:param name="numeroIntName">servicioSiniestroDto.numeroIntName</s:param>
                    <s:param name="cpName">servicioSiniestroDto.cpName</s:param>
                    <s:param name="nuevaColoniaName">servicioSiniestroDto.nuevaColoniaName</s:param>
                    <s:param name="idColoniaCheckName">idColoniaCheck</s:param>  
                    <s:param name="referenciaName">servicioSiniestroDto.referencia</s:param>               
                    <s:param name="labelPais">País</s:param>
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalle">Calle</s:param>
					<s:param name="labelNumero">Número Ext.</s:param>
					<s:param name="labelNumeroInt">Número Int.</s:param>
					<s:param name="labelCodigoPostal">Código Postal</s:param>
					<s:param name="labelReferencia">Referencia</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">3</s:param>
                    <s:param name="readOnly" value="%{#readOnly}"></s:param>
                    <s:param name="requerido" value="0"></s:param>
                    <s:param name="incluirReferencia" value="true"></s:param>
					<s:param name="enableSearchButton" value="false"></s:param>
          	</s:action>
		</td></tr>
		</table>
	<br/>
	<!-- Localizacion -->
	<div class="titulo">
		<s:text name="midas.servicio.siniestros.localizacion"/>
	</div>
		<table width="99%" class="contenedorConFormato">
		<tr>
			<th> <s:text name="midas.servicio.siniestros.telefono1"/>: </th>
			<td> 
				<table>
					<tr>
						
						<td> <s:textfield name="servicioSiniestroDto.telCasaLada" 
										  maxlength="3" 
										  id="telCasaLada"  
										  title="Lada" 
										  cssClass="jQnumeric jQrequired jQrestrict cajaTextoM2 w40" /> </td>
						<td> <s:textfield name="servicioSiniestroDto.telCasa" 
										  maxlength="8" 
										  id="telCasa" 
										  cssClass="jQnumeric jQrequired jQrestrict cajaTextoM2 w60"/> </td>			  
					</tr>
				</table>
			</td>
			
			<th> <s:text name="midas.servicio.siniestros.telefono2"/>: </th>
			<td> 
				<table>
					<tr>
						<td> <s:textfield name="servicioSiniestroDto.telCasaLada2" 
						                  maxlength="3" 
						                  onkeypress="return soloNumeros(this, event, false)" 
						                  id="telCasaLada2" 
						                  title="Lada" 
						                  cssClass="cajaTextoM2 w40" /></td>
						<td> <s:textfield name="servicioSiniestroDto.telCasa2" 
										  maxlength="8" 
										  onkeypress="return soloNumeros(this, event, false)" 
										  id="telCasa2" 
										  cssClass="cajaTextoM2 w120"/> </td>
					</tr>
				</table> 
			</td>
			
			<th> <s:text name="midas.servicio.siniestros.otros1"/>:</th>
			<td>
				<table>
					<tr>
						<td><s:textfield name="servicioSiniestroDto.telOtro1Lada" 
										 maxlength="3" 
										 onkeypress="return soloNumeros(this, event, false)" 
										 id="telOtro2Lada" 
										 title="Lada" 
										 cssClass="cajaTextoM2 w40" /></td>
						<td><s:textfield name="servicioSiniestroDto.telOtro1" 
									     maxlength="8" 
									     onkeypress="return soloNumeros(this, event, false)" 
									     id="telOtro2" 
									     cssClass="cajaTextoM2 w120" /></td>
					</tr>
				</table>
			
			<th> <s:text name="midas.servicio.siniestros.otros2"/>:</th>
			<td>
				<table>
					<tr>
						<td><s:textfield name="servicioSiniestroDto.telOtro2Lada" 
										 maxlength="3"
										 id="telOtro2Lada" 
										 title="Lada" 
										 cssClass="jQnumeric jQrestrict cajaTextoM2 w40" /></td>
						<td><s:textfield name="servicioSiniestroDto.telOtro2" 
									     maxlength="8"
									     id="telOtro2" 
									     cssClass="jQnumeric jQrestrict cajaTextoM2 w120" /></td>
					</tr>
				</table>
			
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<th> <s:text name="midas.reportesiniestromovil.numeroCelular"/>: </th>
			<td> 
				<table>
					<tr>
						
						<td> <s:textfield name="servicioSiniestroDto.telCelularLada" 
										  maxlength="3"
										  id="telCelularLada"  
										  title="Lada" 
										  cssClass="jQnumeric jQrestrict cajaTextoM2 w40" /> </td>
						<td> <s:textfield name="servicioSiniestroDto.telCelular" 
										  maxlength="8"
										  id="telCelular" 
										  cssClass="jQnumeric jQrestrict cajaTextoM2 w60"/> </td>
					</tr>
				</table>
			</td>
			
			<th> <s:text name="midas.cotizacion.emails"/>: </th>
			<td colspan="3">
			<s:if test="tipoServicioOperacion == 1 "> 
				<s:textfield name="servicioSiniestroDto.correoPrincipal" 
							  maxlength="60"
							  id="correoPrincipal"  
							  title="Email" 
							  cssClass="jQemail jQrestrict jQrequired cajaTextoM2  w300" />
			</s:if>
			<s:elseif test="tipoServicioOperacion == 2 ">
				<s:textfield name="servicioSiniestroDto.correoPrincipal" 
							  maxlength="60"
							  id="correoPrincipal"  
							  title="Email" 
							  cssClass="jQemail cajaTextoM2 w300" />
			</s:elseif>
			</td>
			<th> <s:text name="%{getText('midas.servicio.siniestros.rfc')}"/> </th>
			<td> <s:textfield name="servicioSiniestroDto.rfc"
						 		maxlength="13"
						 		id="rfc" 
						 		title="RFC"
						 		cssClass="jQRFC jQrequired jQrestrict cajaTextoM2 w160"/></td>		
		           
		</tr>
	</table>
	
	<!-- Unidad -->
	<!-- Solo aplica para ajustadores -->
	<s:if test=" tipoServicioOperacion == 1 ">
		<div class="titulo">
			<s:text name="%{getText('midas.servicio.siniestros.unidad.titulo')}"/>
		</div>
			<table width="99%" class="contenedorConFormato">
			<tr>
				<td><s:text name="%{getText('midas.servicio.siniestros.unidad.codigo')}"/></td>
				<td> <s:textfield name="servicioSiniestroDto.codigoUnidad"
							 		cssClass="cajaTextoM2 w160"/></td>
				<td><s:text name="%{getText('midas.servicio.siniestros.unidad.placa')}"/></td>
				<td> <s:textfield name="servicioSiniestroDto.placas"
							 		cssClass="cajaTextoM2 w160"/></td>
				<td><s:text name="%{getText('midas.servicio.siniestros.unidad.descripcion')}"/></td>
				<td> <s:textfield name="servicioSiniestroDto.descripcionUnidad"
							 		cssClass="cajaTextoM2 w160"/></td>		
			</tr>
		</table>
	</s:if>
	
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		 <tr>
       
	       <td>		
				<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
					<tr>
						<td>
							<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
								<a href="javascript: void(0);" onclick="regresar();"> 
								<s:text name="midas.boton.cerrar" /> </a>
							</div>	
							
								<s:if test="readOnly != true" > 
								<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar" >
								
									<s:if test="accion == 0 " > 
										<a href="javascript: void(0);" onClick="modificarDetalleServicio(<s:property value="tipoServicioOperacion"/>)" class="icon_guardar2"> 
												<s:text name="midas.boton.guardar" /> 
										</a>
									</s:if>
									<s:else>
										<a href="javascript: void(0);" onClick="agregarDetalleServicio(<s:property value="tipoServicioOperacion"/>)" class="icon_guardar2"> 
												<s:text name="midas.boton.agregar" /> 
										</a>
									</s:else>
											
								</div> 
							</s:if>
							
						</td>							
					</tr>
				</table>				
			</td>
            	
            </tr>
	</table>

	<br/>
		
</s:form>

<div id="cargarFotoModalDiv" style="display: none">
	<form target="transFrame" id="formCargarFoto" action="${pageContext.request.contextPath}/persona/guardarFoto.action" method="post" enctype="multipart/form-data">
		<input type="hidden" name="idPersona" value="${servicioSiniestroDto.idPersonaMidas}" />
		<img class="fotoPersona" alt="Foto" src="${pageContext.request.contextPath}/persona/obtenerFoto.action?idPersona=${servicioSiniestroDto.idPersonaMidas}&random=${random.nextInt()}" />
		<div class="center"><input type="file" name="foto"></inpu><button id="btnCargarFoto"><s:text name="%{getText('midas.boton.actualizar')}"/></button></div>
	</form>
	<iframe id="transFrame" style="display:none" name="transFrame">
	</iframe>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/dwr/engine.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/dwr/interface/componenteService.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/dwr/interface/listadoService.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/midas2/componente/direccion/direccionSiniestroMidas.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/midas2/siniestros/catalogo/serviciosiniestro/servicioSiniestros.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/midas2/siniestros/catalogo/serviciosiniestro/contenedorAltaServicioSiniestro.js" ></script>
<s:if test="readOnly == true" >
	<script type="text/javascript">
		deshabilitarComponentes();
	</script>	
</s:if>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
