<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="c_id" 				type="ro" width="30" 	sort="int" hidden="false">No. de Ajustador</column>
        <column id="c_nombreAjustador" 	type="ro" width="170" 	sort="str" hidden="false">Nombre del Ajustador</column>
        <column id="c_estado" 			type="ro" width="80" 	sort="str" hidden="false">Estado</column>
        <column id="c_municipio" 		type="ro" width="80" 	sort="str" hidden="false">Municipio</column>
      	<column id="c_tipoAjustador" 	type="ro" width="80" 	sort="str" hidden="false">Tipo de Ajustador</column>
		<column id="c_nombrePrestador" 	type="ro" width="170"	sort="str" hidden="false">Nombre Prestador</column>
		<column id="c_estatus" 			type="ro" width="80" 	sort="str" hidden="false">Estatus</column>
		<column id="c_estatus" 			type="ro" width="110" 	sort="str" hidden="false">Tipo de Disponibilidad</column>
		<column id="c_telefono" 		type="ro" width="90" 	sort="str" hidden="false">Telefono</column>
		<column id="c_telefono2" 		type="ro" width="90" 	sort="str" hidden="false">Telefono (2)</column>
		<column id="c_celular" 			type="ro" width="90" 	sort="str" hidden="false">Celular</column>
		<column id="c_otros" 			type="ro" width="90" 	sort="str" hidden="false">Otros</column>
		<column id="c_otros2" 			type="ro" width="90" 	sort="str" hidden="false">Otros(2)</column>
		
	</head>
	<s:iterator value="serviciosSiniestrosGrid" status="row">
		<row id="<s:property value="id" escapeHtml="false" escapeXml="true" />">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="municipio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:text name="%{'midas.servicio.siniestros.asignacion.ambito.' + ambitoPrestadorServicio}"/></cell>
			<cell><s:property value="prestadorServicio.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:text name="%{'midas.servicio.siniestros.asignacion.estatus.' + estatusAsignacion}"/></cell>
			<cell><s:text name="%{'midas.servicio.siniestros.asignacion.disponibilidad.' + tipoDisponibilidad}"/></cell>			
			<cell><s:property value="personaMidas.contacto.telCasaCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.contacto.telOficinaCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.contacto.telCelularCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.contacto.telOtroCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="personaMidas.contacto.telOtro2Completo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>