<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/direccion/direccionSiniestroMidas.js'/>"  type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 

</head>
<body>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<style type="text/css">
.dhx_pbox_light{
	-moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none; 
    border-color: -moz-use-text-color #C2D5DC #C2D5DC;
    border-image: none;
    border-right: 1px solid #C2D5DC;
    border-style: none solid solid;
    border-width: 0 1px 1px;
    font-size: 10px;
    margin-top: 3px; 
    width: 98%;
}

.dhx_pager_info_light{
	color: #055A78;
    cursor: pointer;
    font-family: tahoma;
    font-size: 12px;
    text-align: center; 
}
</style>

<s:form name="busquedaGeneral" id="busquedaServicio" method="post" action="busquedaGeneral" >

	<!-- HIDDEN -->
	<input id="h_modoAsignar"  name="modoAsignar" value="true" type="hidden"/>
	<input id="h_tipoServicioOperacion"  name="tipoServicioOperacion" value="1" type="hidden"/>
	
	<table width="99%" id="filtros">
	
			<tr>
				<td class="titulo" colspan="4">
					<s:property value="leyendaServicioCabeza[0]" />	
				</td>
				<s:hidden name="reporteCabinaId" ></s:hidden>
			</tr>
			<tr>
				<td> 
					<s:textfield name="servicioSiniestroFiltro.noAbogadoAjustadorId" 
								 id="noAbogadoAjustadorId" 
								 cssClass="jQnumeric jQrestrict cajaTextoM2 w120" 
								 maxlength="10"
								 label="No. Ajustador"/>  
				</td>
				<td>
					<s:select list="valoresEstatusAsignacion" 
							  label="Estatus"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							  name="servicioSiniestroFiltro.estatusAsignacion" 
							  id="estatus" 
							  cssClass="cajaTextoM2 w120" 
							  onchange=""/> 
				</td>
				<td>
					<s:select list="estados" 					
							  label="Estados"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							  name="servicioSiniestroFiltro.estado" 
							  id="s_estado" 
							  cssClass="cajaTextoM2 w120" 
							  onchange="onChangeEstadoFiltro('s_municipios','s_estado')"/> 
				</td>
				<td>
					<s:select 
						list="valoresTipoServicio" 						
						label="Tipo de Ajustador"
						headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						name="servicioSiniestroFiltro.tipoAbogadoAjustador" 
						id="tipoAbogadoAjustador" 
						cssClass="cajaTextoM2 w120"/> 
				</td>			

			</tr>
			<tr>	
				<td> 
					<s:textfield name="servicioSiniestroFiltro.nombreAbogadoAjustador" 
								 id="nombreAbogadoAjustador" 
								 maxlength="60"
								 label="Nombre de Ajustador"
								 cssClass="jQalphabeticExt jQrestrict cajaTextoM2 w200" />
				</td>
				<td>
					<s:select list="valoresTipoDisponibilidad" 
							  label="Tipo de Disponibilidad"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							  name="servicioSiniestroFiltro.tipoDisponibilidad" 
							  id="estatus" 
							  cssClass="cajaTextoM2 w120" 
							  onchange=""/> 
				</td>
				<td>
					<s:select list="municipios" 						
							  label="Municipios"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							  name="servicioSiniestroFiltro.municipio" 
							  id="s_municipios" 
							  cssClass="cajaTextoM2 w120"/> 
				</td>
				<td>
					<div class="btn_back w120"  style=" float:left;  ">
						<a href="javascript: void(0);" onClick="busquedaServicioSiniestro();" class="icon_buscar"> 
							<s:text name="midas.boton.consultar" /> 
						</a>
					</div>
				</td>
			</tr>

	</table>
	
	
	<br>
	<div class="titulo">
		<s:property value="leyendaServicioCabeza[1]" />	
	</div>
</s:form>

<div id="indicador"></div>
<div id="servicioSiniestroGrid" style="width: 98%;height:300px"></div>
<div id="pagingArea" >
	<div style="width: 100%; clear: both;">
		<div id="d_boxLight">
			&nbsp;
		</div>
		<div id="d_pagerInfo">
		</div>
		&nbsp;
	</div>
</div>
<div id="infoArea"></div>

<script src="<s:url value='/js/midas2/siniestros/catalogo/serviciosiniestro/servicioSiniestros.js'/>" ></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript">
	busquedaServicioSiniestro();
</script>

</body>
</html>