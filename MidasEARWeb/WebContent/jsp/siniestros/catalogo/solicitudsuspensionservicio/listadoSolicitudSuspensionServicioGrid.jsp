<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="id" type="ro" width="100" sort="int" escapeXml="true" align="center" ><s:text name="midas.solicitudSuspensionServicio.numeroSolicitudSuspensionServicio"/></column>
		<column id="numeroSerie" type="ro" width="200" sort="str"><s:text name="midas.solicitudSuspensionServicio.numeroSerie"/></column>
		<column id="estatus" type="ro" width="150" sort="str" align="center"><s:text name="midas.solicitudSuspensionServicio.estatus"/></column>
		<column id="fechaInicio" type="ro" width="150" sort="date_custom" align="center" ><s:text name="midas.solicitudSuspensionServicio.fechaInicio"/></column>
		<column id="fechaFin" type="ro" width="150"  sort="str" align="center" ><s:text name="midas.solicitudSuspensionServicio.fechaFin"/></column>		
		<column id="oficina" type="ro" width="*"  sort="str" align="center" ><s:text name="midas.solicitudSuspensionServicio.oficina"/></column>
		<column id="editar" type="img" width="40" sort="na" align="center">Acciones</column>
		<column id="consultar" type="img" width="40" sort="na">#cspan</column>	
		<column id="eliminar" type="img" width="40" sort="na">#cspan</column>	
	
	</head>			
	<s:iterator value="listSuspensionServicios" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatusName" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="fechaInicio" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaFin" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="oficina.nombreOficina" escapeHtml="true" escapeXml="true"/></cell>	
			<cell>../img/icons/ico_editar.gif^Editar^javascript: mostrarEditarSuspensionServicio(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript: mostrarConsultarSuspensionServicio(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/delete14.gif^Borrar^javascript:  eliminarConsultarSuspensionServicio(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>																
		</row>
	</s:iterator>	
</rows>