<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/siniestros/catalogo/solicitudsuspensionservicio/solicitudSuspensionServicio.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.labelBlack{
 	color:black;
    width: 98%;
	line-height: 20px;
	margin-top: 6px;
	margin-bottom: 6px;
	font-size: 10px;
	font-weight: bold;
	text-align: right;	
}
</style>
<script type="text/javascript">
var buscarSuspensionServiciosPath = '<s:url action="buscar" namespace="/siniestros/catalogos/suspensionServicio"/>';
var mostrarSuspensionServicioPath = '<s:url action="mostrarSuspensionServicio" namespace="/siniestros/catalogos/suspensionServicio"/>';
</script>
<s:form id="suspensionServicioForm" >
<table id="filtrosM2" style="width: 98%;">
	<tr>
		<td colspan="4">
			<div class="titulo" style="width: 98%;">
				<s:text name="midas.solicitudSuspensionServicio.title"/>	
			</div>	
		</td>
	</tr>
	<tr>
		<td><s:textfield cssClass="jQrestrict cajaTextoM2 jQnumeric" 
							  key="midas.solicitudSuspensionServicio.numeroSolicitudSuspensionServicio"
							 name="filtroCatalogo.numeroCatalogo"							
					labelposition="left" 
							 size="10"					
							   id="txt_id"/>
		</td>
		<td><s:textfield cssClass="jQrestrict cajaTextoM2 jQalphanumeric jQNumeroSerie " 
							  key="midas.solicitudSuspensionServicio.numeroSerie"
							 name="filtroCatalogo.numeroSerie"
					labelposition="left" 
							 size="17"		
							 maxlength="17"									
							   id="txt_numeroSerie"/>
		</td>
		<td>
			<s:select id="s_estatus" key="midas.general.estatus"
						labelposition="left" 
						name="filtroCatalogo.estatus"
						headerKey="" headerValue="%{getText('midas.general.seleccione')}"
				  		list="listEstatus" listKey="key" listValue="value"  
				  		cssClass="cajaTextoM2" /> 	
		</td>
		<td>
			<s:select id="s_oficina" key="midas.solicitudSuspensionServicio.oficina"
						   labelposition="left" 
									name="filtroCatalogo.oficinaId"
							   headerKey="" headerValue="%{getText('midas.general.seleccione')}"
				  				    list="listOficinas" listKey="key" listValue="value"  
				  				cssClass="cajaTextoM2" /> 	
		</td>		
	</tr>
	<tr>
		<td>
		</td>
	    <td>
			<sj:datepicker name="filtroCatalogo.fechaInicio"
							key="midas.solicitudSuspensionServicio.fechaInicio"
				  labelposition="left"
					changeMonth="true"
					 changeYear="true"				
					buttonImage="../img/b_calendario.gif"
					buttonImageOnly="true" 
                      		id="filtroCatalogo.fechaInicio"
					  maxlength="10" cssClass="cajaTextoM2"
						   size="12"
					 onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>
		</td>
		<td colspan="2">
			<sj:datepicker name="filtroCatalogo.fechaFin"
							key="midas.solicitudSuspensionServicio.fechaFin"
				  labelposition="left"
					changeMonth="true"
					 changeYear="true"				
					buttonImage="../img/b_calendario.gif"
					buttonImageOnly="true" 
                      			id="filtroCatalogo.fechaFin"
					  maxlength="10" cssClass="cajaTextoM2"
						   size="12"
					 onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
			</sj:datepicker>
		 </td>	
	</tr>	
	<tr>		
		<td colspan="4">		
			<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td  class= "guardar">
						<div class="alinearBotonALaDerecha" style="display: inline; float: right; margin-left: 2%;" id="b_buscar">
							 <a href="javascript: void(0);" onclick="mostrarListadoSuspensionServicios(true);">
							 <s:text name="midas.boton.buscar" /> </a>
						</div>
						<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
							<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
							<s:text name="midas.boton.limpiar" /> </a>
						</div>	
					</td>							
				</tr>
			</table>				
		</td>		
	</tr>
	</table>
</s:form>
<br/>

			<div class="titulo" style="width: 98%;">
				<s:text name="midas.solicitudSuspensionServicio.listadoSuspension"/>	
			</div>	

<div id="listadoSuspensionServiciosGrid" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="javascript:exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_agregar">
				<a href="javascript: void(0);" onclick="mostrarAgregarSuspensionServicio();"> 
				<s:text name="midas.boton.agregar" /> </a>
			</div>	
		</td>
	</tr>
</table>	
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<script>
	jQuery(document).ready(function(){
		inicializarListadosuspensionServicios();		
	});
</script>