<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/siniestros/catalogo/solicitudsuspensionservicio/solicitudSuspensionServicio.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.labelBlack{
 	color:black;
    width: 98%;
	line-height: 20px;
	margin-top: 6px;
	margin-bottom: 6px;
	font-size: 10px;
	font-weight: bold;
	text-align: right;	
}

.error {
	background-color: red;
	opacity: 0.4;
}
</style>
<script type="text/javascript">
	var mostrarCatalogoSuspensionServicioPath   = '<s:url action="mostrarContenedor" namespace="/siniestros/catalogos/suspensionServicio"/>';
	var guardarSuspensionServicioPath           = '<s:url action="guardar" namespace="/siniestros/catalogos/suspensionServicio"/>';
</script>


<s:form id="suspensionServicioForm"  action="guardar" namespace="/siniestros/catalogos/suspensionServicio" name="suspensionServicioForm">
	<s:hidden name="tipoVentana" id="h_tipoVentana"></s:hidden>
	<s:hidden name="idSuspensionServicio" id="h_idSuspensionServicio"></s:hidden>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.solicitudSuspensionServicio.contenedor.title"/>	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td><s:textfield cssClass="jQalphanumeric jQrequired jQNumeroSerie jQrestrict cajaTextoM2 w160" 
										  key="midas.solicitudSuspensionServicio.numeroSerie"
										 name="entidad.numeroSerie"
										 value="%{entidad.numeroSerie}"
										labelposition="left" 
										 size="18"			
										 maxlength="17"												
										   id="txt_numeroSerie"/>
					</td>
					<td>
						<s:select id="s_estatus" key="midas.general.estatus"
									labelposition="left" 
									name="entidad.estatus"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="listEstatus" listKey="key" listValue="value"  
							  		cssClass="cajaTextoM2 jQrequired" /> 	
					</td>
					<td>
						<s:select id="s_oficina" key="midas.solicitudSuspensionServicio.oficina"
									   labelposition="left" 
												name="entidad.oficina.id"
										   headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  				    list="listOficinas" listKey="key" listValue="value"  
							  				cssClass="cajaTextoM2 jQrequired" /> 	
					</td>		
				</tr>
				<tr>
				    <td>
						<sj:datepicker name="entidad.fechaInicio"
										key="midas.solicitudSuspensionServicio.fechaInicio"
							  labelposition="left"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="/MidasWeb/img/b_calendario.gif"
								buttonImageOnly="true" 
	                        			 id="dp_fechaInicio"
	                        			 value="%{entidad.fechaInicio}"
								  maxlength="10" cssClass="cajaTextoM2 jQrequired"
									   size="12"
									   readOnly="true"
								 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
					</td>
					<td colspan="2">
						<sj:datepicker name="entidad.fechaFin"
										key="midas.solicitudSuspensionServicio.fechaFin"
							  labelposition="left"
								changeMonth="true"
								 changeYear="true"				
								buttonImage="/MidasWeb/img/b_calendario.gif"
								buttonImageOnly="true" 
	                        			 id="dp_fechaFin"
	                        		   value="%{entidad.fechaFin}"
								  maxlength="10" cssClass="cajaTextoM2 jQrequired"
									   size="12"
									   readOnly="true"
								 onkeypress=" return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
					 </td>	
				</tr>	
				<tr>
					<td colspan="4">
						<s:textarea name="entidad.motivo" 
							id="ta_motivo" labelposition="top"
							key="midas.solicitudSuspensionServicio.motivo" 
							cols="100"
							rows="15"
							maxlength="500"
							cssClass="cajaTextoM2 jQrequired jQalphaextra jQrestrict" 
							disabled="consulta"
						/>
					</td>
				</tr>
				<tr>		
					<td colspan="4">		
						<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td>
									<s:if test="%{tipoVentana == 3}">
										<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
											<a href="javascript: void(0);" onclick="javascript: salir();"> 
											<s:text name="midas.boton.cerrar" /> </a>
										</div>	
									</s:if>
									<s:else>
										<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
											<a href="javascript: void(0);" onclick="javascript: salirConfirmar();"> 
											<s:text name="midas.boton.cerrar" /> </a>
										</div>	
										<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
											<a href="javascript: void(0);" onclick="javascript: guardarSuspensionServicio();"> 
											<s:text name="midas.boton.guardar" /> </a>
										</div>
									</s:else>		
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
	</div>
</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
	if( jQuery('#h_tipoVentana').val() == 3 ){
		limpiarFiltros();
		jQuery('#b_agregar').hide();	
		jQuery("#txt_numeroSerie").attr("disabled",true);
		jQuery("#s_estatus").attr("disabled",true);
		jQuery("#s_oficina").attr("disabled",true);
		jQuery("#dp_fechaInicio").attr("disabled",true);
		jQuery("#dp_fechaFin").attr("disabled",true);
		jQuery("#ta_motivo").attr("disabled",true);
		jQuery(".ui-datepicker-trigger").hide();
	}
	onBlurRequeridos();

});

</script>