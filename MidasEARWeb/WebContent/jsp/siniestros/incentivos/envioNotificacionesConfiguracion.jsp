<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/incentivos/incentivos.js'/>"></script>
<script src="<s:url value='/js/midas2/juridico/reclamacionesJuridico.js'/>" ></script>


<sj:head/>


<style type="text/css">
	#superior{
		height: 200px;
		width: 99%;
	}

	#superiorI{
		height: 150px;
		width: 850px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		width: 210px;
		float: left;
		margin-left: 70%;
	}
	
	
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	label{
		font-family: Verdana,Arial,Helvetica,sans-serif; font-size:   9.33333px; text-align:  left
	}

</style>

<div class="titulo" style="margin-left: 2%">Histórico de Envios</div>
<s:form id="envioNotificacionForm" action="enviaNotificacion" namespace="/siniestros/incentivoAjustador" name="envioNotificacionForm">
<s:hidden id="configuracionId" name="configuracionId"/>
<table id="envioNotTable" border="0">
	<tr>
		<td>
			<s:text name="midas.siniestros.rcuperacion.ingresos.facturar.impresion.correosRemitentes"/>
		</td>
	</tr>	
	<tr>		
		<td>
			<s:textfield id="correosRemitentes"
			name="destinatariosStr" cssClass="cajaTextoM2LC jQrequired jQEmailList w500" />	 				 
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.siniestros.rcuperacion.ingresos.facturar.observaciones"/>
		</td>	
	</tr>
	<tr>
		<td colspan="2"><font color="#FF6600"><label for="agenteNombre" ><s:text name="midas.siniestros.rcuperacion.ingresos.facturar.NotaReenvio" ></s:text>:</label></font></td>
	</tr>
</table>	
<div id="botonReenviar" class="btn_back w150 " style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="if(validateAll(false)){enviarNotificacion();}">
		<s:text name="midas.boton.reenviar" /> 
	</a>
</div>

<div id="botonCerrar" class="btn_back w150 " style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('vm_notificacionIncentivo','mostrarConfiguracionIncentivos();');">
		<s:text name="midas.boton.cerrar" /> 
	</a>
</div>
</s:form>

	<div id="indicador"></div>
	<div id="inferior" >
		<div id="historicoDeEnviosGrid" style="width:98%;height:300px;margin-left: 2%">
		</div>
		<div id="pagingArea" style="margin-left: 2%"></div><div id="infoArea" style="margin-left: 2%"></div>
	</div>

<script type="text/javascript">
	cargaHistoricoDeEnvios();
</script>