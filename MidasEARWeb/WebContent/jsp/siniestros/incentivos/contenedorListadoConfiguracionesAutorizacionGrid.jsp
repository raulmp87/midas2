<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="folio"		 type="ro" width="*"  sort="int" ><s:text name="%{'midas.siniestros.incentivos.folio'}"/></column>	
        <column id="fechaInicio"  type="ro" width="*" sort="date_custom" ><s:text name="%{'midas.siniestros.incentivos.periodoInicio'}"/></column>
        <column id="fechaFin"           type="ro" width="*" sort="date_custom" ><s:text name="%{'midas.siniestros.incentivos.periodoFin'}"/> </column>
		<column id="usuarioCreador"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.incentivos.usuarioCreador'}"/> </column>
		<column id="fechaCreacion"   type="ro" width="*" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.fechaCreacion'}"/> </column>
		<column id="UsuarioAutorizacion"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.incentivos.usuarioAutorizador'}"/> </column>
		<column id="fechaAutorizacion"   type="ro" width="*" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.fechaAutorizacion'}"/> </column>
		<column id="oficina"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.incentivos.oficina'}"/> </column>
		<column id="estatus"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.incentivos.estatus'}"/> </column>
		<column id="autorizar" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="rechazar" type="img" width="70" sort="na" align="center">#cspan</column>
		
		
		</head>
		<s:iterator value="configuraciones">
			<row>
				<cell><s:property value="folio" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="periodoInicial" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="periodoFinal" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="UsuarioAutorizador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaAutorizacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="estatusStr == \'XAUTORIZAR\' || estatusStr == \'PREAPROBAD\'" >
				<cell>/MidasWeb/img/icons/ico_terminar.gif^Autorizar^javascript:autoriza(<s:property value="id" escapeHtml="false" escapeXml="true" />)^_self</cell>
				</s:if><s:else><cell>/MidasWeb/img/blank.gif^Autorizar^^_self</cell></s:else>
				<s:if test="estatusStr == \'XAUTORIZAR\' || estatusStr == \'PREAPROBAD\'" >
				<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Rechazar^javascript:rechaza(<s:property value="id" escapeHtml="false" escapeXml="true" />)^_self</cell>
				</s:if>
				<s:else><cell>/MidasWeb/img/blank.gif^NULL^^_self</cell></s:else>
			</row>
		</s:iterator>
	
</rows>
