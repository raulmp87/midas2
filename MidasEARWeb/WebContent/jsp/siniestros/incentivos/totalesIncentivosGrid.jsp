<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaPercepcionesTotales</param>
				<param>true</param>
				<param>infoAreaPercepcionesTotales</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="numeroAjustador"  type="ro" width="125"  sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.numajustador'}"/></column>	
        <column id="nombreAjustador"  type="ro" width="250" sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.ajustador'}"/></column>
        <column id="inhabiles"        type="ro" width="100" sort="int" ><s:text name="%{'midas.siniestros.incentivos.generacion.inhabiles'}"/> </column>
		<column id="efectivo"         type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.incentivos.configuracion.efectivo'}"/> </column>
		<column id="companias"        type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.incentivos.configuracion.companias'}"/> </column>
		<column id="sipac"            type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.incentivos.configuracion.sipac'}"/> </column>
		<column id="rechazos"         type="ro" width="130" sort="int"><s:text name="%{'midas.siniestros.incentivos.generacion.rechazos'}"/> </column>
		<column id="importe"   		  type="ro" width="95" sort="int"><s:text name="%{'midas.siniestros.incentivos.generacion.secciontotal'}"/> </column>
			
		</head>
		<s:iterator value="totalesIncentivos">
			<row>
				<cell><s:property value="numeroAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotalInhabiles})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotalEfectivo})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotalCompanias})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotalSIPAC})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotalRechazos})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoTotal})}" escapeHtml="false" escapeXml="true" /></cell>
				
			</row>
		</s:iterator>
	
</rows>
