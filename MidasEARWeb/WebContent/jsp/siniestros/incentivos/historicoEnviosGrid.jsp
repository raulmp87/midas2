<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="fechaHora"  type="ro" width="*" sort="date_custom" ><s:text name="%{'midas.siniestros.incentivos.configuracion.fechaHora'}"/></column>
        <column id="usuarioRemitente"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.incentivos.configuracion.usuarioRemitente'}"/> </column>
		<column id="destinatario"   type="ro" width="*" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.configuracion.destinatarios'}"/> </column>
	</head>
		<s:iterator value="historicosEnvio">
			<row>
				<cell><s:date name="fechaCreacion"   format="dd/MM/yyyy HH:mm" /></cell>
				<cell><s:property value="usuarioRemitente" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="destinatrarios" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
	
</rows>
