<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<script type="text/javascript">
	var generarConfigIncentivosPath= '<s:url action="generarConfiguracionAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var buscarConfigIncentivosPath = '<s:url action="buscarPercepcionesAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var imprimirConfigIncentivosPath= '<s:url action="imprimirPercepcionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var guardarConfiguracionIncentivosPath = '<s:url action="guardarConfiguracionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var cancelarConfigIncentivosPatch = '<s:url action="cancelarConfiguracionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var consultarDetalleIncentivosPath = '<s:url action="generarConfiguracionAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var mostrarContenedorDesglosePercepcionesPath = '<s:url action="mostrarContenedorDesglosePercepciones" namespace="/siniestros/incentivoAjustador"/>';
	var mostrarConfiguracionIncentivosPath = '<s:url action="mostrarContenedorIncentivosAjustador" namespace="/siniestros/incentivoAjustador"/>';
</script>

<s:form id="configuracionIncentivosForm" >
		<s:hidden id="configuracionId" name="configuracion.id"/>
				
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.incentivos.configuracion.tituloconfiguracion"/>
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<th style="width: 7%;"><s:text name="midas.siniestros.incentivos.configuracion.periodode" /> </th>
					<td style="width: 10%;">
						<sj:datepicker name="configuracion.periodoInicial"
								changeMonth="true"
								changeYear="true" buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" id="periodoDe" maxlength="18"
								cssStyle="" cssClass="txtfield setNew jQrequired" size="10"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
					</td>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.periodohasta" /> </th>
					<td style="">
						<sj:datepicker name="configuracion.periodoFinal"
								changeMonth="true"
								changeYear="true" buttonImage="../img/b_calendario.gif"
								buttonImageOnly="true" id="periodoHasta" maxlength="18"
								cssStyle="" cssClass="txtfield setNew jQrequired" size="10"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
					</td>
					
					<th style="padding-left: 5%;width: 10%;"><s:text name="midas.siniestros.incentivos.configuracion.oficina" /> </th>
					<td style="width: 15%;">
						<s:select list="oficinasActivas" id="oficinasActivas"
							name="configuracion.oficina.id" 
							cssClass="jQrequired setNew txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</td>
				</tr>
			</tbody>
		</table>
		
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<td style="width: 5%;">
						<s:checkbox id="esDiasInhabiles" name="configuracion.esDiasInhabiles" cssClass="" onclick="onClickActivarTextfield(this,jQuery('#montoDiaInhabil'),'$100.00')"></s:checkbox>
					</td>
					<th style="width: 30%;"><s:text name="midas.siniestros.incentivos.configuracion.inhabiles" /> </th>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.monto" /> </th>
					<td style="">
						<s:textfield id="montoDiaInhabil" name="configuracion.montoDiaInhabil" cssClass="cajaTexto w250 monto formatCurrency mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
				</tr>
			</tbody>
		</table>
		
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<td style="width: 5%;">
						<s:checkbox id="esRecuperacionEfectivo" name="configuracion.esRecuperacionEfectivo" cssClass="" onclick="onClickActivarEfectivo();"></s:checkbox>
					</td>
					<th style="width: 30%;"><s:text name="midas.siniestros.incentivos.configuracion.efectivo" /> </th>
					
					<td style="width: 5%;">
						<s:checkbox id="esRangoEfectivo" name="configuracion.esRangoEfectivo" cssClass="" onclick="onClickActivarRangoEfectivo();" disabled="true"></s:checkbox>
					</td>
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.rangomontode" /> </th>
					<td style="">
						<s:textfield id="montoRangoEfectivo" name="configuracion.montoRangoEfectivo" cssClass="cajaTexto w120 monto formatCurrency mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.rangomontohasta" /> </th>
					<td style="">
						<s:textfield id="montoHastaRangoEfectivo" name="configuracion.montoHastaRangoEfectivo" cssClass="cajaTexto w120 formatCurrency monto mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.porcentaje" /> </th>
					<td style="">
						<s:textfield id="porcentajeRangoEfectivo" name="configuracion.porcentajeRangoEfectivo" cssClass="cajaTexto w120 porcentaje mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
				</tr>
				<tr>
					<td style="width: 5%;">
					</td>
					<th style="width: 30%;"></th>
					
					<td style="width: 5%;">
						<s:checkbox id="esExcedenteSiniestro" name="configuracion.esExcedenteSiniestro" cssClass="" onclick="onClickActivarExcedenteEfectivo();" disabled="true"></s:checkbox>
					</td>
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.excedente" /> </th>
					<td style="">
						<s:select list="condicionesEfectivo" id="condicionExcedente"
							name="configuracion.condicionExcedente" 
							cssClass="setNew txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;"  disabled="true">
						</s:select>
					</td>
					
					<td></td>
					
					<td style="">
						<s:textfield id="montoExcedente" name="configuracion.montoExcedente" cssClass="cajaTexto w120 monto formatCurrency mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.porcentaje" /> </th>
					<td style="">
						<s:textfield id="porcentajeExcedente" name="configuracion.porcentajeExcedente" cssClass="cajaTexto w120 porcentaje mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
				</tr>
			</tbody>
		</table>
		
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<td style="width: 5%;">
						<s:checkbox id="esRecuperacionCia" name="configuracion.esRecuperacionCia" cssClass="" onclick="onClickActivarTextfield(this,jQuery('#montoRecuperacionCia'),'$150.00')"></s:checkbox>
					</td>
					<th style="width: 30%;"><s:text name="midas.siniestros.incentivos.configuracion.companias" /> </th>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.monto" /> </th>
					<td style="">
						<s:textfield id="montoRecuperacionCia" name="configuracion.montoRecuperacionCia" cssClass="cajaTexto w250 monto formatCurrency mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
				</tr>
			</tbody>
		</table>
		
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<td style="width: 5%;">
						<s:checkbox id="esRecuperacionSipac" name="configuracion.esRecuperacionSipac" cssClass="" onclick="onClickActivarTextfield(this,jQuery('#montoRecuperacionSipac'),'$150.00')"></s:checkbox>
					</td>
					<th style="width: 30%;"><s:text name="midas.siniestros.incentivos.configuracion.sipac" /> </th>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.monto" /> </th>
					<td style="">
						<s:textfield id="montoRecuperacionSipac" name="configuracion.montoRecuperacionSipac" cssClass="cajaTexto w250 monto formatCurrency mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
				</tr>
			</tbody>
		</table>
		
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					<td style="width: 5%;">
						<s:checkbox id="esSiniestroRechazado" name="configuracion.esSiniestroRechazado" cssClass="" onclick="onClickActivarTextfield(this,jQuery('#porcentajeRechazo'),'10')"></s:checkbox>
					</td>
					<th style="width: 30%;"><s:text name="midas.siniestros.incentivos.configuracion.rechazados" /> </th>
					
					<th style="width: 9%;"><s:text name="midas.siniestros.incentivos.configuracion.porcentaje" /> </th>
					<td style="">
						<s:textfield id="porcentajeRechazo" name="configuracion.porcentajeRechazo" cssClass="cajaTexto w250 porcentaje jQ2float mayorQueCero" onkeypress="return soloNumeros(this, event, true, true)" disabled="true"></s:textfield>
					</td>
					
				</tr>
			</tbody>
		</table>
		</div>
</s:form>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<div id="btn_generar" class="btn_back w160" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:generarConfiguracionIncentivo();">
						<s:text name="midas.siniestros.incentivos.configuracion.generar" />
						<img border='0px' alt='Generar Incentivo' title='Generar Incentivo' src='/MidasWeb/img/b_aceptar.gif'/> 
					</a>
				</div>
				<div id="btn_limpiar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:limpiarConfiguracionIncentivo();"> 
						<s:text name="midas.boton.limpiar" /> 
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.incentivos.titulolistado"/>
</div>
<div id="indicadorPercepcionesAjustadores"></div>	
<div id="percepcionesAjustadoresGridContainer" style="">
	<div id="listadoPercepcionesAjustadoresGrid" style="width:98%;height:200px;"></div>
	<div id="pagingAreaPercepcionesAjustadores"></div><div id="infoAreaPercepcionesAjustadores"></div>
</div>

<script src="<s:url value='/js/midas2/siniestros/incentivos/incentivos.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script>
	jQuery(document).ready(function(){
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA
		initConfiguracionIncentivo();
	});
</script>