<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaPercepcionesAjustadores</param>
				<param>true</param>
				<param>infoAreaPercepcionesAjustadores</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="folio"		 			type="ro" width="125"  sort="int" ><s:text name="%{'midas.siniestros.incentivos.folio'}"/></column>	
        <column id="fechaInicio" 			type="ro" width="95" sort="date_custom" ><s:text name="%{'midas.siniestros.incentivos.periodoInicio'}"/></column>
        <column id="fechaFin"           	type="ro" width="95" sort="date_custom" ><s:text name="%{'midas.siniestros.incentivos.periodoFin'}"/> </column>
		<column id="usuarioCreador"   		type="ro" width="130" sort="str"><s:text name="%{'midas.siniestros.incentivos.usuarioCreador'}"/> </column>
		<column id="fechaCreacion"   		type="ro" width="95" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.fechaCreacion'}"/> </column>
		<column id="usuarioAutorizacion"	type="ro" width="125" sort="str"><s:text name="%{'midas.siniestros.incentivos.usuarioAutorizador'}"/> </column>
		<column id="fechaAutorizacion"   	type="ro" width="95" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.fechaAutorizacion'}"/> </column>
		<column id="oficina"   				type="ro" width="145" sort="str"><s:text name="%{'midas.siniestros.incentivos.oficina'}"/> </column>
		<column id="estatus"   				type="ro" width="95" sort="str"><s:text name="%{'midas.siniestros.incentivos.estatus'}"/> </column>
		<column  type="img"   width="30" sort="na" align="center" >Acciones</column>
		<column  type="img"   width="30" sort="na" align="center" >#cspan</column>
		<column  type="img"   width="30" sort="na" align="center" >#cspan</column>
		<column  type="img"   width="30" sort="na" align="center" >#cspan</column>
		<column  type="img"   width="*" sort="na" align="center" >#cspan</column>
		
		</head>
		<s:iterator value="configuraciones">
			<row>
				<cell><s:property value="folio" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="periodoInicial" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="periodoFinal" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="codigoUsuarioPrimerNivel" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaAutorizacionPrimerNivel" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatus.getLabel()" escapeHtml="false" escapeXml="true" /></cell>
				<s:if test="estatus.toString() == \"REGISTRADO\" || estatus.toString() == \"XAUTORIZAR\" || estatus.toString() == \"PREAPROBAD\" || estatus.toString() == \"RECHAZADO\" || estatus.toString() == \"AUTORIZADO\" || estatus.toString() == \"ENVIADO\" ">
					<cell>../img/icons/ico_verdetalle.gif^Consultar Detalle^javascript:consultarDetalle(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
					<cell>../img/icons/ico_verdetalle.gif^Consultar Percepciones^javascript:consultarPercepcion(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Consultar Detalle^^_self</cell>
					<cell>../img/pixel.gif^Consultar Percepciones^^_self</cell>
				</s:else>
				<s:if test="estatus.toString() == \"RECHAZADO\" || estatus.toString() == \"REGISTRADO\"">
					<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Cancelar Percepciones^javascript:cancelarPercepcionLista(<s:property value="id" escapeHtml="false" escapeXml="true" />)^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Cancelar Percepciones^^_self</cell>
				</s:else>
				<s:if test="estatus.toString() == \"AUTORIZADO\" || estatus.toString() == \"ENVIADO\"">
					<cell>../img/new.png^Enviar Correo^javascript:enviarCorreo(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
					<cell>../img/menu_icons/print.gif^Imprimir Percepciones^javascript:imprimirPercepcionIncentivos(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Enviar Correo^^_self</cell>
					<cell>../img/pixel.gif^Imprimir Percepciones^^_self</cell>
				</s:else>
			</row>
		</s:iterator>
	
</rows>
