<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaDetallePercepcion</param>
				<param>true</param>
				<param>infoAreaDetallePercepcion</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        
        <column id="selected" 			type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
		<column id="concepto"		 	type="ro" width="125"  sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.concepto'}"/></column>	
        <column id="numAjustador" 		type="ro" width="125" sort="int" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.numajustador'}"/></column>
        <column id="ajustador"          type="ro" width="125" sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.ajustador'}"/> </column>
		<column id="oficinaAjustador"   type="ro" width="145" sort="str"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.oficinaajustador'}"/> </column>
		<column id="numReporte"   		type="ro" width="125" sort="str"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.numreporte'}"/> </column>
		<column id="oficinaReporte"		type="ro" width="125" sort="str"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.oficinareporte'}"/> </column>
		<column id="fechaAsignacion"   	type="ro" width="100" sort="date_custom"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.fecha'}"/> </column>
		<column id="horaAsignacion"   	type="ro" width="95" sort="str"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.hora'}"/> </column>
		<column id="montoRecuperado"   	type="ron" width="95" sort="int" format="$0,000.00"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.montorecuperado'}"/> </column>
		<column id="montoRechazado"   	type="ron" width="95" sort="int" format="$0,000.00"><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.montorechazado'}"/> </column>
		</head>
		
		<s:iterator value="incentivos">
			<row id="<s:property value="incentivoId"/>">
				<cell><s:property value="seleccionado" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="conceptoPagoDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficinaAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficinaReporte" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaAsignacionAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:date name="fechaAsignacionAjustador"   format="HH:mm" /></cell>
				<s:if test="tipoConceptoPago != \"RECHAZO\" && montoRecuperacion != null">
					<cell><s:property value="montoRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				</s:if>
				<s:else>
					<cell><s:text name="0.00" /></cell>
				</s:else>
				<s:if test="tipoConceptoPago == \"RECHAZO\" && montoRecuperacion != null">
					<cell><s:property value="montoRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				</s:if>
				<s:else>
					<cell><s:text name="0.00" /></cell>
				</s:else>
			</row>
		</s:iterator>
	
</rows>