<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/incentivos/incentivos.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<style type="text/css">
 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<script type="text/javascript">
	var generarConfigIncentivosPath= '<s:url action="generarConfiguracionAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var buscarConfigIncentivosPath = '<s:url action="buscarPercepcionesAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var imprimirConfigIncentivosPath= '<s:url action="imprimirPercepcionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var guardarConfiguracionIncentivosPath = '<s:url action="guardarConfiguracionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerIncentivosDiaInhabilPath = '<s:url action="obtenerIncentivosHorarioInhabil" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerIncentivosRecEfectivoPath = '<s:url action="obtenerIncentivosRecuperacionEfectivo" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerIncentivosRecCompaniaPath = '<s:url action="obtenerIncentivosRecuperacionCia" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerIncentivosRecSipacPath = '<s:url action="obtenerIncentivosRecuperacionSipac" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerIncentivosRechazosPath = '<s:url action="obtenerIncentivosRechazo" namespace="/siniestros/incentivoAjustador"/>';
	var obtenerTotalesIncentivosPath = '<s:url action="obtenerTotalesIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var registrarPercepcionPath = '<s:url action="registrarPercepcion" namespace="/siniestros/incentivoAjustador"/>';
	var cerrarRegistroPath = '<s:url action="mostrarContenedorIncentivosAjustador.action" namespace="/siniestros/incentivoAjustador"/>';
	var mostrarDetallePath = '<s:url action="generarConfiguracionAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var cancelarConfigIncentivosPatch = '<s:url action="cancelarConfiguracionIncentivos" namespace="/siniestros/incentivoAjustador"/>';
	var eliminarConfigIncentivosPath = '<s:url action="eliminarConfiguracion" namespace="/siniestros/incentivoAjustador"/>';
	var enviarAutorizacionPath = '<s:url action="enviaAutorizacion" namespace="/siniestros/incentivoAjustador"/>';
</script>


<s:form id="desgloseIncentivosForm" >
		<s:hidden id="configuracionId" name="configuracion.id"/>
		<s:hidden id="h_estatus" name="configuracion.estatus"/>
		<s:hidden id="h_esConsulta" name="esConsulta"/>
				
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.incentivos.generacion.titulo"/>
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>		
				<tr>
					
					<td style="width: 15%;">
						<s:textfield name="configuracion.folio" readonly="true" 
						key="midas.siniestros.cabina.reportecabina.historicomovimientos.folio" labelposition="left"></s:textfield>
					</td>
					<td style="width: 15%;">
						<s:textfield name="configuracion.oficina.nombreOficina" readonly="true" 
						key="midas.siniestros.incentivos.configuracion.oficina" labelposition="left"></s:textfield>
					</td>
					<td style="width: 10%;">
						<s:textfield name="configuracion.periodoInicial" readonly="true" 
						key="midas.siniestros.incentivos.configuracion.periodode" labelposition="left"></s:textfield>
					</td>
					<td style="width: 10%;">
						<s:textfield name="configuracion.periodoFinal" readonly="true" 
						key="midas.siniestros.incentivos.configuracion.periodohasta" labelposition="left"></s:textfield>
					</td>
					
					
				</tr>
			</tbody>
		</table>
		</div>
		
		
</s:form>



<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.configuracion.inhabiles"/>
</div>
<div id="indicadorHorarioInhabil"></div>	
<div id="percepcionesHorarioInhabilGridContainer" style="">
	<div id="listadoPercepcionesHorarioInhabilGrid" style="width:60%;height:200px;"></div>
	<div id="pagingAreaPercepcionesHorarioInhabil"></div><div id="infoAreaPercepcionesHorarioInhabil"></div>
</div>

<br/>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.configuracion.efectivo"/>
</div>
<div id="indicadorRecEfectivo"></div>	
<div id="percepcionesRecEfectivoGridContainer" style="">
	<div id="listadoPercepcionesRecEfectivoGrid" style="width:90%;height:200px;"></div>
	<div id="pagingAreaPercepcionesRecEfectivo"></div><div id="infoAreaPercepcionesRecEfectivo"></div>
</div>

<br/>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.configuracion.companias"/>
</div>
<div id="indicadorRecCompania"></div>	
<div id="percepcionesRecCompaniaGridContainer" style="">
	<div id="listadoPercepcionesRecCompaniaGrid" style="width:60%;height:200px;"></div>
	<div id="pagingAreaPercepcionesRecCompania"></div><div id="infoAreaPercepcionesRecCompania"></div>
</div>

<br/>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.configuracion.sipac"/>
</div>
<div id="indicadorRecSipac"></div>	
<div id="percepcionesRecSipacGridContainer" style="">
	<div id="listadoPercepcionesRecSipacGrid" style="width:60%;height:200px;"></div>
	<div id="pagingAreaPercepcionesRecSipac"></div><div id="infoAreaPercepcionesRecSipac"></div>
</div>

<br/>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.configuracion.rechazados"/>
</div>
<div id="indicadorRechazos"></div>	
<div id="percepcionesRechazosGridContainer" style="">
	<div id="listadoPercepcionesRechazosGrid" style="width:60%;height:200px;"></div>
	<div id="pagingAreaPercepcionesRechazos"></div><div id="infoAreaPercepcionesRechazos"></div>
</div>

<br/>
<div class="titulo" style="width: 95%;">
	<s:text name="midas.siniestros.incentivos.generacion.secciontotal"/>
</div>
<div id="indicadorTotales"></div>	
<div id="percepcionesTotalesGridContainer" style="">
	<div id="listadoPercepcionesTotalesGrid" style="width:95%;height:200px;"></div>
	<div id="pagingAreaPercepcionesTotales"></div><div id="infoAreaPercepcionesTotales"></div>
</div>

	<table id="agregar" border="0" width="95%">
			
		<tr>			
			<td>
					<s:textfield id="montoTotal" value="%{getText('struts.money.format',{montoTotal})}"
							labelposition="left" cssClass="txtfield jQrestrict" readOnly="true"
							key="midas.siniestros.incentivos.generacion.secciongrantotal"/>
				</td>
		</tr>	
	</table>
	
	
	<div id="divBotones" style="float:right; margin-right:5%;">
		<div id="btn_registrar" class="btn_back w160" style="display: none; float: right;" >
		<a href="javascript: void(0);" onclick="javascript:registrarPercepcion();">
		<s:text name="midas.siniestros.incentivos.generacion.registrar" />
				</a>
			</div>
			
		<div id="btn_solicitud" class="btn_back w160" style="display: none; float: right;" >
		<a href="javascript: void(0);" onclick="javascript:enviaAutorizacion();">
		<s:text name="midas.boton.solicitarAutorizacionVigencia" />
			</a>
		</div>
		
		<div id="btn_cancelar" class="btn_back w160" style="display: none; float: right;" >
		<a href="javascript: void(0);" onclick="javascript:cancelarPercepcion();">
		<s:text name="midas.boton.cancelar" />
			</a>
		</div>
		<div id="btn_regresar" class="btn_back w160" style="display: none; float: right;" >
		<a href="javascript: void(0);" onclick="javascript:regresarDetalle();"> 
		<s:text name="midas.siniestros.incentivos.generacion.btn.regresadetalle" /> 
				</a>
			</div>	
		<div id="btn_cerrar" class="btn_back w160" style="display: inline; float: right;" >
		<a href="javascript: void(0);" onclick="javascript:cerrarRegistro();">
		<s:text name="midas.boton.cerrar" />
			</a>
		</div>	
				
	</div>


<script>
	jQuery(document).ready(function(){
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA
		obtenerDesgloseIncentivos();
		mostrarOcultarBotones();
	});
</script>
