	

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/incentivos/incentivos.js'/>"></script>



	<div class="titulo"><s:text name="midas.siniestros.catalogo.solicitudautorizacion.catSolicitudAutorizacion.tituloResultadoBusqueda" /></div>
	<div id="indicador"></div>
	<div id="solicitudAutorizacionGrid" style="width:98%;height:340px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>

	<script type="text/javascript">
    	initGridsSolicitudAutorizacion();
	</script>
