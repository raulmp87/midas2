<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<script type="text/javascript">
	var buscarDetallesPercepcionPath = '<s:url action="buscarDetallesPercepcion" namespace="/siniestros/incentivoAjustador"/>';
	var generarPercepcionAjustadoresPath = '<s:url action="generarPercepcionAjustadores" namespace="/siniestros/incentivoAjustador"/>';
	var mostrarConfiguracionIncentivosPath = '<s:url action="mostrarContenedorIncentivosAjustador" namespace="/siniestros/incentivoAjustador"/>';
	var preRegistroIncentivosPath = '<s:url action="preRegistroPercepciones" namespace="/siniestros/incentivoAjustador"/>';
	var eliminarConfigIncentivosPath = '<s:url action="eliminarConfiguracion" namespace="/siniestros/incentivoAjustador"/>';
</script>

<s:hidden id="configuracionId" name="configuracion.id"/>
<s:hidden id="h_estatus" name="configuracion.estatus"/>
<s:hidden id="esConsulta" name="esConsulta"/>
<s:hidden id="incentivosConcat" name="incentivosConcat"/>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.incentivos.detallepercepcion.titulo"/>
</div>

<div id="indicadorDetallePercepcion"></div>	
<div id="detallePercepcionGridContainer" style="">
	<div id="listadoDetallePercepcionGrid" style="width:98%;height:200px;"></div>
	<div id="pagingAreaDetallePercepcion"></div><div id="infoAreaDetallePercepcion"></div>
</div>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<s:if test="!esConsulta" >
					<div id="btn_generar" class="btn_back w160" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:generarPercepcion();">
							<s:text name="midas.siniestros.incentivos.detallepercepcion.generar" />
						</a>
					</div>
				</s:if>
				<div id="btn_limpiar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:cerrarDetallePercepcion();"> 
						<s:text name="midas.boton.cerrar" /> 
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<script src="<s:url value='/js/midas2/siniestros/incentivos/incentivos.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script>
	jQuery(document).ready(function(){
		buscarDetallePercepcion();
	});
</script>