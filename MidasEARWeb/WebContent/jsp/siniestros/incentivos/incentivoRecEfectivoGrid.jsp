<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingAreaPercepcionesRecEfectivo</param>
				<param>true</param>
				<param>infoAreaPercepcionesRecEfectivo</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="numeroAjustador"         type="ro" width="125"  sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.numajustador'}"/></column>	
        <column id="nombreAjustador"         type="ro" width="250" sort="str" ><s:text name="%{'midas.siniestros.incentivos.detallepercepcion.ajustador'}"/></column>
        <column id="rangoRecuperado"         type="ro" width="150" sort="int" ><s:text name="%{'midas.siniestros.incentivos.generacion.rangorecuperado'}"/> </column>
		<column id="montoCalculadoRango"     type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.incentivos.generacion.montocalculado'}"/> </column>
		<column id="excedente"               type="ro" width="150" sort="int" ><s:text name="%{'midas.siniestros.incentivos.generacion.excedentevento'}"/> </column>
		<column id="montoCalculadoExcedente" type="ro" width="150" sort="int"><s:text name="%{'midas.siniestros.incentivos.generacion.montocalculado'}"/> </column>
		<column id="importe"   		         type="ro" width="95" sort="int"><s:text name="%{'midas.siniestros.incentivos.generacion.importe'}"/> </column>
			
		</head>
		<s:iterator value="incentivosRecEfectivo">
			<row>
				<cell><s:property value="numeroAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreAjustador" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoRecuperacionEfectivo})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoCalculadoEfectivo})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoExcedenteEfectivo})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{montoCalculadoExcedente})}" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="%{getText('struts.money.format',{importe})}" escapeHtml="false" escapeXml="true" /></cell>
				
			</row>
		</s:iterator>
	
</rows>
