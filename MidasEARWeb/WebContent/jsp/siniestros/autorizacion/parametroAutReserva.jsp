<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/autorizacion/parametroAutReserva.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

.ocultarTxt{
	display: none;
}

.mostrarTxt{
	display: block;
}
.labelBlack{
 	color:black;
    width: 98%;
	line-height: 20px;
	margin-top: 6px;
	margin-bottom: 6px;
	font-size: 12px;
	font-weight: bold;
	text-align: center;	
}

.montoDe{
	color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

.error {
	background-color: red;
	opacity: 0.4;
}

label[for="s_criterioReserva"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="ch_rangoReserva"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="txt_montoReservaFinal"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="s_criterioRangoDeducible"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="ch_rangoDeducible"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="txt_montoDeducibleFinal"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="s_criterioRangoPorcentajeDeducible"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="ch_rangoPorcentajeDeducible"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

label[for="txt_montoPorcentajeDeducibleFinal"]{
    color:black;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 50px !important;
}

</style>
<script type="text/javascript">
	var obtenerCoberturaPorSeccionPath   = '<s:url action="obtenerCoberturaPorSeccion" namespace="/siniestros/catalogos/configuradorParametros"/>';
	var listarParametrosPath   = '<s:url action="listarParametros" namespace="/siniestros/catalogos/configuradorParametros"/>';
	var buscarParametroPath           = '<s:url action="buscar" namespace="/siniestros/catalogos/configuradorParametros"/>';
	var eliminarParametroPath           = '<s:url action="eliminar" namespace="/siniestros/catalogos/configuradorParametros"/>';
	var guardarParametroPath           = '<s:url action="guardar" namespace="/siniestros/catalogos/configuradorParametros"/>';
</script>

<s:form id="parametroForm">
	<s:hidden name="parametro.id" id="h_idParametro"></s:hidden>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.configuracionParametrosAutorizacion.title"/>	
	</div>	
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td style="width: 20%; text-align: left;">
					<s:select id="s_oficina" 
							  name="parametro.oficina.id"
							  key="midas.configuracionParametrosAutorizacion.oficina"  labelposition="left" 
							  cssClass="cajaTextoM2 w250 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="oficinaList" listKey="key" listValue="value"  /> 	
				</td>
				<td style="width: 20%; text-align: left;">
					<s:textfield id="t_nombreConfiguracion"
								 name="parametro.nombreConfiguracion"
								 key="midas.configuracionParametrosAutorizacion.nombreConfiguracion"  labelposition="left" 
								 cssClass="txtfield requerido" 
								 size="60"			
								 maxlength="100"		
								 onkeypress="return soloAlfanumericos(this, event, false)" />
				</td>
				<td style="width: 60%; text-align: left;">
					<s:textfield id="dp_fechaConfiguracion"
								 name="parametro.fechaCreacion"
								 key="midas.configuracionParametrosAutorizacion.fechaConfiguracion"  labelposition="left" 
								 cssClass="txtfield" 
								 size="12"
								 readonly="true"
								 onkeypress=" return soloFecha(this, event, false);"
								 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
				</td>
			</tr>
			<tr>
				<td style="width: 50%; text-align: left;">
					<s:select id="s_lineaNegocio" 
							  name="parametro.seccion.idToSeccion"
							  key="midas.configuracionParametrosAutorizacion.lineaNegocio"  labelposition="left" 
							  cssClass="cajaTextoM2 w200 requerido"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="seccionList" listKey="idToSeccion" listValue="nombreComercial"
						  	  onchange="onChangeLineaNegocio();"  /> 	
				</td>
				<td>
					<s:select id="s_cobertura" 
							  name="parametro.claveSubCalculo"
							  key="midas.configuracionParametrosAutorizacion.cobertura"  labelposition="left" 
							  cssClass="cajaTextoM2 w400 requerido"
							  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="coberturaList" listKey="claveSubCalculo" listValue="nombreCobertura" 
						  	  onchange="onChangeCobertura();" /> 	
						  	  <s:hidden name="parametro.claveSubCalculo" id="h_claveSubCalculo"></s:hidden>
						  	  <s:hidden name="parametro.cobertura.idToCobertura" id="h_cobertura"></s:hidden>
						  	  <s:hidden name="parametro.cobertura.claveTipoDeducible" id="h_claveTipoDeducible"></s:hidden>
				</td>
				<td>
					<s:select id="s_estatus" 
							  name="parametro.estatus"
							  key="midas.configuracionParametrosAutorizacion.estatus"  labelposition="left" 
							  cssClass="cajaTextoM2 w80 requerido"
						  	  list="estatusList" listKey="key" listValue="value"
						  	  onchange="onChangeEstatus();"   /> 	
						  	  <s:hidden name="estatus" id="h_estatus"></s:hidden>
				</td>		
			</tr>
			<tr>
			    <td colspan="2" style="width: 75%; text-align: left;">
					<table>
						<tbody>
							<tr>
								<td colspan="6">
									<br><br>
									<div class="labelBlack" ><s:text name="midas.configuracionParametrosAutorizacion.rangosMontosReserva"/></div>
								</td>
							</tr>
							<tr>
								<td>
									<s:checkbox id ="ch_condicionReserva"  
												name="parametro.condicionReserva" 
												value="%{parametro.condicionReserva}" 
												onclick="onChangeCondicionReserva();"/>	
								</td>
								<td>
									<s:select id="s_criterioReserva" 
											  name="parametro.criterioRangoReserva"
											  key="midas.configuracionParametrosAutorizacion.condicion" labelposition="left" 
											  cssClass="cajaTextoM2 w200"
											  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
										  	  list="criterioList" listKey="key" listValue="value"
										  	  disabled="true"
										  	  onchange="onChangeCriterioReserva();" /> 	
								</td>
								<td>
									<s:checkbox id ="ch_rangoReserva"  
												key="midas.configuracionParametrosAutorizacion.rango"
												onclick="onChangeRangoReserva();"
												cssClass="setNew" 
												disabled="false"/>	
									<s:hidden id="h_rangoReserva" name="rangoReserva"></s:hidden>
								</td>
								<td>
									<div class="montoDe" id="d_montoReserva" ><s:text name="midas.configuracionParametrosAutorizacion.monto"/></div>
									<div class="montoDe" id="d_montoDeReserva" ><s:text name="midas.configuracionParametrosAutorizacion.montoDe" /></div>
								</td>
								<td>
									<s:textfield id="txt_montoReservaInicial"
												 name="parametro.montoReservaInicial"
												 cssClass="txtfield formatCurrency" 
												 value="%{parametro.montoReservaInicial}"
												 size="12"		
												 onkeypress="return soloNumeros(this, event, true)"
												 onkeyup    = "mascaraDecimales('#txt_montoReservaInicial',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoReservaInicial',this.value);"
												 disabled="true"/>
								</td>
								<td id="td_montoReservaFinal">
									<s:textfield id="txt_montoReservaFinal"
												 name="parametro.montoReservaFinal"
												 key="midas.general.a"  labelposition="left" 
												 cssClass="txtfield formatCurrency" 
												 value="%{parametro.montoReservaFinal}"
												 size="12"		
												 onkeypress="return soloNumeros(this, event, true)"
												 onchange="validaMonto( jQuery('#txt_montoReservaInicial').val(), jQuery('#txt_montoReservaFinal').val() );"  
												 onkeyup    = "mascaraDecimales('#txt_montoReservaFinal',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoReservaFinal',this.value);"
												 />
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<br><br>
									<div class="labelBlack" ><s:text name="midas.configuracionParametrosAutorizacion.rangosMontosDeducible"/></div>
								</td>
							</tr>
							<tr>
								<td>
									<s:checkbox id ="ch_condicionDeducible"  
												name="parametro.condicionDeducible" 
												cssClass="setNew"
												value="%{parametro.condicionDeducible}" 
												onclick="onChangeCondicionDeducible();"/>	
								</td>
								<td>
									<s:select id="s_criterioRangoDeducible" 
											  name="parametro.criterioRangoDeducible"
											  key="midas.configuracionParametrosAutorizacion.condicion" labelposition="left" 
											  cssClass="cajaTextoM2 w200 setNew"
											  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
										  	  list="criterioList" listKey="key" listValue="value"
										  	  disabled="true"
										  	  onchange="onChangeCriterioDeducible();"/> 	
								</td>
								<td>
									<s:checkbox id ="ch_rangoDeducible"  
												key="midas.configuracionParametrosAutorizacion.rango"
												cssClass="setNew"
												onclick="onChangeRangoDeducible();"/>	
									<s:hidden id="h_rangoDeducible" name="rangoDeducible"></s:hidden>
								</td>
								<td>
									<div class="montoDe" id="d_montoDeducible" ><s:text name="midas.configuracionParametrosAutorizacion.monto"/></div>
									<div class="montoDe" id="d_montoDeDeducible" ><s:text name="midas.configuracionParametrosAutorizacion.montoDe" /></div>
								</td>
								<td>
									<s:textfield id="txt_montoDeducibleInicial"
												 name="parametro.montoDeducibleInicial"
												 cssClass="txtfield setNew formatCurrency" 
												 value="%{parametro.montoDeducibleInicial}"
												 size="12"	
												 onkeyup    = "mascaraDecimales('#txt_montoDeducibleInicial',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoDeducibleInicial',this.value);"	
												 onkeypress="return soloNumeros(this, event, true)" disabled="true"/>
								</td>
								<td id="td_montoDeducibleFinal">
									<s:textfield id="txt_montoDeducibleFinal"
												 name="parametro.montoDeducibleFinal"
												 key="midas.general.a"  labelposition="left" 
												 cssClass="txtfield setNew formatCurrency" 
												 value="%{parametro.montoDeducibleFinal}"
												 size="12"		
												 onkeypress="return soloNumeros(this, event, true)"
												 onkeyup    = "mascaraDecimales('#txt_montoDeducibleFinal',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoDeducibleFinal',this.value);"
												 onchange="validaMonto( jQuery('#txt_montoDeducibleInicial').val(), jQuery('#txt_montoDeducibleFinal').val() );" />
								</td>
							</tr>
							<tr>
								<td colspan="6">
									<br><br>
									<div class="labelBlack" ><s:text name="midas.configuracionParametrosAutorizacion.rangosPorcentajeDeducible"/></div>
								</td>
							</tr>
							<tr>
								<td>
									<s:checkbox id ="ch_condicionPorcentajeDeducible"  
												name="parametro.condicionPorcentajeDeducible" 
												cssClass="setNew"
												value="%{parametro.condicionPorcentajeDeducible}" 
												onclick="onChangeCondicionPorcentajeDeducible();"/>	
								</td>
								<td>
									<s:select id="s_criterioRangoPorcentajeDeducible" 
											  name="parametro.criterioRangoPorcentaje"
											  key="midas.configuracionParametrosAutorizacion.condicion" labelposition="left" 
											  cssClass="cajaTextoM2 w200 setNew"
											  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
										  	  list="criterioList" listKey="key" listValue="value"
										  	  disabled="true"
										  	  onchange="onChangeCriterioPorcentajeDeducible();"/> 	
								</td>
								<td>
									<s:checkbox id ="ch_rangoPorcentajeDeducible"  
												key="midas.configuracionParametrosAutorizacion.rango" 
												cssClass="setNew"
												onclick="onChangeRangoPorcentajeDeducible();"/>	
									<s:hidden id="h_rangoPorcentajeDeducible" name="rangoPorcentajeDeducible"></s:hidden>
								</td>
								<td>
									<div class="montoDe" id="d_montoPorcentaje" ><s:text name="midas.configuracionParametrosAutorizacion.porcentaje"/></div>
									<div class="montoDe" id="d_montoDePorcentaje" ><s:text name="midas.configuracionParametrosAutorizacion.porcentajeDe" /></div>
								</td>
								<td>
									<s:textfield id="txt_montoPorcentajeDeducibleInicial"
												 name="parametro.montoPorcentajeInicial"
												 cssClass="txtfield setNew" 
												 value="%{parametro.montoPorcentajeInicial}"
												 size="12"	
												 onkeyup    = "mascaraDecimales('#txt_montoPorcentajeDeducibleInicial',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoPorcentajeDeducibleInicial',this.value);"												 	
												 onkeypress="return soloNumeros(this, event, true)" disabled="true"/>
								</td>
								<td id="td_montoPorcentajeFinal">
									<s:textfield id="txt_montoPorcentajeDeducibleFinal"
												 name="parametro.montoPorcentajeFinal"
												 key="midas.general.a"  labelposition="left" 
												 cssClass="txtfield setNew" 
												 value="%{parametro.montoPorcentajeFinal}"
												 size="12"	
												 onkeyup    = "mascaraDecimales('#txt_montoPorcentajeDeducibleFinal',this.value);"
												 onblur     = "mascaraDecimales('#txt_montoPorcentajeDeducibleFinal',this.value);"													 
												 onkeypress="return soloNumeros(this, event, true)"
												 onchange="validaMonto( jQuery('#txt_montoPorcentajeDeducibleInicial').val(), jQuery('#txt_montoPorcentajeDeducibleFinal').val() );" />
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="width: 30%; text-align: left;">&ensp;</td>
			</tr>
		</tbody>
	</table>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tr>
			<td>	
				<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_guardar">
					<a href="javascript: void(0);" onclick="guardar();"> 
					<s:text name="midas.boton.guardar" /> </a>
				</div>	
			</td>
		</tr>
	</table>
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.configuracionParametrosAutorizacion.listadoConfiguracionesParametros"/>	
	</div>
	<br/>	
	<div id="listadoParametrosGrid" class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br>
</s:form>
<script>
jQuery(document).ready(function(){
	inicializarPantalla();
});

</script>