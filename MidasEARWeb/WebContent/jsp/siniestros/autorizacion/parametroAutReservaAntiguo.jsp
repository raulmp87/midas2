<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.error {
	background-color: red;
	opacity: 0.4;
}

.mensajeError {
	color: red;
	text-align: center;
}

</style>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/autorizacion/parametroAutReservaAnt.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>


<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
var idReporteCabina = '<s:property value="idReporteCabina" />';
</script>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.configuracionDeAutorizacion"/></div>
<div id="contenedorFiltros">
	<s:form id="infoGeneralForm" >
		<table id="filtrosM2" border="0" style="width: 98%;">
			<tbody>
				<tr>
					<td>
						<s:select id="s_oficina" 
									labelposition="left"
									label = "Oficina" 
									name="parametro.oficina.id"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listOficina" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160 requerido"/>
					</td>	
					<td>
						<s:select id="s_coberturas" 
									labelposition="left"
									label = "Cobertura" 
									name="parametro.idCoberturaClaveSubCalculo"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listCoberturas" listKey="key" listValue="value"   
							  		cssClass="txtfield cajaTextoM2 w300 requerido"/>
					</td>
					<td>
						<s:select id="s_tipoAjuste" 
									labelposition="left"
									label = "Tipo de Ajuste" 
									name="parametro.tipoAjuste"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listTipoAjuste" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w130 requerido"/>
					</td>
					<td>
						<s:select id="s_tipoSiniestro" 
									labelposition="left"
									label = "Tipo de Siniestro" 
									name="parametro.tipoSiniestro"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listTipoSiniestro" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w210 requerido"/>
					</td>
				</tr>
				<tr>
					<td>
						<s:select id="s_montosAjuste" 
									labelposition="left"
									label = "Montos de Ajuste" 
									name="parametro.condicionMonto"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listCriterio" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160 requerido"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield cajaTextoM2 w130 requerido formatCurrency"
								name="parametro.monto"
								labelposition="left"
								label = "Monto" 
								id="t_monto" 
								onkeypress="return soloNumeros(this, event, true)" />
					</td>
					<td>
						<s:select id="s_mesesAntiguedad" 
									labelposition="left"
									label = "Montos de Antiguedad" 
									name="parametro.condicionMeses"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listCriterio" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160 requerido"/>
					</td>
					<td>
						<s:textfield cssClass="txtfield cajaTextoM2 w60 requerido"
								name="parametro.meses"
								labelposition="left"
								label = "Meses" 
								id="t_meses" 				
								maxlength="4"
								onkeypress="return soloNumeros(this, event, true)" />
					</td>
					
				</tr>
				<tr>
					<td>
						<b><s:label><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.nivelesAutorizacion"/></s:label></b>
					</td>
				</tr>
				<tr id="messageError" >
					<td colspan=8  class = "mensajeError">
						<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.mensajeErrorRoles"/>
					</td>
				</tr>
				<tr id="messageErrorRolesConsecutivos" >
					<td colspan=8  class = "mensajeError">
						<s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.mensajeErrorRolesConsecutivos"/>
					</td>
				</tr>
				<tr>
					<td><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.primeraAutorizacion" />
						<s:select id="s_autorizacion1" 
									labelposition="left"
									name="parametro.rolPrimeraAutorizacion"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listRoles" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w260 requerido"/>
					</td>
					<td><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.segundaAutorizacion" />
						<s:select id="s_autorizacion2" 
									labelposition="left"
									name="parametro.rolSegundaAutorizacion"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listRoles" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w260 "/>
					</td>
					<td><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.terceraAutorizacion" />
						<s:select id="s_autorizacion3" 
									labelposition="left"
									name="parametro.rolTerceraAutorizacion"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listRoles" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w260 "/>
					</td>
					<td><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.cuartaAutorizacion" />
						<s:select id="s_autorizacion4" 
									labelposition="left"
									name="parametro.rolCuartaAutorizacion"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
 									list="listRoles" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w260 "/>
					</td>
				</tr>
				<tr>
					<td colspan="8">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_guardar">
										 <a href="javascript: void(0);" onclick="guardar();" >
										 <s:text name="midas.boton.guardar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
									
								</td>							
							</tr>
						</table>				
					</td>	
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	
<br/>
<br/>
<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.listadoConfiguraciones"/></div>
<div id="indicador"></div>
<div id="parametrosGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>


<script type="text/javascript">
jQuery(document).ready(
		function(){
			mostrarConfiguracionesEnGrid();
			jQuery("#messageError").hide();
			jQuery("#messageErrorRolesConsecutivos").hide();
			initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
