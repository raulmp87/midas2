<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="oficina" type="ro" width="110" sort="str"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.oficina"/></column>
		<column id="tipoSiniestro" type="ro" width="110" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.tipoSiniestro"/></column>
		<column id="nombreCobertura" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.cobertura"/></column>
		<column id="descripcionConfiguracion" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.descripcionConfiguracion"/></column>
		<column id="autorizadoPor" type="ro" width="150" sort="date_custom" align="center" ><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.autorizadoPor"/></column>
		<column id="estatus" type="ro" width="60" sort="str" align="center"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.estatus"/></column>	
		<column id="inactivar" type="img" width="70" sort="na" align="center"><s:text name="midas.siniestros.cabina.reportecabina.configurador.autorizacionHistorico.inactivar"/></column>
	</head>			
	<s:iterator value="listParametros" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="nombreCoberturaDesc" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="configuracionDesc" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="autorizadores" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus == true" >
				<cell>/MidasWeb/img/ico_green.gif^Activo^javascript: inactivar(<s:property value="id" escapeHtml="false" escapeXml="true"/>);^_self</cell>
			</s:if>
			<s:else>
				<cell>/MidasWeb/img/ico_red.gif^Inactivo</cell>
			</s:else>
		</row>
	</s:iterator>	
</rows>