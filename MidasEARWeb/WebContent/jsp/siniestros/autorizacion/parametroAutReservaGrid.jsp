<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column id="id" type="ro" width="100" sort="int" escapeXml="true" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>
		<column id="nombreOficina" type="ro" width="150" sort="str"><s:text name="midas.configuracionParametrosAutorizacion.oficina"/></column>
		<column id="lineaNegocio" type="ro" width="250" sort="str" align="center"><s:text name="midas.configuracionParametrosAutorizacion.lineaNegocio"/></column>
		<column id="nombreCobertura" type="ro" width="*" sort="str" align="center"><s:text name="midas.configuracionParametrosAutorizacion.cobertura"/></column>
		<column id="nombreConfiguracion" type="ro" width="150" sort="str" align="center"><s:text name="midas.configuracionParametrosAutorizacion.nombreConfiguracion"/></column>
		<column id="fechaConfiguracion" type="ro" width="150" sort="date_custom" align="center" ><s:text name="midas.configuracionParametrosAutorizacion.fechaConfiguracion"/></column>
		<column id="descripcionEstatus" type="ro" width="100" sort="str" align="center"><s:text name="midas.configuracionParametrosAutorizacion.estatus"/></column>	
		<column id="editar" type="img" width="40" sort="na" align="center">Acciones</column>	
		<column id="eliminar" type="img" width="40" sort="na" align="center">#cspan</column>	
	
	</head>			
	<s:iterator value="parametros" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>				
			<cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccion.nombreComercial" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="nombreConfiguracion" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaCreacion" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_editar.gif^Editar^javascript: mostrarEditarParametro(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>			
			<cell>../img/delete14.gif^Borrar^javascript: eliminarParametro(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>																
		</row>
	</s:iterator>	
</rows>