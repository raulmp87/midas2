<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>


<script type="text/javascript">
var mostrarHistoricoRobosPath = '<s:url action="mostrarHistoricoRobos" namespace="/siniestros/cabina/reportecabina/historicoRobos"/>';
var buscarHistoricoRobosPath = '<s:url action="buscarHistoricoRobos" namespace="/siniestros/cabina/reportecabina/historicoRobos"/>';
var regresarDefinirRoboPath = '<s:url action="seguimientoRoboTotal" namespace="/siniestros/cabina/reporteCabina/moduloRobos"/>';
</script>



<s:form id="busquedaHistoricoRobos" >
	<s:hidden id="reporteCabinaId" name="reporteCabinaId" />	
	<s:hidden id="idCobertura" name="idCobertura" />

	<table  id="filtrosM2" width="98%" >
            <tr>
                  <td class="titulo" colspan="3"><s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.busqueda" /></td>
            </tr>
            <tr>
            	 <td  align="left"> <sj:datepicker name="filtroMovimientos.fechaIni"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2" 
					label="Fecha desde"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				
				</td>
			
				<td > <sj:datepicker name="filtroMovimientos.fechaFin"
					id="txtFechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					label="Fecha hasta"
					labelposition="left"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
				</td>
          		<td>
					<s:select list="tipoMovimientos" label="%{getText('midas.siniestros.cabina.reportecabina.historicomovimientos.movimientos')}" labelposition="left" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroMovimientos.tipoMovimiento" id="movimientos_s" cssClass="cajaTextoM2 w250" onchange=""/>
           		</td>
            </tr>
            
            <tr>		
					<td colspan="3">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="buscarHistoricoRobos();" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
      
	<div id="tituloListado" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.listado" /></div>	
	
	<div id="historicoRobosGridContainer">
		<div id="historicoRobosGrid" style="width:98%;height:200px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
	
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tr>
			<td>	
				<div class="btn_back w120" style="display: inline; float: right;"  >
					<a href="javascript: void(0);" onclick="regresarDefinirRobo();">
						<s:text name="midas.boton.cerrar" /> 
					</a>
				</div>
			</td>
		</tr>
	</table>

</s:form>

<script src="<s:url value='/js/midas2/siniestros/robos/historicoRobo.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	buscarHistoricoRobos();
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
