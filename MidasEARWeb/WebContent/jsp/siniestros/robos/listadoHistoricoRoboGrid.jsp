<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="300"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.movimiento" />  </column>
		<column  type="ro"  width="500"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.ordenPago.estimacionDirecta.causaMovimiento" />  </column>
		<column  type="ro"  width="150"  align="center" sort="date_custom"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.fecha" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="midas.siniestros.cabina.reportecabina.historicomovimientos.reserva" />  </column>
	</head>

	<s:iterator value="movimientos">
		<row id="<s:property value="#row.index"/>">
		
			<cell><s:property value="tipoMovimientoDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="causaMovimientoDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaMovimiento" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{reserva})}" escapeHtml="false" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>
	
</rows>