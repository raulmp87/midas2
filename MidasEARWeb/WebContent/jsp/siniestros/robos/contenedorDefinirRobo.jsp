<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/robos/moduloRobos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>


<script type="text/javascript">
	var buscarCondicionesEspecialesIncisoPath = '<s:url action="mostrarCondicionesEspecialesInciso" namespace="/siniestros/cabina/reporteCabina/poliza"/>';
	var mostrarHistoricoRobosPath = '<s:url action="mostrarHistoricoRobos" namespace="/siniestros/cabina/reportecabina/historicoRobos"/>';
</script>

<script
	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/reportecabina.js'/>"></script>
<script
	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.js'/>"></script>
<style type="text/css">
div.ui-datepicker {z
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	var mostrarDatosIncisoPoliza = '<s:url action="mostrarDatosIncisoPoliza" namespace="/siniestros/cabina/reportecabina"/>';
	
</script>
<s:form id="definirRoboForm" class="floatLeft">
<div id="contenido_DefinirRobo" style="width:99%;position: relative;">
		<s:hidden id="reporteCabinaId" name="reporteCabinaId" />	
		<s:hidden id="idCobertura" name="idCobertura" />
		<s:hidden id="idproveedor" name="idproveedor" value="%{definicionSiniestroRoboDTO.proveedor}"/>
      <s:hidden name="definicionSiniestroRoboDTO.reporteCabinaid" id="h_idReporte"></s:hidden>
	  <s:hidden name="definicionSiniestroRoboDTO.idToPoliza" id="h_idPoliza"></s:hidden>
      <s:hidden name="definicionSiniestroRoboDTO.fechaOcurridoMillis" id="h_fechaReporteSiniestroMillis"></s:hidden>
      <s:hidden name="definicionSiniestroRoboDTO.incisoContinuityId" id="h_incisoContinuityId"></s:hidden>	
      <s:hidden name="definicionSiniestroRoboDTO.reporteRoboId" id="reporteRoboIdDto"></s:hidden>	
      <s:hidden name="definicionSiniestroRoboDTO.envioOcra" id="envioOcra"></s:hidden>	
      <s:hidden name="definicionSiniestroRoboDTO.numSerie" id="h_numeroSerie"></s:hidden>
      <s:hidden name="definicionSiniestroRoboDTO.estimacionId" id="h_estimacionId"></s:hidden>
      <s:hidden id="tipoValidacion" value="0"/>
      <s:hidden id="h_soloConsulta" value="0"/>
      <s:hidden id="h_recordFromMillis" name="detalleInciso.recordFromMillis"/>
      <s:hidden id="h_validOnMillis" name="detalleInciso.validOnMillis"/>
		
	  <div id="divInferior" style="width: 99%;  class="floatLeft">
			<div class="titulo" align="left">
			<s:text name="midas.servicio.siniestros.roboDefinicion.titulo" />
			</div>
		 
			<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 100%;">
				<div class="divInterno">
					<div class="floatLeft divInfDivInterno" style="width: 30%;" > 
							<s:textfield id="noSiniestro" name="definicionSiniestroRoboDTO.noSiniestro"  cssClass="readOnly txtfield"  label="No Siniestro"  cssStyle="" readonly="false" ></s:textfield>
					</div>				
					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="noPoliza"  name="definicionSiniestroRoboDTO.noPoliza" cssClass="readOnly txtfield" label="No Poliza" cssStyle=""  readonly="false" ></s:textfield>
						
					</div>					
					<div class="floatLeft divInfDivInterno" style="width: 30%;" >
						<s:textfield id="noReporte" name="definicionSiniestroRoboDTO.noReporte"  cssClass="readOnly txtfield" label="No Reporte" cssStyle="" readonly="false" >
							<form></form>
						</s:textfield> 
					</div>
					
					
				</div>
			</div>
		</div>
		


		<!-- TITULO  -->

	 <div id="divInferior" style="width: 99%; class="floatLeft">
	 
	 		<div class="floatLeft"
				style="height: 15px; width: 50%; ">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.robo.vehiculo" />		
				</div>
			</div>
				
			<div class="floatLeft"
				style="height: 15px; width: 50%; ">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.robo.fechas" />
				</div>
			</div>
				
		</div>
				
	
	<!-- DATOS VEHICULO  -->
	<div id="divInferior" style="width: 100%; height: 350px;" class="floatLeft">
			<div class="floatLeft"
				style="height: 300px; width: 520px; ">
				
				<div class="divContenedorO"
					style="height: 163px; width: 100%; margin-bottom: 7px;">
					
					
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 40%;" >
						<s:textfield id="numSerie" name="definicionSiniestroRoboDTO.numSerie"   cssClass="readOnly txtfield" label="N° de Serie" labelposition="left" cssStyle="width:130px;" readonly="false" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
						<s:textfield id="numSerieOcra" name="definicionSiniestroRoboDTO.numSerieOcra"  cssClass="txtfield" label="N° de Serie Ocra" labelposition="left" cssStyle="width:130px;" readonly="false" ></s:textfield>
						</div>
						
					</div>
					
					<div class="divFormulario">
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;"  >
						<s:textfield id="numMotor" name="definicionSiniestroRoboDTO.numMotor"  cssClass="readOnly txtfield" label="N° de Motor" labelposition="left" cssStyle="width:130px;" readonly="false" ></s:textfield>
						</div>
						
						
					</div>
					
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 38%; padding-left: 30px" >
						
						<s:textfield id="marca" name="definicionSiniestroRoboDTO.marca"   cssClass="readOnly txtfield"  label="Marca" labelposition="left" cssStyle="width:130px;" readonly="false" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 45%;" >
						<s:textfield id="tipoVehiculo" name="definicionSiniestroRoboDTO.tipoVehiculo"   cssClass="readOnly txtfield"  label="Tipo Vehiculo" labelposition="left" cssStyle="width:130px;" readonly="false" ></s:textfield>
						</div>
						
					</div>
					
					<div class="divFormulario" >
					
					<div class="floatLeft divInfDivInterno" style="width: 30%; padding-left: 30px" >
							<s:textfield name="definicionSiniestroRoboDTO.modelo"
									id="modelo" 
									cssClass="txtfield setNew"
									readonly="true"
									cssStyle="width:55px;"
									label="Modelo" labelposition="left"
									>
								</s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;" >
							<s:textfield name="definicionSiniestroRoboDTO.placas"
									id="placas" 
									cssClass="txtfield setNew"
									readonly="true"
									cssStyle="width:55px;"
									label="Placas" labelposition="left"
									>
								</s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;" >
							<s:textfield name="definicionSiniestroRoboDTO.color"
									id="color" 
									cssClass="txtfield setNew"
									readonly="true"
									cssStyle="width:100px;"
									label="Color" labelposition="left"
									>
								</s:textfield>
						</div>
					</div>
				</div>
				<div class="divContenedorO"
					style="height: 163px; width: 520px; ">
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 80%;" >
						<s:textfield id="nombreAsegurado" name="definicionSiniestroRoboDTO.nombreAsegurado"   cssClass="readOnly txtfield w200"  label="Nombre Asegurado" labelposition="left"  readonly="true" ></s:textfield>
						</div>
					</div>	
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 20%;">
						<s:textfield id="ladaAsegurado" name="definicionSiniestroRoboDTO.ladaAsegurado"   cssClass="readOnly txtfield "  label="Telefono" labelposition="left" cssStyle="width:30px;" readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno"  style="width: 40%;" > 
						<s:textfield id="telASegurado" name="definicionSiniestroRoboDTO.telASegurado"   cssClass="readOnly txtfield w200"     readonly="true" ></s:textfield>
						</div>
					</div>	
					
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 80%;" >
						<s:textfield id="nombreContacto" name="definicionSiniestroRoboDTO.nombreContacto"   cssClass="readOnly txtfield w200"  label="Nombre Contacto" labelposition="left" readonly="true" ></s:textfield>
						</div>
					</div>	
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 20%;" >
						<s:textfield id="ladaContacto" name="definicionSiniestroRoboDTO.ladaContacto"   cssClass="readOnly txtfield"  label="Telefono" labelposition="left" cssStyle="width:30px;" readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 40%;" > 
						<s:textfield id="telContacto" name="definicionSiniestroRoboDTO.telContacto"   cssClass="readOnly txtfield w200" readonly="true" ></s:textfield>
						</div>
					</div>	
					
					<div class="divFormulario" >					
						<div class="floatLeft divInfDivInterno" style="width: 45%;" >
							<div id="siniestros" class="btn_back w90" 
									style="display: inline; margin-left: 1%; float: right;">
									<a href="javascript: void(0);" onclick="listadoSiniestros();"> <s:text
													name="Siniestros" /> </a>
							</div>
							
							
							
							
							
						</div>
					</div>
				</div>
			</div>	
			<div class="floatLeft"
				style="height: 300px; width: 520; padding-left: 10px ">
				
				<div class="divContenedorO"
					style="height: 243px; width: 100%; margin-bottom: 7px;">
					
					<div class="divFormulario" >
						
						<div class="floatRight divInfDivInterno" style="width: 50%;" >
								<s:textfield id="vigenciaPolizaFin" name="definicionSiniestroRoboDTO.vigenciaPolizaFin"  label="Al" labelposition="left"  cssClass="txtfield" size="12"   cssStyle=""  readonly="true" ></s:textfield>
						</div>
					
						<div class="floatRight divInfDivInterno" style="width: 50%;" >
								<s:textfield id="vigenciaPolizaIni" name="definicionSiniestroRoboDTO.vigenciaPolizaIni"  label="Vigencia de Poliza Del" labelposition="left"  cssClass="txtfield" size="12"   cssStyle=""  readonly="true" ></s:textfield>
						</div>
					</div>
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<sj:datepicker name="definicionSiniestroRoboDTO.fechaRobo" id="datepickerRobo"
									label="Fecha de Robo" labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaRobo"
									value="%{definicionSiniestroRoboDTO.fechaRobo}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this);">
								</sj:datepicker>					
						</div>
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:textfield id="vigenciaPolizaFin" name="definicionSiniestroRoboDTO.fechaReporte"  label="Fecha de Reporte" labelposition="left"  cssClass="txtfield" size="12"   cssStyle=""  readonly="true" ></s:textfield>
						</div>
						
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<sj:datepicker name="definicionSiniestroRoboDTO.fechaCapOCRA"
									label="Fecha Captura en Ocra" labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaCapOCRA"
									value="%{definicionSiniestroRoboDTO.fechaCapOCRA}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this);">
								</sj:datepicker>			
						</div>
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<sj:datepicker name="definicionSiniestroRoboDTO.fechaAveriguacion"
									label="Fecha Averig. Den." labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaAveriguacion"
									value="%{definicionSiniestroRoboDTO.fechaAveriguacion}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this);">
								</sj:datepicker>			
						</div>
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<sj:datepicker name="definicionSiniestroRoboDTO.fechaLocalizacion" id="datepickerLocalizacion"
									label="Fecha Localizacion" labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaLocalizacion"
									value="%{definicionSiniestroRoboDTO.fechaLocalizacion}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onchange="getNumeroDeDiasLozalizacion();"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this);getNumeroDeDiasLozalizacion();"
									onblur="esFechaValida(this); ">
								</sj:datepicker>		 	
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 20%;">
						<s:textfield id="diasLocalizacion" name="definicionSiniestroRoboDTO.diasLocalizacion"   cssClass="readOnly txtfield"  label="Dias" labelposition="left" cssStyle="width:30px;" readonly="true" ></s:textfield>
						</div>
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<sj:datepicker name="definicionSiniestroRoboDTO.fechaRecuperacion"
									label="Fecha Recuperacion" labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaRecuperacion"
									value="%{definicionSiniestroRoboDTO.fechaRecuperacion}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onchange="getNumeroDeDiasRecuperacion();"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this);getNumeroDeDiasRecuperacion();"
									onblur="esFechaValida(this);">
								</sj:datepicker>			
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 20%;">
						<s:textfield id="diasRecuperacion" name="definicionSiniestroRoboDTO.diasRecuperacion"   cssClass="readOnly txtfield"  label="Dias" labelposition="left" cssStyle="width:30px;" readonly="true" ></s:textfield>
						</div>
						
					</div>
				</div>
				<div class="divContenedorO"
					style="height: 83px; width: 520px; ">
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 80%; padding-left: 13px" >
						<s:textfield id="ajustador" name="definicionSiniestroRoboDTO.ajustador"   cssClass="readOnly txtfield w200"  label="Ajustador" labelposition="left"  readonly="true" ></s:textfield>
						</div>
					</div>
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 80%; padding-left: 13px" >
						<s:textfield id="agente" name="definicionSiniestroRoboDTO.agente"   cssClass="readOnly txtfield w200"  label="Agente" labelposition="left" readonly="true" ></s:textfield>
						</div>
					</div>
					
				</div>
			</div>	
		</div>	
		<!-- DATOS VEHICULO FIN -->
			<!-- DATOS LOCALIDAD -->
		<div id="divLocalidad" style="width: 99%;" class="floatLeft">
					<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 100%; height: 80px;">
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="pais" name="definicionSiniestroRoboDTO.pais"   cssClass="readOnly txtfield"  label="País" labelposition="left" cssStyle="width:150px;" readonly="true" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="estadoMx" name="definicionSiniestroRoboDTO.estado"   cssClass="readOnly txtfield"  label="Estado" labelposition="left" cssStyle="width:150px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="municipio" name="definicionSiniestroRoboDTO.municipio"   cssClass="readOnly txtfield"  label="Municipio" labelposition="left" cssStyle="width:150px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="colonia" name="definicionSiniestroRoboDTO.colonia"   cssClass="readOnly txtfield"  label="Colonia " labelposition="left" cssStyle="width:150px;" readonly="true" ></s:textfield>
							</div>	
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="calle" name="definicionSiniestroRoboDTO.calle"   cssClass="readOnly txtfield"  label="Calle y Número" labelposition="left" cssStyle="width:180px;" readonly="true" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="referencia" name="definicionSiniestroRoboDTO.referencia"   cssClass="readOnly txtfield"  label="Referencia" labelposition="left" cssStyle="width:180px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="cp" name="definicionSiniestroRoboDTO.cp"   cssClass="readOnly txtfield"  label="Código Postal" labelposition="left" cssStyle="width:100px;" readonly="true" ></s:textfield>
							</div>
							
						</div>
						
						
						
						
				</div>
			</div>
			<!-- DATOS SEGUIMIENTO -->
			<div id="divSeguimiento" style="width: 99%; " class="floatLeft">
					<div class="titulo" align="left"  style="padding-top: 5px">
						<s:text name="midas.servicio.siniestros.robo.seguimiento" />
						</div>
					<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 100%;">
						<div class="divFormulario" >					
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tiposRobo" id="roboLista"
									name="definicionSiniestroRoboDTO.tipoRobo" label="Tipo de Robo" labelposition="left" 
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="estatus" id="estatusLista"
									name="definicionSiniestroRoboDTO.estatusVehiculo" label="Estatus del Vehiculo" labelposition="left" 
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numActa" name="definicionSiniestroRoboDTO.numActa" cssClass="jQalphanumeric txtfield"  label="No de Acta" labelposition="left" cssStyle="width:180px;" readonly="false" maxlength="30"></s:textfield>
								</div>
							
						</div>
						<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="averiguacionLocal" name="definicionSiniestroRoboDTO.averiguacionLocal" onkeypress="return soloNumeros(this, event, true)"  cssClass="jQnumeric jQrestrict txtfield"  label="No Averig. Lozaliz" labelposition="left" cssStyle="width:100px;" readonly="false" ></s:textfield>
								</div>					
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="localizador" id="localizadorLista"
									name="definicionSiniestroRoboDTO.localizador" label="Localizador" labelposition="left" 
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="ubicacionVehiculo" name="definicionSiniestroRoboDTO.ubicacionVehiculo"   cssClass="jQalphanumeric txtfield"  label="Ubicacion Vehiculo" labelposition="left" cssStyle="width:180px;" readonly="false" ></s:textfield>
								</div>
						</div>
						<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="averiguacionDen" name="definicionSiniestroRoboDTO.averiguacionDen"   cssClass="jQalphanumeric txtfield"  label="No Averig. Den." labelposition="left" cssStyle="width:100px;" readonly="false" maxlength="30"></s:textfield>
								</div>					
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="recuperador" id="recuperadorLista"
									name="definicionSiniestroRoboDTO.recuperador" label="Recuperador" labelposition="left" 
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
								</div>
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tipoMovimientos" id="causaMovimientoLista"
									name="definicionSiniestroRoboDTO.causaMovimiento" label="Causa de Movimiento" labelposition="left" 
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
								</div>
						</div>
						<div class="divFormulario" >
								<div class="floatLeft divInfDivInterno" style="width: 30%;" >
				 				<label id="estiqueta">Turno a Invertigacion?</label>
				 				<input name="definicionSiniestroRoboDTO.isTurnarInvetigacion" id="isTurnarInvetigaciontrue" value="true" onchange="activarProvedor('true');" type="radio">
								<label for="isTurnarInvetigaciontrue">Si</label>
								<input name="definicionSiniestroRoboDTO.isTurnarInvetigacion" id="isTurnarInvetigacionFalse" value="false" onchange="activarProvedor('false');" type="radio">
								<label for="isTurnarInvetigaciontrue">No</label>
								</div>	
									<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="proveedor" id="proveedorLista"
									name="definicionSiniestroRoboDTO.proveedor" label="Investigador" labelposition="left"  disabled="true"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
									</s:select>						
									</div>
									
									
							<div class="floatLeft divInfDivInterno" style="width: 40%;" >
								<s:textfield id="nombreAgenteMinisterioPublico" name="definicionSiniestroRoboDTO.nombreAgenteMinisterioPublico"   cssClass="jQalphanumeric txtfield"  label="Agente Ministerio Publico" labelposition="left" cssStyle="width:180px;" readonly="false" ></s:textfield>
							</div>	
							
							
									
						</div>
				
					</div>
				</div>
			<!-- Listado Coberturas -->
			<div id="divSeguimiento" style="width: 99%;  " class="floatLeft">
					<div class="titulo" align="left"  style="padding-top: 5px">
						<s:text name="midas.servicio.siniestros.robo.coberturas" />
						</div>
				 
					<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 100%;">
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 15%;" > 
								<s:textfield id="coberturaDes" name="definicionSiniestroRoboDTO.coberturaDes"  cssClass="readOnly txtfield"  label="Coberturas"  cssStyle="" readonly="true" ></s:textfield>
							</div>				
							<div class="floatLeft" style="width: 15%;">
							<s:textfield id="sumaAsegurada"  name="definicionSiniestroRoboDTO.sumaAsegurada"  onkeypress="return soloNumeros(this, event, true)" cssClass="readOnly txtfield formatCurrency" label="Suma Asegurada" cssStyle=""  readonly="true" ></s:textfield>
							
							</div>					
							<div class="floatLeft divInfDivInterno" style="width: 15%;" >
							<s:textfield id="valorComercial" name="definicionSiniestroRoboDTO.valorComercial" onkeypress="return soloNumeros(this, event, true)"  cssClass="readOnly txtfield formatCurrency" label="Valor Comercial" cssStyle="" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 15%;" >
							<s:if test="definicionSiniestroRoboDTO.aplicaDeducible">
								<s:textfield id="montoDed"  name="definicionSiniestroRoboDTO.montoDeducible" onkeypress="return soloNumeros(this, event, true)" cssClass="readOnly txtfield formatCurrency" label="Monto del deducible" cssStyle="" readonly="true" ></s:textfield>
							</s:if>
							<s:else>&nbsp;</s:else>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 15%;" >
							<s:hidden id="porcentajeDeducible" name="definicionSiniestroRoboDTO.porcentajeDeducible" onkeypress="return soloNumeros(this, event, true)" cssClass="readOnly txtfield" label="Porcentaje del Deducible" cssStyle="" readonly="true" />							
							&nbsp;
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 15%;" >
							<s:textfield id="reserva" name="definicionSiniestroRoboDTO.reserva" onkeypress="return soloNumeros(this, event, true)" cssClass="jQmoney txtfield formatCurrency" label="Reserva" cssStyle="" readonly="false" ></s:textfield>
							</div> 
						</div>
						<div class="divFormulario" style="padding-right: 120px" >
							<div id="conesp" class="btn_back w105"
								style="display: inline; margin-left: 1%; float: right; ">
								<a href="javascript: void(0);" onclick="inicializarCondicionesEspeciales(0);"> <s:text
										name="Condiciones Especiales" /> </a>
							</div>
							<div id="fortimax" class="btn_back w105"
								style="display: inline; margin-left: 1%; float: right; ">
								<a href="javascript: void(0);" onclick="ventanaFortimax('ROBO','' , 'FORTIMAX',  jQuery('#reporteCabinaId').val() , jQuery('#noReporte').val() );"> <s:text
										name="Imagenes Especiales" /> </a>
							</div>
						</div>
						<div class="divFormulario"    >
						</div>
						<div class="divInterno">
							<s:textarea cssStyle="width:80%;height:50px;" id="observaciones"
								rows="10" name="definicionSiniestroRoboDTO.observaciones" readonly="false"
								cssClass="textarea" label="Observaciones" labelposition="left"/>			
						</div>
					</div>
					
				</div>
				
			<!-- Montos Recuperacion -->
			<div id="divSeguimiento" style="width: 99%;  " class="floatLeft">
					<div class="titulo" align="left"  style="padding-top: 5px">
						<s:text name="midas.servicio.siniestros.robo.montos" />
						</div>
					<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 100%;">
						<div class="divInterno">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" > 
									<s:textfield id="valorComercialMomento" name="definicionSiniestroRoboDTO.valorComercialMomento"  onkeypress="return soloNumeros(this, event, true)" cssClass="txtfield formatCurrency"   label="Valor Comercial al momento" labelposition="top" cssStyle="" readonly="false" ></s:textfield>
							</div>				
							<div class="floatLeft" style="width: 30%;">
									<s:textfield id="presupuestoDanos" name="definicionSiniestroRoboDTO.presupuestoDanos"  onkeypress="return soloNumeros(this, event, true)" cssClass="txtfield formatCurrency"  label="Presupuesto Daños" labelposition="top" cssStyle="" readonly="false" ></s:textfield>
							</div>
						</div>
						<div class="divInterno">
							<div class="floatLeft" style="width: 30%;">
									<s:textfield id="faltantes" name="definicionSiniestroRoboDTO.faltantes"  cssClass="txtfield"  label="Faltantes" labelposition="top" cssStyle="" readonly="false" ></s:textfield>
								
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" > 
									<s:textfield id="montoProvicion" name="definicionSiniestroRoboDTO.montoProvicion"  onkeypress="return soloNumeros(this, event, true)" cssClass="formatCurrency txtfield"  label="Monto Provision" labelposition="top" cssStyle="" readonly="false" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" > 
									<s:textfield id="importePagado" name="definicionSiniestroRoboDTO.importePagado"  onkeypress="return soloNumeros(this, event, true)" cssClass="formatCurrency txtfield"  label="Importe Pagado" labelposition="top" cssStyle="" readonly="false" ></s:textfield>
							</div> 
						</div>
					</div>
				</div>			
				<div id="contenido_Btn" style="width:100%;height:40px; relative; padding-top: 10px">
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div  class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="cerrarDetalleRobo();">
								<s:text name="midas.boton.cerrar" /> </a>
						</div>
					</div>
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div id="guardar"  class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="salvar();"> <s:text
									name="midas.boton.guardar" /> </a>
						</div>
					</div>
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div id="nuevo"  class="btn_back w150"
						style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="envioOcra( '<s:property value='urlOcra'/>' );" > <s:text name="OCRA" /> </a> 
						</div>
					</div>
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
						<a href="javascript: void(0);"  onclick="abrirRepuve();"> 
							<s:text	name="REPUVE"/> 
						</a>
						</div>
					</div>
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div  class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="mostrarHistoricoRobos();"> <s:text
									name="Historicos de Movimientos" /> </a>
						</div>
					</div>
					<div class="floatRight divInfDivInterno" style="width: 15%; padding-top: 5px" >
						<div  class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a id="btn_Indemnizar" href="javascript: void(0);" onclick="javascript: ventanaModalCompra(jQuery('#h_estimacionId').val());"> 
								<s:text name="Indemnizar" /> 
							</a>
						</div>
					</div>				
				</div>
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		iniContenedorDefinicion();
		initCurrencyFormatOnTxtInput();
	});
	</script>
	
</div>
	</s:form>
