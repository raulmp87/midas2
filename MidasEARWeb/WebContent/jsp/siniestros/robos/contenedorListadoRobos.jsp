<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/robos/moduloRobos.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	var buscarPath = '<s:url action="busquedasSiniestrosRobo" namespace="/siniestros/cabina/reporteCabina/moduloRobos"/>';
	var excelPath = '<s:url action="exportarListado"  namespace="/siniestros/cabina/reporteCabina/moduloRobos"/>';
</script>
<div id="contenido_moduloRobos" style="width:99%;position: relative;">
	<s:form id="robosForm" class="floatLeft">
	<s:hidden id="reporteCabinaId" name="reporteCabinaId" />	
	<s:hidden id="idCobertura" name="idCobertura" />
	
		<div class="titulo" align="left">
			<s:text name="midas.servicio.siniestros.robos.titulo" />
		</div>
	<div id="divInferior" style="width: 1142px !important;" class="floatLeft">
		 
			<div id="contenedorFiltrosRobo" class="divContenedorO" style="width: 99%">
				<div class="divInterno">
					
						<div class="floatLeft divInfDivInterno" style="width: 15%;" >
							 <s:checkbox name="reporteSiniestroRoboDTO.servPublico"  id="servPublico"  label="Servicio Publico" labelposition="right" cssClass="cajaTextoM2"> </s:checkbox>
							 <s:checkbox name="reporteSiniestroRoboDTO.servParticular"  id="servParticular"  label="Servicio Particular" labelposition="right" cssClass="cajaTextoM2"> </s:checkbox>
						</div>
					
					<div style="width: 15%;" class="floatLeft">
							<div class="floatLeft" style="height: 30px; width: 45%;">
								<s:textfield id="nsOficinaS" maxlength="5"
									name="reporteSiniestroRoboDTO.claveOficinaSiniestro" cssStyle="width:95%;"									
									cssClass="cajaTextoM2 jQalphanumeric" label="No Siniestro"></s:textfield>								
							</div>
							<div class="floatLeft" style="height: 30px; width: 25%; padding-top: 24px">
								<s:textfield id="nsConsecutivoRS" maxlength="15"
									name="reporteSiniestroRoboDTO.consecutivoReporteSiniestro" cssStyle="width: 95%;"
									cssClass="cajaTextoM2 jQalphanumeric" ></s:textfield>
								
							</div>
							<div class="floatLeft" style="height: 30px; width: 30%;padding-top: 24px">
								<s:textfield id="nsAnioS" name="reporteSiniestroRoboDTO.anioReporteSiniestro"
									cssStyle="width: 95%;" maxlength="4"
									cssClass="cajaTextoM2 jQalphanumeric" ></s:textfield>
								
							</div>
						</div>
					
					
				
					
					<div class="floatLeft divInfDivInterno"  style="width: 15%;">
						<s:textfield id="numPoliza"  name="reporteSiniestroRoboDTO.numPoliza"  cssClass="cajaTextoM2 jQalphanumeric" label="No Poliza" maxlength="15;" cssStyle=""  readonly="false" ></s:textfield>
						
					</div>
					
					<div style="width: 15%;" class="floatLeft">
							<div class="floatLeft" style="height: 30px; width: 45%;">
								<s:textfield id="nsOficina" maxlength="5"
									name="reporteSiniestroRoboDTO.claveOficina" cssStyle="width:95%;"
									cssClass="cajaTextoM2 jQalphanumeric" label="No Reporte"></s:textfield>								
							</div>
							<div class="floatLeft" style="height: 30px; width: 30%; padding-top: 24px">
								<s:textfield id="nsConsecutivoR" maxlength="15"
									name="reporteSiniestroRoboDTO.consecutivoReporte" cssStyle="width: 95%;"
									cssClass="cajaTextoM2 jQalphanumeric" ></s:textfield>
								
							</div>
							<div class="floatLeft" style="height: 30px; width: 25%;padding-top: 24px">
								<s:textfield id="nsAnio" name="reporteSiniestroRoboDTO.anioReporte"
									cssStyle="width: 95%;" maxlength="4"
									cssClass="cajaTextoM2 jQalphanumeric" ></s:textfield>
								
							</div>
						</div>
					
					
							
					<div class="floatLeft divInfDivInterno" style="width: 15%;" >
						<s:textfield id="numSerie" name="reporteSiniestroRoboDTO.numSerie"  cssClass="cajaTextoM2 jQalphanumeric" label="No Serie" cssStyle="" maxlength="17;" readonly="false" ></s:textfield>
					</div>
					
					<div class="floatLeft divInfDivInterno" style="width: 15%;">
						<s:textfield id="numActa"  name="reporteSiniestroRoboDTO.numActa"  label="No Acta" cssClass="cajaTextoM2 jQalphanumeric"  maxlength="30;" cssStyle="" readonly="false" ></s:textfield>
					</div>
				</div>
				
				
				<div class="divInterno">
				
					
					<div class="floatLeft" style="width: 15%;">
						<s:select list="tiposRobo" id="roboLis"
							name="reporteSiniestroRoboDTO.tipoRobo" label="Tipo de Robo"
							cssClass="cajaTextoM2 txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</div>
						
					
					<div class="floatLeft" style="width: 15%;">
						<s:select list="estatus" id="estatusLis"
							name="reporteSiniestroRoboDTO.estatus" label="Estatus"
							cssClass="cajaTextoM2 txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</div>
					
								
					<div class="floatLeft" style="width: 15%;">
						<s:select list="oficinas" id="ofinasActivas"
							name="reporteSiniestroRoboDTO.oficinaId" label="Oficinas"
							cssClass="cajaTextoM2 txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:select list="estados" id="estadoslis"
							name="reporteSiniestroRoboDTO.estado" label="Estado"
							cssClass="cajaTextoM2 txtfield" headerKey="" onchange="onChangeMunicipio()" 
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:95%;">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:select list="municipios" id="municipioslis"
							name="reporteSiniestroRoboDTO.municipio" label="Municipio"
							cssClass="cajaTextoM2 txtfield" headerKey="" 
							cssStyle="width:95%;">
						</s:select>
					</div>
					
				</div>	
				
				<div class="divInterno">
					
				<div class="floatLeft" style="width: 15%;">
					<s:text	name="Fecha de Reporte" />
						<sj:datepicker name="reporteSiniestroRoboDTO.fechaIniReporte"
							label="Del" labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif" id="fechaIniReporte"
							value="%{reporteSiniestroRoboDTO.fechaIniReporte}" maxlength="10"
							cssClass="cajaTextoM2 txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker>
					</div>
					
					
				<div class="floatLeft" style="width: 15%;">
				<s:text	name="." />
						<sj:datepicker name="reporteSiniestroRoboDTO.fechaFinReporte"
							label="Al" labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif" id="fechaFinReporte"
							value="%{reporteSiniestroRoboDTO.fechaFinReporte}" maxlength="10"
							cssClass="cajaTextoM2 txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker>
					</div>
				
				
				<div class="floatLeft" style="width: 15%;">
					<s:text	name="Fecha de Ocurrido" />
						<sj:datepicker name="reporteSiniestroRoboDTO.fechaIniOcurrido"
							label="Del" labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif" id="fechaIniOcurrido"
							value="%{reporteSiniestroRoboDTO.fechaIniOcurrido}" maxlength="10"
							cssClass="cajaTextoM2 txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker>
					</div>
					
					
				<div class="floatLeft" style="width: 15%;">
					<s:text	name="." />
						<sj:datepicker name="reporteSiniestroRoboDTO.fechaFinOcurrido"
							label="Al"labelposition="left" changeMonth="true" changeYear="true"
							buttonImage="../img/b_calendario.gif" id="fechaFinOcurrido"
							value="%{reporteSiniestroRoboDTO.fechaFinOcurrido}" maxlength="10"
							cssClass="cajaTextoM2 txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);">
						</sj:datepicker>
					</div>
				</div>
				<div class="divInterno" style="padding-right: 150px">
				
					
					<div class="btn_back w80"
					style="display: inline; margin-left: 1%; float: right; ">
					<a href="javascript: void(0);" onclick="limpiarContenedorListadoRobos();">
						<s:text name="midas.boton.limpiar" /> </a>
					</div>
					<div class="btn_back w80"
					style="display: inline; margin-left: 1%; float: right;">
					<a href="javascript: void(0);" onclick="buscarListaRobos(false);">
						<s:text name="midas.boton.buscar" /> </a>
					</div>
					
				</div>
			</div>
		
		<div class="titulo" align="left" style="padding-top: 20px">
			<s:text name="midas.servicio.siniestros.roboLista.titulo" />
		</div>
		<div id="robosGrid" class="dataGridConfigurationClass"
			style="width: 99%; height: 120px; padding-top: 10px"></div>
			
		<div id="pagingArea" style="padding-top: 8px "></div>
		<div id="infoArea"></div>
		
			<div class="divInterno"	style="float: right; position: relative; width: 100%; margin-top: 1%;height: 40px;">
				<div class="btn_back w130" style="display: inline; float: right;"  >
					<a href="javascript: void(0);" onclick="exportarExcelRobos();">
						<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px'  src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
					</a>
				</div>
				
				
				
				
				
			</div>
	
		</div>
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</s:form>
<script>
//just when module ROBO is executed for first time 
	jQuery(document).ready(function() {
		buscarListaRobos(true);
	});
</script>
	
</div>
