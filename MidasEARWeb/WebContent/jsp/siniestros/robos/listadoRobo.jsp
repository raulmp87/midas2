<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>       
		<column id="siniestro" type="ro" width="100" sort="str" >No de Siniestro</column>
		<column id="reporte" type="ro" width="100" sort="str">No de Reporte</column>
		<column id="poliza" type="ro" width="130" sort="str">No de Poliza</column>
		<column id="serie" type="ro" width="150" sort="str">No de Serie</column>	
		<column id="vehiculo" type="ro" width="150" sort="str" >Tipo de Vehiculo</column>	
		<column id="acta" type="ro" width="120" sort="str" >No de Acta</column>	
		<column id="robo" type="ro" width="120" sort="str" >Tipo de Robo</column>	
		<column id="estatus" type="ro" width="100" sort="str" >Estatus</column>	
		<column id="reporteId" type="ro" width="250" sort="int"  hidden="true" >reporteId</column>
		<column id="idCobertura" type="ro" width="250" sort="int" hidden="true" >idCobertura</column>
		<column id="Oficina" type="ro" width="100" sort="str" >Oficina</column>	
		<column id="Estado" type="ro" width="130" sort="str" >Estado</column>	
		<column id="Municipio" type="ro" width="130" sort="str" >Municipio</column>	
		<column id="fechaReporte" type="ro" width="120" sort="str" >Fecha de Reporte</column>	
		<column id="fechaOcurrido" type="ro" width="120" sort="str" >Fecha de Ocurrido</column>	
		<column id="reporteRoboId" type="ro" width="120" sort="int" hidden="true" >reporteRoboId</column>
		<column id="consultar" type="img" width="100" sort="na" align="center">Acciones</column> 
		
		
		
	</head>
	
	<s:iterator value="listadoSiniestrosRobo" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="numSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numReporte" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="numPoliza" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="numSerie" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="tipoVehiculo" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="numActa" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="tipoRobo" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="idCobertura" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="oficina" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="estado" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="municipio" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="fechaReporte" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="fechaOcurrido" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="reporteRoboId" escapeHtml="true" escapeXml="true"/></cell>	
			<s:if test="idCobertura != null && idCobertura != \"\" ">
					<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: consultarRobo()^_self</cell>
			</s:if>
		</row>
	</s:iterator>
	
</rows>