<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
         <column id="proveedorId" type="ro" width="150" sort="str" >No Proveedor </column>        
        <column id="nombreProveedor" type="ro" width="285" sort="str" >Proveedor </column>
        <column id="numOrdenCompra" type="ro" width="100" sort="str" >No. O.C. </column>
        <column id="fechaOrdenCompra" type="ro" width="150" sort="str" >Fecha O.C.</column>
		<column id="montoOrdenCompra" type="ro" width="150" sort="str">Total O.C</column>				
		<column id="adminRefacciones" type="ro" width="*" sort="str" hidden="true" >adminRefacciones</column>	
		<column id="adminRefaccionesId" type="ro" width="*" sort="int" hidden="true" >adminRefaccionesId</column>	
		<column id="correoProveedor" type="ro" width="*" sort="str" hidden="true" >correoProveedor</column>	
		<column id="esRefaccion" type="ro" width="*" sort="str" hidden="true" >esRefaccion</column>	
		<column id="factura" type="ro" width="*" sort="str" hidden="true" >factura</column>	
		<column id="fechaOrdenPago" type="ro" width="*" sort="str" hidden="true" >fechaOrdenPago</column>	
		<column id="marcaVehiculo" type="ro" width="*" sort="str" hidden="true" >marcaVehiculo</column>	
		<column id="modeloVehiculo" type="ro" width="*" sort="str" hidden="true" >modeloVehiculo</column>	
		<column id="montoOrdenPago" type="ro" width="*" sort="str" hidden="true" >montoOrdenCompra</column>	
		<column id="nombreTaller" type="ro" width="*" sort="str" hidden="true" >nombreTaller</column>	
		<column id="nombreValuador" type="ro" width="*" sort="str" hidden="true" >nombreValuador</column>			
		<column id="numValuacion" type="ro" width="*" sort="int" hidden="true" >numValuacion</column>
		<column id="telefonoProveedor" type="ro" width="*" sort="str" hidden="true" >telefonoProveedor</column>
		<column id="tipoVehiculo" type="ro" width="*" sort="str" hidden="true" >tipoVehiculo</column>
		<column id="esResponsabilidadCivil" type="ro" width="*" sort="str" hidden="true" >esResponsabilidadCivil</column>
		<column id="esDanosMateriale" type="ro" width="*" sort="str" hidden="true" >esDanosMateriale</column>		
  	</head>      
   <s:iterator value="listaOrdenCompraRecuperacionDTO" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="noProveedor" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="nombreProveedor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="montoOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="adminRefacciones" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="adminRefaccionesId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="correoProveedor" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="esRefaccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="factura" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaOrdenPago" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="marcaVehiculo" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="modeloVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="montoOrdenPago" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="nombreTaller" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="nombreValuador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numValuacion" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="telefonoProveedor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoVehiculo" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="esResponsabilidadCivil" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="esDanosMateriale" escapeHtml="false" escapeXml="true"/></cell>	
		</row>
	</s:iterator>
	
</rows>
   
