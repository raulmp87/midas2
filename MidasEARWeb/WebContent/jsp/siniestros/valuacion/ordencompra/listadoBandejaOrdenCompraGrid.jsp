<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		
		<column  type="ro"  width="80" align="center">No. de Orden Compra</column>
		<column  type="ro"  width="180" align="center">Tipo</column>
		<column  type="ro"  width="100" align="center"> <s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosGeneralesReporte.noSiniestro" />   </column>
		<column  type="ro"  width="130" align="center"> <s:text name="midas.servicio.siniestros.robo.coberturas" />   </column>
		<column  type="ro"  width="180" align="center">Concepto de Pago</column>
		<column  type="ro"  width="90" align="center"> <s:text name="midas.negocio.fechaCreacion" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="subtotal" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="midas.cotizacion.iva" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="midas.fuerzaventa.configBono.importeTotal" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="total pagado" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="total pendiente" />   </column>
		<column  type="ro"  width="80" align="center"> <s:text name="midas.suscripcion.solicitud.solicitudPoliza.estatus" />   </column>
		<column  type="img"  width="25" align="center"> </column>
		
	</head>


	<s:iterator value="ordenesCompraAutorizar">
		
		<row id="<s:property value="#row.index"/>">
			
		    <cell><s:property value="noOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
		   	<cell><s:property value="tipoOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="noSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="conceptoPago" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaElaboracion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{subtotal})}"   escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{iva})}"   escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{importe})}"  escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{totalPagado})}"   escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="%{getText('struts.money.format',{totalPendiente})}"  escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
		    
		    <cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:consultaOrden(<s:property value="noOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>
	
</rows>