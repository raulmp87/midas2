<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>



<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:form id="ordenCompraIndForm" action="generarOrdenCompraPorIndemnizacion" namespace="/siniestros/valuacion/ordencompra" name="ordenCompraIndForm">
<div id="contenido_ordenCompra" style="width:99%;position: relative;">
	<s:hidden name="idEstimacionReporteCabina" id="idEstimacionReporteCabina"/>
	<s:hidden name="crearIndemnizacion" id="crearIndemnizacion"/>
			<div class="titulo" align="left" >
			<s:text  name="midas.siniestros.indemnizacion.perdidatotal.generar" />
			</div>
	<div id="divInferior" style="width: 100% !important;" class="floatLeft">	
			<s:if test="crearIndemnizacion == true">
				<div id="divGenerales" style="width: 100%;"  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 99%; height: 80px;">
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 99%;font-size:7pt;" >
								<s:select list="tipoIndemnizacionMap" id="tipoIndemnizacionLst"
										name="tipoIndemnizacion" label="%{getText('midas.siniestros.indemnizacion.perdidatotal.tipoindem')}"
										labelposition="left"
										cssClass="txtfield" headerKey=""
										headerValue="%{getText('midas.general.seleccione')}"
										cssStyle="width:46%;"
										>
								</s:select>	
							</div>
						</div>
						
						<div class="divFormulario">
							<div id="divNomBenef"  class="floatLeft divInfDivInterno" style="width: 99%;font-size:7pt;" >
									<s:textfield id="nomBeneficiario" name="ordenCompra.nomBeneficiario"   cssClass="txtfield jQalphanumeric"  label="Beneficiario" maxlength="90" labelposition="left" cssStyle="width:236px;"></s:textfield>
							</div>
						</div>
						
					</div>
					
					<div class="divFormulario">
						
							<div class="floatRight divInfDivInterno" style="width: 42%; padding-top: 10px" >
									<div class="btn_back w130"
									style="display: inline; float: left;">
									<a href="javascript: void(0);" onclick="generarOrdenCompra(tipoIndemnizacionLst.value, idEstimacionReporteCabina.value);"> <s:text
											name="Aceptar" />&nbsp;&nbsp;<img
										align="middle" border='0px' alt='Consultar'
										title='Aceptar' src='/MidasWeb/img/common/b_details.gif'
										style="vertical-align: left;" /> </a>
									</div>
							</div>
						</div>
					
				</div>						
			 </s:if>
			 <s:else>
				<div class="divFormulario">
					<div class="floatLeft divInfDivInterno" style="width: 99%; font-size:7pt;" >
							<s:text name="mensajePantalla" />
					</div>
				</div>
			</s:else>
			
	</div> 


<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
		
</div>
</s:form>
