<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
 <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	var jquery143 = jQuery.noConflict(true); 
</script>
              		          
<script type="text/javascript">
	 var listaPrestadoresServicioOC = new Array();	
</script>     
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	var mostrarDatosIncisoPoliza 	= '<s:url action="mostrarDatosIncisoPoliza" namespace="/siniestros/cabina/reportecabina"/>';
	var autorizarOrdenCompra 		= '<s:url action="autorizarOrdenCompra" namespace="/siniestros/valuacion/ordencompra"/>';
	var rechazarOrdenCompra 		= '<s:url action="rechazarOrdenCompra" namespace="/siniestros/valuacion/ordencompra"/>';
	var valeTallerPath	= '<s:url action="mostrarVale" namespace="/siniestros/cabina/reporteCabina/valeTaller"/>';
</script>
<s:form id="definirOrdenForm" class="floatLeft">
<div id="contenido_DefinirOrden" style="width:99%;position: relative;">
		<s:hidden id="idOrdenCompra_h" name="idOrdenCompra" />	
		<s:hidden id="idReporteCabina" name="idReporteCabina" />
		<s:hidden id="soloLectura" name="soloLectura" />
		<s:hidden id="soloIndemnizacion" name="soloIndemnizacion" />
		<s:hidden id="origen" name="ordenCompra.origen" />
		<s:hidden id="codigoUsuarioCreacion" name="ordenCompra.codigoUsuarioCreacion" />
		<s:hidden id="idCoberturaReporteCabina" name="idCoberturaReporteCabina" />
		<s:hidden id="cveSubTipoCalculoCobertura" name="cveSubTipoCalculoCobertura" />
		<s:hidden id="bandejaPorAutorizar" name="bandejaPorAutorizar" />
		<s:hidden id="aplicaPagoDaños" name="aplicaPagoDaños" />
		<s:hidden id="idDetalleOrdenSeleccionado" name="idDetalleOrdenSeleccionado" />
		<s:hidden id="altaCeros" name="altaCeros" />
		<s:hidden id="origenPantalla" name="origen" />
		<s:hidden name="modoConsulta" id="modoConsulta"></s:hidden>
		<s:hidden id="tipoOrdenCompraLis" name="ordenCompra.tipo" />
		<s:hidden id="reembolsoGastoAList" name="ordenCompra.reembolsoGA" />
		<s:hidden name="tipo" id="tipoOrdenCompra"></s:hidden>
		<s:hidden name="ordenCompra.idOrdenCompraOrigen" id="idOrdenCompraOrigen"></s:hidden>
		<s:hidden id="h_terminoDeAjuste" name="codigoTerminoAjuste" />
		<s:hidden id="oc_aplicaDeducible" name="ordenCompra.aplicaDeducible" />
	  	<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="tituloOrdenCompraDetalle" />
			</div>
		 
			<div id="divGenerales" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 100%; height: 150px;">
					
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 10%;" >
								<s:textfield id="idOrdencompra" name="ordenCompra.id"   cssClass="txtfield"  label="Folio" labelposition="left" cssStyle="width:50px;" readonly="true" ></s:textfield>
							</div>
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 20%; " >
								   <s:textfield id="estatusTxt" name="ordenCompra.estatus"   cssClass="txtfield"  label="Estatus" labelposition="left" cssStyle="width:100px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="numReporte" name="numReporte"   cssClass="txtfield"  label="No Reporte" labelposition="left" cssStyle="width:120px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="numSiniestro" name="numSiniestro"   cssClass="txtfield"  label="No Siniestro" labelposition="left" cssStyle="width:120px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="fechaOcurridoSiniestroId" name="fechaOcurrido"   cssClass="txtfield"  label="Fecha Ocurrido" labelposition="left" cssStyle="width:80px;" readonly="true" ></s:textfield>
							</div>
						</div>
					
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="creadoPor" name="creadoPor" cssClass="txtfield" readonly="true" label="Creado" labelposition="left" cssStyle="width:190px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="modificadoPor" name="modificadoPor" cssClass="txtfield" readonly="true" label="Modificado" labelposition="left" cssStyle="width:190px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tipoPagoMap" id="tipoPagoLis"
									name="ordenCompra.tipoPago" label="Tipo de Pago"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:70%;"
									onchange="changeTipoPago()"
									
									>
									</s:select>							
							</div>
							
							
						</div>
						
						<div class="divFormulario">
						
							
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="terminoAjuste" name="terminoAjuste" cssClass="txtfield" readonly="true" label="Término de Ajuste." labelposition="left" cssStyle="width:198px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="terminoSiniestro" name="terminoSiniestro" cssClass="txtfield" readonly="true" label="Término de Siniestro." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>	
							
							<s:if test="ordenCompra.sipac == '1'">
								<div class="floatLeft divInfDivInterno" style="width: 30%;" > 
									<s:checkbox name="ordenCompra.sipac" label="Sipac" readonly="true" labelposition="left"></s:checkbox>
								</div>
							</s:if>
							
						</div>
						<!--<div id="divdatosBeneficiario"   class="divFormulario">
							
						<div class="floatLeft divInfDivInterno" style="width: 40%;" >
									<s:select list="reembolsoGastoAmap" id="reembolsoGastoAList"
									name="ordenCompra.reembolsoGA" label="Reembolso de Gasto de Ajuste"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:20%;"
									>
									</s:select>							
							</div>  
						
						</div>-->
						
						<div class="divFormulario">
						
							<div id="divcoberura1" class="floatLeft divInfDivInterno" style="width: 30%; " >
									<s:select list="coberturaMap" id="coberturaLis"
									name="coberturaCompuesta" label="Cobertura"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
									onchange="changeCobertura();"
								
									>
									</s:select>							
							</div>
								
							<div id="divterceroafec" class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:select id="terceroAfectadoLis" 
										labelposition="left" 
										label="Afectado"	
										name="ordenCompra.idTercero"									
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="terceroAfectadoMap" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 w250"   />
							</div>
							
								<div id="divAfectadoRembolsoGA" class="floatLeft divInfDivInterno" style="width: 30%; " >
									<s:select id="afectadoLis" 
												labelposition="left" 
												label="Tipo de Afectado."	
												name="ordenCompra.tipoAfectado"									
												headerKey="" headerValue="%{getText('midas.general.seleccione')}"
										  		list="tipoAfectado" listKey="key" listValue="value"  
										  		cssClass="txtfield cajaTextoM2 w160"   
										/>
								</div>
								
							
								
								
								
						</div>
						
					</div>
			</div>
			<div class="titulo" id="tituloPago" align="left" >
				<s:text   name="Datos del Pago"/>
				
			</div>
			<div id="divGenerales2" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltrosCompras2" class="divContenedorO" style="width: 100%; ">
						
						<div class="divFormulario">
							
							<div id="divNomBenef"  class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:textfield id="nomBeneficiario" name="ordenCompra.nomBeneficiario"   cssClass="txtfield"  label="Beneficiario." labelposition="left" cssStyle="width:150px;"></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" id="divdatosBeneficiario" style="width: 30%;" >
									<s:select list="tipoPersona" id="tipoPersonaLis"
									name="ordenCompra.tipoPersona" label="Tipo Persona"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:70%;"
									
									>
									</s:select>							
							</div>
							
							<div id="divProvedor1" class="floatLeft divInfDivInterno" style="width: 30%; 	" >
									<s:select id="tipoProveedorLis" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="ordenCompra.tipoProveedor"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160"   
							  		onchange="changeTipoPrestador(); validaInfoPagoCompania();"
							  	 />
							</div>	
							<s:hidden id="proveedorLis" name="ordenCompra.idBeneficiario" />
							<div id="divProvedorNom1"   class="floatLeft divInfDivInterno" style="width: 40%; " >
										Proveedor:<input value="${nombrePrestadorServicio}"  class="txtfield" style="width: 300px" type="text" id="nombrePrestadorServicio" 
														name="nombrePrestadorServicio" title="Comience a teclear el nombre del proveedor y seleccione una opción de la lista desplegada"/>
						    			<img src='<s:url value="/img/close2.gif"/>' id="btnBorrarProveedor" style="vertical-align: bottom;"  alt="Limpiar descripción" title="Limpiar descripción"
						             		 onclick ="mostrarMensajeConfirm('Está seguro que desea cambiar el proveedor?', '20', 'limpiarProveedor()', null, null);"/>	
										
							<!--
										<s:select id="proveedorLisPrueba" 
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="proveedorMap" listKey="key" listValue="value"  
								  		onchange="changeProveedor()"
								  		cssClass="txtfield cajaTextoM2 w160"   
								  		/>
								  		<s:hidden id="proveedorLis" name="ordenCompra.idBeneficiario"	 />  -->
						
												
						</div>
							
							<div class="floatLeft divInfDivInterno" id="divdatosBeneficiario" style="width: 30%;" >
									<s:select id="s_aplDeducible"  
										list="#{'true':'SI','false':'NO'}"
										headerKey="null" headerValue="SELECCIONE..."
										value="ordenCompra.aplicaDeducible" 
										label="Aplica Deducible" labelposition="left"
										cssClass="txtfield" cssStyle="width:50%;"
										disabled="true"	/>							
							</div>
								
						</div>
						
						
						<div class="divFormulario" style="width:100%;">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="curp" name="ordenCompra.curp"   cssClass="txtfield"  label="C.U.R.P." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="rfc" name="ordenCompra.rfc"   cssClass="txtfield"  label="R.F.C." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="factura" name="ordenCompra.factura"   cssClass="txtfield"  label="Factura." labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
						</div>

						

						<div id="companiaDiv" class="divFormulario">

							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="siniestroTerceroId" name="ordenCompra.cartaPago.siniestroTercero" maxlength="39"  cssClass="txtfield"  label="No Siniestro Tercero" labelposition="left" cssStyle="width:130px;"  ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<sj:datepicker name="ordenCompra.cartaPago.fechaCreacion"
												label="Fecha de Carta"
										  labelposition="left"
											changeMonth="true"
											 changeYear="true"				
											buttonImage="/MidasWeb/img/b_calendario.gif"
											buttonImageOnly="true" 
				                        			 id="dp_cartaPagoFechaCreacion"
											  maxlength="10" cssClass="txtfield cajaTextoM2 w110 requerido"
												   size="12"
											 onkeypress="return soloFecha(this, event, false);"
											onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
											cssStyle="width:100px;">
									</sj:datepicker>
								<!--
								<s:textfield id="fechaFacturaId" name="ordenCompra.cartaPago.fechaCreacion"   cssClass="txtfield"  label="Fecha Factura" labelposition="left" cssStyle="width:100px;"  ></s:textfield>
								-->
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<sj:datepicker name="ordenCompra.cartaPago.fechaRecepcion"
												label="Fecha de Recepcion de Carta"
										  labelposition="left"
											changeMonth="true"
											 changeYear="true"				
											buttonImage="/MidasWeb/img/b_calendario.gif"
											buttonImageOnly="true" 
				                        			 id="dp_cartaPagoFechaRecepcion"
											  maxlength="10" cssClass="txtfield cajaTextoM2 w110 requerido"
												   size="12"
											 onkeypress="return soloFecha(this, event, false);"
											onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
											cssStyle="width:100px;">
									</sj:datepicker>
								<!--
								<s:textfield id="fechaRecepcionFacturaId" name="ordenCompra.cartaPago.fechaRecepcion"   cssClass="txtfield"  label="Fecha de Recepcion Factura" labelposition="left" cssStyle="width:100px;"  ></s:textfield>
								-->
							</div>

						</div>

						
						
						<div class="divFormulario"  id="divReembolsoGA" >
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="rfcOrigen" name="ordenCompra.rfcOrigen"   cssClass="txtfield"  label="R.F.C. Origen." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="proveedorOrigen" name="ordenCompra.proveedorOrigen"   cssClass="txtfield"  label="Proveedor Origen" labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="facturaOrigen" name="ordenCompra.facturaOrigen"   cssClass="txtfield"  label="Factura Origen." labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
							
						</div>
						
						
						
						
						<div class="divFormulario"  id="divDatosIndemnizacion" >
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:select id="bancosLis" 
										labelposition="left" 
										label="Banco Receptor "	
										name="ordenCompra.bancoId"
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="bancos" cssClass="txtfield cajaTextoM2 w160"   
								/>
							</div>
								
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="clabe" name="ordenCompra.clabe"   cssClass="txtfield"  label="CLABE." labelposition="left"  onkeypress="return soloNumeros(this, event, true);"  cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="correo" name="ordenCompra.correo"   cssClass="txtfield"  label="Correo." labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 11%;" >
								<s:textfield id="lada" name="ordenCompra.lada"   cssClass="txtfield"  label="Telefono" labelposition="left" onkeypress="return soloNumeros(this, event, true);" maxlength="3" cssStyle="width:50px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 9%;" >
								<s:textfield id="telefono" name="ordenCompra.telefono"   cssClass="txtfield"   onkeypress="return soloNumeros(this, event, true);" maxlength="8"  cssStyle="width:80px;"  ></s:textfield>
							</div>
							
						</div>
						
						
						
						<div class="divFormulario">
							<s:if test="%{soloLectura==false}">	
								<div id="guardarBtn" class="btn_back w90"
									style="display: inline; margin-left: 1%; float: right;">
									<a href="javascript: void(0);" onclick="salvarEncabezado();"> <s:text
											name="midas.boton.guardar" /> </a>
								</div>
							</s:if>
						</div>
					
					</div>
			</div>
			
			
			
		</div>
			
	
	  <div id="altaConcepto" style="width: 1050px !important;" class="floatLeft">
			
			<div class="titulo" align="left" >
				<s:text name="midas.servicio.siniestros.valuacion.ordencompra.conceptos" />
			</div>
		 
			<div id="divGeneralesdeta" style="width: 1050px;  class="floatLeft">
					<div id="contenedorDetasCompras" class="divContenedorO" style="width: 100%; height: 210px;">
						<div class="divFormulario">
							<div id="conceptosdiv" class="floatLeft divInfDivInterno" style="width: 40%; " >
									<s:select list="conceptoPagoMap" id="conceptoLis"
									name="idConcepto" label="Concepto de Pago"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									onchange="changeConceptos()"
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
							
									>
									</s:select>	
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 17%;" >
										<s:textfield id="costoUnitario" name="detalleOrdenCompra.costoUnitario" onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#costoUnitario',this.value);" onblur="mascaraDecimales('#costoUnitario',this.value);calculaIvaImporte();calculaTotal()" cssClass="txtfield  setNew formatCurrency"   label="Costo Unitario" labelposition="left" cssClass="txtfield   setNew formatCurrency"    readonly="false"  cssStyle="width:80px;" ></s:textfield>

							</div>
														
							<s:if test="origen=='AUT_IND'  ">	
								<div class="floatLeft divInfDivInterno" style="width: 17%;" >
											<s:textfield id="importe" name="detalleOrdenCompra.importe"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#importe',this.value);"  onblur="mascaraDecimales('#importe',this.value);"  cssClass="txtfield  setNew formatCurrency"  	readonly="true"   label="Importe" labelposition="left" cssStyle="width:80px;" ></s:textfield>
								</div>
							</s:if>
							
						</div>
							<div class="divFormulario" id ="divPorcentajes">
									
									<div class="floatLeft divInfDivInterno" style="width: 13%;" >
										<s:textfield id="porcIva" name="detalleOrdenCompra.porcIva"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#porcIva',this.value);"  onblur="mascaraDecimales('#porcIva',this.value);"  cssClass="jQfloat txtfield"  label="% IVA" labelposition="left" readonly="true" cssStyle="width:80px;" ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 13%;" >
										<s:textfield id="iva" name="detalleOrdenCompra.iva"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#iva',this.value);"  onblur="mascaraDecimales('#iva',this.value);calculaTotal()"  cssClass="txtfield  setNew formatCurrency"  	 label="IVA" labelposition="left" cssStyle="width:80px;"  ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 18%;" >
										<s:textfield id="porcIvaRetenido" name="detalleOrdenCompra.porcIvaRetenido"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#porcIvaRetenido',this.value);"  onblur="mascaraDecimales('#porcIvaRetenido',this.value);"  cssClass="jQfloat txtfield" readonly="true" label="% I.V.A. Retenido" labelposition="left" cssStyle="width:80px;" ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 18%;" >
										<s:textfield id="ivaRetenido" name="detalleOrdenCompra.ivaRetenido"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#ivaRetenido',this.value);"  onblur="mascaraDecimales('#ivaRetenido',this.value);calculaTotal()"  cssClass="txtfield  setNew formatCurrency"  	 label="I.V.A. Retenido" labelposition="left" cssStyle="width:80px;"  ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 18%;" >
										<s:textfield id="porcIsr" name="detalleOrdenCompra.porcIsr"   onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#porcIsr',this.value);"  onblur="mascaraDecimales('#porcIsr',this.value);"  cssClass="jQfloat txtfield"  label="%I.S.R. Retenido" labelposition="left" readonly="true" cssStyle="width:80px;" ></s:textfield>
									</div>
									
									<div class="floatLeft divInfDivInterno" style="width: 18%;" >
										<s:textfield id="isr" name="detalleOrdenCompra.isr"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#isr',this.value);"  onblur="mascaraDecimales('#isr',this.value);calculaTotal()"  cssClass="txtfield  setNew formatCurrency"  	 label="I.S.R. Retenido" labelposition="left" cssStyle="width:80px;"  ></s:textfield>
									</div>
							</div>
						
						<s:if test=" origen!='AUT_IND'   ">	
							<div class="divFormulario">
									<div class="floatLeft divInfDivInterno" style="width: 14%;" >
										<s:textfield id="importe" name="detalleOrdenCompra.importe"  onkeypress="return soloNumeros(this, event, true);mascaraDecimales('#importe',this.value);"  onblur="mascaraDecimales('#importe',this.value);"  cssClass="txtfield  setNew formatCurrency"  	readonly="true"   label="Importe" labelposition="left" cssStyle="width:80px;" ></s:textfield>
									</div>
							</div>
						</s:if>
						
						
						<div class="divFormulario">
								<div class="floatLeft divInfDivInterno" style="width: 50%;" >
										<s:textarea cssStyle="width:80%;height:90px;" id="observaciones"
										label="Observaciones" labelposition="left"
										rows="10" name="detalleOrdenCompra.observaciones" 
										 maxlength="150"
										cssClass="textarea" />							
								</div>	
							</div>
							
							<div class="divFormulario">
								<div class="floatLeft divInfDivInterno" style="width: 25%;" >
									
									
									<s:if test="%{soloLectura==false}">
									
									<div id="nuevo" class="btn_back w90" 
											style="display: inline; margin-left: 1%; float: left;">
											<a href="javascript: void(0);" onclick="nuevoConcepto();"> <s:text
												name="Limpiar" /> </a>
									</div>
									
									<div id="alta" class="btn_back w120" 
											style="display: inline; margin-left: 1%; float: left;">
											<a href="javascript: void(0);" onclick="altaConcepto();"> <s:text
												name="Guardar Concepto" /> </a>
									</div>
									
									
									</s:if>					
								</div>	
							</div>
					</div>
			</div>
		</div>
		<!-- TITULO  -->
			<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
				<div class="titulo" align="left" >
				<s:text name="midas.servicio.siniestros.valuacion.ordencompra.listadoConceptos" />
				</div>
			 
				
				<div id="conceptosGrid" class="dataGridConfigurationClass"
				style="width: 99%; height: 250px;"></div>			
				<div id="pagingArea" style="padding-top: 8px "></div>
				<div id="infoArea"></div>
			</div>
			
	 <div id="divInferior" style="width: 1050px !important;" class="floatLeft">
	 
	 		<div class="floatLeft"
				style="height: 15px; width: 620px; ">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.valuacion.ordencompra.importes" />		
				</div>
			</div>
				
			<div class="floatLeft"
				style="height: 15px; width: 420px;  padding-left: 10px">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.valuacion.ordencompra.totales" />
				</div>
			</div>
				
		</div>
	<div id="divInferior" style="width: 100% !important; height: 80px; padding-top: 8px " class="floatLeft">
			<div class="floatLeft"
				style="height: 70px; width: 620px; ">
				
				<div class="divContenedorO"
					style="height: 60px; width: 100%; margin-bottom: 7px;">
					
					
					<div class="divFormulario" >
					
						<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_subtotal"  name="importesDTO.subtotal"    cssClass="txtfield  setNew formatCurrency"  label="Sub Total" labelposition="left" cssStyle="width:70px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="Text_Iva"   name="importesDTO.iva" cssClass="txtfield  setNew formatCurrency"    label="IVA" labelposition="left" cssStyle="width:70px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_IvaRetenido"   name="importesDTO.ivaRetenido" cssClass="txtfield  setNew formatCurrency"    label="IVA Retenido" labelposition="left" cssStyle="width:70px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_Isr"   name="importesDTO.isr" cssClass="txtfield  setNew formatCurrency"    label="ISR" labelposition="left" cssStyle="width:70px;" readonly="true"></s:textfield>
						</div>
						
					</div>
					<div class="divFormulario" >
						
					
						 <s:if test=" tipo != 'GA' && tipo != 'RGA' ">
						 	
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_Deducible"  name="ordenCompra.deducible"    cssClass="txtfield  setNew formatCurrency"  label="Deducible" labelposition="left" cssStyle="width:70px;" disabled="true"></s:textfield>
							</div>
		    			</s:if>
		    			
		    			 <s:if test=" ordenCompra.origen == 'AUT_IND'  ||  ordenCompra.origen == 'HGS_IN_PT'  ">
						 	
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_Descuento"   name="ordenCompra.descuento"   cssClass="txtfield  setNew formatCurrency" label="Descuento" labelposition="left" cssStyle="width:80px;" readonly="true"></s:textfield>
							</div>
		    			</s:if>
		    			
		    			
		    			
						
						
						
						<div class="floatLeft divInfDivInterno" style="width: 25%;padding-left: 15px " >
								<s:textfield id="Text_total"  name="importesDTO.total"    cssClass="txtfield  setNew formatCurrency"   label="Total" labelposition="left" cssStyle="width:70px;" readonly="true"></s:textfield>
						</div>
						
					</div>
				</div>
				
				
				
			</div>	
			<div class="floatLeft"
				style="height: 70px; width: 420px; padding-left: 10px ">
				

				<div class="divContenedorO" style="height: 60px; width: 100%; margin-bottom: 7px;">	
				
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 50%; padding-left: 8px "  >
								<s:textfield id="text_Tpagado"  name="importesDTO.totalPagado"  cssClass="txtfield  setNew formatCurrency"   label="Total Pagado" labelposition="left"  cssStyle="width:80px;"  readonly="true" ></s:textfield>
						</div>
					  	<div class="floatLeft divInfDivInterno" style="width: 42%;" >
								<s:textfield id="text_Tpendiente"  name="importesDTO.totalPendiente"   cssClass="txtfield  setNew formatCurrency"    label="Total Pendiente" labelposition="left"  cssStyle="width:80px;" readonly="true" ></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno" style="width: 50%; padding-left: 8px" >
								<s:textfield id="text_Treserva"  name="reservaDisponible"   cssClass="txtfield  setNew formatCurrency"    label="Reserva Disponible" labelposition="left"  cssStyle="width:80px;" readonly="true" ></s:textfield>
						</div>
					</div>
				
				</div>
				
			</div>	
		</div>		
		<div id="divInferior" style="width: 1050px !important; height: 50px; padding-top: 8px " class="floatLeft">
			
			<div class="divFormulario" >
					
						<div class="floatRight divInfDivInterno" style="width: 12%;" >
							<div class="btn_back w100"
								style="display: inline; margin-left: 2%;  float: right;">
								<a href="javascript: void(0);" onclick="cerrarDetalle();">
									<s:text name="midas.boton.cerrar" /> </a>
							</div>
						</div>
						
						<s:if test="ordenCompra.estatus == 'Tramite' ">	
							<div class="floatRight divInfDivInterno" style="width: 12%;" >
								<div class="btn_back w100"  id="btn_autorizar2"
									style="display: inline; margin-left: 2%;  float: right;">
									<a href="javascript: void(0);" onclick="autorizar();">
										<s:text name="Autorizar" /> </a>
								</div>
							</div>
						</s:if>
						
						<s:if test="bandejaPorAutorizar">	
							<s:if test="ordenCompra.estatus == 'Tramite'">	
								<div class="floatRight divInfDivInterno" style="width: 12%;" >
									<div class="btn_back w100" id="btn_rechazar" style="display: inline;   margin-left: 2%;float: right;">
										<a href="javascript: void(0);"  onclick="rechazar();"> 
											<s:text	name="Rechazar"/> 
										</a>
									</div>
								</div>
							</s:if>
						</s:if>
						
						
						<s:if test="!bandejaPorAutorizar">
							<s:if test="%{ null != ordenCompra.id  &&  ordenCompra.id!=''  && ordenCompra.estatus!='Pagado'  && ordenCompra.estatus!='Cancelado' }">	
								<div class="floatRight divInfDivInterno" style="width: 12%;" >
									<div class="btn_back w100"
										style="display: inline; margin-left: 2%; float: right;">
										<a href="javascript: void(0);"  onclick="cancelacionrOrden();"> 
											<s:text name="Cancelar" /> </a>
									</div>
								</div>
							</s:if>
							
							<s:if test="%{  null != ordenCompra.id  &&  ordenCompra.id!=''    }">	
								<div id="tallerBtn" class="floatRight divInfDivInterno" style="width: 12%;" >
									<div  class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
										<a href="javascript: void(0);"  onclick="imprimirValeTaller();"> 
											<s:text	name="Vale Taller"/> 
										</a>
									</div>
								</div>
								
								
								<div class="floatRight divInfDivInterno" style="width: 12%;" >
									<div id="imprimirBtn" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
										<a href="javascript: void(0);"  onclick="imprimir();"> 
											<s:text	name="Imprimir"/> 
										</a>
									</div>
								</div>
							</s:if>
						
						
						</s:if>
						
						
						
						
						
						
				</div>
		</div>	
			
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		iniContenedoDetarGrid();
		validateIsFromBandeja();
		initCurrencyFormatOnTxtInput();
		validaInfoPagoCompania();
	});
	</script>
	
</div>
	</s:form>



