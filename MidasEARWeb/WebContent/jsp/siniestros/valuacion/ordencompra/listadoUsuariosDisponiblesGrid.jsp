<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>		
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="usuario.idOrdenCompraPermiso" type="ro" width="0" sort="int" >id</column>
		<column id="usuario.idUsuario" type="ro" width="0" sort="int" >id</column>
		<column id="usuario.idUsuarioPermiso" type="ro" width="0" sort="int" >idUsuarioPermiso</column>
		<column id="usuario.nombreUsuario" type="ro"  width="120" sort="str"  align="center"> <s:text name="Clave Usuario" />   </column>
		<column id="usuario.nombre" type="ro"  width="*"  sort="str"  align="center"> <s:text name="Nombre Usuario" />   </column>

	   
	</head>


	<s:iterator value="usuariosDisponibles">
		
		<row id="<s:property value="#row.index"/>">
			
			<cell><s:property value="idOrdenCompraPermiso" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idUsuarioPermiso" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreUsuario" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
		   
			
		</row>
	</s:iterator>
	
</rows>