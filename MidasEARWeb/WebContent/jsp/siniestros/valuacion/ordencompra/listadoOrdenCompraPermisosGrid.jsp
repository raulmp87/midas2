<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		
		<column  type="ro"  width="100" align="center"> <s:text name="midas.negocio.oficina" />   </column>
		<column  type="ro"  width="70" align="center"> <s:text name="midas.suscripcion.solicitud.solicitudPoliza.estatus" />   </column>
		<column  type="ro"  width="120" align="center"> <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionAnexo.nivel" />   </column>
		<column  type="ro"  width="100" align="center"> <s:text name="midas.emision.consulta.poliza.fechacreacion" />   </column>
		<column  type="ro"  width="*" align="left"> <s:text name="midas.general.seccion" />   </column>
		<column  type="ro"  width="*" align="left"> <s:text name="midas.general.cobertura" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.fuerzaventa.movimientomanual.alta.concepto" />   </column>
		<column  type="ro"  width="120" align="center"> <s:text name="midas.servicio.siniestros.tipo" />   </column>
		<column  type="img"  width="30" align="center"> <s:text name="" />   </column>
	    <column  type="img"  width="30" align="center"> <s:text name="" />   </column>
	    <column  type="img"  width="30" align="center"> <s:text name="" />   </column>
		
	</head>


	<s:iterator value="resultadoPermisos">
		
		<row id="<s:property value="#row.index"/>">
			
		    <cell><s:property value="oficina" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nivelAutorizacion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="seccion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="concepto" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell>../img/icons/ico_editar.gif^Editar^javascript:editarPermiso(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/listsicon.gif^Usuarios^javascript:relacionarUsuarios(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/delete16.gif^Eliminar^javascript:eliminarPermisos(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>
	
</rows>