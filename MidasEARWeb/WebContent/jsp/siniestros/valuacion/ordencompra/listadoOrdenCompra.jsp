<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="cveOrigen" type="ro" width="*" sort="int" hidden="true" >cveOrigen</column>	
        <column id="noOrdenCompra" type="ro" width="70" sort="str" >No Orden Compra </column>
        <column id="tipo" type="ro" width="*" sort="str" >Tipo </column>
        <column id="origen" type="ro" width="70" sort="str" >Origen</column>
		<column id="beneficiario" type="ro" width="*" sort="str">Beneficiario</column>
		<column id="impPresupuesto"   type="ron" width="120" format="$0,000.00" sort="int">Imp. Presupuesto</column>
		<column id="impPagado"   type="ron" width="120" format="$0,000.00" sort="int">Imp. Pagado</column>
		<column id="refacciones" type="ro" width="100" sort="str" >Refacciones</column>	
		<column id="estatus" type="ro" width="80" sort="str" >Estatus </column>
		<column id="editar" type="img" width="40" sort="na" align="center"/>
		<column id="consultar" type="img" width="40" sort="na" align="center"/>	   		
		<column id="reporteCabinaId" type="ro" width="*" sort="int" hidden="true" >reporteCabinaId</column>	
		<column id="contieneFirma" type="ro" width="*" sort="str"  hidden="true">contieneFirma </column>
				
  	</head>      
   <s:iterator value="listaOrdenCompra" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="cveOrigen" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="noOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="origen" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="beneficiario" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="impPresupuesto" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="impPagado" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="refacciones" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>			
			 <s:if test="  modoConsulta !=true && estatus == 'Tramite'  && cveOrigen=='MANUAL' &&  contieneFirma!=true  ">
			 	<s:if test=" tipoOrdenCompra =='Gasto de Ajuste'  ">
			 			<cell>/MidasWeb/img/icons/ico_editar.gif^Generar OC^javascript: editarOrden(<s:property value="ordenCompraId" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
			 	</s:if>
			 	<s:else>
			 		<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarOrden(<s:property value="ordenCompraId" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
			 	</s:else>
		    </s:if>
		     <s:else>
		      	 <s:if test="modoConsulta !=true && estatus == 'Tramite'  && ( cveOrigen=='HGS_IN_PT' || cveOrigen=='HGS_IN_DA'  )  ">
		      		<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarOrden(<s:property value="ordenCompraId" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		    	</s:if>	
		    	<s:else>
		      		<cell>/MidasWeb/img/blank.gif^^</cell>	
		   		</s:else>
		    </s:else>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarOrden(<s:property value="ordenCompraId" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="contieneFirma" escapeHtml="true" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>
	
</rows>
   
