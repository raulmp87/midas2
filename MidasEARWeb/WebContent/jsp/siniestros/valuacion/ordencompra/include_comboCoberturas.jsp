<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="!soloConsulta">	
	<s:select list="coberturaMap" id="idCobertura" label="Cobertura" labelposition="left" cssClass="cajaTexto w120 alphaextra consulta requerido" headerKey="" headerValue="Seleccione ..."
					onchange="obtenerConceptoDePago(this);"></s:select>
</s:if>

<s:if test="soloConsulta">	
	<s:textfield id="cobertura"  name="ordenCompraPermiso.coberturaSeccion.coberturaDTO.descripcion"
					cssClass="cajaTexto w150 alphaextra consulta"  label="Cobertura" labelposition="left"  ></s:textfield>
</s:if>