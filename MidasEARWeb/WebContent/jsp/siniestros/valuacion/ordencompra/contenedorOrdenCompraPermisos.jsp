<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/autorizacionOrdenesCompra.js'/>"></script>

<script type="text/javascript">
	var listadoPermisoUsuario 		= '<s:url action="listadoPermisoUsuario" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var cargarConceptoPago 			= '<s:url action="cargarConceptoPago" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var cargarCoberturas 			= '<s:url action="cargarCoberturas" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var guardarPermiso 				= '<s:url action="guardarPermiso" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var mostrarRelacionarUsuarios	= '<s:url action="mostrarRelacionarUsuarios" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var mostrarEditarPermiso		= '<s:url action="mostrarEditarPermiso" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var eliminarPermiso				= '<s:url action="eliminarPermiso" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var nuevoPermiso				= '<s:url action="nuevoPermiso" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	
</script>
<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}

</style>

<s:form  id="ordenCompraPersmisos">

	<div id="altaPermiso">
		<s:include value="/jsp/siniestros/valuacion/ordencompra/include_alta_permiso.jsp"></s:include>
	</div>
	
	<br/><br/>

	<div id="indicadorPermisoUsuario"></div>
	<div id="permisoUsuarioGrid" style="width:98%;height:340px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>


</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
    initGridPermisosUsuario();
    validateHabilitarMontos();
</script>