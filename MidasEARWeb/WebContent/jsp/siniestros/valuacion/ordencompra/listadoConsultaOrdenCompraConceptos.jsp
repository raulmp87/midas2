<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin"> 
				<param>bricks</param>
			</call>   
        </beforeInit>       
		<column id="coberturaDescripcion" type="ro" width="200" sort="str"  align="center" >Cobertura </column>
		<column id="conceptoPago" type="ro" width="200" sort="str" align="center">Concepto de Pago</column>
		<column id="costoUnitario"   type="ron" width="110" format="$0,000.00" sort="int">Costo/Unitario</column>	
		<column id="porcIva" type="ro" width="50" sort="str" align="center" >% IVA</column>	
		<column id="iva"   type="ron" width="110" format="$0,000.00" sort="int">IVA</column>
		
		<column id="porcIvaRetenido" type="ro" width="50" sort="str" align="center" >% IVA Retenido</column>	
		<column id="ivaRetenido"   type="ron" width="110" format="$0,000.00" sort="int">IVA Retenido</column>	
		<column id="porcIsr" type="ro" width="50" sort="str" align="center" >% ISR</column>	
		<column id="isr"   type="ron" width="110" format="$0,000.00" sort="int">ISR</column>	
		
		
		<column id="importe"   type="ron" width="110" format="$0,000.00" sort="int">Importe</column>	
		<column id="importePagado"   type="ron" width="110" format="$0,000.00" sort="int">Pagado</column>	
		<column id="pendiente"   type="ron" width="110" format="$0,000.00" sort="int">Pendiente</column>
		<column id="observaciones" type="ro" width="*" sort="str" hidden="true" >observaciones</column>	
		<column id="idconepto" type="ro" width="*" sort="str" hidden="true" >idconcepto</column>
		<column id="id" type="ro" width="110" sort="str"  hidden="true" >id</column>	
		<column id="estatus" type="img" width="35" align="center" ></column>

	</head>
	
	<s:iterator value="listaDetalleDTO" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="coberturaDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="conceptoPago" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="costoUnitario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="porcIva" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="iva" escapeHtml="true" escapeXml="true"/></cell>	
			
			<cell><s:property value="porcIvaRetenido" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="ivaRetenido" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="porcIsr" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="isr" escapeHtml="true" escapeXml="true"/></cell>
			
			<cell><s:property value="importe" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="importePagado" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="pendiente" escapeHtml="true" escapeXml="true"/></cell>		
			<cell><s:property value="observaciones" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="conceptoAjuste.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus == 0">
				<cell>/MidasWeb/img/ico_yel.gif</cell>	
			</s:if>
			<s:if test="estatus == 1">
				<cell>/MidasWeb/img/ico_green.gif</cell>	
			</s:if>
			<s:if test="estatus == 2">
				<cell>/MidasWeb/img/ico_red.gif</cell>	
			</s:if>

		</row>
	</s:iterator>
	
</rows>