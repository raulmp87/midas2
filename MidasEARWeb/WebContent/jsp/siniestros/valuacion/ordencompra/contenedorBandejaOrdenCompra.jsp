<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/autorizacionOrdenesCompra.js'/>"></script>

<script type="text/javascript">
	var listadoOrdenesPorAutorizar 		= '<s:url action="listadoOrdenesPorAutorizar" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
	var consultarDetalleOrdenCompra 	= '<s:url action="consultarDetalleOrdenCompra" namespace="/siniestros/valuacion/ordencompra"/>';

</script>


<s:form  id="ordenCompraAutorizarForm">
<div class="titulo" style="width: 98%;">
	<s:text name="Listado de Ordenes de Compra por Autorizar"/>	
</div>

<br/>


	<div id="ordenesCompraPorAutorizar" style="width:98%;height:380px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>

</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
initGridsOrdenesCompraAutorizar();
</script>