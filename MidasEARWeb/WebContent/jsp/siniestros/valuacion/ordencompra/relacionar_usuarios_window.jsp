<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">	

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

        
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script> 


 

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/autorizacionOrdenesCompra.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

</style>


<script type="text/javascript">
var obtenerUsuariosDisponibles 		= '<s:url action="obtenerUsuariosDisponibles" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
var obtenerUsuariosRelacionados 	= '<s:url action="obtenerUsuariosRelacionados" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';
var accionRelacionarUsuarios 		= '<s:url action="accionRelacionarUsuarios" namespace="/siniestros/valuacion/ordencompraautorizacion"/>';

</script>

<s:form id="relacionUsuariosForm" >

<s:hidden name="ordenCompraPermisoId"  id="ordenCompraPermisoId"/>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.pasesAtencion.title"/>	
</div>	
<br/>
	<table>
	
		<tr>
			<td><div class="titulo" style="width: 98%;">
					DISPONIBLES	
				</div>	</td>
			<td></td>
			<td><div class="titulo" style="width: 98%;">
					ASOCIADOS	
				</div>	</td>
		</tr>
		
		<tr>
				<td>
					<div id="relacionUsuariosDisponiblesGrid" style="width: 420px; height: 320px;"></div>
				</td>
				
		
				<td align="center" valign="middle" width="8%">
								
				</td >
		
				<td>
					<div id="relacionUsuariosAsociadosGrid" style="width: 420px; height: 320px;float:right;"></div>
				</td>
			</tr>
	</table>
	<br/>
<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarPopUp();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>	
			
		</td>							
	</tr>
</table>		


</s:form>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script>
initGridsRelacionUsuarios();
</script>
