<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>

<script type="text/javascript">

	var regresarReporteCabina = '<s:url action="mostrarBuscarReporte" namespace="/siniestros/cabina/reportecabina"/>';
	
</script>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<s:form id="ordenCompraForm" class="floatLeft">

<div id="contenido_OrdenCompra" style="width:99%;position: relative;">
	<s:hidden id="idToReporte" name="idReporteCabina" />
	
	<s:hidden id="idCobertura" name="idCobertura" />
	<s:hidden name="validOnMillis" id="h_validOnMillis"></s:hidden>
	<s:hidden name="recordFromMillis" id="h_recordFromMillis"></s:hidden>
	<s:hidden name="tipo" id="tipoOrdenCompra"></s:hidden>
	<s:hidden name="origen" id="origen"></s:hidden>
	<s:hidden name="modoConsulta" id="modoConsulta"></s:hidden>
	
		<div class="titulo" align="left">
			<s:text name="tituloOrdenCompra" />
		</div>
	<div id="divInferior" style="width: 1142px !important;" class="floatLeft">
		<div id="indicador"></div>
			<div id="comprasGrid" style="width: 98%;height:460px"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>

			
		</div>
		<div class="divInterno"	style="float: right; position: relative; width: 100%; margin-top: 1%;height: 40px;">
			<s:if test="modoConsulta !=true     ">
				
				
				<s:if test="tipo == \"GA\" || tipo == \"RGA\"">
				
						<div id="nuevo" class="btn_back w150"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="nuevaGA();"> <s:text
									name="Gasto de Ajuste" /> </a>
						</div>
						<div id="nuevo" class="btn_back w190"
							style="display: inline; float: left;margin-left: 1%;">
							<a href="javascript: void(0);" onclick="nuevaReembolsoGA();"> <s:text
									name="Reembolso Gasto Ajuste" /> </a>
						</div>
				</s:if>	
				<s:else>
					<div id="nuevo" class="btn_back w80"
						style="display: inline; float: left;margin-left: 1%;">
						<a href="javascript: void(0);" onclick="nuevaOrden();"> <s:text
								name="midas.boton.nuevo" /> </a>
					</div>
				
				</s:else>
				
				
				
		    </s:if>	
				
				<div class="btn_back w80"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="cerrar();">
						<s:text name="midas.boton.cerrar" /> </a>
				</div>
				
				<div class="btn_back w155"
					style="display: inline; margin-left: 1%; float: left;">
					<a href="javascript: void(0);" onclick="consultarBandejaOrdenesCompra();">
						<s:text name="Bandeja Ordenes Compra" /> </a>
				</div>
				
			</div>
		
		
		
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		cargaGrid();
	});
	</script>
	
</div>
	</s:form>
