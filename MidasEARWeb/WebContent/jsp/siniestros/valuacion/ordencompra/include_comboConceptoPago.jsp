<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<s:if test="!soloConsulta">	
	<s:select list="conceptoPagoMap" name="ordenCompraPermiso.concepto.id"  label="Concepto" labelposition="left"  cssClass="cajaTexto w120 alphaextra consulta requerido" headerKey="" headerValue="Seleccione ..."></s:select>
</s:if>
<s:if test="soloConsulta">
	<s:textfield id="concepto"  name="ordenCompraPermiso.concepto.nombre"
					cssClass="cajaTexto w150 alphaextra consulta"  label="Concepto" labelposition="left"   ></s:textfield>
</s:if>	