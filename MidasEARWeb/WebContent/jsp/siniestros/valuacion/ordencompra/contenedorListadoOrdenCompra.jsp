<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>





<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/bandejaOrdenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<script type="text/javascript">

 
</script>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 40px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel {
	float: left;
	position: relative;
	margin-top: 12px;
}

.divInfDivInterno {
	width: 17%;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<div id="contenido_moduloOrdenCompra"   style="width: 99%; position: relative;">
	<s:form id="bandejaOrdenCompraForm" class="floatLeft">
		<s:hidden id="reporteCabinaId" name="reporteCabinaId" />
		<s:hidden id="idCobertura" name="idCobertura" />

		<div class="titulo" align="left">
			<s:text name="Busqueda Ordenes de Compra" />
		</div>
		<div id="divInferior" style="width: 1142px !important;"
			class="floatLeft">

			<div id="contenedorFiltrosPagos" class="divContenedorO"
				style="width: 99%;background-color: #EDFAE1;">
				<div class="divInterno">

					<div class="floatLeft" style="width: 30%;">
						<div class="floatLeft" style="width: 35%;">
							<s:textfield id="nsOficinaS" maxlength="5"
								name="filtroBandeja.claveOficinaSiniestro" cssStyle="width:35%;"								
								cssClass="txtfield" label="No Siniestro" labelposition="left"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsConsecutivoRS" maxlength="15"
								name="filtroBandeja.consecutivoReporteSiniestro"
								cssStyle="width: 99%;"
								cssClass="txtfield"></s:textfield>

						</div>

						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsAnioS" name="filtroBandeja.anioReporteSiniestro"
								cssStyle="width: 99%;" maxlength="4"
								cssClass="txtfield"></s:textfield>
						</div>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="oficinas" id="ofinasActivas"
							name="filtroBandeja.oficinaId" label="Oficinas"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:80%;">
						</s:select>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="estautsMap" id="estautsId"
							name="filtroBandeja.estatus" label="Estatus" labelposition="left"
							cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:85%;">
						</s:select>
						
						
						
						
					</div>
				</div>
				<div class="divInterno">
					<div class="floatLeft" style="width: 30%;">
						<div class="floatLeft" style="width: 35%;">
							<s:textfield id="nsOficinaR" maxlength="5"
								name="filtroBandeja.claveReporte" cssStyle="width:35%;"								
								cssClass="txtfield" label="No Reporte" labelposition="left"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsConsecutivoR" maxlength="15"
								name="filtroBandeja.conseReporte"
								cssStyle="width: 99%;"
								cssClass="txtfield"></s:textfield>

						</div>
						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsAnioR" name="filtroBandeja.anioReporte"
								cssStyle="width: 99%;" maxlength="4"
								cssClass="txtfield"></s:textfield>
						</div>
					</div>
					
					<div class="floatLeft" style="width: 30%;">
						<s:select list="tipoOrdenCompraMap" id="tipoOrden"
							name="filtroBandeja.tipoOrdenPago" label="Tipo"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:86%;">
						</s:select>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="tipoPagoMap" id="tipoPago"
							name="filtroBandeja.tipoPago" label="Tipo Pago"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:81%;">
						</s:select>
					</div>
					
				</div>

				<div class="divInterno" >
					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="beneficiario" name="filtroBandeja.beneficiario"
							cssClass="txtfield" label="Beneficiario" labelposition="left"
							cssStyle="width:180px;" ></s:textfield>
					</div>			
					
					<div class="floatLeft" style="width: 30%;">
						<s:select id="tipoProveedorMap" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="filtroBandeja.tipoProveedor"
									headerKey="" headerValue="%{getText('Seleccione Tipo... ')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield" 
							  		cssStyle="width:70%;"
							  		onchange="changeTipoPrestador()"
							  	 />
					</div>					
					<div class="floatLeft" style="width: 30%;">
							<s:select id="proveedorLis" 
									labelposition="left" 
									label="Proveedor"
									name="filtroBandeja.idBeneficiario"
									headerKey="" headerValue="%{getText('Seleccione Proveedor... ')}"
							  		list="proveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield" 
							  		cssStyle="width:70%;"  
							  	 />
					</div>	

				</div>
			<div class="divInterno">
					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="numOrdenCompra" name="filtroBandeja.numOrdenCompra"
							onkeypress="buscarListaEnter(event);return soloNumeros(this, event, true);"	
							cssClass="txtfield" label="Orden de Compra" labelposition="left"
							cssStyle="width:80px;"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 30%;">
						<s:select list="terminoAjusteMap" id="terminoAjusteLis"
							name="filtroBandeja.terminoAjuste" label="Termino de Ajuste"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:220px;">
						</s:select>
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 11%;" >
							 <s:checkbox name="filtroBandeja.servPublico"  id="servPublico"  label="Servicio Publico" labelposition="right"> </s:checkbox>
					</div>
					<div class="floatLeft divInfDivInterno" style="width: 19%;" >
							 <s:checkbox name="filtroBandeja.servParticular"  id="servParticular"  label="Servicio Particular" labelposition="right"> </s:checkbox>
					</div>
					

					
					
				</div>				
	
				<div class="divInterno" style="padding-right: 200px">
					<div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);"
							onclick="limpiarContenedor();"> <s:text
								name="midas.boton.limpiar" /> </a>
					</div>
					<div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="buscarLista();">
							<s:text name="midas.boton.buscar" /> </a>
					</div>
				</div>
			</div>

			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="Lista de Ordenes de Compra " />
			</div>
		    <div id="ordenesCGrid" class="dataGridConfigurationClass"
			style="width: 99%; height: 270px;"></div>			
			<div id="pagingArea" style="padding-top: 8px "></div>
			<div id="infoArea"></div>
			<div class="divInterno" >
					
					<div class="btn_back w180"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="busquedaReportes();">
							<s:text name="Busqueda de Reportes" /> </a>
					</div>
					
					
			</div>
		 
		</div>
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</s:form>
	<script>
	/*	jQuery(document).ready(function() {
			iniciarLista();
		});*/
	</script>

</div>
