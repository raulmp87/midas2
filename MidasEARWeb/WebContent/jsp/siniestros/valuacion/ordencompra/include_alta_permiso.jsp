<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:hidden name="ordenCompraPermiso.id"  id="ordenCompraPermisoId"/>
<s:hidden name="soloConsulta"  id="soloConsulta"/>

<table  id="filtrosM2" width="98%">
		<tr>
			<td class="titulo" colspan="7"><s:text name="midas.siniestros.validacion.ordencompra.permisos.titulo" /></td>
		</tr>
		
		<tr>
			
			
			<td>  
				<s:select list="oficinasMap" name="ordenCompraPermiso.oficina.id" label="Oficina" labelposition="left"  cssClass="cajaTexto w120 alphaextra consulta requerido" headerKey="" headerValue="%{getText('midas.general.seleccione')}"></s:select>
			</td>
			
			<td colspan="3">
				<s:radio list="tipoOrdenCompraMap" value="tipoPago"  onchange="validateTipoOrdenDeCompra();" id="tipoPago" name="ordenCompraPermiso.tipo" cssClass="consulta requerido"  value="%{ordenCompraPermiso.tipo}" />
				<s:if test="soloConsulta">
				<s:hidden name="ordenCompraPermiso.tipo"  id="tipoOrdenCompra"/>
				</s:if>
			</td>
			
			
			<td> 
			<s:if test="!soloConsulta">
				<s:select list="lineaNegocioMap" name="ordenCompraPermiso.coberturaSeccion.id.idtoseccion" id="lineaNegocioId" label="Linea de Negocio" labelposition="left"  cssClass="cajaTexto w120 alphaextra consulta requerido" headerKey="" headerValue="Seleccione ..."
					onchange="obtenerCoberturas(this);"></s:select>
			</s:if>
			<s:if test="soloConsulta">		
				<s:textfield id="lineaNegocio"  name="ordenCompraPermiso.coberturaSeccion.seccionDTO.descripcion"
					cssClass="cajaTexto w150 alphaextra consulta"  label="Linea de Negocio" labelposition="left" ></s:textfield>
			</s:if>
			</td>
			
			
			
			<td id="comboCoberturas">  
				<s:include value="/jsp/siniestros/valuacion/ordencompra/include_comboCoberturas.jsp"></s:include>
			</td>
			
			
			
			<td id="comboConceptos">
				<s:include value="/jsp/siniestros/valuacion/ordencompra/include_comboConceptoPago.jsp"></s:include>
			</td>
				
		</tr>
		
		<tr>
	
			
			<td>  
				<s:select list="estautsMap" name="ordenCompraPermiso.estatus" label="Estatus" labelposition="left"  cssClass="cajaTexto w120 alphaextra requerido" headerKey="" headerValue="Seleccione ..."></s:select>
			</td>
			
			<th> 
				<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.inclusionAnexo.nivel" />  <s:text name="midas.suscripcion.cotizacion.auto.complementarEmision.textosAdicional.autorizacion" />:
			</th>
			
			<td>  
				<s:select list="nivelMap" name="ordenCompraPermiso.nivel"  cssClass="cajaTexto w120 alphaextra requerido" headerKey="" headerValue="Seleccione ..."></s:select>
			</td>
			
			
			<td> <s:checkbox  id="validarMontos" name="ordenCompraPermiso.validarMontos" onchange="validateHabilitarMontos();" > </s:checkbox> </td>	
			<th >Habilitar Montos </th>
			
			<td>
				<s:textfield id="montoMin"  name="ordenCompraPermiso.montoMin"
					cssClass="floatLeft setNew txtfield" onkeypress="return soloNumeros(this, event, true)" label="Monto Minimo" 
					labelposition="left"  maxlength="15"></s:textfield>
			</td>
			
			<td>
				<s:textfield id="montoMax"  name="ordenCompraPermiso.montoMax"
					cssClass="floatLeft setNew txtfield" onkeypress="return soloNumeros(this, event, true)" label="Monto Maximo" 
					labelposition="left" maxlength="15"></s:textfield>
			</td>
			
			
		</tr>
		
		<tr>
			<td colspan="7">
			<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="guardar();"> 
								<s:text name="midas.boton.guardar" /> 
							</a>
						</div>
				
						<div id="nuevo" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);" onclick="nuevo();"> <s:text
										name="midas.boton.nuevo" /> 
								</a>
						</div>
					</td>							
				</tr>
			</table>	
			
			
				
				
			</td>
		</tr>
		


	</table>