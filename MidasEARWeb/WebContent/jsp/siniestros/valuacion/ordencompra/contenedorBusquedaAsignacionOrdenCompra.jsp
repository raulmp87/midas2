<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>



<sj:head/>


<style type="text/css">
	#superior{
		height: 200px;
		width: 99%;
	}

	#superiorI{
		height: 150px;
		width: 850px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		width: 850px;
		float: left;
		margin-left: 70%;
	}
	
	
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	label{
		font-family: Verdana,Arial,Helvetica,sans-serif; font-size:   9.33333px; text-align:  left
	}

</style>
 
<s:hidden id="idReporteCabinaH" name="idReporteCabina"/>
<s:hidden id="tipoH" name="tipo"/>
<s:hidden id="cadenaCoberturasH" name="cadenaCoberturas"/>
<s:hidden id="cadenaPaseAtencionH" name="cadenaPaseAtencion"/>
<s:hidden id="fieldNameH" name="fieldName"/>

<form id="formBuscarOrdenCOompraDTO">
<div id="contenido_listadoOrdenCompra" style="margin-left: 2% !important;height: 10px;">
	
	

	
<div class="titulo" style="width: 98%;">
	<s:text  name="tituloOrdenCompra"/>
	
</div>	
	
	
</div>

<br/>

	<div id="indicador"></div>
	<div id="inferior">
		<div id="comprasBusquedaGrid" style="width:95%;height:300px;margin-left: 2%">
		</div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
	
	<div id="superiorD" >
				<div>
					<div class="btn_back w100" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="asignarOrdenesCompra()"> 
							<s:text	name="Asignar"/> 
						</a>
					</div>
					<div class="btn_back w100" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="cerrarVentanaBuscarOC()"> 
							<s:text	name="Cerrar"/> 
						</a>
					</div>
				</div>
			</div>

<script type="text/javascript">
	cargaGridBusquedaOrdenesCompra();
</script>
</form>
































