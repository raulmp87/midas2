<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	var mostrarDatosIncisoPoliza 	= '<s:url action="mostrarDatosIncisoPoliza" namespace="/siniestros/cabina/reportecabina"/>';
	var autorizarOrdenCompra 		= '<s:url action="autorizarOrdenCompra" namespace="/siniestros/valuacion/ordencompra"/>';
	var rechazarOrdenCompra 		= '<s:url action="rechazarOrdenCompra" namespace="/siniestros/valuacion/ordencompra"/>';
	var regresarListaIndemnizacionPath	= '<s:url action="mostrarCatalogo" namespace="/siniestros/indemnizacion/perdidatotal"/>';
</script>
<s:form id="definirOrdenForm" class="floatLeft">
<div id="contenido_DefinirOrden" style="width:99%;position: relative;">
		<s:hidden id="idOrdenCompra_h" name="idOrdenCompra" />	
		<s:hidden id="idReporteCabina" name="idReporteCabina" />
		<s:hidden id="soloLectura" name="soloLectura" />
		<s:hidden id="soloIndemnizacion" name="soloIndemnizacion" />
		<s:hidden id="origen" name="ordenCompra.origen" />
		<s:hidden id="codigoUsuarioCreacion" name="ordenCompra.codigoUsuarioCreacion" />
		<s:hidden id="idCoberturaReporteCabina" name="idCoberturaReporteCabina" />
		<s:hidden id="cveSubTipoCalculoCobertura" name="cveSubTipoCalculoCobertura" />
		<s:hidden id="bandejaPorAutorizar" name="bandejaPorAutorizar" />
		<s:hidden id="pantallaOrigen" name="pantallaOrigen" />
		<s:hidden id="formaPago" name="formaPago" />
		<s:hidden id="idIndemnizacion" name="idIndemnizacion" />
		<s:hidden id="oc_aplicaDeducible" name="ordenCompra.aplicaDeducible" />
		
	  <div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="midas.servicio.siniestros.valuacion.ordencompra.tituloDeta" />
			</div>
		 
			<div id="divGenerales" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 100%; height: 150px;">
						
						<div class="divFormulario">
							<div id="divOC" class="floatLeft divInfDivInterno" style="width: 10%;" >
								<s:textfield id="idOrdencompra" name="ordenCompra.id"   cssClass="txtfield"  label="Folio" labelposition="left" cssStyle="width:50px;" readonly="true" ></s:textfield>
							</div>	
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 20%; " >
								   <s:textfield id="estatusTxt" name="ordenCompra.estatus"   cssClass="txtfield"  label="Estatus" labelposition="left" cssStyle="width:100px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numReporte" name="numReporte"   cssClass="txtfield"  label="No Reporte" labelposition="left" cssStyle="width:190px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numSiniestro" name="numSiniestro"   cssClass="txtfield"  label="No Siniestro" labelposition="left" cssStyle="width:215px;" readonly="true" ></s:textfield>
							</div>
						</div>
						
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="creadoPor" name="creadoPor" cssClass="txtfield" readonly="true" label="Creado" labelposition="left" cssStyle="width:190px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="modificadoPor" name="modificadoPor" cssClass="txtfield" readonly="true" label="Modificado" labelposition="left" cssStyle="width:190px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tipoPagoMap" id="tipoPagoLis"
									name="ordenCompra.tipoPago" label="Tipo de Pago"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
									onchange="changeTipoPago()"
									
									>
									</s:select>							
							</div>
							
							
						</div>
						<div class="divFormulario">
						
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="terminoAjuste" name="terminoAjuste" cssClass="txtfield" readonly="true" label="Término de Ajuste." labelposition="left" cssStyle="width:195px;" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="terminoSiniestro" name="terminoSiniestro" cssClass="txtfield" readonly="true" label="Término de Siniestro." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>	
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tipoOrdenCompraMap" id="tipoOrdenCompraLis"
									name="ordenCompra.tipo" label="Tipo Orden de Compra"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:50%;"	
									onchange="changeTipoOrden()"	
									disabled="soloLectura.value"							
									>
									</s:select>	
									 					
							</div>
							<div id="divcoberura1" class="floatLeft divInfDivInterno" style="width: 30%; " >
									<s:select list="coberturaMap" id="coberturaLis"
									name="coberturaCompuesta" label="Cobertura"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
									onchange="changeCobertura();"
								
									>
									</s:select>							
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 40%;" >
								<s:select id="terceroAfectadoLis" 
										labelposition="left" 
										label="Tercero Afectado"	
										name="ordenCompra.idTercero"									
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="terceroAfectadoMap" listKey="key" listValue="value"  
								  		cssClass="txtfield cajaTextoM2 w250"   />
							</div>
						</div>
						
				
						
						
					</div>
			</div>
			<div class="titulo" id="tituloPago" align="left" >
				<s:text   name="Datos del Pago"/>
			</div>
			
			<div id="divGenerales" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltrosCompras" class="divContenedorO" style="width: 100%; height:130px;">
						
						<div class="divFormulario">
								<div id="divNomBenef"  class="floatLeft divInfDivInterno" style="width: 30%;" >
										<s:textfield id="nomBeneficiario" name="ordenCompra.nomBeneficiario"   cssClass="txtfield"  label="Beneficiario." labelposition="left" cssStyle="width:150px;"></s:textfield>
								</div>
						
								<div id="divProvedor1" class="floatLeft divInfDivInterno" style="width: 30%; 	" >
									<s:select id="tipoProveedorLis" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="ordenCompra.tipoProveedor"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160"   
							  		onchange="changeTipoPrestador()"
							  	 />
								</div>	
								
								<div id="divProvedorNom1"   class="floatLeft divInfDivInterno" style="width: 30%; " >
										<s:select id="proveedorLis" 
										labelposition="left" 
										label="Beneficiario"	
										name="ordenCompra.idBeneficiario"									
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="proveedorMap" listKey="key" listValue="value"  
								  		onchange="changeProveedor()"
								  		cssClass="txtfield cajaTextoM2 w160"   
								  		/>
								</div>
								<div class="floatLeft divInfDivInterno" id="divdatosBeneficiario" style="width: 30%;" >
									<s:select list="tipoPersona" id="tipoPersonaLis"
									name="ordenCompra.tipoPersona" label="Tipo Persona"
									labelposition="left"
									cssClass="txtfield" 
									cssStyle="width:70%;"
									
									>
									</s:select>							
								</div>
								
								<div class="floatLeft divInfDivInterno" style="width: 40%;" >
									<s:select id="s_aplDeducible"  
										list="#{'true':'SI','false':'NO'}"
										headerKey="null" headerValue="SELECCIONE..."
										value="ordenCompra.aplicaDeducible" 
										label="Aplica Deducible" labelposition="left"
										cssClass="txtfield" cssStyle="width:50%;"
										disabled="true"	/>
								</div>
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="curp" name="ordenCompra.curp"   cssClass="txtfield"  label="C.U.R.P." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>	
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="rfc" name="ordenCompra.rfc"   cssClass="txtfield"  label="R.F.C." labelposition="left" cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="factura" name="ordenCompra.factura"   cssClass="txtfield"  label="Factura." labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
							
						</div>
						<div class="divFormulario"  >
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:select id="bancosLis" 
										labelposition="left" 
										label="Banco Receptor "	
										name="ordenCompra.bancoId"
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="bancos" cssClass="txtfield cajaTextoM2 w160"   
								/>
							</div>
								
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="clabe" name="ordenCompra.clabe"   cssClass="txtfield"  label="CLABE." labelposition="left"  onkeypress="return soloNumeros(this, event, true);"  cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="correo" name="ordenCompra.correo"   cssClass="txtfield"  label="Correo." labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 11%;" >
								<s:textfield id="lada" name="ordenCompra.lada"   cssClass="txtfield"  label="Telefono" labelposition="left" onkeypress="return soloNumeros(this, event, true);" maxlength="3" cssStyle="width:50px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 9%;" >
								<s:textfield id="telefono" name="ordenCompra.telefono"   cssClass="txtfield"   onkeypress="return soloNumeros(this, event, true);" maxlength="8"  cssStyle="width:80px;"  ></s:textfield>
							</div>
							
						</div>
			</div>
			
		</div>
		
		<div id="altaConcepto" style="width: 1050px !important;" class="floatLeft">
			
			<div class="titulo" align="left" >
				<s:text name="midas.servicio.siniestros.valuacion.ordencompra.conceptos" />
			</div>
		 
			<div id="divGeneralesdeta" style="width: 1050px;  class="floatLeft">
					<div id="contenedorDetasCompras" class="divContenedorO" style="width: 100%; height: 135px;">
						
						<div class="divFormulario">
							<div id="conceptosdiv" class="floatLeft divInfDivInterno" style="width: 40%; " >
									<s:select list="conceptoPagoMap" id="conceptoLis"
									name="idConcepto" label="Concepto de Pago"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
							
									>
									</s:select>	
							</div>
						</div>
						
						<div class="divFormulario">
								<div class="floatLeft divInfDivInterno" style="width: 50%;" >
										<s:textarea cssStyle="width:80%;height:90px;" id="observaciones"
										label="Observaciones" labelposition="left"
										rows="10" name="detalleOrdenCompra.observaciones" 
										 maxlength="150"
										cssClass="textarea" />							
								</div>	
							</div>
					</div>
			</div>
		</div>
		<!-- TITULO  -->
			<div id="divInferior" style="width: 1050px !important; " class="floatLeft">
				<div class="titulo" align="left" >
				<s:text name="midas.servicio.siniestros.valuacion.ordencompra.listadoConceptos" />
				</div>
			 	<div id="conceptosGrid" class="dataGridConfigurationClass"
				style="width: 99%; height: 70px; padding-top: 10px"></div>
				<div id="pagingArea" style="padding-top: 8px "></div>
				<div id="infoArea"></div>			
			</div>
	
	 <div id="divInferior" style="width: 1050px !important;" class="floatLeft">
	 		<div class="floatLeft"
				style="height: 15px; width: 600px; ">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.valuacion.ordencompra.importes" />		
				</div>
			</div>
			<div class="floatLeft"
				style="height: 15px; width: 440px;  padding-left: 10px">
				
				<div class="titulo" align="left">
					<s:text name="midas.servicio.siniestros.valuacion.ordencompra.totales" />
				</div>
			</div>
		</div>
	<div id="divInferior" style="width: 100% !important; height: 100px; padding-top: 8px " class="floatLeft">
			<div class="floatLeft"
				style="height: 70px; width: 600px; ">
				
				<div class="divContenedorO"
					style="height: 60px; width: 100%; margin-bottom: 7px;">
					
					<div class="divFormulario" >
						<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_subtotal"  name="importesDTO.subtotal"    cssClass="txtfield  setNew formatCurrency"  label="Sub Total" labelposition="left" cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="Text_Deducible" name="ordenCompra.deducible"    cssClass="txtfield  setNew formatCurrency"  label="Deducible" labelposition="left" cssStyle="width:80px;" disabled="true" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 26%;" >
								<s:textfield id="Text_Descuento"   name="ordenCompra.descuento"   cssClass="txtfield  setNew formatCurrency"  label="Descuento" labelposition="left" cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 24%;" >
								<s:textfield id="Text_total"  name="importesDTO.total"    cssClass="txtfield  setNew formatCurrency"   label="Total" labelposition="left" cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
						
					</div>
				</div>
				
			</div>	
			<div class="floatLeft"
				style="height: 70px; width: 440px; padding-left: 10px ">
				
				<div class="divContenedorO"
					style="height: 60px; width: 100%; margin-bottom: 7px;">
					
					<div class="divFormulario" >
						
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:textfield id="text_Tpagado"  name="importesDTO.totalPagado" cssClass="txtfield  setNew formatCurrency"   label="Total Pagado" labelposition="left"  cssStyle="width:80px;"  readonly="true" ></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 50%;" >
								<s:textfield id="text_Tpendiente"  name="importesDTO.totalPendiente"   cssClass="txtfield  setNew formatCurrency"    label="Total Pendiente" labelposition="left"  cssStyle="width:80px;" readonly="true" ></s:textfield>
						</div>
						
					</div>
				</div>
				
			</div>	
		</div>		
				<div class="divInterno"	style="float: right; position: relative; width: 100%; margin-top: 1%;height: 40px;  padding-right: 125px ">
				
				<s:if test="%{  (pantallaOrigen=='INDEMNIZACION' ) }">	
					
					<div class="btn_back w100"
					style="display: inline; margin-left: 2%; float: right;">
					<a href="javascript: void(0);" onclick="cerrarOCIndemnizacion();">
						<s:text name="Regresar" /> </a>
					</div> 
				</s:if>
				<s:else>
		      		<div class="btn_back w100"
					style="display: inline; margin-left: 2%; float: right;">
					<a href="javascript: void(0);" onclick="cerrarDetalle();">
						<s:text name="midas.boton.cerrar" /> </a>
					</div>
		   		</s:else>
				
				<s:if test="bandejaPorAutorizar">	
					<s:if test="ordenCompra.estatus == 'Tramite'">	
						
						<div class="btn_back w100" id="btn_rechazar" style="display: inline;margin-left: 2%;float: right;">
						<a href="javascript: void(0);"  onclick="rechazar();"> 
							<s:text	name="Rechazar"/> 
						</a>
						</div>
						<div class="btn_back w100" id="btn_autorizar" style="display: inline; margin-left: 2%; float: right;">
							<a href="javascript: void(0);" onclick="autorizar();"> <s:text
									name="Autorizar" /> </a>
						</div>
						
					</s:if>
				</s:if>
			
				
				<s:if test="%{  (ordenCompra.estatus=='Tramite' || ordenCompra.estatus=='Autorizada')   &&   (soloIndemnizacion==true) }">	
					<div  id="cancelarOrden" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
					<a href="javascript: void(0);"  onclick="cancelacionrOrden();"> 
						<s:text	name="Cancelar"/> 
					</a>
					</div> 
				</s:if>
				
				<s:if test="%{ordenCompra.estatus=='Tramite' || ordenCompra.estatus=='Indemnizacion' }">	
				<div  id="guardarBtn" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
				<a href="javascript: void(0);"  onclick="guardarOrdenCompraIndemnizacion();"> 
					<s:text	name="midas.boton.guardar"/> 
				</a>
				</div>
				</s:if>
				
				<s:if test="%{ (ordenCompra.estatus=='Tramite' || ordenCompra.estatus=='Cancelado' || ordenCompra.estatus=='Pagado'|| ordenCompra.estatus=='Autorizada') && (soloIndemnizacion==true) }">	
					<div  id="verBtn" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
						<a href="javascript: void(0);"  onclick="imprimir();"> 
							<s:text	name="ver"/> 
						</a>
					</div> 
					<div id="imprimirBtn" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
						<a href="javascript: void(0);"  onclick="imprimir();"> 
							<s:text	name="Imprimir"/> 
						</a>
					</div>
				</s:if>
				
				
			</div>
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		iniContenedoOrdenCompraIndemnizacion();
		validateIsFromBandeja();
		initCurrencyFormatOnTxtInput();
	});
	</script>
	
</div>
	</s:form>
