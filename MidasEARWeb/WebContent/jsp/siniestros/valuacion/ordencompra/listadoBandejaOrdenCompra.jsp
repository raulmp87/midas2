<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>"> 
  	<s:set var="permisosFactura" value="false" />				
	<m:tienePermiso nombre="FN_M2_SN_REGISTRO_FACTURAS">		
		<s:set var="permisosFactura" value="true" />
	</m:tienePermiso>
  	
  	   
   <s:iterator value="listaBandejaOrdenCompra" status="row">
		<row id="<s:property value="#row.index"/>">
			<s:if test="dias >60 && ( estatus=='Tramite'   ||  estatus=='Autorizada' ||  estatus=='Asociada'  )">	
				<cell style="color:red"><s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>			
			  	<cell style="color:red"><s:property value="numSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell style="color:red"><s:property value="numReporte" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red" ><s:property value="terminoAjuste" escapeHtml="false" escapeXml="true"/></cell>
				<cell style="color:red" ><s:property value="coberturaAfectada" escapeHtml="false" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="totalOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>					
				<cell style="color:red"><s:property value="tipoPago" escapeHtml="false" escapeXml="true"/></cell>		
				<cell style="color:red"><s:property value="tipoPagoOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>					
				<cell style="color:red"><s:property value="conceptoPago" escapeHtml="true" escapeXml="true"/></cell>	
				<cell style="color:red"><s:property  value="dias" escapeHtml="true" escapeXml="true"/>	</cell> 	
				<cell style="color:red"><s:property  value="id" escapeHtml="true" escapeXml="true"/>	</cell> 	
				<cell style="color:red"><s:property value="proveedor" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="beneficiario" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>				
				<cell style="color:red"><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="consultarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="editarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
			</s:if>
		  	<s:else>
		  		<cell><s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>		  	
			  	<cell><s:property value="numSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="numReporte" escapeHtml="true" escapeXml="true"/></cell>			  	
				<cell><s:property value="terminoAjuste" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="coberturaAfectada" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="totalOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>	
				<cell><s:property value="tipoPago" escapeHtml="false" escapeXml="true"/></cell>		
				<cell><s:property value="tipoPagoOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="conceptoPago" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property  value="dias" escapeHtml="true" escapeXml="true"/>	</cell> 
				<cell><s:property  value="id" escapeHtml="true" escapeXml="true"/>	</cell> 				
				<cell><s:property value="proveedor" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="beneficiario" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>				
				<cell><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="consultarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="editarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
		    </s:else>
		    <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarOrden(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		  
		    
		     <s:if test="estatus=='Tramite'">	
		     		  <cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarOrdenCompra(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		     
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>
		    </s:else>
		    

		    
		    

		    
		</row>
	</s:iterator>
	
</rows>
   
