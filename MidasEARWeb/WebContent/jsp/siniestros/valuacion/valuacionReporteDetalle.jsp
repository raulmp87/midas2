<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<%-- <script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script> --%>
<%-- <script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script> --%>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
</style>

<script type="text/javascript">
	var actualizarPiezaPath = '<s:url action="actualizarPieza" namespace="/siniestros/valuacion"/>';
	var guardarPath = '<s:url action="guardar" namespace="/siniestros/valuacion"/>';
	var relacionarPiezaPath = '<s:url action="relacionarPieza" namespace="/siniestros/valuacion"/>';
	var actualizarTotalesPath = '<s:url action="actualizarTotales" namespace="/siniestros/valuacion"/>';
	var imprimirValuacionPath = '<s:url action="imprimirValuacion" namespace="/siniestros/valuacion"/>';
</script>

<s:form id="informacionBusquedaValuacionesForm" >
		<s:hidden name="valuacionReporteFiltro.ajustador" id="valuacionReporteFiltro.ajustador"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.estatus" id="valuacionReporteFiltro.estatus"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.fechaFinal" id="valuacionReporteFiltro.fechaFinal"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.fechaInicial" id="valuacionReporteFiltro.fechaInicial"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroReporte" id="valuacionReporteFiltro.numeroReporte"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroSerie" id="valuacionReporteFiltro.numeroSerie"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.numeroValuacion" id="valuacionReporteFiltro.numeroValuacion"></s:hidden>
		<s:hidden name="valuacionReporteFiltro.valuador" id="valuacionReporteFiltro.valuador"></s:hidden>
</s:form>

<s:form id="valuacionDetalleForm" >
		<s:hidden id="idValuacionReporte" name="idValuacionReporte"/>
		<s:hidden id="entidadReporteId" name="entidad.reporte.id"/>
		<s:hidden id="entidadReporteNumero" name="entidad.reporte.numeroReporte"/>
		<s:hidden id="listaPiezasEliminar" name="stringListaPiezasEliminar"/>
		<s:hidden id="banderaEliminar" name="eliminarPiezas"/>
		<s:hidden id="nuevoValorCelda" name="nuevoValorCelda"/>
		<s:hidden id="columna" name="columna"/>
		<s:hidden id="idPiezaValuacion" name="idPiezaValuacion"/>
		<s:hidden id="estatusEntidad" name="entidad.estatus"/>
		<s:hidden id="esConsulta" name="esConsulta"/>
		<s:hidden id="origen" name="origen"/>
		
		
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.valuacion.informacionvaluacion"/>	
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.numvaluacion" /> </th>
					<td><s:textfield name="entidad.id"  cssClass="cajaTexto w300 alphaextra" readonly="true"  readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.numreporte" /> </th>
					<td><s:textfield name="entidad.reporte.numeroReporte" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.estatus" /> </th>
					<td><s:textfield name="entidadEstatusDesc" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.fechahora" /> </th>
					<td><s:textfield name="entidad.fechaValuacion" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.valuador" /> </th>
					<td colspan="3"><s:textfield name="entidad.codigoValuador"  cssClass="cajaTexto w300 alphaextra" readonly="true"  readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.ajustador" /> </th>
					<td colspan="3"><s:textfield name="entidad.ajustador.personaMidas.nombre" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
					
				</tr>
				
			</tbody>
		</table>
		</div>
		
		<br/>
		
		
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.valuacion.datosasegurado"/>	
		</div>
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.marcatipo" /> </th>
					<td><s:textfield name="incisoMarcaTipo"  cssClass="cajaTexto w350 alphaextra" readonly="true"  readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.modelo" /> </th>
					<td><s:textfield name="autoIncisoReporteCabina.modeloVehiculo" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.placas" /> </th>
					<td><s:textfield name="autoIncisoReporteCabina.placa" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
					
					<th><s:text name="midas.siniestros.valuacion.color" /> </th>
					<td><s:textfield name="autoIncisoReporteCabina.descColor" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.numserie" /> </th>
					<td colspan="7"><s:textfield name="autoIncisoReporteCabina.numeroSerie"  cssClass="cajaTexto w350 alphaextra" readonly="true"  readonly="true" ></s:textfield></td>
				</tr>
				
			</tbody>
		</table>
		</div>
	
		<br/>
		
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.valuacion.agregarpieza"/>	
		</div>
		<div id="contenedorFiltros" style="width: 98%;">
			<table id="agregar" border="0">
				
				<tr>
					<td colspan="4">
						<div id="btn_crearPieza" class="btn_back w160" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="mostrarAltaPieza();"> 
								<s:text name="midas.siniestros.valuacion.otrapieza" />
							</a>
						</div>	
						<div class="btn_back w160" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="mostrarVentanaImagenes();"> 
								<s:text name="midas.siniestros.valuacion.imagenes" />
							</a>
						</div>							
					</td>
				</tr>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.seccionauto" /></th>
					<td>
						<s:select list="seccionesAuto" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacionReporte.seccionAutoId" id="seccionAuto_s" cssClass="cajaTextoM2 w250 deshabilitar" onchange="onChangeSeccionAuto('pieza_s')"/>
					</td>
					
					<th><s:text name="midas.siniestros.valuacion.pieza" /></th>
					<td colspan="3">
						<s:select list="piezasPorSeccion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacionReporte.pieza.id" id="pieza_s" cssClass="cajaTextoM2 w250 deshabilitar" onchange=""/>
					</td>
				</tr>
				
				<tr>
					<th><s:text name="midas.siniestros.valuacion.tipoafectacion" /></th>
					<td>
						<s:select list="tiposAfectacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="piezaValuacionReporte.tipoAfectacion" id="tiposAfectacion_s" cssClass="cajaTextoM2 w250 deshabilitar" onchange=""/>
					</td>
					
					<td colspan="4">
						<div id="btn_agregarPieza" class="btn_back w160" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="agregarPieza();"> 
								<s:text name="midas.siniestros.valuacion.agregarlista" />
							</a>
						</div>
					</td>
				</tr>
				
			</table>
	
		</div>
		
		<br/>
		
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.valuacion.listado.piezas"/>		
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<td  colspan="2" width="98%">
						<br/>
						<div id="piezasValuacionGridContainer">
							<div id="piezasValuacionGrid" style="height:200px;"></div>
							<div id="pagingArea"></div><div id="infoArea"></div>
						</div>
						<br/>
					</td>
				</tr>
				<tr>
					<td colspan="2"  width="100%">
						<br/>
							<div id="btn_eliminarPiezas" class="btn_back w160" style="display: inline; float: right;">
											<a href="javascript: void(0);" onclick="eliminarPiezas();"> 
												<s:text name="midas.siniestros.valuacion.eliminarpiezas" />
											</a>
							</div>
						<br/>
					</td>
				</tr>
				<tr>
					<td width="65%">
						<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
							<tbody>
								<tr>
										<td><label class="labelBlack" ><s:text name="midas.siniestros.valuacion.observaciones" /></label></td>
								</tr>
								<tr>
									<td>
										<div id="contenedorTextArea">
											<s:textarea name="entidad.observaciones" id="observaciones_a" 
											cssClass="deshabilitar" cssStyle="height: 80%;width: 95%;font-size: 10pt;"/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td width="30%">
						<div id="totales">
							<s:include value="valuacionReporteDetalleTotales.jsp"/>
						</div>		
					</td>	
				</tr>
			</tbody>
		</table>
	</div>
</s:form>

<br/>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>
					<s:if test="entidad.estatus==1">
						<div id="btn_terminar" class="btn_back w140" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:terminarReporteValuacion();"> 
								<s:text name="midas.boton.terminar" />
								<img border='0px' alt='Terminar Reporte' title='midas.boton.consultarAsegurado' src='/MidasWeb/img/b_aceptar.gif'/> 
							</a>
						</div>
					</s:if>
					<div id="btn_guardar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:guardarReporteValuacion();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar Reporte' title='Guardar Reporte' src='/MidasWeb/img/common/btn_guardar.jpg'/>
						</a>
					</div>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:regresarABusqueda();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_borrar.gif'/>
						</a>
					</div>
					<s:if test="entidad.estatus==2">
						<div id="btn_imprimir" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="javascript:imprimirValuacion();"> 
								<s:text name="midas.boton.imprimir" />
								<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/common/b_imprimir.gif'/>
							</a>
						</div>
					</s:if>
				</td>
			</tr>
		</tbody>
	</table>

<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script src="<s:url value='/js/siniestros/valuacion/valuacionreporte.js'/>"></script>

<script type="text/javascript">
		initConfiguration();
		buscarPiezasValuacion();
		initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>