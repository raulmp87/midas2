<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<script type="text/javascript">
var mostrarBusquedaPath = '<s:url action="mostrar" namespace="/siniestros/valuacion"/>';
var exportarExcelPath = '<s:url action="exportarExcel" namespace="/siniestros/valuacion"/>';
var listarPath = '<s:url action="listar" namespace="/siniestros/valuacion"/>';
var obtenerPath = '<s:url action="obtener" namespace="/siniestros/valuacion"/>';
var imprimirValuacionPath = '<s:url action="imprimirValuacion" namespace="/siniestros/valuacion"/>';
</script>

<style type="text/css">
.floatLeft {
	float: left;
	position: relative;
}
</style>

<s:form id="busquedaValuacionReporte" >
	<s:hidden id="idValuacionReporte" name="idValuacionReporte"/>
	<s:hidden id="esConsulta" name="esConsulta"/>
	<s:hidden id="mostrarTablaVacia" name="mostrarTablaVacia"/>

	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="4"><s:text name="midas.siniestros.valuacion.busqueda" /></td>
            </tr>
            <tr>
            
            	<td>			   
            	<s:textfield name="valuacionReporteFiltro.numeroValuacion" id="numValuacion_t" cssClass="cajaTextoM2 w150 numeric" 
            		label="%{getText('midas.siniestros.valuacion.numvaluacion')}" disabled="false"  maxlength="15"/>
            	</td>
            	
            	<td width="300px">
            		<div style="width: 90%;" class="floatLeft">
            				<div class="floatLeft" style="height: 30px; width: 50%;">
								<label style="margin-left: 5%; margin-top:7%; width: 80%;" class="floatLeft">
								<s:text name="midas.siniestros.valuacion.numreporte" />: </label>
							</div>
						
							<div class="floatLeft" style="height: 30px; width: 25%;">
							</div>
							
							<div class="floatLeft" style="height: 30px; width: 30%;">
								<s:textfield id="nsOficina" maxlength="5"
									name="valuacionReporteFiltro.claveOficina" cssStyle="width:70%;"
									cssClass="floatLeft setNew txtfield" ></s:textfield>
								<label style="margin-left: 10%; width: 10%;" class="floatLeft">
									<b>-</b> </label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 25%;">
								<s:textfield id="nsConsecutivoR" maxlength="15"
									name="valuacionReporteFiltro.consecutivoReporte" cssStyle="width: 70%;"
									cssClass="floatLeft numeric setNew txtfield"></s:textfield>
								<label style="margin-left: 10%; width: 10%;" class="floatLeft">
									<b>-</b> </label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 20%;">
								<s:textfield id="nsAnio" name="valuacionReporteFiltro.anioReporte"
									cssStyle="width: 45%;" maxlength="2"
									cssClass="floatLeft numeric setNew txtfield" ></s:textfield>
							</div>
						</div>
            	
            	</td>
            	
            	<td>
            		<s:select list="ajustadores" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="valuacionReporteFiltro.ajustador" id="ajustadores_s" cssClass="cajaTextoM2 w250" onchange=""
					label="%{getText('midas.siniestros.valuacion.ajustador')}"/>
            	</td>
            	
            	<td>
            		<s:select list="valuadores" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="valuacionReporteFiltro.valuador" id="valuadores_s" cssClass="cajaTextoM2 w250" onchange=""
					label="%{getText('midas.siniestros.valuacion.valuador')}"/>
            	</td>
            	
            </tr>
            <tr>
                <td>
	            	<s:textfield name="valuacionReporteFiltro.numeroSerie" id="numSerie_t" cssClass="cajaTextoM2 w150" 
	            		label="%{getText('midas.siniestros.valuacion.numserie')}" disabled="false" maxlength="20"/>
            	</td>
           		
           		<td  align="left"> <sj:datepicker name="valuacionReporteFiltro.fechaInicial"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.siniestros.valuacion.fechainicial')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				
				</td>
			
				<td > <sj:datepicker name="valuacionReporteFiltro.fechaFinal"
					id="txtFechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2"
					label="%{getText('midas.siniestros.valuacion.fechafinal')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);"></sj:datepicker>
				</td>
				
				<td>
					<s:select list="estatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="valuacionReporteFiltro.estatus" id="estatus_s" cssClass="cajaTextoM2 w250" onchange=""
					label="%{getText('midas.siniestros.valuacion.estatus')}"/>
            	</td>
            	
            </tr>
            <tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="buscarValuacionesCrucero(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
      
	<div id="tituloListado" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.valuacion.listado.valuaciones" /></div>	
	<div id="valuacionesCruceroGridContainer" style="display:none;">
		<div id="valuacionesCruceroGrid" style="width:98%;height:200px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>

</s:form>

<script src="<s:url value='/js/siniestros/valuacion/valuacionreporte.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	validarNumericos();
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
