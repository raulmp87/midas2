<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>	
				<td align="right"><label class="labelBlack" ><s:text name="midas.siniestros.valuacion.totalpintura" />:</label></td>
				<td><s:textfield name="entidad.totalPintura" id="totalPintura_t" cssClass="cajaTexto alphaextra formatCurrency" readonly="true" size="14"></s:textfield></td>	
		</tr>
		<tr>	
				<td align="right"><label class="labelBlack" ><s:text name="midas.siniestros.valuacion.totalhojalateria" />:</label></td>
				<td><s:textfield name="entidad.totalHojalateria" id="totalHojalateria_t" cssClass="cajaTexto alphaextra formatCurrency" readonly="true" size="14"></s:textfield></td>	
		</tr>
		<tr>	
				<td align="right"><label class="labelBlack" ><s:text name="midas.siniestros.valuacion.totalrefacciones" />:</label></td>
				<td><s:textfield name="entidad.totalRefacciones" id="totalRefacciones_t" cssClass="cajaTexto alphaextra formatCurrency" readonly="true" size="14"></s:textfield></td>	
		</tr>
		<tr>	
				<td align="right"><label class="labelBlack" ><s:text name="midas.siniestros.valuacion.totalreparacion" />:</label></td>
				<td><s:textfield name="entidad.total" id="total_t" cssClass="cajaTexto alphaextra formatCurrency" readonly="true" size="14"></s:textfield></td>	
		</tr>
	</tbody>
</table>

<script type="text/javascript">
		formatoCantidades();
</script>