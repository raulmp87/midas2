<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ch" 	width="40" align="center" sort="int" id="piezaSeleccionada"></column>
		<column  type="ro"  width="*"  align="center" sort="str"> <s:text name="midas.siniestros.valuacion.seccion" />   </column>
		<column  type="ro"  width="250"  align="center" sort="str"> <s:text name="midas.siniestros.valuacion.pieza" />  </column>
		<column  type="ro"  width="140"  align="center" sort="str"> <s:text name="midas.siniestros.valuacion.tipoafectacion" />  </column>
		<s:if test="entidad.estatus==1">
			<s:if test="esConsulta==0">
				<column  type="edtxt"  width="125"  align="center" sort="str"> <s:text name="midas.siniestros.valuacion.origen" />  </column>
				<column  type="edn"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.costo" />  </column>
			    <column  type="edn"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.pintura" />  </column>
			    <column  type="edn"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.manoobra" />   </column>
			</s:if>
	    </s:if>
	    <s:else>
	    	<column  type="ro"  width="125"  align="center" sort="str"> <s:text name="midas.siniestros.valuacion.origen" />  </column>
			<column  type="ro"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.costo" />  </column>
		    <column  type="ro"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.pintura" />  </column>
		    <column  type="ro"  width="125"  align="center" format="0.00" sort="int"> <s:text name="midas.siniestros.valuacion.manoobra" />   </column>
	    </s:else>
	</head>

	<s:iterator value="piezas">
		<row id="<s:property value="id"/>">
			
			<cell><s:property value="piezaSeleccionada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionAutoDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="pieza.descripcion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoAfectacionDesc" escapeHtml="false" escapeXml="true"/></cell>
		    &lt;div style='text-transform:uppercase;'&gt;<cell><s:property value="origen" escapeHtml="false" escapeXml="true"/></cell>&lt;/div&gt;
			<cell><s:property value="costo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="pintura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="manoObra" escapeHtml="false" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>
	
</rows>