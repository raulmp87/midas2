<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="*"  align="center" sort="int" > <s:text name="midas.siniestros.valuacion.numvaluacion" />   </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.siniestros.valuacion.numreporte" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.siniestros.valuacion.ajustador" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str"  > <s:text name="midas.siniestros.valuacion.valuador" />  </column>
          <column  type="ro"  width="*"  align="center" sort="str"  > <s:text name="midas.siniestros.valuacion.numserie" />  </column>
          <column  type="ro"  width="150"  align="center" sort="date_custom" > <s:text name="midas.siniestros.valuacion.fechavaluacion" />  </column>
          <column  type="ro"  width="*"  align="center" sort="int" > <s:text name="midas.siniestros.valuacion.totalreparacion" />   </column>
          <column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.siniestros.valuacion.estatus" />   </column>
          <column  type="img"   width="40" sort="na" align="center" >Acciones</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>

	</head>

	<s:iterator value="valuaciones">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="numeroValuacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAjustador" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreValuador" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaValuacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="totalReparacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:consultarValuacion(<s:property value="numeroValuacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<s:if test="estatus==1">
				<cell>../img/icons/ico_editar.gif^Editar^javascript:editarValuacion(<s:property value="numeroValuacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:if>
		    <s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		    <s:if test="estatus==2">
		    	<cell>../img/common/ico_impresion.gif^Imprimir^javascript:imprimirValuacion(<s:property value="numeroValuacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			</s:if>
			<s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>
	
</rows>