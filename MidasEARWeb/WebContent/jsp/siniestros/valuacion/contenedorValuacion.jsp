<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<div style="background:white;">
<s:form id="contenedorValuacionForm" action="guardarDatosValuacionManual" namespace="/siniestros/valuacion" name="contenedorValuacionForm">
		<s:hidden id="idEstimacionCoberturaReporteCabina" name="idEstimacionCoberturaReporteCabina"/>
		<s:hidden id="idCoberturaReporteCabina" name="idCoberturaReporteCabina"/>
		<s:hidden id="methodName" name="methodName"/>
		<s:hidden id="namespace" name="namespace"/>
		<s:hidden id="pasesAtencionInt" name="pasesAtencionInt"/>
		<s:hidden id="idToReporte" name="idToReporte"/>
		<s:hidden id="soloConsulta" name="soloConsulta"/>
		<s:hidden id="tipoCalculo" name="tipoCalculo"/>
		<s:hidden id="tipoEstimacion" name="tipoEstimacion"/>
		<s:hidden id="listValuadores" name="listaValuadores"/>
		<s:hidden id="nombreValuador" name="valuacionHgs.valuadorNombre" />
		<s:hidden id="tipoValuacion" name="tipoValuacion" value="2" />
					

	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tr>
				<th><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numSiniestro" /> </th>
				<td><s:textfield value="%{valuacionHgs.reporteCabina.siniestroCabina.numeroSiniestro}"  cssClass="cajaTexto w150 alphaextra" readonly="true"  ></s:textfield></td>
				
				<th><s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosMovimientoEstimado.folio" /> </th>
				<td><s:textfield value="%{valuacionHgs.folio}" cssClass="cajaTexto w150 alphaextra" readonly="true"  ></s:textfield></td>
				
				<th><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.numReporte" /> </th>
				<td><s:textfield value="%{valuacionHgs.reporteCabina.numeroReporte}" cssClass="cajaTexto w150 alphaextra" readonly="true"  ></s:textfield></td>
				
				<th><s:text name="midas.siniestros.cabina.reportecabina.afectacionInciso.poliza" /> </th>
				<td><s:textfield value="%{valuacionHgs.reporteCabina.poliza.numeroPolizaFormateada}" cssClass="cajaTexto w150 alphaextra" readonly="true"  ></s:textfield></td>
			</tr>				
		</table>
		</div>

		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.valuacion.contenedorValuacion.titulo"/>		
		</div>	
		<div id="contenedorFiltros" style="width: 98%;">
		<table  id="agregar" border="0">
			<tr>
				<td>
					<table id="agregar" style="border: #000000">
					<tr>	
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.tipoProceso" /> </th>
						<td>
						<s:select list="listaTiposProcesoValuacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="valuacionHgs.claveTipoProceso" disabled="false" id="tiposProcesosSelect" cssClass="cajaTextoM2 w200 deshabilitar" />
						</td>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.estatusValuacion" /> </th>						
						<td>
						<s:select list="listaEstatusValuacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="valuacionHgs.claveEstatusVal" disabled="false" id="estatusValuacionSelect" cssClass="cajaTextoM2 w200 deshabilitar" />
						</td>
					</tr>
					<tr>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.idValaucion" /> </th>
							<s:if test="valuacionHgs == null ">
								<td><s:textfield id="idValuacionHgs" name="valuacionHgs.valuacionHgs" cssClass="cajaTexto w150 alphaextra limpiarFecha" onChange="validarNumero(this.value);" ></s:textfield></td>
							</s:if>
							<s:else>
								<td><s:textfield id="idValuacionHgs" name="valuacionHgs.valuacionHgs" cssClass="cajaTexto w150 alphaextra limpiarFecha" readonly="true"></s:textfield></td>
							</s:else>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.fechaReingresoReparacion" /> </th>
						<td>
							<sj:datepicker name="valuacionHgs.fechaReingresoTaller"
							id="fechaReingresoRepar" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="10"
							cssClass="w150 cajaTexto" 
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							></sj:datepicker>
<%-- 						<s:textfield id="fechaReingresoRepar" name="valuacionHgs.fechaReingresoTaller" cssClass="cajaTexto w150 alphaextra limpiarFecha" readonly="false"  ></s:textfield> --%>
						</td>		
					</tr>	
					<tr>	
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.fechaIngresoTaller" /> </th>
						<td>
							<sj:datepicker name="valuacionHgs.fechaIngresoTaller"
							id="fechaIngresoTaller" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="10"
							cssClass="w150 cajaTexto" 
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							></sj:datepicker>
<%-- 						<s:textfield id="fechaIngresoTaller" name="valuacionHgs.fechaIngresoTaller" cssClass="cajaTexto w150 alphaextra limpiarFecha" readonly="false"  ></s:textfield> --%>
						</td>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.fechaUltimoSurtidoRefacciones" /> </th>
						<td>
							<sj:datepicker name="valuacionHgs.fechaUltimoSurtido"
							id="fechaUltimoSurtRefac" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="10"
							cssClass="w150 cajaTexto" 
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							></sj:datepicker>
<%-- 						<s:textfield id="fechaUltimoSurtRefac" name="valuacionHgs.fechaUltimoSurtido" cssClass="cajaTexto w150 alphaextra limpiarFecha" readonly="false"  ></s:textfield> --%>
						</td>
						
					</tr>
					<tr>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.fechaTerminacion" /> </th>
						<td>
							<sj:datepicker name="valuacionHgs.fechaTerminacion"
							id="fechaTerminacion" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="10"
							cssClass="w150 cajaTexto" 
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							></sj:datepicker>
<%-- 						<s:textfield id="fechaTerminacion" name="valuacionHgs.fechaTerminacion" cssClass="cajaTexto w150 alphaextra limpiarFecha" readonly="false"  ></s:textfield> --%>
						</td>	
					</tr>
					<tr>
						<th><s:text name="midas.siniestros.valuacion.contenedorValuacion.valuador" /></th> 
						<td colspan=2 >
									<s:select id="valuador" cssClass="cajaTexto" 
									list="listaComboValuadores"  
									name="valuacionHgs.valuadorId" 
									listKey="id" headerKey=""
									listValue="nombrePrestador"		 
									headerValue="%{getText('midas.general.seleccione')}"
									OnChange="onChangeValuador(this);" />
						</td>					
					</tr>					
					</table>
				
				</td>
				<td>					
							<table id="agregar" style="border: #000000">
							<tr><th><s:text name="midas.siniestros.valuacion.contenedorValuacion.montoRefacciones" /> </th></tr>
							<tr><td><s:textfield id="montoRefacciones" name="valuacionHgs.imRefacciones" 
								cssClass="cajaTexto w150 alphaextra formatCurrency" readonly="false"  ></s:textfield></td></tr>
							<tr><th><s:text name="midas.siniestros.valuacion.contenedorValuacion.montoManoObra" /> </th></tr>
							<tr><td><s:textfield id="montoManoObra" name="valuacionHgs.impNanoObra" 
								cssClass="cajaTexto w150 alphaextra formatCurrency" readonly="false"  ></s:textfield></td></tr>
							<tr><th><s:text name="midas.siniestros.valuacion.contenedorValuacion.montoTotalReparacion" /> </th></tr>
							<tr><td><s:textfield id="montoTotalReparacion" name="valuacionHgs.impSubTotalValuacion" 
								cssClass="cajaTexto w150 alphaextra formatCurrency" readonly="false"  ></s:textfield></td></tr>
							</table>
						
			    </td>	
			</tr>			  	
		</table>
		
		<div style="width: 99%;background:white;">
			<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="closeDhtmlxWindows('DetValuacionHGS');"> 
							<s:text name="midas.boton.cerrar" /> 							
						</a>
					</div>
			<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="guardarDatosValuacion();"> 
							<s:text name="Guardar" /> 							
						</a>
					</div>
		    </div>	
		</div>
					
</s:form>
</div>


<!-- JS para dejar en limpio el text que tenga la clase limpiarFecha y sea igual a 01/01/2000 -->
<script type="text/javascript" >
jQuery( document ).ready(function() {
	initCurrencyFormatOnTxtInput();
	jQuery(".limpiarFecha").each(function (index, elemento ) {
		if( jQuery(this).val() == "02/01/2000" ){
			jQuery(this).attr("value","");
		}
	});
});

</script>
