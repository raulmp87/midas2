<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/bloqueo/bloqueoPagos.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/ordenPago.js'/>"></script>

<script type="text/javascript">
	var mostrarRegistroFactura 			= '<s:url action="mostrarRegistroFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var mostrarDevolucionFactura 		= '<s:url action="mostrarDevolucionFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var mostrarBusquedaDevoluciones 	= '<s:url action="mostrarBusquedaDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	
</script>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 40px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel {
	float: left;
	position: relative;
	margin-top: 12px;
}

.divInfDivInterno {
	width: 17%;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<div id="contenido_moduloPagos" style="width: 99%; position: relative;">
	<s:form id="pagosForm" class="floatLeft">
		<s:hidden id="reporteCabinaId" name="reporteCabinaId" />
		<s:hidden id="idCobertura" name="idCobertura" />

		<div class="titulo" align="left">
			<s:text name="midas.servicio.siniestros.pagos.titulo" />
		</div>
		<div id="divInferior" style="width: 1142px !important;"
			class="floatLeft">

			<div id="contenedorFiltrosPagos" class="divContenedorO"
				style="width: 99%">
				<div class="divInterno">

					<div class="floatLeft" style="width: 30%;">
						<div class="floatLeft" style="width: 35%;">
							<s:textfield id="nsOficinaS" maxlength="5"
								name="filtroOrden.claveOficinaSiniestro" cssStyle="width:35%;"								
								cssClass="txtfield" label="No Siniestro" labelposition="left"></s:textfield>
						</div>
						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsConsecutivoRS" maxlength="15"
								name="filtroOrden.consecutivoReporteSiniestro"
								cssStyle="width: 99%;"
								cssClass="txtfield"></s:textfield>

						</div>

						<div class="floatLeft" style="width: 15%;">
							<s:textfield id="nsAnioS" name="filtroOrden.anioReporteSiniestro"
								cssStyle="width: 99%;" maxlength="4"
								cssClass="txtfield"></s:textfield>
						</div>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="oficinas" id="ofinasActivas"
							name="filtroOrden.oficinaId" label="Oficinas"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:60%;">
						</s:select>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="estautsMap" id="estautsId"
							name="filtroOrden.estatus" label="Estatus" labelposition="left"
							cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:60%;">
						</s:select>
					</div>
				</div>
				<div class="divInterno">


					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="idOrdenPago" name="filtroOrden.id"
							cssClass="txtfield" label="Orden de Pago"
							onkeypress="buscarListaEnter(event);return soloNumeros(this, event, true);"	
							labelposition="left" cssStyle="width:80px;" ></s:textfield>
					</div>

				<!--  	<div class="floatLeft" style="width: 30%;">
						<s:textfield id="idOrdenPagoSeycos"
							name="filtroOrden.idOrdenPagoSeycos" cssClass="txtfield"
							onkeypress="return soloNumeros(this, event, true)"
							label="Orden de Pago Seycos" labelposition="left"
							cssStyle="width:80px;" ></s:textfield>
					</div>-->
					
					<div class="floatLeft" style="width: 30%;">
						<s:select id="tipoProveedorLis" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="filtroOrden.tipoProveedor"
									headerKey="" headerValue="%{getText('Seleccione Tipo... ')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w140"   
							  		onchange="changeTipoPrestador()"
							  	 />
					</div>

					
					<div class="floatLeft" style="width: 30%;">
							<s:select id="proveedorLis" 
									labelposition="left" 
									label="Proveedor"
									name="filtroOrden.idBeneficiario"
									headerKey="" headerValue="%{getText('Seleccione Proveedor... ')}"
							  		list="proveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w200"   
							  	 />
					</div>
				</div>

				<div class="divInterno" style="height: 60px;">
				
				<div class="floatLeft divInfDivInterno" style="width: 30%;" >
							 <s:checkbox name="filtroOrden.servPublico"  id="servPublico"  label="Servicio Publico" labelposition="right"> </s:checkbox>
							 <s:checkbox name="filtroOrden.servParticular"  id="servParticular"  label="Servicio Particular" labelposition="right"> </s:checkbox>
						</div>
				
				
				

					

					<div class="floatLeft" style="width: 30%;">
						<s:select list="tipoOrdenPagoMap" id="tipoOrdenPago"
							name="filtroOrden.tipoPago" label="Tipo de Pago"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:60%;">
						</s:select>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:select list="conceptoPagoMap" id="conceptoPago"
							name="filtroOrden.conceptoAjusteId" label="Concepto"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:45%;">
						</s:select>
					</div>

				</div>

				<div class="divInterno">
					
					<div class="floatLeft" style="width: 30%;">
						<s:select list="terminoAjusteMap" id="terminoAjusteLis"
							name="filtroOrden.terminoAjuste" label="Termino de Ajuste"
							labelposition="left" cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:220px;">
						</s:select>
					</div>

					<div class="floatLeft" style="width: 30%;">
					
					
						<s:textfield id="numOrdenCompra" name="filtroOrden.numOrdenCompra"
							onkeypress="buscarListaEnter(event);return soloNumeros(this, event, true);"	
							cssClass="txtfield" label="Orden de Compra" labelposition="left"
							cssStyle="width:80px;"></s:textfield>
					</div>

					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="beneficiario" name="filtroOrden.beneficiario"
							cssClass="txtfield" label="Beneficiario" labelposition="left"
							cssStyle="width:180px;" ></s:textfield>
					</div>
				</div>
				<div class="divInterno" style="padding-right: 200px">
					<div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);"
							onclick="limpiarContenedorListadoPagos();"> <s:text
								name="midas.boton.limpiar" /> </a>
					</div>
					<div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="buscarListaOrdenesPago();">
							<s:text name="midas.boton.buscar" /> </a>
					</div>
				</div>
			</div>

			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="midas.servicio.siniestros.pagos.listadotitulo" />
			</div>
		   <div id="pagosGrid" class="dataGridConfigurationClass"
			style="width: 99%; height: 380px;"></div>			
			<div id="pagingArea" style="padding-top: 8px "></div>
			<div id="infoArea"></div>
			
			<div class="divInterno" >
					
					<div class="btn_back w180"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="consultarFacturasDevueltas();">
							<s:text name="Consultar Facturas Devueltas" /> </a>
					</div>
					
					<div class="btn_back w180"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="consultarBloqueoPagos();">
							<s:text name="Consultar Bloqueo de Pagos" /> </a>
					</div>
					
				</div>
		
		</div>
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</s:form>
	<script>
		jQuery(document).ready(function() {
			iniciarLista();
		});
	</script>

</div>
