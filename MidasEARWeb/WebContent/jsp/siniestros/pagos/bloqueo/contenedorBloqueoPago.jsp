<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.fuente {
    font-family: arial;
    font-size: 11px;
    color: #666666;
    font-weight: bold;
}

label.label {
    font-family: arial;
    font-size: 11px;
} 

.error {
	background-color: red;
	opacity: 0.4;
}

.mensajeError {
	color: red;
	text-align: center;
}

</style>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/bloqueo/bloqueoPagos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<script type="text/javascript">
var idPagoBloqueado  = '<s:property value="idPagoBloqueado"/>';
var esModoConsulta = '<s:property value="esModoConsulta"/>';
var idPago = '<s:property value="idPago"/>';
</script>


<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<s:if test="esModoConsulta==true">
	<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.pagos.bloqueo.detalleBloqueo"/></div>
</s:if>
<s:else>
	<s:if test="bloqueoPago.estatus==true">
		<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.pagos.bloqueo.pantallaBloqueo"/></div>
	</s:if><s:else>
		<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.pagos.bloqueo.pantallaDesbloqueo"/></div>
	</s:else>
</s:else>


<div id="contenedorDetalle">
	<s:form id="infoGeneralForm" >
	
	<s:hidden id="h_pagoId" name="bloqueoPago.id" />
	<s:hidden id="h_estatusPago" name="bloqueoPago.estatus" />
	<s:hidden id="h_tipoBloqueo" value="%{bloqueoPago.tipo}" />
	<s:hidden id="h_pagoDeListadoPagos" name="idPago" />
	<s:hidden id="h_pagoDeListadoPagosObj" value="%{bloqueoPagobloqueoPago.ordenPago.id}" />
	<s:hidden id="h_esDeOrdenesDePago" name="esDeOrdenesDePago" />

		<table id="filtrosM2" border="0" style="width: 98%;">
			<tbody>
				<tr id="messageError">
					<td class = "mensajeError" colspan="3" align="center"><s:text name="midas.siniestros.pagos.bloqueo.errorEspecificacionDeBloqueo"/></td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<s:radio id="r_tipo_bloqueo" 
							name="bloqueoPago.tipo" 
							list="tiposDeBloqueoMap" 
							listKey="key" 
							listValue="value" 
							onchange="validaTipoDeBloqueo();"/>				
					</td>
				</tr>
				<tr>
					<td align="right">
						<s:select id="s_tipoProveedorList" 
									name = "tipoPrestador"
									labelposition="left" 
									label="Proveedor"
									headerKey="" headerValue="%{getText('Seleccione Tipo... ')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w140"   
							  		onchange="changeTipoPrestador()"/>
					</td>
					<td align="center">
						<s:select id="s_proveedorList" 
									name="proveedorId"
									headerKey="" headerValue="%{getText('Seleccione Proveedor... ')}"
							  		list="proveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w200"/>
					</td>
					<td align="left">
							<s:checkbox 
									id = "ch_bloqueo"
									key="midas.siniestros.pagos.bloqueo.habilitarBloqueo" 
									value="%{bloqueoPago.estatus}"
									labelposition="right"
									onchange="validaBloqueo();" />
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<s:textfield  id="t_ordenDePago" 
							 	cssClass="txtfield cajaTextoM2 w90"
							 	key = "midas.siniestros.pagos.bloqueo.ordenPagoMidas" labelposition="left" 
								name="bloqueoPago.ordenPago.id"
								maxlength="13"
								onkeypress="return soloNumeros(this, event, false)"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<table>
							<tr>
								<td align="center">
									<s:text name="midas.siniestros.pagos.bloqueo.motivoBloqueo"/>
								</td>
								<td align="center">
									<s:text name="midas.siniestros.pagos.bloqueo.motivoDesbloqueo"/>
								</td>
							</tr>
							<tr>
								<td align="center" align="center">
									<s:textarea id="ta_motivoBloqueo" name="bloqueoPago.motivoBloqueo" cols="30" rows="8" maxlength="1000"/>
								</td>
								<td align="center" align="center">
									<s:textarea id="ta_motivoDesbloqueo" name="bloqueoPago.motivoDesbloqueo" cols="30" rows="8" maxlength="1000"/>
								</td>
							</tr>
							<tr>
								<td align="center">
									<s:textfield  id="t_fechaBloqueo" 
									 	cssClass="txtfield jQrestrict cajaTextoM2 w90"
									 	key = "midas.siniestros.pagos.bloqueo.fechaDeBloqueo" labelposition="left" 
										name="bloqueoPago.fechaBloqueo"
										disabled = "true"/>
								</td>
								<td align="center">
									<s:textfield  id="t_fechaDesBloqueo" 
									 	cssClass="txtfield jQrestrict cajaTextoM2 w90"
									 	key = "midas.siniestros.pagos.bloqueo.fechaDesbloqueo" labelposition="left" 
										name="bloqueoPago.fechaDesbloqueo"
										disabled = "true"/>
								</td>
							</tr>
							<tr>
								<td align="center">
									<s:textfield  id="t_usuarioBloqueo" 
									 	cssClass="txtfield jQrestrict cajaTextoM2 w230"
									 	key = "midas.siniestros.pagos.bloqueo.bloqueadoPor" labelposition="left" 
										name="bloqueoPago.usuarioBloqueo"
										disabled = "true"/>
								</td>
								<td align="center">
									<s:textfield  id="t_usuarioDesbloqueo" 
									 	cssClass="txtfield jQrestrict cajaTextoM2 w230"
									 	key = "midas.siniestros.pagos.bloqueo.desbloqueadoPor" labelposition="left" 
										name="bloqueoPago.usuarioDesbloqueo"
										disabled = "true"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				
				</tr>
				<tr>		
					<td colspan="7" align="right">		
						<table  style="padding: 0px; width: 95%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<s:if test="esModoConsulta!=true">
										<div class="btn_back w110" style="display: inline; float: right;" id="btn_guardar">
											<a href="javascript: void(0);" onclick="javascript: guardarPagoBloqueado();"> 
												<s:text name="midas.boton.guardar" /> 
												&nbsp;
												<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
											</a>
										</div>
									</s:if>


									<s:if test="esDeOrdenesDePago=true!=null && esDeOrdenesDePago">
										<div class="btn_back w110" style="display: inline; float: right;" >
											<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: regresaListadoDePagos();"> 
												<s:text name="midas.boton.cerrar" /> 
												&nbsp;
												<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
											</a>
										</div>	
									</s:if><s:else>
										<div class="btn_back w110" style="display: inline; float: right;" >
											<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarDetalle();"> 
												<s:text name="midas.boton.cerrar" /> 
												&nbsp;
												<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
											</a>
										</div>	
									</s:else>
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	
<br/>
<br/>

<script type="text/javascript">
jQuery(document).ready(
		function(){
			jQuery("#messageError").hide();
			validaVisualizacion();
			
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
