<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.fuente {
    font-family: arial;
    font-size: 11px;
    color: #666666;
    font-weight: bold;
}

label.label {
    font-family: arial;
    font-size: 11px;
} 

</style>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/bloqueo/bloqueoPagos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>


<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">


<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.pagos.bloqueo.BusquedaBloqueosPagos"/></div>
<div id="contenedorFiltros">
	<s:form id="infoGeneralForm" >
		<table id="filtrosM2" border="1" style="width: 98%;">
			<tbody>
				<tr>
					
					<td colspan="1">
						<s:select id="s_tipoBloqueo" 
							  name="filtroBloqueos.tipoBloqueo"
							  key="midas.siniestros.pagos.bloqueo.tipoBloqueo"  labelposition="left" 
							  cssClass="cajaTextoM2 w150 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="tiposDeBloqueoMap" listKey="key" listValue="value"  /> 						
					</td>
					<td>
						<s:select id="s_tipoProveedorList" 
									labelposition="left" 
									key="midas.siniestros.pagos.bloqueo.tipoProovedor"
									headerKey="" headerValue="%{getText('Seleccione Tipo... ')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w140"   
							  		onchange="changeTipoPrestador()"/>
					</td>
					<td colspan="2">
						<s:select id="s_proveedorList" 
									labelposition="left" 
									name="filtroBloqueos.proovedorId"
									key="midas.siniestros.pagos.bloqueo.proovedor"
									headerKey="" headerValue="%{getText('Seleccione Proveedor... ')}"
							  		list="proveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w200"/>
					</td>
				</tr>
				<tr>
					<td>
						<s:select id="s_situacionActual" 
							  name="filtroBloqueos.estatusPago"
							  key="midas.siniestros.pagos.bloqueo.situacionActual"  labelposition="left" 
							  cssClass="cajaTextoM2 w250 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="situacionesActualesMap" listKey="key" listValue="value"  />
					</td>
					<td colspan="2">
						<table>
							<tr>
								<td align="center" colspan="2" class="fuente">
									<s:text name="midas.siniestros.pagos.bloqueo.fechaDeBloqueo"/>
								</td>
							</tr>
							<tr>
								<td>
									<sj:datepicker name="filtroBloqueos.fechaBloqueoDe"
												key="midas.siniestros.pagos.bloqueo.fecha.de"
									  labelposition="left" 
										changeMonth="true"
										 changeYear="true"				
										buttonImage="../img/b_calendario.gif"
										buttonImageOnly="true" 
					                			 id="dp_fechaDe"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w90"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>			
								</td>
								<td>
									<sj:datepicker name="filtroBloqueos.fechaBloqueoHasta"
												key="midas.siniestros.pagos.bloqueo.fecha.hasta"
									  labelposition="left" 
										changeMonth="true"
										 changeYear="true"				
										buttonImage="../img/b_calendario.gif"
										buttonImageOnly="true" 
					                			 id="dp_fechaHasta"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w90"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="3">
						<s:textfield  id="t_ordenDePagoMidas" 
							 	cssClass="txtfield cajaTextoM2 w90"
							 	key = "midas.siniestros.pagos.bloqueo.ordenPagoMidas" labelposition="left" 
								name="filtroBloqueos.ordenDePago"
								maxlength="13"
								onkeypress="return soloNumeros(this, event, false)"/>
					</td>
				</tr>
				<tr>		
					<td colspan="7" align="right">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;" border="0">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="realizarBusqueda(true);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	
<br/>
<br/>
<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.pagos.bloqueo.listarBloqueoDePagos"/></div>

<div id="indicador"></div>
<div id="listaDePagosBloquedosGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;" border="0">
	<tr>
		<td  class= "guardar">
			<div class="btn_back w110" style="display: inline; float: right;" id="btn_guardar">
				<a href="javascript: void(0);" onclick="javascript: crearBloqueo();"> 
					<s:text name="midas.siniestros.pagos.bloqueo.bloquearPago" /> 
					&nbsp;
					<img border='0px' alt='Bloquear Pago' title='Bloquear Pago' src='/MidasWeb/img/btn_guardar.jpg'/>
				</a>
			</div>
			<div class="btn_back w110" style="display: inline; float: right;" id="btn_cerrar">
				<a href="javascript: void(0);" onclick="javascript: regresaListadoDePagos();"> 
					<s:text name="midas.boton.cerrar" /> 
				</a>
			</div>
		</td>							
	</tr>
</table>				

<script type="text/javascript">
jQuery(document).ready(
		function(){
			realizarBusqueda(false);
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
