<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		    <column id="fecha_bloqueo" type="ro"  width="*" align="center"><s:text name="midas.siniestros.pagos.bloqueo.fechaDeBloqueo"/></column>
            <column id="fecha_desbloqueo" type="ro"  width="*"  sort="str" align="center"><s:text name="midas.siniestros.pagos.bloqueo.fechaDesbloqueo"/></column>
          	<column id="orden_pago_midas" type="ro"  width="*"  sort="str" align="center"><s:text name="midas.siniestros.pagos.bloqueo.ordenPagoMidas"/></column>
          	<column id="proovedor" type="ro"  width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.bloqueo.proovedor"/></column>
          	<column id="situacionActual" type="ro"  width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.bloqueo.situacionActual"/></column>
          	<column id="tipoDeBloqueo" type="ro"  width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.bloqueo.tipoBloqueo"/></column>
			<column id="editar"  type="img" width="*" sort="na" align="center" ></column> 
			<column id="consultar"  type="img" width="*" sort="na" align="center" ></column> 
	</head>			
	<s:iterator value="listadoBloqueos" status="row">
		<row id="<s:property value="id"/>">
			<cell ><s:property value="fechaBloqueo" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="fechaDesbloqueo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ordenPago.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="proveedor.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="situacionActualDesc" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="tipoDeBloqueoDesc" escapeHtml="false" escapeXml="true"/></cell>	
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarPagoBloqueado(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarPagoBloqueado(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>	
</rows>
