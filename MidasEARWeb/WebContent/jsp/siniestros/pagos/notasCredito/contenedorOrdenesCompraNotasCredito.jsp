<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<%-- <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script> --%>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<%-- <script type="text/javascript"> --%>
<!-- 	jquery143 = jQuery.noConflict(true); -->
<%-- </script> --%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/notasCredito/notaCredito.js'/>"></script>

<script type="text/javascript">
var listarOrdenesCompraPath = '<s:url action="listarOrdenesDeCompra" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';
	
</script>

<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>


<s:form id="recepcionNotaCreditoForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.notasCredito.listaOrdenesCompra.titulo"/>	
</div>	
<s:hidden id="idBatch" name="idBatch"/>
<s:hidden id="idFactura" name="factura.id"/>
<s:hidden id="idCancelacionPreseleccion" name="seleccionNotasCreditoConcat"/>
<s:hidden id="soloLectura" name="soloLectura"/>
<s:hidden id="esCancelacion" name="esCancelacion"/>
<s:hidden id="idConjuntoNotaCredito" name="conjuntoOrdenCompraNota.id"/>
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar">
		<tbody>
			<tr>
				<th>
					<s:text name="midas.siniestros.pagos.notasCredito.numeroProveedor"/>
				</th>					
			    <td>  
			    	<s:textfield id="idProveedor" disabled="true" name="proveedor.id" cssClass="cajaTextoM2 w50" />				    	  				 
				</td>			   	
				<th>
					<s:text name="midas.siniestros.pagos.notasCredito.proveedor"/>
				</th>
				<td>
					<s:textfield id="nombrePrestadorServicio" disabled="true" name="proveedor.personaMidas.nombre" cssClass="cajaTextoM2 w300" />										 
				</td>	
				<th>
					<s:text name="midas.siniestros.pagos.notasCredito.numeroFactura"/>
				</th>
				<td>
					<s:textfield id="numeroFactura" disabled="true" name="factura.numeroFactura" cssClass="cajaTextoM2 w100" />										 
				</td>
				<td>
					<s:if test="!soloLectura && !esCancelacion">
						<div class="btn_back w110" id="buscarFacturasBoton" >
								<a href="javascript: void(0);" title="Buscar Facturas para generar Nota de Crédito"
									onclick="mostrarBusquedaFacturas();">
									<s:text name="midas.boton.buscar"/>
								</a>
						</div>
					</s:if>
				</td>							  
	    	</tr>	    	
	 	</tbody>
	 </table>		
</div>
<div id="divOrdenesCompra">    
   <div id="indicador"></div>	
	<div id="ordenesCompraListadoGrid" style="width:98%;height:180px">	
	<div id="pagingArea"></div><div id="infoArea"></div>
   </div>
</div>
<div id="spacer2" style="height: 15px"></div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.notasCredito.listaNotasCredito.titulo"/>	
</div>	
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<s:if test="!soloLectura && !esCancelacion">
		<table style="width: 98%;">
			<tr>
				<td width="200px;" >
					<div class="btn_back w200" id="cargar" >
						<a href="javascript: void(0);" title="Seleccione un arhivo comprimido extensión ZIP que contenga las notas de crédito a cargar (XMLs) o bien un solo archivo extensión XML para 1 sola nota de crédito"							
						   onclick="cargarZipNotasCredito();">
							<s:text name="midas.siniestros.pagos.notasCredito.cargarNotas.boton"/>
						</a>
					</div>
				</td>
				<td>
				<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
					<font color="#FF6600">
					<s:text name="test">El tamaño máximo del archivo puede ser 40 MB</s:text>			
					</font>
				</div>
				</td>
				<td>
				<td>
					<div style="display:inline; width:175px; float: right; color: #00a000; font-size: 10;" id="divExcelBtn">
		            	<s:text name="test">Total Notas Crédito Cargadas:</s:text>	
		            </div>
				</td>
				<td width="100px;">
					<s:textfield id="numNotasCargadas" disabled="true" name="numNotasCargadas" cssStyle="float: right;" cssClass="cajaTextoM2 w80" />
				</td>					
			</tr>		
		</table>
	</s:if>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="divNotasCredito">    
    <div id="indicador"></div>
	<div id="gridNotasCreditoPaginado">
		<div id="notasCreditoListadoGrid" style="width:98%;height:180px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right" style="width: 100%;">
			<tbody>
				<tr>
				<td> 
					         
            	</td>
            	<td>
            		<div id="cerrarBoton" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
						<a href="javascript: void(0);" onclick="mostrarBusquedaNC();">
							<s:text name="midas.boton.cerrar" /> 
						</a>
					</div>
					<s:if test="esCancelacion">
						<div id="cancelarBoton" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="cancelarNotasCreditoSeleccionadas();">
								<s:text name="midas.boton.cancelar" /> 
							</a>
						</div>
					</s:if> 
					<s:if test="idBatch != null && !soloLectura && !esCancelacion">
						<div id="validarRegistrarBoton" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="validarRegistrarNC();">
								<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.validarRegistrar.boton" /> 
							</a>
						</div>
					</s:if> 
				</td>				
				</tr>
			</tbody>
	</table>		
</div>
</s:form>
<script type="text/javascript">
	buscarOrdenesCompraPorFactura();
	listarNotasCredito(); 	 
</script>
