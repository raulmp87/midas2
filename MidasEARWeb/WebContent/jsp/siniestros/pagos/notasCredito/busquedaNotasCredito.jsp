<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<%-- <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script> --%>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<%-- <script type="text/javascript"> --%>
<!-- 	jquery143 = jQuery.noConflict(true); -->
<%-- </script> --%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script	type="text/javascript" src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/notasCredito/notaCredito.js'/>"></script>

<style type="text/css">
table#agregar th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
</style>
<s:form id="busquedaNotasCreditoForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.notasCredito.listaNotasCredito.titulo"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" style="width:98%;" border="0"> 
			<tbody>
				<tr>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.folio"/>
					</th>					
					<td>
						<s:textfield id="folio" name="filtroNotasCredito.folioNotaCredito" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict" />						 
					</td>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.numeroProveedor"/>
					</th>
					<td>
						<s:textfield id="numProveedor" name="filtroNotasCredito.noProveedor" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict" />					 
					</td>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.proveedor"/>
					</th>					
					<td>	
						<s:textfield id="nombreProveedor" name="filtroNotasCredito.nombreProveedor" cssClass="cajaTextoM2 w200 jQalphanumeric jQrestrict" />			 
					</td>
				</tr>				
				<tr>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.numeroFactura"/>
					</th>					
					<td>
						<s:textfield id="numFactura" name="filtroNotasCredito.noFactura" cssClass="cajaTextoM2 w200 jQnumeric jQrestrict" />					 
					</td>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.numeroSiniestro"/>
					</th>
					<td>
						<s:textfield id="numSiniestro" name="filtroNotasCredito.siniestro" cssClass="cajaTextoM2 w200" onBlur="validaFormatoSiniestro(this);" />						 
					</td>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.estatus"/>
					</th>
					<td> 
						<s:select id="estatus"
						 list="listaEstatusNotasCredito" name="filtroNotasCredito.estatus" headerKey="" 
						 headerValue="%{getText('midas.general.seleccione')}" 						 
						 cssClass="cajaTexto jQrequired w200 campoForma" />					 
					</td>				
				</tr>
				<tr>
					<th>
						<s:text name="Monto"/>
					</th>					
					<td colspan=3">
						<table id="desplegarDetalle">
							<tr>
								<th>
									<s:text name="midas.juridico.citas.fechaCita.de"/>	
								</th>
								<td style="width:130px;">
									<s:textfield id="numSiniestroIni" name="filtroNotasCredito.montoNotaCreditoIni" 
									cssClass="cajaTextoM2 w100 jQfloat jQrestrict"
									onkeyup = "mascaraDecimales('#numSiniestroIni',this.value);"
									onblur  = "mascaraDecimales('#numSiniestroIni',this.value);" />		
								<td>
								<th>
									<s:text name="midas.siniestros.pagos.notasCredito.hasta"/>	
								</th>
								<td>
									<s:textfield id="numSiniestroFin" name="filtroNotasCredito.montoNotaCreditoFin" 
									cssClass="cajaTextoM2 w100 jQfloat jQrestrict" 
									onkeyup = "mascaraDecimales('#numSiniestroFin',this.value);"
									onkeyup = "mascaraDecimales('#numSiniestroFin',this.value);" />	
								<td>
							</tr>						
						</table>					 
					</td>								
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.fechaRegistro"/>
					</th>					
					<td colspan=3">
						<table id="desplegarDetalle">
							<tr>
								<th>
									<s:text name="midas.juridico.citas.fechaCita.de"/>	
								</th>
								<td style="width:130px;">
									<sj:datepicker 
										name="filtroNotasCredito.fechaRegNotaCreditoIni"
										cssStyle="width: 80px;" required="#requiredField"
										buttonImage="../img/b_calendario.gif" id="fechaInicio1"
										changeMonth="true" changeYear="true" maxlength="10" 
										cssClass="txtfield" size="12"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>	
								<td>
								<th>
									<s:text name="midas.siniestros.pagos.notasCredito.hasta"/>	
								</th>
								<td>
									<sj:datepicker 
										name="filtroNotasCredito.fechaRegNotaCreditoFin"
										cssStyle="width: 80px;" required="#requiredField"
										buttonImage="../img/b_calendario.gif" id="fechaFin1"
										changeMonth="true" changeYear="true" maxlength="10" 
										cssClass="txtfield" size="12"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>
								<td>
							</tr>						
						</table>					 
					</td>								
				</tr>
				<tr>
					<th>
						<s:text name="midas.siniestros.pagos.notasCredito.fechaBaja"/>
					</th>					
					<td colspan=3">
						<table id="desplegarDetalle">
						<tr>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.de"/>	
						</th>
						<td style="width:130px;">
						<sj:datepicker 
							name="filtroNotasCredito.fechaBajaNotaCreditoIni"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaInicio2"
							changeMonth="true" changeYear="true" maxlength="10" 
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>	
						<td>
						<th>
							<s:text name="midas.siniestros.pagos.notasCredito.hasta"/>	
						</th>
						<td>
						<sj:datepicker 
							name="filtroNotasCredito.fechaBajaNotaCreditoFin"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaFin2"
							changeMonth="true" changeYear="true" maxlength="10" 
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
						<td>
						</tr>						
						</table>					 
					</td>
					<td colspan="2">
						<table align="right">
							<tr>
								<td align="right">
									<div class="btn_back w110" id="botonLimpiar" >
										<a href="javascript: void(0);"
										   onclick="limpiarFiltrosBusquedaNC();">
								         <s:text name="midas.boton.limpiar"/>
										</a>
									</div>									
								</td>
								<td align="right" style="width:50px;">
									<div class="btn_back w110" id="botonBuscar" >
										<a href="javascript: void(0);"
										   onclick="buscarNotasCredito();">
											<s:text name="midas.boton.buscar"/>
										</a>
									</div>									
								</td>
							</tr>
						</table>						
					</td>								
				</tr>
			</tbody>
	</table>
</div>	
</s:form>			
<div id="spacer1" style="height: 10px"></div>
<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.pagos.notasCredito.busqueda.listaNotasCredito.titulo"/>	
		</div>
<div id="tablaNotasCreditoFiltros">
    <!-- Tabla   -->
    <div id="indicador"></div>
	<div id="busquedaNotasCreditoPaginado">
		<div id="busquedaNotasCreditoGrid" style="width:96%;height:175px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 15px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right">
			<tbody>
				<tr>
					<td>
						<div align="right">
						<div class="btn_back w110" id="nuevoBoton" >
							<a href="javascript: void(0);"
								onclick="nuevaRecepcionNotasCredito();">
								<s:text name="midas.boton.nuevo"/>
							</a>
						</div>
						</div>
					</td>
				</tr>
			</tbody>
	</table>		
</div>
<script type="text/javascript" >
initNotaCredito();
</script>