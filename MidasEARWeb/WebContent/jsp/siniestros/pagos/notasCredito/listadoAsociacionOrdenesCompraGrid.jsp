<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="checkSeleccion" type="ch" width="45" sort="int"><s:text name="midas.siniestros.pagos.notasCredito.seleccionOC"/></column>		
		<column id="numeroSiniestro" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.siniestro"/></column>
		<column id="numeroOrdenPago" type="ro" width="80" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.ordenPago"/></column>
		<column id="numeroOrdenCompra" type="ro" width="80" sort="str"><s:text name="midas.siniestros.pagos.notasCredito.ordenCompra"/></column>		
		<column id="fechaRegistro" type="ro" width="80" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.fechaRegistro"/></column>
		<column id="concepto" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.concepto"/></column>
		<column id="afectado" type="ro" width="150" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.afectado"/></column>
		<column id="checkCancelarOC" type="ch" width="52" sort="int"><s:text name="midas.siniestros.pagos.notasCredito.seleccionCancelarOC"/></column>	
		<column id="subtotal" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.subtotal"/></column>		
		<column id="iva" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.iva"/></column>
		<column id="ivaRet" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ivaRetenido"/></column>
		<column id="isr" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.isr"/></column>
		<column id="total" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>	
	</head>	  		
	<s:iterator value="listaOrdenesCompra" status="stats">
		<row id="<s:property value="ordenCompraId"/>">
			<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>	
		    <cell><s:property value="siniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ordenPago" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="ordenCompraId" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="concepto" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="afectado" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="subTotal" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="iva" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="ivaRetenido" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="isr" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="total" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>
</rows>
