<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="idProveedor" type="ro" width="150" sort="int">No. Proveedor</column>		
		<column id="nombreProveedor" type="ro" width="250" sort="int" align="center">Proveedor</column>
		<column id="noFactura" type="ro" width="150" sort="str" align="center">No. Factura</column>
		<column id="fechaFactura" type="ro" width="150" sort="str">Fecha Factura</column>				
	</head>	  		
	<s:iterator value="listaFacturas" status="stats">
		<row id="<s:property value="idFactura"/>">
		    <cell><s:property value="idProveedor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreProveedor" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="noFactura" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="fechaFactura" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>
</rows>
