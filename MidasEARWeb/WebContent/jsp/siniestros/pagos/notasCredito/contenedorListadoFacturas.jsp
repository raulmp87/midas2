<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
<link href="<html:rewrite page="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<%-- <script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/notasCredito/notaCredito.js'/>"></script>  --%>

<script type="text/javascript">

function buscarFacturas()
{	var facturasListadoGrid;
	document.getElementById("facturasListadoGrid").innerHTML = '';	
	facturasListadoGrid = new dhtmlXGridObject('facturasListadoGrid');
		
	facturasListadoGrid.attachEvent("onXLE", function(grid){
		ocultarIndicadorCarga('indicador');
    });
	
	
	facturasListadoGrid.attachEvent("onRowSelect", function(id,ind){	    
		
		var idFactura = id;
		var idProveedor = facturasListadoGrid.cellById(id,0).getValue()
		var nombreProveedor = facturasListadoGrid.cellById(id,1).getValue()
		var numeroFactura = facturasListadoGrid.cellById(id,2).getValue()

		parent.obtenerDatosFactura(idFactura, idProveedor, nombreProveedor, numeroFactura);		 
	});
	
	var url = '/MidasWeb/siniestros/pagos/notasCredito/buscarFacturasParaNotasCredito.action?';
	
	facturasListadoGrid.attachHeader("#text_filter,#text_filter,#text_filter,#select_filter");
	facturasListadoGrid.load(url);
}

</script>

<div style="background:white;">
<s:form id="busquedaFacturasForm" >
<div id="spacer1" style="height: 10px"></div>
<div id="divFacturas">    
    <div id="indicador"></div>
	<div id="gridFacturasPaginado">
		<div id="facturasListadoGrid" style="width:98%;height:340px">
	</div>
<!-- 	<div id="pagingArea"></div><div id="infoArea"></div> -->
    </div>
</div>
</s:form>
</div>
<script type="text/javascript">
	buscarFacturas(); 	 
</script>
