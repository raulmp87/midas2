<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
	    <column id="idFactura" type="ro" hidden="true" width="100" sort="int" align="center"></column>
	    <column id="idConjunto" type="ro" hidden="true" width="100" sort="int" align="center"></column>
		<column id="folio" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.folio"/></column>
		<column id="monto" type="ro" width="100" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.monto"/></column>
		<column id="numProveedor" type="ro" width="100" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.numeroProveedor"/></column>
		<column id="proveedor" type="ro" width="205" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.proveedor"/></column>
		<column id="factura" type="ro" width="100" sort="str"><s:text name="midas.siniestros.pagos.notasCredito.numeroFactura"/></column>		
		<column id="siniestro" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.numeroSiniestro"/></column>
		<column id="fechaRegistro" type="ro" width="100" sort="str"><s:text name="midas.siniestros.pagos.notasCredito.fechaRegistro"/></column>		
		<column id="fechaBaja" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.fechaBaja"/></column>
		<column id="estatus" type="ro" width="100" sort="str" align="center"><s:text name="midas.siniestros.pagos.notasCredito.estatus"/></column>	
		<column id="consultar" type="img" width="70" align="center"></column>
		<column id="eliminar" type="img" width="70" align="center"></column>	
	</head>	  		
	<s:iterator value="listaNotasCredito" status="stats">
		<row id="<s:property value="notaCreditoId"/>">
		    <cell><s:property value="idFactura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idConjunto" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="folioNotaCredito" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="montoNotaCredito" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="noProveedor" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="nombreProveedor" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="noFactura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="siniestro" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="fechaBaja" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="idConjunto != null">
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Nota de Credito^javascript: verRecepcionNotasCredito(<s:property value="idConjunto"/>,
				null,<s:property value="idFactura"/>,<s:property value="noFactura"/>,<s:property value="noProveedor"/>
						,"<s:property value="nombreProveedor"/>", true, false)^_self</cell>		
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar Nota de Credito^javascript: cancelarNotaCredito(<s:property value="idConjunto"/>,
				<s:property value="notaCreditoId"/>,<s:property value="idFactura"/>,<s:property value="noFactura"/>,
				<s:property value="noProveedor"/>,"<s:property value="nombreProveedor"/>", false, true)^_self</cell>						
			</s:if>
			<s:elseif test="estatusDesc != 'CANCELADA'">
				<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar Nota de Credito^javascript: cancelarNotaCredito(null,
				<s:property value="notaCreditoId"/>,null,null,
				null,"", false, true)^_self</cell>
			</s:elseif>											
			
		</row>
	</s:iterator>
</rows>
