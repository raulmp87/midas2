<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/ordenPago.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	
</script>
<s:form id="definirOrdenPagoForm" class="floatLeft">
<div id="contenido_DefinirOrden" style="width:99%;position: relative;">
		<s:hidden id="idReporteCabina" name="idReporteCabina" />
		<s:hidden id="soloLectura" name="soloLectura" />
		<s:hidden id="tipoOrdenPago" name="filtroOrden.tipoOrdenPago" />
		<s:hidden id="cveTipoOrdenPago" name="filtroOrden.cveTipoOrdenPago" />
				<s:hidden id="estatus_h" name="filtroOrden.estatus" />
		
		
		
		<s:hidden id="idOrdenCompra" name="idOrdenCompra" />
		<s:hidden id="modoPantalla" name="modoPantalla" />
		<s:hidden id="idOrdenPago" name="idOrdenPago" />
		<s:hidden id="index"  />
		
		
		
	  <div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
			<s:text name="midas.servicio.siniestros.pagos.tituloDeta" />
			</div>
			<div id="divGenerales" style="width: 1050px;  class="floatLeft">
					<div id="contenedorFiltros" class="divContenedorO" style="width: 100%; height: 170px;">
						<div class="divFormulario" style="padding-top: 5px">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="id" name="filtroOrden.id"   cssClass="txtfield"  label="Orden de Pago Midas" labelposition="left" cssStyle="width:65px;" readonly="true" ></s:textfield>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="numOrdenCompra" name="filtroOrden.numOrdenCompra"   cssClass="txtfield"  label="Orden de Compra" labelposition="left" cssStyle="width:83px;" readonly="true" ></s:textfield>
							</div>	
							
						</div>
						
						<div  id="divProvedor" class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="tipoProveedor" name="filtroOrden.tipoProveedor"   cssClass="txtfield"  label="Tipo de Proveedor" labelposition="left" cssStyle="width:186px;" readonly="true" ></s:textfield>
							</div>	
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 30%; " >
								   <s:textfield id="proveedor" name="filtroOrden.proveedor"   cssClass="txtfield"  label="Proveedor"  labelposition="left" cssStyle="width:230px;" readonly="true" ></s:textfield>
							</div>
						</div>	
						<div  id="divBeneficiario" class="divFormulario">
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 30%; " >
								   <s:textfield id="beneficiario" name="filtroOrden.beneficiario"   cssClass="txtfield"  label="Beneficiario"  labelposition="left" cssStyle="width:230px;" readonly="true" ></s:textfield>
							</div>
						</div>
						
						
							
						<div   class="divFormulario">
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 30%; " >
								   <s:textfield id="txttipoPago" name="filtroOrden.tipoPago"   cssClass="txtfield"  label="Tipo Pago"  labelposition="left" cssStyle="width:150px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="terminoAjuste" name="filtroOrden.terminoAjuste"   cssClass="txtfield"  label="Termino Ajuste" labelposition="left" cssStyle="width:200px;" readonly="true" ></s:textfield>
							</div>				
							
						</div>
						<div   class="divFormulario">
							
							<div id="divcoberturaAfectada" class="floatLeft divInfDivInterno" style="width: 40%; " >
								   <s:textfield id="coberturaAfectada" name="filtroOrden.coberturaAfectada"   cssClass="txtfield"  label="Cobertura Afectada"  labelposition="left" cssStyle="width:200px;" readonly="true" ></s:textfield>
							</div>
							
						</div>
						
						
					</div>
			</div>
		</div>
		
		
		<div id="divInferior" style="width: 1050px !important;" class="floatLeft">
			<div class="titulo" align="left" >
				<s:text name="midas.servicio.siniestros.pagos.listadoConceptos" />
			</div>
			<div id="divGenerales" style="width: 1050px;"  class="floatLeft">
					<div id="contenedorFiltros" class="divContenedorO" style="width: 100%; height: 250px;">
					
						<div class="divFormulario" style="padding-top: 5px">
							
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:select list="motivoCancelacionMap" id="motivoCancelacion"
									name="filtroOrden.motivoCancelacion" label="Motivo de Cancelacion"
									labelposition="left" cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
								
									</s:select>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 30%; " >
								   <s:textfield id="factura" name="filtroOrden.numFactura"   cssClass="txtfield"  label="Factura"  labelposition="left" cssStyle="width:200px;" readonly="true" ></s:textfield>
							</div>
							
						</div>
						
						<div class="divFormulario" style="padding-top: 5px; height: 125px;">
							<div class="floatLeft divInfDivInterno" style="width: 50%;  " >
								<s:textarea cssStyle="width:99%;height:100px;" id="observacion" name="filtroOrden.comentarios"
									rows="10" label="Comentarios"  labelposition="left" id="txtComentarios"
									cssClass="textarea" />
							</div>
						</div>
						
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 30%; " >
								<s:textfield id="cancealadaPor" name="filtroOrden.cancealadaPor"   cssClass="txtfield"  label="Cancelada Por " labelposition="left" cssStyle="width:200px;" readonly="true" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%; " >
								<sj:datepicker name="filtroOrden.fechaCacelacion"
									labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif"
									id="txtFechaCancela"
									value="%{filtroOrden.fechaCacelacion}" maxlength="10"
									cssClass="txtfield" size="12"
									label="Fecha Cancelacion"  labelposition="left" 
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this);">
								</sj:datepicker>
							</div>
							
				
								
								
						</div>
						
						<div class="divFormulario" >
							<div class="floatLeft divInfDivInterno" style="width: 30%; "   >
							 	<s:checkbox name="filtroOrden.solicitarCancelarOrdenCompra"  id="solicitarCancelarOrdenCompra"  label="Solicitar Cancelar Orden de Compra" labelposition="right"> </s:checkbox>
							</div>
						</div>
						
					</div>
			</div>
		</div>
			
		  
		
				
				<div id="divGenerales" style="width: 1050px; padding-top: 10PX"  class="floatLeft">
				<div class="btn_back w60"
					style="display: inline; margin-left: 1%; float: right; ">
					<a href="javascript: void(0);" onclick="cerrarDetalle();">
						<s:text name="midas.boton.cerrar" /> </a>
				</div>
					
				<div id="btnRechazar" class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
				<a href="javascript: void(0);"  onclick="cancelarOrden();"> 
					<s:text	name="Cancelar"/> 
				</a>
				</div>
				
				
				
			</div>
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
	jQuery(document).ready(function() {
		iniCancelacion();
	});
	</script>
	
</div>
	</s:form>
