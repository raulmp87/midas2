<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numeroSiniestro" type="ro" width="100" sort="str" >No Siniestro</column>	
        <column id="terminoAjuste" type="ro" width="180" sort="str" >Termino de Ajuste </column>
        <column id="numOrdenCompra " type="ro" width="80" sort="int" >No Orden de Compra</column>
        <column id="importe"		type="edn"	width="80"  sort="str" format="$0,000.00">Total Orden de Compra</column>
		<column id="tipoPago" type="ro" width="150" sort="str">Tipo de Pago </column>	
		<column id="conceptoPago" type="ro" width="450" sort="str">Concepto de Pago </column>	
		<column id="días" type="ro" width="80" sort="int" >Dias de Autorizado</column>	
		<column id="numOrdenPago " type="ro" width="80" sort="int" >No Orden de Pago</column>
		<column id="proveedor " type="ro" width="180" sort="str" >Proveedor </column>
		<column id="beneficiario " type="ro" width="180" sort="str" >Beneficiario </column>  		
		<column id="estatus " type="ro" width="100" sort="str" >Estatus </column>  	
		<column id="reporteCabinaId" type="ro" width="*" sort="int" hidden="true" >reporteCabinaId</column>		
		<column id="consultarOrdenPago" type="ro" width="*" sort="str" hidden="true" >consultarOrdenPago</column>	
		<column id="editarOrdenPago" type="ro" width="*" sort="str" hidden="true" >editarOrdenPago</column>	
		<column id="cancelarOrdenPago" type="ro" width="*" sort="str" hidden="true" >cancelarOrdenPago</column>	
		<column id="registarFactura" type="ro" width="*" sort="str" hidden="true" >registarFactura</column>	
		<column id="devolverFactura" type="ro" width="*" sort="str" hidden="true" >devolverFactura</column>	
		<column id="registar" type="img" width="80" sort="na" align="center">Registrar</column>	
		<column id="consultar" type="img" width="80" sort="na" align="center">Consultar</column>	
		<column id="modificar" type="img" width="80" sort="na" align="center">Editar</column>  
		<column id="cancelar" type="img" width="80" sort="na" align="center">Cancelar</column>  
		<column id="registarFactura" type="img" width="100" sort="na" align="center">Registrar Factura</column> 
	<!-- 	<column id="registarFactura" type="img" width="100" sort="na" align="center">Devolucion Factura</column> --> 
		<column id="bloquearPago" type="img" width="100" sort="na" align="center">Bloquear Pago</column> 
  	</head>   
  	<s:set var="permisosFactura" value="false" />				
	<m:tienePermiso nombre="FN_M2_SN_REGISTRO_FACTURAS">		
		<s:set var="permisosFactura" value="true" />
	</m:tienePermiso>
  	
  	   
   <s:iterator value="listadoOrdenPagos" status="row">
		<row id="<s:property value="#row.index"/>">
			<s:if test="dias >60 && estatus!='Pagada'">	
			  	<cell style="color:red"><s:property value="numSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
				<cell style="color:red" ><s:property value="terminoAjuste" escapeHtml="false" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="totalOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>	
				<cell style="color:red"><s:property value="tipoPago" escapeHtml="false" escapeXml="true"/></cell>		
				<cell style="color:red"><s:property value="conceptoPago" escapeHtml="true" escapeXml="true"/></cell>	
				<cell style="color:red"><s:property  value="dias" escapeHtml="true" escapeXml="true"/>	</cell> 	
				<s:if test="null==estatus ||  estatus=='' || idEstatus=='5' ">
		      		<cell></cell>	
		    	</s:if>
		    	<s:else>
		      		<cell><s:property value="id" escapeHtml="true" escapeXml="true"/></cell>
		    	</s:else>
				<cell style="color:red"><s:property value="proveedor" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="beneficiario" escapeHtml="true" escapeXml="true"/></cell>
				<s:if test="idEstatus!='5'">
					<cell style="color:red"><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>
				</s:if>				
				<s:else>
					<cell></cell>	
				</s:else>	
				<cell style="color:red"><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="consultarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="editarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="cancelarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="registarFactura" escapeHtml="true" escapeXml="true"/></cell>
				<cell style="color:red"><s:property value="devolverFactura" escapeHtml="true" escapeXml="true"/></cell>		 	
			</s:if>
		  	<s:else>
			  	<cell><s:property value="numSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property value="terminoAjuste" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="totalOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>	
				<cell><s:property value="tipoPago" escapeHtml="false" escapeXml="true"/></cell>		
				<cell><s:property value="conceptoPago" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property  value="dias" escapeHtml="true" escapeXml="true"/>	</cell> 
				<s:if test="null==estatus ||  estatus=='' || idEstatus=='5' ">
		      		<cell></cell>	
		    	</s:if>
		    	<s:else>
		      		<cell><s:property value="id" escapeHtml="true" escapeXml="true"/></cell>
		    	</s:else>
				<cell><s:property value="proveedor" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="beneficiario" escapeHtml="true" escapeXml="true"/></cell>
				<s:if test="idEstatus!='5'">
					<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>
				</s:if>				
				<s:else>
					<cell></cell>	
				</s:else>
				<cell><s:property value="reporteCabinaId" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="consultarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="editarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="cancelarOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="registarFactura" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="devolverFactura" escapeHtml="true" escapeXml="true"/></cell>
		    </s:else>
			<s:if test="registrarNuevaOrdenPago == true">
		    	<s:if test="null!=id && id!=''">
		    		<cell>/MidasWeb/img/icons/ico_editar.gif^Registrar Nueva Orden de Pago^javascript: registrarOrdenPago(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>, <s:property value="id" escapeHtml="false" escapeXml="true"/>, "nueva")^_self</cell>	
		    	</s:if>
		    	<s:else>
		    		<cell>/MidasWeb/img/icons/ico_editar.gif^Registrar Nueva Orden de Pago^javascript: registrarNuevaOrdenPago(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>, "nueva")^_self</cell>	
		    	</s:else>
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>
		    </s:else>
		    
		    <s:if test="consultarOrdenPago == true">
		      	<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Orden de Pago^javascript: consultarOrdenPago(<s:property value="id" escapeHtml="false" escapeXml="true"/>, <s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>,"consultar")^_self</cell>	
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>	
		    </s:else>
		    
		     <s:if test="editarOrdenPago == true">
		      	<cell>/MidasWeb/img/icons/ico_editar.gif^Editar Orden de Pago^javascript: editarOrdenPago(<s:property value="id" escapeHtml="false" escapeXml="true"/>, <s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>,"editar")^_self</cell>	
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>
		    </s:else>
		    
		     <s:if test="cancelarOrdenPago == true">
		     	<cell>/MidasWeb/img/icons/ico_editar.gif^Cancelar Orden de Pago^javascript: cancelarOrdenPago(<s:property value="id" escapeHtml="false" escapeXml="true"/>, <s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>,"cancelar")^_self</cell>	
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>	
		    </s:else>
		    
		     <s:if test='%{registrarFactura   && #permisosFactura}'   >
		      	<cell>/MidasWeb/img/icons/ico_terminar.gif^Registar Factura^javascript: registarFactura(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		    </s:if>
		    <s:else>
		      		<cell>../img/pixel.gif</cell>
		    </s:else>
		    
		    
		     <!--    <s:if test="devolverFactura == true">
		      	<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Devolver Factura^javascript: devolverFactura(<s:property value="numOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		    </s:if>
		    <s:else>
		      	<cell>../img/pixel.gif</cell>
		    </s:else>-->
 			<s:if test=" bloquearPago == true ">
		    	<cell>/MidasWeb/img/icons/ico_editar.gif^Bloquear Orden Pago^javascript: bloquearPago(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		    </s:if><s:else>
		      	<cell>../img/pixel.gif</cell>	
		    </s:else>
		    
		</row>
	</s:iterator>
	
</rows>
   
