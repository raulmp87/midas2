<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/pagos/facturas/facturasSiniestro.js'/>"></script>


<script type="text/javascript">
	var mostrarDevolucionFactura 		= '<s:url action="mostrarDevolucionFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var guardarDevolucionFactura 		= '<s:url action="guardarDevolucionFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var cerrarRegistroFactura 			= '<s:url action="mostrar" namespace="/siniestros/pagos/pagosSinestro"/>';
	var cargarHistorialDevoluciones		= '<s:url action="cargarHistorialDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var mostrarBusquedaDevoluciones		= '<s:url action="mostrarBusquedaDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var imprimirFacturaDevolucionPath	= '<s:url action="imprimirFacturaDevolucion" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var loadInformacionDevolucion		= '<s:url action="loadInformacionDevolucion" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var editarDevolucionFactura			= '<s:url action="editarDevolucionFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	
</script>

<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}

</style>

<s:form  id="devolucionFactura">

<s:hidden id="idOrdenCompra" 	name="idOrdenCompra" />
<s:hidden id="facturaId" 		name="facturaSiniestro.id" />
<s:hidden id="bandejaPrincipal" name="bandejaPrincipal"></s:hidden>
<s:hidden id="edicion" ></s:hidden>

	<div id="altaPermiso">
		<s:include value="/jsp/siniestros/pagos/facturas/include_DetallesOrdenCompra.jsp"></s:include>
	</div>
	<br/>

   <div class="titulo"><s:text name="midas.siniestros.pagos.factura.devolucion.tituloDetalleDevolucionFactura" /></div>

   <div id="informacionDevolucion">
		<s:include value="/jsp/siniestros/pagos/facturas/include_informacion_devolucion.jsp"></s:include>
	</div>
	
	
	<br/>
	<div class="titulo"><s:text name="midas.siniestros.pagos.factura.devolucion.titulo.historialDevoluciones" /></div>
	<div id="historialDevolucionesOrdenCompra" style="width:98%;height:140px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	
	<br/>
	<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
					
					<s:if test="bandejaPrincipal==false">
							<div id="guardar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="devolverFactura();"> 
								<s:text name="midas.boton.devolver" /> 
							</a>
						</div>
					</s:if>
					
					
					
					<s:if test="bandejaPrincipal==true">
							<div id="guardar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="edicionDevolucionFactura();"> 
								<s:text name="midas.boton.devolver" /> 
							</a>
						</div>
					</s:if>
				
					<s:if test="bandejaPrincipal==false">
							<div id="cerrar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
									<a href="javascript: void(0);" onclick="cerrar();"> <s:text
											name="midas.boton.cerrar" /> 
									</a>
							</div>
					</s:if>
					
					
					
					<s:if test="bandejaPrincipal==true">
							<div id="cerrar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
									<a href="javascript: void(0);" onclick="regresarABandeja();"> <s:text
											name="midas.boton.cerrar" /> 
									</a>
							</div>
					</s:if>
					</td>							
				</tr>
			</table>	
	

</s:form>

<script type="text/javascript">
    initGridsHistorialDevoluciones();    
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>