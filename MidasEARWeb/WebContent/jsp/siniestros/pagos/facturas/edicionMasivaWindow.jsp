<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>


<sj:head/>

<script src="<s:url value='/js/midas2/siniestros/pagos/facturas/facturasSiniestro.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>" charset="ISO-8859-1"></script>


<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}

</style>
<script type="text/javascript">
	var edicionMasivaDevoluciones 		= '<s:url action="edicionMasivaDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
		
</script>

<s:form id="edicionMasivaDevoluciones" >

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.factura.devolucion.edicion.titulo.edicionMasivaDevoluciones"/>		
</div>	

<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td>
					<s:textfield id="devueltaPor"  name="facturaSiniestro.entregada" cssClass="cajaTexto w150 alphaextra consulta" 
						labelposition="left" key="midas.siniestros.pagos.factura.devolucion.facturaEntregadaA" ></s:textfield>
				</td>
			</tr>
			<tr>
				<td>
					<sj:datepicker name="facturaSiniestro.fechaEntrega"
							id="fechaEntrega" buttonImage="/MidasWeb/img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="10"
							cssClass="w100 cajaTextoM2"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.devolucion.fechaDeEntrega" labelposition="left"></sj:datepicker>
				</td>	
			</tr>
		</tbody>
	</table>
</div>
</s:form>

<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w140" style="display: inline; float: right;" >
				<a id="btn_cerrar" href="javascript: void(0);" onclick="javascript: cerrarEdicionMaivaDevoluciones();"> 
					<s:text name="midas.boton.cerrar" /> 
					<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
				</a>
			</div>	
			<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
				<a href="javascript: void(0);" onclick="javascript: guardarEdicionMasivaDevoluciones();"> 
					<s:text name="midas.boton.guardar" /> 
					<img border='0px' alt='Guardar' title='Regresar' src='/MidasWeb/img/btn_guardar.jpg'/>
				</a>
			</div>	
		</td>							
	</tr>
</table>				
