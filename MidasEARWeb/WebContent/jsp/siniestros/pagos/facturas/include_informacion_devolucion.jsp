<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<table  id="filtrosM2" width="98%">

		<tr>
		
			<td>
				<s:select list="mapMotivosDevolucion" name="facturaSiniestro.motivoDevolucion" id="motivoDevolucion" 
				key="midas.siniestros.pagos.factura.devolucion.motivoDevolucion" labelposition="left"  cssClass="cajaTexto w300 alphaextra consulta requerido" 
					headerKey="" headerValue="%{getText(midas.general.seleccione)}" ></s:select>

			</td>
			
			<td>
				<s:textfield id="factura"  name="facturaSiniestro.numeroFactura" cssClass="cajaTexto w150 alphaextra consulta requerido"  
					key="midas.siniestros.pagos.factura.registro.factura" labelposition="left" ></s:textfield>
			</td>
			
		</tr>
		
		
		<tr>
			<td colspan="2">
			<s:textarea id="comentarios"  name="facturaSiniestro.comentariosDevolucion" cssClass="consulta requerido"
			key="midas.negocio.bonocomision.sobrecomision.comentarios" labelposition="left" cols="140" rows="8" onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);" > </s:textarea>
					
			</td>
		</tr>

		
		<tr>
		
			<td>
				<s:textfield id="devueltaPor"  name="facturaSiniestro.usuarioDevolucion" cssClass="cajaTexto w150 alphaextra consulta requerido" 
					labelposition="left" key="midas.siniestros.pagos.factura.devolucion.devueltaPor" maxlength="100"></s:textfield>
			</td>
			
			<td>	
			  <s:if test="bandejaPrincipal==false">			
					<sj:datepicker name="facturaSiniestro.fechaDevolucion"
					id="fechaDevolucion" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2 requerido"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.devolucion.fechaDevolucion" labelposition="left"></sj:datepicker>
				</s:if>
					
					
					
					<s:if test="bandejaPrincipal==true">
							<s:textfield id="fechaDevolucion"  name="facturaSiniestro.fechaDevolucion" cssClass="cajaTexto w150 alphaextra consulta requerido" 
								labelposition="left" key="midas.siniestros.pagos.factura.devolucion.fechaDevolucion" ></s:textfield>
					</s:if>
			</td>
			
		</tr>
		
		
		<tr>
		
			<td>
				<s:textfield id="devueltaPor"  name="facturaSiniestro.entregada" cssClass="cajaTexto w150 alphaextra consulta edicion requerido" 
					labelposition="left" key="midas.siniestros.pagos.factura.devolucion.facturaEntregadaA" maxlength="100" ></s:textfield>
			</td>
			
			<td>
			<sj:datepicker name="facturaSiniestro.fechaEntrega"
					id="fechaEntrega" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2 requerido"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.devolucion.fechaDeEntrega" labelposition="left"></sj:datepicker>
			</td>
			
		</tr>
		
		<tr>
		
			<td>
				<div id="opcionCancelarOrdenCompra">
					<s:checkbox id="cancelarOrden" key="midas.siniestros.pagos.factura.devolucion.solicitarCancelarOC" name="facturaSiniestro.cancelarOrdenCompraStr" labelposition="rigth"></s:checkbox>
				</div>
			</td>
			
			<td>
				
			</td>
			
		</tr>
	
	</table>