<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="numeroFactura" type="ro" width="100" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.numeroFactura" /></column>
		<column id="fechaPago"  type="ro" width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.fechaPago" /></column>
		<column id="nomtoPago" type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.montoPago" /></column>
		<column id="monedaPago" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.moneda"/></column>
		<column id="tipoCambioPago"  type="ron" width="120" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.tipoCambio" /></column>		
		<column id="numeroPago" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.numeroPago" /></column>
		<column id="UUIDFactura" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.UUID" /></column>		
	</head>	  		
	<s:iterator value="listaComplementosPago">
		<row id="<s:property value="uuidCfdi"/>">
		    <cell><s:property value="numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaPagoP" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="importePagado" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="nomedaPago" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoCambioPago" escapeHtml="false"/></cell>
			<cell><s:property value="numeroParcialidad" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="uuidCfdi" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>
