<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
 <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/facturas/recepcion/recepcionComplementoPago.js'/>"></script>
<script type="text/javascript">
	var listarComplementosPago = '<s:url action="listarComplementosPago" namespace="/complementosPago/egresos"/>';
	var listarComplementosCargados = '<s:url action="listarComplementosCargados" namespace="/complementosPago/egresos"/>';
	var listarFacturasPath = '<s:url action="listarFacturas" namespace="/complementosPago/egresos"/>';
	var verDetalleValidacionesPath = '<s:url action="verDetalleValidacionComplemento" namespace="/siniestros/pagos/facturas/recepcionComplementoPago"/>';
	var obtenerOficinaPath = '<s:url action="obtenerOficina" namespace="/siniestros/pagos/facturas/recepcionComplementoPago"/>';
	var listaPrestadoresServicio = new Array();
	<s:if test="#{listaPrestadoresServicio != null && listaPrestadoresServicio.size == 0}"> 
	<s:iterator value="listaPrestadoresServicio" var="proveedorMap" status="index" >
		listaPrestadoresServicio[${index.count - 1}] = { "label": "${proveedorMap.label}", "id": "${proveedorMap.id}" } ;		
	</s:iterator>
	</s:if> 
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
</style>
<s:form id="recepcionFacturaForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoFacturas.titulo"/>	
</div>	
<s:hidden name="UUID" id="UUID"/>
<s:hidden name="origenEnvio" id="origenEnvio"/>
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar">
		<tbody>
			<tr>
				<th>
					<s:text name="midas.siniestros.pagos.facturas.complementosPago.proveedor"/>
				</th>					
			    <td>
			    	
			    	<s:hidden id="idPrestadorServicio" name="idPrestadorServicio"/>
			    	<s:if test="listaPrestadoresServicio.size == 1">
			    		<s:textfield id="nombrePrestadorServicio" disabled="true" name="nombrePrestadorServicio" cssClass="cajaTextoM2 w300" />			    	  				 
					</s:if>
					<s:else>
						<input value="${nombrePrestadorServicio}" class="txtfield" style="width: 300px" type="text" id="nombrePrestadorServicio" name="nombrePrestadorServicio" title="Comience a teclear el nombre del proveedor y seleccione una opción de la lista desplegada"/>
			    			<img src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" title="Limpiar descripción"
			             		 onclick ="mostrarMensajeConfirm('Está seguro que desea cambiar el proveedor?', '20', 'limpiarProveedor()', null, null);"/>							
					</s:else>
				</td>			   	
				<th>
					<s:text name="midas.siniestros.pagos.facturas.complementosPago.oficina"/>
				</th>
				<td>
					<s:hidden id="idOficina" name="oficinaPrestadorServicio.id"/>
					<s:textfield id="nombreOficina" disabled="true" name="oficinaPrestadorServicio.nombreOficina" cssClass="cajaTextoM2 w200" />										 
				</td>							  
	    	</tr>	    	
	 	</tbody>
	 </table>		
</div>
<div id="divFacturas">    
    <div id="indicadorFactura"></div>
	<div id="facturasListadoGrid" style="width:98%;height:180px">
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
</div>
<div id="spacer2" style="height: 15px"></div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.facturas.complementosPago.listadoComplementosPago.titulo"/>	
</div>	
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<table>
		<tr>
			<td>
				<div class="btn_back w110" id="cargar" >
					<a href="javascript: void(0);" title="Seleccione un archivo comprimido extensión ZIP que contenga las facturas a cargar (XMLs)"
					   onclick="cargarZipFacturas();">
						<s:text name="midas.agentes.cargaMasiva.cargarArchivo"/>
					</a>
				</div>
			</td>
			<td>
			<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
				<font color="#FF6600">
				<s:text name="test">El tamaño máximo del archivo puede ser 40 MB</s:text>			
				</font>
			</div>
			</td>
		</tr>		
	</table>	
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="divComplementoPago">    
   <div id="indicadorComplemento"></div>	
	<div id="complementosPagoGrid" style="width:70%;height:180px">	
	<div id="pagingArea"></div><div id="infoArea"></div>
   </div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right" style="width: 100%;">
			<tbody>
				<tr>
				<td> 
					<table>
						<tr>
							<td>
								<div style="display: width:150px; inline; float: left; color: #00a000; font-size: 10;"" id="divExcelBtn" style="float:left;">
	               					<s:text name="test">Total Facturas Cargadas:</s:text>	
	               				</div>
							</td>
							<td>
								<s:textfield id="numComplementosCargados" disabled="true" name="numComplementosCargados" cssClass="cajaTextoM2 w80" />
							</td>
						</tr>
					</table>                   
            	</td>
				</tr>
			</tbody>
	</table>		
</div>
</s:form>
<script type="text/javascript"> 
	 inicializar();
</script>
