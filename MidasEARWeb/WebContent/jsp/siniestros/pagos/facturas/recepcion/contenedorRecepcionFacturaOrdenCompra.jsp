<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
 <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/facturas/recepcion/recepcionFactura.js'/>"></script>

<script type="text/javascript">
var listarOrdenesCompraPath = '<s:url action="listarOrdenesDeCompra" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';
var listarFacturasPath = '<s:url action="listarFacturas" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';
var verDetalleValidacionesPath = '<s:url action="verDetalleValidacionFactura" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';
var validarRegistrarPath = '<s:url action="validarRegistrarFactura" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';
var obtenerOficinaPath = '<s:url action="obtenerOficina" namespace="/siniestros/pagos/facturas/recepcionFacturas"/>';

	var listaPrestadoresServicio = new Array();
	<s:if test="#{listaPrestadoresServicio != null && listaPrestadoresServicio.size == 0}"> 
	<s:iterator value="listaPrestadoresServicio" var="proveedorMap" status="index" >
		listaPrestadoresServicio[${index.count - 1}] = { "label": "${proveedorMap.label}", "id": "${proveedorMap.id}" } ;		
	</s:iterator>
	</s:if> 
	
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
</style>


<s:form id="recepcionFacturaForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.listadoOrdenesCompra.titulo"/>	
</div>	
<s:hidden name="idBatch" id="idBatch"/>
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar">
		<tbody>
			<tr>
				<th>
					<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.proveedor"/>
				</th>					
			    <td>
			    	
			    	<s:hidden id="idPrestadorServicio" name="idPrestadorServicio"/>
			    	<s:if test="listaPrestadoresServicio.size == 1">
			    		<s:textfield id="nombrePrestadorServicio" disabled="true" name="nombrePrestadorServicio" cssClass="cajaTextoM2 w300" />			    	  				 
					</s:if>
					<s:else>
						<input value="${nombrePrestadorServicio}" class="txtfield" style="width: 300px" type="text" id="nombrePrestadorServicio" name="nombrePrestadorServicio" title="Comience a teclear el nombre del proveedor y seleccione una opción de la lista desplegada"/>
			    			<img src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" title="Limpiar descripción"
			             		 onclick ="mostrarMensajeConfirm('Está seguro que desea cambiar el proveedor, se perderán aquellas facturas que aún no han sido registradas?', '20', 'limpiarProveedor()', null, null);"/>							
					</s:else>
				</td>			   	
				<th>
					<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.oficina"/>
				</th>
				<td>
					<s:hidden id="idOficina" name="oficinaPrestadorServicio.id"/>
					<s:textfield id="nombreOficina" disabled="true" name="oficinaPrestadorServicio.nombreOficina" cssClass="cajaTextoM2 w200" />										 
				</td>							  
	    	</tr>	    	
	 	</tbody>
	 </table>		
</div>
<div id="divOrdenesCompra">    
   <div id="indicador"></div>	
	<div id="ordenesCompraListadoGrid" style="width:98%;height:180px">	
	<div id="pagingArea"></div><div id="infoArea"></div>
   </div>
</div>
<div id="spacer2" style="height: 15px"></div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.listadoFacturas.titulo"/>	
</div>	
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<table>
		<tr>
			<td>
				<div class="btn_back w110" id="cargar" >
					<a href="javascript: void(0);" title="Seleccione un archivo comprimido extensión ZIP que contenga las facturas a cargar (XMLs)"
					   onclick="cargarZipFacturas();">
						<s:text name="midas.agentes.cargaMasiva.cargarArchivo"/>
					</a>
				</div>
			</td>
			<td>
			<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
				<font color="#FF6600">
				<s:text name="test">El tamaño máximo del archivo puede ser 40 MB</s:text>			
				</font>
			</div>
			</td>
		</tr>		
	</table>
	
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="divFacturas">    
    <div id="indicador"></div>
	<div id="gridFacturasPaginado">
		<div id="facturasListadoGrid" style="width:98%;height:180px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right" style="width: 100%;">
			<tbody>
				<tr>
				<td> 
					<table>
						<tr>
							<td>
								<div style="display: width:150px; inline; float: left; color: #00a000; font-size: 10;"" id="divExcelBtn" style="float:left;">
	               					<s:text name="test">Total Facturas Cargadas:</s:text>	
	               				</div>
							</td>
							<td>
								<s:textfield id="numFacturasCargadas" disabled="true" name="numFacturasCargadas" cssClass="cajaTextoM2 w80" />
							</td>
						</tr>
					</table>                   
<!--                		<div id="divExcelBtn"  class="w300" style="float:left;"> -->
<!-- 	               		<div style="display: inline; float: left; color: #00a000; font-size: 10;"> -->
<%-- 		               		<s:text name="test">Total Facturas Cargadas: <div style="display: inline;color:black;font-weight:bold; font-size: 10;"><s:textfield id="numFacturasCargadas" disabled="true" name="numFacturasCargadas" cssClass="cajaTextoM2 w100" /></div></s:text>		 --%>
<!-- 	              		 </div> -->
<!--                		</div>                    -->
            	</td>            	  
				<td>
					<s:if test="idBatch != null">
						<div align="right">
						<div class="btn_back w110" id="validarRegistrarBoton" >
							<a href="javascript: void(0);" title="Enviar facturas asociadas a validar"
								onclick="validarRegistrar();">
								<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.validarRegistrar.boton"/>
							</a>
						</div>
						</div>
					</s:if>
				</td>
				<!--	<td>
						<div align="right">
						<div class="btn_back w110" id="cancelarBoton" >
							<a href="javascript: void(0);"
								onclick="crearCitaJuridico();">
								<s:text name="midas.boton.cancelar"/>
							</a>
						</div>
						</div>
					</td>  -->
				</tr>
			</tbody>
	</table>		
</div>
</s:form>
<script type="text/javascript">
	buscarOrdenesCompra();
	 buscarFacturas(); 
	 
</script>
