<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>			
		</beforeInit>
		<column id="numSiniestro" type="ro" width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.siniestro" /></column>
		<column id="idOrdenCompra"  type="ro" width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ordenCompra" /></column>
		<column id="afectado" type="ro" width="200" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.afectado" /></column>		
		<column id="subtotal" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.subtotal" /></column>
		<column id="iva" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.iva"/></column>
		<column id="ivaRetenido"  type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ivaRetenido" /></column>		
		<column id="isr" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.isr"/></column>
		<column id="total" type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
		<column id="fechaRegistro"  type="ro" width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.fechaRegistro" /></column>
		<column id="relacionar" xmlcontent="1" type="combo_v" editable="false"  width="*" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.relacionarFactura" /> 
			<option value="">_</option>
			<s:iterator value="listaFacturasCombo" status="stats">
				<option value="<s:property value="key"/>"><s:property value="value"/></option>
		<!--  	<option value="2">Opcional Default</option>
			<option value="3">Opcional</option> -->
			</s:iterator>
		</column>				
	</head>	  		
	<s:iterator value="listaOrdenesCompra" status="stats">
		<row id="<s:property value="idOrdenCompra"/>">
		    <cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="afectado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="subtotal" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="iva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ivaRetenido" escapeHtml="false"/></cell>
			<cell><s:property value="isr" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="total" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>		
			<cell></cell>								
		</row>
	</s:iterator>	
</rows>
