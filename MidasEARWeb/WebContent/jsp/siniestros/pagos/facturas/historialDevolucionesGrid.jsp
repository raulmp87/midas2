<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.pagos.factura.devolucion.fechaDevolucion" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.pagos.factura.registro.numeroFactura" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.fuerzaventa.negocio.motivo" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.pagos.factura.devolucion.devueltaPor" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.pagos.factura.devolucion.fechaDeEntrega" />   </column>
		<column  type="ro"  width="*" align="center"> <s:text name="midas.siniestros.pagos.factura.devolucion.facturaEntregadaA" />   </column>
		<column  type="img"  width="30" align="center">  </column>
		
	</head>

	<s:iterator value="listaDevoluciones">
		
		<row id="<s:property value="#row.index"/>">
			
		    <cell><s:property value="fechaDevolcion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="motivo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="devueltaPor" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="fechaEntrega" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="entregadaA"   escapeHtml="false" escapeXml="true"/></cell>
		    <cell>../img/b_printer.gif^Imprimir^javascript:imprimir(<s:property value="idFactura" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    
		    <userdata name="idFacturaUD"><s:property value="idFactura" escapeHtml="false" escapeXml="true"/></userdata>
		</row>
	</s:iterator>
	
</rows>