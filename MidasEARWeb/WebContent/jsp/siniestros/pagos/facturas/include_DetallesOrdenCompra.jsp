<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<div class="titulo"><s:text name="midas.servicio.siniestros.valuacion.ordencompra.tituloDeta" /></div>

<table  id="filtrosM2" width="98%">
		<tr>
		
			<td>
				<s:textfield id="numeroOrdenCompra"  name="datosGralOrdenCompraDTO.numeroOrdenCompra" cssClass="cajaTexto w250 alphaextra consulta"  
					key="midas.siniestros.pagos.factura.registro.ordencompra.numeroOrdenCompra" labelposition="left" disabled="true" ></s:textfield>
			</td>
			
			<td>
				<s:textfield id="proveedor"  name="datosGralOrdenCompraDTO.proveedor" cssClass="cajaTexto w250 alphaextra consulta" 
					 key="midas.emision.auto.ppct.proveedor" labelposition="left"  disabled="true"></s:textfield>
			</td>
		
		</tr>
		
			<tr>
		
			<td>
				<s:textfield id="tipoPago"  name="datosGralOrdenCompraDTO.tipoPago" cssClass="cajaTexto w250 alphaextra consulta"
					  key="midas.siniestros.pagos.factura.registro.ordencompra.tipoPago" labelposition="left" disabled="true" ></s:textfield>
			</td>
			
			<td>
				<s:textfield id="conceptoPago"  name="datosGralOrdenCompraDTO.conceptoDePago" cssClass="cajaTexto w250 alphaextra consulta"
					  key="midas.siniestros.pagos.factura.registro.ordencompra.conceptoDePago" labelposition="left" disabled="true"></s:textfield>
			</td>
		
		</tr>
			
			
		<tr>
		
			<td>
				<s:textfield id="terminoAjuste"  name="datosGralOrdenCompraDTO.terminoDeAjuste" cssClass="cajaTexto w250 alphaextra consulta" 
					 key="midas.siniestros.depuracion.terminoAjuste" labelposition="left" disabled="true" ></s:textfield>
			</td>
			
			<td>
				<s:textfield id="coberturaAfectada"  name="datosGralOrdenCompraDTO.coberturaAfectada"  cssClass="cajaTexto w250 alphaextra consulta"  
					key="midas.emision.consulta.siniestro.coberturaafectada" labelposition="left" disabled="true" ></s:textfield>
			</td>
		
		</tr>
	
	</table>