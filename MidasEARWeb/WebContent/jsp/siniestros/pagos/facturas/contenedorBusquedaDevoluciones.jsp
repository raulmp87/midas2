<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/pagos/facturas/facturasSiniestro.js'/>"></script>


<script type="text/javascript">
	var cargarBusquedaDevoluciones 		= '<s:url action="cargarBusquedaDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var mostrarEdicionMasiva 			= '<s:url action="mostrarEdicionMasiva" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var cargarBusquedaDevoluciones 		= '<s:url action="cargarBusquedaDevoluciones" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var mostrarEdicionDevolucion 		= '<s:url action="mostrarEdicionDevolucion" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var imprimirFacturaDevolucionPath	= '<s:url action="imprimirFacturaDevolucion" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
		
</script>

<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}

</style>

<s:form  id="busquedaDevoluciones">

<s:hidden name="idOrdenCompra" id="idOrdenCompra"/>
<s:hidden id="urlRedirect" name="urlRedirect"></s:hidden>



<div class="titulo"><s:text name="midas.siniestros.pagos.factura.devolucion.tituloDetalleDevolucionFactura" /></div>
<table  id="filtrosM2" width="98%">

		<tr>
		
			<td colspan="2">
				<s:textfield id="numeroOrdenCompra"  name="filtroFactura.ordenCompra" cssClass="cajaTexto w150 alphaextra consulta"  
					key="midas.siniestros.pagos.factura.devolucion.numeroOrdenCompra" labelposition="left" ></s:textfield>
			</td>
			
			<td>
				<s:textfield id="factura"  name="filtroFactura.factura" cssClass="cajaTexto w150 alphaextra consulta"  
					key="midas.siniestros.pagos.factura.registro.numeroFactura" labelposition="left" ></s:textfield>
			</td>
			
			<td>
			<!--  	<s:textfield id="proveedor"  name="filtroFactura.proveedor" cssClass="cajaTexto w150 alphaextra consulta"  
					key="midas.emision.auto.ppct.proveedor" labelposition="left" ></s:textfield>-->
					
					<s:select id="tipoProveedorLis" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="filtroFactura.tipoProveedor"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w160"   
							  		onchange="changeTipoPrestador()"
							  	 />
							  	 
							  	 
			</td>
			
			
			<td>
				<s:select id="proveedorLis" 
										labelposition="left" 
										label="Proveedor"	
										name="filtroFactura.idBeneficiario"									
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="proveedorMap" listKey="key" listValue="value"  
								  		onchange="changeProveedor()"
								  		cssClass="txtfield cajaTextoM2 w160"   
								  		/>
			</td>
		
			
			
		</tr>
		
		<tr>
			<th> <s:text name="midas.siniestros.pagos.factura.devolucion.fechaDevolucion" /> </th>
			
			<td>
					<sj:datepicker name="filtroFactura.fechaDevolucionInicio"
					id="fechaDevolucionInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.devolucion.busqueda.desde" labelposition="left"></sj:datepicker>
			</td>
			
			<td>
					<sj:datepicker name="filtroFactura.fechaDevolucionFin"
					id="fechaDevolucionFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.endosos.cotizacionEndosoListado.fechaCotizacionHasta" labelposition="left"></sj:datepicker>
			</td>
			
			<td>
				<s:textfield id="devueltaPor"  name="filtroFactura.devueltaPor" cssClass="cajaTexto w150 alphaextra consulta" 
					labelposition="left" key="midas.siniestros.pagos.factura.devolucion.devueltaPor" ></s:textfield>
			</td>
			
		</tr>
		
		
		<tr>
			<th> <s:text name="midas.siniestros.pagos.factura.devolucion.fechaDeEntrega" /> </th>
			
			<td>
					<sj:datepicker name="filtroFactura.fechaEntregaInicio"
					id="fechaEntregaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.devolucion.busqueda.desde" labelposition="left"></sj:datepicker>
			</td>
			
			<td>
					<sj:datepicker name="filtroFactura.fechaEntregaFin"
					id="fechaEntregaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.endosos.cotizacionEndosoListado.fechaCotizacionHasta" labelposition="left"></sj:datepicker>
			</td>
			
			<td>
				<s:textfield id="entregadaPor"  name="filtroFactura.entregadaPor" cssClass="cajaTexto w150 alphaextra consulta" 
					labelposition="left" key="midas.siniestros.pagos.factura.devolucion.facturaEntregadaA" ></s:textfield>
			</td>
			
			<td>
				<s:select list="mapMotivosDevolucion" name="filtroFactura.motivoDevolucion" id="motivoDevolucion" 
				key="midas.siniestros.pagos.factura.devolucion.motivoDevolucion" labelposition="left"  cssClass="cajaTexto w120 alphaextra consulta requerido" 
					headerKey="" headerValue="%{getText(midas.general.seleccione)}" ></s:select>

			</td>
			
		</tr>

		<tr><td colspan="5">
			<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="buscarDevoluciones();"> 
								<s:text name="midas.boton.buscar" /> 
							</a>
						</div>
				
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);" onclick="limpiarFormulario();"> <s:text
										name="midas.boton.limpiar" /> 
								</a>
						</div>
					</td>							
				</tr>
			</table>	
			</td>
		</tr>
		
		
		
		
	
	</table>
	
	

	
	<br/>
	
	
	<br/>
	<div class="titulo"><s:text name="midas.siniestros.pagos.factura.devolucion.titulo.listadoBusquedaDevoluciones" /></div> 	<br/>
	<div id="busquedaDevolucionesOrdenCompra" style="width:98%;height:300px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
						<!-- div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="probarCheck();"> 
								<s:text name="midas.boton.buscar" /> 
							</a>
						</div>
				
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);" onclick="limpiarFormulario();"> <s:text
										name="midas.boton.limpiar" /> 
								</a>
						</div-->
						
							<div id="cancelar" class="btn_back w200" style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);" onclick="popUpEdicionMasiva();"> <s:text
										name="midas.siniestros.pagos.factura.devolucion.edicion.boton.edicionMasivaDevoluciones" /> 
								</a>
						</div>
					</td>							
				</tr>
			</table>	
	

</s:form>

<script type="text/javascript">
jQuery(document).ready(function(){
	 initGridsBusquedaDevoluciones();    
});
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>