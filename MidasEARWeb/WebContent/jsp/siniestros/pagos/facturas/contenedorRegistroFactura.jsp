<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script src="<s:url value='/js/midas2/siniestros/pagos/facturas/facturasSiniestro.js'/>"></script>

<script type="text/javascript">
	var guardarRegistroFactura 		= '<s:url action="guardarRegistroFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
	var cerrarRegistroFactura 		= '<s:url action="mostrar" namespace="/siniestros/pagos/pagosSinestro"/>';	
	var mostrarDevolucionFactura 	= '<s:url action="mostrarDevolucionFactura" namespace="/siniestros/pagos/facturas/facturaSiniestro"/>';
</script>
<style type="text/css">
.error {
	background-color: red;
	opacity: 0.4;
}

</style>

<s:form  id="registroFactura">

<s:hidden name="idOrdenCompra" id="idOrdenCompra" />
<s:hidden name="validacionDevolucionFactura" id="validacionDevolucionFactura" />



	<div id="altaPermiso">
		<s:include value="/jsp/siniestros/pagos/facturas/include_DetallesOrdenCompra.jsp"></s:include>
	</div>
	
	<br/><br/> <br/>


<table  id="filtrosM2" width="98%">
		<tr>
			<td class="titulo" colspan="3"><s:text name="midas.siniestros.pagos.factura.registro.tituloDetalleFactura" /></td>
		</tr>
		
		<tr>
		
			<td>
				<s:textfield id="numeroFactura"  name="facturaSiniestro.numeroFactura"
					cssClass="cajaTexto w150 alphaextra consulta requerido"  label="Numero de Factura" labelposition="left"  maxlength="100"></s:textfield>
			</td>
			
			<td>
					<sj:datepicker name="facturaSiniestro.fechaFactura"
					id="fechaFactura" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2 requerido"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.registro.fechafactura" labelposition="left"></sj:datepicker>
			</td>
			
			<td>
				<sj:datepicker name="facturaSiniestro.fechaRecepcionMatriz"
					id="fechaRecepcionMatriz" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2 requerido"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.registro.fechaRecepcionMatriz" labelposition="left" ></sj:datepicker>
			</td>
		
		</tr>
		
		<tr>
		
			<td>
				<s:select list="mapMonedaPago" id="monedaPago" name="facturaSiniestro.monedaPago" key="midas.siniestros.pagos.factura.registro.monedaPago" labelposition="left" 
				cssClass="cajaTexto w120 alphaextra consulta requerido" headerKey="" headerValue="%{getText(midas.general.seleccione)}" ></s:select>
			</td>
			
			<td>
				<s:textfield id="total"  name="facturaSiniestro.montoTotal"
					cssClass="cajaTexto w150 alphaextra consulta requerido"  label="Total" labelposition="left" onkeypress="return soloNumeros(this, event, true)" maxlength="15"></s:textfield>
			</td>
			
			<td>
					<sj:datepicker name="facturaSiniestro.fechaRegistro"
					id="fechaRegistro" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w100 cajaTextoM2 requerido"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					onblur="esFechaValida(this);" key="midas.siniestros.pagos.factura.registro.fechaRegistro" labelposition="left"></sj:datepicker>
			</td>
		
		</tr>
		
		<tr>
			
		</tr>
	
	</table>
	
	<br/><br/>
	<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
				<tr>
					<td>
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
							<a href="javascript: void(0);" onclick="guardarFactura();"> 
								<s:text name="midas.boton.guardar" /> 
							</a>
						</div>
				
						<div id="cancelar" class="btn_back w90" style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);" onclick="cerrar();"> <s:text
										name="midas.boton.cerrar" /> 
								</a>
						</div>
					</td>							
				</tr>
			</table>	

</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
validarFacturaToDevolucion();
</script>