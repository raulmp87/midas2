<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin"> 
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="tipo" type="ro" width="300" sort="str" >Tipo de Pago</column>       
		<column id="conceptoPago" type="ro" width="300" sort="str">Concepto de Pago</column>
		<column id="costoUnitario" type="edn" width="105" sort="str" format="$0,000.00" >Subtotal </column>	
		<column id="porIva" type="ro" width="100" sort="str" >%I.V.A</column>	
		<column id="iva" type="edn" width="105" sort="str" format="$0,000.00">I.V.A</column>	
		<column id="subtotal" type="ro" width="*" sort="str" hidden="true" >subtotal</column>	
		<column id="porIvaRetenido" type="ro" width="150" sort="str"  >%I.V.A Retenido</column>	
		<column id="ivaRetenido" type="edn" width="150" sort="str" format="$0,000.00" >I.V.A Retenido</column>	
		<column id="porIsr" type="ro" width="150" sort="str"  >%I.S.R</column>	
		<column id="isr" type="edn" width="150" sort="str" format="$0,000.00" >I.S.R</column>	
		<column id="importe" type="edn" width="105" sort="str" format="$0,000.00" >Total  </column>	
		
		<column id="total" type="ro" width="*" sort="str" hidden="true" >total</column>	
		<column id="descuentos" type="ro" width="*" sort="str" hidden="true" >descuentos</column>	
		<column id="deducible" type="ro" width="*" sort="str" hidden="true" >deducible</column>	
		<column id="totalPagar" type="ro" width="*" sort="str" hidden="true" >totalPagar</column>	
		<column id="salvamento" type="ro" width="*" sort="str" hidden="true" >salvamento</column>	
		<column id="salvamentoPorIva" type="ro" width="*" sort="str" hidden="true" >salvamentoPorIva</column>
		<column id="totalSalvamento" type="ro" width="*" sort="str" hidden="true" >totalSalvamento</column>	
		<column id="idDetalleOrdenCompra" type="ro" width="*" sort="str" hidden="true" >idDetalleOrdenCompra</column>	
		<column id="idDetalleOrdenPago" type="ro" width="*" sort="str" hidden="true" >idDetalleOrdenPago</column>	
		<column id="totalesPagados" type="ro" width="*" sort="str" hidden="true" >totalesPagados</column>	
		<column id="totalesPorPagar" type="ro" width="*" sort="str" hidden="true" >totalesPorPagar</column>	
		<column id="deduciblesTotal" type="ro" width="*" sort="str" hidden="true" >deduciblesTotal</column>	
		<column id="index" type="ro" width="*" sort="str" hidden="true" >index</column>	
		<column id="importePagado" type="ro" width="*" sort="str" hidden="true" >importePagado</column>	
		<column id="reserva" type="ro" width="*" sort="str" hidden="true" >reserva</column>	
		<column id="reservaDisponible" type="ro" width="*" sort="str" hidden="true" >reservaDisponible</column>	
			
	</head>
	
	<s:iterator value="desglosePagoConceptoDTO" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="tipoOrden" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="conceptoPago" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="costoUnitario" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="porIva" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="iva" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="subtotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="porIvaRetenido" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="ivaRetenido" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="porIsr" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="isr" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="importe" escapeHtml="true" escapeXml="true"/></cell>	
		
			<cell><s:property value="total" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="descuentosTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="deduciblesTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totalPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="salvamento" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="salvamentoPorIva" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totalSalvamento" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="idDetalleOrdenCompra" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="idDetalleOrdenPago" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totalesPagados" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totalesPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="deduciblesTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="#row.index" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="importePagado" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="reserva" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="reservaDisponible" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>