<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/pagos/ordenPago.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript">
	var mostrarHistoricoPath = '<s:url action="mostrarHistoricoRobos" namespace="/siniestros/cabina/reportecabina/historicoRobos"/>';
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}

.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	background-color: #FFCCCC;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}

.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel {
	float: left;
	position: relative;
	margin-top: 12px;
}

.divInfDivInterno {
	width: 17%;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	
</script>
<s:form id="definirOrdenPagoForm" class="floatLeft">
	<div id="contenido_DefinirOrden"
		style="width: 99%; position: relative;">
		<s:hidden id="idReporteCabina" name="idReporteCabina" />
		<s:hidden id="soloLectura" name="soloLectura" />
		<s:hidden id="tipoOrdenPago" name="filtroOrden.tipoOrdenPago" />
		<s:hidden id="cveTipoOrdenPago" name="filtroOrden.cveTipoOrdenPago" />
		<s:hidden id="idCobertura" name="idCobertura" />
		<s:hidden id="idOrdenCompra" name="idOrdenCompra" />
		<s:hidden id="modoPantalla" name="modoPantalla" />
		<s:hidden id="idOrdenPago" name="idOrdenPago" />
		<s:hidden id="idDetalleOrdenPago"
			name="deglosePorConcepto.idDetalleOrdenPago" />
		<s:hidden id="idDetalleOrdenCompra"
			name="deglosePorConcepto.idDetalleOrdenCompra" />
		<s:hidden id="index" />
		<div id="divInferior" style="width: 1050px !important;"
			class="floatLeft">
			<div class="titulo" align="left">
				<s:text name="midas.servicio.siniestros.pagos.tituloDeta" />
			</div>
			<div id="divGenerales" style="width: 1050px;"floatLeft">
				<div id="contenedorFiltros" class="divContenedorO"
					style="width: 100%; height: 100px;">
					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="id" name="filtroOrden.id" cssClass="txtfield"
								label="Orden de Pago" labelposition="left"
								cssStyle="width:65px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="numOrdenCompra"
								name="filtroOrden.numOrdenCompra" cssClass="txtfield"
								label="Orden de Compra" labelposition="left"
								cssStyle="width:83px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="numSiniestro" name="filtroOrden.numSiniestro"
								cssClass="txtfield" label="No Siniestro" labelposition="left"
								cssStyle="width:120px;" readonly="true"></s:textfield>
						</div>
					</div>
					
					<div class="divFormulario">
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="terminoAjuste" name="filtroOrden.terminoAjuste"
								cssClass="txtfield" label="Termino Ajuste" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="txttipoPago" name="filtroOrden.tipoPago"
								cssClass="txtfield" label="Tipo Pago" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						
						
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="txttipoPagoDescripcion"
								name="filtroOrden.tipoPagoDescripcion" cssClass="txtfield"
								label="Pago a" labelposition="left" cssStyle="width:200px;"
								readonly="true"></s:textfield>
						</div>
					
					</div>
					
					<div class="divFormulario">

						<div id="divcoberturaAfectada" class="floatLeft divInfDivInterno"
							style="width: 30%;">
							<s:textfield id="coberturaAfectada"
								name="filtroOrden.coberturaAfectada" cssClass="txtfield"
								label="Cobertura Afectada" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						<div id="divestatus" class="floatLeft divInfDivInterno"
							style="width: 30%;">
							<s:textfield id="estatus" name="filtroOrden.estatus"
								cssClass="txtfield" label="Estatus" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
						<div id="divOrigenPago"  class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 10px">
							<s:if test="soloLectura == true">
								<s:select list="pagoaMap" id="origenPagoLis"
									name="filtroOrden.origenPago" label="Origen del Pago "
									labelposition="left" cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%; " disabled="true">
								</s:select>
							</s:if>
							<s:else>
								<s:select list="pagoaMap" id="origenPagoLis"
									name="filtroOrden.origenPago" label="Origen del Pago "
									labelposition="left" cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%; ">
								</s:select>
							</s:else>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
		<div id="divContenedorBeneficiario" style="width: 1050px !important;"
			class="floatLeft">
			<div class="titulo" id="tituloPago" align="left" >
				<s:text   name="Datos de Pago"/>
			</div>

				<div id="contenedorBeneficiarios" class="divContenedorO"
					style="width: 100%; height: 120px;">
					<div id="divProvedor" >
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="tipoProveedor" name="filtroOrden.tipoProveedor"
									cssClass="txtfield" label="Tipo de Proveedor"
									labelposition="left" cssStyle="width:186px;" readonly="true"></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="proveedor" name="filtroOrden.proveedor"
									cssClass="txtfield" label="Proveedor" labelposition="left"
									cssStyle="width:230px;" readonly="true"></s:textfield>
							</div>
						</div>
						
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="rfc" name="filtroOrden.rfc"
									cssClass="txtfield" label="R.F.C"
									labelposition="left" cssStyle="width:186px;" readonly="true"></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="curp" name="filtroOrden.curp"
									cssClass="txtfield" label="C.U.R.P" labelposition="left"
									cssStyle="width:230px;" readonly="true"></s:textfield>
							</div>
						</div>
					</div>
					
					<div id="divBeneficiario" >
						<div  class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="beneficiario" name="filtroOrden.beneficiario"
									cssClass="txtfield" label="Beneficiario" labelposition="left" readonly="true"
									cssStyle="width:230px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="tipoPersona" name="filtroOrden.tipoPersona"
									cssClass="txtfield" label="Tipo Persona" labelposition="left" readonly="true"
									cssStyle="width:230px;" ></s:textfield>
							</div>
						</div>
						<div class="divFormulario">
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="rfc" name="filtroOrden.rfc"
									cssClass="txtfield" label="R.F.C"
									labelposition="left" cssStyle="width:186px;" readonly="true"></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;">
								<s:textfield id="curp" name="filtroOrden.curp"
									cssClass="txtfield" label="C.U.R.P" labelposition="left"
									cssStyle="width:230px;" readonly="true"></s:textfield>
							</div>
						</div>
						
						
						<div class="divFormulario"   >
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="banco" name="filtroOrden.bancoReceptor"   cssClass="txtfield"  readonly="true" label="Banco Receptor" labelposition="left"   cssStyle="width:150px;" ></s:textfield>
							</div>
								
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
								<s:textfield id="clabe" name="filtroOrden.clabe"   cssClass="txtfield"  readonly="true" label="CLABE" labelposition="left"   cssStyle="width:150px;" ></s:textfield>
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="correo" name="filtroOrden.correo"   cssClass="txtfield"  readonly="true" label="Correo" labelposition="left" cssStyle="width:150px;"  ></s:textfield>
							</div>
							
							
							<div class="floatLeft divInfDivInterno" style="width: 20%;" >
								<s:textfield id="telefono" name="filtroOrden.telefono"   cssClass="txtfield"  label="Telefono" labelposition="left"   readonly="true"  maxlength="8"  cssStyle="width:80px;"  ></s:textfield>
							</div>
							
						</div>
						
						
						
						
					
					</div>
					
					
					
					
					
					
					
					
					
					
				</div>
		</div>
		<div id="divFactura" style="width: 1050px !important;"
			class="floatLeft">

			<div class="titulo" align="left">
				<s:text name="Datos de la factura" />
			</div>

			<div id="divDatosFactura" style="width: 1050px;"floatLeft">
				<div id="contenedorFactura" class="divContenedorO"
					style="width: 100%; height: 120px;">

					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 25px">
							<s:textfield id="numFactura" name="filtroOrden.numFactura"
								cssClass="txtfield" label="No Factura" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 50px">
							<s:textfield id="fechaFactura" name="filtroOrden.fechaFactura"
								cssClass="txtfield" label="Fecha Factura" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>

					</div>

					<div class="divFormulario">
						<div class="floatLeft divInfDivInterno" style="width: 30%;">
							<s:textfield id="moneda" name="filtroOrden.monedaPago"
								cssClass="txtfield" label="Moneda de Pago" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 25px">
							<s:textfield id="fechaRecepcionMatriz"
								name="filtroOrden.fechaRecepcionMatriz" cssClass="txtfield"
								label="Fecha Recepcion Matriz" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>


					</div>
					<div class="divFormulario">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 20px"" >
							<s:textfield id="montoFactura" name="filtroOrden.montoFactura"
								cssClass="txtfield formatCurrency" label="Monto Factura" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 50px"" >
							<s:textfield id="fechaRegistroFactura"
								name="filtroOrden.fechaRegistroFactura" cssClass="txtfield"
								label="Fecha Registro" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div id="divInferior" style="width: 1050px !important;"
			class="floatLeft">
			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="midas.servicio.siniestros.pagos.listadoConceptos" />
			</div>
			<div id="conceptosGrid" style="width: 99%; height: 200px;"></div>
			<div id="pagingArea" style="padding-top: 8px"></div>
			<div id="infoArea"></div>
		</div>

		<div id="divInferior" style="display:none;width: 1050px !important;"
			class="floatLeft">
			<div class="titulo" align="left">
				<s:text name="midas.servicio.siniestros.pagos.DesglosePagos" />
			</div>

			<div id="divConcepto" style="width: 1050px;"floatLeft">
				<div id="contenedorConcepto" class="divContenedorO"
					style="width: 100%; height: 290px;">

					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtConcepto"   disabled="true" 
								name="deglosePorConcepto.conceptoPago" cssClass="txtfield"
								label="Concepto de Pago" labelposition="left"
								cssStyle="width:200px;" readonly="true"></s:textfield>
						</div>
					</div>

					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 55px">
							<s:textfield id="txtCostoUnitario"
								name="deglosePorConcepto.subtotal" cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)"  disabled="true"
								label="SubTotal" labelposition="left" cssStyle="width:80px;"
								readonly="true"></s:textfield>
						</div>
					</div>
					
					<div class="divFormulario" style="padding-top: 5px" id="divIVAS">
					
						<div class="floatLeft divInfDivInterno"
							style="width: 15%; padding-left: 55px">
							<s:if test="soloLectura == true">
								<s:textfield id="txtPorcIva" name="deglosePorConcepto.porIva"
									cssClass="txtfield" 
									onkeypress="return soloNumeros(this, event, true)"
									label="% I.V.A " readonly="true" labelposition="left" disabled="true"  
									cssStyle="width:80px;"></s:textfield>
							</s:if>
							<s:else>
								<s:textfield id="txtPorcIva" name="deglosePorConcepto.porIva"
									cssClass="txtfield" 
									onkeypress="return soloNumeros(this, event, true)"
									label="% I.V.A " labelposition="left" readonly="true" cssStyle="width:80px;" disabled="true"></s:textfield>
							</s:else>
						</div>
						
						<div class="floatLeft divInfDivInterno"
							style="width: 13%; ">
							<s:textfield id="txtIva" name="deglosePorConcepto.iva"
								cssClass="txtfield formatCurrency" label="I.V.A "
								onkeypress="return soloNumeros(this, event, true)"
								labelposition="left" cssStyle="width:80px;" readonly="true" disabled="true"></s:textfield>
						</div>
						
						
						<div class="floatLeft divInfDivInterno"
							style="width: 18%; ">
							<s:if test="soloLectura == true">
								<s:textfield id="txtPorIvaRetenido"
									name="deglosePorConcepto.porIvaRetenido"
									onkeypress="return soloNumeros(this, event, true)"
									cssClass="txtfield" label="% I.V.A Retenido" readonly="true"
									labelposition="left" cssStyle="width:80px;" disabled="true"></s:textfield>
							</s:if>
							<s:else>
								<s:textfield id="txtPorIvaRetenido"
									name="deglosePorConcepto.porIvaRetenido"
									onkeypress="return soloNumeros(this, event, true)"
									cssClass="txtfield" label="% I.V.A Retenido"
									labelposition="left" readonly="true" cssStyle="width:80px;" disabled="true"></s:textfield>
							</s:else>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 17%; ">
							<s:textfield id="txtIvaRetenido"
								name="deglosePorConcepto.ivaRetenido" cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)"
								label="I.V.A Retenido" labelposition="left"
								cssStyle="width:80px;" readonly="true"  disabled="true"></s:textfield>
						</div>
						
						
						
						<div class="floatLeft divInfDivInterno"
							style="width: 15%;">
							<s:if test="soloLectura == true">
								<s:textfield id="txtPorIsr" name="deglosePorConcepto.porIsr"
									cssClass="txtfield" 
									onkeypress="return soloNumeros(this, event, true)"
									label="%I.S.R" labelposition="left" readonly="true"
									cssStyle="width:80px;"  disabled="true" ></s:textfield>
							</s:if>
							<s:else>
								<s:textfield id="txtPorIsr" name="deglosePorConcepto.porIsr"
									cssClass="txtfield" 
									onkeypress="return soloNumeros(this, event, true)"
									label="%I.S.R" labelposition="left"  readonly="true"  disabled="true" cssStyle="width:80px;"></s:textfield>
							</s:else>
						</div>
						
						<div class="floatLeft divInfDivInterno"
							style="width:13%; ">
							<s:textfield id="txtIsr" name="deglosePorConcepto.isr"
								cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)" label="I.S.R"
								labelposition="left" cssStyle="width:80px;" readonly="true"  disabled="true"></s:textfield>
						</div>
					</div>
					
					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 13%; padding-left: 75px">
							<s:textfield id="txtTotal" name="deglosePorConcepto.total"
								cssClass="txtfield formatCurrency" disabled="true"
								onkeypress="return soloNumeros(this, event, true)" label="Total"
								labelposition="left" cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
					</div>
					
					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 25px">
							<s:textfield id="txtImportePagado" disabled="true"
								name="deglosePorConcepto.importePagado" cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)"
								label="Importe Pagado" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>

					</div>
					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 25px">
							<s:textfield id="txtTotalPagar"  disabled="true"
								name="deglosePorConcepto.totalPagar" cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)"
								label="Total por Pagar" labelposition="left"
								cssStyle="width:80px;" readonly="true"></s:textfield>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<div id="divDetalleTotales" style="width: 1050px height :   220px;"
			class="floatLeft">

			<div class="titulo" align="left">
				<s:text name="midas.servicio.siniestros.pagos.totalPagar" />
			</div>

			<div id="divTotales" style="width: 1050px;"floatLeft">
				<div id="contenedorTotales" class="divContenedorO"
					style="width: 100%; height: 160px;">
					<div id="aplicaDeducibles" class="divFormulario"
						style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtAplicaDeducible"
								name="filtroOrden.aplicaDeducible" cssClass="txtfield"
								label="Aplica Deducible" labelposition="left"
								cssStyle="width:50px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtDeduciblesTotal"
								name="deglosePorConcepto.deduciblesTotal" cssClass="txtfield formatCurrency"
								label="Deducible" labelposition="left" cssStyle="width:100px;"
								readonly="true"></s:textfield>
						</div>
					</div>

					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtTotalesPorPagar"
								name="deglosePorConcepto.totalesPorPagar" cssClass="txtfield formatCurrency"
								label="Total por Pagar" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtTotalesPagados"
								name="deglosePorConcepto.totalesPagados" cssClass="txtfield formatCurrency"
								label="Total Pagado" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtDescuentos"
								name="deglosePorConcepto.descuentosTotal" cssClass="txtfield formatCurrency"
								label="Descuentos" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>
						
					</div>

					<div class="divFormulario" style="padding-top: 5px">
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtPago" name="filtroOrden.pago"
								cssClass="txtfield formatCurrency" label="Pago" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtReserva" name="deglosePorConcepto.reserva"
								cssClass="txtfield formatCurrency" label="Reserva" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>						
					</div>
					<div class="divFormulario" style="padding-top: 5px">					
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">							
							<s:textfield id="txtSalvamento"
								name="filtroOrden.salvamento" cssClass="txtfield formatCurrency"								
								onkeypress="return soloNumeros(this, event, true)"
								label="Salvamento" labelposition="left" readonly="true"
								cssStyle="width:100px;"></s:textfield>							
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">							
								<s:textfield id="txtSalvamentoPorIva"
									name="filtroOrden.ivaSalvamento"
									cssClass="txtfield"
									onkeypress="return soloNumeros(this, event, true)"
									readonly="true" label="% Salvamento" labelposition="left"
									cssStyle="width:80px;"></s:textfield>
						</div>
						<div class="floatLeft divInfDivInterno"
							style="width: 30%; padding-left: 8px">
							<s:textfield id="txtTotalSalvamento"
								name="filtroOrden.totalSalvamento" cssClass="txtfield formatCurrency"
								onkeypress="return soloNumeros(this, event, true)"
								label="Total Salvamento" labelposition="left"
								cssStyle="width:100px;" readonly="true"></s:textfield>
						</div>
					</div>					
				</div>
			</div>
		</div>



		<div id="divInferior" style="width: 1050px !important;"
			class="floatLeft">
			<div class="btn_back w60"
				style="display: inline; margin-left: 1%; float: right;">
				<a href="javascript: void(0);" onclick="cerrarDetalle();"> <s:text
						name="midas.boton.cerrar" /> </a>
			</div>

			<div id="btnRechazar" class="btn_back w80"
				style="display: inline; margin-left: 2%; float: right;">
				<a href="javascript: void(0);" onclick="rechazar();"> <s:text
						name="Cancelar" /> </a>
			</div>

			<div id="btnAutorizar" class="btn_back w100 h80"
				style="display: inline; margin-left: 2%; float: right;">
				<a href="javascript: void(0);" onclick="autorizar();"> <s:text
						name="Autorizar" /> </a>
			</div>



			<div class="btn_back w190"
				style="display: inline; margin-left: 2%; float: right;">
				<a href="javascript: void(0);"
					onclick="mostrarHistoricoMovimientos();"> <s:text
						name="Historicos de Movimientos" /> </a>
			</div>


		</div>
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
		<script>
	jQuery(document).ready(function() {
		iniContenedoDetarGrid();
		initCurrencyFormatOnTxtInput();
	});
	</script>

	</div>
</s:form>
