<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>

		<column id="nombreAgrupador" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.companias.nombreAgrupador" /></column>
		<column id="numSiniestro" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.companias.siniestro" /></column>
		<column id="oficina" type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.companias.oficina" /></column>
		<column id="compania"  type="ro" width="*" sort="str" align="center"><s:text name="midas.siniestros.pagos.companias.compania" /></column>
		<column id="siniestroTercero"  type="ro" width="*" sort="str" align="center">Siniestro Tercero</column>
		<column id="total"  type="ron" width="*" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.companias.total" /></column>
		<column id="fechaRegistro"  type="ro" width="*" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.companias.fechaRegistro"/></column>
		<column  type="img"   width="100" sort="na" align="center" >Acciones</column>
		


	</head>
	<s:iterator value="resultados" status="stats">
		<row id="<s:property value="id"/>">
			<cell><s:property value="nombreAgrupador" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
	    	<cell><s:property value="oficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="proveedor.personaMidas.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="siniestroTercero" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="montoTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultar(<s:property value="id"/>)^_self</cell>	

		</row>
	</s:iterator>
</rows>
