<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/companias/agrupadorOrdenCompraCompania.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>


<script type="text/javascript">

  var buscarAgrupadoresPath = '<s:url action="buscarAgrupadores" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
  var mostrarRegistroPath = '<s:url action="mostrarRegistro" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
  var mostrarBusquedaPath = '<s:url action="mostrarBusqueda" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';


</script>

<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

	.tituloInterno {
    background-image: url("../img/line_green.gif");
    background-position: left bottom;
    background-repeat: no-repeat;
    font-size: 11px;
    font-weight: bold;
    padding-bottom: 5px;
}
</style>


<style type="text/css">
      #superior{
            height: 230px;
            width: 98%;
      }

      #superiorI{
            height: 100%;
            width: 100%;
            position: relative;
            float:left;
            margin-left: 1%;
      }
      
      #superiorD{
            height: 150px;
            width: 250px;
            position: relative;
            float:left;
      }
      
      #SIS{
            margin-top:1%;
            width: 100%;
            position: relative;
            float:left;
      }
      
      #SII{
            height: 60px;
            margin-top:0.5%;
            width: 100%;
            position: relative;
            float:left;
      }
      
      #SIN{
            height: 60px;
            margin-top:0.5%;
            width: 100%;
            position: relative;
            float:left;
      }
      
      #SDS{
            margin-top:1%;
            height: 25%;
            width: 100%;
            position: relative;
            float:left;
      }
      
      #SDI{
            height: 60px;
            width: 100%;
            margin-top:9%;
            position: relative;
            float:left;
      }
      
      div.ui-datepicker {
            font-size: 10px;
      }
      .divContenedorO {
            border: 1px solid #28b31a;
            background-color: #EDFAE1;
      }
      
      .divContenedorU {
            border-bottom: 1px solid #28b31a;
            border-right: 1px solid #28b31a;
      }
      
      .floatLeft {
            float: left;
            position: relative;
      }

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
      
      
      
<div class="titulo" style="width: 98%;">
      <s:text name="midas.siniestros.pagos.companias.busquedaTitulo"/>    
</div>      
      <div id="superior" class="divContenedorO">
            <form id="buscarAgrupadoresForm">
                  <div id="superiorI">
                        <div id="SIS">
                             <div class="floatLeft" style="width: 18%;">
                                <s:textfield id="t_nombreAgrupador" label="%{getText('midas.siniestros.pagos.companias.nombreAgrupador')}" name="filtroAgrupador.nombreAgrupador"
                                    cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
                              </div>
                             <div class="floatLeft" style="width: 18%;">
                                <s:textfield id="t_siniestro" label="%{getText('midas.siniestros.pagos.companias.siniestro')}" name="filtroAgrupador.numeroSiniestro"
                                    cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
                              </div>
                              <div class="floatLeft" style="width: 18%;">
                                <s:select list="oficinasMap" cssStyle="width: 90%;"
                                  name="filtroAgrupador.idOficina" 
                                  label="%{getText('midas.siniestros.pagos.companias.oficina')}"
                                  cssClass="cleaneable txtfield cajaTextoM2 obligatorio"
                                  headerKey="" headerValue="%{getText('midas.general.seleccione')}">
                                </s:select>
                              </div>
                              <div class="floatLeft" style="width: 18%;">
								<div id="wwlbl_t_nombreAgrupador" class="wwlbl">
									<label for="t_nombreAgrupador" class="label">
									<s:text name="%{getText('midas.siniestros.pagos.companias.compania')}" />:</label>
								</div> 
                                <br>
                                <div id="wwctrl_t_nombreAgrupador" class="wwctrl">
								<input 
									value="${companiaAutoComplete}"  
									class="txtfield obligatorio" style="width: 390px" type="text" id="companiaAutoComplete" 
									name="companiaAutoComplete" />
							    </div>                               
								<s:hidden id="companiaId" name="filtroAgrupador.compania" />
								<s:hidden id="nombreCompletoCia" name="nombreCompletoCia" />
                              </div>

                        </div>

                        <div id="SIS">
                             <div class="floatLeft" style="width: 18%;">
                                <s:textfield id="t_totalDe" label="%{getText('midas.siniestros.pagos.companias.totalDe')}" name="filtroAgrupador.totalDe"
                                    cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
                              </div>
                             
                             <div class="floatLeft" style="width: 18%;">
                                <s:textfield id="t_totalHasta" label="Total Hasta" name="filtroAgrupador.totalHasta"
                                    cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
                              </div>

                              <div class="floatLeft" style="width: 18%;">
                                         <label><s:text
                                                     name="%{getText('midas.siniestros.pagos.companias.fechaRegistroDe')}" />:</label>
                                         <div style="margin-top: 8%">
                                               <sj:datepicker name="filtroAgrupador.fechaRegistroDe"
                                                     changeMonth="true" changeYear="true"
                                                     buttonImage="/MidasWeb/img/b_calendario.gif"
                                                     buttonImageOnly="true" id="fechaDe" maxlength="10" 
                                                     onkeypress="return soloFecha(this, event, false);"
                                                     onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
                                                     cssClass="cleaneable txtfield obligatorio">
                                               </sj:datepicker>
                                         </div>
                             </div>
                             <div class="floatLeft" style="width: 18%;">
                                         <label><s:text
                                                     name="%{getText('midas.siniestros.pagos.companias.fechaRegistroHasta')}" />:</label>
                                         <div style="margin-top: 8%">
                                               <sj:datepicker name="filtroAgrupador.fechaRegistroHasta"
                                                     changeMonth="true" changeYear="true"
                                                     buttonImage="/MidasWeb/img/b_calendario.gif"
                                                     buttonImageOnly="true" id="fechaHasta" maxlength="10" 
                                                     onkeypress="return soloFecha(this, event, false);"
                                                     onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
                                                     cssClass="cleaneable txtfield obligatorio">
                                               </sj:datepicker>
                                         </div>
                             </div>
                             <div class="floatLeft" style="width: 18%;">
                                <s:textfield id="t_siniestroTercero" label="Siniestro Tercero" name="filtroAgrupador.siniestroTercero"
                                    cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
                              </div>

                        </div>
                        <div id="SIS">
                         	<div class="floatLeft" style="width: 18%;">
		                        <s:select
								name="filtroAgrupador.tipoAgrupador" 
								key="midas.siniestros.pagos.companias.tipoAgrupador"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="catTipoAgrupador" listKey="key" listValue="value"
						  		cssClass="txtfield cajaTextoM2 setNew w170 obligatorio" />
					  		</div>
                             
                             <div id="b_nuevo" class="btn_back w120" style="display: inline; margin-top:5%; margin-right: 2%; margin-bottom: 1%; float: right;">
                                   <a href="javascript: void(0);" onclick="mostrarRegistro();"> <s:text name="Nuevo" /> </a>
                             </div>
                             
                             <div id="b_limpiar" class="btn_back w120" style="display: inline; margin-top:5%;  margin-right: 2%; margin-bottom: 1%; float: right;">
                                   <a href="javascript: void(0);" onclick="limpiarFormulario();"> <s:text name="Limpiar" /> </a>
                             </div>
                             
                             <div id="b_buscar" style="display: inline; margin-top:5%;  margin-right: 2%; margin-bottom: 1%; float: right; width: 120px; ">
                                   <a href="javascript: void(0);" onclick="buscarAgrupador();"> <s:text name="Buscar" /> </a>
                             </div>

                        </div>

                  </div>
            </form>
      </div>
      
      <br />
      <br />
      <br />
      
      <div class="titulo" style="width: 98%;">
            <s:text name="midas.siniestros.pagos.companias.listado"/>      
      </div>

      <div id="indicador"></div>
      <div id="listadoGridAgrupadoresGrid" style="width: 99%; height: 380px;"></div>
      <div id="pagingArea"></div>
      <div id="infoArea"></div>


</div>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
jQuery(document).ready(function(){
      cargaGridVacio();
      habilitaAutoComplete();
});
</script>

