<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>

		<column id="seleccion"           type="ch"  width="27" align="center">#master_checkbox</column>
		<column id="numSiniestro"        type="ro"  width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.siniestro" /></column>
		<column id="numSiniestroTercero" type="ro"  width="100" sort="int" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.siniestroTercero" /></column>
		<column id="oficina"             type="ro"  width="120" sort="int" align="center"><s:text name="midas.siniestros.pagos.companias.oficina" /></column>
		<column id="idOrdenCompra"       type="ro"  width="90" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ordenCompra" /></column>
		<column id="afectado"            type="ro"  width="200" sort="int" align="center"><s:text name="midas.siniestros.pagos.notasCredito.afectado" /></column>
		<column id="fechaCarta"          type="ro"  width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.fechaCarta" /></column>
		<column id="fechaRecepcionCarta" type="ro"  width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.fechaCartaRecepcion" /></column>
		<column id="paseAtencion"        type="ro"  width="80"  sort="str"><s:text name="%{'midas.siniestros.pagos.facturas.recepcionFacturas.folioPaseAtencion'}"/> </column>
		<column id="reserva"               type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.reservaDisponible"/></column>
		<!--<column id="subtotal" type="ron" width="100" sort="int" format="$0,000.00"  hidden="true" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.subtotal" /></column>
		<column id="iva" type="ron" width="100" sort="int" format="$0,000.00"  hidden="true" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.iva"/></column>
		<column id="ivaRetenido"  type="ron" width="100" sort="int" format="$0,000.00"  hidden="true" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.ivaRetenido" /></column>
		<column id="isr" type="ron" width="100" sort="int" format="$0,000.00"  hidden="true" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.isr"/></column>-->
		<column id="total"               type="ron" width="100" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
		<column id="fechaRegistro"       type="ro"  width="100" sort="date_custom" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.fechaRegistro" /></column>
		

	</head>
	<s:iterator value="listaOrdenesCompra" status="stats">
		<row id="<s:property value="idOrdenCompra"/>">
			<cell></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="siniestroTercero" escapeHtml="false" escapeXml="true"/></cell>
	    	<cell><s:property value="oficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="afectado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCarta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRecepcionCarta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="folioPaseAtencion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="reservaDisponible" escapeHtml="false" escapeXml="true"/></cell>
			<!-- <cell><s:property value="subtotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="iva" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ivaRetenido" escapeHtml="false"/></cell>
			<cell><s:property value="isr" escapeHtml="false" escapeXml="true"/></cell> -->
			<cell><s:property value="total" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>
