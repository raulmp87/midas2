<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/pagos/companias/agrupadorOrdenCompraCompania.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>

<script type="text/javascript">

var listarOrdenesCompraPath = '<s:url action="buscarOrdenesCompra" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
var guardarPath = '<s:url action="guardar" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
var eliminarPath = '<s:url action="eliminar" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
var mostrarRegistroPath = '<s:url action="mostrarRegistro" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
var mostrarPantallaBusquedaPath = '<s:url action="mostrarBusqueda" namespace="/siniestros/pagos/companias/recepcionFacturas"/>';
</script>

<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

	.tituloInterno {
    background-image: url("../img/line_green.gif");
    background-position: left bottom;
    background-repeat: no-repeat;
    font-size: 11px;
    font-weight: bold;
    padding-bottom: 5px;
}
</style>

<s:form id="recepcionFacturaForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.companias.titutlo"/>
</div>
<input id="h_idAgrupador" value="<s:property value='factura.id' />" type="hidden"/>
<s:hidden id="esError" name="esError" />
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<th>
					<s:text name="midas.siniestros.pagos.companias.proveedor"/>
				</th>
			    <td>
					<input 
							value="${nombreCompletoCia}"  
							class="txtfield escondible" style="width: 400px" type="text" id="companiaAutoComplete" 
							name="companiaAutoComplete" 
					/>
					<s:hidden id="companiaId" name="companiaSeleccionada" />	
					<s:hidden id="nombreCompletoCia" name="nombreCompletoCia" />													
				</td>
				<th>
					<s:text name="midas.siniestros.pagos.companias.oficina"/>
				</th>
				<td>
					<s:select id="s_oficinasList"
										labelposition="left"
										name="oficinaSeleccionada"
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="oficinasMap" listKey="key" listValue="value"
								  		cssClass="txtfield cajaTextoM2 w320 escondible"
								  		/>
				</td>
	    	</tr>
	    	<tr>
	    		<td>
	    			<s:select id="s_tipoAgrupadorList"
								name="filtroAgrupador.tipoAgrupador" 
								key="midas.siniestros.pagos.companias.tipoAgrupador"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="catTipoAgrupador" listKey="key" listValue="value"
						  		cssClass="txtfield cajaTextoM2 setNew w170 escondible" />
	    		</td>
	    		<td colspan="3">
	    			<!--<s:if test="factura == null">-->
		    			<div id="guardarBtn" class="btn_back w90 escondible"
										style="display: inline; margin-right: 1%; float: right;">
										<a href="javascript: void(0);" onclick="buscarOrdenesCompra();"> <s:text
												name="midas.boton.buscar" /> </a>
						</div>
					<!--</s:if>-->
	    		</td>
	    	</tr>
	 	</tbody>
	 </table>
</div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.pagos.companias.listaOrdenes"/>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="divOrdenesCompra">
   <div id="indicador"></div>
	<div id="ordenesCompraListadoGrid" style="width:98%;height:280px">
	<div id="pagingArea"></div><div id="infoArea"></div>
   </div>
</div>
<div id="spacer2" style="height: 15px"></div>
</s:form>
<div class="titulo" style="width: 98%;">
	Total
</div>

<table id="agregar">
	<tr>
		<!-- <td>
			<s:textfield cssClass="txtfield jQalphabeticExt cajaTextoM2 w120" 
							labelposition="left" 
							label="Subtotal"
							id="t_subtotal"
							disabled="true" />
		</td>
		<td>
			<s:textfield cssClass="txtfield jQalphabeticExt cajaTextoM2 w120" 
							labelposition="left" 
							label="I.V.A"
							id="t_iva"
							disabled="true" />
		</td>
		<td>
			<s:textfield cssClass="txtfield jQalphabeticExt cajaTextoM2 w120" 
							labelposition="left" 
							label="I.V.A. Retenido"
							id="t_iva_retenido"
							disabled="true" />
		</td>
		<td>
			<s:textfield cssClass="txtfield jQalphabeticExt cajaTextoM2 w120" 
							labelposition="left" 
							label="I.S.R."
							id="t_isr"
							disabled="true" />
		</td> -->
		<td width="100%">
			<s:textfield cssClass="txtfield jQalphabeticExt cajaTextoM2 w120 " 
							labelposition="left" 
							label="Total"
							id="t_total" 
							disabled="true" />
		</td>
	</tr>
</table>

<div class="titulo" style="width: 98%;">
	Informacion del Agrupador
</div>
<table class="" id="agregar">
	<tr>
		<th>Nombre del Agrupador:</th>
		<td colspan="4" align="left">
			<s:textfield cssClass="txtfield jQrestrict cajaTextoM2 w520 " 
							name="factura.nombreAgrupador" 
							id="t_nombreAgrupador" 
							size="70"		
							readonly = "true"
							maxlength="150"/>
		</td>
		<td  class= "guardar" colspan="2">
			<s:if test="factura == null">
				<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_guardar">
					 <a href="javascript: void(0);" onclick="guardar();" >
					 <s:text name="midas.boton.guardar" /> </a>
				</div>    
			</s:if>
			<s:else>
			    <div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_borrar">
					 <a href="javascript: void(0);" onclick="eliminar();" >
					 <s:text name="midas.boton.borrar" /> </a>
				</div>    
			</s:else>
			
		</td>	
		<td class="cerrar" colspan="2">
			 <div class="btn_back w80 alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_cerrar">
					 <a href="javascript: void(0);" onclick="mostrarBusqueda();" >
					 <s:text name="midas.boton.cerrar" /> </a>
				</div>    
		</td>
	</tr>
	
</table>
<script type="text/javascript">
	jQuery(document).ready(function(){
		inicializa();
		habilitaAutoComplete();
	});		
	
</script>
