<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/emisionFactura/emisionFactura.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
	#superior{
		height: 280px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.emision.facturas.titulo"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="buscarEmisionFacturasForm">
			<s:hidden id="ingresosFacturablesConcat" name="" />
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:textfield id="noFactura" label="%{getText('midas.siniestros.emision.facturas.no.factura')}" name="filtroEmisionFactura.noFactura"
								cssStyle="float: left;width: 60%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaEstatusRecuperacion" cssStyle="width: 90%;"
							name="filtroEmisionFactura.concepto" 
							label="%{getText('midas.siniestros.emision.facturas.concepto.facturacion')}"
							cssClass="cleaneable txtfield obligatorio"
							id="lista"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaEstatusFactura" cssStyle="width: 90%;"
							name="filtroEmisionFactura.estatusFactura" 
							label="%{getText('midas.siniestros.emision.facturas.estatus.factura')}"
							cssClass="cleaneable txtfield obligatorio"
							id="lstEstatusFactura"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaOficinas" cssStyle="width: 90%;"
							name="filtroEmisionFactura.oficina" 
							label="%{getText('midas.siniestros.emision.facturas.oficina')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
				</div>
				<div id="SIS">
				
				</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaEstatusIngreso" cssStyle="width: 90%;"
							name="filtroEmisionFactura.estatusIngreso" 
							label="%{getText('midas.siniestros.emision.facturas.estatus.ingreso')}"
							cssClass="cleaneable txtfield obligatorio"
							id="listadoEstatusIngreso"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 45%;">
						<s:textfield 
								id="razon" 
								label="%{getText('midas.siniestros.emision.facturas.facturada.por')}" 
								name="filtroEmisionFactura.facturadoPor"
								cssStyle="float: left;width: 117%;" 
								maxlength="100" cssClass="cleaneable txtfield obligatorio">
						</s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 45%;">
						<s:textfield 
								id="razon" 
								label="%{getText('midas.siniestros.emision.facturas.nombre.razon')}" 
								name="filtroEmisionFactura.nombreRazonSocial"
								cssStyle="float: left;width: 117%;" 
								maxlength="100" cssClass="cleaneable txtfield obligatorio">
						</s:textfield>
					</div>	
					
				<div id="SIS">
				
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.emision.facturas.fecha.facturacion')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroEmisionFactura.fechaFacturacionDe"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaFacturacionDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.emision.facturas.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroEmisionFactura.fechaFacturacionHasta"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaFacturacionHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					
					<div class="floatLeft" style="width: 12%;">
							<label><s:text
									name="%{getText('midas.siniestros.emision.facturas.fecha.cancelacion')}" />:</label>
							<div style="margin-top: 10%">
								<sj:datepicker name="filtroEmisionFactura.fechaCancelacionDe"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaCancelacionDe" maxlength="10" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
					<div class="floatLeft" style="width: 12%; ">
							<label><s:text
									name="%{getText('midas.siniestros.emision.facturas.hasta')}" />:</label>
							<div style="margin-top: 9%">
								<sj:datepicker name="filtroEmisionFactura.fechaCancelacionHasta"
									changeMonth="true" changeYear="true"
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" id="fechaCancelacionHasta" maxlength="10"
									size="12" disabled="false"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									cssClass="cleaneable txtfield obligatorio">
								</sj:datepicker>
							</div>
					</div>
					
				</div>
				
				<div id="SIS">
					
					&nbsp;

				</div>
				<div id="SIS">
					
					<div id="btnBuscar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="buscarEmisionFactura();"> <s:text name="Buscar" /> </a>
					</div>
					
					<div id="btnLimpiar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarEmisionFactura();"> <s:text name="Limpiar" /> </a>
					</div>

				</div>
				
			</div>
		</form>
	</div>
	</br>
	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.emision.facturas.subtitulo.grid"/>	
	</div>
	<div id="indicador"></div>
	<div id="listadoEmisionFacturas" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>	
	
	
	<div id="btnLimpiar" class="btn_back w120" style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
		<a href="javascript: void(0);" onclick="exportarExcelEmisionFactura();"> <s:text name="Exportar Excel" /> </a>
	</div>

</div>
<script>
jQuery(document).ready(function(){
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	initComboIngreso(); // ELIMINA ESTATUS DIFERENTES A PENDIENTE Y APLICADO	
	initComboEstatusFactura(); // ELIMINA ESTATUS PROCESO

	jQuery("#noFactura").keypress(function(event) {
			var code = event.keyCode || event.which;
			if (code == 13) {
				buscarEmisionFactura();
			} 
		});
		
	});
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
