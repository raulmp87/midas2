<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/emisionFactura/emisionFactura.js'/>"></script>


<style>
<!--
table{
	text-transform: uppercase;
}
</style>

<s:form id="cancelacionPendientePorAplicar" action="mostrarVentanaCancelacionPendientePorAplicar" namespace="/siniestros/recuperacion/ingresoCancelacion" name="cancelacionPendientePorAplicar">
<s:hidden id="ingresoId" name="ingresoId" />
<table id="agregar" style=" border:0; ">
  		<tr>
  			<th> <s:text name="midas.siniestros.recuperacion.ingresos.motivo.cancelacion" />: </th>
  		</tr>
  		<tr>
  			<td>
  				<s:select list="listaMotivosCancelacion" 
  					headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					onchange = "validaMotivoCancelacionFactura();"
					name="" id="listaMotivosCancelacion"
					cssClass="jQrequired cajaTextoM2 w200" /></td>
  		</tr>
  		<tr>
  			<td>
  				<s:textfield
						id="motivoCancelacion"
						name="motivoCancelacion"	
						maxlength = "25"	
						cssStyle="float: left;width: 80%;" 
						cssClass="cleaneable txtfield obligatorio"></s:textfield>
  			</td>
  		</tr>
  		<tr>
  			<td>
	  			<!-- <div id="btnCerraar" class="btn_back w120"
						style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="cerrarFactura();"> <s:text
								name="Cerrar" /> </a>
				</div> -->
					
				<div id="btnCancelar" class="btn_back w120"
						style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="aplicarCancelacionEmisionFactura();"> <s:text
								name="Guardar" /> </a>
				</div>
			</td>
		</tr>
</table>

<s:hidden name="emisionId" id="emisionId" />
</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	jQuery("#motivoCancelacion").attr("disabled",true);
</script>



