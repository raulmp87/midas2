<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="noFactura"		 type="ro" width="75"  sort="str" ><s:text name="%{'midas.siniestros.emision.facturas.detalle.factura.info.no.factura'}"/></column>	
        <column id="estatusFactura"  type="ro" width="120" sort="str" ><s:text name="%{'midas.siniestros.emision.facturas.detalle.factura.info.estatus'}"/></column>
        <column id="fecha"           type="ro" width="100" sort="date_custom" ><s:text name="%{'midas.siniestros.emision.facturas.detalle.factura.info.fecha.estatus'}"/> </column>
		<column id="observaciones"   type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.emision.facturas.detalle.factura.info.motivo'}"/> </column>
		
		</head>
		<s:iterator value="lstEmisionFacturaHistorico">
			<row>
				<cell><s:property value="folioFactura" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusFacturaDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusHistoricoDesc" escapeHtml="false" escapeXml="true" />/<s:property value="observaciones" escapeHtml="false" escapeXml="true" /></cell>
			</row>
		</s:iterator>
	
</rows>
