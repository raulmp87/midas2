<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="noFactura"		    type="ro" width="75"  sort="str" ><s:text name="%{'midas.siniestros.emision.facturas.no.factura'}"/></column>	
        <column id="estatusFactura"     type="ro" width="120" sort="str" ><s:text name="%{'midas.siniestros.emision.facturas.estatus.factura'}"/></column>
        <column id="tipoRecuperacion" 	type="ro" width="100" sort="str" ><s:text name="%{'midas.siniestros.emision.facturas.concepto.recuperacion.tipo'}"/> </column>
		<column id="oficina"            type="ro" width="110" sort="str"><s:text name="%{'midas.siniestros.emision.facturas.oficina'}"/> </column>
		<column id="noSiniestro"	    type="ro" width="150" sort="str"><s:text name="%{'midas.siniestros.emision.facturas.no.siniestro'}"/></column>
		<column id="nombreRazon"        type="ro" width="100" sort="str"><s:text name="%{'midas.siniestros.emision.facturas.nombre.razon'}"/> </column>	
		<column id="facturadoPor"       type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.emision.facturas.facturada.por'}"/> </column>
		<column id="totalFacturado"     type="ron" width="80"  format="$0,000.00" sort="int"><s:text name="%{'midas.siniestros.emision.facturas.facturada.total.facturado'}"/> </column>
		<column id="estatusIngreso"	    type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.emision.facturas.estatus.ingreso'}"/> </column>
		<column id="fechaFacturacion"   type="ro" width="80"  sort="date"><s:text name="%{'midas.siniestros.emision.facturas.fecha.facturacion.busqueda'}"/> </column>
		<column id="fechaCancelacion"   type="ro" width="80"  sort="date"><s:text name="%{'midas.siniestros.emision.facturas.fecha.cancelacion.busqueda'}"/> </column>
		<column id="consultar"          type="img" width="30" sort="na" align="center">Acciones</column>
		<column                         type="img" width="30" sort="na" align="center" >#cspan</column>
	  	</head>
		<s:iterator value="lstEmisionFactura">
			<row>
				<cell><s:property value="noFactura" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusFacturaDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="tipoRecuperacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="oficinaDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="noSiniestro" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="nombreRazonSocial" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="facturadaPor" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="totalFacturado" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="estatusIngresoDesc" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaFacturacion" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="fechaCancelacion" escapeHtml="false" escapeXml="true" /></cell>
			    <s:if test="!noFactura eq ''">
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:verFactura(<s:property value="emisionId" escapeHtml="false" escapeXml="true" />)^_self</cell>
				</s:if>
				<s:if test="estatusFactura eq 'CANC'">
					<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript:editarFactura(<s:property value="emisionId" escapeHtml="false" escapeXml="true" />)^_self</cell>
				</s:if>
							
			</row>
		</s:iterator>
	
</rows>
