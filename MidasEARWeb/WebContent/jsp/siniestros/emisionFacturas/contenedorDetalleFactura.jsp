<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/emisionFactura/emisionFactura.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/recuperacion/ingresos/facturacionIngresos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/validaciones.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<style type="text/css">
	#superior{
		height: 250px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}

</style>


<s:form name="detalleFacturaEmision" id="detalleFacturaEmision">
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.emision.facturas.detalle.factura.ver.titulo1"/>	
</div>	
	<div id="superior" class="divContenedorO">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:select list="listaTiposPersona" name="emisionFactura.tipoPersona" 
									cssClass="cleaneable txtfield obligatorio tipoDedCru"
									id="listaTiposPersona"
									label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.persona.facturar')}" 
									headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
									onchange = "facturarATipoPersona('BUQ');" >
									</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:label name="emisionFactura.folioFactura"
							label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.mov.folio')}" labelposition="top"/>
					</div>							
				</div>
				
				<div id="SIS">
					 
					 <div class="floatLeft" style="width: 36%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.nombre.razon')}" 
								name="emisionFactura.nombreRazonSocial"		
								cssStyle="float: left;width: 80%;" 
								id="idNombrePersona"
								cssClass="cleaneable txtfield tipoDedCru"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.rfc')}" 
								name="emisionFactura.rfc"
								id="idRFC"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield tipoDedCru obligatorioDedCru"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 25%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.calle')}" 
								name="emisionFactura.calle" 
								id="idCalle"
								cssStyle="float: left;width: 90%" cssClass="cleaneable txtfield tipoDedCru obligatorioDedCru"></s:textfield>
					</div>
					
				</div>
				
				
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:select id="idEstadoName" 
									name="emisionFactura.estadoMidas.id" 
									list="estados"  
									onchange="cargarCiudades(this.value);" 
								  	cssClass="txtfield tipoDedCru obligatorioDedCru"  
								  	cssStyle="float: left;width: 90%;"  
								  	headerValue="%{getText('midas.general.seleccione')}"
								  	label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.edoUbicacion')}" labelposition="top"	/>			
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select id="idCiudadName" 
								  name="emisionFactura.ciudadMidas.id" 
								  list="municipios"  
								  onchange="cargarColonias(this.value);" 
								  cssClass="txtfield tipoDedCru obligatorioDedCru"  
								  cssStyle="float: left;width: 90%;"  
								  headerValue="%{getText('midas.general.seleccione')}"
								  label="%{getText('midas.siniestros.recuperacion.recuperacionSvm.ciudadUbicacion')}" 
								  labelposition="top" />			
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:select id="idColoniaName" 
									name="emisionFactura.coloniaMidas.id" 
									list="colonias"  
								  	cssClass="txtfield tipoDedCru obligatorioDedCru" 
								  	cssStyle="float: left;width: 90%;"  
								  	label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.colonia')}" 
								  	headerValue="%{getText('midas.general.seleccione')}"
								  	labelposition="top" />		
					</div>
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.cp')}" 
								id="cpName"
								name="emisionFactura.cp" 
								onkeypress="return soloNumeros(this, event, true)"
								cssStyle="float: left;width: 50%" cssClass="cleaneable txtfield tipoDedCru obligatorioDedCru"></s:textfield>
					 </div>
					
					
					
				</div>		
				
				<div id="SIS">
					
					 <div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.telefono')}" 
								name="emisionFactura.telefono"		
								cssStyle="float: left;width: 90%;" 
								id="idTelefono"
								maxLength="20"
								onkeypress="return soloNumeros(this, event, true)"
								cssClass="cleaneable txtfield tipoDedCru obligatorioDedCru"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.email')}" 
								name="emisionFactura.email"
								id="idEmail"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield tipoDedCru obligatorioDedCru"></s:textfield>
					</div>
					
				</div>						

			</div>
		
	</div>

</br>

<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.emision.facturas.detalle.factura.ver.titulo2"/>	
</div>	
	<div id="superior"  class="divContenedorO" style="height: 210px;" >
		<form id="">
			<div id="superiorI">
				
				<div id="SIS">
					 
					 <div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.importe')}" 
								name="emisionFactura.importe"	
								cssStyle="float: left;width: 80%;" 
								cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.iva')}" 
								name="emisionFactura.iva"
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.total')}" 
								name="emisionFactura.totalFactura"
								cssStyle="float: left;width: 90%" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:select list="listaMetodosPago" cssStyle="width: 90%;"
							name="emisionFactura.metodoPago" 
							label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.metodo.pago')}"
							cssClass="cleaneable txtfield habilitarTipo tipoDedCru tipoCiaSvmPrv obligatorioDedCru obligatorioCiaSvmPrv"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.4digitos')}" 
								name="emisionFactura.digitos" 
								maxLength="4"
								cssStyle="float: left;width: 50%" cssClass="cleaneable txtfield tipoDedCru tipoCiaSvmPrv "></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 20%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.concepto')}" 
								name="emisionFactura.concepto" 
								cssStyle="float: left;width: 75%" cssClass="cleaneable txtfield "></s:textfield>
					</div>
					
				</div>

				<div id="SIS">
					 <div class="floatLeft" style="width: 99%;">
						<s:textarea label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.desc.fact')}" 
								name="emisionFactura.descripcionFacturacion"  		
								cssStyle="float: left;width: 90%;" 
								disabled = "true"
								cssClass="cleaneable txtfield "/>
					</div>
				</div>		
				
				
				<div id="SIS">
					 <div class="floatLeft" style="width:99%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.obser.fact')}" 
								name="emisionFactura.observacionesFacturacion"
								id="observacionFactura"	
								cssStyle="float: left;width: 90%;" 
								cssClass="cleaneable txtfield obligatorio tipoDedCru tipoCiaSvmPrv obligatorioDedCru obligatorioCiaSvmPrv"></s:textfield>
					</div>
				</div>						

			</div>
		</form>
	</div>

<s:if test="%{ emisionFactura.ingreso.recuperacion.tipo == 'SPV' }"  >	
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.emision.facturas.detalle.factura.ver.titulo3"/>	
	</div>	
	<div id="superior"  class="divContenedorO" style="height: 100px;" >
		
			<div id="superiorI">
				
				<div id="SIS">
					 
					 <div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.marca')}" 
								name="emisionFactura.marca"		
								cssStyle="float: left;width: 80%;" 
								cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 15%;">
						<s:textfield 
								label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.tipo')}" 
								name="emisionFactura.tipo" 
								cssStyle="float: left;width: 90%" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 15%;">
						<s:textfield 
								label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.modelo')}" 
								name="emisionFactura.modelo" 
								cssStyle="float: left;width: 90%" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 15%;">
						<s:textfield 
								label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.no.serie')}" 
								name="emisionFactura.noSerie" 
								cssStyle="float: left;width: 50%" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					 
					<div class="floatLeft" style="width: 15%;">
						<s:textfield 
								label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.no.motor')}" 
								name="emisionFactura.noMotor" 		
								cssStyle="float: left;width: 90%;" 
								cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 15%;">
						<s:textfield 
								label="%{getText('midas.siniestros.emision.facturas.detalle.factura.ver.color')}" 
								name="emisionFactura.color" 
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
				</div>		

			</div>
	</div>
</s:if>

	<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.emision.facturas.detalle.factura.info.historial"/>	
	</div>


		<div id="SIS">
			<div id="indicador"></div>
			<div id="listadoGridHistoricoEmisionFactura"
				style="width: 60%; height: 200px;"></div>
			<div id="pagingArea"></div>
			<div id="infoArea"></div>
		</div>


		</br>


		<div id="SIS">

			<div id="btnCerraar" class="btn_back w120"
				style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
				<a href="javascript: void(0);" onclick="cerrarFacturaEmision();"> <s:text
						name="%{getText('midas.siniestros.emision.facturas.detalle.cerrar')}" /> </a>
			</div>
			
			<s:if test="%{emisionFactura.estatus == 'CANC'}">
				<div id="btnEditar" class="btn_back w120"
					style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
					<a href="javascript: void(0);" onclick="editarFacturaEmsion();"> 
						<s:text name="%{getText('midas.siniestros.emision.facturas.detalle.guardar')}" /> </a>
				</div>
			</s:if>
			
			<s:if test="%{emisionFactura.estatus == 'RECHAZ'}">
				<div id="btnVerFactura" class="btn_back w120"
					style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
					<a href="javascript: void(0);" onclick="verDetalleErrores('<c:out value='${emisionFactura.ingreso.numero}'/>','<c:out value='${emisionFactura.folioFactura}'/>');"> 
						<s:text name="%{getText('midas.siniestros.emision.facturas.detalle.rechazo')}" /> </a>
				</div>
			</s:if>
			
			<s:if test="%{emisionFactura.folioFactura != ''  && modoConsultar == true}">
				<div id="btnReenviarFactura" class="btn_back w120"
						style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
						<a href="javascript: void(0);" onclick="mostrarReenvioFactura('<c:out value='${emisionFactura.id}'/>', 
						'<c:out value='${emisionFactura.folioFactura}'/>', idEmail.value);"> <s:text
								name="%{getText('midas.siniestros.emision.facturas.reenviarFactura')}" /> </a>
				</div>
			</s:if>	
			
			<s:if test="%{emisionFactura.folioFactura != ''}">
				<div id="btnVerFactura" class="btn_back w120"
					style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
					<a href="javascript: void(0);" onclick="verFacturaXML('<c:out value='${emisionFactura.id}'/>', '<c:out value='${emisionFactura.folioFactura}'/>');"> <s:text
							name="%{getText('midas.siniestros.emision.facturas.verFactura')}" /> </a>
				</div>
			</s:if>
			
			<s:if test="%{emisionFactura.estatus == 'CANC'}">
				<div id="btnEnviar" class="btn_back w120"
					style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
					<a href="javascript: void(0);" onclick="reFacturarEmision();">
						<s:text name="%{getText('midas.siniestros.emision.facturas.refacturar')}" /> </a>
				</div>
			</s:if>
			
			<s:if test="%{emisionFactura.estatus == 'FACT' && emisionFactura.folioFactura != ''}">
				<div id="btnCancelar" class="btn_back w120"
					style="display: inline; margin-right: 2%; margin-bottom: 2%; float: right;">
					<a href="javascript: void(0);" onclick="verCancelacionFacturaEmision();"> 
					<s:text name="%{getText('midas.siniestros.emision.facturas.cancelar')}" /> </a>
				</div>
			</s:if>

		</div>

</div>

<s:hidden name="emisionFactura.estatus" id="emisionEstatus" />
<s:hidden name="emisionFactura.id" id="emisionId" />
<s:hidden name="modoConsultar" id="modoConsultar" />
<s:hidden name="emisionFactura.ingreso.recuperacion.tipo" id="tipoRecuperacion" />
<s:hidden name="emisionFactura.ingreso.id" id="ingresoId" />
<s:hidden name="emisionFactura.tipoPersona" id="tipoPersona" />
<s:hidden name="pantallOrigen" id="pantallOrigen" value="OBF" />

</s:form>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript">

	jQuery(document).ready(function() {
	
		initDetalleEmisionFactura();
		initGridHistoricoEmisionFactura();
	});
</script>
