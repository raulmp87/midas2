<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
  
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>	

<div class="titulo" style="margin-left: 2%"><s:text name="midas.siniestros.rcuperacion.ingresos.facturar.titulo.reenviar"/></div>
<br/>
<s:form id="reenvioFacturaForm">
<s:hidden name="emisionFactura.id" id="idEmisionFactura"/>	
<table id="agregar" border="0">
	<tr>
		<td>
			<s:text name="midas.siniestros.rcuperacion.ingresos.facturar.impresion.correosRemitentes"/>
		</td>
	</tr>	
	<tr>		
		<td>
			<s:textfield id="correosRemitentes"
			name="destinatariosStr" cssClass="cajaTextoM2LC jQrequired jQEmailListSC w500" />	 				 
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.siniestros.rcuperacion.ingresos.facturar.observaciones"/>
		</td>	
	</tr>
	<tr>	
		<td>
			<s:textarea cols="5" rows="5" id="observaciones" 
			name="observacionesReenvio" cssClass="cajaTextoM2 jQrequired w500"/>					 
		</td>	
	</tr>
	<tr>
		<td colspan="2"><font color="#FF6600"><label for="agenteNombre" ><s:text name="midas.siniestros.rcuperacion.ingresos.facturar.NotaReenvio" ></s:text>:</label></font></td>
	</tr>
</table>	
<div id="botonReenviar" class="btn_back w150 " style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="if(validateAll(false)){parent.reenviarFactura(idEmisionFactura.value, correosRemitentes.value,observaciones.value);}">
		<s:text name="midas.boton.reenviar" /> 
	</a>
</div>

<div id="botonCerrar" class="btn_back w150 " style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="parent.popUpReenvioFactura.close();">
		<s:text name="midas.boton.cerrar" /> 
	</a>
</div>
</s:form>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript">	
		
		
						
</script>