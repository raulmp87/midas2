<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
	


.tabla{
 	color:#000000 !important;
	line-height: 20px; 
	text-align: left;	
	font-size: 9px;
	font-family: arial;
	vertical-align: middle;
}


.warningField {
    background-color: #f5a9a9;
}


</style>

<script type="text/javascript">
	var guardarPath = '<s:url action="guardarExpedienteJuridico" namespace="/siniestros/expedientejuridico"/>';
	var mostrarComentriosPath = '<s:url action="obtenerComentarios" namespace="/siniestros/expedientejuridico"/>';
</script>

<s:form id="expedienteJuridicoForm" >
		<s:hidden id="esConsulta" name="esConsulta"/>
		<s:hidden id="idExpedienteJuridico" name="expedienteJuridico.id"/>
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.expedientejuridico.datosGenerales" />
		</div>	
		
		
		<table id="datosGenerales" border="0"  width="90%">
			<tbody>
				
				<tr>
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaRegistro" /> </td>
					<td class="tabla"><s:textfield value="%{expedienteJuridico.fechaCreacion}"  cssClass="cajaTexto w150 esConsulta" disabled="true" ></s:textfield></td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaCancelacion" /> </td>
					<td class="tabla"><s:textfield value="%{expedienteJuridico.fechaCancelacion}"  cssClass="cajaTexto w150 esConsulta" disabled="true" ></s:textfield></td>

				</tr>
			</tbody>
		</table>

		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.expedientejuridico.informacion" />
		</div>	
		
		<table id="informacion" border="0" width="90%">
			<tbody>
				
				<tr>
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.noSiniestro" /> </td>
					<td class="tabla">
						<s:textfield id="asignaNumSiniestro" name="expedienteJuridico.numeroSiniestro" readonly="true" cssClass="cajaTexto w150 esConsulta requerido" onBlur="validaFormatoSiniestro(this);" ></s:textfield>
						<s:if test="sePuedeBuscarSiniestro == true">
							<div class="btn_back w140" style="display: inline; float: left;" id="b_asignarSiniestro">
								<a href="javascript: void(0);" onclick="asignarSiniestro();"> 
									<s:text name="midas.siniestros.expedientejuridico.noSiniestro"/> 
								</a>
							</div>
						</s:if>
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaTurno" /> </td>
					<td class="tabla">
						<s:if test="esConsulta==true">
								<s:textfield cssClass="txtfield cajaTextoM2 w130"
										size="10"					
										id="txt_fechaTurno" 
										readonly="true"
										value="%{expedienteJuridico.fechaTurno}" />					
							</s:if>
							<s:else>
								<sj:datepicker name="expedienteJuridico.fechaTurno"
											changeMonth="true"
										 changeYear="true"				
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" 
			                        			 id="dp_fechaTurno"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w130 requerido esConsulta"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
								</sj:datepicker>
							</s:else>
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.estatusJuridico" /> </td>
					<td class="tabla">
						<s:select list="estatusJuridicoMap" headerKey="TRAM" 
							name="expedienteJuridico.estatusJuridico" id="s_estatusJuridico" cssClass="cajaTextoM2 w150 esConsulta" />
					</td>

				</tr>

				<tr>
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.motivoTurno" /> </td>
					<td colspan="3">
						<s:select list="motivoTurnoMap" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="expedienteJuridico.motivoTurno" id="s_motivoTurno" cssClass="cajaTextoM2 w450 esConsulta requerido" />
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.expedienteProveedor" /> </td>
					<td class="tabla"><s:textfield name="expedienteJuridico.expedienteProveedor"  maxlength="50" cssClass="cajaTexto w150 esConsulta" ></s:textfield></td>

				</tr>
				<tr>
					
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.vehiculoDetenido" /> </td>
					<td class="tabla">
						<s:select list="#{ '0':'NO','1':'SI'}" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="expedienteJuridico.vehiculoDetenido" id="s_vehiculoDetenido" cssClass="cajaTextoM2 w150 esConsulta requerido" onChange="validaVehiculoDetenido();"/>
					</td>


					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.estatusVehiculo" /> </td>
					<td class="tabla">
						<s:select list="estatusVehiculoMap" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="expedienteJuridico.estatusVehiculo" id="s_estatusVehiculo" cssClass="cajaTextoM2 w150 esConsulta" onChange="validaEstatusVehiculo();"/>
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaLiberacionVehiculo" /> </td>
					<td class="tabla">
						<s:if test="esConsulta==true">
									<s:textfield cssClass="txtfield cajaTextoM2 w130"
											size="10"					
											id="txt_fechaLiberacion" 
											readonly="true"
											value="%{expedienteJuridico.fechaLiberacionVehiculo}" />					
						</s:if>
						<s:else>
							<sj:datepicker name="expedienteJuridico.fechaLiberacionVehiculo"
										changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" 
		                        			 id="dp_fechaLiberacionVehiculo"
									  maxlength="10" cssClass="txtfield cajaTextoM2 w130 esConsulta"
										   size="12"
									 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</s:else>
					</td>
				</tr>

				<tr>
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.tipoAbogado" /> </td>
					<td class="tabla"><s:textfield value="%{expedienteJuridico.abogado.ambitoDesc}"  cssClass="cajaTexto w150" disabled="true" ></s:textfield></td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.abogado" /> </td>
					<td colspan="3">
						<s:select list="abogadosMap" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="expedienteJuridico.abogado.id" id="s_abogado" cssClass="cajaTextoM2 w350 esConsulta" />
					</td>
				</tr>

				<tr>
					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.tipoConclusion" /> </td>
					<td class="tabla">
						<s:select list="tipoConclusionMap" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
							name="expedienteJuridico.tipoConclusion" id="s_tipoConclusion" cssClass="cajaTextoM2 w150 esConsulta" />
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaConclusionLegal" /> </td>
					<td class="tabla">
						<s:if test="esConsulta==true">
									<s:textfield cssClass="txtfield cajaTextoM2 w130"
											size="10"					
											id="txt_fechaConclusionLegal" 
											readonly="true"
											value="%{expedienteJuridico.fechaConclusionLegal}" />					
						</s:if>
						<s:else>
							<sj:datepicker name="expedienteJuridico.fechaConclusionLegal"
										changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" 
		                        			 id="dp_fechaConclusionLegal"
									  maxlength="10" cssClass="txtfield cajaTextoM2 w130 esConsulta"
										   size="12"
									 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</s:else>
					</td>

					<td class="tabla"><s:text name="midas.siniestros.expedientejuridico.fechaConclusionAfirme" /> </td>
					<td class="tabla">
						<s:if test="esConsulta==true">
									<s:textfield cssClass="txtfield cajaTextoM2 w130"
											size="10"					
											id="txt_fechaConclusionAfirme" 
											readonly="true"
											value="%{expedienteJuridico.fechaConclusionAfirme}" />					
						</s:if>
						<s:else>
							<sj:datepicker name="expedienteJuridico.fechaConclusionAfirme"
										changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" 
		                        			 id="dp_fechaConclusionAfirme"
									  maxlength="10" cssClass="txtfield cajaTextoM2 w130 esConsulta"
										   size="12"
									 onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</s:else>
					</td>


				</tr>


				<tr>
					<td colspan="6">
						<table width="100%" border="0">
							<tr>
								<td class="tabla" width="50%" >
									<s:text name="midas.siniestros.expedientejuridico.comentarioAfirme" />
								</td>
								<td width="50%" class="tabla">
									<s:text name="midas.siniestros.expedientejuridico.comentarioProveedor" />
								</td>
							</tr>
							<tr>
								<td width="50%" >
									<s:textarea cols="50" 
										maxlength="300" 
										name="comentarioAfirme"
										label="%{getText(midas.siniestros.expedientejuridico.comentarioAfirme)}"
										labelposition="top" 
										rows="2" 
										readonly="%{consulta}"
										disabled="%{consulta}"
										cssClass="textarea h80 esConsulta"
										theme="simple"
										id="observaciones" />
								</td>
								<td width="50%">
									<s:textarea cols="50" 
										maxlength="100" 
										name="comentarioProveedor"
										label="midas.siniestros.expedientejuridico.comentarioProveedor"
										labelposition="top" 
										rows="2" 
										readonly="%{consulta}"
										disabled="%{consulta}"
										cssClass="textarea  h80 esConsulta"
										theme="simple"
										id="observaciones" />
							</td>
						</tr>	
					</table>
				</td>
					<!--<td colspan="3">
							<s:textarea cols="50" 
							maxlength="2500" 
							name="comentarioAfirme"
							label="midas.siniestros.expedientejuridico.comentarioAfirme"
							labelposition="top" 
							rows="2" 
							readonly="%{consulta}"
							disabled="%{consulta}"
							cssClass="textarea  h80"
							theme="simple"
							id="observaciones" />
					</td>
					<td colspan="3">
							<s:textarea cols="50" 
							maxlength="2500" 
							name="comentarioProveedor"
							label="midas.siniestros.expedientejuridico.comentarioProveedor"
							labelposition="top" 
							rows="2" 
							readonly="%{consulta}"
							disabled="%{consulta}"
							cssClass="textarea  h80"
							theme="simple"
							id="observaciones" />
					</td>-->
				</tr>
	


			</tbody>
		</table>
		
		<table id="acciones" style="padding: 0px; width: 100%; margin: 0px; border: none;">
			<tr>
				<td class="tabla">
					<div class="btn_back w140" style="display: inline; float: left;" id="b_consultarSiniestro">
						<a href="javascript: void(0);" onclick="consultarReporte();"> 
						<s:text name="midas.siniestros.expedientejuridico.consultarSiniestro" /> </a>
					</div>
					
					<div class="btn_back w140"  style="display: inline; float: right; margin-right:7%;" id="b_cerrar">
						<a href="javascript: void(0);" onclick="cerrar();"> 
						<s:text name="midas.boton.cerrar" /> </a>
					</div>	
					<s:if test="esConsulta != true">
						<div class="btn_back w140"  style="display: inline; float: right;  margin-right:1%;" id="b_guardar" >
							<a href="javascript: void(0);" onClick="guardar()" class="icon_guardar2"> 
							<s:text name="midas.boton.guardar" /> </a>
						</div> 
					</s:if>
				</td>							
			</tr>
		</table>	
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.expedientejuridico.comentarios" />
		</div>	

		<div id="indicadorComentarios"></div>
		<div id="comentariosGrid"  class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br/>
		<br/>	

</s:form>			
			

<script src="<s:url value='/js/midas2/siniestros/expedienteJuridico/expedienteJuridico.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>

<script type="text/javascript">
	jQuery(document).ready(
	function(){
		mostrarComentarios();
		validaModoConsulta(<s:property value="esConsulta"/>);
		validaVehiculoDetenido();
		validaEstatusVehiculo();
		validaTipoDeConclusion(<s:property value="sePuedeBuscarSiniestro"/>);

 	}
 );
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>