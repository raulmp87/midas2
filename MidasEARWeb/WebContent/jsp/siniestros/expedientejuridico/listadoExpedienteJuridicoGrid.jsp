<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="noSiniestro"           type="ro" width="140"  sort="str" ><s:text name="%{'midas.siniestros.expedientejuridico.noSiniestro'}"/></column>	
        <column id="fechaTurno"            type="ro" width="120" sort="date" ><s:text name="%{'midas.siniestros.expedientejuridico.fechaTurno'}"/></column>
        <column id="motivoTurno" 		   type="ro" width="135" sort="str" ><s:text name="%{'midas.siniestros.expedientejuridico.motivoTurno'}"/> </column>
		<column id="expedienteProveedor"   type="ro" width="140" sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.expedienteProveedor'}"/> </column>
		<column id="vehiculoDetenido"      type="ro" width="150"  sort="int"><s:text name="%{'midas.siniestros.expedientejuridico.vehiculoDetenido'}"/></column>	
		<column id="estatusVehiculo"       type="ro" width="90"  sort="int"><s:text name="%{'midas.siniestros.expedientejuridico.estatusVehiculo'}"/> </column>
		<column id="tipoAbogado"           type="ro" width="80" sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.tipoAbogado'}"/> </column>
		<column id="abogado"           	   type="ro" width="180"  sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.abogado'}"/> </column>
		<column id="tipoConclusion"        type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.tipoConclusion'}"/> </column>
		<column id="fechaConclusionLegal"  type="ro" width="80"  sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.fechaConclusionLegal'}"/> </column>
		<column id="fechaConclusionAfirme" type="ro" width="180" sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.fechaConclusionAfirme'}"/> </column>
		<column id="estatusJuridico"       type="ro" width="100" sort="str"><s:text name="%{'midas.siniestros.expedientejuridico.estatusJuridico'}"/> </column>
		<column id="editar" type="img" width="40" sort="na" align="center">Acciones</column>
	    <column id="consultar" type="img" width="40" sort="na" align="center">#cspan</column>	  
	 </head>
	<s:iterator value="expedientesJuridicos" status="row">
		<row> 
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaTurno" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="motivoTurnoDesc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="expedienteProveedor" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="vehiculoDetenidoDesc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatusVehiculoDesc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="abogado.ambitoDesc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="abogado.nombrePersona" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoConclusionDesc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaConclusionLegal" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaConclusionAfirme" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatusJuridicoDesc" escapeHtml="false" escapeXml="true" /></cell>
			<s:if test="estatusJuridico== 'TRAM' || estatusJuridico == 'CONSIG' || estatusJuridico == 'SENT'">
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			</s:if><s:else>			
				<cell>../img/pixel.gif^'NA'^^_self</cell>
			</s:else>	
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultar(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>
	
</rows>
   
