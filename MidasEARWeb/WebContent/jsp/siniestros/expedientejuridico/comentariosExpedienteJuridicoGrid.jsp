<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
      <head>
          <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
            <column id="" type="ro"  width="170"	sort="date_custom" ><s:text name="midas.siniestros.expedientejuridico.fechaHora"/></column>
            <column id="" type="ro"  width="150"  sort="str" ><s:text name="midas.siniestros.expedientejuridico.usuario"/></column>
          	<column id="" type="ro"  width="150"  sort="str"><s:text name="midas.siniestros.expedientejuridico.comentarioDe"/></column>
          	<column id="" type="ro"  width="*"  sort="str"><s:text name="midas.siniestros.expedientejuridico.comentario"/></column>
      </head>
      <s:iterator value="comentarios">
            <row id="<s:property value="id" escapeHtml="false" escapeXml="true" />">
                <cell><s:date name="fechaCreacion"   format="dd/MM/yyyy HH:mm" />       </cell>
                <cell><s:property value="codigoUsuarioCreacion"  escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="tipoDesc" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="comentario" escapeHtml="false" escapeXml="true" /></cell>
            </row>
      </s:iterator>
       
</rows>
