<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>


<style type="text/css">
	#superior{
		height: 350px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	.ui-datepicker-trigger{
		display:none;
	}

</style>
<div id="contenido_listadoJuridico" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.expedientejuridico.listado"/>
</div>	
	<div id="superior" class="divContenedorO">
		<form id="buscarExpedienteJuridicoForm">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 32%;">
						<s:textfield 
								label="%{getText('midas.siniestros.expedientejuridico.noSiniestro')}" 
								name="expedienteJuridicoDTO.numeroSiniestro"
								cssStyle="float: left;width: 50%;" 
								maxlength="30" 
								id="noSiniestro"
								onBlur="validaFormatoSiniestro(this);"	
								cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
						</s:textfield>
					</div>
					<div class="floatLeft" style="width: 32%;">
						<s:select list="motivoTurnoMap" 
									label="%{getText('midas.siniestros.expedientejuridico.motivoTurno')}"
									headerKey=""
							        headerValue="%{getText('midas.general.seleccione')}" 
							        name="expedienteJuridicoDTO.motivoTurno" 
							        id="motivoTurno"
							        cssStyle="width: 90%;" 
							        cssClass="cleaneable txtfield obligatorioBusquedaJuridico" />
					</div>
					<div class="floatLeft" style="width: 32%;">
						<s:select list="estatusJuridicoMap" cssStyle="width: 90%;"
							name="expedienteJuridicoDTO.estatusJuridico" 
							id="estatusJuridico"
							label="%{getText('midas.siniestros.expedientejuridico.estatusJuridico')}"
							cssClass="cleaneable txtfield obligatorioBusquedaJuridico"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
				</div>
				<div id="SIS">
					
					<div class="floatLeft" style="width: 32%;">
						<s:textfield 
								label="%{getText('midas.siniestros.expedientejuridico.expedienteProveedor')}" 
								name="expedienteJuridicoDTO.expedienteProveedor" 	
								cssStyle="float: left;width: 50%;" 
								maxlength="30" 
								id="expedienteProveedor"
								cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
						</s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 32%;">
						<s:select 
								list="#{ '0':'NO','1':'SI'}" 
								headerKey="" 
								headerValue="%{getText('midas.general.seleccione')}" 
							    name="expedienteJuridicoDTO.vehiculoDetenido" 
							    id="s_vehiculoDetenido" 
							    label="%{getText('midas.siniestros.expedientejuridico.vehiculoDetenido')}"
							    cssClass="cleaneable txtfield obligatorioBusquedaJuridico w150" />
					</div>
					
					<div class="floatLeft" style="width: 32%;">
						<s:select list="estatusVehiculoMap" cssStyle="width: 90%;"
							name="expedienteJuridicoDTO.estatusVehiculo" 
							id="estatusVehiculo"
							label="%{getText('midas.siniestros.expedientejuridico.estatusVehiculo')}"
							cssClass="cleaneable txtfield obligatorioBusquedaJuridico"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
				</div>
				
				<div id="SIS">
					
					<div class="floatLeft" style="width: 32%;">
						<s:select list="tipoAbogado" cssStyle="width: 90%;"
							name="expedienteJuridicoDTO.abogado.ambitoPrestadorServicio" 
							id="tipoAbogado"
							label="%{getText('midas.siniestros.expedientejuridico.tipoAbogado')}"
							cssClass="cleaneable txtfield obligatorioBusquedaJuridico"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					<div class="floatLeft" style="width: 32%;">
						<s:select list="abogadosMap" cssStyle="width: 90%;"
							name="expedienteJuridicoDTO.abogado.id" 
							id="nombreAbogado"
							label="%{getText('midas.siniestros.expedientejuridico.abogado')}"
							cssClass="cleaneable txtfield obligatorioBusquedaJuridico"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
					
					<div class="floatLeft" style="width: 32%;">
						<s:select list="tipoConclusionMap" cssStyle="width: 90%;"
							name="expedienteJuridicoDTO.tipoConclusion" 
							id="tipoConclusion"
							label="%{getText('midas.siniestros.expedientejuridico.tipoConclusion')}"
							cssClass="cleaneable txtfield obligatorioBusquedaJuridico"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					
				</div>
				
				<div style=" width:100%;" >
					<div style=" width:32%; float:left; margin-top:2%;">
						
						<div style=" width:100%; float:left;">
							
							<div class="floatLeft" style="margin-top:1%; margin-right:10%; ">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.fechaTurno')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaTurnoDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaTurnoDe" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
							<div class="floatLeft" style=" margin-top:1%;">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaTurnoHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaTurnoHasta" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
						</div>					
					</div>
				
					<div style=" width:32%; float:left; margin-top:2%;">
					
							<div class="floatLeft" style="margin-top:1%; margin-right:10%; ">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.fechaConclusionLegal')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaConclusionLegalDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaConclusionLegalDe" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
							<div class="floatLeft" style=" margin-top:1%;">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaConclusionLegalHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaConclusionLegalHasta" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
				</div>
						
					<div style=" width:32%; float:left; margin-top:2%;">
							
							<div class="floatLeft" style="margin-top:1%; margin-right:10%; ">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.fechaConclusionAfirme')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaConclusionAfirmeDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaConclusionAfirmeDe" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
							<div class="floatLeft" style=" margin-top:1%;">
								<label><s:text name="%{getText('midas.siniestros.expedientejuridico.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="expedienteJuridicoDTO.fechaConclusionAfirmeHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaConclusionAfirmeHasta" maxlength="10" size="18"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorioBusquedaJuridico">
									</sj:datepicker>
								</div>
							</div>
					</div>					
					
				</div>
				
				<div id="SIS"  style=" margin-top:5%;">
					<div class="btn_back w80" style="display: inline; margin-right: 7%;  float: right;">
						<a href="javascript: void(0);" onclick="buscarExpedienteJuridico();">
						<s:text name="midas.boton.buscar" /> </a>
					</div>
					<div class="btn_back w80" style="display: inline; margin-right: 4%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarExpedienteJuridico();">
						<s:text name="midas.boton.limpiar" /> </a>
					</div>
				</div>
				
			</div>
			

		</form>
	</div>
	
	<br/>
	
	
	<div id="indicador"></div>
	<div id="listadoExpedientesJuridicoGrid" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>

	<div id="SIS">
		<div class="btn_back w120" style="display: inline; float: right;"  id="btn_exportar" >
				<a href="javascript: void(0);" onclick="javascript:exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
		</div>
		<div id="divNuevoBtn" class="w150"
			style="float: right; ">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="nuevo();"> <s:text
						name="midas.boton.nuevo" /> </a>
			</div>
		</div>
	</div>


</div>

<script src="<s:url value='/js/midas2/siniestros/expedienteJuridico/expedienteJuridico.js'/>"></script>
<script>
jQuery(document).ready(function(){
	initGridJuridico();
});
</script>
