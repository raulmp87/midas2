<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="numeroDepuracion" type="ro" width="50" sort="int"><s:text name="midas.depuracion.reserva.historico.numeroDepuracion"/></column>
		<column id="fechaHoraProgramadaDepuracion" type="ro" width="*" sort="date" align="center"><s:text name="midas.depuracion.reserva.historico.fechaHoraProgramadaDepuracion"/></column>
		<column id="fechaHoraDepuracion" type="ro" width="200" sort="date" align="center" ><s:text name="midas.depuracion.reserva.historico.fechaHoraDepuracion"/></column>
		<column id="ejecutoDepuracion" type="ro" width="140" sort="str" align="center" ><s:text name="midas.depuracion.reserva.historico.ejecutoDepuracion"/></column>
		<column id="numeroRegistrosDepurados" type="ro" width="130"  sort="int" align="center" ><s:text name="midas.depuracion.reserva.historico.numeroRegistrosDepurados"/></column>
		<column id="montoReservaDepurada" type="ro" width="200"  sort="int" align="center" ><s:text name="midas.depuracion.reserva.historico.montoReservaDepurada"/></column>
		<column id="estatus" type="ro" width="130"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.historico.estatus"/></column>		
		<column id="acciones" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
	</head>			
	<s:iterator value="depuracionesList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="numeroDepuracion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaProgramacion" format="dd/MM/yyyy HH:mm" /></cell>	
			  <cell>
				<s:if test="fechaEjecucion == null">
					<s:text name="midas.general.guion"/> 			
				</s:if>	
				<s:else>
					<s:date name="fechaEjecucion" format="dd/MM/yyyy HH:mm" />
				</s:else>
			</cell>	
			<cell>
				<s:if test="codigoUsuarioEjecucion == null">
					<s:text name="midas.general.guion"/> 			
				</s:if>	
				<s:else>
					<s:property value="codigoUsuarioEjecucion" escapeHtml="false" escapeXml="true"/>
				</s:else>
			</cell>
			<cell>
				<s:if test="registrosDepurados == null">
					<s:text name="midas.general.guion"/> 			
				</s:if>	
				<s:else>
					<s:property value="registrosDepurados" escapeHtml="false" escapeXml="true"/>
				</s:else>	
			</cell>
			<cell>
				<s:if test="montoReservaDepurada == null">
					<s:text name="midas.general.guion"/> 			
				</s:if>	
				<s:else>
					<s:property value="%{getText('struts.money.format',{montoReservaDepurada})}" escapeHtml="true" escapeXml="true"/>
				</s:else>
			</cell>

			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus==\"PE\"">
				<cell>../img/icons/ico_verdetalle.gif^Ver Reservas a Depurar^javascript:mostrarReservasParaDepurar( <s:property value="id" /> );^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/icons/ico_verdetalle.gif^Ver Reservas Depuradas^javascript:mostrarReservasDepuradas( <s:property value="id" />);^_self</cell>
			</s:else>
		</row>
	</s:iterator>	
</rows> 