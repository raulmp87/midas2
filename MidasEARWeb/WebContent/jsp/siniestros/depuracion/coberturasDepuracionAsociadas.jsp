<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<tree id="0">	
	<s:if test="%{!coberturasAsociadas.isEmpty()}">
		<s:iterator value="coberturasAsociadas" var="secciones">		
			<item text="<s:property value="seccion.nombreComercial" escapeHtml="false" escapeXml="true"/>" 
				id="SEC_<s:property value="seccion.idToSeccion" />" 
				nocheckbox="true" child="1">
				<s:if test="%{!coberturas.isEmpty()}">
					<s:iterator value="coberturas" var="cobertura" >
						<item text="<s:property value="nombreCobertura"  escapeHtml="false" escapeXml="true"/>"
							id="COB_<s:property value="coberturaSeccionDTO.coberturaDTO.idToCobertura"/>-SEC_<s:property value="seccion.idToSeccion"/>-CVECAL_<s:property value="claveSubCalculo"/>"
							nocheckbox="true" child="0" />
					</s:iterator>
				</s:if>
			</item>
		</s:iterator>
	</s:if>
</tree>
