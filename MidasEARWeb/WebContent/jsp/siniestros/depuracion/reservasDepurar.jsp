<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/historicoDepuracion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/configuracionDepuracion.js'/>"></script>

<style type="text/css">
.dhxcont_main_content {
	background: white;
}
</style>



<script type="text/javascript">
	var mostrarListadoReservasDepurarPath = '<s:url action="mostrarListadoReservasParaDepurar" namespace="/siniestros/depuracion/reserva/historico"/>';
	var ejecutarDepuracionPath = '<s:url action="ejecutarDepuracion" namespace="/siniestros/depuracion/reserva/historico"/>';
</script>

	<s:form id="reservasDepurarForm" >	
		
		<s:hidden id="h_idConfigDepuracionReserva" name="idConfigDepuracionReserva"></s:hidden>
		<s:hidden id="h_depuracionReservaId" name="depuracionReservaId"></s:hidden>
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.siniestros.depuracion.reservasDepurar.title"/>	
		</div>	
		<br/>
		<div id="indicadorReservasDepurar"></div>
		<div id="reservasDepurarGrid" class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br/>
		<br/>
		<br/>
</s:form>
	<br/>
	<table width="95%">
	<tr>
	<td>
		<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
			<a href="javascript: void(0);" onclick="javascript:verHistoricoDepuracionReserva(<s:property value="idConfigDepuracionReserva" escapeHtml="true" escapeXml="true"/>);"> 
				<s:text name="midas.boton.cerrar" /> 
				<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
			</a>
		</div>	
		<div class="btn_back w120" style="display: inline; float: right;"  >
			<a href="javascript: void(0);" onclick="javascript:exportarExcelReservaDepurar();">
				<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
			</a>
		</div>
		<div id="btn_cerrar" class="btn_back w140" style="display: none; float: right;" >
			<a href="javascript: void(0);" onclick="javascript:ejecutarDepuracion();"> 
				<s:text name="midas.siniestros.depuracion.ejectuar" /> 
			</a>
		</div>
					
	</td>			
	</tr>
	</table>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarReservasDepurar();
	});
</script>

