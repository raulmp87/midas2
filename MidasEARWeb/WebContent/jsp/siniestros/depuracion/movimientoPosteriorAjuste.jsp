<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.fuente {
    font-family: arial;
    font-size: 11px;
    color: #666666;
    font-weight: bold;
}

label.label {
    font-family: arial;
    font-size: 11px;
} 

</style>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/depuracion/movimientoPosteriorAjuste.js'/>"></script>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">


<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.depuracion.busquedaDeMovimientos"/></div>
<div id="contenedorFiltros">
	<s:form id="infoGeneralForm" >
	    <s:hidden name="filtroMovimientos.tipoServicio" id="h_tipo_servicio"></s:hidden>
		<table id="filtrosM2" border="0" style="width: 98%;">
			<tbody>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td class="fuente">
									<input id="ch_sparticular" type="checkbox" value="1" onclick="seleccionaCheckButton()">
										<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioParticular"/></input>
								</td>
							</tr>
							<tr>
								<td class="fuente">
									<input id="ch_sprivado" type="checkbox" value="2" onclick="seleccionaCheckButton()">
										<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioPublico"/></input>
								</td>
							</tr>
						</table>
					</td>
					<td class="fuente">
						<s:select id="s_oficina" 
							  name="filtroMovimientos.oficinaId"
							  key="midas.siniestros.depuracion.oficina"  labelposition="top" 
							  cssClass="cajaTextoM2 w150 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="lstOficinas" listKey="key" listValue="value"  /> 						
					</td>
					<td class="fuente">
						<s:textfield  id="txt_numPoliza" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.noDePoliza"
								name="filtroMovimientos.numeroPoliza"
								maxlength="20"/>
					</td>
					<td>
						<s:textfield  	id="txt_inciso" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w70"
							 	key = "midas.siniestros.depuracion.inciso"
								name="filtroMovimientos.numeroInciso"
								onkeypress="return soloNumeros(this, event, true)"
								maxlength="10"/>
					</td>
					<td>
						<s:textfield  	id="txt_numeroSiniestro" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w70"
							 	key = "midas.siniestros.depuracion.noDeSiniestro"
								name="filtroMovimientos.numeroSiniestro"
								onkeypress="return soloAlfanumericos(this, event, true)"
								maxlength="20"/>
					</td>
					<td>
						<s:textfield  	id="txt_numeroReporte" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w70"
							 	key = "midas.siniestros.depuracion.noDeReporte"
								name="filtroMovimientos.numeroReporte"
								onkeypress="return soloAlfanumericos(this, event, true)"
								maxlength="20"/>
					</td>					
				</tr>
				<tr>
					<td>
						<s:select id="s_cobertura" 
							  name="filtroMovimientos.idCoberturaClaveSubCalculo"
							  key="midas.siniestros.depuracion.cobertura"  labelposition="top" 
							  cssClass="cajaTextoM2 w250 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="listCoberturas" listKey="key" listValue="value"   />
					</td>
					<td>
						<s:select id="s_terminoSiniestro" 
							  name="filtroMovimientos.terminoSiniestro"
							  key="midas.siniestros.depuracion.terminoDeSiniestro"  labelposition="top" 
							  cssClass="cajaTextoM2 w250 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="lstTerminoSiniestro" listKey="key" listValue="value"  />
					</td>
					<td>
						<s:select id="s_conceptoDepuracion" 
							  name="filtroMovimientos.ConceptoId"
							  key="midas.siniestros.depuracion.conceptoDepuracion"  labelposition="top" 
							  cssClass="cajaTextoM2 w150 requerido"
						      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  	  list="lstConceptos" listKey="id" listValue="nombre"  />
					</td>
					<td colspan="5">
						<table border="0" width="100%">
							<tr>
								<td colspan="4" align="center" class="fuente">
									<s:text name="midas.siniestros.depuracion.fechaDeAjuste" />
								</td>
							</tr>
								<td align="right">
									<sj:datepicker name="filtroMovimientos.fechaInicio"
												key="midas.siniestros.depuracion.fechaDeAjuste.de"
									  labelposition="left" 
										changeMonth="true"
										 changeYear="true"				
										buttonImage="../img/b_calendario.gif"
										buttonImageOnly="true" 
					                			 id="dp_fechaDe"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w90"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>
								</td>
								<td align="left">
									<sj:datepicker name="filtroMovimientos.fechaFin"
												key="midas.siniestros.depuracion.fechaDeAjuste.hasta"
									  labelposition="left" 
										changeMonth="true"
										 changeYear="true"				
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" 
					                			 id="dp_fechaHasta"
										  maxlength="10" cssClass="txtfield cajaTextoM2 w90"
											   size="12"
										 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
									</sj:datepicker>
								</td>
							<tr>
							</tr>
						</table>
					</td>
				</tr>
				<tr>		
					<td colspan="7" align="right">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;" border="0">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="realizarBusqueda(true);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	
<br/>
<br/>
<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.depuracion.listadoMovimientos"/></div>

<div id="indicadorMovPosterior"></div>
<div id="listaDeMovimientosGrid"  class="dataGridConfigurationClass" style="width:98%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>
<table style="padding: 0px; width: 100%; margin: 0px; border: none;" width="98%">
		<tr>
			<td  class= "guardar">
				<div class="btn_back w130" style="display: inline; float: right;" >
					<a id="btn_excel" href="javascript: void(0);" onclick="javascript: exportarExcel();"> 
						<s:text name="midas.boton.exportarExcel" /> &nbsp;<img align="middle" border='0px' alt='Exportar' title='Exportar' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
					</a>
				</div>
			</td>							
		</tr>
</table>

<script type="text/javascript">
jQuery(document).ready(
		function(){
			//realizarBusqueda(false); 
		}
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
