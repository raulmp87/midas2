<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">

.fuente {
    font-family: arial;
    font-size: 11px;
}

</style>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/depuracion/movimientoPosteriorAjuste.js'/>"></script>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<div class="titulo" style="width: 98%;"><s:text name="midas.siniestros.depuracion.movimientosPosteriores"/></div>
<div id="contenedorFiltros">
	<s:form id="infoGeneralForm" >
		<table id="filtrosM2" border="0" style="width: 98%;">
			<tbody>
				<tr>
					<td>
						<s:textfield  id="t_oficina" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w200"
							 	key = "midas.siniestros.depuracion.oficina"
								name="detalleMovimiento.oficinaDesc"
								disabled = "true"/>
					</td>
					<td>
						<s:textfield  id="t_noSiniestro" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.noDeSiniestro"
								name="detalleMovimiento.numeroSiniestro"
								disabled = "true"/>
					</td>
					<td>
						<s:textfield  id="t_noReporte" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.noDeReporte"
								name="detalleMovimiento.numeroReporte"
								disabled = "true"/>
					</td>
					<td>
						<s:textfield  id="t_noPoliza" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.noDePoliza"
								name="detalleMovimiento.numeroPoliza"
								disabled = "true"/>
					</td>
					<td>
						<s:textfield  id="t_inciso" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w50"
							 	key = "midas.siniestros.depuracion.inciso"
								name="detalleMovimiento.numeroInciso"
								disabled = "true"/>
					</td>
				<tr>	
				<tr>
					<td>
						<s:textfield  id="t_contratante" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w250"
							 	key = "midas.siniestros.depuracion.contratante"
								name="detalleMovimiento.contratante"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_terminoSiniestro" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w250"
							 	key = "midas.siniestros.depuracion.terminoDeSiniestro"
								name="detalleMovimiento.terminoSiniestroDesc"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_conceptoDepuracion" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w200"
							 	key = "midas.siniestros.depuracion.conceptoDepuracion"
								name="detalleMovimiento.nombreConcepto"
								disabled = "true"/>	
					</td>
				</tr>
				<tr>
					<td>
						<s:textfield  id="t_cobertura" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w250"
							 	key = "midas.siniestros.depuracion.cobertura"
								name="detalleMovimiento.nombreCobertura"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_montoAjuste" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.montoAjuste"
							 	value="%{getText('struts.money.format',{detalleMovimiento.montoAjuste})}"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_fechaMovimiento" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.fechaDeMovimiento"
								name="detalleMovimiento.fechaAjuste"
								disabled = "true"/>	
					</td>
				</tr>
				<tr>
					<td>
						<s:textfield  id="t_causaAjuste" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w200"
							 	key = "midas.siniestros.depuracion.causaAjuste"
								name="detalleMovimiento.causaAjuste"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_reservaActual" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
							 	key = "midas.siniestros.depuracion.rservaActual"
								value="%{getText('struts.money.format',{detalleMovimiento.reservaActual})}"
								disabled = "true"/>	
					</td>
					<td colspan="2">
						<s:textfield  id="t_usuario" 
							 	cssClass="txtfield jQrestrict cajaTextoM2 w250"
							 	key = "midas.siniestros.depuracion.usuario"
								name="detalleMovimiento.nombreUsuario"
								disabled = "true"/>	
					</td>
				</tr>
				<tr>
					<td colspan="7" align="right">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;" border="0">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="cerrarDetalle();"> 
										<s:text name="midas.boton.cerrar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
				</tr>
			</tbody>
		</table>
		</s:form>
</div>	


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
