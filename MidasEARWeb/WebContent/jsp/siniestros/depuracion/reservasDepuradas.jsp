<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/historicoDepuracion.js'/>"></script>

<style type="text/css">
.dhxcont_main_content {
	background: white;
}
</style>



<script type="text/javascript">
	var mostrarListadoReservasDepuradasPath = '<s:url action="mostrarListadoReservasDepuradas" namespace="/siniestros/depuracion/reserva/historico"/>';
	var mostrarHistoricoPath = '<s:url action="mostrarHistorico" namespace="/siniestros/depuracion/reserva/historico"/>';
</script>

	<s:form id="historicoForm" >	
		<s:hidden id="h_depuracionReservaId" name="depuracionReservaId"></s:hidden>
		<s:hidden id="h_idConfigDepuracionReserva" name="idConfigDepuracionReserva"></s:hidden>
		<s:hidden id="h_depuracionReservaDetString" name="idsDepuracionDetalle"></s:hidden>
		
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.depuracion.reserva.depuradas.title"/>	
		</div>	
		<table id="agregar" border="0" style="width: 95%;">
			<tbody>
				<tr>
					<td align="left" >
						<b><s:text name="midas.depuracion.reserva.depuradas.numeroDepuracion"/></b>:	<s:property  value="%{depuracionReserva.numeroDepuracion}"/>
					</td>
					<td align="left">
						<b><s:text name="midas.depuracion.reserva.depuradas.ejecutoDepuracion"/></b>: <s:property value="%{depuracionReserva.codigoUsuarioEjecucion}"/>
					</td>
					<td align="left" >
						<b><s:text name="midas.depuracion.reserva.depuradas.fechaHoraDepuracion" /></b>: <s:date name="%{depuracionReserva.fechaEjecucion}" format="dd/MM/yyyy HH:mm"/>
					</td>
				</tr>
						
					
			</tbody>
		</table>
		<br/>
		<br/>
		<div id="indicadorReservasDepuradas"></div>
		<div id="reservasDepuradasGrid" class="dataGridConfigurationClass" style="width:95%;height:350px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br/>
</s:form>
	<br/>
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td  width="90%">
					<div class="btn_back w120" style="display: inline; float: right;"  >
						<a href="javascript: void(0);" onclick="javascript:exportarExcelReservaDepurada();">
							<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar a Excel' title='Exportar a Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
						</a>
					</div>
				</td>
				<td>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:regresarHistoricoDepuracionReserva();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
						</a>
					</div>
				</td>
				
			</tr>
		</tbody>
	</table>		
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarReservasDepuradas();
	});
</script>

