<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="numero" type="ro" width="70" sort="int"><s:text name="midas.depuracion.reserva.depuradas.numero"/></column>
		<column id="siniestro" type="ro" width="120" sort="str" align="center"><s:text name="midas.depuracion.reserva.depuradas.siniestro"/></column>
		<column id="poliza" type="ro" width="120" sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.poliza"/></column>
		<column id="inciso" type="ro" width="80" sort="int" align="center" ><s:text name="midas.depuracion.reserva.depuradas.inciso"/></column>
		<column id="folioPase" type="ro" width="120"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.folioPase"/></column>
		<column id="cobertura" type="ro" width="250"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.cobertura"/></column>
		<column id="contratante" type="ro" width="250"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.contratante"/></column>
		<column id="terminoAjuste" type="ro" width="180"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.terminoAjuste"/></column>
		<column id="nombreConfiguracion" type="ro" width="200"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.nombreConfiguracion"/></column>
		<column id="reservaActual" type="ro" width="120"  sort="int" align="center" ><s:text name="midas.depuracion.reserva.depuradas.reservaActual"/></column>
		<column id="reservaDepurada" type="ro" width="120"  sort="int" align="center" ><s:text name="midas.depuracion.reserva.depuradas.reservaDepurada"/></column>
		<column id="porcentajeDepurado" type="ro" width="80"  sort="int" align="center" ><s:text name="midas.depuracion.reserva.depuradas.porcentajeDepurado"/></column>
		<column id="depurado" type="ro" width="70"  sort="str" align="center" ><s:text name="midas.depuracion.reserva.depuradas.depurado"/></column>
	</head>			
	<s:iterator value="reservasList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="numero" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.siniestroCabina.numeroSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.poliza.numeroPolizaFormateada" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estimacionCobertura.folio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.nombreAsegurado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="terminoAjusteDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="depuracionReserva.configuracion.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="%{getText('struts.money.format',{reservaActual})}" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="%{getText('struts.money.format',{reservaDepurada})}" escapeHtml="true" escapeXml="true"/></cell>
			<s:if test="estatus==1">
				<cell><s:property value="depuracionReserva.configuracion.porcentaje" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:else>
				<cell></cell>
			</s:else>
			<s:if test="estatus==0">
				<cell><s:text name="midas.depuracion.reserva.depuradas.depurado.no" /></cell>
			</s:if>
			<s:else>
				<cell><s:text name="midas.depuracion.reserva.depuradas.depurado.si" /></cell>
			</s:else>
		</row>
	</s:iterator>	
</rows> 